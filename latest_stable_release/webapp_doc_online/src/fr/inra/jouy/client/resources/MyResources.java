package fr.inra.jouy.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;


public interface MyResources extends ClientBundle {
	
	public static final MyResources INSTANCE =  GWT.create(MyResources.class);

	// gss
	@Source("Insyght_doc_online.gss")
	public CssResource gss();
	
	//other
	@Source("start_video.JPG")
	ImageResource start_video();
			
	//logo
	@Source("MaIAGE_V3_logo_gris_bis.png")
	ImageResource imgLogoMIG();
	
	@Source("Logotype-INRA-Black-transparent.png")
	ImageResource imgLogoINRA();
	

	//ortho table
	@Source("img_homoBro_layout.JPG")
	ImageResource img_homoBro_layout();

	@Source("img_homoBro_infoAnnot.JPG")
	ImageResource img_homoBro_infoAnnot();

	@Source("img_homoBro_synteny2.jpg")
	ImageResource img_homoBro_synteny2();

	@Source("img_homoBro_offshoot.JPG")
	ImageResource img_homoBro_offshoot();

	@Source("img_homoBro_buildGeneSet.JPG")
	ImageResource img_homoBro_buildGeneSet();

	
	//annot compa
	@Source("img_AnnotCompa.jpg")
	ImageResource img_AnnotCompa();
	
	@Source("img_AnnotCompa_restrictOrga.JPG")
	ImageResource img_AnnotCompa_restrictOrga();

	@Source("img_AnnotCompa_restrictOrtho.JPG")
	ImageResource img_AnnotCompa_restrictOrtho();
	

	//genomic orga
	@Source("img_GenomicContext_BrowseSyntenies.jpg")
	ImageResource img_GenomicContext_BrowseSyntenies();
	
	@Source("img_GenomicContext_etc.jpg")
	ImageResource img_GenomicContext_etc();
	
	
	//interconnected views
	@Source("img_interconnectedViews_syntheny.png")
	ImageResource img_interconnectedViews_syntheny();
	
	@Source("img_interconnectedViews_gene.png")
	ImageResource img_interconnectedViews_gene();
	
			
	//symbols
	@Source("symbloGeneInsertionQ.jpeg")
	ImageResource symbloGeneInsertionQ();
	
	@Source("symblolGeneInsertionS.jpeg")
	ImageResource symblolGeneInsertionS();

	@Source("symbolsGeneHomo.jpeg")
	ImageResource symbolsGeneHomo();
	
	@Source("imgSymbolGRegIns.JPG")
	ImageResource symbolGRegIns();
	
	@Source("symbolsInverseSynteny.jpeg")
	ImageResource symbolsInverseSynteny();
	
	@Source("symbolsSynteny.jpeg")
	ImageResource symbolsSynteny();
	
	@Source("symbolCollision.JPG")
	ImageResource symbolCollision();
	
}
