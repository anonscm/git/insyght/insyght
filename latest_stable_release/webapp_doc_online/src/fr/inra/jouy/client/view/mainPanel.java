package fr.inra.jouy.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.resources.MyResources;

public class mainPanel extends Composite {

	private static mainPanelUiBinder uiBinder = GWT.create(mainPanelUiBinder.class);

	interface mainPanelUiBinder extends UiBinder<Widget, mainPanel> {
	}

	//other
	@UiField Image start_video;
	
	// ortholog table
	@UiField Image img_homoBro_layout;
	@UiField Image img_homoBro_infoAnnot;
	@UiField Image img_homoBro_synteny2;
	@UiField Image img_homoBro_offshoot;
	@UiField Image img_homoBro_buildGeneSet;
	

	//annota comparator
	@UiField Image img_AnnotCompa;
	@UiField Image img_AnnotCompa_restrictOrga;
	@UiField Image img_AnnotCompa_restrictOrtho;
	
	//genomic orga
	@UiField Image logo_GenomicContext_BrowseSyntenies;
	@UiField Image logo_GenomicContext_etc;

	//interconnected views
	@UiField Image img_interconnectedViews_syntheny;
	@UiField Image img_interconnectedViews_gene;
	
	//symbols
	@UiField Image imgSymbolGRegIns;
	@UiField Image imgSymbolsSynteny;
	@UiField Image imgSymbolsInverseSynteny;
	@UiField Image imgSymbloGeneInsertionQ;
	@UiField Image imgSymblolGeneInsertionS;
	@UiField Image imgSymbolsGeneHomo;
	@UiField Image imgSymbolCollision;
	
	public mainPanel() {
		initWidget(uiBinder.createAndBindUi(this));

		//other
		start_video.setResource(MyResources.INSTANCE.start_video());
		
		// ortholog table
		img_homoBro_layout.setResource(MyResources.INSTANCE.img_homoBro_layout());
		img_homoBro_infoAnnot.setResource(MyResources.INSTANCE.img_homoBro_infoAnnot());
		img_homoBro_synteny2.setResource(MyResources.INSTANCE.img_homoBro_synteny2());
		img_homoBro_offshoot.setResource(MyResources.INSTANCE.img_homoBro_offshoot());
		img_homoBro_buildGeneSet.setResource(MyResources.INSTANCE.img_homoBro_buildGeneSet());
		
		//annota comparator
		img_AnnotCompa.setResource(MyResources.INSTANCE.img_AnnotCompa());
		img_AnnotCompa_restrictOrga.setResource(MyResources.INSTANCE.img_AnnotCompa_restrictOrga());
		img_AnnotCompa_restrictOrtho.setResource(MyResources.INSTANCE.img_AnnotCompa_restrictOrtho());
		
		//genomic orga
		logo_GenomicContext_BrowseSyntenies.setResource(MyResources.INSTANCE.img_GenomicContext_BrowseSyntenies());
		logo_GenomicContext_etc.setResource(MyResources.INSTANCE.img_GenomicContext_etc());
		
		//interconnected views
		img_interconnectedViews_syntheny.setResource(MyResources.INSTANCE.img_interconnectedViews_syntheny());
		img_interconnectedViews_gene.setResource(MyResources.INSTANCE.img_interconnectedViews_gene());
		
		//symbols
		imgSymbolGRegIns.setResource(MyResources.INSTANCE.symbolGRegIns());
		imgSymbolsSynteny.setResource(MyResources.INSTANCE.symbolsSynteny());
		imgSymbolsInverseSynteny.setResource(MyResources.INSTANCE
				.symbolsInverseSynteny());
		imgSymbloGeneInsertionQ.setResource(MyResources.INSTANCE
				.symbloGeneInsertionQ());
		imgSymblolGeneInsertionS.setResource(MyResources.INSTANCE
				.symblolGeneInsertionS());
		imgSymbolsGeneHomo.setResource(MyResources.INSTANCE.symbolsGeneHomo());
		imgSymbolCollision.setResource(MyResources.INSTANCE.symbolCollision());
		
		
	}

	@UiHandler("start_video")
	void onStartVideoClick(ClickEvent e) {
		Window.open("http://www.dailymotion.com/video/x1s24uc_insyght-analysis-dispensable-genome-and-pathogenicity_tech", "insyght-analysis-dispensable-genome-and-pathogenicity", null);
	}
	
}
