package fr.inra.jouy.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.resources.MyResources;

public class HeaderView extends Composite {

	private static HeaderViewUiBinder uiBinder = GWT
			.create(HeaderViewUiBinder.class);

	interface HeaderViewUiBinder extends UiBinder<Widget, HeaderView> {
	}
	
	@UiField
	Image imgLogoMIG;
	@UiField
	Image imgLogoINRA;

	public HeaderView() {
		initWidget(uiBinder.createAndBindUi(this));
		imgLogoMIG.setResource(MyResources.INSTANCE.imgLogoMIG());
		imgLogoMIG.setTitle("http://maiage.jouy.inra.fr/?q=en");
		imgLogoINRA.setResource(MyResources.INSTANCE.imgLogoINRA());
		imgLogoINRA.setTitle("http://www.international.inra.fr/");
	}


	@UiHandler("imgLogoMIG")
	void onImgLogoMIGClick(ClickEvent e) {
		Window.open("http://maiage.jouy.inra.fr/?q=en", "MaIAGE", null);
	}

	@UiHandler("imgLogoINRA")
	void onImgLogoINRAClick(ClickEvent e) {
		Window.open("http://www.international.inra.fr/", "INRA", null);
	}
}
