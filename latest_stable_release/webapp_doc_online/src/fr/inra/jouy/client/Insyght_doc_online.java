package fr.inra.jouy.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;

import fr.inra.jouy.client.resources.MyResources;
import fr.inra.jouy.client.view.HeaderView;
import fr.inra.jouy.client.view.mainPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Insyght_doc_online implements EntryPoint {

	private final HeaderView headerView = new HeaderView();
	private final mainPanel mainPanelView = new mainPanel();
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
	    
		MyResources.INSTANCE.gss().ensureInjected();

		Window.enableScrolling(false);
		Window.setMargin("0px");
	
		DockLayoutPanel outer = new DockLayoutPanel(Unit.EM);
		outer.addNorth(headerView, 6);
		outer.add(mainPanelView);
		
		// Add the outer panel to the RootLayoutPanel, so that it will be
		// displayed.
		RootLayoutPanel root = RootLayoutPanel.get();
		root.add(outer);
		
	}
}
