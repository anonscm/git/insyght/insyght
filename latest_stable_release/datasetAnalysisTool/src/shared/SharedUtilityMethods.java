package shared;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.TreeMap;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

public class SharedUtilityMethods {
	
	public static void deleteFileIfExists(String outputFileToDelete_asString) throws IOException {
		File outputFile_asJavaIoFile = new File(outputFileToDelete_asString);
		if(outputFile_asJavaIoFile.exists()){
			outputFile_asJavaIoFile.delete();
			outputFile_asJavaIoFile.createNewFile();
		}
	}

	public static void printWarning(String warningIT, Path outputFileErrorsAndWarnings) throws IOException {
		ArrayList<String> warningLineToPrint = new ArrayList<>();
		warningLineToPrint.add(warningIT);
		Files.write(outputFileErrorsAndWarnings, warningLineToPrint, StandardCharsets.UTF_8,
				Files.exists(outputFileErrorsAndWarnings) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		//System.out.println(warningIT);
	}


	public static void printError(
			Exception ex
			, Path outputFileErrorsAndWarnings
			, String methodNameToReport
			) throws IOException {
		ArrayList<String> errorLinesToPrint = new ArrayList<>();
		errorLinesToPrint.add("Error in "+methodNameToReport);
		errorLinesToPrint.add(ex.getMessage());
		Files.write(outputFileErrorsAndWarnings, errorLinesToPrint, StandardCharsets.UTF_8,
				Files.exists(outputFileErrorsAndWarnings) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		System.out.println(ex.getMessage());
		ex.printStackTrace();
	}
	
	
	public static void initTreeMapPercentage(
			TreeMap<Integer, Long> tm_percentageIdentity_sent) {
		for (int i=0;i<101;i++) {
			tm_percentageIdentity_sent.put(i, 0L);
		}
		//System.err.println(tm_percentageIdentity_sent.size());
	}
	


	public static void addPercentageToSummaryLHM(
			double doubleSent
			, int multiplyingFactor
			, TreeMap<Integer, Long> tmSummary
			, String tmSummaryName
			//, int qGeneId
			//, int sGeneId
			, int incrementValue
			//, Path outputFileErrorsAndWarnings
			) throws Exception {
		int keyIT = (int) Math.round(doubleSent * multiplyingFactor);
//		NumberFormat formatter = new DecimalFormat("0.#####E0");
//		System.err.println("addPercentageToSummaryLHM doubleSent="+formatter.format(doubleSent)+" ; roundedInt="+keyIT);
		if (tmSummary.containsKey(keyIT)) {
			Long longCountIt = tmSummary.get(keyIT);
			longCountIt = longCountIt + incrementValue;
			tmSummary.put(keyIT, longCountIt);
//			if (keyIT <= 4) {
//				String warningIT = "Warning in method addPercentageToSummaryLHM "+tmSummaryName+" : the percentage is "+keyIT+" for the qGeneId = "+qGeneId+" - sGeneId = "+sGeneId+"";
//				printWarning(warningIT, outputFileErrorsAndWarnings);
//			}
		} else {
			throw new Exception("Error in addPercentageIdentityToSummaryTM : The keyIT "+ keyIT
					+ " was not found as key of lhmSummary :"+tmSummaryName
					+ " ; doubleSent = "+doubleSent
					+ " ; multiplyingFactor = "+multiplyingFactor
					);
		}
	}
	


	public static void addScoreToSummaryLHM(
			double scoreSent
			, TreeMap<Integer, Long> tmSummary
			, String tmSummaryName
			, int incrementValue
			) throws Exception {
		int roundedScoreIT = (int) Math.round(scoreSent);
		if (tmSummary.containsKey(roundedScoreIT)) {
			Long longCountIt = tmSummary.get(roundedScoreIT);
			longCountIt = longCountIt + incrementValue;
			tmSummary.put(roundedScoreIT, longCountIt);
		} else {
			Long lToStore = (long) incrementValue;
			tmSummary.put(roundedScoreIT, lToStore);
		}
	}

	
	
	public static void fillUpForContinuousIntDiscretValueAsKeys(
			TreeMap<Integer, Long> tmToFill
			, int lowestKey
			, int highestKey
			) {
		for (int i=lowestKey;i<=highestKey;i++) {
			if (tmToFill.containsKey(i)) {
				//do nothing
			} else {
				tmToFill.put(i, 0L);
			}
		}		
	}
	
	
}
