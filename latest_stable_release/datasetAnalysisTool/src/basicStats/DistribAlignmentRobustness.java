package basicStats;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.queriesTable.QueriesTableHomologs;
import fr.inra.jouy.server.queriesTable.QueriesTableOrganisms;
import fr.inra.jouy.shared.pojos.databaseMapping.GeneMatchItem;
import shared.SharedUtilityMethods;


public class DistribAlignmentRobustness {


	static String methodNameToReport = "DistribAlignmentRobustness";
	private class Option {
	     String flag, opt;
	     public Option(String flag, String opt) { this.flag = flag; this.opt = opt; }
	}
	public enum MethodToRun { 
		identityLenghtCoverageEvalue
		//, 
	}

	public static String outputDir = System.getProperty("user.home"); // default
	public static int restrictToQOrgaIdsWithLastDigit = -1;
	
	//script scope variables
	//identityLenghtCoverageEvalue
	static TreeMap<Integer, Long> tm_identityLenghtCoverageEvalue_percentageIdentity2NumberOrthoBDBH = new TreeMap<>();
	static TreeMap<Integer, Long> tm_identityLenghtCoverageEvalue_percentageIdentity2NumberHomolNonBDBH = new TreeMap<>();
	static TreeMap<Integer, Long> tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberOrthoBDBH = new TreeMap<>();
	static TreeMap<Integer, Long> tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberHomolNonBDBH = new TreeMap<>();
	static TreeMap<Integer, Long> tm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH = new TreeMap<>();
	static TreeMap<Integer, Long> tm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH = new TreeMap<>();
	static TreeMap<Integer, Long> tm_identityLenghtCoverageEvalue_score2NumberOrthoBDBH = new TreeMap<>();
	static TreeMap<Integer, Long> tm_identityLenghtCoverageEvalue_score2NumberHomolNonBDBH = new TreeMap<>();
	static Integer counterEValue0_numberOrthoBDBH = 0;
	static Integer counterEValue0_numberHomolNonBDBH = 0;
	static Integer counterEValue0_01to0_05_numberOrthoBDBH = 0;
	static Integer counterEValue0_01to0_05_numberHomolNonBDBH = 0;
	static Integer counterEValueGreater0_05_numberOrthoBDBH = 0;
	static Integer counterEValueGreater0_05_numberHomolNonBDBH = 0;
	

	
	private static String printUsage() {
		String usage = "";
		usage += "\nProgram "+methodNameToReport;
		usage += "\nThe supported arguments are :";
		List<MethodToRun> enumList = Arrays.asList(MethodToRun.class.getEnumConstants());
		for (MethodToRun methodToRunIT : enumList) {
			usage += "\n\t-"+methodToRunIT.toString()+" {ON/OFF}";
		}
		usage += "\n\t-outputDir {path to output directory}";
		usage += "\n\t-restrictToQOrgaIdsWithLastDigit {one digit}";
		usage += "\nOnly one argument regarding the analysis to run is supported at a time";
		return usage;
	}
//
//	private static void deleteFileIfExists(String outputFileToDelete_asString) throws IOException {
//		File outputFile_asJavaIoFile = new File(outputFileToDelete_asString);
//		if(outputFile_asJavaIoFile.exists()){
//			outputFile_asJavaIoFile.delete();
//			outputFile_asJavaIoFile.createNewFile();
//		}
//	}
	


	
	public static void main(String[] args) throws IOException  {
		

		long milli = System.currentTimeMillis();
		long milliPrint2 = -1;
		milliPrint2 = System.currentTimeMillis() - milli;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateStart = new Date();
		System.out.println("Starting "+methodNameToReport+" at "+dateFormat.format(dateStart));
		

		DistribAlignmentRobustness dar = new DistribAlignmentRobustness();
		ArrayList<String> argsList = new ArrayList<String>();  
		ArrayList<Option> optsList = new ArrayList<Option>();
		ArrayList<String> doubleOptsList = new ArrayList<String>();

	    for (int i = 0; i < args.length; i++) {
	        switch (args[i].charAt(0)) {
	        case '-':
	            if (args[i].length() < 2)
	                throw new IllegalArgumentException("Not a valid argument: "+args[i]);
	            if (args[i].charAt(1) == '-') {
	                if (args[i].length() < 3)
	                    throw new IllegalArgumentException("Not a valid argument: "+args[i]);
	                // --opt
	                doubleOptsList.add(args[i].substring(2, args[i].length()));
	            } else {
	                if (args.length-1 == i)
	                    throw new IllegalArgumentException("Expected arg after: "+args[i]);
	                // -opt
	                optsList.add(dar.new Option(args[i], args[i+1]));
	                i++;
	            }
	            break;
	        default:
	            // arg
	            argsList.add(args[i]);
	            break;
	        }
	    }

	    ArrayList<MethodToRun> alMethodToRunIT = new ArrayList<>();
	    boolean printUsage = false;
	    
	    for (String argIT : argsList) {
	    	switch (argIT) {
	        case "-help":
	        	printUsage = true;
	            break;
	        default:
	            // arg
	        	printUsage = true;
	        }
	    }
	    for (String doubleOptsArgIT : doubleOptsList) {
	    	switch (doubleOptsArgIT) {
	        case "--help":
	        	printUsage = true;
	            break;
	        default:
	            // arg
	        	printUsage = true;
	        }
	    }
	    
	    
	    if (printUsage) {
	    	printUsage();
	    	System.exit(0);
	    }

	    for (Option optionIT : optsList) {
	    	switch (optionIT.flag) {
	        case "-identityLenghtCoverageEvalue":
	        	if (optionIT.opt.matches("^ON$")) {
		        	alMethodToRunIT.add(MethodToRun.identityLenghtCoverageEvalue);
	        	}
	            break;
	        case "-outputDir":
	        	//if (optionIT.opt.matches("^ON$")) {
	        		outputDir = optionIT.opt;
	        	//}
	            break;
	        case "-restrictToQOrgaIdsWithLastDigit":
	        	if (optionIT.opt.matches("^\\d$")) {
	        		restrictToQOrgaIdsWithLastDigit = Integer.parseInt(optionIT.opt);
	        	} else {
	        		throw new IllegalArgumentException("The flag -restrictToQOrgaIdsWithLastDigit must be a single digit."+printUsage());
	        	}
	            break;
	        default:
	            // arg
	        	throw new IllegalArgumentException("The flag "+optionIT.flag+" is not supported."+printUsage());
	        }
	    }

	    if (alMethodToRunIT.size() > 1) {
	    	throw new IllegalArgumentException("Only one argument at a time is supported."+printUsage());
	    } else if (alMethodToRunIT.isEmpty()) {
	    	throw new IllegalArgumentException("No flag was provided to run method."+printUsage());
	    }
	    
	    MethodToRun methodToRunIT = null;
	    for (MethodToRun methodToRunArgs : alMethodToRunIT) {
	    	methodToRunIT = methodToRunArgs;
	    }
	    
	    switch (methodToRunIT) {
        case identityLenghtCoverageEvalue:
        	identityLenghtCoverageEvalue();
            break;
        default:
            // arg
        	throw new IllegalArgumentException("The method "+methodToRunIT.toString()+" is not yet implemented."+printUsage());
        }
	    

		milliPrint2 = System.currentTimeMillis() - milli;
		Date dateEnd = new Date();
		System.out.println("Done "+methodNameToReport+" at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.");

	}



	private static void identityLenghtCoverageEvalue() throws IOException {

		/*
		 * Graph output : see file /home/tlacroix/montages_reseau/home_maiage/Desktop/documents/Projets/Insyght/databases/db_public/bacteria_2017/analyse/basic_stats/mock_data_graph_basic_stat.xlsx
		 */
		
		String methodNameToReport = "identityLenghtCoverageEvalue";
		
		String extensionPostFile = "";
		if (restrictToQOrgaIdsWithLastDigit >= 0) {
			extensionPostFile = "_restrictToQOrgaIdsWithLastDigit"+restrictToQOrgaIdsWithLastDigit;
		}
		String outputFilePercentageIdentity_asString = outputDir+"/result_identityLenghtCoverageEvalue_percentageIdentity"+extensionPostFile+".txt";
		String outputFileEValue_asString = outputDir+"/result_identityLenghtCoverageEvalue_eValue"+extensionPostFile+".txt";
		String outputFileScore_asString = outputDir+"/result_identityLenghtCoverageEvalue_score"+extensionPostFile+".txt";
		String outputFileErrorsAndWarnings_asString = outputDir+"/identityLenghtCoverageEvalue_errorsAndWarnings"+extensionPostFile+".txt";
		
		Path outputFilePercentageIdentity = Paths.get(outputFilePercentageIdentity_asString);
		Path outputFileEValue = Paths.get(outputFileEValue_asString);
		Path outputFileScore = Paths.get(outputFileScore_asString);
		Path outputFileErrorsAndWarnings = Paths.get(outputFileErrorsAndWarnings_asString);
		
		Connection conn = null;
		
		try {

			SharedUtilityMethods.deleteFileIfExists(outputFilePercentageIdentity_asString);
			SharedUtilityMethods.deleteFileIfExists(outputFileEValue_asString);
			SharedUtilityMethods.deleteFileIfExists(outputFileScore_asString);
			SharedUtilityMethods.deleteFileIfExists(outputFileErrorsAndWarnings_asString);
			
			String titleAnalyse = "Distribution of %identity, %lenght coverage, score, and e-value for the ortholog BDBH and the homolog non BDBH";
			
			ArrayList<String> alHeaderPercentageIdentity = new ArrayList<>();
			alHeaderPercentageIdentity.add("%");
			alHeaderPercentageIdentity.add("Number of ortho BDBH for a given % identity");
			alHeaderPercentageIdentity.add("Number of homol non BDBH for a given % identity");
			alHeaderPercentageIdentity.add("Number of ortho BDBH for a given % length coverage");
			alHeaderPercentageIdentity.add("Number of homol non BDBH for a given % length coverage");

			// min_evalue by default = 5e-2
			ArrayList<String> alHeaderEValue = new ArrayList<>();
			alHeaderEValue.add("E-value (0, <=1e-180, <=1e-160, <=1e-140, ..., <=1e-2, <=5e-2, >5e-2)");
			alHeaderEValue.add("Number of ortho BDBH within a given range of e-value");
			alHeaderEValue.add("Number of homol non BDBH within a given range of e-value");
			
			ArrayList<String> alHeaderScore = new ArrayList<>();
			alHeaderScore.add("Score");
			alHeaderScore.add("Number of ortho BDBH for a given score");
			alHeaderScore.add("Number of homol non BDBH for a given score");
			
			
			long milli = System.currentTimeMillis();
			long milliPrint2 = -1;
			milliPrint2 = System.currentTimeMillis() - milli;
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date dateStart = new Date();
			System.out.println("Starting "+methodNameToReport+" at "+dateFormat.format(dateStart));
						
			conn = DatabaseConf.getConnection_db();

			// init treeMap percentage
			SharedUtilityMethods.initTreeMapPercentage(tm_identityLenghtCoverageEvalue_percentageIdentity2NumberOrthoBDBH);
			SharedUtilityMethods.initTreeMapPercentage(tm_identityLenghtCoverageEvalue_percentageIdentity2NumberHomolNonBDBH);
			SharedUtilityMethods.initTreeMapPercentage(tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberOrthoBDBH);
			SharedUtilityMethods.initTreeMapPercentage(tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberHomolNonBDBH);
//			// init treeMap evalue
//			initTreeMapEvalue(lhm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH);
//			initTreeMapEvalue(lhm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH);

			// get all organisms_id of database
			ArrayList<Integer> alOrganismIds = QueriesTableOrganisms.getAlOrganismIds(conn, true, true);//onlyPublic, onlyWithElement
			Collections.sort(alOrganismIds);
			
			Long counter = 1L;
			Long countGMTTreated = 0L;
			Long countGMTRank1Treated = 0L;
			Long countGMTRank2Treated = 0L;
			Long numberCombination = (long) ( alOrganismIds.size() * ( alOrganismIds.size() - 1 ) / 2); //rq +1 if we do organism against itself
			OUTERLOOP: for (int i = 0; i < alOrganismIds.size() ; i++) {
				int qOrganismId = alOrganismIds.get(i);
				
				if (i == ( alOrganismIds.size() - 1) ) {
					//last iteration
				} else {
					if (restrictToQOrgaIdsWithLastDigit >= 0) {
						if ( ( qOrganismId % 10 ) != restrictToQOrgaIdsWithLastDigit ) { //modulo 10 give the last digit
							counter += alOrganismIds.size() - 1 - i;
							System.out.println("( "+(counter-1)+" / "+numberCombination+" ) : skipping qOrganismId = "+qOrganismId+" because of the flag -restrictToQOrgaIdsWithLastDigit "+restrictToQOrgaIdsWithLastDigit
									+ ".");
							continue OUTERLOOP;
						}
					}
				}

				for (int j = i+1; j < alOrganismIds.size() ; j++) {
					int sOrganismId = alOrganismIds.get(j);
					
//					if (counter == 3) {
//						break OUTERLOOP;
//					}
										
					System.out.println("( "+counter+" / "+numberCombination+" ) : starting qOrganismId = "+qOrganismId+" - sOrganismId = "+sOrganismId);
					
					//for each qOrganismId - sOrganismId
				
					ArrayList<GeneMatchItem> alLGMIIT = QueriesTableHomologs.getAlGeneMatchItem_QAndS_RankPercIdentityPercLengthCoverageEValue_withQOrganismIdAndSOrganismId(
							conn
							, qOrganismId
							, sOrganismId
							, false
							);
					
										
					
					// for each CDS of the table homology, get the rank, % identity, % length coverage, e-value
					//some pairwise comparison de not yield any significative aligniments
//					if (alLGMIIT.isEmpty()) {
//						throw new Exception("Error after QueriesTableHomologs.getAlGeneMatchItem_QAndS_RankPercIdentityPercLengthCoverageEValue_withQOrganismIdAndSOrganismId :"
//								+ " alLGMIIT is empty for qOrganismId = "+ qOrganismId
//								+ " and sOrganismId = "+sOrganismId
//								);
//					}
					
					for (GeneMatchItem gmiIT : alLGMIIT) {
						countGMTTreated++;
						// depending on the rank (1=BDBH, 2=homolog non BDBH)
						if ( gmiIT.getRank() == 1 ) {
							// depending on % identity, store in tm_identityLenghtCoverageEvalue_percentageIdentity2NumberOrthoBDBH
							SharedUtilityMethods.addPercentageToSummaryLHM(
									gmiIT.getIdentity() //double doubleSent
									, 1 //multiplyingFactor
									, tm_identityLenghtCoverageEvalue_percentageIdentity2NumberOrthoBDBH //tmSummary
									, "percentageIdentity2NumberOrthoBDBH" //tmSummaryName
									//, gmiIT.getQGeneId() //qGeneId
									//, gmiIT.getSGeneId() //sGeneId
									, 2 //incrementValue
									//, outputFileErrorsAndWarnings //outputFileErrorsAndWarnings
									);
							
							// depending on % length coverage, store in tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberOrthoBDBH
							SharedUtilityMethods.addPercentageToSummaryLHM(
									gmiIT.getQAlignFrac()
									, 100
									, tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberOrthoBDBH
									, "percentageLengthCoverage2NumberOrthoBDBH"
									//, gmiIT.getQGeneId()
									//, gmiIT.getSGeneId()
									, 1
									//, outputFileErrorsAndWarnings
									);
							SharedUtilityMethods.addPercentageToSummaryLHM(
									gmiIT.getSAlignFrac()
									, 100, tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberOrthoBDBH
									, "percentageLengthCoverage2NumberOrthoBDBH"
									//, gmiIT.getQGeneId()
									//, gmiIT.getSGeneId()
									, 1
									//, outputFileErrorsAndWarnings
									);
							// depending on % e-value, store in tm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH
							addEvalueToSummaryLHM(
									gmiIT.getEValue()
									, true
									, tm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH
									, "evalue2NumberOrthoBDBH"
									, gmiIT.getQGeneId()
									, gmiIT.getSGeneId()
									, 2
									, outputFileErrorsAndWarnings
									);
							SharedUtilityMethods.addScoreToSummaryLHM(
									gmiIT.getScore()
									, tm_identityLenghtCoverageEvalue_score2NumberOrthoBDBH
									, "score2NumberOrthoBDBH"
									, 2
									);
							countGMTRank1Treated++;
						} else if ( gmiIT.getRank() == 2 ) {
							// depending on % identity, store in tm_identityLenghtCoverageEvalue_percentageIdentity2NumberHomolNonBDBH
							SharedUtilityMethods.addPercentageToSummaryLHM(
									gmiIT.getIdentity()
									, 1
									, tm_identityLenghtCoverageEvalue_percentageIdentity2NumberHomolNonBDBH
									, "percentageIdentity2NumberHomolNonBDBH"
									//, gmiIT.getQGeneId()
									//, gmiIT.getSGeneId()
									, 2
									//, outputFileErrorsAndWarnings
									);
							// depending on % length coverage, store in tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberHomolNonBDBH
							SharedUtilityMethods.addPercentageToSummaryLHM(
									gmiIT.getQAlignFrac()
									, 100
									, tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberHomolNonBDBH
									, "percentageLengthCoverage2NumberHomolNonBDBH"
									//, gmiIT.getQGeneId()
									//, gmiIT.getSGeneId()
									, 1
									//, outputFileErrorsAndWarnings
									);
							SharedUtilityMethods.addPercentageToSummaryLHM(
									gmiIT.getSAlignFrac()
									, 100
									, tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberHomolNonBDBH
									, "percentageLengthCoverage2NumberHomolNonBDBH"
									//, gmiIT.getQGeneId()
									//, gmiIT.getSGeneId()
									, 1
									//, outputFileErrorsAndWarnings
									);
							// depending on % e-value, store in tm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH
							addEvalueToSummaryLHM(
									gmiIT.getEValue()
									, false
									, tm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH
									, "evalue2NumberHomolNonBDBH"
									, gmiIT.getQGeneId()
									, gmiIT.getSGeneId()
									, 2
									, outputFileErrorsAndWarnings
									);
							SharedUtilityMethods.addScoreToSummaryLHM(
									gmiIT.getScore()
									, tm_identityLenghtCoverageEvalue_score2NumberHomolNonBDBH
									, "score2NumberHomolNonBDBH"
									, 2
									);
							countGMTRank2Treated++;
						} else {
							throw new Exception("The GeneMatchItem "
									+ " ; QGeneId = "+gmiIT.getQGeneId()
									+ " ; SGeneId = "+gmiIT.getSGeneId()
									+ " ; has a Rank != 1 or 2 :"+gmiIT.getRank()
									);
						}
					}

					counter++;

				}
			}
			
			System.out.println("Done treating  "+countGMTTreated+" GeneMatchItem. countGMTRank1Treated = "+countGMTRank1Treated+" ; countGMTRank2Treated = "+countGMTRank2Treated);

			//deal with printing output file Percentage Identity
			{
				ArrayList<String> linesToPrintPercentageIdentity = new ArrayList<>();
				linesToPrintPercentageIdentity.add(titleAnalyse);
				linesToPrintPercentageIdentity.add(String.join("\t", alHeaderPercentageIdentity));
				ArrayList<Integer> keyListLHMPercentage = new ArrayList<Integer>(tm_identityLenghtCoverageEvalue_percentageIdentity2NumberOrthoBDBH.keySet());
				Collections.sort(keyListLHMPercentage);
				for(int i = 0; i < keyListLHMPercentage.size(); i++) {
					/*
					alHeaderPercentageIdentity.add("%");
					alHeaderPercentageIdentity.add("Number of ortho BDBH with a given range of % identity");
					alHeaderPercentageIdentity.add("Number of homol non BDBH with a given range of % identity");
					alHeaderPercentageIdentity.add("Number of ortho BDBH with a given range of % length coverage");
					alHeaderPercentageIdentity.add("Number of homol non BDBH with a given range of % length coverage");
					 */
				    int keyPercentageIT = keyListLHMPercentage.get(i);
				    Long countNumberPercentageIdentity2NumberOrthoBDBH = tm_identityLenghtCoverageEvalue_percentageIdentity2NumberOrthoBDBH.get(keyPercentageIT);
				    Long countNumberPercentageIdentity2NumberHomolNonBDBH = tm_identityLenghtCoverageEvalue_percentageIdentity2NumberHomolNonBDBH.get(keyPercentageIT);
				    Long countNumberPercentageLengthCoverage2NumberOrthoBDBH = tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberOrthoBDBH.get(keyPercentageIT);
				    Long countNumberPercentageLengthCoverage2NumberHomolNonBDBH = tm_identityLenghtCoverageEvalue_percentageLengthCoverage2NumberHomolNonBDBH.get(keyPercentageIT);
				    String lineIT = 
				    		keyPercentageIT
			        		+"\t"+countNumberPercentageIdentity2NumberOrthoBDBH
			        		+"\t"+countNumberPercentageIdentity2NumberHomolNonBDBH
			        		+"\t"+countNumberPercentageLengthCoverage2NumberOrthoBDBH
			        		+"\t"+countNumberPercentageLengthCoverage2NumberHomolNonBDBH
			        		;
				    linesToPrintPercentageIdentity.add(lineIT);
				}
				Files.write(outputFilePercentageIdentity, linesToPrintPercentageIdentity, StandardCharsets.UTF_8,
						Files.exists(outputFilePercentageIdentity) ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE);
			}
			
			
			//deal with printing output file Evalue
			{
				ArrayList<String> linesToPrintEvalue = new ArrayList<>();
				linesToPrintEvalue.add(titleAnalyse);
				linesToPrintEvalue.add(String.join("\t", alHeaderEValue));
				String lineEValue0IT = 
				    		"0"
			        		+"\t"+counterEValue0_numberOrthoBDBH
			        		+"\t"+counterEValue0_numberHomolNonBDBH
			        		;
				linesToPrintEvalue.add(lineEValue0IT);
				// filler for treemap to have intermediate empty values as 0
				//get lowest negative value as key
				ArrayList<Integer> keyListLHMEvalue = new ArrayList<Integer>(tm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH.keySet());
				Collections.sort(keyListLHMEvalue);
				int lowestKey = keyListLHMEvalue.get(0);
				keyListLHMEvalue = new ArrayList<Integer>(tm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH.keySet());
				Collections.sort(keyListLHMEvalue);
				if (keyListLHMEvalue.get(0) < lowestKey) {
					lowestKey = keyListLHMEvalue.get(0);
				}
				SharedUtilityMethods.fillUpForContinuousIntDiscretValueAsKeys(tm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH, lowestKey, -2);
				SharedUtilityMethods.fillUpForContinuousIntDiscretValueAsKeys(tm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH, lowestKey, -2);
	//			Set<Integer> keysFromLhm_identityLenghtCoverageEvalue_evalueBothUnion = tm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH.keySet();
	//			Set<Integer> keysFromLhm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH = tm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH.keySet();
	//			keysFromLhm_identityLenghtCoverageEvalue_evalueBothUnion.addAll(keysFromLhm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH);
				keyListLHMEvalue = new ArrayList<Integer>(tm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH.keySet());
				Collections.sort(keyListLHMEvalue);
				for(int i = 0; i < keyListLHMEvalue.size(); i++) {
					/*
					alHeaderEValue.add("E-value (0, <=1e-180, <=1e-160, <=1e-140, ..., <=1e-2, >1e-2)");
					alHeaderEValue.add("Number of ortho BDBH with a given range of e-value");
					alHeaderEValue.add("Number of homol non BDBH with a given range of e-value");
					 */
				    int keyEvalueIT = keyListLHMEvalue.get(i);
				    Long countNumberEvalue2NumberOrthoBDBH = tm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH.get(keyEvalueIT);
				    if (countNumberEvalue2NumberOrthoBDBH == null) {
				    	throw new Exception("Error in deal with printing output file Evalue : The keyEvalueIT of tm_identityLenghtCoverageEvalue_evalue2NumberOrthoBDBH "+ keyEvalueIT
								+ " has countNumberEvalue2NumberOrthoBDBH == null"
								);
				    }
				    Long countNumberEvalue2NumberHomolNonBDBH = tm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH.get(keyEvalueIT);
				    if (countNumberEvalue2NumberHomolNonBDBH == null) {
				    	throw new Exception("Error in deal with printing output file Evalue : The keyEvalueIT of tm_identityLenghtCoverageEvalue_evalue2NumberHomolNonBDBH "+ keyEvalueIT
								+ " has countNumberEvalue2NumberOrthoBDBH == null"
								);
				    }
				    String lineIT = 
				    		keyEvalueIT
			        		+"\t"+countNumberEvalue2NumberOrthoBDBH
			        		+"\t"+countNumberEvalue2NumberHomolNonBDBH
			        		;
				    linesToPrintEvalue.add(lineIT);
				}
				String lineEValueEValue0_01to0_05IT = 
			    		"<=5e-2"
		        		+"\t"+counterEValue0_01to0_05_numberOrthoBDBH
		        		+"\t"+counterEValue0_01to0_05_numberHomolNonBDBH
		        		;
				linesToPrintEvalue.add(lineEValueEValue0_01to0_05IT);
				String lineEValueGreater0_05IT = 
			    		">5e-2"
		        		+"\t"+counterEValueGreater0_05_numberOrthoBDBH
		        		+"\t"+counterEValueGreater0_05_numberHomolNonBDBH
		        		;
				linesToPrintEvalue.add(lineEValueGreater0_05IT);
				
				Files.write(outputFileEValue, linesToPrintEvalue, StandardCharsets.UTF_8,
						Files.exists(outputFileEValue) ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE);
			}
			

			//deal with printing output file Score
			ArrayList<String> linesToPrintScore = new ArrayList<>();
			linesToPrintScore.add(titleAnalyse);
			linesToPrintScore.add(String.join("\t", alHeaderScore));
			// filler for treemap to have intermediate empty values as 0
			//get lowest negative value as key
			ArrayList<Integer> keyListTmScore = new ArrayList<Integer>(tm_identityLenghtCoverageEvalue_score2NumberOrthoBDBH.keySet());
			Collections.sort(keyListTmScore);
			int lowestKey = keyListTmScore.get(0);
			int highestKey = keyListTmScore.get(keyListTmScore.size()-1);
			keyListTmScore = new ArrayList<Integer>(tm_identityLenghtCoverageEvalue_score2NumberHomolNonBDBH.keySet());
			Collections.sort(keyListTmScore);
			if (keyListTmScore.get(0) < lowestKey) {
				lowestKey = keyListTmScore.get(0);
			}
			if (keyListTmScore.get(keyListTmScore.size()-1) > highestKey) {
				highestKey = keyListTmScore.get(keyListTmScore.size()-1);
			}
			SharedUtilityMethods.fillUpForContinuousIntDiscretValueAsKeys(tm_identityLenghtCoverageEvalue_score2NumberOrthoBDBH, lowestKey, highestKey);
			SharedUtilityMethods.fillUpForContinuousIntDiscretValueAsKeys(tm_identityLenghtCoverageEvalue_score2NumberHomolNonBDBH, lowestKey, highestKey);
			keyListTmScore = new ArrayList<Integer>(tm_identityLenghtCoverageEvalue_score2NumberOrthoBDBH.keySet());
			Collections.sort(keyListTmScore);
			for(int i = 0; i < keyListTmScore.size(); i++) {
			    int keyScoreIT = keyListTmScore.get(i);
			    Long countNumberScore2NumberOrthoBDBH = tm_identityLenghtCoverageEvalue_score2NumberOrthoBDBH.get(keyScoreIT);
			    if (countNumberScore2NumberOrthoBDBH == null) {
			    	throw new Exception("Error in deal with printing output file Score : The keyScoreIT of tm_identityLenghtCoverageEvalue_score2NumberOrthoBDBH "+ keyScoreIT
							+ " has countNumberEvalue2NumberOrthoBDBH == null"
							);
			    }
			    Long countNumberScore2NumberHomolNonBDBH = tm_identityLenghtCoverageEvalue_score2NumberHomolNonBDBH.get(keyScoreIT);
			    if (countNumberScore2NumberHomolNonBDBH == null) {
			    	throw new Exception("Error in deal with printing output file Score : The keyScoreIT of tm_identityLenghtCoverageEvalue_score2NumberHomolNonBDBH "+ keyScoreIT
							+ " has countNumberEvalue2NumberOrthoBDBH == null"
							);
			    }
			    String lineIT = 
			    		keyScoreIT
		        		+"\t"+countNumberScore2NumberOrthoBDBH
		        		+"\t"+countNumberScore2NumberHomolNonBDBH
		        		;
			    linesToPrintScore.add(lineIT);
				Files.write(outputFileScore, linesToPrintScore, StandardCharsets.UTF_8,
						Files.exists(outputFileScore) ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE);
			}

			milliPrint2 = System.currentTimeMillis() - milli;
			Date dateEnd = new Date();
			System.out.println("Done "+methodNameToReport+" at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.");
			System.out.println("Output files :");
			System.out.println("\t Percentage Identity identity and alignment coverage : "+outputFilePercentageIdentity_asString);
			System.out.println("\t Evalue : "+outputFileEValue_asString);
			System.out.println("\t Score : "+outputFileScore_asString);
			System.out.println("\t Errors and warnings : "+outputFileErrorsAndWarnings_asString);

		} catch (Exception ex) {
			SharedUtilityMethods.printError(ex, outputFileErrorsAndWarnings, methodNameToReport);

		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}
		
	}




	public static void addEvalueToSummaryLHM(
			Double eValueIT
			, boolean BDBH
			, TreeMap<Integer, Long> tmSummary
			, String lhmSummaryName
			, int qGeneId
			, int sGeneId
			, int incrementValue
			, Path outputFileErrorsAndWarnings
			) throws IOException {
		if (eValueIT == 0) {
			if (BDBH) {
				counterEValue0_numberOrthoBDBH = counterEValue0_numberOrthoBDBH + incrementValue;
			} else {
				counterEValue0_numberHomolNonBDBH = counterEValue0_numberHomolNonBDBH + incrementValue;
			}
		} else if (eValueIT > 0.05) {
			if (BDBH) {
				counterEValueGreater0_05_numberOrthoBDBH = counterEValueGreater0_05_numberOrthoBDBH + incrementValue;
			} else {
				counterEValueGreater0_05_numberHomolNonBDBH = counterEValueGreater0_05_numberHomolNonBDBH + incrementValue;
			}
			String warningIT = "Warning in method addEvalueToSummaryLHM : the evalue is "+eValueIT+" for the qGeneId = "+qGeneId+" - sGeneId = "+sGeneId+"";
			SharedUtilityMethods.printWarning(warningIT, outputFileErrorsAndWarnings);
		} else if (eValueIT > 0.01) {
			if (BDBH) {
				counterEValue0_01to0_05_numberOrthoBDBH = counterEValue0_01to0_05_numberOrthoBDBH + incrementValue;
			} else {
				counterEValue0_01to0_05_numberHomolNonBDBH = counterEValue0_01to0_05_numberHomolNonBDBH + incrementValue;
			}
		} else {
			Double log10eValueIT = Math.log10(eValueIT);
			int exponenteValueIT = (int) Math.floor(log10eValueIT);
//			NumberFormat formatter = new DecimalFormat("0.#####E0");
//			System.err.println("addEvalueToSummaryLHM eValueIT="+formatter.format(eValueIT)+" ; exponenteValueIT="+exponenteValueIT);
//			log10(11) = 1.041
//			log10(10) = 1
//			log10(1) = 0
//			log10(0.11) = -0.95
//			log10(0.1) OR log10(1E-1) = -1
//			log10(0.09) = -1.04
//			log10(0.051) = -1.29
//			log10(0.05) = -1.30
//			log10(0.049) = -1.309
//			log10(0.02) = -1.69
//			log10(0.01) = -2
//			log10(0.0099) = -2.004
//			log10(0.009) = -2.04
//			log10(0.003) = -2.52
//			log10(0.001) = -3
//			log10(0.000001) = -6
//			log10(2.3E-10) = -9.63
//			log10(0.0000000001) OR log10(1E-10) = -10
//			log10(0) = undefined
			if (tmSummary.containsKey(exponenteValueIT)) {
				Long longCountIt = tmSummary.get(exponenteValueIT);
				longCountIt = longCountIt + incrementValue;
				tmSummary.put(exponenteValueIT, longCountIt);
			} else {
				Long lToStore = (long) incrementValue;
				tmSummary.put(exponenteValueIT, lToStore);
			}
		}
	}


	



	

}
