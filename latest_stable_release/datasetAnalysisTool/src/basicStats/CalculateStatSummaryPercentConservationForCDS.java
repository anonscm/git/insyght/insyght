package basicStats;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.TreeMap;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.queriesTable.mutlipleTables.ComparativeGenomics;
import fr.inra.jouy.shared.pojos.databaseMapping.GeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import shared.SharedUtilityMethods;

/*
// how to create executable jars from eclipse
https://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2Ftasks%2Ftasks-37.htm
1.0 Select this script in package explorer
1.1 From the menu bar's File menu, select Export.
2. Expand the Java node and select Runnable JAR file. Click Next.
3. In the  Opens the Runnable JAR export wizard Runnable JAR File Specification page, select a 'Java Application' launch configuration to use to create a runnable JAR.
4. In the Export destination field, either type or click Browse to select a location for the JAR file : datasetAnalysisTool/executableJars/basicStat
5. Select an appropriate library handling strategy : Extract required librairies into generated JAR
6. Optionally, you can also create an ANT script to quickly regenerate a previously created runnable JAR file : No
 */


//NOT DONE !!! analyse with r script /home/tlacroix/montages_reseau/home_maiage/Desktop/documents/Projets/ICE-IME/comparison_genomic_analysis/R/CalculateStatSummaryPercentConservationForCDS.R


public class CalculateStatSummaryPercentConservationForCDS {


	static String methodNameToReport = "calculateStatSummaryPercentConservationForCDS";
	private class Option {
		String flag, opt;
		public Option(String flag, String opt) { this.flag = flag; this.opt = opt; }
	}

	//script scope variables
	public static String outputDir = System.getProperty("user.home"); // default
	public static LinkedHashMap<String,ArrayList<String>> hmInputCDSFromScope2IncludeExcludeFeatured = new LinkedHashMap<>();
	public static LinkedHashMap<String,ArrayList<String>> hmGenomesComparisonScope2IncludeExcludeFeatured = new LinkedHashMap<>();
	public static boolean VERBOSE = true; // default
	public static boolean doNotPerformGenomicComparisonAnalysis = false; // default
	public static boolean detailedOutput = false; // default
	//detailedOutput = true
	static TreeMap<Integer, Long> tm_percentGenomesWithPresenceCDSConservedDuringEvolution = new TreeMap<>();
	static TreeMap<Integer, Long> tm_percentGenomesWithPresenceCDSSyntenyRelationship = new TreeMap<>();
	static TreeMap<Integer, Long> tm_percentIdentityAlignment = new TreeMap<>();
	static TreeMap<Integer, Long> tm_percentQuerySubjectLenghtCoverageAlignment = new TreeMap<>();
	static ArrayList<GeneItem> alMaxDoublePercentGenomesWithPresenceCDSConservedDuringEvolution = new ArrayList<>();
	static ArrayList<GeneItem> alMinDoublePercentGenomesWithPresenceCDSConservedDuringEvolution = new ArrayList<>();
	static ArrayList<GeneItem> alMaxDoublePercentGenomesWithPresenceCDSSyntenyRelationship = new ArrayList<>();
	static ArrayList<GeneItem> alMinDoublePercentGenomesWithPresenceCDSSyntenyRelationship = new ArrayList<>();
	static ArrayList<GeneItem> alMaxDoublePercentIdentityAlignment = new ArrayList<>();
	static ArrayList<GeneItem> alMinDoublePercentIdentityAlignment = new ArrayList<>();
	static ArrayList<GeneItem> alMaxDoublePercentQuerySubjectLenghtCoverageAlignment = new ArrayList<>();
	static ArrayList<GeneItem> alMinDoublePercentQuerySubjectLenghtCoverageAlignment = new ArrayList<>();
	static TreeMap<Integer, Long> tm_organismAvgPercentGenomesWithPresenceCDSConservedDuringEvolution = new TreeMap<>();
	static TreeMap<Integer, Long> tm_organismAvgPercentGenomesWithPresenceCDSSyntenyRelationship = new TreeMap<>();
	static TreeMap<Integer, Long> tm_organismAvgPercentIdentityAlignment = new TreeMap<>();
	static TreeMap<Integer, Long> tm_organismAvgPercentQuerySubjectLenghtCoverageAlignment = new TreeMap<>();
	
	private static String getUsageArgsAsString() {

		String usage = "";
		usage += "\nProgram "+methodNameToReport;
		usage += "\nThe supported arguments are :";
		usage += "\n\t-outputDir {path to output directory} : default : user.home. If multiple -outputDir, only takes into consideration the last one. If no -outputDir argument is provided, create a single outputDir with default values";

		usage += "\n\t-input_CDS_from_scope_name {STRING} : create a new \"input CDS from\" scope with default values. \"Input_CDS\" scope arguments between this input_CDS_from_scope_name and the next scope_name (\"input_CDS\" or \"genomes_comparison\") or the end of the list will be applied to this input_CDS_from_scope_name."
				+ " If no -input_CDS_from_scope_name argument is provided, create a input_CDS_from_scope_name with default values."
				+ " If multiple -input_CDS_from_scope_name argument are provided, concatenate all the CDSs from the different scope to be the input set of CDSs";
		usage += "\n\t-input_CDS_from_scope_taxon_id_include {\"all\", DIGIT_taxon_id} :  default : \"all\"";
		usage += "\n\t-input_CDS_from_scope_taxon_id_exclude {\"none\", DIGIT_taxon_id} :  default : \"none\"";
		
		usage += "\n\t-genomes_comparison_scope_name {STRING} : create a new \"genomes_comparison\" scope with default values. \"Genomes_comparison\" scope arguments between this genomes_comparison_scope_name and the next scope_name (\"input_CDS\" or \"genomes_comparison\") or the end of the list will be applied to this genomes_comparison_scope_name."
				+ " If no -genomes_comparison_scope_name argument is provided, create a genomes_comparison_scope_name with default values."
				+ " No multiple \"-genomes_comparison_scope_name\" are allowed.";
		usage += "\n\t-genomes_comparison_scope_taxon_id_include {\"species\", \"all\", DIGIT_taxon_id} :  default : \"all\"";
		usage += "\n\t-genomes_comparison_scope_taxon_id_exclude {\"species\", \"none\", DIGIT_taxon_id} :  default : \"none\"";

		usage += "\n\t-VERBOSE {ON/OFF} : whether or not this script should print information as it is running (Default ON)";
		usage += "\n\t-doNotPerformGenomicComparisonAnalysis {ON/OFF} : whether or not this script should not perform the genomic comparison analysis (Default OFF)";
		usage += "\n\t-detailedOutput {ON/OFF} : if ON, this script will output all values for CDSs line by line ; if OFF, it will print a summary output (Default OFF)";
		return usage;
	}

	public static void main(String[] args) throws Exception {

		long milli = System.currentTimeMillis();
		long milliPrint2 = -1;
		milliPrint2 = System.currentTimeMillis() - milli;

		CalculateStatSummaryPercentConservationForCDS csspcfc = new CalculateStatSummaryPercentConservationForCDS();
		//ArrayList<String> argsList = new ArrayList<String>();  
		ArrayList<Option> optsList = new ArrayList<Option>();
		//ArrayList<String> doubleOptsList = new ArrayList<String>();

		for (int i = 0; i < args.length; i++) {
			switch (args[i].charAt(0)) {
			case '-':
				if (args[i].length() < 2)
					throw new IllegalArgumentException("Not a valid argument: "+args[i]+"\n"+getUsageArgsAsString());
				if (args[i].charAt(1) == '-') {
					if (args[i].compareTo("--help")==0) {
						System.out.println(getUsageArgsAsString());
						System.exit(0);
					}
					if (args[i].length() < 3) {
						throw new IllegalArgumentException("Not a valid argument: "+args[i]+"\n"+getUsageArgsAsString());
					}
					
					// --opt
					//doubleOptsList.add(args[i].substring(2, args[i].length()));
					throw new IllegalArgumentException("Not a valid argument: "+args[i]+"\n"+getUsageArgsAsString());
				} else {
					if (args[i].compareTo("-help")==0) {
						System.out.println(getUsageArgsAsString());
						System.exit(0);
					}
					if (args.length-1 == i) {
						throw new IllegalArgumentException("Expected arg after: "+args[i]+"\n"+getUsageArgsAsString());
					}
					// -opt
					optsList.add(csspcfc.new Option(args[i], args[i+1]));
					
					//System.err.println(args[i]+" ; "+args[i+1]);
					
					i++;
					
				}
				break;
			default:
				// arg
				//argsList.add(args[i]);
				throw new IllegalArgumentException("Not a valid argument: "+args[i]+"\n"+getUsageArgsAsString());
				
				//System.err.println(args[i]+"; args[i].charAt(0)="+args[i].charAt(0)+"; NOT "+args[i+1]);
				//
				//break;
			}
		}
		

		String currentScopeName = "";
		boolean currentScopeNameIsInputCDSFrom = true;
		for (Option optionIT : optsList) {
			switch (optionIT.flag) {
			case "-outputDir":
				//if (optionIT.opt.matches("^ON$")) {
				outputDir = optionIT.opt;
				//}
				break;
			case "-input_CDS_from_scope_name":
				currentScopeNameIsInputCDSFrom = true;
				if (hmInputCDSFromScope2IncludeExcludeFeatured.containsKey(optionIT.opt)) {
					throw new IllegalArgumentException("The value "+optionIT.opt+" for the flag "+optionIT.flag+" is duplicated."+getUsageArgsAsString());
				} else {
					currentScopeName = optionIT.opt;
					ArrayList<String> alIncludeExcludeFeaturedIT = new ArrayList<>();
					alIncludeExcludeFeaturedIT.add("all"); // idx 0 : -input_CDS_from_scope_taxon_id_include {\"all\", DIGIT_taxon_id} :  default : \"all\""
					alIncludeExcludeFeaturedIT.add("none"); // idx 1 : -input_CDS_from_scope_taxon_id_exclude {\"none\", DIGIT_taxon_id} :  default : \"none\"";
					//alIncludeExcludeFeaturedIT.add("featured"); // idx 2 : -scope_featured_or_main_to_consider ["featured", "main_list"] // default : featured
					hmInputCDSFromScope2IncludeExcludeFeatured.put(optionIT.opt, alIncludeExcludeFeaturedIT);
				}
				break;
			case "-input_CDS_from_scope_taxon_id_include":
				if (currentScopeName.isEmpty()) {
					throw new IllegalArgumentException("The flag -input_CDS_from_scope_taxon_id_include "+optionIT.opt+" must be set after its related flag -input_CDS_from_scope_name."+getUsageArgsAsString());
				} else if ( ! currentScopeNameIsInputCDSFrom) {
					throw new IllegalArgumentException("The flag -input_CDS_from_scope_taxon_id_include "+optionIT.opt+" must not be set after a flag -genomes_comparison_scope_name."+getUsageArgsAsString());
				} else {
					if (optionIT.opt.compareTo("all") == 0
							//|| optionIT.opt.compareTo("species") == 0
							//|| optionIT.opt.compareTo("none") == 0
							|| optionIT.opt.matches("^\\d+$")
							) {
						if (hmInputCDSFromScope2IncludeExcludeFeatured.containsKey(currentScopeName)) {
							ArrayList<String> alIncludeExcludeFeaturedIT = hmInputCDSFromScope2IncludeExcludeFeatured.get(currentScopeName);
							alIncludeExcludeFeaturedIT.set(0, optionIT.opt); // idx 0 : -input_CDS_from_scope_taxon_id_include {\"all\", DIGIT_taxon_id} :  default : \"all\""
						} else {
							throw new IllegalArgumentException("Error with the flag -input_CDS_from_scope_taxon_id_include "+optionIT.opt+". No scope name "+currentScopeName+" has been found to be related with a flag -input_CDS_from_scope_name"+getUsageArgsAsString());
						}
					} else {
						throw new IllegalArgumentException("The value of the flag -input_CDS_from_scope_taxon_id_include "+optionIT.opt+" is incorrect. The value must be one of the following : all, DIGIT_taxon_id."+getUsageArgsAsString());
					}
				}
				break;
			case "-input_CDS_from_scope_taxon_id_exclude":
				if (currentScopeName.isEmpty()) {
					throw new IllegalArgumentException("The flag -input_CDS_from_scope_taxon_id_exclude "+optionIT.opt+" must be set after its related flag -input_CDS_from_scope_name."+getUsageArgsAsString());
				} else if ( ! currentScopeNameIsInputCDSFrom) {
					throw new IllegalArgumentException("The flag -input_CDS_from_scope_taxon_id_exclude "+optionIT.opt+" must not be set after a flag -genomes_comparison_scope_name."+getUsageArgsAsString());
				} else {
					if (optionIT.opt.compareTo("none") == 0
							//|| optionIT.opt.compareTo("all") == 0
							//|| optionIT.opt.compareTo("species") == 0
							|| optionIT.opt.matches("^\\d+$")
							) {
						if (hmInputCDSFromScope2IncludeExcludeFeatured.containsKey(currentScopeName)) {
							ArrayList<String> alIncludeExcludeFeaturedIT = hmInputCDSFromScope2IncludeExcludeFeatured.get(currentScopeName);
							alIncludeExcludeFeaturedIT.set(1, optionIT.opt); // idx 1 : -input_CDS_from_scope_taxon_id_exclude {\"none\", DIGIT_taxon_id} :  default : \"none\"";
						} else {
							throw new IllegalArgumentException("Error with the flag -input_CDS_from_scope_taxon_id_exclude "+optionIT.opt+". No scope name "+currentScopeName+" has been found to be related with a flag -input_CDS_from_scope_name"+getUsageArgsAsString());
						}
					} else {
						throw new IllegalArgumentException("The value of the flag -input_CDS_from_scope_taxon_id_exclude "+optionIT.opt+" is incorrect. The value must be one of the following : none, DIGIT_taxon_id."+getUsageArgsAsString());
					}
				}
				break;
			case "-genomes_comparison_scope_name":
				currentScopeNameIsInputCDSFrom = false;
				if (hmGenomesComparisonScope2IncludeExcludeFeatured.containsKey(optionIT.opt)) {
					throw new IllegalArgumentException("The value "+optionIT.opt+" for the flag "+optionIT.flag+" is duplicated."+getUsageArgsAsString());
				} else {
					currentScopeName = optionIT.opt;
					ArrayList<String> alIncludeExcludeFeaturedIT = new ArrayList<>();
					alIncludeExcludeFeaturedIT.add("all"); // idx 0 : -genomes_comparison_scope_taxon_id_include {\"species\", \"all\", DIGIT_taxon_id} :  default : \"all\"";
					alIncludeExcludeFeaturedIT.add("none"); // idx 1 : -genomes_comparison_scope_taxon_id_exclude {\"species\", \"none\", DIGIT_taxon_id} :  default : \"none\"";
					hmGenomesComparisonScope2IncludeExcludeFeatured.put(optionIT.opt, alIncludeExcludeFeaturedIT);
				}
				break;
			case "-genomes_comparison_scope_taxon_id_include":
				if (currentScopeName.isEmpty()) {
					throw new IllegalArgumentException("The flag -genomes_comparison_scope_taxon_id_include "+optionIT.opt+" must be set after its related flag -genomes_comparison_scope_name."+getUsageArgsAsString());
				} else if ( currentScopeNameIsInputCDSFrom) {
					throw new IllegalArgumentException("The flag -genomes_comparison_scope_taxon_id_include "+optionIT.opt+" must not be set after a flag -genomes_comparison_scope_name."+getUsageArgsAsString());
				} else {
					if (optionIT.opt.compareTo("all") == 0
							|| optionIT.opt.compareTo("species") == 0
							//|| optionIT.opt.compareTo("none") == 0
							|| optionIT.opt.matches("^\\d+$")
							) {
						if (hmGenomesComparisonScope2IncludeExcludeFeatured.containsKey(currentScopeName)) {
							ArrayList<String> alIncludeExcludeFeaturedIT = hmGenomesComparisonScope2IncludeExcludeFeatured.get(currentScopeName);
							alIncludeExcludeFeaturedIT.set(0, optionIT.opt); // idx 0 : -genomes_comparison_scope_taxon_id_include {\"species\", \"all\", DIGIT_taxon_id} :  default : \"all\"";
						} else {
							throw new IllegalArgumentException("Error with the flag -genomes_comparison_scope_taxon_id_include "+optionIT.opt+". No scope name "+currentScopeName+" has been found to be related with a flag -genomes_comparison_scope_name"+getUsageArgsAsString());
						}
					} else {
						throw new IllegalArgumentException("The value of the flag -genomes_comparison_scope_taxon_id_include "+optionIT.opt+" is incorrect. The value must be one of the following : all, DIGIT_taxon_id."+getUsageArgsAsString());
					}
				}
				break;
			case "-genomes_comparison_scope_taxon_id_exclude":
				if (currentScopeName.isEmpty()) {
					throw new IllegalArgumentException("The flag -genomes_comparison_scope_taxon_id_exclude "+optionIT.opt+" must be set after its related flag -genomes_comparison_scope_name."+getUsageArgsAsString());
				} else if ( currentScopeNameIsInputCDSFrom) {
					throw new IllegalArgumentException("The flag -genomes_comparison_scope_taxon_id_exclude "+optionIT.opt+" must not be set after a flag -genomes_comparison_scope_name."+getUsageArgsAsString());
				} else {
					if (optionIT.opt.compareTo("none") == 0
							//|| optionIT.opt.compareTo("organism") == 0
							|| optionIT.opt.compareTo("species") == 0
							|| optionIT.opt.matches("^\\d+$")
							) {
						if (hmGenomesComparisonScope2IncludeExcludeFeatured.containsKey(currentScopeName)) {
							ArrayList<String> alIncludeExcludeFeaturedIT = hmGenomesComparisonScope2IncludeExcludeFeatured.get(currentScopeName);
							alIncludeExcludeFeaturedIT.set(1, optionIT.opt); // idx 1 : -genomes_comparison_scope_taxon_id_exclude {\"species\", \"none\", DIGIT_taxon_id} :  default : \"none\"";
						} else {
							throw new IllegalArgumentException("Error with the flag -genomes_comparison_scope_taxon_id_exclude "+optionIT.opt+". No scope name "+currentScopeName+" has been found to be related with a flag -genomes_comparison_scope_name"+getUsageArgsAsString());
						}
					} else {
						throw new IllegalArgumentException("The value of the flag -genomes_comparison_scope_taxon_id_exclude "+optionIT.opt+" is incorrect. The value must be one of the following : none, DIGIT_taxon_id."+getUsageArgsAsString());
					}
				}
				break;
			case "-VERBOSE":
				if (optionIT.opt.compareTo("ON") == 0) {
					VERBOSE = true;
				} else if (optionIT.opt.compareTo("OFF") == 0) {
					VERBOSE = false;
				} else {
					throw new IllegalArgumentException("The value of the flag -VERBOSE "+optionIT.opt+" is incorrect. The value must be one of the following : ON, OFF."+getUsageArgsAsString());
				}
				break;
			case "-doNotPerformGenomicComparisonAnalysis":
				if (optionIT.opt.compareTo("ON") == 0) {
					doNotPerformGenomicComparisonAnalysis = true;
				} else if (optionIT.opt.compareTo("OFF") == 0) {
					doNotPerformGenomicComparisonAnalysis = false;
				} else {
					throw new IllegalArgumentException("The value of the flag -doNotPerformGenomicComparisonAnalysis "+optionIT.opt+" is incorrect. The value must be one of the following : ON, OFF."+getUsageArgsAsString());
				}
				break;
			case "-detailedOutput":
				if (optionIT.opt.compareTo("ON") == 0) {
					detailedOutput = true;
				} else if (optionIT.opt.compareTo("OFF") == 0) {
					detailedOutput = false;
				} else {
					throw new IllegalArgumentException("The value of the flag -detailedOutput "+optionIT.opt+" is incorrect. The value must be one of the following : ON, OFF."+getUsageArgsAsString());
				}
				break;
			default:
				// arg
				throw new IllegalArgumentException("The flag "+optionIT.flag+" is not supported."+getUsageArgsAsString());
			}
		}
		
		if (hmInputCDSFromScope2IncludeExcludeFeatured.isEmpty()) {
			ArrayList<String> alIncludeExcludeFeaturedIT = new ArrayList<>();
			alIncludeExcludeFeaturedIT.add("all"); // idx 0 : -input_CDS_from_scope_taxon_id_include {\"all\", DIGIT_taxon_id} :  default : \"all\""
			alIncludeExcludeFeaturedIT.add("none"); // idx 1 : -input_CDS_from_scope_taxon_id_exclude {\"none\", DIGIT_taxon_id} :  default : \"none\"";
			//alIncludeExcludeFeaturedIT.add("featured"); // idx 2 : -scope_featured_or_main_to_consider ["featured", "main_list"] // default : featured
			hmInputCDSFromScope2IncludeExcludeFeatured.put("DefaultnputCDSFrom", alIncludeExcludeFeaturedIT);
		}
		if (hmGenomesComparisonScope2IncludeExcludeFeatured.isEmpty()) {
			ArrayList<String> alIncludeExcludeFeaturedIT = new ArrayList<>();
			alIncludeExcludeFeaturedIT.add("all"); // idx 0 : -genomes_comparison_scope_taxon_id_include {\"species\", \"all\", DIGIT_taxon_id} :  default : \"all\"";
			alIncludeExcludeFeaturedIT.add("none"); // idx 1 : -genomes_comparison_scope_taxon_id_exclude {\"species\", \"none\", DIGIT_taxon_id} :  default : \"none\"";
			hmGenomesComparisonScope2IncludeExcludeFeatured.put("DefaultGenomesComparison", alIncludeExcludeFeaturedIT);
		}
		if (hmGenomesComparisonScope2IncludeExcludeFeatured.size() >= 2) {
			throw new IllegalArgumentException("No multiple \"-genomes_comparison_scope_name\" are allowed."+getUsageArgsAsString());
		}
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateStart = new Date();
	    if (VERBOSE) {
	    	System.out.println("Starting "+methodNameToReport+" at "+dateFormat.format(dateStart));
	    	System.out.println("");
	    }
	    
	    if (VERBOSE) {
	    	//recap of your options
	    	System.out.println(
	    			"Running script with the following options : "
	    			+ "\n\t-outputDir : " + outputDir
	    			+ "\n\t-VERBOSE : " + VERBOSE
	    			+ "\n\t-detailedOutput : " + detailedOutput
	    			+ "\n\t-doNotPerformGenomicComparisonAnalysis : " + doNotPerformGenomicComparisonAnalysis
	    			);
	    	for (Entry<String, ArrayList<String>> entry : hmInputCDSFromScope2IncludeExcludeFeatured.entrySet()) {
				String scopeIT = entry.getKey();
				ArrayList<String> alIncludeExcludeFeaturedIT = entry.getValue();
				System.out.println(
		    			"\t-input_CDS_from_scope_name : " + scopeIT
		    			+ " ; -input_CDS_from_scope_taxon_id_include : " + alIncludeExcludeFeaturedIT.get(0)
		    			+ " ; -input_CDS_from_scope_taxon_id_exclude : " + alIncludeExcludeFeaturedIT.get(1)
		    			//+ " ; -scope_featured_or_main_to_consider : " + alIncludeExcludeFeaturedIT.get(2)
		    			);
			}
	    	for (Entry<String, ArrayList<String>> entry : hmGenomesComparisonScope2IncludeExcludeFeatured.entrySet()) {
				String scopeIT = entry.getKey();
				ArrayList<String> alIncludeExcludeFeaturedIT = entry.getValue();
				System.out.println(
		    			"\t-genomes_comparison_scope_name : " + scopeIT
		    			+ " ; -genomes_comparison_scope_taxon_id_include : " + alIncludeExcludeFeaturedIT.get(0)
		    			+ " ; -genomes_comparison_scope_taxon_id_exclude : " + alIncludeExcludeFeaturedIT.get(1)
		    			//+ " ; -scope_featured_or_main_to_consider : " + alIncludeExcludeFeaturedIT.get(2)
		    			);
			}
	    	System.out.println("");
	    }
	    
	    launchCalculateStatSummaryPercentConservationForCDS();
	    
	    milliPrint2 = System.currentTimeMillis() - milli;
		Date dateEnd = new Date();
		if (VERBOSE) {
			System.out.println("Done "+methodNameToReport+" at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.");
		}
		
		
	} // main(String[] args)

	private static void launchCalculateStatSummaryPercentConservationForCDS() throws Exception {

		String methodNameToReport = "launchCalculateStatSummaryPercentConservationForCDS";

		long milli = System.currentTimeMillis();
		long milliPrint2 = -1;
		milliPrint2 = System.currentTimeMillis() - milli;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateStart = new Date();
		
		 if (VERBOSE) {
			 System.out.println("Starting "+methodNameToReport+" at "+dateFormat.format(dateStart));
		    	System.out.println("");
		 }

		Connection connComparaisonDB = null;
		Connection connTaxoDB = null;

		try {

			connComparaisonDB = DatabaseConf.getConnection_db();
			connTaxoDB = DatabaseConf.getConnection_taxo();
			//connICEDB = DatabaseConfICEDB.getConnection();

			String outputFile_asString = outputDir+"/resultAnalysis_calculateStatSummaryPercentConservationForCDS.txt";
			//ArrayList<String> linesToPrint = new ArrayList<>();


			File outputFile_asJavaIoFile = new File(outputFile_asString);
			if(outputFile_asJavaIoFile.exists()){
				outputFile_asJavaIoFile.delete();
				//try {
				outputFile_asJavaIoFile.createNewFile();
				//} catch (IOException e) {
				//	e.printStackTrace();
				//}
			}
			Path outputFile = Paths.get(outputFile_asString);
			SharedUtilityMethods.deleteFileIfExists(outputFile_asString);
			
			//print header
			ArrayList<String> headerLinesToPrint = new ArrayList<>();
			if (detailedOutput) {
				headerLinesToPrint.add(String.join("\t", getHeaderDetailedOutputAsAlString()));
			} else {
				headerLinesToPrint.add(String.join("\t", getHeaderSummaryOutputAsAlString()));
				SharedUtilityMethods.initTreeMapPercentage(tm_percentGenomesWithPresenceCDSConservedDuringEvolution);
				SharedUtilityMethods.initTreeMapPercentage(tm_percentGenomesWithPresenceCDSSyntenyRelationship);
				SharedUtilityMethods.initTreeMapPercentage(tm_percentIdentityAlignment);
				SharedUtilityMethods.initTreeMapPercentage(tm_percentQuerySubjectLenghtCoverageAlignment);
				SharedUtilityMethods.initTreeMapPercentage(tm_organismAvgPercentGenomesWithPresenceCDSConservedDuringEvolution);
				SharedUtilityMethods.initTreeMapPercentage(tm_organismAvgPercentGenomesWithPresenceCDSSyntenyRelationship);
				SharedUtilityMethods.initTreeMapPercentage(tm_organismAvgPercentIdentityAlignment);
				SharedUtilityMethods.initTreeMapPercentage(tm_organismAvgPercentQuerySubjectLenghtCoverageAlignment);
			}

			Files.write(outputFile, headerLinesToPrint, StandardCharsets.UTF_8,
					Files.exists(outputFile) ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE); //, StandardOpenOption.WRITE
			
			ArrayList<LightOrganismItem> alAllOrganismsInDB = fr.inra.jouy.server.queriesTable.QueriesTableOrganisms.getAlLightOrganismItems_orderByScoreAscIfProvided(
					connComparaisonDB
					, null //ArrayList<Integer> limitToAlOrgaIds
					, null //ArrayList<Integer> excludeAlOrgaIds
					, null //HashMap<Integer, Integer> hmOrgaId2fScoreToStore
					, false //boolean setScoreTo0IfNotProvided
					, false //boolean orderByScoreAsc
					, true //boolean onlyPublic
					, true //boolean onlyWithElement
					);


			HashSet<Integer> hsOrgaIdsInputCDSFrom = new HashSet<>();
			for (Entry<String, ArrayList<String>> entryhmInputCDSFromScope2 : hmInputCDSFromScope2IncludeExcludeFeatured.entrySet()) {
				String scopeNameIT = entryhmInputCDSFromScope2.getKey();
				ArrayList<String> alIncludeExcludeFeatured = entryhmInputCDSFromScope2.getValue();
				String scopeIncludeIT = alIncludeExcludeFeatured.get(0);
				String scopeExcludeIT = alIncludeExcludeFeatured.get(1);
				//String scopeFeaturedIT = alIncludeExcludeFeatured.get(2);

				HashSet<Integer> hsTaxonIdsFeatured = new HashSet<>();
				HashSet<Integer> hsTaxonIdsScopeInclude = fr.inra.jouy.server.queriesTable.QueriesTableTaxonomy.getHsTaxonIds_withScopeToConsider(
						connTaxoDB
						, scopeIncludeIT
						, -1
						);
				hsTaxonIdsFeatured.addAll(hsTaxonIdsScopeInclude);
				
				HashSet<Integer> hsTaxonIdsScopeExclude = fr.inra.jouy.server.queriesTable.QueriesTableTaxonomy.getHsTaxonIds_withScopeToConsider(
							connTaxoDB
							, scopeExcludeIT
							, -1
							);
				hsTaxonIdsFeatured.removeAll(hsTaxonIdsScopeExclude);
				
				HashSet<Integer> hsOrgaIdsFeaturedGenomes = new HashSet<>();
				for (LightOrganismItem loi_fromAlAllOrganismsInDB : alAllOrganismsInDB) {
//					if (loi_fromAlAllOrganismsInDB.getOrganismId() == loiIT.getOrganismId()) { 
//						//skip, ref organism
//					} else  
					if (hsTaxonIdsFeatured.contains(loi_fromAlAllOrganismsInDB.getTaxonId())) {
						hsOrgaIdsFeaturedGenomes.add(loi_fromAlAllOrganismsInDB.getOrganismId());
					}
				}
				
				hsOrgaIdsInputCDSFrom.addAll(hsOrgaIdsFeaturedGenomes);
				if (VERBOSE) {
			    	System.out.println(
			    				"Getting organisms for \"Input CDS\" scope "+scopeNameIT+" : "+hsOrgaIdsFeaturedGenomes.size() + " organisms found."
			    			);
			    }
			}	
			
			HashSet<LightOrganismItem> hsOrgaInputCDSFrom = new HashSet<>();
			for (LightOrganismItem loi_fromAlAllOrganismsInDB : alAllOrganismsInDB) {
//				if (loi_fromAlAllOrganismsInDB.getOrganismId() == loiIT.getOrganismId()) { 
//					//skip, ref organism
//				} else  
				if (hsOrgaIdsInputCDSFrom.contains(loi_fromAlAllOrganismsInDB.getOrganismId())) {
					hsOrgaInputCDSFrom.add(loi_fromAlAllOrganismsInDB);
				}
			}
			
			if (VERBOSE) {
		    	System.out.println(
		    				"A total of "+hsOrgaInputCDSFrom.size()+" organisms found have been found to be eligible for \"Input CDS\" scope."
		    			);
		    }
			
			int countOrgaAnalysed = 0;
			LOOP_ORGANISMS: for (LightOrganismItem loi_fromHsOrgaInputCDSFrom : hsOrgaInputCDSFrom) {
				countOrgaAnalysed++;
				if (VERBOSE) {
			    	System.out.println(
			    				"[Treating organism "+countOrgaAnalysed+"/"+hsOrgaInputCDSFrom.size()+"] : Starting with organism "+loi_fromHsOrgaInputCDSFrom.getFullName()
			    			);
			    }
				
				ArrayList<String> orgaLinesToPrint = new ArrayList<>();
				Double sumForOrgaPercentGenomesWithPresenceCDSConservedDuringEvolution = 0d;
				Double sumForOrgaPercentGenomesWithPresenceCDSSyntenyRelationship = 0d;
				Double sumForOrgaPercentIdentityAlignment = 0d;
				Double sumForOrgaPercentQuerySubjectLenghtCoverageAlignment = 0d;
				int sizeCDSInOrga = 0;
				
				ArrayList<Integer> alElementIdIT = fr.inra.jouy.server.queriesTable.QueriesTableElements.getAlElementIdWithOrganismId_optionalOrderBySizeDesc(
						connComparaisonDB
						, loi_fromHsOrgaInputCDSFrom.getOrganismId()
						, true
						, true
						);
						
				int countElementAnalysed = 0;
				for (Integer elementIdIT : alElementIdIT) {
					countElementAnalysed++;
					
					HashSet<Integer> hsElementIds = new HashSet<>();
					hsElementIds.add(elementIdIT);
					LightElementItem leiIT = fr.inra.jouy.server.queriesTable.QueriesTableElements.getLightElementItemWithElementId(
							connComparaisonDB
							, elementIdIT
							);
					
					if (VERBOSE) {
				    	System.out.println(
				    				"[Treating organism "+countOrgaAnalysed+"/"+hsOrgaInputCDSFrom.size()+", element "+countElementAnalysed+"/"+alElementIdIT.size()+"] : Starting with element "+leiIT.getAccession()
				    			);
				    }
					

					ArrayList<LightGeneItem> alGIIT = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAllLightGeneItemWithElementId(
							connComparaisonDB
							, elementIdIT
							);
					sizeCDSInOrga = alGIIT.size();

					int countGeneAnalysed = 0;
					for (LightGeneItem lgi_fromAlGIIT : alGIIT) {
						countGeneAnalysed++;
						if (VERBOSE) {
							System.out.println(
				    				"[Treating organism "+countOrgaAnalysed+"/"+hsOrgaInputCDSFrom.size()+", element "+countElementAnalysed+"/"+alElementIdIT.size()+", gene "+countGeneAnalysed+"/"+alGIIT.size()+"] : "
				    						+ "Starting with gene "+lgi_fromAlGIIT.getMostSignificantGeneNameAsStrippedText()
				    			);
					    }


						HashMap<String, HashSet<Integer>> hmScope2HsOrgaIdsFeaturedGenomes = new HashMap<>();
						for (Entry<String, ArrayList<String>> entryHmScope2 : hmGenomesComparisonScope2IncludeExcludeFeatured.entrySet()) {
							String scopeNameIT = entryHmScope2.getKey();
							ArrayList<String> alIncludeExcludeFeatured = entryHmScope2.getValue();
							String scopeIncludeIT = alIncludeExcludeFeatured.get(0);
							String scopeExcludeIT = alIncludeExcludeFeatured.get(1);
							//String scopeFeaturedIT = alIncludeExcludeFeatured.get(2);

							HashSet<Integer> hsTaxonIdsFeatured = new HashSet<>();
							//usage += "\n\t-genomes_comparison_scope_taxon_id_include {\"species\", \"all\", DIGIT_taxon_id} :  default : \"all\"";
							HashSet<Integer> hsTaxonIdsScopeInclude = fr.inra.jouy.server.queriesTable.QueriesTableTaxonomy.getHsTaxonIds_withScopeToConsider(
									connTaxoDB
									, scopeIncludeIT
									, loi_fromHsOrgaInputCDSFrom.getTaxonId()
									);
							hsTaxonIdsFeatured.addAll(hsTaxonIdsScopeInclude);

							//usage += "\n\t-genomes_comparison_scope_taxon_id_exclude {\"organism\", \"species\", \"none\", DIGIT_taxon_id} :  default : \"organism\"";
							if ( scopeExcludeIT.compareTo("organism")==0 ) {
								//do nothing, will be dealt with latter hsTaxonIdsFeatured.remove(loi_fromHsOrgaInputCDSFrom.getOrganismId());
							} else {
								HashSet<Integer> hsTaxonIdsScopeExclude = fr.inra.jouy.server.queriesTable.QueriesTableTaxonomy.getHsTaxonIds_withScopeToConsider(
										connTaxoDB
										, scopeExcludeIT
										, loi_fromHsOrgaInputCDSFrom.getTaxonId()
										);
								hsTaxonIdsFeatured.removeAll(hsTaxonIdsScopeExclude);
							}

							//alAllOrganismsInDB
							
							HashSet<Integer> hsOrgaIdsFeaturedGenomes = new HashSet<>();
							//HashSet<Integer> hsOrgaIdsMainList = new HashSet<>();
							for (LightOrganismItem loi_fromAlAllOrganismsInDB : alAllOrganismsInDB) {
								if (loi_fromAlAllOrganismsInDB.getOrganismId() == loi_fromHsOrgaInputCDSFrom.getOrganismId()) { 
									//skip, ref organism
								} else  if (hsTaxonIdsFeatured.contains(loi_fromAlAllOrganismsInDB.getTaxonId())) {
									hsOrgaIdsFeaturedGenomes.add(loi_fromAlAllOrganismsInDB.getOrganismId());
								}
								else {
									//hsOrgaIdsMainList.add(loi_fromAlAllOrganismsInDB.getOrganismId());
								}
							}
							
							hmScope2HsOrgaIdsFeaturedGenomes.put(scopeNameIT, hsOrgaIdsFeaturedGenomes);
							if (VERBOSE) {
						    	System.out.println(
						    			"[Treating organism "+countOrgaAnalysed+"/"+hsOrgaInputCDSFrom.size()+", element "+countElementAnalysed+"/"+alElementIdIT.size()+", gene "+countGeneAnalysed+"/"+alGIIT.size()+"] : "
						    				+"getting organisms for scope "+scopeNameIT+" : "+hsOrgaIdsFeaturedGenomes.size() + " organisms found."
						    			);
						    }
							
						}
						
						GeneItem geneItemIT = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getGeneItemWithGeneId(connComparaisonDB, lgi_fromAlGIIT.getGeneId());
						
						ArrayList<String> geneGeneralInfoIT = new ArrayList<>();// ICEIMEGeneralInfo
						
						geneGeneralInfoIT.add(lgi_fromAlGIIT.getLocusTagAsStrippedString());//"Locus_tag"
						geneGeneralInfoIT.add(lgi_fromAlGIIT.getNameAsStrippedString());//"Gene_name");
						geneGeneralInfoIT.add(leiIT.getAccession());//"Organism_accession"); 
						geneGeneralInfoIT.add(""+loi_fromHsOrgaInputCDSFrom.getOrganismId());//"Organism_id_in_db");
						geneGeneralInfoIT.add(loi_fromHsOrgaInputCDSFrom.getFullName());//"Organism_name_and_strain");	
						geneGeneralInfoIT.add(""+lgi_fromAlGIIT.getStart());//"Start"); 
						geneGeneralInfoIT.add(""+lgi_fromAlGIIT.getStop());//"Stop"); 
						geneGeneralInfoIT.add(geneItemIT.getListProteinId().toString());//"Protein_id"); 
						geneGeneralInfoIT.add(geneItemIT.getListProduct().toString());//"List_products");
						
						if (VERBOSE) {
					    	System.out.println(
					    			"[Treating organism "+countOrgaAnalysed+"/"+hsOrgaInputCDSFrom.size()+", element "+countElementAnalysed+"/"+alElementIdIT.size()+", gene "+countGeneAnalysed+"/"+alGIIT.size()+"] : "
					    						+ "printing general Info for gene "+lgi_fromAlGIIT.getLocusTagAsStrippedString()
					    			);
					    }
						
						if ( ! doNotPerformGenomicComparisonAnalysis ) {
							
							if (VERBOSE) {
						    	System.out.println(
						    			"[Treating organism "+countOrgaAnalysed+"/"+hsOrgaInputCDSFrom.size()+", element "+countElementAnalysed+"/"+alElementIdIT.size()+", gene "+countGeneAnalysed+"/"+alGIIT.size()+"] : "
						    						+ "performing genomic comparison..."
						    			);
						    }
														
							ArrayList<String> comparaisonInformationAllScopesIT = new ArrayList<>();
							for (Entry<String, ArrayList<String>> entryHmScope2 : hmGenomesComparisonScope2IncludeExcludeFeatured.entrySet()) {
								String scopeNameIT = entryHmScope2.getKey();
								HashSet<Integer> hsOrgaIdsFeaturedGenomesIT = new HashSet<>(hmScope2HsOrgaIdsFeaturedGenomes.get(scopeNameIT));

								
								// idx 0 : numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution 
								// idx 1 : percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution
								// idx 2 : numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship 
								// idx 3 : percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
								// idx 4 : avgPercentIdentityWithCDSsConservedDuringEvolution
								// idx 5 : avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
								// idx 6 : medianEvalueAlignmentsWithCDSsConservedDuringEvolution
								ArrayList<String> alStatSummaryIT = new ArrayList<>();
								// if hsOrgaIdsFeaturedGenomesIT small size (< 15), consider not stat significant and do not calculate and fill up with empty string instead
								if (hsOrgaIdsFeaturedGenomesIT.size() < 15) {									
									alStatSummaryIT.add(""); // numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution
									alStatSummaryIT.add(""); // percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution
									alStatSummaryIT.add(""); // numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship
									alStatSummaryIT.add(""); // percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
									alStatSummaryIT.add(""); // avgPercentIdentityWithCDSsConservedDuringEvolution
									alStatSummaryIT.add(""); // avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
									alStatSummaryIT.add(""); // medianEvalueAlignmentsWithCDSsConservedDuringEvolution
									comparaisonInformationAllScopesIT.addAll(
											alStatSummaryIT
										);
								} else {
									HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneNameIT = new HashMap<>();
									HashMap<Integer, String> qGeneId2qGeneNameIT = new HashMap<>();
									qGeneId2qGeneNameIT.put(lgi_fromAlGIIT.getGeneId(), "NA"); // qGeneName not important here
									qOrganismId2qGeneId2qGeneNameIT.put(loi_fromHsOrgaInputCDSFrom.getOrganismId(), qGeneId2qGeneNameIT);
									alStatSummaryIT = ComparativeGenomics.getStatSummaryRefCDS_asAlString( //ArrayList<String>
											connComparaisonDB
											, qOrganismId2qGeneId2qGeneNameIT //contains only 1 qOrganismId, 1 qGeneId and 1 qGeneName
											, hsOrgaIdsFeaturedGenomesIT
											//, hsOrgaIdsMainListIT
											);
									if (detailedOutput) {
										comparaisonInformationAllScopesIT.addAll(
												alStatSummaryIT
											);
									} else {										
										
										// idx 1 : percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution ; 0-100									
										Double percentGenomesWithPresenceCDSConservedDuringEvolution = Double.parseDouble(alStatSummaryIT.get(1).replace(",", ".")); // ; replace comma by dot for decimal separator
										if (percentGenomesWithPresenceCDSConservedDuringEvolution >= 100 && alMaxDoublePercentGenomesWithPresenceCDSConservedDuringEvolution.size() < 20) {
											alMaxDoublePercentGenomesWithPresenceCDSConservedDuringEvolution.add(geneItemIT);
										}
										if (percentGenomesWithPresenceCDSConservedDuringEvolution == 0 && alMinDoublePercentGenomesWithPresenceCDSConservedDuringEvolution.size() < 20) {
											alMinDoublePercentGenomesWithPresenceCDSConservedDuringEvolution.add(geneItemIT);
										}
										sumForOrgaPercentGenomesWithPresenceCDSConservedDuringEvolution += percentGenomesWithPresenceCDSConservedDuringEvolution;
										SharedUtilityMethods.addPercentageToSummaryLHM(
												percentGenomesWithPresenceCDSConservedDuringEvolution //double doubleSent
												, 1 //multiplyingFactor
												, tm_percentGenomesWithPresenceCDSConservedDuringEvolution //tmSummary
												, "percentGenomesWithPresenceCDSConservedDuringEvolution" //tmSummaryName
												, 1 //incrementValue
												);
										// idx 3 : percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
										Double percentGenomesWithPresenceCDSSyntenyRelationship = Double.parseDouble(alStatSummaryIT.get(3).replace(",", "."));
										if (percentGenomesWithPresenceCDSSyntenyRelationship >= 100 && alMaxDoublePercentGenomesWithPresenceCDSSyntenyRelationship.size() < 20) {
											alMaxDoublePercentGenomesWithPresenceCDSSyntenyRelationship.add(geneItemIT);
										}
										if (percentGenomesWithPresenceCDSSyntenyRelationship == 0 && alMinDoublePercentGenomesWithPresenceCDSSyntenyRelationship.size() < 20) {
											alMinDoublePercentGenomesWithPresenceCDSSyntenyRelationship.add(geneItemIT);
										}
										sumForOrgaPercentGenomesWithPresenceCDSSyntenyRelationship += percentGenomesWithPresenceCDSSyntenyRelationship;
										SharedUtilityMethods.addPercentageToSummaryLHM(
												percentGenomesWithPresenceCDSSyntenyRelationship //double doubleSent ; replace comma by dot for decimal separator
												, 1 //multiplyingFactor
												, tm_percentGenomesWithPresenceCDSSyntenyRelationship //tmSummary
												, "percentGenomesWithPresenceCDSSyntenyRelationship" //tmSummaryName
												, 1 //incrementValue
												);
										// idx 4 : avgPercentIdentityWithCDSsConservedDuringEvolution
										Double percentIdentityAlignment = Double.parseDouble(alStatSummaryIT.get(4).replace(",", "."));
										if (percentIdentityAlignment >= 100 && alMaxDoublePercentIdentityAlignment.size() < 20) {
											alMaxDoublePercentIdentityAlignment.add(geneItemIT);
										}
										if (percentIdentityAlignment == 0 && alMinDoublePercentIdentityAlignment.size() < 20) {
											alMinDoublePercentIdentityAlignment.add(geneItemIT);
										}
										sumForOrgaPercentIdentityAlignment += percentIdentityAlignment;
										SharedUtilityMethods.addPercentageToSummaryLHM(
												percentIdentityAlignment //double doubleSent ; replace comma by dot for decimal separator
												, 1 //multiplyingFactor
												, tm_percentIdentityAlignment //tmSummary
												, "percentIdentityAlignment" //tmSummaryName
												, 1 //incrementValue
												);
										// idx 5 : avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
										Double percentQuerySubjectLenghtCoverageAlignment = Double.parseDouble(alStatSummaryIT.get(5).replace(",", "."));
										if (percentQuerySubjectLenghtCoverageAlignment >= 100 && alMaxDoublePercentQuerySubjectLenghtCoverageAlignment.size() < 20) {
											alMaxDoublePercentQuerySubjectLenghtCoverageAlignment.add(geneItemIT);
										}
										if (percentQuerySubjectLenghtCoverageAlignment == 0 && alMinDoublePercentQuerySubjectLenghtCoverageAlignment.size() < 20) {
											alMinDoublePercentQuerySubjectLenghtCoverageAlignment.add(geneItemIT);
										}
										sumForOrgaPercentQuerySubjectLenghtCoverageAlignment += percentQuerySubjectLenghtCoverageAlignment;
										SharedUtilityMethods.addPercentageToSummaryLHM(
												percentQuerySubjectLenghtCoverageAlignment //double doubleSent ; replace comma by dot for decimal separator
												, 1 //multiplyingFactor
												, tm_percentQuerySubjectLenghtCoverageAlignment //tmSummary
												, "percentQuerySubjectLenghtCoverageAlignment" //tmSummaryName
												, 1 //incrementValue
												);
										
									}
									
								}
								
							} //for (Entry<String, ArrayList<String>> entryHmScope2 : hmGenomesComparisonScope2IncludeExcludeFeatured.entrySet()) {
							
							if (detailedOutput) {
								ArrayList<String> alLineInfoToPrint = new ArrayList<>();
								alLineInfoToPrint.addAll(geneGeneralInfoIT);
								alLineInfoToPrint.addAll(comparaisonInformationAllScopesIT);
								orgaLinesToPrint.add(String.join("\t", alLineInfoToPrint));			
							}


							
						} else {
							if (VERBOSE) {
						    	System.out.println(
						    			"[Treating organism "+countOrgaAnalysed+"/"+hsOrgaInputCDSFrom.size()+", element "+countElementAnalysed+"/"+alElementIdIT.size()+", gene "+countGeneAnalysed+"/"+alGIIT.size()+"] : "
						    						+ "skipping genomic comparison as requested in argument"
						    			);
						    }
							
						}//doNotPerformGenomicComparisonAnalysis
						
					} //for (LightGeneItem lgi_fromAlGIIT : alGIIT) {
					
				} // for (Integer elementIdIT : alElementIdIT) {
				
				if (detailedOutput) {
					Files.write(outputFile, orgaLinesToPrint, StandardCharsets.UTF_8,
							Files.exists(outputFile) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE); //, StandardOpenOption.WRITE
				}
				
				// avg % conservation CDS per organism
				Double organismAvgPercentGenomesWithPresenceCDSConservedDuringEvolution = (double) sumForOrgaPercentGenomesWithPresenceCDSConservedDuringEvolution / (double) sizeCDSInOrga;
				SharedUtilityMethods.addPercentageToSummaryLHM(
						organismAvgPercentGenomesWithPresenceCDSConservedDuringEvolution //double doubleSent
						, 1 //multiplyingFactor
						, tm_organismAvgPercentGenomesWithPresenceCDSConservedDuringEvolution //tmSummary
						, "organismAvgPercentGenomesWithPresenceCDSConservedDuringEvolution" //tmSummaryName
						, 1 //incrementValue
						);
				Double organismAvgPercentGenomesWithPresenceCDSSyntenyRelationship = (double) sumForOrgaPercentGenomesWithPresenceCDSSyntenyRelationship / (double) sizeCDSInOrga;
				SharedUtilityMethods.addPercentageToSummaryLHM(
						organismAvgPercentGenomesWithPresenceCDSSyntenyRelationship //double doubleSent
						, 1 //multiplyingFactor
						, tm_organismAvgPercentGenomesWithPresenceCDSSyntenyRelationship //tmSummary
						, "organismAvgPercentGenomesWithPresenceCDSSyntenyRelationship" //tmSummaryName
						, 1 //incrementValue
						);
				Double organismAvgPercentIdentityAlignment = (double) sumForOrgaPercentIdentityAlignment / (double) sizeCDSInOrga;
				SharedUtilityMethods.addPercentageToSummaryLHM(
						organismAvgPercentIdentityAlignment //double doubleSent
						, 1 //multiplyingFactor
						, tm_organismAvgPercentIdentityAlignment //tmSummary
						, "organismAvgPercentIdentityAlignment" //tmSummaryName
						, 1 //incrementValue
						);
				Double organismAvgPercentQuerySubjectLenghtCoverageAlignment = (double) sumForOrgaPercentQuerySubjectLenghtCoverageAlignment / (double) sizeCDSInOrga;
				SharedUtilityMethods.addPercentageToSummaryLHM(
						organismAvgPercentQuerySubjectLenghtCoverageAlignment //double doubleSent
						, 1 //multiplyingFactor
						, tm_organismAvgPercentQuerySubjectLenghtCoverageAlignment //tmSummary
						, "organismAvgPercentQuerySubjectLenghtCoverageAlignment" //tmSummaryName
						, 1 //incrementValue
						);
				
				//break LOOP_ORGANISMS;

			} // LOOP_ORGANISMS:  for (LightOrganismItem loi_fromHsOrgaInputCDSFrom : hsOrgaInputCDSFrom) {
			
			if (!detailedOutput) {
				if (VERBOSE) {
					System.out.println(
		    				"Start writing to output file"
			    			);
				}
				ArrayList<Integer> keyListLHMPercentage = new ArrayList<Integer>(tm_percentGenomesWithPresenceCDSConservedDuringEvolution.keySet());
				Collections.sort(keyListLHMPercentage);
				ArrayList<String> linesToPrintPercentageIdentity = new ArrayList<>();
				for(int i = 0; i < keyListLHMPercentage.size(); i++) {
					int keyPercentageIT = keyListLHMPercentage.get(i);
					Long countNumberPercentGenomesWithPresenceCDSConservedDuringEvolution = tm_percentGenomesWithPresenceCDSConservedDuringEvolution.get(keyPercentageIT);
					Long countNumberPercentGenomesWithPresenceCDSSyntenyRelationship = tm_percentGenomesWithPresenceCDSSyntenyRelationship.get(keyPercentageIT);
					Long countNumberPercentIdentityAlignment = tm_percentIdentityAlignment.get(keyPercentageIT);
					Long countNumberPercentQuerySubjectLenghtCoverageAlignment = tm_percentQuerySubjectLenghtCoverageAlignment.get(keyPercentageIT);
					
					Long countNumberOrganismAvgPercentGenomesWithPresenceCDSConservedDuringEvolution = tm_organismAvgPercentGenomesWithPresenceCDSConservedDuringEvolution.get(keyPercentageIT);
					Long countNumberOrganismAvgPercentGenomesWithPresenceCDSSyntenyRelationship = tm_organismAvgPercentGenomesWithPresenceCDSSyntenyRelationship.get(keyPercentageIT);
					Long countNumberOrganismAvgPercentIdentityAlignment = tm_organismAvgPercentIdentityAlignment.get(keyPercentageIT);
					Long countNumberOrganismAvgPercentQuerySubjectLenghtCoverageAlignment = tm_organismAvgPercentQuerySubjectLenghtCoverageAlignment.get(keyPercentageIT);
					
					String lineIT = 
							keyPercentageIT
							+"\t"+countNumberPercentGenomesWithPresenceCDSConservedDuringEvolution
							+"\t"+countNumberPercentGenomesWithPresenceCDSSyntenyRelationship
							+"\t"+countNumberPercentIdentityAlignment
							+"\t"+countNumberPercentQuerySubjectLenghtCoverageAlignment
							
							+"\t"+countNumberOrganismAvgPercentGenomesWithPresenceCDSConservedDuringEvolution
							+"\t"+countNumberOrganismAvgPercentGenomesWithPresenceCDSSyntenyRelationship
							+"\t"+countNumberOrganismAvgPercentIdentityAlignment
							+"\t"+countNumberOrganismAvgPercentQuerySubjectLenghtCoverageAlignment
							;
					linesToPrintPercentageIdentity.add(lineIT);
				}
				Files.write(outputFile, linesToPrintPercentageIdentity, StandardCharsets.UTF_8,
						Files.exists(outputFile) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE); //, StandardOpenOption.WRITE
				
				if (VERBOSE) {
					System.out.println(
		    				"Exemples of CDSs with at least 1 CDS conserved during evolution found in 100% of the compared genomes:"
			    			);
					for (GeneItem giIT : alMaxDoublePercentGenomesWithPresenceCDSConservedDuringEvolution) {
						System.out.println(
			    				"\t"+giIT.getMostSignificantGeneNameAsStrippedText()+ " " + giIT.getListProduct().toString()
				    			);
					}
					System.out.println(
		    				"Exemples of CDSs with at least 1 CDS conserved during evolution found in 0% of the compared genomes:"
			    			);
					for (GeneItem giIT : alMinDoublePercentGenomesWithPresenceCDSConservedDuringEvolution) {
						System.out.println(
			    				"\t"+giIT.getMostSignificantGeneNameAsStrippedText()+ " " + giIT.getListProduct().toString()
				    			);
					}
					System.out.println(
		    				"Exemples of CDSs with at least 1 CDS in synteny relationship found in 100% of the compared genomes:"
			    			);
					for (GeneItem giIT : alMaxDoublePercentGenomesWithPresenceCDSSyntenyRelationship) {
						System.out.println(
			    				"\t"+giIT.getMostSignificantGeneNameAsStrippedText()+ " " + giIT.getListProduct().toString()
				    			);
					}
					System.out.println(
		    				"Exemples of CDSs with at least 1 CDS in synteny relationship found in 0% of the compared genomes:"
			    			);
					for (GeneItem giIT : alMinDoublePercentGenomesWithPresenceCDSSyntenyRelationship) {
						System.out.println(
			    				"\t"+giIT.getMostSignificantGeneNameAsStrippedText()+ " " + giIT.getListProduct().toString()
				    			);
					}
					// not alMaxDoublePercentIdentityAlignment
					// not alMinDoublePercentIdentityAlignment
					// not alMaxDoublePercentQuerySubjectLenghtCoverageAlignment
					// not alMinDoublePercentQuerySubjectLenghtCoverageAlignment
				}
				
				
			}


			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			DatabaseConf.freeConnection(connComparaisonDB, methodNameToReport);
			DatabaseConf.freeConnection(connTaxoDB, methodNameToReport);
			//DatabaseConfICEDB.freeConnection(connICEDB, methodNameToReport);
		}

		milliPrint2 = System.currentTimeMillis() - milli;
		Date dateEnd = new Date();
		if (VERBOSE) {
			System.out.println("\nDone "+methodNameToReport+" at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.");	
		}
		
	}

	private static ArrayList<String> getHeaderSummaryOutputAsAlString() throws Exception {

		ArrayList<String> alToReturn = new ArrayList<>();
		alToReturn.add("Percent");
		for (Entry<String, ArrayList<String>> entry : hmGenomesComparisonScope2IncludeExcludeFeatured.entrySet()) {
			String scopeIT = entry.getKey();
			//ArrayList<String> alIncludeExcludeFeaturedIT = entry.getValue();
			alToReturn.add("Number_CDSs_for_given_percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT);
			alToReturn.add("Number_CDSs_for_given_percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT);
			alToReturn.add("Number_CDSs_for_given_average_percent_identity_alignment_scope_"+scopeIT);
			alToReturn.add("Number_CDSs_for_given_average_percent_query_and_subject_lenght_coverage_alignment_scope_"+scopeIT);
			
			alToReturn.add("Number_organism_for_given_CDS_avg_percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT);
			alToReturn.add("Number_organism_for_given_CDS_avg_percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT);
			alToReturn.add("Number_organism_for_given_CDS_avg_percent_identity_alignment_scope_"+scopeIT);
			alToReturn.add("Number_organism_for_given_CDS_avg_percent_query_and_subject_lenght_coverage_alignment_scope_"+scopeIT);
			
		}
		return alToReturn;
		
	}
	
	
	private static ArrayList<String> getHeaderDetailedOutputAsAlString() throws Exception {
		//output
		// columns : Locus_tag, Gene_name, Organism_accession, Organism_id_in_db, Organism_name_and_strain, Start, Stop, Protein_id, List_products,
		//Number_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT
		//Percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT
		//Number_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT
		//Percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT
		//Average_percent_identity_alignment_scope_"+scopeIT
		//Average_percent_query_and_subject_lenght_coverage_alignment_scope_"+scopeIT
		//Median_Evalue_alignment_scope_"+scopeIT
		ArrayList<String> alToReturn = new ArrayList<>();
		alToReturn.add("Locus_tag");
		alToReturn.add("Gene_name");
		alToReturn.add("Organism_accession"); 
		alToReturn.add("Organism_id_in_db");
		alToReturn.add("Organism_name_and_strain");	
		alToReturn.add("Start"); 
		alToReturn.add("Stop"); 
		alToReturn.add("Protein_id"); 
		alToReturn.add("List_products");
		for (Entry<String, ArrayList<String>> entry : hmGenomesComparisonScope2IncludeExcludeFeatured.entrySet()) {
			String scopeIT = entry.getKey();
			//ArrayList<String> alIncludeExcludeFeaturedIT = entry.getValue();
			alToReturn.add("Number_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT);
			alToReturn.add("Percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT);
			alToReturn.add("Number_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT);
			alToReturn.add("Percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT);
			alToReturn.add("Average_percent_identity_alignment_scope_"+scopeIT);
			alToReturn.add("Average_percent_query_and_subject_lenght_coverage_alignment_scope_"+scopeIT);
			alToReturn.add("Median_Evalue_alignment_scope_"+scopeIT);
		}
		return alToReturn;
	}
	
	
}
