package basicStats;


/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentPairs;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentParams;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignments;
import fr.inra.jouy.server.queriesTable.QueriesTableGenes;
import fr.inra.jouy.server.queriesTable.QueriesTableOrganisms;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignments.AlignmentsTmpObjAlignmentParamIdAndPairs;


public class PrevalenceCDSsInOrthologsSynteny {

	
	static String methodNameToReport = "PrevalenceCDSsInOrthologsSynteny";
	private class Option {
	     String flag, opt;
	     public Option(String flag, String opt) { this.flag = flag; this.opt = opt; }
	}
	public enum MethodToRun { 
		byPairwiseComparisons
		//distribOrthoHomoSyntenies
		//, 
	}
	public static String outputDir = System.getProperty("user.home"); // default
	public static int restrictToQOrgaIdsWithLastDigit = -1;
	
	//script scope variables
	//byPairwiseComparisons
	static TreeMap<Integer, Long> byPairwiseComparisons_biggerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison = new TreeMap<>();
	static TreeMap<Integer, Long> byPairwiseComparisons_biggerOrganism_hmPercCDSSyntenyRelationship2CountNumberPairwiseComparison = new TreeMap<>();
	static TreeMap<Integer, Long> byPairwiseComparisons_biggerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparison = new TreeMap<>();
	static TreeMap<Integer, Long> byPairwiseComparisons_biggerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparison = new TreeMap<>();
	static TreeMap<Integer, Long> byPairwiseComparisons_smallerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison = new TreeMap<>();
	static TreeMap<Integer, Long> byPairwiseComparisons_smallerOrganism_hmPercCDSSyntenyRelationship2CountNumberPairwiseComparison = new TreeMap<>();
	static TreeMap<Integer, Long> byPairwiseComparisons_smallerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparison = new TreeMap<>();
	static TreeMap<Integer, Long> byPairwiseComparisons_smallerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparison = new TreeMap<>();
	

	private static String printUsage() {
		String usage = "";
		usage += "\nProgram "+methodNameToReport;
		usage += "\nThe supported arguments are :";
		List<MethodToRun> enumList = Arrays.asList(MethodToRun.class.getEnumConstants());
		for (MethodToRun methodToRunIT : enumList) {
			usage += "\n\t-"+methodToRunIT.toString()+" {ON/OFF}";
		}
		usage += "\n\t-outputDir {path to output directory}";
		usage += "\n\t-restrictToQOrgaIdsWithLastDigit {one digit}";
		usage += "\nOnly one argument regarding the analysis to run is supported at a time";
		return usage;
	}
	
	public static void main(String[] args) throws Exception {
		
		long milli = System.currentTimeMillis();
		long milliPrint2 = -1;
		milliPrint2 = System.currentTimeMillis() - milli;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateStart = new Date();
		System.out.println("Starting "+methodNameToReport+" at "+dateFormat.format(dateStart));
		
		PrevalenceCDSsInOrthologsSynteny bpwc = new PrevalenceCDSsInOrthologsSynteny();
		ArrayList<String> argsList = new ArrayList<String>();  
		ArrayList<Option> optsList = new ArrayList<Option>();
		ArrayList<String> doubleOptsList = new ArrayList<String>();

	    for (int i = 0; i < args.length; i++) {
	        switch (args[i].charAt(0)) {
	        case '-':
	            if (args[i].length() < 2)
	                throw new IllegalArgumentException("Not a valid argument: "+args[i]);
	            if (args[i].charAt(1) == '-') {
	                if (args[i].length() < 3)
	                    throw new IllegalArgumentException("Not a valid argument: "+args[i]);
	                // --opt
	                doubleOptsList.add(args[i].substring(2, args[i].length()));
	            } else {
	                if (args.length-1 == i)
	                    throw new IllegalArgumentException("Expected arg after: "+args[i]);
	                // -opt
	                optsList.add(bpwc.new Option(args[i], args[i+1]));
	                i++;
	            }
	            break;
	        default:
	            // arg
	            argsList.add(args[i]);
	            break;
	        }
	    }

	    ArrayList<MethodToRun> alMethodToRunIT = new ArrayList<>();
	    boolean printUsage = false;
	    
	    for (String argIT : argsList) {
	    	switch (argIT) {
	        case "-help":
	        	printUsage = true;
	            break;
	        default:
	            // arg
	        	printUsage = true;
	        }
	    }
	    for (String doubleOptsArgIT : doubleOptsList) {
	    	switch (doubleOptsArgIT) {
	        case "--help":
	        	printUsage = true;
	            break;
	        default:
	            // arg
	        	printUsage = true;
	        }
	    }
	    
	    
	    if (printUsage) {
	    	printUsage();
	    	System.exit(0);
	    }
	    
	    for (Option optionIT : optsList) {
	    	switch (optionIT.flag) {
	        case "-byPairwiseComparisons":
	        	if (optionIT.opt.matches("^ON$")) {
		        	alMethodToRunIT.add(MethodToRun.byPairwiseComparisons);
	        	}
	            break;
	        case "-outputDir":
	        	//if (optionIT.opt.matches("^ON$")) {
	        		outputDir = optionIT.opt;
	        	//}
	            break;
	        case "-restrictToQOrgaIdsWithLastDigit":
	        	if (optionIT.opt.matches("^\\d$")) {
	        		restrictToQOrgaIdsWithLastDigit = Integer.parseInt(optionIT.opt);
	        	} else {
	        		throw new IllegalArgumentException("The flag -restrictToQOrgaIdsWithLastDigit must be a single digit."+printUsage());
	        	}
	            break;
	        default:
	            // arg
	        	throw new IllegalArgumentException("The flag "+optionIT.flag+" is not supported."+printUsage());
	        }
	    }
	    
	    if (alMethodToRunIT.size() > 1) {
	    	throw new IllegalArgumentException("Only one argument at a time is supported."+printUsage());
	    } else if (alMethodToRunIT.isEmpty()) {
	    	throw new IllegalArgumentException("No flag was provided to run method."+printUsage());
	    }
	    
	    MethodToRun methodToRunIT = null;
	    for (MethodToRun methodToRunArgs : alMethodToRunIT) {
	    	methodToRunIT = methodToRunArgs;
	    }
	    
	    switch (methodToRunIT) {
        case byPairwiseComparisons:
        	byPairwiseComparisons();
            break;
        default:
            // arg
        	throw new IllegalArgumentException("The method "+methodToRunIT.toString()+" is not yet implemented."+printUsage());
        }


		milliPrint2 = System.currentTimeMillis() - milli;
		Date dateEnd = new Date();
		System.out.println("Done "+methodNameToReport+" at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.");
		
	}

	private static void byPairwiseComparisons() throws Exception {


		String methodNameToReport = "byPairwiseComparisons";
		
		String extensionPostFile = "";
		if (restrictToQOrgaIdsWithLastDigit >= 0) {
			extensionPostFile = "_restrictToQOrgaIdsWithLastDigit"+restrictToQOrgaIdsWithLastDigit;
		}
		String outputFile_asString = outputDir+"/result_byPairwiseComparisons"+extensionPostFile+".txt";
		//String warningFile_asString = System.getProperty("user.home")+"/warning_prevalenceCDSsInOrthologsSynteny.txt";
		
		
		File outputFile_asJavaIoFile = new File(outputFile_asString);
		if(outputFile_asJavaIoFile.exists()){
			outputFile_asJavaIoFile.delete();
			//try {
			outputFile_asJavaIoFile.createNewFile();
			//} catch (IOException e) {
			//	e.printStackTrace();
			//}
		}
		Path outputFile = Paths.get(outputFile_asString);
//		File warningFile_asJavaIoFile = new File(warningFile_asString);
//		if(warningFile_asJavaIoFile.exists()){
//			warningFile_asJavaIoFile.delete();
//			//try {
//			warningFile_asJavaIoFile.createNewFile();
//			//} catch (IOException e) {
//			//	e.printStackTrace();
//			//}
//		}
		//Path warningFile = Paths.get(warningFile_asString);

		
		
		/*
		** Questions : 
			distribution by comparisons of genomes in pairs

			** Result :
			 * pairwise comparison -------> EN COURS
			
			Number of pairwise comparison
			^
			|
			|
			|
			-------------> % CDS of genome
			-------------> Diff of number of CDS between compared organisms
			
			Legends:
			 - % CDS in ortho BDBH relationship
			 - % CDS in synteny relationship
			 - % CDS not in ortho BDBH or synteny relationship
			
			Remark : do same graph for smaller organism only and bigger organism only.

		*/
		
		String titleAnalyse = "Prevalence CDSs In Orthologs and Synteny ; distribution of the number of pairwise comparison by the % of CDS in genome in ortho BDBH or synteny relationship";
		
		ArrayList<String> alHeader = new ArrayList<>();
		alHeader.add("% CDS of genome");
		alHeader.add("CDS in singleton orthologs BDBH (not synteny) relationship ; bigger compared organisms only");
		alHeader.add("CDS in synteny relationship ; bigger compared organisms only");
		alHeader.add("CDS both in singleton orthologs and synteny relationships ; bigger compared organisms only");
		alHeader.add("CDS not in singleton orthologs or synteny relationship ; bigger compared organisms only");
		alHeader.add("CDS in singleton orthologs BDBH (not synteny) relationship ; smaller compared organisms only");
		alHeader.add("CDS in synteny relationship ; smaller compared organisms only");
		alHeader.add("CDS both in singleton orthologs and synteny relationships ; smaller compared organisms only");
		alHeader.add("CDS not in singleton orthologs or synteny relationship ; smaller compared organisms only");
		alHeader.add("CDS in singleton orthologs BDBH (not synteny) relationship ; all organisms");
		alHeader.add("CDS in synteny relationship ; all organisms");
		alHeader.add("CDS both in singleton orthologs and synteny relationships ; all organisms");
		alHeader.add("CDS not in singleton orthologs or synteny relationship ; all organisms");


		
		//alHeader.add("bigger organism total number CDSs with at least one meaningful functional annotation");
//		alHeader.add("bigger organism number orthologs BDBH not in synteny both with at least one meaningful functional annotation");
//		alHeader.add("bigger organism number orthologs BDBH not in synteny both without any meaningful functional annotation");
//		alHeader.add("bigger organism number orthologs BDBH not in synteny q CDS at least one meaningful functional annotation s CDS without");
//		alHeader.add("bigger organism number orthologs BDBH not in synteny s CDS at least one meaningful functional annotation q CDS without");
//		alHeader.add("bigger organism number CDSs in synteny both with at least one meaningful functional annotation");
//		alHeader.add("bigger organism number CDSs in synteny both without any meaningful functional annotation");
//		alHeader.add("bigger organism number CDSs in synteny q CDS at least one meaningful functional annotation s CDS without");
//		alHeader.add("bigger organism number CDSs in synteny s CDS at least one meaningful functional annotation q CDS without");
//		alHeader.add("bigger organism number CDSs not in orthologs BDBH or synteny relationship with at least one meaningful functional annotation");
//		alHeader.add("smaller organism total number CDSs with at least one meaningful functional annotation");
//		alHeader.add("smaller organism number orthologs BDBH not in synteny both with at least one meaningful functional annotation");
//		alHeader.add("smaller organism number orthologs BDBH not in synteny both without any meaningful functional annotation");
//		alHeader.add("smaller organism number orthologs BDBH not in synteny q CDS at least one meaningful functional annotation s CDS without");
//		alHeader.add("smaller organism number orthologs BDBH not in synteny s CDS at least one meaningful functional annotation q CDS without");
//		alHeader.add("smaller organism number CDSs in synteny both with at least one meaningful functional annotation");
//		alHeader.add("smaller organism number CDSs in synteny both without any meaningful functional annotation");
//		alHeader.add("smaller organism number CDSs in synteny q CDS at least one meaningful functional annotation s CDS without");
//		alHeader.add("smaller organism number CDSs in synteny s CDS at least one meaningful functional annotation q CDS without");
//		alHeader.add("smaller organism number CDSs not in orthologs BDBH or synteny relationship with at least one meaningful functional annotation");
		
		
		long milli = System.currentTimeMillis();
		long milliPrint2 = -1;
		milliPrint2 = System.currentTimeMillis() - milli;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateStart = new Date();
		System.out.println("Starting "+methodNameToReport+" at "+dateFormat.format(dateStart));
		
		Connection conn = null;
		try {
			
			conn = DatabaseConf.getConnection_db();
			
			initTreeMapsBbyPairwiseComparisonsBiggerOrganismAndSmaller();
			
			ArrayList<Integer> alOrganismIds = QueriesTableOrganisms.getAlOrganismIds(conn, true, true);//onlyPublic, onlyWithElement
			Collections.sort(alOrganismIds);
			int numberCombination = ( alOrganismIds.size() * ( alOrganismIds.size() + 1 ) / 2);
			int counter = 0;
			OUTERLOOP: for (int i = 0; i < alOrganismIds.size() ; i++) {
				int qOrganismId = alOrganismIds.get(i);
				
				if (restrictToQOrgaIdsWithLastDigit >= 0) {
					if ( ( qOrganismId % 10 ) != restrictToQOrgaIdsWithLastDigit ) { //modulo 10 give the last digit
						
						System.out.println("skipping qOrganismId = "+qOrganismId+" because of the flag -restrictToQOrgaIdsWithLastDigit "+restrictToQOrgaIdsWithLastDigit
								+ ".");
						counter += alOrganismIds.size() - 1 - i;
						continue OUTERLOOP;
					}
				}
				
				
				for (int j = i+1; j < alOrganismIds.size() ; j++) {
					int sOrganismId = alOrganismIds.get(j);
					
//					if (counter == 10) {
//						break OUTERLOOP;
//					}
					
					System.out.println("( "+counter+" / "+numberCombination+" ) : starting qOrganismId = "+qOrganismId+" - sOrganismId = "+sOrganismId);
					
					//for each qOrganismId - sOrganismId
					HashSet<Integer> hsQOrganismId = new HashSet<>();
					hsQOrganismId.add(qOrganismId);
					HashSet<Integer> hsQGeneIds = QueriesTableGenes.getHsGeneIds_withHsOrgaId(conn, hsQOrganismId);
					HashSet<Integer> hsSOrganismId = new HashSet<>();
					hsSOrganismId.add(sOrganismId);
					HashSet<Integer> hsSGeneIds = QueriesTableGenes.getHsGeneIds_withHsOrgaId(conn, hsSOrganismId);
					
//					System.err.println("hsQGeneIds.size() = "+hsQGeneIds.size());
//					System.err.println("hsSGeneIds.size() = "+hsSGeneIds.size());
					
					//HashSet<String> hsQAccessions = QueriesTableElements.getHsAccessions_withOrgaId(conn, qOrganismId);
					//HashSet<String> hsSAccessions = QueriesTableElements.getHsAccessions_withOrgaId(conn, sOrganismId);
					
					HashSet<Long> hsAlignmentParamId = QueriesTableAlignmentParams.getHsAlignmentParamId_withQOrganismIdAndSOrganismId(
							conn
							, qOrganismId
							, sOrganismId
							, false //isMirrorRequest
							);

					if (hsAlignmentParamId.isEmpty()) {
						continue;
					}
					
					HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> HMalignmentId2AlignmentParamIdAndPairs = QueriesTableAlignments.getHMalignmentId2AlignmentParamIdAndPairs_WithSetAlignmentParamId(
							conn
							, hsAlignmentParamId
							, true
							, true
							);

					HashSet<Long> hsAlignmentIdsOrthologsBDBHNotInSynteny = new HashSet<>();
					HashSet<Long> hsAlignmentIdsSynteny = new HashSet<>();
					for (Map.Entry<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> entry : HMalignmentId2AlignmentParamIdAndPairs.entrySet()) {
						if (entry.getValue().getPairs() == 1) {
							hsAlignmentIdsOrthologsBDBHNotInSynteny.add(entry.getKey());
						} else {
							hsAlignmentIdsSynteny.add(entry.getKey());
						}
					}

					//idx 0 : hsQGeneId ; idx 1 : hsSGeneId
					HashSet<Integer> typeToFetch = new HashSet<>();
					typeToFetch.add(1);
					typeToFetch.add(2);
					
					HashSet<Integer> hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny = new HashSet<>();
					HashSet<Integer> hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny = new HashSet<>();
					if ( ! hsAlignmentIdsOrthologsBDBHNotInSynteny.isEmpty()) {
						ArrayList<HashSet<Integer>> alHsQGeneIdAndHsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny = 
								QueriesTableAlignmentPairs.getAlHsQGeneIdAndHsSGeneIdOfAlignmentPairs_withHsAlignmentIdsAndType(
										conn
										, hsAlignmentIdsOrthologsBDBHNotInSynteny
										, true
										, typeToFetch //only Type1 Or 2
										);
						hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny = alHsQGeneIdAndHsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.get(0);
						hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny = alHsQGeneIdAndHsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.get(1);
					}

					HashSet<Integer> hsQGeneIdOfAlignmentPairs_synteny = new HashSet<>();
					HashSet<Integer> hsSGeneIdOfAlignmentPairs_synteny = new HashSet<>();
					if ( ! hsAlignmentIdsSynteny.isEmpty()) {
						ArrayList<HashSet<Integer>> alHsQGeneIdAndHsSGeneIdOfAlignmentPairs_synteny = 
								QueriesTableAlignmentPairs.getAlHsQGeneIdAndHsSGeneIdOfAlignmentPairs_withHsAlignmentIdsAndType(
										conn
										, hsAlignmentIdsSynteny
										, true
										, typeToFetch //only Type1 Or 2
										);
						hsQGeneIdOfAlignmentPairs_synteny = alHsQGeneIdAndHsSGeneIdOfAlignmentPairs_synteny.get(0);
						hsSGeneIdOfAlignmentPairs_synteny = alHsQGeneIdAndHsSGeneIdOfAlignmentPairs_synteny.get(1);
					}
					
					//get gene ids dups between hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny and hsQGeneIdOfAlignmentPairs_synteny
					HashSet<Integer> interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny = new HashSet<>(hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny);
					interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.retainAll(hsQGeneIdOfAlignmentPairs_synteny);
					if ( ! interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.isEmpty()) {
//						String message = "For qOrganismId = "+qOrganismId+ " and sOrganismId = "+sOrganismId
//								+ ", there are "+interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size()
//								+ " q gene ids that are both in orthologs BDBH and synteny relationship : "+interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.toString()
//								;
//						appendWarning(warningFile, methodNameToReport, message);
						hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.removeAll(interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny);
						hsQGeneIdOfAlignmentPairs_synteny.removeAll(interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny);
					}
					//get gene ids dups between hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny and hsSGeneIdOfAlignmentPairs_synteny
					HashSet<Integer> interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny = new HashSet<>(hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny);
					interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.retainAll(hsSGeneIdOfAlignmentPairs_synteny);
					if ( ! interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.isEmpty()) {
//						String message = "For qOrganismId = "+qOrganismId+ " and sOrganismId = "+sOrganismId
//								+ ", there are "+interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size()
//								+ " s gene ids that are both in orthologs BDBH and synteny relationship : "+interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.toString()
//								;
//						appendWarning(warningFile, methodNameToReport, message);
						hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.removeAll(interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny);
						hsSGeneIdOfAlignmentPairs_synteny.removeAll(interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny);
					}

//					System.err.println("interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size() = "+interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size());
//					System.err.println("interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size() = "+interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size());
//					System.err.println("hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.size() = "+hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.size());
//					System.err.println("hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.size() = "+hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.size());
//					System.err.println("hsQGeneIdOfAlignmentPairs_synteny.size() = "+hsQGeneIdOfAlignmentPairs_synteny.size());
//					System.err.println("hsSGeneIdOfAlignmentPairs_synteny.size() = "+hsSGeneIdOfAlignmentPairs_synteny.size());
					
					HashSet<Integer> hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship = new HashSet<>(hsQGeneIds);
					hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.removeAll(hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny);
					hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.removeAll(hsQGeneIdOfAlignmentPairs_synteny);
					hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.removeAll(interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny);
					HashSet<Integer> hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship = new HashSet<>(hsSGeneIds);
					hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.removeAll(hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny);
					hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.removeAll(hsSGeneIdOfAlignmentPairs_synteny);
					hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.removeAll(interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny);


//					System.err.println("hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.size() = "+hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.size());
//					System.err.println("hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.size() = "+hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.size());
					
					// which one is bigger organism ?
					//ArrayList<String> alLine = new ArrayList<>();
					if (hsQGeneIds.size() >= hsSGeneIds.size()) {
						
						addToFinalHmPercCDS2Count(
								"biggerOrganism"
								, hsQGeneIds
								, hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny
								, hsQGeneIdOfAlignmentPairs_synteny
								, interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny
								, hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship
								);
						addToFinalHmPercCDS2Count(
								"smallerOrganism"
								, hsSGeneIds
								, hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny
								, hsSGeneIdOfAlignmentPairs_synteny
								, interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny
								, hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship
								);
						
						
						
						//PB : Worksheet size: Maximum of 1,048,576 rows and 16,384 columns
						// 5171 * 5172 / 2 = 13,372,206 combinations of pairwise comparison = rows...
//						alLine.add(String.valueOf(qOrganismId));
//						alLine.add(String.valueOf(sOrganismId));
//						alLine.add(String.valueOf(hsQGeneIds.size()));
//						alLine.add(String.valueOf(hsSGeneIds.size()));
//						alLine.add(String.valueOf(hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.size()));
//						alLine.add(String.valueOf(hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.size()));
//						alLine.add(String.valueOf(hsQGeneIdOfAlignmentPairs_synteny.size()));
//						alLine.add(String.valueOf(hsSGeneIdOfAlignmentPairs_synteny.size()));
//						alLine.add(String.valueOf(interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size()));
//						alLine.add(String.valueOf(interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size()));
//						alLine.add(String.valueOf(hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.size()));
//						alLine.add(String.valueOf(hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.size()));
					} else {
						addToFinalHmPercCDS2Count(
								"smallerOrganism"
								, hsQGeneIds
								, hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny
								, hsQGeneIdOfAlignmentPairs_synteny
								, interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny
								, hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship
								);
						addToFinalHmPercCDS2Count(
								"biggerOrganism"
								, hsSGeneIds
								, hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny
								, hsSGeneIdOfAlignmentPairs_synteny
								, interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny
								, hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship
								);
//						alLine.add(String.valueOf(sOrganismId));
//						alLine.add(String.valueOf(qOrganismId));
//						alLine.add(String.valueOf(hsSGeneIds.size()));
//						alLine.add(String.valueOf(hsQGeneIds.size()));
//						alLine.add(String.valueOf(hsSGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.size()));
//						alLine.add(String.valueOf(hsQGeneIdOfAlignmentPairs_orthologsBDBHNotInSynteny.size()));
//						alLine.add(String.valueOf(hsSGeneIdOfAlignmentPairs_synteny.size()));
//						alLine.add(String.valueOf(hsQGeneIdOfAlignmentPairs_synteny.size()));
//						alLine.add(String.valueOf(interesctSGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size()));
//						alLine.add(String.valueOf(interesctQGeneIdBothOrthologsBDBHNotInSyntenyAndSynteny.size()));
//						alLine.add(String.valueOf(hsSGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.size()));
//						alLine.add(String.valueOf(hsQGeneIdsNotInOrthologsBDBHOrSyntenyRelationship.size()));
					}
//					ArrayList<String> lineToPrint = new ArrayList<>();
//					lineToPrint.add(String.join("\t", alLine));
//					Files.write(file, lineToPrint, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
					
					counter++;


					
				}
			} //for (int i = 0; i < alOrganismIds.size() ; i++) {
			
			ArrayList<String> linesToPrint = new ArrayList<>();
			linesToPrint.add(titleAnalyse);
			linesToPrint.add(String.join("\t", alHeader));
			for (Integer keyIT : byPairwiseComparisons_biggerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison.keySet()) {
				Long countNumberPairwiseComparison_biggerOrganism_hmPercCDSOrthoBDBHRelationship =  byPairwiseComparisons_biggerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison.get(keyIT);
				Long countNumberPairwiseComparison_biggerOrganism_hmPercCDSSyntenyRelationship =  byPairwiseComparisons_biggerOrganism_hmPercCDSSyntenyRelationship2CountNumberPairwiseComparison.get(keyIT);
				Long countNumberPairwiseComparison_biggerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship =  byPairwiseComparisons_biggerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparison.get(keyIT);
				Long countNumberPairwiseComparison_biggerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship =  byPairwiseComparisons_biggerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparison.get(keyIT);
				Long countNumberPairwiseComparison_smallerOrganism_hmPercCDSOrthoBDBHRelationship =   byPairwiseComparisons_smallerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison.get(keyIT);
				Long countNumberPairwiseComparison_smallerOrganism_hmPercCDSSyntenyRelationship =   byPairwiseComparisons_smallerOrganism_hmPercCDSSyntenyRelationship2CountNumberPairwiseComparison.get(keyIT);
				Long countNumberPairwiseComparison_smallerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship =  byPairwiseComparisons_smallerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparison.get(keyIT);
				Long countNumberPairwiseComparison_smallerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship =  byPairwiseComparisons_smallerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparison.get(keyIT);
				Long sumCountNumberPairwiseComparison_hmPercCDSOrthoBDBHRelationship = countNumberPairwiseComparison_biggerOrganism_hmPercCDSOrthoBDBHRelationship + countNumberPairwiseComparison_smallerOrganism_hmPercCDSOrthoBDBHRelationship;
				Long sumCountNumberPairwiseComparison_hmPercCDSSyntenyRelationship = countNumberPairwiseComparison_biggerOrganism_hmPercCDSSyntenyRelationship + countNumberPairwiseComparison_smallerOrganism_hmPercCDSSyntenyRelationship;
				Long sumCountNumberPairwiseComparison_hmPercCDSBothOrthoBDBHAndSyntenyRelationship = countNumberPairwiseComparison_biggerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship + countNumberPairwiseComparison_smallerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship;
				Long sumCountNumberPairwiseComparison_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship = countNumberPairwiseComparison_biggerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship + countNumberPairwiseComparison_smallerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship;
				String lineIT = 
		        		keyIT
		        		+"\t"+countNumberPairwiseComparison_biggerOrganism_hmPercCDSOrthoBDBHRelationship
		        		+"\t"+countNumberPairwiseComparison_biggerOrganism_hmPercCDSSyntenyRelationship
		        		+"\t"+countNumberPairwiseComparison_biggerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship
		        		+"\t"+countNumberPairwiseComparison_biggerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship
		        		+"\t"+countNumberPairwiseComparison_smallerOrganism_hmPercCDSOrthoBDBHRelationship
		        		+"\t"+countNumberPairwiseComparison_smallerOrganism_hmPercCDSSyntenyRelationship
		        		+"\t"+countNumberPairwiseComparison_smallerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship
		        		+"\t"+countNumberPairwiseComparison_smallerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship
		        		+"\t"+sumCountNumberPairwiseComparison_hmPercCDSOrthoBDBHRelationship
		        		+"\t"+sumCountNumberPairwiseComparison_hmPercCDSSyntenyRelationship
		        		+"\t"+sumCountNumberPairwiseComparison_hmPercCDSBothOrthoBDBHAndSyntenyRelationship
		        		+"\t"+sumCountNumberPairwiseComparison_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship
		        		;
				linesToPrint.add(lineIT);
			}


			Files.write(outputFile, linesToPrint, StandardCharsets.UTF_8,
					Files.exists(outputFile) ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE);


		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}
		
		milliPrint2 = System.currentTimeMillis() - milli;
		Date dateEnd = new Date();
		System.out.println("Done "+methodNameToReport+" at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.");
		
	}

//	private static void appendWarning(Path warningFile, String methodNameToReport, String message) throws IOException {
//		String warningToPrint = "Warning from "+methodNameToReport+" : "+message;
//		ArrayList<String> linesToPrint = new ArrayList<>();
//		linesToPrint.add(warningToPrint);
//		Files.write(warningFile, linesToPrint, StandardCharsets.UTF_8,
//				Files.exists(warningFile) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
//	}

	private static void initTreeMapsBbyPairwiseComparisonsBiggerOrganismAndSmaller() {
		for (int i=0; i<=100 ; i++) {
			byPairwiseComparisons_biggerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison.put(i, (long) 0);
			byPairwiseComparisons_biggerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison.put(i, (long) 0);
			byPairwiseComparisons_biggerOrganism_hmPercCDSSyntenyRelationship2CountNumberPairwiseComparison.put(i, (long) 0);
			byPairwiseComparisons_biggerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparison.put(i, (long) 0);
			byPairwiseComparisons_biggerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparison.put(i, (long) 0);
			byPairwiseComparisons_smallerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison.put(i, (long) 0);
			byPairwiseComparisons_smallerOrganism_hmPercCDSSyntenyRelationship2CountNumberPairwiseComparison.put(i, (long) 0);
			byPairwiseComparisons_smallerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparison.put(i, (long) 0);
			byPairwiseComparisons_smallerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparison.put(i, (long) 0);
		}
	}

	private static void addToFinalHmPercCDS2Count(
			String biggerOrSmallerOrganism
			, HashSet<Integer> hsGeneIdsIT
			, HashSet<Integer> hsGeneIdOfAlignmentPairs_orthologsBDBHNotInSyntenyIT
			, HashSet<Integer> hsGeneIdOfAlignmentPairs_syntenyIT
			, HashSet<Integer> interesctGeneIdBothOrthologsBDBHNotInSyntenyAndSyntenyIT
			, HashSet<Integer> hsGeneIdsNotInOrthologsBDBHOrSyntenyRelationshipIT
			) throws Exception {
		
		TreeMap<Integer, Long> hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparisonIT = null;
		TreeMap<Integer, Long> hmPercCDSSyntenyRelationship2CountNumberPairwiseComparisonIT = null;
		TreeMap<Integer, Long> hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparisonIT = null;
		TreeMap<Integer, Long> hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparisonIT = null;
		
		if (biggerOrSmallerOrganism.compareTo("biggerOrganism")==0) {
			hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparisonIT = byPairwiseComparisons_biggerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison;
			hmPercCDSSyntenyRelationship2CountNumberPairwiseComparisonIT = byPairwiseComparisons_biggerOrganism_hmPercCDSSyntenyRelationship2CountNumberPairwiseComparison;
			hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparisonIT = byPairwiseComparisons_biggerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparison;
			hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparisonIT = byPairwiseComparisons_biggerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparison;
		} else if (biggerOrSmallerOrganism.compareTo("smallerOrganism")==0) {
			hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparisonIT = byPairwiseComparisons_smallerOrganism_hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparison;
			hmPercCDSSyntenyRelationship2CountNumberPairwiseComparisonIT = byPairwiseComparisons_smallerOrganism_hmPercCDSSyntenyRelationship2CountNumberPairwiseComparison;
			hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparisonIT = byPairwiseComparisons_smallerOrganism_hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparison;
			hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparisonIT = byPairwiseComparisons_smallerOrganism_hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparison;
		} else {
			throw new Exception("The method addToFinalHmPercCDS2Count found a biggerOrSmallerOrganism argument not supported :"+biggerOrSmallerOrganism);
		}

		//bigger organism is q
		double percCDSOrthoBDBHRelationshipIT = (double) ((double) hsGeneIdOfAlignmentPairs_orthologsBDBHNotInSyntenyIT.size() / (double) hsGeneIdsIT.size())*100;
		double percCDSSyntenyRelationshipIT = (double) ((double) hsGeneIdOfAlignmentPairs_syntenyIT.size() / (double) hsGeneIdsIT.size())*100;
		double percCDSBothOrthoBDBHAndSyntenyRelationshipIT = (double) ((double) interesctGeneIdBothOrthologsBDBHNotInSyntenyAndSyntenyIT.size() / (double) hsGeneIdsIT.size())*100;
		double percCDSNotInOrthoBDBHOrSyntenyRelationshipIT = (double) ((double) hsGeneIdsNotInOrthologsBDBHOrSyntenyRelationshipIT.size() / (double) hsGeneIdsIT.size())*100;
		int percCDSOrthoBDBHRelationship_intIT = (int) Math.round(percCDSOrthoBDBHRelationshipIT);
		int percCDSSyntenyRelationship_intIT = (int) Math.round(percCDSSyntenyRelationshipIT);
		int percCDSBothOrthoBDBHAndSyntenyRelationship_intIT = (int) Math.round(percCDSBothOrthoBDBHAndSyntenyRelationshipIT);
		int percCDSNotInOrthoBDBHOrSyntenyRelationship_intIT = (int) Math.round(percCDSNotInOrthoBDBHOrSyntenyRelationshipIT);
		
	
		if (hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparisonIT.containsKey(percCDSOrthoBDBHRelationship_intIT)) {
			Long countNumberPairwiseComparisonIT = hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparisonIT.get(percCDSOrthoBDBHRelationship_intIT);
			countNumberPairwiseComparisonIT++;
			hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparisonIT.put(percCDSOrthoBDBHRelationship_intIT, countNumberPairwiseComparisonIT);
		} else {
			throw new Exception("The method addToFinalHmPercCDS2Count has an error :"
					+ " key not found for hmPercCDSOrthoBDBHRelationship2CountNumberPairwiseComparisonIT.containsKey(percCDSOrthoBDBHRelationship_intIT)"
					+ " with key "+percCDSOrthoBDBHRelationship_intIT);
		}
		if (hmPercCDSSyntenyRelationship2CountNumberPairwiseComparisonIT.containsKey(percCDSSyntenyRelationship_intIT)) {
			Long countNumberPairwiseComparisonIT = hmPercCDSSyntenyRelationship2CountNumberPairwiseComparisonIT.get(percCDSSyntenyRelationship_intIT);
			countNumberPairwiseComparisonIT++;
			hmPercCDSSyntenyRelationship2CountNumberPairwiseComparisonIT.put(percCDSSyntenyRelationship_intIT, countNumberPairwiseComparisonIT);
		} else {
			throw new Exception("The method addToFinalHmPercCDS2Count has an error :"
					+ " key not found for hmPercCDSSyntenyRelationship2CountNumberPairwiseComparisonIT.containsKey(percCDSSyntenyRelationship_intIT)"
					+ " with key "+percCDSSyntenyRelationship_intIT);
		}
		if (hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparisonIT.containsKey(percCDSBothOrthoBDBHAndSyntenyRelationship_intIT)) {
			Long countNumberPairwiseComparisonIT = hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparisonIT.get(percCDSBothOrthoBDBHAndSyntenyRelationship_intIT);
			countNumberPairwiseComparisonIT++;
			hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparisonIT.put(percCDSBothOrthoBDBHAndSyntenyRelationship_intIT, countNumberPairwiseComparisonIT);
		} else {
			throw new Exception("The method addToFinalHmPercCDS2Count has an error :"
					+ " key not found for hmPercCDSBothOrthoBDBHAndSyntenyRelationship2CountNumberPairwiseComparisonIT.containsKey(percCDSBothOrthoBDBHAndSyntenyRelationship_intIT)"
					+ " with key "+percCDSBothOrthoBDBHAndSyntenyRelationship_intIT);
		}
		if (hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparisonIT.containsKey(percCDSNotInOrthoBDBHOrSyntenyRelationship_intIT)) {
			Long countNumberPairwiseComparisonIT = hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparisonIT.get(percCDSNotInOrthoBDBHOrSyntenyRelationship_intIT);
			countNumberPairwiseComparisonIT++;
			hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparisonIT.put(percCDSNotInOrthoBDBHOrSyntenyRelationship_intIT, countNumberPairwiseComparisonIT);
		} else {
			throw new Exception("The method addToFinalHmPercCDS2Count has an error :"
					+ " key not found for hmPercCDSNotInOrthoBDBHOrSyntenyRelationship2CountNumberPairwiseComparisonIT.containsKey(percCDSNotInOrthoBDBHOrSyntenyRelationship_intIT)"
					+ " with key "+percCDSNotInOrthoBDBHOrSyntenyRelationship_intIT);
		}

		
	}



	
	 
}
