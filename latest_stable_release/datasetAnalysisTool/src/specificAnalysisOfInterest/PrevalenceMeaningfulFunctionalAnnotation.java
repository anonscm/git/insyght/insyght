package specificAnalysisOfInterest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import fr.inra.jouy.server.queriesTable.QueriesTableMicadoQualifiers;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

public class PrevalenceMeaningfulFunctionalAnnotation {

	static String methodNameToReport = "PrevalenceMeaningfulFunctionalAnnotation";
	static String outputFile = System.getProperty("user.home")+"/PrevalenceMeaningfulFunctionalAnnotation.txt"; //default
	private class Option {
	     String flag, opt;
	     public Option(String flag, String opt) { this.flag = flag; this.opt = opt; }
	}
	public enum MethodToRun { 
		orthologsBDBHAndsyntenyRelationship
		//, 
	}
	

	private static String printUsage() {
		String usage = "";
		usage += "\nProgram "+methodNameToReport;
		usage += "\nThe supported arguments are :";
		List<MethodToRun> enumList = Arrays.asList(MethodToRun.class.getEnumConstants());
		for (MethodToRun methodToRunIT : enumList) {
			usage += "\n\t-"+methodToRunIT.toString()+" ON/OFF";
		}
		usage += "\nOnly one argument at a time is supported";
		return usage;
	}
	

	public static void main(String[] args) {
		
		long milli = System.currentTimeMillis();
		long milliPrint2 = -1;
		milliPrint2 = System.currentTimeMillis() - milli;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateStart = new Date();
		System.out.println("Starting "+methodNameToReport+" at "+dateFormat.format(dateStart));
		PrevalenceMeaningfulFunctionalAnnotation pmfa = new PrevalenceMeaningfulFunctionalAnnotation();
		ArrayList<String> argsList = new ArrayList<String>();  
		ArrayList<Option> optsList = new ArrayList<Option>();
		ArrayList<String> doubleOptsList = new ArrayList<String>();

	    for (int i = 0; i < args.length; i++) {
	        switch (args[i].charAt(0)) {
	        case '-':
	            if (args[i].length() < 2)
	                throw new IllegalArgumentException("Not a valid argument: "+args[i]);
	            if (args[i].charAt(1) == '-') {
	                if (args[i].length() < 3)
	                    throw new IllegalArgumentException("Not a valid argument: "+args[i]);
	                // --opt
	                doubleOptsList.add(args[i].substring(2, args[i].length()));
	            } else {
	                if (args.length-1 == i)
	                    throw new IllegalArgumentException("Expected arg after: "+args[i]);
	                // -opt
	                optsList.add(pmfa.new Option(args[i], args[i+1]));
	                i++;
	            }
	            break;
	        default:
	            // arg
	            argsList.add(args[i]);
	            break;
	        }
	    }

	    ArrayList<MethodToRun> alMethodToRunIT = new ArrayList<>();
	    boolean printUsage = false;
	    
	    for (String argIT : argsList) {
	    	switch (argIT) {
	        case "-help":
	        	printUsage = true;
	            break;
	        default:
	            // arg
	        	printUsage = true;
	        }
	    }
	    for (String doubleOptsArgIT : doubleOptsList) {
	    	switch (doubleOptsArgIT) {
	        case "--help":
	        	printUsage = true;
	            break;
	        default:
	            // arg
	        	printUsage = true;
	        }
	    }
	    
	    
	    if (printUsage) {
	    	printUsage();
	    	System.exit(0);
	    }
	    
	    for (Option optionIT : optsList) {
	    	switch (optionIT.flag) {
	        case "-orthologsBDBHAndsyntenyRelationship":
	        	if (optionIT.opt.matches("^ON$")) {
		        	alMethodToRunIT.add(MethodToRun.orthologsBDBHAndsyntenyRelationship	);
	        	}
	            break;
	        default:
	            // arg
	        	throw new IllegalArgumentException("The flag "+optionIT.flag+" is not supported."+printUsage());
	        }
	    }
	    
	    if (alMethodToRunIT.size() > 1) {
	    	throw new IllegalArgumentException("Only one argument at a time is supported."+printUsage());
	    } else if (alMethodToRunIT.isEmpty()) {
	    	throw new IllegalArgumentException("No flag was provided to run method."+printUsage());
	    }
	    
	    MethodToRun methodToRunIT = null;
	    for (MethodToRun methodToRunArgs : alMethodToRunIT) {
	    	methodToRunIT = methodToRunArgs;
	    }
	    
	    switch (methodToRunIT) {
        case orthologsBDBHAndsyntenyRelationship:
        	orthologsBDBHAndsyntenyRelationship();
            break;
        default:
            // arg
        	throw new IllegalArgumentException("The method "+methodToRunIT.toString()+" is not yet implemented."+printUsage());
        }


		milliPrint2 = System.currentTimeMillis() - milli;
		Date dateEnd = new Date();
		System.out.println("Done "+methodNameToReport+" at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.");
		
	}


	private static void orthologsBDBHAndsyntenyRelationship() {
		// TODO 

		String methodNameToReport = "orthologsBDBHAndsyntenyRelationship";
		
		ArrayList<String> alHeader = new ArrayList<>();
		alHeader.add("number orthologs BDBH not in synteny both with at least one meaningful functional annotation");
		alHeader.add("number orthologs BDBH not in synteny both without any meaningful functional annotation");
		alHeader.add("number orthologs BDBH not in synteny q CDS at least one meaningful functional annotation s CDS without");
		alHeader.add("number orthologs BDBH not in synteny s CDS at least one meaningful functional annotation q CDS without");
		alHeader.add("number CDSs in synteny both with at least one meaningful functional annotation");
		alHeader.add("number CDSs in synteny both without any meaningful functional annotation");
		alHeader.add("number CDSs in synteny q CDS at least one meaningful functional annotation s CDS without");
		alHeader.add("number CDSs in synteny s CDS at least one meaningful functional annotation q CDS without");
		//alHeader.add("number CDSs not in orthologs BDBH or synteny relationship with at least one meaningful functional annotation");
		//alHeader.add("number CDSs not in orthologs BDBH or synteny relationship without any meaningful functional annotation");
		
		//TODO
		//first TestMethodsServer_tableQualifiers test_checkIfQualifierIsReallyAKnownAnnotation print out example annotation considered as Meaningful ; ask JF what he things
		HashMap<String, HashSet<Integer>> hmQAccession2HsCodeFeatWithAtLeastOneMeaningfulFunctionalAnnotation = 
				QueriesTableMicadoQualifiers.getHmAccession2HsCodeFeatWithAtLeastOneMeaningfulFunctionalAnnotation(conn, hsQAccessions);
		HashMap<String, HashSet<Integer>> hmSAccession2HsCodeFeatWithAtLeastOneMeaningfulFunctionalAnnotation = 
				QueriesTableMicadoQualifiers.getHmAccession2HsCodeFeatWithAtLeastOneMeaningfulFunctionalAnnotation(conn, hsSAccessions);
		
		
	}
	
}
