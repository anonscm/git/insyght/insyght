package phenotype;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.queriesTable.QueriesTableTaxonomy;

public class OverlapListTaxonIds {

	static String methodNameToReport = "OverlapListTaxonIds";
	static String listTaxWithAtLeastOneHabitat = "XXX";
	static String listTaxWithAtLeastOnePhenotype = "XXX";
	//regex
	static Pattern pParseListTaxWithAtLeastOneHabitat = Pattern.compile("^(\\d+)\\s+.+$");
	static Pattern pParseListTaxWithAtLeastOnePhenotype = Pattern.compile("^(\\d+)\\s+.+$");
	
	public static void main(String[] args) {

		long milli = System.currentTimeMillis();
		long milliPrint2 = -1;
		milliPrint2 = System.currentTimeMillis() - milli;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateStart = new Date();
		System.out.println("Starting overlapTaxonIds at "+dateFormat.format(dateStart)+"\n");

		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();
			
			//QueriesTableOrganisms.getHsTaxonIds_withIspublicAndIsOnlyWithElement(conn, true, true);
				// taille list taxon ids initial = 5168
				// Il y a 1175 organisms de bacteria_2017 (=22.7%) qui sont présents dans au moins une relation Taxon-Habitat de la base Florilège.
				// Il y a 460 organisms de bacteria_2017 (=8.9%) qui sont présents dans au moins une relation Taxon-Phenotype de la base Florilège.
			//QueriesTableTaxonomy.getHsTaxonIds(conn, true, true);
				// idem result up
			//QueriesTableTaxonomy.getHsTaxonIds(conn, false, true);
			
			HashSet<Integer> hsTaxonIdsFromOrigamiDb = QueriesTableTaxonomy.getHsTaxonIds(conn, true, true);
			System.err.println("Finished fetching taxonIds from database origami : hsTaxonIdsFromOrigamiDb.size() = "+hsTaxonIdsFromOrigamiDb.size());
			
//			ArrayList<String> lines = new ArrayList<>(hsTaxonIdsFromOrigamiDb.size());
//			lines.add("TAXONID");
//			for(Integer taxonIdsIT : hsTaxonIdsFromOrigamiDb)
//				lines.add(taxonIdsIT.toString());
//			Path file = Paths.get("~/listTaxonidsOrganisms.txt");
//			Files.write(file, lines, Charset.forName("UTF-8"));
			
			
			InputStream ins = null; // raw byte-stream
			Reader r = null; // cooked reader
			BufferedReader br = null; // buffered for readLine()

			//parse listTaxWithAtLeastOneHabitat into hsTaxonIdsFromAtLeastOneHabitatFile
			HashSet<Integer> hsTaxonIdsFromAtLeastOneHabitatFile = new HashSet<>();
			try {
			    String s;
			    ins = new FileInputStream(listTaxWithAtLeastOneHabitat);
			    r = new InputStreamReader(ins, "UTF-8"); // leave charset out for default
			    br = new BufferedReader(r);
			    while ((s = br.readLine()) != null) {
			    				    	
			    	// création d'un moteur de recherche
			    	Matcher m = pParseListTaxWithAtLeastOneHabitat.matcher(s);
			    	// lancement de la recherche de toutes les occurrences
			    	boolean b = m.matches();
			    	// si recherche fructueuse
			    	if(b) {
			    		hsTaxonIdsFromAtLeastOneHabitatFile.add(Integer.parseInt(m.group(1)));
			    	}
			    }
			}
			catch (Exception e)
			{
			    System.err.println(e.getMessage()); // handle exception
			}
			finally {
			    if (br != null) { try { br.close(); } catch(Throwable t) { /* ensure close happens */ } }
			    if (r != null) { try { r.close(); } catch(Throwable t) { /* ensure close happens */ } }
			    if (ins != null) { try { ins.close(); } catch(Throwable t) { /* ensure close happens */ } }
			}
			System.err.println("Finished parse listTaxWithAtLeastOneHabitat into hsTaxonIdsFromAtLeastOneHabitatFile : hsTaxonIdsFromAtLeastOneHabitatFile.size() = "+hsTaxonIdsFromAtLeastOneHabitatFile.size());
		
			
			ins = null; // raw byte-stream
			r = null; // cooked reader
			br = null; // buffered for readLine()

//			
			//parse listTaxWithAtLeastOnePhenotype into hsTaxonIdsFromAtLeastOnePhenotypeFile
			HashSet<Integer> hsTaxonIdsFromAtLeastOnePhenotypeFile = new HashSet<>();
			try {
			    String s;
			    ins = new FileInputStream(listTaxWithAtLeastOnePhenotype);
			    r = new InputStreamReader(ins, "UTF-8"); // leave charset out for default
			    br = new BufferedReader(r);
			    while ((s = br.readLine()) != null) {
			    	// création d'un moteur de recherche
			    	Matcher m = pParseListTaxWithAtLeastOnePhenotype.matcher(s);
			    	// lancement de la recherche de toutes les occurrences
			    	boolean b = m.matches();
			    	// si recherche fructueuse
			    	if(b) {
			    		hsTaxonIdsFromAtLeastOnePhenotypeFile.add(Integer.parseInt(m.group(1)));
			    	}
			    }
			}
			catch (Exception e)
			{
			    System.err.println(e.getMessage()); // handle exception
			}
			finally {
			    if (br != null) { try { br.close(); } catch(Throwable t) { /* ensure close happens */ } }
			    if (r != null) { try { r.close(); } catch(Throwable t) { /* ensure close happens */ } }
			    if (ins != null) { try { ins.close(); } catch(Throwable t) { /* ensure close happens */ } }
			}

			System.err.println("Finished parse listTaxWithAtLeastOnePhenotype into hsTaxonIdsFromAtLeastOnePhenotypeFile : hsTaxonIdsFromAtLeastOnePhenotypeFile.size() = "+hsTaxonIdsFromAtLeastOnePhenotypeFile.size());

			
			//get intersection of hsTaxonIdsFromAtLeastOneHabitatFile and hsTaxonIdsFromOrigamiDb
			HashSet<Integer> hsTaxonIdsFromOrigamiDbWithInteresctAtLeastOneHabitat = new HashSet<>(hsTaxonIdsFromOrigamiDb);
			hsTaxonIdsFromOrigamiDbWithInteresctAtLeastOneHabitat.retainAll(hsTaxonIdsFromAtLeastOneHabitatFile);
			System.err.println("Finished get intersection of hsTaxonIdsFromAtLeastOneHabitatFile and hsTaxonIdsFromOrigamiDb : hsTaxonIdsFromOrigamiDbWithInteresctAtLeastOneHabitat.size() = "+hsTaxonIdsFromOrigamiDbWithInteresctAtLeastOneHabitat.size());
			
			//get intersection of hsTaxonIdsFromAtLeastOnePhenotypeFile and hsTaxonIdsFromOrigamiDb
			HashSet<Integer> hsTaxonIdsFromOrigamiDbWithInteresctAtLeastOnePhenotype = new HashSet<>(hsTaxonIdsFromOrigamiDb);
			hsTaxonIdsFromOrigamiDbWithInteresctAtLeastOnePhenotype.retainAll(hsTaxonIdsFromAtLeastOnePhenotypeFile);
			System.err.println("Finished get intersection of hsTaxonIdsFromAtLeastOnePhenotypeFile and hsTaxonIdsFromOrigamiDb : hsTaxonIdsFromOrigamiDbWithInteresctAtLeastOnePhenotype.size() = "+hsTaxonIdsFromOrigamiDbWithInteresctAtLeastOnePhenotype.size());
			
			
			
//			Runtime r = Runtime.getRuntime();
//			Iterator<Integer> iterAtLeastOneHabitat = hsTaxonIdsFromOrigamiDb.iterator();
//			int countTaxonIdsPresent = 0;
//			int countTaxonIdsNotPresent = 0;
//			while (iterAtLeastOneHabitat.hasNext()) {
//			    //System.out.println(iter.next());
//				Integer taxonIdIT = iterAtLeastOneHabitat.next();
//				boolean taxonIdsPresent = false;
//				//String grepCmdIT = "grep -P '^"+taxonIdIT+"\\t' "+listTaxWithAtLeastOneHabitat;
//				String grepCmdIT = "host -t a google.com";
//
//				Process p = r.exec(grepCmdIT);
//				p.waitFor();
//				BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
//				String line = "";
//				while ((line = b.readLine()) != null) {
//
//					System.err.println("line = "+line);
//										
//					if ( ! line.isEmpty()) {
//						if (taxonIdsPresent) {
//							  throw new Exception("Error for taxon id = "+taxonIdIT
//									  + "; multiple lines found with command = "+grepCmdIT
//									  );
//						} else {
//							taxonIdsPresent = true;
//						}
//					}
//				}
//				b.close();
//				if (taxonIdsPresent) {
//					countTaxonIdsPresent++;
//				} else {
//					countTaxonIdsNotPresent++;
//				}
//			}
//			System.err.println("Finished checking against listTaxWithAtLeastOneHabitat : countTaxonIdsPresent = "+countTaxonIdsPresent
//					+ " ; countTaxonIdsNotPresent = "+countTaxonIdsNotPresent
//					);
//			
//			Iterator<Integer> iterAtLeastOnePhenotype = hsTaxonIdsFromOrigamiDb.iterator();
//			countTaxonIdsPresent = 0;
//			countTaxonIdsNotPresent = 0;
//			while (iterAtLeastOnePhenotype.hasNext()) {
//								
//			    //System.out.println(iter.next());
//				Integer taxonIdIT = iterAtLeastOnePhenotype.next();
//				boolean taxonIdsPresent = false;
//				String grepCmdIT = "grep -P '^"+taxonIdIT+"\t' "+listTaxWithAtLeastOnePhenotype;
//				
//				
//				Process p = r.exec(grepCmdIT);
//				p.waitFor();
//				BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
//				String line = "";
//				while ((line = b.readLine()) != null) {
//					if ( ! line.isEmpty()) {
//						if (taxonIdsPresent) {
//							  throw new Exception("Error for taxon id = "+taxonIdIT
//									  + "; multiple lines found with command = "+grepCmdIT
//									  );
//						} else {
//							taxonIdsPresent = true;
//						}
//					}
//				}
//				b.close();
//				if (taxonIdsPresent) {
//					countTaxonIdsPresent++;
//				} else {
//					countTaxonIdsNotPresent++;
//				}
//			}
//			System.err.println("Finished checking against listTaxWithAtLeastOnePhenotype : countTaxonIdsPresent = "+countTaxonIdsPresent
//					+ " ; countTaxonIdsNotPresent = "+countTaxonIdsNotPresent
//					);
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}
		
		milliPrint2 = System.currentTimeMillis() - milli;
		Date dateEnd = new Date();
		System.out.println("\nDone overlapTaxonIds at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.\n");
		
	}

}
