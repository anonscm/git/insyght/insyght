package comparaisonByCategoriesOfCDSs;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.io.File;
import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ICEIME.serveur.pojos.fileMapping.ICEIMEItem_fromFile;
import ICEIME.serveur.pojos.fileMapping.ICEIMEItem_fromFile.Category_of_elementType;
import ICEIME.serveur.pojos.fileMapping.ICEIMEItem_fromFile.ICEIMEInsertion;
import ICEIME.serveur.pojos.fileMapping.ICEIMEItem_fromFile.IntegraseType;
//import ICEIME.serveur.DatabaseConfICEDB;
//import ICEIME.serveur.pojos.databaseMapping.ICEIMEItem;
//import ICEIME.serveur.pojos.databaseMapping.LightICEIMEItem;
//import ICEIME.serveur.queriesTable.QueriesTableMobileElement;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.queriesTable.mutlipleTables.ComparativeGenomics;
import fr.inra.jouy.shared.TransStartStopGeneInfo;
import fr.inra.jouy.shared.pojos.databaseMapping.GeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;


/*
// how to create executable jars from eclipse
https://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2Ftasks%2Ftasks-37.htm
1.0 Select this script in package explorer
1.1 From the menu bar's File menu, select Export.
2. Expand the Java node and select Runnable JAR file. Click Next.
3. In the  Opens the Runnable JAR export wizard Runnable JAR File Specification page, select a 'Java Application' launch configuration to use to create a runnable JAR.
4. In the Export destination field, either type or click Browse to select a location for the JAR file : datasetAnalysisTool/executableJars/comparaisonByCategoriesOfCDSs
5. Select an appropriate library handling strategy : Extract required librairies into generated JAR
6. Optionally, you can also create an ANT script to quickly regenerate a previously created runnable JAR file : No
 */


//-pathToFileICEIME "/home/tlacroix/montages_reseau/home_maiage/Desktop/documents/Projets/ICE-IME/liste_ICE_IME_Dynamics/excel_file/ICE + IME_124 genomes_30_03_2017.csv - ICE_IME_w_locations.tsv"
//-pathToFileICEIME "/home/tlacroix/montages_reseau/home_maiage/Desktop/documents/Projets/ICE-IME/liste_ICE_IME_Dynamics/excel_file/ICE_+_IME_124 genomes_21_05_2019.tsv"

// analyse with r script /home/tlacroix/montages_reseau/home_maiage/Desktop/documents/Projets/ICE-IME/comparison_genomic_analysis/R/analysisPlot.R


/*
** outputFile : outputDir/resultAnalysisICEIME_allStreptococcus_byCategoriesCDSICEIME.txt

** Output column :
- Element_name
- Genome_id_accession
- Genome_id_in_comparaisondb
- Organism_name_and_strain
- Organism_id_in_comparaisondb
- Element_type
- Element_family
- Element_left_end
- Element_right_end
- Is_mobile_element_accretion
- Is_mobile_element_insertion
- Integrase_type
- integrase_location_compared_to_target
- integrase_orientation_compared_to_target
- insertion_gene_function
- number_of_target_gene
- number_of_integrase
- number_of_relaxase
- number_of_coupling_protein
- number_of_virB4
- Locus_tag
- Category_of_CDS_in_relationship_to_ICE_IME
- Gene name
- Protein_id
- List_products
- Length_residues
And for each scopes scopeIT of compared organisms defined as argument :
	 - Number_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT
	 - Percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT
	 - Number_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT
	 - Percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT
	 - Average_percent_identity_alignment_scope_"+scopeIT
	 - Average_percent_query_and_subject_lenght_coverage_alignment_scope_"+scopeIT
	 - Median_Evalue_alignment_scope_"+scopeIT
*/
	 
	 
public class AnalysisICEIME {

	static String methodNameToReport = "AnalysisICEIME";
	private class Option {
		String flag, opt;
		public Option(String flag, String opt) { this.flag = flag; this.opt = opt; }
	}

	//	public enum MethodToRun { 
	//		categoriesCDSICEIME
	//	}

//	static Pattern ComparisonWithFeaturedList = Pattern.compile("^.*"
//			+ "<br\\/><b><i>Comparison with \\d+ genomes from the featured list:<\\/i><\\/b><ul>"
//			+ "<li>"
//			+ "Number of genomes with presence of at least 1 CDS conserved during evolution: (\\d+)" // group(1)
//			+ " \\((.+)" // group(2)
//			+ "\\%\\)"
//			+ "<\\/li>"
//			+ "<li>"
//			+ "Number of genomes with presence of at least 1 CDS in synteny relationship: (\\d+)" // group(3)
//			+ " \\((.+)" // group(4)
//			+ "\\%\\)"
//			+ "<\\/li>"
//			+ "<li>Average \\% identity for the alignment\\(s\\)\\: (.+)" // group(5)
//			+ "\\%<\\/li>"
//			+ "<li>Average \\% query and subject lenght coverage for the alignment\\(s\\): (.+)" // group(6)
//			+ "\\%<\\/li>"
//			+ "<li>Median Evalue for the alignment\\(s\\): (.+)" // group(7)
//			+ "<\\/li>"
//			+ "<\\/ul>"
//			+ ".*$");

//	static Pattern ComparisonWithMainList = Pattern.compile("^.*"
//			+ "<br\\/><b><i>Comparison with \\d+ genomes from the main list:<\\/i><\\/b><ul>"
//			+ "<li>"
//			+ "Number of genomes with presence of at least 1 CDS conserved during evolution: (\\d+)" // group(1)
//			+ " \\((.+)" // group(2)
//			+ "\\%\\)"
//			+ "<\\/li>"
//			+ "<li>"
//			+ "Number of genomes with presence of at least 1 CDS in synteny relationship: (\\d+)" // group(3)
//			+ " \\((.+)" // group(4)
//			+ "\\%\\)"
//			+ "<\\/li>"
//			+ "<li>Average \\% identity for the alignment\\(s\\)\\: (.+)" // group(5)
//			+ "\\%<\\/li>"
//			+ "<li>Average \\% query and subject lenght coverage for the alignment\\(s\\): (.+)" // group(6)
//			+ "\\%<\\/li>"
//			+ "<li>Median Evalue for the alignment\\(s\\): (.+)" // group(7)
//			+ "<\\/li>"
//			+ "<\\/ul>"
//			+ ".*$");
	
	static Pattern geneIdWithVersion = Pattern.compile(
			"^"
			+ "(.+)" // group(1)
			+ "\\.\\d+"
			+ "$");
	static Pattern geneIdDotPseudo = Pattern.compile(
			"^"
			+ "(.+)" // group(1)
			+ "\\.pseudo(gene)?.*"
			+ "$");
	
	public enum Category_of_CDS_in_relationship_to_ICE_IME { 
		nextTargetGeneLeft("CDS border ICE/IME next target gene")
		,nextTargetGeneRight("CDS border ICE/IME next target gene")
		,oppositeTargetGene("CDS border ICE/IME opposite target gene")
		,integrase("Integrase")
		,relaxase("Relaxase")
		,CouplingProtein("Coupling protein")
		,virB4("VirB4")
		,CDSWithinICEIME("CDS within ICE/IME")
		,CDSOtherElementAccretionInsertion("CDS other(s) element(s) in accretion / insertion")
		,targetGene("Target gene")
		;

		private String displayName;

		Category_of_CDS_in_relationship_to_ICE_IME(String displayName) {
			this.displayName = displayName;
		}

		public String displayName() { return displayName; }

		// Optionally and/or additionally, toString.
		@Override public String toString() { return displayName; }
	}

	public enum Location_target_gene_compared_to_ICE_IME { 
		downstream("downstream")
		, upstream("upstream")
		, upstreamDownstream("upstream / downstream");

		private String displayName;

		Location_target_gene_compared_to_ICE_IME(String displayName) {
			this.displayName = displayName;
		}

		public String displayName() { return displayName; }

		// Optionally and/or additionally, toString.
		@Override public String toString() { return displayName; }
	}

	//script scope variables
	public static String outputDir = System.getProperty("user.home"); // default
	public static String typeOfAnalysis = "categoriesCDSICEIME"; // default
	public static LinkedHashMap<String,ArrayList<String>> hmScope2IncludeExcludeFeatured = new LinkedHashMap<>();
	public static String pathToFileICEIME = "";
	public static boolean VERBOSE = true; // default
	public static boolean parseFileWithoutGenomicComparisonAnalysis = false; // default
	public static int grabCDSWithAtLeastOnePbInThisStartStopLocusIT = 75; 	// at 60, ICE_ScoC232_rumA will fail over Error categoriesCDSICEIME : unexpected failure : gene id 6276344 = SCRE_1268, AGU73091.1 = Coupling_protein_embl_cds, 1294243 - 1294704
																			// at 73, IME_Spy2096_SNF2, found redundance of genes between the following categories of CDS within the ICE IME : CDS border ICE/IME opposite target gene and Integrase ; redundant gene(s) : MGAS2096_Spy1140 [ABF36192.1](1092958-1094889) ; 1093031 - 1103987

	private static String getUsageArgsAsString() {

		String usage = "";
		usage += "\nProgram "+methodNameToReport;
		usage += "\nThe supported arguments are :";
		usage += "\n\t-outputDir {path to output directory} : default : user.home. If multiple -outputDir, only takes into consideration the last one. If no -outputDir argument is provided, create a single outputDir with default values";
		usage += "\n\t-typeOfAnalysis {\"categoriesCDSICEIME\"} : default : categoriesCDSICEIME. If multiple -typeOfAnalysis, only takes into consideration the last one. If no -typeOfAnalysis argument is provided, create a single typeOfAnalysis with default values";
		usage += "\n\t-scope_name {STRING} : create a new scope with default values. Scope arguments between this scope_name and the next scope_name or the end of the list will be applied to this scope_name. If no -scope_name argument is provided, create a scope_name with default values";
		usage += "\n\t-scope_taxon_id_include {\"species\", \"all\", \"none\", DIGIT_taxon_id} :  default : \"all\"";
		usage += "\n\t-scope_taxon_id_exclude {\"species\", \"all\", \"none\", DIGIT_taxon_id} :  default : \"none\"";
		//usage += "\n\t-scope_featured_or_main_to_consider {\"featured\", \"main_list\"} :  default : \"featured\"";
		usage += "\n\t-pathToFileICEIME {path to output directory} : the path to the dynamic excel file that list ICE IME";
		usage += "\n\t-VERBOSE {ON/OFF} : whether or not this script should print information as it is running (Default ON)";
		usage += "\n\t-parseFileWithoutGenomicComparisonAnalysis {ON/OFF} : whether or not this script should just parse the file without performing the genomic comparison analysis (Default OFF)";
		return usage;
	}

	public static void main(String[] args) throws Exception {

		long milli = System.currentTimeMillis();
		long milliPrint2 = -1;
		milliPrint2 = System.currentTimeMillis() - milli;

		AnalysisICEIME aii = new AnalysisICEIME();
		//ArrayList<String> argsList = new ArrayList<String>();  
		ArrayList<Option> optsList = new ArrayList<Option>();
		//ArrayList<String> doubleOptsList = new ArrayList<String>();

		for (int i = 0; i < args.length; i++) {
			switch (args[i].charAt(0)) {
			case '-':
				if (args[i].length() < 2)
					throw new IllegalArgumentException("Not a valid argument: "+args[i]+"\n"+getUsageArgsAsString());
				if (args[i].charAt(1) == '-') {
					if (args[i].compareTo("--help")==0) {
						System.out.println(getUsageArgsAsString());
						System.exit(0);
					}
					if (args[i].length() < 3) {
						throw new IllegalArgumentException("Not a valid argument: "+args[i]+"\n"+getUsageArgsAsString());
					}
					
					// --opt
					//doubleOptsList.add(args[i].substring(2, args[i].length()));
					throw new IllegalArgumentException("Not a valid argument: "+args[i]+"\n"+getUsageArgsAsString());
				} else {
					if (args[i].compareTo("-help")==0) {
						System.out.println(getUsageArgsAsString());
						System.exit(0);
					}
					if (args.length-1 == i) {
						throw new IllegalArgumentException("Expected arg after: "+args[i]+"\n"+getUsageArgsAsString());
					}
					// -opt
					optsList.add(aii.new Option(args[i], args[i+1]));
					
					//System.err.println(args[i]+" ; "+args[i+1]);
					
					i++;
					
				}
				break;
			default:
				// arg
				//argsList.add(args[i]);
				throw new IllegalArgumentException("Not a valid argument: "+args[i]+"\n"+getUsageArgsAsString());
				
				//System.err.println(args[i]+"; args[i].charAt(0)="+args[i].charAt(0)+"; NOT "+args[i+1]);
				//
				//break;
			}
		}

	    
		//ArrayList<MethodToRun> alMethodToRunIT = new ArrayList<>();
		/*boolean printUsage = false;
		for (String argIT : argsList) {
			switch (argIT) {
			case "-help":
				printUsage = true;
				break;
			default:
				// arg
				printUsage = false;
			}
		}
		for (String doubleOptsArgIT : doubleOptsList) {
			switch (doubleOptsArgIT) {
			case "--help":
				printUsage = true;
				break;
			default:
				// arg
				printUsage = false;
			}
		}
		if (printUsage) {
			printUsage();
			System.exit(0);
		}*/

	    
		String currentScopeName = "";
		for (Option optionIT : optsList) {
			switch (optionIT.flag) {
			case "-outputDir":
				//if (optionIT.opt.matches("^ON$")) {
				outputDir = optionIT.opt;
				//}
				break;
			case "-typeOfAnalysis":
				if (optionIT.opt.matches("^categoriesCDSICEIME$")) {
					typeOfAnalysis = "categoriesCDSICEIME";
				} else {
					throw new IllegalArgumentException("The value "+optionIT.opt+" for the flag "+optionIT.flag+" is not supported."+getUsageArgsAsString());
				}
				break;
			case "-scope_name":
				if (hmScope2IncludeExcludeFeatured.containsKey(optionIT.opt)) {
					throw new IllegalArgumentException("The value "+optionIT.opt+" for the flag "+optionIT.flag+" is duplicated."+getUsageArgsAsString());
				} else {
					currentScopeName = optionIT.opt;
					ArrayList<String> alIncludeExcludeFeaturedIT = new ArrayList<>();
					alIncludeExcludeFeaturedIT.add("all"); // idx 0 : -scope_taxon_id_include ["species", "all", "none", DIGIT_taxon_id] // default : all
					alIncludeExcludeFeaturedIT.add("none"); // idx 1 : -scope_taxon_id_exclude ["species", "all", "none", DIGIT_taxon_id] // default : none
					//alIncludeExcludeFeaturedIT.add("featured"); // idx 2 : -scope_featured_or_main_to_consider ["featured", "main_list"] // default : featured
					hmScope2IncludeExcludeFeatured.put(optionIT.opt, alIncludeExcludeFeaturedIT);
				}
				break;
			case "-scope_taxon_id_include":
				if (currentScopeName.isEmpty()) {
					throw new IllegalArgumentException("The flag -scope_taxon_id_include "+optionIT.opt+" must be set after its related flag -scope_name."+getUsageArgsAsString());
				} else {
					if (optionIT.opt.compareTo("species") == 0
							|| optionIT.opt.compareTo("all") == 0
							|| optionIT.opt.compareTo("none") == 0
							|| optionIT.opt.matches("^\\d+$")
							) {
						ArrayList<String> alIncludeExcludeFeaturedIT = hmScope2IncludeExcludeFeatured.get(currentScopeName);
						alIncludeExcludeFeaturedIT.set(0, optionIT.opt); // idx 0 : -scope_taxon_id_include ["species", "all", "none", DIGIT_taxon_id] // default : all
					} else {
						throw new IllegalArgumentException("The value of the flag -scope_taxon_id_include "+optionIT.opt+" is incorrect. The value must be one of the following : species, all, none, DIGIT_taxon_id."+getUsageArgsAsString());
					}
				}
				break;
			case "-scope_taxon_id_exclude":
				if (currentScopeName.isEmpty()) {
					throw new IllegalArgumentException("The flag -scope_taxon_id_exclude "+optionIT.opt+" must be set after its related flag -scope_name."+getUsageArgsAsString());
				} else {
					if (optionIT.opt.compareTo("species") == 0
							|| optionIT.opt.compareTo("all") == 0
							|| optionIT.opt.compareTo("none") == 0
							|| optionIT.opt.matches("^\\d+$")
							) {
						ArrayList<String> alIncludeExcludeFeaturedIT = hmScope2IncludeExcludeFeatured.get(currentScopeName);
						alIncludeExcludeFeaturedIT.set(1, optionIT.opt); // idx 1 : -scope_taxon_id_exclude ["species", "all", "none", DIGIT_taxon_id] // default : none
					} else {
						throw new IllegalArgumentException("The value of the flag -scope_taxon_id_exclude "+optionIT.opt+" is incorrect. The value must be one of the following : species, all, none, DIGIT_taxon_id."+getUsageArgsAsString());
					}
				}
				break;
//			case "-scope_featured_or_main_to_consider":
//				if (currentScopeName.isEmpty()) {
//					throw new IllegalArgumentException("The flag -scope_taxon_id_exclude "+optionIT.opt+" must be set after its related flag -scope_name."+getUsageArgsAsString());
//				} else {
//					if (optionIT.opt.compareTo("featured") == 0
//							|| optionIT.opt.compareTo("main_list") == 0
//							) {
//						ArrayList<String> alIncludeExcludeFeaturedIT = hmScope2IncludeExcludeFeatured.get(currentScopeName);
//						alIncludeExcludeFeaturedIT.set(2, optionIT.opt); // idx 2 : -scope_featured_or_main_to_consider ["featured", "main_list"] // default : featured
//					} else {
//						throw new IllegalArgumentException("The value of the flag -scope_featured_or_main_to_consider "+optionIT.opt+" is incorrect. The value must be one of the following : featured, main_list."+getUsageArgsAsString());
//					}
//				}
//				break;
			case "-pathToFileICEIME":
				pathToFileICEIME = optionIT.opt;
				break;
			case "-VERBOSE":
				if (optionIT.opt.compareTo("ON") == 0) {
					VERBOSE = true;
				} else if (optionIT.opt.compareTo("OFF") == 0) {
					VERBOSE = false;
				} else {
					throw new IllegalArgumentException("The value of the flag -VERBOSE "+optionIT.opt+" is incorrect. The value must be one of the following : ON, OFF."+getUsageArgsAsString());
				}
				break;
			case "-parseFileWithoutGenomicComparisonAnalysis":
				if (optionIT.opt.compareTo("ON") == 0) {
					parseFileWithoutGenomicComparisonAnalysis = true;
				} else if (optionIT.opt.compareTo("OFF") == 0) {
					parseFileWithoutGenomicComparisonAnalysis = false;
				} else {
					throw new IllegalArgumentException("The value of the flag -parseFileWithoutGenomicComparisonAnalysis "+optionIT.opt+" is incorrect. The value must be one of the following : ON, OFF."+getUsageArgsAsString());
				}
				break;
			default:
				// arg
				throw new IllegalArgumentException("The flag "+optionIT.flag+" is not supported."+getUsageArgsAsString());
			}
		}

		if (hmScope2IncludeExcludeFeatured.isEmpty()) {
			ArrayList<String> alIncludeExcludeFeaturedIT = new ArrayList<>();
			alIncludeExcludeFeaturedIT.add("all"); // idx 0 : -scope_taxon_id_include ["species", "all", "none", DIGIT_taxon_id] // default : all
			alIncludeExcludeFeaturedIT.add("none"); // idx 1 : -scope_taxon_id_exclude ["species", "all", "none", DIGIT_taxon_id] // default : none
			hmScope2IncludeExcludeFeatured.put("DefaultScope", alIncludeExcludeFeaturedIT);
		}
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateStart = new Date();
	    if (VERBOSE) {
	    	System.out.println("Starting "+methodNameToReport+" at "+dateFormat.format(dateStart));
	    	System.out.println("");
	    }
	    
	    if (VERBOSE) {
	    	//recap of your options
	    	System.out.println(
	    			"Running script with the following options : "
	    			+ "\n\t-outputDir : " + outputDir
	    			+ "\n\t-typeOfAnalysis : " + typeOfAnalysis
	    			+ "\n\t-pathToFileICEIME : " + pathToFileICEIME
	    			+ "\n\t-VERBOSE : " + VERBOSE
	    			);
	    	for (Entry<String, ArrayList<String>> entry : hmScope2IncludeExcludeFeatured.entrySet()) {
				String scopeIT = entry.getKey();
				ArrayList<String> alIncludeExcludeFeaturedIT = entry.getValue();
				System.out.println(
		    			"\t-scope_name : " + scopeIT
		    			+ " ; -scope_taxon_id_include : " + alIncludeExcludeFeaturedIT.get(0)
		    			+ " ; -scope_taxon_id_exclude : " + alIncludeExcludeFeaturedIT.get(1)
		    			//+ " ; -scope_featured_or_main_to_consider : " + alIncludeExcludeFeaturedIT.get(2)
		    			);
			}
	    	System.out.println("");
	    }
	    
		if (typeOfAnalysis.compareTo("categoriesCDSICEIME") == 0 ){
			categoriesCDSICEIME();
		}

		milliPrint2 = System.currentTimeMillis() - milli;
		Date dateEnd = new Date();
		if (VERBOSE) {
			System.out.println("Done "+methodNameToReport+" at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.");
		}

	}

	private static void categoriesCDSICEIME() throws Exception {

		String methodNameToReport = "categoriesCDSICEIME";

		long milli = System.currentTimeMillis();
		long milliPrint2 = -1;
		milliPrint2 = System.currentTimeMillis() - milli;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateStart = new Date();
		
		 if (VERBOSE) {
			 System.out.println("Starting "+methodNameToReport+" at "+dateFormat.format(dateStart));
		    	System.out.println("");
		 }

		Connection connComparaisonDB = null;
		Connection connTaxoDB = null;
		//Connection connICEDB = null;

		try {

			connComparaisonDB = DatabaseConf.getConnection_db();
			connTaxoDB = DatabaseConf.getConnection_taxo();
			//connICEDB = DatabaseConfICEDB.getConnection();

			String outputFile_asString = outputDir+"/resultAnalysisICEIME_allStreptococcus_byCategoriesCDSICEIME.txt";
			ArrayList<String> linesToPrint = new ArrayList<>();
			
			//print header
			ArrayList<String> alLineHeaderToPrint = getHeaderAsAlString();
			linesToPrint.add(String.join("\t", alLineHeaderToPrint));

			File outputFile_asJavaIoFile = new File(outputFile_asString);
			if(outputFile_asJavaIoFile.exists()){
				outputFile_asJavaIoFile.delete();
				//try {
				outputFile_asJavaIoFile.createNewFile();
				//} catch (IOException e) {
				//	e.printStackTrace();
				//}
			}
			Path outputFile = Paths.get(outputFile_asString);
			deleteFileIfExists(outputFile_asString);
			//String comparaisonDBName = connComparaisonDB.getCatalog();
			//String titleAnalyse = "Analysis of CDSs conserved during evolution within or around known ICE/IME ; x=Category of CDSs in relationship to ICE/IME ; y = Percentage of compared genomes with presence\n of at least 1 CDS conserved during evolution";
			//titleAnalyse += "(comparative genomique databases : "+comparaisonDBName+")";

			//Category_of_CDS_in_relationship_to_ICE_IME
			//Location_target_gene_compared_to_ICE_IME
			//hmScopesIncludeExcludeFeatured
			// idx 0 : -scope_taxon_id_include ["species", "all", "none", DIGIT_taxon_id] // default : all
			// idx 1 : -scope_taxon_id_exclude ["species", "all", "none", DIGIT_taxon_id] // default : none
			// idx 2 : -scope_featured_or_main_to_consider ["featured", "main_list"] // default : featured



			//get all the ICE and IME
			//ArrayList<LightICEIMEItem> allICEIME = ICEIME.serveur.queriesTable.QueriesTableMobileElement.getAllLightICEIME(connICEDB);

			
			// pathToFileICEIME
			// https://docs.google.com/spreadsheets/d/1tAjFTh6xzjDLKAH5b2GFTPaP3vwLTzRvxJfEjCESt_U/edit#gid=131534732
			// /home/tlacroix/montages_reseau/home_maiage/Desktop/documents/Projets/ICE-IME/ICE_IME_Dynamics/excel_file/ICE + IME_124 genomes_30_03_2017.csv - ICE_IME_w_locations.tsv

			
			
			ArrayList<ICEIMEItem_fromFile> alPotentialHostICEIME = new ArrayList<>();
			ArrayList<ICEIMEItem_fromFile> alPotentialGuestICEIME = new ArrayList<>();
			ArrayList<ICEIMEItem_fromFile> allICEIME = parseFileICEIME(
					pathToFileICEIME
					, alPotentialHostICEIME
					, alPotentialGuestICEIME
					);
			
			//arraylist of 
			// idx 0 : leftEndGuest
			// idx 1 : rightEndGuest
			HashMap<String, ArrayList<ArrayList<Integer>>> ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME = buildICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME(
					//allICEIME
					alPotentialHostICEIME
					, alPotentialGuestICEIME
					);
			
			ArrayList<ICEIMEItem_fromFile> ICEIMEAccessionNotFoundInComparativeGenomicsDB = new ArrayList<>();
			//int countICEIMEWithElementId = 0;
			HashSet<Integer> hsElementIds_onlyICEIMEWithNoProblems = new HashSet<>();
			HashSet<Integer> hsOrgaIds_onlyICEIMEWithNoProblems = new HashSet<>();
			//ArrayList<ICEIMEItem_fromFile> ICEIMEAccessionFoundInComparativeGenomicsDBButDifferentVersion = new ArrayList<>();
			int countShouldBeICEIMESuccessfullyAnalysed = 0;
			int countICEIMEcheckedAgainstComparaisonDB = 0;
			ArrayList<String> warningFails = new ArrayList<>();
			int countSignatureProteinIdentifierNotFoundInCompaDb = 0;
			int countElementTypeICENotAllFourSignatureProteins = 0;
			int countElementDefinedAsHostButNoGuestICEIMEFound = 0;
			int countElementNotDefinedAsHostButGuestICEIMEFound = 0;
			int countTargetGeneIdentifierProblemsInCompaDb = 0;
			int countTargetGeneFailedToDetermineCDSRightLeftOfICE = 0;
			int countTargetRedundanceGenesBetweenDifferentCategoriesCDS = 0;
			//int countTargetGeneIdentifiedFailedToBeFoundInDb = 0;
			int countTargetGeneTRNAButFoundCompaDB = 0;
			int countTargetGeneIsTRNA = 0;
			int countNoTargetGeneIdentifierSpecified = 0;
			int countNoTargetGeneIdentifiedBecauseAnnotatedAsPseudogene = 0;
			int countNoTargetGeneIdentifiedBecauseMarkedAsQuestionMarkORnoneORnot_annotated = 0;
			int countNoTargetGeneIdentifiedBecauseIntergenic = 0;
			int countNoTargetGeneIdentifiedBecauseNonSpecific = 0;
			int countNoTargetGeneIdentifiedButWrongSpecificity = 0;
			int countICEIMEWith1GeneTarget = 0;
			int countICEIMEWithMultipleGeneTargetOnTheLeft = 0;
			int countICEIMEWithMultipleGeneTargetOnTheRight = 0;
			int countICEIMEWithMultipleGeneTargetOnBothSides = 0;
			int countShouldBeICEIMESuccessfullyAnalysed_intTyr = 0;
			int countShouldBeICEIMESuccessfullyAnalysed_intSer = 0;
			int countShouldBeICEIMESuccessfullyAnalysed_intDDE = 0;
			int countShouldBeICEIMESuccessfullyAnalysed_intUNKNOWN = 0;
			int countShouldBeICEIMESuccessfullyAnalysed_intUNSURE = 0;
			LOOP_ALL_ICEIME : for (ICEIMEItem_fromFile liiIT : allICEIME) {
				countICEIMEcheckedAgainstComparaisonDB++;

				if (VERBOSE) {
			    	System.out.println(
			    				"Querying the comparaison genomic database for ICE/IME "+liiIT.getElementName()+" : " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()
			    			);
			    }
				//get info on genome and strain
//				AbstractMap.SimpleEntry<String, Integer> seGenomeAccession2Version = ICEIME.serveur.queriesTable.QueriesTableElements.getAccessionAndVersion_withElementId(
//						connICEDB
//						, liiIT.getId_element()
//						);
//				String accessionFromICEIMESourceIT = seGenomeAccession2Version.getKey();
//				int versionFromICEIMESourceIT = seGenomeAccession2Version.getValue();


				
				int elementIdIT = fr.inra.jouy.server.queriesTable.QueriesTableElements.getElementIdWithAccession (
						connComparaisonDB
						, liiIT.getGenomeId() //String accession
						, false //boolean shouldBePresentInTable
						, true //boolean checkValidFreeStringInput
						);
				
				//System.err.println(liiIT.getGenomeId()+" ; "+elementIdIT);
				
				if (elementIdIT < 0) {
					// accnum not found in comparative genomics db
					ICEIMEAccessionNotFoundInComparativeGenomicsDB.add(liiIT);
					if (VERBOSE) {
				    	System.out.println(
				    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : accnum not found in comparative genomics db, skipping"
				    			);
				    }
					continue LOOP_ALL_ICEIME;
				}

				//ElementItem private int version = -1;//integer
				//RQ : new method inspired from getLightElementItemWithElementId, + organismId already there ?
				//countICEIMEWithElementId++;
				HashSet<Integer> hsElementIds = new HashSet<>();
				hsElementIds.add(elementIdIT);
				LightElementItem leiIT = fr.inra.jouy.server.queriesTable.QueriesTableElements.getLightElementItemWithElementId(
						connComparaisonDB
						, elementIdIT
						);
				

				//HashSet<Integer> hsOrgaIds = new HashSet<>();
				//hsOrgaIds.add(leiIT.getOrganismId());
				LightOrganismItem loiIT = fr.inra.jouy.server.queriesTable.QueriesTableOrganisms.getLightOrganismItemWithOrgaId(
						connComparaisonDB
						, leiIT.getOrganismId()
						, true
						, true
						);
				

				HashMap<String, HashSet<Integer>> hmScope2HsOrgaIdsFeaturedGenomes = new HashMap<>();
				for (Entry<String, ArrayList<String>> entryHmScope2 : hmScope2IncludeExcludeFeatured.entrySet()) {
					String scopeNameIT = entryHmScope2.getKey();
					ArrayList<String> alIncludeExcludeFeatured = entryHmScope2.getValue();
					String scopeIncludeIT = alIncludeExcludeFeatured.get(0);
					String scopeExcludeIT = alIncludeExcludeFeatured.get(1);
					//String scopeFeaturedIT = alIncludeExcludeFeatured.get(2);

					HashSet<Integer> hsTaxonIdsFeatured = new HashSet<>();
					HashSet<Integer> hsTaxonIdsScopeInclude = fr.inra.jouy.server.queriesTable.QueriesTableTaxonomy.getHsTaxonIds_withScopeToConsider(
							connTaxoDB
							, scopeIncludeIT
							, loiIT.getTaxonId()
							);
					hsTaxonIdsFeatured.addAll(hsTaxonIdsScopeInclude);
					
					HashSet<Integer> hsTaxonIdsScopeExclude = fr.inra.jouy.server.queriesTable.QueriesTableTaxonomy.getHsTaxonIds_withScopeToConsider(
							connTaxoDB
							, scopeExcludeIT
							, loiIT.getTaxonId()
							);
					hsTaxonIdsFeatured.removeAll(hsTaxonIdsScopeExclude);
					
					
					ArrayList<LightOrganismItem> alAllOrganismsInDB = fr.inra.jouy.server.queriesTable.QueriesTableOrganisms.getAlLightOrganismItems_orderByScoreAscIfProvided(
							connComparaisonDB
							, null //ArrayList<Integer> limitToAlOrgaIds
							, null //ArrayList<Integer> excludeAlOrgaIds
							, null //HashMap<Integer, Integer> hmOrgaId2fScoreToStore
							, false //boolean setScoreTo0IfNotProvided
							, false //boolean orderByScoreAsc
							, true //boolean onlyPublic
							, true //boolean onlyWithElement
							);
					
					HashSet<Integer> hsOrgaIdsFeaturedGenomes = new HashSet<>();
					//HashSet<Integer> hsOrgaIdsMainList = new HashSet<>();
					for (LightOrganismItem loi_fromAlAllOrganismsInDB : alAllOrganismsInDB) {
						if (loi_fromAlAllOrganismsInDB.getOrganismId() == loiIT.getOrganismId()) { 
							//skip, ref organism
						} else  if (hsTaxonIdsFeatured.contains(loi_fromAlAllOrganismsInDB.getTaxonId())) {
							hsOrgaIdsFeaturedGenomes.add(loi_fromAlAllOrganismsInDB.getOrganismId());
						}
						else {
							//hsOrgaIdsMainList.add(loi_fromAlAllOrganismsInDB.getOrganismId());
						}
					}
					
					hmScope2HsOrgaIdsFeaturedGenomes.put(scopeNameIT, hsOrgaIdsFeaturedGenomes);
					if (VERBOSE) {
				    	System.out.println(
				    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : getting organisms for scope "+scopeNameIT+" : "+hsOrgaIdsFeaturedGenomes.size() + " organisms found."
				    			);
				    }
					
				}	

				//if (elementItemIT.getVersion() != versionFromICEIMESourceIT) {
				//	// accnum found in comparative genomics db but different version
				//	ICEIMEAccessionFoundInComparativeGenomicsDBButDifferentVersion.add(liiIT);
				//	continue LOOP_ALL_ICEIME;
				//}

				ArrayList<String> ICEIMEGeneralInfoIT = new ArrayList<>();// ICEIMEGeneralInfo
				// BEFORE WAS : Element_name	Strain	Genome_id_accession	Organism_id_origami_cg	Organism_id_bacteria_2017	Element_type	Integrase_type	integrase_location_compared_to_target	integrase_orientation_compared_to_target	Family	Left_end	Right_end	Accretion	Insertion	Location_target_gene_compared_to_ICE_IME
				ICEIMEGeneralInfoIT.add(liiIT.getElementName()); //Mobile_element_name
				ICEIMEGeneralInfoIT.add(liiIT.getGenomeId()); //Genome_accession
				ICEIMEGeneralInfoIT.add(""+leiIT.getElementId()); //Genome_id_in_comparaisondb
				ICEIMEGeneralInfoIT.add(loiIT.getFullName()); //Organism_name_and_strain
				ICEIMEGeneralInfoIT.add(""+loiIT.getOrganismId()); //Organism_id_in_comparaisondb				
				ICEIMEGeneralInfoIT.add(liiIT.getElementType().toString()); //Mobile_element_type
				ICEIMEGeneralInfoIT.add(liiIT.getFamily()); //Mobile_element_family
				ICEIMEGeneralInfoIT.add(""+liiIT.getLeftEnd()); //Mobile_element_left_end
				ICEIMEGeneralInfoIT.add(""+liiIT.getRightEnd()); //Mobile_element_right_end
				ICEIMEGeneralInfoIT.add(liiIT.getAccretion().toString()); //Is_mobile_element_accretion
				ICEIMEGeneralInfoIT.add(liiIT.getInsertion().toString()); //Is_mobile_element_insertion
				ICEIMEGeneralInfoIT.add(liiIT.getIntegraseType().toString()); //Integrase_type
				ICEIMEGeneralInfoIT.add(liiIT.getIntegraseLocation()); //integrase_location_compared_to_target
				ICEIMEGeneralInfoIT.add(liiIT.getIntegraseOrientation()); //integrase_orientation_compared_to_target
				ICEIMEGeneralInfoIT.add(liiIT.getInsertionGeneFunction()); //insertion_gene_function
				if (VERBOSE) {
			    	System.out.println(
			    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : printing ICE IME General Info for "+liiIT.getElementName()
			    			);
			    }

				//ICEIMEGeneralInfoIT.add(liiIT.getTarget_name()); // target_name
				//ICEIMEGeneralInfoIT.add(liiIT.getTarget_position()); //target_position : 3' / 5' / W10 / None
				//ICEIMEGeneralInfoIT.add(liiIT.getTarget_insertion()); //target_insertion: in / out
				//ICEIMEGeneralInfoIT.add(liiIT.getTarget_type()); //target_type: gene / tRNA / rRNA / protein_family
				//ICEIMEGeneralInfoIT.add(liiIT.getTarget_comment()); //target_comment
				//ICEIMEGeneralInfoIT.add(liiIT.getTarget_validation()); //target_validation

				if (liiIT.getLeftEnd() >= liiIT.getRightEnd()) {
					throw new Exception("Error categoriesCDSICEIME : unexpected liiIT.getLeftEnd() ("+liiIT.getLeftEnd()+") >= liiIT.getRightEnd() ("+liiIT.getRightEnd()+")");
				}

				//get all the CDS related to the mobile element by category
				// see public enum Category_of_CDS_in_relationship_to_ICE_IME
				ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT = new ArrayList<>();
				listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT.add(leiIT.getElementId());
				listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT.add(liiIT.getLeftEnd());
				listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT.add(liiIT.getRightEnd());
				
				ArrayList<Integer> alGeneIdsICEIME = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
						connComparaisonDB
						, listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT
						, null // listGeneIdsToExclude // set to null or empty if none
						, true // grabCDSWithAtLeastOnePbInThisStartStopLocus
						//, true // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
						, grabCDSWithAtLeastOnePbInThisStartStopLocusIT // sometimes the DR can be inside the intgrase
						, true // orderCDSByStartAsc
						, true // shouldBePresentInTable
						);
				if (VERBOSE) {
			    	System.out.println(
			    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : found "+alGeneIdsICEIME.size()+" CDS in ICE / IME"
			    			);
			    }

				HashMap<Category_of_CDS_in_relationship_to_ICE_IME, ArrayList<Integer>> hmCategoCDSRelationshipToICEIME2alGeneIds = new HashMap<>();
				//init all result arrays
				for (Category_of_CDS_in_relationship_to_ICE_IME categoryCDSRelationshipICEIME : Category_of_CDS_in_relationship_to_ICE_IME.values()) { 
				    hmCategoCDSRelationshipToICEIME2alGeneIds.put(categoryCDSRelationshipICEIME, new ArrayList<>());
				}
				
				boolean integraseFound = false;
				boolean relaxaseFound = false;
				boolean couplingProteinFound = false;
				boolean virB4Found = false;
				
				//,integrase("Integrase")
				ArrayList<Integer> alGeneIdsIntegrase = new ArrayList<>();
				if ( liiIT.getIntegraseReference() != null && ! liiIT.getIntegraseReference().isEmpty() ) {
					// can be locus tag, or protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsIntegrase = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getIntegraseReference()
							, warningFails
							, liiIT.getElementName()
							, "integrase"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countSignatureProteinIdentifierNotFoundInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : integrase identifier "+liiIT.getIntegraseReference()+" not found in database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				} else if ( liiIT.getIntegraseEmblCds() != null && ! liiIT.getIntegraseEmblCds().isEmpty() ) {
					// can be locus tag, protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsIntegrase = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getIntegraseEmblCds()
							, warningFails
							, liiIT.getElementName()
							, "integrase"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countSignatureProteinIdentifierNotFoundInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : integrase identifier "+liiIT.getIntegraseEmblCds()+" not found in database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				}
				if ( alGeneIdsIntegrase != null && ! alGeneIdsIntegrase.isEmpty() ) {
					if (alGeneIdsICEIME.containsAll(alGeneIdsIntegrase)) {
						hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.integrase, alGeneIdsIntegrase);
						alGeneIdsICEIME.removeAll(alGeneIdsIntegrase);
						integraseFound = true;
					} else {
						throw new Exception("Error categoriesCDSICEIME : unexpected failure for alGeneIdsICEIME.containsAll(alGeneIdsIntegrase)"
								+ " ; getIntegraseReference="+liiIT.getIntegraseReference()
								+ " ; getIntegraseEmblCds="+liiIT.getIntegraseEmblCds()
								+ " ; alGeneIdsIntegrase="+alGeneIdsIntegrase.toString()
								+ " ; alGeneIdsICEIME="+alGeneIdsICEIME.toString());
					}
				}
				if (VERBOSE) {
			    	System.out.println(
			    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : found "+alGeneIdsIntegrase.size()+" integrase(s) in ICE / IME "
			    						+ " ; IntegraseReference="+liiIT.getIntegraseReference()
			    						+ " ; IntegraseEmblCds="+liiIT.getIntegraseEmblCds()
			    						+ " ; "+alGeneIdsICEIME.size()+" CDSs left to assign in ICE / IME."
			    			);
			    }
				

				//,relaxase("Relaxase")
				ArrayList<Integer> alGeneIdsRelaxase = new ArrayList<>();
				if ( liiIT.getRelaxaseReference() != null && ! liiIT.getRelaxaseReference().isEmpty() ) {
					// can be locus tag, or protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsRelaxase = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getRelaxaseReference()
							, warningFails
							, liiIT.getElementName()
							, "relaxase"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countSignatureProteinIdentifierNotFoundInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : relaxase identifier "+liiIT.getRelaxaseReference()+" not found in database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				} else if ( liiIT.getRelaxaseEmblCds() != null && ! liiIT.getRelaxaseEmblCds().isEmpty() ) {
					// can be locus tag, protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsRelaxase = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getRelaxaseEmblCds()
							, warningFails
							, liiIT.getElementName()
							, "relaxase"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countSignatureProteinIdentifierNotFoundInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : relaxase identifier "+liiIT.getRelaxaseEmblCds()+" not found in database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				}
				if ( alGeneIdsRelaxase != null && ! alGeneIdsRelaxase.isEmpty() ) {
					if (alGeneIdsICEIME.containsAll(alGeneIdsRelaxase)) {
						hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.relaxase, alGeneIdsRelaxase);
						alGeneIdsICEIME.removeAll(alGeneIdsRelaxase);
						relaxaseFound = true;
					} else {
						throw new Exception("Error categoriesCDSICEIME : unexpected failure for alGeneIdsICEIME.containsAll(alGeneIdsRelaxase)"
								+ " ; getRelaxaseReference="+liiIT.getRelaxaseReference()
								+ " ; getRelaxaseEmblCds="+liiIT.getRelaxaseEmblCds()
								+ " ; alGeneIdsRelaxase="+alGeneIdsRelaxase.toString()
								+ " ; alGeneIdsICEIME="+alGeneIdsICEIME.toString());
					}
				}
				if (VERBOSE) {
			    	System.out.println(
			    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : found "+alGeneIdsRelaxase.size()+" relaxase(s) in ICE / IME "
			    						+ " ; RelaxaseReference="+liiIT.getRelaxaseReference()
			    						+ " ; RelaxaseEmblCds="+liiIT.getRelaxaseEmblCds()
			    						+ " ; "+alGeneIdsICEIME.size()+" CDSs left to assign in ICE / IME."
			    			);
			    }
				

				//,CouplingProtein("Coupling protein")
				ArrayList<Integer> alGeneIdsCouplingProtein = new ArrayList<>();
				if ( liiIT.getCouplingProteinReference() != null && ! liiIT.getCouplingProteinReference().isEmpty() ) {
					// can be locus tag, or protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsCouplingProtein = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getCouplingProteinReference()
							, warningFails
							, liiIT.getElementName()
							, "coupling protein"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countSignatureProteinIdentifierNotFoundInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : coupling protein identifier "+liiIT.getCouplingProteinReference()+" not found in database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				} else if ( liiIT.getCouplingProteinEmblCds() != null && ! liiIT.getCouplingProteinEmblCds().isEmpty() ) {
					// can be locus tag, protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsCouplingProtein = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getCouplingProteinEmblCds()
							, warningFails
							, liiIT.getElementName()
							, "coupling protein"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countSignatureProteinIdentifierNotFoundInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : coupling protein identifier "+liiIT.getCouplingProteinEmblCds()+" not found in database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				}
				if ( alGeneIdsCouplingProtein != null && ! alGeneIdsCouplingProtein.isEmpty() ) {
					if (alGeneIdsICEIME.containsAll(alGeneIdsCouplingProtein)) {
						hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.CouplingProtein, alGeneIdsCouplingProtein);
						alGeneIdsICEIME.removeAll(alGeneIdsCouplingProtein);
						couplingProteinFound = true;
					} else {
						throw new Exception("Error categoriesCDSICEIME : unexpected failure for alGeneIdsICEIME.containsAll(alGeneIdsCouplingProtein)"
								+ " ; getCouplingProteinReference="+liiIT.getCouplingProteinReference()
								+ " ; getCouplingProteinEmblCds="+liiIT.getCouplingProteinEmblCds()
								+ " ; alGeneIdsCouplingProtein="+alGeneIdsCouplingProtein.toString()
								+ " ; alGeneIdsICEIME="+alGeneIdsICEIME.toString());
					}
				}
				if (VERBOSE) {
			    	System.out.println(
			    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : found "+alGeneIdsCouplingProtein.size()+" coupling protein(s) in ICE / IME "
			    						+ " ; CouplingProteinReference="+liiIT.getCouplingProteinReference()
			    						+ " ; CouplingProteinEmblCds="+liiIT.getCouplingProteinEmblCds()
			    						+ " ; "+alGeneIdsICEIME.size()+" CDSs left to assign in ICE / IME."
			    			);
			    }
				
				//,virB4("VirB4")
				ArrayList<Integer> alGeneIdsVirB4 = new ArrayList<>();
				if ( liiIT.getVirb4_reference() != null && ! liiIT.getVirb4_reference().isEmpty() ) {
					// can be locus tag, or protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsVirB4 = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getVirb4_reference()
							, warningFails
							, liiIT.getElementName()
							, "virB4"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countSignatureProteinIdentifierNotFoundInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : virB4 identifier "+liiIT.getVirb4_reference()+" not found in database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				} else if ( liiIT.getVirB4Cds() != null && ! liiIT.getVirB4Cds().isEmpty() ) {
					// can be locus tag, protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsVirB4 = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getVirB4Cds()
							, warningFails
							, liiIT.getElementName()
							, "virB4"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countSignatureProteinIdentifierNotFoundInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : virB4 identifier "+liiIT.getVirB4Cds()+" not found in database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				}
				if ( alGeneIdsVirB4 != null && ! alGeneIdsVirB4.isEmpty() ) {
					if (alGeneIdsICEIME.containsAll(alGeneIdsVirB4)) {
						hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.virB4, alGeneIdsVirB4);
						alGeneIdsICEIME.removeAll(alGeneIdsVirB4);
						virB4Found = true;
					} else {
						throw new Exception("Error categoriesCDSICEIME : unexpected failure for alGeneIdsICEIME.containsAll(alGeneIdsvirB4)"
								+ " ; getVirB4Reference="+liiIT.getVirb4_reference()
								+ " ; getVirB4EmblCds="+liiIT.getVirB4Cds()
								+ " ; alGeneIdsVirB4="+alGeneIdsVirB4.toString()
								+ " ; alGeneIdsICEIME="+alGeneIdsICEIME.toString());
					}
				}
				if (VERBOSE) {
			    	System.out.println(
			    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : found "+alGeneIdsVirB4.size()+" VirB4(s) in ICE / IME "
			    						+ " ; VirB4Reference="+liiIT.getVirb4_reference()
			    						+ " ; VirB4EmblCds="+liiIT.getVirB4Cds()
			    						+ " ; "+alGeneIdsICEIME.size()+" CDSs left to assign in ICE / IME."
			    			);
			    }

				//if ICE, need to have found all 4 signature proteins
				if (liiIT.getElementType().compareTo(Category_of_elementType.ICE)==0) {
					if (integraseFound && relaxaseFound && couplingProteinFound && virB4Found) {
						//ok
					} else {
						warningFails.add("WARNING for the element "+liiIT.getElementName()+". It is of type "+liiIT.getElementType().toString()+" but not all the 4 signature proteins are found : "
								+ " integrase found = "+integraseFound
								+ ", relaxase found = "+relaxaseFound
								+ ", coupling protein found = "+couplingProteinFound
								+ ", virB4 found = "+virB4Found);
						countElementTypeICENotAllFourSignatureProteins++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : Warning for the element "+liiIT.getElementName()+". It is of type "+liiIT.getElementType().toString()+" but not all the 4 signature proteins are found, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
						
					}
				}
				
				
				// CDSOtherElementAccretionInsertion("CDS other(s) element(s) in accretion / insertion")
				if (liiIT.getInsertion().compareTo(ICEIMEInsertion.host)==0 || liiIT.getInsertion().compareTo(ICEIMEInsertion.host_and_guest)==0 ) {
					//find the other ICE IME that are guest of this one
					// use ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME
					if (ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME.containsKey(liiIT.getElementName())) {
						ArrayList<Integer> alGeneIdsCDSOtherElementInsertion = new ArrayList<>();
						ArrayList<ArrayList<Integer>> alAlStartStopGuestIT = ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME.get(liiIT.getElementName());
						String logStartStops = "";
						for (ArrayList<Integer> alStartStopGuestIT : alAlStartStopGuestIT ){
							//int leftEndGuest = ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME.get(liiIT.getElementName()).get(0);
							//int rightEndGuest = ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME.get(liiIT.getElementName()).get(1);
							int leftEndGuest = alStartStopGuestIT.get(0);
							int rightEndGuest = alStartStopGuestIT.get(1);
							logStartStops += " ("+leftEndGuest+"-"+rightEndGuest+")";
							ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedGuestICEIME = new ArrayList<>();
							listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedGuestICEIME.add(leiIT.getElementId());
							listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedGuestICEIME.add(leftEndGuest);
							listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedGuestICEIME.add(rightEndGuest);
							ArrayList<Integer> alGeneIdsCDSOtherElementInsertion_inter = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
									connComparaisonDB
									, listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedGuestICEIME
									, null // listGeneIdsToExclude // set to null or empty if none
									, true // grabCDSWithAtLeastOnePbInThisStartStopLocus
									//, false // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
									, grabCDSWithAtLeastOnePbInThisStartStopLocusIT // sometimes the DR can be inside the intgrase
									, true // orderCDSByStartAsc
									, true // shouldBePresentInTable
									);
							alGeneIdsCDSOtherElementInsertion.addAll(alGeneIdsCDSOtherElementInsertion_inter);
							//ArrayList<ArrayList<Integer>> al_0AlGeneIdsCDSWithinICEIME_1AlGeneIdsCDSOtherElementAccretionInsertion = getAl_0AlGeneIdsCDSWithinICEIME_1AlGeneIdsCDSOtherElementAccretionInsertion(liiIT);
						}
						
						if (alGeneIdsICEIME.containsAll(alGeneIdsCDSOtherElementInsertion)) {
							hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.CDSOtherElementAccretionInsertion, alGeneIdsCDSOtherElementInsertion);
							alGeneIdsICEIME.removeAll(alGeneIdsCDSOtherElementInsertion);
						} else {
							throw new Exception("Error categoriesCDSICEIME : unexpected failure for alGeneIdsICEIME.containsAll(alGeneIdsCDSOtherElementInsertion)"
									+ " ; element name = "+liiIT.getElementName()
									+ " ; insertion type = "+liiIT.getInsertion().toString()
									+ " ; start - stop Guest ="+logStartStops
									+ " ; alGeneIdsICEIME="+alGeneIdsICEIME.toString()
									+ " ; alGeneIdsCDSOtherElementInsertion="+alGeneIdsCDSOtherElementInsertion.toString()
									);
							
						}	
					} else {
						warningFails.add(
								"WARNING for ICE IME "+liiIT.getElementName()
								+ " : this element is defined as insertion guest ("+liiIT.getInsertion().toString()
								+ ") but no guest ICE IME was found."
								);
						countElementDefinedAsHostButNoGuestICEIMEFound++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : "
					    				+ "WARNING for ICE IME "+liiIT.getElementName()
										+ " : this element is defined as insertion guest ("+liiIT.getInsertion().toString()
										+ ") but no guest ICE IME was found."
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				} else if ( ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME.containsKey(liiIT.getElementName()) ) {
					warningFails.add(
							"WARNING for ICE IME "+liiIT.getElementName()
							+ " : this element is not defined as insertion guest ("+liiIT.getInsertion().toString()
							+ ") but a guest ICE IME "
							+ "("+ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME.get(liiIT.getElementName()).get(0)
							+"-"
							+ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME.get(liiIT.getElementName()).get(1)
							+")"
							+ " was found."
							);
					countElementNotDefinedAsHostButGuestICEIMEFound++;
					if (VERBOSE) {
				    	System.out.println(
				    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : "
				    				+ "WARNING for ICE IME "+liiIT.getElementName()
									+ " : this element is not defined as insertion guest ("+liiIT.getInsertion().toString()
									+ ") but a guest ICE IME "
									+ "("+ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME.get(liiIT.getElementName()).get(0)
									+"-"
									+ICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME.get(liiIT.getElementName()).get(1)
									+")"
									+ " was found."
				    			);
				    }
					continue LOOP_ALL_ICEIME;
				}
				

				
				//,targetGene("Target gene")
				ArrayList<Integer> alGeneIdsTargetGene = new ArrayList<>();
				if ( liiIT.getInsertionGeneReference() != null && ! liiIT.getInsertionGeneReference().isEmpty() ) {
					// can be locus tag, or protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsTargetGene = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getInsertionGeneReference()
							, warningFails
							, liiIT.getElementName()
							, "target gene"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countTargetGeneIdentifierProblemsInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : target gene identifier "+liiIT.getInsertionGeneReference()
					    				+" has some poblem being mapped to the genomic comparaison database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				} else if ( liiIT.getInsertionGeneCds() != null && ! liiIT.getInsertionGeneCds().isEmpty() ) {
					// can be locus tag, protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsTargetGene = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getInsertionGeneCds()
							, warningFails
							, liiIT.getElementName()
							, "target gene"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countTargetGeneIdentifierProblemsInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : target gene identifier "+liiIT.getInsertionGeneCds()
					    				+" has some poblem being mapped to the genomic comparaison database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				} else if ( liiIT.getInsertionGeneLocusTag() != null && ! liiIT.getInsertionGeneLocusTag().isEmpty() ) {
					// can be locus tag, protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsTargetGene = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getInsertionGeneLocusTag()
							, warningFails
							, liiIT.getElementName()
							, "target gene"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countTargetGeneIdentifierProblemsInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : target gene identifier "+liiIT.getInsertionGeneLocusTag()
					    				+" has some poblem being mapped to the genomic comparaison database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				} else if ( liiIT.getInsertionGene() != null && ! liiIT.getInsertionGene().isEmpty() ) {
					// can be locus tag, protein id ; error if genomic location
					int sizeWarningFailsBefore = warningFails.size();
					alGeneIdsTargetGene = getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
							connComparaisonDB
							, hsElementIds
							, liiIT.getInsertionGene()
							, warningFails
							, liiIT.getElementName()
							, "target gene"
							);
					if (sizeWarningFailsBefore != warningFails.size()) {
						// querying failed, error stored in warningFails, continue
						countTargetGeneIdentifierProblemsInCompaDb++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : target gene identifier \""+liiIT.getInsertionGene()
					    				+"\" has some poblem being mapped to the genomic comparaison database, fail this ICE/IME"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					}
				}
				if ( alGeneIdsTargetGene != null && ! alGeneIdsTargetGene.isEmpty() ) {

					if (liiIT.getElementName().toLowerCase().matches("^.*trna.*$")) {
						warningFails.add(
								"WARNING : Target gene identified for "+liiIT.getElementName()+" is identified as tRNA but was found in the compariason database"
								+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
								+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
								+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
								+ " ; getInsertionGene="+liiIT.getInsertionGene()
						);
						countTargetGeneTRNAButFoundCompaDB++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
					    				+ "WARNING : Target gene identified for "+liiIT.getElementName()+" is identified as tRNA but was found in the compariason database (see warning below)"
										//+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
										//+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
										//+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
										//+ " ; getInsertionGene="+liiIT.getInsertionGene()
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					} else {
						hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.targetGene, alGeneIdsTargetGene);
						//if target gene was listed among alGeneIdsICEIME, remove it
						alGeneIdsICEIME.removeAll(alGeneIdsTargetGene);
					}


				} else {
					if (liiIT.getElementName().toLowerCase().matches("^.*trna.*$")) {
						countTargetGeneIsTRNA++;
						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
					    				+ " The target gene for "+liiIT.getElementName()+"is a tRNA, this ICE IME is skipped"
					    			);
					    }
						continue LOOP_ALL_ICEIME;
					} else {
						String catAllTargetGeneReference = 
								liiIT.getInsertionGeneReference()
								+ " " + liiIT.getInsertionGeneCds()
								+ " " + liiIT.getInsertionGeneLocusTag()
								+ " " + liiIT.getInsertionGene()
								;
						catAllTargetGeneReference = catAllTargetGeneReference.trim();
						if (catAllTargetGeneReference.isEmpty()) {
							countNoTargetGeneIdentifierSpecified++;
							if (VERBOSE) {
						    	System.out.println(
						    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
						    				+ " No target gene identifier was specified for "+liiIT.getElementName()
											+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
											+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
											+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
											+ " ; getInsertionGene="+liiIT.getInsertionGene()
						    			);
						    }
							continue LOOP_ALL_ICEIME;
						} else if (catAllTargetGeneReference.matches("^.*\\?.*$")
								|| catAllTargetGeneReference.matches("^.*unknown.*$")
								|| catAllTargetGeneReference.matches("^.*not\\s+known.*$")
								|| catAllTargetGeneReference.toLowerCase().matches("^.*none.*$")
								|| catAllTargetGeneReference.matches("^.*absent.*$")
								|| catAllTargetGeneReference.toLowerCase().matches("^.*not[\\s_-]+annotated.*$")
								|| catAllTargetGeneReference.toLowerCase().matches("^.*feat_exist_no_locus_tag.*$")
								|| catAllTargetGeneReference.toLowerCase().matches("^.*no_feat.*$")
								|| catAllTargetGeneReference.toLowerCase().matches("^.*wrong annotation.*$")
								) {
							countNoTargetGeneIdentifiedBecauseMarkedAsQuestionMarkORnoneORnot_annotated++;
							if (VERBOSE) {
						    	System.out.println(
						    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
						    				+ " No target gene was identified for "+liiIT.getElementName() + " because all or part of the identifiers were marked as question mark or none or not annotated, etc."
											+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
											+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
											+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
											+ " ; getInsertionGene="+liiIT.getInsertionGene()
						    			);
						    }
							continue LOOP_ALL_ICEIME;
						} else if (catAllTargetGeneReference.matches("^.*intergenic.*$")
								) {
							
							countNoTargetGeneIdentifiedBecauseIntergenic++;
							if (VERBOSE) {
						    	System.out.println(
						    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
						    				+ " No target gene was identified for "+liiIT.getElementName() + " because all or part of the identifiers were marked as intergenic"
											+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
											+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
											+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
											+ " ; getInsertionGene="+liiIT.getInsertionGene()
						    			);
						    }
							continue LOOP_ALL_ICEIME;
						} else if ( catAllTargetGeneReference.matches("^.+\\s+NS$")
								|| catAllTargetGeneReference.matches("^NS\\s+.+$")
								|| catAllTargetGeneReference.matches("^NS$")
								) {
							
							countNoTargetGeneIdentifiedBecauseNonSpecific++;
							if (VERBOSE) {
						    	System.out.println(
						    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
						    				+ " No target gene was identified for "+liiIT.getElementName() + " because all or part of the identifiers were marked as NS (non specific)"
											+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
											+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
											+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
											+ " ; getInsertionGene="+liiIT.getInsertionGene()
						    			);
						    }
							continue LOOP_ALL_ICEIME;
							
						} else if ( catAllTargetGeneReference.toLowerCase().matches("^.*integrase\\s+specific\\s+of\\s+.+,\\s+but.+$")
								) {
							countNoTargetGeneIdentifiedButWrongSpecificity++;
							if (VERBOSE) {
						    	System.out.println(
						    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
						    				+ " No target gene was identified for "+liiIT.getElementName() + " because the identifiers for the target gene refer to a wrong specificity (integrase specific of XXX, but )"
											+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
											+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
											+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
											+ " ; getInsertionGene="+liiIT.getInsertionGene()
						    			);
						    }
							continue LOOP_ALL_ICEIME;
							
						} else {

							Matcher mDotPseudo = geneIdDotPseudo.matcher(catAllTargetGeneReference);
							if (mDotPseudo.matches() ) {
								countNoTargetGeneIdentifiedBecauseAnnotatedAsPseudogene++;
								if (VERBOSE) {
							    	System.out.println(
							    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
							    				+ " No target gene was identified for "+liiIT.getElementName() + " because the identifiers for the target gene are annotated as pseudogene"
												+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
												+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
												+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
												+ " ; getInsertionGene="+liiIT.getInsertionGene()
							    			);
							    }
								continue LOOP_ALL_ICEIME;
							} else {

								throw new Exception("Error categoriesCDSICEIME : No target gene identified for "+liiIT.getElementName()
										+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
										+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
										+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
										+ " ; getInsertionGene="+liiIT.getInsertionGene()
										);
//								warningFails.add(
//										"WARNING : No target gene identified for "+liiIT.getElementName()
//										+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
//										+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
//										+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
//										+ " ; getInsertionGene="+liiIT.getInsertionGene()
//								);
//								countTargetGeneIdentifiedFailedToBeFoundInDb++;
//								if (VERBOSE) {
//							    	System.out.println(
//							    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
//							    				+ " WARNING : No target gene identified for "+liiIT.getElementName()
//												+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
//												+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
//												+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
//												+ " ; getInsertionGene="+liiIT.getInsertionGene()
//							    			);
//							    }
//								continue LOOP_ALL_ICEIME;
							}
									
							
						}
						
						
					}
					
				}
				if (VERBOSE) {
			    	System.out.println(
			    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : found "+alGeneIdsTargetGene.size()+" target gene(s) in ICE / IME "
										+ " ; getInsertionGeneReference="+liiIT.getInsertionGeneReference()
										+ " ; getTargetGeneEmblCds="+liiIT.getInsertionGeneCds()
										+ " ; getInsertionGeneLocusTag="+liiIT.getInsertionGeneLocusTag()
										+ " ; getInsertionGene="+liiIT.getInsertionGene()
										//+ " ; alGeneIdsTargetGene="+alGeneIdsTargetGene.toString()
			    			);
			    }

				// CDSWithinICEIME
				hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.CDSWithinICEIME, alGeneIdsICEIME);
				
				

				//addNextTargetGene, addOppositeTargetGene
				//nextTargetGene4("CDS border ICE/IME next target gene + 4")
				//,nextTargetGene3("CDS border ICE/IME next target gene + 3")
				//,nextTargetGene2("CDS border ICE/IME next target gene + 2")
				//,nextTargetGene1("CDS border ICE/IME next target gene + 1")
				//,oppositeTargetGene1("CDS border ICE/IME opposite target gene + 1")
				//	,oppositeTargetGene2("CDS border ICE/IME opposite target gene + 2")
				//	,oppositeTargetGene3("CDS border ICE/IME opposite target gene + 3")
				//	,oppositeTargetGene4("CDS border ICE/IME opposite target gene + 4");
				boolean boolCountICEIMEWith1GeneTarget = false;
				boolean boolCountICEIMEWithMultipleGeneTargetOnBothSides = false;
				boolean boolCountICEIMEWithMultipleGeneTargetOnTheRight = false;
				boolean boolCountICEIMEWithMultipleGeneTargetOnTheLeft = false;
				if (alGeneIdsTargetGene == null || alGeneIdsTargetGene.isEmpty()) {
					//error
					throw new Exception("Error categoriesCDSICEIME : alGeneIdsTargetGene == null || alGeneIdsTargetGene.isEmpty()");
				} else {
					ArrayList<TransStartStopGeneInfo> alTransStartStopGeneInfo = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAlTransientStartStopGeneInfoWithListGeneIds(
							connComparaisonDB
							, alGeneIdsTargetGene
							);
					if (alTransStartStopGeneInfo.size() == 1) {	// 1 target gene
						// Get start stop of target gene
						int startTargetGeneIT = alTransStartStopGeneInfo.get(0).getStart();
						int stopTargetGeneIT = alTransStartStopGeneInfo.get(0).getStop();
						// Compare to left and right of ICE IME to determine where it stands
						if (startTargetGeneIT < liiIT.getLeftEnd()
								&& stopTargetGeneIT < liiIT.getRightEnd()
								) {
							boolCountICEIMEWith1GeneTarget = true;
							// target gene to the left of ICE IME
							addNextTargetGene_left(
									leiIT.getElementId()
									, startTargetGeneIT
									, connComparaisonDB
									//, alTransStartStopGeneInfo.get(0).getOrigamiGeneId()
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);
							addOppositeTargetGene_left(
									leiIT.getElementId()
									, liiIT.getRightEnd()
									, connComparaisonDB
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);

						} else if (
								startTargetGeneIT > liiIT.getLeftEnd()
								&& stopTargetGeneIT > liiIT.getRightEnd()
								) {
							boolCountICEIMEWith1GeneTarget = true;
							// target gene to the right of ICE IME
							// Get neighbor genes next to target gene
							addNextTargetGene_right(
									leiIT.getElementId()
									//, startTargetGeneIT
									, stopTargetGeneIT
									, connComparaisonDB
									//, alTransStartStopGeneInfo.get(0).getOrigamiGeneId()
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);
							
							addOppositeTargetGene_right(
									leiIT.getElementId()
									, liiIT.getLeftEnd()
									, connComparaisonDB
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);
							
						} else {
//							throw new Exception("Error categoriesCDSICEIME : could not determine if target to the left or the right of ICE IME ; one target gene"
//									+ " ; startTargetGeneIT="+startTargetGeneIT
//									+ " ; stopTargetGeneIT="+stopTargetGeneIT
//									+ " ; liiIT.getLeftEnd()="+liiIT.getLeftEnd()
//									+ " ; liiIT.getRightEnd()="+liiIT.getRightEnd()
//									);
							warningFails.add(
									"WARNING : Could not determine if target gene for "+liiIT.getElementName()+" is completely to the right or the left of the ICE/IME, failed to determine CDS to the right or the left"
											+ " ; start target gene="+startTargetGeneIT
											+ " ; stop target gene ="+stopTargetGeneIT
											+ " ; Left end ICE/IME="+liiIT.getLeftEnd()
											+ " ; right end ICE/IME="+liiIT.getRightEnd()
							);
							countTargetGeneFailedToDetermineCDSRightLeftOfICE++;
							if (VERBOSE) {
						    	System.out.println(
						    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
						    				+ "WARNING : Could not determine if target gene for "+liiIT.getElementName()+" is completely to the right or the left of the ICE/IME, failed to determine CDS to the right or the left"
											+ " ; start target gene="+startTargetGeneIT
											+ " ; stop target gene ="+stopTargetGeneIT
											+ " ; Left end ICE/IME="+liiIT.getLeftEnd()
											+ " ; right end ICE/IME="+liiIT.getRightEnd()
						    			);
						    }
							continue LOOP_ALL_ICEIME;
						}
					} else {
						// Multiple target gene
						int startTargetGeneOnTheLeft = -1;
						int stopTargetGeneOnTheRight = -1;
						for (TransStartStopGeneInfo tssgiIT : alTransStartStopGeneInfo) {
							int startTargetGeneIT = tssgiIT.getStart();
							int stopTargetGeneIT = tssgiIT.getStop();
							// Compare to left and right of ICE IME to determine where it stands
							if (startTargetGeneIT < liiIT.getLeftEnd()
									&& stopTargetGeneIT < liiIT.getRightEnd()
									) {
								if (startTargetGeneOnTheLeft == -1) {
									startTargetGeneOnTheLeft = startTargetGeneIT;
								} else if (startTargetGeneIT < startTargetGeneOnTheLeft) {
									startTargetGeneOnTheLeft = startTargetGeneIT;
								}
							} else if (
									startTargetGeneIT > liiIT.getLeftEnd()
									&& stopTargetGeneIT > liiIT.getRightEnd()
									) {
								if (stopTargetGeneOnTheRight == -1) {
									stopTargetGeneOnTheRight = stopTargetGeneIT;
								} else if (stopTargetGeneIT > stopTargetGeneOnTheRight) {
									stopTargetGeneOnTheRight = stopTargetGeneIT;
								}
							} else {
//								throw new Exception("Error categoriesCDSICEIME : could not determine if target to the left or the right of ICE IME ; Multiple target gene"
//										+ " ; startTargetGeneIT="+startTargetGeneIT
//										+ " ; stopTargetGeneIT="+stopTargetGeneIT
//										+ " ; liiIT.getLeftEnd()="+liiIT.getLeftEnd()
//										+ " ; liiIT.getRightEnd()="+liiIT.getRightEnd()
//										);
								warningFails.add(
										"WARNING : Could not determine if target gene for "+liiIT.getElementName()+" is completely to the right or the left of the ICE/IME, failed to determine CDS to the right or the left"
												+ " ; start target gene="+startTargetGeneIT
												+ " ; stop target gene ="+stopTargetGeneIT
												+ " ; Left end ICE/IME="+liiIT.getLeftEnd()
												+ " ; right end ICE/IME="+liiIT.getRightEnd()
								);
								countTargetGeneFailedToDetermineCDSRightLeftOfICE++;
								if (VERBOSE) {
							    	System.out.println(
							    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
							    				+ "WARNING : Could not determine if target gene for "+liiIT.getElementName()+" is completely to the right or the left of the ICE/IME, failed to determine CDS to the right or the left"
												+ " ; start target gene="+startTargetGeneIT
												+ " ; stop target gene ="+stopTargetGeneIT
												+ " ; Left end ICE/IME="+liiIT.getLeftEnd()
												+ " ; right end ICE/IME="+liiIT.getRightEnd()
							    			);
							    }
								continue LOOP_ALL_ICEIME;
							}
						}
						if ( startTargetGeneOnTheLeft == -1 && stopTargetGeneOnTheRight == -1 ) {
							throw new Exception("Error categoriesCDSICEIME : Multiple target gene startTargetGeneOnTheLeft == -1 && stopTargetGeneOnTheRight == -1"
									+ " ; startTargetGeneOnTheLeft="+startTargetGeneOnTheLeft
									+ " ; stopTargetGeneOnTheRight="+stopTargetGeneOnTheRight
									+ " ; liiIT.getLeftEnd()="+liiIT.getLeftEnd()
									+ " ; liiIT.getRightEnd()="+liiIT.getRightEnd()
									);
						} else if (startTargetGeneOnTheLeft != -1 && stopTargetGeneOnTheRight == -1 ) {
							boolCountICEIMEWithMultipleGeneTargetOnTheLeft = true;
							//there is a target gene on the left but not on the right
							addNextTargetGene_left(
									leiIT.getElementId()
									, startTargetGeneOnTheLeft
									, connComparaisonDB
									//, alTransStartStopGeneInfo.get(0).getOrigamiGeneId()
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);
							addOppositeTargetGene_left(
									leiIT.getElementId()
									, liiIT.getRightEnd()
									, connComparaisonDB
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);
						} else if (startTargetGeneOnTheLeft == -1 && stopTargetGeneOnTheRight != -1 ) {
							boolCountICEIMEWithMultipleGeneTargetOnTheRight = true;
							//there is a target gene on the right but not on the left
							addNextTargetGene_right(
									leiIT.getElementId()
									//, startTargetGeneIT
									//, stopTargetGeneIT
									, stopTargetGeneOnTheRight
									, connComparaisonDB
									//, alTransStartStopGeneInfo.get(0).getOrigamiGeneId()
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);
							
							addOppositeTargetGene_right(
									leiIT.getElementId()
									, liiIT.getLeftEnd()
									, connComparaisonDB
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);
						} else if (startTargetGeneOnTheLeft != -1 && stopTargetGeneOnTheRight != -1 ) {
							boolCountICEIMEWithMultipleGeneTargetOnBothSides = true;
							//there is a target gene on the left and  on the right						
							addNextTargetGene_left(
									leiIT.getElementId()
									, startTargetGeneOnTheLeft
									, connComparaisonDB
									//, alTransStartStopGeneInfo.get(0).getOrigamiGeneId()
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);
							addNextTargetGene_right(
									leiIT.getElementId()
									//, startTargetGeneIT
									//, stopTargetGeneIT
									, stopTargetGeneOnTheRight
									, connComparaisonDB
									//, alTransStartStopGeneInfo.get(0).getOrigamiGeneId()
									, hmCategoCDSRelationshipToICEIME2alGeneIds
									);
							
						}
					} // Multiple target gene
				} //addNextTargetGene, addOppositeTargetGene
				if (VERBOSE) {
			    	System.out.println(
			    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") :"
			    						+ " found "+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.nextTargetGeneLeft).size()+" CDSs not part of the ICE IME that are left of the target gene(s)"
			    						+ ", "+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.nextTargetGeneRight).size()+" CDSs not part of the ICE IME that are right of the target gene(s)"
			    						+ ", "+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.oppositeTargetGene).size()+" CDSs not part of the ICE IME that are opposite of the side of the target gene(s)"

										//+ " ; alGeneIdsTargetGene="+alGeneIdsTargetGene.toString()
			    			);
			    }
				
				// check no redundance of gene ids between all the different arrays of the ICE IME
				List<Category_of_CDS_in_relationship_to_ICE_IME> alCategory_of_CDS_in_relationship_to_ICE_IME = Arrays.asList(Category_of_CDS_in_relationship_to_ICE_IME.values());
				for (int i = 0 ; i < alCategory_of_CDS_in_relationship_to_ICE_IME.size() ; i++) {
					Category_of_CDS_in_relationship_to_ICE_IME firstCat = alCategory_of_CDS_in_relationship_to_ICE_IME.get(i);
					ArrayList<Integer> firstAlIT =  hmCategoCDSRelationshipToICEIME2alGeneIds.get(firstCat);
					for (int j = i+1 ; j < alCategory_of_CDS_in_relationship_to_ICE_IME.size() ; j++) {
						Category_of_CDS_in_relationship_to_ICE_IME secondCat = alCategory_of_CDS_in_relationship_to_ICE_IME.get(j);
						ArrayList<Integer> secondAlIT =  hmCategoCDSRelationshipToICEIME2alGeneIds.get(secondCat);
						if ( ! Collections.disjoint(firstAlIT, secondAlIT) ) {
							
							firstAlIT.retainAll(secondAlIT);
							String duplicatedGenes = "";
							for (int k = 0; k < firstAlIT.size() ; k++) {
								int geneIdIT = firstAlIT.get(k);
								GeneItem geneItemIT = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getGeneItemWithGeneId(connComparaisonDB, geneIdIT);
								duplicatedGenes += geneItemIT.getMostSignificantGeneNameAsStrippedText() +" " + geneItemIT.getListProteinId().toString() + "(" + geneItemIT.getStart() + "-" +geneItemIT.getStop() + ")";
							}
							warningFails.add(
									"WARNING : something went wrong for "+liiIT.getElementName()+", found redundance of genes between the following categories of CDS within the ICE IME : "
									+ firstCat
									+ " and "+secondCat
									+ " ; redundant gene(s) : " + duplicatedGenes
							);
							countTargetRedundanceGenesBetweenDifferentCategoriesCDS++;
							if (VERBOSE) {
						    	System.out.println(
						    			"WARNING : something went wrong for "+liiIT.getElementName()+", found redundance of genes between the following categories of CDS within the ICE IME : "
												+ firstCat
												+ " and "+secondCat
												+ " ; redundant gene(s) : " + duplicatedGenes
						    			);
						    }
							continue LOOP_ALL_ICEIME;
							
//							throw new Exception(
//									"WARNING : bad parsing, found redundance of genes between the following categories of CDS within the ICE IME : "
//									+ " ; colliding categories of CDS = "+firstCat
//									+ " and "+secondCat
//									+ " ; list gene ids for the category "+firstCat+" : "+firstAlIT.toString()
//									+ " ; list gene ids for the category "+secondCat+" : "+secondAlIT.toString()
//									);
						}
					}
				}
				
				ICEIMEGeneralInfoIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.targetGene).size()); //number_of_target_gene
				ICEIMEGeneralInfoIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.integrase).size());//number_of_integrase
				ICEIMEGeneralInfoIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.relaxase).size());//number_of_relaxase
				ICEIMEGeneralInfoIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.CouplingProtein).size());//number_of_coupling_protein
				ICEIMEGeneralInfoIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.virB4).size());//number_of_virB4
				
				if (boolCountICEIMEWith1GeneTarget) {
					countICEIMEWith1GeneTarget++;
				} else if (boolCountICEIMEWithMultipleGeneTargetOnBothSides) {
					countICEIMEWithMultipleGeneTargetOnBothSides++;
				} else if (boolCountICEIMEWithMultipleGeneTargetOnTheRight) {
					countICEIMEWithMultipleGeneTargetOnTheRight++;
				} else if (boolCountICEIMEWithMultipleGeneTargetOnTheLeft) {
					countICEIMEWithMultipleGeneTargetOnTheLeft++;
				}

				if ( ! parseFileWithoutGenomicComparisonAnalysis) {

					for (Map.Entry<Category_of_CDS_in_relationship_to_ICE_IME, ArrayList<Integer>> entryHmCategoCDSRelationshipToICEIME2 : hmCategoCDSRelationshipToICEIME2alGeneIds.entrySet()) {
						Category_of_CDS_in_relationship_to_ICE_IME categoryCDSRelationshipToICEIMEIT = entryHmCategoCDSRelationshipToICEIME2.getKey();
						ArrayList<Integer> alGeneIdsIT = entryHmCategoCDSRelationshipToICEIME2.getValue();

						if (VERBOSE) {
					    	System.out.println(
					    				"(comparaison " + countICEIMEcheckedAgainstComparaisonDB + " / " + allICEIME.size()+") : start comparing CDS category "+categoryCDSRelationshipToICEIMEIT.toString()
					    						+ " : "+alGeneIdsIT.size()+" gene(s)"
					    						+ ", " + hmScope2IncludeExcludeFeatured.entrySet().size() + " scope(s)"
					    						+ "."
					    			);
					    }
						
						for (int i = 0; i < alGeneIdsIT.size() ; i++) {
							int geneIdIT = alGeneIdsIT.get(i);
							GeneItem geneItemIT = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getGeneItemWithGeneId(connComparaisonDB, geneIdIT);
							String categoryCDSRelationshipToICEIMEIT_asString = "";
							if (categoryCDSRelationshipToICEIMEIT.compareTo(Category_of_CDS_in_relationship_to_ICE_IME.nextTargetGeneLeft) == 0
									|| categoryCDSRelationshipToICEIMEIT.compareTo(Category_of_CDS_in_relationship_to_ICE_IME.nextTargetGeneRight) == 0
									|| categoryCDSRelationshipToICEIMEIT.compareTo(Category_of_CDS_in_relationship_to_ICE_IME.oppositeTargetGene) == 0
									) {
								categoryCDSRelationshipToICEIMEIT_asString = categoryCDSRelationshipToICEIMEIT.displayName() + " + "+ (i + 1);
							} else {
								categoryCDSRelationshipToICEIMEIT_asString = categoryCDSRelationshipToICEIMEIT.displayName();
							}

							ArrayList<String> geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT = new ArrayList<>();
							// GeneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IME
							// Locus_tag	Category_of_CDS_in_relationship_to_ICE_IME	Protein_id	List_products	Length_residues	Gene_percent_GC	Diff_percent_GC_with_median_accession
							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(geneItemIT.getLocusTagAsStrippedString());
							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(categoryCDSRelationshipToICEIMEIT_asString);
							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(geneItemIT.getNameAsRawString());
							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(geneItemIT.getListProteinId().toString());
							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(geneItemIT.getListProduct().toString());
							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(""+geneItemIT.getLengthResidues());
//							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.targetGene).size()); //number_of_target_gene
//							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.integrase).size());//number_of_integrase
//							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.relaxase).size());//number_of_relaxase
//							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.CouplingProtein).size());//number_of_coupling_protein
//							geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(""+hmCategoCDSRelationshipToICEIME2alGeneIds.get(Category_of_CDS_in_relationship_to_ICE_IME.virB4).size());//number_of_virB4


							// hmScope2IncludeExcludeFeatured
							// idx 0 : -scope_taxon_id_include ["species", "all", "none", DIGIT_taxon_id] // default : all
							// idx 1 : -scope_taxon_id_exclude ["species", "all", "none", DIGIT_taxon_id] // default : none
							// idx 2 : -scope_featured_or_main_to_consider ["featured", "main_list"] // default : featured
							ArrayList<String> comparaisonInformationAllScopesIT = new ArrayList<>();
							for (Entry<String, ArrayList<String>> entryHmScope2 : hmScope2IncludeExcludeFeatured.entrySet()) {
								String scopeNameIT = entryHmScope2.getKey();
								//ArrayList<String> alIncludeExcludeFeatured = entryHmScope2.getValue();
								//String scopeIncludeIT = alIncludeExcludeFeatured.get(0);
								//String scopeExcludeIT = alIncludeExcludeFeatured.get(1);
								//String scopeFeaturedIT = alIncludeExcludeFeatured.get(2);

								
//								HashSet<Integer> hsTaxonIdsFeatured = new HashSet<>();
//								HashSet<Integer> hsTaxonIdsScopeInclude = fr.inra.jouy.server.queriesTable.QueriesTableTaxonomy.getHsTaxonIds_withScopeToConsider(
//										connTaxoDB
//										, scopeIncludeIT
//										, loiIT.getTaxonId()
//										);
//								hsTaxonIdsFeatured.addAll(hsTaxonIdsScopeInclude);
//								
//								HashSet<Integer> hsTaxonIdsScopeExclude = fr.inra.jouy.server.queriesTable.QueriesTableTaxonomy.getHsTaxonIds_withScopeToConsider(
//										connTaxoDB
//										, scopeExcludeIT
//										, loiIT.getTaxonId()
//										);
//								hsTaxonIdsFeatured.removeAll(hsTaxonIdsScopeExclude);
//								
//								
//								ArrayList<LightOrganismItem> alAllOrganismsInDB = fr.inra.jouy.server.queriesTable.QueriesTableOrganisms.getAlLightOrganismItems_orderByScoreAscIfProvided(
//										connComparaisonDB
//										, null //ArrayList<Integer> limitToAlOrgaIds
//										, null //ArrayList<Integer> excludeAlOrgaIds
//										, null //HashMap<Integer, Integer> hmOrgaId2fScoreToStore
//										, false //boolean setScoreTo0IfNotProvided
//										, false //boolean orderByScoreAsc
//										, true //boolean onlyPublic
//										, true //boolean onlyWithElement
//										);
//								
//								HashSet<Integer> hsOrgaIdsFeaturedGenomes = new HashSet<>();
//								//HashSet<Integer> hsOrgaIdsMainList = new HashSet<>();
//								for (LightOrganismItem loi_fromAlAllOrganismsInDB : alAllOrganismsInDB) {
//									if (loi_fromAlAllOrganismsInDB.getOrganismId() == loiIT.getOrganismId()) {
//										//skip, ref organism
//									} else if (hsTaxonIdsFeatured.contains(loi_fromAlAllOrganismsInDB.getTaxonId())) {
//										hsOrgaIdsFeaturedGenomes.add(loi_fromAlAllOrganismsInDB.getOrganismId());
//									} else {
//										//hsOrgaIdsMainList.add(loi_fromAlAllOrganismsInDB.getOrganismId());
//									}
//								}

								HashSet<Integer> hsOrgaIdsFeaturedGenomesIT = new HashSet<>(hmScope2HsOrgaIdsFeaturedGenomes.get(scopeNameIT));
								//hsOrgaIdsFeaturedGenomesIT.remove(loiIT.getOrganismId());
								
//								boolean getResultFoFeaturedList_mainListIfFalse = true;
//								if (scopeFeaturedIT.compareTo("featured")==0) {
//									getResultFoFeaturedList_mainListIfFalse = true;
//								} else if (scopeFeaturedIT.compareTo("main_list")==0) {
//									getResultFoFeaturedList_mainListIfFalse = false;
//								} else {
//									throw new Exception("scopeFeaturedIT neither featured or main_list for scopeNameIT = "+scopeNameIT+" ; alIncludeExcludeFeatured = "+alIncludeExcludeFeatured.toString());
//								}

								
								// idx 0 : numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution 
								// idx 1 : percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution
								// idx 2 : numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship 
								// idx 3 : percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
								// idx 4 : avgPercentIdentityWithCDSsConservedDuringEvolution
								// idx 5 : avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
								// idx 6 : medianEvalueAlignmentsWithCDSsConservedDuringEvolution
								ArrayList<String> alStatSummaryIT = new ArrayList<>();
								// if hsOrgaIdsFeaturedGenomesIT small size (< 15), consider not stat significant and do not calculate and fill up with empty string instead
								if (hsOrgaIdsFeaturedGenomesIT.size() < 15) {
									alStatSummaryIT.add(""); // numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution
									alStatSummaryIT.add(""); // percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution
									alStatSummaryIT.add(""); // numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship
									alStatSummaryIT.add(""); // percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
									alStatSummaryIT.add(""); // avgPercentIdentityWithCDSsConservedDuringEvolution
									alStatSummaryIT.add(""); // avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
									alStatSummaryIT.add(""); // medianEvalueAlignmentsWithCDSsConservedDuringEvolution
								} else {
									HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneNameIT = new HashMap<>();
									HashMap<Integer, String> qGeneId2qGeneNameIT = new HashMap<>();
									qGeneId2qGeneNameIT.put(geneIdIT, "NA"); // qGeneName not important here
									qOrganismId2qGeneId2qGeneNameIT.put(loiIT.getOrganismId(), qGeneId2qGeneNameIT);
									alStatSummaryIT = ComparativeGenomics.getStatSummaryRefCDS_asAlString( //ArrayList<String>
											connComparaisonDB
											, qOrganismId2qGeneId2qGeneNameIT //contains only 1 qOrganismId, 1 qGeneId and 1 qGeneName
											, hsOrgaIdsFeaturedGenomesIT
											//, hsOrgaIdsMainListIT
											);
								}
								
								comparaisonInformationAllScopesIT.addAll(
										alStatSummaryIT
//										calculateStatSummaryForCDS(
//												connComparaisonDB
//												, geneIdIT // geneID
//												, loiIT.getOrganismId() // organismId
//												, hsOrgaIdsFeaturedGenomesIT// scope (hsOrgaIdsFeaturedGenomes)
//												//, hsOrgaIdsMainList// scope (hsOrgaIdsMainList)
//												//, getResultFoFeaturedList_mainListIfFalse// getResultFoFeaturedList_mainListIfFalse // get Result From featured or main list
//										)
									);
							}
							
							ArrayList<String> alLineInfoToPrint = new ArrayList<>();
							alLineInfoToPrint.addAll(ICEIMEGeneralInfoIT);
							alLineInfoToPrint.addAll(geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT);
							alLineInfoToPrint.addAll(comparaisonInformationAllScopesIT);
							linesToPrint.add(String.join("\t", alLineInfoToPrint));
							
						}
					}
				}
				
				

				hsElementIds_onlyICEIMEWithNoProblems.add(leiIT.getElementId());
				hsOrgaIds_onlyICEIMEWithNoProblems.add(loiIT.getOrganismId());
				
//				if (
//						//liiIT.getElementName().compareTo("ICE_ScoC232_rumA")==0
//						countICEIMEcheckedAgainstComparaisonDB > 50
//						) {
//					break LOOP_ALL_ICEIME;
//					//throw new Exception("TEST EXIT after loop = "+countICEIMEcheckedAgainstComparaisonDB);
//				}
				
				countShouldBeICEIMESuccessfullyAnalysed++;
				if (liiIT.getIntegraseType().compareTo(IntegraseType.Tyr)==0) {
					countShouldBeICEIMESuccessfullyAnalysed_intTyr++;
				} else if (liiIT.getIntegraseType().compareTo(IntegraseType.Ser)==0) {
					countShouldBeICEIMESuccessfullyAnalysed_intSer++;
				} else if (liiIT.getIntegraseType().compareTo(IntegraseType.DDE)==0) {
					countShouldBeICEIMESuccessfullyAnalysed_intDDE++;
				} else if (liiIT.getIntegraseType().compareTo(IntegraseType.UNKNOWN)==0) {
					countShouldBeICEIMESuccessfullyAnalysed_intUNKNOWN++;
				} else if (liiIT.getIntegraseType().compareTo(IntegraseType.UNSURE)==0) {
					countShouldBeICEIMESuccessfullyAnalysed_intUNSURE++;
				}
				
			}//LOOP_ALL_ICEIME : for (ICEIMEItem_fromFile liiIT : allICEIME) {
			
			if (VERBOSE) {
				int countICEIMESuccessfullyAnalysed = allICEIME.size()
						- ICEIMEAccessionNotFoundInComparativeGenomicsDB.size()
						- countSignatureProteinIdentifierNotFoundInCompaDb
						- countElementTypeICENotAllFourSignatureProteins
						- countElementDefinedAsHostButNoGuestICEIMEFound
						- countElementNotDefinedAsHostButGuestICEIMEFound
						- countTargetGeneIdentifierProblemsInCompaDb
						//- countTargetGeneIdentifiedFailedToBeFoundInDb
						- countTargetGeneTRNAButFoundCompaDB
						- countTargetGeneIsTRNA
						- countNoTargetGeneIdentifierSpecified
						- countNoTargetGeneIdentifiedBecauseMarkedAsQuestionMarkORnoneORnot_annotated
						- countNoTargetGeneIdentifiedBecauseIntergenic
						- countNoTargetGeneIdentifiedBecauseNonSpecific
						- countNoTargetGeneIdentifiedButWrongSpecificity
						- countTargetGeneFailedToDetermineCDSRightLeftOfICE
						- countTargetRedundanceGenesBetweenDifferentCategoriesCDS
						- countNoTargetGeneIdentifiedBecauseAnnotatedAsPseudogene
						;
				if (countShouldBeICEIMESuccessfullyAnalysed != countICEIMESuccessfullyAnalysed) {
					throw new Exception("countShouldBeICEIMESuccessfullyAnalysed ("+countShouldBeICEIMESuccessfullyAnalysed+") != countICEIMESuccessfullyAnalysed ("+countICEIMESuccessfullyAnalysed+")");

				}
		    	System.out.println(
		    				"\nDone with querying the comparaison genomic database : "
		    				+ "\n\t"+allICEIME.size()+" different ICEs orIMEs with name and that were correctly parsed from the excel file have been analysed"
		    				+ "\n\t" + ICEIMEAccessionNotFoundInComparativeGenomicsDB.size() + " ICEs OR IMEs were skipped because no organism could be associated with their accession number (Genome_id) in the genomic comparaison database."
		    				+ "\n\t" + countSignatureProteinIdentifierNotFoundInCompaDb + " ICEs OR IMEs were skipped because at least one of their identifier for a signature protein was not found in the genomic compariason database (see warning below for details)."
		    				+ "\n\t" + countElementTypeICENotAllFourSignatureProteins + " ICEs OR IMEs were skipped because their type is defined as ICE and they do not contains all 4 signature proteins (see warning below for details)."
		    				+ "\n\t" + countElementDefinedAsHostButNoGuestICEIMEFound + " ICEs OR IMEs were skipped because they are defined as host but no guest ICE IME was found (see warning below for details)."
		    				+ "\n\t" + countElementNotDefinedAsHostButGuestICEIMEFound + " ICEs OR IMEs were skipped because they are not defined as host but a guest ICE IME was found (see warning below for details)."
		    				+ "\n\t" + countTargetGeneIdentifierProblemsInCompaDb + " ICEs OR IMEs were skipped because of problems finding the target gene in the genomic comparaison database (see warning below for details)."
		    				//+ "\n\t" + countTargetGeneIdentifiedFailedToBeFoundInDb + " ICEs OR IMEs were skipped because their target genes identifier failed to be found in the comparison database (see warning below for details)."
		    				+ "\n\t" + countTargetGeneTRNAButFoundCompaDB + " ICEs OR IMEs were skipped because the target gene is identified as tRNA but it was found in the comparaison database (see warning below for details)."
		    				+ "\n\t" + countTargetGeneFailedToDetermineCDSRightLeftOfICE + " ICEs OR IMEs were skipped because of a failure to identify the CDS right and left outside the ICE/IME and the target gene (see warning below for details)."
		    				+ "\n\t" + countTargetRedundanceGenesBetweenDifferentCategoriesCDS + " ICEs OR IMEs were skipped because something went wrong during parsing, found redundance of genes between the different categories of CDS within the ICE IME (see warning below for details)."
		    				+ "\n\t" + countTargetGeneIsTRNA + " ICEs OR IMEs were skipped because the target gene is identified as tRNA."
		    				+ "\n\t" + countNoTargetGeneIdentifierSpecified + " ICEs OR IMEs were skipped because no target gene identifier was specified."
		    				+ "\n\t" + countNoTargetGeneIdentifiedBecauseAnnotatedAsPseudogene + " ICEs OR IMEs were skipped because their target gene was annotated as pseudogene."
		    				+ "\n\t" + countNoTargetGeneIdentifiedBecauseMarkedAsQuestionMarkORnoneORnot_annotated + " ICEs OR IMEs were skipped because all or part of the target gene identifiers was marked as question mark or none or not annotated, etc."
		    				+ "\n\t" + countNoTargetGeneIdentifiedBecauseIntergenic + " ICEs OR IMEs were skipped because all or part of the target gene identifiers was marked as intergenic."
		    				+ "\n\t" + countNoTargetGeneIdentifiedBecauseNonSpecific + " ICEs OR IMEs were skipped because all or part of the target gene identifiers was marked as non specific (NS)."
		    				+ "\n\t" + countNoTargetGeneIdentifiedButWrongSpecificity + " ICEs OR IMEs were skipped because the identifiers for the target gene refer to a wrong specificity (integrase specific of XXX, but ...)."
		    				+ "\n\t" + countICEIMESuccessfullyAnalysed + " ICEs OR IMEs were successfully analysed from "+hsElementIds_onlyICEIMEWithNoProblems.size()+" different molecules (chromosomes) and from " + hsOrgaIds_onlyICEIMEWithNoProblems.size() + " different organisms."
		    				+ "\n\t" + countICEIMEWith1GeneTarget + " of ICEs OR IMEs successfully analysed have 1 target gene, "
		    					+countICEIMEWithMultipleGeneTargetOnBothSides+" have multiple target genes located on both sides, " 
		    					+countICEIMEWithMultipleGeneTargetOnTheLeft+" have multiple target genes located on the left, " 
		    					+countICEIMEWithMultipleGeneTargetOnTheRight+" have multiple target genes located on the right." 
		    				+ "\n\t" + countShouldBeICEIMESuccessfullyAnalysed_intTyr + " of ICEs OR IMEs successfully analysed are integrase Tyr, "
	    						+ countShouldBeICEIMESuccessfullyAnalysed_intSer+" are integrase Ser, " 
	    						+ countShouldBeICEIMESuccessfullyAnalysed_intDDE+" are integrase DDE, " 
	    						+ countShouldBeICEIMESuccessfullyAnalysed_intUNKNOWN+" are integrase of unknown type, " 
	    						+ countShouldBeICEIMESuccessfullyAnalysed_intUNSURE+" are integrase of unsure type." 
		    				);
		    	if ( ! warningFails.isEmpty()) {
		    		System.out.println("\nWarning(s) for "+warningFails.size()+" ICE IME element(s) were raised while analysing the ICE IME with the comparaison genomic database : ");
		    		for (String warningIT : warningFails) {
		    			System.out.println(warningIT);
		    		}
		    	}
		    	
		    }
			 
			Files.write(outputFile, linesToPrint, StandardCharsets.UTF_8,
					Files.exists(outputFile) ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE); //, StandardOpenOption.WRITE

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			DatabaseConf.freeConnection(connComparaisonDB, methodNameToReport);
			DatabaseConf.freeConnection(connTaxoDB, methodNameToReport);
			//DatabaseConfICEDB.freeConnection(connICEDB, methodNameToReport);
		}

		milliPrint2 = System.currentTimeMillis() - milli;
		Date dateEnd = new Date();
		if (VERBOSE) {
			System.out.println("\nDone "+methodNameToReport+" at "+dateFormat.format(dateEnd)+", it took "+milliPrint2+" milliseconds.");	
		}

	}


	private static ArrayList<String> getHeaderAsAlString() throws Exception {

//		ArrayList<String> alHeader = new ArrayList<>();
//		alHeader.add("Element_name");
//		alHeader.add("Strain");
//		alHeader.add("Genome_id_accession");
//		alHeader.add("Organism_id_database");
//		alHeader.add("Element_type");
//		alHeader.add("Integrase_type");
//		alHeader.add("integrase_location_compared_to_target");
//		alHeader.add("integrase_orientation_compared_to_target");
//		alHeader.add("Family");
//		alHeader.add("Left_end");
//		alHeader.add("Right_end");
//		alHeader.add("Accretion");
//		alHeader.add("Insertion");
//		alHeader.add("Location_target_gene_compared_to_ICE_IME");
		
		
		ArrayList<String> alToReturn = new ArrayList<>();// ICEIMEGeneralInfo
		// BEFORE WAS : Element_name	Strain	Genome_id_accession	Organism_id_origami_cg	Organism_id_bacteria_2017	Element_type	Integrase_type	integrase_location_compared_to_target	integrase_orientation_compared_to_target	Family	Left_end	Right_end	Accretion	Insertion	Location_target_gene_compared_to_ICE_IME
		alToReturn.add("Element_name"); //		ICEIMEGeneralInfoIT.add(liiIT.getElementName()); //Mobile_element_name
		alToReturn.add("Genome_id_accession"); //ICEIMEGeneralInfoIT.add(liiIT.getGenomeId()); //Genome_accession
		alToReturn.add("Genome_id_in_comparaisondb"); //ICEIMEGeneralInfoIT.add(""+leiIT.getElementId()); //Genome_id_in_comparaisondb
		alToReturn.add("Organism_name_and_strain"); //ICEIMEGeneralInfoIT.add(loiIT.getFullName()); //Organism_name_and_strain
		alToReturn.add("Organism_id_in_comparaisondb"); //ICEIMEGeneralInfoIT.add(""+loiIT.getOrganismId()); //Organism_id_in_comparaisondb				
		alToReturn.add("Element_type"); //ICEIMEGeneralInfoIT.add(liiIT.getElementType().toString()); //Mobile_element_type
		alToReturn.add("Element_family"); //ICEIMEGeneralInfoIT.add(liiIT.getFamily()); //Mobile_element_family
		alToReturn.add("Element_left_end"); //ICEIMEGeneralInfoIT.add(""+liiIT.getLeftEnd()); //Mobile_element_left_end
		alToReturn.add("Element_right_end"); //ICEIMEGeneralInfoIT.add(""+liiIT.getRightEnd()); //Mobile_element_right_end
		alToReturn.add("Is_mobile_element_accretion"); //ICEIMEGeneralInfoIT.add(liiIT.getAccretion().toString()); //Is_mobile_element_accretion
		alToReturn.add("Is_mobile_element_insertion"); //ICEIMEGeneralInfoIT.add(liiIT.getInsertion().toString()); //Is_mobile_element_insertion
		alToReturn.add("Integrase_type"); //ICEIMEGeneralInfoIT.add(liiIT.getIntegraseType().toString()); //Integrase_type
		alToReturn.add("integrase_location_compared_to_target"); //ICEIMEGeneralInfoIT.add(liiIT.getIntegraseLocation()); //integrase_location_compared_to_target
		alToReturn.add("integrase_orientation_compared_to_target"); //ICEIMEGeneralInfoIT.add(liiIT.getIntegraseOrientation()); //integrase_orientation_compared_to_target
		alToReturn.add("insertion_gene_function"); //ICEIMEGeneralInfoIT.add(liiIT.getInsertionGeneFunction()); //insertion_gene_function
		alToReturn.add("number_of_target_gene");
		alToReturn.add("number_of_integrase");
		alToReturn.add("number_of_relaxase");
		alToReturn.add("number_of_coupling_protein");
		alToReturn.add("number_of_virB4");
		
//		alHeader.add("Locus_tag");
//		alHeader.add("Category_of_CDS_in_relationship_to_ICE_IME");
//		alHeader.add("Protein_id");
//		alHeader.add("List_products");
//		alHeader.add("Length_residues");
//		alHeader.add("Gene_percent_GC");
//		alHeader.add("Diff_percent_GC_with_median_accession");
//		alHeader.add("number_of_target_gene");
//		alHeader.add("number_of_integrase");
//		alHeader.add("number_of_relaxase");
//		alHeader.add("number_of_coupling_protein");
//		alHeader.add("number_of_virB4");


//		ArrayList<String> geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT = new ArrayList<>();
//		// GeneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IME
//		// Locus_tag	Category_of_CDS_in_relationship_to_ICE_IME	Protein_id	List_products	Length_residues	Gene_percent_GC	Diff_percent_GC_with_median_accession
		alToReturn.add("Locus_tag"); 
		alToReturn.add("Category_of_CDS_in_relationship_to_ICE_IME"); 
		alToReturn.add("Gene name"); 
		alToReturn.add("Protein_id"); 
		alToReturn.add("List_products"); 
		alToReturn.add("Length_residues");

//		geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(geneItemIT.getLocusTagAsStrippedString());
//		geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(categoryCDSRelationshipToICEIMEIT_asString);
//		geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(geneItemIT.getNameAsRawString());
//		geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(geneItemIT.getListProteinId().toString());
//		geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(geneItemIT.getListProduct().toString());
//		geneGeneralInfoAndCategory_of_CDS_in_relationship_to_ICE_IMEIT.add(""+geneItemIT.getLengthResidues());

		
		for (Entry<String, ArrayList<String>> entry : hmScope2IncludeExcludeFeatured.entrySet()) {
			String scopeIT = entry.getKey();
			//ArrayList<String> alIncludeExcludeFeaturedIT = entry.getValue();
			alToReturn.add("Number_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT);
			alToReturn.add("Percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_scope_"+scopeIT);
			alToReturn.add("Number_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT);
			alToReturn.add("Percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_scope_"+scopeIT);
			alToReturn.add("Average_percent_identity_alignment_scope_"+scopeIT);
			alToReturn.add("Average_percent_query_and_subject_lenght_coverage_alignment_scope_"+scopeIT);
			alToReturn.add("Median_Evalue_alignment_scope_"+scopeIT);
		}
//		alToReturn.add(numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution);
//		alToReturn.add(percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution);
//		alToReturn.add(numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship);
//		alToReturn.add(percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship);
//		alToReturn.add(avgPercentIdentityWithCDSsConservedDuringEvolution);
//		alToReturn.add(avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution);
//		alToReturn.add(medianEvalueAlignmentsWithCDSsConservedDuringEvolution);

		return alToReturn;
	}

	private static void addNextTargetGene_left(
			int elementIdIT
			, int startTargetGeneOnTheLeft
			//, int startTargetGeneIT
			//, int stopTargetGeneIT
			, Connection connComparaisonDB
			//, int targetGeneId
			, HashMap<Category_of_CDS_in_relationship_to_ICE_IME, ArrayList<Integer>> hmCategoCDSRelationshipToICEIME2alGeneIds
			) throws Exception {
		// Get neighbor genes next to target gene
		ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSNextTarget = new ArrayList<>();
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSNextTarget.add(elementIdIT);
		int startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget = startTargetGeneOnTheLeft - 15000;
		int stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget = startTargetGeneOnTheLeft;
		if (startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget < 0) {
			startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget = 0;
		}
		if (stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget < 0) {
			stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget = 0;
		}
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSNextTarget.add(startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget);
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSNextTarget.add(stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget);
		//the last CDS in the array will be the target gene
		ArrayList<Integer> alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
				connComparaisonDB
				, listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSNextTarget
				, null // listGeneIdsToExclude // set to null or empty if none
				, true // grabCDSWithAtLeastOnePbInThisStartStopLocus
				//, true // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
				, grabCDSWithAtLeastOnePbInThisStartStopLocusIT // sometimes the DR can be inside the intgrase
				, true // orderCDSByStartAsc
				, false // shouldBePresentInTable
				);
//		if ( alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-1) != targetGeneId ) {
//			throw new Exception("Error categoriesCDSICEIME : alGeneIdsTargetGeneLeftICEIME.get(alGeneIdsTargetGeneLeftICEIME.size()-1) != alTransStartStopGeneInfo.get(0).getOrigamiGeneId()"
//					+ " ; alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-1)="+alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-1)
//					+ " ; alTransStartStopGeneInfo.get(0).getOrigamiGeneId()="+targetGeneId
//					);
//		}
		

//		System.err.println(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.toString());
//		System.err.println(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-1));
//		System.err.println(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-2));
//		System.err.println(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-3));
//		System.err.println(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-4));
		
		
		if ( alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size() < 4) {
			throw new Exception("Error categoriesCDSICEIME : alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size() < 4"
					+ " ; alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()="+alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()
					+ " ; leiIT.getElementId()="+elementIdIT
					+ " ; startToConsiderForCDSBorderICEIME="+startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget
					+ " ; stopToConsiderForCDSBorderICEIME="+stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSNextTarget
					);
		} else {
			ArrayList<Integer> alGeneIdsNextTargetGene = new ArrayList<>();
			alGeneIdsNextTargetGene.add(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-1));
			alGeneIdsNextTargetGene.add(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-2));
			alGeneIdsNextTargetGene.add(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-3));
			alGeneIdsNextTargetGene.add(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.get(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSNextTarget.size()-4));
			hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.nextTargetGeneLeft, alGeneIdsNextTargetGene);
		}
	}
	
	private static void addNextTargetGene_right(
			int elementId
			, int stopTargetGeneOnTheRight
			//, int startTargetGeneIT
			//, int stopTargetGeneIT
			, Connection connComparaisonDB
			//, int targetGeneId
			, HashMap<Category_of_CDS_in_relationship_to_ICE_IME, ArrayList<Integer>> hmCategoCDSRelationshipToICEIME2alGeneIds
			) throws Exception {
		ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSNextTarget = new ArrayList<>();
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSNextTarget.add(elementId);
		int startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget = stopTargetGeneOnTheRight;
		int stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget = stopTargetGeneOnTheRight + 15000;
		if (startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget < 0) {
			startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget = 0;
		}
		if (stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget < 0) {
			stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget = 0;
		}
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSNextTarget.add(startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget);
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSNextTarget.add(stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget);

		//the first CDS in the array will be the target gene
		ArrayList<Integer> alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
				connComparaisonDB
				, listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSNextTarget
				, null // listGeneIdsToExclude // set to null or empty if none
				, true // grabCDSWithAtLeastOnePbInThisStartStopLocus
				//, true // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
				, grabCDSWithAtLeastOnePbInThisStartStopLocusIT // sometimes the DR can be inside the intgrase
				, true // orderCDSByStartAsc
				, false // shouldBePresentInTable
				);
//		if ( alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.get(0) != targetGeneId ) {
//			throw new Exception("Error categoriesCDSICEIME : alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.get(0) != alTransStartStopGeneInfo.get(0).getOrigamiGeneId()"
//					+ " ; alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.get(0)="+alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.get(0)
//					+ " ; alTransStartStopGeneInfo.get(0).getOrigamiGeneId()="+targetGeneId
//					);
//		}
		if ( alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.size() < 4) {
			throw new Exception("Error categoriesCDSICEIME : alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.size() < 4"
					+ " ; alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.size()="+alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.size()
					+ " ; leiIT.getElementId()="+elementId
					+ " ; startToConsiderForCDSBorderICEIME="+startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget
					+ " ; stopToConsiderForCDSBorderICEIME="+stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSNextTarget
					);
		} else {
			ArrayList<Integer> alGeneIdsNextTargetGene = new ArrayList<>();
			alGeneIdsNextTargetGene.add(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.get(0));
			alGeneIdsNextTargetGene.add(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.get(1));
			alGeneIdsNextTargetGene.add(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.get(2));
			alGeneIdsNextTargetGene.add(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSNextTarget.get(3));
			hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.nextTargetGeneRight, alGeneIdsNextTargetGene);
		}
	}

	private static void addOppositeTargetGene_left(
			int elementIdIT
			, int liiITRightEnd
			, Connection connComparaisonDB
			, HashMap<Category_of_CDS_in_relationship_to_ICE_IME, ArrayList<Integer>> hmCategoCDSRelationshipToICEIME2alGeneIds
			) throws Exception {
		// Get neighbor genes opposite to target gene
		ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSOppositeTarget = new ArrayList<>();
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSOppositeTarget.add(elementIdIT);
		int startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget = liiITRightEnd;
		int stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget = liiITRightEnd + 15000;
		if (startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget < 0) {
			startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget = 0;
		}
		if (stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget < 0) {
			stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget = 0;
		}
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSOppositeTarget.add(startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget);
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSOppositeTarget.add(stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget);

		ArrayList<Integer> alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSOppositeTarget = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
				connComparaisonDB
				, listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneLeftICEIME_CDSOppositeTarget
				, null // listGeneIdsToExclude // set to null or empty if none
				, true // grabCDSWithAtLeastOnePbInThisStartStopLocus
				//, true // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
				, grabCDSWithAtLeastOnePbInThisStartStopLocusIT // sometimes the DR can be inside the intgrase
				, true // orderCDSByStartAsc
				, false // shouldBePresentInTable
				);
		if ( alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSOppositeTarget.size() < 4) {
			throw new Exception("Error categoriesCDSICEIME : alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSOppositeTarget.size() < 4"
					+ " ; alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSOppositeTarget.size()="+alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSOppositeTarget.size()
					+ " ; leiIT.getElementId()="+elementIdIT
					+ " ; startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget="+startToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget
					+ " ; stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget="+stopToConsiderForCDSBorderICEIME_targetGeneLeftICEIME_CDSOppositeTarget
					);
		} else {
			ArrayList<Integer> alGeneIdsOppositeTargetGene = new ArrayList<>();
			alGeneIdsOppositeTargetGene.add(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSOppositeTarget.get(0));
			alGeneIdsOppositeTargetGene.add(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSOppositeTarget.get(1));
			alGeneIdsOppositeTargetGene.add(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSOppositeTarget.get(2));
			alGeneIdsOppositeTargetGene.add(alGeneIdsneighborCDS_targetGeneLeftICEIME_CDSOppositeTarget.get(3));
			hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.oppositeTargetGene, alGeneIdsOppositeTargetGene);
		}
		
	}

	private static void addOppositeTargetGene_right(
			int elementIdIT
			, int liiITLeftEnd
			, Connection connComparaisonDB
			, HashMap<Category_of_CDS_in_relationship_to_ICE_IME, ArrayList<Integer>> hmCategoCDSRelationshipToICEIME2alGeneIds
			) throws Exception {
		// Get neighbor genes opposite to target gene
		ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSOppositeTarget = new ArrayList<>();
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSOppositeTarget.add(elementIdIT);
		int startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget = liiITLeftEnd - 15000;
		int stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget = liiITLeftEnd;
		if (startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget < 0) {
			startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget = 0;
		}
		if (stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget < 0) {
			stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget = 0;
		}
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSOppositeTarget.add(startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget);
		listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSOppositeTarget.add(stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget);
		ArrayList<Integer> alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
				connComparaisonDB
				, listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedIT_targetGeneRightICEIME_CDSOppositeTarget
				, null // listGeneIdsToExclude // set to null or empty if none
				, true // grabCDSWithAtLeastOnePbInThisStartStopLocus
				//, true // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
				, grabCDSWithAtLeastOnePbInThisStartStopLocusIT // sometimes the DR can be inside the intgrase
				, true // orderCDSByStartAsc
				, false // shouldBePresentInTable
				);
		if ( alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.size() < 4) {
			throw new Exception("Error categoriesCDSICEIME : alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.size() < 4"
					+ " ; alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.size()="+alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.size()
					+ " ; leiIT.getElementId()="+elementIdIT
					+ " ; startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget="+startToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget
					+ " ; stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget="+stopToConsiderForCDSBorderICEIME_targetGeneRightICEIME_CDSOppositeTarget
					);
		} else {
			ArrayList<Integer> alGeneIdsOppositeTargetGene = new ArrayList<>();
			alGeneIdsOppositeTargetGene.add(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.get(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.size()-1));
			alGeneIdsOppositeTargetGene.add(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.get(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.size()-2));
			alGeneIdsOppositeTargetGene.add(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.get(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.size()-3));
			alGeneIdsOppositeTargetGene.add(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.get(alGeneIdsneighborCDS_targetGeneRightICEIME_CDSOppositeTarget.size()-4));
			hmCategoCDSRelationshipToICEIME2alGeneIds.put(Category_of_CDS_in_relationship_to_ICE_IME.oppositeTargetGene, alGeneIdsOppositeTargetGene);
		}
	}


	private static HashMap<String, ArrayList<ArrayList<Integer>>> buildICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME(
			//ArrayList<ICEIMEItem_fromFile> allICEIME
			ArrayList<ICEIMEItem_fromFile> alPotentialHostICEIME
			, ArrayList<ICEIMEItem_fromFile> alPotentialGuestICEIME
			) throws Exception {
		
		HashMap<String, ArrayList<ArrayList<Integer>>> hmToReturn = new HashMap<>();
		
		for (int i=0; i < alPotentialHostICEIME.size();i++) {
			ICEIMEItem_fromFile iiPotentialHost = alPotentialHostICEIME.get(i);
			if (iiPotentialHost.getLeftEnd() >= iiPotentialHost.getRightEnd()) {
				throw new Exception("Error buildICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME : iiPotentialHost.getLeftEnd() >= iiPotentialHost.getRightEnd()"
						+ " for element "+iiPotentialHost.getElementName()+" iiPotentialHost.getLeftEnd() ("+iiPotentialHost.getLeftEnd()+") >= iiPotentialHost.getRightEnd() ("+iiPotentialHost.getRightEnd()+")"
						);
			}
			for (int j=0; j < alPotentialGuestICEIME.size();j++) {
				ICEIMEItem_fromFile iiPotentialGuest = alPotentialGuestICEIME.get(j);
				if (iiPotentialGuest.getLeftEnd() >= iiPotentialGuest.getRightEnd()) {
					throw new Exception("Error buildICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME : iiPotentialGuest.getLeftEnd() >= iiPotentialGuest.getRightEnd()"
							+ " for element "+iiPotentialGuest.getElementName()+" iiPotentialGuest.getLeftEnd() ("+iiPotentialGuest.getLeftEnd()+") >= iiPotentialGuest.getRightEnd() ("+iiPotentialGuest.getRightEnd()+")"
							);
				}
				
				// skip if same element
				if (iiPotentialHost.getLeftEnd() == iiPotentialGuest.getLeftEnd() && iiPotentialHost.getRightEnd() == iiPotentialGuest.getRightEnd()) {
					continue;
				}
				
				//check if same element
				if (iiPotentialHost.getGenomeId().compareTo(iiPotentialGuest.getGenomeId())==0) {
					if (iiPotentialHost.getLeftEnd() > iiPotentialGuest.getLeftEnd() && iiPotentialHost.getRightEnd() < iiPotentialGuest.getRightEnd()) {
						//case outer contained within inner
						if (hmToReturn.containsKey(iiPotentialGuest.getElementName())) {
							ArrayList<Integer> alStartStop = new ArrayList<>();
							alStartStop.add(iiPotentialHost.getLeftEnd());
							alStartStop.add(iiPotentialHost.getRightEnd());
							ArrayList<ArrayList<Integer>> alStartStopAllGuestIT = hmToReturn.get(iiPotentialGuest.getElementName());
							alStartStopAllGuestIT.add(alStartStop);
//							ArrayList<Integer> alStartStop = hmToReturn.get(iiPotentialGuest.getElementName());
//							if (iiPotentialHost.getLeftEnd() < alStartStop.get(0)) {
//								alStartStop.set(0, iiPotentialHost.getLeftEnd());
//							}
//							if (iiPotentialHost.getRightEnd() > alStartStop.get(1)) {
//								alStartStop.set(1, iiPotentialHost.getRightEnd());
//							}
						} else {
							ArrayList<Integer> alStartStop = new ArrayList<>();
							alStartStop.add(iiPotentialHost.getLeftEnd());
							alStartStop.add(iiPotentialHost.getRightEnd());
							ArrayList<ArrayList<Integer>> alStartStopAllGuestIT = new ArrayList<>();
							alStartStopAllGuestIT.add(alStartStop);
							hmToReturn.put(iiPotentialGuest.getElementName(), alStartStopAllGuestIT);
						}
					} else if (iiPotentialHost.getLeftEnd() < iiPotentialGuest.getLeftEnd() && iiPotentialHost.getRightEnd() > iiPotentialGuest.getRightEnd()) {
						//case inner contained within outer
						if (hmToReturn.containsKey(iiPotentialHost.getElementName())) {
							ArrayList<Integer> alStartStop = new ArrayList<>();
							alStartStop.add(iiPotentialGuest.getLeftEnd());
							alStartStop.add(iiPotentialGuest.getRightEnd());
							ArrayList<ArrayList<Integer>> alStartStopAllGuestIT = hmToReturn.get(iiPotentialHost.getElementName());
							alStartStopAllGuestIT.add(alStartStop);
//							ArrayList<Integer> alStartStop = hmToReturn.get(iiPotentialHost.getElementName());
//							if (iiPotentialGuest.getLeftEnd() < alStartStop.get(0)) {
//								alStartStop.set(0, iiPotentialGuest.getLeftEnd());
//							}
//							if (iiPotentialGuest.getRightEnd() > alStartStop.get(1)) {
//								alStartStop.set(1, iiPotentialGuest.getRightEnd());
//							}
						} else {
							ArrayList<Integer> alStartStop = new ArrayList<>();
							alStartStop.add(iiPotentialGuest.getLeftEnd());
							alStartStop.add(iiPotentialGuest.getRightEnd());
							ArrayList<ArrayList<Integer>> alStartStopAllGuestIT = new ArrayList<>();
							alStartStopAllGuestIT.add(alStartStop);
							hmToReturn.put(iiPotentialHost.getElementName(), alStartStopAllGuestIT);
						}
					} else if (iiPotentialHost.getRightEnd() < iiPotentialGuest.getLeftEnd()) {
						//case outer left of inner
						// do nothing
					} else if (iiPotentialHost.getLeftEnd() > iiPotentialGuest.getRightEnd()) {	
						//case outer right of inner
						// do nothing
					} else if (iiPotentialHost.getLeftEnd() <= iiPotentialGuest.getLeftEnd() && iiPotentialHost.getRightEnd() >= iiPotentialGuest.getLeftEnd()) {
						//case outer overlap partially left of inner
						if (iiPotentialHost.getRightEnd()-iiPotentialGuest.getLeftEnd() <= 60) {
							//consider small overlap due to accretion
						} else {
							throw new Exception("Error buildICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME :"
									+ " comparing element "+iiPotentialHost.getElementName()+" ("+iiPotentialHost.getLeftEnd()+"-"+iiPotentialHost.getRightEnd()+")"
									+ " with element "+iiPotentialGuest.getElementName()+" ("+iiPotentialGuest.getLeftEnd()+"-"+iiPotentialGuest.getRightEnd()+")"
									+ " resulted in a case outer overlap partially left of inner."
							);
						}
					} else if (iiPotentialHost.getLeftEnd() <= iiPotentialGuest.getRightEnd() && iiPotentialHost.getRightEnd() >= iiPotentialGuest.getRightEnd()) {
						//case outer overlap partially right of inner
						if (iiPotentialHost.getLeftEnd()-iiPotentialGuest.getRightEnd() <= 60) {
							//consider small overlap due to accretion
						} else {
							throw new Exception("Error buildICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME :"
									+ " comparing element "+iiPotentialHost.getElementName()+" ("+iiPotentialHost.getLeftEnd()+"-"+iiPotentialHost.getRightEnd()+")"
									+ " with element "+iiPotentialGuest.getElementName()+" ("+iiPotentialGuest.getLeftEnd()+"-"+iiPotentialGuest.getRightEnd()+")"
									+ " resulted in a case outer overlap partially right of inner."
							);
						}
						
					} else {
						//else error case non treated
						throw new Exception("Error buildICEIMEElementName2GenomicBoundariesWithinThatDelineateGuestInsertionICEIME :"
										+ " comparing element "+iiPotentialHost.getElementName()+" ("+iiPotentialHost.getLeftEnd()+"-"+iiPotentialHost.getRightEnd()+")"
										+ " with element "+iiPotentialGuest.getElementName()+" ("+iiPotentialGuest.getLeftEnd()+"-"+iiPotentialGuest.getRightEnd()+")"
										+ " resulted in a case that was not defined."
								);
					}
				}
				
				
			}
		}
		
		return hmToReturn;
	}

	private static ArrayList<Integer> getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField(
			Connection connComparaisonDB
			, HashSet<Integer> hsElementIds
			, String rawGenesIdentifiersIT
			, ArrayList<String> warningFails
			, String ICEIMEElementName
			, String typeOfIdentifier // integrase, relaxase, coupling protein or virB4, target gene
			) throws Exception {

		ArrayList<Integer> alToReturn = new ArrayList<>();
		
		if (rawGenesIdentifiersIT.matches("^.*\\?.*$")
				|| rawGenesIdentifiersIT.matches("^.*unknown.*$")
				|| rawGenesIdentifiersIT.matches("^.*not\\s+known.*$")
				|| rawGenesIdentifiersIT.equalsIgnoreCase("none")
				|| rawGenesIdentifiersIT.matches("^.*absent.*$")
				|| rawGenesIdentifiersIT.equalsIgnoreCase("not annotated")
				|| rawGenesIdentifiersIT.equalsIgnoreCase("feat_exist_no_locus_tag")
				|| rawGenesIdentifiersIT.equalsIgnoreCase("no_feat")
				|| rawGenesIdentifiersIT.equalsIgnoreCase("wrong annotation")
				|| rawGenesIdentifiersIT.matches("^.*intergenic.*$")
				|| rawGenesIdentifiersIT.matches("^NS$")
				|| rawGenesIdentifiersIT.toLowerCase().matches("^.*integrase\\s+specific\\s+of\\s+.+,\\s+but.+$")
				) {
			//assume no gene reference on purpose
			return new ArrayList<Integer>();
		}
		
		//System.out.println("getAlGeneIdsMatchingLocusTagOrProteinId_withRawGenesIdentifiersFromICEIMEField HERE = "+rawGenesIdentifiersIT);
		
		String[] genesIdentifiersIT = rawGenesIdentifiersIT.split("[\\+\\-\\s\\/]+");//"\\+|\\-|\\s|\\/"
		ArrayList<String> alMatchRegexStringIT = new ArrayList<String>();
		//ArrayList<String> alMatchRegexStringIT_pseudo = new ArrayList<String>();
		
		if (genesIdentifiersIT.length >= 8
				//|| rawGenesIdentifiersIT.contains("'")
				|| rawGenesIdentifiersIT.matches("^.*[^35l]'.*$")
				|| rawGenesIdentifiersIT.contains(";")
				//|| rawGenesIdentifiersIT.contains("é")
				//|| rawGenesIdentifiersIT.contains("è")
				) {
			warningFails.add(
					"WARNING for "+ICEIMEElementName
					+ " : sentence was detected in string of identifiers : \""+rawGenesIdentifiersIT+"\"."
					);
			//assume sentence on purpose
			return new ArrayList<Integer>();
		}
		
		for (int i=0; i<genesIdentifiersIT.length; i++) {
            String geneIdentifierIT = genesIdentifiersIT[i];
            
            //System.err.println(geneIdentifierIT);
            
            Matcher mWithVersion = geneIdWithVersion.matcher(geneIdentifierIT);
            Matcher mDotPseudo = geneIdDotPseudo.matcher(geneIdentifierIT);
            if (geneIdentifierIT.equalsIgnoreCase("absent")
            		|| geneIdentifierIT.equalsIgnoreCase("None")
            		|| geneIdentifierIT.equalsIgnoreCase("3'")
            		|| geneIdentifierIT.equalsIgnoreCase("5'")
            		|| geneIdentifierIT.equalsIgnoreCase("in")
            		|| geneIdentifierIT.equalsIgnoreCase("(modifié")
            		|| geneIdentifierIT.equalsIgnoreCase("par")
            		|| geneIdentifierIT.equalsIgnoreCase("insertion)")
            		|| geneIdentifierIT.equalsIgnoreCase("annoté")
            		|| geneIdentifierIT.equalsIgnoreCase("comme)")
            		|| geneIdentifierIT.equalsIgnoreCase("l'intégration)")
            		) {
            	//skip
            	continue;
//            } else if (geneIdentifierIT.matches("^.*[\\(\\)\\.\\+].*$")) { // may create regex problem
//            	//skip
//            	continue;
            } else if (mWithVersion.matches()) {
            	String stToMatch = mWithVersion.group(1);
            	stToMatch = replaceStringForRegexPostresql(stToMatch);
            	if ( ! stToMatch.isEmpty()) {
                	alMatchRegexStringIT.add(" ~* \'\\m"+stToMatch+"\\M\'");// PostgreSQL uses \m, \M, \y and \Y as word boundaries: \m   matches only at the beginning of a word \M   matches only at the end of a word	
            	}
            } else if (mDotPseudo.matches()
            		|| geneIdentifierIT.equalsIgnoreCase("pseudo")
            		|| geneIdentifierIT.equalsIgnoreCase("pseudogene")
            		) {
            	//alMatchRegexStringIT_pseudo.add(" ~* \'"+mDotPseudo.group(1)+"\'");
            	//skip
            	continue;
            } else {
				// exemple alMatchRegexString.add(" ~* \'dna\'");
				// exemple alMatchRegexString.add(" ~* \'^[^{]*16\\.9\\:\\s+Replicate.*$\'");
            	String stToMatch = geneIdentifierIT;
            	stToMatch = replaceStringForRegexPostresql(stToMatch);
            	if ( ! stToMatch.isEmpty()) {
    	            alMatchRegexStringIT.add(" ~* \'\\m"+stToMatch+"\\M\'");// PostgreSQL uses \m, \M, \y and \Y as word boundaries: \m   matches only at the beginning of a word \M   matches only at the end of a word
            	}
            }
		}
		
		ArrayList<String> alTypeQualToMatchIT = new ArrayList<String>();
		alTypeQualToMatchIT.add("gene");
		alTypeQualToMatchIT.add("gene_id");
		alTypeQualToMatchIT.add("gene_synonym");
		alTypeQualToMatchIT.add("orf_name");
		alTypeQualToMatchIT.add("protein_id");
		alTypeQualToMatchIT.add("prot_id");
		alTypeQualToMatchIT.add("locus_tag");
		alTypeQualToMatchIT.add("old_locus_tag");
		
		if ( ! alMatchRegexStringIT.isEmpty()) {
			
			alToReturn = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAlGeneIdWithAlElementIdAndAlTypeQualToMatchAndAlStringToMatch(
					connComparaisonDB
					, alMatchRegexStringIT //ArrayList<String> alMatchRegexString
					, false //boolean isAnd
					, hsElementIds
					, alTypeQualToMatchIT
					, false // getMirrorList
					, false // isNotNull
					, true // searchForGeneNameInTableGeneToo
					);
			
			//System.err.println("alMatchRegexStringIT.size() = "+alMatchRegexStringIT.size()+ " ; alToReturn.size() = "+alToReturn.size());
			
			if (alMatchRegexStringIT.size() > alToReturn.size()) {
				if (typeOfIdentifier.compareTo("target gene") == 0 && ICEIMEElementName.toLowerCase().matches("^.*trna.*$")) {
					//will deal with exception latter
					return new ArrayList<Integer>();
				} else {
					warningFails.add(
							"WARNING for "+ICEIMEElementName
							+ " : all or some part of the "+typeOfIdentifier+" identifier \""+rawGenesIdentifiersIT+"\" was not found in the comparaison genomic database."
							);
					return new ArrayList<Integer>();
				}
			} else if (alMatchRegexStringIT.size() < alToReturn.size()) {
				warningFails.add(
						"WARNING for "+ICEIMEElementName
						+ " : multiple matchs in the database were attributed for "+typeOfIdentifier+" identifier \""+rawGenesIdentifiersIT+"\"."
						);
				return new ArrayList<Integer>();
			}
		}

//		// idem for pseudo, expect ok if not found
//		if ( ! alMatchRegexStringIT_pseudo.isEmpty()) {
//			ArrayList<Integer> alGeneIdsIntegrase_pseudo = fr.inra.jouy.server.queriesTable.QueriesTableGenes.getAlGeneIdWithAlElementIdAndAlTypeQualToMatchAndAlStringToMatch(
//					connComparaisonDB
//					, alMatchRegexStringIT_pseudo //ArrayList<String> alMatchRegexString
//					, false //boolean isAnd
//					, hsElementIds
//					, alTypeQualToMatchIT
//					, false // getMirrorList
//					, false // isNotNull
//					, false // searchForGeneNameInTableGeneToo
//					);
//			if (alGeneIdsIntegrase_pseudo != null && ! alGeneIdsIntegrase_pseudo.isEmpty()) {
//				alToReturn.addAll(alGeneIdsIntegrase_pseudo);
//			}
//		}
		
		/*QueriesTableGenes.getElementIdAndFeatureIdWithGeneId
		getGeneIdWithtypeQual2QueryStringAndOrganismId
		ArrayList<String> alTypeQualIT = new ArrayList<String>();
		alTypeQualIT.add("gene_name");
		alTypeQualIT.add("gene");
		ArrayList<String> alTypeQualGeneName = fr.inra.jouy.server.queriesTable.QueriesTableMicadoQualifiers.getAlGeneQualifiers_withAccnumAndAlTypeQual_optionalFeatId(
						conn,
						accession,
						featureId,
						alTypeQualIT,
						false,
						false
						);
						*/
		return alToReturn;
		
	}

	private static String replaceStringForRegexPostresql(String stToMatch) {

		stToMatch = stToMatch.replace(".", "\\.");
		stToMatch = stToMatch.replace("(", "\\(");
		stToMatch = stToMatch.replace(")", "\\)");
		
		
//		String a = "\\*\\"; // or: String a = "/*/";
//		String replacement = Matcher.quoteReplacement(a);
//		String searchString = Pattern.quote(".");
//		String str = xpath.replaceAll(searchString, replacement);

		
		
		return stToMatch;
	}

	private static ArrayList<ICEIMEItem_fromFile> parseFileICEIME(
			String pathToFileICEIME
			, ArrayList<ICEIMEItem_fromFile> alPotentialHostICEIME
			, ArrayList<ICEIMEItem_fromFile> alPotentialGuestICEIME
			) throws Exception {

		ArrayList<ICEIMEItem_fromFile> alToReturn = new ArrayList<>();
		String errorICEIMETOReport = "";
		
		int countParsedICE = 0;
		int countParsedICEDerivate = 0;
		int countParsedIME = 0;
		int countParsedIMEDerivate = 0;
		int countParsedUnsure = 0;
		int countSkippedElementWithoutName = 0;
		int countLimiteICEIMENotSure = 0;
		int countLimiteICEIMEEmpty = 0;
		int countSkippedOtherTypeOfElement = 0;
		int countICEIMEParsedIncorrectly = 0;
		int countParsedIntegraseTyr = 0;
		int countParsedIntegraseSer = 0;
		int countParsedIntegraseDDE = 0;
		int countParsedIntegraseUnknown = 0;
		int countParsedIntegraseUnsure = 0;
		
		//if changes, change below too !
		HashMap<String, Integer> hmMandatoryColmunsInFile2idx = new HashMap<>();
		hmMandatoryColmunsInFile2idx.put("Element name",-1);
		hmMandatoryColmunsInFile2idx.put("Genome_id",-1);
		hmMandatoryColmunsInFile2idx.put("element type",-1);
		hmMandatoryColmunsInFile2idx.put("integrase type",-1);
		hmMandatoryColmunsInFile2idx.put("Family",-1);
		hmMandatoryColmunsInFile2idx.put("Left end",-1);
		hmMandatoryColmunsInFile2idx.put("Right end",-1);
		hmMandatoryColmunsInFile2idx.put("Accretion",-1);
		hmMandatoryColmunsInFile2idx.put("Insertion",-1);
		hmMandatoryColmunsInFile2idx.put("Integrase_embl_cds",-1);
		hmMandatoryColmunsInFile2idx.put("Relaxase_embl_cds",-1);
		hmMandatoryColmunsInFile2idx.put("Coupling_protein_embl_cds",-1);
		hmMandatoryColmunsInFile2idx.put("VirB4_cds",-1);
		hmMandatoryColmunsInFile2idx.put("Insertion gene",-1);
		hmMandatoryColmunsInFile2idx.put("insertion_gene_cds",-1);
		hmMandatoryColmunsInFile2idx.put("insertion_gene_function",-1);

		//if changes, change below too !
		HashMap<String, Integer> hmNotMandatoryColmunsInFile2idx = new HashMap<>();
		hmNotMandatoryColmunsInFile2idx.put("integrase location (compared to target)",-1);
		hmNotMandatoryColmunsInFile2idx.put("integrase orientation (compared to target)",-1);
		hmNotMandatoryColmunsInFile2idx.put("Autre remarque/ précision (GG)",-1);
		hmNotMandatoryColmunsInFile2idx.put("Remarques/ défaut Artemis",-1);
		hmNotMandatoryColmunsInFile2idx.put("integrase_reference",-1);
		hmNotMandatoryColmunsInFile2idx.put("relaxase_reference",-1);
		hmNotMandatoryColmunsInFile2idx.put("coupling_protein_reference",-1);
		hmNotMandatoryColmunsInFile2idx.put("virb4_reference",-1);
		hmNotMandatoryColmunsInFile2idx.put("insertion_gene_locus_tag",-1);
		hmNotMandatoryColmunsInFile2idx.put("insertion_gene_reference",-1);
		
		/*
		//column of interest in file
		Element name ; skip if no Element name, consider not clean ; should be 312 mobile element
		Genome_id
		element type ; values for Element name that are not empty :
			ICE
			putative ICE
			putative ICE ?
			ICE derivate
			putative ICE or ICE derivate
			Dérivé d'ICE
			putative IME
			IME derivate
			//NOT TAKEN INTO ACCOUNT : MGI
			//NOT TAKEN INTO ACCOUNT : MGI or IME remnant
			//NOT TAKEN INTO ACCOUNT : putative satellite prophage
			//NOT TAKEN INTO ACCOUNT : MGI or putative satellite
			//NOT TAKEN INTO ACCOUNT : satellite prophage
			//NOT TAKEN INTO ACCOUNT : putative satellite
			//NOT TAKEN INTO ACCOUNT : putative satellit
		integrase type			
			Tyr
			TYr	
			Ser
			DDE
			3 Ser
			[EMPTY]
		integrase location (compared to target)
			adjacent
			NA
			a gene encoding a FIC/DOC protein located between the 3 Int and the end, adjacent to the 5' part of the target gene
			opposite end
			adjacent to the 3' part of the target gene
			adjacent to the 5' part of the target gene
			adjacent to the 3' part
			2 genes (ATPase and peptidase) located between int and element end, adjacent to the 3' part of the target
			ORF6N located between the 3 Int Ser and the end
			adajacent to the 3' end
			3 gene between tRNA gene and Int (orientation identical to the one of Int)
			1 gene  encoding a protein (PF08020 = DUF1706 = DinB, unknown function) located between rpmG and Int, same orientation that int
			7 genes encoding proteins between integrase gene and tRNA, see comments
			3 genes located between tRNALys and int
			adjacent to the CIME
			2 genes encoding proteins located between the att site and integrase gene (CBZ47253 phage derived protein Gp49-like near int, CBZ47254 HTH_3 pfam01381 near att site), same orientation than int
			adjacent to one end
			1 gene encoding protein located between the att site and integrase gene (no domain, unrelated to proteins with the same location in other IMEs integrated in rpmG)
			1 gene encoding a protein (EGE54886, no domain,  not related to AEF24524, located between int and tRNALeu for IME_Sparau11537_tRNAleu) located between the att site and integrase gene, same orientation than int
			2 genes encoding ptoteins located between int and rpmG (no domain for the one located near int, ABC_MJ0796_LolCDE_FtsE  cd03255 ABC transporter for the one located nera rpmG), same orientation than rpmG
		integrase orientation (compared to target)
			complement
			NA
			identical
			complement to the one of luxS
			outward facing
			[EMPTY]
		Family
			Tn916
			ICESt3
			Tn1549
			TnGBS1
			Tn5252
			TnGBS2
			[EMPTY]
			VanG
			vanG
		Left end
		Right end
		Accretion
		Insertion
		Autre remarque/ précision (GG)
		Remarques/ défaut Artemis
		Integrase_embl_cds
		integrase_reference
		Relaxase_embl_cds
		relaxase_reference
		Coupling_protein_embl_cds
		coupling_protein_reference
		VirB4_cds
		virb4_reference
		Insertion gene
		insertion_gene_locus_tag
		insertion_gene_cds
		insertion_gene_reference
		insertion_gene_function
		*/
		
		
		// pass the path to the file as a parameter 
	   
	  
	    int numberLine = 0;
	    //BufferedReader reader = new BufferedReader(new FileReader(pathToFileICEIME2));
	    //String lineIT = reader.readLine();
		//LINE_IN_FILE: while (lineIT != null) {
	    File file = new File(pathToFileICEIME); 
	    Scanner sc = new Scanner(file); 
	    //int numberFieldsInFile = -1;

	    LINE_IN_FILE: while (sc.hasNextLine()) {
	    	numberLine++;
	    	String lineIT = sc.nextLine();
	    	//lineIT = lineIT.trim();
	    	if (lineIT.isEmpty()) {
	    		continue LINE_IN_FILE;
	    	}
    		//System.err.println("lineIT = ["+lineIT+"]");
	        String[] tabsInLineIT = lineIT.split("\\t");
	    	if (tabsInLineIT.length <= 1) {
	    		continue LINE_IN_FILE;
	    	}
	      //System.out.println(sc.nextLine());
	    	if (numberLine == 1) {
	    		//parse header
	    		int countParsedMandatoryColmuns = 0;
	    		int countParsedNotMandatoryColmuns = 0;
		        for (int i=0; i<tabsInLineIT.length; i++) {
		            String tabInLineIT = tabsInLineIT[i];
		            if (hmMandatoryColmunsInFile2idx.containsKey(tabInLineIT)) {
		            	hmMandatoryColmunsInFile2idx.put(tabInLineIT, i);
		            	countParsedMandatoryColmuns++;
		            } else if (hmNotMandatoryColmunsInFile2idx.containsKey(tabInLineIT)) {
		            	hmNotMandatoryColmunsInFile2idx.put(tabInLineIT, i);
		            	countParsedNotMandatoryColmuns++;
		            }
		        }
	    		//check For hmMandatoryColmunsInFile2idx
		        for (Entry<String, Integer> entryHmMandatoryColmunsInFile2 : hmMandatoryColmunsInFile2idx.entrySet()) {
		            String colmunIT = entryHmMandatoryColmunsInFile2.getKey();
		            Integer idxIT = entryHmMandatoryColmunsInFile2.getValue();
		            if (idxIT < 0) {
		            	throw new Exception("Error parseFileICEIME : The column "+colmunIT+" is not found in the header of the file "+pathToFileICEIME);
		            }
		        }
	    		//check For hmNotMandatoryColmunsInFile2idx
//		        for (Entry<String, Integer> entryHmNotMandatoryColmunsInFile2 : hmNotMandatoryColmunsInFile2idx.entrySet()) {
//		            String colmunIT = entryHmNotMandatoryColmunsInFile2.getKey();
//		            Integer idxIT = entryHmNotMandatoryColmunsInFile2.getValue();
//		            if (idxIT < 0) {
//		            	throw new Exception("Error parseFileICEIME : The column "+colmunIT+" is not found in the header of the file "+pathToFileICEIME);
//		            }
//		        }
		    	//numberFieldsInFile = tabsInLineIT.length;
		    	if (VERBOSE) {
		    		System.out.println("Done with header of file "+pathToFileICEIME+". "
		    				+ countParsedMandatoryColmuns + " mandatory colmun(s) were parsed"
		    				+ " ; "+ countParsedNotMandatoryColmuns + " not mandatory colmun(s) were parsed.");
		    	}
		    	
	    	} else {
		    	ICEIMEItem_fromFile IIIT = new ICEIMEItem_fromFile();
		    	boolean ICEIMEElementSkippedForAnalysis = false;
		    	//if (numberFieldsInFile != tabsInLineIT.length) {
		    	//	throw new Exception("Error parseFileICEIME : different numbers of columns for the header ("+numberFieldsInFile+") and the line ("+tabsInLineIT.length+") (line number "+numberLine+") : \n"+lineIT+"\n of the file "+pathToFileICEIME);
		    	//}
		    	
	    		//parse line
		    	int countMandatoryColmunTreated = 0;
		    	//int countNotMandatoryColmunTreated = 0;
		        for (int i=0; i<tabsInLineIT.length; i++) {
		            String tabInLineIT = tabsInLineIT[i];
		            tabInLineIT = tabInLineIT.trim();
		            String mandatoryColmunIT = getKeyByValue(hmMandatoryColmunsInFile2idx, i);
		            String notMandatoryColmunIT = getKeyByValue(hmNotMandatoryColmunsInFile2idx, i);
		            
		            //System.err.println("idx tab = "+i+" ; mandatoryColmunIT = "+mandatoryColmunIT);
		            //System.err.println("idx tab = "+i+" ; notMandatoryColmunIT = "+notMandatoryColmunIT);
		            
		            if (mandatoryColmunIT != null) {
		            	if (mandatoryColmunIT.compareTo("Element name")==0) {
		            		//System.err.println("Element name at idx tab = "+i+" ; tabInLineIT = ["+tabInLineIT+"]");
		            		if (tabInLineIT == null || tabInLineIT.isEmpty() ) {
		            			countSkippedElementWithoutName++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else {
			            		IIIT.setElementName(tabInLineIT);
			            		countMandatoryColmunTreated++;
		            		}
		            	} else if (mandatoryColmunIT.compareTo("Genome_id")==0) {
		            		if (tabInLineIT == null || tabInLineIT.isEmpty() ) {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				errorICEIMETOReport += "\tWARNING : for the element " + IIIT.getElementName() + " : the value from the colum \"Genome_id\" could not be inferred\n";
		            			}
		            		} else {
		            			IIIT.setGenomeId(tabInLineIT);
			            		countMandatoryColmunTreated++;
		            		}
		            	} else if (mandatoryColmunIT.compareTo("element type")==0) {
		            		ICEIMEItem_fromFile.Category_of_elementType categoryOfElementTypeIT = null;
		            		if (tabInLineIT.compareTo("ICE")==0) {
		            			categoryOfElementTypeIT = ICEIMEItem_fromFile.Category_of_elementType.ICE;
		            			//countParsedICE++;
		            		} else if (tabInLineIT.compareTo("putative ICE")==0) {
		            			categoryOfElementTypeIT = ICEIMEItem_fromFile.Category_of_elementType.ICE;
		            			//countParsedICE++;
		            		} else if (tabInLineIT.compareTo("putative ICE ?")==0) {
		            			categoryOfElementTypeIT = ICEIMEItem_fromFile.Category_of_elementType.UNSURE;
		            			//countParsedUnsure++;
		            		} else if (tabInLineIT.compareTo("ICE derivate")==0) {
		            			categoryOfElementTypeIT = ICEIMEItem_fromFile.Category_of_elementType.ICE_derivate;
		            			//countParsedICEDerivate++;
		            		} else if (tabInLineIT.compareTo("putative ICE or ICE derivate")==0) {
		            			categoryOfElementTypeIT = ICEIMEItem_fromFile.Category_of_elementType.ICE_derivate;
		            			//countParsedICEDerivate++;
		            		} else if (tabInLineIT.compareTo("Dérivé d'ICE")==0) {
		            			categoryOfElementTypeIT = ICEIMEItem_fromFile.Category_of_elementType.ICE_derivate;
		            			//countParsedICEDerivate++;
		            		} else if (tabInLineIT.compareTo("putative IME")==0) {
		            			categoryOfElementTypeIT = ICEIMEItem_fromFile.Category_of_elementType.IME;
		            			//countParsedIME++;
		            		} else if (tabInLineIT.compareTo("IME derivate")==0) {
		            			categoryOfElementTypeIT = ICEIMEItem_fromFile.Category_of_elementType.IME_derivate;
		            			//countParsedIMEDerivate++;
		            		// element NOT TAKEN INTO ACCOUNT, skip this line
		            		} else if (tabInLineIT.compareTo("MGI")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else if (tabInLineIT.compareTo("MGI or IME remnant")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else if (tabInLineIT.compareTo("ICE remnant")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else if (tabInLineIT.compareTo("IME remnant")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else if (tabInLineIT.compareTo("integrated plasmid or IME")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else if (tabInLineIT.compareTo("putative satellite prophage")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else if (tabInLineIT.compareTo("MGI or putative satellite")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else if (tabInLineIT.compareTo("satellite prophage")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else if (tabInLineIT.compareTo("putative satellite")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else if (tabInLineIT.compareTo("putative satellit")==0) {
		            			countSkippedOtherTypeOfElement++;
		            			//continue LINE_IN_FILE;
		            			ICEIMEElementSkippedForAnalysis = true;
		            		} else {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				throw new Exception("Error parseFileICEIME : unrecognized Category_of_elementType : \""+tabInLineIT+"\" for mandatoryColmunIT element type with tab number "+i+" ("+tabInLineIT+") from the line\n"+lineIT+"\n of the file "+pathToFileICEIME);
		            			}
		            		}
		            		if (categoryOfElementTypeIT != null) {
		            			IIIT.setElementType(categoryOfElementTypeIT);
			            		countMandatoryColmunTreated++;
		            		} else {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
			            			throw new Exception("Error parseFileICEIME : categoryOfElementTypeIT IS null : \""+tabInLineIT+"\" for mandatoryColmunIT element type with tab number "+i+" ("+tabInLineIT+") from the line\n"+lineIT+"\n of the file "+pathToFileICEIME);
		            			}
		            		}
		            	} else if (mandatoryColmunIT.compareTo("integrase type")==0) {
		            		ICEIMEItem_fromFile.IntegraseType integraseTypeIT = null;
		            		if (tabInLineIT.equalsIgnoreCase("Tyr")) {
		            			integraseTypeIT = ICEIMEItem_fromFile.IntegraseType.Tyr;
		            			//countParsedIntegraseTyr++;
		            		} else if (
		            				tabInLineIT.equalsIgnoreCase("Ser")
		            				|| tabInLineIT.equalsIgnoreCase("3 Ser")
		            				) {
		            			integraseTypeIT = ICEIMEItem_fromFile.IntegraseType.Ser;
		            			//countParsedIntegraseSer++;
		            		} else if (tabInLineIT.equalsIgnoreCase("DDE")) {
		            			integraseTypeIT = ICEIMEItem_fromFile.IntegraseType.DDE;
		            			//countParsedIntegraseDDE++;
		            		} else if (tabInLineIT.isEmpty()) {
		            			integraseTypeIT = ICEIMEItem_fromFile.IntegraseType.UNKNOWN;
		            			//countParsedIntegraseUnknown++;
		            		} else if (tabInLineIT.matches("^.*\\?.*$")) {
				            	integraseTypeIT = ICEIMEItem_fromFile.IntegraseType.UNSURE;
				            	//countParsedIntegraseUnsure++;
		            		} else {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				throw new Exception("Error parseFileICEIME : unrecognized IntegraseType : "+tabInLineIT+" for mandatoryColmunIT element type with tab number "+i+" ("+tabInLineIT+") from the line\n"+lineIT+"\n of the file "+pathToFileICEIME);
		            			}
		            		}
		            		if (integraseTypeIT != null) {
		            			IIIT.setIntegraseType(integraseTypeIT);
			            		countMandatoryColmunTreated++;
		            		} else {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				throw new Exception("Error parseFileICEIME : IntegraseType IS null : "+tabInLineIT+" for mandatoryColmunIT element type with tab number "+i+" ("+tabInLineIT+") from the line\n"+lineIT+"\n of the file "+pathToFileICEIME);
		            			}
		            		}
		            	} else if (mandatoryColmunIT.compareTo("Family")==0) {
		            		IIIT.setFamily(tabInLineIT);
		            		countMandatoryColmunTreated++;
		            	} else if (mandatoryColmunIT.compareTo("Left end")==0) {
		            		tabInLineIT = tabInLineIT.replace(" (see comments)", "");
		            		//tabInLineIT = tabInLineIT.replace(" ?", "");
		            		if (tabInLineIT.contains("?")) {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				countLimiteICEIMENotSure++;
			            			ICEIMEElementSkippedForAnalysis = true;
		            			}
		            		} else if (tabInLineIT.isEmpty()) {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				//errorICEIMETOReport += "\tWARNING : for the element " + IIIT.getElementName() + " : the value from the colum \"Left end\" could not be inferred\n";
		            				countLimiteICEIMEEmpty++;
			            			ICEIMEElementSkippedForAnalysis = true;
		            			}
		            		} else {
		            			try {
									int leftEndIT = Integer.parseInt(tabInLineIT);
									IIIT.setLeftEnd(leftEndIT);
				            		countMandatoryColmunTreated++;
								} catch (Exception e) {
									if ( ! ICEIMEElementSkippedForAnalysis) {
										errorICEIMETOReport += "\tWARNING : for the element " + IIIT.getElementName() + " : the value from the colum \"Left end\" could not be inferred. Exception thrown : "+e.getLocalizedMessage()+"\n";
									}
								}
		            		}
		            	} else if (mandatoryColmunIT.compareTo("Right end")==0) {
		            		tabInLineIT = tabInLineIT.replace(" (see comments)", "");
		            		//tabInLineIT = tabInLineIT.replace(" ?", "");
		            		if (tabInLineIT.contains("?")) {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				countLimiteICEIMENotSure++;
			            			ICEIMEElementSkippedForAnalysis = true;
		            			}
		            		} else if (tabInLineIT.isEmpty()) {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				//errorICEIMETOReport += "\tWARNING : for the element " + IIIT.getElementName() + " : the value from the colum \"Right end\" could not be inferred\n";
		            				countLimiteICEIMEEmpty++;
			            			ICEIMEElementSkippedForAnalysis = true;
		            			}
		            		} else {
		            			try {
									int rightEndIT = Integer.parseInt(tabInLineIT);
									IIIT.setRightEnd(rightEndIT);
				            		countMandatoryColmunTreated++;
								} catch (Exception e) {
									if ( ! ICEIMEElementSkippedForAnalysis) {
										errorICEIMETOReport += "\tWARNING : for the element " + IIIT.getElementName() + " : the value from the colum \"Right end\" could not be inferred. Exception thrown : "+e.getLocalizedMessage()+"\n";
									}
								}
			            		//IIIT.setRightEnd(Integer.parseInt(tabInLineIT));
			            		//countMandatoryColmunTreated++;
		            		}
		            	} else if (mandatoryColmunIT.compareTo("Accretion")==0) {
		            		if (tabInLineIT.compareTo("n")==0) {
		            			IIIT.setAccretion(ICEIMEItem_fromFile.ICEIMEAccretion.no);
		            		} else if (tabInLineIT.compareTo("")==0) {
		            			IIIT.setAccretion(ICEIMEItem_fromFile.ICEIMEAccretion.unsure);
		            		} else if (tabInLineIT.compareTo("y")==0) {
		            			IIIT.setAccretion(ICEIMEItem_fromFile.ICEIMEAccretion.yes);
		            		} else if (tabInLineIT.compareTo("?")==0) {
		            			IIIT.setAccretion(ICEIMEItem_fromFile.ICEIMEAccretion.unsure);
		            		} else if (tabInLineIT.compareTo("y?")==0) {
		            			IIIT.setAccretion(ICEIMEItem_fromFile.ICEIMEAccretion.unsure);
		            		} else if (tabInLineIT.compareTo("y ?")==0) {
		            			IIIT.setAccretion(ICEIMEItem_fromFile.ICEIMEAccretion.unsure);
		            		} else if (tabInLineIT.compareTo("y  (int seule)")==0) {
		            			IIIT.setAccretion(ICEIMEItem_fromFile.ICEIMEAccretion.yes);
		            		} else {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				throw new Exception("Error parseFileICEIME : unrecognized Accretion : "+tabInLineIT+" for mandatoryColmunIT element type with tab number "+i+" ("+tabInLineIT+") from the line\n"+lineIT+"\n of the file "+pathToFileICEIME);
		            			}
		            		}
		            		countMandatoryColmunTreated++;
		            	} else if (mandatoryColmunIT.compareTo("Insertion")==0) {
		            		if (tabInLineIT.compareTo("n")==0) {
		            			IIIT.setInsertion(ICEIMEItem_fromFile.ICEIMEInsertion.no);
		            		} else if (tabInLineIT.compareTo("")==0) {
		            			IIIT.setInsertion(ICEIMEItem_fromFile.ICEIMEInsertion.unsure);	
		            		} else if (tabInLineIT.compareTo("host")==0) {
		            			IIIT.setInsertion(ICEIMEItem_fromFile.ICEIMEInsertion.host);		            			
		            		} else if (tabInLineIT.compareTo("guest")==0) {
		            			IIIT.setInsertion(ICEIMEItem_fromFile.ICEIMEInsertion.guest);	
		            		} else if (tabInLineIT.compareTo("?")==0) {
		            			IIIT.setInsertion(ICEIMEItem_fromFile.ICEIMEInsertion.unsure);	
		            		} else if (tabInLineIT.compareTo("guest + host")==0) {
		            			IIIT.setInsertion(ICEIMEItem_fromFile.ICEIMEInsertion.host_and_guest);	
		            		} else if (tabInLineIT.compareTo("guest/host ?")==0) {
		            			IIIT.setInsertion(ICEIMEItem_fromFile.ICEIMEInsertion.unsure);	
		            		} else {
		            			if ( ! ICEIMEElementSkippedForAnalysis) {
		            				throw new Exception("Error parseFileICEIME : unrecognized Insertion : "+tabInLineIT+" for mandatoryColmunIT element type with tab number "+i+" ("+tabInLineIT+") from the line\n"+lineIT+"\n of the file "+pathToFileICEIME);
		            			}
		            		}		
		            		//IIIT.setInsertion(tabInLineIT);
		            		countMandatoryColmunTreated++;
		            	} else if (mandatoryColmunIT.compareTo("Integrase_embl_cds")==0) {
		            		IIIT.setIntegraseEmblCds(tabInLineIT);
		            		countMandatoryColmunTreated++;
		            	} else if (mandatoryColmunIT.compareTo("Relaxase_embl_cds")==0) {
		            		IIIT.setRelaxaseEmblCds(tabInLineIT);
		            		countMandatoryColmunTreated++;
		            	} else if (mandatoryColmunIT.compareTo("Coupling_protein_embl_cds")==0) {
		            		IIIT.setCouplingProteinEmblCds(tabInLineIT);
		            		countMandatoryColmunTreated++;
		            	} else if (mandatoryColmunIT.compareTo("VirB4_cds")==0) {
		            		IIIT.setVirB4Cds(tabInLineIT);
		            		countMandatoryColmunTreated++;
		            	} else if (mandatoryColmunIT.compareTo("Insertion gene")==0) {
		            		IIIT.setInsertionGene(tabInLineIT);
		            		countMandatoryColmunTreated++;
		            	} else if (mandatoryColmunIT.compareTo("insertion_gene_cds")==0) {
		            		IIIT.setInsertionGeneCds(tabInLineIT);
		            		countMandatoryColmunTreated++;
		            	} else if (mandatoryColmunIT.compareTo("insertion_gene_function")==0) {
		            		IIIT.setInsertionGeneFunction(tabInLineIT);
		            		countMandatoryColmunTreated++;
		            	}
		            } else if (notMandatoryColmunIT != null) {
		            	if (notMandatoryColmunIT.compareTo("integrase location (compared to target)")==0) {
		            		IIIT.setIntegraseLocation(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	} else if (notMandatoryColmunIT.compareTo("integrase orientation (compared to target)")==0) {
		            		IIIT.setIntegraseOrientation(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	} else if (notMandatoryColmunIT.compareTo("Autre remarque/ précision (GG)")==0) {
		            		IIIT.setAutreRemarquePrecisionGG(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	} else if (notMandatoryColmunIT.compareTo("Remarques/ défaut Artemis")==0) {
		            		IIIT.setRemarquesDefautArtemis(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	} else if (notMandatoryColmunIT.compareTo("integrase_reference")==0) {
		            		IIIT.setIntegraseReference(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	} else if (notMandatoryColmunIT.compareTo("relaxase_reference")==0) {
		            		IIIT.setRelaxaseReference(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	} else if (notMandatoryColmunIT.compareTo("coupling_protein_reference")==0) {
		            		IIIT.setCouplingProteinReference(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	} else if (notMandatoryColmunIT.compareTo("virb4_reference")==0) {
		            		IIIT.setVirb4_reference(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	} else if (notMandatoryColmunIT.compareTo("insertion_gene_locus_tag")==0) {
		            		IIIT.setInsertionGeneLocusTag(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	} else if (notMandatoryColmunIT.compareTo("insertion_gene_reference")==0) {
		            		IIIT.setInsertionGeneReference(tabInLineIT);
		            		//countNotMandatoryColmunTreated++;
		            	}
		            } else {
		            	// column not used
		            	//throw new Exception("Error parseFileICEIME : no column registred associated ; Tab number "+i+" ("+tabInLineIT+") from the line\n"+lineIT+"\n of the file "+pathToFileICEIME2);
		            }
		            
		        }// parse line
		        
		       
		        if (IIIT.getInsertion() != null) {
		        	 //alPotentialHostICEIME
			        if (IIIT.getInsertion().compareTo(ICEIMEItem_fromFile.ICEIMEInsertion.host)==0
			        		|| IIIT.getInsertion().compareTo(ICEIMEItem_fromFile.ICEIMEInsertion.host_and_guest)==0) {
			        	if (IIIT.getElementName()!= null && ! IIIT.getElementName().isEmpty() && IIIT.getLeftEnd() >= 0 && IIIT.getRightEnd() >= 0 ) {
			        		alPotentialHostICEIME.add(IIIT);
			        	}
			        }
			        // alPotentialGuestICEIME
			        if (IIIT.getInsertion().compareTo(ICEIMEItem_fromFile.ICEIMEInsertion.guest)==0
			        		|| IIIT.getInsertion().compareTo(ICEIMEItem_fromFile.ICEIMEInsertion.host_and_guest)==0) {
			        	if (IIIT.getLeftEnd() >= 0 && IIIT.getRightEnd() >= 0 ) {
			        		alPotentialGuestICEIME.add(IIIT);
			        	}
			        }
		        }

		        
		        if ( countMandatoryColmunTreated != hmMandatoryColmunsInFile2idx.size() ) {
		        	//throw new Exception("Error parseFileICEIME : ! countMandatoryColmunTreated ("+countMandatoryColmunTreated+") != hmMandatoryColmunsInFile2idx.size() ("+hmMandatoryColmunsInFile2idx.size()+") ;  (line number "+numberLine+") Line\n"+lineIT+"\n of the file "+pathToFileICEIME);
		        	if ( ! ICEIMEElementSkippedForAnalysis ) {
		        		errorICEIMETOReport += "\tWARNING : The element " + IIIT.getElementName() + " will be skipped for the analysis because it doesn't have a value for all the mandatory fields\n";
			        	countICEIMEParsedIncorrectly++;
		        	}
		        	
		        } else {
		        	if ( ! ICEIMEElementSkippedForAnalysis ) {
		        		alToReturn.add(IIIT);
		        		if (IIIT.getElementType().compareTo(ICEIMEItem_fromFile.Category_of_elementType.ICE) ==0 ) {
		        			countParsedICE++;
		        		} else if (IIIT.getElementType().compareTo(ICEIMEItem_fromFile.Category_of_elementType.UNSURE) ==0 ) {
		        			countParsedUnsure++;
		        		} else if (IIIT.getElementType().compareTo(ICEIMEItem_fromFile.Category_of_elementType.ICE_derivate) ==0 ) {
		        			countParsedICEDerivate++;
		        		} else if (IIIT.getElementType().compareTo(ICEIMEItem_fromFile.Category_of_elementType.IME) ==0 ) {
		        			countParsedIME++;
		        		} else if (IIIT.getElementType().compareTo(ICEIMEItem_fromFile.Category_of_elementType.IME_derivate) ==0 ) {
		        			countParsedIMEDerivate++;
		        		}
		        		if (IIIT.getIntegraseType().compareTo(ICEIMEItem_fromFile.IntegraseType.Tyr)==0) {
		        			countParsedIntegraseTyr++;
		        		} else if (IIIT.getIntegraseType().compareTo(ICEIMEItem_fromFile.IntegraseType.Ser)==0) {
		        			countParsedIntegraseSer++;
		        		} else if (IIIT.getIntegraseType().compareTo(ICEIMEItem_fromFile.IntegraseType.DDE)==0) {
		        			countParsedIntegraseDDE++;
		        		} else if (IIIT.getIntegraseType().compareTo(ICEIMEItem_fromFile.IntegraseType.UNKNOWN)==0) {
		        			countParsedIntegraseUnknown++;
		        		} else if (IIIT.getIntegraseType().compareTo(ICEIMEItem_fromFile.IntegraseType.UNSURE)==0) {
		        			countParsedIntegraseUnsure++;
		        		}
		        		
		        	} else {
		        		throw new Exception("Error parseFileICEIME : countMandatoryColmunTreated == hmMandatoryColmunsInFile2idx.size() but ICEIMEElementSkippedForAnalysis ; elementName = "+IIIT.getElementName()+" ; From the line\n"+lineIT+"\n of the file "+pathToFileICEIME);
		        	}
		        }
		       
	    	}
	    	//lineIT = reader.readLine();
	    }
		//reader.close();
	    sc.close();
	    
	    if (VERBOSE) {
	    	
	    	System.out.println(
	    					"Done Parsing the file that list the ICEs and IMEs : "
	    					+ "\n\t" + alToReturn.size() + " ICEs OR IMEs have been parsed correctly."
	    					+ "\n\t" + "It includes " + countParsedICE + " ICE, " + countParsedICEDerivate + " ICE derivate, " + countParsedIME + " IME, " + countParsedIMEDerivate + " IME derivate, and "+countParsedUnsure+" unsure element type."
	    					+ "\n\t" + "It includes " + countParsedIntegraseTyr + " element with integrase Tyr, " + countParsedIntegraseSer + " element with integrase Ser, " + countParsedIntegraseDDE + " element with integrase DDE, " + countParsedIntegraseUnknown + " element with integrase of unknown type, " + countParsedIntegraseUnsure + " element with integrase of unsure type." 
	    					+ "\n\t" + countICEIMEParsedIncorrectly + " ICEs OR IMEs have been parsed incorrectly because they doesn't have a value for all the mandatory fields, see the related error(s) below for details."
	    					+ "\n\t"+ countSkippedElementWithoutName + " element(s) will be skipped for the analysis because they have no Element name."
	    					+ "\n\t"+ countLimiteICEIMENotSure + " element(s) will be skipped for the analysis because their right end and left end limits are unsure (question marks)."
	    					+ "\n\t"+ countLimiteICEIMEEmpty + " element(s) will be skipped for the analysis because their right end and left end limits are empty."
	    					+ "\n\t"+ countSkippedOtherTypeOfElement + " element(s) with a name will be skipped for the analysis because their type was different from ICE, ICE derivate, IME, or IME derivate."
	    			);
	    	if ( ! errorICEIMETOReport.isEmpty()) {
	    		System.out.println(
    					"\n" + errorICEIMETOReport
    			);
	    	} else {
	    		System.out.println(
    					"\n"
    			);
	    	}
	    }
	    
	    
	    
	    return alToReturn;
	    
	}

	public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
	    for (Entry<T, E> entry : map.entrySet()) {
	        if (Objects.equals(value, entry.getValue())) {
	            return entry.getKey();
	        }
	    }
	    return null;
	}
	
//	// idx 0 : numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution 
//	// idx 1 : percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution
//	// idx 2 : numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship 
//	// idx 3 : percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
//	// idx 4 : avgPercentIdentityWithCDSsConservedDuringEvolution
//	// idx 5 : avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
//	// idx 6 : medianEvalueAlignmentsWithCDSsConservedDuringEvolution
//	private static ArrayList<String> calculateStatSummaryForCDS(
//			Connection conn
//			, int geneIDIT // geneID
//			, int organismIdIT //organismId
//			, HashSet<Integer> hsOrgaIdsFeaturedGenomesIT // scope (hsOrgaIdsFeaturedGenomes)
//			//, HashSet<Integer> hsOrgaIdsMainListIT // scope (hsOrgaIdsMainList)
//			//, boolean getResultFoFeaturedList_mainListIfFalse // get Result From featured or main list
//			) throws Exception {
//
//		String methodToReport = "AnalysisICEIME calculateStatSummaryForCDS "
//				+ " ; geneIDIT="+geneIDIT
//				+ " ; organismIdIT="+organismIdIT
//				+ " ; hsOrgaIdsFeaturedGenomesIT="+ ( (hsOrgaIdsFeaturedGenomesIT != null ) ? hsOrgaIdsFeaturedGenomesIT.toString() : "NULL" )
//				//+ " ; hsOrgaIdsMainListIT="+ ( (hsOrgaIdsMainListIT != null ) ? hsOrgaIdsMainListIT.toString() : "NULL" )
//				//+ " ; getResultFoFeaturedList_mainListIfFalse="+getResultFoFeaturedList_mainListIfFalse
//				;
//
//		HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneNameIT = new HashMap<>();
//		HashMap<Integer, String> qGeneId2qGeneNameIT = new HashMap<>();
//		qGeneId2qGeneNameIT.put(geneIDIT, "NA"); // qGeneName not important here
//		qOrganismId2qGeneId2qGeneNameIT.put(organismIdIT, qGeneId2qGeneNameIT);
//
//		ArrayList<String> alToReturn = new ArrayList<>();
//		String rawOutputStatSummaryRefCDS = "";
//
//		// if hsOrgaIdsFeaturedGenomesIT small size (< 15), consider not stat significant and do not calculate and fill up with empty string instead
//		if (hsOrgaIdsFeaturedGenomesIT.size() < 15) {
//			alToReturn.add(""); // numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution
//			alToReturn.add(""); // percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution
//			alToReturn.add(""); // numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship
//			alToReturn.add(""); // percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
//			alToReturn.add(""); // avgPercentIdentityWithCDSsConservedDuringEvolution
//			alToReturn.add(""); // avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
//			alToReturn.add(""); // medianEvalueAlignmentsWithCDSsConservedDuringEvolution
//		} else {
//
//
//			
//			rawOutputStatSummaryRefCDS = ComparativeGenomics.getStatSummaryRefCDS(
//					conn
//					, qOrganismId2qGeneId2qGeneNameIT //contains only 1 qOrganismId, 1 qGeneId and 1 qGeneName
//					, hsOrgaIdsFeaturedGenomesIT
//					//, hsOrgaIdsMainListIT
//					);
//
////			CallForInfoDBImpl cfidbiIT = new CallForInfoDBImpl();
////			rawOutputStatSummaryRefCDS = cfidbiIT.getStatSummaryRefCDS(
////					qOrganismId2qGeneId2qGeneNameIT //contains only 1 qOrganismId, 1 qGeneId and 1 qGeneName
////					, hsOrgaIdsFeaturedGenomesIT
////					//, hsOrgaIdsMainListIT
////					);
//
////			Matcher m = null;
////			if (getResultFoFeaturedList_mainListIfFalse) {
////				m = ComparisonWithFeaturedList.matcher(rawOutputStatSummaryRefCDS);
////			} else {
////				m = ComparisonWithMainList.matcher(rawOutputStatSummaryRefCDS);
////			}
////			Matcher m = ComparisonWithFeaturedList.matcher(rawOutputStatSummaryRefCDS);
////			if (m.matches()) {
////				String numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = m.group(1);
////				String percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = m.group(2);
////				String numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship  = m.group(3);
////				String percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = m.group(4);
////				String avgPercentIdentityWithCDSsConservedDuringEvolution = m.group(5);
////				String avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = m.group(6);
////				String medianEvalueAlignmentsWithCDSsConservedDuringEvolution = m.group(7);
////				if (numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution != null && ! numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution.isEmpty() ) {
////					alToReturn.add(numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution);
////				} else {
////					throw new Exception(
////							"NOT numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution != null && ! numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution.isEmpty()"
////									+ " ; numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = "+numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution
////									+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
////									+ " ; methodToReport="+methodToReport);
////				}
////				if (percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution != null && ! percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution.isEmpty() ) {
////					alToReturn.add(percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution);
////				} else {
////					throw new Exception(
////							"NOT percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution != null && ! percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution.isEmpty()"
////									+ " ; percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = "+percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution
////									+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
////									+ " ; methodToReport="+methodToReport);
////				}
////				if (numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship != null && ! numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship.isEmpty() ) {
////					alToReturn.add(numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship);
////				} else {
////					throw new Exception(
////							"NOT numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship != null && ! numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship.isEmpty()"
////									+ " ; numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship = "+numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship
////									+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
////									+ " ; methodToReport="+methodToReport);
////				}
////				if (percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship != null && ! percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship.isEmpty() ) {
////					alToReturn.add(percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship);
////				} else {
////					throw new Exception(
////							"NOT percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship != null && ! percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship.isEmpty()"
////									+ " ; percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = "+percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
////									+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
////									+ " ; methodToReport="+methodToReport);
////				}
////				if (avgPercentIdentityWithCDSsConservedDuringEvolution != null && ! avgPercentIdentityWithCDSsConservedDuringEvolution.isEmpty() ) {
////					alToReturn.add(avgPercentIdentityWithCDSsConservedDuringEvolution);
////				} else {
////					throw new Exception(
////							"NOT avgPercentIdentityWithCDSsConservedDuringEvolution != null && ! avgPercentIdentityWithCDSsConservedDuringEvolution.isEmpty()"
////									+ " ; avgPercentIdentityWithCDSsConservedDuringEvolution = "+avgPercentIdentityWithCDSsConservedDuringEvolution
////									+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
////									+ " ; methodToReport="+methodToReport);
////				}
////				if (avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution != null && ! avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution.isEmpty() ) {
////					alToReturn.add(avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution);
////				} else {
////					throw new Exception(
////							"NOT avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution != null && ! avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution.isEmpty()"
////									+ " ; avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = "+avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
////									+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
////									+ " ; methodToReport="+methodToReport);
////				}
////				if (medianEvalueAlignmentsWithCDSsConservedDuringEvolution != null && ! medianEvalueAlignmentsWithCDSsConservedDuringEvolution.isEmpty() ) {
////					alToReturn.add(medianEvalueAlignmentsWithCDSsConservedDuringEvolution);
////				} else {
////					throw new Exception(
////							"NOT medianEvalueAlignmentsWithCDSsConservedDuringEvolution != null && ! medianEvalueAlignmentsWithCDSsConservedDuringEvolution.isEmpty()"
////									+ " ; medianEvalueAlignmentsWithCDSsConservedDuringEvolution = "+medianEvalueAlignmentsWithCDSsConservedDuringEvolution
////									+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
////									+ " ; methodToReport="+methodToReport);
////				}
////			} else {
////				throw new Exception(
////						"no match in rawOutputStatSummaryRefCDS for the expected regulr expression"
////								+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
////								+ " ; methodToReport="+methodToReport);
////			}
//		}
//		
//		
//
//		if (alToReturn.size() != 7) {
//			throw new Exception(
//					"alToReturn.size() != 7"
//							+ " ; alToReturn="+alToReturn.toString()
//							+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
//							+ " ; methodToReport="+methodToReport);
//		}
//		return alToReturn;
//
//	}

	private static void deleteFileIfExists(String outputFileToDelete_asString) throws IOException {
		File outputFile_asJavaIoFile = new File(outputFileToDelete_asString);
		if(outputFile_asJavaIoFile.exists()){
			outputFile_asJavaIoFile.delete();
			outputFile_asJavaIoFile.createNewFile();
		}
	}


}
