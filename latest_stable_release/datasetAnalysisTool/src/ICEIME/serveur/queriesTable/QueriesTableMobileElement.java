package ICEIME.serveur.queriesTable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import ICEIME.serveur.DatabaseConfICEDB;
import ICEIME.serveur.pojos.databaseMapping.LightICEIMEItem;
import fr.inra.jouy.server.UtilitiesMethodsServer;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

public class QueriesTableMobileElement {

	
	public static ArrayList<LightICEIMEItem> getAllLightICEIME(
			Connection conn
			) throws Exception {
	

		String methodNameToReport = "QueriesTableMobileElement getAllLightICEIME"
				;
		
		//args to check
		//no
		//mandatory args
		
		ArrayList<LightICEIMEItem> alToReturn = new ArrayList<>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConfICEDB.getConnection();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT * FROM mobile_element;";

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				LightICEIMEItem liiIT = new LightICEIMEItem();
				liiIT.setId_mobile_element(rs.getInt("id_mobile_element"));
				liiIT.setId_element(rs.getInt("id_element"));
				liiIT.setDynamic_name(rs.getString("dynamic_name"));
				//rs.getString("published_name");
				//rs.getString("published_name_reference");
				//rs.getString("other_name");
				liiIT.setType(rs.getString("type"));
				liiIT.setStatus(rs.getString("status"));
				liiIT.setType_comment(rs.getString("type_comment"));
				liiIT.setValidation(rs.getString("validation"));
				liiIT.setIntegrase_type(rs.getString("integrase_type"));
				liiIT.setPosmin(rs.getInt("posmin"));
				liiIT.setPosmax(rs.getInt("posmax"));
				liiIT.setDr_target_length(rs.getInt("dr_target_length"));
				liiIT.setDr_opposite_length(rs.getInt("dr_opposite_length"));
				liiIT.setIr_target_length(rs.getInt("ir_target_length"));
				liiIT.setIr_opposite_length(rs.getInt("ir_opposite_length"));
				liiIT.setTarget_name(rs.getString("target_name"));
				liiIT.setIntegrase_location(rs.getString("integrase_location"));
				liiIT.setIntegrase_orientation(rs.getString("integrase_orientation"));
				liiIT.setTarget_position(rs.getString("target_position"));
				liiIT.setTarget_insertion(rs.getString("target_insertion"));
				liiIT.setInsertion_specificity(rs.getString("insertion_specificity"));
				liiIT.setTarget_type(rs.getString("target_type"));
				liiIT.setTarget_comment(rs.getString("target_comment"));
				liiIT.setTarget_validation(rs.getString("target_validation"));
				liiIT.setIce_family(rs.getString("ice_family"));
				liiIT.setIme_mob_domain(rs.getString("ime_mob_domain"));
				liiIT.setIme_cp_domain(rs.getString("ime_cp_domain"));
				liiIT.setAccretion(rs.getString("accretion"));
				liiIT.setInsertion(rs.getString("insertion"));
				//rs.getString("comments_GG");
				//rs.getString("comments_CC");
				//rs.getString("comments_limits");
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConfICEDB.freeConnection(conn, methodNameToReport);
			}
		}// try	
		
		return alToReturn;
	}
}
