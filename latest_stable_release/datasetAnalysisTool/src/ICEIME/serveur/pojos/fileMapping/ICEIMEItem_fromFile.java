package ICEIME.serveur.pojos.fileMapping;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
 */
public class ICEIMEItem_fromFile {

	public enum IntegraseType { 
		Tyr
		,Ser
		,DDE
		,UNKNOWN
		,UNSURE
		;
	}
	
	public enum Category_of_elementType { 
		ICE
		,ICE_derivate
		,IME
		,IME_derivate
		,UNSURE
		;
	}
	
	public enum ICEIMEAccretion { 
		no
		,yes
		,unsure
		;
	}
	
	public enum ICEIMEInsertion { 
		no
		,host
		,guest
		,host_and_guest
		,unsure
		;
	}
	
	private String elementName = ""; //column Element name
	private String genomeId = "";//column Genome_id
	private Category_of_elementType elementType = null;//column element type
	private IntegraseType integraseType = null;//column integrase type
	private String integraseLocation = "";//column integrase location (compared to target)
	private String integraseOrientation = "";//column integrase orientation (compared to target)
	private String family = "";//column Family
	private int leftEnd = -1;//column Left end
	private int rightEnd = -1;//column Right end
	private ICEIMEAccretion accretion = null;//column Accretion
	private ICEIMEInsertion insertion = null;//column Insertion
	private String autreRemarquePrecisionGG = "";//column Autre remarque/ précision (GG)
	private String remarquesDefautArtemis = "";//column Remarques/ défaut Artemis
	private String integraseEmblCds = "";//column Integrase_embl_cds // can be locus tag, protein id or gene location
	private String integraseReference = "";//column integrase_reference // can be locus tag, protein id or gene location
	private String relaxaseEmblCds = "";//column Relaxase_embl_cds // can be locus tag, protein id or gene location
	private String relaxaseReference = "";//column relaxase_reference // can be locus tag, protein id or gene location
	private String couplingProteinEmblCds = "";//column Coupling_protein_embl_cds // can be locus tag, protein id or gene location
	private String couplingProteinReference = "";//column coupling_protein_reference // can be locus tag, protein id or gene location
	private String VirB4Cds = "";//column VirB4_cds // can be locus tag, protein id or gene location
	private String virb4_reference = "";//virb4_reference // can be locus tag, protein id or gene location
	private String insertionGene = "";//column Insertion gene // can be locus tag, protein id or gene location
	private String insertionGeneLocusTag = "";//column insertion_gene_locus_tag // can be locus tag, protein id or gene location
	private String insertionGeneCds = "";//column insertion_gene_cds // can be locus tag, protein id or gene location
	private String insertionGeneReference = "";//column insertion_gene_reference // can be locus tag, protein id or gene location
	private String insertionGeneFunction = "";//column insertion_gene_function

	public ICEIMEItem_fromFile(){
		
	}

	public String getElementName() {
		return elementName;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName;
	}

	public String getGenomeId() {
		return genomeId;
	}

	public void setGenomeId(String genomeId) {
		this.genomeId = genomeId;
	}

	public Category_of_elementType getElementType() {
		return elementType;
	}

	public void setElementType(Category_of_elementType elementType) {
		this.elementType = elementType;
	}

	public IntegraseType getIntegraseType() {
		return integraseType;
	}

	public void setIntegraseType(IntegraseType integraseType) {
		this.integraseType = integraseType;
	}

	public String getIntegraseLocation() {
		return integraseLocation;
	}

	public void setIntegraseLocation(String integraseLocation) {
		this.integraseLocation = integraseLocation;
	}

	public String getIntegraseOrientation() {
		return integraseOrientation;
	}

	public void setIntegraseOrientation(String integraseOrientation) {
		this.integraseOrientation = integraseOrientation;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public int getLeftEnd() {
		return leftEnd;
	}

	public void setLeftEnd(int leftEnd) {
		this.leftEnd = leftEnd;
	}

	public int getRightEnd() {
		return rightEnd;
	}

	public void setRightEnd(int rightEnd) {
		this.rightEnd = rightEnd;
	}

	public ICEIMEAccretion getAccretion() {
		return accretion;
	}

	public void setAccretion(ICEIMEAccretion accretion) {
		this.accretion = accretion;
	}

	public ICEIMEInsertion getInsertion() {
		return insertion;
	}

	public void setInsertion(ICEIMEInsertion insertion) {
		this.insertion = insertion;
	}

	public String getAutreRemarquePrecisionGG() {
		return autreRemarquePrecisionGG;
	}

	public void setAutreRemarquePrecisionGG(String autreRemarquePrecisionGG) {
		this.autreRemarquePrecisionGG = autreRemarquePrecisionGG;
	}

	public String getRemarquesDefautArtemis() {
		return remarquesDefautArtemis;
	}

	public void setRemarquesDefautArtemis(String remarquesDefautArtemis) {
		this.remarquesDefautArtemis = remarquesDefautArtemis;
	}

	public String getIntegraseEmblCds() {
		return integraseEmblCds;
	}

	public void setIntegraseEmblCds(String integraseEmblCds) {
		this.integraseEmblCds = integraseEmblCds;
	}

	public String getIntegraseReference() {
		return integraseReference;
	}

	public void setIntegraseReference(String integraseReference) {
		this.integraseReference = integraseReference;
	}

	public String getRelaxaseEmblCds() {
		return relaxaseEmblCds;
	}

	public void setRelaxaseEmblCds(String relaxaseEmblCds) {
		this.relaxaseEmblCds = relaxaseEmblCds;
	}

	public String getRelaxaseReference() {
		return relaxaseReference;
	}

	public void setRelaxaseReference(String relaxaseReference) {
		this.relaxaseReference = relaxaseReference;
	}

	public String getCouplingProteinEmblCds() {
		return couplingProteinEmblCds;
	}

	public void setCouplingProteinEmblCds(String couplingProteinEmblCds) {
		this.couplingProteinEmblCds = couplingProteinEmblCds;
	}

	public String getCouplingProteinReference() {
		return couplingProteinReference;
	}

	public void setCouplingProteinReference(String couplingProteinReference) {
		this.couplingProteinReference = couplingProteinReference;
	}

	public String getVirB4Cds() {
		return VirB4Cds;
	}

	public void setVirB4Cds(String virB4Cds) {
		VirB4Cds = virB4Cds;
	}

	public String getVirb4_reference() {
		return virb4_reference;
	}

	public void setVirb4_reference(String virb4_reference) {
		this.virb4_reference = virb4_reference;
	}

	public String getInsertionGene() {
		return insertionGene;
	}

	public void setInsertionGene(String insertionGene) {
		this.insertionGene = insertionGene;
	}

	public String getInsertionGeneLocusTag() {
		return insertionGeneLocusTag;
	}

	public void setInsertionGeneLocusTag(String insertionGeneLocusTag) {
		this.insertionGeneLocusTag = insertionGeneLocusTag;
	}

	public String getInsertionGeneCds() {
		return insertionGeneCds;
	}

	public void setInsertionGeneCds(String insertionGeneCds) {
		this.insertionGeneCds = insertionGeneCds;
	}

	public String getInsertionGeneReference() {
		return insertionGeneReference;
	}

	public void setInsertionGeneReference(String insertionGeneReference) {
		this.insertionGeneReference = insertionGeneReference;
	}

	public String getInsertionGeneFunction() {
		return insertionGeneFunction;
	}

	public void setInsertionGeneFunction(String insertionGeneFunction) {
		this.insertionGeneFunction = insertionGeneFunction;
	}

	
}
