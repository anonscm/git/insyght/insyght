package ICEIME.serveur.pojos.databaseMapping;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
 */

public class LightICEIMEItem {

	private int id_mobile_element = -1;
	private int id_element = -1;
	private String dynamic_name = "";
	//private String published_name = "";
	//private String published_name_reference = "";
	//private String other_name = "";
	private String type = ""; // putative ICE, MGI, CIME, putative IME, ICE remnant, etc...
	private String status = ""; // whole / derivated / remnant / None
	private String type_comment = "";
	private String validation = ""; // uncertain / validated
	private String integrase_type = ""; // integrase type : Tyr, or Ser
	private int posmin = -1;
	private int posmax = -1;
	private int dr_target_length = -1;
	private int dr_opposite_length = -1;
	private int ir_target_length = -1;
	private int ir_opposite_length = -1;
	private String target_name = "";
	private String integrase_location = "";//compared to target : opposite end, adjacent, 1 gene encoding a protein, ...
	private String integrase_orientation = "";//compared to target : identical, complement, ...
	private String target_position = ""; // 3' / 5' / W10 / None
	private String target_insertion = ""; // in / out
	private String insertion_specificity = ""; // S / NS / ND
	private String target_type = ""; // gene / tRNA / rRNA / protein_family
	private String target_comment = ""; // Famille de la protéine par ex
	private String target_validation = ""; // uncertain / validated
	private String ice_family = "";
	private String ime_mob_domain = "";
	private String ime_cp_domain = "";
	private String accretion = "";
	private String insertion = "";
	//	   comments_GG TEXT,
	//	   comments_CC TEXT,
	//	   comments_limits TEXT

	public LightICEIMEItem() {
		super();
	}

	public int getId_mobile_element() {
		return id_mobile_element;
	}

	public void setId_mobile_element(int id_mobile_element) {
		this.id_mobile_element = id_mobile_element;
	}
	public int getId_element() {
		return id_element;
	}
	public void setId_element(int id_element) {
		this.id_element = id_element;
	}
	public String getDynamic_name() {
		return dynamic_name;
	}
	public void setDynamic_name(String dynamic_name) {
		this.dynamic_name = dynamic_name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType_comment() {
		return type_comment;
	}
	public void setType_comment(String type_comment) {
		this.type_comment = type_comment;
	}
	public String getValidation() {
		return validation;
	}
	public void setValidation(String validation) {
		this.validation = validation;
	}
	public String getIntegrase_type() {
		return integrase_type;
	}
	public void setIntegrase_type(String integrase_type) {
		this.integrase_type = integrase_type;
	}
	public int getPosmin() {
		return posmin;
	}
	public void setPosmin(int posmin) {
		this.posmin = posmin;
	}
	public int getPosmax() {
		return posmax;
	}
	public void setPosmax(int posmax) {
		this.posmax = posmax;
	}
	public int getDr_target_length() {
		return dr_target_length;
	}
	public void setDr_target_length(int dr_target_length) {
		this.dr_target_length = dr_target_length;
	}
	public int getDr_opposite_length() {
		return dr_opposite_length;
	}
	public void setDr_opposite_length(int dr_opposite_length) {
		this.dr_opposite_length = dr_opposite_length;
	}
	public int getIr_target_length() {
		return ir_target_length;
	}
	public void setIr_target_length(int ir_target_length) {
		this.ir_target_length = ir_target_length;
	}
	public int getIr_opposite_length() {
		return ir_opposite_length;
	}
	public void setIr_opposite_length(int ir_opposite_length) {
		this.ir_opposite_length = ir_opposite_length;
	}
	public String getTarget_name() {
		return target_name;
	}
	public void setTarget_name(String target_name) {
		this.target_name = target_name;
	}
	public String getIntegrase_location() {
		return integrase_location;
	}
	public void setIntegrase_location(String integrase_location) {
		this.integrase_location = integrase_location;
	}
	public String getIntegrase_orientation() {
		return integrase_orientation;
	}
	public void setIntegrase_orientation(String integrase_orientation) {
		this.integrase_orientation = integrase_orientation;
	}
	public String getTarget_position() {
		return target_position;
	}
	public void setTarget_position(String target_position) {
		this.target_position = target_position;
	}
	public String getTarget_insertion() {
		return target_insertion;
	}
	public void setTarget_insertion(String target_insertion) {
		this.target_insertion = target_insertion;
	}
	public String getInsertion_specificity() {
		return insertion_specificity;
	}
	public void setInsertion_specificity(String insertion_specificity) {
		this.insertion_specificity = insertion_specificity;
	}
	public String getTarget_type() {
		return target_type;
	}
	public void setTarget_type(String target_type) {
		this.target_type = target_type;
	}
	public String getTarget_comment() {
		return target_comment;
	}
	public void setTarget_comment(String target_comment) {
		this.target_comment = target_comment;
	}
	public String getTarget_validation() {
		return target_validation;
	}
	public void setTarget_validation(String target_validation) {
		this.target_validation = target_validation;
	}
	public String getIce_family() {
		return ice_family;
	}
	public void setIce_family(String ice_family) {
		this.ice_family = ice_family;
	}
	public String getIme_mob_domain() {
		return ime_mob_domain;
	}
	public void setIme_mob_domain(String ime_mob_domain) {
		this.ime_mob_domain = ime_mob_domain;
	}
	public String getIme_cp_domain() {
		return ime_cp_domain;
	}
	public void setIme_cp_domain(String ime_cp_domain) {
		this.ime_cp_domain = ime_cp_domain;
	}
	public String getAccretion() {
		return accretion;
	}
	public void setAccretion(String accretion) {
		this.accretion = accretion;
	}
	public String getInsertion() {
		return insertion;
	}
	public void setInsertion(String insertion) {
		this.insertion = insertion;
	}

}
