#include "OrthologRanker.h"

void
OrthologRanker::transform(float **scores, const int cols, const int rows) {

  for (int i=0;i<cols;i++) {
    float max_score=scores[i][0];
    for (int j=1;j<rows;j++) {
      if (max_score<scores[i][j]) {
	max_score=scores[i][j];
      }
    }
    for (int j=1;j<rows;j++) {
      scores[i][j]=scores[i][j]/max_score;
    }
  }
}
