#ifndef _ALIGNMENTFACTORY_H_
#define _ALIGNMENTFACTORY_H_

#include <set>
#include <map>
using namespace std;

#include "Alignment.h"
#include "GeneList.h"
//#include "GeneList_mod.h"
#include "HomologyMatrix.h"

typedef std::map <int, std::set<int> > mapInt2SetInt;

class AlignmentFactory {
public:
  AlignmentFactory(GeneList *, GeneList *, HomologyMatrix *);
  void set_query_gene_list(GeneList *);
  void set_subject_gene_list(GeneList *);
  void set_homology_matrix(HomologyMatrix *);
  void set_ortholog_score(float);

  void set_gap_creation_penalty(const float);
  void set_max_gap_size_for_creation_penalty(const float);
  void set_gap_extension_penalty(const float);
  void set_min_score(const float);
  void set_min_align_size(const int);
  void set_include_all_orthologs(const bool);
  void set_print_out_tandem_dups(const bool);

  inline float get_gap_creation_penalty() const 
    { return _gap_creation_penalty; };
  inline float get_max_gap_size_for_creation_penalty() const
    { return _max_gap_size_for_creation_penalty; };
  inline float get_gap_extension_penalty() const
    { return _gap_extension_penalty; };
  inline float get_min_score() const
    { return _min_score; };
  inline int get_min_align_size() const
    { return _min_align_size; };
  inline bool get_include_all_orthologs() const
    { return _include_all_orthologs; };

  multiset<Alignment> &get_alignments();

  static const int GAP_STARTS;
  static const int GAP_CONTINUES;
  mapInt2SetInt _col_tandem_dups_idxGeneList1ToVecIdxGeneList2;
  mapInt2SetInt _row_tandem_dups_idxGeneList2ToVecIdxGeneList1;

private:
  static const float DEFAULT_GAP_CREATION_PENALTY;
  static const float DEFAULT_MAX_GAP_SIZE_CREATION_PENALTY;
  static const float DEFAULT_GAP_EXTENSION_PENALTY;
  static const float DEFAULT_MIN_SCORE;
  static const int DEFAULT_MIN_ALIGN_SIZE;
  GeneList *_query_gene_list;
  GeneList *_subject_gene_list;
  HomologyMatrix *_homology_matrix;

  float _ortholog_score;

  float _gap_creation_penalty;
  float _max_gap_size_for_creation_penalty;
  float _gap_extension_penalty;
  float _min_score;
  int _min_align_size;
  bool _include_all_orthologs;
  bool _print_out_tandem_dups;

  bool _up_to_date;
  multiset<Alignment> _alignment_set;



  string build_backward_alignment(
		  vector<int> &
		  , vector<int> &
		  , vector<float> &
		  , float **
		  , short **
		  , int
		  , int
		  , int
		  , int
		  , bool isHighestScoreInSyteny
		  );
  float trim_and_recalculate_alignment(
		  	  	 vector<int> &
				 , vector<int> &
				 , vector<float> &
				//float **
				//, short **
				//int, int
				, int, int
				//, float max_score
				);
  void buildAndStoreAlignmentObj(
				float //max_score
				, vector<float> &//scores
				, int //direction
				, vector<int> &//align1
				, vector<int> &//align2
				, int //listsize2
				, float //max_matrix
				, map<int, int>  &//ortholog_map
				, short **//trackmat
				, string collidedWithForbiddenCellOnBacktrack
  	  	  	 	);
  void build_ortholog_map(map<int,int> &,
			  GeneList *, GeneList *,
			  HomologyMatrix *);

};

#endif
