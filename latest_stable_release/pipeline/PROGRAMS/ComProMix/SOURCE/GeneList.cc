#include <cassert>
#include <cstdlib>
//#include <ext/hash_map>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>     /* exit, EXIT_FAILURE */
using namespace std;


#include "GeneList.h"

int 
GeneList::gene_index(const int gene_id_sent) {
  assert (_index_by_gene_id.count(gene_id_sent)==1);
  return _index_by_gene_id[gene_id_sent];
}

int
GeneList::organism_id() {
  return _organism_id;
}

int
GeneList::element_id(const int gene_index_sent) {
  return _gene_info_by_index[gene_index_sent]._element_id;
}

int
GeneList::length(const int gene_index_sent) {
  return _gene_info_by_index[gene_index_sent]._length;
}


string  
GeneList::gene_name(const int gene_index_sent) {
  return _gene_info_by_index[gene_index_sent]._gene_name;
}

int
GeneList::gene_id(const int gene_index_sent) {
  return _gene_info_by_index[gene_index_sent]._gene_id;
}

int
GeneList::start(const int gene_index_sent)  {
  return _gene_info_by_index[gene_index_sent]._start;
}

int
GeneList::stop(const int gene_index_sent)  {
  return _gene_info_by_index[gene_index_sent]._stop;
}

int
GeneList::size() const {
  return _size;
}

void GeneList::serialize(string filename){

  ofstream ofs(filename.c_str());
  if(!ofs.is_open()) {
    cerr << "unable to open file " << filename << " for writing\n";
    exit(EXIT_FAILURE);
  }

  ofs << "serialisation::archive" << endl;
  ofs << _organism_id << " " << _size << endl;

  for (int gene_index_counter=0; gene_index_counter<_size; gene_index_counter++) {
    ofs << _gene_info_by_index[gene_index_counter]._gene_index << " "
	<< _gene_info_by_index[gene_index_counter]._gene_id << " "
	<< _gene_info_by_index[gene_index_counter]._gene_name << " "
	<< _gene_info_by_index[gene_index_counter]._element_id << " "
	<< _gene_info_by_index[gene_index_counter]._start << " "
	<< _gene_info_by_index[gene_index_counter]._stop << " "
	<< _gene_info_by_index[gene_index_counter]._length << endl;
  }
  ofs.close();
}

GeneList::GeneList() {
  // empty but needed for inheriting class GeneListPSQL
}

GeneList::GeneList(const int organism_id_sent, const string archiveDir) {
  
  int organism_id_tmp;
  string line;
  //int Nel;
  //int fld1, fld2;
  string fld;
  //GeneInfo info;
  stringstream ss;

  _organism_id = organism_id_sent;
  char filename[2048];
  sprintf(filename,"%s/%d.archive",archiveDir.c_str(),organism_id_sent);
  ifstream ifs(filename);

  if(!ifs.is_open()) {
    cerr << "Error: unable to open file " << filename << " for reading\n";
    exit(EXIT_FAILURE);
  }

  getline(ifs,line);
  if(line.compare("serialisation::archive") != 0) {
    cerr << "Error: file: " << filename << " does not appear to be an archive file" << endl;
    exit(EXIT_FAILURE);
  }

  getline(ifs,line);
  ss.str(line);
  ss >> organism_id_tmp >> _size;
  if(organism_id_tmp != organism_id_sent) {
    cerr << "Error: in file: " << filename << ": organism_id  is: " << organism_id_tmp << " whereas the function was expecting organism_id: " << organism_id_sent << endl;
    exit(EXIT_FAILURE);
  }
  ss.clear();

  _gene_info_by_index = new GeneInfo [_size];
  for(int i = 1; i <= _size; i++) {
    getline(ifs,line);
    ss.str(line);

    GeneInfo info;
    ss >> info._gene_index >> info._gene_id >> info._gene_name >> info._element_id >> info._start >> info._stop 
       >> info._length >> info._element_id;
    ss.clear();

    _gene_info_by_index[info._gene_index]=info;
    _index_by_gene_id[info._gene_id]=info._gene_index;
  }
}

GeneList::~GeneList() {
}
