#ifndef _ORTHOLOGRANKER_H_
#define _ORTHOLOGRANKER_H_

#include "ScoreInverter.h"

class OrthologRanker : public ScoreInverter {
public:
  OrthologRanker() : ScoreInverter() {};
  virtual ~OrthologRanker() {};
  virtual void transform(float **, const int, const int);
};

#endif

