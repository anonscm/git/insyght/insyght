#ifndef _SCORENORMALIZER_H_
#define _SCORENORMALIZER_H_

#include "ScoreTransformer.h"

class ScoreNormalizer : public ScoreTransformer {
public:
  ScoreNormalizer(const float os, const float hs, const float us) : 
	ScoreTransformer(),
    _ortholog_score(os), _homolog_score(hs), _unrelated_score(us)
	 {} ;
  virtual ~ScoreNormalizer() {};
  virtual void transform(float **, const int, const int);
private:
  float _ortholog_score;
  float _homolog_score;
  float _unrelated_score;
};

#endif
