#ifndef _GENELIST_H_
#define _GENELIST_H_

//#include <ext/hash_map>
#include <map>
#include <string>
#include <vector>
using namespace std;
using namespace __gnu_cxx;

struct _GeneInfo {
  int _gene_index;
  int _gene_id;
  string _gene_name;
  int _element_id;
  int _start;
  int _stop;
  int _length;
};

typedef struct _GeneInfo GeneInfo;


class GeneList {
public:
  GeneList();
  GeneList(const int organism_id, const string archiveDir);

  virtual ~GeneList();

  string gene_name(const int gene_index);
  int gene_id(const int gene_index);
  int start(const int gene_index);
  int stop(const int gene_index);
  int length(const int gene_index);
  int element_id(const int gene_index);

  int organism_id();
  int size() const;

  void serialize(string filename);
  int gene_index(const int gene_id);

  friend class RegionFilter;
  
protected:
  int _size;                 // number of genes in _element_id
  int _offset;               // number of the first gene in _element_id
  int _organism_id;
  GeneInfo* _gene_info_by_index;
  map<int,int> _index_by_gene_id;

};

#endif

