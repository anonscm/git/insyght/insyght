#ifndef _HOMOLOGYMATRIX_H_
#define _HOMOLOGYMATRIX_H_

#include "ScoreTransformer.h"
#include "GeneList.h"
#include "GeneFusionCheckerObj.h"
//#include "GeneList_mod.h"
#include <string>
#include <set>
#include <unordered_map>
using namespace std;

struct _BlastResults {
  float pid;       // percentage identity in alignment
  int hsp_len;     // alignment length
  int mismatches;  // number of mismatches
  int gaps;        // number of gaps
  int qstart;      // start of alignment in query protein
  int qend;        // end of alignment in query protein
  int qlength;     // query protein length (in aa)
  int hstart;      // start of alignment in target protein
  int hend;       // end of alignment in target protein
  int hlength;     // target protein length (in aa)
  float evalue;    // BLAST e-value
  float bits;      // alignment score in bits
  int bdbh;        // if bdbh == 2 the protein pair corresponds to a bdbh 
  int rank;        // rank == 1 if target protein comes first in the list of homologs else set to 2
};

typedef struct _BlastResults BlastRes;
typedef map<int,BlastRes> innerMap;
typedef map<int,innerMap> Map3D;

typedef std::multiset<GeneFusionCheckerObj> SetGeneFusion;
typedef std::unordered_map<int, SetGeneFusion> MapIdx2setGeneFusionCheckerObj;

class HomologyMatrix {
public:
  enum { ORTHOLOGS = 1, HOMOLOGS = 2, UNRELATED = 3};
  HomologyMatrix(GeneList & glist1, GeneList & glist2, std::string blast_output, float prot_frac, ScoreTransformer * =0);//, bool print_out_gene_fusions
  ~HomologyMatrix();
  inline int cols() const { return _size_1;};
  inline int rows() const { return _size_2;};
  inline float min() const {return _min;};
  inline float max() const {return _max;};
  inline BlastRes blast_res(int q_gene_index, int h_gene_index){return _gene_pairs[q_gene_index][h_gene_index];};
  float score(const int, const int) const;
  MapIdx2setGeneFusionCheckerObj idxI2VectHomologsIdxJ; //this map is used to find gene fusion taking genes from orga1 as ref (genelist1, idx i)
  MapIdx2setGeneFusionCheckerObj idxJ2VectHomologsIdxI; //this map is used to find gene fusion taking genes from orga2 as ref (genelist2, idx j)
private:
  int _size_1,_size_2;
  float **_scores;
  float _min;
  float _max;
  Map3D _gene_pairs; // this map contains for each query-target pair the results of BLAST (see _BlastResults above): < q_gene_id, h_gene_id, br >
};

#endif
