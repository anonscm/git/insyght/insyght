#ifndef _SCORETRANSFORMER_H_
#define _SCORETRANSFORMER_H_

class ScoreTransformer {
public:
  virtual void transform(float **, const int, const int) =0;
protected:
  ScoreTransformer() {};
};

#endif
