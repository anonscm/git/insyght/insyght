#ifndef _GLOBALPARAMS_H_
#define _GLOBALPARAMS_H_

class GlobalParams {

public:
	static bool print_out_gene_fusions;
	static bool print_out_close_best_match;
	static float geneFusions_minPercentLenghtProtThreshold;
	static float closeBestMatchs_minPercentThreshold;
	static float tandemDups__minPercentWithinBestPidThreshold;
private:
};

#endif


