#ifndef _SCOREINVERTER_H_
#define _SCOREINVERTER_H_

#include "ScoreTransformer.h"

class ScoreInverter : public ScoreTransformer {
public:
  ScoreInverter() : ScoreTransformer() {};
  virtual ~ScoreInverter() {};
  virtual void transform(float **, const int, const int);
};

#endif

