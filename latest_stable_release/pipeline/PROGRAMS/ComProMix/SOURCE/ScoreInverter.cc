#include <iostream>
using namespace std;

#include "ScoreInverter.h"


void
ScoreInverter::transform(float **scores, const int cols, const int rows) {

  for (int i=0;i<cols;i++)
    for (int j=0;j<rows;j++) {
      if (scores[i][j]==1.0)
	scores[i][j]=2.0;
      else if (scores[i][j]>0.0 && scores[i][j]<1)
	scores[i][j]=1.0;
      else 
	scores[i][j]=-2.0;
    }
}
