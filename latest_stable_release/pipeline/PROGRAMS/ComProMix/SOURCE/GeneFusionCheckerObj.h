#ifndef _GENEFUSIONCHECKEROBJ_H_
#define _GENEFUSIONCHECKEROBJ_H_

class GeneFusionCheckerObj {

public:
	inline int idx() const
	{ return  _idx; };
	inline int alignment_size_on_query() const
	{ return _alignment_size_on_query; };
	inline float bits() const
	{ return _bits; };
	friend int operator<(const GeneFusionCheckerObj &a, const GeneFusionCheckerObj &b) {
		return (a._bits > b._bits); // Descending order
	};
	int _idx;
	int _alignment_size_on_query;
	float _bits;

private:

};

#endif
