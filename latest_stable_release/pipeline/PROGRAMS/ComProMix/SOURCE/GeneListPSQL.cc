#include <cassert>
#include <cstdlib>
//#include <ext/hash_map>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>     /* exit, EXIT_FAILURE */
#include "GeneListPSQL.h"
#include "Common.h"

using namespace std;



GeneListPSQL::GeneListPSQL(const int organism_id_sent) {

  _organism_id=organism_id_sent;
    
  PGconn *connection=PQconnectdb(connectionstring.data());
  
  if (PQstatus(connection) == CONNECTION_BAD) {
    cerr << "Unable to connect to database : " << 
      connectionstring.data() << ", " <<
      PQerrorMessage(connection) << endl;
    exit(1);
  }

  char querybuffer[1024];

  //
  // Query database to retrieve all genes
  //
  // OLD COMPATIBILTY : length(residues) instead of length_residues
  sprintf(querybuffer,"SELECT gene_id, name, element_id, start, stop, length_residues "
	  "FROM genes "
	  "WHERE organism_id = %d "
	  "ORDER BY element_id, start",organism_id_sent);
  
  PGresult *result=PQexec(connection,querybuffer);
  if (PQresultStatus(result) != PGRES_TUPLES_OK) {
    cerr << "Query (" << querybuffer <<") failed." << endl;
    exit(1);
  }

  _size = PQntuples(result);
  _offset = std::stoi(PQgetvalue(result,0,0));

  _gene_info_by_index = new GeneInfo [PQntuples(result)];
  for (int i=0; i<PQntuples(result); i++) {
    GeneInfo info;
    info._gene_name=string(PQgetvalue(result,i,0));
    info._gene_id=std::stoi(PQgetvalue(result,i,0));
    info._gene_index=i; 
    info._start=std::stoi(PQgetvalue(result,i,3));
    info._stop=std::stoi(PQgetvalue(result,i,4));
    info._element_id=std::stoi(PQgetvalue(result,i,2));
    info._length=std::stoi(PQgetvalue(result,i,5));
    info._gene_index=i;
    _gene_info_by_index[i]=info;
    _index_by_gene_id[info._gene_id]=info._gene_index;
  }

  PQclear(result);
  PQfinish(connection);

}
