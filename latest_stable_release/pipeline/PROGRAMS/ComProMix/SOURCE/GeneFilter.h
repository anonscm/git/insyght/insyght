#ifndef _GENEFILTER_H_
#define _GENEFILTER_H_

#include "GeneList.h"
//#include "GeneList_mod.h"

class GeneFilter {
 public:
  virtual void filter(GeneList *) = 0;
};

#endif
