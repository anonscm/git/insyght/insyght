#ifndef _ALIGNMENT_H_
#define _ALIGNMENT_H_

#include "AlignedSequence.h"

class Alignment {
  enum Orientation { CONSERVED, REVERSED };
public:
  inline float score() const
    { return  _score; };
  inline int size() const
    { return _scores.size(); };
  inline int orthologs() const
    { return _orthologs; };
  inline int homologs() const
    { return _homologs; };
  inline Orientation orientation() const
    { return _orientation; };
  inline AlignedSequence query() const
    { return _query; };
  inline AlignedSequence subject() const
    { return _subject; };
  inline string query_name(const int i) const
    { return _query._gene_names[i]; }
  inline string subject_name(const int i) const 
    { return _subject._gene_names[i]; }
  inline int query_gene_id(const int i) const
    { return _query._gene_ids[i]; }
  inline int subject_gene_id(const int i) const 
    { return _subject._gene_ids[i]; }
  inline float alignment_score(const int i) const
    { return _scores[i]; };
  inline float homology_score(const int i) const
    { return _homologies[i]; };
  inline int query_start(const int i) const
    { return _query._start_points[i]; };
  inline int query_end(const int i) const
    { return _query._end_points[i]; };
  inline int subject_start(const int i) const
    { return _subject._start_points[i]; };
  inline int subject_end(const int i) const
    { return _subject._end_points[i]; };
  inline string isBranchedToAnotherSynteny() const
    { return _isBranchedToAnotherSynteny; };

  friend int operator<(const Alignment &a, const Alignment &b) {
    return (a._score < b._score);
  };

  friend class AlignmentFactory;
private:
  AlignedSequence _query;
  AlignedSequence _subject;
  float _score;
  int _orthologs;
  int _homologs;
  Orientation _orientation;
  vector<float> _scores;
  vector<float> _homologies;
  string _isBranchedToAnotherSynteny;
};

#endif
