#include <cstdio>
#include <iostream>
#include <cstring>
#include <string>
#include <vector>
using namespace std;


#include "AlignmentFactory.h"
#include "CellWithScoreGreaterThanCutoff.h"
#include "GlobalParams.h"

const float AlignmentFactory::DEFAULT_GAP_CREATION_PENALTY=-4;
const float AlignmentFactory::DEFAULT_MAX_GAP_SIZE_CREATION_PENALTY=1;
const float AlignmentFactory::DEFAULT_GAP_EXTENSION_PENALTY=-1;
const float AlignmentFactory::DEFAULT_MIN_SCORE=1;
const int AlignmentFactory::DEFAULT_MIN_ALIGN_SIZE=1;
const int AlignmentFactory::GAP_STARTS=-1.03333;
const int AlignmentFactory::GAP_CONTINUES=-2.03333;

const int UP  =1;
const int LEFT=2;
const int DIAG=3;
const int FORBIDDEN=4;
const int INITALN=-1;
const int NOALN=5;
const float eps=0.00001;

AlignmentFactory::AlignmentFactory(GeneList *query_gene_list,
		GeneList *subject_gene_list,
		HomologyMatrix *homology_matrix)
{
	_query_gene_list=query_gene_list;
	_subject_gene_list=subject_gene_list;
	_homology_matrix=homology_matrix;
	_gap_creation_penalty=DEFAULT_GAP_CREATION_PENALTY;
	_max_gap_size_for_creation_penalty=DEFAULT_MAX_GAP_SIZE_CREATION_PENALTY;
	_gap_extension_penalty=DEFAULT_GAP_EXTENSION_PENALTY;
	_min_score=DEFAULT_MIN_SCORE;
	_min_align_size=DEFAULT_MIN_ALIGN_SIZE;
	_include_all_orthologs=false;
	_print_out_tandem_dups=false;

	_up_to_date=false;

}

void
AlignmentFactory::set_ortholog_score(float ortholog_score) {
	_ortholog_score=ortholog_score;
	_up_to_date=false;
}

void
AlignmentFactory::set_query_gene_list(GeneList *query_gene_list) {
	_query_gene_list=query_gene_list;
	_up_to_date=false;
}

void
AlignmentFactory::set_subject_gene_list(GeneList *subject_gene_list) {
	_subject_gene_list=subject_gene_list;
	_up_to_date=false;
}

void
AlignmentFactory::set_homology_matrix(HomologyMatrix *homology_matrix) {
	_homology_matrix=homology_matrix;
	_up_to_date=false;
}

void
AlignmentFactory::set_gap_creation_penalty(const float gap_creation_penalty) {
	_gap_creation_penalty=gap_creation_penalty;
	_up_to_date=false;
}

void
AlignmentFactory::set_max_gap_size_for_creation_penalty(const float max_gap_size_for_creation_penalty) {
	_max_gap_size_for_creation_penalty=max_gap_size_for_creation_penalty;
	_up_to_date=false;
}

void
AlignmentFactory::set_gap_extension_penalty(const float gap_extension_penalty){
	_gap_extension_penalty=gap_extension_penalty;
	_up_to_date=false;
}

void
AlignmentFactory::set_min_score(const float min_score) {
	_min_score=min_score;
	_up_to_date=false;
}

void
AlignmentFactory::set_min_align_size(const int min_align_size) {
	_min_align_size=min_align_size;
	_up_to_date=false;
}

void
AlignmentFactory::set_include_all_orthologs(const bool include_all_orthologs) {
	_include_all_orthologs=include_all_orthologs;
}

void
AlignmentFactory::set_print_out_tandem_dups(const bool print_out_tandem_dups) {
	_print_out_tandem_dups=print_out_tandem_dups;
}

multiset<Alignment> &
AlignmentFactory::get_alignments() {
	if (_up_to_date == true)
		return _alignment_set;

	int listsize1=_query_gene_list->size();
	int listsize2=_subject_gene_list->size();

#ifdef DEBUG
cerr << "Sizes = " << listsize1 << ", " << listsize2 << endl;
#endif



map<int, int> ortholog_map;
if (_include_all_orthologs) {
	build_ortholog_map(ortholog_map,
			_query_gene_list,_subject_gene_list,
			_homology_matrix);
}

//_query_gene_list->gene_index(1083932);
//_query_gene_list->gene_index(1083935);
//_subject_gene_list->gene_index(1241);
//cout << "1083932 1241 score: " << _homology_matrix->score(_query_gene_list->gene_index(1083932),_subject_gene_list->gene_index(1241)) << endl;
//cout << "1083935 1241 score: " << _homology_matrix->score(_query_gene_list->gene_index(1083935),_subject_gene_list->gene_index(1241)) << endl;
//cout << "1083936 " << "("<<_query_gene_list->gene_index(1083936)<<") 1239 "<< "(" << _subject_gene_list->gene_index(1239) << ") score: " << _homology_matrix->score(_query_gene_list->gene_index(1083936),_subject_gene_list->gene_index(1239)) << endl;
//exit(1);



int direction=0;
while (direction<2) {

#ifdef DEBUG
	cerr << "Direction = " << direction << endl;
#endif

	//init tmpMatrice
	float *tmpfloat=new float[listsize1*listsize2];
	memset(tmpfloat,0,listsize1*listsize2*sizeof(float));//was memset(tmpfloat,listsize1*listsize2*sizeof(float),0);
	float **alignmat=new float *[listsize1];
	for (int i=0;i<listsize1;i++) {
		alignmat[i]=&(tmpfloat[i*listsize2]);
	}

	short *tmpshort=new short[listsize1*listsize2];
	short **trackmat=new short *[listsize1];
	for (int i=0;i<listsize1;i++) {
		trackmat[i]=&(tmpshort[i*listsize2]);
	}

	// a matrix that keep track on how wide the total gap is
	int *tmpGapSizeCountMat=new int[listsize1*listsize2];
	int **gapSizeCountMat=new int *[listsize1];
	for (int i=0;i<listsize1;i++) {
		gapSizeCountMat[i]=&(tmpGapSizeCountMat[i*listsize2]);
	}


	// a matrix that keep track on how big a synteny is, needed when 2 max scores are equal
	int *tmpTotalSyntenySizeMat=new int[listsize1*listsize2];
	int **totalSyntenySizeMat=new int *[listsize1];
	for (int i=0;i<listsize1;i++) {
		totalSyntenySizeMat[i]=&(tmpTotalSyntenySizeMat[i*listsize2]);
	}

	float max_score;
	memset(tmpshort,0,sizeof(short)*listsize1*listsize2);

	// calculate the alignmat just once
	for (int i=0;i<listsize1;i++) {
		int index1=i;
		int index1_im1=i-1;
		bool newelementi=false;
		if (i==0 ||
				_query_gene_list->element_id(index1)!=_query_gene_list->element_id(index1_im1)) {
			newelementi=true;
		}
		for (int j=0;j<listsize2;j++) {
			int index2=j;
			int index2_jm1=j-1;
			if (direction==1) {
				index2=(listsize2-1)-index2;
				index2_jm1=(listsize2-1)-index2_jm1;
			}

			bool newelementj=false;
			if (j==0 ||
					_subject_gene_list->element_id(index2)!=_subject_gene_list->element_id(index2_jm1)) {
				newelementj=true;
			}

			float up=0;
			if (!newelementj) {

				if (gapSizeCountMat[i][j-1] < _max_gap_size_for_creation_penalty) {
					up=alignmat[i][j-1]+_gap_creation_penalty;
				} else {
					up=alignmat[i][j-1]+_gap_extension_penalty;
				}

				if (_print_out_tandem_dups) {

					/*if (index1 == 3 && index2 == 4) {
						cout << "_print_out_tandem_dups at index1 == 3 && index2 == 4 : "
								<< " ; _homology_matrix->score(index1,index2)=" << _homology_matrix->score(index1,index2)
								<< " ; _homology_matrix->score(index1,index2_jm1)=" << _homology_matrix->score(index1,index2_jm1)
								<< endl;
					}*/

					if ( _homology_matrix->score(index1,index2) > 0 && _homology_matrix->score(index1,index2_jm1) > 0
							&& ( _homology_matrix->score(index1,index2) == _homology_matrix->max() || _homology_matrix->score(index1,index2_jm1) == _homology_matrix->max() ) // only DBDH
					) {
						bool validTandemDup = false;
						//tandem dup are close match
						if ( _homology_matrix->blast_res(index1,index2).bits >= _homology_matrix->blast_res(index1,index2_jm1).bits
								&& ( (float) (  (float) _homology_matrix->blast_res(index1,index2_jm1).bits / (float) _homology_matrix->blast_res(index1,index2).bits ) >= GlobalParams::tandemDups__minPercentWithinBestPidThreshold) ) {
							validTandemDup = true;
						} else if (_homology_matrix->blast_res(index1,index2_jm1).bits > _homology_matrix->blast_res(index1,index2).bits
								&& ( (float) ( (float) _homology_matrix->blast_res(index1,index2).bits  / (float) _homology_matrix->blast_res(index1,index2_jm1).bits ) >= GlobalParams::tandemDups__minPercentWithinBestPidThreshold)) {
							validTandemDup = true;
						}


						/*cout << "found 1 _print_out_tandem_dups ! : "
									<< " ; index1=" << index1
									<< " ; index2=" << index2
									<< " ; _homology_matrix->score(index1,index2)=" << _homology_matrix->score(index1,index2)
									<< " ; _homology_matrix->score(index1,index2_jm1)=" << _homology_matrix->score(index1,index2_jm1)
									<< endl;*/

						//store as tandem dups
						if (validTandemDup) {
							map<int, std::set<int> >::iterator it = _col_tandem_dups_idxGeneList1ToVecIdxGeneList2.find(index1);
							if ( it == _col_tandem_dups_idxGeneList1ToVecIdxGeneList2.end() ) {
								//std::cout << "not found";
								set<int> tmpVec;
								tmpVec.insert(index2);
								tmpVec.insert(index2_jm1);
								_col_tandem_dups_idxGeneList1ToVecIdxGeneList2.insert(make_pair(index1,tmpVec));
							} else {
								//std::cout << it->first << " is " << it->second;
								it->second.insert(index2);
								it->second.insert(index2_jm1);
							}
						}


					}
				}

			}

			float left=0;
			if (!newelementi) {
				if (gapSizeCountMat[i-1][j] < _max_gap_size_for_creation_penalty) {
					left=alignmat[i-1][j]+_gap_creation_penalty;
				} else {
					left=alignmat[i-1][j]+_gap_extension_penalty;
				}

				//store as tandem dups
				if (_print_out_tandem_dups) {
					if ( _homology_matrix->score(index1,index2) > 0 && _homology_matrix->score(index1_im1,index2) > 0
							&& (_homology_matrix->score(index1,index2) == _homology_matrix->max() || _homology_matrix->score(index1_im1,index2) == _homology_matrix->max() ) ) {

						bool validTandemDup = false;
						//tandem dup are close match
						if ( _homology_matrix->blast_res(index1,index2).bits >= _homology_matrix->blast_res(index1_im1,index2).bits
								&& ( (float) (  (float) _homology_matrix->blast_res(index1_im1,index2).bits / (float) _homology_matrix->blast_res(index1,index2).bits ) >= GlobalParams::tandemDups__minPercentWithinBestPidThreshold) ) {
							validTandemDup = true;
						} else if (_homology_matrix->blast_res(index1_im1,index2).bits > _homology_matrix->blast_res(index1,index2).bits
								&& ( (float) ( (float) _homology_matrix->blast_res(index1,index2).bits  / (float) _homology_matrix->blast_res(index1_im1,index2).bits ) >= GlobalParams::tandemDups__minPercentWithinBestPidThreshold)) {
							validTandemDup = true;
						}
						if (validTandemDup) {
							map<int, std::set<int> >::iterator it = _row_tandem_dups_idxGeneList2ToVecIdxGeneList1.find(index2);
							if ( it == _row_tandem_dups_idxGeneList2ToVecIdxGeneList1.end() ) {
								//std::cout << "not found";
								set<int> tmpVec;
								tmpVec.insert(index1);
								tmpVec.insert(index1_im1);
								_row_tandem_dups_idxGeneList2ToVecIdxGeneList1.insert(make_pair(index2,tmpVec));
							} else {
								//std::cout << it->first << " is " << it->second;
								it->second.insert(index1);
								it->second.insert(index1_im1);
							}
						}
					}
				}
			}


			float diag=_homology_matrix->score(index1,index2);

			//if ( (i == 13 && index2 == 9) || ( i == 13 && index2 == 8 ) || ( i == 14 && index2 == 7 ) || ( i == 15 && index2 == 6 ) ) {
			//	cout << "diag at " << i << ";" << index2 << " : " << diag << endl;
			//}

			if ((!newelementi) && (!newelementj)) {
				if (gapSizeCountMat[i-1][j-1] >= _max_gap_size_for_creation_penalty && _homology_matrix->score(index1,index2) <= 0) {
					diag = alignmat[i-1][j-1] + _gap_extension_penalty;
				} else {
					diag +=alignmat[i-1][j-1];
				}
			}

			alignmat[i][j]=diag;
			if ((!newelementi) && (!newelementj)) {
				trackmat[i][j]=DIAG;
				if ( _homology_matrix->score(index1,index2) > 0 ) {
					gapSizeCountMat[i][j]=0;
				} else {
					gapSizeCountMat[i][j] = gapSizeCountMat[i-1][j-1] + 1;
				}
				totalSyntenySizeMat[i][j] = totalSyntenySizeMat[i-1][j-1] + 1;
			} else {
				trackmat[i][j]=INITALN;
				gapSizeCountMat[i][j]=0;
				if ( _homology_matrix->score(index1,index2) > 0 ) {
					totalSyntenySizeMat[i][j]=1;
				} else {
					totalSyntenySizeMat[i][j]=0;
				}
			}
			if (left>alignmat[i][j] && !newelementi) {
				alignmat[i][j]=left;
				trackmat[i][j]=LEFT;
				gapSizeCountMat[i][j] = gapSizeCountMat[i-1][j] + 1;
				totalSyntenySizeMat[i][j] = totalSyntenySizeMat[i-1][j] + 1;
			}

			//if (i==3 && j==4) {
			//	cout << "!!!!!!! i=3&&j=4 ; diag = " << alignmat[i][j] << " ; up = " << up << endl;
			//}

			if (up>alignmat[i][j] && !newelementj) {
				alignmat[i][j]=up;
				trackmat[i][j]=UP;
				gapSizeCountMat[i][j] = gapSizeCountMat[i][j-1] + 1;
				totalSyntenySizeMat[i][j] = totalSyntenySizeMat[i][j-1] + 1;
			}
			if (alignmat[i][j]<0-eps) {
				alignmat[i][j]=0;
				trackmat[i][j]=NOALN;
				gapSizeCountMat[i][j]=0;
				totalSyntenySizeMat[i][j] = 0;
			}
			//} else {
			//	alignmat[i][j]=0;
			//}

			//if ( (i == 13 && index2 == 9) || ( i == 13 && index2 == 8 ) || ( i == 14 && index2 == 7 ) || ( i == 15 && index2 == 6 ) ) {
			//	cout << "1 alignmat at " << i << ";" << index2 << " : " << alignmat[i][j] << endl;
			//}

		}
	}

	// new way do not loop multiple time to find max score, record it in a multiset

	// store all the score in the matrix > _min_score
	//then sort descending : std::sort(numbers.begin(), numbers.end(), std::greater<float>());
	multiset<CellWithScoreGreaterThanCutoff> SetCellWithScoreGreaterThanCutoff;
	for (int i=0;i<listsize1;i++) {
		for(int j=0;j<listsize2;j++) {
			int index2=j;
			if (direction==1) {
				index2=(listsize2-1)-index2;
			}
			if ( ( _homology_matrix->score(i,index2)>0 ) && ( alignmat[i][j] >= _min_score-eps ) ) {
				CellWithScoreGreaterThanCutoff cellWithScoreGreaterThanCutoff;
				cellWithScoreGreaterThanCutoff._col= i ;
				cellWithScoreGreaterThanCutoff._row= j ;
				cellWithScoreGreaterThanCutoff._score= alignmat[i][j] ;
				cellWithScoreGreaterThanCutoff._totalSyntenySize= totalSyntenySizeMat[i][j] ;
				SetCellWithScoreGreaterThanCutoff.insert(SetCellWithScoreGreaterThanCutoff.end(),cellWithScoreGreaterThanCutoff);
			}
		}
	}

	//loop through SetCellWithScoreGreaterThanCutoff (except if marked as FORBIDDEN) and store alignment when appropriate after recalculating their score
	set<CellWithScoreGreaterThanCutoff>::reverse_iterator rit;
	for (rit=SetCellWithScoreGreaterThanCutoff.rbegin(); rit != SetCellWithScoreGreaterThanCutoff.rend(); rit++) {
		//loop through all max score sorted descending

		//cout << "Evaluating cell " << rit->col() << ";" << rit->row() << " with score " << rit->score() << endl;

		if (trackmat[rit->col()][rit->row()]==FORBIDDEN) {
			//cout << "\t FORBIDDEN" << endl;
			continue;
		}
		//cout << "\t ok, not FORBIDDEN" << endl;

		vector<int> align1;
		vector<int> align2;
		vector<float> scores;
		string collidedWithForbiddenCellOnBacktrack = build_backward_alignment(align1,align2,
				scores,
				alignmat,trackmat,
				rit->col(),rit->row()
				, direction, listsize2
				, true);

		/*
		int rowIT = rit->row();
		if (direction==1) {
			rowIT = (listsize2-1)-rowIT;
		}
		cout << "max_i=" << rit->col() << " ; max_j=" << rit->row() << " ; score = " << rit->score() << " ; _homology_matrix->score(rit->col(),rit->row())=" << _homology_matrix->score(rit->col(),rowIT) << endl;
		*/

		max_score = trim_and_recalculate_alignment(
				align1
				, align2
				, scores
				//alignmat
				//,trackmat
				//maxcol,maxrow,
				, direction
				, listsize2
				//, max_score
				);
		if ((int) align1.size()>=_min_align_size && max_score >= _min_score) {


			float max_matrix=_homology_matrix->max();
			buildAndStoreAlignmentObj(
									max_score
									, scores
									, direction
									, align1
									, align2
									, listsize2
									, max_matrix
									, ortholog_map
									, trackmat
									, collidedWithForbiddenCellOnBacktrack
									);
		}


    }

	//destroy SetCellWithScoreGreaterThanCutoff when done
	SetCellWithScoreGreaterThanCutoff.clear();

	direction++;

	//delete all tmp vectors
	delete[] trackmat;
	delete[] tmpshort;

	delete[] alignmat;
	delete[] tmpfloat;

	delete[] gapSizeCountMat;
	delete[] tmpGapSizeCountMat;

	delete[] totalSyntenySizeMat;
	delete[] tmpTotalSyntenySizeMat;


#ifdef DEBUG
	cerr << "Changing direction: " << direction << endl;
#endif
}

if (_include_all_orthologs) {
	map<int,int>::iterator it;
	for (it=ortholog_map.begin();it != ortholog_map.end(); it++) {

		int index1=(*it).first;
		int index2=(*it).second;

		Alignment alignment;
		alignment._score=_homology_matrix->max();
		alignment._scores.clear();
		alignment._scores.push_back(alignment._score);
		alignment._homologies.clear();
		alignment._homologies.push_back(alignment._score);
		alignment._orthologs=1;
		alignment._homologs=0;
		alignment._orientation=Alignment::CONSERVED;

		AlignedSequence query;
		query._start_base=_query_gene_list->start(index1);
		//query._start_gene=_query_gene_list->position(id1);
		query._end_base=_query_gene_list->stop(index1);
		//query._end_gene=_query_gene_list->position(id1);
		query._gene_ids.clear();
		query._gene_ids.push_back(_query_gene_list->gene_id(index1));
		query._gene_names.clear();
		query._gene_names.push_back(_query_gene_list->gene_name(index1));
		query._start_points.clear();
		query._start_points.push_back(_query_gene_list->start(index1));
		query._end_points.clear();
		query._end_points.push_back(_query_gene_list->stop(index1));
		query._organism_id=_query_gene_list->organism_id();

		AlignedSequence subject;
		subject._gene_ids.clear();
		subject._gene_ids.push_back(_subject_gene_list->gene_id(index2));
		subject._gene_names.clear();
		subject._start_base=_subject_gene_list->start(index2);
		//subject._start_gene=_subject_gene_list->position(index2);
		subject._end_base=_subject_gene_list->stop(index2);
		//subject._end_gene=_subject_gene_list->position(id2);
		subject._gene_names.push_back(_subject_gene_list->gene_name(index2));
		subject._start_points.clear();
		subject._start_points.push_back(_subject_gene_list->start(index2));
		subject._end_points.clear();
		subject._end_points.push_back(_subject_gene_list->stop(index2));
		subject._organism_id=_subject_gene_list->organism_id();

		alignment._query=query;
		alignment._subject=subject;
		_alignment_set.insert(_alignment_set.end(),alignment);
	}
}



return _alignment_set;
}


float
AlignmentFactory::trim_and_recalculate_alignment(
		vector<int> &align1
		, vector<int> &align2
		, vector<float> &scores
		//float **alignmat
		//, short **trackmat
		//int col, int row
		, int direction
		, int listsize2
		//, float max_score
		)
{

	//trim matrice and recalculate max score because matrix score can be influenced by previous alignment in the matrix
	int firstValidIndex = -1;
	float first_homology_score=-1;
	for (int i=0;i<(int) align1.size();i++) {
		int index1=align1[i];
		int index2=align2[i];
		if (direction==1) {
			index2=(listsize2-1)-index2;
		}

		//cout << "for i=" << i << ", index1=" << index1 << " and index2=" << index2 << " ; direction=" << direction << " ; align1.size()=" << align1.size() << endl;
		if (align1[i]>=0 && align2[i]>=0) {
			float homology_scoreIT=_homology_matrix->score(index1,index2);
			//cout << "for i=" << i << ", _homology_matrix->score=" << homology_scoreIT << endl;
			if (homology_scoreIT > 0) {
				//ok we have an ortho or homolog
				firstValidIndex = i;
				first_homology_score=homology_scoreIT;
				break;
			}
		}
	}

	//cout << "trim_and_recalculate_alignment firstValidIndex=" << firstValidIndex << " ; init size align1= " << align1.size() << endl;
	for (int i=firstValidIndex-1;i>=0;i--) {
		//cout << "erasing index " << i << endl;
		align1.erase(align1.begin() + i);
		align2.erase(align2.begin() + i);
		scores.erase(scores.begin() + i);
	}
	//cout << "final size align1= " << align1.size() << endl;


	//recalculate max_score by diff max score and first index score
	if (scores.empty() || first_homology_score == -1) {
		cerr << "Error AlignmentFactory::trim_and_recalculate_alignment : scores.empty() || first_homology_score == -1" << endl;
		exit(1);
	} else if (scores.size() == 1) {
		//cout << "corrected max_score= " << first_homology_score << endl;
		return first_homology_score;
	} else {
		float score_first_element = scores[0];
		if (score_first_element < first_homology_score) {
			cerr << "Error AlignmentFactory::trim_and_recalculate_alignment : score_first_element < first_homology_score" << endl;
			exit(1);
		}
		float diffToSubstract = score_first_element - first_homology_score;
		//cout << "corrected max_score= " << scores[scores.size() - 1] - diffToSubstract << endl;
		return scores[scores.size() - 1] - diffToSubstract; // score_last_element - diff
	}


}


string
AlignmentFactory::build_backward_alignment(
		vector<int> &align1
		, vector<int> &align2
		, vector<float> &scores
		, float **alignmat
		, short **trackmat
		, int col
		, int row
		, int direction
		, int listsize2
		, bool isHighestScoreInSyteny
		)
{



	int index2=row;
	int index2_jm1=row-1;
	if (direction==1) {
		index2=(listsize2-1)-index2;
		index2_jm1=(listsize2-1)-index2_jm1;
	}


	//cout << "build_backward_alignment trackmat["<<col<<"]["<<row<<"] " << trackmat[col][row] << " alignmat["<<col<<"]["<<row<<"] " << alignmat[col][row] << endl;
	//if (alignmat[col][row]<=0+eps) {
	//return;
	//}
	if ( trackmat[col][row]==NOALN ) {
		return "";
	} else if (trackmat[col][row]==FORBIDDEN) {
		//cout << " !!! FOUND FORBIDDEN at " << col << ";" << row << endl;
		string stToReturn;
		stToReturn.append(std::to_string(col));
		stToReturn.append("_");
		stToReturn.append(std::to_string(index2));
		return stToReturn;
	}

	int i1=col;
	int i2=row;
	//float score=alignmat[col][row];
	if (trackmat[col][row] == DIAG) {
		string collidedWithForbiddenCellOnBacktrack = build_backward_alignment(align1, align2,
				scores,
				alignmat,trackmat,col-1,row-1, direction, listsize2, false);
		align1.push_back(i1);
		align2.push_back(i2);
		scores.push_back(alignmat[col][row]);
		//trackmat[col][row]=FORBIDDEN; //only set FORBIDDEN if the alignment is accepted as valid after recalculate exact score and such

		return collidedWithForbiddenCellOnBacktrack;


	} else if (trackmat[col][row] == INITALN) {
		align1.push_back(i1);
		align2.push_back(i2);
		scores.push_back(alignmat[col][row]);
		return "";
		//trackmat[col][row]=FORBIDDEN;
	} else if (trackmat[col][row] == UP) {
		string collidedWithForbiddenCellOnBacktrack = build_backward_alignment(align1,align2,
				scores,
				alignmat,trackmat,col,row-1, direction, listsize2, false);
		if (isHighestScoreInSyteny) {
			 // for some homolog in tandem dups, 1st homolog related to the best score can come from up
			if (_homology_matrix->score(col,index2) > 0) {
				align1.push_back(i1);
				align2.push_back(i2);
			} else {
				cerr << "Error AlignmentFactory::build_backward_alignment: UP isHighestScoreInSyteny but _homology_matrix->score(col,index2) not > 0" << _homology_matrix->score(col,index2) << endl;
				exit(1);
			}
		} else {
			align1.push_back(-1);
			align2.push_back(i2);
		}
		scores.push_back(alignmat[col][row]);
		//trackmat[col][row]=FORBIDDEN;
		return collidedWithForbiddenCellOnBacktrack;

	} else if (trackmat[col][row] == LEFT) {
		string collidedWithForbiddenCellOnBacktrack = build_backward_alignment(align1,align2,
				scores,
				alignmat,trackmat,col-1,row, direction, listsize2, false);

		if (isHighestScoreInSyteny) {
			if (_homology_matrix->score(col,index2) > 0) {
				align1.push_back(i1);
				align2.push_back(i2);
			} else {
				cerr << "Error AlignmentFactory::build_backward_alignment: LEFT isHighestScoreInSyteny but _homology_matrix->score(col,index2) not > 0" << _homology_matrix->score(col,index2) << endl;
				exit(1);
			}
		} else {
			align1.push_back(i1);
			align2.push_back(-1);
		}

		scores.push_back(alignmat[col][row]);
		//trackmat[col][row]=FORBIDDEN;
		return collidedWithForbiddenCellOnBacktrack;

	} else {
		cerr << "Error AlignmentFactory::build_backward_alignment : unknown code for trackmat=" <<  trackmat[col][row]<< endl;
		exit(1);
	}
}





void
AlignmentFactory::buildAndStoreAlignmentObj(
		float max_score
		, vector<float> &scores
		, int direction
		, vector<int> &align1
		, vector<int> &align2
		, int listsize2
		, float max_matrix
		, map<int, int>  &ortholog_map
		, short **trackmat
		, string collidedWithForbiddenCellOnBacktrack
) {

	Alignment alignment;
	alignment._score=max_score;//alignmat[maxcol][maxrow];
	alignment._scores=scores;
	alignment._homologies.clear();
	alignment._orthologs=0;
	alignment._homologs=0;

	if (direction == 0) {
		alignment._orientation=Alignment::CONSERVED;
	}
	if (direction ==1) {
		alignment._orientation=Alignment::REVERSED;
	}
	alignment._isBranchedToAnotherSynteny=collidedWithForbiddenCellOnBacktrack;

	AlignedSequence query;
	query._gene_names.clear();
	query._gene_ids.clear();
	query._start_points.clear();
	query._end_points.clear();
	query._organism_id=_query_gene_list->organism_id();
	query._start_base=-1;
	query._end_base=-1;

	AlignedSequence subject;
	subject._gene_names.clear();
	subject._gene_ids.clear();
	subject._start_points.clear();
	subject._end_points.clear();
	subject._organism_id=_subject_gene_list->organism_id();
	subject._start_base=-1;
	subject._end_base=-1;

	int idx_i_mark_as_forbidden = -1;
	int idx_j_mark_as_forbidden = -1;

	for (int i=0;i<(int) align1.size();i++) {
		int index1=align1[i];
		int index2=align2[i];
		//int gid1=-1;
		//int gid2=-1;
		if (direction==1) {
			index2=(listsize2-1)-index2;
		}

		if (align1[i]>=0 ) {

#ifdef DEBUG
			cerr << "query gene name for id" << index1 << " = " << _query_gene_list->gene_name(index1) << endl;
#endif
			//gid1=_query_gene_list->gene_id(index1);
			query._gene_ids.push_back(_query_gene_list->gene_id(index1));
			query._gene_names.push_back(_query_gene_list->gene_name(index1));
			query._start_points.push_back(_query_gene_list->start(index1));
			query._end_points.push_back(_query_gene_list->stop(index1));
			idx_i_mark_as_forbidden = index1;
		} else {
			if (align1[i-1]>=0){
				alignment._homologies.push_back(GAP_STARTS);//_gap_creation_penalty
			} else {
				alignment._homologies.push_back(GAP_CONTINUES);//_gap_extension_penalty
			}
			query._gene_ids.push_back(0);
			query._gene_names.push_back("-");
			query._start_points.push_back(0);
			query._end_points.push_back(0);
		}

		if (align2[i]>=0) {

#ifdef DEBUG
			cerr << "subject gene name for id" << index2 << " = " << _subject_gene_list->gene_name(index2) << endl;
#endif
			//gid2=_subject_gene_list->gene_id(index2);
			subject._gene_ids.push_back(_subject_gene_list->gene_id(index2));
			subject._gene_names.push_back(_subject_gene_list->gene_name(index2));
			subject._start_points.push_back(_subject_gene_list->start(index2));
			subject._end_points.push_back(_subject_gene_list->stop(index2));
			idx_j_mark_as_forbidden = align2[i];
		} else {
			if (align2[i-1]>=0) {
				alignment._homologies.push_back(GAP_STARTS);//_gap_creation_penalty
			} else {
				alignment._homologies.push_back(GAP_CONTINUES);//_gap_extension_penalty
			}
			subject._gene_ids.push_back(0);
			subject._gene_names.push_back("-");
			subject._start_points.push_back(0);
			subject._end_points.push_back(0);
		}

		if (align1[i]>=0 && align2[i]>=0) {
			float homology=_homology_matrix->score(index1,index2);
			alignment._homologies.push_back(homology);
			if (homology == max_matrix) {
				alignment._orthologs++;
				if (_include_all_orthologs) {
					ortholog_map.erase(index1);
				}
			}
			else if (homology > 0)
				alignment._homologs++;
			if (i==0) {
#ifdef DEBUG
cerr << "Initializing alignment at both ends." << endl;
#endif
				query._start_base=_query_gene_list->start(index1);
				if (direction == 0) {
					subject._start_base=_subject_gene_list->start(index2);
				} else {
					subject._end_base=_subject_gene_list->stop(index2);
				}
			}
			if (i== (int) align1.size()-1) {
				query._end_base=_query_gene_list->stop(index1);
				if (direction == 0) {
					subject._end_base=_subject_gene_list->stop(index2);
				} else {
					subject._start_base=_subject_gene_list->start(index2);
				}
			}
		}
		/*cout <<  "FORBIDDEN " << idx_i_mark_as_forbidden << "(" << align1[i] << ");" << idx_j_mark_as_forbidden << "(" << align2[i] << ")"
								<< " of matrix " << listsize1 << ";" << listsize2
								<< endl;*/

		//cout << " set FORBIDDEN at " << idx_i_mark_as_forbidden << ";" << idx_j_mark_as_forbidden << endl;
		trackmat[idx_i_mark_as_forbidden][idx_j_mark_as_forbidden]=FORBIDDEN;
	}
	//cout << endl;

	alignment._query=query;
	alignment._subject=subject;
	_alignment_set.insert(_alignment_set.end(),alignment);

}

void
AlignmentFactory::build_ortholog_map(map<int, int> &ortholog_map,
		GeneList *query_gene_list,
		GeneList *subject_gene_list,
		HomologyMatrix *hmatrix)
{

	for (int i=0;i<query_gene_list->size();i++) {
		for (int j=0;j<subject_gene_list->size();j++) {
			if (hmatrix->score(i,j) == _ortholog_score ) {
				ortholog_map[i]=j;
			}
		}
	}

}
