#ifndef _GENELISTPSQL_H_
#define _GENELISTPSQL_H_

using namespace std;
#include "GeneList.h"

#include "include/libpq-fe.h"

class GeneListPSQL : public GeneList 
{
 public:
  GeneListPSQL(const int organism_id);
 
};

#endif
