#include "HomologyMatrix.h"
#include "ScoreNormalizer.h"

void
ScoreNormalizer::transform(float **scores, const int cols, const int rows) {

  for (int i=0;i<cols;i++)
    for (int j=0;j<rows;j++) {
      if (scores[i][j]==HomologyMatrix::ORTHOLOGS)
	scores[i][j]=_ortholog_score;
      else if (scores[i][j]==HomologyMatrix::HOMOLOGS)
	scores[i][j]=_homolog_score;
      else 
	scores[i][j]=_unrelated_score;
    }
}
