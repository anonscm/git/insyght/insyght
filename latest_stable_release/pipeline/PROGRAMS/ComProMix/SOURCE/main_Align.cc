#include <fstream>
#include <iostream>
#include <sstream>
#include <set>
#include <cstring>
#include <algorithm>
using namespace std;

#include "AlignmentFactory.h"
#include "GeneList.h"
//#include "GeneList_mod.h"
#include "HomologyMatrix.h"
//#include "RegionFilter.h"
#include "ScoreNormalizer.h"
#include "GlobalParams.h"

//#include <ctime> // time_t
//time fonction
//time_t begin,end; // time_t is a datatype to store time values.
//time (&begin); // note time before execution
//time (&end); // note time after execution
//double difference = difftime (end,begin);
//printf ("time taken for function() %.2lf seconds.\n", difference );


#define ORTHOLOG_SCORE  4
#define HOMOLOG_SCORE  2
#define MISMATCH_PENALTY -4
#define PROT_FRAC 0.5
#define MIN_ALIGNMENT_SIZE 2
#define CUTOFF_SCORE 6
#define GAP_CREATION_PENALTY -3
#define MAX_GAP_SIZE_CREATION_PENALTY 3
#define GAP_EXTENSION_PENALTY -5000
#define INCLUDE_ALL_ORTHOLOGS true
#define PRINT_OUT_TANDEM_DUPS false
//#define PRINT_OUT_GENE_FUSIONS true

static int PROT_FUSION_ID = 0;
static int CLOSE_BEST_MATCH_ID = 0;

void usage(char *fcn)
{
  cerr << "\nUsage " << fcn << " [-os ortholog_score] "
    "[-hs homolog_score] [-mp mismatch_penalty] "
    "[-gc gap_creation_penalty] "
    "[-mgsc max_gap_size_for_creation_penalty] "
    "[-ge gap_extension_penalty]" << endl;
  cerr << "                   [-m min_align_size] "
    "[-o include_all_orthologs (t/f)] "
    "[-c cutoff_score] " 
    "[-Adir archiveDir] "
    "[-pf prot_frac] "
	"[-potd (t/f)] "
	"[-pogf (t/f)] "
    " -org1 organism_id1 -org2 organism_id2 -fn blast_output_file"

		  << endl;
  cerr << "where" << endl;
  cerr << "ortholog score:        default (" << ORTHOLOG_SCORE << ")" << endl;
  cerr << "homolog score:         default (" << HOMOLOG_SCORE << ")" << endl;
  cerr << "mismatch penalty:      default (" << MISMATCH_PENALTY << ")" << endl;
  cerr << "gap creation penalty:  default (" << GAP_CREATION_PENALTY << ")" << endl;
  cerr << "max gap size for creation penalty:  default (" << MAX_GAP_SIZE_CREATION_PENALTY << ")" << endl;
  cerr << "gap extension penalty: default (" << GAP_EXTENSION_PENALTY << ")" << endl;
  cerr << "min alignment size:    default (" << MIN_ALIGNMENT_SIZE << ")" << endl;
  cerr << "include all orthologs: default (" << INCLUDE_ALL_ORTHOLOGS << ")" << endl;
  cerr << "cutoff score:          default (" << CUTOFF_SCORE <<")" << endl;
  //  cerr << "region filter file:    default (not used)" << endl;
  cerr << "archiveDir:            default (./)" << endl;
  cerr << "prot_frac:             default (" << PROT_FRAC << ")" << endl;
  cerr << "organism_id1:          (compulsory)" << endl;
  cerr << "organism_id2:          (compulsory)" << endl;
  cerr << "blast output file:     (compulsory)" << endl;
  cerr << "print_out_tandem_dups: default (true)" << endl;
  cerr << "print_out_gene_fusions: default (true)" << endl;
  cerr << "print_out_close_best_match: default (true)" << endl;
  cerr << "                       Only the lines that concern organism_id1 as query and organism_id2 as hit are considered" << endl;
  cerr << "                       Notice 1: only element_ids in the BLAST output file name are used later in the program" << endl;
  cerr << "                       Notice 2: a regular unix path can precede the BLAST output file name," << endl;
  cerr << "                                 e.g., /dir1/dir2/Blast_output_file or ../../dir1/Blast_output_file\n" << endl;
}



/************************************************************************************************************
 *                                                                                                          *
 *  This version of main_Align does not require the connection to the relational database ORIGAMI           *
 *  Instead, it needs three files: two archives containing data for element_id_1 and element_id_2           *
 *  and a BLAST output file containing the comparisons of all proteins of element_id_1 with those           *
 *  of element_id_2. Archive files are created with the main_GeneList program                               *
 *                                                                                                          *
 * The program creates 4 ORIGAMI tables (in tabulation separated variable format, *.tsv)                    *
 *   - table homologies:       the name of the tsv file is el1_and_el2_homologies_table.tsv                 *
 *   - table alignment_pairs:  the name of the tsv file is el1_and_el2_alignments_pairs_table.tsv           *
 *   - table alignments:       the name of the tsv file is el1_and_el2_alignments_table.tsv                 *
 *   - table alignment_params: the name of the tsv file is el1_and_el2_alignments_params_table.tsv          *
 *   where el1 and el2 are element_id_1 and element_id_2 respectively                                       *
 *                                                                                                          *
 * Notice: the program reads only one BLAST output file [element_id_1 vs element_id_2]. It dump data for    *
 * the ORIGAMI tables in one directions: [element_id_1 vs element_id_2], not [element_id_1 vs element_id_2] *
 *                                                                                                          *
 ************************************************************************************************************/



void
print_batch_tandem_dups(
		int tandem_dups_id
		, int mapitr_first
		, vector<int> &vConsecutiveIdx
		, GeneList &glist1
		, GeneList &glist2
		, bool gene_id_single_is_q
		, ofstream &fp_out_tandem_dups
		, HomologyMatrix &hmat
)
{

	int i = 0;
	int q_element_id_stored = -1;
	int s_element_id_stored = -1;

	for(int idxIT : vConsecutiveIdx) {

		int single_organims_id, single_element_id, single_gene_id, tandem_organims_id, tandem_element_id, tandem_gene_id;
		float pid;       // percentage identity in alignment
		int hsp_len;     // alignment length
		int mismatches;  // number of mismatches
		int gaps;        // number of gaps
		int qstart;      // start of alignment in query protein
		int qend;        // end of alignment in query protein
		int qlength;     // query protein length (in aa)
		int hstart;      // start of alignment in target protein
		int hend;       // end of alignment in target protein
		int hlength;     // target protein length (in aa)
		float evalue;    // BLAST e-value
		float bits;      // alignment score in bits
		int bdbh;        // if bdbh == 2 the protein pair corresponds to a bdbh
		int rank;        // rank == 1 if target protein comes first in the list of homologs else set to 2
		if ( gene_id_single_is_q ) {
			single_organims_id = glist1.organism_id();
			single_element_id = glist1.element_id(mapitr_first);
			single_gene_id = glist1.gene_id(mapitr_first);
			tandem_organims_id = glist2.organism_id();
			tandem_element_id = glist2.element_id(idxIT);
			tandem_gene_id = glist2.gene_id(idxIT);
			pid = hmat.blast_res(mapitr_first, idxIT).pid;
			hsp_len = hmat.blast_res(mapitr_first, idxIT).hsp_len;
			mismatches = hmat.blast_res(mapitr_first, idxIT).mismatches;
			gaps = hmat.blast_res(mapitr_first, idxIT).gaps;
			qstart = hmat.blast_res(mapitr_first, idxIT).qstart;
			qend = hmat.blast_res(mapitr_first, idxIT).qend;
			qlength = hmat.blast_res(mapitr_first, idxIT).qlength;
			hstart = hmat.blast_res(mapitr_first, idxIT).hstart;
			hend = hmat.blast_res(mapitr_first, idxIT).hend;
			hlength = hmat.blast_res(mapitr_first, idxIT).hlength;
			evalue = hmat.blast_res(mapitr_first, idxIT).evalue;
			bits = hmat.blast_res(mapitr_first, idxIT).bits;
			bdbh = hmat.blast_res(mapitr_first, idxIT).bdbh;
			rank = hmat.blast_res(mapitr_first, idxIT).rank;
		}else {
			single_organims_id = glist2.organism_id();
			single_element_id = glist2.element_id(mapitr_first);
			single_gene_id = glist2.gene_id(mapitr_first);
			tandem_organims_id = glist1.organism_id();
			tandem_element_id = glist1.element_id(idxIT);
			tandem_gene_id = glist1.gene_id(idxIT);
			pid = hmat.blast_res(idxIT, mapitr_first).pid;
			hsp_len = hmat.blast_res(idxIT, mapitr_first).hsp_len;
			mismatches = hmat.blast_res(idxIT, mapitr_first).mismatches;
			gaps = hmat.blast_res(idxIT, mapitr_first).gaps;
			qstart = hmat.blast_res(idxIT, mapitr_first).hstart;
			qend = hmat.blast_res(idxIT, mapitr_first).hend;
			qlength = hmat.blast_res(idxIT, mapitr_first).hlength;
			hstart = hmat.blast_res(idxIT, mapitr_first).qstart;
			hend = hmat.blast_res(idxIT, mapitr_first).qend;
			hlength = hmat.blast_res(idxIT, mapitr_first).qlength;
			evalue = hmat.blast_res(idxIT, mapitr_first).evalue;
			bits = hmat.blast_res(idxIT, mapitr_first).bits;
			bdbh = hmat.blast_res(idxIT, mapitr_first).bdbh;
			rank = hmat.blast_res(idxIT, mapitr_first).rank;
		}



		int q_element_id = -1;
		if (gene_id_single_is_q) {
			q_element_id = glist1.element_id(mapitr_first);
		} else {
			q_element_id = glist1.element_id(idxIT);
		}
		if (q_element_id_stored == -1) {
			q_element_id_stored = q_element_id;
		} else if (q_element_id_stored == q_element_id) {
			//ok
		} else {
			cerr << "Error print_batch_tandem_dups: NOT q_element_id_stored == q_element_id : "
													<< " ; q_element_id_stored=" << q_element_id_stored
													<< " ; q_element_id=" << q_element_id
													<< endl;
											exit(EXIT_FAILURE);
		}
		int s_element_id = -1;
		if (gene_id_single_is_q) {
			s_element_id = glist2.element_id(idxIT);
		} else {
			s_element_id = glist2.element_id(mapitr_first);
		}
		if (s_element_id_stored == -1) {
			s_element_id_stored = s_element_id;
		} else if (s_element_id_stored == s_element_id) {
			//ok
		} else {
			cerr << "Error print_batch_tandem_dups: NOT s_element_id_stored == s_element_id : "
													<< " ; s_element_id_stored=" << s_element_id_stored
													<< " ; s_element_id=" << s_element_id
													<< endl;
											exit(EXIT_FAILURE);
		}
		string alignment_param_id = "ALN_";
		alignment_param_id += std::to_string(q_element_id);
		alignment_param_id += "_";
		alignment_param_id += std::to_string(s_element_id);
		//COPY tandem_dups (tandem_dups_id, alignment_param_id, gene_id_single_is_q,
		// single_organims_id, single_element_id, single_gene_id, tandem_organims_id, tandem_element_id, tandem_gene_id;
		// pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank
		//idx_in_dup) FROM stdin;\n";
		fp_out_tandem_dups << tandem_dups_id << "\t" << alignment_param_id << "\t" << gene_id_single_is_q
				<< "\t" << single_organims_id
				<< "\t" << single_element_id
				<< "\t" << single_gene_id
				<< "\t" << tandem_organims_id
				<< "\t" << tandem_element_id
				<< "\t" << tandem_gene_id
				<< "\t" << pid
				<< "\t" << hsp_len
				<< "\t" << mismatches
				<< "\t" << gaps
				<< "\t" << qstart
				<< "\t" << qend
				<< "\t" << qlength
				<< "\t" << hstart
				<< "\t" << hend
				<< "\t" << hlength
				<< "\t" << evalue
				<< "\t" << bits
				<< "\t" << bdbh
				<< "\t" << rank
				<< "\t" << i
				<< std::endl;
		i++;
	}


}

vector<string> getVecCloseBestMatch(
		  MapIdx2setGeneFusionCheckerObj &mi2sgfco
		  , HomologyMatrix &hmat
		  , bool iIsJInHmat
		  , GeneList &glist1
		  , GeneList &glist2
	  	  ) {
	vector<string> vecToReturn;
	for (auto mapitr = mi2sgfco.begin(); mapitr != mi2sgfco.end(); ++mapitr) {
		//idx i = mapitr->first
		vector<int> closeBestMatch_idx;
		bool firstIter = true;
		float BestBitsAmongAll = -1;
		for(auto alnrGeneFusionIT : mapitr->second) {
			// use GeneFusionCheckerObj setGeneFusionIT here
			if (firstIter) {
				//first longuest protalign
				closeBestMatch_idx.push_back(alnrGeneFusionIT.idx());
				BestBitsAmongAll = alnrGeneFusionIT.bits();
			} else {
				// is bits IT within GlobalParams::closeBestMatchs_minPercentThreshold
				if ( alnrGeneFusionIT.bits() >= ( BestBitsAmongAll * GlobalParams::closeBestMatchs_minPercentThreshold ) ) {
					//ok close match
					closeBestMatch_idx.push_back(alnrGeneFusionIT.idx());
				} else {
					// below threshold, break
					break;
				}
			}
			firstIter = false;	
		}

		if (closeBestMatch_idx.size() > 1) {
			CLOSE_BEST_MATCH_ID++;

			int idx_best_subject = closeBestMatch_idx[0];
			int q_organims_id, q_element_id, q_gene_id, s_organims_id;
			//iIsJInHmat
			//closeBestMatch_idx.size()
			bool contain_bdbh = false;
			float pid_best_match;       // percentage identity in alignment
			float evalue_best_match;    // BLAST e-value
			float bits_best_match;      // alignment score in bits
			string list_close_match_gene_ids = "";
			if ( ! iIsJInHmat ) {
				q_organims_id = glist1.organism_id();
				q_element_id = glist1.element_id(mapitr->first);
				q_gene_id = glist1.gene_id(mapitr->first);
				s_organims_id = glist2.organism_id();
				if (hmat.blast_res(mapitr->first, idx_best_subject).bdbh == 2 ){
					contain_bdbh = true;
				}
				//gene_id_best_match = glist2.gene_id(idx_best_subject);
				pid_best_match = hmat.blast_res(mapitr->first, idx_best_subject).pid;
				evalue_best_match = hmat.blast_res(mapitr->first, idx_best_subject).evalue;
				bits_best_match = hmat.blast_res(mapitr->first, idx_best_subject).bits;
			}else {
				q_organims_id = glist2.organism_id();
				q_element_id = glist2.element_id(mapitr->first);
				q_gene_id = glist2.gene_id(mapitr->first);
				s_organims_id = glist1.organism_id();
				if ( hmat.blast_res(idx_best_subject, mapitr->first).bdbh == 2 ){
					contain_bdbh = true;
				}
				//gene_id_best_match = glist1.gene_id(idx_best_subject);
				pid_best_match = hmat.blast_res(idx_best_subject, mapitr->first).pid;
				evalue_best_match = hmat.blast_res(idx_best_subject, mapitr->first).evalue;
				bits_best_match = hmat.blast_res(idx_best_subject, mapitr->first).bits;
			}

			bool first_iter = true;
			for(auto const& idxSubject: closeBestMatch_idx) {
				if ( ! iIsJInHmat ) {
					if (first_iter) {
						list_close_match_gene_ids.append(std::to_string(glist2.gene_id(idxSubject)));
					} else {
						list_close_match_gene_ids.append(",");
						list_close_match_gene_ids.append(std::to_string(glist2.gene_id(idxSubject)));
					}
				} else {
					if (first_iter) {
						list_close_match_gene_ids.append(std::to_string(glist1.gene_id(idxSubject)));
					} else {
						list_close_match_gene_ids.append(",");
						list_close_match_gene_ids.append(std::to_string(glist1.gene_id(idxSubject)));
					}
				}
				first_iter = false;
			}

			//new header close_best_match_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, is_mirror_data, number_of_close_matchs, contain_bdbh
			//, pid_best_match, evalue_best_match, bits_best_match, list_close_match_gene_ids
			ostringstream myString;
			myString << CLOSE_BEST_MATCH_ID << "\t";
			myString << q_organims_id << "\t";
			myString << q_element_id << "\t";
			myString << q_gene_id << "\t";
			myString << s_organims_id << "\t";
			myString << iIsJInHmat << "\t";
			myString << closeBestMatch_idx.size() << "\t";
			myString << contain_bdbh << "\t";
			myString << pid_best_match << "\t";
			myString << evalue_best_match << "\t";
			myString << bits_best_match << "\t";
			myString << list_close_match_gene_ids;
			vecToReturn.push_back(myString.str());
			/* OLD way detailled header
			int s_rank_among_all_close_best_match = 0;
			for(auto const& idxSubject: closeBestMatch_idx) {

				int q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id;
				float pid;       // percentage identity in alignment
				int hsp_len;     // alignment length
				int mismatches;  // number of mismatches
				int gaps;        // number of gaps
				int qstart;      // start of alignment in query protein
				int qend;        // end of alignment in query protein
				int qlength;     // query protein length (in aa)
				int hstart;      // start of alignment in target protein
				int hend;       // end of alignment in target protein
				int hlength;     // target protein length (in aa)
				float evalue;    // BLAST e-value
				float bits;      // alignment score in bits
				int bdbh;        // if bdbh == 2 the protein pair corresponds to a bdbh
				int rank;        // rank == 1 if target protein comes first in the list of homologs else set to 2
				if ( ! iIsJInHmat ) {
					q_organims_id = glist1.organism_id();
					q_element_id = glist1.element_id(mapitr->first);
					q_gene_id = glist1.gene_id(mapitr->first);
					s_organims_id = glist2.organism_id();
					s_element_id = glist2.element_id(idxSubject);
					s_gene_id = glist2.gene_id(idxSubject);

					pid = hmat.blast_res(mapitr->first, idxSubject).pid;
					hsp_len = hmat.blast_res(mapitr->first, idxSubject).hsp_len;
					mismatches = hmat.blast_res(mapitr->first, idxSubject).mismatches;
					gaps = hmat.blast_res(mapitr->first, idxSubject).gaps;
					qstart = hmat.blast_res(mapitr->first, idxSubject).qstart;
					qend = hmat.blast_res(mapitr->first, idxSubject).qend;
					qlength = hmat.blast_res(mapitr->first, idxSubject).qlength;
					hstart = hmat.blast_res(mapitr->first, idxSubject).hstart;
					hend = hmat.blast_res(mapitr->first, idxSubject).hend;
					hlength = hmat.blast_res(mapitr->first, idxSubject).hlength;
					evalue = hmat.blast_res(mapitr->first, idxSubject).evalue;
					bits = hmat.blast_res(mapitr->first, idxSubject).bits;
					bdbh = hmat.blast_res(mapitr->first, idxSubject).bdbh;
					rank = hmat.blast_res(mapitr->first, idxSubject).rank;

				}else {
					q_organims_id = glist2.organism_id();
					q_element_id = glist2.element_id(mapitr->first);
					q_gene_id = glist2.gene_id(mapitr->first);
					s_organims_id = glist1.organism_id();
					s_element_id = glist1.element_id(idxSubject);
					s_gene_id = glist1.gene_id(idxSubject);

					pid = hmat.blast_res(idxSubject, mapitr->first).pid;
					hsp_len = hmat.blast_res(idxSubject, mapitr->first).hsp_len;
					mismatches = hmat.blast_res(idxSubject, mapitr->first).mismatches;
					gaps = hmat.blast_res(idxSubject, mapitr->first).gaps;
					qstart = hmat.blast_res(idxSubject, mapitr->first).hstart;
					qend = hmat.blast_res(idxSubject, mapitr->first).hend;
					qlength = hmat.blast_res(idxSubject, mapitr->first).hlength;
					hstart = hmat.blast_res(idxSubject, mapitr->first).qstart;
					hend = hmat.blast_res(idxSubject, mapitr->first).qend;
					hlength = hmat.blast_res(idxSubject, mapitr->first).qlength;
					evalue = hmat.blast_res(idxSubject, mapitr->first).evalue;
					bits = hmat.blast_res(idxSubject, mapitr->first).bits;
					bdbh = hmat.blast_res(idxSubject, mapitr->first).bdbh;
					rank = hmat.blast_res(idxSubject, mapitr->first).rank;

				}
				float percent_within_best_matchs = bits / (float) BestBitsAmongAll;
				//print as string
				//header = (close_best_match_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, isMirrorData, s_rank_among_all_close_best_match
				//percent_within_best_matchs,
				//pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank
				//)
				ostringstream myString;
				myString << CLOSE_BEST_MATCH_ID << "\t";
				myString << q_organims_id << "\t";
				myString << q_element_id << "\t";
				myString << q_gene_id << "\t";
				myString << s_organims_id << "\t";
				myString << s_element_id << "\t";
				myString << s_gene_id << "\t";
				myString << iIsJInHmat << "\t";
				myString << s_rank_among_all_close_best_match << "\t";
				myString << percent_within_best_matchs << "\t";
				myString << pid << "\t";
				myString << hsp_len << "\t";
				myString << mismatches << "\t";
				myString << gaps << "\t";
				myString << qstart << "\t";
				myString << qend << "\t";
				myString << qlength << "\t";
				myString << hstart << "\t";
				myString << hend << "\t";
				myString << hlength << "\t";
				myString << evalue << "\t";
				myString << bits << "\t";
				myString << bdbh << "\t";
				myString << rank;
				vecToReturn.push_back(myString.str());

				s_rank_among_all_close_best_match++;
			}*/
		}


	}


	return vecToReturn;
}

vector<pair<int, int>> mergeRangeVector(
		vector<pair<int, int>> &vectorToMerge)
{
	vector<pair<int, int> > mergedToReturn;
	if (vectorToMerge.empty()) {
		return mergedToReturn;
	}
	std::sort(vectorToMerge.begin(),vectorToMerge.end());
	vector<pair<int, int> >::iterator it = vectorToMerge.begin();
	pair<int,int> current = *(it)++;
	while (it != vectorToMerge.end()){
		if (current.second > it->first){ // you might want to change it to >=
			current.second = std::max(current.second, it->second);
		} else {
			mergedToReturn.push_back(current);
			current = *(it);
		}
		it++;
	}
	mergedToReturn.push_back(current);
	return mergedToReturn;
}


vector<string> getVecProtFusion(
		  MapIdx2setGeneFusionCheckerObj &mi2sgfco
		  , HomologyMatrix &hmat
		  , bool iIsJInHmat
		  , GeneList &glist1
		  , GeneList &glist2
	  	  ) {
	vector<string> vecToReturn;
	for (auto mapitr = mi2sgfco.begin(); mapitr != mi2sgfco.end(); ++mapitr) {
		//idx i = mapitr->first
		int lengthOfQuery = -1;

		vector<int> validContributorProtalign_vecIdx;
		vector<int> vecLengthTotalCoverageSOnQ;
		vector<int> vecLenghtNewContribCoverageSOnQ;
		vector<pair<int, int>> vecAlreadyAlignedInMainProt;
		vector<int> validContributorProtalign_idxInmMapitrSecond;
		bool firstIter = true;
		bool performAFusionAnalysis = true;
		int geneIdBestSingleLongMatchHidingProtFusion = -1;
		int numberSingleLongMatchHidingProtFusion = 0;
		int numberProtFusionDetectedBeforeThisOne = -1;
		do {

			int idxInmMapitrSecond = -1;
			for(auto alnrGeneFusionIT : mapitr->second) {
				idxInmMapitrSecond++;
				// use GeneFusionCheckerObj setGeneFusionIT here
				/*cout << "getVecProtFusion iIsJInHmat=" << iIsJInHmat
						<< " ; idx i=" << mapitr->first
						<< " ; idx j=" << alnrGeneFusionIT._idx
						<< " ; _bits=" << alnrGeneFusionIT._bits
						<< " ; _alignment_size_on_query=" << alnrGeneFusionIT._alignment_size_on_query
						<< endl;*/
				int startBProtAlignOnQuery;
				int endBProtAlignOnQuery;
				int lengthOfSubject;
				if ( ! iIsJInHmat) {
					startBProtAlignOnQuery = hmat.blast_res(mapitr->first, alnrGeneFusionIT.idx()).qstart;
					endBProtAlignOnQuery = hmat.blast_res(mapitr->first, alnrGeneFusionIT.idx()).qend;
					lengthOfQuery = hmat.blast_res(mapitr->first, alnrGeneFusionIT.idx()).qlength;
					lengthOfSubject = hmat.blast_res(mapitr->first, alnrGeneFusionIT.idx()).hlength;
				} else {
					startBProtAlignOnQuery = hmat.blast_res(alnrGeneFusionIT.idx(), mapitr->first).hstart;
					endBProtAlignOnQuery = hmat.blast_res(alnrGeneFusionIT.idx(), mapitr->first).hend;
					lengthOfQuery = hmat.blast_res(alnrGeneFusionIT.idx(), mapitr->first).hlength;
					lengthOfSubject = hmat.blast_res(alnrGeneFusionIT.idx(), mapitr->first).qlength;
				}

				

				if (firstIter) {
					//first longuest protalign
					validContributorProtalign_vecIdx.push_back(alnrGeneFusionIT.idx());
					vecLengthTotalCoverageSOnQ.push_back(endBProtAlignOnQuery-startBProtAlignOnQuery + 1);
					vecLenghtNewContribCoverageSOnQ.push_back(endBProtAlignOnQuery-startBProtAlignOnQuery + 1);
					pair<int,int> current =  std::make_pair(startBProtAlignOnQuery,endBProtAlignOnQuery);
					vecAlreadyAlignedInMainProt.push_back(current);
					validContributorProtalign_idxInmMapitrSecond.push_back(idxInmMapitrSecond);
					/*cout << "\t firstIter"
							<< endl;*/

				} else {
					//check against valid_contributor_protalign to see if valid
					vector<pair<int, int>> partOfAlignItAlreadyCoveredInValidContributorProtalign;
					for(auto const& value: vecAlreadyAlignedInMainProt) {
						/* std::cout << vecNonAlignedInMainProt_startStopLooped[i]; ... */
						int startAProtAlignContributorIT = value.first;
						int endAProtAlignContributorIT = value.second;
						if (startBProtAlignOnQuery >= endAProtAlignContributorIT) {
							/*cout << "\t case 1"
									<< endl;*/
							// case 1 : B whole new right on A
							// add nothing to partOfAlignItAlreadyCoveredInValidContributorProtalign_startStopLooped
						} else if (startAProtAlignContributorIT >= endBProtAlignOnQuery) {
							/*cout << "\t case 2"
									<< endl;*/
							// case 2 : B whole new left on A
							// add nothing to partOfAlignItAlreadyCoveredInValidContributorProtalign_startStopLooped
						} else if (startBProtAlignOnQuery >= startAProtAlignContributorIT && endBProtAlignOnQuery <= endAProtAlignContributorIT) {
							/*cout << "\t case 3"
									<< endl;*/
							// case 3 : B included totally in A
							pair<int,int> current =  std::make_pair(startBProtAlignOnQuery,endBProtAlignOnQuery);
							partOfAlignItAlreadyCoveredInValidContributorProtalign.push_back(current);
						} else if (startAProtAlignContributorIT  >= startBProtAlignOnQuery && endAProtAlignContributorIT <= endBProtAlignOnQuery) {
							/*cout << "\t case 4"
									<< endl;*/
							// case 4 : A included totally in B
							pair<int,int> current =  std::make_pair(startAProtAlignContributorIT,endAProtAlignContributorIT);
							partOfAlignItAlreadyCoveredInValidContributorProtalign.push_back(current);
						} else if (
								startBProtAlignOnQuery >= startAProtAlignContributorIT
								&& startBProtAlignOnQuery < endAProtAlignContributorIT
								&& endBProtAlignOnQuery > endAProtAlignContributorIT
						) {
							/*cout << "\t case 5"
									<< endl;*/
							// case 5 : B overlapp partially with A but add new alignment on the right side
							pair<int,int> current =  std::make_pair(startBProtAlignOnQuery,endAProtAlignContributorIT);
							partOfAlignItAlreadyCoveredInValidContributorProtalign.push_back(current);
						} else if (
								endBProtAlignOnQuery > startAProtAlignContributorIT
								&& endBProtAlignOnQuery <= endAProtAlignContributorIT
								&& startBProtAlignOnQuery < startAProtAlignContributorIT
						) {
							/*cout << "\t case 6"
									<< endl;*/
							// case 6 : B overlapp partially with A but add new alignment on the left side
							pair<int,int> current =  std::make_pair(startAProtAlignContributorIT,endBProtAlignOnQuery);
							partOfAlignItAlreadyCoveredInValidContributorProtalign.push_back(current);
						}
					}
					//check what remain of IT after removing partOfAlignItAlreadyCoveredInValidContributorProtalign_startStopLooped
					vector<pair<int, int> > mergedAlreadyCovered = mergeRangeVector(partOfAlignItAlreadyCoveredInValidContributorProtalign);
					int removeFromNewContribByB = 0;
					for(auto const& value: mergedAlreadyCovered) {
						int lenghtToRemove = value.second - value.first + 1;
						removeFromNewContribByB += lenghtToRemove;
					}
					int lenghtNewContribByB = endBProtAlignOnQuery - startBProtAlignOnQuery + 1 - removeFromNewContribByB;
					/*cout << "\tTest significance ?"
							<< " ; lenghtNewContribByB=" << lenghtNewContribByB
							<< " ; compared to " << (lengthOfSubject * GlobalParams::geneFusions_minPercentLenghtProtThreshold)
							<< " ; endBProtAlignOnQuery=" << endBProtAlignOnQuery
							<< " ; startBProtAlignOnQuery=" << startBProtAlignOnQuery
							<< " ; removeFromNewContribByB=" << removeFromNewContribByB
							<< endl;*/
					//Is it significant ?
					if ( lenghtNewContribByB >= (lengthOfSubject * GlobalParams::geneFusions_minPercentLenghtProtThreshold) ) {
						/*cout << "\t => significant"
								<< endl;*/
						//contrib is significative, store as contributor
						validContributorProtalign_vecIdx.push_back(alnrGeneFusionIT.idx());
						vecLengthTotalCoverageSOnQ.push_back(endBProtAlignOnQuery-startBProtAlignOnQuery+1);
						vecLenghtNewContribCoverageSOnQ.push_back(lenghtNewContribByB);
						pair<int,int> current =  std::make_pair(startBProtAlignOnQuery,endBProtAlignOnQuery);
						vecAlreadyAlignedInMainProt.push_back(current);
						vecAlreadyAlignedInMainProt = mergeRangeVector(vecAlreadyAlignedInMainProt);
						validContributorProtalign_idxInmMapitrSecond.push_back(idxInmMapitrSecond);
					} else {
						/*cout << "\t NOT significant"
								<< endl;*/
					}
				}
				firstIter = false;

				//check how much non aligned lenght is still left, do not continue if < minPercentQueryProtThresholdSignificantContribFusion
				if ( ! vecAlreadyAlignedInMainProt.empty()) {
					int lenghtAlreadyAligned = 0;
					for(auto const& value: vecAlreadyAlignedInMainProt) {
						int lenghtToAdd = value.second - value.first + 1;
						lenghtAlreadyAligned += lenghtToAdd;
					}
					if ( ( (float) lenghtAlreadyAligned / (float) lengthOfQuery ) > 0.98 ) {
						/*cout << "\t no much room available break"
								<< endl;*/
						break; // no much room available
					}
				}

			}


			int queryProtCoverageLenght = 0;
			for(auto const& value: vecAlreadyAlignedInMainProt) {
				int lenghtCovered = value.second - value.first + 1;
				queryProtCoverageLenght += lenghtCovered;
			}

			/*cout << "Done analysing GeneFusionCheckerObj"
					<< " ; queryProtCoverageLenght=" << queryProtCoverageLenght
					<< " ; lengthOfQuery=" << lengthOfQuery
					<< " ; validContributorProtalign_vecIdx.size()=" << validContributorProtalign_vecIdx.size()
					<< endl;*/

			if ( (float) ( (float) queryProtCoverageLenght / (float) lengthOfQuery) >= GlobalParams::geneFusions_minPercentLenghtProtThreshold) {
				/*cout << "\t covergae lenght above threshold: "
						<< (float) queryProtCoverageLenght / (float) lengthOfQuery
						<< endl;*/

				if (validContributorProtalign_vecIdx.size() == 1) {
					numberSingleLongMatchHidingProtFusion++;
					if (geneIdBestSingleLongMatchHidingProtFusion == -1) {
						if ( ! iIsJInHmat ) {
							geneIdBestSingleLongMatchHidingProtFusion = glist2.gene_id(validContributorProtalign_vecIdx[0]);
						} else {
							geneIdBestSingleLongMatchHidingProtFusion = glist1.gene_id(validContributorProtalign_vecIdx[0]);
						}
					}
					//singleLongMatchWasHidingProtFusion = true;
					// del this long prot and start over
					mapitr->second.erase(mapitr->second.begin());
					/*cout << "\t singleLongMatchWasHidingProtFusion ; size after erase: "
							<< mapitr->second.size()
							<< endl;*/
					if ( ! mapitr->second.empty()) {
						validContributorProtalign_vecIdx.clear();
						vecLengthTotalCoverageSOnQ.clear();
						vecLenghtNewContribCoverageSOnQ.clear();
						vecAlreadyAlignedInMainProt.clear();
						vecLenghtNewContribCoverageSOnQ.clear();
						validContributorProtalign_idxInmMapitrSecond.clear();
						performAFusionAnalysis = true;
						firstIter = true;
					} else {
						performAFusionAnalysis = false;
					}
				} else if (validContributorProtalign_vecIdx.size() > 1) {
					numberProtFusionDetectedBeforeThisOne++;
					/*cout << "\t printing valid Contributors"
							<< endl;*/

					// we have a prot fusion, store in vecToReturn as String
					PROT_FUSION_ID++;
					//lengthOfQuery
					int s_rank_among_all_s_fusions = 0;
					for(auto const& idxSubject: validContributorProtalign_vecIdx) {
						//header = prot_fusion (prot_fusion_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, isMirrorData, s_rank_among_all_s_fusions,
						//lengthTotalCoverageSOnQ, lenghtNewContribCoverageSOnQ,
						//pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank
						//, geneIdBestSingleLongMatchHidingProtFusion, numberSingleLongMatchHidingProtFusion, numberProtFusionDetectedBeforeThisOne
						//)
						int q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id;
						float pid;       // percentage identity in alignment
						int hsp_len;     // alignment length
						int mismatches;  // number of mismatches
						int gaps;        // number of gaps
						int qstart;      // start of alignment in query protein
						int qend;        // end of alignment in query protein
						int qlength;     // query protein length (in aa)
						int hstart;      // start of alignment in target protein
						int hend;       // end of alignment in target protein
						int hlength;     // target protein length (in aa)
						float evalue;    // BLAST e-value
						float bits;      // alignment score in bits
						int bdbh;        // if bdbh == 2 the protein pair corresponds to a bdbh
						int rank;        // rank == 1 if target protein comes first in the list of homologs else set to 2
						if ( ! iIsJInHmat ) {
							q_organims_id = glist1.organism_id();
							q_element_id = glist1.element_id(mapitr->first);
							q_gene_id = glist1.gene_id(mapitr->first);
							s_organims_id = glist2.organism_id();
							s_element_id = glist2.element_id(idxSubject);
							s_gene_id = glist2.gene_id(idxSubject);

							pid = hmat.blast_res(mapitr->first, idxSubject).pid;
							hsp_len = hmat.blast_res(mapitr->first, idxSubject).hsp_len;
							mismatches = hmat.blast_res(mapitr->first, idxSubject).mismatches;
							gaps = hmat.blast_res(mapitr->first, idxSubject).gaps;
							qstart = hmat.blast_res(mapitr->first, idxSubject).qstart;
							qend = hmat.blast_res(mapitr->first, idxSubject).qend;
							qlength = hmat.blast_res(mapitr->first, idxSubject).qlength;
							hstart = hmat.blast_res(mapitr->first, idxSubject).hstart;
							hend = hmat.blast_res(mapitr->first, idxSubject).hend;
							hlength = hmat.blast_res(mapitr->first, idxSubject).hlength;
							evalue = hmat.blast_res(mapitr->first, idxSubject).evalue;
							bits = hmat.blast_res(mapitr->first, idxSubject).bits;
							bdbh = hmat.blast_res(mapitr->first, idxSubject).bdbh;
							rank = hmat.blast_res(mapitr->first, idxSubject).rank;

						}else {
							q_organims_id = glist2.organism_id();
							q_element_id = glist2.element_id(mapitr->first);
							q_gene_id = glist2.gene_id(mapitr->first);
							s_organims_id = glist1.organism_id();
							s_element_id = glist1.element_id(idxSubject);
							s_gene_id = glist1.gene_id(idxSubject);

							pid = hmat.blast_res(idxSubject, mapitr->first).pid;
							hsp_len = hmat.blast_res(idxSubject, mapitr->first).hsp_len;
							mismatches = hmat.blast_res(idxSubject, mapitr->first).mismatches;
							gaps = hmat.blast_res(idxSubject, mapitr->first).gaps;
							qstart = hmat.blast_res(idxSubject, mapitr->first).hstart;
							qend = hmat.blast_res(idxSubject, mapitr->first).hend;
							qlength = hmat.blast_res(idxSubject, mapitr->first).hlength;
							hstart = hmat.blast_res(idxSubject, mapitr->first).qstart;
							hend = hmat.blast_res(idxSubject, mapitr->first).qend;
							hlength = hmat.blast_res(idxSubject, mapitr->first).qlength;
							evalue = hmat.blast_res(idxSubject, mapitr->first).evalue;
							bits = hmat.blast_res(idxSubject, mapitr->first).bits;
							bdbh = hmat.blast_res(idxSubject, mapitr->first).bdbh;
							rank = hmat.blast_res(idxSubject, mapitr->first).rank;

						}
						int lengthTotalCoverageSOnQ = vecLengthTotalCoverageSOnQ[s_rank_among_all_s_fusions];
						int lenghtNewContribCoverageSOnQ = vecLenghtNewContribCoverageSOnQ[s_rank_among_all_s_fusions];
						//make and store string

						ostringstream myString;
						myString << PROT_FUSION_ID << "\t";
						myString << q_organims_id << "\t";
						myString << q_element_id << "\t";
						myString << q_gene_id << "\t";
						myString << s_organims_id << "\t";
						myString << s_element_id << "\t";
						myString << s_gene_id << "\t";
						myString << iIsJInHmat << "\t";
						myString << s_rank_among_all_s_fusions << "\t";
						myString << lengthTotalCoverageSOnQ << "\t";
						myString << lenghtNewContribCoverageSOnQ << "\t";
						myString << pid << "\t";
						myString << hsp_len << "\t";
						myString << mismatches << "\t";
						myString << gaps << "\t";
						myString << qstart << "\t";
						myString << qend << "\t";
						myString << qlength << "\t";
						myString << hstart << "\t";
						myString << hend << "\t";
						myString << hlength << "\t";
						myString << evalue << "\t";
						myString << bits << "\t";
						myString << bdbh << "\t";
						myString << rank << "\t";
						myString << geneIdBestSingleLongMatchHidingProtFusion << "\t";
						myString << numberSingleLongMatchHidingProtFusion << "\t";
						myString << numberProtFusionDetectedBeforeThisOne;
						vecToReturn.push_back(myString.str());

						/*string stToStore = "";
						stToStore.append(std::to_string(PROT_FUSION_ID));
						stToStore.append("\t");
						stToStore.append(std::to_string(q_organims_id));
						stToStore.append("\t");
						stToStore.append(std::to_string(q_element_id));
						stToStore.append("\t");
						stToStore.append(std::to_string(q_gene_id));
						stToStore.append("\t");
						stToStore.append(std::to_string(s_organims_id));
						stToStore.append("\t");
						stToStore.append(std::to_string(s_element_id));
						stToStore.append("\t");
						stToStore.append(std::to_string(s_gene_id));
						stToStore.append("\t");
						stToStore.append(std::to_string(s_rank_among_all_s_fusions));
						stToStore.append("\t");
						stToStore.append(std::to_string(lengthTotalCoverageSOnQ));
						stToStore.append("\t");
						stToStore.append(std::to_string(lenghtNewContribCoverageSOnQ));
						stToStore.append("\t");
						stToStore.append(std::to_string(pid));
						stToStore.append("\t");
						stToStore.append(std::to_string(hsp_len));
						stToStore.append("\t");
						stToStore.append(std::to_string(mismatches));
						stToStore.append("\t");
						stToStore.append(std::to_string(gaps));
						stToStore.append("\t");
						stToStore.append(std::to_string(qstart));
						stToStore.append("\t");
						stToStore.append(std::to_string(qend));
						stToStore.append("\t");
						stToStore.append(std::to_string(qlength));
						stToStore.append("\t");
						stToStore.append(std::to_string(hstart));
						stToStore.append("\t");
						stToStore.append(std::to_string(hend));
						stToStore.append("\t");
						stToStore.append(std::to_string(hlength));
						stToStore.append("\t");
						stToStore.append(std::to_string(evalue));
						stToStore.append("\t");
						stToStore.append(std::to_string(bits));
						stToStore.append("\t");
						stToStore.append(std::to_string(bdbh));
						stToStore.append("\t");
						stToStore.append(std::to_string(rank));
						vecToReturn.push_back(stToStore);*/

						s_rank_among_all_s_fusions++;
					}

					/*cout << "\t Erasing mapitr->second ; size before: "
							<< mapitr->second.size()
							<< endl;*/
					for (vector<int>::reverse_iterator riter = validContributorProtalign_idxInmMapitrSecond.rbegin(); riter != validContributorProtalign_idxInmMapitrSecond.rend(); ++riter ) {
						auto it = std::next(mapitr->second.begin(), *riter);
						/*cout << "\t Erasing idx "
								<< *riter
								<< endl;*/
						mapitr->second.erase(it);
					}

					/*cout << "\t Erased mapitr->second ; size after: "
							<< mapitr->second.size()
							<< endl;*/

					if ( ! mapitr->second.empty() ) {
						/*cout << "\t ! mapitr->second.empty(), continuing: "
								<< mapitr->second.size()
								<< endl;*/
						validContributorProtalign_vecIdx.clear();
						vecLengthTotalCoverageSOnQ.clear();
						vecLenghtNewContribCoverageSOnQ.clear();
						vecAlreadyAlignedInMainProt.clear();
						vecLenghtNewContribCoverageSOnQ.clear();
						validContributorProtalign_idxInmMapitrSecond.clear();
						performAFusionAnalysis = true;
						firstIter = true;
					} else {
						/*cout << "\t mapitr->second.empty(), stoping: "
								<< mapitr->second.size()
								<< endl;*/
						performAFusionAnalysis = false;
					}
				}
			} else {
				/*cout << "\t covergae lenght below threshold: "
						<< (float) queryProtCoverageLenght / (float) lengthOfQuery
						<< endl;*/
				performAFusionAnalysis = false;
			}
		} while (performAFusionAnalysis);
	}
	return vecToReturn;
}




int main(int argc, char **argv) {
  
  float ortholog_score = ORTHOLOG_SCORE;
  float homolog_score = HOMOLOG_SCORE;
  float mismatch_penalty = MISMATCH_PENALTY;
  float gap_creation_penalty = GAP_CREATION_PENALTY;
  float max_gap_size_for_creation_penalty = MAX_GAP_SIZE_CREATION_PENALTY;
  float gap_extension_penalty = GAP_EXTENSION_PENALTY;
  int min_align_size = MIN_ALIGNMENT_SIZE;
  float min_score = CUTOFF_SCORE;
  bool include_all_orthologs = INCLUDE_ALL_ORTHOLOGS;
  bool print_out_tandem_dups = PRINT_OUT_TANDEM_DUPS;
  //GlobalParams::print_out_gene_fusions = PRINT_OUT_GENE_FUSIONS;
  //char *filter_file = NULL;
  //bool filter_file_flag=false;
  string blast_output_file("void");
  int organism_id_1=-1;
  int organism_id_2=-1;
  string archiveDir("./");
  float prot_frac = PROT_FRAC ; // Two proteins are considered as orthologs if they give rise to a bdbh 
                                // AND if alignment_length / query_length > prot_frac AND alignment_length / target_length > prot_frac
  
  if(argc == 1) {
    usage(argv[0]);
    exit(0);
  }

  for (int i=1;i<argc;i +=2) {
    if (!strcmp(argv[i],"-os")) {
      ortholog_score=atof(argv[i+1]);
    } else if (!strcmp(argv[i],"-pf")) {
      prot_frac=atof(argv[i+1]);
      if(prot_frac < 0.0 || prot_frac > 1.0) {
	cerr << "Error: parameter prot_frac must lie between 0 and 1 included. Here prot_frac= " << prot_frac << endl;
	exit(EXIT_FAILURE);
      }
    } else if (!strcmp(argv[i],"-hs")) {
      homolog_score=atof(argv[i+1]);
    } else if (!strcmp(argv[i],"-mp")) {
      mismatch_penalty=atof(argv[i+1]);
    } else if (!strcmp(argv[i],"-gc")) {
      gap_creation_penalty=atof(argv[i+1]);
    } else if (!strcmp(argv[i],"-mgsc")) {
      max_gap_size_for_creation_penalty=atof(argv[i+1]);
    } else if (!strcmp(argv[i],"-ge")) {
      gap_extension_penalty=atof(argv[i+1]);
    } else if (!strcmp(argv[i],"-m")) {
      min_align_size=std::stoi(argv[i+1]);
    } else if (!strcmp(argv[i],"-c")) {
      min_score=atof(argv[i+1]);
    } else if (!strcmp(argv[i],"-o")) {
      include_all_orthologs=false;
      if ( argv[i+1][0] == 't' || argv[i+1][0] == 'T' ||
    		  argv[i+1][0] == 'y' || argv[i+1][0] == 'Y' ||
			  argv[i+1][0] == '1') {
    	  include_all_orthologs=true;
      }
    } else if (!strcmp(argv[i],"-potd")) {
    	print_out_tandem_dups=false;
      if ( argv[i+1][0] == 't' || argv[i+1][0] == 'T' ||
    		  argv[i+1][0] == 'y' || argv[i+1][0] == 'Y' ||
			  argv[i+1][0] == '1') {
    	  print_out_tandem_dups=true;
      }
    } else if (!strcmp(argv[i],"-pogf")) {
    	GlobalParams::print_out_gene_fusions=false;
      if ( argv[i+1][0] == 't' || argv[i+1][0] == 'T' ||
    		  argv[i+1][0] == 'y' || argv[i+1][0] == 'Y' ||
			  argv[i+1][0] == '1') {
    	  GlobalParams::print_out_gene_fusions=true;
      }
    } else if (!strcmp(argv[i],"-pocbm")) {
    	GlobalParams::print_out_close_best_match=false;
      if ( argv[i+1][0] == 't' || argv[i+1][0] == 'T' ||
    		  argv[i+1][0] == 'y' || argv[i+1][0] == 'Y' ||
			  argv[i+1][0] == '1') {
    	  GlobalParams::print_out_close_best_match=true;
      }
    } else if(!strcmp(argv[i],"-org1")) {
      organism_id_1 = std::stoi(argv[i+1]);
    } else if(!strcmp(argv[i],"-org2")) {
      organism_id_2 = std::stoi(argv[i+1]);
    } else if(!strcmp(argv[i],"-fn")) {
      blast_output_file = argv[i+1];
    } else if(!strcmp(argv[i],"-Adir")) {
      archiveDir = argv[i+1];
    } else {
      cerr << "Error: unknown command line option " << argv[i] << endl;
      usage(argv[0]);
      exit(EXIT_FAILURE);
    }

  }
    
  if(blast_output_file.compare("void") == 0) {
    cerr << "Error: you must specify a BLAST output file" << endl;
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  if(organism_id_1 == -1 || organism_id_2 == -1) {
    cerr << "Error: you must specify two organism_id's" << endl;
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /*
  if (gap_creation_penalty!=gap_extension_penalty) {
    cerr << "Error: affine gap function not yet implemented" << endl;
    cerr << "       affine gap function has been disabled wrt older versions due to a bug" << endl;
    cerr << "       set gap_creation_penalty==gap_extension_penalty using args -gc -ge" << endl;
    exit(1);
  }*/

  ScoreNormalizer *sn=new ScoreNormalizer(ortholog_score,
					  homolog_score,
					  mismatch_penalty);
  //
  // Read the required data on archive files and BLAST output file.
  //

  GeneList glist1(organism_id_1,archiveDir);
  GeneList glist2(organism_id_2,archiveDir);

  HomologyMatrix hmat(glist1, glist2, blast_output_file, prot_frac, sn); // , print_out_gene_fusions


  //
  // Initialize data structures for dynamic programming alignment
  //
  AlignmentFactory afactory(&glist1,&glist2,&hmat);
  afactory.set_gap_creation_penalty(gap_creation_penalty);
  afactory.set_max_gap_size_for_creation_penalty(max_gap_size_for_creation_penalty);
  afactory.set_gap_extension_penalty(gap_extension_penalty);
  afactory.set_min_align_size(min_align_size);
  afactory.set_min_score(min_score);
  afactory.set_ortholog_score(ortholog_score);
  afactory.set_include_all_orthologs(include_all_orthologs);
  afactory.set_print_out_tandem_dups(print_out_tandem_dups);

  //
  // Dump data for the ORIGAMI table 'alignment_params'
  // only [element_id_1 vs element_id_2] not [element_id_2 and element_id_1]
  //

  string ortho_included;
  ofstream fp_out, fp_out1, fp_out2;
  char outputfile[1024];
  sprintf(outputfile,"%d_and_%d_alignment_params_table.tsv",organism_id_1,organism_id_2);
  fp_out.open(outputfile);
  if(afactory.get_include_all_orthologs() == 1) {
    ortho_included = "T";
  } else {
    ortho_included = "F";
  }

  fp_out <<  "BEGIN WORK;\n";
  fp_out <<  "COPY alignment_params (alignment_param_id, q_organism_id, q_element_id, s_organism_id, s_element_id, ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, max_gap_size_for_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac, geneFusions_minPctLenghtProtThres, closeBestMatchs_minPctThres, tandemDups_minPctWithinBestPidThres) FROM stdin;\n";

  // element_id_1 vs element_id_2
  int element_id_1=-1;
  for (int ig1=0; ig1<glist1.size(); ig1++) {
    if (glist1.element_id(ig1)!=element_id_1) {
      element_id_1=glist1.element_id(ig1);
      int element_id_2=-1;
      for (int ig2=0; ig2<glist2.size(); ig2++) {
		if (glist2.element_id(ig2)!=element_id_2) {
		  element_id_2=glist2.element_id(ig2);

		  if ( ( organism_id_1 == organism_id_2 ) && (element_id_1 > element_id_2) ) {
			  //do not print duplicate if organism_id_1 == organism_id_2
		  } else {
			  fp_out <<  "ALN_" << element_id_1 << "_" << element_id_2 << "\t";
			  fp_out << glist1.organism_id() << "\t" << element_id_1 << "\t" << glist2.organism_id() << "\t" << element_id_2 << "\t";
			  fp_out << ortholog_score << "\t" << homolog_score << "\t" << mismatch_penalty << "\t";
			  fp_out << afactory.get_gap_creation_penalty() << "\t" << afactory.get_max_gap_size_for_creation_penalty() << "\t" <<  afactory.get_gap_extension_penalty() << "\t";
			  fp_out << afactory.get_min_align_size() << "\t" << afactory.get_min_score() << "\t";
			  fp_out <<  ortho_included << "\t" << prot_frac << "\t";
			  fp_out <<  GlobalParams::geneFusions_minPercentLenghtProtThreshold << "\t";
			  fp_out <<  GlobalParams::closeBestMatchs_minPercentThreshold << "\t";
			  fp_out <<  GlobalParams::tandemDups__minPercentWithinBestPidThreshold << "\n";
		  }
		  // element_id_2 vs element_id_1
		  /* commented as we do not want mirror data
		  if(element_id_1 != element_id_2){//ajout thomas
			fp_out <<  "ALN_" << element_id_2 << "_" << element_id_1 << "\t";
			fp_out << glist2.organism_id() << "\t" << element_id_2 << "\t" << glist1.organism_id() << "\t" << element_id_1 << "\t";
			fp_out << ortholog_score << "\t" << homolog_score << "\t" << mismatch_penalty << "\t";
			fp_out << afactory.get_gap_creation_penalty() << "\t" << afactory.get_max_gap_size_for_creation_penalty() << "\t" <<  afactory.get_gap_extension_penalty() << "\t";
			fp_out << afactory.get_min_align_size() << "\t" << afactory.get_min_score() << "\t";
			fp_out << ortho_included << "\t" << prot_frac << "\n";
		  }*/
		}
      }
    }
  }
  fp_out <<  "\\.\n";
  fp_out <<  "COMMIT WORK;\n";

  fp_out.close();
  
  //
  // Perform the dynamic programming alignment of the genes of the two elements
  //

  multiset<Alignment> alignset=afactory.get_alignments();
  vector<string> vecIsBranchedToAnotherSynteny;

  //
  // Dump the alignment data on files
  // Create the ORIGAMI 'alignment_pairs', 'alignments' and 'homologies' tables
  // Data are dumped only for the pair: [element_id_1 vs element_id_2]
  //


  int mismatches, gaps, total_gap_size;
  int type = -1;
  string orientation;

  sprintf(outputfile,"%d_and_%d_alignment_table.tsv",organism_id_1,organism_id_2);
  fp_out.open(outputfile);
  fp_out <<  "BEGIN WORK;\n";
  fp_out <<  "COPY alignments (alignment_id, alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_size_kb, s_size_kb, q_start, q_stop, s_start, s_stop) FROM stdin;\n";

  sprintf(outputfile,"%d_and_%d_alignment_pairs_table.tsv",organism_id_1,organism_id_2);
  fp_out1.open(outputfile);
  fp_out1 <<  "BEGIN WORK;\n";
  fp_out1 <<  "COPY alignment_pairs (alignment_id, q_gene_id, s_gene_id, type) FROM stdin;\n";

  sprintf(outputfile,"%d_and_%d_homologies_table.tsv",organism_id_1,organism_id_2);
  fp_out2.open(outputfile);
  fp_out2 << "BEGIN WORK;\n";
  fp_out2 << "COPY homologies (q_organism_id,q_element_id,q_gene_id,q_length,s_organism_id,s_element_id, s_gene_id, s_length, identity, score, e_value, rank, q_first, q_first_frac, q_last, q_last_frac, q_align_length, q_align_frac, s_first, s_first_frac, s_last, s_last_frac, s_align_length, s_align_frac) FROM stdin;\n";





  set<Alignment>::reverse_iterator rit;
  int counter_alignment = 0;
  for (rit=alignset.rbegin(); rit != alignset.rend(); rit++) {

	  if ( ( organism_id_1 == organism_id_2 ) && ( glist1.element_id(glist1.gene_index(rit->query_gene_id(0))) > glist2.element_id(glist2.gene_index(rit->subject_gene_id(0))) ) ) {
		  //do not print duplicate if organism_id_1 == organism_id_2
		  continue;
	  }


	  counter_alignment++;
	  fp_out << counter_alignment << "\t" << "ALN_" << glist1.element_id(glist1.gene_index(rit->query_gene_id(0))) << "_" << glist2.element_id(glist2.gene_index(rit->subject_gene_id(0))) << "\t";

	  if(rit->orientation() == 0) {
		  orientation = "T";
	  } else {
		  orientation = "F";
	  }
	  fp_out << rit->score() << "\t" << rit->orthologs() + rit->homologs() << "\t" <<  orientation << "\t" << rit->orthologs() << "\t" << rit->homologs() << "\t";

	  AlignedSequence query=rit->query();
	  AlignedSequence subject=rit->subject();
	  mismatches = 0;
	  gaps = 0;
	  total_gap_size = 0;
	  for (int i=0;i<rit->size();i++) {
		  string qgene=rit->query_name(i);
		  string sgene=rit->subject_name(i);
		  int q_gene_id=rit->query_gene_id(i);
		  int s_gene_id=rit->subject_gene_id(i);
		  float homology=rit->homology_score(i);
		  //float score=rit->alignment_score(i);
		  //int qstart=rit->query_start(i);
		  //int qend=rit->query_end(i);
		  //int sstart=rit->subject_start(i);
		  //int send=rit->subject_end(i);
		  if(homology == ortholog_score) {
			  type = 1;
		  } else if(homology == homolog_score) {
			  type = 2;
		  } else if(homology == mismatch_penalty) {
			  type = 3;
			  mismatches++;
		  } else if(homology == AlignmentFactory::GAP_STARTS) { // gap_creation_penalty
			  gaps++;
			  total_gap_size++;
		  } else if(homology == AlignmentFactory::GAP_CONTINUES ) { // gap_extension_penalty
			  total_gap_size++;
		  }
		  if(qgene.compare("-") == 0) {
			  type = 4;
		  }
		  if(sgene.compare("-") == 0) {
			  type = 5;
		  }


		  // Write data for table 'alignment_pairs'
		  fp_out1 << counter_alignment << "\t";
		  fp_out1 << q_gene_id << "\t" << s_gene_id << "\t" << type;
		  //if(type == 1 || type == 2){ //ajout thomas
		  //int q_gene_index=glist1.gene_index(q_gene_id);
		  //int s_gene_index=glist2.gene_index(s_gene_id);
		  //fp_out1 << "\t" << glist1.element_id(q_gene_index) << "\t" << glist2.element_id(s_gene_index);
		  //}
		  fp_out1 << "\n";

		  // Write data for table 'homologies'
		  // RQ : rank 1 mean BDBH, rank 2 means non BDBH
		  if(type == 1 || type == 2){ //ajout thomas
			  int q_gene_index=glist1.gene_index(q_gene_id);
			  int s_gene_index=glist2.gene_index(s_gene_id);
			  BlastRes br = hmat.blast_res(q_gene_index,s_gene_index);
			  fp_out2 << glist1.organism_id() << "\t" << glist1.element_id(q_gene_index) << "\t" << q_gene_id << "\t" << br.qlength << "\t";
			  fp_out2 << glist2.organism_id() << "\t" << glist2.element_id(s_gene_index) << "\t" << s_gene_id << "\t" << br.hlength << "\t";
			  fp_out2 << br.pid << "\t" << br.bits << "\t" << br.evalue << "\t" << br.rank << "\t";
			  fp_out2 << br.qstart << "\t" << ((float) br.qstart - 1) / (float) br.qlength << "\t";
			  fp_out2 << (br.qend + 0) << "\t" << (float) (br.qend + 0) / (float) br.qlength << "\t";
			  fp_out2 << br.qend - br.qstart + 1 << "\t" << (float) (br.qend - br.qstart + 1) / (float) br.qlength << "\t";
			  fp_out2 << br.hstart << "\t" << ((float) br.hstart - 1) / (float) br.hlength << "\t";
			  fp_out2 << (br.hend + 0) << "\t" << ((float) br.hend + 0) / (float) br.hlength << "\t";
			  fp_out2 << br.hend - br.hstart + 1 << "\t" << (float) (br.hend - br.hstart + 1) / (float) br.hlength << "\n";
		  }

	  }

	  fp_out << mismatches << "\t" << gaps << "\t" << total_gap_size << "\t";
	  fp_out <<  query.size_bp()/1e3 << "\t";
	  fp_out <<  subject.size_bp()/1e3 << "\t";
	  fp_out << query.start_base() << "\t" << query.end_base() << "\t" << subject.start_base() << "\t" << subject.end_base();
	  //fp_out << "\t" << rit->isBranchedToAnotherSynteny();
	  fp_out << "\n";

	  if ( ! rit->isBranchedToAnotherSynteny().empty() ) {

		  string isBranchedToAnotherSyntenyLine = "";
		  isBranchedToAnotherSyntenyLine.append(std::to_string(counter_alignment));
		  isBranchedToAnotherSyntenyLine.append("\t");
		  isBranchedToAnotherSyntenyLine.append("ALN_");
		  isBranchedToAnotherSyntenyLine.append(std::to_string(glist1.element_id(glist1.gene_index(rit->query_gene_id(0)))));
		  isBranchedToAnotherSyntenyLine.append("_");
		  isBranchedToAnotherSyntenyLine.append(std::to_string(glist2.element_id(glist2.gene_index(rit->subject_gene_id(0)))));

		  //split rit->isBranchedToAnotherSynteny() on _ should be ixd i j
		  istringstream iq(rit->isBranchedToAnotherSynteny());
		  int Nfields = 0;
		  string token;
		  int i_branched = -1;
		  int j_branched = -1;
		  while(getline(iq,token,'_')) {
			  if (Nfields == 0) {
				  // i
				  i_branched = std::stoi( token );
			  } else if (Nfields == 1) {
				  // j
				  j_branched = std::stoi( token );
			  } else {
				  cerr << "Error rit->isBranchedToAnotherSynteny : Nfields > 1 while spliting on _ : " << rit->isBranchedToAnotherSynteny()
								<< endl;
				  exit(EXIT_FAILURE);
			  }
			  Nfields++;
		  }
		  if (i_branched >=0 && j_branched >= 0) {
			  isBranchedToAnotherSyntenyLine.append("\t");
			  isBranchedToAnotherSyntenyLine.append(std::to_string(glist1.gene_id(i_branched)));
			  isBranchedToAnotherSyntenyLine.append("\t");
			  isBranchedToAnotherSyntenyLine.append(std::to_string(glist2.gene_id(j_branched)));
		  }else {
			  cerr << "Error rit->isBranchedToAnotherSynteny : NOT i_branched >=0 && j_branched >= 0 : " << i_branched << " ; " << j_branched
					  << endl;
			  exit(EXIT_FAILURE);
		  }

		  vecIsBranchedToAnotherSynteny.push_back(isBranchedToAnotherSyntenyLine);
		  //fp_out3 << counter_alignment << "\t" << "ALN_" << glist1.element_id(glist1.gene_index(rit->query_gene_id(0))) << "_" << glist2.element_id(glist2.gene_index(rit->subject_gene_id(0))) << "\t";
	  }


  }
  
  //
  // Execute the loop once more to dump the data for tables 'alignments' and 'alignment_pairs'
  // for the pair: [element_id_2 vs element_id_1]
  //
  /* Comment to avoid printing mirror data
  if(organism_id_1 != organism_id_2){//ajout thomas
	  for (rit=alignset.rbegin(); rit != alignset.rend(); rit++) {

	    counter_alignment++;

	    fp_out << counter_alignment << "\t" << "ALN_" << glist2.element_id(glist2.gene_index(rit->subject_gene_id(0))) << "_" << glist1.element_id(glist1.gene_index(rit->query_gene_id(0))) << "\t";
	    if(rit->orientation() == 0) {
	      orientation = "T";
	    } else {
	      orientation = "F";
	    }
	    fp_out << rit->score() << "\t" << rit->size() << "\t" <<  orientation << "\t" << rit->orthologs() << "\t" << rit->homologs() << "\t";

	    AlignedSequence query=rit->query();
	    AlignedSequence subject=rit->subject();

	    mismatches = 0;
	    gaps = 0;
	    total_gap_size = 0;

	    if(rit->orientation() == 0) {
	      for (int i=0;i<rit->size();i++) {
			string qgene=rit->query_name(i);
			string sgene=rit->subject_name(i);
			int q_gene_id=rit->query_gene_id(i);
			int s_gene_id=rit->subject_gene_id(i);
			float homology=rit->homology_score(i);
			float score=rit->alignment_score(i);
			int qstart=rit->query_start(i);
			int qend=rit->query_end(i);
			int sstart=rit->subject_start(i);
			int send=rit->subject_end(i);
			if(homology == ortholog_score) {
			  type = 1;
			} else if(homology == homolog_score) {
			  type = 2;
			} else if(homology == mismatch_penalty) {
			  type = 3;
			  mismatches++;
			} else if(homology == gap_creation_penalty) {
			  gaps++;
			  total_gap_size++;
			} else if(homology == gap_extension_penalty) {
			  total_gap_size++;
			}
			if(qgene.compare("-") == 0) {
			  type = 5;
			}
			if(sgene.compare("-") == 0) {
			  type = 4;
			}

			// Write data for table 'alignment_pairs'
			fp_out1 << counter_alignment << "\t";
			fp_out1 << s_gene_id << "\t" << q_gene_id << "\t" << type << "\n";
			// Write data for table 'homologies'
			if(type == 1 || type == 2){ //ajout thomas
			  int q_gene_index=glist1.gene_index(q_gene_id);
			  int s_gene_index=glist2.gene_index(s_gene_id);
			  BlastRes br = hmat.blast_res(q_gene_index,s_gene_index);
			  fp_out2 << glist2.organism_id() << "\t" << glist2.element_id(s_gene_index) << "\t" << s_gene_id << "\t" << br.hlength << "\t";
			  fp_out2 << glist1.organism_id() << "\t" << glist1.element_id(q_gene_index) << "\t" << q_gene_id << "\t" << br.qlength << "\t";
			  fp_out2 << br.pid << "\t" << br.bits << "\t" << br.evalue << "\t" << br.rank << "\t";
			  fp_out2 << br.hstart << "\t" << ((float) br.hstart - 1) / (float) br.hlength << "\t";
			  fp_out2 << (br.hend + 0) << "\t" << ((float) br.hend + 0) / (float) br.hlength << "\t";
			  fp_out2 << br.hend - br.hstart + 1 << "\t" << (float) (br.hend - br.hstart + 1) / (float) br.hlength << "\t";
			  fp_out2 << br.qstart << "\t" << ((float) br.qstart - 1) / (float) br.qlength << "\t";
			  fp_out2 << (br.qend + 0) << "\t" << (float) (br.qend + 0) / (float) br.qlength << "\t";
			  fp_out2 << br.qend - br.qstart + 1 << "\t" << (float) (br.qend - br.qstart + 1) / (float) br.qlength << "\n";
			}
		  }
	    } else {
	      for (int i= rit->size()-1; i >= 0; i--) {
			string qgene=rit->query_name(i);
			string sgene=rit->subject_name(i);
			int q_gene_id=rit->query_gene_id(i);
			int s_gene_id=rit->subject_gene_id(i);
			float homology=rit->homology_score(i);
			float score=rit->alignment_score(i);
			int qstart=rit->query_start(i);
			int qend=rit->query_end(i);
			int sstart=rit->subject_start(i);
			int send=rit->subject_end(i);
			if(homology == ortholog_score) {
			  type = 1;
			} else if(homology == homolog_score) {
			  type = 2;
			} else if(homology == mismatch_penalty) {
			  type = 3;
			  mismatches++;
			} else if(homology == gap_creation_penalty) {
			  gaps++;
			  total_gap_size++;
			} else if(homology == gap_extension_penalty) {
			  total_gap_size++;
			}
			if(qgene.compare("-") == 0) {
			  type = 5;
			}
			if(sgene.compare("-") == 0) {
			  type = 4;
			}

			// Write data for table 'alignment_pairs'
			fp_out1 << counter_alignment << "\t";
			fp_out1 << s_gene_id << "\t" << q_gene_id << "\t" << type << "\n";
			// Write data for table 'homologies'

			if(type == 1 || type == 2){ //ajout thomas
			  int q_gene_index=glist1.gene_index(q_gene_id);
			  int s_gene_index=glist2.gene_index(s_gene_id);
			  BlastRes br = hmat.blast_res(q_gene_index,s_gene_index);
			  fp_out2 << glist2.organism_id() << "\t" << glist2.element_id(s_gene_index) << "\t" << s_gene_id << "\t" << br.hlength << "\t";
			  fp_out2 << glist1.organism_id() << "\t" << glist1.element_id(q_gene_index) << "\t" << q_gene_id << "\t" << br.qlength << "\t";
			  fp_out2 << br.pid << "\t" << br.bits << "\t" << br.evalue << "\t" << br.rank << "\t";
			  fp_out2 << br.hstart << "\t" << ((float) br.hstart - 1) / (float) br.hlength << "\t";
			  fp_out2 << (br.hend + 0) << "\t" << ((float) br.hend + 0) / (float) br.hlength << "\t";
			  fp_out2 << br.hend - br.hstart + 1 << "\t" << (float) (br.hend - br.hstart + 1) / (float) br.hlength << "\t";
			  fp_out2 << br.qstart << "\t" << ((float) br.qstart - 1) / (float) br.qlength << "\t";
			  fp_out2 << (br.qend + 0) << "\t" << (float) (br.qend + 0) / (float) br.qlength << "\t";
			  fp_out2 << br.qend - br.qstart + 1 << "\t" << (float) (br.qend - br.qstart + 1) / (float) br.qlength << "\n";
			}
	      }
	    }
	    
	    fp_out << mismatches << "\t" << gaps << "\t" << total_gap_size << "\t";
	    fp_out <<  subject.size_bp()/1e3 << "\t";
	    fp_out <<  query.size_bp()/1e3 << "\t";
	    fp_out << subject.start_base() << "\t" << subject.end_base() << "\t" << query.start_base() << "\t" << query.end_base() << "\n";
	    
	  }
  } */

	  

  fp_out <<  "\\.\n";
  fp_out <<  "COMMIT WORK;\n";
  fp_out.close();

  fp_out1 <<  "\\.\n";
  fp_out1 <<  "COMMIT WORK;\n";
  fp_out1.close();

  fp_out2 <<  "\\.\n";
  fp_out2 <<  "COMMIT WORK;\n";
  fp_out2.close();




  // print IsBranchedToAnotherSynteny if any
  if ( ! vecIsBranchedToAnotherSynteny.empty()) {
	  ofstream fp_out3;
	  sprintf(outputfile,"%d_and_%d_isBranchedToAnotherSynteny_table.tsv",organism_id_1,organism_id_2);
	  fp_out3.open(outputfile);
	  fp_out3 << "BEGIN WORK;\n";
	  fp_out3 << "COPY isBranchedToAnotherSynteny (alignment_id_branched, alignment_param_id, branche_on_q_gene_id, branche_on_s_gene_id) FROM stdin;\n";
	  for (auto stToPrint : vecIsBranchedToAnotherSynteny) {
		  fp_out3 << stToPrint << "\n";
	  }
	  fp_out3 <<  "\\.\n";
	  fp_out3 <<  "COMMIT WORK;\n";
	  fp_out3.close();
	  //cout << "for " << organism_id_1 << " and " << organism_id_2
	  //		  	<< " : " << vecIsBranchedToAnotherSynteny.size()
	  //			<< endl;
  }




  // print tandem duplication
  if (print_out_tandem_dups) {

	  if ( ! afactory._col_tandem_dups_idxGeneList1ToVecIdxGeneList2.empty() || ! afactory._row_tandem_dups_idxGeneList2ToVecIdxGeneList1.empty()) {
		  ofstream fp_out_tandem_dups;
		  	  sprintf(outputfile,"%d_and_%d_tandem_dups_table.tsv",organism_id_1,organism_id_2);
		  	  fp_out_tandem_dups.open(outputfile);
		  	  fp_out_tandem_dups <<  "BEGIN WORK;\n";
		  	  fp_out_tandem_dups <<  "COPY tandem_dups (tandem_dups_id, alignment_param_id, gene_id_single_is_q, single_organims_id, single_element_id, single_gene_id, tandem_organims_id, tandem_element_id, tandem_gene_id, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, idx_in_dup) FROM stdin;\n";

		  	  //print in batch of successive idx
		  	  int tandem_dups_id = 1;
		  	  for (auto mapitr = afactory._col_tandem_dups_idxGeneList1ToVecIdxGeneList2.begin(); mapitr != afactory._col_tandem_dups_idxGeneList1ToVecIdxGeneList2.end(); ++mapitr) {

		  		  if (mapitr->second.size() > 1) {

		  			  // Create a vector containing integers
		  			  std::vector<int> vConsecutiveIdx;
		  			  int idxm1 = -1;
		  			  int countLoop=0;
		  			  for (auto setItem: mapitr->second) {

		  				  /*cout << "\t setItem=" << setItem
		  				  		<< endl;*/

		  				  if (countLoop == 0) {
		  					  // 1st iter
		  					  vConsecutiveIdx.push_back(setItem);
		  				  } else {

		  					  /*cout << " ; idxm1=" << idxm1
		  							<< " ; setItem=" << setItem
		  					  		<< endl;*/

		  					  if ( idxm1 == setItem - 1) {
		  						  //ok successive
		  						  vConsecutiveIdx.push_back(setItem);
		  					  } else {
		  						  // not successive, new tandem_dups_id
		  						  // Iterate and print values of vector
		  						  if (vConsecutiveIdx.size() > 1) {
		  							  print_batch_tandem_dups(
		  									  tandem_dups_id
		  									  , mapitr->first
		  									  , vConsecutiveIdx
		  									  , glist1
		  									  , glist2
		  									  , true
		  									  , fp_out_tandem_dups
											  , hmat
		  							  	  	  );
		  						  } else {
		  								cerr << "Error print_out_tandem_dups: NOT vConsecutiveIdx.size() > 1 : " << vConsecutiveIdx.size()
		  										<< " ; idxGeneSingle=" << mapitr->first
		  										<< " ; setItem=" << setItem
		  										<< endl;
		  								exit(EXIT_FAILURE);
		  						  }
		  						  //clear current tandem_dup
		  						  tandem_dups_id++;
		  						  vConsecutiveIdx.clear();
		  						  vConsecutiveIdx.push_back(setItem);
		  					  }
		  				  }
		  				  idxm1 = setItem;
		  				  countLoop++;
		  			  }
		  			  if (vConsecutiveIdx.size() > 1) {
		  				  print_batch_tandem_dups(
		  						  tandem_dups_id
		  						  , mapitr->first
		  						  , vConsecutiveIdx
		  						  , glist1
		  						  , glist2
		  						  , true
		  						  , fp_out_tandem_dups
								  , hmat
		  				  );
		  				  //clear current tandem_dup
		  				  tandem_dups_id++;
		  				  vConsecutiveIdx.clear();
		  			  } else {
		  				  cerr << "Error print_out_tandem_dups: final NOT vConsecutiveIdx.size() > 1 : " << vConsecutiveIdx.size()
		  			  												<< " ; idxGeneSingle=" << mapitr->first
		  															<< endl;
		  				  exit(EXIT_FAILURE);
		  			  }
		  		  }
		  	  }
		  	  for (auto mapitr = afactory._row_tandem_dups_idxGeneList2ToVecIdxGeneList1.begin(); mapitr != afactory._row_tandem_dups_idxGeneList2ToVecIdxGeneList1.end(); ++mapitr) {
		  		  if (mapitr->second.size() > 1) {

		  			  // Create a vector containing integers
		  			  std::vector<int> vConsecutiveIdx;
		  			  int idxm1 = -1;
		  			  int countLoop=0;
		  			  for (auto setItem: mapitr->second) {
		  				  if (countLoop == 0) {
		  					  // 1st iter
		  					  vConsecutiveIdx.push_back(setItem);
		  				  } else {
		  					  if ( idxm1 == setItem - 1) {
		  						  //ok successive
		  						  vConsecutiveIdx.push_back(setItem);
		  					  } else {
		  						  // not successive, new tandem_dups_id
		  						  // Iterate and print values of vector
		  						  if (vConsecutiveIdx.size() > 1) {
		  							  print_batch_tandem_dups(
		  									  tandem_dups_id
		  									  , mapitr->first
		  									  , vConsecutiveIdx
		  									  , glist1
		  									  , glist2
		  									  , false
		  									  , fp_out_tandem_dups
											  , hmat
		  							  );
		  						  } else {
		  							  cerr << "Error print_out_tandem_dups: NOT vConsecutiveIdx.size() > 1 : " << vConsecutiveIdx.size()
		  			  												<< " ; idxGeneSingle=" << mapitr->first
		  															<< " ; setItem=" << setItem
		  															<< endl;
		  							  exit(EXIT_FAILURE);
		  						  }
		  						  //clear current tandem_dup
		  						  tandem_dups_id++;
		  						  vConsecutiveIdx.clear();
		  						  vConsecutiveIdx.push_back(setItem);
		  					  }
		  				  }
		  				  idxm1 = setItem;
		  				  countLoop++;
		  			  }
		  			  if (vConsecutiveIdx.size() > 1) {
		  				  print_batch_tandem_dups(
		  						  tandem_dups_id
		  						  , mapitr->first
		  						  , vConsecutiveIdx
		  						  , glist1
		  						  , glist2
		  						  , false
		  						  , fp_out_tandem_dups
								  , hmat
		  				  );
		  				  //clear current tandem_dup
		  				  tandem_dups_id++;
		  				  vConsecutiveIdx.clear();
		  			  } else {
		  				  cerr << "Error print_out_tandem_dups: NOT vConsecutiveIdx.size() > 1 : " << vConsecutiveIdx.size()
		  			  			  														<< " ; idxGeneSingle=" << mapitr->first
		  																				<< endl;
		  				  exit(EXIT_FAILURE);
		  			  }
		  		  }
		  	  }
		  	  fp_out_tandem_dups <<  "\\.\n";
		  	  fp_out_tandem_dups <<  "COMMIT WORK;\n";
		  	  fp_out_tandem_dups.close();
	  }

  }




  // resue match within 5% of best score
  // RQ : perform print_out_close_best_match before print_out_gene_fusions because the latter destro data in idxI2VectHomologsIdxJ and idxJ2VectHomologsIdxI
  if (GlobalParams::print_out_close_best_match) {
	
	  vector<string> vecCloseBestMatchFromIdxI2VectHomologsIdxJ = getVecCloseBestMatch(
			  hmat.idxI2VectHomologsIdxJ
			  , hmat
			  , false
			  , glist1
			  , glist2
	  	  	  );
	  vector<string> vecCloseBestMatchFromidxJ2VectHomologsIdxI;
	  if (glist1.organism_id() != glist2.organism_id()) {
		  vecCloseBestMatchFromidxJ2VectHomologsIdxI = getVecCloseBestMatch(
				  hmat.idxJ2VectHomologsIdxI
				  , hmat
				  , true
				  , glist1
				  , glist2
				  );
	  }



	  //print vecCloseBestMatch if any
	  if ( ! vecCloseBestMatchFromIdxI2VectHomologsIdxJ.empty() || ! vecCloseBestMatchFromidxJ2VectHomologsIdxI.empty() ) {
		  ofstream fp_out4;
		  sprintf(outputfile,"%d_and_%d_closeBestMatchs_table.tsv", organism_id_1, organism_id_2);
		  fp_out4.open(outputfile);
		  fp_out4 << "BEGIN WORK;\n";
		  //new header close_best_match_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, is_mirror_data, number_of_close_matchs, contain_bdbh
		  	//, pid_best_match, evalue_best_match, bits_best_match, list_close_match_gene_ids
		  // OLD header = (close_best_match_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, , is_mirror_data, s_rank_among_all_close_best_matchs
		  //percent_within_best_match,
		  //pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank
		  //)
		  fp_out4 << "COPY close_best_match ("
				  << "close_best_match_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, is_mirror_data, number_of_close_matchs, contain_bdbh"
				  << ", pid_best_match, evalue_best_match, bits_best_match, list_close_match_gene_ids"
				  << ") FROM stdin;\n";
		  for (auto stToPrint : vecCloseBestMatchFromIdxI2VectHomologsIdxJ) {
			  fp_out4 << stToPrint << "\n";
		  }
		  for (auto stToPrint : vecCloseBestMatchFromidxJ2VectHomologsIdxI) {
			  fp_out4 << stToPrint << "\n";
		  }
		  fp_out4 <<  "\\.\n";
		  fp_out4 <<  "COMMIT WORK;\n";
		  fp_out4.close();
	  }
	
  }

  // print_out_gene_fusions
  if (GlobalParams::print_out_gene_fusions) {


	  vector<string> vecProtFusionFromIdxI2VectHomologsIdxJ = getVecProtFusion(
			  hmat.idxI2VectHomologsIdxJ
			  , hmat
			  , false
			  , glist1
			  , glist2
	  	  	  );
	  vector<string> vecProtFusionFromidxJ2VectHomologsIdxI;
	  if (glist1.organism_id() != glist2.organism_id()) {
		  vecProtFusionFromidxJ2VectHomologsIdxI = getVecProtFusion(
		  			  hmat.idxJ2VectHomologsIdxI
		  			  , hmat
		  			  , true
		  			  , glist1
		  			  , glist2
		  			  );
	  }


	  //print vecProtFusion if any
	  if ( ! vecProtFusionFromIdxI2VectHomologsIdxJ.empty() || ! vecProtFusionFromidxJ2VectHomologsIdxI.empty() ) {
		  ofstream fp_out4;
		  sprintf(outputfile,"%d_and_%d_protFusion_table.tsv",organism_id_1,organism_id_2);
		  fp_out4.open(outputfile);
		  fp_out4 << "BEGIN WORK;\n";
		  //header = prot_fusion (prot_fusion_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, s_rank_among_all_s_fusions,
		  //lengthTotalCoverageSOnQ, lenghtNewContribCoverageSOnQ,
		  //pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank
		  // , geneIdBestSingleLongMatchHidingProtFusion, numberSingleLongMatchHidingProtFusion, numberProtFusionDetectedBeforeThisOne
		  //)
		  fp_out4 << "COPY prot_fusion ("
				  << "prot_fusion_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, isMirrorData, s_rank_among_all_s_fusions"
				  << ", lengthTotalCoverageSOnQ, lenghtNewContribCoverageSOnQ"
				  << ", pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank"
				  << ", geneIdBestSingleLongMatchHidingProtFusion, numberSingleLongMatchHidingProtFusion, numberProtFusionDetectedBeforeThisOne"
				  << ") FROM stdin;\n";
		  for (auto stToPrint : vecProtFusionFromIdxI2VectHomologsIdxJ) {
			  fp_out4 << stToPrint << "\n";
		  }
		  for (auto stToPrint : vecProtFusionFromidxJ2VectHomologsIdxI) {
			  fp_out4 << stToPrint << "\n";
		  }
		  fp_out4 <<  "\\.\n";
		  fp_out4 <<  "COMMIT WORK;\n";
		  fp_out4.close();
	  }
  }

  delete sn;

  return 0;

}
