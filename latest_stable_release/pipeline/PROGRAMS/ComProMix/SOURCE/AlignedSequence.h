#ifndef _ALIGNEDSEQUENCE_H_
#define _ALIGNEDSEQUENCE_H_

#include <string>
#include <cmath>
#include <vector>
#include <stdlib.h>
using namespace std;

class AlignedSequence {
public:
  inline int organism_id() const
    { return _organism_id; };
  inline int start_base() const
    { return _start_base; };
  inline int end_base() const
    { return _end_base; };
  inline int size_genes() const
    { return _gene_names.size(); };
  int size_bp() const
    { return abs(_end_base-_start_base+1); };
  vector<string> gene_names() const
    { return _gene_names; };

  friend class Alignment;
  friend class AlignmentFactory;
private:
  int _organism_id;
  int _start_base;
  int _end_base;
  vector<string> _gene_names;
  vector<int> _gene_ids;
  vector<int> _start_points;
  vector<int> _end_points;

};

#endif
