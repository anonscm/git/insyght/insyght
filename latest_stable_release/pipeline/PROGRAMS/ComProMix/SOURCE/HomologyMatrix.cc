#include <iostream>
#include <sstream>
#include <cstdio>
#include <vector>
#include <stdlib.h>
#include <fstream>
#include <map>
#include <unordered_map>
#include <algorithm>
#include "HomologyMatrix.h"
#include "GlobalParams.h"

using namespace std;

typedef map<int,float> innerMap1;
typedef map<int,innerMap1> tempMap;

static bool compare_br(innerMap::value_type &i1, innerMap::value_type &i2)
{
	return i1.second.bits < i2.second.bits;
}

static bool compare_bits(innerMap1::value_type &i1, innerMap1::value_type &i2)
{
	return i1.second < i2.second;
}


HomologyMatrix::HomologyMatrix(
		GeneList & glist1
		, GeneList & glist2
		, string blast_output
		, float prot_frac
		, ScoreTransformer *tr
		//, bool print_out_gene_fusions
		) {

	//
	// Allocate and initialize _score matrix
	//

	_size_1 = glist1.size();
	_size_2 = glist2.size();

	float *tmp_scores=new float[_size_1*_size_2];
	_scores=new float*[_size_1];
	for (int i=0;i<_size_1;i++) {
		_scores[i]=&(tmp_scores[i*_size_2]);
		for (int j=0;j<_size_2;j++)
			_scores[i][j] = (float) UNRELATED;
	}

	//
	// Read Blast results and determine pairs of proteins that correspond to BDBHs (bi-directional best hits)
	//

	string q_id, h_id;
	BlastRes br;
	tempMap tM;              // this map contains for each target-query pair the value of BLAST bit score: < h_gene_id, q_gene_id, br.bits >
	map<int, innerMap>::iterator it;
	map<int, BlastRes>::iterator inner_it;
	ifstream ifs(blast_output.c_str());

	if (!ifs.is_open()) {
		cerr << "unable to open file " << blast_output << " for reading\n";
		exit(EXIT_FAILURE);
	}

	vector<string> fields;
	string token;
	string line;
	int Nfields;
	int q_gene_id, h_gene_id;
	//int q_element_id, h_element_id;
	int q_organism_id, h_organism_id;

	// Process BLAST output file
	// In Blast output file, gene identities are specified by the string: {gene_name}_{organism_id}_{element_id}_{gene_id}, where gene_name can be empty, e.g.
	// _5_9_24470      copA_397_843_1223430    37.97     669        383      12      66      713     164      821      1e-124     387
	//    q_id               h_id             %resId   hsplength  mismat.   gaps   q_start  q_end   h_start  h_end      e-val   bitscore
	// Notice: care must be taken that some gene names can themselves contain underscores, e.g., cadA_2_397_843_1224554.

	int nblastlines_skipped=0;
	int nblastlines_kept=0;

	while(getline(ifs,line)) {

		stringstream ss(line);
		ss >> q_id >> h_id;
		istringstream iq(q_id);
		Nfields = 0;
		while(getline(iq,token,'_')) {
			fields.push_back(token);
			Nfields++;
		}
		try {
			q_gene_id = std::stoi(fields[Nfields-1].c_str()); // gene_id should always be the last field whatever the number of fields (this takes care of the existence of underscores in the gene name)
		} catch (...) {
			cerr << "Error while parsing line:\n" << line << "\n"
					<< "from blast file:\n" << blast_output << "\n"
					<< "Couldn't determine q_gene_id.\n\n"
					;
			exit(EXIT_FAILURE);
		}
		//q_element_id = std::stoi(fields[Nfields-2].c_str());
		try {
			q_organism_id = std::stoi(fields[Nfields-3].c_str());
		} catch (...) {
			cerr << "Error while parsing line:\n" << line << "\n"
					<< "from blast file:\n" << blast_output << "\n"
					<< "Couldn't determine q_organism_id.\n\n"
					;
			exit(EXIT_FAILURE);
		}

		istringstream ih(h_id);
		while(getline(ih,token,'_')) {
			fields.push_back(token);
			Nfields++;
		}
		try {
			h_gene_id = std::stoi(fields[Nfields-1].c_str());
		} catch (...) {
			cerr << "Error while parsing line:\n" << line << "\n"
					<< "from blast file:\n" << blast_output << "\n"
					<< "Couldn't determine h_gene_id.\n\n"
					;
			exit(EXIT_FAILURE);
		}
		try {
			h_organism_id = std::stoi(fields[Nfields-3].c_str());
		} catch (...) {
			cerr << "Error while parsing line:\n" << line << "\n"
					<< "from blast file:\n" << blast_output << "\n"
					<< "Couldn't determine h_organism_id.\n\n"
					;
			exit(EXIT_FAILURE);
		}
		fields.clear();

		if (q_organism_id==glist1.organism_id() && h_organism_id==glist2.organism_id() && q_gene_id!=h_gene_id) {
			int q_gene_index=glist1.gene_index(q_gene_id);
			int h_gene_index=glist2.gene_index(h_gene_id);

			ss >> br.pid >> br.hsp_len >> br.mismatches >> br.gaps;
			ss >> br.qstart >> br.qend >> br.hstart >> br.hend;
			ss >> br.evalue >> br.bits;
			if (br.pid > 100) {
				br.pid = 100;
			}
			if (br.pid < 0) {
				br.pid = 0;
			}

			br.bdbh = 0;
			br.rank = 2;

			if((it = _gene_pairs.find(q_gene_index)) == _gene_pairs.end()) {
				_gene_pairs.insert(make_pair(q_gene_index,innerMap()));
				_gene_pairs[q_gene_index].insert(make_pair(h_gene_index,br));
				tM[h_gene_index][q_gene_index] = br.bits;
			} else {
				if((inner_it = (*it).second.find(h_gene_index)) == (*it).second.end()){
					_gene_pairs.insert(make_pair(q_gene_index,innerMap()));
					_gene_pairs[q_gene_index].insert(make_pair(h_gene_index,br));
					tM[h_gene_index][q_gene_index] = br.bits;
					// If there exist several HSPs for the same gene pair, keep the HSP with the largest bit score value
				} else if((*inner_it).second.bits < br.bits) {
					(*inner_it).second = br;
					tM[h_gene_index][q_gene_index] = br.bits;
				}
			}
			nblastlines_kept++;
		} else {
			nblastlines_skipped++;
		}
	}
	//cout << "keeping only blast result lines with organism " << glist1.organism_id() << " as query and organism " << glist2.organism_id() << " as hit" << endl;
	//cout << "# kept: " << nblastlines_kept << " # skipped: " << nblastlines_skipped << endl;

	//
	// Determine which gene pairs correspond to BDBHs (bi-directional best hits)
	//


	for(it = _gene_pairs.begin(); it != _gene_pairs.end(); it++)  {
		map<int, BlastRes>::iterator itmax = max_element((*it).second.begin(),(*it).second.end(),compare_br);
		_gene_pairs[(*it).first][(*itmax).first].bdbh += 1;
	}

	for(map<int, innerMap1>::iterator it_innerMap1 = tM.begin(); it_innerMap1 != tM.end(); it_innerMap1++)  {
		map<int, float>::iterator itmax = max_element((*it_innerMap1).second.begin(),(*it_innerMap1).second.end(),compare_bits);
		_gene_pairs[(*itmax).first][(*it_innerMap1).first].bdbh += 1;
		if(_gene_pairs[(*itmax).first][(*it_innerMap1).first].bdbh == 2) {
			_gene_pairs[(*itmax).first][(*it_innerMap1).first].rank = 1;
		}
	}

	//
	// fill the score matrix
	//

	int h_len, q_len, hsp_len;
	float h_frac, q_frac;

	for (map<int, innerMap>::iterator it_bis = _gene_pairs.begin() ; it_bis != _gene_pairs.end(); it_bis++) {

		q_len = glist1.length((*it_bis).first);
		for(map<int, BlastRes>::iterator inner_it_bis = (*it_bis).second.begin(); inner_it_bis != (*it_bis).second.end(); inner_it_bis++) {

			//if((*it_bis).first == (*inner_it_bis).first){//ajout thomas
			//continue;//continue if same gene id, we do not want the diagonal of the matrix when looking for paralogues
			//}

			h_len = glist2.length((*inner_it_bis).first);

			// Store the query and target protein lengths
			if(_gene_pairs[(*it_bis).first][(*inner_it_bis).first].qend > q_len){ //ajout thomas
				//When structural annotation in genbank file contains a mistake regarding start and stop, correct it if lenght match > lenght protein
				_gene_pairs[(*it_bis).first][(*inner_it_bis).first].qlength = _gene_pairs[(*it_bis).first][(*inner_it_bis).first].qend;
			}else{
				_gene_pairs[(*it_bis).first][(*inner_it_bis).first].qlength = q_len;
			}
			if(_gene_pairs[(*it_bis).first][(*inner_it_bis).first].hend > h_len){ //ajout thomas
				//When structural annotation in genbank file contains a mistake regarding start and stop, correct it if lenght match > lenght protein
				_gene_pairs[(*it_bis).first][(*inner_it_bis).first].hlength = _gene_pairs[(*it_bis).first][(*inner_it_bis).first].hend;
			}else{
				_gene_pairs[(*it_bis).first][(*inner_it_bis).first].hlength = h_len;
			}

			hsp_len = (*inner_it_bis).second.qend - (*inner_it_bis).second.qstart  + 1;
			q_frac = (float) hsp_len / (float) q_len;
			hsp_len = (*inner_it_bis).second.hend - (*inner_it_bis).second.hstart  + 1;
			h_frac = (float) hsp_len / (float) h_len;

			if((*inner_it_bis).second.bdbh == 2 && q_frac > prot_frac && h_frac > prot_frac ) {
				_scores[(*it_bis).first][(*inner_it_bis).first] = ORTHOLOGS;
			} else {
				_scores[(*it_bis).first][(*inner_it_bis).first] = HOMOLOGS;
			}

		}
	}

	//
	// Check consistency of the score matrix (there should be at most one ortholog for each row and column)
	//

	int kount[2];
	int nerr = 0;
	for (int i=0;i<_size_1;i++) {
		kount[0] = 0;
		kount[1] = 0;
		std::multiset<GeneFusionCheckerObj> setGeneFusionIT;
		for (int j = 0; j < _size_2; j++) {

			/*if (_scores[i][j] == ORTHOLOGS || _scores[i][j] == HOMOLOGS) {
				cout << "CHECKING HomologyMatrix GeneFusion"
								<< " ; idx i=" << i
								<< " ; idx j=" << j
								<< " ; GlobalParams::geneFusions_minPercentLenghtProtThreshold=" << GlobalParams::geneFusions_minPercentLenghtProtThreshold
								<< " ; _gene_pairs[i][j].hend=" << _gene_pairs[i][j].hend
								<< " ; _gene_pairs[i][j].hstart=" << _gene_pairs[i][j].hstart
								<< " ; _gene_pairs[i][j].hlength=" << _gene_pairs[i][j].hlength
								<< " ; test1=" << (float) ((abs( (float) _gene_pairs[i][j].hend - (float) _gene_pairs[i][j].hstart + 1 )) / (float) _gene_pairs[i][j].hlength)
								<< " ; test2=" << (float) ((abs( _gene_pairs[i][j].qend - _gene_pairs[i][j].qstart )) / _gene_pairs[i][j].qlength)
								<< endl;
			}*/



			if (_scores[i][j] == ORTHOLOGS) {
				kount[0]++;
				if (GlobalParams::print_out_gene_fusions || GlobalParams::print_out_close_best_match) {
					/*if ( (float) ((abs( (float) _gene_pairs[i][j].hend - (float) _gene_pairs[i][j].hstart + 1 )) / (float) _gene_pairs[i][j].hlength) >= GlobalParams::geneFusions_minPercentLenghtProtThreshold //subject is mostly aligned as a whole
							&& (float) ((abs( (float) _gene_pairs[i][j].qend - (float) _gene_pairs[i][j].qstart + 1)) / (float) _gene_pairs[i][j].qlength) < GlobalParams::geneFusions_minPercentLenghtProtThreshold// and do not take too much space on query
							) {*/

						/*cout << "PASSED ORTHOLOGS HomologyMatrix GeneFusion"
										<< " ; idx i=" << i
										<< " ; idx j=" << j
										<< " ; GlobalParams::geneFusions_minPercentLenghtProtThreshold=" << GlobalParams::geneFusions_minPercentLenghtProtThreshold
										<< " ; test1=" << (float) ((abs( (float) _gene_pairs[i][j].hend - (float) _gene_pairs[i][j].hstart + 1 )) / (float) _gene_pairs[i][j].hlength)
										<< " ; test2=" << (float) ((abs( (float) _gene_pairs[i][j].qend - (float) _gene_pairs[i][j].qstart + 1)) / (float) _gene_pairs[i][j].qlength)
										<< endl;*/

						GeneFusionCheckerObj gfco;
						gfco._idx = j;
						gfco._alignment_size_on_query = abs( _gene_pairs[i][j].qend - _gene_pairs[i][j].qstart );
						gfco._bits = _gene_pairs[i][j].bits;
						setGeneFusionIT.insert(gfco);
					//}
				}
			} else if (_scores[i][j] == HOMOLOGS) {
				kount[1]++;
				if (GlobalParams::print_out_gene_fusions || GlobalParams::print_out_close_best_match) {
					/*if ( (float) ((abs( (float) _gene_pairs[i][j].hend - (float) _gene_pairs[i][j].hstart + 1 )) / (float) _gene_pairs[i][j].hlength) >= GlobalParams::geneFusions_minPercentLenghtProtThreshold //subject is mostly aligned as a whole
												&& (float) ((abs( (float) _gene_pairs[i][j].qend - (float) _gene_pairs[i][j].qstart + 1)) / (float) _gene_pairs[i][j].qlength) < GlobalParams::geneFusions_minPercentLenghtProtThreshold// and do not take too much space on query
												) {*/

						/*cout << "PASSED HOMOLOGS HomologyMatrix GeneFusion"
										<< " ; idx i=" << i
										<< " ; idx j=" << j
										<< " ; GlobalParams::geneFusions_minPercentLenghtProtThreshold=" << GlobalParams::geneFusions_minPercentLenghtProtThreshold
										<< " ; test1=" << (float) ((abs( (float) _gene_pairs[i][j].hend - (float) _gene_pairs[i][j].hstart + 1 )) / (float) _gene_pairs[i][j].hlength)
										<< " ; test2=" << (float) ((abs( (float) _gene_pairs[i][j].qend - (float) _gene_pairs[i][j].qstart + 1)) / (float) _gene_pairs[i][j].qlength)
										<< endl;*/


						GeneFusionCheckerObj gfco;
						gfco._idx = j;
						gfco._alignment_size_on_query = abs( _gene_pairs[i][j].qend - _gene_pairs[i][j].qstart );
						gfco._bits = _gene_pairs[i][j].bits;
						setGeneFusionIT.insert(gfco);
					//}
				}
			}
		}
		if ( setGeneFusionIT.size() > 1 && ( GlobalParams::print_out_gene_fusions || GlobalParams::print_out_close_best_match ) ) {
			std::pair<int, SetGeneFusion> myPair (i,setGeneFusionIT);
			idxI2VectHomologsIdxJ.insert(myPair);
		}
		if(kount[0] > 1) {
			cerr << "Error for row: " << i << " the number of orthologs is larger than 1: " << kount[0] << " (number of homologs is: " << kount[1] << ")\n";
			nerr++;
		}
	}

	for(int j=0;j<_size_2;j++) {
		kount[0] = 0;
		kount[1] = 0;
		std::multiset<GeneFusionCheckerObj> setGeneFusionIT;
		for (int i=0;i<_size_1;i++) {
			if(_scores[i][j] == ORTHOLOGS) {
				kount[0]++;
				if (GlobalParams::print_out_gene_fusions || GlobalParams::print_out_close_best_match) {
					/*if ( (float) ((abs( (float) _gene_pairs[i][j].qend - (float) _gene_pairs[i][j].qstart + 1 )) / (float) _gene_pairs[i][j].qlength) >= GlobalParams::geneFusions_minPercentLenghtProtThreshold //subject is mostly aligned as a whole
							&& (float) ((abs( (float) _gene_pairs[i][j].hend - (float) _gene_pairs[i][j].hstart + 1 )) / (float) _gene_pairs[i][j].hlength) < GlobalParams::geneFusions_minPercentLenghtProtThreshold// and do not take too much space on query
							) {*/

						/*cout << "PASSED ORTHOLOGS HomologyMatrix GeneFusion"
										<< " ; idx i=" << i
										<< " ; idx j=" << j
										<< " ; GlobalParams::geneFusions_minPercentLenghtProtThreshold=" << GlobalParams::geneFusions_minPercentLenghtProtThreshold
										<< " ; test1=" << (float) ((abs( (float) _gene_pairs[i][j].qend - (float) _gene_pairs[i][j].qstart + 1 )) / (float) _gene_pairs[i][j].qlength)
										<< " ; test2=" << (float) ((abs( (float) _gene_pairs[i][j].hend - (float) _gene_pairs[i][j].hstart + 1 )) / (float) _gene_pairs[i][j].hlength)
										<< endl;*/

						GeneFusionCheckerObj gfco;
						gfco._idx = i;
						gfco._alignment_size_on_query = abs( _gene_pairs[i][j].hend - _gene_pairs[i][j].hstart );
						gfco._bits = _gene_pairs[i][j].bits;
						setGeneFusionIT.insert(gfco);
					//}
				}
			} else if(_scores[i][j] == HOMOLOGS) {
				kount[1]++;
				if (GlobalParams::print_out_gene_fusions || GlobalParams::print_out_close_best_match) {
					/*if ( (float) ((abs( (float) _gene_pairs[i][j].qend - (float) _gene_pairs[i][j].qstart + 1 )) / (float) _gene_pairs[i][j].qlength) >= GlobalParams::geneFusions_minPercentLenghtProtThreshold //subject is mostly aligned as a whole
							&& (float) ((abs( (float) _gene_pairs[i][j].hend - (float) _gene_pairs[i][j].hstart + 1 )) / (float) _gene_pairs[i][j].hlength) < GlobalParams::geneFusions_minPercentLenghtProtThreshold// and do not take too much space on query
							) {*/

						/*cout << "PASSED HOMOLOGS HomologyMatrix GeneFusion"
										<< " ; idx i=" << i
										<< " ; idx j=" << j
										<< " ; GlobalParams::geneFusions_minPercentLenghtProtThreshold=" << GlobalParams::geneFusions_minPercentLenghtProtThreshold
										<< " ; test1=" << (float) ((abs( (float) _gene_pairs[i][j].qend - (float) _gene_pairs[i][j].qstart + 1 )) / (float) _gene_pairs[i][j].qlength)
										<< " ; test2=" << (float) ((abs( (float) _gene_pairs[i][j].hend - (float) _gene_pairs[i][j].hstart + 1 )) / (float) _gene_pairs[i][j].hlength)
										<< endl;*/


						GeneFusionCheckerObj gfco;
						gfco._idx = i;
						gfco._alignment_size_on_query = abs( _gene_pairs[i][j].hend - _gene_pairs[i][j].hstart );
						gfco._bits = _gene_pairs[i][j].bits;
						setGeneFusionIT.insert(gfco);
					//}
				}
			}
		}
		if ( setGeneFusionIT.size() > 1 && ( GlobalParams::print_out_gene_fusions || GlobalParams::print_out_close_best_match ) ) {
			std::pair<int, SetGeneFusion> myPair (j,setGeneFusionIT);
			idxJ2VectHomologsIdxI.insert(myPair);
		}

		if(kount[0] > 1) {
			cerr << "Error for column: " << j << " the number of orthologs is larger than 1: " << kount[0] << " (number of homologs is: " << kount[1] << ")\n";
			nerr++;
		}
	}

	if(nerr > 0) {
		cerr << "There were " << nerr << " errors in HomologyMatrix (see above). Aborting. Check the errors before rerunning the program\n";
		exit(EXIT_FAILURE);
	}

	//
	// Use scores specified by the user
	//

	if (tr)
		tr->transform(_scores,_size_1,_size_2);

	_max=_scores[0][0];
	_min=_scores[0][0];
	for (int i=0;i<_size_1;i++)
		for (int j=0;j<_size_2;j++) {
			_max=(_scores[i][j]>_max?_scores[i][j]:_max);
			_min=(_scores[i][j]<_min?_scores[i][j]:_min);

			/* Print matrice for debug
		if (_scores[i][j] > 0) {
			cout << "score " << i << ";" << j << " = " << _scores[i][j]
					<< "\n";
		}*/

		}

}

/***********************************************************************
 *                                                                     *
 ***********************************************************************/
float HomologyMatrix::score(const int gene_index1, const int gene_index2) const {
	return _scores[gene_index1][gene_index2];
}


/***********************************************************************
 *                                                                     *
 ***********************************************************************/
HomologyMatrix::~HomologyMatrix() {
	delete[] _scores[0];
	delete[] _scores;
}
