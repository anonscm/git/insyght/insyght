#include <iostream>
#include <string>
#include <cstdio>
using namespace std;

#include <stdlib.h>

#include "GeneListPSQL.h"


int
main(int argc, char **argv) {

  if (argc != 2) {
    cerr << "Usage: " << argv[0] << " organism_id" << endl;
    exit(EXIT_FAILURE);
  }

  GeneListPSQL glist(std::stoi(argv[1]));

  cout << "Size : " << glist.size() << endl;

  int gene_index=0;
  cout << "First element " << "\n"
       << "\t" << "gene_id    : " << glist.gene_id(gene_index) << "\n" 
       << "\t" << "gene_name  : " << glist.gene_name(gene_index) << "\n"
       << "\t" << "start : " << glist.start(gene_index) << "\n"
       << "\t" << "stop   : " << glist.stop(gene_index) << "\n"
       << "\t" << "element_id   : " << glist.element_id(gene_index) << endl;

  gene_index=glist.size()-1;
  cout << "Last element " << "\n"
       << "\t" << "gene_id    : " << glist.gene_id(gene_index) << "\n" 
       << "\t" << "gene_name  : " << glist.gene_name(gene_index) << "\n"
       << "\t" << "start : " << glist.start(gene_index) << "\n"
       << "\t" << "stop   : " << glist.stop(gene_index) << "\n"
       << "\t" << "element_id   : " << glist.element_id(gene_index) << endl;


  char filename[1024];
  sprintf(filename,"%d.archive",std::stoi(argv[1]));
  cout << "\nSerializing GenList object in archive file: " << filename << endl;
  glist.serialize(string(filename));

}
