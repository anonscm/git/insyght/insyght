#include "GlobalParams.h"

//TODO move all GlobalParams here and get ride of #define in main_Align.cc

bool GlobalParams::print_out_gene_fusions = true;
bool GlobalParams::print_out_close_best_match = true;
float GlobalParams::geneFusions_minPercentLenghtProtThreshold = 0.85;
float GlobalParams::closeBestMatchs_minPercentThreshold = 0.99;
float GlobalParams::tandemDups__minPercentWithinBestPidThreshold = 0.85;
