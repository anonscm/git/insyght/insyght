#ifndef _CELLWITHSCOREGRATERTHANCUTOFF_H_
#define _CELLWITHSCOREGRATERTHANCUTOFF_H_

class CellWithScoreGreaterThanCutoff {

public:
  inline int col() const
    { return  _col; };
  inline int row() const
    { return  _row; };
  inline float score() const
    { return  _score; };
  inline float totalSyntenySize() const
    { return  _totalSyntenySize; };

  friend int operator<(const CellWithScoreGreaterThanCutoff &a, const CellWithScoreGreaterThanCutoff &b) {
	  if (a._score == b._score) {
		  return (a._totalSyntenySize < b._totalSyntenySize);
	  } else {
		  return (a._score < b._score);
	  }
  };
  int _col;
  int _row;
  float _score;
  int _totalSyntenySize;

private:


};

#endif

