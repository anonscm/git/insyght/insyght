LOCUS       NC_018222              12893 bp    DNA     circular CON 09-JUN-2013
DEFINITION  Enterococcus faecalis D32 plasmid EFD32pA, complete sequence.
ACCESSION   NC_018222
VERSION     NC_018222.1  GI:397698492
DBLINK      Project: 171261
KEYWORDS    RefSeq.
SOURCE      Enterococcus faecalis D32
  ORGANISM  Enterococcus faecalis D32
            Bacteria; Firmicutes; Bacilli; Lactobacillales; Enterococcaceae;
            Enterococcus.
REFERENCE   1  (bases 1 to 12893)
  AUTHORS   Zischka,M., Kuenne,C., Blom,J., Dabrowski,P.W., Linke,B., Hain,T.,
            Nitsche,A., Goesmann,A., Larsen,J., Jensen,L.B., Witte,W. and
            Werner,G.
  TITLE     Complete genome sequence of the porcine isolate Enterococcus
            faecalis D32
  JOURNAL   J. Bacteriol. 194 (19), 5490-5491 (2012)
   PUBMED   22965105
REFERENCE   2  (bases 1 to 12893)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (24-JUL-2012) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   3  (bases 1 to 12893)
  AUTHORS   Zischka,M., Kuenne,C., Blom,J., Dabrowski,P.W., Linke,B., Hain,T.,
            Nitsche,A., Goesmann,A., Larsen,J., Jensen,L.B., Witte,W. and
            Werner,G.
  TITLE     Direct Submission
  JOURNAL   Submitted (10-JUL-2012) Robert Koch Institute, Burgstrasse 37,
            Wernigerode 38855, Germany
COMMENT     PROVISIONAL REFSEQ: This record has not yet been subject to final
            NCBI review. The reference sequence is identical to CP003727.
            
            ##Assembly-Data-START##
            Assembly Method       :: Celera v. 6.1
            Sequencing Technology :: 454
            ##Assembly-Data-END##
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..12893
                     /organism="Enterococcus faecalis D32"
                     /mol_type="genomic DNA"
                     /strain="D32"
                     /isolation_source="feces"
                     /host="pig"
                     /db_xref="taxon:1206105"
                     /plasmid="EFD32pA"
                     /country="Denmark"
                     /collection_date="2001"
     gene            1..1491
                     /locus_tag="EFD32_pA0001"
                     /db_xref="GeneID:13348249"
     CDS             1..1491
                     /locus_tag="EFD32_pA0001"
                     /function="Essential for replication."
                     /codon_start=1
                     /transl_table=11
                     /product="putative RepE protein"
                     /protein_id="REF_cebbi:EFD32_pA0001"
                     /protein_id="YP_006539248.1"
                     /db_xref="GI:397698493"
                     /db_xref="GeneID:13348249"
                     /translation="MNIPFVVETVLHDGLLKYKFRNSKIRSLTTKPGKSKGAIFAYRS
                     KSSMIGGRGIVLTSEEAINENQDTFTHWTPNVYRYGTYADENRSYTKGHSENNLRQIN
                     TFFIDFDIHTAKEAISASDILTTAIDLGFMPTMIIKSDKGYQAYFVLETPVYVTSKSE
                     FKSVKAAKIISQNIREYFGKSLPVDLTCNHFGIARIPRTDNVEFFDPNYRYSFKEWQD
                     WSFKQTDNKGFTRSSLTVLSGTEGKKQVDEPWFNLLLHETKFSGEKGLVGRNSVMFTL
                     SLAYFSSGYSIETCEYNMFEFNNRLDQPLEEKEVIKIVRSAYSENYQGANREYITILC
                     KAWVSSDLTSKDLFVRQGWFKFKKKRSERQRVHLSEWKEDLMAYISEKSDVYKPYLAT
                     TKKEIREALGIPERTLDKLLKVLKANQEIFFKIKSGRNGGIQLASVKSLLLSIIKVKK
                     EERESYIKALTASFNLERTFIQETLNKLVERPKTATQLDLFGYDTG"
     gene            2271..2441
                     /locus_tag="EFD32_pA0002"
                     /db_xref="GeneID:13348237"
     CDS             2271..2441
                     /locus_tag="EFD32_pA0002"
                     /codon_start=1
                     /transl_table=11
                     /product="hypothetical protein"
                     /protein_id="REF_cebbi:EFD32_pA0002"
                     /protein_id="YP_006539249.1"
                     /db_xref="GI:397698494"
                     /db_xref="GeneID:13348237"
                     /translation="MKKFIYRVLENDEVVAIFNEQQYAQDFIAYEKTISDKQFEIEKV
                     DIADWLLQPREF"
     gene            2455..3072
                     /locus_tag="EFD32_pA0003"
                     /db_xref="GeneID:13348250"
     CDS             2455..3072
                     /locus_tag="EFD32_pA0003"
                     /codon_start=1
                     /transl_table=11
                     /product="resolvase"
                     /protein_id="REF_cebbi:EFD32_pA0003"
                     /protein_id="YP_006539250.1"
                     /db_xref="GI:397698495"
                     /db_xref="GeneID:13348250"
                     /translation="MAKIGYARVSSKEQNLDRQLQALQGVSKVFSDKLSGQSVERPQL
                     QAMLNYIREGDIVIVTELDRLGRNNKELTELMNQIQIKGATLEVLNLPSMNGIEDENL
                     RRLINSLVIELYKYQAESERKKIKERQAQGIEIAKKKGKFKGRQHKFKENDPRLKHAF
                     DLFLNGLSDKEVEEQTGINRRTFRRYRARYNVTVDQRKNNEKRDS"
     gene            3072..5216
                     /locus_tag="EFD32_pA0004"
                     /db_xref="GeneID:13348238"
     CDS             3072..5216
                     /locus_tag="EFD32_pA0004"
                     /codon_start=1
                     /transl_table=11
                     /product="type 1 topoisomerase"
                     /protein_id="REF_cebbi:EFD32_pA0004"
                     /protein_id="YP_006539251.1"
                     /db_xref="GI:397698496"
                     /db_xref="GeneID:13348238"
                     /translation="MSTVILAEKPSQALAYASALKQSTKKDGYFEIKDPLFTDETFIT
                     FGFGHLVELAEPGHYDEKWQNWKLESLPIFPDRYDFEVAKDKGKQFKIVAELLKKANT
                     IIVATDSDREGENIAWSIIHKANAFSKDKTFKRLWINSLEKDVIRSGFQNLQPGMNYY
                     PFYQEAQTRQIADWLIGMNASPLYTLNLQQKGVQGTFSLGRVQTPTLYLIFQRQEAIE
                     NFKKEPFFEVEASIKVNQGSFKGVLSPTQRFKTQEELLAFVSSKQAKIGNQEGIIADV
                     QTKEKKTNSPSLFSLSSLQSKVNQLYKATASQTLKAMQGLYEAKLLSYPRTDTPFITE
                     NEFAYLKANFGKYSGFLGLDLEMVQTEPRKRYVDGSKVQEHHAIISTKQVPTESALAK
                     MDDLQRKIYALVVKTTVAMFLPDYLYEETKIQTKVADLLFQSIGKTPKQEGWKILFKQ
                     QTKEEKEDVQTLPLVIIGERAEVGVKSVEKETQPPKAFTEGTLLTAMKTANKTVDDEE
                     AIKILQEVEGIGTEATRASIIEALKQKEYIQVIKNKLVVTEKGKLLCQAVESQHLLTS
                     AEMTAKWETYLKKIGKREGNQENFITNIKKFIVHLLEAVPNDIEKLNFSDYQEQKEKE
                     AEKSIVGKCPKCGNNIVLKKSFYGCSNYPECKFTLAEHFRKKKLTKTNVKELLEGKET
                     LVKGIKNKEKKPYNAVVKIGEKGYIDFISFSK"
     gene            5336..5605
                     /locus_tag="EFD32_pA0005"
                     /db_xref="GeneID:13348239"
     CDS             5336..5605
                     /locus_tag="EFD32_pA0005"
                     /codon_start=1
                     /transl_table=11
                     /product="omega Transcriptional Repressor family protein"
                     /protein_id="REF_cebbi:EFD32_pA0005"
                     /protein_id="YP_006539252.1"
                     /db_xref="GI:397698497"
                     /db_xref="GeneID:13348239"
                     /translation="MIVGNLGAQKEKRNDTPISAKKDIMGDKTVRVRADLHHIIKIET
                     AKNGGNVKEVMEIRLRSKLKSVLIVQYLKILCIIGIEVKLDAKNL"
     gene            5834..6571
                     /locus_tag="EFD32_pA0006"
                     /db_xref="GeneID:13348240"
     CDS             5834..6571
                     /locus_tag="EFD32_pA0006"
                     /function="This protein produces a dimethylation of the
                     adenine residue at position 2085 in 23S rRNA, resulting in
                     reduced affinity between ribosomes and macrolide-
                     lincosamide-streptogramin B antibiotics."
                     /codon_start=1
                     /transl_table=11
                     /product="ErmB, rRNA adenine N-6-methyltransferase"
                     /protein_id="REF_cebbi:EFD32_pA0006"
                     /protein_id="YP_006539253.1"
                     /db_xref="GI:397698498"
                     /db_xref="GeneID:13348240"
                     /translation="MNKNIKYSQNFLTSEKVLNQIIKQLNLKETDTVYEIGTGKGHLT
                     TKLAKISKQVTSIELDSHLFNLSSEKLKLNTRVTLIHQDILQFQFPNKQRYKIVGSIP
                     YHLSTQIIKKVVFESHASDIYLIVEEGFYKRTLDIHRTLGLLLHTQVSIQQLLKLPAE
                     CFHPKPKVNSVLIKLTRHTTDVPDKYWKLYTYFVSKWVNREYRQLFTKNQFHQAMKHA
                     KVNNLSTVTYEQVLSIFNSYLLFNGRK"
     gene            7104..8000
                     /locus_tag="EFD32_pA0007"
                     /db_xref="GeneID:13348241"
     CDS             7104..8000
                     /locus_tag="EFD32_pA0007"
                     /codon_start=1
                     /transl_table=11
                     /product="ParA, putative ATPase"
                     /protein_id="REF_cebbi:EFD32_pA0007"
                     /protein_id="YP_006539254.1"
                     /db_xref="GI:397698499"
                     /db_xref="GeneID:13348241"
                     /translation="MIQYYYTKKEWGVVMEKEKLKILEELRRILNNKNEAIIILNNYF
                     KGGVGKSKLSTMFAYLTDKLNLKVLMIDKDLQATLTKDLAKTFEVELPRVNFYEGLKN
                     GNLASSIVHLTDNLDLIPGTFDLMLLPKLTRSWTFENESRLLATLLAPLKSDYDLIII
                     DTVPTPSVYTNNAIVASDYVMIPLQAEEESTNNIQNYISYLIDLQEQFNPGLDMIGFV
                     PYLVDTDSATIKSNLEELYKQHKEDNLVFRNIIKRSNKVSTWSKNGITEHKGYDKKVL
                     SMYENVFFEMIERIIQLENEKE"
     gene            8165..8308
                     /locus_tag="EFD32_pA0008"
                     /db_xref="GeneID:13348242"
     CDS             8165..8308
                     /locus_tag="EFD32_pA0008"
                     /codon_start=1
                     /transl_table=11
                     /product="omega Transcriptional Repressor family protein"
                     /protein_id="REF_cebbi:EFD32_pA0008"
                     /protein_id="YP_006539255.1"
                     /db_xref="GI:397698500"
                     /db_xref="GeneID:13348242"
                     /translation="MGDKTVRVRADLHHIIKIETAKNGGNVKEVMDQALEEYIRKYLP
                     DKL"
     gene            8413..8598
                     /locus_tag="EFD32_pA0009"
                     /db_xref="GeneID:13348243"
     CDS             8413..8598
                     /locus_tag="EFD32_pA0009"
                     /codon_start=1
                     /transl_table=11
                     /product="putative epsilon-antitoxin"
                     /protein_id="REF_cebbi:EFD32_pA0009"
                     /protein_id="YP_006539256.1"
                     /db_xref="GI:397698501"
                     /db_xref="GeneID:13348243"
                     /translation="MNHELDTKNTRLLEVNLLNQLEVAQEVDLFQQPFEELQAIHEYW
                     RSMNQYSKQILNKEKVA"
     gene            8600..9409
                     /locus_tag="EFD32_pA0010"
                     /db_xref="GeneID:13348244"
     CDS             8600..9409
                     /locus_tag="EFD32_pA0010"
                     /function="Part of a postsegregational killing (PSK)
                     system involved in the killing of plasmid-free cells. The
                     zeta-toxin induces programmed cell death (By similarity)."
                     /codon_start=1
                     /transl_table=11
                     /product="putative zeta-toxin"
                     /protein_id="REF_cebbi:EFD32_pA0010"
                     /protein_id="YP_006539257.1"
                     /db_xref="GI:397698502"
                     /db_xref="GeneID:13348244"
                     /translation="MANITDFTEKQFEDRLEKNVERLTKNRLAVESPTAFLLGGQPGS
                     GKTSLRSAIFEETQGNVIVIDNDTFKQQHPNFDELVKLYEKDVVKHVTPYSNRMTEAL
                     ISRLSDQGYNLVIEGTGRTTDVPIQTATMLQSKGYETKIYVMAVPKIESYLGTIERYE
                     TMYADDPMTARATPKQAHDIVVKNLPTNLETLHKTGLFSDIRLYNREGVKLYSSLETP
                     SISPKETLERELNRKVSGKEIQPTLERIEQKMVQNQHQGFCCKVSDKLILR"
     gene            9443..10123
                     /locus_tag="EFD32_pA0011"
                     /db_xref="GeneID:13348245"
     CDS             9443..10123
                     /locus_tag="EFD32_pA0011"
                     /function="Involved in the transposition of the insertion
                     sequence (By similarity)."
                     /codon_start=1
                     /transl_table=11
                     /product="putative IS431mec transposase"
                     /protein_id="REF_cebbi:EFD32_pA0011"
                     /protein_id="YP_006539258.1"
                     /db_xref="GI:397698503"
                     /db_xref="GeneID:13348245"
                     /translation="MTQFKGKQFQKDVIIVAVGYYLRYNLSYREVQEILYDRGIHVCH
                     TTIYRWVQEYGKILYQIWKKKNKQSFYSWKMDETYIKIKGKWHYLYRAIDVDGLTLDI
                     WLRKKRDTQAAYAFLKRLKNQFGEPKVLVTDKAPSIKSAFRKLQKNGLYITTEHRTIK
                     YLNNLIEQDHRPIKRRNKFYQSLRTASTTIKGMEAIRGIYKKSRKEGSLFGFSVCTEI
                     KGLLGIPA"
     gene            complement(10636..10740)
                     /locus_tag="EFD32_pA0012"
                     /db_xref="GeneID:13348246"
     CDS             complement(10636..10740)
                     /locus_tag="EFD32_pA0012"
                     /codon_start=1
                     /transl_table=11
                     /product="hypothetical protein"
                     /protein_id="REF_cebbi:EFD32_pA0012"
                     /protein_id="YP_006539259.1"
                     /db_xref="GI:397698504"
                     /db_xref="GeneID:13348246"
                     /translation="MENGSHFTKLSGWVITKYPLKQKRNETIVELDWK"
     gene            10875..11432
                     /locus_tag="EFD32_pA0013"
                     /db_xref="GeneID:13348247"
     CDS             10875..11432
                     /locus_tag="EFD32_pA0013"
                     /codon_start=1
                     /transl_table=11
                     /product="hypothetical protein"
                     /protein_id="REF_cebbi:EFD32_pA0013"
                     /protein_id="YP_006539260.1"
                     /db_xref="GI:397698505"
                     /db_xref="GeneID:13348247"
                     /translation="MDSIKLENISLAFKGKPIFKNLNFKIKKGETIGIIGANGVGKSV
                     LFKIICGLEKANSGKVFINDAEIGKKEDFPRNIGILINEPTFTPIYSGFKNLLLLAEI
                     NNVIDENEVREYMIKVGLDPDNKTVLRNYSLGMKKKLAICQAIMENQSIILLDEPFNG
                     LDFSTMSDIRSILDTLKKRKDYSVD"
     gene            12244..12492
                     /locus_tag="EFD32_pA0014"
                     /db_xref="GeneID:13348248"
     CDS             12244..12492
                     /locus_tag="EFD32_pA0014"
                     /codon_start=1
                     /transl_table=11
                     /product="CopS, plasmid copy control protein"
                     /protein_id="REF_cebbi:EFD32_pA0014"
                     /protein_id="YP_006539261.1"
                     /db_xref="GI:397698506"
                     /db_xref="GeneID:13348248"
                     /translation="MDLAYRERLKKMRGTKTVTKFAEELGMTKSNYSKIELGRTNASI
                     TTLQRIAKLTNSTLVVDLIPNEPNQSEQMELEIEKDKQ"
CONTIG      join(CP003727.1:1..12893)
ORIGIN      
        1 atgaatatcc cttttgttgt agaaaccgtg cttcatgacg gcttgttaaa gtataaattt
       61 agaaatagta aaattcgctc actcactacc aagccaggta aaagcaaagg ggctattttt
      121 gcgtaccgtt caaaatcaag catgattggc ggacgtggca ttgttctgac ttccgaggaa
      181 gcgattaacg aaaatcaaga tacatttacg cattggacac ccaacgttta tcgttatgga
      241 acatatgcag acgaaaaccg ttcatacact aaaggacatt ctgaaaacaa tttaagacaa
      301 atcaatacct tctttattga ttttgatatt cacacagcaa aagaagctat ttcagcaagc
      361 gatattttaa caacagctat tgatttaggt tttatgccta ctatgattat caaatctgat
      421 aaaggttatc aagcgtattt tgttttagaa acgccagtct atgtgacttc aaaatcagaa
      481 tttaagtctg tcaaagcagc caaaataatc tcgcaaaata tccgagaata ttttggaaag
      541 tctttgccag ttgatctaac gtgcaatcat tttgggattg ctcgcatacc aagaacggac
      601 aatgtagaat tttttgatcc caattaccgt tattctttca aagaatggca agattggtct
      661 ttcaaacaaa cagataataa gggctttact cgttcaagtc taacggtttt aagcggtaca
      721 gaaggcaaaa aacaagtaga cgaaccctgg tttaatctct tattacacga aacgaaattt
      781 tcaggagaaa aggggttagt agggcgtaat agcgttatgt ttactctctc tttagcttac
      841 tttagttcag gctattcaat cgaaacgtgc gaatataata tgtttgagtt taataatcga
      901 ttggatcaac ccttagaaga aaaagaagtg atcaaaattg ttagaagtgc ctattcagaa
      961 aactaccaag gggctaatag ggaatacatt accattcttt gcaaagcttg ggtatcaagt
     1021 gatttaacca gtaaagattt atttgtccgt caagggtggt ttaaattcaa gaaaaaaaga
     1081 agcgaacgtc aacgtgttca tttgtcagaa tggaaagaag atttaatggc ttatattagc
     1141 gaaaaaagcg atgtatacaa gccttattta gcgacgacta aaaaagagat tagagaagcg
     1201 ctaggcattc ctgaacggac attagataaa ttgctgaagg tactgaaggc gaaccaggag
     1261 attttcttta agattaaatc aggaagaaat ggtggtattc aacttgctag tgttaaatca
     1321 ttgttgctat cgatcattaa agttaaaaaa gaagaacgag aaagctatat aaaggcgctg
     1381 acagcttcgt ttaatttaga acgtacattt attcaagaaa ctctaaacaa gttggtagaa
     1441 cgtccaaaaa cagcgacaca actcgatttg tttggctatg atacaggctg aaattaaaac
     1501 ccgcactatg ccattacatt tatatctatg atacgtgttt gttttcttcc gtggtgttca
     1561 gtgattgatt agcagatatt aaaagaatga tattctattt attggttaca aggagaaggt
     1621 agcccgaaaa ctttttgtta aatatcgaag gagtttacga ctgatattta acaaaaacgt
     1681 cgaggggcag tggaagcgaa gcgcacactt gatcttttga atttaatttt ttcagtacac
     1741 tagagtaata ttttaaataa ttttatagaa aaggtggatt attatgtttg atttaactaa
     1801 agcagaatac gaagttttat ttatttttat gattcaagta agtcattaac taaacaggaa
     1861 ttgttagaaa gtgtactgag tttaaataag aatacaaccg cagctgtaat aagatcatta
     1921 ttagataaag gatatttaga gatagcagat attaggtatt ctacaactgt attggctaga
     1981 gcgtataaac cttgtgtgtc tatactagaa tttttgaaaa atgaatatgg ggaattaagt
     2041 gttgaaaagg tagtaggtca tttgataagc actatggaag atacacaggt attaaatcaa
     2101 ctattagcac ttatatcaga aaaaaatatt ctatccaaaa aggtagttga aagttaaaag
     2161 ataaacgata atgggacaat agagtatacc tatttgtcct aatatgattt tagcagtata
     2221 attgacttgg tgaataggtc atttaagttg ggcataatag gaggagtaaa atgaaaaaat
     2281 ttatttatcg agttttagaa aatgacgaag tggtggctat ttttaatgag caacaatatg
     2341 cgcaagattt tatcgcttac gaaaagacaa tttctgataa gcaatttgaa attgaaaaag
     2401 tagatattgc tgattggtta ttgcaaccga gagaatttta gaggttggtt gaaaatggct
     2461 aaaattggtt atgcacgtgt cagtagcaaa gaacagaact tagatcggca attacaagcg
     2521 ttacagggcg tttctaaggt cttttcagac aaattaagcg gtcaatcggt cgaacgccca
     2581 caattacaag ctatgcttaa ctatattcgt gaaggggata ttgttattgt tactgaatta
     2641 gatcgattag gacgaaataa taaagaatta acagaattga tgaatcaaat tcaaattaag
     2701 ggggcaaccc tggaagtctt aaatttaccc tcaatgaatg gtattgaaga tgaaaattta
     2761 aggcgtttga ttaatagcct tgtcattgaa ttgtacaagt atcaagcaga atcagaacga
     2821 aaaaaaatta aggaacgtca ggcacaagga atcgaaattg ctaagaaaaa aggcaaattc
     2881 aaaggtcgtc agcataaatt taaagaaaat gatccacgtt taaagcatgc tttcgatttg
     2941 tttttgaatg gtttatccga taaagaagtt gaagaacaaa ctggaatcaa tcgccgaacg
     3001 tttagaaggt atcgagcaag atataacgtg acagtcgatc aaagaaaaaa caatgaaaag
     3061 agggatagtt aatgagtacg gttattttag ctgaaaaacc aagccaggca ttagcctacg
     3121 caagtgcttt aaaacaaagc accaaaaaag acggttattt tgagatcaaa gacccactat
     3181 ttacagatga aacgtttatc acctttggtt ttgggcattt agtggaatta gcagaaccag
     3241 gtcattatga cgaaaagtgg caaaattgga aacttgaatc tttgccgatt tttcctgatc
     3301 gatacgattt tgaagttgca aaagataagg gaaagcagtt taaaattgtt gcagaacttc
     3361 tcaaaaaggc aaatacaatt attgttgcaa cagatagcga cagagaaggt gaaaatatcg
     3421 cctggtcgat tatccataaa gcaaatgcct tttcaaaaga taaaacattt aaaagactat
     3481 ggatcaatag cttagaaaaa gatgtaatcc gaagcggttt tcaaaatttg caacctggaa
     3541 tgaattacta tcccttttat caagaagcgc aaacacgcca aattgccgat tggttgatcg
     3601 gcatgaacgc aagccctttg tatacgttaa atttacaaca gaagggcgta caaggtacat
     3661 tttcactagg acgtgttcaa acgcccacct tataccttat ttttcagcgc caggaagcca
     3721 tagagaattt taaaaaagaa ccttttttcg aggtggaagc tagtataaaa gtaaaccaag
     3781 ggtcgtttaa gggcgttcta agccccacac agcgttttaa aacccaagag gagcttttag
     3841 cttttgtttc ttctaaacaa gctaaaatag gcaatcaaga ggggataatt gctgatgttc
     3901 aaaccaaaga gaagaaaacg aatagtccga gtttgttttc tttaagtagt ttgcaatcaa
     3961 aagtcaatca gctttataaa gcgacagcga gccaaacttt aaaagctatg caaggactgt
     4021 atgaagcaaa attattgagt tatccaagaa cagatacacc atttattaca gagaacgaat
     4081 ttgcttattt aaaagcgaat tttggcaaat atagcggttt tttaggactt gatcttgaaa
     4141 tggttcaaac agagcctaga aagcgttatg tggacggtag taaggtacag gaacaccacg
     4201 ccattatctc aacaaaacaa gtacctaccg aatctgcatt agcgaaaatg gacgatttac
     4261 aacgaaaaat ttatgcttta gtcgttaaaa cgaccgttgc catgttttta cctgattact
     4321 tgtatgaaga aaccaagata caaacgaaag tagctgattt gctgtttcaa tcgatcggaa
     4381 agacaccaaa gcaagaaggg tggaaaattc ttttcaaaca acaaaccaaa gaagaaaaag
     4441 aggacgttca aacgttacca ctcgttatca ttggcgaacg tgccgaagtt ggtgttaaga
     4501 gtgttgaaaa agaaacgcaa ccgccaaaag cttttacaga gggtacatta ttaactgcta
     4561 tgaaaacggc gaataaaacg gttgatgatg aagaagcaat caagatttta caagaagttg
     4621 aggggattgg aacagaagcg acaagagcaa gtattattga agcgttaaaa caaaaagaat
     4681 atatccaagt gattaagaat aagcttgttg taactgaaaa aggaaaatta ttgtgccagg
     4741 cagttgaaag tcagcacctt ttaacgagtg ctgaaatgac ggctaaatgg gaaacgtatt
     4801 taaaaaaaat cggtaaaaga gaaggcaatc aagagaactt tattacgaat atcaaaaaat
     4861 tcattgttca tttactggaa gctgtaccta acgatataga aaaactaaat ttttctgatt
     4921 accaggaaca gaaagaaaaa gaagcagaaa aaagtattgt aggaaaatgt cctaagtgtg
     4981 gcaacaatat tgtattaaaa aaatcgtttt atggttgttc aaattatcct gaatgtaagt
     5041 ttactttagc tgaacatttt agaaagaaaa aactaaccaa aacgaatgta aaagaattac
     5101 tggagggaaa agaaaccctg gtaaaaggaa tcaaaaacaa agagaaaaag ccctacaatg
     5161 ccgttgtaaa aattggggaa aagggatata ttgattttat atctttctca aaataaacat
     5221 aaaagccctt taaagagggc ttttatatat taatcacaaa tcacttatca caaatcacaa
     5281 gtgatttgtg attgttgatg ataaaataag aataagaaga aatagaaaga agtgagtgat
     5341 tgtgggaaat ttaggcgcac aaaaagaaaa acgaaatgat acaccaatca gtgcaaaaaa
     5401 agatataatg ggagataaga cggttcgtgt tcgtgctgac ttgcaccata tcataaaaat
     5461 cgaaacagca aagaatggcg gaaacgtaaa agaagttatg gaaataagac ttagaagcaa
     5521 acttaagagt gtgttgatag tgcagtatct taaaattttg tgtataatag gaattgaagt
     5581 taaattagat gctaaaaatt tgtaattaag aaggagggat tcgtcatgtt ggtattccaa
     5641 atgcgtaatg tagataaaac atctactgtt ttgaaacaga ctaaaaacag tgattacgca
     5701 gataaataaa tacgttagat taattcctac cagtgactaa tcttatgact ttttaaacag
     5761 ataactaaaa ttacaaacaa atcgtttaac ttctgtattt atttacagat gtaatcactt
     5821 caggagtgat tacatgaaca aaaatataaa atattctcaa aactttttaa cgagtgaaaa
     5881 agtactcaac caaataataa aacaattgaa tttaaaagaa accgataccg tttacgaaat
     5941 tggaacaggt aaagggcatt taacgacgaa actggctaaa ataagtaaac aggtaacgtc
     6001 tattgaatta gacagtcatc tattcaactt atcgtcagaa aaattaaaac tgaacactcg
     6061 tgtcacttta attcaccaag atattctaca gtttcaattc cctaacaaac agaggtataa
     6121 aattgttggg agtattcctt accatttaag cacacaaatt attaaaaaag tggtttttga
     6181 aagccatgcg tctgacatct atctgattgt tgaagaagga ttctacaagc gtaccttgga
     6241 tattcaccga acactagggt tgctcttgca cactcaagtc tcgattcagc aattgcttaa
     6301 gctgccagcg gaatgctttc atcctaaacc aaaagtaaac agtgtcttaa taaaacttac
     6361 ccgccatacc acagatgttc cagataaata ttggaagcta tatacgtact ttgtttcaaa
     6421 atgggtcaat cgagaatatc gtcaactgtt tactaaaaat cagtttcatc aagcaatgaa
     6481 acacgccaaa gtaaacaatt taagtaccgt tacttatgag caagtattgt ctatttttaa
     6541 tagttatcta ttatttaacg ggaggaaata attctatgag tcgctttttt aaatttggaa
     6601 agttacacgt tactaaaggg aatggagata aattattaga tatactactg acagcttcca
     6661 agaagctaaa gaggtcccta gcgcctacgg ggaatttgta tcgataaggg gtacaaattc
     6721 ccactaagcg ctcgggaccc cttgtaggaa aatgtcctaa gtgtggcaac aatattgtat
     6781 taaaaaaatc gttttatggt tgttcaaatt atcctgaatg taagtttact ttagctgaac
     6841 attttagaaa gaaaaaacta accaaaacga atgtaaaaga attactggag ggaaaagaaa
     6901 ccctggtaaa aggaatcaaa aacaaagaga aaaagcccta caatgccgtt gtaaaaattg
     6961 gggaaaaggg atatattgat tttatctctt tttcaaaata aacataaaag ccctttaaag
     7021 agggctttta tatattaatc acaaatcact tatcacaaat cacaagtgat taatcacaaa
     7081 tcacttgtga tttgtgattc ttaatgatac aatattacta tacaaaaaaa gaatggggcg
     7141 tagttatgga gaaggaaaaa ctaaaaatac ttgaagaatt aagacgtatt ttaaacaata
     7201 aaaatgaagc aattattatc ttgaataatt actttaaagg tggtgttgga aagtccaaat
     7261 tatcgactat gtttgcttac ttgacagaca aattgaattt aaaagtttta atgatcgata
     7321 aggacttaca ggcaacattg acaaaagact tagcaaaaac ttttgaggta gaattgccac
     7381 gtgtcaattt ttatgaaggc ttgaaaaatg gaaacttggc ttcttctatt gttcatttga
     7441 ctgataattt agacttgatc cctggcacgt ttgatttgat gttactgcca aaattaactc
     7501 gctcatggac ttttgaaaat gaaagtagat tgcttgctac tcttttagca cctttaaaaa
     7561 gtgactatga tctcattatt attgatactg taccaacgcc aagcgtttat acaaataatg
     7621 caatcgtggc gagtgattac gttatgatcc ctttacaagc agaagaagaa agtacaaaca
     7681 acattcaaaa ctatatttcc tatttgattg atttacaaga acaatttaac cctggactag
     7741 atatgatcgg ttttgttcct tatttagttg atacggacag cgcaacgata aaatcaaacc
     7801 tggaagaact gtacaagcaa cataaagaag ataacttggt tttccgaaat attatcaagc
     7861 gaagtaataa agtaagtact tggtctaaaa atggcattac agaacacaaa ggctatgaca
     7921 aaaaagtttt gtctatgtat gagaacgtat tttttgaaat gattgagcga atcattcaat
     7981 tagaaaatga aaaagaatag aatcacaaat cacaagtgat taatcacaaa tcacttgtga
     8041 tttgtgatag gtgatgataa aataaatagt aagaagaaat agaaagaagt gagcgatcgt
     8101 gggaaattta ggcgcacaga aagcaaaacg aaatgatacg ccaatcagcg caaaaaaaga
     8161 tataatgggg gataagacgg ttcgtgttcg tgccgacttg caccatatca taaaaatcga
     8221 aacagcaaag aatggcggaa acgtaaaaga agttatggat caagccttag aagaatatat
     8281 acggaaatat ttacctgaca aactttaaaa aggagtgaaa atgaaatggc agttacgtat
     8341 gaaaaaacat ttgaaattga aatcattaat gagttatcgg caagtgttta taatcgggta
     8401 ttaaattatg ttttgaatca tgaactagat actaaaaata ctcgtttact agaagtgaat
     8461 cttttaaatc aattagaagt ggcacaagaa gttgatttat ttcaacaacc atttgaagaa
     8521 ttacaagcta ttcatgagta ttggcggtca atgaatcaat attcaaaaca aattttgaat
     8581 aaagagaaag tggcttaaca tggcgaatat tactgacttc accgaaaagc aatttgaaga
     8641 tcgtttagaa aagaatgttg aacgactaac taaaaataga ctagcggttg aatcgccaac
     8701 cgctttttta cttggtgggc aaccagggtc agggaaaacc agtttgcgat cagcaatttt
     8761 tgaagaaaca caagggaatg ttattgtcat tgataatgat acctttaaac aacagcaccc
     8821 aaattttgac gaattagtga aactctatga aaaagacgta gtaaaacacg ttacccctta
     8881 ttcgaatcgc atgacagaag cacttattag ccgtttaagc gatcaaggct ataatttagt
     8941 gattgaagga acaggacgaa caacagacgt tcccattcaa acagccacaa tgcttcaatc
     9001 taaaggttat gaaacaaaaa tatacgtcat ggcagtaccg aaaattgaat catacttagg
     9061 aacgattgaa cgatatgaaa ccatgtatgc agatgatcca atgacagcta gggcaacacc
     9121 aaaacaagcg catgatattg ttgtcaaaaa cttaccgacc aatttagaaa cccttcataa
     9181 aacgggctta tttagcgata taaggcttta caatagagaa ggagtgaaac tctattcaag
     9241 cttagaaacg ccttccatta gtccgaaaga gaccttagaa agagaattaa atcgtaaagt
     9301 atcagggaaa gaaattcaac cgactttgga gagaatagag caaaaaatgg ttcaaaatca
     9361 acaccaaggg ttctgttgca aagtttccga taagctcatt ttgaggtaaa ataattgaaa
     9421 aagatagctt ggaggaatga agatgactca gttcaaagga aaacaatttc aaaaagacgt
     9481 gattatcgta gccgtcggct actatcttcg gtacaacctg agttatcgtg aagtgcagga
     9541 aattctgtat gaccgaggaa ttcacgtttg tcacaccaca atttaccgtt gggtgcaaga
     9601 atacggaaag attctttatc aaatttggaa aaagaaaaac aaacagtcct tttattcatg
     9661 gaaaatggac gagacttata tcaaaattaa aggaaagtgg cattatttat atcgcgctat
     9721 cgatgtagat ggcttaacct tagatatctg gttacggaaa aagcgcgaca cacaggctgc
     9781 gtatgctttt ctaaaacgac tgaagaacca gtttggagaa ccaaaggttc tagtaacgga
     9841 taaagcaccc tctattaaga gtgccttcag aaagcttcag aaaaatggac tgtatataac
     9901 aacagaacat cgaacaatca agtatctgaa taatctgatt gaacaagacc atcgtccaat
     9961 taagcgaaga aacaaatttt accaaagttt gcgaacagcc tcaaccacga ttaaaggcat
    10021 ggaagcgatt cgaggaattt ataaaaaaag ccgaaaagaa gggtctcttt ttggcttttc
    10081 cgtctgtaca gaaatcaaag gactattagg aatccctgct taaataagaa tcatcttgaa
    10141 aaactaatga cctttttgag actttgcaac agaaccttcg agagccttct agtgttacca
    10201 ttctaatccg atcatctttc ttcgctattg aaagaatcaa attcatcatt tcctgttcac
    10261 ttctcattat gaaacactcc ttttattctt acttatgtat tgttgtacac ctattttagc
    10321 ataggagaag attatataca gtcggtgaaa gtttatttta gtctaaaaat cggatagaat
    10381 gcagagaagc agtcgactca actagaaaag cagagtgggt tgactgtatt gttggtctct
    10441 aggcttgact cactcggtta ttctgtttga gttaattaca gtacttaagt atgtcgcaat
    10501 acacttgagt attcttatga ttgtattgtc tatactcatt aatctatcag gtgttactat
    10561 ggacgggctt ctttttagga gtatagtgtt aaattttttg gaaattattt ttattacaat
    10621 cacctactta ttaatttatt tccaatcaag ttctacgatt gtttcatttc ttttttgttt
    10681 aagtgggtat ttggtgatta cccatccact aagttttgta aaatggcttc cattttccat
    10741 gtcttctgtc ttaagagcaa gtgatattgg atataatcta gtaattatga tgtttttagg
    10801 aattatatta attgaagttt attacattat gtattcaaaa aaagtgacaa ttaagtttgg
    10861 aaggaatgag catattggat agtattaaat tggaaaatat aagtttagca tttaaaggaa
    10921 aaccaatttt taaaaattta aattttaaaa taaaaaaagg tgaaacgata ggcataattg
    10981 gagctaatgg tgtaggaaaa tctgttttat tcaaaataat ttgtggattg gaaaaagcta
    11041 atagcggtaa agtatttatc aatgatgcag aaattggaaa gaaagaagat ttcccacgaa
    11101 atataggtat tttaattaat gaacctacgt ttacccctat ctatagtggt tttaaaaatt
    11161 tattactttt agctgaaatt aataatgtta tcgatgaaaa cgaagtaagg gaatatatga
    11221 ttaaagtagg acttgatcct gataacaaaa cagtcttaag aaattactcg ttaggtatga
    11281 aaaagaaatt agcaatttgt caggcgatta tggaaaatca atctattatt cttttggacg
    11341 aaccttttaa tggtttagat ttttctacga tgtcagatat tcgttctata cttgatactt
    11401 taaaaaaaag gaaagactat agtgttgact agtcaccatc aaaatgattt agattccttt
    11461 tgtgataagt tatatttcat tgataatcaa aatctaattg tttttacaga cgaattgaga
    11521 gagaaatatt tttcttaatt ctagtgtgat aaatgtcaaa ttcagcaact tggcttcatt
    11581 ttgaaatcat gctataatga atgtgcgaaa tgtgaagatt tcgagcatgt atagcacaaa
    11641 agcccaacct attacgagta ggttgggctt ttttttcact agttgcgacg gctagcgttg
    11701 tgggttagtc agcaacttgc ttttaatcgt cgtctttctc gtctaaccaa tgagaaacta
    11761 gttcaatgac taagcccacg aaaagtggtg cgacaatcaa tgtgaagagt tgttggagca
    11821 tgtatagcac cccctttcgg ggcaagttgc cgttaattag tatagcatac ttaatattct
    11881 aagcagttga taaaattaag gtaaaagtta aaatcacaaa tcacaagtga tttgtgatta
    11941 tctgttttta catgataaat aagtcgtgat acaatgaatt tgtaagagca ataaaggatc
    12001 aaactattcg ctgtatgttt gactattttc ttttctatgg ttttaaatac ctgaaaagaa
    12061 ttgttgctag aaaaccaatc cataaaacag gattggtttt tctttactta aaaaaataac
    12121 ctatcacaaa tcacttatca caaatcacaa gtgattaatc acaaatcact tgtttataaa
    12181 gagattaaaa gctataattt aaaataaagc gtgaatttta tgacacacaa aaagggtgat
    12241 tatatggact tagcttatag agagcgcttg aaaaaaatga ggggtacaaa gacagttact
    12301 aaatttgctg aagaactagg catgacaaaa tcaaattatt ctaaaataga attaggaaga
    12361 accaatgctt caattacaac attacaacga atagcaaaac tgactaattc aaccttagta
    12421 gttgacttga ttccaaatga accaaatcaa tcagaacaga tggaattgga gatagaaaaa
    12481 gataagcaat aattttgtgt aattaactga cacgaaatta ttgcttaatt ttttttaaag
    12541 aggtatactt agttataaca agtaaatata ttcaaattga aaatgtgcag acaaaaagca
    12601 ctagcttata gagtggctac caaccacttt ataagaatct cgattgatac cgacatatca
    12661 atgaaattac ttctagtacc ttgtccgctt ataatatcgc ttttcataac tctattatag
    12721 caaacaaagt gctaaaatag tagttcaaag cttattataa cggtaaagtg gaagaaaatc
    12781 aagatagtag tctaaggtat caatcaaaat cactgattga tacctttttt gttgtgatcc
    12841 aactttgatt gtagcttcta aattaatttt ttgtaagaaa ggagaacagc tga
//
