#!/usr/local/bin/perl

package Object_stat_avg_homologies;

use strict;
use Carp qw(confess carp);


sub new {
    my $class = shift;

    my $self = {

	_avg_score => "",
	_stddev_score => "",
	_avg_e_value => "",
	_stddev_e_value => "",
	_avg_identity => "",
	_stddev_identity => "",
	_avg_q_align_frac => "",
	_stddev_q_align_frac => "",
	_avg_s_align_frac => "",
	_stddev_s_align_frac => "",
	_avg_q_length => "",
	_stddev_q_length => "",
	_avg_s_length => "",
	_stddev_s_length => "",
    };

    bless $self, $class;
    return $self;
}


sub init {

    my $class = shift;
    my $avg_score = shift;
    my $stddev_score = shift;
    my $avg_e_value = shift;
    my $stddev_e_value = shift;
    my $avg_identity = shift;
    my $stddev_identity = shift;
    my $avg_q_align_frac = shift;
    my $stddev_q_align_frac = shift;
    my $avg_s_align_frac = shift;
    my $stddev_s_align_frac = shift;
    my $avg_q_length = shift;
    my $stddev_q_length = shift;
    my $avg_s_length = shift;
    my $stddev_s_length = shift;

    set_avg_score($class, $avg_score);
    set_stddev_score($class, $stddev_score);
    set_avg_e_value($class, $avg_e_value);
    set_stddev_e_value($class, $stddev_e_value);
    set_avg_identity($class, $avg_identity);
    set_stddev_identity($class, $stddev_identity);
    set_avg_q_align_frac($class, $avg_q_align_frac);
    set_stddev_q_align_frac($class, $stddev_q_align_frac);
    set_avg_s_align_frac($class, $avg_s_align_frac);
    set_stddev_s_align_frac($class, $stddev_s_align_frac);
    set_avg_q_length($class, $avg_q_length);
    set_stddev_q_length($class, $stddev_q_length);
    set_avg_s_length($class, $avg_s_length);
    set_stddev_s_length($class, $stddev_s_length);

}

# _avg_score => "",
sub set_avg_score {
    my ( $self, $avg_score ) = @_;
    if ( defined($avg_score) ) {
	$self->{_avg_score} = $avg_score;
	return $self->{_avg_score};
    } else {
	confess "ERROR in set_avg_score : undefined avg_score\n";
    }
}
sub get_avg_score {
    my( $self ) = @_;
    return $self->{_avg_score};
}

# _stddev_score => "",
sub set_stddev_score {
    my ( $self, $stddev_score ) = @_;
    if ( defined($stddev_score) ) {
	$self->{_stddev_score} = $stddev_score;
	return $self->{_stddev_score};
    } else {
	confess "ERROR in set_stddev_score : undefined stddev_score\n";
    }
}
sub get_stddev_score {
    my( $self ) = @_;
    return $self->{_stddev_score};
}

# _avg_e_value => "",
sub set_avg_e_value {
    my ( $self, $avg_e_value ) = @_;
    if ( defined($avg_e_value) ) {
	$self->{_avg_e_value} = $avg_e_value;
	return $self->{_avg_e_value};
    } else {
	confess "ERROR in set_avg_e_value : undefined avg_e_value\n";
    }
}
sub get_avg_e_value {
    my( $self ) = @_;
    return $self->{_avg_e_value};
}

# _stddev_e_value => "",
sub set_stddev_e_value {
    my ( $self, $stddev_e_value ) = @_;
    if ( defined($stddev_e_value) ) {
	$self->{_stddev_e_value} = $stddev_e_value;
	return $self->{_stddev_e_value};
    } else {
	confess "ERROR in set_stddev_e_value : undefined stddev_e_value\n";
    }
}
sub get_stddev_e_value {
    my( $self ) = @_;
    return $self->{_stddev_e_value};
}

# _avg_identity => "",
sub set_avg_identity {
    my ( $self, $avg_identity ) = @_;
    if ( defined($avg_identity) ) {
	$self->{_avg_identity} = $avg_identity;
	return $self->{_avg_identity};
    } else {
	confess "ERROR in set_avg_identity : undefined avg_identity\n";
    }
}
sub get_avg_identity {
    my( $self ) = @_;
    return $self->{_avg_identity};
}

# _stddev_identity => "",
sub set_stddev_identity {
    my ( $self, $stddev_identity ) = @_;
    if ( defined($stddev_identity) ) {
	$self->{_stddev_identity} = $stddev_identity;
	return $self->{_stddev_identity};
    } else {
	confess "ERROR in set_stddev_identity : undefined stddev_identity\n";
    }
}
sub get_stddev_identity {
    my( $self ) = @_;
    return $self->{_stddev_identity};
}


# _avg_q_align_frac => "",
sub set_avg_q_align_frac {
    my ( $self, $avg_q_align_frac ) = @_;
    if ( defined($avg_q_align_frac) ) {
	$self->{_avg_q_align_frac} = $avg_q_align_frac;
	return $self->{_avg_q_align_frac};
    } else {
	confess "ERROR in set_avg_q_align_frac : undefined avg_q_align_frac\n";
    }
}
sub get_avg_q_align_frac {
    my( $self ) = @_;
    return $self->{_avg_q_align_frac};
}


# _stddev_q_align_frac => "",
sub set_stddev_q_align_frac {
    my ( $self, $stddev_q_align_frac ) = @_;
    if ( defined($stddev_q_align_frac) ) {
	$self->{_stddev_q_align_frac} = $stddev_q_align_frac;
	return $self->{_stddev_q_align_frac};
    } else {
	confess "ERROR in set_stddev_q_align_frac : undefined stddev_q_align_frac\n";
    }
}
sub get_stddev_q_align_frac {
    my( $self ) = @_;
    return $self->{_stddev_q_align_frac};
}

# _avg_s_align_frac => "",
sub set_avg_s_align_frac {
    my ( $self, $avg_s_align_frac ) = @_;
    if ( defined($avg_s_align_frac) ) {
	$self->{_avg_s_align_frac} = $avg_s_align_frac;
	return $self->{_avg_s_align_frac};
    } else {
	confess "ERROR in set_avg_s_align_frac : undefined avg_s_align_frac\n";
    }
}
sub get_avg_s_align_frac {
    my( $self ) = @_;
    return $self->{_avg_s_align_frac};
}


# _stddev_s_align_frac => "",
sub set_stddev_s_align_frac {
    my ( $self, $stddev_s_align_frac ) = @_;
    if ( defined($stddev_s_align_frac) ) {
	$self->{_stddev_s_align_frac} = $stddev_s_align_frac;
	return $self->{_stddev_s_align_frac};
    } else {
	confess "ERROR in set_stddev_s_align_frac : undefined stddev_s_align_frac\n";
    }
}
sub get_stddev_s_align_frac {
    my( $self ) = @_;
    return $self->{_stddev_s_align_frac};
}


# _avg_q_length => "",
sub set_avg_q_length {
    my ( $self, $avg_q_length ) = @_;
    if ( defined($avg_q_length) ) {
	$self->{_avg_q_length} = $avg_q_length;
	return $self->{_avg_q_length};
    } else {
	confess "ERROR in set_avg_q_length : undefined avg_q_length\n";
    }
}
sub get_avg_q_length {
    my( $self ) = @_;
    return $self->{_avg_q_length};
}


# _stddev_q_length => "",
sub set_stddev_q_length {
    my ( $self, $stddev_q_length ) = @_;
    if ( defined($stddev_q_length) ) {
	$self->{_stddev_q_length} = $stddev_q_length;
	return $self->{_stddev_q_length};
    } else {
	confess "ERROR in set_stddev_q_length : undefined stddev_q_length\n";
    }
}
sub get_stddev_q_length {
    my( $self ) = @_;
    return $self->{_stddev_q_length};
}


# _avg_s_length => "",
sub set_avg_s_length {
    my ( $self, $avg_s_length ) = @_;
    if ( defined($avg_s_length) ) {
	$self->{_avg_s_length} = $avg_s_length;
	return $self->{_avg_s_length};
    } else {
	confess "ERROR in set_avg_s_length : undefined avg_s_length\n";
    }
}
sub get_avg_s_length {
    my( $self ) = @_;
    return $self->{_avg_s_length};
}


# _stddev_s_length => "",
sub set_stddev_s_length {
    my ( $self, $stddev_s_length ) = @_;
    if ( defined($stddev_s_length) ) {
	$self->{_stddev_s_length} = $stddev_s_length;
	return $self->{_stddev_s_length};
    } else {
	confess "ERROR in set_stddev_s_length : undefined stddev_s_length\n";
    }
}
sub get_stddev_s_length {
    my( $self ) = @_;
    return $self->{_stddev_s_length};
}


1;
