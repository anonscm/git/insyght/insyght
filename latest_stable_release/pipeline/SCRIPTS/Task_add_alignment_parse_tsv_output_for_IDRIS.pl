#!/usr/local/bin/perl
#
# Task_add_alignment_parse_tsv_output_for_IDRIS.pl
#
# TO TEST : suitable for incremental adding of genomes: add checking of various sorts ?
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#


use strict;
use Time::HiRes qw(sleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;


# whole script scoped variable
my $max_curr_alignment_param_id;
my $max_curr_alignment_id;
my @alignment_pairs_table_tsv_file            = ();
my @alignment_params_table_tsv_file            = ();
my @alignment_table_tsv_file            = ();
my @homologies_table_tsv_file            = ();
my @intermediary_alignment_table_tsv_file            = ();
my $st;
my $res;
my $TSV_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv";
my $SQL_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sql";
#my $INPUT_DIR_IS_TAR = "OFF";

foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-TSV_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$TSV_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG "incorrect -TSV_INPUT_DIR argument ; usage : perl Task_add_alignment_parse_tsv_output_for_IDRIS.pl  -TSV_INPUT_DIR {PATH_TO_DIRECTORY}";
			die "incorrect -TSV_INPUT_DIR argument ; usage : perl Task_add_alignment_parse_tsv_output_for_IDRIS.pl  -TSV_INPUT_DIR {PATH_TO_DIRECTORY}";
		}
	}elsif ( $ARGV[$argnum] =~ m/^-SQL_OUTPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$SQL_OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG "incorrect -SQL_OUTPUT_DIR argument ; usage : perl Task_add_alignment_parse_tsv_output_for_IDRIS.pl  -SQL_OUTPUT_DIR {PATH_TO_DIRECTORY}";
			die "incorrect -SQL_OUTPUT_DIR argument ; usage : perl Task_add_alignment_parse_tsv_output_for_IDRIS.pl  -SQL_OUTPUT_DIR {PATH_TO_DIRECTORY}";
		}
=pod
	}elsif ( $ARGV[$argnum] =~ m/^-INPUT_DIR_IS_TAR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i)
		{
			$INPUT_DIR_IS_TAR = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG "incorrect -INPUT_DIR_IS_TAR argument ; usage : perl Task_add_alignment_parse_tsv_output_for_IDRIS.pl -INPUT_DIR_IS_TAR {ON/OFF}";
			die "incorrect -INPUT_DIR_IS_TAR argument ; usage : perl Task_add_alignment_parse_tsv_output_for_IDRIS.pl -INPUT_DIR_IS_TAR {ON/OFF}";
		}
=cut
	}
}


# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_parse_tsv_output_for_IDRIS.log"
);
print LOG
"\n---------------------------------------------------\n Task_add_alignment_parse_tsv_output_for_IDRIS.pl started at :",
  scalar(localtime), "\n";

# print in and out directory
print LOG "\n---------------------------------------------------\n\tThe TSV input directory is : $TSV_INPUT_DIR\n\tThe SQL output directory is : $SQL_OUTPUT_DIR\n";


# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::LOGDIR/Task_blast_all/`;
`$SiteConfig::CMDDIR/mkdir -p $SQL_OUTPUT_DIR`;

print LOG "Deleting files under $SQL_OUTPUT_DIR/* \n";
`$SiteConfig::CMDDIR/rm -r -f $SQL_OUTPUT_DIR/*`;


# get max current alignment_param_id
$st = $ORIGAMI::dbh->prepare("SELECT max(alignment_param_id) FROM alignment_params");
$st->execute or die("Pb execute $!");
while ( $res = $st->fetchrow_hashref() ) {
	$max_curr_alignment_param_id = $res->{max};
}
if($max_curr_alignment_param_id <= 0){
	$max_curr_alignment_param_id = 0;
}

$max_curr_alignment_param_id++;

print LOG "\nthe alignment_param_id that will be used for start is $max_curr_alignment_param_id \n";

#get max current alignments_id
#$st = $ORIGAMI::dbh->selectall_arrayref("SELECT max(alignment_id) FROM alignments") or die "database request get max current alignment_id failed";
$st = $ORIGAMI::dbh->prepare("SELECT max(alignment_id) FROM alignments");
$st->execute or die("Pb execute $!");

while ( $res = $st->fetchrow_hashref() ) {
	$max_curr_alignment_id = $res->{max};
}
if($max_curr_alignment_id <= 0){
	$max_curr_alignment_id = 0;
}
$max_curr_alignment_id++;

print LOG "the alignment_id that will be used for start is $max_curr_alignment_id \n\n";



sub sub_list_alignment_params_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die "Unable to open $path: $!";
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_alignment_params_files_recursively ($_);
        } elsif ($_ =~ m/^.+_alignment_params_table\.tsv$/ ) {
            # File *.blast.ll, store it in @launch_blast_batch_files_unsorted
            push ( @alignment_params_table_tsv_file, $_);
	} else {
            print LOG "Error in sub_list_alignment_params_files_recursively : the file $_ does not match the appropriate regex";
            die("Error in sub_list_alignment_params_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
#get all *_alignment_params_table.tsv file
#@alignment_params_table_tsv_file = <$TSV_INPUT_DIR/*_alignment_params_table.tsv>;
sub_list_alignment_params_files_recursively("$TSV_INPUT_DIR/alignment_params/");
print LOG "\n\n".@alignment_params_table_tsv_file." alignment_params_table_tsv_file found \n";


sub sub_list_alignments_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die "Unable to open $path: $!";
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_alignments_files_recursively ($_);
        } elsif ($_ =~ m/^.+_alignment_table\.tsv$/ ) {
            # File *.blast.ll, store it in @launch_blast_batch_files_unsorted
            push ( @alignment_table_tsv_file, $_);
        } else {
            print LOG "Error in sub_list_alignments_files_recursively : the file $_ does not match the appropriate regex";
            die("Error in sub_list_alignments_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
#get all *_alignment_table.tsv file
#@alignment_table_tsv_file = <$TSV_INPUT_DIR/*_alignment_table.tsv>;
sub_list_alignments_files_recursively("$TSV_INPUT_DIR/alignments/");
print LOG @alignment_table_tsv_file." alignment_table_tsv_file found \n";


sub sub_list_alignment_pairs_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die "Unable to open $path: $!";
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_alignment_pairs_files_recursively ($_);
        } elsif ($_ =~ m/^.+_alignment_pairs_table\.tsv$/ ) {
            # File *.blast.ll, store it in @launch_blast_batch_files_unsorted
            push ( @alignment_pairs_table_tsv_file, $_);
        } else {
            print LOG "Error in sub_list_alignment_pairs_files_recursively : the file $_ does not match the appropriate regex";
            die("Error in sub_list_alignment_pairs_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
#get all *_alignment_pairs_table.tsv file
#@alignment_pairs_table_tsv_file = <$TSV_INPUT_DIR/*_alignment_pairs_table.tsv>;
sub_list_alignment_pairs_files_recursively("$TSV_INPUT_DIR/alignment_pairs/");
print LOG @alignment_pairs_table_tsv_file." alignment_pairs_table_tsv_file found \n";


sub sub_list_homologies_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die "Unable to open $path: $!";
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_homologies_files_recursively ($_);
        } elsif ($_ =~ m/^.+_homologies_table\.tsv$/ ) {
            # File *.blast.ll, store it in @launch_blast_batch_files_unsorted
            push ( @homologies_table_tsv_file, $_);
        } else {
            print LOG "Error in sub_list_homologies_files_recursively : the file $_ does not match the appropriate regex";
            die("Error in sub_list_homologies_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
#get all *_homologies_table.tsv file
#@homologies_table_tsv_file = <$TSV_INPUT_DIR/*_homologies_table.tsv>;
sub_list_homologies_files_recursively("$TSV_INPUT_DIR/homologies/");
print LOG @homologies_table_tsv_file." homologies_table_tsv_file found \n";


# for each *_alignment_params_table.tsv file and its associate *_alignment_table.tsv file, replace Forward and Reverse by appropriate Iary key alignment_param_id
print LOG "\nReplacing Forward and Reverse by appropriate Iary key alignment_param_id...\n";
foreach my $alignment_params_table_tsv_file_IT (@alignment_params_table_tsv_file) {
	my $master_elet_id;
	my $sub_elet_id;
	if ( $alignment_params_table_tsv_file_IT =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table.tsv$/ ) {
		$master_elet_id = $1;
		$sub_elet_id = $2;
	} else {
		print LOG "could not parse both elet_id for file $alignment_params_table_tsv_file_IT.\n";
		die("could not parse both elet_id for file $alignment_params_table_tsv_file_IT.\n");
	}

	#get associated alignment_table.tsv file
	my $associated_alignment_table_tsv_file = "$TSV_INPUT_DIR/alignments/element_id_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_table.tsv";
	if( -e "$associated_alignment_table_tsv_file" ) {

		my $in;
		my $out;

		# deal with Forward <-> alignment_params_table file
		open $in,  '<',  $alignment_params_table_tsv_file_IT      or die "Can't read old file: $!";
		open $out, '>', "$SQL_OUTPUT_DIR/${master_elet_id}_and_${sub_elet_id}_alignment_params_table.sql.partial" or die "Can't write new file: $!";
		while( <$in> ) {
			$_ =~ s/Forward/$max_curr_alignment_param_id/;
		    	print $out $_;
		}
		close $out;
		close $in;

		# deal with Forward <-> alignment_table file
		open $in,  '<',  $associated_alignment_table_tsv_file      or die "Can't read old file: $!";
		open $out, '>', "$SQL_OUTPUT_DIR/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql.partial" or die "Can't write new file: $!";
		while( <$in> ) {
			$_ =~ s/Forward/$max_curr_alignment_param_id/;
		    	print $out $_;
		}
		close $out;
		close $in;

		#$max_curr_alignment_param_id++
		$max_curr_alignment_param_id++;

		# deal with Reverse <-> alignment_params_table file
		open $in,  '<', "$SQL_OUTPUT_DIR/${master_elet_id}_and_${sub_elet_id}_alignment_params_table.sql.partial" or die "Can't read old file: $!";
		`$SiteConfig::CMDDIR/mkdir -p $SQL_OUTPUT_DIR/alignment_params/element_id_${master_elet_id}/`;
		open $out, '>', "$SQL_OUTPUT_DIR/alignment_params/element_id_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_params_table.sql" or die "Can't write new file: $!";
		while( <$in> ) {
			if($_ =~ m/Reverse/){
				if($master_elet_id == $sub_elet_id){
					#do not print, skip reverse if elt against itself
				}else{
					$_ =~ s/Reverse/$max_curr_alignment_param_id/;
					print $out $_;
				}
			}else{
				print $out $_;
			}
		}
		close $out;
		close $in;
		`$SiteConfig::CMDDIR/rm -f $SQL_OUTPUT_DIR/${master_elet_id}_and_${sub_elet_id}_alignment_params_table.sql.partial`;

		# deal with Forward <-> alignment_table file
		open $in,  '<', "$SQL_OUTPUT_DIR/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql.partial" or die "Can't read old file: $!";
		open $out, '>', "$SQL_OUTPUT_DIR/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql.intermediary" or die "Can't write new file: $!";
		while( <$in> ) {
			if($_ =~ m/Reverse/){
				if($master_elet_id == $sub_elet_id){
					#do not print, skip reverse if elt against itself
				}else{
					$_ =~ s/Reverse/$max_curr_alignment_param_id/;
					print $out $_;
				}
			}else{
				print $out $_;
			}
		}
		close $out;
		close $in;
		`$SiteConfig::CMDDIR/rm -f $SQL_OUTPUT_DIR/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql.partial`;

		if($master_elet_id == $sub_elet_id){
			#do not print, skip reverse if elt against itself
		}else{
			#$max_curr_alignment_param_id++
			$max_curr_alignment_param_id++;
		}

	}else{
		print LOG "could not associated alignment_table.tsv file $associated_alignment_table_tsv_file.\n";
		die("could not associated alignment_table.tsv file $associated_alignment_table_tsv_file.\n");
	}
}
print LOG "Done replacing Forward and Reverse by appropriate Iary key alignment_param_id\n";

# get all *_alignment_table.tsv file
@intermediary_alignment_table_tsv_file = <$SQL_OUTPUT_DIR/*_alignment_table.sql.intermediary>;
print LOG "\n".@intermediary_alignment_table_tsv_file." alignment_table_sql_intermediary file found \n";


# for each *_alignment_table.tsv file and its associate *_alignment_pairs_table.tsv file, replace tmp Iary key by more permanant Iary
print LOG "\nReplacing tmp Iary key alignment_id by more permanant Iary key...\n";
foreach my $alignment_table_tsv_file_IT (@intermediary_alignment_table_tsv_file) {
	my $master_elet_id;
	my $sub_elet_id;
	if ( $alignment_table_tsv_file_IT =~ m/^.*\/(\d+)_and_(\d+)_alignment_table.sql.intermediary$/ ) {
		$master_elet_id = $1;
		$sub_elet_id = $2;
	} else {
		print LOG "could not parse both elet_id for file $alignment_table_tsv_file_IT.\n";
		die("could not parse both elet_id for file $alignment_table_tsv_file_IT.\n");
	}

	#get associated alignment_table.tsv file
	my $associated_alignment_pairs_table_tsv_file = "$TSV_INPUT_DIR/alignment_pairs/element_id_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_pairs_table.tsv";
	if( -e "$associated_alignment_pairs_table_tsv_file" ) {

		my $in;
		my $out;
		my %tmp_alignment_id_2_perm_alignment_id		            = ();

		# deal with alignment_table file
		open $in,  '<',  $alignment_table_tsv_file_IT      or die "Can't read old file: $!";
		`$SiteConfig::CMDDIR/mkdir -p $SQL_OUTPUT_DIR/alignments/element_id_${master_elet_id}/`;
		open $out, '>', "$SQL_OUTPUT_DIR/alignments/element_id_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql" or die "Can't write new file: $!";
		while( <$in> ) {
			if($_ =~ m/^BEGIN WORK;$/
				|| $_ =~ m/^COPY alignments.+$/
				|| $_ =~ m/^\\\.$/
				|| $_ =~ m/^COMMIT WORK;$/
			){
				print $out $_;
			}elsif($_ =~ m/^(\d+)\t\d+\t\d+\t\d+\t.+$/){
				$tmp_alignment_id_2_perm_alignment_id{ $1 } = $max_curr_alignment_id;
				$_ =~ s/$1/$max_curr_alignment_id/;
				$max_curr_alignment_id++;
				print $out $_;
			}else{
				print LOG "error parsing line\n$_\n in file $alignment_table_tsv_file_IT.\n";
				die("error parsing line\n$_\n in file $alignment_table_tsv_file_IT.\n");
			}
		}
		close $out;
		close $in;

		# deal with alignment_pairs_table file
		open $in,  '<',  $associated_alignment_pairs_table_tsv_file      or die "Can't read old file: $!";
		`$SiteConfig::CMDDIR/mkdir -p $SQL_OUTPUT_DIR/alignment_pairs/element_id_${master_elet_id}/`;
		open $out, '>', "$SQL_OUTPUT_DIR/alignment_pairs/element_id_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_pairs_table.sql" or die "Can't write new file: $!";
		while( <$in> ) {
			if($_ =~ m/^BEGIN WORK;$/
				|| $_ =~ m/^COPY alignment_pairs.+$/
				|| $_ =~ m/^\\\.$/
				|| $_ =~ m/^COMMIT WORK;$/
			){
				print $out $_;
			}elsif($_ =~ m/^(\d+)\t(\d+)\t(\d+)\t(\d+)$/){
				if (exists $tmp_alignment_id_2_perm_alignment_id{ $1 }) {
					my $replacement_alignment_id = $tmp_alignment_id_2_perm_alignment_id{ $1 };
					my $replacement_q_gene_id = $2;
					my $replacement_s_gene_id = $3;
					#$_ =~ s/$1/$replac/;
					#if($_ =~ m/-/){ $_ =~ s/-/\\N/g; }
					if($2 == "-" || $2 <= 0){
						if($4 == 4){
							#ok
							$replacement_q_gene_id = "\\N";
						} else {
							print LOG "failed checking for replacement_q_gene_id in deal with alignment_pairs_table file. Pb with line:\n$_\n";
							die("failed checking for replacement_q_gene_id in deal with alignment_pairs_table file. Pb with line:\n$_\n");
						}	
					}

					if($3 == "-" || $3 <= 0){
						if($4 == 5){
							#ok
							$replacement_s_gene_id = "\\N";
						} else {
							print LOG "failed checking for replacement_s_gene_id in deal with alignment_pairs_table file. Pb with line:\n$_\n";
							die("failed checking for replacement_s_gene_id in deal with alignment_pairs_table file. Pb with line:\n$_\n");
						}	
					}
					my $modified_line = "${replacement_alignment_id}\t${replacement_q_gene_id}\t${replacement_s_gene_id}\t${4}\n";
					print $out $modified_line;

				} elsif ($master_elet_id == $sub_elet_id) {
				    #can happen as we do not duplicate forward - reverse for element against itself
				} else {
					print LOG "key tmp_alignment_id_2_perm_alignment_id{ dollar1 } does not exists for dollar1 = $1.\n";
					die("key tmp_alignment_id_2_perm_alignment_id{ dollar1 } does not exists for dollar1 = $1.\n");
				}

				
			}else{
				print LOG "error parsing line\n$_\n in file $associated_alignment_pairs_table_tsv_file.\n";
				die("error parsing line\n$_\n in file $associated_alignment_pairs_table_tsv_file.\n");
			}
			
		}
		close $out;
		close $in;


	}else{
		print LOG "could not associated alignment_pairs_table.tsv file $associated_alignment_pairs_table_tsv_file.\n";
		die("could not associated alignment_pairs_table.tsv file $associated_alignment_pairs_table_tsv_file.\n");
	}

}
print LOG "Done Replacing tmp Iary key alignment_id by more permanant Iary key.\n";

# deleting intermediary files
`$SiteConfig::CMDDIR/rm -f $SQL_OUTPUT_DIR/*_alignment_table.sql.intermediary`;

# delete redundant lines in homologies_table.tsv
print LOG "\nDeleting redundant lines in homologies_table.tsv...\n";
foreach my $homologies_table_sql_file_IT (@homologies_table_tsv_file) {
	my $master_element_id;
	my $core_file_name;
	if ( $homologies_table_sql_file_IT =~ m/^.*\/(\d+)_(and_\d+_homologies_table).tsv$/ ) {
		$master_element_id = $1;
		$core_file_name = "$1_$2";
	} else {
		print LOG "could not parse tsv file $homologies_table_sql_file_IT.\n";
		die("could not parse tsv file $homologies_table_sql_file_IT.\n");
	}

	my $output_duplicates_lines_in_file = `sort $homologies_table_sql_file_IT | uniq -d`;
	my @array_output_duplicates_lines_in_file = split("\n",$output_duplicates_lines_in_file);
	my %hash_duplicates_lines_in_file            = ();
	foreach my $line_duplicate_IT (@array_output_duplicates_lines_in_file) {
		$hash_duplicates_lines_in_file{$line_duplicate_IT} = 0;
	}

	my $in;
	my $out;
	open $in,  '<',  $homologies_table_sql_file_IT      or die "Can't read old file: $!";
	`$SiteConfig::CMDDIR/mkdir -p $SQL_OUTPUT_DIR/homologies/element_id_${master_element_id}/`;
	open $out, '>', "$SQL_OUTPUT_DIR/homologies/element_id_${master_element_id}/${core_file_name}.sql" or die "Can't write new file: $!";
	while( <$in> ) {
		chomp($_);
		if( exists $hash_duplicates_lines_in_file{$_} ){
			if($hash_duplicates_lines_in_file{$_} > 0){
				#already printed, do not print a second time
			}else{
				#not already printed
				print $out "$_\n";
				$hash_duplicates_lines_in_file{$_} = 1;
			}
		} else {
			print $out "$_\n";
		}
	}
}
print LOG "Done deleting redundant lines in homologies_table.tsv.\n";


#if ( $INPUT_DIR_IS_TAR =~ m/^ON$/i ) {

#	print LOG "\nStarting to rm tmp tsv files\n";
#	`$SiteConfig::CMDDIR/rm -f -r $TSV_INPUT_DIR/alignment_params/*`;
#	`$SiteConfig::CMDDIR/rm -f -r $TSV_INPUT_DIR/alignments/*`;
#	`$SiteConfig::CMDDIR/rm -f -r $TSV_INPUT_DIR/alignment_pairs/*`;
#	`$SiteConfig::CMDDIR/rm -f -r $TSV_INPUT_DIR/homologies/*`;
#	print LOG "Done rm tmp tsv files\n";

#}

#end of script
print LOG
"\n---------------------------------------------------\n\n\n Task_add_alignment_parse_tsv_output_for_IDRIS.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);



