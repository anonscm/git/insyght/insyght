#!/usr/local/bin/perl

# import_individual_blast_result.pl : import into the db via the command
# psql -f FILE-TO-IMPORT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname && $SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql_import_done;
# launched on the cluster if needed
#
#arguments :
# 1 : FILE_TO_IMPORT
# 2 : VERBOSE
# 3 : [optional], if this argument is present or 0 then the command are executed in the bash locally. Example of command for cluster: -CMD_CLUSTER "qsub -m ea -q short.q"
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use lib $ENV {SCRIPTS_DIR};

use strict;
use Time::HiRes qw(sleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;

#script level variables

my $FILE_TO_IMPORT;
my $CORE_NAME_FILE_TO_IMPORT;
my $VERBOSE;
my $cmd_cluster_or_bash = "$SiteConfig::CMDDIR/bash";

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::LOGDIR/Task_blast_all/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/`;

=pod
# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/import_individual_blast_result.log"
);
print LOG
"\n---------------------------------------------------\n import_individual_blast_result.pl started at :",
  scalar(localtime), "\n";
=cut

#set the variables according to the argument or exit with usage if the arguments are incorrect

foreach my $argnum ( 0 .. $#ARGV ) {

	if ( $argnum == 0 ) {
		if ( $ARGV[$argnum] =~ m/^.+$/ ) {
			$FILE_TO_IMPORT = $ARGV[$argnum];
		}
		else {
			print
"incorrect 1st argument (FILE_TO_IMPORT) : must be alphanumeric plus _ ou /.\n";
			die
"incorrect 1st argument (FILE_TO_IMPORT) : must be alphanumeric plus _ ou /.\n";
		}

	}

	elsif ( $argnum == 1 ) {
		if ( $ARGV[$argnum] =~ m/^[ONF]+$/ ) {
			$VERBOSE = $ARGV[$argnum];
		}
		else {
			print "incorrect 3rd argument (VERBOSE) : must be ON or OFF.\n";
			die "incorrect 3rd argument (VERBOSE) : must be ON or OFF.\n";
		}

	}

	elsif ( $argnum == 2 ) {
		if ( $ARGV[$argnum] =~ m/^\'0\'$/i ) {

			#do nothing
			print "not using the cluster, command is bash.\n";
		}
		else {
			if ( $ARGV[$argnum] =~ m/^\'(.+)\'$/i ) {
				$cmd_cluster_or_bash = $1;
                                
				print "using the cluster, command is $cmd_cluster_or_bash\n";
			}
		}

	}
	else {
		print
		  "The argument $ARGV[ $argnum ] has not been taking into account\n";
	}
}

if (   !defined($FILE_TO_IMPORT)
	|| !defined($VERBOSE) )
{
	print
"incorrect arguments : import_individual_blast_result.pl [ACCNUM -> FILE_TO_IMPORT] [ON or OFF -> VERBOSE] [optional, 0 or STRING -> CMD_SUBMIT_CLUSTER] \n";
	die
"incorrect arguments : import_individual_blast_result.pl [ACCNUM -> FILE_TO_IMPORT] [ON or OFF -> VERBOSE] [optional, 0 or STRING -> CMD_SUBMIT_CLUSTER] \n";

}

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/import_individual_blast_result`;
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/import_individual_blast_result/import_file_${CORE_NAME_FILE_TO_IMPORT}.log"
);
print LOG
"\n---------------------------------------------------\n import_individual_blast_result.pl started at :",
  scalar(localtime), " for file = ${FILE_TO_IMPORT}\n"
  unless $VERBOSE =~ m/^OFF$/i;

#if ${FILE_TO_IMPORT} file does not exists, exit with error
if ( !-e "${FILE_TO_IMPORT}" ) {
	print
"ERROR import_individual_blast_result.pl : ${FILE_TO_IMPORT} does not exists\n";
	die
"ERROR import_individual_blast_result.pl : ${FILE_TO_IMPORT} does not exists\n";
}

# make directory $SiteConfig::DATADIR/Task_blast_all/tmp/launch_import_db_batch_files/ if does not exists
`mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_import_db_batch_files/`;

# extract core name from file
#file name of sql output is
#$SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/${element_id_querry}_${accnum_correspondance_elt_id_querry}_VS_${element_id_subject}_${accnum_correspondance_elt_id_subject}.sql

if ( $FILE_TO_IMPORT =~ m/^.+\/(.+_VS_.+)\.sql$/ ) {
	$CORE_NAME_FILE_TO_IMPORT = $1;
}
else {
	print LOG
"ERROR import_individual_blast_result.pl : output sql file to import $FILE_TO_IMPORT do not match the regex ^(.+_VS_.+).sql\n";
	die
"ERROR import_individual_blast_result.pl : output sql file to import $FILE_TO_IMPORT do not match the regex ^(.+_VS_.+).sql\n";
}

#open BATCHFILE_IMPORT and print in it
open( BATCHFILE_IMPORT,
"> $SiteConfig::DATADIR/Task_blast_all/tmp/launch_import_db_batch_files/batch_${CORE_NAME_FILE_TO_IMPORT}.sql.ll"
  )
  or die(
"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${CORE_NAME_FILE_TO_IMPORT}.sql.ll\n"
  );

print BATCHFILE_IMPORT "#!/bin/bash
#\$ -S /bin/bash
# (n) No mail is sent ; Avertir au début (b)egin, à la fin (e)nd, à l'éliminaton (a)bort et
# à la suspension (s)uspend d'un job
#\$ -m n
# Adresse mail à laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -o $SiteConfig::DATADIR/Task_blast_all/tmp/launch_import_db_batch_files/batch_${CORE_NAME_FILE_TO_IMPORT}.out
#\$ -e $SiteConfig::DATADIR/Task_blast_all/tmp/launch_import_db_batch_files/batch_${CORE_NAME_FILE_TO_IMPORT}.err
# Lance la commande depuis le répertoire où est lancé le script
#\$ -cwd\n";

print BATCHFILE_IMPORT
"time psql -f ${FILE_TO_IMPORT} -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname && $SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${CORE_NAME_FILE_TO_IMPORT}.sql_import_done\n";

#close BATCHFILE_IMPORT and qsub -m ea the BATCHFILE ON the cluster
close(BATCHFILE_IMPORT);

#`qsub -m ea -q short.q $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll` if there is any blast to submit;
if (
	system(
"$cmd_cluster_or_bash $SiteConfig::DATADIR/Task_blast_all/tmp/launch_import_db_batch_files/batch_${CORE_NAME_FILE_TO_IMPORT}.sql.ll"
	) != 0
  )
{
`$SiteConfig::CMDDIR/touch $SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED`;
	print LOG
"$cmd_cluster_or_bash $SiteConfig::DATADIR/Task_blast_all/tmp/launch_import_db_batch_files/batch_${CORE_NAME_FILE_TO_IMPORT}.sql.ll failed: $?\n";
	exit(1);
}

print LOG "import_individual_blast_result.pl completed succesfully at :",
  scalar(localtime), "\n---------------------------------------------------\n"
  unless $VERBOSE =~ m/^OFF$/i;
close(LOG);

