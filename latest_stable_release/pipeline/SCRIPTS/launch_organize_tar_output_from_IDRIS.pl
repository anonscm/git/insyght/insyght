#!/usr/local/bin/perl
#
# launch_organize_tar_output_from_IDRIS.pl
#
# Example command run on cluster mig : perl launch_organize_tar_output_from_IDRIS.pl -MAX_JOBS 200 -CMD_CLUSTER "qsub -m ea -q short.q"
#
# Date : 01/2015
# LT
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use SiteConfig;
use Time::HiRes qw(usleep);
use POSIX ":sys_wait_h";
use Term::ReadKey;

# whole script scoped variable
my $VERBOSE = "ON";
my $CMD_SUBMIT_CLUSTER = "$SiteConfig::CMDDIR/bash";
my $MAX_JOBS = 1;
my $CORE_INPUT_DIR = undef;

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_alignments/`;


# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_add_alignments/launch_organize_tar_output_from_IDRIS.log"
);
print LOG
"\n---------------------------------------------------\n launch_organize_tar_output_from_IDRIS.pl started at :",
  scalar(localtime), "\n";




foreach my $argnum ( 0 .. $#ARGV ) {
	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -VERBOSE argument ; usage : perl launch_organize_tar_output_from_IDRIS.pl -VERBOSE {ON, OFF}";
			die
"incorrect -VERBOSE argument ; usage : perl launch_organize_tar_output_from_IDRIS.pl -VERBOSE {ON, OFF}";
		}

	} elsif ( $ARGV[$argnum] =~ m/^-CMD_CLUSTER/ ) {
		if ( $ARGV[$argnum] =~ m/^0$/i ) {

			#do nothing
		} 
		else {
			if ( $ARGV[$argnum] =~ m/^.+$/i ) {
				$CMD_SUBMIT_CLUSTER = $ARGV[ $argnum + 1 ];
			}
		}

	} elsif ( $ARGV[$argnum] =~ m/^-MAX_JOBS$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_JOBS = $ARGV[ $argnum + 1 ];
			if($MAX_JOBS < 1){
				print LOG
	"incorrect -MAX_JOBS argument ; usage : perl launch_organize_tar_output_from_IDRIS.pl  -MAX_JOBS {int > 0}";
				die
	"incorrect -MAX_JOBS argument ; usage : perl launch_organize_tar_output_from_IDRIS.pl  -MAX_JOBS {int > 0}";
			}
		}
		else {
			print LOG
"incorrect -MAX_JOBS argument ; usage : perl launch_organize_tar_output_from_IDRIS.pl  -MAX_JOBS {int > 0}\n";
			die
"incorrect -MAX_JOBS argument ; usage : perl launch_organize_tar_output_from_IDRIS.pl  -MAX_JOBS {int > 0}\n";
		}

	} elsif ( $ARGV[$argnum] =~ m/^-CORE_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$CORE_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG "incorrect -CORE_INPUT_DIR argument ; usage : perl process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}";
			die "incorrect -CORE_INPUT_DIR argument ; usage : perl process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}";
		}
	}

}

print LOG "\nYou choose the following options :\n\tMAX_JOBS : $MAX_JOBS\n\tVERBOSE : $VERBOSE\n" unless $VERBOSE =~ m/^OFF$/;
if ( $CMD_SUBMIT_CLUSTER =~ m/^0$/i ) {
	#do nothing
}
else {
	print LOG "CMD_CLUSTER : $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/;
}

if (! defined $CORE_INPUT_DIR) {
	print LOG "Undefined CORE_INPUT_DIR ; usage : perl process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}";
	die "Undefined CORE_INPUT_DIR ; usage : perl process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}";
}


# deal with dir
`$SiteConfig::CMDDIR/mkdir -p $CORE_INPUT_DIR/alignment_params`;
`$SiteConfig::CMDDIR/mkdir -p $CORE_INPUT_DIR/alignments`;
`$SiteConfig::CMDDIR/mkdir -p $CORE_INPUT_DIR/alignment_pairs`;
`$SiteConfig::CMDDIR/mkdir -p $CORE_INPUT_DIR/homologies`;
`$SiteConfig::CMDDIR/rm -f -r $CORE_INPUT_DIR/alignment_params/*`;
`$SiteConfig::CMDDIR/rm -f -r $CORE_INPUT_DIR/alignments/*`;
`$SiteConfig::CMDDIR/rm -f -r $CORE_INPUT_DIR/alignment_pairs/*`;
`$SiteConfig::CMDDIR/rm -f -r $CORE_INPUT_DIR/homologies/*`;

# rm files
`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_add_alignments/tmp/launch_process_organize_files/ERR_process_organize_*.ERR`;
`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_*.err`;
`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_*.out`;
`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_*.done`;



# list files under $SiteConfig::DATADIR/Task_add_alignments/tmp/launch_process_organize_files/
my @ll_files = <$SiteConfig::DATADIR/Task_add_alignments/tmp/launch_process_organize_files/batch_process_organize_*.ll>;
print LOG scalar(@ll_files) . " .ll files found under $SiteConfig::DATADIR/Task_add_alignments/tmp/launch_process_organize_files/\n";

my $number_untar_started  = 0;
my $number_untar_done = 0;

print LOG "Start submit ll files on cluster\n";
sub sub_count_output_and_del_tmp_files {


	#my @list_new_files_alignment_done = <$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/*.main_Align_done>;
	#$SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_file}.done`;
	my @list_new_files_untar_done = ();
	opendir(my $dh, "$SiteConfig::LOGDIR/Task_add_alignments/") or die "opendir($SiteConfig::LOGDIR/Task_add_alignments/): $!";
	while (my $de = readdir($dh)) {
	  next if $de =~ /^\./ or $de !~ /process_organize_tar_output_from_IDRIS_.+\.done$/;
	  push ( @list_new_files_untar_done, "$SiteConfig::LOGDIR/Task_add_alignments/$de");
	}
	closedir($dh);

	foreach my $list_new_files_untar_done_IT (@list_new_files_untar_done) {

		#parse file name
		my $core_name_list_new_files_untar_done_IT = "";
		if ( $list_new_files_untar_done_IT =~ m/^.+\/process_organize_tar_output_from_IDRIS_(.+)\.done$/i ) {
			$core_name_list_new_files_untar_done_IT = $1;
		} else {
			print LOG "Could not parse core name in file $list_new_files_untar_done_IT\n";
			die("Could not parse core name in file $list_new_files_untar_done_IT\n");
		}

		# if .err not empty
		# $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_file}.err
		if (! $CMD_SUBMIT_CLUSTER eq "$SiteConfig::CMDDIR/bash" ) {
			if (! -z "$SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_list_new_files_untar_done_IT}.err" ) {
				print LOG "The file $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_list_new_files_untar_done_IT}.err is not empty : $!\n";
				die("The file $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_list_new_files_untar_done_IT}.err is not empty : $!\n");
			}
		}
		# $SiteConfig::LOGDIR/Task_add_alignments/tmp/launch_process_organize_files/ERR_process_organize_${core_name_file}.ERR
		if (! -z "$SiteConfig::LOGDIR/Task_add_alignments/tmp/launch_process_organize_files/ERR_process_organize_${core_name_list_new_files_untar_done_IT}.ERR" ) {
			print LOG "The file $SiteConfig::LOGDIR/Task_add_alignments/tmp/launch_process_organize_files/ERR_process_organize_${core_name_list_new_files_untar_done_IT}.ERR is not empty : $!\n";
			die("The file $SiteConfig::LOGDIR/Task_add_alignments/tmp/launch_process_organize_files/ERR_process_organize_${core_name_list_new_files_untar_done_IT}.ERR is not empty : $!\n");
		}


		#rm .err, .out, and marker file
		#$SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_file}.out
		`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_list_new_files_untar_done_IT}.err`;
		`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_add_alignments/tmp/launch_process_organize_files/ERR_process_organize_${core_name_list_new_files_untar_done_IT}.ERR`;
		`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_list_new_files_untar_done_IT}.out`;
		`$SiteConfig::CMDDIR/rm -f $list_new_files_untar_done_IT`;

		# increment counter
		$number_untar_done++;

	}
}


foreach my $ll_file_IT (@ll_files) {

	# wait a bit if there more than $MAX_JOBS child process that have been launched simultaneously
	my $count_sleep = 0;
	while ( $count_sleep < 2 ) {

		sub_count_output_and_del_tmp_files();

		if ($MAX_JOBS == 1) {
			last;
		} else {
			if( $number_untar_started - $number_untar_done < $MAX_JOBS ){
				#wait a bit so that there is a shifting in time and exit loop
				#usleep(10000);
				print "Continuing to submit as there is $number_untar_started - $number_untar_done = ".($number_untar_started - $number_untar_done)." simulteneous process ;  MAX_JOBS =  $MAX_JOBS\n" unless $VERBOSE =~ m/^OFF$/i;
				last;
			}

			if ($count_sleep == 0 ) {
				print LOG "waiting a bit as there is more than $MAX_JOBS processes that have been launched simultenously : $number_untar_started - $number_untar_done = ".($number_untar_started - $number_untar_done)."\n" unless $VERBOSE =~ m/^OFF$/i;
				$count_sleep = 1;
				usleep(10000);
			} else {
				usleep(10000);
			}
		}
	}#while ( $count_sleep < 2 ) {


	# launch of the bash file
	# case no cluster, no paralelisation
	if ( $CMD_SUBMIT_CLUSTER eq "$SiteConfig::CMDDIR/bash") {

		if($MAX_JOBS > 1){
			system("$CMD_SUBMIT_CLUSTER $ll_file_IT &");
		} else {
			system("$CMD_SUBMIT_CLUSTER $ll_file_IT");
		}

		if ( $? != 0 ) {
			print LOG "command $CMD_SUBMIT_CLUSTER $ll_file_IT 2>&1 failed: $!\n";
			die("command $CMD_SUBMIT_CLUSTER $ll_file_IT 2>&1 failed: $!\n");
		}
		#print LOG $output_cmd;
		$number_untar_started++;
		print LOG "file $ll_file_IT successfuly submited by $CMD_SUBMIT_CLUSTER (Total submitted = $number_untar_started ; Total done = $number_untar_done ; Total Expected = ".scalar(@ll_files).")\n" unless $VERBOSE =~ m/^OFF$/i;


	} else {
		# use of cluster
		my $output_cmd = `$CMD_SUBMIT_CLUSTER $ll_file_IT 2>&1`;
		if ( $? != 0 )
		{
			print LOG "command $CMD_SUBMIT_CLUSTER $ll_file_IT 2>&1 failed: $!\n";
			die("command $CMD_SUBMIT_CLUSTER $ll_file_IT 2>&1 failed: $!\n");
		}
		if($output_cmd =~ m/^Your job .+ has been submitted$/i
			|| $output_cmd eq ""
		){
			$number_untar_started++;
			print LOG "file $ll_file_IT successfuly submited by $CMD_SUBMIT_CLUSTER (Total submitted = $number_untar_started ; Total done = $number_untar_done ; Total Expected = ".scalar(@ll_files).")\n" unless $VERBOSE =~ m/^OFF$/i;
		}else{
			print LOG "Error launch of : $CMD_SUBMIT_CLUSTER $ll_file_IT :\n$output_cmd\n";
			die("Error launch of : $CMD_SUBMIT_CLUSTER $ll_file_IT :\n$output_cmd\n");
		}
	}
} #foreach my $ll_file_IT (@ll_files) {


print LOG "done submitting commands on cluster\n";
print LOG "wait for all commands to complete...\n";

#wait for all files
my $count_finish = 0;
my $count_wait_hours = 0;
while ( $count_finish >= 0 ) {

	sub_count_output_and_del_tmp_files();

	if($number_untar_done == scalar(@ll_files) ){
		last;
	}

	usleep(1000000);#sleep 1s
	$count_finish++;
	if ($count_finish > 3600){#1 H
		#print LOG "Error, waited for untar output file for more than 1 hour\n";
		#die ("Error, waited for blast untar file for more than 1 hour\n");
		$count_wait_hours++;
		print LOG "All jobs have been launched. Waiting for blast output files ; Total waiting time = $count_wait_hours hours ; Total output files found yet = $number_untar_done. (This message will display every hour until all output files are found).\n";
		$count_finish = 0;
	}

}

#end of script
print LOG "$number_untar_done jobs have been successfully submitted in this run.\n";

print LOG
"\n---------------------------------------------------\n\n\n launch_organize_tar_output_from_IDRIS.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";


close(LOG);





