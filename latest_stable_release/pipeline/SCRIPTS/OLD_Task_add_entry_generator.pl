#!/usr/local/bin/perl

# Date : 03/2017
#
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2022)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use DBI;
use File::stat;
use Time::HiRes qw(sleep);
use Time::HiRes qw(gettimeofday);
use Bio::SeqIO; #https://metacpan.org/pod/Bio::SeqIO
use SiteConfig;
use ORIGAMI;
use lib '$SiteConfig::SCRIPTSDIR';
use BlastConfig;
use Bio::Seq;
use Bio::SeqIO;
use Bio::SeqFeatureI ;
use Bio::Location::SplitLocationI;
use Bio::FASTASequence;
use JSON;
use Data::Dumper;
use Try::Tiny;

#global var
my $format_genomes = undef;
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
#my $SKIP_NEIGHBOR_INFO = "OFF";
my $VERBOSE = "ON";
my $PRINT_SUGGESTION_ON_SCREEN = "OFF";
my $DO_NOT_DELETE_DIR_TMP_DOWNLOAD = "OFF";
my $DIR_TMP_DOWNLOAD = "$SiteConfig::BANKDIR/../DIR_TMP_DOWNLOAD/";
my $FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES = "OFF"; # else will attempt to group related accnum into organism (= assembly)
my $MAX_FILES_TO_TREAT_IN_THIS_RUN = -1;
my @WARNING_accnum_suppressed = ();
my $DELETE_FAILURE_FROM_PREVIOUS_RUN = undef;
my $sql_query = undef;
my $output_backtick = "";
my $counter_loop = 0;
my $url_to_download_IT = "";
my $try_fetch_with_curl = 0;

my $extension_genomes;
my $pattern_features;
my $pattern_date;
my $pattern_version;
my %new_accnum_from_gr2species_migale = ();
my @entry = ();
my @new_genomes = ();
my %hash_new_genomes2accession = ();
my @new_version = ();
my @new_version_list_accnum = ();
my $type_MAJ;
my %accnum_from_gr2species_migale_To_be_commented = ();
my $next_free_organism_id_for_db = undef;
my $next_free_element_id_for_db = undef;
my $next_free_gene_id_for_db = undef;
my %species2ref_list_hash_organism_fields = ();
my %assemblyaccession2last_organism_id = ();
my %accession2element_id = ();
# my @list_supported_element_types = ("complete_genome","chromosome","plasmid","draft","synthetic","transfer_element","satellite","genomic_region","contig","scaffold","unknown_grouped");

my $count_group_accnum_in_existing_orga_because_similar_assemblyaccession = 0;
my $count_group_accnum_in_existing_orga_because_similar_organism_fields = 0;
my $count_create_new_orga_because_similar_species_but_no_similar_organism_fields = 0;
my $count_create_new_orga_because_no_similar_species = 0;
my $count_create_new_orga = 0;
my $count_already_stored_orga_in_db = 0;
my $NCBI_linksets_found = 0;
my $NCBI_assemblyaccession_found = 0;
my $accnum_NCBI_sourcedb_found = 0;

sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $SiteConfig::LOGDIR/manage_primary_data_generator.error`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $SiteConfig::LOGDIR/manage_primary_data_generator.error :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $SiteConfig::LOGDIR/manage_primary_data_generator.error :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


#set the variables according to the argument or exit with usage if the arguments are incorrect
foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-GENOME_FILES_FORMAT$/ ) {
		if( $ARGV[ $argnum + 1 ] =~ /^genbank/i
			|| $ARGV[ $argnum + 1 ] =~ m/^gbk$/i 
			|| $ARGV[ $argnum + 1 ] =~ m/^embl$/i 
			|| $ARGV[ $argnum + 1 ] =~ m/^dat$/i )
		{
			$format_genomes = $ARGV[ $argnum + 1 ];
		} else {
			#print "incorrect -GENOME_FILES_FORMAT argument ; usage : perl Task_add_entry_generator.pl -GENOME_FILES_FORMAT = {genbank, embl}";
			die_with_error_mssg("incorrect -GENOME_FILES_FORMAT argument ; usage : perl Task_add_entry_generator.pl -GENOME_FILES_FORMAT = {genbank, embl}");
		}
=pod
	} elsif ( $ARGV[$argnum] =~ m/^-SKIP_NEIGHBOR_INFO$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$SKIP_NEIGHBOR_INFO = $ARGV[ $argnum + 1 ];
		} else {
			#print LOG "incorrect -SKIP_NEIGHBOR_INFO argument ; usage : perl Task_add_entry_generator.pl -SKIP_NEIGHBOR_INFO = {ON, OFF}";
			die_with_error_mssg("incorrect -SKIP_NEIGHBOR_INFO argument ; usage : perl Task_add_entry_generator.pl -SKIP_NEIGHBOR_INFO = {ON, OFF}");
		}
=cut
	} elsif ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		} else {
			#print "incorrect -VERBOSE argument ; usage : perl Task_add_entry_generator.pl -VERBOSE = {ON, OFF}";
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl Task_add_entry_generator.pl -VERBOSE = {ON, OFF}");
		}
	}
#PRINT_SUGGESTION_ON_SCREEN
	 elsif ( $ARGV[$argnum] =~ m/^-PRINT_SUGGESTION_ON_SCREEN$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$PRINT_SUGGESTION_ON_SCREEN = $ARGV[ $argnum + 1 ];
		} else {
			#print "incorrect -PRINT_SUGGESTION_ON_SCREEN argument ; usage : perl Task_add_entry_generator.pl -PRINT_SUGGESTION_ON_SCREEN = {ON, OFF}";
			die_with_error_mssg("incorrect -PRINT_SUGGESTION_ON_SCREEN argument ; usage : perl Task_add_entry_generator.pl -PRINT_SUGGESTION_ON_SCREEN = {ON, OFF}");
		}
	}
#DO_NOT_DELETE_DIR_TMP_DOWNLOAD
	elsif ( $ARGV[$argnum] =~ m/^-DO_NOT_DELETE_DIR_TMP_DOWNLOAD$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DO_NOT_DELETE_DIR_TMP_DOWNLOAD = $ARGV[ $argnum + 1 ];
		}
		else {
			#print "incorrect -DO_NOT_DELETE_DIR_TMP_DOWNLOAD argument ; usage : perl Task_add_entry_generator.pl -DO_NOT_DELETE_DIR_TMP_DOWNLOAD = {ON, OFF}";
			die_with_error_mssg("incorrect -DO_NOT_DELETE_DIR_TMP_DOWNLOAD argument ; usage : perl Task_add_entry_generator.pl -DO_NOT_DELETE_DIR_TMP_DOWNLOAD = {ON, OFF}");
		}
	}
# FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES
	elsif ( $ARGV[$argnum] =~ m/^-FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES = $ARGV[ $argnum + 1 ];
		}
		else {
			#print "incorrect -FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES argument ; usage : perl Task_add_entry_generator.pl -FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES = {ON, OFF}";
			die_with_error_mssg ("incorrect -FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES argument ; usage : perl Task_add_entry_generator.pl -FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES = {ON, OFF}");
		}
	}
# MAX_FILES_TO_TREAT_IN_THIS_RUN
	elsif ( $ARGV[$argnum] =~ m/^-MAX_FILES_TO_TREAT_IN_THIS_RUN$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i )
		{
			$MAX_FILES_TO_TREAT_IN_THIS_RUN = $ARGV[ $argnum + 1 ];
			if($MAX_FILES_TO_TREAT_IN_THIS_RUN < 1){
				#print LOG "incorrect -MAX_FILES_TO_TREAT_IN_THIS_RUN argument ; usage : perl Task_add_entry_generator.pl  -MAX_FILES_TO_TREAT_IN_THIS_RUN {int > 0}\n";
				die_with_error_mssg ("incorrect -MAX_FILES_TO_TREAT_IN_THIS_RUN argument ; usage : perl Task_add_entry_generator.pl  -MAX_FILES_TO_TREAT_IN_THIS_RUN {int > 0}\n");
			}
		}
		else {
			#print "incorrect -MAX_FILES_TO_TREAT_IN_THIS_RUN argument ; usage : perl Task_add_entry_generator.pl -MAX_FILES_TO_TREAT_IN_THIS_RUN = {DIGIT}";
			die_with_error_mssg("incorrect -MAX_FILES_TO_TREAT_IN_THIS_RUN argument ; usage : perl Task_add_entry_generator.pl -MAX_FILES_TO_TREAT_IN_THIS_RUN = {DIGIT}");
		}
# -DELETE_FAILURE_FROM_PREVIOUS_RUN 
	} elsif ( $ARGV[$argnum] =~ m/^-DELETE_FAILURE_FROM_PREVIOUS_RUN$/ ) {
		$DELETE_FAILURE_FROM_PREVIOUS_RUN = 1;

	}
}


# on cr�� le r�pertoire pour les fichiers de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_entry`;
# ouvre en pipant sur tee.pl pour �crire � la fois sur la sortie standard et dans le fichier de log
open(LOG, ">$SiteConfig::LOGDIR/Task_add_entry/Task_add_entry_generator.log"); # | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG "***Starting Task_add_entry_generator.pl at ",scalar(localtime),"\n\n" unless $VERBOSE =~ m/^OFF$/;


# rm marker file done script
$output_backtick = `rm -f $SiteConfig::LOGDIR/manage_primary_data_generator.done`;
$output_backtick .= `rm -f $SiteConfig::LOGDIR/manage_primary_data_generator.error`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $SiteConfig::LOGDIR/manage_primary_data_generator.done or .error :\n$output_backtick \n $!");
}

if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
        #$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
        #$molecule_type = "orgaCluster";
        #die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.\n");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
        #$molecule_type = "element";
        #print LOG "BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.\n";
        die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.\n");
} else {
        #print LOG "BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately\n";
        die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately\n");
}


# mkdir and rm file
`$SiteConfig::CMDDIR/mkdir -p $DIR_TMP_DOWNLOAD`;
if ( $DO_NOT_DELETE_DIR_TMP_DOWNLOAD =~ m/^OFF$/i ) {
	$output_backtick = `$SiteConfig::CMDDIR/rm -rf $DIR_TMP_DOWNLOAD/*`;
}


#check if still some data computation_in_process from previous runs
$sql_query = $ORIGAMI::dbh->prepare("SELECT organism_id FROM organisms where computation_in_process IS TRUE");
$sql_query->execute or die_with_error_mssg("Pb execute SELECT organism_id FROM organisms where computation_in_process IS TRUE: $!");
my $DO_DROP_INDX_before_primary_data_insert = 1;
while ( my $res = $sql_query->fetchrow_hashref() ) {
	my $organism_id_IT = $res->{organism_id};
	if (defined $DELETE_FAILURE_FROM_PREVIOUS_RUN) {

		if (defined $DO_DROP_INDX_before_primary_data_insert) {
			$output_backtick = `psql --quiet -f $SiteConfig::SCRIPTSDIR/../../database/SQL/drop_idx_before_primary_data_insert.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete psql --quiet -f $SiteConfig::SCRIPTSDIR/../../database/SQL/drop_idx_before_primary_data_insert.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1 :\n$output_backtick \n $!");
			}
			$DO_DROP_INDX_before_primary_data_insert = undef;
		}

		my $sql_query_bis = $ORIGAMI::dbh->prepare("SELECT accession FROM elements where organism_id = $organism_id_IT");
		$sql_query_bis->execute or die_with_error_mssg("Pb execute SELECT accession FROM elements where organism_id = $organism_id_IT: $!");
		while ( my $res_bis = $sql_query_bis->fetchrow_hashref() ) {
			my $accession_IT = $res_bis->{accession};
			print LOG "option -DELETE_FAILURE_FROM_PREVIOUS_RUN, cleaning up Accession: $accession_IT for organism $organism_id_IT.\n" unless $VERBOSE =~ m/^OFF$/;
			$output_backtick = `perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/delete_entry.pl $accession_IT 2>&1`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/delete_entry.pl $accession_IT :\n$output_backtick \n $!");
			}
		}

	} else {
        	die_with_error_mssg("Error : there is still some data computation_in_process from previous runs, please specify option -DELETE_FAILURE_FROM_PREVIOUS_RUN to get rid of them and start over with new data.\n");
	}
}

if ( ! defined $DO_DROP_INDX_before_primary_data_insert) {
	$output_backtick = `psql --quiet -f $SiteConfig::SCRIPTSDIR/../../database/SQL/recreate_idx_after_primary_data_insert.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1`;
	if ($output_backtick eq "") {
		#ok
	} else {
		die_with_error_mssg("Error could not complete psql --quiet -f $SiteConfig::SCRIPTSDIR/../../database/SQL/recreate_idx_after_primary_data_insert.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1 :\n$output_backtick \n $!");
	}
}


# lecture du fichier gr2species_migale
open( GR2SPECIES_MIGALE, "<$SiteConfig::BANKDIR/gr2species_migale.txt" ) or die_with_error_mssg("Can not open $SiteConfig::BANKDIR/gr2species_migale.txt\n");
while ( my $line = <GR2SPECIES_MIGALE> ) {
	chomp($line);
	$line =~ s/^\s+//;
	$line =~ s/\s+$//;
	my @split = split( '\s+', $line );
	if ( $line =~ m/^#/ ) {
		print LOG "skipping line $line in $SiteConfig::BANKDIR/gr2species_migale.txt as it starts with a comment\n" unless $VERBOSE =~ m/^OFF$/;
		next;
	}
	if ( $line !~ /^(Bacteria|Archaea)\s+(\w+)/){
		print LOG "skipping line $line in $SiteConfig::BANKDIR/gr2species_migale.txt as it does not starts with Bacteria or Archaea\n" unless $VERBOSE =~ m/^OFF$/;
		$accnum_from_gr2species_migale_To_be_commented { $split[1] } = 1;
		next;
	}
	$new_accnum_from_gr2species_migale{ $split[1] } = $new_accnum_from_gr2species_migale{ $split[1] } + 1;
	if ( $new_accnum_from_gr2species_migale{ $split[1] } > 1 ) {
		#print LOG "the file $SiteConfig::BANKDIR/gr2species_migale.txt has a redundancy for the accession number $split[1], please check this file.\n";
		die_with_error_mssg("the file $SiteConfig::BANKDIR/gr2species_migale.txt has a redundancy for the accession number $split[1], please check this file.\n");
	}
}
close(GR2SPECIES_MIGALE);


if ($format_genomes eq undef){
	# no format was specified, do both .dat and .gbk files
	my @entry_gbk = <$SiteConfig::BANKDIR/*.gbk>;
	foreach my $gbk_file (@entry_gbk) {
		if ( $MAX_FILES_TO_TREAT_IN_THIS_RUN > 0 && scalar(@entry) >= $MAX_FILES_TO_TREAT_IN_THIS_RUN )  {
			print LOG "WARNING : The option -MAX_FILES_TO_TREAT_IN_THIS_RUN $MAX_FILES_TO_TREAT_IN_THIS_RUN is activated, $MAX_FILES_TO_TREAT_IN_THIS_RUN will be treated by this script. If you want to treat all the genome files, you will need to run the Task_add_entry_integrator.pl and then Task_add_entry_generator.pl again to continue treating all the files.\n" unless $VERBOSE =~ m/^OFF$/;
			last;
		}
		if ($gbk_file =~ /^.+\/(.+)\.gbk$/){
			if( exists $new_accnum_from_gr2species_migale{$1} ){
				print LOG "The file $1 has been added to be treated by the pipeline.\n" unless $VERBOSE =~ m/^OFF$/;
				push(@entry, $gbk_file);
			} else {
				print LOG "The file $1 is not listed in the file $SiteConfig::BANKDIR/gr2species_migale.txt, the file $gbk_file will therefore be ignored.\n" unless $VERBOSE =~ m/^OFF$/;
			}
		} else {
			#print LOG "Error the file $gbk_file could not be parsed correctly.\n";
			die_with_error_mssg("Error the file $gbk_file could not be parsed correctly.\n");
		}
	}
	my @entry_dat = <$SiteConfig::BANKDIR/*.dat>;
	foreach my $dat_file (@entry_dat) {
		if ( $MAX_FILES_TO_TREAT_IN_THIS_RUN > 0 && scalar(@entry) >= $MAX_FILES_TO_TREAT_IN_THIS_RUN )  {
			print LOG "WARNING : The option -MAX_FILES_TO_TREAT_IN_THIS_RUN $MAX_FILES_TO_TREAT_IN_THIS_RUN is activated, $MAX_FILES_TO_TREAT_IN_THIS_RUN will be treated by this script. If you want to treat all the genome files, you will need to run the Task_add_entry_integrator.pl and then Task_add_entry_generator.pl again to continue treating all the files.\n" unless $VERBOSE =~ m/^OFF$/;
			last;
		}
		if ($dat_file =~ /^.+\/(.+)\.dat$/){
			if( exists $new_accnum_from_gr2species_migale{$1} ){
				print LOG "The file $1 has been added to be treated by the pipeline.\n" unless $VERBOSE =~ m/^OFF$/;
				push(@entry, $dat_file);
			} else {
				print LOG "The file $1 is not listed in the file $SiteConfig::BANKDIR/gr2species_migale.txt, the file $dat_file will therefore be ignored.\n" unless $VERBOSE =~ m/^OFF$/;
			}
		} else {
			#print LOG "Error the file $dat_file could not be parsed correctly.\n";
			die_with_error_mssg("Error the file $dat_file could not be parsed correctly.\n");
		}
	}


} else {

	if($format_genomes =~ /^genbank/i){
		$format_genomes = "gbk";
	}
	if($format_genomes =~ /^embl/i){
		$format_genomes = "dat";
	}
	#specified format file, ignore other format
	my @entry_spec = <$SiteConfig::BANKDIR/*.$format_genomes>;
	foreach my $spec_file (@entry_spec) {
		if ( $MAX_FILES_TO_TREAT_IN_THIS_RUN > 0 && scalar(@entry) >= $MAX_FILES_TO_TREAT_IN_THIS_RUN )  {
			print LOG "WARNING : The option -MAX_FILES_TO_TREAT_IN_THIS_RUN $MAX_FILES_TO_TREAT_IN_THIS_RUN is activated, $MAX_FILES_TO_TREAT_IN_THIS_RUN will be treated by this script. If you want to treat all the genome files, you will need to run the Task_add_entry_integrator.pl and then Task_add_entry_generator.pl again to continue treating all the files.\n" unless $VERBOSE =~ m/^OFF$/;
			last;
		}
		if ($spec_file =~ /^.+\/(.+)\.$format_genomes$/){
			if( exists $new_accnum_from_gr2species_migale{$1} ){
				print LOG "The file $1 has been added to be treated by the pipeline.\n" unless $VERBOSE =~ m/^OFF$/;
				push(@entry, $spec_file);
			} else {
				print LOG "The file $1 is not listed in the file $SiteConfig::BANKDIR/gr2species_migale.txt, the file $spec_file will therefore be ignored.\n" unless $VERBOSE =~ m/^OFF$/;
			}
		} else {
			#print LOG "Error the file $spec_file could not be parsed correctly.\n";
			die_with_error_mssg("Error the file $spec_file could not be parsed correctly.\n");
		}
	}
}


# pour convertir le nom du mois en nombre
my %lesmois = (	"JAN", 1,
		"FEB", 2,
		"MAR", 3,
		"APR", 4,
		"MAY", 5,
		"JUN", 6,
		"JUL", 7,
		"AUG", 8,
		"SEP", 9,
		"OCT", 10,
		"NOV", 11,
		"DEC", 12 );

# pour l'insertion dans suivi_MAJ
my $insert_handle = $ORIGAMI::dbh->prepare_cached(qq{ INSERT INTO suivi_maj(accession,type_maj) VALUES(?,?) });

my %accnumToInsert = ();
# pour tous les genomes contenus dans la base (GenBank) on verifie s'ils sont deja present dans seqDB
foreach my $genome (@entry) {

	my $accession = undef;
	my $accnum_from_file_name = undef;
	my $pattern_accession = undef;

	if ($genome =~ /^.+\/(.+)(\..+)*\.gbk$/){
		$format_genomes = 'genbank';
		$extension_genomes = 'gbk';
		$pattern_features = 'FEATURES';
		$pattern_date = 'LOCUS.*(\d\d-\w\w\w-\d\d\d\d)';
		$pattern_version = 'VERSION\s+\w+\.(\d+)';

		$accnum_from_file_name = $1;
		$pattern_accession = 'ACCESSION';

	} elsif ($genome =~ /^.+\/(.+)(\..+)*\.dat$/) {
		$format_genomes = 'embl';
		$extension_genomes = 'dat';
		$pattern_features = '\nFT\s+';
		$pattern_date = '\nXX.*\nDT\s+(\d\d-\w\w\w-\d\d\d\d)';
		$pattern_version = 'ID\s+\w+;\s+SV\s(\d+)';

		$accnum_from_file_name = $1;
		$pattern_accession = 'AC';

	} else {
		#print LOG "Unrecognized file format, the file $genome is neither a .gbk or .dat.\n";
		die_with_error_mssg("Unrecognized file format, the file $genome is neither a .gbk or .dat.\n");
	}

	# r�cuperation du numero d'acession
	# check accnum in file same as in filename
	my $output_cmd_grep = `$SiteConfig::CMDDIR/grep -E \'^${pattern_accession} +.+\$\' $genome`;
	#print "TEST :::: $SiteConfig::CMDDIR/grep -E \'^${pattern_accession} +.+\$\' $genome\n\t$output_cmd_grep";
	chomp($output_cmd_grep);
	$output_cmd_grep =~ s/^\s+//;
	$output_cmd_grep =~ s/\s+$//;
	if($output_cmd_grep =~ m/^${pattern_accession}\s+${accnum_from_file_name};?(\s+.+)?$/){
		#ok, accnum = file name
		#print "TEST :::: ok, accnum = file name ; accession = $accession ; accnum_from_file_name = $accnum_from_file_name";
		$accession = $accnum_from_file_name;
	} elsif ($output_cmd_grep =~ m/^${pattern_accession}\s*(unknown)?$/i ) {
		#accnum in file is empty (ex output prokka), take accnum from file name
		$accession = $accnum_from_file_name;
		#print LOG "Error: No accession number has been detected in file $genome.\n";
		#die_with_error_mssg("Error: No accession number has been detected in file $genome.\n");
	} elsif ( $output_cmd_grep =~ m/^${pattern_accession}\s+(.+);?(\s+.+)?$/ ) {
		#difference between accnum and file name, will take accnum over file name
		$accession = $1;
		chomp($accession);
		$accession =~ s/^\s+//;
		$accession =~ s/\s+$//;
		$accession =~ s/[\s\.:]/_/g;
		#print "TEST :::: difference between accnum and file name, will take accnum over file name ; accession = $accession ; accnum_from_file_name = $accnum_from_file_name";
	} else {
		#print LOG "Error: No accession number has been detected in file $genome.\n";
		die_with_error_mssg("Error: No accession number has been detected in file $genome.\n");
	}
	if (exists $accnumToInsert {$accession}) {
		#print LOG "Error : The new accession number to insert in the file $genome is also found in another genome file to insert.\n";
		die_with_error_mssg("Error : The new accession number to insert in the file $genome is also found in another genome file to insert.\n");
	} else {
		$accnumToInsert {$accession} = 1;
	}


	$type_MAJ = '';
	
	# v�rification si l'entr�e existe d�ja
	my $res = $ORIGAMI::dbh->selectall_arrayref("SELECT accession FROM elements WHERE accession = '$accession'");
	my $present = $res->[0];
	# si elle n'existe pas on l'ajoute � la liste des nouveaux genomes
	if($present->[0] ne $accession){
		push(@new_genomes,$genome);
		$hash_new_genomes2accession{ $genome } = $accession;
		$type_MAJ = 'i'; # i pour insert (= nouveau g�nome)
	}
	# verification si un fichier plus re�ent n'existe pas
	else{
		# Recuperation de la date et de la version dans le fichier embl
		my @newdate = ('00','JAN','0000');
		my $ficversion = "null";
		open(FIC, "< $genome") || die_with_error_mssg("Could not open $genome\n"); # ouverture du fichier
		my $ligne;
		my $lignes_fic = '';
		# On suppose que les lignes contenant la date et la version se situent
		# avant la premi�re ligne des features
		while( ($ligne = <FIC>) && ($ligne !~ /$pattern_features/) ){$lignes_fic .= $ligne;}
		close(FIC); # fermeture
		# Teste si le fichier contient la date ou la version
		if($lignes_fic =~ /$pattern_date/){@newdate = split(/-/,$1);}
		if($lignes_fic =~ /$pattern_version/){$ficversion = $1;}
		my $day = $newdate[0];
		my $mois = $lesmois{$newdate[1]};
		my $year = $newdate[2];
		# Recuperation de la date dans la base
		my $date_seq = $ORIGAMI::dbh->selectall_arrayref("SELECT date_seq FROM sequences WHERE accession = '$accession'");
		my $dbdate = $date_seq->[0]->[0];
		my @ymj = split(/-/,$dbdate);
		$ymj[1] = $lesmois{$ymj[1]};
		# Recuperation de la version dans la base
		my $dbversion = $ORIGAMI::dbh->selectall_arrayref("SELECT version FROM sequences WHERE accession = '$accession'");
		my $version = $dbversion->[0]->[0];
		if($version eq ""){$version = "null";}
	
		print LOG "$accession :  fichier : $day/$mois/$year v$ficversion, base : $ymj[0]/$ymj[1]/$ymj[2] v$version\n" unless $VERBOSE =~ m/^OFF$/;

		# Teste si la version/date dans le fichier est > � celle de la base => mise � jour base
		
		my $rapport_base_fic = 0; # -1 : entr�e base plus vieille, 0 : m�me date, 1 : entr�e base plus r�cente
		if($ymj[2] < $year){$rapport_base_fic = -1;}
		else{
			if($ymj[2] == $year){
				if($ymj[1] < $mois){$rapport_base_fic = -1;}
				else{
					if($ymj[1] == $mois){
						if($ymj[0] < $day){$rapport_base_fic = -1;}
						else{
							if($ymj[0] > $day){$rapport_base_fic = 1;}
						}
					}
					else{$rapport_base_fic = 1;}
				}
			}
			else{$rapport_base_fic = 1;}
		}
		
		# Si au moins une des deux versions (fichier et entr�e base) n'est pas d�fini
		if($version eq "null" || $ficversion eq "null"){
			# Si le fichier contient une information de version ou la date de l'entr�e dans le base
			# est ant�rieure � celle du fichier => mise � jour base
			if($ficversion ne "null" && $rapport_base_fic == 0 || $rapport_base_fic == -1)
			{
				$type_MAJ = 'u';
				push(@new_version,$genome);
				$hash_new_genomes2accession{ $genome } = $accession;
				push(@new_version_list_accnum,$accession);
			} else {
				my $element_inserted_in_table_alignment_params_prev = $ORIGAMI::dbh->selectall_arrayref("SELECT elements.accession FROM elements INNER JOIN alignment_params ON elements.element_id = alignment_params.q_element_id WHERE accession = '$accession'");
				my $element_inserted_in_table_alignment_params = $element_inserted_in_table_alignment_params_prev->[0]->[0];
				if($element_inserted_in_table_alignment_params ne $accession){
					die_with_error_mssg("The accession $accession is already present in the table elements but is not inserted in the table alignment_param. Either the script Task_add_entry_generator.pl was launched twice by mistake or this accnum was associated by mistake to an entry previously inserted. As a result the database may become instable. Please manually check what is wrong with this accnum and delete its file from $SiteConfig::BANKDIR");
					#print LOG "The accession $accession is not inserted in the table alignment_param, therefore the script assumes that Task_add_entry_generator.pl was launched twice by mistake, this accnum will not be commented out in the file gr2species.txt\n" unless $VERBOSE =~ m/^OFF$/;
				} else {
					#not inserted in table alignment_param, assume Task_add_entry_generator.pl launched twice by mistake, comment accnum
					print LOG "The accession $accession is inserted in the table alignment_param, therefore the script assumes that this accnum was listed under the directory Origami_bank by mistake, this accnum will be commented out in the file gr2species.txt\n" unless $VERBOSE =~ m/^OFF$/;
					$accnum_from_gr2species_migale_To_be_commented { $accession } = 1;
				}
			}
		} else {
			# Teste s'il y a une incoh�rence date/version (date sup�rieur mais version inf�rieure, ou l'inverse)
			if($rapport_base_fic == 1 && $version < $ficversion || $rapport_base_fic == -1 && $version > $ficversion){
				print LOG "WARNING In $genome :\nInconsistency of the date/version between base entry and file.\n" unless $VERBOSE =~ m/^OFF$/; # Erreur => pas de M�J
			} elsif($rapport_base_fic == -1 && $version <= $ficversion || $rapport_base_fic == 0 && $version < $ficversion){
				# Sinon on peut mettre � jour ce genome dans la base
				$type_MAJ = 'u';
				push(@new_version,$genome);
				$hash_new_genomes2accession{ $genome } = $accession;
				push(@new_version_list_accnum,$accession);
			} else {
				my $element_inserted_in_table_alignment_params_prev = $ORIGAMI::dbh->selectall_arrayref("SELECT elements.accession FROM elements INNER JOIN alignment_params ON elements.element_id = alignment_params.q_element_id WHERE accession = '$accession'");
				my $element_inserted_in_table_alignment_params = $element_inserted_in_table_alignment_params_prev->[0]->[0];
				if($element_inserted_in_table_alignment_params ne $accession){
					die_with_error_mssg("The accession $accession is already present in the table elements but is not inserted in the table alignment_param. Either the script Task_add_entry_generator.pl was launched twice by mistake or this accnum was associated by mistake to an entry previously inserted. As a result the database may become instable. Please manually check what is wrong with this accnum and delete its file from $SiteConfig::BANKDIR");
					#print LOG "The accession $accession is not inserted in the table alignment_param, therefore the script assumes that Task_add_entry_generator.pl was launched twice by mistake, this accnum will not be commented out in the file gr2species.txt\n" unless $VERBOSE =~ m/^OFF$/;
				} else {
					#not inserted in table alignment_param, assume Task_add_entry_generator.pl launched twice by mistake, comment accnum
					print LOG "The accession $accession is inserted in the table alignment_param, therefore the script assumes that this accnum was listed under the directory Origami_bank by mistake, this accnum will be commented out in the file gr2species.txt\n" unless $VERBOSE =~ m/^OFF$/;
					$accnum_from_gr2species_migale_To_be_commented { $accession } = 1;
				}
			}
		}
	}
	
	if(!$type_MAJ eq '')
	{
		$insert_handle->execute($accession,$type_MAJ); # ajout d'une ligne dans suivi_MAJ
		print LOG "Ajout ligne $accession,$type_MAJ dans suivi_maj\n" unless $VERBOSE =~ m/^OFF$/;
	}
	
}

my $nbn = @new_genomes;
my $utp = @new_version;
print LOG "\n\n => $nbn new elements or genomes\n => $utp elements or genomes to update\n\n" unless $VERBOSE =~ m/^OFF$/;

# suppression de la base des genomes a updater et suppresion des fichiers precedament g�n�r�es
my $count_loop_del = 0;
foreach my $accnum_to_del (@new_version_list_accnum) {
	$count_loop_del++;
	#if ($genome =~ /(\w+)\.$extension_genomes$/) {$accession = $1;}
	print LOG "( $count_loop_del / ".scalar (@new_version_list_accnum)." ) : Deleting accession: $accnum_to_del. Clean up the base.\n" unless $VERBOSE =~ m/^OFF$/;
	print LOG `perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/delete_entry.pl $accnum_to_del`;
}


# comment accnum in gr2species_migale if necessary
open( GR2SPECIES_MIGALE, "<$SiteConfig::BANKDIR/gr2species_migale.txt" ) or die_with_error_mssg("Can not open $SiteConfig::BANKDIR/gr2species_migale.txt\n");
open( GR2SPECIES_MIGALE_TMP, ">$SiteConfig::BANKDIR/gr2species_migale_tmp.txt" ) or die_with_error_mssg("Can not open $SiteConfig::BANKDIR/gr2species_migale_tmp.txt\n");
while ( my $line = <GR2SPECIES_MIGALE> ) {
	chomp($line);
	my @split = split( '\s+', $line );
	if ( $line =~ m/^#/ ) {
		# print as it
		print GR2SPECIES_MIGALE_TMP "$line\n";
		next;
	} elsif ( exists $accnum_from_gr2species_migale_To_be_commented { $split[1] }) {
		# print with comment
		print GR2SPECIES_MIGALE_TMP "#$line\n";
		next;
	} else {
		# print as it
		print GR2SPECIES_MIGALE_TMP "$line\n";
		next;
	}
}
close(GR2SPECIES_MIGALE);
close(GR2SPECIES_MIGALE_TMP);
my $mv_output = `$SiteConfig::CMDDIR/mv $SiteConfig::BANKDIR/gr2species_migale_tmp.txt $SiteConfig::BANKDIR/gr2species_migale.txt`;
if($mv_output eq ""){
	#ok
} else {
	#print LOG "Error $SiteConfig::CMDDIR/mv $SiteConfig::BANKDIR/gr2species_migale_tmp.txt $SiteConfig::BANKDIR/gr2species_migale.txt.\n";
	die_with_error_mssg("Error $SiteConfig::CMDDIR/mv $SiteConfig::BANKDIR/gr2species_migale_tmp.txt $SiteConfig::BANKDIR/gr2species_migale.txt.\n");
}



=pod
NOT doing table organism in this script, but script check_for_element_types_and_reorganize_elements_into_organisms
# get initial %species2strain2substrain2organism_id
$sql_query = $ORIGAMI::dbh->prepare("SELECT organism_id, species, strain, substrain FROM organisms");
$sql_query->execute or die_with_error_mssg("Pb execute SELECT organism_id, species, strain, substrain FROM organisms: $!");
while ( my $res = $sql_query->fetchrow_hashref() ) {
	my $organism_id_IT = $res->{organism_id};
	my $species_IT = $res->{species};
	my $strain_IT = $res->{strain};
	my $substrain_IT = $res->{substrain};
	if ( ! $species_IT) {
		#print LOG "Error get initial \%species2strain2substrain2organism_id. No species found for \$organism_id_IT $organism_id_IT\n";
		die_with_error_mssg("Error get initial \%species2strain2substrain2organism_id. No species found for \$organism_id_IT $organism_id_IT\n");
	}
	if ( ! $strain_IT) {
		$strain_IT = "NULL";
	}
	if ( ! $substrain_IT) {
		$substrain_IT = "NULL";
	}
	if ( exists $species2strain2substrain2organism_id{$species_IT} ) {
		my $ref_strain2substrain2organism_id = $species2strain2substrain2organism_id{$species_IT};
		if ( exists ${$ref_strain2substrain2organism_id}{$strain_IT}) {
			my $ref_substrain2organism_id = ${$ref_strain2substrain2organism_id}{$strain_IT};
			if ( exists ${$ref_substrain2organism_id}{$substrain_IT}) {
				# do nothing, it means 2 entries in 
				#NOT fniished
			} else {
				#NOT fniished
			}

		} else {
			my %substrain2organism_id = ();
			$substrain2organism_id{$substrain_IT} = $organism_id_IT;
			${$ref_strain2substrain2organism_id}{$strain_IT} = \%substrain2organism_id;
		}
	} else {
		my %strain2substrain2organism_id = ();
		my %substrain2organism_id = ();
		$substrain2organism_id{$substrain_IT} = $organism_id_IT;
		$strain2substrain2organism_id{$strain_IT} = \%substrain2organism_id;
		$species2strain2substrain2organism_id{$species_IT} = \%strain2substrain2organism_id;
	}
}
=cut


# get initial %species2ref_list_hash_organism_fields (organism_id, strain, substrain, taxon_id, ncbi_assemblyaccession, ncbi_isolate, serotype ) and %assemblyaccession2last_organism_id
$sql_query = $ORIGAMI::dbh->prepare("SELECT organism_id, species, strain, substrain, taxon_id, ncbi_assemblyaccession_it, ncbi_isolate_it, serotype FROM organisms");
$sql_query->execute or die_with_error_mssg("Pb execute SELECT organism_id, species, strain, substrain, taxon_id, ncbi_assemblyaccession_it, ncbi_isolate_it, serotype FROM organisms: $!");
while ( my $res = $sql_query->fetchrow_hashref() ) {
	$count_already_stored_orga_in_db++;
	my $organism_id_IT = $res->{organism_id};
	my $species_IT = $res->{species};
	my $strain_IT = $res->{strain};
	my $substrain_IT = $res->{substrain};
	my $taxon_id_IT = $res->{taxon_id};
	my $ncbi_assemblyaccession_IT = $res->{ncbi_assemblyaccession_it};
	my $ncbi_isolate_IT = $res->{ncbi_isolate_it};
	my $serotype_IT = $res->{serotype};
	my %organism_fields = ();
	$organism_fields{"organism_id"} = $organism_id_IT;
	$organism_fields{"strain"} = $strain_IT;
	$organism_fields{"substrain"} = $substrain_IT;
	$organism_fields{"taxon_id"} = $taxon_id_IT;
	$organism_fields{"ncbi_assemblyaccession"} = $ncbi_assemblyaccession_IT;
	$organism_fields{"ncbi_isolate"} = $ncbi_isolate_IT;
	$organism_fields{"serotype"} = $serotype_IT;

	if ( exists $species2ref_list_hash_organism_fields{$species_IT} ) {
		my $ref_list_hash_organism_fields = $species2ref_list_hash_organism_fields{$species_IT};
		push ( @{$ref_list_hash_organism_fields}, \%organism_fields );
	} else {
		my @list_hash_organism_fields = ();
		push (@list_hash_organism_fields, \%organism_fields) ;
		$species2ref_list_hash_organism_fields{$species_IT} = \@list_hash_organism_fields;
	}


	if ($ncbi_assemblyaccession_IT) {
		$assemblyaccession2last_organism_id{$ncbi_assemblyaccession_IT} = $organism_id_IT;
	}
	#%species2ref_list_hash_organism_fields -> hash_organism_fields {organism_id} {strain} {substrain} {taxon_id} {ncbi_assemblyaccession} {ncbi_isolate} {serotype}

}


# get initial %accession2element_id
$sql_query = $ORIGAMI::dbh->prepare("SELECT element_id, accession FROM elements");
$sql_query->execute or die_with_error_mssg("Pb execute SELECT element_id, accession FROM elements: $!");
while ( my $res = $sql_query->fetchrow_hashref() ) {
	my $element_id_IT = $res->{element_id};
	my $accession_IT = $res->{accession};
	if ( exists $accession2element_id{$accession_IT} ) {
		#print LOG "Error get initial \%accession2element_id, \$accession2element_id{\$accession_IT} already exists for \$accession_IT $accession_IT\n";
		die_with_error_mssg("Error get initial \%accession2element_id, \$accession2element_id{\$accession_IT} already exists for \$accession_IT $accession_IT\n");
	} else {
		$accession2element_id{$accession_IT} = $element_id_IT;
	}
}

# get initial $next_free_organism_id_for_db
my $max_organism_id = 0;
$sql_query = $ORIGAMI::dbh->prepare("SELECT max(organism_id) as max_organism_id FROM organisms");
$sql_query -> execute() || die_with_error_mssg("Impossible d'executer l'instruction SQL : SELECT max(organism_id) as max_organism_id FROM organisms  $DBI::errstr\n");
my $last_organism_id = $sql_query -> fetchrow_hashref;
$sql_query -> finish();
$max_organism_id = $last_organism_id -> {max_organism_id};
if ($max_organism_id) {
	$next_free_organism_id_for_db = $max_organism_id + 1;
} else {
	$next_free_organism_id_for_db = 1;
}
print LOG "Starting next free organism id for db is $next_free_organism_id_for_db\n" unless $VERBOSE =~ m/^OFF$/;


# get initial $next_free_element_id_for_db
my $max_element_id = 0;
$sql_query = $ORIGAMI::dbh->prepare("SELECT max(element_id) as max_element_id FROM elements");
$sql_query -> execute() || die_with_error_mssg("Impossible d'executer l'instruction SQL : SELECT max(element_id) as max_element_id FROM elements  $DBI::errstr\n");
my $last_element_id = $sql_query -> fetchrow_hashref;
$sql_query -> finish();
$max_element_id = $last_element_id -> {max_element_id};
if ($max_element_id) {
	$next_free_element_id_for_db = $max_element_id + 1;
} else {
	$next_free_element_id_for_db = 1;
}
print LOG "Starting next free element id for db is $next_free_element_id_for_db\n" unless $VERBOSE =~ m/^OFF$/;

# get initial $next_free_gene_id_for_db
my $max_gene_id = 0;
$sql_query = $ORIGAMI::dbh->prepare("SELECT max(gene_id) as max_gene_id FROM genes");
$sql_query -> execute() || die_with_error_mssg("Impossible d'executer l'instruction SQL : SELECT max(gene_id) as max_gene_id FROM genes  $DBI::errstr\n");
my $last_gene_id = $sql_query -> fetchrow_hashref;
$sql_query -> finish();
$max_gene_id = $last_gene_id -> {max_gene_id};
if ($max_gene_id) {
	$next_free_gene_id_for_db = $max_gene_id + 1;
} else {
	$next_free_gene_id_for_db = 1;
}
print LOG "Starting next free gene id for db is $next_free_gene_id_for_db\n" unless $VERBOSE =~ m/^OFF$/;



sub open_sql_files {
	my ($accession) = @_;       
	
	# ouverture des fichiers tables.sql
	$output_backtick = `$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_entry/$accession`;
	if ($output_backtick eq "") {
		#ok
	} else {
		#print LOG "Error could not complete $SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_entry/$accession : $!\n";
		die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_entry/$accession : $!\n");
	}

	open(ACCESS,">$SiteConfig::DATADIR/Task_add_entry/$accession/accessions.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/accessions.sql\n");
	open(ARTICLES,">$SiteConfig::DATADIR/Task_add_entry/$accession/articles.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/articles.sql\n");
	open(COMMENTS,">$SiteConfig::DATADIR/Task_add_entry/$accession/comments.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/comments.sql\n");
	open(DNA_LOC,">$SiteConfig::DATADIR/Task_add_entry/$accession/dna_loc.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/dna_loc.sql\n");
  	open(DNA_SEQ,">$SiteConfig::DATADIR/Task_add_entry/$accession/dna_seq.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/dna_seq.sql\n");
	open(FEATURES,">$SiteConfig::DATADIR/Task_add_entry/$accession/features.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/features.sql\n");
	open(KEYWORDS,">$SiteConfig::DATADIR/Task_add_entry/$accession/keywords.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/keywords.sql\n");
	open(LOCATIONS,">$SiteConfig::DATADIR/Task_add_entry/$accession/locations.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/locations.sql\n");
	open(PROT_FEAT,">$SiteConfig::DATADIR/Task_add_entry/$accession/prot_feat.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/prot_feat.sql\n");
	open(QUALIFIERS,">$SiteConfig::DATADIR/Task_add_entry/$accession/qualifiers.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/qualifiers.sql\n");
	open(SEQUENCES,">$SiteConfig::DATADIR/Task_add_entry/$accession/sequences.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/sequences.sql\n");
	open(ORGANISMS,">$SiteConfig::DATADIR/Task_add_entry/$accession/organisms.sql")|| die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/organisms.sql\n");
	open(ELEMENTS,">$SiteConfig::DATADIR/Task_add_entry/$accession/elements.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/elements.sql\n");
	open(GENES,">$SiteConfig::DATADIR/Task_add_entry/$accession/genes.sql") || die_with_error_mssg("Sorry, I can't open $SiteConfig::DATADIR/Task_add_entry/$accession/genes.sql\n");

}

# ACCESS ARTICLES COMMENTS DNA_LOC DNA_SEQ FEATURES KEYWORDS LOCATIONS PROT_FEAT QUALIFIERS SEQUENCES ORGANISMS ELEMENTS GENES
#DEPRECATED
=pod
sub print_header_sql_files { 

	print ACCESS "BEGIN WORK;\n";
	print ACCESS "set search_path to micado,public;\n";
	print ACCESS "COPY accessions (accession, old_accession) FROM stdin WITH NULL AS 'NULL';\n";

	print ARTICLES "BEGIN WORK;\n";
	print ARTICLES "set search_path to micado,public;\n";
	print ARTICLES "COPY articles (accession, num_art, title, author_list, location, medline) FROM stdin WITH NULL AS 'NULL';\n";

	print COMMENTS "BEGIN WORK;\n";
	print COMMENTS "set search_path to micado,public;\n";
	print COMMENTS "COPY comments (accession, comments) FROM stdin WITH NULL AS 'NULL';\n";

	print DNA_LOC "BEGIN WORK;\n";
	print DNA_LOC "set search_path to micado,public;\n";
	print DNA_LOC "COPY dna_loc (accession, code_feat, sequences) FROM stdin WITH NULL AS 'NULL';\n";

	print DNA_SEQ "BEGIN WORK;\n";
	print DNA_SEQ "set search_path to micado,public;\n";
	print DNA_SEQ "COPY dna_seq (accession, sequences) FROM stdin WITH NULL AS 'NULL';\n";

	print FEATURES "BEGIN WORK;\n";
	print FEATURES "set search_path to micado,public;\n";
	print FEATURES "COPY features (accession, code_feat, type_feat, location, operator_feat, posmin, posmax, strand) FROM stdin WITH NULL AS 'NULL';\n";

	print KEYWORDS "BEGIN WORK;\n";
	print KEYWORDS "set search_path to micado,public;\n";
	print KEYWORDS "COPY keywords (accession, lst_keyword) FROM stdin WITH NULL AS 'NULL';\n";

	print LOCATIONS "BEGIN WORK;\n";
	print LOCATIONS "set search_path to micado,public;\n";
	print LOCATIONS "COPY locations (accession, code_feat, code_loc, startpos_begin, startpos_end, startpos_type, stoppos_begin, stoppos_end, stoppos_type, strand, link_access) FROM stdin WITH NULL AS 'NULL';\n";

	print PROT_FEAT "BEGIN WORK;\n";
	print PROT_FEAT "set search_path to micado,public;\n";
	print PROT_FEAT "COPY prot_feat (accession, code_feat, proteine, length, crc) FROM stdin WITH NULL AS 'NULL';\n";

	print QUALIFIERS "BEGIN WORK;\n";
	print QUALIFIERS "set search_path to micado,public;\n";
	print QUALIFIERS "COPY qualifiers (accession, code_feat, code_qual, type_qual, qualifier) FROM stdin WITH NULL AS 'NULL';\n";

	print SEQUENCES "BEGIN WORK;\n";
	print SEQUENCES "set search_path to micado,public;\n";
	print SEQUENCES "COPY sequences (accession, name_seq, species, ncbi_taxid, length, molecule, subbank, date_seq, definition, version) FROM stdin WITH NULL AS 'NULL';\n";

	print ORGANISMS "BEGIN WORK;\n";
	!! NOT UPDATED print ORGANISMS "COPY organisms (organism_id,  species, strain, substrain, taxon_id) FROM stdin WITH NULL AS 'NULL';\n";

	print ELEMENTS "BEGIN WORK;\n";
	!! NOT UPDATED pprint ELEMENTS "COPY elements (element_id, organism_id, type, size, accession, mdate) FROM stdin WITH NULL AS 'NULL';\n";

	print GENES "BEGIN WORK;\n";
	print GENES "COPY genes (gene_id, organism_id, element_id, name, strand, start, stop, feature_id, locus_tag, accession) FROM stdin WITH NULL AS 'NULL';\n";
}
=cut

# ACCESS ARTICLES COMMENTS DNA_LOC DNA_SEQ FEATURES KEYWORDS LOCATIONS PROT_FEAT QUALIFIERS SEQUENCES ORGANISMS ELEMENTS GENES
#deprecated
=pod
sub print_footer_sql_files { 
	print ACCESS "\\.\nCOMMIT WORK;\n";
	print ARTICLES "\\.\nCOMMIT WORK;\n";
	print COMMENTS "\\.\nCOMMIT WORK;\n";
	print DNA_LOC "\\.\nCOMMIT WORK;\n";
	print DNA_SEQ "\\.\nCOMMIT WORK;\n";
	print FEATURES "\\.\nCOMMIT WORK;\n";
	print KEYWORDS "\\.\nCOMMIT WORK;\n";
	print LOCATIONS "\\.\nCOMMIT WORK;\n";
	print PROT_FEAT "\\.\nCOMMIT WORK;\n";
	print QUALIFIERS "\\.\nCOMMIT WORK;\n";
	print SEQUENCES "\\.\nCOMMIT WORK;\n";
	print ORGANISMS "\\.\nCOMMIT WORK;\n";
	print ELEMENTS "\\.\nCOMMIT WORK;\n";
	print GENES "\\.\nCOMMIT WORK;\n";
}
=cut


sub close_sql_files { 

	# ACCESS ARTICLES COMMENTS DNA_LOC DNA_SEQ FEATURES KEYWORDS LOCATIONS PROT_FEAT QUALIFIERS SEQUENCES ORGANISMS ELEMENTS GENES
 	close(ACCESS);
 	close(ARTICLES);
 	close(COMMENTS);
 	close(DNA_LOC);
 	close(DNA_SEQ);
	close(FEATURES);
 	close(KEYWORDS);
	close(LOCATIONS);
	close(PROT_FEAT);
 	close(QUALIFIERS);
	close(SEQUENCES);
	close(ORGANISMS);
	close(ELEMENTS);
	close(GENES);

}


# =====================================================================
# Supprime les cotes et les tabulations d'une chaine de caracteres
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub string_encod {
	my ($string) = @_;
	#if ($string){
	 	$string =~ s/'/ /g;
 	        $string =~ s/\t/ /g;
		$string =~ s/[;\\]//g;
	 	return $string;
	#} else {
	#	return "";
	#}
}

=pod
# =====================================================================
# Supprime les tabulations d'une chaine de caracteres
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub string_encodtab {
	my ($string) = @_;
	
	if ($string){
 	        $string =~ s/\t/ /g;
	 	return $string;
	} else {
		return "";
	}
}
=cut

sub insert_sequences_into_sql_files {
	my ($seq, $access, $genome_file) = @_;
	
	my %stored_info_from_sequences_to_return = (); # {def} {length} {taxon_id} {version}

	my $name_seq = $seq->display_id;
	my $species = "not defined";
	my $ncbi_taxid = "NULL" ;	
	my $version = "NULL" ;
        # retourne un objet Bio::Species    ;
 	my $spec=$seq->species();
	if (defined $spec){
	    #$species=$spec->common_name ; # nom commun
            #$species=$spec->species ;      # nom scientifique
	    $species=$spec->binomial ;      # maj pour Genome_Reviews
	    if (defined $spec->ncbi_taxid) {
	 	$ncbi_taxid = $spec->ncbi_taxid;
		$stored_info_from_sequences_to_return{"taxon_id"} = $ncbi_taxid;
	    }else{
		#more $genome_file | grep '/db_xref="taxon:'
		#result in FT                   /db_xref="taxon:222891"
		my $taxon_id_from_file = `more $genome_file | grep '/db_xref="taxon:'`;
		if ( $taxon_id_from_file =~ m/^FT\s*\/db\_xref\=\"taxon\:(\d+).*$/ ) {
			$ncbi_taxid = $1;
			$stored_info_from_sequences_to_return{"taxon_id"} = $ncbi_taxid;
		}
	    }
	}

	my $length = $seq->length;
	if (!$length){
		$length = 'NULL';
	} else {
		$stored_info_from_sequences_to_return{"length"} = $length;
	}
	my $molecule = $seq->molecule;
	if (!$molecule){
		$molecule = 'NULL';
	}
	my $subbank = $seq->division;
	if (!$subbank){
		$subbank = 'NULL';
	}
 	my @dates = $seq->get_dates();
	my $date_seq = $dates[0];

	my $i = index($date_seq,' (');
        if ($i>0) {
	    $date_seq = substr($date_seq,0,$i);
	}

	my $def = $seq->desc;
	if (!$def){
		$def = 'NULL';
	} else {
		$stored_info_from_sequences_to_return{"def"} = $def;
	}
	if (defined $seq->version) {
	    $version=$seq->version;
	    $stored_info_from_sequences_to_return{"version"} = $version;
	}

	#"COPY sequences (accession, name_seq, species, ncbi_taxid, length, molecule, subbank, date_seq, definition, version) FROM stdin WITH NULL AS 'NULL';\n";
	$access = string_encod($access);
	$name_seq = string_encod($name_seq);
	$species = string_encod($species);
	$ncbi_taxid = string_encod($ncbi_taxid);
	$length = string_encod($length);
	$molecule = string_encod($molecule);
	$subbank = string_encod($subbank);
	$date_seq = string_encod($date_seq);
	$def = string_encod($def);
	$version = string_encod($version);
	print SEQUENCES "$access\t$name_seq\t$species\t$ncbi_taxid\t$length\t$molecule\t$subbank\t$date_seq\t$def\t$version\n";
	return \%stored_info_from_sequences_to_return;
}


sub insert_accessions_into_sql_files {
# table des numeros d'accessions synonymes
    my ( $seq, $accession) = @_;
    my @access_bis=$seq->get_secondary_accessions();
    foreach my $access (@access_bis) {
	$accession = string_encod($accession);
	$access = string_encod($access);
    	print ACCESS $accession, "\t",$access,"\n" ;
    }
}


sub insert_comments_into_sql_files {
# table des commentaires
    my ( $seq, $accession)=  @_;
    my $ac = $seq->annotation();
    my @comment=$ac->get_Annotations('comment');
    foreach my $value ( @comment ) {
	my $comment_IT = $value->text();
	my $comment_IT =~ s/[\n\r]/\. /g;
	$accession = string_encod($accession);
	$comment_IT = string_encod($comment_IT);
	print COMMENTS "$accession\t$comment_IT\n";
    }
}

sub insert_keywords_into_sql_files {	
	my ( $seq, $access ) = @_;
	my $words = join(" ",$seq->keywords);
	if ((!$words) || ($words eq "")){
		return;
	}
	$access = string_encod($access);
	$words = string_encod($words);
	print KEYWORDS "$access\t$words\n";
	return $words;
}

sub insert_references_into_sql_files {
	my ($seq, $access) = @_;
	#my $tab_name = 'articles';
	my $num_art = 1;
	my $ann = $seq->annotation(); # annotation object
	foreach my $ref ( $ann->get_Annotations('reference') ) {
		my $title = $ref->title;
		if (!$title){
			$title = 'NULL';
		}
				
		my $authors = $ref->authors;
		if (!$authors){
			$authors = 'Unknown';
		}

		my $location = $ref->location;
		if (!$location){
			$location = 'NULL';
		}

		my $medline = $ref->medline;
		if (!$medline){
			$medline = 'NULL';
		}
		
		# on insert l'enregistrement
		$access = string_encod($access);
		$num_art = string_encod($num_art);
		$title = string_encod($title);
		$authors = string_encod($authors);
		$location = string_encod($location);
		$medline = string_encod($medline);
		print ARTICLES "$access\t$num_art\t$title\t$authors\t$location\t$medline\n";
		$num_art++;
	}
}

sub insert_dnaseq_into_sql_files {
	my ( $seq, $access ) = @_;
	#my $tab_name = "dna_seq";
	my $sequence = $seq->seq();
	# on insert l'enregistrement
	$access = string_encod($access);
	$sequence = string_encod($sequence);
	print DNA_SEQ "$access\t$sequence\n";
}


sub insert_feature_into_sql_files {
	my ( $feat, $access, $seq, $code_feat ) = @_;
	# valeurs par defaut
	my $pos_min = 0;
	my $pos_max = 0;
	my $strand = "NULL";
	my $operator_feat = "NULL" ;
	my $type_feat = "NULL" ;
	my $location_feat = "NULL" ;
	
	my $location = $feat->location();

	if (defined $location->min_start) {
	    $pos_min=$location->min_start;
	}
	else {
	    $pos_min=$location->start;
	}
	if (defined $location->max_end) {
	    $pos_max=$location->max_end;
	}
	else {
	    $pos_max=$location->end;
	}	
	#if (!($strand = $feat->strand)){
	#	$strand = 0;
	#}
	 # Cas d'un location avec operateur JOIN ou ORDER
	if ($location->isa('Bio::Location::SplitLocationI')) {
	 $operator_feat = $feat->location->splittype();
        }
	else {
	    $operator_feat="NULL";
	}
	$type_feat=$feat->primary_tag ;
	#$strand=$location->strand ;
	if (defined $feat->strand ) {
	    $strand= $feat->strand ;
	}
	if (defined $location->to_FTstring()) {
	    $location_feat=$location->to_FTstring();
	}
	else {
	    $location_feat="NULL";
	}
	# on insert l'enregistrement
	$access = string_encod($access);
	$code_feat = string_encod($code_feat);
	$type_feat = string_encod($type_feat);
	$location_feat = string_encod($location_feat);
	$operator_feat = string_encod($operator_feat);
	$pos_min = string_encod($pos_min);
	$pos_max = string_encod($pos_max);
	$strand = string_encod($strand);
	print FEATURES "$access\t$code_feat\t$type_feat\t$location_feat\t$operator_feat\t$pos_min\t$pos_max\t$strand\n";
	
	my %to_return_from_insert_feature_into_sql_files = ();
	$to_return_from_insert_feature_into_sql_files{"pos_min"} = $pos_min;
	$to_return_from_insert_feature_into_sql_files{"pos_max"} = $pos_max;
	$to_return_from_insert_feature_into_sql_files{"strand"} = $strand;
	$to_return_from_insert_feature_into_sql_files{"type_feat"} = $type_feat;
	return \%to_return_from_insert_feature_into_sql_files;
}


sub insert_locations_into_sql_files{

	my ( $feat, $access, $code_feat, $seq) = @_;
	
	my $stop_pos_begin;
	my $stop_pos_end;
	my $stop_pos_type;
	my $start_pos_end;
	my $start_pos_type;
	my $start_pos_begin;
	my $link_access="NULL";
	my $strand=0; 
	my $code_loc=0 ;

	if (defined $feat->strand) {
	 $strand = $feat->strand;	
        }
	my $location = $feat->location();

	if ($location->isa("Bio::Location::SplitLocationI")) { # cas d'une location complexe avec join ou order
		my @sublocations = $location->sub_Location;	
		for ($code_loc = 0 ; $code_loc < @sublocations ; $code_loc++) {
		    $start_pos_type = $sublocations[$code_loc]->start_pos_type();
		    $stop_pos_type = $sublocations[$code_loc]->end_pos_type();
		    if  (defined $location->seq_id) {
			$link_access=$location->seq_id ;
		    } else {
			$link_access="NULL" ;
		    }
		    ($start_pos_begin = $sublocations[$code_loc]->min_start()) 
			|| ($start_pos_begin = $sublocations[$code_loc]->start) ;
		    ($start_pos_end = $sublocations[$code_loc]->max_start()) 
			|| ($start_pos_end = $sublocations[$code_loc]->start) ;
		    ($stop_pos_begin = $sublocations[$code_loc]->min_end()) 
			|| ($stop_pos_begin = $sublocations[$code_loc]->end) ;
		    ($stop_pos_end = $sublocations[$code_loc]->max_end()) 
			|| ($stop_pos_end = $sublocations[$code_loc]->end) ;
		   $access = string_encod($access);
		   $code_feat = string_encod($code_feat);
		   $code_loc = string_encod($code_loc);
		   $start_pos_begin = string_encod($start_pos_begin);
		   $start_pos_end = string_encod($start_pos_end);
		   $start_pos_type = string_encod($start_pos_type);
		   $stop_pos_begin = string_encod($stop_pos_begin);
		   $stop_pos_end = string_encod($stop_pos_end);
		   $stop_pos_type = string_encod($stop_pos_type);
		   $strand = string_encod($strand);
		   $link_access = string_encod($link_access);
		   print LOCATIONS "$access\t$code_feat\t$code_loc\t$start_pos_begin\t$start_pos_end\t$start_pos_type\t$stop_pos_begin\t$stop_pos_end\t$stop_pos_type\t$strand\t$link_access\n" ;
		}
	} else { # cas d'une location simple sans operateur	
		$start_pos_type = $location->start_pos_type();
		$stop_pos_type = $location->end_pos_type();
		$link_access="NULL" ;
		($start_pos_begin = $location->min_start()) || ($start_pos_begin = $location->start) ;
		($start_pos_end = $location->max_start()) || ($start_pos_end = $location->start) ;
		($stop_pos_begin = $location->min_end()) || ($stop_pos_begin = $location->end) ;
		($stop_pos_end = $location->max_end()) || ($stop_pos_end = $location->end) ;
		$access = string_encod($access);
		$code_feat = string_encod($code_feat);
		$code_loc = string_encod($code_loc);
		$start_pos_begin = string_encod($start_pos_begin);
		$start_pos_end = string_encod($start_pos_end);
		$start_pos_type = string_encod($start_pos_type);
		$stop_pos_begin = string_encod($stop_pos_begin);
		$stop_pos_end = string_encod($stop_pos_end);
		$stop_pos_type = string_encod($stop_pos_type);
		$strand = string_encod($strand);
		$link_access = string_encod($link_access);
		print LOCATIONS "$access\t$code_feat\t$code_loc\t$start_pos_begin\t$start_pos_end\t$start_pos_type\t$stop_pos_begin\t$stop_pos_end\t$stop_pos_type\t$strand\t$link_access\n" ; 
	}	
	#return $code_loc ;
}


sub insert_dnaloc_into_sql_files{
	my ( $feat, $access, $code_feat, $seq ) = @_;
	my $sequence = "";
	if ($feat->primary_tag ne "source") {
           $sequence = $feat->spliced_seq()->seq();
	   if ($sequence ne "") {
	      $access = string_encod($access);
	      $code_feat = string_encod($code_feat);
	      $sequence = string_encod($sequence);
	      print DNA_LOC "$access\t$code_feat\t$sequence\n";
           }
        }
}


sub insert_protfeat_into_sql_files{
	my ( $feat, $access, $prot, $code_feat, $seq) = @_;
	$prot =~ s/O/X/gi; # fix for some sequence that have a typo in them, the letter O doesn't exists in 20 natural amino acid notation but not supported by some version of blast/plast ; replace with X unknown amino acid
	my $length_residues_to_return = 0;
	if($prot){
		my $bpseq = Bio::FASTASequence->new(">sp|$access|t$code_feat\n".$prot);
		my $checksum = $bpseq->getCrc64();
		$length_residues_to_return = length($prot);
                # on insert l'enregistrement
		$access = string_encod($access);
		$code_feat = string_encod($code_feat);
		$prot = string_encod($prot);
		$length_residues_to_return = string_encod($length_residues_to_return);
		$checksum = string_encod($checksum);
		print PROT_FEAT "$access\t$code_feat\t$prot\t$length_residues_to_return\t$checksum\n";
	} else {
		print LOG "WARNING : CDS with code_feat $code_feat and accession $access has an empty protein sequence : $prot\n" unless $VERBOSE =~ m/^OFF$/;
	}
	return $length_residues_to_return;
}	



sub insert_qualifiers_into_sql_files{

	my ( $feat, $access, $code_feat, $seq, $type_feat_IT) = @_;
	
	my $qualifier = "";
	my $type_qual = "";
	my $code_qual = 1; 
	
	my %to_return_from_insert_qualifiers_into_sql_files = (); # {name} {locus_tag} {chromosome} {plasmid} {organism} {subspecies} {strain} {substrain} {species} {isolate} {serotype} {pseudo} {length_residues}
	my $translation_tag_found = 0;
	my $is_pseudogene = 0;
	foreach my $tags ($feat->get_all_tags){ # pour chaque features
		my @values = $feat->get_tag_values($tags);
		#print "\$tags = $tags ; \@values = ".join(", ", @values)."\n";
		if($tags ne "translation"){ # pour tous les qualifiers sauf translation
			foreach my $val (@values){
				$qualifier = string_encod($val);
				#$qualifier = string_encodtab($val);
				$type_qual = string_encod($tags);
				
				# on insert l'enregistrement
				$access = string_encod($access);
				$code_feat = string_encod($code_feat);
				$code_qual = string_encod($code_qual);
				#$type_qual = string_encod($type_qual);
				#$qualifier = string_encod($qualifier);
				print QUALIFIERS "$access\t$code_feat\t$code_qual\t$type_qual\t$qualifier\n";
				if ( $type_feat_IT eq "CDS" && ( $type_qual eq "pseudo" || $type_qual eq "pseudogene" ) ) {
					$to_return_from_insert_qualifiers_into_sql_files{"pseudo"} = $qualifier;
					$is_pseudogene = 1;
				}
				if ( $type_feat_IT eq "CDS" && $type_qual eq "gene_name" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"name"} = $qualifier;
				}
				if ( $type_feat_IT eq "CDS" && $type_qual eq "gene" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"name"} = $qualifier;
				}
				if ( $type_feat_IT eq "CDS" && $type_qual eq "locus_tag" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"locus_tag"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "chromosome" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"chromosome"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "plasmid" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"plasmid"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "organism" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"organism"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "subspecies" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"subspecies"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "sub_species" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"subspecies"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "strain" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"strain"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "substrain" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"substrain"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "sub_strain" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"substrain"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "species" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"species"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "isolate" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"isolate"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "serovar" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"serotype"} = $qualifier;
				}
				if ( $type_feat_IT eq "source" && $type_qual eq "serotype" ) {
					$to_return_from_insert_qualifiers_into_sql_files{"serotype"} = $qualifier;
				}
				$code_qual++;
			}
		} else {
			$translation_tag_found = 1;
			my $length_residues = insert_protfeat_into_sql_files($feat, $access, $values[0], $code_feat, $seq);
			if ( $type_feat_IT eq "CDS") {
				$to_return_from_insert_qualifiers_into_sql_files{"length_residues"} = $length_residues;
			}
		}
	}
	if ( $type_feat_IT eq "CDS" && $translation_tag_found != 1 && $is_pseudogene != 1 ) {
		print LOG "WARNING : CDS with locus_tag = ".$to_return_from_insert_qualifiers_into_sql_files{"locus_tag"}." (code_feat $code_feat) and accession $access has no translation tag and is not marked as pseudogene\n" unless $VERBOSE =~ m/^OFF$/;
	}
	return \%to_return_from_insert_qualifiers_into_sql_files;
}



sub insert_features_into_sql_files {
	my ( $seq, $access) = @_;
	my %stored_info_from_features = (); # {chromosome} {plasmid} {ref_hash_gene_id2gene_fields} # {name} {locus_tag} {chromosome} {plasmid} {organism} {subspecies} {strain} {substrain} {species} {isolate} {serotype}
	my %gene_id2gene_fields = ();

	my @features = $seq->get_SeqFeatures;
	my $code_feat = 1;
	foreach my $feat ( @features ) {

		my $ref_hash_from_insert_feature_into_sql_files = insert_feature_into_sql_files($feat, $access, $seq, $code_feat); # FEATURES return {pos_min} {pos_max} {strand} {type_feat}
		insert_locations_into_sql_files($feat, $access, $code_feat, $seq);# LOCATIONS
		insert_dnaloc_into_sql_files($feat, $access, $code_feat, $seq);# DNA_LOC
		my $type_feat_IT = "";
		if (exists ${$ref_hash_from_insert_feature_into_sql_files}{"type_feat"}) {
			$type_feat_IT = ${$ref_hash_from_insert_feature_into_sql_files}{"type_feat"};
		}
		my $ref_hash_from_insert_qualifiers_into_sql_files = insert_qualifiers_into_sql_files($feat, $access, $code_feat, $seq, $type_feat_IT);# QUALIFIERS PROT_FEAT return {name} {locus_tag} {chromosome} {plasmid} {organism} {subspecies} {strain} {substrain} {species} {isolate} {serotype} {pseudo} {length_residues}

		if ( $type_feat_IT eq "CDS" ) {
			# store gene info for the accnum
			my %gene_fields_IT = ();
			my $name = "NULL";
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"name"}) {
				$name = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"name"};
			}
			$gene_fields_IT{"name"} = $name;
			my $strand = "NULL";
			if ( exists ${$ref_hash_from_insert_feature_into_sql_files}{"strand"}) {
				$strand = ${$ref_hash_from_insert_feature_into_sql_files}{"strand"};
			}
			$gene_fields_IT{"strand"} = $strand;
			my $start = "NULL";
			if ( exists ${$ref_hash_from_insert_feature_into_sql_files}{"pos_min"}) {
				$start = ${$ref_hash_from_insert_feature_into_sql_files}{"pos_min"};
			}
			$gene_fields_IT{"start"} = $start;
			my $stop = "NULL";
			if ( exists ${$ref_hash_from_insert_feature_into_sql_files}{"pos_max"}) {
				$stop = ${$ref_hash_from_insert_feature_into_sql_files}{"pos_max"};
			}
			$gene_fields_IT{"stop"} = $stop;
			my $locus_tag = "NULL";
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"locus_tag"}) {
				$locus_tag = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"locus_tag"};
			}
			$gene_fields_IT{"locus_tag"} = $locus_tag;
			my $is_pseudo = "FALSE";
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"pseudo"}) {
				$is_pseudo = "TRUE";
			}
			$gene_fields_IT{"is_pseudo"} = $is_pseudo;
			my $length_residues = 0;
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"length_residues"}) {
				$length_residues = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"length_residues"};
			}
			$gene_fields_IT{"length_residues"} = $length_residues;

			$gene_fields_IT{"code_feat"} = $code_feat;
			$gene_id2gene_fields{$next_free_gene_id_for_db} = \%gene_fields_IT; # name, strand, start, stop, locus_tag, code_feat, is_pseudo, length_residues
			$next_free_gene_id_for_db++;
		} elsif ( $type_feat_IT eq "source" ) {
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"chromosome"}) {
				$stored_info_from_features{"chromosome"} = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"chromosome"};
			}
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"plasmid"}) {
				$stored_info_from_features{"plasmid"} = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"plasmid"};
			}
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"organism"}) {
				$stored_info_from_features{"organism"} = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"organism"};
			}
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"subspecies"}) {
				$stored_info_from_features{"subspecies"} = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"subspecies"};
			}
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"strain"}) {
				$stored_info_from_features{"strain"} = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"strain"};
			}
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"substrain"}) {
				$stored_info_from_features{"substrain"} = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"substrain"};
			}
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"species"}) {
				$stored_info_from_features{"species"} = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"species"};
			}
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"isolate"}) {
				$stored_info_from_features{"isolate"} = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"isolate"};
			}
			if ( exists ${$ref_hash_from_insert_qualifiers_into_sql_files}{"serotype"}) {
				$stored_info_from_features{"serotype"} = ${$ref_hash_from_insert_qualifiers_into_sql_files}{"serotype"};
			}

		}

		$code_feat++;
	}
	$stored_info_from_features{"ref_hash_gene_id2gene_fields"} = \%gene_id2gene_fields;
	return \%stored_info_from_features;

}


sub get_ncbi_assemblyaccession_for_accnum {
	my ( $accnum, $version, $counter_loop, $scalar_new_genomes ) = @_;
	my %ncbi_assembly_and_accession_info_from_url_request = (); # assembly_NCBI_internal_id_IT, assembly_NCBI_assemblyaccession_IT, assembly_NCBI_assemblyname_IT, assembly_NCBI_lastupdatedate_IT, assembly_NCBI_seqreleasedate_IT, assembly_NCBI_species_IT, assembly_NCBI_strain_IT, assembly_NCBI_substrain_IT, assembly_NCBI_isolate_IT, assembly_NCBI_serotype_IT, assembly_NCBI_speciestaxid_IT, assembly_NCBI_taxon_id_IT, assembly_NCBI_biosampleid_IT, assembly_NCBI_biosampleaccn_IT, assembly_NCBI_list_bioprojectid_IT, assembly_NCBI_list_bioprojectaccn_IT, assembly_NCBI_assemblyclass_IT, assembly_NCBI_assemblystatus_IT, accnum_NCBI_internal_id_IT, accnum_NCBI_sourcedb, accnum_NCBI_genome, accnum_NCBI_tech, accnum_NCBI_geneticcode, accnum_NCBI_topology, accnum_NCBI_completeness, accnum_NCBI_status, accnum_NCBI_comment

	my $accnum_IT = $accnum;
	$accnum_IT =~ s/_GR$//;
	my $assembly_NCBI_internal_id_IT = undef;
	my $assembly_NCBI_assemblyaccession_IT = undef;
	my $assembly_NCBI_assemblyname_IT = undef;
	my $assembly_NCBI_lastupdatedate_IT = undef;
	my $assembly_NCBI_seqreleasedate_IT = undef;
	my $assembly_NCBI_species_IT = undef;
	my $assembly_NCBI_strain_IT = undef;
	my $assembly_NCBI_substrain_IT = undef;
	my $assembly_NCBI_isolate_IT = undef;
	my $assembly_NCBI_serotype_IT = undef;
	my $assembly_NCBI_speciestaxid_IT = undef;
	my $assembly_NCBI_taxon_id_IT = undef;
	my $assembly_NCBI_biosampleid_IT = undef;
	my $assembly_NCBI_biosampleaccn_IT = undef;
	my @assembly_NCBI_list_bioprojectid_IT = ();
	my @assembly_NCBI_list_bioprojectaccn_IT = ();
	my $assembly_NCBI_assemblyclass_IT = undef;
	my $assembly_NCBI_assemblystatus_IT = undef;
	my $accnum_NCBI_internal_id_IT = undef;
	my $accnum_NCBI_sourcedb = undef;
	my $accnum_NCBI_genome = undef;
	my $accnum_NCBI_tech = undef;
	my $accnum_NCBI_geneticcode = undef;
	my $accnum_NCBI_topology = undef;
	my $accnum_NCBI_completeness = undef;
	my $accnum_NCBI_status = undef;
	my $accnum_NCBI_comment = undef;



	# get assembly_id for accnum
	# https://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=nucleotide&db=assembly&id=NC_018221&retmode=json
	# https://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=nucleotide&db=assembly&id=NC_018222&retmode=json
	# https://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=nucleotide&db=assembly&id=NC_018223&retmode=json

	my @list_potential_assembly_NCBI_internal_id = ();
	my @list_potential_accnum_NCBI_internal_id = ();
	$try_fetch_with_curl = 0;
	TRY_FETCH_CURL_elink: while ($try_fetch_with_curl < 5) {
		$try_fetch_with_curl++;
		# download url
		$url_to_download_IT = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=nucleotide&db=assembly&id=${accnum_IT}&retmode=json";
		$output_backtick = `rm -f ${DIR_TMP_DOWNLOAD}/elink.out`;
		$output_backtick = `curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error`;
		if ($output_backtick eq "" && -e "${DIR_TMP_DOWNLOAD}/elink.out") {
			#ok
			print LOG "( $counter_loop / $scalar_new_genomes ) : Parsing file downloaded from download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n" unless $VERBOSE =~ m/^OFF$/i;
			# parse json with perl
			my $json_text_elink = "";
			{
			  local $/ = undef;
			  open FILE, "<${DIR_TMP_DOWNLOAD}/elink.out" or die_with_error_mssg("Error. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nCouldn't open file ${DIR_TMP_DOWNLOAD}/elink.out: $!\n");
			  binmode FILE;
			  $json_text_elink = <FILE>;
			  close FILE;
			}
			#print "json_text=\n$json_text\n";
			my $json_elink = JSON->new->pretty;
			my $json_object_elink = "";
			try {
				$json_object_elink = $json_elink->decode($json_text_elink);
			} catch {
				if ($try_fetch_with_curl >= 2) {
					#die_with_error_mssg("Error elink.out try/catch. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n");
					# assume private genome or genome with no info in ncbi
					print LOG "( $counter_loop / $scalar_new_genomes ) : Error decoding JSON file after $try_fetch_with_curl tries for the file from : curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error.\nAssuming it is a private genome or a genome with no info in ncbi, skipping those specific information.\n" unless $VERBOSE =~ m/^OFF$/i;
					last TRY_FETCH_CURL_elink;
				} else {
					my $sleep_time = 10 * $try_fetch_with_curl * $try_fetch_with_curl * $try_fetch_with_curl;
					print LOG "( $counter_loop / $scalar_new_genomes ) : Error decoding JSON file, sleep $sleep_time second and try again ($try_fetch_with_curl times) : curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n" unless $VERBOSE =~ m/^OFF$/i;
					sleep($sleep_time);
					next TRY_FETCH_CURL_elink;
				}
			};
			#print  Dumper($json_object);



			foreach my $ref_hash_linksets_IT ( @{ $json_object_elink->{linksets} } )
			{
				$NCBI_linksets_found++;
				#print "link_IT = $ref_hash_linksets_IT\n";
				my $ref_array_linksetdbs = ${$ref_hash_linksets_IT}{linksetdbs};
				foreach my $ref_hash_linksetdbs_IT ( @{ $ref_array_linksetdbs } ) {
					my $ref_array_links = ${$ref_hash_linksetdbs_IT}{links};
					foreach my $link_IT ( @{ $ref_array_links } ) {
						# can be one assembly_NCBI_internal_id_IT for each version (.1, .2 etc...)
						push ( @list_potential_assembly_NCBI_internal_id, $link_IT) ;
						#if ($assembly_NCBI_internal_id_IT) {
						#	#print LOG "Error multiple assembly NCBI internal id for accnum $accnum_IT file:\n$json_text_elink \n";
						#	die_with_error_mssg("Error multiple assembly NCBI internal id for accnum $accnum_IT file:\n$json_text_elink \n");
						#} else {
						#	$assembly_NCBI_internal_id_IT = $link_IT;
						#}
					}
				}

				my $ref_array_ids = ${$ref_hash_linksets_IT}{ids};
				foreach my $ids_IT ( @{ $ref_array_ids } ) {
					push ( @list_potential_accnum_NCBI_internal_id, $ids_IT) ;
					#if ($accnum_NCBI_internal_id_IT) {
					#	#print LOG "Error multiple accnum NCBI internal id IT for accnum $accnum_IT file:\n$json_text_elink \n";
					#	die_with_error_mssg("Error multiple accnum NCBI internal id IT for accnum $accnum_IT file:\n$json_text_elink \n");
					#} else {
					#	$accnum_NCBI_internal_id_IT = $ids_IT;
					#}
				}

			}
			last;
		} else {
			if ($try_fetch_with_curl >= 4) {
				#print LOG "Error elink.out. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n";
				die_with_error_mssg("Error elink.out. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n");
			} else {
				my $sleep_time = 10 * $try_fetch_with_curl * $try_fetch_with_curl * $try_fetch_with_curl;
				print LOG "( $counter_loop / $scalar_new_genomes ) : sleep $sleep_time second and try again ($try_fetch_with_curl times) : curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n" unless $VERBOSE =~ m/^OFF$/i;
				sleep($sleep_time);
				next TRY_FETCH_CURL_elink;
			}

			# do not die because some accnum do not have corresponding assembly
			##print LOG "Error could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n";
			#die_with_error_mssg("Error could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/elink.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n");
		}	

	}
	

	# and then get more assembly info
	# https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=assembly&id=402858&retmode=json
	POTENTIAL_ASSEMBLY_NCBI_INTERNAL_ID: foreach my $potential_assembly_NCBI_internal_id_IT ( @list_potential_assembly_NCBI_internal_id ) {
	#if ($assembly_NCBI_internal_id_IT) {

		$try_fetch_with_curl = 0;
		TRY_FETCH_CURL_esummary1: while ($try_fetch_with_curl < 5) {
			$try_fetch_with_curl++;

			# download url
			$url_to_download_IT = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=assembly&id=${potential_assembly_NCBI_internal_id_IT}&retmode=json";
			$output_backtick = `rm -f ${DIR_TMP_DOWNLOAD}/esummary.out`;
			$output_backtick = `curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error`;
			if ($output_backtick eq "" && -e "${DIR_TMP_DOWNLOAD}/esummary.out") {
				#ok
				print LOG "( $counter_loop / $scalar_new_genomes ) : Done download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n" unless $VERBOSE =~ m/^OFF$/i;

				# parse json with perl
				my $json_text_esummary = "";
				{
				  local $/ = undef;
				  open FILE, "<${DIR_TMP_DOWNLOAD}/esummary.out" or die_with_error_mssg("Error. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nCouldn't open file ${DIR_TMP_DOWNLOAD}/esummary.out: $!");
				  binmode FILE;
				  $json_text_esummary = <FILE>;
				  close FILE;
				}
				my $json_esummary = JSON->new->pretty;
				my $json_object_esummary = "";
				try {
					$json_object_esummary = $json_esummary->decode($json_text_esummary);
				} catch {
					if ($try_fetch_with_curl >= 4) {
						#print LOG "Error esummary.out 1. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n";
						die_with_error_mssg("Error esummary.out 1. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n");
					} else {
						my $sleep_time = 10 * $try_fetch_with_curl * $try_fetch_with_curl * $try_fetch_with_curl;
						print LOG "( $counter_loop / $scalar_new_genomes ) : sleep $sleep_time second and try again ($try_fetch_with_curl times) : curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n" unless $VERBOSE =~ m/^OFF$/i;
						sleep($sleep_time);
						next TRY_FETCH_CURL_esummary1;
					}
				};
				#print  Dumper($json_object_esummary);


			#fields of interest:
			#root structure
			# $VAR1 = { 'result' => { '${assembly_NCBI_internal_id_IT}' => {
				my $ref_hash_root_structure = $json_object_esummary->{result}->{${potential_assembly_NCBI_internal_id_IT}};
			# ids
				#'assemblyaccession' => 'GCF_000281195.1'
				$assembly_NCBI_assemblyaccession_IT = ${ref_hash_root_structure}->{assemblyaccession};


				# set $assembly_NCBI_internal_id_IT for the right $version 
				if ($assembly_NCBI_assemblyaccession_IT =~ m/\.(\d+)$/i) {
					my $version_from_url = $1;
					if ($version && $version_from_url && $version == $version_from_url) {
						# ok match
						#print "assembly $accnum_IT right version $version\n";
						$assembly_NCBI_internal_id_IT = $potential_assembly_NCBI_internal_id_IT;
					} else {
						# not the right match
						#print "assembly $accnum_IT not right version $version VS $version_from_url\n";
						#$assembly_NCBI_internal_id_IT = undef;
						$assembly_NCBI_assemblyaccession_IT = undef;
						next POTENTIAL_ASSEMBLY_NCBI_INTERNAL_ID;
					}
				} else {
					# no version detected
					if ( ! $version) {
						# ok match
						#print "assembly $accnum_IT right version $version\n";
						$assembly_NCBI_internal_id_IT = $potential_assembly_NCBI_internal_id_IT;
					} else {
						# not the right match
						#$assembly_NCBI_internal_id_IT = undef;
						#print "assembly $accnum_IT not right version $version VS [EMPTY]\n";
						$assembly_NCBI_assemblyaccession_IT = undef;
						next POTENTIAL_ASSEMBLY_NCBI_INTERNAL_ID;
					}
				}

				if ($assembly_NCBI_assemblyaccession_IT) {
					$NCBI_assemblyaccession_found++;
				}
				#'assemblyname' => 'ASM28119v1'
				$assembly_NCBI_assemblyname_IT = ${ref_hash_root_structure}->{assemblyname};
			# date
				#'lastupdatedate' => '2012/07/25 00:00',
				$assembly_NCBI_lastupdatedate_IT = ${ref_hash_root_structure}->{lastupdatedate};
				#'seqreleasedate' => '2012/07/24 00:00',
				$assembly_NCBI_seqreleasedate_IT = ${ref_hash_root_structure}->{seqreleasedate};
			# orga info
				#'speciesname' => 'Enterococcus faecalis',
				$assembly_NCBI_species_IT = ${ref_hash_root_structure}->{speciesname};
				#'biosource' => {
				#      'sex' => '',
				#      'infraspecieslist' => [
				#           {
				#           'sub_value' => 'D32',
				#           'sub_type' => 'strain'
				#            }
				my $ref_array_infraspecieslist = ${ref_hash_root_structure}->{biosource}->{infraspecieslist};
				foreach my $ref_hash_infraspecieslist_IT ( @{ $ref_array_infraspecieslist } ) {
					if (${$ref_hash_infraspecieslist_IT}{"sub_type"} eq "strain") {
						$assembly_NCBI_strain_IT = ${$ref_hash_infraspecieslist_IT}{"sub_value"};
					}
					if (${$ref_hash_infraspecieslist_IT}{"sub_type"} eq "substrain") {
						$assembly_NCBI_substrain_IT = ${$ref_hash_infraspecieslist_IT}{"sub_value"};
					}
					if (${$ref_hash_infraspecieslist_IT}{"sub_type"} eq "sub_strain") {
						$assembly_NCBI_substrain_IT = ${$ref_hash_infraspecieslist_IT}{"sub_value"};
					}
				}
				#      ],
				#      'isolate' => ''
				$assembly_NCBI_isolate_IT = ${ref_hash_root_structure}->{isolate};
				$assembly_NCBI_serotype_IT = ${ref_hash_root_structure}->{serovar};
				$assembly_NCBI_serotype_IT = ${ref_hash_root_structure}->{serotype};

				#'speciestaxid' => '1351',
				$assembly_NCBI_speciestaxid_IT = ${ref_hash_root_structure}->{speciestaxid};
				#'taxid' => '1206105', # = our taxon_id
				$assembly_NCBI_taxon_id_IT = ${ref_hash_root_structure}->{taxid};
			# links to bio_ids
				#'biosampleid' => '2603765',
				$assembly_NCBI_biosampleid_IT = ${ref_hash_root_structure}->{biosampleid};
				#'biosampleaccn' => 'SAMN02603765',
				$assembly_NCBI_biosampleaccn_IT = ${ref_hash_root_structure}->{biosampleaccn};
				#'gb_bioprojects' => [
				#                                                            {
				#                                                              'bioprojectid' => 169860,
				#                                                              'bioprojectaccn' => 'PRJNA169860'
				#                                                            }
				#                                                          ],
				my $ref_array_gb_bioprojects = ${ref_hash_root_structure}->{gb_bioprojects};
				foreach my $ref_hash_gb_bioprojects_IT ( @{ $ref_array_gb_bioprojects } ) {
					push (@assembly_NCBI_list_bioprojectid_IT, ${$ref_hash_gb_bioprojects_IT}{"bioprojectid"} );
					push (@assembly_NCBI_list_bioprojectaccn_IT, ${$ref_hash_gb_bioprojects_IT}{"bioprojectaccn"} );
				}
			# other info
				#'assemblyclass' => 'haploid',
				$assembly_NCBI_assemblyclass_IT = ${ref_hash_root_structure}->{assemblyclass};
				#'assemblystatus' => 'Complete Genome'
				$assembly_NCBI_assemblystatus_IT = ${ref_hash_root_structure}->{assemblystatus};
				last POTENTIAL_ASSEMBLY_NCBI_INTERNAL_ID; # if here then passed the version test, no need to continue loop
			} else {
				if ($try_fetch_with_curl >= 4) {
					#print LOG "Error esummary.out 1. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n";
					die_with_error_mssg("Error esummary.out 1. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n");
				} else {
					my $sleep_time = 10 * $try_fetch_with_curl * $try_fetch_with_curl * $try_fetch_with_curl;
					print LOG "( $counter_loop / $scalar_new_genomes ) : sleep $sleep_time second and try again ($try_fetch_with_curl times) : curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n" unless $VERBOSE =~ m/^OFF$/i;
					sleep($sleep_time);
					next TRY_FETCH_CURL_esummary1;
				}
				##print LOG "Error could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n";
				#die_with_error_mssg("Error could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n");
			}
		}
	}


	#get more info on accnum

	POTENTIAL_ACCNUM_NCBI_INTERNAL_ID: foreach my $potential_accnum_NCBI_internal_id_IT ( @list_potential_accnum_NCBI_internal_id ) {
	#if ($accnum_NCBI_internal_id_IT) {

		$try_fetch_with_curl = 0;
		TRY_FETCH_CURL_esummary2: while ($try_fetch_with_curl < 5) {
			$try_fetch_with_curl++;

			# download url
			$url_to_download_IT = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=nucleotide&id=${potential_accnum_NCBI_internal_id_IT}&retmode=json";
			$output_backtick = `rm -f ${DIR_TMP_DOWNLOAD}/esummary.out`;
			$output_backtick = `curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error`;
			if ($output_backtick eq "" && -e "${DIR_TMP_DOWNLOAD}/esummary.out") {
				#ok
				print LOG "( $counter_loop / $scalar_new_genomes ) : Done download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n" unless $VERBOSE =~ m/^OFF$/i;

				# parse json with perl
				my $json_text_esummary = "";
				{
				  local $/ = undef;
				  open FILE, "<${DIR_TMP_DOWNLOAD}/esummary.out" or die_with_error_mssg("Error. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nCouldn't open file ${DIR_TMP_DOWNLOAD}/esummary.out: $!");
				  binmode FILE;
				  $json_text_esummary = <FILE>;
				  close FILE;
				}
				my $json_esummary = JSON->new->pretty;
				my $json_object_esummary = "";
				try {
					$json_object_esummary = $json_esummary->decode($json_text_esummary);
				} catch {
					#print LOG "$_\n";
					if ($try_fetch_with_curl >= 4) {
						#print LOG "Error esummary.out 2 try catch. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n";
						die_with_error_mssg("Error esummary.out 2 try catch. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n");
					} else {
						my $sleep_time = 10 * $try_fetch_with_curl * $try_fetch_with_curl * $try_fetch_with_curl;
						print LOG "( $counter_loop / $scalar_new_genomes ) : sleep $sleep_time second and try again ($try_fetch_with_curl times) : curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n" unless $VERBOSE =~ m/^OFF$/i;
						sleep($sleep_time);
						next TRY_FETCH_CURL_esummary2;
					}
				};
				#print  Dumper($json_object_esummary);


			#fields of interest:
			#root structure $VAR1 = { 'result' => { '$accnum_NCBI_internal_id_IT' => {
				my $ref_hash_root_structure = $json_object_esummary->{result}->{${potential_accnum_NCBI_internal_id_IT}};
			#check we have the right version
				#"accessionversion": "CP001603.1"
				my $accessionversion_from_url =  ${ref_hash_root_structure}->{accessionversion};
				if ($accessionversion_from_url =~ m/\.(\d+)$/i) {
					my $version_from_url = $1;
					if ($version && $version_from_url && $version == $version_from_url) {
						# ok match
						#print "accnum $accnum_IT right version $version\n";
						$accnum_NCBI_internal_id_IT = $potential_accnum_NCBI_internal_id_IT;
					} else {
						# not the right match
						#print "accnum $accnum_IT not right version $version VS $version_from_url\n";
						next POTENTIAL_ACCNUM_NCBI_INTERNAL_ID;
					}
				} else {
					# no version detected
					if ( ! $version) {
						# ok match
						#print "accnum $accnum_IT right version $version\n";
						$accnum_NCBI_internal_id_IT = $potential_accnum_NCBI_internal_id_IT;
					} else {
						# not the right match
						#print "accnum $accnum_IT not right version $version VS [EMPTY]\n";
						next POTENTIAL_ACCNUM_NCBI_INTERNAL_ID;
					}
				}
				# count accnum_NCBI
				$accnum_NCBI_sourcedb_found++;
			# other info
				#'sourcedb' => 'refseq'
				$accnum_NCBI_sourcedb = ${ref_hash_root_structure}->{sourcedb};
				#'genome' => 'chromosome' # equal type
				$accnum_NCBI_genome = ${ref_hash_root_structure}->{genome};
				#'tech' => '',
				$accnum_NCBI_tech = ${ref_hash_root_structure}->{tech};
				#'geneticcode' => '11',
				$accnum_NCBI_geneticcode = ${ref_hash_root_structure}->{geneticcode};
				#'topology' => 'circular',
				$accnum_NCBI_topology = ${ref_hash_root_structure}->{topology};
				#'completeness' => 'complete',
				$accnum_NCBI_completeness = ${ref_hash_root_structure}->{completeness};
				#"status": "suppressed",
				$accnum_NCBI_status = ${ref_hash_root_structure}->{status};
				if ($accnum_NCBI_status eq "suppressed") {
					push ( @WARNING_accnum_suppressed, $accnum);
				}
				#"comment": "This record was removed by RefSeq staff. Please contact info@ncbi.nlm.nih.gov for further details.",
				my $accnum_NCBI_comment_pre = ${ref_hash_root_structure}->{comment};
				$accnum_NCBI_comment = substr($accnum_NCBI_comment_pre, 0, 2000);

				last POTENTIAL_ACCNUM_NCBI_INTERNAL_ID; # if here then passed the version test, no need to continue loop
			} else {

				if ($try_fetch_with_curl >= 4) {
					#print LOG "Error esummary.out 2. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n";
					die_with_error_mssg("Error esummary.out 2. Please manually delete uncomplete repository \$DATA_DIR/Task_add_entry/$accnum_IT if you wish to insert partial data.\nError could not complete download curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n");
				} else {
					my $sleep_time = 10 * $try_fetch_with_curl * $try_fetch_with_curl * $try_fetch_with_curl;
					print LOG "( $counter_loop / $scalar_new_genomes ) : sleep $sleep_time second and try again ($try_fetch_with_curl times) : curl \"$url_to_download_IT\" -o \"${DIR_TMP_DOWNLOAD}/esummary.out\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n" unless $VERBOSE =~ m/^OFF$/i;
					sleep($sleep_time);
					next TRY_FETCH_CURL_esummary2;
				}


			}

		}

	}

	if ( $assembly_NCBI_internal_id_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_internal_id_IT"} = $assembly_NCBI_internal_id_IT;
	}
	if ( $assembly_NCBI_assemblyaccession_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_assemblyaccession_IT"} = $assembly_NCBI_assemblyaccession_IT;
	}
	if ( $assembly_NCBI_assemblyname_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_assemblyname_IT"} = $assembly_NCBI_assemblyname_IT;
	}
	if ( $assembly_NCBI_lastupdatedate_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_lastupdatedate_IT"} = $assembly_NCBI_lastupdatedate_IT;
	}
	if ( $assembly_NCBI_seqreleasedate_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_seqreleasedate_IT"} = $assembly_NCBI_seqreleasedate_IT;
	}
	if ( $assembly_NCBI_species_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_species_IT"} = $assembly_NCBI_species_IT;
	}
	if ( $assembly_NCBI_strain_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_strain_IT"} = $assembly_NCBI_strain_IT;
	}
	if ( $assembly_NCBI_substrain_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_substrain_IT"} = $assembly_NCBI_substrain_IT;
	}
	if ( $assembly_NCBI_isolate_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_isolate_IT"} = $assembly_NCBI_isolate_IT;
	}
	if ( $assembly_NCBI_serotype_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_serotype_IT"} = $assembly_NCBI_serotype_IT;
	}
	if ( $assembly_NCBI_speciestaxid_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_speciestaxid_IT"} = $assembly_NCBI_speciestaxid_IT;
	}
	if ( $assembly_NCBI_taxon_id_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_taxon_id_IT"} = $assembly_NCBI_taxon_id_IT;
	}
	if ( $assembly_NCBI_biosampleid_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_biosampleid_IT"} = $assembly_NCBI_biosampleid_IT;
	}
	if ( $assembly_NCBI_biosampleaccn_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_biosampleaccn_IT"} = $assembly_NCBI_biosampleaccn_IT;
	}
	if ( @assembly_NCBI_list_bioprojectid_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_list_bioprojectid_IT"} = join (",",@assembly_NCBI_list_bioprojectid_IT);
	}
	if ( @assembly_NCBI_list_bioprojectaccn_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_list_bioprojectaccn_IT"} = join (",",@assembly_NCBI_list_bioprojectaccn_IT);
	}
	if ( $assembly_NCBI_assemblyclass_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_assemblyclass_IT"} = $assembly_NCBI_assemblyclass_IT;
	}
	if ( $assembly_NCBI_assemblystatus_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"assembly_NCBI_assemblystatus_IT"} = $assembly_NCBI_assemblystatus_IT;
	}

	if ( $accnum_NCBI_internal_id_IT ) {
		$ncbi_assembly_and_accession_info_from_url_request{"accnum_NCBI_internal_id_IT"} = $accnum_NCBI_internal_id_IT;
	}
	if ( $accnum_NCBI_sourcedb ) {
		$ncbi_assembly_and_accession_info_from_url_request{"accnum_NCBI_sourcedb"} = $accnum_NCBI_sourcedb;
	}
	if ( $accnum_NCBI_genome ) {
		$ncbi_assembly_and_accession_info_from_url_request{"accnum_NCBI_genome"} = $accnum_NCBI_genome;
	}
	if ( $accnum_NCBI_tech ) {
		$ncbi_assembly_and_accession_info_from_url_request{"accnum_NCBI_tech"} = $accnum_NCBI_tech;
	}
	if ( $accnum_NCBI_geneticcode ) {
		$ncbi_assembly_and_accession_info_from_url_request{"accnum_NCBI_geneticcode"} = $accnum_NCBI_geneticcode;
	}
	if ( $accnum_NCBI_topology ) {
		$ncbi_assembly_and_accession_info_from_url_request{"accnum_NCBI_topology"} = $accnum_NCBI_topology;
	}
	if ( $accnum_NCBI_completeness ) {
		$ncbi_assembly_and_accession_info_from_url_request{"accnum_NCBI_completeness"} = $accnum_NCBI_completeness;
	}
	if ( $accnum_NCBI_status ) {
		$ncbi_assembly_and_accession_info_from_url_request{"accnum_NCBI_status"} = $accnum_NCBI_status;
	}
	if ( $accnum_NCBI_comment ) {
		$ncbi_assembly_and_accession_info_from_url_request{"accnum_NCBI_comment"} = $accnum_NCBI_comment;
	}

	return \%ncbi_assembly_and_accession_info_from_url_request;
}

sub create_new_organism {

	my ( $accession, $species_to_store_in_db, $strain_to_store_in_db, $substrain_to_store_in_db, $taxon_id_to_store_in_db, $assembly_NCBI_internal_id_IT, $assembly_NCBI_assemblyaccession_IT, $assembly_NCBI_assemblyname_IT, $assembly_NCBI_lastupdatedate_IT, $assembly_NCBI_seqreleasedate_IT, $ncbi_isolate_it_to_store_in_db, $assembly_NCBI_speciestaxid_IT, $assembly_NCBI_biosampleid_IT, $assembly_NCBI_biosampleaccn_IT, $assembly_NCBI_list_bioprojectid_IT, $assembly_NCBI_list_bioprojectaccn_IT, $assembly_NCBI_assemblyclass_IT, $assembly_NCBI_assemblystatus_IT, $alt_strain_to_store_in_db, $ncbi_serotype_it_to_store_in_db ) = @_;

	my $organism_id = $next_free_organism_id_for_db;
	$next_free_organism_id_for_db++;

	#fill %species2ref_list_hash_organism_fields (organism_id, strain, substrain, taxon_id, ncbi_assemblyaccession, ncbi_isolate, serotype) and %assemblyaccession2last_organism_id
	# for use when group elements into organisms
	if ($assembly_NCBI_assemblyaccession_IT && $assembly_NCBI_assemblyaccession_IT ne "NULL" ) {
		# check if there is an organuism stored in db with similar $assembly_NCBI_assemblyaccession_IT
		$assemblyaccession2last_organism_id{$assembly_NCBI_assemblyaccession_IT} = $organism_id;
	}

	if ( exists $species2ref_list_hash_organism_fields{$species_to_store_in_db} ) {
		my $ref_list_hash_organism_fields = $species2ref_list_hash_organism_fields{$species_to_store_in_db};
		my %organism_fields = ();
		$organism_fields{"organism_id"} = $organism_id;
		if ($strain_to_store_in_db && $strain_to_store_in_db ne "NULL" ) {
			$organism_fields{"strain"} = $strain_to_store_in_db;
		}
		if ($substrain_to_store_in_db && $substrain_to_store_in_db ne "NULL" ) {
			$organism_fields{"substrain"} = $substrain_to_store_in_db;
		}
		if ($taxon_id_to_store_in_db && $taxon_id_to_store_in_db ne "NULL" ) {
			$organism_fields{"taxon_id"} = $taxon_id_to_store_in_db;
		}
		if ($assembly_NCBI_assemblyaccession_IT && $assembly_NCBI_assemblyaccession_IT ne "NULL" ) {
			$organism_fields{"ncbi_assemblyaccession"} = $assembly_NCBI_assemblyaccession_IT;
		}
		if ($ncbi_isolate_it_to_store_in_db && $ncbi_isolate_it_to_store_in_db ne "NULL" ) {
			$organism_fields{"ncbi_isolate"} = $ncbi_isolate_it_to_store_in_db;
		}
		if ($ncbi_serotype_it_to_store_in_db && $ncbi_serotype_it_to_store_in_db ne "NULL" ) {
			$organism_fields{"serotype"} = $ncbi_serotype_it_to_store_in_db;
		}
		push ( @{$ref_list_hash_organism_fields}, \%organism_fields );
	} else {
		my @list_hash_organism_fields = ();
		my %organism_fields = ();
		$organism_fields{"organism_id"} = $organism_id;
		if ($strain_to_store_in_db && $strain_to_store_in_db ne "NULL" ) {
			$organism_fields{"strain"} = $strain_to_store_in_db;
		}
		if ($substrain_to_store_in_db && $substrain_to_store_in_db ne "NULL" ) {
			$organism_fields{"substrain"} = $substrain_to_store_in_db;
		}
		if ($taxon_id_to_store_in_db && $taxon_id_to_store_in_db ne "NULL" ) {
			$organism_fields{"taxon_id"} = $taxon_id_to_store_in_db;
		}
		if ($assembly_NCBI_assemblyaccession_IT && $assembly_NCBI_assemblyaccession_IT ne "NULL" ) {
			$organism_fields{"ncbi_assemblyaccession"} = $assembly_NCBI_assemblyaccession_IT;
		}
		if ($ncbi_isolate_it_to_store_in_db && $ncbi_isolate_it_to_store_in_db ne "NULL" ) {
			$organism_fields{"ncbi_isolate"} = $ncbi_isolate_it_to_store_in_db;
		}
		if ($ncbi_serotype_it_to_store_in_db && $ncbi_serotype_it_to_store_in_db ne "NULL" ) {
			$organism_fields{"serotype"} = $ncbi_serotype_it_to_store_in_db;
		}
		push (@list_hash_organism_fields, \%organism_fields) ;
		$species2ref_list_hash_organism_fields{$species_to_store_in_db} = \@list_hash_organism_fields;
	}

	#"COPY organisms (organism_id, species, strain, substrain, taxon_id, ispublic, ncbi_internal_id_it, ncbi_assemblyaccession_it, ncbi_assemblyname_it, ncbi_lastupdatedate_it, ncbi_seqreleasedate_it, ncbi_isolate_it, ncbi_speciestaxid_it, ncbi_biosampleid_it, ncbi_biosampleaccn_it, ncbi_list_bioprojectid_it, ncbi_list_bioprojectaccn_it, ncbi_assemblyclass_it, ncbi_assemblystatus_it, alt_strain, serotype) FROM stdin WITH NULL AS 'NULL';\n";
	$organism_id = string_encod($organism_id);
	$species_to_store_in_db = string_encod($species_to_store_in_db);
	$strain_to_store_in_db = string_encod($strain_to_store_in_db);
	$substrain_to_store_in_db = string_encod($substrain_to_store_in_db);
	$taxon_id_to_store_in_db = string_encod($taxon_id_to_store_in_db);
	$assembly_NCBI_internal_id_IT = string_encod($assembly_NCBI_internal_id_IT);
	$assembly_NCBI_assemblyaccession_IT = string_encod($assembly_NCBI_assemblyaccession_IT);
	$assembly_NCBI_assemblyname_IT = string_encod($assembly_NCBI_assemblyname_IT);
	$assembly_NCBI_lastupdatedate_IT = string_encod($assembly_NCBI_lastupdatedate_IT);
	$assembly_NCBI_seqreleasedate_IT = string_encod($assembly_NCBI_seqreleasedate_IT);
	$ncbi_isolate_it_to_store_in_db = string_encod($ncbi_isolate_it_to_store_in_db);
	$assembly_NCBI_speciestaxid_IT = string_encod($assembly_NCBI_speciestaxid_IT);
	$assembly_NCBI_biosampleid_IT = string_encod($assembly_NCBI_biosampleid_IT);
	$assembly_NCBI_biosampleaccn_IT = string_encod($assembly_NCBI_biosampleaccn_IT);
	$assembly_NCBI_list_bioprojectid_IT = string_encod($assembly_NCBI_list_bioprojectid_IT);
	$assembly_NCBI_list_bioprojectaccn_IT = string_encod($assembly_NCBI_list_bioprojectaccn_IT);
	$assembly_NCBI_assemblyclass_IT = string_encod($assembly_NCBI_assemblyclass_IT);
	$assembly_NCBI_assemblystatus_IT = string_encod($assembly_NCBI_assemblystatus_IT);
	$alt_strain_to_store_in_db = string_encod($alt_strain_to_store_in_db);
	$ncbi_serotype_it_to_store_in_db = string_encod($ncbi_serotype_it_to_store_in_db);
	print ORGANISMS "$organism_id\t$species_to_store_in_db\t$strain_to_store_in_db\t$substrain_to_store_in_db\t$taxon_id_to_store_in_db\tTRUE\t$assembly_NCBI_internal_id_IT\t$assembly_NCBI_assemblyaccession_IT\t$assembly_NCBI_assemblyname_IT\t$assembly_NCBI_lastupdatedate_IT\t$assembly_NCBI_seqreleasedate_IT\t$ncbi_isolate_it_to_store_in_db\t$assembly_NCBI_speciestaxid_IT\t$assembly_NCBI_biosampleid_IT\t$assembly_NCBI_biosampleaccn_IT\t$assembly_NCBI_list_bioprojectid_IT\t$assembly_NCBI_list_bioprojectaccn_IT\t$assembly_NCBI_assemblyclass_IT\t$assembly_NCBI_assemblystatus_IT\t$alt_strain_to_store_in_db\t$ncbi_serotype_it_to_store_in_db\tTRUE\n";
	$count_create_new_orga++;
	return $organism_id;

}


sub group_with_already_present_or_create_new_organism {

	my ($accession, $species_to_store_in_db, $strain_to_store_in_db, $substrain_to_store_in_db, $taxon_id_to_store_in_db, $assembly_NCBI_internal_id_IT, $assembly_NCBI_assemblyaccession_IT, $assembly_NCBI_assemblyname_IT, $assembly_NCBI_lastupdatedate_IT, $assembly_NCBI_seqreleasedate_IT, $ncbi_isolate_it_to_store_in_db, $assembly_NCBI_speciestaxid_IT, $assembly_NCBI_biosampleid_IT, $assembly_NCBI_biosampleaccn_IT, $assembly_NCBI_list_bioprojectid_IT, $assembly_NCBI_list_bioprojectaccn_IT, $assembly_NCBI_assemblyclass_IT, $assembly_NCBI_assemblystatus_IT, $alt_strain_to_store_in_db, $ncbi_serotype_it_to_store_in_db, $type_element ) = @_;

	#Rq : above var are eq "NULL" if undef
	# check if there is an organuism stored in db with similar $assembly_NCBI_assemblyaccession_IT
	if ( $assembly_NCBI_assemblyaccession_IT && $assembly_NCBI_assemblyaccession_IT ne "NULL" && exists $assemblyaccession2last_organism_id{$assembly_NCBI_assemblyaccession_IT} ) {
		# ok found our organism already stored in db
		$count_group_accnum_in_existing_orga_because_similar_assemblyaccession++;
		#print "group accnum $accession in existing orga ".$assemblyaccession2last_organism_id{$assembly_NCBI_assemblyaccession_IT}." because similar assemblyaccession ".$assembly_NCBI_assemblyaccession_IT."\n";
		return $assemblyaccession2last_organism_id{$assembly_NCBI_assemblyaccession_IT};
	} elsif ( exists $species2ref_list_hash_organism_fields{$species_to_store_in_db} ) {
		#TODO NOT PRIORITY check for compatibility between type element, to not have 2 complete genome as same species ??
		#TODO NOT PRIORITY check for compatibility between version, to not have 2 genomes with differetn version ??
		my $ref_list_hash_organism_fields = $species2ref_list_hash_organism_fields{$species_to_store_in_db};
		foreach my $ref_hash_organism_fields ( @{$ref_list_hash_organism_fields} ) {
			#%species2ref_list_hash_organism_fields -> hash_organism_fields {organism_id} {strain} {substrain} {taxon_id} {ncbi_assemblyaccession} {ncbi_isolate} {serotype}
			my $strain_from_stored_specise = "NULL";
			if ( exists ${$ref_hash_organism_fields}{"strain"} ) {
				$strain_from_stored_specise = ${$ref_hash_organism_fields}{"strain"};
			}
			my $substrain_from_stored_specise = "NULL";
			if ( exists ${$ref_hash_organism_fields}{"substrain"} ) {
				$substrain_from_stored_specise = ${$ref_hash_organism_fields}{"substrain"};
			}
			my $taxon_id_from_stored_specise = "NULL";
			if ( exists ${$ref_hash_organism_fields}{"taxon_id"} ) {
				$taxon_id_from_stored_specise = ${$ref_hash_organism_fields}{"taxon_id"};
			}
			my $ncbi_isolate_from_stored_specise = "NULL";
			if ( exists ${$ref_hash_organism_fields}{"ncbi_isolate"} ) {
				$ncbi_isolate_from_stored_specise = ${$ref_hash_organism_fields}{"ncbi_isolate"};
			}
			my $serotype_from_stored_specise = "NULL";
			if ( exists ${$ref_hash_organism_fields}{"serotype"} ) {
				$serotype_from_stored_specise = ${$ref_hash_organism_fields}{"serotype"};
			}
			if ($strain_from_stored_specise eq $strain_to_store_in_db
				&& $substrain_from_stored_specise eq $substrain_to_store_in_db
				&& $taxon_id_from_stored_specise eq $taxon_id_to_store_in_db
				&& $ncbi_isolate_from_stored_specise eq $ncbi_isolate_it_to_store_in_db
				&& $serotype_from_stored_specise eq $ncbi_serotype_it_to_store_in_db
			) {
				# ok found our organism already stored in db
				$count_group_accnum_in_existing_orga_because_similar_organism_fields++;
				#print "group accnum $accession in existing orga ".${$ref_hash_organism_fields}{"organism_id"}." because similar organism fields\n";
				return ${$ref_hash_organism_fields}{"organism_id"};
			}
		}
		# no match found, create new orga
		my $orga_id_to_return = create_new_organism ($accession, $species_to_store_in_db, $strain_to_store_in_db, $substrain_to_store_in_db, $taxon_id_to_store_in_db, $assembly_NCBI_internal_id_IT, $assembly_NCBI_assemblyaccession_IT, $assembly_NCBI_assemblyname_IT, $assembly_NCBI_lastupdatedate_IT, $assembly_NCBI_seqreleasedate_IT, $ncbi_isolate_it_to_store_in_db, $assembly_NCBI_speciestaxid_IT, $assembly_NCBI_biosampleid_IT, $assembly_NCBI_biosampleaccn_IT, $assembly_NCBI_list_bioprojectid_IT, $assembly_NCBI_list_bioprojectaccn_IT, $assembly_NCBI_assemblyclass_IT, $assembly_NCBI_assemblystatus_IT, $alt_strain_to_store_in_db, $ncbi_serotype_it_to_store_in_db);
		$count_create_new_orga_because_similar_species_but_no_similar_organism_fields++;
		#print "create new orga $orga_id_to_return for accnum $accession because similar species found but no similar organism fields\n";
		return $orga_id_to_return;
	} else {
		my $orga_id_to_return = create_new_organism ($accession, $species_to_store_in_db, $strain_to_store_in_db, $substrain_to_store_in_db, $taxon_id_to_store_in_db, $assembly_NCBI_internal_id_IT, $assembly_NCBI_assemblyaccession_IT, $assembly_NCBI_assemblyname_IT, $assembly_NCBI_lastupdatedate_IT, $assembly_NCBI_seqreleasedate_IT, $ncbi_isolate_it_to_store_in_db, $assembly_NCBI_speciestaxid_IT, $assembly_NCBI_biosampleid_IT, $assembly_NCBI_biosampleaccn_IT, $assembly_NCBI_list_bioprojectid_IT, $assembly_NCBI_list_bioprojectaccn_IT, $assembly_NCBI_assemblyclass_IT, $assembly_NCBI_assemblystatus_IT, $alt_strain_to_store_in_db, $ncbi_serotype_it_to_store_in_db);
		$count_create_new_orga_because_no_similar_species++;
		#print "create new orga $orga_id_to_return for accnum $accession because no similar species found\n";
		return $orga_id_to_return;
	}
	

}


sub insert_organisms_into_sql_files {

	my ($accession, $ref_stored_info_from_features, $ref_stored_info_from_sequences, $type_element, $ref_hash_ncbi_assembly_and_accession_info_from_url_request ) = @_;

	# $ref_stored_info_from_features {organism} {subspecies} {strain} {substrain} {species} {isolate} {serotype}
	my $organism_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_features}{"organism"} ) {
		$organism_from_qualifiers = ${$ref_stored_info_from_features}{"organism"};
	}
	my $subspecies_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_features}{"subspecies"} ) {
		$subspecies_from_qualifiers = ${$ref_stored_info_from_features}{"subspecies"};
	}
	my $strain_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_features}{"strain"} ) {
		$strain_from_qualifiers = ${$ref_stored_info_from_features}{"strain"};
	}
	my $substrain_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_features}{"substrain"} ) {
		$substrain_from_qualifiers = ${$ref_stored_info_from_features}{"substrain"};
	}
	my $species_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_features}{"species"} ) {
		$species_from_qualifiers = ${$ref_stored_info_from_features}{"species"};
	}
	my $isolate_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_features}{"isolate"} ) {
		$isolate_from_qualifiers = ${$ref_stored_info_from_features}{"isolate"};
	}
	my $serotype_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_features}{"serotype"} ) {
		$serotype_from_qualifiers = ${$ref_stored_info_from_features}{"serotype"};
	}
	# $ref_stored_info_from_sequences {taxon_id} {version}
	my $taxon_id_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_sequences}{"taxon_id"} ) {
		$taxon_id_from_qualifiers = ${$ref_stored_info_from_sequences}{"taxon_id"};
	}
	my $version_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_sequences}{"version"} ) {
		$version_from_qualifiers = ${$ref_stored_info_from_sequences}{"version"};
	}

	my $assembly_NCBI_internal_id_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_internal_id_IT"}) {
		$assembly_NCBI_internal_id_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_internal_id_IT"};
	}
	my $assembly_NCBI_assemblyaccession_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_assemblyaccession_IT"}) {
		$assembly_NCBI_assemblyaccession_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_assemblyaccession_IT"};
	}
	my $assembly_NCBI_assemblyname_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_assemblyname_IT"}) {
		$assembly_NCBI_assemblyname_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_assemblyname_IT"};
	}
	my $assembly_NCBI_lastupdatedate_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_lastupdatedate_IT"}) {
		$assembly_NCBI_lastupdatedate_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_lastupdatedate_IT"};
	}
	my $assembly_NCBI_seqreleasedate_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_seqreleasedate_IT"}) {
		$assembly_NCBI_seqreleasedate_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_seqreleasedate_IT"};
	}
	my $assembly_NCBI_species_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_species_IT"}) {
		$assembly_NCBI_species_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_species_IT"};
	}
	my $assembly_NCBI_strain_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_strain_IT"}) {
		$assembly_NCBI_strain_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_strain_IT"};
	}
	my $assembly_NCBI_substrain_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_substrain_IT"}) {
		$assembly_NCBI_substrain_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_substrain_IT"};
	}
	my $assembly_NCBI_isolate_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_isolate_IT"}) {
		$assembly_NCBI_isolate_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_isolate_IT"};
	}
	my $assembly_NCBI_serotype_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_serotype_IT"}) {
		$assembly_NCBI_serotype_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_serotype_IT"};
	}
	my $assembly_NCBI_speciestaxid_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_speciestaxid_IT"}) {
		$assembly_NCBI_speciestaxid_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_speciestaxid_IT"};
	}
	my $assembly_NCBI_taxon_id_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_taxon_id_IT"}) {
		$assembly_NCBI_taxon_id_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_taxon_id_IT"};
	}
	my $assembly_NCBI_biosampleid_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_biosampleid_IT"}) {
		$assembly_NCBI_biosampleid_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_biosampleid_IT"};
	}
	my $assembly_NCBI_biosampleaccn_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_biosampleaccn_IT"}) {
		$assembly_NCBI_biosampleaccn_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_biosampleaccn_IT"};
	}
	my $assembly_NCBI_list_bioprojectid_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_list_bioprojectid_IT"}) {
		$assembly_NCBI_list_bioprojectid_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_list_bioprojectid_IT"};
	}
	my $assembly_NCBI_list_bioprojectaccn_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_list_bioprojectaccn_IT"}) {
		$assembly_NCBI_list_bioprojectaccn_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_list_bioprojectaccn_IT"};
	}
	my $assembly_NCBI_assemblyclass_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_assemblyclass_IT"}) {
		$assembly_NCBI_assemblyclass_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_assemblyclass_IT"};
	}
	my $assembly_NCBI_assemblystatus_IT = "NULL";
	if ( defined ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_assemblystatus_IT"}) {
		$assembly_NCBI_assemblystatus_IT = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"assembly_NCBI_assemblystatus_IT"};
	}

=pod
**correspondance
db fields | data_from_file | data_from_ncbi_assembly
species | $species_from_qualifiers IF NOT $organism_from_qualifiers | assembly_NCBI_species_IT
strain | $strain_from_qualifiers | assembly_NCBI_strain_IT
substrain | $substrain_from_qualifiers | assembly_NCBI_substrain_IT
taxon_id | $taxon_id_from_qualifiers | assembly_NCBI_taxon_id_IT
ncbi_isolate_it | $isolate_from_qualifiers | assembly_NCBI_isolate_IT
RQ : when data from data_from_ncbi_assembly are available, take precedence over data_from_file (still stored as qualifiers anyway)
=cut

#species | $species_from_qualifiers IF NOT $organism_from_qualifiers | assembly_NCBI_species_IT
	my $species_to_store_in_db = "NULL";
	if ( $assembly_NCBI_species_IT && $assembly_NCBI_species_IT ne "NULL" ) {
		$species_to_store_in_db = $assembly_NCBI_species_IT;
	} elsif ( $species_from_qualifiers && $species_from_qualifiers ne "NULL" ) {
		$species_to_store_in_db = $species_from_qualifiers;
	} elsif ( $organism_from_qualifiers && $organism_from_qualifiers ne "NULL" ) {
		$species_to_store_in_db = $organism_from_qualifiers;
	}
#strain | $strain_from_qualifiers | assembly_NCBI_strain_IT
	my $strain_to_store_in_db = "NULL";
	my $alt_strain_to_store_in_db = "NULL";
	if ( $assembly_NCBI_strain_IT && $assembly_NCBI_strain_IT ne "NULL" ) {
		$strain_to_store_in_db = $assembly_NCBI_strain_IT;
		if ( $strain_from_qualifiers && $strain_from_qualifiers ne "NULL" && $strain_from_qualifiers ne $assembly_NCBI_strain_IT) {
			$alt_strain_to_store_in_db = $strain_from_qualifiers;
		}
	} elsif ( $strain_from_qualifiers && $strain_from_qualifiers ne "NULL"  ) {
		$strain_to_store_in_db = $strain_from_qualifiers;
	}
#substrain | $substrain_from_qualifiers | assembly_NCBI_substrain_IT
	my $substrain_to_store_in_db = "NULL";
	if ( $assembly_NCBI_substrain_IT && $assembly_NCBI_substrain_IT ne "NULL" ) {
		$substrain_to_store_in_db = $assembly_NCBI_substrain_IT;
	} elsif ( $substrain_from_qualifiers && $substrain_from_qualifiers ne "NULL"  ) {
		$substrain_to_store_in_db = $substrain_from_qualifiers;
	}
#taxon_id | $taxon_id_from_qualifiers | assembly_NCBI_taxon_id_IT
	my $taxon_id_to_store_in_db = "NULL";
	if ( $assembly_NCBI_taxon_id_IT && $assembly_NCBI_taxon_id_IT ne "NULL" ) {
		$taxon_id_to_store_in_db = $assembly_NCBI_taxon_id_IT;
	} elsif ( $taxon_id_from_qualifiers && $taxon_id_from_qualifiers ne "NULL"  ) {
		$taxon_id_to_store_in_db = $taxon_id_from_qualifiers;
	}
#ncbi_isolate_it | $isolate_from_qualifiers | assembly_NCBI_isolate_IT
	my $ncbi_isolate_it_to_store_in_db = "NULL";
	if ( $assembly_NCBI_isolate_IT && $assembly_NCBI_isolate_IT ne "NULL" ) {
		$ncbi_isolate_it_to_store_in_db = $assembly_NCBI_isolate_IT;
	} elsif ( $isolate_from_qualifiers && $isolate_from_qualifiers ne "NULL"  ) {
		$ncbi_isolate_it_to_store_in_db = $isolate_from_qualifiers;
	}
# serotype
	my $ncbi_serotype_it_to_store_in_db = "NULL";
	if ( $assembly_NCBI_serotype_IT && $assembly_NCBI_serotype_IT ne "NULL" ) {
		$ncbi_serotype_it_to_store_in_db = $assembly_NCBI_serotype_IT;
	} elsif ( $serotype_from_qualifiers && $serotype_from_qualifiers ne "NULL"  ) {
		$ncbi_serotype_it_to_store_in_db = $serotype_from_qualifiers;
	}


	my $organism_id = -1;
	if ( $FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES =~ m/^ON$/ ) {
		$organism_id = create_new_organism($accession, $species_to_store_in_db, $strain_to_store_in_db, $substrain_to_store_in_db, $taxon_id_to_store_in_db, $assembly_NCBI_internal_id_IT, $assembly_NCBI_assemblyaccession_IT, $assembly_NCBI_assemblyname_IT, $assembly_NCBI_lastupdatedate_IT, $assembly_NCBI_seqreleasedate_IT, $ncbi_isolate_it_to_store_in_db, $assembly_NCBI_speciestaxid_IT, $assembly_NCBI_biosampleid_IT, $assembly_NCBI_biosampleaccn_IT, $assembly_NCBI_list_bioprojectid_IT, $assembly_NCBI_list_bioprojectaccn_IT, $assembly_NCBI_assemblyclass_IT, $assembly_NCBI_assemblystatus_IT, $alt_strain_to_store_in_db, $ncbi_serotype_it_to_store_in_db);
	} else {
		# try to group accnum by organism
		$organism_id = group_with_already_present_or_create_new_organism($accession, $species_to_store_in_db, $strain_to_store_in_db, $substrain_to_store_in_db, $taxon_id_to_store_in_db, $assembly_NCBI_internal_id_IT, $assembly_NCBI_assemblyaccession_IT, $assembly_NCBI_assemblyname_IT, $assembly_NCBI_lastupdatedate_IT, $assembly_NCBI_seqreleasedate_IT, $ncbi_isolate_it_to_store_in_db, $assembly_NCBI_speciestaxid_IT, $assembly_NCBI_biosampleid_IT, $assembly_NCBI_biosampleaccn_IT, $assembly_NCBI_list_bioprojectid_IT, $assembly_NCBI_list_bioprojectaccn_IT, $assembly_NCBI_assemblyclass_IT, $assembly_NCBI_assemblystatus_IT, $alt_strain_to_store_in_db, $ncbi_serotype_it_to_store_in_db, $type_element);
	}
	return $organism_id;
=pod
	my %organism_id_and_extra_accession_info_to_return = (); # %organism_id_and_extra_accession_info_to_return = organism_id, accnum_NCBI_internal_id_IT, accnum_NCBI_sourcedb, accnum_NCBI_genome, accnum_NCBI_tech, accnum_NCBI_geneticcode, accnum_NCBI_topology, accnum_NCBI_completeness, accnum_NCBI_status, accnum_NCBI_comment
	$organism_id_and_extra_accession_info_to_return{"organism_id"} = $organism_id;
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_internal_id_IT"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_internal_id_IT"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_internal_id_IT"};
	}
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_sourcedb"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_sourcedb"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_sourcedb"};
	}
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_genome"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_genome"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_genome"};
	}
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_tech"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_tech"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_tech"};
	}
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_geneticcode"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_geneticcode"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_geneticcode"};
	}
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_geneticcode"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_geneticcode"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_geneticcode"};
	}
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_topology"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_topology"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_topology"};
	}
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_completeness"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_completeness"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_completeness"};
	}
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_status"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_status"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_status"};
	}
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_comment"} ) {
		$organism_id_and_extra_accession_info_to_return{"accnum_NCBI_comment"} = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_comment"};
	}
	return \%organism_id_and_extra_accession_info_to_return;
=cut

}



sub suggest_new_type_to_element {
	my($new_type, $reason, $accession, $genome_file) = @_;
	print LOG "type = $new_type for accession $accession ($genome_file) $reason\n" unless ( $PRINT_SUGGESTION_ON_SCREEN =~ m/^OFF$/i );
}


sub get_type_of_element {
	my ($accession, $genome_file, $description, $chromosome_from_features, $plasmid_from_features, $keywords, $accnum_NCBI_genome) = @_;
	my %type_to_return = ();
	my $type_to_test_IT;

	if ($accnum_NCBI_genome) {
		suggest_new_type_to_element($accnum_NCBI_genome, "because of the NCBI_genome tag found in the related fetched url", $accession, $genome_file);
		return $accnum_NCBI_genome;
	}

	$type_to_test_IT = "genomic_region";
	if ($accession =~ /_REGION_/i) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a the keyword region in the accession number $accession", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}

	$type_to_test_IT = "chromosome";
	if ( $description =~ /\bchromosome\b/i
		|| $description =~ /\bchromosomal\s+sequence\b/i 
		|| $description =~ /\breplicon\b/i 
	) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}
	if ($chromosome_from_features) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a qualifier chromosome in the source feature", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}

	$type_to_test_IT = "plasmid";
	if ($description =~ /plasmid/i
		|| $description =~ /\bpasmid\b/i
	) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}
	if ($plasmid_from_features) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a qualifier plasmid in the source feature", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}

	$type_to_test_IT = "draft";
	if ($description =~ /\bdraft\b/i
	|| $description =~ /\bprovisional\s+genome\b/i) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}

	$type_to_test_IT = "synthetic";
	if ($description =~ /\bsynthetic\b/i
	|| $description =~ /\bintegrating\s+conjugative\s+element\b/i
	|| $description =~ /\btransgenic\s+clone\b/i
	) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}

	$type_to_test_IT = "transfer_element";
	if ($description =~ /\bpathogenicity\s+island\b/i
	|| $description =~ /\bincision\s+element\b/i
	|| $description =~ /\bintegrative\s+conjugative\s+element\b/i) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}

	$type_to_test_IT = "satellite";
	if ($description =~ /satellite\b/i) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}

	$type_to_test_IT = "supercontig";
	if ($description =~ /supercontig/i
		|| $description =~ /supercont/i
	) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	} else {
		$type_to_test_IT = "contig";
		if ($description =~ /contig/i
		) {
			suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
			$type_to_return{$type_to_test_IT} = undef;
		}
	}

	$type_to_test_IT = "scaffold";
	if ($description =~ /scaffold/i) {
		suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
		$type_to_return{$type_to_test_IT} = undef;
	}

	if ( ! %type_to_return ) {
		$type_to_test_IT = "complete_genome";
		if ( $description =~ /\bcomplete\s+genome\b/i ) {
			suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the description $description", $accession, $genome_file);
			$type_to_return{$type_to_test_IT} = undef;
		}
		if ( $keywords =~ /\bcomplete\s+genome\b/i ) {
			suggest_new_type_to_element($type_to_test_IT, "because of the existence of a keyword in the keywords section $keywords", $accession, $genome_file);
			$type_to_return{$type_to_test_IT} = undef;
		}
	}

	my $single_type_to_return = undef;
	if ( ! %type_to_return ) {
		suggest_new_type_to_element("undef", "because no matches were found", $accession, $genome_file);
		$single_type_to_return = "undef";# need undef for prokka output
	} else {
		my $concat_IT = join("_", keys %type_to_return );
		suggest_new_type_to_element($concat_IT, "after concatenation of all the matches", $accession, $genome_file);
		$single_type_to_return = $concat_IT;
	}
	return $single_type_to_return;
}


sub insert_elements_into_sql_files {

	my ( $accession, $ref_stored_info_from_sequences, $organism_id, $type_element, $ref_hash_ncbi_assembly_and_accession_info_from_url_request ) = @_;

	# $ref_hash_ncbi_assembly_and_accession_info_from_url_request = organism_id, accnum_NCBI_internal_id_IT, accnum_NCBI_sourcedb, accnum_NCBI_genome, accnum_NCBI_tech, accnum_NCBI_geneticcode, accnum_NCBI_topology, accnum_NCBI_completeness, accnum_NCBI_status, accnum_NCBI_comment

	# %ref_stored_info_from_sequences {def} {length}
	my $length_from_sequences = "NULL";
	if ( exists ${$ref_stored_info_from_sequences}{"length"} ) {
		$length_from_sequences = ${$ref_stored_info_from_sequences}{"length"};
	}
	my $accnum_NCBI_internal_id = "NULL";
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_internal_id_IT"} ) {
		$accnum_NCBI_internal_id = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_internal_id_IT"};
	}
	my $accnum_NCBI_sourcedb = "NULL";
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_sourcedb"} ) {
		$accnum_NCBI_sourcedb = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_sourcedb"};
	}
	my $accnum_NCBI_tech = "NULL";
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_tech"} ) {
		$accnum_NCBI_tech = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_tech"};
	}
	my $accnum_NCBI_geneticcode = "NULL";
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_geneticcode"} ) {
		$accnum_NCBI_geneticcode = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_geneticcode"};
	}
	my $accnum_NCBI_topology = "NULL";
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_topology"} ) {
		$accnum_NCBI_topology = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_topology"};
	}
	my $accnum_NCBI_completeness = "NULL";
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_completeness"} ) {
		$accnum_NCBI_completeness = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_completeness"};
	}
	my $accnum_NCBI_status = "NULL";
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_status"} ) {
		$accnum_NCBI_status = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_status"};
	}
	my $accnum_NCBI_comment = "NULL";
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_comment"} ) {
		$accnum_NCBI_comment = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_comment"};
	}

	my $element_id = $next_free_element_id_for_db;
	$next_free_element_id_for_db++;
	$accession2element_id{$accession} = $element_id;
	#"COPY elements (element_id, organism_id, type, size, accession, mdate, ncbi_internal_id, ncbi_sourcedb, ncbi_tech, ncbi_geneticcode, ncbi_topology, ncbi_completeness, ncbi_status, ncbi_comment) FROM stdin WITH NULL AS 'NULL';\n";
	$element_id = string_encod($element_id);
	$organism_id = string_encod($organism_id);
	$type_element = string_encod($type_element);
	$length_from_sequences = string_encod($length_from_sequences);
	$accession = string_encod($accession);
	$accnum_NCBI_internal_id = string_encod($accnum_NCBI_internal_id);
	$accnum_NCBI_sourcedb = string_encod($accnum_NCBI_sourcedb);
	$accnum_NCBI_tech = string_encod($accnum_NCBI_tech);
	$accnum_NCBI_geneticcode = string_encod($accnum_NCBI_geneticcode);
	$accnum_NCBI_topology = string_encod($accnum_NCBI_topology);
	$accnum_NCBI_completeness = string_encod($accnum_NCBI_completeness);
	$accnum_NCBI_status = string_encod($accnum_NCBI_status);
	$accnum_NCBI_comment = string_encod($accnum_NCBI_comment);
	print ELEMENTS "$element_id\t$organism_id\t$type_element\t$length_from_sequences\t$accession\tnow()\t$accnum_NCBI_internal_id\t$accnum_NCBI_sourcedb\t$accnum_NCBI_tech\t$accnum_NCBI_geneticcode\t$accnum_NCBI_topology\t$accnum_NCBI_completeness\t$accnum_NCBI_status\t$accnum_NCBI_comment\n";

	return $element_id;
}


sub insert_genes_into_sql_files {

	my ( $accession, $organism_id, $element_id, $ref_hash_gene_id2gene_fields) = @_;

	foreach my $gene_id_IT ( keys %{$ref_hash_gene_id2gene_fields} ) {
		my $ref_hash_gene_fields = ${$ref_hash_gene_id2gene_fields}{$gene_id_IT};
		my $name_IT = ${$ref_hash_gene_fields}{"name"};
		my $strand_IT = ${$ref_hash_gene_fields}{"strand"};
		my $start_IT = ${$ref_hash_gene_fields}{"start"};
		my $stop_IT = ${$ref_hash_gene_fields}{"stop"};
		my $locus_tag_IT = ${$ref_hash_gene_fields}{"locus_tag"};
		my $code_feat_IT = ${$ref_hash_gene_fields}{"code_feat"};
		my $is_pseudo_IT = ${$ref_hash_gene_fields}{"is_pseudo"};
		my $length_residues_IT = ${$ref_hash_gene_fields}{"length_residues"};

		#"COPY genes (gene_id, organism_id, element_id, name, strand, start, stop, feature_id, locus_tag, accession) FROM stdin WITH NULL AS 'NULL';\n";
		$gene_id_IT = string_encod($gene_id_IT);
		$organism_id = string_encod($organism_id);
		$element_id = string_encod($element_id);
		$name_IT = string_encod($name_IT);
		$strand_IT = string_encod($strand_IT);
		$start_IT = string_encod($start_IT);
		$stop_IT = string_encod($stop_IT);
		$code_feat_IT = string_encod($code_feat_IT);
		$locus_tag_IT = string_encod($locus_tag_IT);
		$accession = string_encod($accession);
		$is_pseudo_IT = string_encod($is_pseudo_IT);
		$length_residues_IT = string_encod($length_residues_IT);
		print GENES "$gene_id_IT\t$organism_id\t$element_id\t$name_IT\t$strand_IT\t$start_IT\t$stop_IT\t$code_feat_IT\t$locus_tag_IT\t$accession\t$is_pseudo_IT\t$length_residues_IT\n";
	}
}

sub parse_seq_into_sql_files {
  	my ( $seq, $accession, $genome_file, $counter_loop, $scalar_new_genomes ) = @_;
	# *ACCESS* *ARTICLES* *COMMENTS* *DNA_LOC* *DNA_SEQ* *FEATURES* *KEYWORDS* *LOCATIONS* *PROT_FEAT* *QUALIFIERS* *SEQUENCES* ORGANISMS ELEMENTS GENES
 	my $ref_stored_info_from_sequences = insert_sequences_into_sql_files($seq, $accession, $genome_file ); #SEQUENCES + store info for ELEMENTS %ref_stored_info_from_sequences {def} {length} {taxon_id} {version}
	insert_accessions_into_sql_files($seq, $accession);#ACCESS
 	insert_comments_into_sql_files($seq, $accession);#COMMENTS
 	my $keywords = insert_keywords_into_sql_files($seq, $accession);#KEYWORDS
	insert_references_into_sql_files($seq, $accession);#ARTICLES
	insert_dnaseq_into_sql_files($seq, $accession);#DNA_SEQ
 	my $ref_stored_info_from_features = insert_features_into_sql_files($seq, $accession); #FEATURES LOCATIONS DNA_LOC QUALIFIERS + store info for ELEMENTS and GENES %stored_info_from_features {chromosome} {plasmid} {ref_hash_gene_id2gene_fields} (= name, strand, start, stop, locus_tag, code_feat) {organism} {subspecies} {strain} {substrain} {species}
	my $def_from_sequences = undef;
	if ( exists ${$ref_stored_info_from_sequences}{"def"} ) {
		$def_from_sequences = ${$ref_stored_info_from_sequences}{"def"};
	}
	my $version_from_qualifiers = undef;
	if ( exists ${$ref_stored_info_from_sequences}{"version"} ) {
		$version_from_qualifiers = ${$ref_stored_info_from_sequences}{"version"};
	}
	# %ref_stored_info_from_features
	my $chromosome_from_features = undef;
	if ( exists ${$ref_stored_info_from_features}{"chromosome"} ) {
		$chromosome_from_features = ${$ref_stored_info_from_features}{"chromosome"};
	}
	my $plasmid_from_features = undef;
	if ( exists ${$ref_stored_info_from_features}{"plasmid"} ) {
		$plasmid_from_features = ${$ref_stored_info_from_features}{"plasmid"};
	}

	my $ref_hash_ncbi_assembly_and_accession_info_from_url_request = get_ncbi_assemblyaccession_for_accnum($accession, $version_from_qualifiers, $counter_loop, $scalar_new_genomes);  # assembly_NCBI_internal_id_IT, assembly_NCBI_assemblyaccession_IT, assembly_NCBI_assemblyname_IT, assembly_NCBI_lastupdatedate_IT, assembly_NCBI_seqreleasedate_IT, assembly_NCBI_species_IT, assembly_NCBI_strain_IT, assembly_NCBI_substrain_IT, assembly_NCBI_isolate_IT, assembly_NCBI_serotype_IT, assembly_NCBI_speciestaxid_IT, assembly_NCBI_taxon_id_IT, assembly_NCBI_biosampleid_IT, assembly_NCBI_biosampleaccn_IT, assembly_NCBI_list_bioprojectid_IT, assembly_NCBI_list_bioprojectaccn_IT, assembly_NCBI_assemblyclass_IT, assembly_NCBI_assemblystatus_IT, accnum_NCBI_internal_id_IT, accnum_NCBI_sourcedb, accnum_NCBI_genome, accnum_NCBI_tech, accnum_NCBI_geneticcode, accnum_NCBI_topology, accnum_NCBI_completeness, accnum_NCBI_status, accnum_NCBI_comment

	my $accnum_NCBI_genome = undef;
	if ( exists ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_genome"} ) {
		$accnum_NCBI_genome = ${$ref_hash_ncbi_assembly_and_accession_info_from_url_request}{"accnum_NCBI_genome"};
	}
	my $type_element = get_type_of_element($accession, $genome_file, $def_from_sequences, $chromosome_from_features, $plasmid_from_features, $keywords, $accnum_NCBI_genome);
	my $organism_id = insert_organisms_into_sql_files ( $accession, $ref_stored_info_from_features, $ref_stored_info_from_sequences, $type_element, $ref_hash_ncbi_assembly_and_accession_info_from_url_request ); # ORGANISMS
	#my $organism_id = ${$ref_hash_organism_id_and_extra_accession_info_returned}{"organism_id"}; # %organism_id_and_extra_accession_info_to_return = organism_id, accnum_NCBI_internal_id, accnum_NCBI_sourcedb, accnum_NCBI_genome, accnum_NCBI_tech, accnum_NCBI_geneticcode, accnum_NCBI_topology, accnum_NCBI_completeness, accnum_NCBI_status, accnum_NCBI_comment

	my $element_id = insert_elements_into_sql_files( $accession, $ref_stored_info_from_sequences, $organism_id, $type_element, $ref_hash_ncbi_assembly_and_accession_info_from_url_request); # ELEMENTS
	my $ref_hash_gene_id2gene_fields = ${$ref_stored_info_from_features}{"ref_hash_gene_id2gene_fields"}; # %gene_id2gene_fields = name, strand, start, stop, locus_tag, code_feat, is_pseudo, length_residues
	insert_genes_into_sql_files($accession, $organism_id, $element_id, $ref_hash_gene_id2gene_fields);# GENES 


}


push(@new_genomes,@new_version);
# pour tous les genomes non present dans seqDB on ajoute une entr�e dans la base
$counter_loop = 0;
my $scalar_new_genomes = scalar(@new_genomes);
foreach my $genome (@new_genomes) {
	$counter_loop++;
	if ($genome =~ /^.+\/(.+)\.gbk$/){
		$format_genomes = 'genbank';
		$extension_genomes = 'gbk';
	} elsif ($genome =~ /^.+\/(.+)\.dat$/) {
		$format_genomes = 'embl';
		$extension_genomes = 'dat';
	} else {
		#print LOG "Unrecognized file format, the file $genome is neither a .gbk or .dat.\n";
		die_with_error_mssg("Unrecognized file format, the file $genome is neither a .gbk or .dat.\n");
	}


	# recuperartion du numero d'acession
	my $accession = undef;
	$accession = $hash_new_genomes2accession { $genome };
	if (!defined($accession) || $accession eq '' ) {
		#print LOG "Error : accession undef or empty string in file $genome\n";
		die_with_error_mssg("Error : accession undef or empty string in file $genome\n");
	}


	print LOG "( $counter_loop / $scalar_new_genomes ) : parsing file $genome ; Accession: $accession... \n" unless $VERBOSE =~ m/^OFF$/;

	if ( exists $accession2element_id{$accession} ) {
		#print LOG "Error : accession $accession alrady exists in \%accession2element_id for file $genome\n";
		die_with_error_mssg("Error : accession $accession alrady exists in \%accession2element_id for file $genome\n");
	}

	# Parsing des fichiers Genbank
	my $seqio = Bio::SeqIO->new( '-file' => $genome, '-format' => $format_genomes);
	open_sql_files($accession);
	#print_header_sql_files();
	while( my $seq = $seqio->next_seq() ) {
		parse_seq_into_sql_files($seq, $accession, $genome, $counter_loop, $scalar_new_genomes);
	}
	#print_footer_sql_files();
	close_sql_files();


	print LOG "( $counter_loop / ".scalar(@new_genomes)." ) : Done file $genome ; Accession: $accession \n" unless $VERBOSE =~ m/^OFF$/;

}


if ( $DO_NOT_DELETE_DIR_TMP_DOWNLOAD =~ m/^OFF$/i ) {
	$output_backtick = `$SiteConfig::CMDDIR/rm -rf $DIR_TMP_DOWNLOAD/*`;
}


if (@WARNING_accnum_suppressed) {
	print LOG "\nWARNING : A total of ".scalar(@WARNING_accnum_suppressed)." accnum have been found to be suppressed : ".join(", ",@WARNING_accnum_suppressed)."\n" unless $VERBOSE =~ m/^OFF$/;
}
print LOG "\nA total of $NCBI_linksets_found accnum have been linked to NCBI eutils/elink tool\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "A total of $NCBI_assemblyaccession_found corresponding assembly accession have been found\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "A total of $accnum_NCBI_sourcedb_found accnum have been found in the eutils/esummary tool\n" unless $VERBOSE =~ m/^OFF$/;

print LOG "A total of $count_already_stored_orga_in_db organisms are already stored in db.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "A total of $count_group_accnum_in_existing_orga_because_similar_assemblyaccession accnum have been grouped in existing orga because similar assemblyaccession.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "A total of $count_group_accnum_in_existing_orga_because_similar_organism_fields accnum have been grouped in existing orga because similar organism fields.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "A total of $count_create_new_orga_because_similar_species_but_no_similar_organism_fields new organisms have been created because similar species found but no similar organism fields.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "A total of $count_create_new_orga_because_no_similar_species new organisms have been created because no similar species found.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "A total of $count_create_new_orga new organisms have been created.\n" unless $VERBOSE =~ m/^OFF$/;

print LOG "Task_add_entry_generator.pl finished at : ",scalar(localtime),"\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "----------------------------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;


# touch marker file done script when script done
$output_backtick = `touch $SiteConfig::LOGDIR/manage_primary_data_generator.done`;
if ($output_backtick eq "") {
	#ok
} else {
	#print LOG "Error could not complete touch $SiteConfig::LOGDIR/manage_primary_data_generator.done :\n$output_backtick \n $! \n";#
	die_with_error_mssg("Error could not complete touch $SiteConfig::LOGDIR/manage_primary_data_generator.done :\n$output_backtick \n $! \n");#
}

close(LOG);



