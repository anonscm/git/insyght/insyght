#!/usr/local/bin/perl
=pod

#
# download_genome_files_from_ncbi.pl : helper to download the genome file from ncbi
#
# Date : 01/2017
# LT
#
#LOG FILE : $SiteConfig::LOGDIR/download/download_genome_files_from_ncbi.pl
#LAUNCHED AS : perl download_genome_files_from_ncbi.pl with the following possible arguments
# arguments

 -VERBOSE { ON | OFF }
RQ : default ON

 -LIST_AND_EXIT { ON | OFF }
RQ : default OFF
RQ : Do not download

 -SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE {PATH_TO_LOCAL_REP} -> if file we want exists in this directory or subdirectories, create a softlink to $BANK_DIR
RQ : this options can be repeated multiple times
RQ : ex /projet/mig/work/tlacroix/pipeline_origami_STABLE/ORIGAMI_bank/TMP_LOCAL_PUBLIC_REPOSITORY/

 -NCBI_ASSEMBLY_SUMMARY_FILE {PATH_TO_FILE}
RQ : this options can be repeated multiple times
RQ if nothing provided, default to all genbank | refseq and archaea | bacteria :
ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/assembly_summary.txt
ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/archaea/assembly_summary.txt
ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt
ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/archaea/assembly_summary.txt
#   See ftp://ftp.ncbi.nlm.nih.gov/genomes/README_assembly_summary.txt for a description of the columns in this file.
# assembly_accession	bioproject	biosample	wgs_master	refseq_category	taxid	species_taxid	organism_name	infraspecific_name	isolate	version_status	assembly_level	release_type	genome_rep	seq_rel_date	asm_name	submitter	gbrs_paired_asm	paired_asm_comp	ftp_path	excluded_from_refseq
GCF_000010525.1	PRJNA224116	SAMD00060925		representative genome	438753	7	Azorhizobium caulinodans ORS 571	strain=ORS 571		latest	Complete Genome	Major	Full	2007/10/16	ASM1052v1	University of Tokyo	GCA_000010525.1	identical	ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/525/GCF_000010525.1_ASM1052v1	
GCF_000007365.1	PRJNA224116	SAMN02604269		representative genome	198804	9	Buchnera aphidicola str. Sg (Schizaphis graminum)	strain=Sg		latest	Complete Genome	Major	Full	2002/07/02	ASM736v1	Uppsala Univ.	GCA_000007365.1	identical	ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/007/365/GCF_000007365.1_ASM736v1
RQ : it is possible to get the Assembly accession.version & assembly name
GCA_000005845.2_ASM584v2 – this directory layer is named using the pattern: [Assembly accession.version]_[assembly name]
RQ : FTP path indicated in column 20 (ftp_path) to download the data.

 -RESTRICT_TO_FILENAME_OF_INTEREST {STRING}
RQ : default is *_genomic.gbff*
Append the filename of interest, in this case "*_genomic.gbff.gz" to the FTP directory names. One way to do this would be using the following awk command:
awk 'BEGIN{FS=OFS="/";filesuffix="genomic.gbff.gz"}{ftpdir=$0;asm=$10;file=asm"_"filesuffix;print ftpdir,file}' ftpdirpaths > ftpfilepaths

 -RESTRICT_TO_LATEST_ASSEMBLY {ON | OFF}
RQ : default ON
select those assemblies that are marked as "latest" in the version_status column (11)
RQ : One way to do this :
awk -F "\t" '$12=="Complete Genome" && $11=="latest"{print $20}' assembly_summary.txt > ftpdirpaths

 -RESTRICT_TO_COMPLETE_GENOME {ON | OFF}
RQ : default ON
select those assemblies that are marked as "Complete Genome" in the assembly_level column (column 12)
RQ : One way to do this :
awk -F "\t" '$12=="Complete Genome" && $11=="latest"{print $20}' assembly_summary.txt > ftpdirpaths

 -RESTRICT_TO_DATABANK {genbank | refseq}
RQ : optional

 -RESTRICT_TO_TAXA_DOMAIN {archaea | bacteria]
RQ : optional
RQ : optional
RQ : default empty = OFF

 -RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES {DIGIT}
RQ : this options can be repeated multiple times
RQ : optional
RQ : default empty = OFF

 -UNIPROT_GENOME_TABLE_FILE {PATH_TO_FILE}
RQ : this options can be repeated multiple times
RQ : optional
#RQ : ex download table from http://www.uniprot.org/proteomes/?query=redundant%3Ano+AND+reference%3Ayes+AND+taxonomy%3A%22Archaea+%5B2157%5D%22&sort=name&desc=no
RQ : ex file is /projet/mig/work/tlacroix/pipeline_origami_STABLE/ORIGAMI_bank/UNIPROT_GENOME_TABLE/proteomes-redundant%3Ano+AND+reference%3Ayes+AND+taxonomy%3A-Archaea+%5B2157%5--.tab
ex
Proteome ID	Organism	Organism ID	Protein count	Genome assembly ID	Proteome components	Taxon mnemonic	Taxonomic lineage
UP000008458	Acidianus hospitalis (strain W1)	933801	2329	GCA_000213215.1	Chromosome	ACIHW	Archaea, Crenarchaeota, Thermoprotei, Sulfolobales, Sulfolobaceae, Acidianus
UP000000346	Acidilobus saccharovorans (strain DSM 16705 / JCM 18335 / VKM B-2471 / 345-15)	666510	1499	GCA_000144915.1	Chromosome	ACIS3	Archaea, Crenarchaeota, Thermoprotei, Acidilobales, Acidilobaceae, Acidilobus
UP000050301	Acidiplasma cupricumulans	312540	1683	GCA_001402935.1	Unassembled WGS sequence		Archaea, Euryarchaeota, Thermoplasmata, Thermoplasmatales, Ferroplasmaceae, Acidiplasma
RQ : detect column Genome assembly ID in header
RQ : then get the ftp directory by cutting the assambly id by 3 caracters : ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/006/025/ ; there should be only 1 directory par ex: GCA_001402935.1_ASM140293v1/ and a _genomic.gbff.gz file under it: GCA_001402935.1_ASM140293v1_genomic.gbff.gz

 -LIMIT_TO_X_RANDOM_SAMPLES
RQ : optional
RQ : default -1 = OFF

 -LIMIT_TO_X_FIRST_ORDERED_SAMPLES
RQ : optional
RQ : default -1 = OFF

RQ : best protocol to use to download data : use rsync
Replace the "ftp:" at the beginning of the FTP path with "rsync:". E.g. If the FTP path is ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/696/305/GCF_001696305.1_UCN72.1, then the directory and its contents could be downloaded using the following rsync command:
rsync --copy-links --recursive --times --verbose rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/696/305/GCF_001696305.1_UCN72.1 my_dir/
A file with FTP path ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/696/305/GCF_001696305.1_UCN72.1/GCF_001696305.1_UCN72.1_genomic.gbff.gz could be downloaded using the following rsync command:
rsync --copy-links --times --verbose rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/696/305/GCF_001696305.1_UCN72.1/GCF_001696305.1_UCN72.1_genomic.gbff.gz my_dir/



# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

=cut

use strict;
use Time::HiRes qw(usleep);
use SiteConfig;
use Data::Dumper;

#script level variables
#arguments scripts
my $VERBOSE = "OFF";
my @NCBI_ASSEMBLY_SUMMARY_FILE_PRE = ();#{PATH_TO_FILE,DEFAULT_genbank_archaea,DEFAULT_refseq_archaea,DEFAULT_genbank_bacteria,DEFAULT_refseq_bacteria}
my @NCBI_ASSEMBLY_SUMMARY_FILE = ();
my @RESTRICT_TO_FILENAME_OF_INTEREST = ();#default to "_genomic.gbff" ; Append the filename of interest to the FTP directory names
my $RESTRICT_TO_LATEST_ASSEMBLY = "ON"; # "latest" version_status (column 11) in ASSEMBLY_SUMMARY file
my $RESTRICT_TO_COMPLETE_GENOME = "ON"; # "Complete Genome" assembly_level (column 12)in ASSEMBLY_SUMMARY file
my @RESTRICT_TO_DATABANK = (); #{genbank | refseq} ; assembly_accession start with GCA_ for genbank and GCF_ for refseq
my @RESTRICT_TO_TAXA_DOMAIN = (); #{archaea | bacteria]
my @RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES = (); #{DIGIT}
my @UNIPROT_GENOME_TABLE_FILE = ();#{PATH_TO_FILE}
my @RESTRICT_TO_LIST_ASSEMBLY_ACCESSION = ();#{PATH_TO_FILE}
my $LIMIT_TO_X_RANDOM_SAMPLES = -1;
my $LIMIT_TO_X_FIRST_ORDERED_SAMPLES = -1;
my $LIST_AND_EXIT = "OFF";
my @SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE = ();# {PATH_TO_LOCAL_REP}
my $DO_NOT_DELETE_DIR_TMP_DOWNLOAD = "OFF";
my $DO_NOT_DELETE_FILES_UNDER_BANKDIR_BEFORE_RUNNING = "ON";
my $DIE_ON_WARNING = "ON";
my $SKIP_GENBANK_IF_REFSEQ_AVAILABLE = "OFF";
#other
my $DIR_TMP_DOWNLOAD = "$SiteConfig::BANKDIR/../DIR_TMP_DOWNLOAD/";
my $output_backtick = "";
my @list_mandatory_columns_in_assembly_file =  ("assembly_accession","version_status","assembly_level","gbrs_paired_asm","paired_asm_comp","ftp_path","bioproject","biosample","refseq_category","taxid","asm_name","submitter","excluded_from_refseq");
	#assembly_accession RQ: {VARCHAR} ; start with GCA_ for genbank and GCF_ for refseq
	#version_status RQ: {latest}
	#assembly_level RQ: {Chromosome,Complete Genome,Contig,Scaffold}
	#ftp_path ex ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/525/GCF_000010525.1_ASM1052v1
	#bioproject RQ: {VARCHAR} link to NCBI bioproject
	#biosample RQ: {VARCHAR} link to NCBI biosample
	#refseq_category RQ: {na,representative genome,reference genome}
	#taxid {DIGIT}
	#asm_name RQ: {VARCHAR}, = assembly_name
	#submitter RQ: {VARCHAR}
my @list_mandatory_columns_in_uniprot_genome_table = ("Proteome ID","Genome assembly ID");
	#Proteome ID ex UP000008458
	#Genome assembly ID ex GCA_000213215.1 ; link to NCBI assembly_accession
my %assembly_accession_of_interest2ftp_path = ();
my @assembly_accession_to_treat_pre = ();
my @assembly_accession_to_treat = ();
my %restrict_to_hash_taxon_id = ();
my %restrict_to_hash_assembly_accession = ();
my %parent_tax_id2ref_list_child_tax_id = ();
my %SOFTLINKS_TO_CREATE = ();
my %URL_FILES_TO_DOWNLOAD = ();
my $counter_loop = 0;

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/download/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/download/download_genome_files_from_ncbi.pl"
);
print LOG
"\n---------------------------------------------------\n download_genome_files_from_ncbi.pl started at :",
  scalar(localtime), "\n";


#set the variables according to the argument or exit with usage if the arguments are incorrect
foreach my $argnum ( 0 .. $#ARGV ) {
#$VERBOSE
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -VERBOSE argument ; usage : perl download_genome_files_from_ncbi.pl -VERBOSE = {ON, OFF}";
			die
"incorrect -VERBOSE argument ; usage : perl download_genome_files_from_ncbi.pl -VERBOSE = {ON, OFF}";
		}
	}
#$LIST_AND_EXIT = "OFF";
	elsif ( $ARGV[$argnum] =~ m/^-LIST_AND_EXIT$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$LIST_AND_EXIT = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -LIST_AND_EXIT argument ; usage : perl download_genome_files_from_ncbi.pl -LIST_AND_EXIT = {ON, OFF}";
			die
"incorrect -LIST_AND_EXIT argument ; usage : perl download_genome_files_from_ncbi.pl -LIST_AND_EXIT = {ON, OFF}";
		}
	}

#@SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE = ();# {PATH_TO_LOCAL_REP}
	elsif ( $ARGV[$argnum] =~ m/^-SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			push ( @SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE, $ARGV[ $argnum + 1 ] );
		}
		else {
			print LOG
"incorrect -SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE argument ; usage : perl download_genome_files_from_ncbi.pl -SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE = {PATH_TO_LOCAL_REP}";
			die
"incorrect -SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE argument ; usage : perl download_genome_files_from_ncbi.pl -SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE = {PATH_TO_LOCAL_REP}";
		}
	}

#@NCBI_ASSEMBLY_SUMMARY_FILE = ();#{PATH_TO_FILE,DEFAULT_genbank_archaea,DEFAULT_refseq_archaea,DEFAULT_genbank_bacteria,DEFAULT_refseq_bacteria}
	elsif ( $ARGV[$argnum] =~ m/^-NCBI_ASSEMBLY_SUMMARY_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			push ( @NCBI_ASSEMBLY_SUMMARY_FILE_PRE, $ARGV[ $argnum + 1 ] );
		}
		else {
			print LOG
"incorrect -NCBI_ASSEMBLY_SUMMARY_FILE argument ; usage : perl download_genome_files_from_ncbi.pl -NCBI_ASSEMBLY_SUMMARY_FILE = {PATH_TO_FILE}";
			die
"incorrect -NCBI_ASSEMBLY_SUMMARY_FILE argument ; usage : perl download_genome_files_from_ncbi.pl -NCBI_ASSEMBLY_SUMMARY_FILE = {PATH_TO_FILE}";
		}
	}

#$RESTRICT_TO_FILENAME_OF_INTEREST
	elsif ( $ARGV[$argnum] =~ m/^-RESTRICT_TO_FILENAME_OF_INTEREST$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			push ( @RESTRICT_TO_FILENAME_OF_INTEREST, $ARGV[ $argnum + 1 ] );
		}
		else {
			print LOG
"incorrect -RESTRICT_TO_FILENAME_OF_INTEREST argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_FILENAME_OF_INTEREST = {STRING}";
			die
"incorrect -RESTRICT_TO_FILENAME_OF_INTEREST argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_FILENAME_OF_INTEREST = {STRING}";
		}
	}

#$RESTRICT_TO_LATEST_ASSEMBLY = "ON";
	elsif ( $ARGV[$argnum] =~ m/^-RESTRICT_TO_LATEST_ASSEMBLY$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$RESTRICT_TO_LATEST_ASSEMBLY = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -RESTRICT_TO_LATEST_ASSEMBLY argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_LATEST_ASSEMBLY = {ON, OFF}";
			die
"incorrect -RESTRICT_TO_LATEST_ASSEMBLY argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_LATEST_ASSEMBLY = {ON, OFF}";
		}
	}

#$RESTRICT_TO_COMPLETE_GENOME = "ON";
	elsif ( $ARGV[$argnum] =~ m/^-RESTRICT_TO_COMPLETE_GENOME$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$RESTRICT_TO_COMPLETE_GENOME = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -RESTRICT_TO_COMPLETE_GENOME argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_COMPLETE_GENOME = {ON, OFF}";
			die
"incorrect -RESTRICT_TO_COMPLETE_GENOME argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_COMPLETE_GENOME = {ON, OFF}";
		}
	}

#$RESTRICT_TO_DATABANK = ""; #{genbank | refseq}
	elsif ( $ARGV[$argnum] =~ m/^-RESTRICT_TO_DATABANK$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^genbank$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^refseq$/i )
		{
			push ( @RESTRICT_TO_DATABANK, $ARGV[ $argnum + 1 ] );
		}
		else {
			print LOG
"incorrect -RESTRICT_TO_DATABANK argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_DATABANK = {genbank, refseq}";
			die
"incorrect -RESTRICT_TO_DATABANK argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_DATABANK = {genbank, refseq}";
		}
	}

#$RESTRICT_TO_TAXA_DOMAIN= ""; #{archaea | bacteria]
	elsif ( $ARGV[$argnum] =~ m/^-RESTRICT_TO_TAXA_DOMAIN$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^archaea$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^bacteria$/i )
		{
			push ( @RESTRICT_TO_TAXA_DOMAIN, $ARGV[ $argnum + 1 ] );
		}
		else {
			print LOG
"incorrect -RESTRICT_TO_DATABANK argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_DATABANK = {archaea, bacteria}";
			die
"incorrect -RESTRICT_TO_DATABANK argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_DATABANK = {archaea, bacteria}";
		}
	}
#$RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES
	elsif ( $ARGV[$argnum] =~ m/^-RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i )
		{
			push ( @RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES, $ARGV[ $argnum + 1 ] );
		}
		else {
			print LOG
"incorrect -RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES = {DIGIT}";
			die
"incorrect -RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES = {DIGIT}";
		}
	}

#@UNIPROT_GENOME_TABLE_FILE = ();#{PATH_TO_FILE}
	elsif ( $ARGV[$argnum] =~ m/^-UNIPROT_GENOME_TABLE_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			push ( @UNIPROT_GENOME_TABLE_FILE, $ARGV[ $argnum + 1 ] );
		}
		else {
			print LOG
"incorrect -UNIPROT_GENOME_TABLE_FILE argument ; usage : perl download_genome_files_from_ncbi.pl -UNIPROT_GENOME_TABLE_FILE = {PATH_TO_FILE}";
			die
"incorrect -UNIPROT_GENOME_TABLE_FILE argument ; usage : perl download_genome_files_from_ncbi.pl -UNIPROT_GENOME_TABLE_FILE = {PATH_TO_FILE}";
		}
	}
# @RESTRICT_TO_LIST_ASSEMBLY_ACCESSION
	elsif ( $ARGV[$argnum] =~ m/^-RESTRICT_TO_LIST_ASSEMBLY_ACCESSION$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			push ( @RESTRICT_TO_LIST_ASSEMBLY_ACCESSION, $ARGV[ $argnum + 1 ] );
		}
		else {
			print LOG
"incorrect -RESTRICT_TO_LIST_ASSEMBLY_ACCESSION argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_LIST_ASSEMBLY_ACCESSION = {ASSEMBLY_ACCESSION}";
			die
"incorrect -RESTRICT_TO_LIST_ASSEMBLY_ACCESSION argument ; usage : perl download_genome_files_from_ncbi.pl -RESTRICT_TO_LIST_ASSEMBLY_ACCESSION = {ASSEMBLY_ACCESSION}";
		}
	}
# LIMIT_TO_X_RANDOM_SAMPLES
	elsif ( $ARGV[$argnum] =~ m/^-LIMIT_TO_X_RANDOM_SAMPLES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i )
		{
			$LIMIT_TO_X_RANDOM_SAMPLES = $ARGV[ $argnum + 1 ] ;
		}
		else {
			print LOG
"incorrect -LIMIT_TO_X_RANDOM_SAMPLES argument ; usage : perl download_genome_files_from_ncbi.pl -LIMIT_TO_X_RANDOM_SAMPLES = {DIGIT}";
			die
"incorrect -LIMIT_TO_X_RANDOM_SAMPLES argument ; usage : perl download_genome_files_from_ncbi.pl -LIMIT_TO_X_RANDOM_SAMPLES = {DIGIT}";
		}
	}

#LIMIT_TO_X_FIRST_ORDERED_SAMPLES
	elsif ( $ARGV[$argnum] =~ m/^-LIMIT_TO_X_FIRST_ORDERED_SAMPLES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i )
		{
			$LIMIT_TO_X_FIRST_ORDERED_SAMPLES = $ARGV[ $argnum + 1 ] ;
		}
		else {
			print LOG
"incorrect -LIMIT_TO_X_FIRST_ORDERED_SAMPLES argument ; usage : perl download_genome_files_from_ncbi.pl -LIMIT_TO_X_FIRST_ORDERED_SAMPLES = {DIGIT}";
			die
"incorrect -LIMIT_TO_X_FIRST_ORDERED_SAMPLES argument ; usage : perl download_genome_files_from_ncbi.pl -LIMIT_TO_X_FIRST_ORDERED_SAMPLES = {DIGIT}";
		}
	}

#DO_NOT_DELETE_DIR_TMP_DOWNLOAD
	elsif ( $ARGV[$argnum] =~ m/^-DO_NOT_DELETE_DIR_TMP_DOWNLOAD$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DO_NOT_DELETE_DIR_TMP_DOWNLOAD = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -DO_NOT_DELETE_DIR_TMP_DOWNLOAD argument ; usage : perl download_genome_files_from_ncbi.pl -DO_NOT_DELETE_DIR_TMP_DOWNLOAD = {ON, OFF}";
			die
"incorrect -DO_NOT_DELETE_DIR_TMP_DOWNLOAD argument ; usage : perl download_genome_files_from_ncbi.pl -DO_NOT_DELETE_DIR_TMP_DOWNLOAD = {ON, OFF}";
		}
	}
# DO_NOT_DELETE_FILES_UNDER_BANKDIR_BEFORE_RUNNING
	elsif ( $ARGV[$argnum] =~ m/^-DO_NOT_DELETE_FILES_UNDER_BANKDIR_BEFORE_RUNNING$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DO_NOT_DELETE_FILES_UNDER_BANKDIR_BEFORE_RUNNING = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -DO_NOT_DELETE_FILES_UNDER_BANKDIR_BEFORE_RUNNING argument ; usage : perl download_genome_files_from_ncbi.pl -DO_NOT_DELETE_FILES_UNDER_BANKDIR_BEFORE_RUNNING = {ON, OFF}";
			die
"incorrect -DO_NOT_DELETE_FILES_UNDER_BANKDIR_BEFORE_RUNNING argument ; usage : perl download_genome_files_from_ncbi.pl -DO_NOT_DELETE_FILES_UNDER_BANKDIR_BEFORE_RUNNING = {ON, OFF}";
		}
	}
#DIE_ON_WARNING
	elsif ( $ARGV[$argnum] =~ m/^-DIE_ON_WARNING$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DIE_ON_WARNING = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -DIE_ON_WARNING argument ; usage : perl download_genome_files_from_ncbi.pl -DIE_ON_WARNING = {ON, OFF}";
			die
"incorrect -DIE_ON_WARNING argument ; usage : perl download_genome_files_from_ncbi.pl -DIE_ON_WARNING = {ON, OFF}";
		}
	}
#SKIP_GENBANK_IF_REFSEQ_AVAILABLE
	elsif ( $ARGV[$argnum] =~ m/^-SKIP_GENBANK_IF_REFSEQ_AVAILABLE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$SKIP_GENBANK_IF_REFSEQ_AVAILABLE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -SKIP_GENBANK_IF_REFSEQ_AVAILABLE argument ; usage : perl download_genome_files_from_ncbi.pl -SKIP_GENBANK_IF_REFSEQ_AVAILABLE = {ON, OFF}";
			die
"incorrect -SKIP_GENBANK_IF_REFSEQ_AVAILABLE argument ; usage : perl download_genome_files_from_ncbi.pl -SKIP_GENBANK_IF_REFSEQ_AVAILABLE = {ON, OFF}";
		}
	}
}#foreach my $argnum ( 0 .. $#ARGV ) {

#default
if (@RESTRICT_TO_FILENAME_OF_INTEREST) { # @RESTRICT_TO_FILENAME_OF_INTEREST is not empty...
	# ok do nothing
} else { # @RESTRICT_TO_FILENAME_OF_INTEREST is empty
	#default to "*_genomic.gbff*"
	@RESTRICT_TO_FILENAME_OF_INTEREST = ("_genomic.gbff");
}



print LOG "\nYou choose the following options :
\t VERBOSE : $VERBOSE
\t LIST_AND_EXIT : $LIST_AND_EXIT
\t SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE : ".join(', ',@SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE)."
\t NCBI_ASSEMBLY_SUMMARY_FILE : ".join(', ',@NCBI_ASSEMBLY_SUMMARY_FILE_PRE)."
\t RESTRICT_TO_FILENAME_OF_INTEREST : ".join(', ',@RESTRICT_TO_FILENAME_OF_INTEREST)."
\t RESTRICT_TO_LATEST_ASSEMBLY : $RESTRICT_TO_LATEST_ASSEMBLY
\t RESTRICT_TO_COMPLETE_GENOME : $RESTRICT_TO_COMPLETE_GENOME
\t RESTRICT_TO_DATABANK : ".join(', ',@RESTRICT_TO_DATABANK)."
\t RESTRICT_TO_TAXA_DOMAIN : ".join(', ',@RESTRICT_TO_TAXA_DOMAIN)."
\t RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES : ".join(', ',@RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES)."
\t UNIPROT_GENOME_TABLE_FILE : ".join(', ',@UNIPROT_GENOME_TABLE_FILE)."
\t RESTRICT_TO_LIST_ASSEMBLY_ACCESSION : ".join(', ',@RESTRICT_TO_LIST_ASSEMBLY_ACCESSION)."
\t LIMIT_TO_X_RANDOM_SAMPLES : $LIMIT_TO_X_RANDOM_SAMPLES
\t LIMIT_TO_X_FIRST_ORDERED_SAMPLES : $LIMIT_TO_X_FIRST_ORDERED_SAMPLES
\t DO_NOT_DELETE_DIR_TMP_DOWNLOAD : $DO_NOT_DELETE_DIR_TMP_DOWNLOAD
\t DIE_ON_WARNING : $DIE_ON_WARNING
\t SKIP_GENBANK_IF_REFSEQ_AVAILABLE : $SKIP_GENBANK_IF_REFSEQ_AVAILABLE
" unless $VERBOSE =~ m/^OFF$/;


# mkdir and rm file
`$SiteConfig::CMDDIR/mkdir -p $DIR_TMP_DOWNLOAD`;
if ( $DO_NOT_DELETE_DIR_TMP_DOWNLOAD =~ m/^OFF$/i ) {
	$output_backtick = `$SiteConfig::CMDDIR/rm -rf $DIR_TMP_DOWNLOAD/*`;
}
$output_backtick = `$SiteConfig::CMDDIR/mkdir -p $SiteConfig::BANKDIR/`;
if ( $DO_NOT_DELETE_FILES_UNDER_BANKDIR_BEFORE_RUNNING =~ m/^OFF$/i ) {
	$output_backtick = `$SiteConfig::CMDDIR/rm -rf $SiteConfig::BANKDIR/*`;
}

#$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/*.err`;
#$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/*.out`;
if ($output_backtick eq "") {
	#ok
} else {
	print LOG "Error could not complete mkdir and rm file in download_genome_files_from_ncbi.pl : $!\n";
	die("Error could not complete mkdir and rm file in download_genome_files_from_ncbi.pl : $!\n");
}




=pod
head of the file nodes.dmp:
    # read file nodes.dmp and construct %parent_tax_id2list_child_tax_id ()
      7 nodes.dmp file consists of taxonomy nodes. The description for each node includes the following
      8 fields:
      9         tax_id                                  -- node id in GenBank taxonomy database
     10         parent tax_id                           -- parent node id in GenBank taxonomy database
=cut
# fill %parent_tax_id2ref_list_child_tax_id
sub read_nodes_dmp_and_construct_parent_tax_id2ref_list_child_tax_id {
	my ( $path_to_nodes_dmp_file ) = @_;

	open( NODES_DMP_FH, "<$path_to_nodes_dmp_file" ) or die("Can not open NODES_DMP_FH $path_to_nodes_dmp_file\n");
	while ( my $line = <NODES_DMP_FH> ) {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;

		if ( $line =~ m/^(\d+)\s*\|\s*(\d+)\s*\|/ ) {
			my $tax_id = $1;
			my $parent_tax_id = $2;
			if ( exists $parent_tax_id2ref_list_child_tax_id{$parent_tax_id} ) {
				my $ref_list_child_tax_id = $parent_tax_id2ref_list_child_tax_id{$parent_tax_id};
				push (@{$ref_list_child_tax_id}, $tax_id) ;
			} else {
				my @list_child_tax_id = ();
				push (@list_child_tax_id, $tax_id) ;
				$parent_tax_id2ref_list_child_tax_id{$parent_tax_id} = \@list_child_tax_id;
			}
		} else {
			print LOG "Error read_nodes_dmp_and_construct_parent_tax_id2ref_list_child_tax_id. Could not parse nodes.dmp line:\n$line\n";
			die("Error read_nodes_dmp_and_construct_parent_tax_id2ref_list_child_tax_id. Could not parse nodes.dmp line:\n$line\n");
			
		}
	}

	#print Dumper(\%parent_tax_id2ref_list_child_tax_id);
=pod
	if ( exists $parent_tax_id2ref_list_child_tax_id{"1351"} ) {
		print Dumper($parent_tax_id2ref_list_child_tax_id{"1351"});
	} else {
		print LOG "No 1351 in parent_tax_id2ref_list_child_tax_id\n";
	}
	if ( exists $parent_tax_id2ref_list_child_tax_id{"458032"} ) {
		print Dumper($parent_tax_id2ref_list_child_tax_id{"458032"});
	} else {
		print LOG "No 458032 in parent_tax_id2ref_list_child_tax_id\n";
	}
	if ( exists $parent_tax_id2ref_list_child_tax_id{"935733"} ) {
		print Dumper($parent_tax_id2ref_list_child_tax_id{"935733"});
	} else {
		print LOG "No 935733 in parent_tax_id2ref_list_child_tax_id\n";
	}
=cut

}


# fill %restrict_to_hash_taxon_id
sub add_recursively_children_taxon_id {
	my ( $parent_taxon_id ) = @_;
	if ( exists $parent_tax_id2ref_list_child_tax_id{$parent_taxon_id} ) {
		my $ref_list_child_tax_id = $parent_tax_id2ref_list_child_tax_id{$parent_taxon_id};
		my @list_child_tax_id = @{$ref_list_child_tax_id};
		foreach my $child_tax_id_IT (@list_child_tax_id) {
			$restrict_to_hash_taxon_id{$child_tax_id_IT} = undef;
			add_recursively_children_taxon_id($child_tax_id_IT);
		}
	}
}


if ( @RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES ) {
=pod
	print LOG "The RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES option is not yet implemented\n";
	die ("The RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES option is not yet implemented\n");
=cut
	print LOG "\n\nStart listing valid taxon ids and subnodes for RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES ".join(', ',@RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES)."\n" unless $VERBOSE =~ m/^OFF$/i;
	
	print LOG "Downloading taxdump.tar.gz...\n" unless $VERBOSE =~ m/^OFF$/;
	$output_backtick = `curl https://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz -o "$DIR_TMP_DOWNLOAD/taxdump.tar.gz" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error`;
	if ($output_backtick eq "" && -e "$DIR_TMP_DOWNLOAD/taxdump.tar.gz" ) {
		#ok
	} else {
		print LOG "Error could not complete download taxdump : curl https://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz -o \"$DIR_TMP_DOWNLOAD/taxdump.tar.gz\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error: $!\n";
		die("Error could not complete download taxdump : curl https://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz -o \"$DIR_TMP_DOWNLOAD/taxdump.tar.gz\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error: $!\n");
	}
	
	print LOG "Exctracting taxdump.tar.gz...\n" unless $VERBOSE =~ m/^OFF$/;
	$output_backtick = `tar xzf $DIR_TMP_DOWNLOAD/taxdump.tar.gz -C $DIR_TMP_DOWNLOAD`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not exctract taxdump : tar xzf $DIR_TMP_DOWNLOAD/taxdump.tar.gz -C $DIR_TMP_DOWNLOAD: $!\n";
		die("Error could not exctract taxdump : tar xzf $DIR_TMP_DOWNLOAD/taxdump.tar.gz -C $DIR_TMP_DOWNLOAD: $!\n");
	}

	if (-e "$DIR_TMP_DOWNLOAD/nodes.dmp") {
		read_nodes_dmp_and_construct_parent_tax_id2ref_list_child_tax_id("$DIR_TMP_DOWNLOAD/nodes.dmp");
		foreach my $taxon_id_to_restrict (@RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES) {
			$restrict_to_hash_taxon_id{$taxon_id_to_restrict} = undef;
			add_recursively_children_taxon_id($taxon_id_to_restrict);
		}
		#print Dumper(\%restrict_to_hash_taxon_id);
	} else {
		print LOG "Error file $DIR_TMP_DOWNLOAD/nodes.dmp does not exists\n";
		die("Error file $DIR_TMP_DOWNLOAD/nodes.dmp does not exists\n");
	}

	#print Dumper(\%restrict_to_hash_taxon_id);

	print LOG "Done listing ".scalar(keys %restrict_to_hash_taxon_id)." valid taxon ids and subnodes for RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES ".join(', ',@RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES)."\n" unless $VERBOSE =~ m/^OFF$/i;

}


# @RESTRICT_TO_LIST_ASSEMBLY_ACCESSION restrict_to_hash_assembly_accession
if ( @RESTRICT_TO_LIST_ASSEMBLY_ACCESSION ) {
	foreach my $RESTRICT_TO_ASSEMBLY_ACCESSION_IT (@RESTRICT_TO_LIST_ASSEMBLY_ACCESSION) {
		$restrict_to_hash_assembly_accession{$RESTRICT_TO_ASSEMBLY_ACCESSION_IT} = undef;
	}

}

# parse @UNIPROT_GENOME_TABLE_FILE needs to be before @ASSEMBLY_NCBI because the latter files are more complete regarding url_path and will overrides
# parse @UNIPROT_GENOME_TABLE_FILE to add to %assembly_accession_of_interest2ftp_path
# -UNIPROT_GENOME_TABLE_FILE {PATH_TO_FILE}
#RQ : optional
#RQ : set -RESTRICT_TO_COMPLETE_GENOME to OFF
#RQ : ex download table from http://www.uniprot.org/proteomes/?query=redundant%3Ano+AND+reference%3Ayes+AND+taxonomy%3A%22Archaea+%5B2157%5D%22&sort=name&desc=no
#RQ : ex file is /projet/mig/work/tlacroix/pipeline_origami_STABLE/ORIGAMI_bank/UNIPROT_GENOME_TABLE/proteomes-redundant%3Ano+AND+reference%3Ayes+AND+taxonomy%3A-Archaea+%5B2157%5--.tab
#ex
#Proteome ID	Organism	Organism ID	Protein count	Genome assembly ID	Proteome components	Taxon mnemonic	Taxonomic lineage
#UP000008458	Acidianus hospitalis (strain W1)	933801	2329	GCA_000213215.1	Chromosome	ACIHW	Archaea, Crenarchaeota, Thermoprotei, Sulfolobales, Sulfolobaceae, Acidianus
#UP000000346	Acidilobus saccharovorans (strain DSM 16705 / JCM 18335 / VKM B-2471 / 345-15)	666510	1499	GCA_000144915.1	Chromosome	ACIS3	Archaea, Crenarchaeota, Thermoprotei, Acidilobales, Acidilobaceae, Acidilobus
#UP000050301	Acidiplasma cupricumulans	312540	1683	GCA_001402935.1	Unassembled WGS sequence		Archaea, Euryarchaeota, Thermoplasmata, Thermoplasmatales, Ferroplasmaceae, Acidiplasma
#RQ : detect column Genome assembly ID in header
#RQ : then get the ftp directory by cutting the assambly id by 3 caracters : ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/006/025/ ; there should be only 1 directory par ex: GCA_001402935.1_ASM140293v1/ and a _genomic.gbff.gz file under it: GCA_001402935.1_ASM140293v1_genomic.gbff.gz
if(@UNIPROT_GENOME_TABLE_FILE){
	print LOG "\nStart parsing ".scalar (@UNIPROT_GENOME_TABLE_FILE)." Uniprot genomes table file(s)\n" unless $VERBOSE =~ m/^OFF$/i;
	if ( $RESTRICT_TO_LATEST_ASSEMBLY =~ m/^ON$/i ) {
		print LOG "No information on latest assembly is available in the Uniprot genome table file, the RESTRICT_TO_LATEST_ASSEMBLY filter is not applied on this type of file.\n" unless $VERBOSE =~ m/^OFF$/i;
	}
	if ( $RESTRICT_TO_COMPLETE_GENOME =~ m/^ON$/i ) {
		print LOG "No information on complete genome  is available in the Uniprot genome table file, the RESTRICT_TO_COMPLETE_GENOME filter is not applied on this type of file.\n" unless $VERBOSE =~ m/^OFF$/i;
	}
	UNIPROT_GENOME_TABLE_FILE: foreach my $UNIPROT_GENOME_TABLE_FILE_IT (@UNIPROT_GENOME_TABLE_FILE) {

		# lecture du fichier UNIPROT_GENOME_TABLE_FILE_IT
		print LOG "Start processing Uniprot genomes table file $UNIPROT_GENOME_TABLE_FILE_IT\n" unless $VERBOSE =~ m/^OFF$/i;
		open( UNIPROT_GENOME_TABLE_FILE_IT_FH, "<$UNIPROT_GENOME_TABLE_FILE_IT" ) or die("Can not open $UNIPROT_GENOME_TABLE_FILE_IT\n");
		my $header_found = 0;
		my %header_column_name_to_column_position_uniprot_genome_table = ();
		my $index_proteome_id = -1;
		my $index_genome_assembly_id = -1; # ex : GCA_000213215.1
		my $index_taxonomic_lineage = -1;
		my $index_NCBI_TaxId = -1;
		my $Regex_RESTRICT_TO_DATABANK = ""; # can be A for genbank, F for refseq, or a combination of both for both
		if (@RESTRICT_TO_DATABANK) {
			#{genbank | refseq} ; assembly_accession start with GCA_ for genbank and GCF_ for refseq
			foreach my $RESTRICT_TO_DATABANK_IT (@RESTRICT_TO_DATABANK) {
				if ($RESTRICT_TO_DATABANK_IT eq "genbank") {
					$Regex_RESTRICT_TO_DATABANK = $Regex_RESTRICT_TO_DATABANK."A";
				} elsif ($RESTRICT_TO_DATABANK_IT eq "refseq") {
					$Regex_RESTRICT_TO_DATABANK = $Regex_RESTRICT_TO_DATABANK."F";
				} else {
					print LOG "Error parsing \@RESTRICT_TO_DATABANK : value $RESTRICT_TO_DATABANK_IT is not supported, must be genbank or refseq\n";
					die("Error parsing \@RESTRICT_TO_DATABANK : value $RESTRICT_TO_DATABANK_IT is not supported, must be genbank or refseq\n");
				}
			}
		}

		LINE_IN_FILE_UNIPROT_GENOME_TABLE: while ( my $line = <UNIPROT_GENOME_TABLE_FILE_IT_FH> ) {
			chomp($line);
			$line =~ s/^\s+//;
			$line =~ s/\s+$//;

			if ( $line =~ m/^Proteome ID\t/ ) {
				print LOG "Processing header...\n" unless $VERBOSE =~ m/^OFF$/i;
				$header_found = 1;
				#$line =~ s/^#\s+//;
				my @fields = split( '\t', $line );

				for my $i (0 .. $#fields) {
					my $field_IT = $fields[$i];
					$header_column_name_to_column_position_uniprot_genome_table{$field_IT} = $i ;
				}
				# check for mandatory header
				foreach my $mandatory_columns_in_uniprot_genome_table_IT (@list_mandatory_columns_in_uniprot_genome_table) {
					if ( ! exists $header_column_name_to_column_position_uniprot_genome_table{$mandatory_columns_in_uniprot_genome_table_IT} ) {
						print LOG "Error reading assembly file $UNIPROT_GENOME_TABLE_FILE_IT : mandatory column $mandatory_columns_in_uniprot_genome_table_IT not found in header\n";
						die("Error reading assembly file $UNIPROT_GENOME_TABLE_FILE_IT : mandatory column $mandatory_columns_in_uniprot_genome_table_IT not found in header\n");
					} elsif ($mandatory_columns_in_uniprot_genome_table_IT eq "Proteome ID") {
						$index_proteome_id = $header_column_name_to_column_position_uniprot_genome_table{"Proteome ID"};
					} elsif ($mandatory_columns_in_uniprot_genome_table_IT eq "Genome assembly ID") {
						$index_genome_assembly_id = $header_column_name_to_column_position_uniprot_genome_table{"Genome assembly ID"};
					}
				
				}
				# add index other column used afterward
				if ( exists $header_column_name_to_column_position_uniprot_genome_table{"Taxonomic lineage"} ) {
					$index_taxonomic_lineage = $header_column_name_to_column_position_uniprot_genome_table{"Taxonomic lineage"};
				} else {
					if (@RESTRICT_TO_TAXA_DOMAIN) {
						print LOG "The column \"Taxonomic lineage\" is not present in the Uniprot genome table file, therefore the RESTRICT_TO_TAXA_DOMAIN filter is not applied on the file $UNIPROT_GENOME_TABLE_FILE_IT.\n" unless $VERBOSE =~ m/^OFF$/i;
					}		
				}
				if ( exists $header_column_name_to_column_position_uniprot_genome_table{"Organism ID"} ) {
					$index_NCBI_TaxId = $header_column_name_to_column_position_uniprot_genome_table{"Organism ID"};
				} else {
					if (@RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES) {
						print LOG "The column \"Organism ID\" (equivalent to NCBI TaxId) is not present in the Uniprot genome table file, therefore the RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES filter is not applied on the file $UNIPROT_GENOME_TABLE_FILE_IT.\n" unless $VERBOSE =~ m/^OFF$/i;
					}		
				}


				print LOG "Done processing header...\n" unless $VERBOSE =~ m/^OFF$/i;
			} else {

				if ($header_found != 1) {
					print LOG "Error reading assembly file $UNIPROT_GENOME_TABLE_FILE_IT : no header found at begining of file\n";
					die("Error reading assembly file $UNIPROT_GENOME_TABLE_FILE_IT : no header found at begining of file\n");
				}
				my @fields = split( '\t', $line );

				# Tests on RESTRICT
				if (@RESTRICT_TO_DATABANK) {
					# #{genbank | refseq} ; assembly_accession start with GCA_ for genbank and GCF_ for refseq
					# ex : GCA_000213215.1
					if ( $fields[$index_genome_assembly_id] =~ m/^GC[$Regex_RESTRICT_TO_DATABANK]_/i ) {
						#ok pass the test
					} else {
						#do not pass the test
						print LOG "the field $fields[$index_genome_assembly_id] of the line \n$line\n do not pass the RESTRICT_TO_DATABANK test (REGEX = ^GC[$Regex_RESTRICT_TO_DATABANK]_), it will be ignored\n" unless $VERBOSE =~ m/^OFF$/i;
						next LINE_IN_FILE_UNIPROT_GENOME_TABLE;
					}
				}



				if (@RESTRICT_TO_TAXA_DOMAIN && $index_taxonomic_lineage >= 0) { #{archaea | bacteria]

					my $taxonomic_lineage_IT = $fields[$index_taxonomic_lineage];
					my $pass_test_RESTRICT_TO_TAXA_DOMAIN = 0;
					foreach my $RESTRICT_TO_TAXA_DOMAIN_IT (@RESTRICT_TO_TAXA_DOMAIN) {
						if ( $taxonomic_lineage_IT =~ m/^$RESTRICT_TO_TAXA_DOMAIN_IT/i ) {
							$pass_test_RESTRICT_TO_TAXA_DOMAIN = 1;
						}
					}
					if ($pass_test_RESTRICT_TO_TAXA_DOMAIN == 0) {
						print LOG "The file $UNIPROT_GENOME_TABLE_FILE_IT doesn't pass the pass_test_RESTRICT_TO_TAXA_DOMAIN argument and will not be treated\n" unless $VERBOSE =~ m/^OFF$/i;
						next ASSEMBLY_SUMMARY_FILE;
					} else {
						print LOG "The file $UNIPROT_GENOME_TABLE_FILE_IT pass the pass_test_RESTRICT_TO_TAXA_DOMAIN argument and will be treated\n" unless $VERBOSE =~ m/^OFF$/i;
					}
				}


				#Proteome ID	Organism	Organism ID	Protein count	Genome assembly ID	Proteome components	Taxon mnemonic	Taxonomic lineage
				#UP000008458	Acidianus hospitalis (strain W1)	933801	2329	GCA_000213215.1	Chromosome	ACIHW	Archaea, Crenarchaeota, Thermoprotei, Sulfolobales, Sulfolobaceae, Acidianus
				#UP000000346	Acidilobus saccharovorans (strain DSM 16705 / JCM 18335 / VKM B-2471 / 345-15)	666510	1499	GCA_000144915.1	Chromosome	ACIS3	Archaea, Crenarchaeota, Thermoprotei, Acidilobales, Acidilobaceae, Acidilobus
				#@RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES : fields Organism ID <-> NCBI TaxId AND using %restrict_to_hash_taxon_id
				if (@RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES && $index_NCBI_TaxId >= 0) {
					my $NCBI_TaxId_IT = $fields[$index_NCBI_TaxId];
					if (exists $restrict_to_hash_taxon_id{$NCBI_TaxId_IT} ) {
						#ok pass the test
					} else {
						#do not pass the test
						print LOG "the field $fields[$index_NCBI_TaxId] of the line \n$line\n do not pass the RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES test, it will be ignored\n" unless $VERBOSE =~ m/^OFF$/i;
						next LINE_IN_FILE_UNIPROT_GENOME_TABLE;
					}
				}

				#did pass all test, add assembly accnum to assembly_accession_of_interest2ftp_path
				my $assembly_accession_to_store = $fields[$index_genome_assembly_id];
				my $ftp_path_to_store = undef; # no such info in uniprot file
				if ($assembly_accession_to_store eq "") {
					print LOG "The Genome assembly ID in the line $line is empty \n" unless $VERBOSE =~ m/^OFF$/i;
				} elsif ( exists $assembly_accession_of_interest2ftp_path{$assembly_accession_to_store} ) {
					print LOG "The Genome assembly ID in the line $line is redondant and will not be added \n" unless $VERBOSE =~ m/^OFF$/i;
				} else {
					$assembly_accession_of_interest2ftp_path{$assembly_accession_to_store} = $ftp_path_to_store;
				}

			}
		} # LINE_IN_FILE_UNIPROT_GENOME_TABLE: while ( my $line = <UNIPROT_GENOME_TABLE_FILE_IT_FH> ) {

		close(UNIPROT_GENOME_TABLE_FILE_IT_FH);
		print LOG "Done processing uniprot genomes table file $UNIPROT_GENOME_TABLE_FILE_IT\n" unless $VERBOSE =~ m/^OFF$/i;
	}
	print LOG "Done parsing Uniprot genomes table file(s)\n" unless $VERBOSE =~ m/^OFF$/i;
}



# download NCBI_ASSEMBLY_SUMMARY_FILE if ftp and store them in @NCBI_ASSEMBLY_SUMMARY_FILE
if (@NCBI_ASSEMBLY_SUMMARY_FILE_PRE) {
	print LOG "\nRetrieving ".scalar (@NCBI_ASSEMBLY_SUMMARY_FILE_PRE)." NCBI assembly summary file(s)\n" unless $VERBOSE =~ m/^OFF$/i;
	foreach my $NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT (@NCBI_ASSEMBLY_SUMMARY_FILE_PRE) {

		if ($NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT =~ m/^DEFAULT_genbank_archaea$/i ) {
			$NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT = "ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/archaea/assembly_summary.txt";
			print LOG "DEFAULT_genbank_archaea file is ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/archaea/assembly_summary.txt\n" unless $VERBOSE =~ m/^OFF$/i;
		} elsif ($NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT =~ m/^DEFAULT_refseq_archaea$/i ) {
			$NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT = "ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/archaea/assembly_summary.txt";
			print LOG "DEFAULT_refseq_archaea file is ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/archaea/assembly_summary.txt\n" unless $VERBOSE =~ m/^OFF$/i;
		} elsif ($NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT =~ m/^DEFAULT_genbank_bacteria$/i ) {
			$NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT = "ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/assembly_summary.txt";
			print LOG "DEFAULT_genbank_bacteria file is ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/assembly_summary.txt\n" unless $VERBOSE =~ m/^OFF$/i;
		} elsif ($NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT =~ m/^DEFAULT_refseq_bacteria$/i ) {
			$NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT = "ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt";
			print LOG "DEFAULT_refseq_bacteria file is ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt\n" unless $VERBOSE =~ m/^OFF$/i;
		}

		if ($NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT =~ m/^ftp\:\/\/(.+)\/(.+)$/i ) {
			my $base_url = $1;
			my $file_name = $2;
			#ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/assembly_summary.txt", "ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/archaea/assembly_summary.txt"
			my @words_from_url = $base_url =~ /(\w+(?:'\w+)*)/g;
			my $join_words_from_url = join('_',@words_from_url);
			#$output_backtick = `mkdir -p ${DIR_TMP_DOWNLOAD}/${join_words_from_url}/`;
			#$output_backtick = `cd ${DIR_TMP_DOWNLOAD}/${join_words_from_url}/`;
			print LOG "Downloading ftp file $NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT\n" unless $VERBOSE =~ m/^OFF$/i; # in directory ${DIR_TMP_DOWNLOAD}/${join_words_from_url} with command:\ncurl https://$base_url/$file_name -o "${DIR_TMP_DOWNLOAD}/${join_words_from_url}/$file_name" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error
			#$output_backtick = `wget https://$base_url/$file_name . &> /dev/null`;
			$output_backtick = `curl https://$base_url/$file_name -o "${DIR_TMP_DOWNLOAD}/${join_words_from_url}/$file_name" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error`;
			if ($output_backtick eq "") {
				#ok
			} else {
				print LOG "Error could not complete download NCBI_ASSEMBLY_SUMMARY_FILE if ftp in download_genome_files_from_ncbi.pl : $!\n";
				die("Error could not complete download NCBI_ASSEMBLY_SUMMARY_FILE if ftp in download_genome_files_from_ncbi.pl : $!\n");
			}
			push ( @NCBI_ASSEMBLY_SUMMARY_FILE , "${DIR_TMP_DOWNLOAD}/${join_words_from_url}/$file_name") ;
			print LOG "Finished downloading NCBI_ASSEMBLY_SUMMARY_FILE file ${DIR_TMP_DOWNLOAD}/${join_words_from_url}/$file_name\n" unless $VERBOSE =~ m/^OFF$/i;
		} elsif ( -e "$NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT" ) {
			push ( @NCBI_ASSEMBLY_SUMMARY_FILE , "$NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT") ;
			print LOG "Added NCBI_ASSEMBLY_SUMMARY_FILE file $NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT\n" unless $VERBOSE =~ m/^OFF$/i;
		} else {
			print LOG "Error NCBI_ASSEMBLY_SUMMARY_FILE $NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT could not be processed : $!\n";
			die("Error NCBI_ASSEMBLY_SUMMARY_FILE $NCBI_ASSEMBLY_SUMMARY_FILE_PRE_IT could not be processed : $!\n");
		}
	}
	print LOG "Number of NCBI assembly file(s) found = ".scalar (@NCBI_ASSEMBLY_SUMMARY_FILE) ." \n" unless $VERBOSE =~ m/^OFF$/i;
	print LOG "Done retrieving NCBI assembly summary file(s)\n" unless $VERBOSE =~ m/^OFF$/i;
}


# parse @UNIPROT_GENOME_TABLE_FILE needs to be before @ASSEMBLY_NCBI because the latter files are more complete regarding url_path and will overrides
#parse @NCBI_ASSEMBLY_SUMMARY_FILE to add to %assembly_accession_of_interest2ftp_path
# read each assembly file and process each line to see if the assembly meet our criteria
# assembly_accession	bioproject	biosample	wgs_master	refseq_category	taxid	species_taxid	organism_name	infraspecific_name	isolate	version_status	assembly_level	release_type	genome_rep	seq_rel_date	asm_name	submitter	gbrs_paired_asm	paired_asm_comp	ftp_path	excluded_from_refseq
#GCF_000010525.1	PRJNA224116	SAMD00060925		representative genome	438753	7	Azorhizobium caulinodans ORS 571	strain=ORS 571		latest	Complete Genome	Major	Full	2007/10/16	ASM1052v1	University of Tokyo	GCA_000010525.1	identical	ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/525/GCF_000010525.1_ASM1052v1	
if(@NCBI_ASSEMBLY_SUMMARY_FILE){
	print LOG "\nStart parsing ".scalar (@NCBI_ASSEMBLY_SUMMARY_FILE)." NCBI assembly summary file(s)\n" unless $VERBOSE =~ m/^OFF$/i;
	ASSEMBLY_SUMMARY_FILE: foreach my $NCBI_ASSEMBLY_SUMMARY_FILE_IT (@NCBI_ASSEMBLY_SUMMARY_FILE) {

		if (@RESTRICT_TO_TAXA_DOMAIN) {
			my $pass_test_RESTRICT_TO_TAXA_DOMAIN = 0;
			foreach my $RESTRICT_TO_TAXA_DOMAIN_IT (@RESTRICT_TO_TAXA_DOMAIN) {
				if ( $NCBI_ASSEMBLY_SUMMARY_FILE_IT =~ m/$RESTRICT_TO_TAXA_DOMAIN_IT/i ) {
					$pass_test_RESTRICT_TO_TAXA_DOMAIN = 1;
				}
			}
			if ($pass_test_RESTRICT_TO_TAXA_DOMAIN == 0) {
				print LOG "The file $NCBI_ASSEMBLY_SUMMARY_FILE_IT doesn't pass the RESTRICT_TO_DATABANK argument and will not be treated\n" unless $VERBOSE =~ m/^OFF$/i;
				next ASSEMBLY_SUMMARY_FILE;
			} else {
				print LOG "The file $NCBI_ASSEMBLY_SUMMARY_FILE_IT pass the RESTRICT_TO_DATABANK argument and will be treated\n" unless $VERBOSE =~ m/^OFF$/i;
			}
		}

		# lecture du fichier NCBI_ASSEMBLY_SUMMARY_FILE_IT
		print LOG "Start processing assembly summary file $NCBI_ASSEMBLY_SUMMARY_FILE_IT\n" unless $VERBOSE =~ m/^OFF$/i;
		open( NCBI_ASSEMBLY_SUMMARY_FILE_IT_FH, "<$NCBI_ASSEMBLY_SUMMARY_FILE_IT" ) or die("Can not open $NCBI_ASSEMBLY_SUMMARY_FILE_IT\n");
		my $header_found = 0;
		my %header_column_name_to_column_position_ncbi_assembly = ();
		my $index_assembly_accession = -1;
		my $index_version_status = -1;
		my $index_assembly_level = -1;
		my $index_ftp_path = -1;
		my $index_taxid = -1;
		my $index_gbrs_paired_asm = -1;
		my $index_paired_asm_comp = -1;
		my $index_excluded_from_refseq = -1;

		my $Regex_RESTRICT_TO_DATABANK = ""; # can be A for genbank, F for refseq, or a combination of both for both
		if (@RESTRICT_TO_DATABANK) {
			#{genbank | refseq} ; assembly_accession start with GCA_ for genbank and GCF_ for refseq
			foreach my $RESTRICT_TO_DATABANK_IT (@RESTRICT_TO_DATABANK) {
				if ($RESTRICT_TO_DATABANK_IT eq "genbank") {
					$Regex_RESTRICT_TO_DATABANK = $Regex_RESTRICT_TO_DATABANK."A";
				} elsif ($RESTRICT_TO_DATABANK_IT eq "refseq") {
					$Regex_RESTRICT_TO_DATABANK = $Regex_RESTRICT_TO_DATABANK."F";
				} else {
					print LOG "Error parsing \@RESTRICT_TO_DATABANK : value $RESTRICT_TO_DATABANK_IT is not supported, must be genbank or refseq\n";
					die("Error parsing \@RESTRICT_TO_DATABANK : value $RESTRICT_TO_DATABANK_IT is not supported, must be genbank or refseq\n");
				}
			}
		}


		LINE_IN_FILE_ASSEMBLY_SUMMARY: while ( my $line = <NCBI_ASSEMBLY_SUMMARY_FILE_IT_FH> ) {
			chomp($line);
			$line =~ s/^\s+//;
			$line =~ s/\s+$//;

			if ( $line =~ m/^#\s+assembly_accession/ ) {
				print LOG "Processing header...\n" unless $VERBOSE =~ m/^OFF$/i;
				$header_found = 1;
				$line =~ s/^#\s+//;
				my @fields = split( '\t', $line );

				for my $i (0 .. $#fields) {
					my $field_IT = $fields[$i];
					$header_column_name_to_column_position_ncbi_assembly{$field_IT} = $i ;
				}
				# check for mandatory header
				foreach my $mandatory_column_assembly_file_IT (@list_mandatory_columns_in_assembly_file) {
					if ( ! exists $header_column_name_to_column_position_ncbi_assembly{$mandatory_column_assembly_file_IT} ) {
						print LOG "Error reading assembly file $NCBI_ASSEMBLY_SUMMARY_FILE_IT : mandatory column $mandatory_column_assembly_file_IT not found in header\n";
						die("Error reading assembly file $NCBI_ASSEMBLY_SUMMARY_FILE_IT : mandatory column $mandatory_column_assembly_file_IT not found in header\n");
					} elsif ($mandatory_column_assembly_file_IT eq "assembly_accession") {
						$index_assembly_accession = $header_column_name_to_column_position_ncbi_assembly{"assembly_accession"};
					} elsif ($mandatory_column_assembly_file_IT eq "version_status") {
						$index_version_status = $header_column_name_to_column_position_ncbi_assembly{"version_status"};
					} elsif ($mandatory_column_assembly_file_IT eq "assembly_level") {
						$index_assembly_level = $header_column_name_to_column_position_ncbi_assembly{"assembly_level"};
					} elsif ($mandatory_column_assembly_file_IT eq "ftp_path") {
						$index_ftp_path = $header_column_name_to_column_position_ncbi_assembly{"ftp_path"};
					} elsif ($mandatory_column_assembly_file_IT eq "taxid") {
						$index_taxid = $header_column_name_to_column_position_ncbi_assembly{"taxid"};
					} elsif ($mandatory_column_assembly_file_IT eq "gbrs_paired_asm") {
						$index_gbrs_paired_asm = $header_column_name_to_column_position_ncbi_assembly{"gbrs_paired_asm"};
					} elsif ($mandatory_column_assembly_file_IT eq "paired_asm_comp") {
						$index_paired_asm_comp = $header_column_name_to_column_position_ncbi_assembly{"paired_asm_comp"};
					} elsif ($mandatory_column_assembly_file_IT eq "excluded_from_refseq") {
						$index_excluded_from_refseq = $header_column_name_to_column_position_ncbi_assembly{"excluded_from_refseq"};
					}
				
				}

				print LOG "Done processing header...\n" unless $VERBOSE =~ m/^OFF$/i;
			} elsif ( $line =~ m/^#/ ) {
				print LOG "Skipping comment : $line\n" unless $VERBOSE =~ m/^OFF$/i;
				next LINE_IN_FILE_ASSEMBLY_SUMMARY;
			} else {
				if ($header_found != 1) {
					print LOG "Error reading assembly file $NCBI_ASSEMBLY_SUMMARY_FILE_IT : no header found at begining of file\n";
					die("Error reading assembly file $NCBI_ASSEMBLY_SUMMARY_FILE_IT : no header found at begining of file\n");
				}
				my @fields = split( '\t', $line );

				# Tests on RESTRICT
				if (@RESTRICT_TO_DATABANK) {
					# #{genbank | refseq} ; assembly_accession start with GCA_ for genbank and GCF_ for refseq
					if ( $fields[$index_assembly_accession] =~ m/^GC[$Regex_RESTRICT_TO_DATABANK]_/i ) {
						#ok pass the test
					} else {
						#do not pass the test
						print LOG "the field assembly_accession ".$fields[$index_assembly_accession]." of the line \n$line\n do not pass the RESTRICT_TO_DATABANK test (REGEX = ^GC[$Regex_RESTRICT_TO_DATABANK]_), it will be ignored\n" unless $VERBOSE =~ m/^OFF$/i;
						next LINE_IN_FILE_ASSEMBLY_SUMMARY;
					}
				}

				if ( $RESTRICT_TO_LATEST_ASSEMBLY =~ m/^ON$/i ) {
					# "latest" version_status (column 11) in ASSEMBLY_SUMMARY file
					if ( $fields[$index_version_status] =~ m/^latest$/i ) {
						#ok pass the test
					} else {
						#do not pass the test
						print LOG "the field version_status ".$fields[$index_version_status]." of the line \n$line\n do not pass the RESTRICT_TO_LATEST_ASSEMBLY test, it will be ignored\n" unless $VERBOSE =~ m/^OFF$/i;
						next LINE_IN_FILE_ASSEMBLY_SUMMARY;
					}
				}

				if ( $RESTRICT_TO_COMPLETE_GENOME =~ m/^ON$/i ) {
					# "Complete Genome" assembly_level (column 12)in ASSEMBLY_SUMMARY file
					if ( $fields[$index_assembly_level] =~ m/^Complete\s+Genome$/i ) {
						#ok pass the test
					} else {
						#do not pass the test
						print LOG "the field assembly_level ".$fields[$index_assembly_level]." of the line \n$line\n do not pass the RESTRICT_TO_COMPLETE_GENOME test, it will be ignored\n" unless $VERBOSE =~ m/^OFF$/i;
						next LINE_IN_FILE_ASSEMBLY_SUMMARY;
					}
				}

				#@RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES : fields taxid AND %restrict_to_hash_taxon_id
				if (@RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES) {
					my $taxid_IT = $fields[$index_taxid];
					if ( exists $restrict_to_hash_taxon_id{$taxid_IT} ) {
						#ok pass the test
					} else {
						#do not pass the test
						print LOG "the field taxid $taxid_IT #of the line \n$line\n do not pass the RESTRICT_TO_LIST_TAXON_IDS_AND_SUBNODES test, it will be ignored\n" unless $VERBOSE =~ m/^OFF$/i;
						next LINE_IN_FILE_ASSEMBLY_SUMMARY;
					}
				}

				if (@RESTRICT_TO_LIST_ASSEMBLY_ACCESSION) {
					my $assembly_accession_IT = $fields[$index_assembly_accession];
					my $assembly_accession_no_version_IT = $assembly_accession_IT;
					if ($assembly_accession_IT =~ m/^(.+)\.\d+$/i) {
						$assembly_accession_no_version_IT = $1;
					}
					if ( exists $restrict_to_hash_assembly_accession{$assembly_accession_IT} || exists $restrict_to_hash_assembly_accession{$assembly_accession_no_version_IT} ) {
						#ok pass the test
					} else {
						#do not pass the test
						print LOG "the field assembly_accession ".$fields[$index_assembly_accession]." of the line \n$line\n do not pass the RESTRICT_TO_LIST_ASSEMBLY_ACCESSION test, it will be ignored\n" unless $VERBOSE =~ m/^OFF$/i;
						next LINE_IN_FILE_ASSEMBLY_SUMMARY;
					}
				}
				

				if ($SKIP_GENBANK_IF_REFSEQ_AVAILABLE =~ m/^ON$/i) {
					#print "HERE ".$index_gbrs_paired_asm." = ".$fields[$index_gbrs_paired_asm]."\n" ;
					if ( $fields[$index_gbrs_paired_asm] =~ m/^GCF_.+$/i
						) {

						
						my $excluded_from_refseq_IT = $fields[$index_excluded_from_refseq];
						if ($excluded_from_refseq_IT eq "") {
							#do not pass the test
							print LOG "the field assembly_level ".$fields[$index_gbrs_paired_asm]." of the line \n$line\n do not pass the SKIP_GENBANK_IF_REFSEQ_AVAILABLE test, it will be ignored\n" unless $VERBOSE =~ m/^OFF$/i;
							next LINE_IN_FILE_ASSEMBLY_SUMMARY;
						} else {
							#ok pass the test
							print "WARNING excluded_from_refseq\n$line\n";
						}
					} else {
						#ok pass the test
					}
				}


				#did pass all test, add assembly accnum to assembly_accession_of_interest2ftp_path
				my $assembly_accession_to_store = $fields[$index_assembly_accession];
				my $ftp_path_to_store = $fields[$index_ftp_path];
				if ($assembly_accession_to_store eq "") {
					print LOG "The assembly accession in the line $line is empty \n" unless $VERBOSE =~ m/^OFF$/i;
				} else {
					if ( exists $assembly_accession_of_interest2ftp_path{$assembly_accession_to_store} ) {
						print LOG "The assembly accession in the line $line is redondant \n" unless $VERBOSE =~ m/^OFF$/i;
					}
					$assembly_accession_of_interest2ftp_path{$assembly_accession_to_store} = $ftp_path_to_store;
				}
			}
		}
		close(NCBI_ASSEMBLY_SUMMARY_FILE_IT_FH);
		print LOG "Done processing assembly summary file $NCBI_ASSEMBLY_SUMMARY_FILE_IT\n" unless $VERBOSE =~ m/^OFF$/i;
	}
	print LOG "Done parsing NCBI assembly summary file(s)\n" unless $VERBOSE =~ m/^OFF$/i;
}


print LOG "\n\nA total of ".scalar (keys %assembly_accession_of_interest2ftp_path)." assembly accession have been found to be adequat from input files\n" unless $VERBOSE =~ m/^OFF$/i;


$counter_loop = 0;
foreach my $assembly_accession_IT ( sort ( keys %assembly_accession_of_interest2ftp_path ) ) {
	push ( @assembly_accession_to_treat_pre, $assembly_accession_IT );
	$counter_loop++;
	if($LIMIT_TO_X_FIRST_ORDERED_SAMPLES > 0 && $counter_loop >= $LIMIT_TO_X_FIRST_ORDERED_SAMPLES){
		last;
	}
}
if($LIMIT_TO_X_FIRST_ORDERED_SAMPLES > 0) {
	print LOG "\n\nA total of ".scalar (@assembly_accession_to_treat_pre)." assembly accession remains after the cutoff LIMIT_TO_X_FIRST_ORDERED_SAMPLES\n" unless $VERBOSE =~ m/^OFF$/i;
}

if ( $LIMIT_TO_X_RANDOM_SAMPLES > 0 ) {

	for (my $i = 0; $i < $LIMIT_TO_X_RANDOM_SAMPLES; $i++) {
		#my $last_index_in_array_pre = $#assembly_accession_to_treat_pre;
		#print LOG "\$last_index_in_array_pre = $last_index_in_array_pre\n";
		if ( ! @assembly_accession_to_treat_pre ) {
			#empty array exit
			last;
		}
		my $minimum_random = 0;
		my $maximum_random = $#assembly_accession_to_treat_pre;
		my $random_index = $minimum_random + int(rand($maximum_random - $minimum_random));
		#print LOG "\$i = $i ; \$minimum_random = $minimum_random  ; \$maximum_random = $maximum_random ; \$random_index = $random_index\n";
		push ( @assembly_accession_to_treat, $assembly_accession_to_treat_pre[$random_index] );
		#delete $assembly_accession_to_treat_pre[$random_index];
		splice @assembly_accession_to_treat_pre, $random_index, 1;
	}
	print LOG "\n\nA total of ".scalar (@assembly_accession_to_treat)." assembly accession remains after the cutoff LIMIT_TO_X_RANDOM_SAMPLES\n" unless $VERBOSE =~ m/^OFF$/i;
} else {
	push ( @assembly_accession_to_treat, @assembly_accession_to_treat_pre );
}

print LOG "\n\nA total of ".scalar (@assembly_accession_to_treat)." assembly accession are registered to be processed :\n".join(', ',@assembly_accession_to_treat)."\n" unless $VERBOSE =~ m/^OFF$/i;


sub chomp_and_remove_ws_and_cr {
	my ( $string_to_process ) = @_;
	chomp($string_to_process);
	$string_to_process =~ s/^\s+//;
	$string_to_process =~ s/\s+$//;
	$string_to_process =~ s/\r|\n//g;
	return $string_to_process;
}

#for each %assembly_accession_of_interest2ftp_path and @RESTRICT_TO_FILENAME_OF_INTEREST, fill %SOFTLINKS_TO_CREATE or %URL_FILES_TO_DOWNLOAD
$counter_loop = 0;
foreach my $assembly_accession_IT ( @assembly_accession_to_treat ) {
	my $ftp_path_IT = undef;

	$counter_loop++;
	print LOG "\tDetermining path for assembly accession $assembly_accession_IT ($counter_loop / ".scalar (@assembly_accession_to_treat).")\n" unless $VERBOSE =~ m/^OFF$/i;
	my $redo_counter = 0;
	FILENAME_OF_INTEREST: foreach my $FILENAME_OF_INTEREST_IT ( @RESTRICT_TO_FILENAME_OF_INTEREST ) {

		$redo_counter++;

		#@SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE
		foreach my $LOCAL_PUBLIC_REPOSITORY_IT ( @SOFTLINK_LOCAL_PUBLIC_REPOSITORY_IF_AVAILABLE ) {
			#find /projet/mig/work/tlacroix/pipeline_origami_STABLE/ORIGAMI_bank/TMP_LOCAL_PUBLIC_REPOSITORY/ -type f -name '*GCA_001316345.1*_genomic.gbff*'
			my $list_local_rep = `find $LOCAL_PUBLIC_REPOSITORY_IT -type f -name '*$assembly_accession_IT*$FILENAME_OF_INTEREST_IT*'`;
			my @lines_list_local_rep = split( '\n', $list_local_rep );
			if ( ! @lines_list_local_rep ) {
				# nothing found
				print LOG "no file found in local repository $LOCAL_PUBLIC_REPOSITORY_IT for assembly accession $assembly_accession_IT and filename of interest $FILENAME_OF_INTEREST_IT. For detail run command:\nfind $LOCAL_PUBLIC_REPOSITORY_IT -type f -name '*$assembly_accession_IT*$FILENAME_OF_INTEREST_IT*\n" unless $VERBOSE =~ m/^OFF$/i;
			} elsif ( scalar(@lines_list_local_rep) == 1 ) {
				# ok 1 match
				if ($list_local_rep =~ m/^.+\/(.+?)$/i) {
					my $stripped_list_local_rep = chomp_and_remove_ws_and_cr($list_local_rep);
					$SOFTLINKS_TO_CREATE{$1} = $stripped_list_local_rep;
					next FILENAME_OF_INTEREST;
				} else {
					print LOG "Error getting suffix filename out of \$list_local_rep $list_local_rep\n";
					die("Error getting suffix filename out of \$list_local_rep $list_local_rep\n");
				}
			} else {
				# multiples matches, choose none
				print LOG "multiples file found in local repository $LOCAL_PUBLIC_REPOSITORY_IT for assembly accession $assembly_accession_IT and filename of interest $FILENAME_OF_INTEREST_IT. For detail run command:\nfind $LOCAL_PUBLIC_REPOSITORY_IT -type f -name '*$assembly_accession_IT*$FILENAME_OF_INTEREST_IT*\n" unless $VERBOSE =~ m/^OFF$/i;
			}
		}


		# SOFTLINK_LOCAL_PUBLIC_REPOSITORY, failed, try url
		# set $ftp_path_IT for first time in loop
		if ( ! defined $ftp_path_IT ) {
			if ( ! exists $assembly_accession_of_interest2ftp_path{$assembly_accession_IT} ) {
				# need to find full rep with assembly name from listing remote directory
				if($assembly_accession_IT =~ m/^(GC[AF])_(\d{3})(\d{3})(\d{3})\.\d+$/i ){
					my $assembly_accession_slice1 = $1;
					my $assembly_accession_slice2 = $2;
					my $assembly_accession_slice3 = $3;
					my $assembly_accession_slice4 = $4;
					my $list_dir_ftp = `curl -l https://ftp.ncbi.nlm.nih.gov/genomes/all/$assembly_accession_slice1/$assembly_accession_slice2/$assembly_accession_slice3/$assembly_accession_slice4/ --retry 2 --retry-delay 5 --silent --show-error`;
					my @lines_list_dir_ftp = split( '\n', $list_dir_ftp );
					my $assembly_name = undef;
					foreach my $line_IT ( @lines_list_dir_ftp ) {
						if ($line_IT =~ m/${assembly_accession_IT}_(.+?)[\/\"]/i ) {
							if ($assembly_name) {
								print LOG "Error listing ftp dir : \$assembly_name $assembly_name is already set from output\n$list_dir_ftp\nbut line \n$line_IT\nattempt to set it again. Possible multiple path.\n";
								die("Error listing ftp dir : \$assembly_name $assembly_name is already set from output\n$list_dir_ftp\nbut line \n$line_IT\nattempt to set it again. Possible multiple path.\n");
							} else {
								$assembly_name = $1;
							}
						}
					}
					if ($assembly_name) {
						$ftp_path_IT = "ftp://ftp.ncbi.nlm.nih.gov/genomes/all/$assembly_accession_slice1/$assembly_accession_slice2/$assembly_accession_slice3/$assembly_accession_slice4/${assembly_accession_IT}_${assembly_name}";
						print LOG "identified ftp path $ftp_path_IT for assembly accession $assembly_accession_IT with ftp directory listing\n" unless $VERBOSE =~ m/^OFF$/i;
					} else {
						print LOG "WARNING listing ftp dir : Skipping $assembly_accession_IT because no \$assembly_name found in $list_dir_ftp. \n";
						if ($DIE_ON_WARNING  =~ m/^ON$/i) {
							die("WARNING listing ftp dir : Skipping $assembly_accession_IT because no \$assembly_name found in $list_dir_ftp. \n");
						}
					}
				} else {
					print LOG "Error parsing \$assembly_accession_IT $assembly_accession_IT : do not match regex ^(GC[AF]_(\\d{3})(\\d{3})(\\d{3})\\.\\d+)\$\n";
					die("Error parsing \$assembly_accession_IT $assembly_accession_IT : do not match regex ^(GC[AF]_(\\d{3})(\\d{3})(\\d{3})\\.\\d+)\$\n");
				}
			} else {
				$ftp_path_IT = $assembly_accession_of_interest2ftp_path{$assembly_accession_IT};
				print LOG "\$ftp_path_IT already store in \%assembly_accession_of_interest2ftp_path : $ftp_path_IT\n" unless $VERBOSE =~ m/^OFF$/i;
			}
		} else {
			print LOG "\$ftp_path_IT already set: $ftp_path_IT\n" unless $VERBOSE =~ m/^OFF$/i;
		}

		$ftp_path_IT =~ s/^\s*ftp:\/\//https:\/\//;

		if ($ftp_path_IT =~ m/\/$/i ) {
			#ok
		} else {
			$ftp_path_IT .= "/";
		}
		my $list_files_ftp = `curl -l $ftp_path_IT --retry 2 --retry-delay 5 --silent --show-error`;
		my @lines_list_files_ftp = split( '\n', $list_files_ftp );
		my $ftp_file_of_interset = undef;
		my $filename_ftp_file_of_interset = undef;
		foreach my $line_IT ( @lines_list_files_ftp ) {
			if ($line_IT =~ m/href=\"(${assembly_accession_IT}.+${FILENAME_OF_INTEREST_IT}.*)\"/i ) {
				if ($ftp_file_of_interset) {
					print LOG "Error listing file ${FILENAME_OF_INTEREST_IT} in remote url \"curl -l $ftp_path_IT\" : \$ftp_file_of_interset $ftp_file_of_interset was set but line\n$line_IT\nattempt to set the value also.\n";
					die("Error listing file ${FILENAME_OF_INTEREST_IT} in remote url \"curl -l $ftp_path_IT\" : \$ftp_file_of_interset $ftp_file_of_interset was set but line\n$line_IT\nattempt to set the value also.\n");
				} else {
					$filename_ftp_file_of_interset = $1;
					$ftp_file_of_interset = "$ftp_path_IT/$filename_ftp_file_of_interset";
				}
			}
		}

		if ($ftp_file_of_interset) {
			$redo_counter = 0;
			$URL_FILES_TO_DOWNLOAD{$filename_ftp_file_of_interset} = $ftp_file_of_interset;
		} else {

			if ($redo_counter < 5) {
				print LOG "sleep 8 second and try again ($redo_counter times) listing file ${FILENAME_OF_INTEREST_IT} in remote url \"curl -l $ftp_path_IT\"\n";
				usleep(8000000);
				redo FILENAME_OF_INTEREST;
			} else {
				$redo_counter = 0;
				print LOG "WARNING listing file ${FILENAME_OF_INTEREST_IT} in remote url \"curl -l $ftp_path_IT\" : no ftp file of interset was found. The assembly accession $assembly_accession_IT will be skipped.\n";
				if ($DIE_ON_WARNING  =~ m/^ON$/i) {
					die("WARNING listing file ${FILENAME_OF_INTEREST_IT} in remote url \"curl -l $ftp_path_IT\" : no ftp file of interset was found. The assembly accession $assembly_accession_IT will be skipped.\n");
				}
			}
		}
	}
}









#deal with %SOFTLINKS_TO_CREATE
if (%SOFTLINKS_TO_CREATE) {
	print LOG "\n\nA total of ".scalar ( keys %SOFTLINKS_TO_CREATE)." softlink(s) to create have been found\n" unless $VERBOSE =~ m/^OFF$/i;
	if ($LIST_AND_EXIT =~ m/^ON$/i) {
		print LOG "argument LIST_AND_EXIT is ON, skipping actual creation of softlinks\n" unless $VERBOSE =~ m/^OFF$/i;
	} else {
		foreach my $filesuffix_IT ( keys %SOFTLINKS_TO_CREATE ) {
			my $softlink_to_create_IT = $SOFTLINKS_TO_CREATE{$filesuffix_IT};
			#if ( -e "$SiteConfig::BANKDIR/$filesuffix_IT" || -l "$SiteConfig::BANKDIR/$filesuffix_IT" ) {
			#	print LOG "Error could not softlink, file \"$SiteConfig::BANKDIR/$filesuffix_IT\" already exists\n";
			#	die("Error could not softlink, file \"$SiteConfig::BANKDIR/$filesuffix_IT\" already exists\n");
			#}
			$output_backtick = `ln -s $softlink_to_create_IT $SiteConfig::BANKDIR/$filesuffix_IT`;
			if ($output_backtick eq "") {
				#ok
				print LOG "Done ln -s $softlink_to_create_IT $SiteConfig::BANKDIR/$filesuffix_IT\n" unless $VERBOSE =~ m/^OFF$/i;
			} else {
				print LOG "Error could not softlink ln -s $softlink_to_create_IT \"$SiteConfig::BANKDIR/$filesuffix_IT\" : $!\n";
				die("Error could not softlink ln -s $softlink_to_create_IT \"$SiteConfig::BANKDIR/$filesuffix_IT\" : $!\n");
			}
		}
		print LOG "Done creating ".scalar (keys %SOFTLINKS_TO_CREATE)." softlink(s)\n" unless $VERBOSE =~ m/^OFF$/i;
	}
}





#deal with %URL_FILES_TO_DOWNLOAD
if(%URL_FILES_TO_DOWNLOAD){
=pod
RQ : best protocol to use to download data : use rsync
Replace the "ftp:" at the beginning of the FTP path with "rsync:". E.g. If the FTP path is ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/696/305/GCF_001696305.1_UCN72.1, then the directory and its contents could be downloaded using the following rsync command:
rsync --copy-links --recursive --times --verbose rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/696/305/GCF_001696305.1_UCN72.1 my_dir/
A file with FTP path ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/696/305/GCF_001696305.1_UCN72.1/GCF_001696305.1_UCN72.1_genomic.gbff.gz could be downloaded using the following rsync command:
rsync --copy-links --times --verbose rsync://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/001/696/305/GCF_001696305.1_UCN72.1/GCF_001696305.1_UCN72.1_genomic.gbff.gz my_dir/
BUT NOT WORKING IN MY ENVIRONEMENT? USING curl 
=cut
	print LOG "\n\nA total of ".scalar ( keys %URL_FILES_TO_DOWNLOAD)." url file(s) have been found to be downloaded\n"; #  unless $VERBOSE =~ m/^OFF$/i
	if ($LIST_AND_EXIT =~ m/^ON$/i) {
		print LOG "argument LIST_AND_EXIT is ON, skipping actual downloading of files\n"; #unless $VERBOSE =~ m/^OFF$/i
	} else {
		foreach my $filesuffix_IT ( keys %URL_FILES_TO_DOWNLOAD ) {
			my $url_to_download_IT = $URL_FILES_TO_DOWNLOAD{$filesuffix_IT};
			$output_backtick = `curl $url_to_download_IT -o "$SiteConfig::BANKDIR/$filesuffix_IT" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error`;
			if ($output_backtick eq "") {
				#ok
				print LOG "Done download curl $url_to_download_IT -o \"$SiteConfig::BANKDIR/$filesuffix_IT\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error\n"; #  unless $VERBOSE =~ m/^OFF$/i
			} else {
				print LOG "Error could not complete download curl $url_to_download_IT -o \"$SiteConfig::BANKDIR/$filesuffix_IT\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n";
				die("Error could not complete download curl $url_to_download_IT -o \"$SiteConfig::BANKDIR/$filesuffix_IT\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error : $!\n");
			}
		}
		print LOG "Done downloading ".scalar (keys %URL_FILES_TO_DOWNLOAD)." url file(s)\n"; # unless $VERBOSE =~ m/^OFF$/i
	}
}


if ( $DO_NOT_DELETE_DIR_TMP_DOWNLOAD =~ m/^OFF$/i ) {
	$output_backtick = `$SiteConfig::CMDDIR/rm -rf $DIR_TMP_DOWNLOAD/*`;
}


#end script
print LOG
"\n---------------------------------------------------\n\n\n download_genome_files_from_ncbi.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);










