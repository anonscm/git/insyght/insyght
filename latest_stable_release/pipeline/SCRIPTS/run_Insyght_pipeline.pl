#!/usr/local/bin/perl
#
# This is the master script that run the pipeline and insert data in the database
# insert primary data from genome files into the database
#
#
#

use 5.014;
use strict;
use warnings;
use SiteConfig;
use ORIGAMI;
use Time::HiRes qw(usleep gettimeofday tv_interval);
use BlastConfig;




# whole script scoped variable
my $FEEDBACK_PROGRESS_STEPS = "ON";
my $output_backtick = "";
my $GENERAL_FORMULA_TO_DO = "**formula to be implemented**";
my $GENERAL_FORMULA_number_new_organisms = "the number of new organisms";
my $GENERAL_FORMULA_number_previously_inserted_organisms = "the number of previously inserted organisms";
my $GENERAL_FORMULA_number_clusters_organism_best_adjust = "the number of clusters for organisms (".$BlastConfig::number_clusters_organism_best_adjust.")";
my $GENERAL_FORMULA_number_elements = "the number elements";
my $GENERAL_FORMULA_number_total_number_organisms_with_genes = "the total number of organisms with genes";
my $GENERAL_FORMULA_pairwise_comparaison_organisms = "the number of pairwise comparaison for organisms";
my $calculate_FORMULA_pairwise_comparaison_cluster_best_adjust = ($BlastConfig::number_clusters_organism_best_adjust * ($BlastConfig::number_clusters_organism_best_adjust+1))/2;
my $GENERAL_FORMULA_pairwise_comparaison_cluster_best_adjust = "the number of pairwise comparaison for clusters ($calculate_FORMULA_pairwise_comparaison_cluster_best_adjust)";
my $GENERAL_FORMULA_1 = "1";
my $finished_date = "";
my $runtime = "";
my $t0 = [gettimeofday];
# through args
my $VERBOSE = "";
my $NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS = "";
#my $FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES = "";
my $DELETE_FAILURE_FROM_PREVIOUS_RUN = "-DELETE_FAILURE_FROM_PREVIOUS_RUN";
my $MAX_JOBS = "";
my $CMD_CLUSTER = "";
my $PROGRAM_TO_USE = "";
my $NUM_PROC_PER_BLAST_PS = "";
my $NO_PARALOG = "";
my $MAIN_ALIGN_OPTION_os = "";
my $MAIN_ALIGN_OPTION_hs = "";
my $MAIN_ALIGN_OPTION_mp = "";
my $MAIN_ALIGN_OPTION_gc = "";
my $MAIN_ALIGN_OPTION_ge = "";
my $MAIN_ALIGN_OPTION_m = "";
my $MAIN_ALIGN_OPTION_c = "";
my $MAIN_ALIGN_OPTION_o = "";
my $MAIN_ALIGN_OPTION_pf = "";
my $SKIP_INTEGRATE_NCBI_TAXON_DATA = "OFF";
my $PRINT_FEEDBACK_PROGRESS_SAME_LINE = "ON";

sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	print LOG "$error_mssg\n";
	$finished_date = scalar(localtime);
	$runtime = tv_interval ($t0, [gettimeofday]);
	if ($NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS ne "") {
		`echo -e "Hello,\nThe script run_Insyght_pipeline.pl has finished but was not successfully completed at $finished_date (it took $runtime seconds).\nThe error message is:\n$error_mssg\nSee the log file $SiteConfig::LOGDIR/run_Insyght_pipeline/run_Insyght_pipeline.log for more details.\nBest regards.\nInsyght notification" | mail -s 'notification run_Insyght_pipeline.pl NOT successfully completed' -r "insyght_notification_no_reply" $NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS`;
	}
	die("$error_mssg\n");
	exit -1;
}


#set the variables according to the argument or exit with usage if the arguments are incorrect
foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = "-VERBOSE ".$ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl run_Insyght_pipeline.pl -VERBOSE = {ON, OFF}");
		}
	}
#PRINT_FEEDBACK_PROGRESS_SAME_LINE
	 elsif ( $ARGV[$argnum] =~ m/^-PRINT_FEEDBACK_PROGRESS_SAME_LINE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$PRINT_FEEDBACK_PROGRESS_SAME_LINE = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl run_Insyght_pipeline.pl -PRINT_FEEDBACK_PROGRESS_SAME_LINE = {ON, OFF}");
		}
	}
#SKIP_INTEGRATE_NCBI_TAXON_DATA
	elsif ( $ARGV[$argnum] =~ m/^-SKIP_INTEGRATE_NCBI_TAXON_DATA$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$SKIP_INTEGRATE_NCBI_TAXON_DATA = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -SKIP_INTEGRATE_NCBI_TAXON_DATA argument ; usage : perl run_Insyght_pipeline.pl -SKIP_INTEGRATE_NCBI_TAXON_DATA = {ON, OFF}");
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-FEEDBACK_PROGRESS_STEPS$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FEEDBACK_PROGRESS_STEPS = $ARGV[ $argnum + 1 ];
		} else {
			#print "incorrect -FEEDBACK_PROGRESS_STEPS argument ; usage : perl run_Insyght_pipeline.pl -FEEDBACK_PROGRESS_STEPS = {ON, OFF}";
			die_with_error_mssg("incorrect -FEEDBACK_PROGRESS_STEPS argument ; usage : perl run_Insyght_pipeline.pl -FEEDBACK_PROGRESS_STEPS = {ON, OFF}");
		}
	}
# NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS
	elsif ( $ARGV[$argnum] =~ m/^-NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/.+@.+/i  )
		{
			$NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS = $ARGV[ $argnum + 1 ];
		} else {
			#print "incorrect -NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS argument ; usage : perl run_Insyght_pipeline.pl -NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS = {your_email_adress\@domain}";
			die_with_error_mssg("incorrect -NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS argument ; usage : perl run_Insyght_pipeline.pl -NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS = {your_email_adress AT domain}");
		}
	}
=pod
# FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES
	elsif ( $ARGV[$argnum] =~ m/^-FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES = "-FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES ".$ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES argument ; usage : perl run_Insyght_pipeline.pl -FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES = {ON, OFF}";
			die_with_error_mssg("incorrect -FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES argument ; usage : perl run_Insyght_pipeline.pl -FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES = {ON, OFF}");
		}
	}
=cut
#my $DELETE_FAILURE_FROM_PREVIOUS_RUN = "";
	elsif ( $ARGV[$argnum] =~ m/^-DELETE_FAILURE_FROM_PREVIOUS_RUN$/ ) {
	
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i) {
			$DELETE_FAILURE_FROM_PREVIOUS_RUN = "-DELETE_FAILURE_FROM_PREVIOUS_RUN";
		} elsif ($ARGV[ $argnum + 1 ] =~ m/^OFF$/i) {
			$DELETE_FAILURE_FROM_PREVIOUS_RUN = "";
		} else {
			#print LOG "incorrect -DELETE_FAILURE_FROM_PREVIOUS_RUN argument ; usage : perl run_Insyght_pipeline.pl -DELETE_FAILURE_FROM_PREVIOUS_RUN = {ON, OFF}";
			die_with_error_mssg("incorrect -DELETE_FAILURE_FROM_PREVIOUS_RUN argument ; usage : perl run_Insyght_pipeline.pl -DELETE_FAILURE_FROM_PREVIOUS_RUN = {ON, OFF}");
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-MAX_JOBS$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_JOBS = "-MAX_JOBS ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] < 1){
				die_with_error_mssg("incorrect -MAX_JOBS argument ; usage : perl run_Insyght_pipeline.pl -MAX_JOBS {INTEGER > 0}");
			}
		}
		else {
			die_with_error_mssg("incorrect -MAX_JOBS argument ; usage : perl run_Insyght_pipeline.pl -MAX_JOBS {INTEGER > 0}");
		}

	}
	elsif ( $ARGV[$argnum] =~ m/^-CMD_CLUSTER/ ) {
		if ( $ARGV[$argnum + 1] =~ m/^0$/i ) {
			#do nothing
		} else {
			$CMD_CLUSTER = "-CMD_CLUSTER \"".$ARGV[ $argnum + 1 ]."\"";
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-PROGRAM_TO_USE/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^BLAST$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^PLAST$/i )
		{
			$PROGRAM_TO_USE = "-PROGRAM_TO_USE ".$ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -PROGRAM_TO_USE argument ; usage : perl run_Insyght_pipeline.pl -PROGRAM_TO_USE {BLAST, PLAST}");
		}

	}
	elsif ( $ARGV[$argnum] =~ m/^-NUM_PROC_PER_BLAST_PS/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i )
		{
			$NUM_PROC_PER_BLAST_PS = "-NUM_PROC_PER_BLAST_PS ".$ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -NUM_PROC_PER_BLAST_PS argument ; usage : perl run_Insyght_pipeline.pl -NUM_PROC_PER_BLAST_PS {Integer}");
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-NO_PARALOG$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$NO_PARALOG = "-NO_PARALOG ".$ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -NO_PARALOG argument ; usage : perl run_Insyght_pipeline.pl -NO_PARALOG = {ON, OFF}");
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_os$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i)
		{
			$MAIN_ALIGN_OPTION_os = "-MAIN_ALIGN_OPTION_os ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] < 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_os argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_os {int > 0}");
			}
		} else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_os argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_os {int > 0}");
		}

	}
	elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_hs$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i)
		{
			$MAIN_ALIGN_OPTION_hs = "-MAIN_ALIGN_OPTION_hs ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] < 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_hs argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_hs {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_hs argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_hs {int > 0}");
		}
	}	
	elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_mp$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[-\d]+$/i)
		{
			$MAIN_ALIGN_OPTION_mp = "-MAIN_ALIGN_OPTION_mp ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] > 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_mp argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_mp {int < 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_mp argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_mp {int < 0}");
		}

	}
	elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_gc$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[-\d]+$/i)
		{
			$MAIN_ALIGN_OPTION_gc = "-MAIN_ALIGN_OPTION_gc ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] > 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_gc argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_gc {int < 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_gc argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_gc {int < 0}");
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_ge$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[-\d]+$/i)
		{
			$MAIN_ALIGN_OPTION_ge = "-MAIN_ALIGN_OPTION_ge ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] > 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_ge argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_ge {int < 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_ge argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_ge {int < 0}");
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_m$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i)
		{
			$MAIN_ALIGN_OPTION_m = "-MAIN_ALIGN_OPTION_m ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] < 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_m argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_m {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_m argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_m {int > 0}");
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_c$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i)
		{
			$MAIN_ALIGN_OPTION_c = "-MAIN_ALIGN_OPTION_c ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] < 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_c argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_c {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_c argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_c {int > 0}");
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_o$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[01]$/i)
		{
			$MAIN_ALIGN_OPTION_o = "-MAIN_ALIGN_OPTION_o ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] == 0
				|| $ARGV[ $argnum + 1 ] == 1){

			}else{
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_o argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_o {0 or 1}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_o argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_o {0 or 1}");
		}

	}
	elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_pf$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[\d\.]+$/i)
		{
			$MAIN_ALIGN_OPTION_pf = "-MAIN_ALIGN_OPTION_pf ".$ARGV[ $argnum + 1 ];
			if($ARGV[ $argnum + 1 ] < 0
				|| $ARGV[ $argnum + 1 ] > 1){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_pf argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_pf {double between 0 and 1. ex : 0.5}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_pf argument ; usage : perl run_Insyght_pipeline.pl  -MAIN_ALIGN_OPTION_pf {double between 0 and 1. ex : 0.5}");
		}
	}
	elsif ( $ARGV[$argnum] =~ m/^-[^\d]/ ) {
		die_with_error_mssg ("The argument $ARGV[$argnum] is not recognized.");
	}

}



# step variables to define after command line options
my @list_steps_to_run = (
	#"check and prepare genomes files"
	"generate primary data"
	,"drop database indexes before insertion of primary data"
	,"insert primary data into database"
	,"recreate database indexes after insertion of primary data" 
	,"delete temporary DATADIR files"
	,"count inserted data in database"
	,"generate fasta proteome"
	,"makeblastdb"
	,"generate blast launcher files"
	,"launch blast"
	,"generate syntenies finder launcher files"
	,"launch finder evolutionary conserved CDS"
	,"generate launcher files for output primary key counter"
	,"launch output primary key counter files"
	,"generate correct primary key launcher files"
	,"launch correct primary key"
	,"drop database indexes before insertion of syntenies data"
	,"insert homologies and syntenies data into database"
	,"validate organisms as being inserted"
	,"recreate database indexes after insertion of syntenies data" 
	,"count inserted data in database"
	,"calculate whole genome data score"
	,"insert whole genome data score into database"
	,"generate specific NCBI TaxonId Files"
	,"insert specific NCBI TaxonId Files into database"
	,"vacuum full database"
	,"check database consistency"
	,"delete temporary DATADIR files"
);

my %step_to_command = (
        # WAS "check and prepare genomes files" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/List_check_and_split_genomes_files.pl $VERBOSE",
        # WAS "generate primary data" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/Task_add_entry_generator.pl $VERBOSE $FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES $DELETE_FAILURE_FROM_PREVIOUS_RUN",
        "generate primary data" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/generate_organisms_annotations_as_sql__one_orga_per_file.pl $VERBOSE $DELETE_FAILURE_FROM_PREVIOUS_RUN",
        "drop database indexes before insertion of primary data" => "psql --quiet -f $SiteConfig::SCRIPTSDIR/../../database/SQL/drop_idx_before_primary_data_insert.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1",
        "insert primary data into database" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/Task_add_entry_integrator.pl $VERBOSE",
        "recreate database indexes after insertion of primary data" => "psql --quiet -f $SiteConfig::SCRIPTSDIR/../../database/SQL/recreate_idx_after_primary_data_insert.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1",
        "delete temporary DATADIR files" => "rm -rf $SiteConfig::DATADIR/* 2>&1 1>/dev/null", #redirect STDERR to STDOUT and then STDOUT to dev/null
        "count inserted data in database" => "NA", # Not Applicable
        "generate fasta proteome" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/Task_generate_fasta.pl $VERBOSE",
        "makeblastdb" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/Task_blast_all_launch_makeblastdb_IDRIS.pl $VERBOSE $MAX_JOBS $CMD_CLUSTER",
        "generate blast launcher files" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/Task_blast_all_generator_for_IDRIS.pl $VERBOSE $PROGRAM_TO_USE $NUM_PROC_PER_BLAST_PS $NO_PARALOG",
        "launch blast" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/Task_blast_all_launch_cluster_blast_IDRIS.pl $VERBOSE $MAX_JOBS $CMD_CLUSTER",
        "generate syntenies finder launcher files" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/Task_add_alignment_generator_for_IDRIS.pl $VERBOSE $MAIN_ALIGN_OPTION_os $MAIN_ALIGN_OPTION_hs $MAIN_ALIGN_OPTION_mp $MAIN_ALIGN_OPTION_gc $MAIN_ALIGN_OPTION_ge $MAIN_ALIGN_OPTION_m $MAIN_ALIGN_OPTION_c $MAIN_ALIGN_OPTION_o $MAIN_ALIGN_OPTION_pf", # -GZIP_AND_MV_OUTPUT_IN_BASH ON -COPY_ALIGN_BIN_TO_NODE_TMP OFF -GUNZIP_AND_GZIP_BLAST_INPUT_CLUSTER_MODE ON
        "launch finder evolutionary conserved CDS" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/Task_add_alignment_launch_jobs.pl $VERBOSE $MAX_JOBS $CMD_CLUSTER",
        "generate launcher files for output primary key counter" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/generate_count_primary_key_increment.pl $VERBOSE",
        "launch output primary key counter files" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/launch_count_primary_key_increment.pl $VERBOSE $MAX_JOBS $CMD_CLUSTER",
        "generate correct primary key launcher files" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/generate_correct_primary_key_launcher_files.pl $VERBOSE",
        "launch correct primary key" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/launch_correct_primary_key.pl $VERBOSE $MAX_JOBS $CMD_CLUSTER",
        "drop database indexes before insertion of syntenies data" => "psql --quiet -f $SiteConfig::SCRIPTSDIR/../../database/SQL/drop_idx_before_alignment_insert.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1", #redirect STDERR to STDOUT ; could redirect STDOUT to dev/null but --quiet is doing the job
        "insert homologies and syntenies data into database" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/Task_add_alignment_integrator_for_IDRIS.pl $VERBOSE",
        "validate organisms as being inserted" => "psql --quiet -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname -c 'UPDATE organisms set computation_in_process = FALSE WHERE computation_in_process IS TRUE' 2>&1",
        "recreate database indexes after insertion of syntenies data" => "psql --quiet -f $SiteConfig::SCRIPTSDIR/../../database/SQL/recreate_idx_after_alignment_insert.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1", #redirect STDERR to STDOUT ; could redirect STDOUT to dev/null but --quiet is doing the job
        "calculate whole genome data score" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/generate_sorted_list_comp_elements.pl -DO_ALL $VERBOSE",
        "insert whole genome data score into database" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/integrate_sorted_list_comp_orga_whole.pl -DO_ALL $VERBOSE",
        "generate specific NCBI TaxonId Files" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/generate_NCBITaxonomy.pl $VERBOSE",
        "insert specific NCBI TaxonId Files into database" => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/integrate_taxo_data.pl $VERBOSE",
        "vacuum full database" => "psql --quiet -f $SiteConfig::SCRIPTSDIR/../../database/SQL/vacuum_full_tables.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1", #redirect STDERR to STDOUT ; could redirect STDOUT to dev/null but --quiet is doing the job
        "check database consistency" => "NA", # Not Applicable
    );

my %step_to_log_file = (
        #"check and prepare genomes files" => "$SiteConfig::LOGDIR/Add_entries/List_check_and_split_genomes_files.log",
        "generate primary data" => "$SiteConfig::LOGDIR/Task_add_entry/generate_organisms_annotations_as_sql__one_orga_per_file.log",#Task_add_entry_generator
        "drop database indexes before insertion of primary data" => "NA",
        "insert primary data into database" => "$SiteConfig::LOGDIR/Task_add_entry/Task_add_entry_integrator.log",
        "recreate database indexes after insertion of primary data" => "NA",
        "delete temporary DATADIR files" => "NA", # Not Applicable
        "count inserted data in database" => "NA", # Not Applicable
        "generate fasta proteome" => "$SiteConfig::LOGDIR/Task_generate_fasta/Task_generate_fasta.log",
        "makeblastdb" => "$SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_launch_makeblastdb_IDRIS.log",
        "generate blast launcher files" => "$SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_generator_for_IDRIS.log",
        "launch blast" => "$SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_launch_cluster_blast_IDRIS.log",
        "generate syntenies finder launcher files" => "$SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_generator_for_IDRIS.log",
        "launch finder evolutionary conserved CDS" => "$SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_launch_jobs.log",
        "generate launcher files for output primary key counter" => "$SiteConfig::LOGDIR/generate_count_primary_key_increment.log",
        "launch output primary key counter files" => "$SiteConfig::LOGDIR/launch_count_primary_key_increment.log",
        "generate correct primary key launcher files" => "$SiteConfig::LOGDIR/generate_correct_primary_key_launcher_files.log",
        "launch correct primary key" => "$SiteConfig::LOGDIR/launch_correct_primary_key.log",
        # OLD "generate tsv output parser" => "$SiteConfig::LOGDIR/generate_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.log",
        # OLD "launch tsv output parser" => "$SiteConfig::LOGDIR/launch_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.log",
	# OLD "remove mirror synteny data" => "$SiteConfig::LOGDIR/Task_add_alignments/remove_mirror_rows_synteny.log",
        "drop database indexes before insertion of syntenies data" => "NA", # Not Applicable
        "insert homologies and syntenies data into database" => "$SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_integrator_for_IDRIS.log",
        "validate organisms as being inserted" => "NA", # Not Applicable
        "recreate database indexes after insertion of syntenies data" => "NA", # Not Applicable
        "calculate whole genome data score" => "$SiteConfig::LOGDIR/compute/generate_sorted_list_comp_elements.log",
        "insert whole genome data score into database" => "$SiteConfig::LOGDIR/compute/integrate_sorted_list_comp_orga_whole.log",
        "generate specific NCBI TaxonId Files" => "$SiteConfig::LOGDIR/Update_taxo/generate_NCBITaxonomy.log",
        "insert specific NCBI TaxonId Files into database" => "$SiteConfig::LOGDIR/Update_taxo/integrate_taxo_data.log",
        "vacuum full database" => "NA", # Not Applicable
        "check database consistency" => "NA", # Not Applicable
    );


my %step_to_done_marker_file = (
        #"check and prepare genomes files" => "$SiteConfig::LOGDIR/check_and_prepare_genomes_files.done",
        "generate primary data" => "$SiteConfig::LOGDIR/generate_organisms_annotations_as_sql__one_orga_per_file.done",#manage_primary_data_generator
        "drop database indexes before insertion of primary data" => "NA",
        "insert primary data into database" => "$SiteConfig::LOGDIR/manage_primary_data_integrator.done",
        "recreate database indexes after insertion of primary data" => "NA",
        "delete temporary DATADIR files" => "NA", # Not Applicable
        "count inserted data in database" => "NA", # Not Applicable
        "generate fasta proteome" => "$SiteConfig::LOGDIR/generate_fasta_proteome.done",
        "makeblastdb" => "$SiteConfig::LOGDIR/Task_blast_all_launch_makeblastdb_IDRIS.done",
        "generate blast launcher files" => "$SiteConfig::LOGDIR/Task_blast_all_generator_for_IDRIS.done",
        "launch blast" => "$SiteConfig::LOGDIR/Task_blast_all_launch_cluster_blast_IDRIS.done",
        "generate syntenies finder launcher files" => "$SiteConfig::LOGDIR/Task_add_alignment_generator_for_IDRIS.done",
        "launch finder evolutionary conserved CDS" => "$SiteConfig::LOGDIR/Task_add_alignment_launch_jobs.done",
        "generate launcher files for output primary key counter" => "$SiteConfig::LOGDIR/generate_count_primary_key_increment.done",
        "launch output primary key counter files" => "$SiteConfig::LOGDIR/launch_count_primary_key_increment.done",
        "generate correct primary key launcher files" => "$SiteConfig::LOGDIR/generate_correct_primary_key_launcher_files.done",
        "launch correct primary key" => "$SiteConfig::LOGDIR/launch_correct_primary_key.done",
        # OLD "generate tsv output parser" => "$SiteConfig::LOGDIR/generate_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.done",
        # OLD "launch tsv output parser" => "$SiteConfig::LOGDIR/launch_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.done",
	# OLD "remove mirror synteny data" => "$SiteConfig::LOGDIR/remove_mirror_rows_synteny.done",
        "drop database indexes before insertion of syntenies data" => "NA", # Not Applicable
        "insert homologies and syntenies data into database" => "$SiteConfig::LOGDIR/Task_add_alignment_integrator_for_IDRIS.done",
        "validate organisms as being inserted" => "NA", # Not Applicable
        "recreate database indexes after insertion of syntenies data" => "NA", # Not Applicable
        "calculate whole genome data score" => "$SiteConfig::LOGDIR/generate_sorted_list_comp_elements.done",
        "insert whole genome data score into database" => "$SiteConfig::LOGDIR/integrate_sorted_list_comp_orga_whole.done",
        "generate specific NCBI TaxonId Files" => "$SiteConfig::LOGDIR/generate_NCBITaxonomy.done",
        "insert specific NCBI TaxonId Files into database" => "$SiteConfig::LOGDIR/integrate_taxo_data.done",
        "vacuum full database" => "NA", # Not Applicable
        "check database consistency" => "NA", # Not Applicable
    );


my %step_to_error_marker_file = (
        #"check and prepare genomes files" => "$SiteConfig::LOGDIR/check_and_prepare_genomes_files.error",
        "generate primary data" => "$SiteConfig::LOGDIR/generate_organisms_annotations_as_sql__one_orga_per_file.error",#manage_primary_data_generator
        "drop database indexes before insertion of primary data" => "NA",
        "insert primary data into database" => "$SiteConfig::LOGDIR/manage_primary_data_integrator.error",
        "recreate database indexes after insertion of primary data" => "NA",
        "delete temporary DATADIR files" => "NA", # Not Applicable
        "count inserted data in database" => "NA", # Not Applicable
        "generate fasta proteome" => "$SiteConfig::LOGDIR/generate_fasta_proteome.error",
        "makeblastdb" => "$SiteConfig::LOGDIR/Task_blast_all_launch_makeblastdb_IDRIS.error",
        "generate blast launcher files" => "$SiteConfig::LOGDIR/Task_blast_all_generator_for_IDRIS.error",
        "launch blast" => "$SiteConfig::LOGDIR/Task_blast_all_launch_cluster_blast_IDRIS.error",
        "generate syntenies finder launcher files" => "$SiteConfig::LOGDIR/Task_add_alignment_generator_for_IDRIS.error",
        "launch finder evolutionary conserved CDS" => "$SiteConfig::LOGDIR/Task_add_alignment_launch_jobs.error",
        "generate launcher files for output primary key counter" => "$SiteConfig::LOGDIR/generate_count_primary_key_increment.error",
        "launch output primary key counter files" => "$SiteConfig::LOGDIR/launch_count_primary_key_increment.error",
        "generate correct primary key launcher files" => "$SiteConfig::LOGDIR/generate_correct_primary_key_launcher_files.error",
        "launch correct primary key" => "$SiteConfig::LOGDIR/launch_correct_primary_key.error",
        # OLD "generate tsv output parser" => "$SiteConfig::LOGDIR/generate_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.error",
        # OLD "launch tsv output parser" => "$SiteConfig::LOGDIR/launch_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.error",
	# OLD "remove mirror synteny data" => "$SiteConfig::LOGDIR/remove_mirror_rows_synteny.error",
        "drop database indexes before insertion of syntenies data" => "NA", # Not Applicable
        "insert homologies and syntenies data into database" => "$SiteConfig::LOGDIR/Task_add_alignment_integrator_for_IDRIS.error",
        "validate organisms as being inserted" => "NA", # Not Applicable
        "recreate database indexes after insertion of syntenies data" => "NA", # Not Applicable
        "calculate whole genome data score" => "$SiteConfig::LOGDIR/generate_sorted_list_comp_elements.error",
        "insert whole genome data score into database" => "$SiteConfig::LOGDIR/integrate_sorted_list_comp_orga_whole.error",
        "generate specific NCBI TaxonId Files" => "$SiteConfig::LOGDIR/generate_NCBITaxonomy.error",
        "insert specific NCBI TaxonId Files into database" => "$SiteConfig::LOGDIR/integrate_taxo_data.error",
        "vacuum full database" => "NA", # Not Applicable
        "check database consistency" => "NA", # Not Applicable
    );

=pod
	    "check and prepare genomes files" => {
		"Disk space used by genomes files"   => "du -hcs $SiteConfig::BANKDIR | tail -1 | awk -F\" \" '{print \$1}'",
		#"Number of lines in file gr2species_migale"   => "more $SiteConfig::BANKDIR/gr2species_migale.txt | wc -l",
		#"Number of commented lines in genomes master file (gr2species_migale)"   => "more $SiteConfig::BANKDIR/gr2species_migale.txt | grep -E '^#Bact' | wc -l",
		"Number of accession(s)"   => "more $SiteConfig::BANKDIR/gr2species_migale.txt | grep -E '^Bact' | wc -l",
	    },
=cut

my %step_to_hash_post_step_info = (
	    "generate primary data" => {
		"WARNING(S)"   => "grep 'WARNING' $SiteConfig::LOGDIR/Task_add_entry/generate_organisms_annotations_as_sql__one_orga_per_file.log",#Task_add_entry_generator
		#"Number of accession(s)"   => "more $SiteConfig::BANKDIR/gr2species_migale.txt | grep -E '^Bact' | wc -l",
		"Number of files generated (should be 14 x number of accessions)"   => "find $SiteConfig::DATADIR/Task_add_entry/ -type f -name '*.sql' ! -name 'chargebd.sql' | wc -l",
		"Disk space used by generated files"   => "du -hcs $SiteConfig::DATADIR/Task_add_entry/ | tail -1 | awk -F\" \" '{print \$1}'",
		# less $BANK_DIR/../DIR_TMP_DOWNLOAD/elink.out
	    },
	    "drop database indexes before insertion of primary data" => {
		"Size of database so far"   => "psql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname -c '\\l+ $ORIGAMI::dbname' | tail -3 | head -1 | awk -F\"|\" '{print \$7}'",
	    },
	    "insert primary data into database" => {
		"WARNING(S)"   => "grep 'WARNING' $SiteConfig::LOGDIR/Task_add_entry/Task_add_entry_integrator.log",
		"Number of database files processed (should be 14 x number of accessions)"   => "find $SiteConfig::DATADIR/Task_add_entry/ -type f -name '*.sql.gz' | wc -l",
		"Number of database files that remain to be processed (should be 0)"   => "find $SiteConfig::DATADIR/Task_add_entry/ -type f -name '*.sql' ! -name 'chargebd.sql' | wc -l",
		"Disk space used by compressed processed files"   => "du -hcs $SiteConfig::DATADIR/Task_add_entry/ | tail -1 | awk -F\" \" '{print \$1}'",
	    },
	    "recreate database indexes after insertion of primary data" => {
		"Size of database so far"   => "psql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname -c '\\l+ $ORIGAMI::dbname' | tail -3 | head -1 | awk -F\"|\" '{print \$7}'",
	    },
	    "delete temporary DATADIR files" => {
		"Disk space used by DATADIR files after cleaning"   => "du -hcs $SiteConfig::DATADIR/ | tail -1 | awk -F\" \" '{print \$1}'",
		"Disk space left on device"   => "df -hT $SiteConfig::SCRIPTSDIR/List_check_and_split_genomes_files.pl | tail -1 | awk -F\" \" '{print \$5}'",
	    },
	    "count inserted data in database" => {
		"Count inserted data in database"   => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/count_inserted_data.pl",
		"Size of database so far"   => "psql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname -c '\\l+ $ORIGAMI::dbname' | tail -3 | head -1 | awk -F\"|\" '{print \$7}'",
	    },
	    "generate fasta proteome" => {
		"Disk space used by generated files"   => "du -hcs $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/ | tail -1 | awk -F\" \" '{print \$1}'",
		"Number of fasta files generated"   => "find $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/ -type f -name '*.faa' | wc -l",
		"Number of makeblastdb files generated"   => "find $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/ -type f -name '*.makeblastdb.ll' | wc -l",
	    },
	    "makeblastdb" => {
		"Number of .pin files generated"   => "find $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/ -type f -name '*.pin' | wc -l",
		"Number of .phr files generated"   => "find $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/ -type f -name '*.phr' | wc -l",
		"Number of .psq files generated"   => "find $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/ -type f -name '*.psq' | wc -l",
	    },
	    "generate blast launcher files" => {
		"Disk space used by error files (should be 0)"   => "du -hcs $SiteConfig::LOGDIR/Task_blast_all/launch_blast/generate_individual_bash_file_blast_IDRIS_for_* | tail -1 | awk -F\" \" '{print \$1}'",
		"Number of blast launcher files generated"   => "find $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/ -type f -name '*.blast.ll' | wc -l",
	    },
	    "launch blast" => {
		"Number of blast files generated"   => "find $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/ -type f -name '*.blast' | wc -l",
	    },
	    "generate syntenies finder launcher files" => {
		"Number of syntenies archive files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/archive_files/ -type f -name '*.archive' | wc -l",
		"Number of syntenies launcher files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/ -type f -name '*.ll' | wc -l",
	    },
	    "launch finder evolutionary conserved CDS" => {
		"Number of alignment_params tsv files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignment_params/ -type f -name '*_alignment_params_table.tsv.gz' | wc -l",
		"Number of alignment tsv files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignments/ -type f -name '*_alignment_table.tsv.gz' | wc -l",
		"Number of alignment_pairs tsv files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignment_pairs/ -type f -name '*_alignment_pairs_table.tsv.gz' | wc -l",
		"Number of homologie tsv files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/homologies/ -type f -name '*_homologies_table.tsv.gz' | wc -l",
		"Number of tandem_dups tsv files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/tandem_dups/ -type f -name '*_tandem_dups_table.tsv.gz' | wc -l",
		"Number of isBranchedToAnotherSynteny tsv files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/isBranchedToAnotherSynteny/ -type f -name '*_isBranchedToAnotherSynteny_table.tsv.gz' | wc -l",
		"Number of protFusion tsv files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/protFusion/ -type f -name '*_protFusion_table.tsv.gz' | wc -l",
		"Number of closeBestMatchs tsv files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/closeBestMatchs/ -type f -name '*_closeBestMatchs_table.tsv.gz' | wc -l",
		"Number of tmp done files (should be 0)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/ -type f -name '*.main_Align_done' | wc -l",
		"Number of tmp error files (should be 0)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/ -type f -name '*.err' ! -size 0 | wc -l",
		"Number of tmp out files (should be 0)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/ -type f -name '*.out' | wc -l",
	    },
	    "generate launcher files for output primary key counter" => {
		"Number of launcher files for output primary key counter generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/bash_script_submit -type f -name '*.ll' | wc -l",
	    },
	    "launch output primary key counter files" => {
		"Number of launcher files for output primary key counter left to be treated (should be 0)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/bash_script_submit -type f -name '*.ll' | wc -l",
		"Number of launcher files for output primary key counter treated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/bash_script_submit -type f -name '*.ll.gz' | wc -l",
		"Number of count output files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/count_files -type f -name '*.out' | wc -l",
		"Number of tmp done files (should be 0)"   => "find $SiteConfig::LOGDIR/Task_add_alignments/ -type f -name '*count_primary_key_increment_*.done' | wc -l",
		"Number of tmp error files (should be 0)"   => "find $SiteConfig::LOGDIR/Task_add_alignments/ -type f -name '*count_primary_key_increment_*.err' | wc -l",
	    },
	    "generate correct primary key launcher files" => {
		"File incremental_runs_master_file.txt"   => "more $SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit/incremental_runs_master_file.txt | perl -pe 's/\n/ ; /g'",
		"Temporary incremental_runs_master_file.txt.tmp (should be 0)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit/ -type f -name '*incremental_runs_master_file.txt.tmp' | wc -l",
		"Number of correct primary key launcher files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit/ -type f -name '*.ll' | wc -l",
		"Number of count output files left to be treated (should be 0)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/count_files -type f -name '*.out' | wc -l",
		"Number of count output files treated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/count_files -type f -name '*.out.gz' | wc -l",
	    },
	    "launch correct primary key" => {
		"Number of alignment_params sql files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/alignment_params/ -type f -name '*_alignment_params_table.sql' | wc -l",
		"Number of alignment sql files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/alignments/ -type f -name '*_alignment_table.sql' | wc -l",
		"Number of alignment_pairs sql files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/alignment_pairs/ -type f -name '*_alignment_pairs_table.sql' | wc -l",
		"Number of homologie sql files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/homologies/ -type f -name '*_homologies_table.sql' | wc -l",
		"Number of tandem_dups sql files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/tandem_dups/ -type f -name '*_tandem_dups_table.sql' | wc -l",
		"Number of isBranchedToAnotherSynteny sql files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/isBranchedToAnotherSynteny/ -type f -name '*_isBranchedToAnotherSynteny_table.sql' | wc -l",
		"Number of protFusion sql files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/protFusion/ -type f -name '*_protFusion_table.sql' | wc -l",
		"Number of closeBestMatchs sql files generated"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/closeBestMatchs/ -type f -name '*_closeBestMatchs_table.sql' | wc -l",
		"Number of params_scores_algo_syntenies sql files generated (should be 1)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/ -type f -name '*params_scores_algo_syntenies.sql' | wc -l",
		"Number of tmp done files (should be 0)"   => "find $SiteConfig::LOGDIR/Task_add_alignments -type f -name 'correct_primary_key_*.done' | wc -l",
		"Number of tmp error files (should be 0)"   => "find $SiteConfig::LOGDIR/Task_add_alignments -type f -name 'correct_primary_key_*.err' | wc -l",

		"Number of launch correct primary key files processed"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit/ -type f -name '*.ll.gz' | wc -l",
		"Number of files that remain to be processed (should be 0)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit/ -type f -name '*.ll' | wc -l",
	    },
	    "drop database indexes before insertion of syntenies data" => {
		"Size of database so far"   => "psql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname -c '\\l+ $ORIGAMI::dbname' | tail -3 | head -1 | awk -F\"|\" '{print \$7}'",
	    },
	    "insert homologies and syntenies data into database" => {
		"Number of alignment_params sql files processed"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/alignment_params/ -type f -name '*_alignment_params_table.sql.gz' | wc -l",
		"Number of alignment sql files processed"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/alignments/ -type f -name '*_alignment_table.sql.gz' | wc -l",
		"Number of alignment_pairs sql files processed"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/alignment_pairs/ -type f -name '*_alignment_pairs_table.sql.gz' | wc -l",
		"Number of homologie sql files processed"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/homologies/ -type f -name '*_homologies_table.sql.gz' | wc -l",
		"Number of tandem_dups sql files processed"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/tandem_dups/ -type f -name '*_tandem_dups_table.sql.gz' | wc -l",
		"Number of isBranchedToAnotherSynteny sql files processed"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/isBranchedToAnotherSynteny/ -type f -name '*_isBranchedToAnotherSynteny_table.sql.gz' | wc -l",
		"Number of protFusion sql files processed"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/protFusion/ -type f -name '*_protFusion_table.sql.gz' | wc -l",
		"Number of closeBestMatchs sql files processed"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/closeBestMatchs/ -type f -name '*_closeBestMatchs_table.sql.gz' | wc -l",
		"Number of params_scores_algo_syntenies sql files processed (should be 1)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sql/ -type f -name '*params_scores_algo_syntenies.sql.gz' | wc -l",
	    },
	    "validate organisms as being inserted" => {
		"Number of organisms left to process in the database (should be 0)"   => "psql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname -c 'SELECT count(*) FROM organisms WHERE computation_in_process IS TRUE' | tail -3 | head -1 | awk -F\" \" '{print \$1}'",
	    },
	    "recreate database indexes after insertion of syntenies data" => {
		"Size of database so far"   => "psql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname -c '\\l+ $ORIGAMI::dbname' | tail -3 | head -1 | awk -F\"|\" '{print \$7}'",
	    },
	    "calculate whole genome data score" => {
		"Number of whole genome data score files generated (should be 3)"   => "find $SiteConfig::DATADIR/Task_add_alignments/tmp/sorted_list_comp_orga_whole/ -type f -name '*.sql' | wc -l",
	    },
	    "insert whole genome data score into database" => {
		"Count inserted data in database"   => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/count_inserted_data.pl",
		"Size of database so far"   => "psql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname -c '\\l+ $ORIGAMI::dbname' | tail -3 | head -1 | awk -F\"|\" '{print \$7}'",
	    },
	    "generate specific NCBI TaxonId Files" => {
		"Number of taxo.dat files generated (should be 1)"   => "find $SiteConfig::DATADIR/Update_taxo/ -type f -name '*ncbi_taxonomy.dat' | wc -l",
		#"Number of taxo_names.dat files generated (should be 1)"   => "find $SiteConfig::DATADIR/Update_taxo/ -type f -name 'taxo_names.dat' | wc -l",
	    },
	    "insert specific NCBI TaxonId Files into database" => {
		"Count inserted data in database"   => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/count_inserted_data.pl",
		"Size of database so far"   => "psql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname -c '\\l+ $ORIGAMI::dbname' | tail -3 | head -1 | awk -F\"|\" '{print \$7}'",
	    },
	    "vacuum full database" => {
		# Not Applicable
	    },
	    "check database consistency" => {
		"check database consistency"   => "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/check_db_consistency.pl -DB_VERSION 3",
	    },
    );



# $GENERAL_FORMULA_number_new_organisms
# $GENERAL_FORMULA_number_previously_inserted_organisms
# $GENERAL_FORMULA_number_clusters_organism_best_adjust
# $GENERAL_FORMULA_number_elements
# $GENERAL_FORMULA_number_total_number_organisms_with_genes
# $GENERAL_FORMULA_1
sub get_formula_expected_number_fasta_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_number_new_organisms." + 1"
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_number_elements;
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_number_clusters_organism_best_adjust;
		} else {
			return "dependant on the variable \$BlastConfig::avg_number_proteines_per_cluster_best_adjust";
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_fasta_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_makeblastdb_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_1;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_number_elements;
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_number_clusters_organism_best_adjust;
		} else {
			return "dependant on the variable \$BlastConfig::avg_number_proteines_per_cluster_best_adjust";
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_makeblastdb_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}

sub get_formula_expected_number_pin_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_1;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_number_elements;
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_number_clusters_organism_best_adjust;
		} else {
			return "dependant on the variable \$BlastConfig::avg_number_proteines_per_cluster_best_adjust";
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_pin_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}

sub get_formula_expected_number_phr_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_1;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_number_elements;
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_number_clusters_organism_best_adjust;
		} else {
			return "dependant on the variable \$BlastConfig::avg_number_proteines_per_cluster_best_adjust";
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_phr_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}

sub get_formula_expected_number_psq_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_1;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_number_elements;
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_number_clusters_organism_best_adjust;
		} else {
			return "dependant on the variable \$BlastConfig::avg_number_proteines_per_cluster_best_adjust";
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_psq_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_blast_launcher_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		if ($NO_PARALOG =~ m/^-NO_PARALOG ON$/i) {
			return $GENERAL_FORMULA_number_new_organisms." - 1";
		} else {
			return $GENERAL_FORMULA_number_new_organisms;
		}
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_cluster_best_adjust;
		} else {
			return $GENERAL_FORMULA_TO_DO; #TODO
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_blast_launcher_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_blast_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		if ($NO_PARALOG =~ m/^-NO_PARALOG ON$/i) {
			return $GENERAL_FORMULA_number_new_organisms." - 1";
		} else {
			return $GENERAL_FORMULA_number_new_organisms;
		}
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_cluster_best_adjust;
		} else {
			return $GENERAL_FORMULA_TO_DO; #TODO
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_blast_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_syntenies_archive_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_number_total_number_organisms_with_genes;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_number_total_number_organisms_with_genes;
		} else {
			return $GENERAL_FORMULA_number_total_number_organisms_with_genes;
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_syntenies_archive_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_syntenies_launcher_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_cluster_best_adjust;
		} else {
			return $GENERAL_FORMULA_TO_DO; #TODO
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_syntenies_launcher_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_alignment_params_tsv_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_alignment_params_tsv_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_alignment_tsv_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_alignment_tsv_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_alignment_pairs_tsv_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_alignment_pairs_tsv_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_homologie_tsv_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_homologie_tsv_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_of_count_output_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_of_count_output_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_launcher_files_for_output_primary_key_counter_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_launcher_files_for_output_primary_key_counter_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_of_correct_primary_key_launcher_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms;
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_of_correct_primary_key_launcher_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}

=pod OLD
sub get_formula_expected_number_tsv_output_parser_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_TO_DO; #TODO
		} else {
			return $GENERAL_FORMULA_TO_DO; #TODO
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_tsv_output_parser_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
=cut
sub get_formula_expected_number_alignment_params_sql_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_alignment_params_sql_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_alignment_sql_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_alignment_sql_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_alignment_pairs_sql_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_alignment_pairs_sql_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_homologie_sql_files_generated() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_homologie_sql_files_generated unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}
sub get_formula_expected_number_correct_primary_key_files_processed() {
	if ($BlastConfig::taxonomic_granularity eq "ORGANISM_VS_ALL") {
		return $GENERAL_FORMULA_pairwise_comparaison_organisms;
	} elsif ($BlastConfig::taxonomic_granularity eq "ELEMENT_VS_ELEMENT") {
		return $GENERAL_FORMULA_TO_DO; #TODO
	} elsif ($BlastConfig::taxonomic_granularity eq "CLUSTER_ORGANISM") {
		if ($BlastConfig::number_clusters_organism_best_adjust > 0) {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		} else {
			return $GENERAL_FORMULA_pairwise_comparaison_organisms; 
		}
	} else {
		die_with_error_mssg("ERROR get_formula_expected_number_correct_primary_key_files_processed unrecognized \$BlastConfig::taxonomic_granularity : $BlastConfig::taxonomic_granularity");
	}
}




# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log 
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/run_Insyght_pipeline/`;
if ( -e "$SiteConfig::LOGDIR/run_Insyght_pipeline/run_Insyght_pipeline.log" ) {
	`$SiteConfig::CMDDIR/rm -rf $SiteConfig::LOGDIR/run_Insyght_pipeline/run_Insyght_pipeline.log`;
}

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/run_Insyght_pipeline/run_Insyght_pipeline.log"
) or die("Can not open $SiteConfig::LOGDIR/run_Insyght_pipeline/run_Insyght_pipeline.log\n");
#open( LOG, ">$SiteConfig::LOGDIR/run_Insyght_pipeline/run_Insyght_pipeline.log" ) or die("Can not open $SiteConfig::LOGDIR/run_Insyght_pipeline/run_Insyght_pipeline.log\n");

# autoflush
select(LOG);
$| = 1;
select(STDOUT);
$| = 1;



print LOG
"\n---------------------------------------------------\n run_Insyght_pipeline.pl started at :",
  scalar(localtime), "\n\n";

print LOG "** You choose the following specific arguments : \n";
if (defined $VERBOSE && $VERBOSE ne "") {
	print LOG " -VERBOSE : ".$VERBOSE."\n";
}
if (defined $NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS && $NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS ne "") {
	print LOG " -NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS : ".$NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS."\n";
}
=pod
if (defined $FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES && $FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES ne "") {
	print LOG " -FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES : ".$FORCE_SPLIT_ACCNUM_INTO_SEPARATE_ENTRIES."\n";
}
=cut
if (defined $MAX_JOBS && $MAX_JOBS ne "") {
	print LOG " -MAX_JOBS : ".$MAX_JOBS."\n";
}
if (defined $CMD_CLUSTER && $CMD_CLUSTER ne "") {
	print LOG " -CMD_CLUSTER : ".$CMD_CLUSTER."\n";
}
if (defined $PROGRAM_TO_USE && $PROGRAM_TO_USE ne "") {
	print LOG " -PROGRAM_TO_USE : ".$PROGRAM_TO_USE."\n";
}
if (defined $NUM_PROC_PER_BLAST_PS && $NUM_PROC_PER_BLAST_PS ne "") {
	print LOG " -NUM_PROC_PER_BLAST_PS : ".$NUM_PROC_PER_BLAST_PS."\n";
}
if (defined $NO_PARALOG && $NO_PARALOG ne "") {
	print LOG " -NO_PARALOG : ".$NO_PARALOG."\n";
}
if (defined $MAIN_ALIGN_OPTION_os && $MAIN_ALIGN_OPTION_os ne "") {
	print LOG " -MAIN_ALIGN_OPTION_os : ".$MAIN_ALIGN_OPTION_os."\n";
}
if (defined $MAIN_ALIGN_OPTION_hs && $MAIN_ALIGN_OPTION_hs ne "") {
	print LOG " -MAIN_ALIGN_OPTION_hs : ".$MAIN_ALIGN_OPTION_hs."\n";
}
if (defined $MAIN_ALIGN_OPTION_mp && $MAIN_ALIGN_OPTION_mp ne "") {
	print LOG " -MAIN_ALIGN_OPTION_mp : ".$MAIN_ALIGN_OPTION_mp."\n";
}
if (defined $MAIN_ALIGN_OPTION_gc && $MAIN_ALIGN_OPTION_gc ne "") {
	print LOG " -MAIN_ALIGN_OPTION_gc : ".$MAIN_ALIGN_OPTION_gc."\n";
}
if (defined $MAIN_ALIGN_OPTION_ge && $MAIN_ALIGN_OPTION_ge ne "") {
	print LOG " -MAIN_ALIGN_OPTION_ge : ".$MAIN_ALIGN_OPTION_ge."\n";
}
if (defined $MAIN_ALIGN_OPTION_m && $MAIN_ALIGN_OPTION_m ne "") {
	print LOG " -MAIN_ALIGN_OPTION_m : ".$MAIN_ALIGN_OPTION_m."\n";
}
if (defined $MAIN_ALIGN_OPTION_c && $MAIN_ALIGN_OPTION_c ne "") {
	print LOG " -MAIN_ALIGN_OPTION_c : ".$MAIN_ALIGN_OPTION_c."\n";
}
if (defined $MAIN_ALIGN_OPTION_o && $MAIN_ALIGN_OPTION_o ne "") {
	print LOG " -MAIN_ALIGN_OPTION_o : ".$MAIN_ALIGN_OPTION_o."\n";
}
if (defined $MAIN_ALIGN_OPTION_pf && $MAIN_ALIGN_OPTION_pf ne "") {
	print LOG " -MAIN_ALIGN_OPTION_pf : ".$MAIN_ALIGN_OPTION_pf."\n";
}
if (defined $SKIP_INTEGRATE_NCBI_TAXON_DATA && $SKIP_INTEGRATE_NCBI_TAXON_DATA ne "") {
	print LOG " -SKIP_INTEGRATE_NCBI_TAXON_DATA : ".$SKIP_INTEGRATE_NCBI_TAXON_DATA."\n";
}
if (defined $PRINT_FEEDBACK_PROGRESS_SAME_LINE && $PRINT_FEEDBACK_PROGRESS_SAME_LINE ne "") {
	print LOG " -PRINT_FEEDBACK_PROGRESS_SAME_LINE : ".$PRINT_FEEDBACK_PROGRESS_SAME_LINE."\n";
}
print LOG "\n";


# del all marker files %step_to_done_marker_file
print LOG "** Deleting markers files\n";
foreach my $step (keys %step_to_done_marker_file) {
	my $tmp_done_file = $step_to_done_marker_file{$step};
	$output_backtick = `rm -f $tmp_done_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		die_with_error_mssg("Error could not complete rm -f $tmp_done_file :\n$output_backtick \n $!");
	}
}
foreach my $step (keys %step_to_error_marker_file) {
	my $tmp_error_file = $step_to_error_marker_file{$step};
	$output_backtick = `rm -f $tmp_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		die_with_error_mssg("Error could not complete rm -f $tmp_error_file :\n$output_backtick \n $!");
	}
}

sub formated_last_line_log_output {
	my ($raw_last_line_log_output) = @_; 
	chomp $raw_last_line_log_output;
	#my $string_to_return = printf '< %-150.150s >', $raw_last_line_log_output;
	my $string_to_return = "";
	my $standarised_length = 150;
	my $raw_last_line_length = length($raw_last_line_log_output);
	if ($raw_last_line_length == $standarised_length) {
		$string_to_return = $raw_last_line_log_output;
	} elsif ( $raw_last_line_length > $standarised_length) {
		$string_to_return = substr( $raw_last_line_log_output, 0, $standarised_length );
	} else {
		$raw_last_line_log_output .= (" " x ($standarised_length - length($raw_last_line_log_output)));
		$string_to_return = $raw_last_line_log_output;
	}
	return "< ".$string_to_return." >";
}


my $count_step = 0;
my $last_raw_last_line_printed = "";
foreach my $step_to_run_IT (@list_steps_to_run) {

	if ($SKIP_INTEGRATE_NCBI_TAXON_DATA =~ m/^ON$/i  && 
		( $step_to_run_IT eq "generate specific NCBI TaxonId Files"
			|| $step_to_run_IT eq "insert specific NCBI TaxonId Files into database"
		)
	) {
		print LOG "\n** Skipping step $count_step of ".scalar(@list_steps_to_run)." : $step_to_run_IT because of the option -SKIP_INTEGRATE_NCBI_TAXON_DATA $SKIP_INTEGRATE_NCBI_TAXON_DATA\n";
		next;
	}


	$count_step++;
	my $t1 = [gettimeofday];
	print LOG "\n** Step $count_step of ".scalar(@list_steps_to_run)." : $step_to_run_IT\n";

	my $command_line_to_run = "";
	if (exists $step_to_command{$step_to_run_IT}) {
		$command_line_to_run = $step_to_command{$step_to_run_IT};
		chomp($command_line_to_run);
		$command_line_to_run =~ s/^\s+//;
		$command_line_to_run =~ s/\s+$//;
	} else {
		die_with_error_mssg("Error step $step_to_run_IT does not exists in \%step_to_command");
	}
	my $log_file_to_check = "";
	if (exists $step_to_log_file{$step_to_run_IT}) {
		$log_file_to_check = $step_to_log_file{$step_to_run_IT};
	} else {
		die_with_error_mssg("Error step $step_to_run_IT does not exists in \%step_to_log_file");
	}
	my $marker_file_done = "";
	if (exists $step_to_done_marker_file{$step_to_run_IT}) {
		$marker_file_done = $step_to_done_marker_file{$step_to_run_IT};
	} else {
		die_with_error_mssg("Error step $step_to_run_IT does not exists in \%step_to_done_marker_file");
	}
	my $marker_file_error = "";
	if (exists $step_to_error_marker_file{$step_to_run_IT}) {
		$marker_file_error = $step_to_error_marker_file{$step_to_run_IT};
	} else {
		die_with_error_mssg("Error step $step_to_run_IT does not exists in \%step_to_error_marker_file");
	}

	my $step_is_done = 0;
	my $child_pid = undef;

	if ($command_line_to_run eq "NA") {
		$step_is_done = 1;
	} else {
		if ($marker_file_done eq "NA") {
			# does not run in background
			$output_backtick = `$command_line_to_run`;
			if($output_backtick eq ''){
				#ok
			} else {
				die_with_error_mssg("ERROR: $command_line_to_run :\n$output_backtick");		
			}
			$step_is_done = 1;
		} else {

			$child_pid = fork;

			if (!defined $child_pid) {
			    die_with_error_mssg ("ERROR: Cannot fork: $!");
			}
			elsif ($child_pid == 0) {
				# client process
				#exec("$command_line_to_run") or die_with_error_mssg("$command_line_to_run failed: $?"); # problem with script that are waitinf after qsub
				system("$command_line_to_run") == 0 or die_with_error_mssg("$command_line_to_run failed: $?");
				exit 0;
			}
#			else {
#				# parent process
#				waitpid $pid, 0;
#			}

			# OLD
			# system("$command_line_to_run &") == 0 or die_with_error_mssg("$command_line_to_run & failed: $?");

		}
	}

	my $counter_process_not_running = 0;
	while ($step_is_done != 1) {

		# check for $marker_file_error -> script done without error
		if ( -e "$marker_file_error" ) {
			die_with_error_mssg("ERROR. see the end of the file $log_file_to_check for details about the error");
		}

		# check for $marker_file_done -> script done without error
		if ( -e "$marker_file_done" ) {
			if ( $FEEDBACK_PROGRESS_STEPS =~ m/^ON$/ && -e "$log_file_to_check" && -s "$log_file_to_check" ) {#-s  File has nonzero size (returns size in bytes).
				my $formated_last_line_log_output = formated_last_line_log_output(" ");
				if ($PRINT_FEEDBACK_PROGRESS_SAME_LINE =~ m/^ON$/) {
					print "\r$formated_last_line_log_output\n";
				} else {
					print LOG "$formated_last_line_log_output\n";
				}

			}
			$step_is_done = 1;
			# kill child zombie process 
			if (defined $child_pid) {
				waitpid $child_pid, 0;
			}

		} else {

			if (defined $child_pid) {

				# ps -p $child_pid -o command=
				# pmap $child_pid | head -1
				my $command_line_from_pid = `ps h $child_pid | awk -F\" \" '{print \$3}'`;
				chomp($command_line_from_pid);
				$command_line_from_pid =~ s/^\s+//;
				$command_line_from_pid =~ s/\s+$//;
				#if ($command_line_from_pid =~ m/^$child_pid:\s+$command_line_to_run$/) { # for pmap with exec
				#if ($command_line_from_pid eq $command_line_to_run ) { # for ps -p  with exec

				if (defined $command_line_from_pid && $command_line_from_pid ne "" && $command_line_from_pid ne "Z" && $command_line_from_pid ne "Z+") { # for ps -p  with exec
					# ok process running

				} else {
					# process not running or zombie ?
					$counter_process_not_running++;

					if ($counter_process_not_running > 200 ) { # 200 -> wait 20 seconds just in case
						die_with_error_mssg("ERROR: the command :\n$command_line_to_run\nwas associated with the child pid $child_pid but it seems to have been terminated without being completed. Please check the log file for this step ($log_file_to_check) for more information. Sometimes the kernel terminates a process if it exceeds the available RAM or disk space. The minimum RAM needed by this process is 2 GB. Please check the log file of the kernel (i.e. /var/log/kern.log for ubuntu) for more details.");
					}
				}
			}

			# in the meantime, print last line of $log_file_to_check and sleep 100ms
			if ( $FEEDBACK_PROGRESS_STEPS =~ m/^ON$/ && -e "$log_file_to_check" && -s "$log_file_to_check" ) {#-s  File has nonzero size (returns size in bytes).
				my $raw_last_line_log_output = `tail -1 $log_file_to_check`;
				if ( ! defined $raw_last_line_log_output) {
					$raw_last_line_log_output = "";
				}
				if ($raw_last_line_log_output eq $last_raw_last_line_printed
					|| $raw_last_line_log_output eq "") {
					#no new info, do not print anything new
				} else {
					$last_raw_last_line_printed = $raw_last_line_log_output;
					my $formated_last_line_log_output = formated_last_line_log_output($raw_last_line_log_output);
					if ($PRINT_FEEDBACK_PROGRESS_SAME_LINE =~ m/^ON$/) {
						print "\r$formated_last_line_log_output";
					} else {
						print LOG "$formated_last_line_log_output\n";
					}
				}
			}
			# 1 millisecond == 1000 microseconds
			#print LOG "sleep 100 millisecond";
			usleep(100000);
		}
	}

	
	if ( scalar ( keys  %{ $step_to_hash_post_step_info{$step_to_run_IT} } ) > 0) {
		print LOG "Post information for this step:\n" unless ($command_line_to_run eq "NA");
		for my $post_step_info_IT ( sort keys %{ $step_to_hash_post_step_info{$step_to_run_IT} } ) {
			my $post_step_command_IT = $step_to_hash_post_step_info{$step_to_run_IT}{$post_step_info_IT};
			$output_backtick = `$post_step_command_IT`;
			chomp $output_backtick;
			my $post_step_to_print = $post_step_info_IT;
			if ($post_step_info_IT eq "Count inserted data in database") {
				my $int_number_new_organisms = 0;
				my $int_number_previously_inserted_organisms = 0;
				if ( $output_backtick =~ m/Count number of new organisms with genes\t=>\s(\d+)\s/i ) {
					$GENERAL_FORMULA_number_new_organisms .= " = ".$1;
					$int_number_new_organisms = $1;
				}
				if ( $output_backtick =~ m/Count number of previously inserted organisms with genes\t=>\s(\d+)\s/i ) {
					$GENERAL_FORMULA_number_previously_inserted_organisms .= " = ".$1;
					$int_number_previously_inserted_organisms = $1;
				}
				if ( $output_backtick =~ m/Count table elements\t=>\s(\d+)\s/i ) {
					$GENERAL_FORMULA_number_elements .= " (".$1.")";
				}
				if ( $output_backtick =~ m/Count number of total number of organisms with genes\t=>\s(\d+)\s/i ) {
					$GENERAL_FORMULA_number_total_number_organisms_with_genes .= " (".$1.")";
				}
				my $int_pairwise_comparaison_organisms = (($int_number_new_organisms * ($int_number_new_organisms+1))/2) + ($int_number_previously_inserted_organisms * $int_number_new_organisms);
				$GENERAL_FORMULA_pairwise_comparaison_organisms = "the number of pairwise comparaison for organisms ($int_pairwise_comparaison_organisms)";
			} elsif ($post_step_info_IT eq "Number of fasta files generated") {
				$post_step_to_print = $post_step_info_IT."(should be ".get_formula_expected_number_fasta_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of makeblastdb files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_makeblastdb_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of .pin files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_pin_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of .phr files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_phr_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of .psq files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_psq_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of blast launcher files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_blast_launcher_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of blast files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_blast_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of syntenies archive files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_syntenies_archive_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of syntenies launcher files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_syntenies_launcher_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of alignment_params tsv files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_alignment_params_tsv_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of alignment tsv files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_alignment_tsv_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of alignment_pairs tsv files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_alignment_pairs_tsv_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of homologie tsv files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_homologie_tsv_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of launcher files for output primary key counter generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_launcher_files_for_output_primary_key_counter_generated().")";
			} elsif ($post_step_info_IT eq "Number of launcher files for output primary key counter treated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_launcher_files_for_output_primary_key_counter_generated().")";
			} elsif ($post_step_info_IT eq "Number of count output files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_of_count_output_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of correct primary key launcher files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_of_correct_primary_key_launcher_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of count output files treated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_of_count_output_files_generated().")";
			# OLD } elsif ($post_step_info_IT eq "Number of tsv output parser files generated") {
			#	$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_tsv_output_parser_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of alignment_params sql files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_alignment_params_sql_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of alignment sql files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_alignment_sql_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of alignment_pairs sql files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_alignment_pairs_sql_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of homologie sql files generated") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_homologie_sql_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of launch correct primary key files processed") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_correct_primary_key_files_processed().")";
			} elsif ($post_step_info_IT eq "Number of alignment_params sql files processed") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_alignment_params_sql_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of alignment sql files processed") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_alignment_sql_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of alignment_pairs sql files processed") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_alignment_pairs_sql_files_generated().")";
			} elsif ($post_step_info_IT eq "Number of homologie sql files processed") {
				$post_step_to_print = $post_step_info_IT." (should be ".get_formula_expected_number_homologie_sql_files_generated().")";
			}

			print LOG "\t $post_step_to_print : $output_backtick\n";
		}
	}
	print LOG "Successfully finished in ".tv_interval ($t1, [gettimeofday])." seconds    \n";
	if ($log_file_to_check ne "NA") {
		print LOG "See log file $log_file_to_check for details.\n";
	}

}


# end of script

$finished_date = scalar(localtime);
$runtime = tv_interval ($t0, [gettimeofday]);
print LOG
"\n** Your data have been successfully inserted into the database, run the Insyght web application to browse the results\n\n---------------------------------------------------\nrun_Insyght_pipeline.pl successfully completed at $finished_date (it took $runtime seconds)\n\n";
close(LOG);

if ($NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS ne "") {
	`echo -e "Hello,\nThe script run_Insyght_pipeline.pl has successfully completed at $finished_date (it took $runtime seconds).\nSee the log file $SiteConfig::LOGDIR/run_Insyght_pipeline/run_Insyght_pipeline.log for more details.\nBest regards.\nInsyght notification" | mail -s 'notification run_Insyght_pipeline.pl successfully completed' -r "insyght_notification_no_reply" $NOTIFY_IF_POSSIBLE_JOB_COMPLETION_AT_EMAIL_ADRESS`;
}




