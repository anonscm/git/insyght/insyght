#!/usr/local/bin/perl
#
# Task_add_alignment_generator_for_IDRIS.pl
#
# Example command normal bash no parralelization : perl Task_add_alignment_generator_for_IDRIS.pl
# Example command generate file for IDRIS : perl Task_add_alignment_generator_for_IDRIS.pl -GENERATE_FOR_IDRIS ON
#
# -GENERATE_FOR_IDRIS: generate files to send to IDRIS team in /Task_add_alignments/tmp/archive_files/ and /Task_add_alignments/tmp/cmd_file_idris/.
# RQ: with option -GENERATE_FOR_IDRIS, print all the command in 1 file without header for qsub
# -MAIN_ALIGN_OPTION_os : ortholog score default value 4. int > 0.
# -MAIN_ALIGN_OPTION_hs : homolog score default value 2. int > 0.
# -MAIN_ALIGN_OPTION_mp : mismatch penalty score default value -4. int < 0.
# -MAIN_ALIGN_OPTION_gc : gap creation score default value -8. int < 0.
# -MAIN_ALIGN_OPTION_ge : gap extension score default value -2. int < 0.
# -MAIN_ALIGN_OPTION_m : min_align_size score default value 1. int > 0.
# -MAIN_ALIGN_OPTION_c : cutoff_score score default value 8. int > 0.
# -MAIN_ALIGN_OPTION_o : include_all_orthologs score default value 1. 1 or 0.
# -MAIN_ALIGN_OPTION_pf : min protein fraction for ortholog score default value 0.5. between 0 and 1.
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#


use strict;
use Time::HiRes qw(usleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;
use Find_new_old_molecules;
use BlastConfig;

# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_generator_for_IDRIS.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/Task_add_alignment_generator_for_IDRIS.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/Task_add_alignment_generator_for_IDRIS.error";
my $output_backtick = "";
my $VERBOSE = "ON";
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my @list_new_molecule_ids		            = ();
my @list_old_molecule_ids		            = ();
my %molecule_id_2_size		            = ();
my %element_id2orga_id	            = ();
my %mainClusterOrgaId2AlAllOragIdsInCluster	            = ();
my $molecule_type;
my $GENERATE_FOR_IDRIS = "OFF";
my $MAIN_ALIGN_OPTION_os = 4; #ortholog score default value. int > 0.
my $MAIN_ALIGN_OPTION_hs = 2; #homolog score default value. int > 0.
my $MAIN_ALIGN_OPTION_mp = -4; #mismatch penalty score default value. int < 0.
my $MAIN_ALIGN_OPTION_gc = -3; #gap creation score default value. int < 0.
my $MAIN_ALIGN_OPTION_mgsc = 4; # MAX_GAP_SIZE_CREATION_PENALTY
my $MAIN_ALIGN_OPTION_ge = -5000; #gap extension score default value. int < 0.
my $MAIN_ALIGN_OPTION_m = 2; #min_align_size score default value. int > 0.
my $MAIN_ALIGN_OPTION_c = 6; #cutoff_score score default value. int > 0.
my $MAIN_ALIGN_OPTION_o = 1; #include_all_orthologs score default value. 1 or 0.
my $MAIN_ALIGN_OPTION_pf = 0.5; #min protein fraction for ortholog score default value. between 0 and 1.
my $OUTPUT_ARCHIVE_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/archive_files/";
my $DO_ARCHIVE_ONLY = "OFF";
my $GENERATE_WITH_ENVIRONENT_VARIABLES = "OFF";
my $EXPORT_QSUB_LOG_DIR = "";
my $EXPORT_ALIGN_EXEC_PATH = "";
my $EXPORT_BLAST_INPUT_DIR = "";
my $EXPORT_ARCHIVE_INPUT_DIR = "";
my $EXPORT_ALIGN_OUTPUT_DIR = "";
my $EXPORT_MARKER_DONE_ALIGN_DIR = "";
my $GZIP_AND_MV_OUTPUT_IN_BASH = "ON";
my $COPY_ALIGN_BIN_TO_NODE_TMP = "OFF"; # RQ : can lead to a potential error if deleteing a /tmp/program that was about to be run by another node
my $GUNZIP_AND_GZIP_BLAST_INPUT_CLUSTER_MODE = "ON";


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}

foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-GENERATE_FOR_IDRIS$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i ) {
			$GENERATE_FOR_IDRIS = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg ("incorrect -GENERATE_FOR_IDRIS argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -GENERATE_FOR_IDRIS {ON, OFF}");
		}

	}elsif ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -VERBOSE argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -VERBOSE {ON, OFF}");
		}


	}elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_os$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i)
		{
			$MAIN_ALIGN_OPTION_os = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_os < 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_os argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_os {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_os argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_os {int > 0}");
		}

	}elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_hs$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i)
		{
			$MAIN_ALIGN_OPTION_hs = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_hs < 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_hs argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_hs {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_hs argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_hs {int > 0}");
		}

	}elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_mp$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[-\d]+$/i)
		{
			$MAIN_ALIGN_OPTION_mp = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_mp > 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_mp argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_mp {int < 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_mp argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_mp {int < 0}");
		}

	}elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_gc$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[-\d]+$/i)
		{
			$MAIN_ALIGN_OPTION_gc = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_gc > 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_gc argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_gc {int < 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_gc argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_gc {int < 0}");
		}
#MAIN_ALIGN_OPTION_mgsc
	}elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_mgsc$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[-\d]+$/i)
		{
			$MAIN_ALIGN_OPTION_mgsc = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_mgsc <= 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_mgsc argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_mgsc {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_mgsc argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_mgsc {int > 0}");
		}
	}elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_ge$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[-\d]+$/i)
		{
			$MAIN_ALIGN_OPTION_ge = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_ge > 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_ge argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_ge {int < 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_ge argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_ge {int < 0}");
		}

	}elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_m$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i)
		{
			$MAIN_ALIGN_OPTION_m = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_m < 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_m argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_m {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_m argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_m {int > 0}");
		}

	}elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_c$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i)
		{
			$MAIN_ALIGN_OPTION_c = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_c < 0){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_c argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_c {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_c argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_c {int > 0}");
		}

	}elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_o$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[01]$/i)
		{
			$MAIN_ALIGN_OPTION_o = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_o == 0
				|| $MAIN_ALIGN_OPTION_o == 1){

			}else{
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_o argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_o {0 or 1}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_o argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_o {0 or 1}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-MAIN_ALIGN_OPTION_pf$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[\d\.]+$/i)
		{
			$MAIN_ALIGN_OPTION_pf = $ARGV[ $argnum + 1 ];
			if($MAIN_ALIGN_OPTION_pf < 0
				|| $MAIN_ALIGN_OPTION_pf > 1){
				die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_pf argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_pf {double between 0 and 1. ex : 0.5}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAIN_ALIGN_OPTION_pf argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl  -MAIN_ALIGN_OPTION_pf {double between 0 and 1. ex : 0.5}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-OUTPUT_ARCHIVE_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$OUTPUT_ARCHIVE_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -OUTPUT_ARCHIVE_DIR argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -OUTPUT_ARCHIVE_DIR {PATH_TO_DIR}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-DO_ARCHIVE_ONLY$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DO_ARCHIVE_ONLY = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -DO_ARCHIVE_ONLY argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -DO_ARCHIVE_ONLY {ON, OFF}");
		}
	}
# GENERATE_WITH_ENVIRONENT_VARIABLES
	 elsif ( $ARGV[$argnum] =~ m/^-GENERATE_WITH_ENVIRONENT_VARIABLES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$GENERATE_WITH_ENVIRONENT_VARIABLES = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -GENERATE_WITH_ENVIRONENT_VARIABLES argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -GENERATE_WITH_ENVIRONENT_VARIABLES {ON, OFF}");
		}
	}
# EXPORT_QSUB_LOG_DIR
	 elsif ( $ARGV[$argnum] =~ m/^-EXPORT_QSUB_LOG_DIR$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_QSUB_LOG_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_QSUB_LOG_DIR argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -EXPORT_QSUB_LOG_DIR {ENVIRONENT_VARIABLE}");
		}
	}
# EXPORT_ALIGN_EXEC_PATH
	 elsif ( $ARGV[$argnum] =~ m/^-EXPORT_ALIGN_EXEC_PATH$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_ALIGN_EXEC_PATH = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_ALIGN_EXEC_PATH argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -EXPORT_ALIGN_EXEC_PATH {ENVIRONENT_VARIABLE}");
		}
	}
# EXPORT_BLAST_INPUT_DIR
	 elsif ( $ARGV[$argnum] =~ m/^-EXPORT_BLAST_INPUT_DIR$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_BLAST_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_BLAST_INPUT_DIR argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -EXPORT_BLAST_INPUT_DIR {ENVIRONENT_VARIABLE}");
		}
	}
# EXPORT_ARCHIVE_INPUT_DIR
	 elsif ( $ARGV[$argnum] =~ m/^-EXPORT_ARCHIVE_INPUT_DIR$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_ARCHIVE_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_ARCHIVE_INPUT_DIR argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -EXPORT_ARCHIVE_INPUT_DIR {ENVIRONENT_VARIABLE}");
		}
	}
# EXPORT_ALIGN_OUTPUT_DIR
	 elsif ( $ARGV[$argnum] =~ m/^-EXPORT_ALIGN_OUTPUT_DIR$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_ALIGN_OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_ALIGN_OUTPUT_DIR argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -EXPORT_ALIGN_OUTPUT_DIR {ENVIRONENT_VARIABLE}");
		}
	}
# EXPORT_MARKER_DONE_ALIGN_DIR = "";
	 elsif ( $ARGV[$argnum] =~ m/^-EXPORT_MARKER_DONE_ALIGN_DIR$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_MARKER_DONE_ALIGN_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_MARKER_DONE_ALIGN_DIR argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -EXPORT_MARKER_DONE_ALIGN_DIR {ENVIRONENT_VARIABLE}");
		}
	}
# GZIP_AND_MV_OUTPUT_IN_BASH
	 elsif ( $ARGV[$argnum] =~ m/^-GZIP_AND_MV_OUTPUT_IN_BASH$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$GZIP_AND_MV_OUTPUT_IN_BASH = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -GZIP_AND_MV_OUTPUT_IN_BASH argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -GZIP_AND_MV_OUTPUT_IN_BASH {ON, OFF}");
		}
	}
# COPY_ALIGN_BIN_TO_NODE_TMP
	 elsif ( $ARGV[$argnum] =~ m/^-COPY_ALIGN_BIN_TO_NODE_TMP$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$COPY_ALIGN_BIN_TO_NODE_TMP = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -COPY_ALIGN_BIN_TO_NODE_TMP argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -COPY_ALIGN_BIN_TO_NODE_TMP {ON, OFF}");
		}
	}
# GUNZIP_AND_GZIP_BLAST_INPUT_CLUSTER_MODE
	 elsif ( $ARGV[$argnum] =~ m/^-GUNZIP_AND_GZIP_BLAST_INPUT_CLUSTER_MODE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$GUNZIP_AND_GZIP_BLAST_INPUT_CLUSTER_MODE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -GUNZIP_AND_GZIP_BLAST_INPUT_CLUSTER_MODE argument ; usage : perl Task_add_alignment_generator_for_IDRIS.pl -GUNZIP_AND_GZIP_BLAST_INPUT_CLUSTER_MODE {ON, OFF}");
		}
	}
}

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_alignments/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_generator_for_IDRIS.log");
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG
"\n---------------------------------------------------\n Task_add_alignments_generator_for_IDRIS.pl started at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;



if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
	#die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}


my $MAIN_ALIGN_OPTIONS_STRING = "-os $MAIN_ALIGN_OPTION_os -hs $MAIN_ALIGN_OPTION_hs -mp $MAIN_ALIGN_OPTION_mp -gc $MAIN_ALIGN_OPTION_gc -mgsc $MAIN_ALIGN_OPTION_mgsc -ge $MAIN_ALIGN_OPTION_ge -m $MAIN_ALIGN_OPTION_m -o $MAIN_ALIGN_OPTION_o -c $MAIN_ALIGN_OPTION_c -pf $MAIN_ALIGN_OPTION_pf";

# del /tmp/ before creating dir otherwise they gt pruned
if ( $DO_ARCHIVE_ONLY =~ m/^OFF$/) {
	print LOG "Deleting files under $SiteConfig::DATADIR/Task_add_alignments/tmp/ \n" unless $VERBOSE =~ m/^OFF$/;
	`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_add_alignments/tmp/`;
}

#deal with rep
`mkdir -p $OUTPUT_ARCHIVE_DIR`;
if($GENERATE_FOR_IDRIS =~ m/^ON$/i){
	`mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_file_idris/`;# if cmd -GENERATE_FOR_IDRIS ON
}else{
	`mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/`;
}

# deleting files
print LOG "Deleting *.archive files under current directory and OUTPUT_ARCHIVE_DIR $OUTPUT_ARCHIVE_DIR\n" unless $VERBOSE =~ m/^OFF$/;
`$SiteConfig::CMDDIR/rm -f *.archive`;
`$SiteConfig::CMDDIR/rm -f -r $OUTPUT_ARCHIVE_DIR/*.archive`;
# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}



#check attribute number_fragment_hit is in table homologies
$ORIGAMI::dbh->selectall_arrayref("SELECT number_fragment_hit FROM homologies LIMIT 1") or die_with_error_mssg("no column number_fragment_hit in table homologies");
#check attribute ortho_min_prot_frac is in table alignment_params
#$ORIGAMI::dbh->selectall_arrayref("SELECT ortho_min_prot_frac FROM alignment_params LIMIT 1") or die "no column ortho_min_prot_frac in table alignment_params";
my ($column_ortho_min_prot_frac_present_alignment_params) = $ORIGAMI::dbh->selectrow_array("SELECT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_schema='public' AND table_name='alignment_params' AND column_name='ortho_min_prot_frac')");
my ($column_ortho_min_prot_frac_present_params_scores_algo_syntenies) = $ORIGAMI::dbh->selectrow_array("SELECT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_schema='public' AND table_name='params_scores_algo_syntenies' AND column_name='ortho_min_prot_frac')");
#print LOG "column_ortho_min_prot_frac_present_alignment_params = $column_ortho_min_prot_frac_present_alignment_params\n";
#print LOG "column_ortho_min_prot_frac_present_params_scores_algo_syntenies = $column_ortho_min_prot_frac_present_params_scores_algo_syntenies\n";
if ( ! $column_ortho_min_prot_frac_present_alignment_params && ! $column_ortho_min_prot_frac_present_params_scores_algo_syntenies ) {
	die_with_error_mssg("ERROR : no column ortho_min_prot_frac in table alignment_params or params_scores_algo_syntenies");
}




# find new old molecules
=pod
from db data
#return (\@list_ordered_new_molecule_id, \@list_ordered_old_molecule_id, \%element_id_2_accnum, \%molecule_id_2_size, \%element_id2orga_id);
my ($list_ordered_new_molecule_id_ref, $list_ordered_old_molecule_id_ref, $molecule_id_2_size_ref, $element_id2orga_id_ref) = Find_new_old_molecules::find_new_old_molecules_from_db_data($BLAST_TAXO_GRANULARITY);
@list_ordered_new_molecule_id = @$list_ordered_new_molecule_id_ref;
@list_ordered_old_molecule_id = @$list_ordered_old_molecule_id_ref;
#%element_id_2_accnum = %$element_id_2_accnum_ref;
%molecule_id_2_size = %$molecule_id_2_size_ref;
%element_id2orga_id = %$element_id2orga_id_ref;
print LOG @list_ordered_new_molecule_id." new elements found.\n";
print LOG @list_ordered_old_molecule_id." old elements found.\n";
=cut
# list *faa files under $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/new_molecules/ and $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/old_molecules/ and store only the molecule ids
sub sub_list_faa_files_recursively {
    my ( $path, $ref_array_to_push ) = @_;
    #my $path = shift;
    opendir (DIR, $path) or do {
		die_with_error_mssg ("Unable to open $path: $!");
    };

    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);

    for (@files) {
        if (-d $_) {
            # directory, run recursively
            #print "$_ is a dir\n";
            sub_list_faa_files_recursively ($_);
        } elsif ($_ =~ m/^.+\/${molecule_type}_([\d_]+)\.faa$/ ) {
	    push ( @$ref_array_to_push, $1);
        }
=pod
	 elsif ($_ =~ m/\.faa~$/ ) {
		#del ~ files
		`$SiteConfig::CMDDIR/rm -f $_`;
	} else {
		die_with_error_mssg("Error in sub_list_faa_files_recursively : the file $_ does not match the regex *.faa\n");
        }
=cut
    }
}
if ( -d "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/new_molecules/" ) {
	sub_list_faa_files_recursively ("$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/new_molecules/", \@list_new_molecule_ids);
}
if ( -d "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/old_molecules/" ) {
	sub_list_faa_files_recursively ("$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/old_molecules/", \@list_old_molecule_ids);
}
print LOG @list_new_molecule_ids." new $BLAST_TAXO_GRANULARITY found : ".join(',', @list_new_molecule_ids)."\n" unless $VERBOSE =~ m/^OFF$/;
print LOG @list_old_molecule_ids." old $BLAST_TAXO_GRANULARITY found".join(',', @list_old_molecule_ids)."\n" unless $VERBOSE =~ m/^OFF$/;



my @list_new_orga_ids = ();
my @list_old_orga_ids = ();
if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	@list_new_orga_ids = @list_new_molecule_ids;
	@list_old_orga_ids = @list_old_molecule_ids;
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	my $filenameListAllCluster = "$SiteConfig::DATADIR/Task_blast_all/tmp/list_cluster/list_cluster.txt";
	open(my $fhListAllCluster, '<:encoding(UTF-8)', $filenameListAllCluster)
	  or die "Could not open file '$filenameListAllCluster' $!";
	while (my $rowListAllCluster = <$fhListAllCluster>) {
		chomp $rowListAllCluster;
		$rowListAllCluster =~ s/^\s+//;
		$rowListAllCluster =~ s/\s+$//;
		if ($rowListAllCluster =~ m/^cluster\s+number\s+=\s+\d+\s+;\s+molecules\s+ids\s+=\s+([\d\s,]+)$/) {
			my @list_orga_ids_IT = split /,\s/, $1;
			my $is_new_orga_ids = 0;
			my $is_old_orga_ids = 0;
			foreach my $new_molecule_idsIT (@list_new_molecule_ids) {
				if ($new_molecule_idsIT == $list_orga_ids_IT[0]) {
					$is_new_orga_ids = 1;
					last;
				}
			}
			if ($is_new_orga_ids == 1) {
				push (@list_new_orga_ids, @list_orga_ids_IT) ;
				$mainClusterOrgaId2AlAllOragIdsInCluster{$list_orga_ids_IT[0]} = \@list_orga_ids_IT;
			} else {
				foreach my $old_molecule_idsIT (@list_old_molecule_ids) {
					if ($old_molecule_idsIT == $list_orga_ids_IT[0]) {
						$is_old_orga_ids = 1;
						last;
					}
				}
				if ($is_old_orga_ids == 1) {
					push (@list_old_orga_ids, @list_orga_ids_IT) ;
					$mainClusterOrgaId2AlAllOragIdsInCluster{$list_orga_ids_IT[0]} = \@list_orga_ids_IT;
				} else {
					die_with_error_mssg("Error could figure out if orgaCluster ".$list_orga_ids_IT[0]." and associated orga ids in file $SiteConfig::DATADIR/Task_blast_all/tmp/list_cluster/list_cluster.txt is old or new");
				}
			}
		}
	}
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}


#run main_GeneList and mv to tmp/archive_files/
print LOG "Creating .archive files...\n" unless $VERBOSE =~ m/^OFF$/;
foreach my $curr_element_id (@list_new_orga_ids, @list_old_orga_ids) {
	#my $main_GeneList_output = 
	`$SiteConfig::ALIGN/main_GeneList $curr_element_id`;
	#if ($main_GeneList_output ne "") {
	#	print LOG "$SiteConfig::ALIGN/main_GeneList $curr_element_id failed :\n$main_GeneList_output\n";
	#	die_with_error_mssg ("$SiteConfig::ALIGN/main_GeneList $curr_element_id failed :\n$main_GeneList_output\n");
	#}
}
print LOG "Done creating .archive files.\n" unless $VERBOSE =~ m/^OFF$/;


print LOG "Moving .archive files to $OUTPUT_ARCHIVE_DIR...\n" unless $VERBOSE =~ m/^OFF$/;
my $output_mv = `$SiteConfig::CMDDIR/mv *.archive $OUTPUT_ARCHIVE_DIR`;
if ($output_mv eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/mv *.archive $OUTPUT_ARCHIVE_DIR : $output_mv");
}
print LOG "Done moving .archive files to $OUTPUT_ARCHIVE_DIR\n" unless $VERBOSE =~ m/^OFF$/;

if ($DO_ARCHIVE_ONLY =~ m/^ON$/) {
	print LOG "option -DO_ARCHIVE_ONLY is ON, skipping creation of batchfiles\n" unless $VERBOSE =~ m/^OFF$/;
	#end of script
	print LOG
	"\n---------------------------------------------------\n\n\n Task_add_alignments_generator_for_IDRIS.pl successfully completed at :",
	  scalar(localtime),
	  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;
	# touch marker file done script when script done
	$output_backtick = `touch $path_step_done_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
	}
	close(LOG);
	exit;
}


if($GENERATE_FOR_IDRIS =~ m/^ON$/i){
	open( BATCHFILE_FOR_IDRIS,
	"> $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_file_idris/cmd_file_for_IDRIS.ll"
	  )
	  or die_with_error_mssg(
	"Can not open $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_file_idris/cmd_file_for_IDRIS.ll"
	  );
}



sub printFooterOfBashFile {
	my ( $mainMasterOrgaClusterId, $mainSubOrgaClusterId, $QSUB_LOG_DIR, $blast_file_is_versus_all_orga, $BLAST_INPUT_DIR, $new_old_molecules, $MARKER_DONE_ALIGN_DIR ) = @_;

	if ( $GUNZIP_AND_GZIP_BLAST_INPUT_CLUSTER_MODE =~ m/^ON$/ && $BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
		my $end_name_blast_file_sub = "";
		if ( $blast_file_is_versus_all_orga =~ m/^YES$/ ) {
			#$end_name_blast_file_sub = "all";
			# we do not want to gzip file for blast_file_is_versus_all_orga because some other processes in parrallel could be using this file
		} elsif ( $blast_file_is_versus_all_orga =~ m/^NO$/ ) {
			$end_name_blast_file_sub = $mainSubOrgaClusterId;
			print BATCHFILE_CLUSTER_MIG "
gzip $BLAST_INPUT_DIR/${new_old_molecules}/${molecule_type}_${mainMasterOrgaClusterId}/${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${end_name_blast_file_sub}.blast
";
		} else {
			die_with_error_mssg("Error in printFooterOfBashFile: blast_file_is_versus_all_orga neither YES or NO : $blast_file_is_versus_all_orga");
		}
	}


	# COPY_ALIGN_BIN_TO_NODE_TMP
	if ( $COPY_ALIGN_BIN_TO_NODE_TMP =~ m/^ON$/ ) {
		print BATCHFILE_CLUSTER_MIG "if [ -f \"/tmp/main_Align\" ]
then
\trm -f /tmp/main_Align
else
\t:
fi
";
	}

	print BATCHFILE_CLUSTER_MIG "if [ -s \"$QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err\" ] 
then
\t:
elif [ -f \"$QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err\" ] 
then
\trm -f $QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
else
\t:
fi
";

	print BATCHFILE_CLUSTER_MIG "if [ -s \"$QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.out\" ] 
then
\t:
elif [ -f \"$QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.out\" ] 
then
\trm -f $QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.out
else
\t:
fi
";

	# $SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/${molecule_type}_\${master_mol_id_IT}_VS_${molecule_type}_\${mainSubOrgaClusterId}.main_Align_done
	print BATCHFILE_CLUSTER_MIG "mkdir -p $MARKER_DONE_ALIGN_DIR/\n";
	print BATCHFILE_CLUSTER_MIG "touch $MARKER_DONE_ALIGN_DIR/${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.main_Align_done\n";

}


sub printHeaderOfBashFile {
	my ( $master_mol_id_IT, $sub_mol_id_IT, $QSUB_LOG_DIR, $blast_file_is_versus_all_orga, $BLAST_INPUT_DIR, $new_old_molecules, $ALIGN_EXEC_PATH ) = @_;

	print BATCHFILE_CLUSTER_MIG "#!/bin/bash\n";
	#EXPORT_ALIGN_EXEC_PATH
	if ($EXPORT_ALIGN_EXEC_PATH ne "") {
		print BATCHFILE_CLUSTER_MIG "export ALIGN_EXEC_PATH=$EXPORT_ALIGN_EXEC_PATH\n";
	}
	#EXPORT_ARCHIVE_INPUT_DIR
	if ($EXPORT_ARCHIVE_INPUT_DIR ne "") {
		print BATCHFILE_CLUSTER_MIG "export ARCHIVE_INPUT_DIR=$EXPORT_ARCHIVE_INPUT_DIR\n";
	}
	#EXPORT_BLAST_INPUT_DIR
	if ($EXPORT_BLAST_INPUT_DIR ne "") {
		print BATCHFILE_CLUSTER_MIG "export BLAST_INPUT_DIR=$EXPORT_BLAST_INPUT_DIR\n";
	}
	#EXPORT_ALIGN_OUTPUT_DIR
	if ($EXPORT_ALIGN_OUTPUT_DIR ne "") {
		print BATCHFILE_CLUSTER_MIG "export ALIGN_OUTPUT_DIR=$EXPORT_ALIGN_OUTPUT_DIR\n";
	}
	#EXPORT_MARKER_DONE_ALIGN_DIR
	if ($EXPORT_MARKER_DONE_ALIGN_DIR ne "") {
		print BATCHFILE_CLUSTER_MIG "export MARKER_DONE_ALIGN_DIR=$EXPORT_MARKER_DONE_ALIGN_DIR\n";
	}
	#open BATCHFILE_CLUSTER_MIG and print in it

	print BATCHFILE_CLUSTER_MIG "#\$ -S /bin/bash
# (n) No mail is sent ; Avertir au début (b)egin, à la fin (e)nd, à l'éliminaton (a)bort et
# à la suspension (s)uspend d'un job
#\$ -m n
# Adresse mail à laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#\$ -o $QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.out
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -e $QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.err
# Lance la commande depuis le répertoire où est lancé le script
#\$ -cwd\n\n";


	my $end_name_blast_file_sub = "";
	if ( $blast_file_is_versus_all_orga =~ m/^YES$/ ) {
		$end_name_blast_file_sub = "all";
	} elsif ( $blast_file_is_versus_all_orga =~ m/^NO$/ ) {
		$end_name_blast_file_sub = $sub_mol_id_IT;
	} else {
		die_with_error_mssg("Error in printHeaderOfBashFile: blast_file_is_versus_all_orga neither YES or NO : $blast_file_is_versus_all_orga");
	}


	# gunzip input file if necessary
	if ( $GUNZIP_AND_GZIP_BLAST_INPUT_CLUSTER_MODE =~ m/^ON$/ && $BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
		print BATCHFILE_CLUSTER_MIG "if [ -f \"$BLAST_INPUT_DIR/${new_old_molecules}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${end_name_blast_file_sub}.blast.gz\" ]
then
\tgunzip $BLAST_INPUT_DIR/${new_old_molecules}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${end_name_blast_file_sub}.blast.gz
else
\t:
fi
";
	}

	# check for presence of input file
	print BATCHFILE_CLUSTER_MIG "if [ -f \"$BLAST_INPUT_DIR/${new_old_molecules}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${end_name_blast_file_sub}.blast\" ]
then
\t:
else
\ttouch $QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.err
\techo \"file $BLAST_INPUT_DIR/${new_old_molecules}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${end_name_blast_file_sub}.blast not found.\" >> $QSUB_LOG_DIR/sge_err_out/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.err
\texit 1
fi
";

	# COPY_ALIGN_BIN_TO_NODE_TMP
	if ( $COPY_ALIGN_BIN_TO_NODE_TMP =~ m/^ON$/ ) {
		print BATCHFILE_CLUSTER_MIG "cp $ALIGN_EXEC_PATH/main_Align /tmp\n";
	}

	# mv to /tmp
	print BATCHFILE_CLUSTER_MIG "cd /tmp\n";

}




#ref_al_all_orga_ids_in_cluster_master, ref_al_all_orga_ids_in_cluster_sub, new_old_molecules, $blast_file_is_versus_all_orga, $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR, ${master_mol_id_IT}, ${sub_mol_id_IT}
sub printContentOfBashFile {

	my ( $ref_al_all_orga_ids_in_cluster_master, $ref_al_all_orga_ids_in_cluster_sub, $new_old_molecules, $blast_file_is_versus_all_orga, $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR, $mainMasterOrgaClusterId, $mainSubOrgaClusterId) = @_;

	if ( $COPY_ALIGN_BIN_TO_NODE_TMP =~ m/^ON$/ ) {
		$ALIGN_EXEC_PATH = "/tmp";
	}

	# $blast_file_is_versus_all_orga: NO or YES
	my $end_name_blast_file_sub = "";
	if ( $blast_file_is_versus_all_orga =~ m/^YES$/ ) {
		$end_name_blast_file_sub = "all";
	} elsif ( $blast_file_is_versus_all_orga =~ m/^NO$/ ) {
		$end_name_blast_file_sub = $mainSubOrgaClusterId;
	} else {
		die_with_error_mssg("Error in printContentOfBashFile: blast_file_is_versus_all_orga neither YES or NO : $blast_file_is_versus_all_orga");
	}

	my $st_al_all_orga_ids_in_cluster_master = "declare -a al_all_orga_ids_in_cluster_master=(";
	foreach my $orga_ids_in_cluster_masterIt (@{$ref_al_all_orga_ids_in_cluster_master}) {
		$st_al_all_orga_ids_in_cluster_master .= " $orga_ids_in_cluster_masterIt";
	}
	$st_al_all_orga_ids_in_cluster_master .= " )";

	my $st_al_all_orga_ids_in_cluster_sub = "declare -a al_all_orga_ids_in_cluster_sub=(";
	foreach my $orga_ids_in_cluster_subIt (@{$ref_al_all_orga_ids_in_cluster_sub}) {
		$st_al_all_orga_ids_in_cluster_sub .= " $orga_ids_in_cluster_subIt";
	}
	$st_al_all_orga_ids_in_cluster_sub .= " )";
	print BATCHFILE_CLUSTER_MIG "\n";
	print BATCHFILE_CLUSTER_MIG "$st_al_all_orga_ids_in_cluster_master\n";
	print BATCHFILE_CLUSTER_MIG "$st_al_all_orga_ids_in_cluster_sub\n";
	print BATCHFILE_CLUSTER_MIG "al_cluster_master_length=\${#al_all_orga_ids_in_cluster_master[@]}\n";
	print BATCHFILE_CLUSTER_MIG "al_cluster_sub_length=\${#al_all_orga_ids_in_cluster_sub[@]}\n";
	print BATCHFILE_CLUSTER_MIG "for (( i=1; i<\${al_cluster_master_length}+1; i++ ));\ndo\n";

	if ( $mainMasterOrgaClusterId == $mainSubOrgaClusterId) { 
		# do not print redundant if cluster against itself
		print BATCHFILE_CLUSTER_MIG "\tstart_counter_j=\$i\n";
	} else {
		print BATCHFILE_CLUSTER_MIG "\tstart_counter_j=1\n";
	}
	print BATCHFILE_CLUSTER_MIG "\tfor (( j=\${start_counter_j}; j<\${al_cluster_sub_length}+1; j++ ));\n\tdo\n";
	#print BATCHFILE_CLUSTER_MIG "\n# launching align for $master_mol_id_IT VS $sub_mol_id_IT\n";
	# NO NEED : source $SiteConfig::ALIGN/main_GeneList.conf -> export POSTGRESLIB=/usr/local/public/postgres-9.3.9/lib && export LD_LIBRARY_PATH=/usr/local/public/postgres-9.3.9/lib
	# exemple : main_Align -org1 191 -org2 1 -gc -4 -ge -4 -fn orga_191_VS_orga_all.blast -Adir .
	# $SiteConfig::ALIGN/main_Align -org1 ${master_mol_id_IT} -org2 ${sub_mol_id_IT} $MAIN_ALIGN_OPTIONS_STRING -fn $blast_file_IT -Adir $OUTPUT_ARCHIVE_DIR
	print BATCHFILE_CLUSTER_MIG "\t\tmaster_mol_id_IT=\${al_all_orga_ids_in_cluster_master[\$i-1]}\n";
	print BATCHFILE_CLUSTER_MIG "\t\tsub_mol_id_IT=\${al_all_orga_ids_in_cluster_sub[\$j-1]}\n";
	print BATCHFILE_CLUSTER_MIG "\t\t$ALIGN_EXEC_PATH/main_Align -org1 \${master_mol_id_IT} -org2 \${sub_mol_id_IT} $MAIN_ALIGN_OPTIONS_STRING -fn $BLAST_INPUT_DIR/${new_old_molecules}/${molecule_type}_${mainMasterOrgaClusterId}/${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${end_name_blast_file_sub}.blast -Adir $ARCHIVE_INPUT_DIR\n";

	if ( $GZIP_AND_MV_OUTPUT_IN_BASH =~ m/^ON$/ ) {
		# gzip and mv output files to correct final directory
		# homologie file
		print BATCHFILE_CLUSTER_MIG "\t\tmkdir -p $ALIGN_OUTPUT_DIR/homologies/${molecule_type}_\${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "\t\tif [ -f \"\${master_mol_id_IT}_and_\${sub_mol_id_IT}_homologies_table.tsv\" ]
\t\tthen
\t\t\tgzip \${master_mol_id_IT}_and_\${sub_mol_id_IT}_homologies_table.tsv
\t\t\tmv \${master_mol_id_IT}_and_\${sub_mol_id_IT}_homologies_table.tsv.gz $ALIGN_OUTPUT_DIR/homologies/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_homologies_table.tsv.gz
\t\t\tif [ -f \"$ALIGN_OUTPUT_DIR/homologies/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_homologies_table.tsv.gz\" ]
\t\t\tthen
\t\t\t\t:
\t\t\telse
\t\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\techo \"file $ALIGN_OUTPUT_DIR/homologies/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_homologies_table.tsv.gz not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\texit 1
\t\t\tfi
\t\telse
\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\techo \"file \${master_mol_id_IT}_and_\${sub_mol_id_IT}_homologies_table.tsv not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\texit 1
\t\tfi
";
		# alignment_params and _alignment_params_table.tsv
		print BATCHFILE_CLUSTER_MIG "\t\tmkdir -p $ALIGN_OUTPUT_DIR/alignment_params/${molecule_type}_\${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "\t\tif [ -f \"\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_params_table.tsv\" ]
\t\tthen
\t\t\tgzip \${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_params_table.tsv
\t\t\tmv \${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_params_table.tsv.gz $ALIGN_OUTPUT_DIR/alignment_params/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_params_table.tsv.gz
\t\t\tif [ -f \"$ALIGN_OUTPUT_DIR/alignment_params/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_params_table.tsv.gz\" ]
\t\t\tthen
\t\t\t\t:
\t\t\telse
\t\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\techo \"file $ALIGN_OUTPUT_DIR/alignment_params/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_params_table.tsv.gz not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\texit 1
\t\t\tfi
\t\telse
\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\techo \"file \${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_params_table.tsv not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\texit 1
\t\tfi
";
		# alignments and _alignment_table.tsv
		print BATCHFILE_CLUSTER_MIG "\t\tmkdir -p $ALIGN_OUTPUT_DIR/alignments/${molecule_type}_\${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "\t\tif [ -f \"\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_table.tsv\" ]
\t\tthen
\t\t\tgzip \${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_table.tsv
\t\t\tmv \${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_table.tsv.gz $ALIGN_OUTPUT_DIR/alignments/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_table.tsv.gz
\t\t\tif [ -f \"$ALIGN_OUTPUT_DIR/alignments/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_table.tsv.gz\" ]
\t\t\tthen
\t\t\t\t:
\t\t\telse
\t\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\techo \"file $ALIGN_OUTPUT_DIR/alignments/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_table.tsv.gz not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\texit 1
\t\t\tfi
\t\telse
\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\techo \"file \${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_table.tsv not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\texit 1
\t\tfi
";
		# alignment_pairs and _alignment_pairs_table.tsv
		print BATCHFILE_CLUSTER_MIG "\t\tmkdir -p $ALIGN_OUTPUT_DIR/alignment_pairs/${molecule_type}_\${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "\t\tif [ -f \"\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_pairs_table.tsv\" ]
\t\tthen
\t\t\tgzip \${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_pairs_table.tsv
\t\t\tmv \${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_pairs_table.tsv.gz $ALIGN_OUTPUT_DIR/alignment_pairs/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_pairs_table.tsv.gz
\t\t\tif [ -f \"$ALIGN_OUTPUT_DIR/alignment_pairs/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_pairs_table.tsv.gz\" ]
\t\t\tthen
\t\t\t\t:
\t\t\telse
\t\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\techo \"file $ALIGN_OUTPUT_DIR/alignment_pairs/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_pairs_table.tsv.gz not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\texit 1
\t\t\tfi
\t\telse
\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\techo \"file \${master_mol_id_IT}_and_\${sub_mol_id_IT}_alignment_pairs_table.tsv not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\texit 1
\t\tfi
";

		# [optional] tandem_dups and _tandem_dups_table.tsv
		print BATCHFILE_CLUSTER_MIG "\t\tmkdir -p $ALIGN_OUTPUT_DIR/tandem_dups/${molecule_type}_\${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "\t\tif [ -f \"\${master_mol_id_IT}_and_\${sub_mol_id_IT}_tandem_dups_table.tsv\" ]
\t\tthen
\t\t\tgzip \${master_mol_id_IT}_and_\${sub_mol_id_IT}_tandem_dups_table.tsv
\t\t\tmv \${master_mol_id_IT}_and_\${sub_mol_id_IT}_tandem_dups_table.tsv.gz $ALIGN_OUTPUT_DIR/tandem_dups/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_tandem_dups_table.tsv.gz
\t\t\tif [ -f \"$ALIGN_OUTPUT_DIR/tandem_dups/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_tandem_dups_table.tsv.gz\" ]
\t\t\tthen
\t\t\t\t:
\t\t\telse
\t\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\techo \"file $ALIGN_OUTPUT_DIR/tandem_dups/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_tandem_dups_table.tsv.gz not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\texit 1
\t\t\tfi
\t\telse
\t\t\t:
\t\tfi
";

		# [optional] isBranchedToAnotherSynteny and _isBranchedToAnotherSynteny_table.tsv
		print BATCHFILE_CLUSTER_MIG "\t\tmkdir -p $ALIGN_OUTPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_\${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "\t\tif [ -f \"\${master_mol_id_IT}_and_\${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv\" ]
\t\tthen
\t\t\tgzip \${master_mol_id_IT}_and_\${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv
\t\t\tmv \${master_mol_id_IT}_and_\${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv.gz $ALIGN_OUTPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv.gz
\t\t\tif [ -f \"$ALIGN_OUTPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv.gz\" ]
\t\t\tthen
\t\t\t\t:
\t\t\telse
\t\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\techo \"file $ALIGN_OUTPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv.gz not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\texit 1
\t\t\tfi
\t\telse
\t\t\t:
\t\tfi
";

		# [optional] protFusion _protFusion_table.tsv
		print BATCHFILE_CLUSTER_MIG "\t\tmkdir -p $ALIGN_OUTPUT_DIR/protFusion/${molecule_type}_\${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "\t\tif [ -f \"\${master_mol_id_IT}_and_\${sub_mol_id_IT}_protFusion_table.tsv\" ]
\t\tthen
\t\t\tgzip \${master_mol_id_IT}_and_\${sub_mol_id_IT}_protFusion_table.tsv
\t\t\tmv \${master_mol_id_IT}_and_\${sub_mol_id_IT}_protFusion_table.tsv.gz $ALIGN_OUTPUT_DIR/protFusion/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_protFusion_table.tsv.gz
\t\t\tif [ -f \"$ALIGN_OUTPUT_DIR/protFusion/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_protFusion_table.tsv.gz\" ]
\t\t\tthen
\t\t\t\t:
\t\t\telse
\t\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\techo \"file $ALIGN_OUTPUT_DIR/protFusion/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_protFusion_table.tsv.gz not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\texit 1
\t\t\tfi
\t\telse
\t\t\t:
\t\tfi
";

		# [optional] closeBestMatchs _closeBestMatchs_table.tsv
		print BATCHFILE_CLUSTER_MIG "\t\tmkdir -p $ALIGN_OUTPUT_DIR/closeBestMatchs/${molecule_type}_\${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "\t\tif [ -f \"\${master_mol_id_IT}_and_\${sub_mol_id_IT}_closeBestMatchs_table.tsv\" ]
\t\tthen
\t\t\tgzip \${master_mol_id_IT}_and_\${sub_mol_id_IT}_closeBestMatchs_table.tsv
\t\t\tmv \${master_mol_id_IT}_and_\${sub_mol_id_IT}_closeBestMatchs_table.tsv.gz $ALIGN_OUTPUT_DIR/closeBestMatchs/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_closeBestMatchs_table.tsv.gz
\t\t\tif [ -f \"$ALIGN_OUTPUT_DIR/closeBestMatchs/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_closeBestMatchs_table.tsv.gz\" ]
\t\t\tthen
\t\t\t\t:
\t\t\telse
\t\t\t\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\techo \"file $ALIGN_OUTPUT_DIR/closeBestMatchs/${molecule_type}_\${master_mol_id_IT}/\${master_mol_id_IT}_and_\${sub_mol_id_IT}_closeBestMatchs_table.tsv.gz not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\t\t\t\texit 1
\t\t\tfi
\t\telse
\t\t\t:
\t\tfi
";

	}
	print BATCHFILE_CLUSTER_MIG "\tdone\n";
	print BATCHFILE_CLUSTER_MIG "done\n";



=pod OLD printin command
	#print BATCHFILE_CLUSTER_MIG "\n# launching align for $master_mol_id_IT VS $sub_mol_id_IT\n";
	# NO NEED : source $SiteConfig::ALIGN/main_GeneList.conf -> export POSTGRESLIB=/usr/local/public/postgres-9.3.9/lib && export LD_LIBRARY_PATH=/usr/local/public/postgres-9.3.9/lib
	# exemple : main_Align -org1 191 -org2 1 -gc -4 -ge -4 -fn orga_191_VS_orga_all.blast -Adir .
	# $SiteConfig::ALIGN/main_Align -org1 ${master_mol_id_IT} -org2 ${sub_mol_id_IT} $MAIN_ALIGN_OPTIONS_STRING -fn $blast_file_IT -Adir $OUTPUT_ARCHIVE_DIR
	print BATCHFILE_CLUSTER_MIG "$ALIGN_EXEC_PATH/main_Align -org1 ${master_mol_id_IT} -org2 ${sub_mol_id_IT} $MAIN_ALIGN_OPTIONS_STRING -fn $BLAST_INPUT_DIR/${new_old_molecules}/${molecule_type}_${mainMasterOrgaClusterId}/${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${end_name_blast_file_sub}.blast -Adir $ARCHIVE_INPUT_DIR\n";
	if ( $GZIP_AND_MV_OUTPUT_IN_BASH =~ m/^ON$/ ) {
		# gzip and mv output files to correct final directory
		# homologie file
		print BATCHFILE_CLUSTER_MIG "mkdir -p $ALIGN_OUTPUT_DIR/homologies/${molecule_type}_${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "if [ -f \"${master_mol_id_IT}_and_${sub_mol_id_IT}_homologies_table.tsv\" ]
then
\tgzip ${master_mol_id_IT}_and_${sub_mol_id_IT}_homologies_table.tsv
\tmv ${master_mol_id_IT}_and_${sub_mol_id_IT}_homologies_table.tsv.gz $ALIGN_OUTPUT_DIR/homologies/${molecule_type}_${master_mol_id_IT}/${master_mol_id_IT}_and_${sub_mol_id_IT}_homologies_table.tsv.gz
else
\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\techo \"file ${master_mol_id_IT}_and_${sub_mol_id_IT}_homologies_table.tsv not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\texit 1
fi
";
		# alignment_params and _alignment_params_table.tsv
		print BATCHFILE_CLUSTER_MIG "mkdir -p $ALIGN_OUTPUT_DIR/alignment_params/${molecule_type}_${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "if [ -f \"${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_params_table.tsv\" ]
then
\tgzip ${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_params_table.tsv
\tmv ${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_params_table.tsv.gz $ALIGN_OUTPUT_DIR/alignment_params/${molecule_type}_${master_mol_id_IT}/${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_params_table.tsv.gz
else
\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\techo \"file ${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_params_table.tsv not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\texit 1
fi
";
		# alignments and _alignment_table.tsv
		print BATCHFILE_CLUSTER_MIG "mkdir -p $ALIGN_OUTPUT_DIR/alignments/${molecule_type}_${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "if [ -f \"${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_table.tsv\" ]
then
\tgzip ${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_table.tsv
\tmv ${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_table.tsv.gz $ALIGN_OUTPUT_DIR/alignments/${molecule_type}_${master_mol_id_IT}/${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_table.tsv.gz
else
\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\techo \"file ${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_table.tsv not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\texit 1
fi
";
		# alignment_pairs and _alignment_pairs_table.tsv
		print BATCHFILE_CLUSTER_MIG "mkdir -p $ALIGN_OUTPUT_DIR/alignment_pairs/${molecule_type}_${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "if [ -f \"${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_pairs_table.tsv\" ]
then
\tgzip ${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_pairs_table.tsv
\tmv ${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_pairs_table.tsv.gz $ALIGN_OUTPUT_DIR/alignment_pairs/${molecule_type}_${master_mol_id_IT}/${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_pairs_table.tsv.gz
else
\ttouch $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\techo \"file ${master_mol_id_IT}_and_${sub_mol_id_IT}_alignment_pairs_table.tsv not found.\" >> $QSUB_LOG_DIR/batch_${molecule_type}_${mainMasterOrgaClusterId}_VS_${molecule_type}_${mainSubOrgaClusterId}.err
\texit 1
fi
";


		# [optional] tandem_dups and _tandem_dups_table.tsv
		print BATCHFILE_CLUSTER_MIG "mkdir -p $ALIGN_OUTPUT_DIR/tandem_dups/${molecule_type}_${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "if [ -f \"${master_mol_id_IT}_and_${sub_mol_id_IT}_tandem_dups_table.tsv\" ]
then
\tgzip ${master_mol_id_IT}_and_${sub_mol_id_IT}_tandem_dups_table.tsv
\tmv ${master_mol_id_IT}_and_${sub_mol_id_IT}_tandem_dups_table.tsv.gz $ALIGN_OUTPUT_DIR/tandem_dups/${molecule_type}_${master_mol_id_IT}/${master_mol_id_IT}_and_${sub_mol_id_IT}_tandem_dups_table.tsv.gz
else
\t:
fi
";


		# [optional] isBranchedToAnotherSynteny and _isBranchedToAnotherSynteny_table.tsv
		print BATCHFILE_CLUSTER_MIG "mkdir -p $ALIGN_OUTPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "if [ -f \"${master_mol_id_IT}_and_${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv\" ]
then
\tgzip ${master_mol_id_IT}_and_${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv
\tmv ${master_mol_id_IT}_and_${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv.gz $ALIGN_OUTPUT_DIR/tandem_dups/${molecule_type}_${master_mol_id_IT}/${master_mol_id_IT}_and_${sub_mol_id_IT}_isBranchedToAnotherSynteny_table.tsv.gz
else
\t:
fi
";

		# [optional] protFusion _protFusion_table.tsv
		print BATCHFILE_CLUSTER_MIG "mkdir -p $ALIGN_OUTPUT_DIR/protFusion/${molecule_type}_${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "if [ -f \"${master_mol_id_IT}_and_${sub_mol_id_IT}_protFusion_table.tsv\" ]
then
\tgzip ${master_mol_id_IT}_and_${sub_mol_id_IT}_protFusion_table.tsv
\tmv ${master_mol_id_IT}_and_${sub_mol_id_IT}_protFusion_table.tsv.gz $ALIGN_OUTPUT_DIR/protFusion/${molecule_type}_${master_mol_id_IT}/${master_mol_id_IT}_and_${sub_mol_id_IT}_protFusion_table.tsv.gz
else
\t:
fi
";

		# [optional] closeBestMatchs _closeBestMatchs_table.tsv
		print BATCHFILE_CLUSTER_MIG "mkdir -p $ALIGN_OUTPUT_DIR/closeBestMatchs/${molecule_type}_${master_mol_id_IT}/\n";
		print BATCHFILE_CLUSTER_MIG "if [ -f \"${master_mol_id_IT}_and_${sub_mol_id_IT}_closeBestMatchs_table.tsv\" ]
then
\tgzip ${master_mol_id_IT}_and_${sub_mol_id_IT}_closeBestMatchs_table.tsv
\tmv ${master_mol_id_IT}_and_${sub_mol_id_IT}_closeBestMatchs_table.tsv.gz $ALIGN_OUTPUT_DIR/closeBestMatchs/${molecule_type}_${master_mol_id_IT}/${master_mol_id_IT}_and_${sub_mol_id_IT}_closeBestMatchs_table.tsv.gz
else
\t:
fi
";

	}
	# $SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.main_Align_done
	print BATCHFILE_CLUSTER_MIG "mkdir -p $MARKER_DONE_ALIGN_DIR/\n";
	print BATCHFILE_CLUSTER_MIG "touch $MARKER_DONE_ALIGN_DIR/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.main_Align_done\n";
=cut
}


sub sub_print_batch_file {

	my ( $master_mol_id_IT, $sub_mol_id_IT, $new_old_molecules ) = @_;

	if($GENERATE_FOR_IDRIS =~ m/^ON$/i) {
		my $blast_file_IT = "\$BLAST_RES_DIR/${master_mol_id_IT}_${molecule_type}_VS_${sub_mol_id_IT}_${molecule_type}.blast";
		print BATCHFILE_FOR_IDRIS "main_Align $MAIN_ALIGN_OPTIONS_STRING -fn $blast_file_IT -Adir \$ARCHIVE_FILES_DIR \n";
	} else {

		#OLD move in bash file to be submitted
		#my $blast_file_IT = "";
		#if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
		#	$blast_file_IT = "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${new_old_molecules}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_all.blast";
		#} else {
		#	$blast_file_IT = "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${new_old_molecules}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast";
		#}

		# move in bash file to be submitted, taken care of in bash script directly
#		if ( -e "$blast_file_IT.gz" ) {
#			print LOG "gunzip $blast_file_IT.gz :".`gunzip $blast_file_IT.gz`."\n" unless $VERBOSE =~ m/^OFF$/;
#		} elsif ( -e "$blast_file_IT.bz2" ) {
#			print LOG "bunzip2 $blast_file_IT.bz2 :".`bunzip2 $blast_file_IT.bz2`."\n" unless $VERBOSE =~ m/^OFF$/;
#		} elsif ( -e "$blast_file_IT.todo_gzip" ) {
#			print LOG "mv $blast_file_IT.todo_gzip $blast_file_IT :".`mv $blast_file_IT.todo_gzip $blast_file_IT`."\n" unless $VERBOSE =~ m/^OFF$/;
#		}
#		if ( !-e $blast_file_IT ) {
#			die_with_error_mssg("Error sub_print_batch_file: the file $blast_file_IT does not exists.");
#		}


		my $QSUB_LOG_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig";
		if ($EXPORT_QSUB_LOG_DIR ne "") {
			$QSUB_LOG_DIR = $EXPORT_QSUB_LOG_DIR;
		}
		my $ALIGN_EXEC_PATH = "$SiteConfig::ALIGN";
		if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
			$ALIGN_EXEC_PATH = "\${ALIGN_EXEC_PATH}";
		}
		my $ARCHIVE_INPUT_DIR = $OUTPUT_ARCHIVE_DIR;
		if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
			$ARCHIVE_INPUT_DIR = "\${ARCHIVE_INPUT_DIR}";
		}
		my $BLAST_INPUT_DIR = "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output";
		if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
			$BLAST_INPUT_DIR = "\${BLAST_INPUT_DIR}";
		}
		my $ALIGN_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv";
		if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
			$ALIGN_OUTPUT_DIR = "\${ALIGN_OUTPUT_DIR}";
		}
		my $MARKER_DONE_ALIGN_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/align_done";
		if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
			$MARKER_DONE_ALIGN_DIR = "\${MARKER_DONE_ALIGN_DIR}";
		}

		`mkdir -p $QSUB_LOG_DIR/sge_err_out/`;
		`mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/${new_old_molecules}/${molecule_type}_${master_mol_id_IT}/`;
		open( BATCHFILE_CLUSTER_MIG, "> $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/${new_old_molecules}/${molecule_type}_${master_mol_id_IT}/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.ll" ) or die_with_error_mssg( "Can not open$SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/${new_old_molecules}/${molecule_type}_${master_mol_id_IT}/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.ll" );


		# old way
		#print BATCHFILE_CLUSTER_MIG "source $SiteConfig::ALIGN/main_GeneList.conf && $SiteConfig::ALIGN/main_Align -org1 ${master_mol_id_IT} -org2 ${sub_mol_id_IT} $MAIN_ALIGN_OPTIONS_STRING -fn $blast_file_IT -Adir $OUTPUT_ARCHIVE_DIR && $SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.main_Align_done\n";


		if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
			# new way
			printHeaderOfBashFile($master_mol_id_IT, $sub_mol_id_IT, $QSUB_LOG_DIR, "YES", $BLAST_INPUT_DIR, $new_old_molecules, $ALIGN_EXEC_PATH);

			my @al_all_orga_ids_in_cluster_master = ();
	   		push ( @al_all_orga_ids_in_cluster_master, $master_mol_id_IT);
			my @al_all_orga_ids_in_cluster_sub = ();
	   		push ( @al_all_orga_ids_in_cluster_sub, $sub_mol_id_IT);

			#printContentOfBashFile($master_mol_id_IT, $sub_mol_id_IT, $new_old_molecules, "YES", $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR, $master_mol_id_IT, $sub_mol_id_IT, undef, undef); #$master_mol_id_IT, $sub_mol_id_IT, $blast_file_is_versus_all_orga, $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR, ref_al_all_orga_ids_in_cluster_master, ref_al_all_orga_ids_in_cluster_sub
			printContentOfBashFile(\@al_all_orga_ids_in_cluster_master, \@al_all_orga_ids_in_cluster_sub, $new_old_molecules, "YES", $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR, $master_mol_id_IT, $sub_mol_id_IT); #ref_al_all_orga_ids_in_cluster_master, ref_al_all_orga_ids_in_cluster_sub, new_old_molecules, $blast_file_is_versus_all_orga, $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR, ${master_mol_id_IT}, ${sub_mol_id_IT}
			printFooterOfBashFile($master_mol_id_IT, $sub_mol_id_IT, $QSUB_LOG_DIR, "YES", $BLAST_INPUT_DIR, $new_old_molecules, $MARKER_DONE_ALIGN_DIR);

		} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
			# idem new way but loop for all orga ids associated with main id sent
			printHeaderOfBashFile($master_mol_id_IT, $sub_mol_id_IT, $QSUB_LOG_DIR, "NO", $BLAST_INPUT_DIR, $new_old_molecules, $ALIGN_EXEC_PATH);
			if ( exists $mainClusterOrgaId2AlAllOragIdsInCluster{${master_mol_id_IT}} ) {
				my $ref_al_all_orga_ids_in_cluster_master = $mainClusterOrgaId2AlAllOragIdsInCluster{${master_mol_id_IT}};
				if ( exists $mainClusterOrgaId2AlAllOragIdsInCluster{${sub_mol_id_IT}} ) {
					my $ref_al_all_orga_ids_in_cluster_sub = $mainClusterOrgaId2AlAllOragIdsInCluster{${sub_mol_id_IT}};
					my $count_out_loop = 0;

					printContentOfBashFile($ref_al_all_orga_ids_in_cluster_master, $ref_al_all_orga_ids_in_cluster_sub, $new_old_molecules, "NO", $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR, ${master_mol_id_IT}, ${sub_mol_id_IT}); #ref_al_all_orga_ids_in_cluster_master, ref_al_all_orga_ids_in_cluster_sub, new_old_molecules, $blast_file_is_versus_all_orga, $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR, ${master_mol_id_IT}, ${sub_mol_id_IT}


=pod OLD
					foreach my $orga_ids_in_cluster_masterIt (@{$ref_al_all_orga_ids_in_cluster_master}) {
						$count_out_loop++;
						my $count_in_loop = 0; 
						foreach my $orga_ids_in_cluster_subIt (@{$ref_al_all_orga_ids_in_cluster_sub}) {
							$count_in_loop++;
							if ( $master_mol_id_IT == $sub_mol_id_IT && $count_out_loop > $count_in_loop ) { 
								# do not print redundant if cluster against itself
							} else {
								printContentOfBashFile($orga_ids_in_cluster_masterIt, $orga_ids_in_cluster_subIt, $new_old_molecules, "NO", $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR, ${master_mol_id_IT}, ${sub_mol_id_IT}); #$master_mol_id_IT, $sub_mol_id_IT, $blast_file_is_versus_all_orga, $QSUB_LOG_DIR, $ALIGN_EXEC_PATH, $BLAST_INPUT_DIR, $ARCHIVE_INPUT_DIR, $ALIGN_OUTPUT_DIR, $MARKER_DONE_ALIGN_DIR
							}
						}
					}
=cut
				} else {
					die_with_error_mssg("Error for sub_mol_id_IT ${sub_mol_id_IT} : no entry found in \%mainClusterOrgaId2AlAllOragIdsInCluster");
				}

			} else {
				die_with_error_mssg("Error for master_mol_id_IT ${master_mol_id_IT} : no entry found in \%mainClusterOrgaId2AlAllOragIdsInCluster");
			}
			printFooterOfBashFile($master_mol_id_IT, $sub_mol_id_IT, $QSUB_LOG_DIR, "NO", $BLAST_INPUT_DIR, $new_old_molecules, $MARKER_DONE_ALIGN_DIR);
		} else {
			die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
		}


		#close BATCHFILE_CLUSTER_MIG
		close(BATCHFILE_CLUSTER_MIG);
	
	}
}






print LOG "Start print main_Align command for new mol against new\n" unless $VERBOSE =~ m/^OFF$/;
for my $i (0 .. $#list_new_molecule_ids){
	my $master_elet_id = $list_new_molecule_ids[$i];
	for my $j ($i .. $#list_new_molecule_ids){
		my $sub_elet_id = $list_new_molecule_ids[$j];
		sub_print_batch_file ($master_elet_id, $sub_elet_id, "new_molecules");
	}
}
print LOG "Done print main_Align command for new mol against new\n" unless $VERBOSE =~ m/^OFF$/;


#print main_Align command for new mol against old
if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	#do nothing as we have old organisms in all.faa
} else {
	print LOG "Start print main_Align command for new mol against old\n";
	for my $i (0 .. $#list_new_molecule_ids){
		my $new_elet_id = $list_new_molecule_ids[$i];
		for my $j (0 .. $#list_old_molecule_ids){
			my $old_elet_id = $list_old_molecule_ids[$j];
			sub_print_batch_file ($new_elet_id, $old_elet_id, "old_molecules");
		}
	}
	print LOG "Done print main_Align command for new mol against old\n";

}


#close, file, cleaning up archive
if($GENERATE_FOR_IDRIS =~ m/^ON$/i){
	close(BATCHFILE_FOR_IDRIS);
}



#end of script
print LOG
"\n---------------------------------------------------\n\n\n Task_add_alignments_generator_for_IDRIS.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);






