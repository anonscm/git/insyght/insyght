#!/usr/local/bin/perl
#
# perl analyse_stats_db.pl
#
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use Time::HiRes qw(sleep gettimeofday tv_interval );
use SiteConfig;
use ORIGAMI;
use ORIGAMI_ncbi_taxonomy;
use POSIX ":sys_wait_h";
use Term::ReadKey;
use POSIX;
use Date::Parse;
use DateTime;
use Object_stat_avg_homologies;

# whole script scoped variable
my $st;
my $output_cmd_grep = "";
my $string_cmd_grep = "";
my $command_sql_queryIT = "";
my $countIT = 0; 
my $res_formule = 0;
my $VACUUM_PRIOR_TO_STAT = "OFF";
my $FORCE_YES_PROMPT_USER = "OFF";
my $CREATE_TMP_BUILD_FILES = "OFF";
#my $suffix_file is defined below
# args and hash build
my $BUILD_from_table_elements = "OFF";
my %accession2element_id = ();
my %element_id2accession = ();
my %element_id2organism_id = ();
my %organism_id2earliest_date_seq = ();
my %organism_id2latest_date_seq = ();
my $BUILD_from_table_genes = "OFF";
my %gene_id2organism_id = ();
my %organism_id2gene_count = ();
my %organism_id2list_gene_ids = ();
my %accessionAndFeatureId2GeneId = ();
my %organism_id2taxon_id = ();
my $RESTRICT_list_taxon_id_from_file = "";
my @list_orga_ids_to_analyse = ();
my @list_accession_to_analyse = ();
my @list_alignment_param_id_to_analyse = ();
#my @list_alignment_id_to_analyse = ();
my $count_organism_id_to_analyse = 0;
my $BUILD_from_table_taxo = "OFF";
my %taxon_id2scientific_name = ();
# args and hash stats
my $STATS_ON_ORTHOLOGS_HOMOLOGS = "OFF";
my $STATS_TABLE_HOMOLOGIES_PER_ORGANISM = "OFF";
my $STATS_TABLE_HOMOLOGIES_FOR_ALL_ORTHO_HOMO = "OFF";
my $STATS_COUNT_SYNTENIES_SINGLETON = "OFF";
my $STAT_CDS_ABNORMAL_STRUCTURE = "OFF";
my $PRINT_DETAILS_TEST_ANNOT_STRUCT = "OFF";
my $STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION = "OFF";
my $STATS_ALL_GENES_WITH_KNOWN_ANNOTATION = "OFF";
my %orga_id2hash_gene_id_known_annotation = ();
my %orga_id2count_pseudogene = ();
my %orga_id2count_non_coding_gene = ();
my $STATS_FUSION_SYNTENIES_PAIRWISE = "OFF";
my $STATS_HOMOLOGS_ALL_GENOMES = "OFF";
my $STATS_FUSION_SYNTENIES_ALL_GENOMES = "OFF";
my $STATS_ON_PROFILE_PHYLO = "OFF";
my $command_string = "";
my $count_loop_IT = 0;
my $total_loops_to_do = 0;
my $number_tuples_table_alignment_params = 0;
my $number_tuples_table_alignments = 0;
my $number_tuples_table_alignment_pairs = 0;
my $number_tuples_table_homologies = 0;

# set autoflush
BEGIN { $| = 1 }

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log et data
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_alignments/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Analyse_stats/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Analyse_stats/profil_phylo_per_organism/`;

#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_add_alignments/analyse_stats_db.log"); #OLD using tee.pl
my $log_file = "$SiteConfig::LOGDIR/Task_add_alignments/analyse_stats_db__BUILD_from_table_taxo__.log";
open(LOG, '>:encoding(UTF-8)', $log_file) or die ("Can not open $log_file\n");



print LOG
"\n---------------------------------------------------\n analyse_stats_db.pl started at :",
  scalar(localtime), "\n\n\n";

foreach my $argnum ( 0 .. $#ARGV ) {
# -PRINT_DETAILS_TEST_ANNOT_STRUCT
	if ( $ARGV[$argnum] =~ m/^-PRINT_DETAILS_TEST_ANNOT_STRUCT$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$PRINT_DETAILS_TEST_ANNOT_STRUCT = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -PRINT_DETAILS_TEST_ANNOT_STRUCT argument ; usage : perl analyse_stats_db.pl -PRINT_DETAILS_TEST_ANNOT_STRUCT {ON, OFF}\n";
			die
"incorrect -PRINT_DETAILS_TEST_ANNOT_STRUCT argument ; usage : perl analyse_stats_db.pl -PRINT_DETAILS_TEST_ANNOT_STRUCT {ON, OFF}\n";
		}
# -FORCE_YES_PROMPT_USER
	} elsif ( $ARGV[$argnum] =~ m/^-FORCE_YES_PROMPT_USER$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FORCE_YES_PROMPT_USER = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -FORCE_YES_PROMPT_USER argument ; usage : perl analyse_stats_db.pl -FORCE_YES_PROMPT_USER {ON, OFF}\n";
			die
"incorrect -FORCE_YES_PROMPT_USER argument ; usage : perl analyse_stats_db.pl -FORCE_YES_PROMPT_USER {ON, OFF}\n";
		}
# -VACUUM_PRIOR_TO_STAT
	} elsif ( $ARGV[$argnum] =~ m/^-VACUUM_PRIOR_TO_STAT$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VACUUM_PRIOR_TO_STAT = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -VACUUM_PRIOR_TO_STAT argument ; usage : perl analyse_stats_db.pl -VACUUM_PRIOR_TO_STAT {ON, OFF}\n";
			die
"incorrect -VACUUM_PRIOR_TO_STAT argument ; usage : perl analyse_stats_db.pl -VACUUM_PRIOR_TO_STAT {ON, OFF}\n";
		}
# -CREATE_TMP_BUILD_FILES
	} elsif ( $ARGV[$argnum] =~ m/^-CREATE_TMP_BUILD_FILES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$CREATE_TMP_BUILD_FILES = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -CREATE_TMP_BUILD_FILES argument ; usage : perl analyse_stats_db.pl -CREATE_TMP_BUILD_FILES {ON, OFF}\n";
			die
"incorrect -CREATE_TMP_BUILD_FILES argument ; usage : perl analyse_stats_db.pl -CREATE_TMP_BUILD_FILES {ON, OFF}\n";
		}
# -STATS_ON_ORTHOLOGS_HOMOLOGS
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_ON_ORTHOLOGS_HOMOLOGS$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_ON_ORTHOLOGS_HOMOLOGS = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_ON_ORTHOLOGS_HOMOLOGS argument ; usage : perl analyse_stats_db.pl -STATS_ON_ORTHOLOGS_HOMOLOGS {ON, OFF}\n";
			die
"incorrect -STATS_ON_ORTHOLOGS_HOMOLOGS argument ; usage : perl analyse_stats_db.pl -STATS_ON_ORTHOLOGS_HOMOLOGS {ON, OFF}\n";
		}

# -STATS_TABLE_HOMOLOGIES_PER_ORGANISM
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_TABLE_HOMOLOGIES_PER_ORGANISM$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_TABLE_HOMOLOGIES_PER_ORGANISM = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_TABLE_HOMOLOGIES_PER_ORGANISM argument ; usage : perl analyse_stats_db.pl -STATS_TABLE_HOMOLOGIES_PER_ORGANISM {ON, OFF}\n";
			die
"incorrect -STATS_TABLE_HOMOLOGIES_PER_ORGANISM argument ; usage : perl analyse_stats_db.pl -STATS_TABLE_HOMOLOGIES_PER_ORGANISM {ON, OFF}\n";
		}

# -STATS_TABLE_HOMOLOGIES_FOR_ALL_ORTHO_HOMO
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_TABLE_HOMOLOGIES_FOR_ALL_ORTHO_HOMO$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_TABLE_HOMOLOGIES_FOR_ALL_ORTHO_HOMO = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_TABLE_HOMOLOGIES_FOR_ALL_ORTHO_HOMO argument ; usage : perl analyse_stats_db.pl -STATS_TABLE_HOMOLOGIES_FOR_ALL_ORTHO_HOMO {ON, OFF}\n";
			die
"incorrect -STATS_TABLE_HOMOLOGIES_FOR_ALL_ORTHO_HOMO argument ; usage : perl analyse_stats_db.pl -STATS_TABLE_HOMOLOGIES_FOR_ALL_ORTHO_HOMO {ON, OFF}\n";
		}

# -STATS_COUNT_SYNTENIES_SINGLETON
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_COUNT_SYNTENIES_SINGLETON$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_COUNT_SYNTENIES_SINGLETON = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_COUNT_SYNTENIES_SINGLETON argument ; usage : perl analyse_stats_db.pl -STATS_COUNT_SYNTENIES_SINGLETON {ON, OFF}\n";
			die
"incorrect -STATS_COUNT_SYNTENIES_SINGLETON argument ; usage : perl analyse_stats_db.pl -STATS_COUNT_SYNTENIES_SINGLETON {ON, OFF}\n";
		}

# -STAT_CDS_ABNORMAL_STRUCTURE
	} elsif ( $ARGV[$argnum] =~ m/^-STAT_CDS_ABNORMAL_STRUCTURE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STAT_CDS_ABNORMAL_STRUCTURE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STAT_CDS_ABNORMAL_STRUCTURE argument ; usage : perl analyse_stats_db.pl -STAT_CDS_ABNORMAL_STRUCTURE {ON, OFF}\n";
			die
"incorrect -STAT_CDS_ABNORMAL_STRUCTURE argument ; usage : perl analyse_stats_db.pl -STAT_CDS_ABNORMAL_STRUCTURE {ON, OFF}\n";
		}
# -STATS_FUSION_SYNTENIES_PAIRWISE
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_FUSION_SYNTENIES_PAIRWISE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_FUSION_SYNTENIES_PAIRWISE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_FUSION_SYNTENIES_PAIRWISE argument ; usage : perl analyse_stats_db.pl -STATS_FUSION_SYNTENIES_PAIRWISE {ON, OFF}\n";
			die
"incorrect -STATS_FUSION_SYNTENIES_PAIRWISE argument ; usage : perl analyse_stats_db.pl -STATS_FUSION_SYNTENIES_PAIRWISE {ON, OFF}\n";
		}
# -STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION argument ; usage : perl analyse_stats_db.pl -STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION {ON, OFF}\n";
			die
"incorrect -STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION argument ; usage : perl analyse_stats_db.pl -STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION {ON, OFF}\n";
		}
# -STATS_ALL_GENES_WITH_KNOWN_ANNOTATION
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_ALL_GENES_WITH_KNOWN_ANNOTATION$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_ALL_GENES_WITH_KNOWN_ANNOTATION = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_ALL_GENES_WITH_KNOWN_ANNOTATION argument ; usage : perl analyse_stats_db.pl -STATS_ALL_GENES_WITH_KNOWN_ANNOTATION {ON, OFF}\n";
			die
"incorrect -STATS_ALL_GENES_WITH_KNOWN_ANNOTATION argument ; usage : perl analyse_stats_db.pl -STATS_ALL_GENES_WITH_KNOWN_ANNOTATION {ON, OFF}\n";
		}
=pod
# -BUILD_AccessionAndFeatureId2GeneId

	} elsif ( $ARGV[$argnum] =~ m/^-BUILD_AccessionAndFeatureId2GeneId$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$BUILD_AccessionAndFeatureId2GeneId = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -BUILD_AccessionAndFeatureId2GeneId argument ; usage : perl analyse_stats_db.pl -BUILD_AccessionAndFeatureId2GeneId {ON, OFF}\n";
			die
"incorrect -BUILD_AccessionAndFeatureId2GeneId argument ; usage : perl analyse_stats_db.pl -BUILD_AccessionAndFeatureId2GeneId {ON, OFF}\n";
		}
=cut
# -BUILD_from_table_elements
	} elsif ( $ARGV[$argnum] =~ m/^-BUILD_from_table_elements$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$BUILD_from_table_elements = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -BUILD_from_table_elements argument ; usage : perl analyse_stats_db.pl -BUILD_from_table_elements {ON, OFF}\n";
			die
"incorrect -BUILD_from_table_elements argument ; usage : perl analyse_stats_db.pl -BUILD_from_table_elements {ON, OFF}\n";
		}
# -BUILD_from_table_genes
	} elsif ( $ARGV[$argnum] =~ m/^-BUILD_from_table_genes$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$BUILD_from_table_genes = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -BUILD_from_table_genes argument ; usage : perl analyse_stats_db.pl -BUILD_from_table_genes {ON, OFF}\n";
			die
"incorrect -BUILD_from_table_genes argument ; usage : perl analyse_stats_db.pl -BUILD_from_table_genes {ON, OFF}\n";
		}

# RESTRICT_list_taxon_id_from_file
# file need to be one taxon id per line
	} elsif ( $ARGV[$argnum] =~ m/^-RESTRICT_list_taxon_id_from_file$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^[a-z0-9\/\\_-]+$/i)
		{
			$RESTRICT_list_taxon_id_from_file = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -RESTRICT_list_taxon_id_from_file argument ; usage : perl analyse_stats_db.pl -RESTRICT_list_taxon_id_from_file {PATH_TO_FILE}\n";
			die
"incorrect -RESTRICT_list_taxon_id_from_file argument ; usage : perl analyse_stats_db.pl -RESTRICT_list_taxon_id_from_file {PATH_TO_FILE}\n";
		}
# STATS_HOMOLOGS_ALL_GENOMES
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_HOMOLOGS_ALL_GENOMES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_HOMOLOGS_ALL_GENOMES = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_HOMOLOGS_ALL_GENOMES argument ; usage : perl analyse_stats_db.pl -STATS_HOMOLOGS_ALL_GENOMES {ON, OFF}\n";
			die
"incorrect -STATS_HOMOLOGS_ALL_GENOMES argument ; usage : perl analyse_stats_db.pl -STATS_HOMOLOGS_ALL_GENOMES {ON, OFF}\n";
		}
# STATS_FUSION_SYNTENIES_ALL_GENOMES
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_FUSION_SYNTENIES_ALL_GENOMES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_FUSION_SYNTENIES_ALL_GENOMES = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_FUSION_SYNTENIES_ALL_GENOMES argument ; usage : perl analyse_stats_db.pl -STATS_FUSION_SYNTENIES_ALL_GENOMES {ON, OFF}\n";
			die
"incorrect -STATS_FUSION_SYNTENIES_ALL_GENOMES argument ; usage : perl analyse_stats_db.pl -STATS_FUSION_SYNTENIES_ALL_GENOMES {ON, OFF}\n";
		}
# STATS_ON_PROFILE_PHYLO
	} elsif ( $ARGV[$argnum] =~ m/^-STATS_ON_PROFILE_PHYLO$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$STATS_ON_PROFILE_PHYLO = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -STATS_ON_PROFILE_PHYLO argument ; usage : perl analyse_stats_db.pl -STATS_ON_PROFILE_PHYLO {ON, OFF}\n";
			die
"incorrect -STATS_ON_PROFILE_PHYLO argument ; usage : perl analyse_stats_db.pl -STATS_ON_PROFILE_PHYLO {ON, OFF}\n";
		}
# BUILD_from_table_taxo
	} elsif ( $ARGV[$argnum] =~ m/^-BUILD_from_table_taxo$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$BUILD_from_table_taxo = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -BUILD_from_table_taxo argument ; usage : perl analyse_stats_db.pl -BUILD_from_table_taxo {ON, OFF}\n";
			die
"incorrect -BUILD_from_table_taxo argument ; usage : perl analyse_stats_db.pl -BUILD_from_table_taxo {ON, OFF}\n";
		}
# END
	} elsif ( $ARGV[$argnum] =~ m/^-/ ) {
		print LOG "ERROR unrecognized argument : $ARGV[$argnum]\n\n";
		die("ERROR unrecognized argument : $ARGV[$argnum]\n\n");
	}
}


################################################
################ global subroutines
################################################

sub promptUser {
  my($prompt, $default) = @_;
  my $defaultValue = $default ? "[$default]" : "";
  print "$prompt $defaultValue: ";
  chomp(my $input = <STDIN>);
  return $input ? $input : $default;
}

# return 1 if use existing file, 0 otherwise to create it
sub check_file_exists_ask_use_it {
	my($file, $subroutine) = @_;

	if( ! -e $file){
		# file does not exist, query db and create it
		return 0;
	} elsif ($FORCE_YES_PROMPT_USER =~ m/^ON$/i) {
		# file does exist, force yes
		return 1;
	} else {
		my $date = POSIX::strftime("%d-%m-%y", localtime( ( stat $file )[9]));
		# file does exist, ask permission to use it or not
		my $user_answer = &promptUser("\n\n\nA data file already exists created by the $subroutine for the database ".$ORIGAMI::dbname."\n - date (DD-MM-YY): $date\n - location: $file\nDo you want to use that file instead of querying the database [Y/n] ?");
		if($user_answer =~ m/^yes$/i || $user_answer =~ m/^y$/i ){
			return 1;
		} else {
			return 0;
		}
	}
}




################################################
################ vacuum before if needed
################################################


if($VACUUM_PRIOR_TO_STAT =~ m/^ON$/i){

	my @list_tables_to_vacuum = (
"accessions",
"features",
"qualifiers",
"sequences",
"taxo",
"taxo_names",
"alignment_pairs",
"alignment_params",
"alignments",
"elements",
"genes",
"homologies",
"organisms",
"q_element_id_2_sorted_list_comp_orga_whole"
);

	sub vacuum_table {
		
		my ($table) = @_;
		print LOG "\n** Vacuuming table ".$table."...\n";
		$st = $ORIGAMI::dbh->prepare("VACUUM ".$table);
		$st->execute or die("Pb execute $!");

	}

	print LOG "\nStart vacuuming tables...\n";
	foreach my $tableIT (@list_tables_to_vacuum) { 
	    vacuum_table($tableIT);
	}
	print LOG "\nDone vacuuming tables.\n";
}

sub select_reltuples_table {
	my ($table) = @_;
	print LOG "\n** Start select reltuples table $table...\n";
	$st = $ORIGAMI::dbh->prepare("SELECT reltuples FROM pg_class WHERE relname = '".$table."'");
	$st->execute or die("Pb execute $!");
	my $number_tuples_to_return = -1;
	while ( my $res = $st->fetchrow_hashref() ) {
		$number_tuples_to_return = $res->{reltuples};
		print LOG "Number of tuples in table $table : $number_tuples_to_return\n";
	}
	print LOG "Done select reltuples table $table\n";
	return $number_tuples_to_return;
}
$number_tuples_table_alignment_params = select_reltuples_table("alignment_params");
$number_tuples_table_alignments = select_reltuples_table("alignments");
$number_tuples_table_alignment_pairs = select_reltuples_table("alignment_pairs");
$number_tuples_table_homologies = select_reltuples_table("homologies");


################################################
################ set list_orga_ids_to_analyse
################################################

{

	print LOG "\nStart setting list of orga ids to analyse.\n";
	my $t0      = [gettimeofday()];

	#@list_orga_ids_to_analyse
	my $count_lines_taxo_ids_to_analyse = 0;
	if ($RESTRICT_list_taxon_id_from_file ne "") {
		# parse existing file
		print LOG "\tset list_orga_ids_to_analyse : parsing file $RESTRICT_list_taxon_id_from_file\n";
		open(my $fh, '<:encoding(UTF-8)', $RESTRICT_list_taxon_id_from_file) or die "Could not open file '$RESTRICT_list_taxon_id_from_file' $!";
		my @list_taxo_ids = ();
		while (my $row = <$fh>) {
			$count_lines_taxo_ids_to_analyse++;
			chomp $row;
			$row =~ s/^\s+|\s+$//g;
			if($row !~ m/^\d+$/){
				print LOG "ERROR set list_orga_ids_to_analyse : parsing data line \"$row\" in file \"$RESTRICT_list_taxon_id_from_file\" but !~ m/^\\d+\$/\n";
				die("ERROR set list_orga_ids_to_analyse : parsing data line \"$row\" in file \"$RESTRICT_list_taxon_id_from_file\" but !~ m/^\\d+\$/\n");
			}
			push @list_taxo_ids, $row;
		}
		print LOG "\tquerying table organisms WHERE taxon_id IN ...\n";

		$st = $ORIGAMI::dbh->prepare("SELECT organism_id, taxon_id FROM organisms WHERE taxon_id IN (".join(",", @list_taxo_ids).") ORDER BY taxon_id");
		$st->execute or die("Pb execute $!");
		my $stored_taxon_id = -1;
		while ( my $res = $st->fetchrow_hashref() ) {
			my $organism_id = $res->{organism_id};
			my $taxon_id = $res->{taxon_id};
			if($stored_taxon_id == $taxon_id) {
				# meme espece, skip
			} else {
				# new species
				$stored_taxon_id = $taxon_id;
				$count_organism_id_to_analyse++;
				push @list_orga_ids_to_analyse, $organism_id;
			}
		}
		#@list_accession_to_analyse
		print LOG "\tquerying table elements ...\n";
		$st = $ORIGAMI::dbh->prepare("SELECT accession FROM elements WHERE organism_id IN (".join(",", @list_orga_ids_to_analyse).")");
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {
			my $accession = $res->{accession};
			push @list_accession_to_analyse, $accession;
		}
		#@list_alignment_param_id_to_analyse
		print LOG "\tquerying alignment_params ...\n";
		$st = $ORIGAMI::dbh->prepare("SELECT alignment_param_id FROM alignment_params WHERE q_organism_id IN (".join(",", @list_orga_ids_to_analyse).") AND s_organism_id IN (".join(",", @list_orga_ids_to_analyse).")");
		$st->execute or die("Pb execute $!");
		#my @list_alignment_param_id_to_analyse = ();
		while ( my $res = $st->fetchrow_hashref() ) {
			my $alignment_param_id = $res->{alignment_param_id};
			push @list_alignment_param_id_to_analyse, $alignment_param_id;
		}
=pod
		# takes too much memory and time to build, find another way ???
		#@list_alignment_id_to_analyse
		print LOG "\tquerying table alignments...\n";
		$st = $ORIGAMI::dbh->prepare("SELECT alignment_id FROM alignments WHERE alignment_param_id IN (".join(",", @list_alignment_param_id_to_analyse).")");
		$st->execute or die("Pb execute $!");
		my @list_alignment_id_to_analyse = ();
		while ( my $res = $st->fetchrow_hashref() ) {
			my $alignment_id = $res->{alignment_id};
			push @list_alignment_id_to_analyse, $alignment_id;
		}
=cut

		print LOG "\t Number of taxon ids in file -RESTRICT_list_taxon_id = $count_lines_taxo_ids_to_analyse.\n";
		print LOG "\t Number of organism found in db = $count_organism_id_to_analyse.\n";
		print LOG "\t Number of alignment param id found in db = ".scalar(@list_alignment_param_id_to_analyse).".\n";
		#print LOG "\tNumber of alignment id found in db = ".scalar(@list_alignment_id_to_analyse).".\n";
		print LOG "\n";

	} else {
		#TODO modify
		#all orga, keep @list_orga_ids_to_analyse, @list_accession_to_analyse, and @list_alignment_param_id_to_analyse empty
		print LOG "\t Analysing all organisms\n";
=pod
		print LOG "\tquerying table organisms ...\n";
		$st = $ORIGAMI::dbh->prepare("SELECT organism_id FROM organisms");
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {
			$count_organism_id_to_analyse++;
			my $organism_id = $res->{organism_id};
			push @list_orga_ids_to_analyse, $organism_id;
		}
=cut
	}
	
	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "Done setting list of orga ids to analyse in $elapsed seconds ; used $rounded MB of RAM.\n"; 


}

################################################
################ set $suffix_file after ${count_organism_id_to_analyse}
################################################

my $suffix_file = "db_".$ORIGAMI::dbname."_nbre_orga_analysed_${count_organism_id_to_analyse}.txt";



################################################
################ package build hash
################################################


# build %accession2element_id and %element_id2accession and %organism_id2earliest_date_seq and %organism_id2latest_date_seq, and %element_id2organism_id, and %organism_id2taxon_id
sub build_from_table_elements {

	print LOG "** Starting build_from_table_elements...\n";
	my $t0      = [gettimeofday()];

	# skip if already done
	if (%accession2element_id && %element_id2accession && %element_id2organism_id && %organism_id2taxon_id && %organism_id2earliest_date_seq && %organism_id2latest_date_seq) {
		print LOG "data hash created by build_from_table_elements are not empty, skipping creating them again\n";
		return;
	} elsif (%accession2element_id || %element_id2accession || %element_id2organism_id || %organism_id2taxon_id || %organism_id2earliest_date_seq || %organism_id2latest_date_seq) {
		print LOG "ERROR build_from_table_elements : Some of the data hash created by build_from_table_elements are empty and others are not\n";
		die("ERROR build_from_table_elements : Some of the data hash created by build_from_table_elements are empty and others are not\n");
	}
	
	# dependencies
	# no

	my $file = "$SiteConfig::DATADIR/Analyse_stats/build_from_table_elements_$suffix_file";
	my $use_existing_file = check_file_exists_ask_use_it($file, "build_from_table_elements");

	if($use_existing_file == 1){

		# parse existing file
		print LOG "build_from_table_elements : Using and parsing existing file $file\n";
		open(my $fh, '<:encoding(UTF-8)', $file) or die "Could not open file '$file' $!";
		while (my $row = <$fh>) {
			chomp $row;
			$row =~ s/^\s+|\s+$//g;
			#print "$row\n";
			my $idx_accession = 0;
			my $idx_element_id = 1;
			my $idx_organism_id = 2;
			my $idx_date_seq = 3;
			my $idx_taxon_id = 4;
			if($row =~ m/^#/){
				# header should be #accession\telement_id\torganism_id\tdate_seq\ttaxon_id\n
				$row =~ s/#//g; # remove #
				my @columns = split /\t/, $row;
				if($columns[$idx_accession] ne "accession"){
					print LOG "ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_accession] ne accession\n";
					die("ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_accession] ne accession\n");
				}
				if($columns[$idx_element_id] ne "element_id"){
					print LOG "ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_element_id] ne element_id\n";
					die("ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_element_id] ne element_id\n");
				}
				if($columns[$idx_organism_id] ne "organism_id"){
					print LOG "ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_organism_id] ne organism_id\n";
					die("ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_organism_id] ne organism_id\n");
				}
				if($columns[$idx_date_seq] ne "date_seq"){
					print LOG "ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_date_seq] ne date_seq\n";
					die("ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_date_seq] ne date_seq\n");
				}
				if($columns[$idx_taxon_id] ne "taxon_id"){
					print LOG "ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_taxon_id] ne taxon_id\n";
					die("ERROR build_from_table_elements : parsing header in file \"$file\" but \$columns[$idx_taxon_id] ne taxon_id\n");
				}
			} else {
				# data
				my @columns = split /\t/, $row;
				my $accession = "";
				my $element_id = -1;
				my $organism_id = -1;
				my $date_seq = "";
				my $taxon_id = "";
				if($columns[$idx_accession] eq ""){
					print LOG "ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_accession] eq \"\"\n";
					die("ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_accession] eq \"\"\n");
				}
				if($columns[$idx_element_id] !~ m/^\d+$/){
					print LOG "ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_element_id]  !~ m/^\d+\$/\n";
					die("ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_element_id]  !~ m/^\d+\$/\n");
				}
				if($columns[$idx_organism_id] !~ m/^\d+$/){
					print LOG "ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_organism_id]  !~ m/^\d+\$/\n";
					die("ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_organism_id]  !~ m/^\d+\$/\n");
				}
				if($columns[$idx_taxon_id] !~ m/^\d+$/){
					print LOG "ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_taxon_id]  !~ m/^\d+\$/\n";
					die("ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_taxon_id]  !~ m/^\d+\$/\n");
				}
				$accession = $columns[$idx_accession];
				$element_id = $columns[$idx_element_id];
				$organism_id = $columns[$idx_organism_id];
				if($columns[$idx_date_seq] ne ""){$date_seq = $columns[$idx_date_seq];}
				$taxon_id = $columns[$idx_taxon_id];
				if($accession eq "" || $element_id == -1  || $organism_id == -1 || $taxon_id eq ""){
					print LOG "ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$accession = $accession eq \"\" || \$element_id = $element_id  || \$organism_id = $organism_id || \$date_seq = $date_seq || \$taxon_id = $taxon_id\n";
					die("ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but \$accession = $accession eq \"\" || \$element_id = $element_id || \$organism_id = $organism_id || \$date_seq = $date_seq || \$taxon_id = $taxon_id\n");
				} else {
					if(exists $accession2element_id{$accession}){
						print LOG "ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but exists \$accession2element_id{\$accession} : ".$accession2element_id{$accession}."\n";
						die("ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but exists \$accession2element_id{\$accession} : ".$accession2element_id{$accession}."\n");
					} else {
						$accession2element_id{$accession} = $element_id;
					}

					if (exists $element_id2accession{$element_id}) {
						print LOG "ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but exists \$element_id2accession{\$element_id} : ".$element_id2accession{$element_id}."\n";
						die("ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but exists \$element_id2accession{\$element_id} : ".$element_id2accession{$element_id}."\n");
					} else {
						$element_id2accession{$element_id} = $accession;
					}

					if (exists $element_id2organism_id{$element_id}) {
						print LOG "ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but exists \$element_id2organism_id{\$element_id} : ".$element_id2organism_id{$element_id}."\n";
						die("ERROR build_from_table_elements : parsing data line \"$row\" in file \"$file\" but exists \$element_id2organism_id{\$element_id} : ".$element_id2organism_id{$element_id}."\n");
					} else {
						$element_id2organism_id{$element_id} = $organism_id;
					}
					if ($date_seq ne "") {
						#my $time = Time::Piece->strptime("October 28, 2011 9:00 PM PDT", "%B %d, %Y %r %Z");
						my $epoch_to_test = str2time($date_seq);# use Date::Parse;
						#my $datetime = DateTime->from_epoch(epoch => $epoch); #  use DateTime;
						# %organism_id2earliest_date_seq and %organism_id2latest_date_seq
						if(exists $organism_id2earliest_date_seq{$organism_id}){
							my $epoch_registred = str2time($organism_id2earliest_date_seq{$organism_id});
							if ($epoch_to_test < $epoch_registred){
								$organism_id2earliest_date_seq{$organism_id} = $date_seq
							}
						} else {
							$organism_id2earliest_date_seq{$organism_id} = $date_seq
						}
						if(exists $organism_id2latest_date_seq{$organism_id}){
							my $epoch_registred = str2time($organism_id2latest_date_seq{$organism_id});
							if ($epoch_to_test > $epoch_registred){
								$organism_id2latest_date_seq{$organism_id} = $date_seq
							}
						} else {
							$organism_id2latest_date_seq{$organism_id} = $date_seq
						}
					}
					if (exists $organism_id2taxon_id{$organism_id}) {
						#do nothing
					} else {
						$organism_id2taxon_id{$organism_id} = $taxon_id;
					}
				}
			}
		}

	} else {		
		# query db and create file
		print LOG "build_from_table_elements : querying db to create hash data";
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){print LOG " and file $file";}
		print LOG "\n";

		#open file
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){open(accession2element_id_FILE, '>:encoding(UTF-8)', $file) or die ("Can not open $file\n");}
		#print header
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){print accession2element_id_FILE "#accession\telement_id\torganism_id\tdate_seq\ttaxon_id\n";}

		$command_string = "SELECT elements.element_id, elements.accession, elements.organism_id, sequences.date_seq, organisms.taxon_id FROM elements, organisms WHERE elements.organism_id = organisms.organism_id AND elements.accession = sequences.accession";
		if (@list_orga_ids_to_analyse) {
			$command_string .=  " AND elements.organism_id IN (".join(",", @list_orga_ids_to_analyse).")";
		}
		#$st = $ORIGAMI::dbh->prepare("SELECT elements.element_id, elements.accession, elements.organism_id, elements.date_seq, organisms.taxon_id FROM elements, organisms WHERE elements.organism_id IN (".join(",", @list_orga_ids_to_analyse).") AND elements.organism_id = organisms.organism_id");
		$st = $ORIGAMI::dbh->prepare($command_string);
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {
			my $element_id = $res->{element_id};
			my $accession = $res->{accession};
			my $organism_id = $res->{organism_id};
			my $date_seq = $res->{date_seq};
			my $taxon_id = $res->{taxon_id};
			if($accession eq ""){
				print LOG "ERROR build_from_table_elements : accession $accession is empty\n";
				die("ERROR build_from_table_elements : accession $accession is empty\n");
			}
			if($element_id !~ m/^\d+$/i){
				print LOG "ERROR build_from_table_elements : element_id $element_id !~ m/^\\d+\$/i\n";
				die("ERROR build_from_table_elements : element_id $element_id !~ m/^\\d+\$/i\n");
			}
			if($organism_id !~ m/^\d+$/i){
				print LOG "ERROR build_from_table_elements : organism_id $organism_id !~ m/^\\d+\$/i\n";
				die("ERROR build_from_table_elements : organism_id $organism_id !~ m/^\\d+\$/i\n");
			}
			$accession2element_id{$accession} = $element_id;
			$element_id2accession{$element_id} = $accession;
			$element_id2organism_id{$element_id} = $organism_id;
			if ($date_seq ne "") {
				#my $time = Time::Piece->strptime("October 28, 2011 9:00 PM PDT", "%B %d, %Y %r %Z");
				my $epoch_to_test = str2time($date_seq);# use Date::Parse;
				#my $datetime = DateTime->from_epoch(epoch => $epoch); #  use DateTime;
				# %organism_id2earliest_date_seq and %organism_id2latest_date_seq
				if(exists $organism_id2earliest_date_seq{$organism_id}){
					my $epoch_registred = str2time($organism_id2earliest_date_seq{$organism_id});
					if ($epoch_to_test == $epoch_registred) {
						#print LOG "epoch_to_test == epoch_registred\n";
					} elsif ($epoch_to_test < $epoch_registred){
						$organism_id2earliest_date_seq{$organism_id} = $date_seq;
						#print LOG "For organism id $organism_id, epoch_to_test < epoch_registred : $epoch_to_test < $epoch_registred ; diff = ".(($epoch_registred-$epoch_to_test)/86400)." days\n";
					} else {
						#print LOG "For organism id $organism_id, epoch_to_test > epoch_registred : $epoch_to_test > $epoch_registred ; diff = ".(($epoch_to_test-$epoch_registred)/86400)." days\n";
					}
				} else {
					$organism_id2earliest_date_seq{$organism_id} = $date_seq
				}
				if(exists $organism_id2latest_date_seq{$organism_id}){
					my $epoch_registred = str2time($organism_id2latest_date_seq{$organism_id});
					if ($epoch_to_test > $epoch_registred){
						$organism_id2latest_date_seq{$organism_id} = $date_seq
					}
				} else {
					$organism_id2latest_date_seq{$organism_id} = $date_seq
				}
			}
			$organism_id2taxon_id{$organism_id} = $taxon_id;

			#print data
			if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){print accession2element_id_FILE "$accession\t$element_id\t$organism_id\t$date_seq\t$taxon_id\n";}
		}

		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){close(accession2element_id_FILE);}

	}

	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done build_from_table_elements in $elapsed seconds ; used $rounded MB of RAM.\n";
	print LOG "\t-> created ".scalar(keys %accession2element_id)." entries in \%accession2element_id.\n";
	print LOG "\t-> created ".scalar(keys %element_id2accession)." entries in \%element_id2accession.\n";
	print LOG "\t-> created ".scalar(keys %organism_id2earliest_date_seq)." entries in \%organism_id2earliest_date_seq.\n";
	print LOG "\t-> created ".scalar(keys %organism_id2latest_date_seq)." entries in \%organism_id2latest_date_seq.\n";
	print LOG "\t-> created ".scalar(keys %element_id2organism_id)." entries in \%element_id2organism_id.\n";
	print LOG "\t-> created ".scalar(keys %organism_id2taxon_id)." entries in \%organism_id2taxon_id.\n";
}
if ($BUILD_from_table_elements =~ m/^ON$/i){
	build_from_table_elements();
}


#build %gene_id2organism_id and %organism_id2gene_count and %organism_id2list_gene_ids and %accessionAndFeatureId2GeneId (with key ${accession}_A2FID_${feature_id})
sub build_from_table_genes {
	print LOG "** Starting build_from_table_genes...\n";
	my $t0      = [gettimeofday()];

	# skip if already done
	if (%gene_id2organism_id && %organism_id2gene_count && %organism_id2list_gene_ids && %accessionAndFeatureId2GeneId) {
		print LOG "data hash created by build_from_table_genes are not empty, skipping creating them again\n";
		return;
	} elsif (%gene_id2organism_id || %organism_id2gene_count || %organism_id2list_gene_ids || %accessionAndFeatureId2GeneId) {
		print LOG "ERROR build_from_table_genes : Some of the data hash created by build_from_table_genes are empty and others are not\n";
		die("ERROR build_from_table_genes : Some of the data hash created by build_from_table_genes are empty and others are not\n");
	}

	# dependencies
	print LOG "Resolving dependencies...\n";
	build_from_table_elements();

	my $file = "$SiteConfig::DATADIR/Analyse_stats/build_from_table_genes_$suffix_file";
	my $use_existing_file = check_file_exists_ask_use_it($file, "build_from_table_genes");

	if($use_existing_file == 1){
		# parse existing file
		print LOG "build_from_table_genes : Using and parsing existing file $file\n";
		open(my $fh, '<:encoding(UTF-8)', $file) or die "Could not open file '$file' $!";
		while (my $row = <$fh>) {
			chomp $row;
			$row =~ s/^\s+|\s+$//g;
			#print "$row\n";
			my $idx_gene_id = 0;
			my $idx_organism_id = 1;
			my $idx_element_id = 2;
			my $idx_feature_id = 3;
			if($row =~ m/^#/){
				# header should be #gene_id\torganism_id\telement_id\tfeature_id\n
				$row =~ s/#//g; # remove #
				my @columns = split /\t/, $row;
				if($columns[$idx_gene_id] ne "gene_id"){
					print LOG "ERROR build_from_table_genes : parsing header in file \"$file\" but \$columns[$idx_gene_id] ne gene_id\n";
					die("ERROR build_from_table_genes : parsing header in file \"$file\" but \$columns[$idx_gene_id] ne gene_id\n");
				}
				if($columns[$idx_organism_id] ne "organism_id"){
					print LOG "ERROR build_from_table_genes : parsing header in file \"$file\" but \$columns[$idx_organism_id] ne organism_id\n";
					die("ERROR build_from_table_genes : parsing header in file \"$file\" but \$columns[$idx_organism_id] ne organism_id\n");
				}
				if($columns[$idx_element_id] ne "element_id"){
					print LOG "ERROR build_from_table_genes : parsing header in file \"$file\" but \$columns[$idx_element_id] ne element_id\n";
					die("ERROR build_from_table_genes : parsing header in file \"$file\" but \$columns[$idx_element_id] ne element_id\n");
				}
				if($columns[$idx_feature_id] ne "feature_id"){
					print LOG "ERROR build_from_table_genes : parsing header in file \"$file\" but \$columns[$idx_feature_id] ne feature_id\n";
					die("ERROR build_from_table_genes : parsing header in file \"$file\" but \$columns[$idx_feature_id] ne feature_id\n");
				}
			} else {
				# data
				my @columns = split /\t/, $row;
				my $gene_id = -1;
				my $organism_id = -1;
				my $element_id = -1;
				my $feature_id = -1;
				if($columns[$idx_gene_id] !~ m/^\d+$/){
					print LOG "ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_gene_id] !~ m/^\d+\$/\n";
					die("ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_gene_id] !~ m/^\d+\$/\n");
				}
				if($columns[$idx_organism_id] !~ m/^\d+$/){
					print LOG "ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_organism_id] !~ m/^\d+\$/\n";
					die("ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_organism_id] !~ m/^\d+\$/\n");
				}
				if($columns[$idx_element_id] !~ m/^\d+$/){
					print LOG "ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_element_id] !~ m/^\d+\$/\n";
					die("ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_element_id] !~ m/^\d+\$/\n");
				}
				if($columns[$idx_feature_id] !~ m/^\d+$/){
					print LOG "ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_feature_id] !~ m/^\d+\$/\n";
					die("ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_feature_id] !~ m/^\d+\$/\n");
				}
				$gene_id = $columns[$idx_gene_id];
				$organism_id = $columns[$idx_organism_id];
				$element_id = $columns[$idx_element_id];
				$feature_id = $columns[$idx_feature_id];
				if($gene_id == -1 || $organism_id == -1 || $element_id == -1 || $feature_id == -1 ){
					print LOG "ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but at least one of the variables == -1 :  \$gene_id = $gene_id ; \$organism_id = $organism_id ; \$element_id = $element_id ; \$feature_id = $feature_id\n";
					die("ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but at least one of the variables == -1 :  \$gene_id = $gene_id ; \$organism_id = $organism_id ; \$element_id = $element_id ; \$feature_id = $feature_id\n");
				} else {
					if(exists $gene_id2organism_id{$gene_id}){
						print LOG "ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but exists \$gene_id2organism_id{\$gene_id} : ".$gene_id2organism_id{$gene_id}."\n";
						die("ERROR build_from_table_genes : parsing data line \"$row\" in file \"$file\" but exists \$gene_id2organism_id{\$gene_id} : ".$gene_id2organism_id{$gene_id}."\n");
					} else {
						$gene_id2organism_id{$gene_id} = $organism_id;
					}
					if(exists $organism_id2gene_count{$organism_id}){
						$organism_id2gene_count{$organism_id}++;
					} else {
						$organism_id2gene_count{$organism_id} = 1;
					}
					if(exists $organism_id2list_gene_ids{$organism_id}){
						my $ref_list_gene_idIT = $organism_id2list_gene_ids{$organism_id};
						push @$ref_list_gene_idIT, $gene_id;
					} else {
						my @list_gene_idIT = ();
						push @list_gene_idIT, $gene_id;
						$organism_id2list_gene_ids{$organism_id} = \@list_gene_idIT;
					}
					my $accession = "";
					if(exists $element_id2accession{$element_id}){
						$accession = $element_id2accession{$element_id};
					} else {
						print LOG "ERROR build_from_table_genes : element_id $element_id was not found in \%element_id2accession\n";
						die("ERROR build_from_table_genes : element_id $element_id was not found in \%element_id2accession\n");
					}
					my $gene_id_alt = "${accession}_A2FID_${feature_id}";
					if(exists $accessionAndFeatureId2GeneId{$gene_id_alt}){
						print LOG "ERROR build_from_table_genes : gene_id_alt $gene_id_alt was already found in \%accessionAndFeatureId2GeneId\n";
						die("ERROR build_from_table_genes : gene_id_alt $gene_id_alt was already found in \%accessionAndFeatureId2GeneId\n");
					} else {
						$accessionAndFeatureId2GeneId{$gene_id_alt} = $gene_id;
					}
				}
			}
		}

	} else {
		# query db and create file
		print LOG "build_from_table_genes : querying db to create hash data";
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){print LOG " and file $file";}
		print LOG "\n";

		#open file
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){open(gene_id2organism_id_FILE, '>:encoding(UTF-8)', $file) or die ("Can not open $file\n");}
		#print header in file
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){print gene_id2organism_id_FILE "#gene_id\torganism_id\telement_id\tfeature_id\n";}
		$command_string = "SELECT gene_id, organism_id, element_id, feature_id FROM genes";
		if (@list_orga_ids_to_analyse) {
			$command_string .=  " WHERE organism_id IN (".join(",", @list_orga_ids_to_analyse).")";
		}
		#$st = $ORIGAMI::dbh->prepare("SELECT gene_id, organism_id, element_id, feature_id FROM genes WHERE organism_id IN (".join(",", @list_orga_ids_to_analyse).")");
		$st = $ORIGAMI::dbh->prepare($command_string);
		
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {
			my $gene_id = $res->{gene_id};
			my $organism_id = $res->{organism_id};
			my $element_id = $res->{element_id};
			my $feature_id = $res->{feature_id};
			if($gene_id !~ m/^\d+$/i){
				print LOG "ERROR build_from_table_genes : gene_id $gene_id !~ m/^\\d+\$/i\n";
				die("ERROR build_from_table_genes : gene_id $gene_id !~ m/^\\d+\$/i\n");
			}
			if($organism_id !~ m/^\d+$/i){
				print LOG "ERROR build_from_table_genes : organism_id $organism_id !~ m/^\\d+\$/i\n";
				die("ERROR build_from_table_genes : organism_id $organism_id !~ m/^\\d+\$/i\n");
			}
			if($element_id !~ m/^\d+$/i){
				print LOG "ERROR build_from_table_genes : element_id $element_id !~ m/^\\d+\$/i\n";
				die("ERROR build_from_table_genes : element_id $element_id !~ m/^\\d+\$/i\n");
			}
			if($feature_id !~ m/^\d+$/i){
				print LOG "ERROR build_from_table_genes : feature_id $feature_id !~ m/^\\d+\$/i\n";
				die("ERROR build_from_table_genes : feature_id $feature_id !~ m/^\\d+\$/i\n");
			}
			if(exists $gene_id2organism_id{$gene_id}){
				print LOG "ERROR build_from_table_genes : gene_id $gene_id was already found in \%gene_id2organism_id\n";
				die("ERROR build_from_table_genes : gene_id $gene_id was already found in \%gene_id2organism_id\n");
			} else {
				$gene_id2organism_id{$gene_id} = $organism_id;
			}
			if(exists $organism_id2gene_count{$organism_id}){
				$organism_id2gene_count{$organism_id}++;
			} else {
				$organism_id2gene_count{$organism_id} = 1;
			}
			if(exists $organism_id2list_gene_ids{$organism_id}){
				my $ref_list_gene_idIT = $organism_id2list_gene_ids{$organism_id};
				push @$ref_list_gene_idIT, $gene_id;
			} else {
				my @list_gene_idIT = ();
				push @list_gene_idIT, $gene_id;
				$organism_id2list_gene_ids{$organism_id} = \@list_gene_idIT;
			}
			my $accession = "";
			if(exists $element_id2accession{$element_id}){
				$accession = $element_id2accession{$element_id};
			} else {
				print LOG "ERROR build_from_table_genes : element_id $element_id was not found in \%element_id2accession\n";
				die("ERROR build_from_table_genes : element_id $element_id was not found in \%element_id2accession\n");
			}
			my $gene_id_alt = "${accession}_A2FID_${feature_id}";
			if(exists $accessionAndFeatureId2GeneId{$gene_id_alt}){
				print LOG "ERROR build_from_table_genes : gene_id_alt $gene_id_alt was already found in \%accessionAndFeatureId2GeneId\n";
				die("ERROR build_from_table_genes : gene_id_alt $gene_id_alt was already found in \%accessionAndFeatureId2GeneId\n");
			} else {
				$accessionAndFeatureId2GeneId{$gene_id_alt} = $gene_id;
			}
			#print data in file
			if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){print gene_id2organism_id_FILE "$gene_id\t$organism_id\t$element_id\t$feature_id\n";}

		}

		#close file
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){close(gene_id2organism_id_FILE);}
	}

	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done build_from_table_genes in $elapsed seconds ; used $rounded MB of RAM.\n";
	print LOG "\t-> created ".scalar(keys %gene_id2organism_id)." entries in \%gene_id2organism_id.\n";
	print LOG "\t-> created ".scalar(keys %organism_id2gene_count)." entries in \%organism_id2gene_count.\n";
	print LOG "\t-> created ".scalar(keys %organism_id2list_gene_ids)." entries in \%organism_id2list_gene_ids.\n";
	print LOG "\t-> created ".scalar(keys %accessionAndFeatureId2GeneId)." entries in \%accessionAndFeatureId2GeneId.\n";

}
if ($BUILD_from_table_genes =~ m/^ON$/i){
	build_from_table_genes();
}



# store in %taxon_id2scientific_name
sub build_from_table_taxo_name {

	print LOG "** Starting build from table taxo name...\n";
	my $t0      = [gettimeofday()];

	# dependancies
	# no

	my $requete_st = "SELECT ncbi_taxid, name FROM micado.taxo_names WHERE type = 'scientific name'";
	$st = $ORIGAMI_ncbi_taxonomy::dbh->prepare($requete_st);
	$st->execute or die("Pb execute $!");
	while ( my $res = $st->fetchrow_hashref() ) {
		my $ncbi_taxid = $res->{ncbi_taxid};
		my $name = $res->{name};
		$taxon_id2scientific_name{$ncbi_taxid} = $name;
	}

	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done build from table taxo name in $elapsed seconds ; used $rounded MB of RAM.\n";
}


if ($BUILD_from_table_taxo =~ m/^ON$/i){

	print LOG "** Starting build from table taxo...\n";
	my $t0      = [gettimeofday()];

	#dependancies
	# list all organisms with genes : %gene_id2organism_id and %organism_id2gene_count and %organism_id2list_gene_ids
	build_from_table_genes();
	# list all genes with known annotation -> %orga_id2hash_gene_id_known_annotation
	stats_all_genes_with_known_annotation();
	# list all genes with known annotation -> %taxon_id2scientific_name
	build_from_table_taxo_name();


	# define output files
	my $file = "$SiteConfig::DATADIR/Analyse_stats/build_from_table_taxo_$suffix_file";

	# skip if already output file
	if( -e $file ){
		print LOG "ERROR BUILD_from_table_taxo : the output files ($file) already exists, please remove it manually and launch the script again if you wish to recreate it.\n";
		die("ERROR BUILD_from_table_taxo : the output files ($file) already exists, please remove it manually and launch the script again if you wish to recreate it.\n");
	}

	# open files and print headers
	#print in fie Q_ORGA_FUSION_SYNTENIES_FILE
	open(BUILD_from_table_taxo_FILE, '>:encoding(UTF-8)', $file) or die ("Can not open $file\n");
	#print header
	print BUILD_from_table_taxo_FILE "#q_organism_id\ttaxon id\ttaxo node 1\ttaxo node 2\ttaxo node 3\ttaxo node 4\ttaxo node 5\ttaxo node 6\ttaxo node 7\ttaxo node 8\ttaxo node 9\ttaxo node 10\n";


	print LOG "Start printing file $file\n";

	my @list_organism_id = sort { $a <=> $b } keys %organism_id2list_gene_ids;
	my $nbre_possibilite = scalar (@list_organism_id);
	print LOG "getting taxonomic info for each q_orga ... ($nbre_possibilite combinations to do)\n";


	# for each q_organism
	my $count_combination_done = 0;
	for my $i (0 .. $#list_organism_id) {
	      	my $q_orga_id = $list_organism_id[$i];

		my $q_taxon_id = -1;
		if ( exists $organism_id2taxon_id{$q_orga_id} ) {
			$q_taxon_id = $organism_id2taxon_id{$q_orga_id};
		} else {
			print LOG "ERROR BUILD_from_table_taxo : didn't find q_taxon_id in \%organism_id2taxon_id for orga = $q_orga_id\n";
			die("ERROR BUILD_from_table_taxo : didn't find q_taxon_id in \%organism_id2taxon_id for orga = $q_orga_id\n");
		}

		$count_combination_done++;
		print LOG "\tStarting q organism id : $q_orga_id ($count_combination_done / $nbre_possibilite)\n";
		if ($count_combination_done > 1) {
			my $elapsed = tv_interval( $t0 );
			my $MEM_used = `ps h -o vsz $$`;
			$MEM_used =~ s/\R//g;
			my $rounded_MEM = sprintf("%.1f", ($MEM_used/1000) );
			my $rounded_avg_seconds = sprintf("%.3f", ($elapsed/$count_combination_done) );
			print LOG "\t\t $rounded_avg_seconds seconds per combination in average ; Total used $rounded_MEM MB of RAM.\n\n";
		}

		my $requete_st = "SELECT ncbi_taxid, lineage FROM micado.taxo WHERE ncbi_taxid = $q_taxon_id";

		$st = $ORIGAMI_ncbi_taxonomy::dbh->prepare($requete_st);
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {
			my $ncbi_taxid = $res->{ncbi_taxid};
			my $ref_array_lineage = $res->{lineage};

			#print "$ncbi_taxid : ".join(",", @$ref_array_lineage)."\n";

			print BUILD_from_table_taxo_FILE "$q_orga_id\t$ncbi_taxid";
			#my @split_lineage = split /,/, $lineage;
			foreach my $lineage_taxidIT (@$ref_array_lineage) {

				#print "$lineage_taxidIT from ".join(",", @$ref_array_lineage)."\n";

				if ( exists $taxon_id2scientific_name{$lineage_taxidIT} ) {
					print BUILD_from_table_taxo_FILE "\t".$taxon_id2scientific_name{$lineage_taxidIT};
				} else {
					print LOG "ERROR BUILD_from_table_taxo : didn't find taxid $lineage_taxidIT in \%taxon_id2scientific_name ; orga tested = $q_orga_id ; full lineage = ".join(",", @$ref_array_lineage)."\n";
					die("ERROR BUILD_from_table_taxo : didn't find taxid $lineage_taxidIT in \%taxon_id2scientific_name ; orga tested = $q_orga_id ; full lineage = ".join(",", @$ref_array_lineage)."\n");
				}
			}
			print BUILD_from_table_taxo_FILE "\n";

		}# while ( my $res = $st->fetchrow_hashref() ) {
	}# for my $i (0 .. $#list_organism_id) {

	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done build from table taxo in $elapsed seconds ; used $rounded MB of RAM.\n";

}

# RQ : hash is too memory consuming to complete on origami_cg : MAX MEM% = +50 (+68G) alors que 23% était complèter 182,960,265 sur 572,761,311 total rows
# RQ : it takes about 1 minute for a grep to complete on the origami_cg_file ()
# a fast way to read a line ex 52 and exit : sed '52q;d' $file ; if file is sorted out then can do divideAndConquer
# querying database by alignment_id is almost instantaneous, go with this technique
=pod
# build %alignment_id2pairs_greater_1
sub build_from_table_alignments{

	print LOG "** Starting build_from_table_alignments...\n";
	my $t0      = [gettimeofday()];

	# skip if already done
	if (%alignment_id2pairs_greater_1) {
		print LOG "data hash created by build_from_table_alignments are not empty, skipping creating them again\n";
		return;
	}

	# dependencies
	# no

	my $file = "$SiteConfig::DATADIR/Analyse_stats/build_from_table_alignments_$suffix_file";
	my $use_existing_file = check_file_exists_ask_use_it($file, "build_from_table_alignments");

	if($use_existing_file == 1){
		# parse existing file
		print LOG "build_from_table_alignments : Using and parsing existing file $file\n";
		open(my $fh, '<:encoding(UTF-8)', $file) or die "Could not open file '$file' $!";
		while (my $row = <$fh>) {
			chomp $row;
			$row =~ s/^\s+|\s+$//g;
			#print "$row\n";
			my $idx_alignment_id = 0;
			my $idx_pairs = 1;
			if($row =~ m/^#/){
				# header should be #alignment_id\tpairs\n
				$row =~ s/#//g; # remove #
				my @columns = split /\t/, $row;
				if($columns[$idx_alignment_id] ne "alignment_id"){
					print LOG "ERROR build_from_table_alignments : parsing header in file \"$file\" but \$columns[$idx_alignment_id] ne alignment_id\n";
					die("ERROR build_from_table_alignments : parsing header in file \"$file\" but \$columns[$idx_alignment_id] ne alignment_id\n");
				}
				if($columns[$idx_pairs] ne "pairs"){
					print LOG "ERROR build_from_table_alignments : parsing header in file \"$file\" but \$columns[$idx_pairs] ne pairs\n";
					die("ERROR build_from_table_alignments : parsing header in file \"$file\" but \$columns[$idx_pairs] ne pairs\n");
				}
			} else {
				# data
				my @columns = split /\t/, $row;
				my $alignment_id = -1;
				my $pairs = -1;
				if($columns[$idx_alignment_id] !~ m/^\d+$/){
					print LOG "ERROR build_from_table_alignments : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_alignment_id] !~ m/^\d+\$/\n";
					die("ERROR build_from_table_alignments : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_alignment_id] !~ m/^\d+\$/\n");
				}
				if($columns[$idx_pairs] !~ m/^\d+$/){
					print LOG "ERROR build_from_table_alignments : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_pairs] !~ m/^\d+\$/\n";
					die("ERROR build_from_table_alignments : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_pairs] !~ m/^\d+\$/\n");
				}
				$alignment_id = $columns[$idx_alignment_id];
				$pairs = $columns[$idx_pairs];
				if($alignment_id == -1 || $pairs == -1  ){
					print LOG "ERROR build_from_table_alignments : parsing data line \"$row\" in file \"$file\" but at least one of the variables == -1 :  \$alignment_id = $alignment_id ; \$pairs = $pairs \n";
					die("ERROR build_from_table_alignments : parsing data line \"$row\" in file \"$file\" but at least one of the variables == -1 :  \$alignment_id = $alignment_id ; \$pairs = $pairs \n");
				} else {
					if(exists $alignment_id2pairs_greater_1{$alignment_id}){
						print LOG "ERROR build_from_table_alignments : parsing data line \"$row\" in file \"$file\" but exists \$alignment_id2pairs_greater_1{\$alignment_id} : ".$alignment_id2pairs_greater_1{$alignment_id}."\n";
						die("ERROR build_from_table_alignments : parsing data line \"$row\" in file \"$file\" but exists \$alignment_id2pairs_greater_1{\$alignment_id} : ".$alignment_id2pairs_greater_1{$alignment_id}."\n");
					} else {
						$alignment_id2pairs_greater_1{$alignment_id} = $pairs;
					}
				}
			}
		}

	} else {
		# query db and create file
		print LOG "build_from_table_alignments : querying db to create hash data";
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){print LOG " and file $file";}
		print LOG "\n";

		#open file
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){open(build_from_table_alignments_FILE, '>:encoding(UTF-8)', $file) or die ("Can not open $file\n");}
		#print header in file
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){print build_from_table_alignments_FILE "#alignment_id\tpairs\n";}

		print LOG "querying db to create file build_from_table_alignments $file\n";
		$st = $ORIGAMI::dbh->prepare("SELECT alignment_id, pairs FROM alignments WHERE pairs >= 2");
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {
			my $alignment_id = $res->{alignment_id};
			my $pairs = $res->{pairs};
			if($alignment_id !~ m/^\d+$/i){
				print LOG "ERROR build_from_table_alignments : alignment_id $alignment_id !~ m/^\\d+\$/i\n";
				die("ERROR build_from_table_alignments : alignment_id $alignment_id !~ m/^\\d+\$/i\n");
			}
			if($pairs !~ m/^\d+$/i){
				print LOG "ERROR build_from_table_alignments : pairs $pairs !~ m/^\\d+\$/i\n";
				die("ERROR build_from_table_alignments : pairs $pairs !~ m/^\\d+\$/i\n");
			}
			if(exists $alignment_id2pairs_greater_1{$alignment_id}){
				print LOG "ERROR build_from_table_alignments : alignment_id $alignment_id was already found in \%alignment_id2pairs_greater_1\n";
				die("ERROR build_from_table_alignments : alignment_id $alignment_id was already found in \%alignment_id2pairs_greater_1\n");
			} else {
				$alignment_id2pairs_greater_1{$alignment_id} = $pairs;
			}

			#print data in file
			if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){print build_from_table_alignments_FILE "$alignment_id\t$pairs\n";}
		}

		#close file
		if ($CREATE_TMP_BUILD_FILES =~ m/^ON$/i){close(build_from_table_alignments_FILE);}
	}

	my $elapsed = tv_interval( $t0 );
	print LOG "** Done build_from_table_alignments in $elapsed seconds ; used ".( total_size( \%:: ) / 1000000000)." GB of RAM.\n";
	print LOG "\t-> created ".scalar(keys %alignment_id2pairs_greater_1)." entries in \%alignment_id2pairs_greater_1.\n";

}
# build_from_table_alignments
if ($BUILD_from_table_alignments =~ m/^ON$/i){
	build_from_table_alignments();
}
=cut


################################################
################ package stats
################################################


# STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION
if ($STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION =~ m/^ON$/i){

	print LOG "** Starting stats on genes with a 'gene' name qualifier but no 'product' or 'function'...\n";
	my $t0      = [gettimeofday()];

	$st = $ORIGAMI::dbh->prepare("SELECT elements.accession, genes.feature_id FROM elements, genes WHERE genes.element_id = elements.element_id AND elements.organism_id IN (".join(",", @list_orga_ids_to_analyse).")");
	$st->execute or die("Pb execute $!");
	my @accession_featureid = ();
	while ( my $res = $st->fetchrow_hashref() ) {
		my $accession = $res->{accession};
		my $feature_id = $res->{feature_id};
		push @accession_featureid, "${accession}_A2FID_${feature_id}";
	}
	my $count_genes_name_function_locus_tag = 0;
	my $count_genes_function_locus_tag_NO_name = 0;
	my $count_genes_function_name_NO_locus_tag = 0;
	my $count_genes_name_NO_locus_tag_function = 0;
	my $count_genes_NO_locus_tag_name = 0;
	my $count_genes_name_locus_tag_NO_function = 0;
	my $count_genes_locus_tag_NO_function_name = 0;

	foreach my $accession_featureidIT (@accession_featureid) {
		my @accession_featureidIT = split(/_A2FID_/, $accession_featureidIT);
		my $accession = $accession_featureidIT[0];
		my $feature_id = $accession_featureidIT[1];

		$st = $ORIGAMI::dbh->prepare("SELECT type_qual FROM micado.qualifiers WHERE accession = \'$accession\' AND code_feat = $feature_id AND (type_qual = 'gene' OR type_qual = 'gene_name' OR type_qual = 'product' OR type_qual = 'function' OR type_qual = 'locus_tag')");
		$st->execute or die("Pb execute $!");
		my $found_gene_name = 0;
		my $found_function = 0;
		my $found_locus_tag = 0;
		while ( my $res = $st->fetchrow_hashref() ) {
			my $type_qual = $res->{type_qual};
			if($type_qual eq "gene"
				|| $type_qual eq "gene_name"){
				$found_gene_name = 1;
			}
			if($type_qual eq "product"
				|| $type_qual eq "function"){
				$found_function = 1;
			}
			if($type_qual eq "locus_tag"){
				$found_locus_tag = 1;
			}
		}
		if($found_gene_name == 1 && $found_function == 1 && $found_locus_tag == 1){
			$count_genes_name_function_locus_tag++;
		} elsif ($found_gene_name == 0 && $found_function == 1 && $found_locus_tag == 1) {
			$count_genes_function_locus_tag_NO_name++;
		} elsif ($found_gene_name == 1 && $found_function == 1 && $found_locus_tag == 0) {
			$count_genes_function_name_NO_locus_tag++;
			print LOG "WARNING gene accession $accession feature_id $feature_id has a gene name and function but no locus tag\n";
		} elsif ($found_gene_name == 1 && $found_function == 0 && $found_locus_tag == 0) {
			$count_genes_name_NO_locus_tag_function++;
			print LOG "WARNING gene accession $accession feature_id $feature_id has a gene name but no locus tag or function\n";
		} elsif ($found_gene_name == 0 && $found_locus_tag == 0) {
			$count_genes_NO_locus_tag_name++;
			print LOG "WARNING gene accession $accession feature_id $feature_id has no locus tag neither name\n";
		} elsif ($found_gene_name == 1 && $found_function == 0 && $found_locus_tag == 1) {
			$count_genes_name_locus_tag_NO_function++;
			print LOG "WARNING gene accession $accession feature_id $feature_id has a gene name and locus tag but no function\n";
		} elsif ($found_gene_name == 0 && $found_function == 0 && $found_locus_tag == 1) {
			$count_genes_locus_tag_NO_function_name++;
		} else {
			print LOG "ERROR STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION : \$found_gene_name == $found_gene_name && \$found_function == $found_function && \$found_locus_tag == $found_locus_tag\n";
			die("ERROR STATS_GENE_WITH_NAME_BUT_NO_PRODUCT_FONCTION : \$found_gene_name == $found_gene_name && \$found_function == $found_function && \$found_locus_tag == $found_locus_tag\n");
		}

	}

	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done stats on genes with a 'gene' name qualifier but no 'product' or 'function' in $elapsed seconds ; used $rounded MB of RAM.\n";
	print LOG "\tcount genes with qualifiers name, function, and locus tag = $count_genes_name_function_locus_tag\n";
	print LOG "\tcount genes with qualifiers function, and locus tag but no name = $count_genes_function_locus_tag_NO_name\n";
	print LOG "\tcount genes with qualifiers name and function BUT NO locus tag = $count_genes_function_name_NO_locus_tag\n";
	print LOG "\tcount genes with qualifiers name BUT NO function or locus tag = $count_genes_name_NO_locus_tag_function\n";
	print LOG "\tcount genes with qualifiers NO name or locus tag = $count_genes_NO_locus_tag_name\n";
	print LOG "\tcount genes with qualifiers name, and locus tag BUT NO function = $count_genes_name_locus_tag_NO_function\n";
	print LOG "\tcount genes with qualifiers locus tag but no name or function = $count_genes_locus_tag_NO_function_name\n";

}


# package list all genes with known annotation

# return 1 if qualifier is a known annotation, -1 if this gene is pseudogene, 0 otherwise
sub check_if_qualifier_is_really_a_known_annotation {
	my ($qualifier) = @_;
	if($qualifier =~ m/unknown/i
		|| $qualifier =~ m/no\s+known/i
		|| $qualifier =~ m/not\s+known/i
		|| $qualifier =~ m/hypothetical/i
		|| $qualifier =~ m/^(conserved)?\s*(putative)?\s*uncharacterized\s+(conserved)?\s*(domain)?\s*protein,?\s*([\w\(\)]+)?$/i
		|| $qualifier =~ m/^protein\s+not\s+characterized$/i
		|| $qualifier =~ m/^(conserved)?\s*(putative)?\s*unclassified\s+(conserved)?\s*(domain)?\s*protein,?\s*([\w\(\)]+)?$/i
		|| $qualifier =~ m/^protein\s+not\s+classified$/i
		|| $qualifier =~ m/^uncharacterized protein conserved in bacteria$/i
		|| $qualifier =~ m/^conservede uncharacterized protein$/i
		|| $qualifier =~ m/^uncharacterized domain ,?\s*(\w+)?$/i
		|| $qualifier =~ m/full=(conserved)?\s*(putative)?\s*uncharacterized\s+(conserved)?\s*(domain)?\s*protein\;/i
		|| $qualifier =~ m/^[\d\.\,\-]*phenotype[\d\.\,\-]*$/i
		|| $qualifier =~ m/^[\d\.\,\-]*putative[\d\.\,\-]*$/i
		|| $qualifier =~ m/^[\d\.\,\-]*No\s+COGs+number[\d\.\,\-]*$/i
		|| $qualifier =~ m/^[\d\.\,\-]*enzyme[\d\.\,\-]*$/i
		|| $qualifier =~ m/^[\d\.\,\-]*putative\s+enzyme[\d\.\,\-]*$/i
		|| $qualifier =~ m/^[\d\.\,\-]*miscellaneous[\d\.\,\-]*$/i
		|| $qualifier =~ m/^[\d\.\,\-]*general[\d\.\,\-]*$/i
		|| $qualifier =~ m/^[\d\.\,\-]*other[\d\.\,\-]*$/i
		|| $qualifier =~ m/^[\d\.\,\-]*conserved[\d\.\,\-]*$/i
		|| $qualifier =~ m/^[\d\.\,\-]*conserved\s+domain\s+protein[\d\.\,\-]*$/i
		|| $qualifier =~ m/^6\s+:\s+no\s+similarity$/i
		|| $qualifier =~ m/^5\.\d\s+:\s+from\s+.+$/i
	){
		# not a really known annotation
		return 0;
	} elsif ($qualifier =~ m/pseudogene/i) {
		return -1;
	}
	return 1;
=pod
A protein has a known function if its annotation has either :
- a product not containing the keywords (case insensitive)
	X- unknown
	X- no known
	X- not known
	- hypothetical
	- uncharacterized
	- not characterized
	- pseudogene
	- unclassified
	- Not classified
	- phenotype [by itself or with only numbers, point, comma, dash]
	- putative [by itself or with only numbers, point, comma, dash]
	- No COG number [by itself or with only numbers, point, comma, dash]
	- enzyme [by itself or with only numbers, point, comma, dash]
	- putative enzyme [by itself or with only numbers, point, comma, dash]
	- Miscellaneous [by itself or with only numbers, point, comma, dash]
	- General [by itself or with only numbers, point, comma, dash]
	- Other [by itself or with only numbers, point, comma, dash]
	- Conserved [by itself or with only numbers, point, comma, dash]
	- conserved domain protein [by itself or with only numbers, point, comma, dash]
	- protein [by itself or with only numbers, point, comma, dash]
To exclude from SubtiList Functional classification codes :	- 6 : No similarity
To exclude from SubtiList Functional classification codes :    	- 5.1 : From ((((((((( B. subtilis or other )))))))))
To exclude from SubtiList Functional classification codes :    	- 5.2 : From ((((((((( other organisms )))))))))
	((((((((( - putative PAS RETENU ))))))))))))
- a function (idem product above)
- a Molecular Function (idem product above)
- a Biological Process (idem product above)
- an EC_number non null
AND is not annotated with type_qual
- pseudogene or pseudo
=cut

}

#fill %orga_id2hash_gene_id_known_annotation and file stats_all_genes_with_known_annotation_$suffix_file"
# create 2 graphs :
# - distribution du nombre d'occurence d'organismes (X) selon pourcentage de gènes dont l'annotation est connu (Y)
# - distribution pourcentage de gènes dont l'annotation est connu (X) selon année d'annotation (earliest_date_seq_annotation_organism) ; RQ : tracer tous les points + courbes en prennant la moyenne pourcentage selon année
sub stats_all_genes_with_known_annotation {

	print LOG "** Starting listing all genes with known annotation...\n";
	my $t0      = [gettimeofday()];

	# skip if already done
	if (%orga_id2hash_gene_id_known_annotation) {
		print LOG "\%orga_id2hash_gene_id_known_annotation is not empty, skipping creating it again\n";
		return;
	}

	# dependencies
	print LOG "Resolving dependencies...\n";
	#build_AccessionAndFeatureId2GeneId();
	build_from_table_genes();

	my $file = "$SiteConfig::DATADIR/Analyse_stats/stats_all_genes_with_known_annotation_$suffix_file";
	my $use_existing_file = check_file_exists_ask_use_it($file, "stats_all_genes_with_known_annotation");

	if($use_existing_file == 1){
		# parse existing file
		print LOG "Using and parsing existing file  $file\n";
		open(my $fh, '<:encoding(UTF-8)', $file) or die "Could not open file '$file' $!";
		while (my $row = <$fh>) {
			chomp $row;
			$row =~ s/^\s+|\s+$//g;
			#print "$row\n";
			my $idx_organism_id = 0;
			my $idx_list_gene_id = 6;
			if($row =~ m/^#/){
				# header should be #organism_id\tearliest_date_seq_annotation_organism\tcount_genes_known_annotation\tcount_non_coding_genes\tcount_pseudogene\torga_CDS_count\tlist_gene_id_known_annotation\n
				$row =~ s/#//g; # remove #
				my @columns = split /\t/, $row;
				if($columns[$idx_organism_id] ne "organism_id"){
					print LOG "ERROR stats_all_genes_with_known_annotation : parsing header in file \"$file\" but \$columns[$idx_organism_id] ne organism_id\n";
					die("ERROR stats_all_genes_with_known_annotation : parsing header in file \"$file\" but \$columns[$idx_organism_id] ne organism_id\n");
				}
				if($columns[$idx_list_gene_id] ne "list_gene_id_known_annotation"){
					print LOG "ERROR stats_all_genes_with_known_annotation : parsing header in file \"$file\" but \$columns[$idx_list_gene_id] ne list_gene_id_known_annotation\n";
					die("ERROR stats_all_genes_with_known_annotation : parsing header in file \"$file\" but \$columns[$idx_list_gene_id] ne list_gene_id_known_annotation\n");
				}
			} else {
				# data
				my @columns = split /\t/, $row;
				my $orga_id = -1;
				my %hash_gene_id = ();
				if($columns[$idx_organism_id] !~ m/^\d+$/){
					print LOG "ERROR stats_all_genes_with_known_annotation : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_organism_id]  !~ m/^\d+\$/\n";
					die("ERROR stats_all_genes_with_known_annotation : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_organism_id]  !~ m/^\d+\$/\n");
				}
				if($columns[$idx_list_gene_id] !~ m/^[\d,]+$/){
					print LOG "ERROR stats_all_genes_with_known_annotation : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_list_gene_id]  !~ m/^[\d,]+\$/\n";
					die("ERROR stats_all_genes_with_known_annotation : parsing data line \"$row\" in file \"$file\" but \$columns[$idx_list_gene_id]  !~ m/^[\d,]+\$/\n");
				}
				$orga_id = $columns[$idx_organism_id];
				my @list_gene_id_TMP = split /,/, $columns[$idx_list_gene_id];
				@hash_gene_id{@list_gene_id_TMP} = undef;
				if($orga_id <= 0 || scalar(keys %hash_gene_id) <= 0 ){
					print LOG "ERROR stats_all_genes_with_known_annotation : parsing data line \"$row\" in file \"$file\" but \$orga_id = $orga_id <= 0 || scalar(keys \%list_gene_id) = ".scalar(keys %hash_gene_id)." <= 0\n";
					die("ERROR stats_all_genes_with_known_annotation : parsing data line \"$row\" in file \"$file\" but \$orga_id = $orga_id <= 0 || scalar(keys \%list_gene_id) = ".scalar(keys %hash_gene_id)." <= 0\n");
				} else {
					if(exists $orga_id2hash_gene_id_known_annotation{$orga_id}){
						my $ref_hash_gene_ids = $orga_id2hash_gene_id_known_annotation{$orga_id};
						print LOG "ERROR stats_all_genes_with_known_annotation : parsing data line \"$row\" in file \"$file\" but exists \$orga_id2hash_gene_id_known_annotation{\$orga_id} : ".join(",",keys %$ref_hash_gene_ids)."\n";
						die("ERROR stats_all_genes_with_known_annotation : parsing data line \"$row\" in file \"$file\" but exists \$orga_id2hash_gene_id_known_annotation{\$orga_id} : ".join(",",keys %$ref_hash_gene_ids)."\n");
					} else {
						$orga_id2hash_gene_id_known_annotation{$orga_id} = \%hash_gene_id;
					}
				}
			}
		}

	} else {
		# query db and create file
		print LOG "querying db to create file with known annotation $file\n";
		$st = $ORIGAMI::dbh->prepare("SELECT accession, code_feat, type_qual, qualifier FROM micado.qualifiers WHERE ( type_qual = \'product\' OR type_qual = \'function\' OR type_qual = \'Molecular Function\' OR type_qual = \'Biological Process\' OR type_qual = \'EC_number\' OR type_qual = \'pseudogene\' OR type_qual = \'pseudo\' ) AND accession IN ('".join("','", @list_accession_to_analyse)."')");
		$st->execute or die("Pb execute $!");
		my %gene_id_known_annotation = ();
		my %gene_id_pseudogene = ();
		while ( my $res = $st->fetchrow_hashref() ) {
			my $accession = $res->{accession};
			my $code_feat = $res->{code_feat};
			my $gene_id_alt = "${accession}_A2FID_${code_feat}";
			my $type_qual = $res->{type_qual};
			my $qualifier = $res->{qualifier};
			if($type_qual eq "product" || $type_qual eq "function"  || $type_qual eq "Molecular Function" || $type_qual eq "Biological Process"){
				$qualifier =~ s/^\s+|\s+$//g; #trim both ends
				my $qualifier_is_really_a_known_annotation = check_if_qualifier_is_really_a_known_annotation($qualifier);
				if ($qualifier_is_really_a_known_annotation == 1) {
					print LOG "Accepted known qualifier term for gene alt id $accession A2FID $code_feat : $qualifier\n";
					$gene_id_known_annotation{$gene_id_alt} = 1;
				} elsif ($qualifier_is_really_a_known_annotation == -1) {
					print LOG "Pseudogene gene alt id $accession A2FID $code_feat : $qualifier\n";
					$gene_id_pseudogene{$gene_id_alt} = 1;
				} else {
					print LOG "Rejected qualifier term for gene alt id $accession A2FID $code_feat as not really a known annotation : $qualifier\n";
				}
			} elsif ($type_qual eq "EC_number") {
				$qualifier =~ s/^\s+|\s+$//g; #trim both ends
				if($qualifier ne ""){
					$gene_id_known_annotation{$gene_id_alt} = 1;
				}
			} elsif ($type_qual eq "pseudogene" || $type_qual eq "pseudo") {
				print LOG "Pseudogene gene alt id (type_qual) $accession A2FID $code_feat : $qualifier\n";
				$gene_id_pseudogene{$gene_id_alt} = 1;
			}
		}

		# remove keys of %gene_id_pseudogene from %gene_id_known_annotation
		print LOG "Start removing pseudogene from known annotation : ".scalar(keys %gene_id_pseudogene)." entries to check...\n";
		foreach my $key_to_remove (keys %gene_id_pseudogene) {
			if(exists $gene_id_known_annotation{$key_to_remove}){
				delete $gene_id_known_annotation{$key_to_remove};
			}
		}
		print LOG "Done removing pseudogene from known annotation.\n";

		#create %orga_id2count_pseudogene (transform each gene_id_alt from %gene_id_known_annotation to real gene_id)
		print LOG "Start creating \%orga_id2count_pseudogene : ".scalar(keys %gene_id_pseudogene)." entries to check...\n";
		foreach my $gene_id_alt (keys %gene_id_pseudogene) {
				my @accession_featureidIT = split(/_A2FID_/, $gene_id_alt);
				my $accession = $accession_featureidIT[0];
				if (exists $accession2element_id{$accession}) {
					my $element_id = $accession2element_id{$accession};
					if (exists $element_id2organism_id{$element_id}) {
						my $organism_id = $element_id2organism_id{$element_id};
						if (exists $orga_id2count_pseudogene{$organism_id}) {
							$orga_id2count_pseudogene{$organism_id}++;
						} else {
							$orga_id2count_pseudogene{$organism_id} = 1;
						}
					} else {
						print LOG "ERROR stats_all_genes_with_known_annotation : element_id $element_id was not found in \%element_id2organism_id\n";
						die("ERROR stats_all_genes_with_known_annotation : element_id $element_id was not found in \%element_id2organism_id\n");
					}
				} else {
					print LOG "ERROR stats_all_genes_with_known_annotation : accession $accession was not found in \%accession2element_id\n";
					die("ERROR stats_all_genes_with_known_annotation : accession $accession was not found in \%accession2element_id\n");
				}
		}
		print LOG "Done creating \%orga_id2count_pseudogene.\n";

		# create %orga_id2hash_gene_id_known_annotation and %orga_id2count_non_coding_gene (transform each gene_id_alt from %gene_id_known_annotation to real gene_id)
		print LOG "Start creating \%orga_id2hash_gene_id_known_annotation and \%orga_id2count_non_coding_gene : ".scalar(keys %gene_id_known_annotation)." entries to check...\n";
		foreach my $gene_id_alt (keys %gene_id_known_annotation) {
			if(exists $accessionAndFeatureId2GeneId{$gene_id_alt}){
				my $gene_id = $accessionAndFeatureId2GeneId{$gene_id_alt};
				if(exists $gene_id2organism_id{$gene_id}){
					my $orga_id = $gene_id2organism_id{$gene_id};
					if(exists $orga_id2hash_gene_id_known_annotation{$orga_id}){
						#my $ref_list_gene_id = $orga_id2hash_gene_id_known_annotation{$orga_id};
						#push @$ref_list_gene_id, $gene_id;
						my $ref_hash_gene_id = $orga_id2hash_gene_id_known_annotation{$orga_id};
						if (exists $$ref_hash_gene_id{$gene_id} ) {
							print LOG "ERROR stats_all_genes_with_known_annotation : gene_id $gene_id was already found in \%ref_hash_gene_id\n";
							die("ERROR stats_all_genes_with_known_annotation : gene_id $gene_id was already found in \%ref_hash_gene_id\n");
						} else {
							$$ref_hash_gene_id{$gene_id} = undef;
						}
					} else {
						#my @list_gene_id = ();
						#push @list_gene_id, $gene_id;
						my %hash_gene_id = ();
						$hash_gene_id{$gene_id} = undef;
						$orga_id2hash_gene_id_known_annotation{$orga_id} = \%hash_gene_id;
					}
				} else {
					print LOG "ERROR stats_all_genes_with_known_annotation : gene_id $gene_id was not found in \%gene_id2organism_id\n";
					die("ERROR stats_all_genes_with_known_annotation : gene_id $gene_id was not found in \%gene_id2organism_id\n");
				}
			} else {
				# not in table genes so not a CDS, can be a RNA, ...
				my @accession_featureidIT = split(/_A2FID_/, $gene_id_alt);
				my $accession = $accession_featureidIT[0];
				if (exists $accession2element_id{$accession}) {
					my $element_id = $accession2element_id{$accession};
					if (exists $element_id2organism_id{$element_id}) {
						my $organism_id = $element_id2organism_id{$element_id};
						if (exists $orga_id2count_non_coding_gene{$organism_id}) {
							$orga_id2count_non_coding_gene{$organism_id}++;
						} else {
							$orga_id2count_non_coding_gene{$organism_id} = 1;
						}
					} else {
						print LOG "ERROR stats_all_genes_with_known_annotation : element_id $element_id was not found in \%element_id2organism_id\n";
						die("ERROR stats_all_genes_with_known_annotation : element_id $element_id was not found in \%element_id2organism_id\n");
					}
				} else {
					print LOG "ERROR stats_all_genes_with_known_annotation : accession $accession was not found in \%accession2element_id\n";
					die("ERROR stats_all_genes_with_known_annotation : accession $accession was not found in \%accession2element_id\n");
				}
				#print LOG "ERROR stats_all_genes_with_known_annotation : alt_gene_id $gene_id_alt (accession_A2FID_feature_id) was not found in \%accessionAndFeatureId2GeneId\n";
				#die("ERROR stats_all_genes_with_known_annotation : alt_gene_id $gene_id_alt (accession_A2FID_feature_id) was not found in \%accessionAndFeatureId2GeneId\n");
			}
		}
		print LOG "Done creating \%orga_id2hash_gene_id_known_annotation and \%orga_id2count_non_coding_gene.\n";

		print LOG "Start printing file $file : ".scalar(keys %orga_id2hash_gene_id_known_annotation)." entries to print...\n";
		#print in tmp file
		open(GENES_KNOWN_ANNOTATION_FILE, '>:encoding(UTF-8)', $file) or die ("Can not open $file\n");
		#open( GENES_KNOWN_ANNOTATION_FILE, "> $file" ) or die ("Can not open $file\n");
		#print header
		print GENES_KNOWN_ANNOTATION_FILE "#organism_id\tearliest_date_seq_annotation_organism\tcount_genes_known_annotation\tcount_non_coding_genes\tcount_pseudogene\torga_CDS_count\tlist_gene_id_known_annotation\n";
		#print data
		foreach my $orga_id (keys %orga_id2hash_gene_id_known_annotation) {
			my $orga_gene_count = 0;
			if(exists $organism_id2gene_count{$orga_id}){
				$orga_gene_count = $organism_id2gene_count{$orga_id};
			} else {
				print LOG "ERROR stats_all_genes_with_known_annotation : orga_id $orga_id was not found in \%organism_id2gene_count\n";
				die("ERROR stats_all_genes_with_known_annotation : orga_id $orga_id was not found in \%organism_id2gene_count\n");
			}
			if($orga_gene_count <= 0){
				print LOG "ERROR stats_all_genes_with_known_annotation : orga_id $orga_id has orga_gene_count <= 0 : $orga_gene_count\n";
				die("ERROR stats_all_genes_with_known_annotation : orga_id $orga_id has orga_gene_count <= 0 : $orga_gene_count\n");
			}
			my $earliest_date_seq_annotation_organism = "";
			if(exists $organism_id2earliest_date_seq{$orga_id}){
				my $date_as_string = $organism_id2earliest_date_seq{$orga_id};
				#RQ : remove 1 day to actual date, no consequences
				my $epoch = str2time($date_as_string);
				my $datetime = DateTime->from_epoch(epoch => $epoch);
				$earliest_date_seq_annotation_organism = $datetime->ymd;   # Retrieves date as a string in 'yyyy-mm-dd' format
			}
			my $ref_hash_gene_known_annotation = $orga_id2hash_gene_id_known_annotation{$orga_id};
			my $count_genes_known_annotation = scalar(keys %$ref_hash_gene_known_annotation);
			#my $percent_genes_known_annotation = ( $count_genes_known_annotation / $orga_gene_count ) * 100;
			# Round number
			#my $rounded_percent = int ( $percent_genes_known_annotation + 0.5 );
			#my $rounded_percent = sprintf("%d", $percent_genes_known_annotation);#was "%.3f"  to 3 digits after decimal point
			my $count_non_coding_genes = 0;
			if(exists $orga_id2count_non_coding_gene{$orga_id}){
				$count_non_coding_genes = $orga_id2count_non_coding_gene{$orga_id};
			} else {
				$count_non_coding_genes = 0;
			}

			my $count_pseudogene = 0;
			if(exists $orga_id2count_pseudogene{$orga_id}){
				$count_pseudogene = $orga_id2count_pseudogene{$orga_id};
			} else {
				$count_pseudogene = 0;
			}

			print GENES_KNOWN_ANNOTATION_FILE "$orga_id\t$earliest_date_seq_annotation_organism\t$count_genes_known_annotation\t$count_non_coding_genes\t$count_pseudogene\t$orga_gene_count\t".join(",", keys %$ref_hash_gene_known_annotation)."\n";

		}
		close(GENES_KNOWN_ANNOTATION_FILE);
		print LOG "Done printing file $file.\n";
	}

	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done listing all genes with known annotation in $elapsed seconds ; used $rounded MB of RAM.\n";
	print LOG "\t-> created ".scalar(keys %orga_id2hash_gene_id_known_annotation)." entries in \%orga_id2hash_gene_id_known_annotation.\n";
}
if ($STATS_ALL_GENES_WITH_KNOWN_ANNOTATION =~ m/^ON$/i){
	stats_all_genes_with_known_annotation();
}


# return 1 for fusion, 2 for duplication, 3 for both, 0 for neither (ie small domain duplication < 0.% total lenght protein), die error otherwise
sub get_fusion_duplication_info_and_print_q_gene_file {
	my($main_gene_id, $q_organism_id, $ref_list_homol_gene_id, $s_organism_id) = @_;

	my $is_fusion = 0;
	my $is_duplication = 0;

	#TODO reuse file if present and grep
	my $requete_st = "SELECT q_first, q_last, q_length, s_gene_id, s_length FROM homologies WHERE q_gene_id = $main_gene_id AND s_gene_id IN (".join(",", @$ref_list_homol_gene_id).")";
	#print LOG "q orga $q_orga_id - s orga $s_orga_id : $requete_st\n";
	$st = $ORIGAMI::dbh->prepare($requete_st);
	$st->execute or die("Pb execute $!");
	my @q_first_stored = ();
	my @q_last_stored = ();
	my $q_length_stored = -1;
	my @s_gene_id_stored = ();
	my @s_length_stored = ();

	while ( my $res = $st->fetchrow_hashref() ) {
		my $q_first = $res->{q_first};
		my $q_last = $res->{q_last};
		my $q_length = $res->{q_length};
		my $s_gene_id = $res->{s_gene_id};
		my $s_length = $res->{s_length};
		push @q_first_stored, $q_first;
		push @q_last_stored, $q_last;
		$q_length_stored = $q_length;
		push @s_gene_id_stored, $s_gene_id;
		push @s_length_stored, $s_length;
	}

	for my $i (0 .. $#q_first_stored) {
	      	my $q_first_IT = $q_first_stored[$i];
		my $q_last_IT = $q_last_stored[$i];
		my $s_gene_id_IT = $s_gene_id_stored[$i];
		my $s_length_IT = $s_length_stored[$i];

		for my $j ( ($i+1) .. $#q_first_stored) {
	      		my $q_first_comp = $q_first_stored[$j];
			my $q_last_comp = $q_last_stored[$j];
			my $s_gene_id_comp = $s_gene_id_stored[$j];
			my $s_length_comp = $s_length_stored[$j];

			if ($q_last_IT <= $q_first_comp) {
				# no overlap, fusion
				$is_fusion = 1;
				# #q_gene_id\tq_organism_id\tpair_s_gene_id\ts_organism_id\tq_length\tpair_s_length\tis_fusion\tis_duplication\tlength_q_overlap_both_alignment\tlength_q_first_alignment_only\tlength_q_second_alignment_only
				print Q_GENE_ID_FUSION_DUPLICATION_FILE "$main_gene_id\t$q_organism_id\t$s_gene_id_IT,$s_gene_id_comp\t$s_organism_id\t$q_length_stored\t$s_length_IT,$s_length_comp\t$is_fusion\t$is_duplication\t0\t".($q_last_IT-$q_first_IT)."\t".($q_last_comp-$q_first_comp)."\n";
			} elsif ($q_first_IT >= $q_last_comp) {
				# no overlap, fusion
				$is_fusion = 1;
				# #q_gene_id\tq_organism_id\tpair_s_gene_id\ts_organism_id\tq_length\tpair_s_length\tis_fusion\tis_duplication\tlength_q_overlap_both_alignment\tlength_q_first_alignment_only\tlength_q_second_alignment_only
				print Q_GENE_ID_FUSION_DUPLICATION_FILE "$main_gene_id\t$q_organism_id\t$s_gene_id_IT,$s_gene_id_comp\t$s_organism_id\t$q_length_stored\t$s_length_IT,$s_length_comp\t$is_fusion\t$is_duplication\t0\t".($q_last_IT-$q_first_IT)."\t".($q_last_comp-$q_first_comp)."\n";
			} elsif ($q_first_IT < $q_first_comp
				&& $q_last_IT < $q_last_comp) {
				my $length_q_overlap_both_alignment = $q_last_IT - $q_first_comp;
				my $length_q_first_alignment_only = $q_first_comp - $q_first_IT;
				my $length_q_second_alignment_only = $q_last_comp - $q_last_IT;
				if( ( $length_q_overlap_both_alignment/$q_length_stored ) > 0.1 ){
					$is_duplication = 1;
				}
				if( ( $length_q_first_alignment_only/$q_length_stored ) > 0.1 && ( $length_q_second_alignment_only/$q_length_stored ) > 0.1 ){
					$is_fusion = 1;
				}
				# #q_gene_id\tq_organism_id\tpair_s_gene_id\ts_organism_id\tq_length\tpair_s_length\tis_fusion\tis_duplication\tlength_q_overlap_both_alignment\tlength_q_first_alignment_only\tlength_q_second_alignment_only
				print Q_GENE_ID_FUSION_DUPLICATION_FILE "$main_gene_id\t$q_organism_id\t$s_gene_id_IT,$s_gene_id_comp\t$s_organism_id\t$q_length_stored\t$s_length_IT,$s_length_comp\t$is_fusion\t$is_duplication\t$length_q_overlap_both_alignment\t$length_q_first_alignment_only\t$length_q_second_alignment_only\n";
			} elsif ($q_first_comp < $q_first_IT
				&& $q_last_comp < $q_last_IT) {
				my $length_q_overlap_both_alignment = $q_last_comp - $q_first_IT;
				my $length_q_first_alignment_only = $q_last_IT - $q_last_comp;
				my $length_q_second_alignment_only = $q_first_IT - $q_first_comp;
				if( ( $length_q_overlap_both_alignment/$q_length_stored ) > 0.1 ){
					$is_duplication = 1;
				}
				if( ( $length_q_first_alignment_only/$q_length_stored ) > 0.1 && ( $length_q_second_alignment_only/$q_length_stored ) > 0.1 ){
					$is_fusion = 1;
				}
				# #q_gene_id\tq_organism_id\tpair_s_gene_id\ts_organism_id\tq_length\tpair_s_length\tis_fusion\tis_duplication\tlength_q_overlap_both_alignment\tlength_q_first_alignment_only\tlength_q_second_alignment_only
				print Q_GENE_ID_FUSION_DUPLICATION_FILE "$main_gene_id\t$q_organism_id\t$s_gene_id_IT,$s_gene_id_comp\t$s_organism_id\t$q_length_stored\t$s_length_IT,$s_length_comp\t$is_fusion\t$is_duplication\t$length_q_overlap_both_alignment\t$length_q_first_alignment_only\t$length_q_second_alignment_only\n";
			} elsif ($q_first_IT >= $q_first_comp
				&& $q_last_IT <= $q_last_comp) {
				my $length_q_overlap_both_alignment = $q_last_IT - $q_first_IT;
				my $length_q_first_alignment_only = 0;
				my $length_q_second_alignment_only = ($q_first_IT-$q_first_comp) + ($q_last_comp - $q_last_IT);
				if( ( $length_q_overlap_both_alignment/$q_length_stored ) > 0.1 ){
					$is_duplication = 1;
				}
				# #q_gene_id\tq_organism_id\tpair_s_gene_id\ts_organism_id\tq_length\tpair_s_length\tis_fusion\tis_duplication\tlength_q_overlap_both_alignment\tlength_q_first_alignment_only\tlength_q_second_alignment_only
				print Q_GENE_ID_FUSION_DUPLICATION_FILE "$main_gene_id\t$q_organism_id\t$s_gene_id_IT,$s_gene_id_comp\t$s_organism_id\t$q_length_stored\t$s_length_IT,$s_length_comp\t$is_fusion\t$is_duplication\t$length_q_overlap_both_alignment\t$length_q_first_alignment_only\t$length_q_second_alignment_only\n";
			} elsif ($q_first_IT <= $q_first_comp
				&& $q_last_IT >= $q_last_comp) {
				my $length_q_overlap_both_alignment = $q_last_comp - $q_first_comp;
				my $length_q_first_alignment_only = ($q_first_comp - $q_first_IT) + ($q_last_IT - $q_last_comp);
				my $length_q_second_alignment_only = 0;
				if( ( $length_q_overlap_both_alignment/$q_length_stored ) > 0.1 ){
					$is_duplication = 1;
				}
				# #q_gene_id\tq_organism_id\tpair_s_gene_id\ts_organism_id\tq_length\tpair_s_length\tis_fusion\tis_duplication\tlength_q_overlap_both_alignment\tlength_q_first_alignment_only\tlength_q_second_alignment_only
				print Q_GENE_ID_FUSION_DUPLICATION_FILE "$main_gene_id\t$q_organism_id\t$s_gene_id_IT,$s_gene_id_comp\t$s_organism_id\t$q_length_stored\t$s_length_IT,$s_length_comp\t$is_fusion\t$is_duplication\t$length_q_overlap_both_alignment\t$length_q_first_alignment_only\t$length_q_second_alignment_only\n";
			} else {
				#error
				print LOG "ERROR get_fusion_duplication_info_and_print_q_gene_file : main_gene_id $main_gene_id and list_homol_gene_id [".join(",", @$ref_list_homol_gene_id)."] does not fall into a fusion / duplication category\n";
				die("ERROR get_fusion_duplication_info_and_print_q_gene_file : main_gene_id $main_gene_id and list_homol_gene_id [".join(",", @$ref_list_homol_gene_id)."] does not fall into a fusion / duplication category\n");
			}
		}
	}


	if ($is_duplication && $is_fusion) {
		return 3;
	} elsif ($is_duplication) {
		return 2;
	} elsif ($is_fusion) {
		return 1;
	} else {
		return 0;
		#print LOG "ERROR get_fusion_duplication_info_and_print_q_gene_file : main_gene_id $main_gene_id and list_homol_gene_id [".join(",", @$ref_list_homol_gene_id)."] does not return a fusion or duplication\n";
		#die("ERROR get_fusion_duplication_info_and_print_q_gene_file : main_gene_id $main_gene_id and list_homol_gene_id [".join(",", @$ref_list_homol_gene_id)."] does not return a fusion or duplication\n");
	}

}


sub get_pairs_with_alignment_id {
	my($alignment_id) = @_;
	$st = $ORIGAMI::dbh->prepare("SELECT pairs FROM alignments WHERE alignment_id = $alignment_id");
	$st->execute or die("Pb execute $!");
	my $pairs_to_return = 0;
	while ( my $res = $st->fetchrow_hashref() ) {
		$pairs_to_return = $res->{pairs};
	}
	if ($pairs_to_return > 0) {
		return $pairs_to_return;
	} else {
		print LOG "ERROR get_pairs_with_alignment_id : alignment_id = $alignment_id returned pairs_to_return = $pairs_to_return\n";
		die("ERROR get_pairs_with_alignment_id : alignment_id = $alignment_id returned pairs_to_return = $pairs_to_return\n");
	}
}


# create files stats_fusion_syntenies_pairwise_q_orga_$suffix_file
#	 and stats_fusion_syntenies_pairwise_q_gene_id_fusion_duplication_$suffix_file
#	 and stats_fusion_syntenies_pairwise_q_gene_id_synteny_$suffix_file
#TODO CREATE graphs stats_fusion_syntenies_pairwise_q_orga_$suffix_file :
=pod
** Distribution of the % of CDS with homologues for the 1,053,702 pairwise comparisons of 1027 non redundant genomes
	X : number of genomes ; Y : ratio homologue / total genes per genome [categories]
	Output columns to use : count_total_homologs/count_total_q_genes
	RQ : idem miror as we compute only half of the matrice : count_total_homologs/count_total_s_genes
	Contexte : Pour chaque génome dans le cadre d'une comparaison 2 à 2, on veut évaluer la proportion de CDS avec homologues (définis avec notre approche bdbh + genes in synteny)

** Distribution of the % of CDS with homolog of known functional annotation for the 1,053,702 pairwise comparisons of 1027 non redundant genomes
	X : number of genomes ; Y : ratio CDS with homolog of known functional annotation / total CDS
	Output columns to use : (count_both_q_and_s_homologs_known+count_only_s_homologs_known)/count_total_q_genes
	RQ : idem miror as we compute only half of the matrice : (count_both_q_and_s_homologs_known+count_only_q_homologs_known)/count_total_s_genes
	Contexte : Pour chaque génome dans le cadre d'une comparaison 2 à 2, on suppose que notre génome est non annoté et on le compare avec 1 autres génome non redondant déjà annoté. Quel proportion de CDS ont un homologues avec une fonction connue ?

** Distribution of the number of syntenies for for the 1,053,702 pairwise comparisons of 1027 non redundant genomes
	X : number of genomes ; Y : number of syntenies
	Output columns to use : count_number_syntenies
	RQ : for miror data, just do as if each line should be duplicated
	Contexte : Pour chaque génome dans le cadre d'une comparaison 2 à 2, on veut savoir le nombre de syntenies définis avec notre approche (programation dynamique ...)

** Distribution of the % of CDS with homolog in a syntenie for the 1,053,702 pairwise comparisons of 1027 non redundant genomes
	X : number of genomes ; Y : ratio number of homologs in syntenie
	Output columns to use : count_homologs_in_syntenie/count_total_q_genes
	RQ : idem miror as we compute only half of the matrice : count_homologs_in_syntenie/count_total_s_genes
	Contexte : Pour chaque génome dans le cadre d'une comparaison 2 à 2, on veut savoir le % de CDS avec homologues en syntenies

** Distribution of the % of CDS with multiple homologs in fusion / fission relationship for the 1,053,702 pairwise comparisons of 1027 non redundant genomes
	X : number of genomes ; Y : ratio number of homologs in fusion / fission relationship / count_total_q_genes
	Output columns to use : count_1q_Xs_fusion / count_total_q_genes
	Miror data : count_1s_Xq_fusion / count_total_s_genes
	Contexte : Pour chaque génome dans le cadre d'une comparaison 2 à 2, on veut savoir le % de CDS avec homologues en fusion / fission relationship

** Distribution of the % of CDS with multiple duplicates homologs in for the 1,053,702 pairwise comparisons of 1027 non redundant genomes
	X : number of genomes ; Y : ratio number of CDS with duplicates homologs / count_total_q_genes
	Output columns to use : count_1q_Xs_duplication / count_total_q_genes
	Miror data : count_1s_Xq_duplication / count_total_s_genes
	RQ : at the time we were doing 1 element vs 1 element for BDBH, so 1 BDBH per combination of element can be found whereas the estimated number of duplication may be overestimated slighly (..% of organism in our db are single elements)
	Contexte : Pour chaque génome dans le cadre d'une comparaison 2 à 2, on veut savoir le % de CDS avec homologues en fusion / fission relationship
=cut
if ($STATS_FUSION_SYNTENIES_PAIRWISE =~ m/^ON$/i){

	print LOG "** Starting stats fusion syntenies...\n";
	my $t0      = [gettimeofday()];

	#dependancies
	# list all organisms with genes : %gene_id2organism_id and %organism_id2gene_count and %organism_id2list_gene_ids
	build_from_table_genes();
	# list all genes with known annotation -> %orga_id2hash_gene_id_known_annotation
	stats_all_genes_with_known_annotation();
	# OLD BECAUSE TOO MEMORY CONSOMING list all alignment_id with syntenie -> %alignment_id2pairs_greater_1
	# build_from_table_alignments();


	# define output files
	my $file_q_orga = "$SiteConfig::DATADIR/Analyse_stats/stats_fusion_syntenies_pairwise_q_orga_$suffix_file";
	# RQ for both files below, if change here, change also in STATS_FUSION_SYNTENIES_ALL_GENOMES
	my $file_fusion_duplication_q_gene_id = "$SiteConfig::DATADIR/Analyse_stats/stats_fusion_syntenies_pairwise_q_gene_id_fusion_duplication_$suffix_file";
	my $file_synteny_q_gene_id = "$SiteConfig::DATADIR/Analyse_stats/stats_fusion_syntenies_pairwise_q_gene_id_synteny_$suffix_file";

	# skip if already output file
	if( -e $file_q_orga && -e $file_fusion_duplication_q_gene_id && -e $file_synteny_q_gene_id ){
		print LOG "ERROR STATS_FUSION_SYNTENIES_PAIRWISE : the output files file_q_orga ($file_q_orga), file_fusion_duplication_q_gene_id ($file_fusion_duplication_q_gene_id), file_synteny_q_gene_id ($file_synteny_q_gene_id) already exist, please remove them manually and launch the script again if you wish to recreate them.\n";
		die("ERROR STATS_FUSION_SYNTENIES_PAIRWISE : the output files file_q_orga ($file_q_orga), file_fusion_duplication_q_gene_id ($file_fusion_duplication_q_gene_id), file_synteny_q_gene_id ($file_synteny_q_gene_id) already exist, please remove them manually and launch the script again if you wish to recreate them.\n");
	} elsif ( -e $file_q_orga || -e $file_fusion_duplication_q_gene_id || -e $file_synteny_q_gene_id ){
		print LOG "ERROR STATS_FUSION_SYNTENIES_PAIRWISE : At least one of the output files file_q_orga ($file_q_orga), file_fusion_duplication_q_gene_id ($file_fusion_duplication_q_gene_id), file_synteny_q_gene_id ($file_synteny_q_gene_id) already exist, please remove them manually and launch the script again if you wish to recreate them.\n";
		die("ERROR STATS_FUSION_SYNTENIES_PAIRWISE : At least one of the output files file_q_orga ($file_q_orga), file_fusion_duplication_q_gene_id ($file_fusion_duplication_q_gene_id), file_synteny_q_gene_id ($file_synteny_q_gene_id) already exist, please remove them manually and launch the script again if you wish to recreate them.\n");
	}

	# open files and print headers
	#print in fie Q_ORGA_FUSION_SYNTENIES_FILE
	open(Q_ORGA_FUSION_SYNTENIES_FILE, '>:encoding(UTF-8)', $file_q_orga) or die ("Can not open $file_q_orga\n");
	#print header
	print Q_ORGA_FUSION_SYNTENIES_FILE "#q_organism_id\ts_organism_id\tcount_total_q_genes\tcount_total_s_genes\tcount_total_homologs\tcount_both_q_and_s_homologs_known\tcount_only_q_homologs_known\tcount_only_s_homologs_known\tcount_neither_q_and_s_homologs_known\tcount_number_syntenies\tcount_homologs_in_syntenie\tcount_homologs_singelton\tcount_1q_Xs_fusion\tcount_1q_Xs_duplication\tcount_1s_Xq_fusion\tcount_1s_Xq_duplication\n";
	#print in file Q_GENE_ID_FUSION_DUPLICATION_FILE
	open(Q_GENE_ID_FUSION_DUPLICATION_FILE, '>:encoding(UTF-8)', $file_fusion_duplication_q_gene_id) or die ("Can not open $file_fusion_duplication_q_gene_id\n");
	#print header
	print Q_GENE_ID_FUSION_DUPLICATION_FILE "#q_gene_id\tq_organism_id\tpair_s_gene_id\ts_organism_id\tq_length\tpair_s_length\tis_fusion\tis_duplication\tlength_q_overlap_both_alignment\tlength_q_first_alignment_only\tlength_q_second_alignment_only\n";
	#print in file Q_GENE_ID_SYNTENY_FILE
	open(Q_GENE_ID_SYNTENY_FILE, '>:encoding(UTF-8)', $file_synteny_q_gene_id) or die ("Can not open $file_synteny_q_gene_id\n");
	#print header
	print Q_GENE_ID_SYNTENY_FILE "#q_gene_id\tq_organism_id\ts_gene_id\ts_organism_id\t# WARNING NO REVERSE PRINT, CONSIDER EACH LINE TO BE REVERSIBLE s -> q\n";

	print LOG "Start printing file $file_q_orga, $file_fusion_duplication_q_gene_id and $file_synteny_q_gene_id\n";

	my @list_organism_id = sort { $a <=> $b } keys %organism_id2list_gene_ids;
	my $nbre_possibilite = ( ( scalar (@list_organism_id) * ( scalar (@list_organism_id) -1 ) ) / 2 );
	print LOG "Comparing each q_orga to each s_orga... ($nbre_possibilite combinations to do)\n";


	# for each q_organism
	my $count_combination_done = 0;
	for my $i (0 .. $#list_organism_id) {
	      	my $q_orga_id = $list_organism_id[$i];
		my $ref_list_q_gene_idIT = $organism_id2list_gene_ids{$q_orga_id};
		my $ref_hash_q_gene_id_knownIT = $orga_id2hash_gene_id_known_annotation{$q_orga_id};
		#print LOG "\tlist_gene_ids = ".join(", ", sort @$ref_list_q_gene_idIT)."\n";

		# for each s_organism
		for my $j ( ($i+1) .. $#list_organism_id) {

			my $s_orga_id = $list_organism_id[$j];
			$count_combination_done++;
			print LOG "\tStarting q organism id : $q_orga_id vs s organism id $s_orga_id ($count_combination_done / $nbre_possibilite)\n";
			if ($count_combination_done > 1) {
				my $elapsed = tv_interval( $t0 );
				my $MEM_used = `ps h -o vsz $$`;
				$MEM_used =~ s/\R//g;
				my $rounded_MEM = sprintf("%.1f", ($MEM_used/1000) );
				my $rounded_avg_seconds = sprintf("%.3f", ($elapsed/$count_combination_done) );
				print LOG "\t\t $rounded_avg_seconds seconds per combination in average ; Total used $rounded_MEM MB of RAM.\n\n";
			}

			my $ref_list_s_gene_idIT = $organism_id2list_gene_ids{$s_orga_id};
			my $ref_hash_s_gene_id_knownIT = $orga_id2hash_gene_id_known_annotation{$s_orga_id};


			my $requete_st = "SELECT alignment_pairs.alignment_id, alignment_pairs.q_gene_id, alignment_pairs.s_gene_id, alignments.pairs FROM alignment_pairs, alignments WHERE alignment_pairs.q_gene_id IN (".join(",", @$ref_list_q_gene_idIT).") AND alignment_pairs.s_gene_id IN (".join(",", @$ref_list_s_gene_idIT).") AND ( alignment_pairs.type = 1 OR alignment_pairs.type = 2 ) AND alignment_pairs.alignment_id = alignments.alignment_id";# ORDER BY alignment_pairs.alignment_id
			#print LOG "q orga $q_orga_id - s orga $s_orga_id : $requete_st\n";
			$st = $ORIGAMI::dbh->prepare($requete_st);
			$st->execute or die("Pb execute $!");

			# variables count
			my %count_total_homologs = ();
			my %q_gene_id2hash_s_gene_id = ();
			my %s_gene_id2hash_q_gene_id = ();
			my %count_both_q_and_s_homologs_known = ();
			my %count_only_q_homologs_known = ();
			my %count_only_s_homologs_known = ();
			my %count_neither_q_and_s_homologs_known = ();
			#my %alignment_id2pairs = ();
			my %count_number_syntenies = ();
			my %count_homologs_in_syntenie = ();
			my %count_homologs_singelton = ();
			my $count_1q_Xs_fusion = 0;
			my $count_1q_Xs_duplication = 0;
			my $count_1s_Xq_fusion = 0;
			my $count_1s_Xq_duplication = 0;
			#my %count_q_gene_with_at_least_synteny_or_fusion = ();
			#my $stored_prev_alignment_id = -1;

			while ( my $res = $st->fetchrow_hashref() ) {
				my $alignment_id = $res->{alignment_id};
				my $q_gene_id = $res->{q_gene_id};
				my $s_gene_id = $res->{s_gene_id};
				my $pairs_in_alignment = $res->{pairs};

				# count total homologs
				$count_total_homologs{"$q_gene_id-$s_gene_id"} = undef;

				# prep  %q_gene_id2hash_s_gene_id and %s_gene_id2list_q_gene_id
				if (exists $q_gene_id2hash_s_gene_id{$q_gene_id}) {
					my $ref_hash_gene_idIT = $q_gene_id2hash_s_gene_id{$q_gene_id};
					$$ref_hash_gene_idIT{$s_gene_id} = undef;
				} else {
					my %hash_gene_idIT = ();
					$hash_gene_idIT{$s_gene_id} = undef;
					$q_gene_id2hash_s_gene_id{$q_gene_id} = \%hash_gene_idIT;
				}
				if (exists $s_gene_id2hash_q_gene_id{$s_gene_id}) {
					my $ref_hash_gene_idIT = $s_gene_id2hash_q_gene_id{$s_gene_id};
					$$ref_hash_gene_idIT{$q_gene_id} = undef;
				} else {
					my %hash_gene_idIT = ();
					$hash_gene_idIT{$q_gene_id} = undef;
					$s_gene_id2hash_q_gene_id{$s_gene_id} = \%hash_gene_idIT;
				}


				# check if q_gene and s_gene have known annotation
				if(exists $$ref_hash_q_gene_id_knownIT{$q_gene_id} && exists $$ref_hash_s_gene_id_knownIT{$s_gene_id}){
					$count_both_q_and_s_homologs_known{"$q_gene_id-$s_gene_id"} = undef;
					#print LOG "count_both_q_and_s_homologs_known : $q_gene_id-$s_gene_id\n";
				} elsif (exists $$ref_hash_q_gene_id_knownIT{$q_gene_id}) {
					$count_only_q_homologs_known{"$q_gene_id-$s_gene_id"} = undef;
					#print LOG "count_only_q_homologs_known : $q_gene_id-$s_gene_id\n";
				} elsif (exists $$ref_hash_s_gene_id_knownIT{$s_gene_id}) {
					$count_only_s_homologs_known{"$q_gene_id-$s_gene_id"} = undef;
					#print LOG "count_only_s_homologs_known : $q_gene_id-$s_gene_id\n";
				} else {
					$count_neither_q_and_s_homologs_known{"$q_gene_id-$s_gene_id"} = undef;
					#print LOG "count_neither_q_and_s_homologs_known : $q_gene_id-$s_gene_id\n";
				}


				# check if alignment_id is in syntenie > 2
				# do a join sql request because else too memory consuming
=pod
				my $pairs_in_alignment = 0;
				if (exists $alignment_id2pairs{$alignment_id}) {
					$pairs_in_alignment = $alignment_id2pairs{$alignment_id};
				} else {
					$pairs_in_alignment = get_pairs_with_alignment_id($alignment_id);
					$alignment_id2pairs{$alignment_id} = $pairs_in_alignment;
				}
				$pairs_in_alignment = get_pairs_with_alignment_id($alignment_id)
=cut
=pod
				if ($alignment_id != $stored_prev_alignment_id) {
					$stored_prev_alignment_id = $alignment_id;
				}
=cut
				if ($pairs_in_alignment > 1) {

					$count_number_syntenies{$alignment_id} = undef;
					if(exists $count_homologs_in_syntenie{"$q_gene_id-$s_gene_id"}){
						#do nothing
					} else {
						$count_homologs_in_syntenie{"$q_gene_id-$s_gene_id"} = undef;
						#q_gene_id\tq_organism_id\ts_gene_id\ts_organism_id\t# WARNING NO REVERSE PRINT, CONSIDER EACH LINE TO BE REVERSIBLE s -> q
						# print q and s NO REVERSE
						print Q_GENE_ID_SYNTENY_FILE "$q_gene_id\t$q_orga_id\t$s_gene_id\t$s_orga_id\n";
					}

					#$count_q_gene_with_at_least_synteny_or_fusion{$q_gene_id} = undef;
				} else {
					$count_homologs_singelton{"$q_gene_id-$s_gene_id"} = undef;
				}
			}

			# check for duplication or fusion in %q_gene_id2hash_s_gene_id and %s_gene_id2list_q_gene_id
			foreach my $key_q_gene_id (keys %q_gene_id2hash_s_gene_id) {
				my $ref_hash_s_gene_id = $q_gene_id2hash_s_gene_id{$key_q_gene_id};
				my $count_s_gene_id = scalar (keys %$ref_hash_s_gene_id);
				if ($count_s_gene_id > 1) {
					# duplication or fusion ?
					my @list_s_gene_id = keys %$ref_hash_s_gene_id;
					my $check_fus_or_dup = get_fusion_duplication_info_and_print_q_gene_file($key_q_gene_id, $q_orga_id, \@list_s_gene_id, $s_orga_id);
					if ($check_fus_or_dup == 1) {
						#fusion
						$count_1q_Xs_fusion++;
						#$count_q_gene_with_at_least_synteny_or_fusion{$key_q_gene_id} = undef;
					} elsif ($check_fus_or_dup == 2) {
						#dupli
						$count_1q_Xs_duplication++;
					} elsif ($check_fus_or_dup == 3) {
						#fusion
						$count_1q_Xs_fusion++;
						#$count_q_gene_with_at_least_synteny_or_fusion{$key_q_gene_id} = undef;
						#dupli
						$count_1q_Xs_duplication++;
					}
				}
			}
			foreach my $key_s_gene_id (keys %s_gene_id2hash_q_gene_id) {
				my $ref_hash_q_gene_id = $s_gene_id2hash_q_gene_id{$key_s_gene_id};
				my $count_q_gene_id = scalar (keys %$ref_hash_q_gene_id);
				if ($count_q_gene_id > 1) {
					# duplication or fusion ?
					my @list_q_gene_id = keys %$ref_hash_q_gene_id;
					my $check_fus_or_dup = get_fusion_duplication_info_and_print_q_gene_file($key_s_gene_id, $s_orga_id, \@list_q_gene_id, $q_orga_id);
					if ($check_fus_or_dup == 1) {
						#fusion
						$count_1s_Xq_fusion++;
						#foreach my $q_gene_idIT (keys %$ref_hash_q_gene_id) {
						#	$count_q_gene_with_at_least_synteny_or_fusion{$q_gene_idIT} = undef;
						#}
					} elsif ($check_fus_or_dup == 2) {
						#dupli
						$count_1s_Xq_duplication++;
					} elsif ($check_fus_or_dup == 3) {
						#fusion
						$count_1s_Xq_fusion++;
						#foreach my $q_gene_idIT (keys %$ref_hash_q_gene_id) {
						#	$count_q_gene_with_at_least_synteny_or_fusion{$q_gene_idIT} = undef;
						#}
						#dupli
						$count_1s_Xq_duplication++;
					}
				}
			}

			#print data to Q_ORGA_FUSION_SYNTENIES_FILE
			print Q_ORGA_FUSION_SYNTENIES_FILE "$q_orga_id\t$s_orga_id\t".$organism_id2gene_count{$q_orga_id}."\t".$organism_id2gene_count{$s_orga_id}."\t".scalar (keys %count_total_homologs)."\t".scalar (keys %count_both_q_and_s_homologs_known)."\t".scalar (keys %count_only_q_homologs_known)."\t".scalar (keys %count_only_s_homologs_known)."\t".scalar (keys %count_neither_q_and_s_homologs_known)."\t".scalar ( keys %count_number_syntenies )."\t".scalar ( keys %count_homologs_in_syntenie )."\t".scalar ( keys %count_homologs_singelton )."\t$count_1q_Xs_fusion\t$count_1q_Xs_duplication\t$count_1s_Xq_fusion\t$count_1s_Xq_duplication\n";

		}# for each s_organism
	}# for each q_organism

	close(Q_ORGA_FUSION_SYNTENIES_FILE);
	close(Q_GENE_ID_FUSION_DUPLICATION_FILE);
	close(Q_GENE_ID_SYNTENY_FILE);
	print LOG "Done printing files $file_q_orga and $file_fusion_duplication_q_gene_id\n";

	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done stats fusion syntenies homologs in $elapsed seconds ; used $rounded MB of RAM.\n";

}





# create files stats_homologs_q_orga_vs_all_orga_$suffix_file
=pod
** Distribution of the % of CDS with homologues for the 1027 comparisons of a genomes against 1026 other non redundant genomes
	Contexte : Pour chaque génome dans le cadre d'une comparaison contre 1026 autres génomes non redondant, on veut évaluer la proportion de CDS avec au moins 1 homologues (définis avec notre technique notre technique bdbh + genes in synteny)

** Distribution of the % of CDS with at least 1 homolog of known functional annotation for the 1027 comparisons of a genomes against 1026 other non redundant genomes
	Contexte : Pour chaque génome dans le cadre d'une comparaison contre 1026 autres génomes non redondant, on suppose que notre génome est non annoté et on le compare avec 1026 autres génome non redondants déjà annotés. Quelle proportion de CDS ont au moins un homologue avec une fonction connue ?

** Evolution by year of the average % of CDS with at least 1 homolog of known functional annotation for the comparisons of a genome against genomes annotated at a earlier date among a pool of 1026 other non redundant genomes. 2nd axis : number of compared genomes
	Contexte : idem up mais on se place adns le cas de figure des génomes comparés annotés à une date ultérieure constituent des informations non disponibles. Est-ce que la puissance de cette analyse (% of CDS with at least 1 homolog of known functional annotation) augmente en fonction de l'année ou du nombre de génomes comparés ?

=cut
if ($STATS_HOMOLOGS_ALL_GENOMES =~ m/^ON$/i){

	print LOG "** Starting stats homologs query organism against all genomes...\n";
	my $t0      = [gettimeofday()];

	#dependancies
	# list all organisms with genes : %gene_id2organism_id and %organism_id2gene_count and %organism_id2list_gene_ids
	build_from_table_genes();
	# list all genes with known annotation -> %orga_id2hash_gene_id_known_annotation
	stats_all_genes_with_known_annotation();
	# OLD BECAUSE TOO MEMORY CONSOMING list all alignment_id with syntenie -> %alignment_id2pairs_greater_1
	# build_from_table_alignments();

	# define output files
	my $file = "$SiteConfig::DATADIR/Analyse_stats/stats_homologs_q_orga_vs_all_orga_$suffix_file";

	# skip if already output file
	if( -e $file ){
		print LOG "ERROR STATS_HOMOLOGS_ALL_GENOMES : the output files ($file) already exists, please remove it manually and launch the script again if you wish to recreate it.\n";
		die("ERROR STATS_HOMOLOGS_ALL_GENOMES : the output files ($file) already exists, please remove it manually and launch the script again if you wish to recreate it.\n");
	}

	# open files and print headers
	#print in fie Q_ORGA_FUSION_SYNTENIES_FILE
	open(HOMOLOGS_Q_ORGA_VS_ALL_ORGA_FILE, '>:encoding(UTF-8)', $file) or die ("Can not open $file\n");
	#print header
	print HOMOLOGS_Q_ORGA_VS_ALL_ORGA_FILE "#q_organism_id\tq_organism_earliest_date_annotation\tcount_total_CDS\tcount_number_CDS_with_homologs_in_at_least_1_compared_organism\tcount_number_CDS_with_homologs_known_annotation_in_at_least_1_compared_organism\tcount_number_CDS_with_homologs_in_at_least_1_compared_organism_annotated_at_a_earlier_date\tcount_number_CDS_with_homologs_known_annotation_in_at_least_1_compared_organism_annotated_at_a_earlier_date\tnumber_of_compared_organism_annotated_at_a_earlier_date\n";


	print LOG "Start printing file $file\n";

	my @list_organism_id = sort { $a <=> $b } keys %organism_id2list_gene_ids;
	my $nbre_possibilite = scalar (@list_organism_id);
	print LOG "Comparing each q_orga to all s_orga... ($nbre_possibilite combinations to do)\n";


	# for each q_organism
	my $count_combination_done = 0;
	for my $i (0 .. $#list_organism_id) {
	      	my $q_orga_id = $list_organism_id[$i];
		my $ref_list_q_gene_idIT = $organism_id2list_gene_ids{$q_orga_id};
		my %hash_list_q_gene_idIT = ();
		@hash_list_q_gene_idIT{@$ref_list_q_gene_idIT} = ();#undef

		$count_combination_done++;
		print LOG "\tStarting q organism id : $q_orga_id vs all other organisms ($count_combination_done / $nbre_possibilite)\n";
		if ($count_combination_done > 1) {
			my $elapsed = tv_interval( $t0 );
			my $MEM_used = `ps h -o vsz $$`;
			$MEM_used =~ s/\R//g;
			my $rounded_MEM = sprintf("%.1f", ($MEM_used/1000) );
			my $rounded_avg_seconds = sprintf("%.3f", ($elapsed/$count_combination_done) );
			print LOG "\t\t $rounded_avg_seconds seconds per combination in average ; Total used $rounded_MEM MB of RAM.\n\n";
		}

		#date q organism
		my $date_q_organism = "";
		my $epoch_q_organism = -1;
		if(exists $organism_id2earliest_date_seq{$q_orga_id}){
			my $q_date_as_string = $organism_id2earliest_date_seq{$q_orga_id};
			#RQ : remove 1 day to actual date, no consequences
			$epoch_q_organism = str2time($q_date_as_string);
			my $datetime = DateTime->from_epoch(epoch => $epoch_q_organism);
			$date_q_organism = $datetime->ymd;   # Retrieves date as a string in 'yyyy-mm-dd' format
		} else {
			print LOG "ERROR STATS_HOMOLOGS_ALL_GENOMES : q orga id $q_orga_id is not found in \%organism_id2earliest_date_seq\n";
			die("ERROR STATS_HOMOLOGS_ALL_GENOMES : q orga id $q_orga_id is not found in \%organism_id2earliest_date_seq\n");
		}

		my $requete_st = "SELECT alignment_pairs.q_gene_id, alignment_pairs.s_gene_id, genes.organism_id AS s_organism_id FROM alignment_pairs, genes WHERE alignment_pairs.q_gene_id IN (".join(",", @$ref_list_q_gene_idIT).") AND ( alignment_pairs.type = 1 OR alignment_pairs.type = 2 ) AND alignment_pairs.s_gene_id = genes.gene_id"; #  AND alignment_pairs.s_gene_id NOT IN (".join(",", @$ref_list_q_gene_idIT).") #  AND genes.organism_id != $q_orga_id
		#print LOG "q orga $q_orga_id - s orga $s_orga_id : $requete_st\n";
		$st = $ORIGAMI::dbh->prepare($requete_st);
		$st->execute or die("Pb execute $!");
		# variables count
		my %count_q_gene_id_with_homologs = ();
		my %count_q_gene_id_with_homologs_known_annotation = ();
		my %count_q_gene_id_with_homologs_earlier_date = ();
		my %count_q_gene_id_with_homologs_known_annotation_earlier_date = ();
		my %count_s_orga_id_earlier_date = ();

		while ( my $res = $st->fetchrow_hashref() ) {
			my $q_gene_id = $res->{q_gene_id};
			my $s_gene_id = $res->{s_gene_id};
			my $s_organism_id = $res->{s_organism_id};
			my $ref_hash_s_gene_id_knownIT = $orga_id2hash_gene_id_known_annotation{$s_organism_id};

			# we don't want paralogs
			if ( exists $hash_list_q_gene_idIT{$s_gene_id} ) {
				next;
			}

			# we don't want s_orga that are not part of %organism_id2list_gene_ids
 			if ( ! exists $organism_id2list_gene_ids{$s_organism_id} ) {
				next;
			}

			$count_q_gene_id_with_homologs{$q_gene_id} = undef;

			# check if q_gene and s_gene have known annotation
			if (exists $$ref_hash_s_gene_id_knownIT{$s_gene_id}) {
				$count_q_gene_id_with_homologs_known_annotation{$q_gene_id} = undef;
				#print LOG "count_only_s_homologs_known : $q_gene_id-$s_gene_id\n";
			}

			#date s organism
			my $date_s_organism = "";
			my $epoch_s_organism = -1;
			if(exists $organism_id2earliest_date_seq{$s_organism_id}){
				my $s_date_as_string = $organism_id2earliest_date_seq{$s_organism_id};
				#RQ : remove 1 day to actual date, no consequences
				$epoch_s_organism = str2time($s_date_as_string);
			} else {
				print LOG "ERROR STATS_HOMOLOGS_ALL_GENOMES : s orga id $s_organism_id is not found in \%organism_id2earliest_date_seq\n";
				die("ERROR STATS_HOMOLOGS_ALL_GENOMES : s orga id $s_organism_id is not found in \%organism_id2earliest_date_seq\n");
			}

			# we don't want s orga annotataed at a later date than q organism
			if ( $epoch_s_organism > $epoch_q_organism ) {
				# do nothing;
			} else {
				#print "s orga EARLIER than q orga : $epoch_s_organism < $epoch_q_organism ; ".$organism_id2earliest_date_seq{$s_organism_id}." < ".$organism_id2earliest_date_seq{$q_orga_id}."\n";
				$count_s_orga_id_earlier_date{$s_organism_id} = undef;

				#count
				$count_q_gene_id_with_homologs_earlier_date{$q_gene_id} = undef;

				# check if q_gene and s_gene have known annotation
				if (exists $$ref_hash_s_gene_id_knownIT{$s_gene_id}) {
					$count_q_gene_id_with_homologs_known_annotation_earlier_date{$q_gene_id} = undef;
					#print LOG "count_only_s_homologs_known : $q_gene_id-$s_gene_id\n";
				}
			}




		}# while ( my $res = $st->fetchrow_hashref() ) {

		#print data to Q_ORGA_FUSION_SYNTENIES_FILE
		#q_organism_id\tq_organism_earliest_date_annotation\tcount_total_CDS\tcount_number_CDS_with_homologs_in_at_least_1_compared_organism\tcount_number_CDS_with_homologs_known_annotation_in_at_least_1_compared_organism\tcount_number_CDS_with_homologs_in_at_least_1_compared_organism_annotated_at_a_earlier_date\tcount_number_CDS_with_homologs_known_annotation_in_at_least_1_compared_organism_annotated_at_a_earlier_date\tnumber_of_compared_organism_annotated_at_a_earlier_date\n
		print HOMOLOGS_Q_ORGA_VS_ALL_ORGA_FILE "$q_orga_id\t$date_q_organism\t".$organism_id2gene_count{$q_orga_id}."\t".scalar (keys %count_q_gene_id_with_homologs)."\t".scalar (keys %count_q_gene_id_with_homologs_known_annotation)."\t".scalar (keys %count_q_gene_id_with_homologs_earlier_date)."\t".scalar (keys %count_q_gene_id_with_homologs_known_annotation_earlier_date)."\t".scalar (keys %count_s_orga_id_earlier_date)."\n";



	}# for my $i (0 .. $#list_organism_id) {

	close(HOMOLOGS_Q_ORGA_VS_ALL_ORGA_FILE);

	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done stats homologs query organism against all genomes in $elapsed seconds ; used $rounded MB of RAM.\n";

} #STATS_HOMOLOGS_ALL_GENOMES




#TODO STATS_ON_PROFILE_PHYLO
if ($STATS_ON_PROFILE_PHYLO =~ m/^ON$/i){

	
	print LOG "** Starting stats on profile phylo...\n";
	my $t0      = [gettimeofday()];

	#dependancies
	# list all organisms with genes : %gene_id2organism_id and %organism_id2gene_count and %organism_id2list_gene_ids
	build_from_table_genes();
	# list all genes with known annotation -> %orga_id2hash_gene_id_known_annotation
	stats_all_genes_with_known_annotation();
	# OLD BECAUSE TOO MEMORY CONSOMING list all alignment_id with syntenie -> %alignment_id2pairs_greater_1
	# build_from_table_alignments();

	# define output files
	my $output_file = "$SiteConfig::DATADIR/Analyse_stats/stats_profile_phylo_$suffix_file";

	# skip if already output file
	if( -e $output_file ){
		print LOG "ERROR STATS_ON_PROFILE_PHYLO : the output files ($output_file) already exists, please remove it manually and launch the script again if you wish to recreate it.\n";
		die("ERROR STATS_ON_PROFILE_PHYLO : the output files ($output_file) already exists, please remove it manually and launch the script again if you wish to recreate it.\n");
	}

	# open files and print headers
	#print in fie Q_ORGA_FUSION_SYNTENIES_FILE
	open(STATS_ON_PROFILE_PHYLO_OUTPUT_FILE, '>:encoding(UTF-8)', $output_file) or die ("Can not open $output_file\n");
	#print header
#TODO
	print STATS_ON_PROFILE_PHYLO_OUTPUT_FILE "#q_organism_id\tTODO\n";


	print LOG "Start printing file $output_file\n";


#TODO
=pod ADAPT


		#print in file_profil_phylo_for_q_orga
		if ( $writte_profil_phylo_orga_files == 1 ){
			my $file_profil_phylo_for_q_orga = "$SiteConfig::DATADIR/Analyse_stats/profil_phylo_per_organism/profil_phylo_orga_${q_orga_id}_$suffix_file";
			open(PROFILE_PHYLO_Q_ORGA_WRITE_FILE, '>>:encoding(UTF-8)', $file_profil_phylo_for_q_orga) or die ("Can not open $file_profil_phylo_for_q_orga\n");
			#print header
			# no header because file can be opened by s_orga too
			# header is "q_gene_id\ts_gene_id"
		}



			#print in file_profil_phylo_for_s_orga
			if ( $writte_profil_phylo_orga_files == 1 ){
				my $file_profil_phylo_for_s_orga = "$SiteConfig::DATADIR/Analyse_stats/profil_phylo_per_organism/profil_phylo_orga_${s_orga_id}_$suffix_file";
				open(PROFILE_PHYLO_S_ORGA_WRITE_FILE, '>>:encoding(UTF-8)', $file_profil_phylo_for_s_orga) or die ("Can not open $file_profil_phylo_for_s_orga\n");
				#print header
				# no header because file can be opened by s_orga too
				# header is "q_gene_id\ts_gene_id"
			}


			while ( my $res = $st->fetchrow_hashref() ) {
				my $q_gene_id = $res->{q_gene_id};
				my $s_gene_id = $res->{s_gene_id};


				#TODO print file for profile phylo, 1 file per orga_id
				if ( $writte_profil_phylo_orga_files == 1 ){
					print PROFILE_PHYLO_Q_ORGA_WRITE_FILE "$q_gene_id\t$s_gene_id\n";
					print PROFILE_PHYLO_S_ORGA_WRITE_FILE "$s_gene_id\t$q_gene_id\n";
				}


			if ( $writte_profil_phylo_orga_files == 1 ){
				close(PROFILE_PHYLO_S_ORGA_WRITE_FILE);
			}


		if ( $writte_profil_phylo_orga_files == 1 ){
			close(PROFILE_PHYLO_Q_ORGA_WRITE_FILE);
		}

=cut

	close(STATS_ON_PROFILE_PHYLO_OUTPUT_FILE);

	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done stats on profile phylo in $elapsed seconds ; used $rounded MB of RAM.\n";


} #STATS_ON_PROFILE_PHYLO



=pod
#TODO CREATE graph from stats_fusion_syntenies_pairwise_q_gene_id_fusion_duplication_$suffix_file :
** for all CDS with at least 1 homolog in a fusion / fission relationship , what is the distribution regarding the lenght of the fusion protein and the multiple fission proteins
	X : number of homolog in a fusion / fission relationship ; Y : lenght of the query
	Output columns to use : fusion protein : q_length
	Output columns to use : fission proteins : pair_s_length
	Miror data : will appear as separate lines in the output
	Contexte : 



** for all CDS with at least 1 homolog in a fusion / fission relationship , what is the distribution regarding the % coverage for the non overlapping and overlapping portions of alignments
	X : number of homolog in a fusion / fission relationship ; Y : % coverage of alignment on the query
	Output columns to use : non overlapping : (length_q_first_alignment_only+length_q_second_alignment_only)/q_length
	Output columns to use : overlapping : length_q_overlap_both_alignment/q_length
	Miror data : will appear as separate lines in the output
	Contexte : 



** for all CDS with at least 1 homolog in a duplication relationship , what is the distribution regarding the lenght of the fusion protein and the multiple fission proteins
	X : number of homolog in a duplication relationship ; Y : lenght of the query
	Output columns to use : fusion protein : q_length
	Output columns to use : fission proteins : pair_s_length
	Miror data : will appear as separate lines in the output
	Contexte : 


** for all CDS with at least 1 homolog in a duplication relationship , what is the distribution regarding the % coverage for the non overlapping and overlapping portions of alignments
	X : number of homolog in a duplication relationship ; Y : % coverage of alignment on the query
	Output columns to use : non overlapping : (length_q_first_alignment_only+length_q_second_alignment_only)/q_length
	Output columns to use : overlapping : length_q_overlap_both_alignment/q_length
	Miror data : will appear as separate lines in the output
	Contexte : 
=cut



=pod

#TODO script started but add file from profile phylo to analyse at same time ?

# use data from -STATS_FUSION_SYNTENIES_PAIRWISE but consolidate data for comparaison against all genomes instead of pairwise ; typical text : comparisons of a genomes against 1026 other non redundant genomes
#TODO graph stats_q_gene_id_fusion_duplication_$suffix_file :
** Distribution of the % of CDS with at least 1 homolog in a fusion / fission relationship for the 1027 comparisons of a genomes against 1026 other non redundant genomes
	Contexte : Pour chaque génome dans le cadre d'une comparaison contre 1026 autres génomes non redondant. Quelle proportion de CDS ont au moins un homologue en relation de fusion / fission ?
** Distribution of the % of CDS with duplicates homologs for the 1027 comparisons of a genomes against 1026 other non redundant genomes
	Contexte : Pour chaque génome dans le cadre d'une comparaison contre 1026 autres génomes non redondant. Quelle proportion de CDS ont au moins un homologue dupliquée chez un autre organisme ?
#TODO graph stats_q_gene_id_synteny_$suffix_file :
** Distribution of the % of CDS with at least 1 homolog in a syntenie for the 1027 comparisons of a genomes against 1026 other non redundant genomes
	Contexte : Pour chaque génome dans le cadre d'une comparaison contre 1026 autres génomes non redondant. Quelle proportion de CDS ont au moins un homologue en relation de syntenie ?


if ($STATS_FUSION_SYNTENIES_ALL_GENOMES =~ m/^ON$/i){

	print LOG "** Starting stats fusion syntenies against all genomes...\n";
	my $t0      = [gettimeofday()];

	#dependancies
	# list all organisms with genes : %gene_id2organism_id and %organism_id2gene_count and %organism_id2list_gene_ids
	build_from_table_genes();
	# list all genes with known annotation -> %orga_id2hash_gene_id_known_annotation
	stats_all_genes_with_known_annotation();
	# OLD BECAUSE TOO MEMORY CONSOMING list all alignment_id with syntenie -> %alignment_id2pairs_greater_1
	# build_from_table_alignments();
	# RQ for both files below, if change here, change also in STATS_FUSION_SYNTENIES_PAIRWISE
	# mandatory input files
	#TODO add file from profile phylo ?
	my $file_fusion_duplication_q_gene_id = "$SiteConfig::DATADIR/Analyse_stats/stats_fusion_syntenies_pairwise_q_gene_id_fusion_duplication_$suffix_file";
	my $file_synteny_q_gene_id = "$SiteConfig::DATADIR/Analyse_stats/stats_fusion_syntenies_pairwise_q_gene_id_synteny_$suffix_file";
	# skip if already output file
	if( ! -e $file_fusion_duplication_q_gene_id || !  -e $file_synteny_q_gene_id ){
		print LOG "ERROR STATS_FUSION_SYNTENIES_ALL_GENOMES : one or both input files ($file_fusion_duplication_q_gene_id and $file_synteny_q_gene_id) are missing, please make sure you ran the script with the option -STATS_FUSION_SYNTENIES_PAIRWISE before running with the option -STATS_FUSION_SYNTENIES_ALL_GENOMES.\n";
		die("ERROR STATS_FUSION_SYNTENIES_ALL_GENOMES : one or both input files ($file_fusion_duplication_q_gene_id and $file_synteny_q_gene_id) are missing, please make sure you ran the script with the option -STATS_FUSION_SYNTENIES_PAIRWISE before running with the option -STATS_FUSION_SYNTENIES_ALL_GENOMES.\n");
	}


	# define output files
	my $output_file_STATS_FUSION_SYNTENIES_ALL_GENOMES = "$SiteConfig::DATADIR/Analyse_stats/stats_fusion_syntenies_all_genomes_$suffix_file";


	# skip if already output file
	if( -e $output_file_STATS_FUSION_SYNTENIES_ALL_GENOMES ){
		print LOG "ERROR STATS_FUSION_SYNTENIES_ALL_GENOMES : the output files ($output_file_STATS_FUSION_SYNTENIES_ALL_GENOMES) already exists, please remove it manually and launch the script again if you wish to recreate it.\n";
		die("ERROR STATS_FUSION_SYNTENIES_ALL_GENOMES : the output files ($output_file_STATS_FUSION_SYNTENIES_ALL_GENOMES) already exists, please remove it manually and launch the script again if you wish to recreate it.\n");
	}


	# open ouput file and print header
	open(STATS_FUSION_SYNTENIES_ALL_GENOMES_Q_ORGA_OUTPUT_FILE, '>:encoding(UTF-8)', $file_fusion_duplication_q_organism_id) or die ("Can not open $file_fusion_duplication_q_organism_id\n");
	#print header
	print STATS_FUSION_SYNTENIES_ALL_GENOMES_Q_ORGA_OUTPUT_FILE "#q_organism_id\tq_organism_earliest_date_annotation\tcount_total_CDS\tcount_CDS_with_at_least_1_homolog_in_a_fusion_fission_relationship\tcount_CDS_with_at_least_1_event_of_duplicates_homologs\tcount_CDS_with_at_least_1_homolog_in_syntenie\tcount_CDS_with_at_least_1_homolog_in_a_fusion_fission_relationship_comparing_to_organisms_annotated_at_a_earlier_date_only\tcount_CDS_with_at_least_1_event_of_duplicates_homologs_comparing_to_organisms_annotated_at_a_earlier_date_only\tcount_CDS_with_at_least_1_homolog_in_syntenie_comparing_to_organisms_annotated_at_a_earlier_date_only\tnumber_of_compared_organism_annotated_at_a_earlier_date\n"
 #TODO add count_CDS_with_profile_phylo
 #TODO add count_CDS_with_either_fusion_or_syntenie_or_profile_phylo
 #TODO add count_CDS_with_profile_phylo_comparing_to_organisms_annotated_at_a_earlier_date_only
 #TODO add count_CDS_with_either_fusion_or_syntenie_or_profile_phylo_comparing_to_organisms_annotated_at_a_earlier_date_only


#TODO needed to open file if just grep ?
	#open file Q_GENE_ID_FUSION_DUPLICATION_FILE
	# open(STATS_FUSION_SYNTENIES_ALL_GENOMES_Q_GENE_ID_FUSION_DUPLICATION_INPUT_FILE, '<:encoding(UTF-8)', $file_fusion_duplication_q_gene_id) or die ("Can not open $file_fusion_duplication_q_gene_id\n");
	#header #q_gene_id\tq_organism_id\tpair_s_gene_id\ts_organism_id\tq_length\tpair_s_length\tis_fusion\tis_duplication\tlength_q_overlap_both_alignment\tlength_q_first_alignment_only\tlength_q_second_alignment_only\n";
	#open file Q_GENE_ID_SYNTENY_FILE
	# open(STATS_FUSION_SYNTENIES_ALL_GENOMES_Q_GENE_ID_SYNTENY_INPUT_FILE, '<:encoding(UTF-8)', $file_synteny_q_gene_id) or die ("Can not open $file_synteny_q_gene_id\n");
	# header #q_gene_id\tq_organism_id\ts_gene_id\ts_organism_id\t# WARNING NO REVERSE PRINT, CONSIDER EACH LINE TO BE REVERSIBLE s -> q\n";
	#TODO add file from profile phylo ?


	print LOG "Start printing output file $output_file_STATS_FUSION_SYNTENIES_ALL_GENOMES\n";


	my @list_organism_id = sort { $a <=> $b } keys %organism_id2list_gene_ids;
	my $nbre_possibilite = scalar (@list_organism_id);
	print LOG "Comparing each q_orga to all s_orga... ($nbre_possibilite combinations to do)\n";


	# for each q_organism
	my $count_combination_done = 0;
	for my $i (0 .. $#list_organism_id) {
	      	my $q_orga_id = $list_organism_id[$i];
		my $ref_list_q_gene_idIT = $organism_id2list_gene_ids{$q_orga_id};
		my %hash_list_q_gene_idIT = ();
		@hash_list_q_gene_idIT{@$ref_list_q_gene_idIT} = ();#undef

		$count_combination_done++;
		print LOG "\tStarting q organism id : $q_orga_id vs all other organisms ($count_combination_done / $nbre_possibilite)\n";
		if ($count_combination_done > 1) {
			my $elapsed = tv_interval( $t0 );
			my $MEM_used = `ps h -o vsz $$`;
			$MEM_used =~ s/\R//g;
			my $rounded_MEM = sprintf("%.1f", ($MEM_used/1000) );
			my $rounded_avg_seconds = sprintf("%.3f", ($elapsed/$count_combination_done) );
			print LOG "\t\t $rounded_avg_seconds seconds per combination in average ; Total used $rounded_MEM MB of RAM.\n\n";
		}

		#date q organism
		my $date_q_organism = "";
		my $epoch_q_organism = -1;
		if(exists $organism_id2earliest_date_seq{$q_orga_id}){
			my $q_date_as_string = $organism_id2earliest_date_seq{$q_orga_id};
			#RQ : remove 1 day to actual date, no consequences
			$epoch_q_organism = str2time($q_date_as_string);
			my $datetime = DateTime->from_epoch(epoch => $epoch_q_organism);
			$date_q_organism = $datetime->ymd;   # Retrieves date as a string in 'yyyy-mm-dd' format
		} else {
			print LOG "STATS_FUSION_SYNTENIES_ALL_GENOMES : q orga id $q_orga_id is not found in \%organism_id2earliest_date_seq\n";
			die("STATS_FUSION_SYNTENIES_ALL_GENOMES : q orga id $q_orga_id is not found in \%organism_id2earliest_date_seq\n");
		}

		# variables count
		#q_organism_id\tq_organism_earliest_date_annotation\tcount_total_CDS\tcount_CDS_with_at_least_1_homolog_in_a_fusion_fission_relationship\tcount_CDS_with_at_least_1_event_of_duplicates_homologs\tcount_CDS_with_at_least_1_homolog_in_syntenie\tcount_CDS_with_at_least_1_homolog_in_a_fusion_fission_relationship_comparing_to_organisms_annotated_at_a_earlier_date_only\tcount_CDS_with_at_least_1_event_of_duplicates_homologs_comparing_to_organisms_annotated_at_a_earlier_date_only\tcount_CDS_with_at_least_1_homolog_in_syntenie_comparing_to_organisms_annotated_at_a_earlier_date_only\n"; #TODO add count_CDS_with_profile_phylo, count_CDS_with_either_fusion_or_syntenie_or_profile_phylo and idem redundant with _comparing_to_organisms_annotated_at_a_earlier_date_only
		my %count_CDS_with_at_least_1_homolog_in_a_fusion_fission_relationship = ();
		my %count_CDS_with_at_least_1_event_of_duplicates_homologs = ();
		my %count_CDS_with_at_least_1_homolog_in_syntenie = ();
		#TODO my %count_CDS_with_profile_phylo = ();
		#TODO my %count_CDS_with_either_fusion_or_syntenie_or_profile_phylo = ();
		my %count_CDS_with_at_least_1_homolog_in_a_fusion_fission_relationship_comparing_to_organisms_annotated_at_a_earlier_date_only = ();
		my %count_CDS_with_at_least_1_event_of_duplicates_homologs_comparing_to_organisms_annotated_at_a_earlier_date_only = ();
		my %count_CDS_with_at_least_1_homolog_in_syntenie_comparing_to_organisms_annotated_at_a_earlier_date_only = ();
		#TODO my %count_CDS_with_profile_phylo_comparing_to_organisms_annotated_at_a_earlier_date_only = ();
		#TODO my %count_CDS_with_either_fusion_or_syntenie_or_profile_phylo_comparing_to_organisms_annotated_at_a_earlier_date_only = ();
		my %count_s_orga_id_earlier_date = ();


#TODO HERE

		# grep the input file_fusion_duplication_q_gene_id
		#header #q_gene_id\tq_organism_id\tpair_s_gene_id\ts_organism_id\tq_length\tpair_s_length\tis_fusion\tis_duplication\tlength_q_overlap_both_alignment\tlength_q_first_alignment_only\tlength_q_second_alignment_only\n";
		$output_cmd_grep = `$SiteConfig::CMDDIR/grep -E \'^[0-9]+\\t$q_orga_id\\t\$\' $file_fusion_duplication_q_gene_id`;
		chomp($output_cmd_grep);
		$output_cmd_grep =~ s/^\s+|\s+$//g; #remove leading and trailing space

	}# for my $i (0 .. $#list_organism_id) {

	close(STATS_FUSION_SYNTENIES_ALL_GENOMES_Q_ORGA_OUTPUT_FILE);
# needed to open file if just grep ?
	#close(STATS_FUSION_SYNTENIES_ALL_GENOMES_Q_GENE_ID_FUSION_DUPLICATION_INPUT_FILE);
	#close(STATS_FUSION_SYNTENIES_ALL_GENOMES_Q_GENE_ID_SYNTENY_INPUT_FILE);
	#TODO add file from profile phylo ?


	my $elapsed = tv_interval( $t0 );
	my $MEM_used = `ps h -o vsz $$`;
	$MEM_used =~ s/\R//g;
	my $rounded = sprintf("%.1f", ($MEM_used/1000) );
	print LOG "** Done stats fusion syntenies against all genomes in $elapsed seconds ; used $rounded MB of RAM.\n";

} # if ($STATS_FUSION_SYNTENIES_ALL_GENOMES =~ m/^ON$/i){



=cut




if ($STATS_ON_ORTHOLOGS_HOMOLOGS =~ m/^ON$/i){

	print LOG "** Starting stats on orthologs / homologs\n";

	sub stat_table_alignment_pairs {
		my ($type) = @_;

		my $count_tuples_alignment_pairs = 0;
		my $count_orthologs_with_mirror = 0;
		my $count_orthologs_without_mirror = 0;
		my $count_orthologs_other_unknown_case = 0;
		my $count_orthologs_with_mirror_multiple = 0;
		$command_string = "SELECT alignment_pairs.q_gene_id, alignment_pairs.s_gene_id FROM alignment_pairs, alignments WHERE alignment_pairs.type = $type AND alignment_pairs.alignment_id = alignments.alignment_id";
		if (@list_alignment_param_id_to_analyse) {
			$command_string .=  "AND alignments.alignment_param_id IN (".join(",", @list_alignment_param_id_to_analyse).")";
		}
		#$st = $ORIGAMI::dbh->prepare("SELECT alignment_pairs.q_gene_id, alignment_pairs.s_gene_id FROM alignment_pairs, alignments WHERE alignment_pairs.type = $type AND alignment_pairs.alignment_id = alignments.alignment_id AND alignments.alignment_param_id IN (".join(",", @list_alignment_param_id_to_analyse).")");
		$st = $ORIGAMI::dbh->prepare($command_string);
		$st->execute or die("Pb execute $!");
		my %q_s_gene_id_couple2count = ();
		while ( my $res = $st->fetchrow_hashref() ) {
			$count_tuples_alignment_pairs++;
			my $q_gene_id = $res->{q_gene_id};
			my $s_gene_id = $res->{s_gene_id};
			my $keyIT = "";
			if($q_gene_id < $s_gene_id){
				$keyIT = "${q_gene_id}-${s_gene_id}";
				if(exists $q_s_gene_id_couple2count{$keyIT}){
					$q_s_gene_id_couple2count{$keyIT} .= "+";
				} else {
					$q_s_gene_id_couple2count{$keyIT} = "+";
				}
			} else {
				$keyIT = "${s_gene_id}-${q_gene_id}";
				if(exists $q_s_gene_id_couple2count{$keyIT}){
					$q_s_gene_id_couple2count{$keyIT} .= "-";
				} else {
					$q_s_gene_id_couple2count{$keyIT} = "-";
				}
			}
		}
		my $count_uniq_orthologs = 0;
		foreach my $key (keys %q_s_gene_id_couple2count) {
			$count_uniq_orthologs++;
			my $valIT = $q_s_gene_id_couple2count{$key};
			if( $valIT =~ m/\+/ && $valIT =~ m/\-/
			){
				$count_orthologs_with_mirror++;
				if(length($valIT) != 2){
					$count_orthologs_with_mirror_multiple++;
				}
			} elsif (length($valIT) == 1) {
				#print "WARNING : count_orthologs_without_mirror : $key $valIT\n";
				$count_orthologs_without_mirror++;
			} else {
				#print "WARNING : count_orthologs_other_unknown_case : $key $valIT\n";
				$count_orthologs_other_unknown_case++;
			}
		}
		print LOG "\t\t Number of tuples in table : $count_tuples_alignment_pairs\n";
		print LOG "\t\t Number of tuples with mirror : $count_orthologs_with_mirror\n";
		print LOG "\t\t\t including $count_orthologs_with_mirror_multiple that are not only mirror but also found in multiple entries\n";
		#TODO check that they are paralogue ???
		print LOG "\t\t Number of tuples without mirror (check that they are paralogue ???) : $count_orthologs_without_mirror\n";
		#TODO check that they are multiple paralogue ???
		print LOG "\t\t Number of tuples unknown case : $count_orthologs_other_unknown_case\n";
		print LOG "\t\t Number of unique tuples : $count_uniq_orthologs\n";


	}
	# number of orthologs / homologs / mismatch / s_insertion / q_insertion : 
	print LOG "\t* stats in table alignment_pairs WHERE type = 1 (orthologs)\n";
	stat_table_alignment_pairs(1);
	print LOG "\t* stats in table alignment_pairs WHERE type = 2 (homologs)\n";
	stat_table_alignment_pairs(2);
	print LOG "\t* stats in table alignment_pairs WHERE type = 3 (mismatch)\n";
	stat_table_alignment_pairs(3);
	print LOG "\t* stats in table alignment_pairs WHERE type = 4 (s_insertion)\n";
	stat_table_alignment_pairs(4);
	print LOG "\t* stats in table alignment_pairs WHERE type = 5 (q_insertion)\n";
	stat_table_alignment_pairs(5);

}


sub stat_table_homologies {
	my ($ref_list_q_organism_id, $ref_list_s_organism_id, $ref_list_type) = @_;
#TODO
	$command_string = "SELECT 
 avg(homologies.score) as avg_score, stddev_pop(homologies.score) as stddev_score,
 avg(homologies.e_value) as avg_e_value, stddev_pop(homologies.e_value) as stddev_e_value,
 avg(homologies.identity) as avg_identity, stddev_pop(homologies.identity) as stddev_identity,
 avg(homologies.q_align_frac) as avg_q_align_frac, stddev_pop(homologies.q_align_frac) as stddev_q_align_frac,
 avg(homologies.s_align_frac) as avg_s_align_frac, stddev_pop(homologies.s_align_frac) as stddev_s_align_frac,
 avg(homologies.q_length) as avg_q_length, stddev_pop(homologies.q_length) as stddev_q_length,
 avg(homologies.s_length) as avg_s_length, stddev_pop(homologies.s_length) as stddev_s_length
 FROM alignment_pairs, homologies
 WHERE alignment_pairs.type IN (".join(",", @$ref_list_type).")
 AND alignment_pairs.q_gene_id = homologies.q_gene_id
 AND alignment_pairs.s_gene_id = homologies.s_gene_id
";
	if (@$ref_list_q_organism_id) {
		$command_string .=  " AND homologies.q_organism_id IN (".join(",", @$ref_list_q_organism_id).")";
	}
	if (@$ref_list_s_organism_id) {
		$command_string .=  " AND homologies.s_organism_id IN (".join(",", @$ref_list_s_organism_id).")";
	}
	$st = $ORIGAMI::dbh->prepare($command_string);
=pod
	$st = $ORIGAMI::dbh->prepare("SELECT 
 avg(homologies.score) as avg_score, stddev_pop(homologies.score) as stddev_score,
 avg(homologies.e_value) as avg_e_value, stddev_pop(homologies.e_value) as stddev_e_value,
 avg(homologies.identity) as avg_identity, stddev_pop(homologies.identity) as stddev_identity,
 avg(homologies.q_align_frac) as avg_q_align_frac, stddev_pop(homologies.q_align_frac) as stddev_q_align_frac,
 avg(homologies.s_align_frac) as avg_s_align_frac, stddev_pop(homologies.s_align_frac) as stddev_s_align_frac,
 avg(homologies.q_length) as avg_q_length, stddev_pop(homologies.q_length) as stddev_q_length,
 avg(homologies.s_length) as avg_s_length, stddev_pop(homologies.s_length) as stddev_s_length
 FROM alignment_pairs, homologies
 WHERE homologies.q_organism_id IN (".join(",", @$ref_list_q_organism_id).")
 AND homologies.s_organism_id IN (".join(",", @$ref_list_s_organism_id).")
 AND alignment_pairs.type IN (".join(",", @$ref_list_type).")
 AND alignment_pairs.q_gene_id = homologies.q_gene_id
 AND alignment_pairs.s_gene_id = homologies.s_gene_id
");
=cut
	$total_loops_to_do = $st->execute or die("Pb execute $!");
	my $object_stat_avg_homologies = new Object_stat_avg_homologies();
	$count_loop_IT = 0;
	while ( my $res = $st->fetchrow_hashref() ) {

		$count_loop_IT++;
		print LOG "progress stat_table_homologies: $count_loop_IT / $total_loops_to_do\n";	

		#my $avg_scoreIT = $res->{avg_score};
		$object_stat_avg_homologies->set_avg_score($res->{avg_score});
		#my $stddev_scoreIT = $res->{stddev_score};
		$object_stat_avg_homologies->set_stddev_score($res->{stddev_score});
		#my $avg_e_valueIT = $res->{avg_e_value};
		$object_stat_avg_homologies->set_avg_e_value($res->{avg_e_value});
		#my $stddev_e_valueIT = $res->{stddev_e_value};
		$object_stat_avg_homologies->set_stddev_e_value($res->{stddev_e_value});
		#my $avg_identityIT = $res->{avg_identity};
		$object_stat_avg_homologies->set_avg_identity($res->{avg_identity});
		#my $stddev_identityIT = $res->{stddev_identity};
		$object_stat_avg_homologies->set_stddev_identity($res->{stddev_identity});
		#my $avg_q_align_fracIT = $res->{avg_q_align_frac};
		$object_stat_avg_homologies->set_avg_q_align_frac($res->{avg_q_align_frac});
		#my $stddev_q_align_fracIT = $res->{stddev_q_align_frac};
		$object_stat_avg_homologies->set_stddev_q_align_frac($res->{stddev_q_align_frac});
		#my $avg_s_align_fracIT = $res->{avg_s_align_frac};
		$object_stat_avg_homologies->set_avg_s_align_frac($res->{avg_s_align_frac});
		#my $stddev_s_align_fracIT = $res->{stddev_s_align_frac};
		$object_stat_avg_homologies->set_stddev_s_align_frac($res->{stddev_s_align_frac});
		#my $avg_q_lengthIT = $res->{avg_q_length};
		$object_stat_avg_homologies->set_avg_q_length($res->{avg_q_length});
		#my $stddev_q_lengthIT = $res->{stddev_q_length};
		$object_stat_avg_homologies->set_stddev_q_length($res->{stddev_q_length});
		#my $avg_s_lengthIT = $res->{avg_s_length};
		$object_stat_avg_homologies->set_avg_s_length($res->{avg_s_length});
		#my $stddev_s_lengthIT = $res->{stddev_s_length};
		$object_stat_avg_homologies->set_stddev_s_length($res->{stddev_s_length});
	}
	return \$object_stat_avg_homologies;

}


if ($STATS_TABLE_HOMOLOGIES_FOR_ALL_ORTHO_HOMO =~ m/^ON$/i){

	print LOG "** Starting stats table homologies for all orthologs and homologs...\n";
	my $t0      = [gettimeofday()];

	#dependancies
	# list all organisms with genes : %gene_id2organism_id and %organism_id2gene_count and %organism_id2list_gene_ids
	build_from_table_genes();

	my @list_organism_id = sort { $a <=> $b } keys %organism_id2list_gene_ids;

	# orthologs
	print LOG "\ttStart stats for all orthologs (type 1)...\n";
	my @list_type = ();
	push @list_type, "1"; 
	my $ref_object_stat_avg_homologies = stat_table_homologies(\@list_organism_id, \@list_organism_id, \@list_type);
	print LOG "\tstats table homologies for all orthologs:\n\t - avg_score = ".$$ref_object_stat_avg_homologies->get_avg_score()."\n\t - stddev_score = ".$$ref_object_stat_avg_homologies->get_stddev_score()."\n\t - avg_e_value = ".$$ref_object_stat_avg_homologies->get_avg_e_value()."\n\t - stddev_e_value = ".$$ref_object_stat_avg_homologies->get_stddev_e_value()."\n\t - avg_identity = ".$$ref_object_stat_avg_homologies->get_avg_identity()."\n\t - stddev_identity = ".$$ref_object_stat_avg_homologies->get_stddev_identity()."\n\t - avg_q_align_frac = ".$$ref_object_stat_avg_homologies->get_avg_q_align_frac()."\n\t - stddev_q_align_frac = ".$$ref_object_stat_avg_homologies->get_stddev_q_align_frac()."\n\t - avg_s_align_frac = ".$$ref_object_stat_avg_homologies->get_avg_s_align_frac()."\n\t - stddev_s_align_frac = ".$$ref_object_stat_avg_homologies->get_stddev_s_align_frac()."\n\t - avg_q_length = ".$$ref_object_stat_avg_homologies->get_avg_q_length()."\n\t - stddev_q_length = ".$$ref_object_stat_avg_homologies->get_stddev_q_length()."\n\t - avg_s_length = ".$$ref_object_stat_avg_homologies->get_avg_s_length()."\n\t - stddev_s_length = ".$$ref_object_stat_avg_homologies->get_stddev_s_length()."\n";

	# homologs
	print LOG "\tStart stats for all orthologs (type 1)...\n";
	my @list_type = ();
	push @list_type, "2"; 
	my $ref_object_stat_avg_homologies = stat_table_homologies(\@list_organism_id, \@list_organism_id, \@list_type);
	print LOG "\tstats table homologies for all homologs:\n\t - avg_score = ".$$ref_object_stat_avg_homologies->get_avg_score()."\n\t - stddev_score = ".$$ref_object_stat_avg_homologies->get_stddev_score()."\n\t - avg_e_value = ".$$ref_object_stat_avg_homologies->get_avg_e_value()."\n\t - stddev_e_value = ".$$ref_object_stat_avg_homologies->get_stddev_e_value()."\n\t - avg_identity = ".$$ref_object_stat_avg_homologies->get_avg_identity()."\n\t - stddev_identity = ".$$ref_object_stat_avg_homologies->get_stddev_identity()."\n\t - avg_q_align_frac = ".$$ref_object_stat_avg_homologies->get_avg_q_align_frac()."\n\t - stddev_q_align_frac = ".$$ref_object_stat_avg_homologies->get_stddev_q_align_frac()."\n\t - avg_s_align_frac = ".$$ref_object_stat_avg_homologies->get_avg_s_align_frac()."\n\t - stddev_s_align_frac = ".$$ref_object_stat_avg_homologies->get_stddev_s_align_frac()."\n\t - avg_q_length = ".$$ref_object_stat_avg_homologies->get_avg_q_length()."\n\t - stddev_q_length = ".$$ref_object_stat_avg_homologies->get_stddev_q_length()."\n\t - avg_s_length = ".$$ref_object_stat_avg_homologies->get_avg_s_length()."\n\t - stddev_s_length = ".$$ref_object_stat_avg_homologies->get_stddev_s_length()."\n";

	print LOG "** Done stats stats table homologies for all orthologs and homologs...\n";
}



if ($STATS_TABLE_HOMOLOGIES_PER_ORGANISM =~ m/^ON$/i){
#TODO

=pod

#transfer code to adapt

print STATS_FUSION_SYNTENIES_HOMOLOGS_FILE "#q_organism_id\tavg_score_homologs\tstddev_score_homologs\tavg_e_value_homologs\tstddev_e_value_homologs\tavg_identity_homologs\tstddev_identity_homologs\tavg_q_align_frac_homologs\tstddev_q_align_frac_homologs\tavg_s_align_frac_homologs\tstddev_s_align_frac_homologs\tavg_q_length_homologs\tstddev_q_length_homologs\tavg_s_length_homologs\tstddev_s_length_homologs\n";

			#stat table homologie
			my @list_q_organism_id = ();
			push @list_q_organism_id, $q_orga_id;
			my @list_s_organism_id = ();
			push @list_s_organism_id, $s_orga_id;
			my @list_type = ();
			push @list_type, "1";
			push @list_type, "2";
			my $ref_object_stat_avg_homologies = stat_table_homologies(\@list_q_organism_id, \@list_s_organism_id, \@list_type);

\t".$$ref_object_stat_avg_homologies->get_avg_score()."\t".$$ref_object_stat_avg_homologies->get_stddev_score()."\t".$$ref_object_stat_avg_homologies->get_avg_e_value()."\t".$$ref_object_stat_avg_homologies->get_stddev_e_value()."\t".$$ref_object_stat_avg_homologies->get_avg_identity()."\t".$$ref_object_stat_avg_homologies->get_stddev_identity()."\t".$$ref_object_stat_avg_homologies->get_avg_q_align_frac()."\t".$$ref_object_stat_avg_homologies->get_stddev_q_align_frac()."\t".$$ref_object_stat_avg_homologies->get_avg_s_align_frac()."\t".$$ref_object_stat_avg_homologies->get_stddev_s_align_frac()."\t".$$ref_object_stat_avg_homologies->get_avg_q_length()."\t".$$ref_object_stat_avg_homologies->get_stddev_q_length()."\t".$$ref_object_stat_avg_homologies->get_avg_s_length()."\t".$$ref_object_stat_avg_homologies->get_stddev_s_length()."

=cut

=pod

# OLD way stat on all organism for type 1 or 2

	print LOG "** Starting stats on table homologies\n";

	print LOG "\t* stats in table homologies WHERE type = 1 (orthologs)\n";
	my @list_type_1 = ();
	push @list_type_1, "1";
	my $ref_object_stat_avg_homologies_1 = stat_table_homologies(\@list_orga_ids_to_analyse, \@list_orga_ids_to_analyse, \@list_type_1);
	# score
	print LOG "\t\t average score : ".$ref_object_stat_avg_homologies_1->get_avg_score()." ; stddev ".$ref_object_stat_avg_homologies_1->get_stddev_score()."\n";
	# e_value
	print LOG "\t\t average e_value : ".$ref_object_stat_avg_homologies_1->get_avg_e_value()." ; stddev ".$ref_object_stat_avg_homologies_1->get_stddev_e_value()."\n";
	# identity
	print LOG "\t\t average identity : ".$ref_object_stat_avg_homologies_1->get_avg_identity()." ; stddev ".$ref_object_stat_avg_homologies_1->get_stddev_identity()."\n";
	# q_align_frac
	print LOG "\t\t average q_align_frac : ".$ref_object_stat_avg_homologies_1->get_avg_q_align_frac()." ; stddev ".$ref_object_stat_avg_homologies_1->get_stddev_q_align_frac()."\n";
	# s_align_frac
	print LOG "\t\t average s_align_frac : ".$ref_object_stat_avg_homologies_1->get_avg_s_align_frac()." ; stddev ".$ref_object_stat_avg_homologies_1->get_stddev_s_align_frac()."\n";
	# q_length
	print LOG "\t\t average q_length : ".$ref_object_stat_avg_homologies_1->get_avg_q_length()." ; stddev ".$ref_object_stat_avg_homologies_1->get_stddev_q_length()."\n";
	# s_length
	print LOG "\t\t average s_length : ".$ref_object_stat_avg_homologies_1->get_avg_s_length()." ; stddev ".$ref_object_stat_avg_homologies_1->get_stddev_s_length()."\n";


	print LOG "\t* stats in table homologies WHERE type = 2 (homologs)\n";
	my @list_type_2 = ();
	push @list_type_2, "2";
	my $ref_object_stat_avg_homologies_2 = stat_table_homologies(\@list_orga_ids_to_analyse, \@list_orga_ids_to_analyse, \@list_type_2);
	# score
	print LOG "\t\t average score : ".$ref_object_stat_avg_homologies_2->get_avg_score()." ; stddev ".$ref_object_stat_avg_homologies_2->get_stddev_score()."\n";
	# e_value
	print LOG "\t\t average e_value : ".$ref_object_stat_avg_homologies_2->get_avg_e_value()." ; stddev ".$ref_object_stat_avg_homologies_2->get_stddev_e_value()."\n";
	# identity
	print LOG "\t\t average identity : ".$ref_object_stat_avg_homologies_2->get_avg_identity()." ; stddev ".$ref_object_stat_avg_homologies_2->get_stddev_identity()."\n";
	# q_align_frac
	print LOG "\t\t average q_align_frac : ".$ref_object_stat_avg_homologies_2->get_avg_q_align_frac()." ; stddev ".$ref_object_stat_avg_homologies_2->get_stddev_q_align_frac()."\n";
	# s_align_frac
	print LOG "\t\t average s_align_frac : ".$ref_object_stat_avg_homologies_2->get_avg_s_align_frac()." ; stddev ".$ref_object_stat_avg_homologies_2->get_stddev_s_align_frac()."\n";
	# q_length
	print LOG "\t\t average q_length : ".$ref_object_stat_avg_homologies_2->get_avg_q_length()." ; stddev ".$ref_object_stat_avg_homologies_2->get_stddev_q_length()."\n";
	# s_length
	print LOG "\t\t average s_length : ".$ref_object_stat_avg_homologies_2->get_avg_s_length()." ; stddev ".$ref_object_stat_avg_homologies_2->get_stddev_s_length()."\n";
=cut

}


if ($STATS_COUNT_SYNTENIES_SINGLETON =~ m/^ON$/i){

	print LOG "** Starting stats on count syntenies / singletons\n";

	# number of reversed q-s syntenies :
	# for each tuple in alignments
	# 	store the query_score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size
	# 	store the query_q_start, q_stop, s_start, s_stop
	# 	get the alignment_param_id2mirror_alignment_param_id
	my $count_mirror = 0;
	my $count_not_mirror = 0;
	my $count_mirror_singleton = 0;
	my $count_mirror_syntenies = 0;
	my $count_not_mirror_singleton = 0;
	my $count_not_mirror_syntenies = 0;
	$command_string = "SELECT * FROM alignments, alignment_params WHERE alignments.alignment_param_id = alignment_params.alignment_param_id";
	if (@list_alignment_param_id_to_analyse) {
		$command_string .=  "AND alignment_params.alignment_param_id IN (".join(",", @list_alignment_param_id_to_analyse).")";
	}
	$st = $ORIGAMI::dbh->prepare($command_string);
	#$st = $ORIGAMI::dbh->prepare("SELECT * FROM alignments, alignment_params WHERE alignment_params.alignment_param_id IN (".join(",", @list_alignment_param_id_to_analyse).") AND alignments.alignment_param_id = alignment_params.alignment_param_id");
	$total_loops_to_do = $st->execute or die("Pb execute $!");

	$count_loop_IT = 0;
	#if (@list_alignment_param_id_to_analyse) {
	#	$total_loops_to_do = scalar (@list_alignment_param_id_to_analyse) ;
	#} else {
	#	$total_loops_to_do = $number_tuples_table_alignment_params;
	#}
	while ( my $res = $st->fetchrow_hashref() ) {

		$count_loop_IT++;
		print LOG "progress STATS_COUNT_SYNTENIES_SINGLETON: $count_loop_IT / $total_loops_to_do\n";		
		
		my $query_alignment_id = $res->{alignment_id};
		my $query_alignment_param_id = $res->{alignment_param_id};
		my $query_score = $res->{score};
		my $query_pairs = $res->{pairs};
		my $query_orientation_conserved = $res->{orientation_conserved};
		my $query_orthologs = $res->{orthologs};
		my $query_homologs = $res->{homologs};
		my $query_mismatches = $res->{mismatches};
		my $query_gaps = $res->{gaps};
		my $query_total_gap_size = $res->{total_gap_size};
		my $query_q_start = $res->{q_start};
		my $query_q_stop = $res->{q_stop};
		my $query_s_start = $res->{s_start};
		my $query_s_stop = $res->{s_stop};
		my $query_q_organism_id = $res->{q_organism_id};
		my $query_q_element_id = $res->{q_element_id};
		my $query_s_organism_id = $res->{s_organism_id};
		my $query_s_element_id = $res->{s_element_id};
		my $query_ortho_score = $res->{ortho_score};
		my $query_homo_score = $res->{homo_score};
		my $query_mismatch_penalty = $res->{mismatch_penalty};
		my $query_gap_creation_penalty = $res->{gap_creation_penalty};
		my $query_gap_extension_penalty = $res->{gap_extension_penalty};
		my $query_min_align_size = $res->{min_align_size};
		my $query_min_score = $res->{min_score};
		my $query_orthologs_included = $res->{orthologs_included};

		my $mirror_alignment_param_id = -1;
		my $st2 = $ORIGAMI::dbh->prepare("SELECT alignment_param_id FROM alignment_params WHERE q_element_id = $query_s_element_id AND s_element_id = $query_q_element_id AND ortho_score = $query_ortho_score AND homo_score = $query_homo_score AND mismatch_penalty = $query_mismatch_penalty AND gap_creation_penalty = $query_gap_creation_penalty AND gap_extension_penalty = $query_gap_extension_penalty AND min_align_size = $query_min_align_size AND min_score = $query_min_score");
		$st2->execute or die("Pb execute $!");
		my $count_rowsIT = 0;
		while ( my $res2 = $st2->fetchrow_hashref() ) {
			$mirror_alignment_param_id = $res2->{alignment_param_id};
			$count_rowsIT++;
			if($count_rowsIT > 1){
				die("ERROR number of reversed q-s syntenies \$count_rowsIT = $count_rowsIT for alignment_id = $query_alignment_id");
			}
		}

		# 	check for mirror duplicates : select * from alignments WHERE 
		#	score = $query_score AND pairs = $query_pairs AND orientation_conserved = $query_orientation_conserved AND orthologs = $query_orthologs AND homologs = $query_homologs AND mismatches = $query_mismatches AND gaps = $query_gaps AND total_gap_size = $query_total_gap_size
		#	AND q_start = $query_s_start AND q_stop = $query_s_stop AND s_start = $query_q_start AND s_stop = $query_q_stop
		#	AND alignment_param_id = mirror_alignment_param_id
		my $query_orientation_conserved_bool = "false";
		if($query_orientation_conserved == 1){
			$query_orientation_conserved_bool = "true";
		}
		my $st3 = $ORIGAMI::dbh->prepare("select * from alignments WHERE score = $query_score AND pairs = $query_pairs AND orientation_conserved = $query_orientation_conserved_bool AND orthologs = $query_orthologs AND homologs = $query_homologs AND mismatches = $query_mismatches AND gaps = $query_gaps AND total_gap_size = $query_total_gap_size AND q_start = $query_s_start AND q_stop = $query_s_stop AND s_start = $query_q_start AND s_stop = $query_q_stop AND alignment_param_id = $mirror_alignment_param_id");
		$st3->execute or die("Pb execute $!");
		my $count_rowsIT = 0;
		my $found_mirror = -1;
		while ( my $res3 = $st3->fetchrow_hashref() ) {
			$found_mirror = 1;
		}
		if ($found_mirror) {
			$count_mirror++;
			if($query_pairs == 1){
				$count_mirror_singleton++;
			} else {
				$count_mirror_syntenies++;
			}
		} else {
			$count_not_mirror++;
			if($query_pairs == 1){
				$count_not_mirror_singleton++;
			} else {
				$count_not_mirror_syntenies++;
			}
		}

	}
	print LOG "** Number of alignments in db with mirror : $count_mirror\n";
	print LOG "** Number of alignments in db without mirror : $count_not_mirror\n";
	$res_formule = $count_not_mirror+($count_mirror/2);
	print LOG "** Number of unique alignments in db : $res_formule\n";
	$res_formule = $count_not_mirror_singleton+($count_mirror_singleton/2);
	print LOG "** Number of unique singleton orthologs : $res_formule ($count_not_mirror_singleton+($count_mirror_singleton/2))\n";
	$res_formule = $count_not_mirror_syntenies+($count_mirror_syntenies/2);
	print LOG "** Number of unique syntenies (orthologs / homologs >= 2) : $res_formule ($count_not_mirror_syntenies+($count_mirror_syntenies/2))\n";

}


if ($STAT_CDS_ABNORMAL_STRUCTURE =~ m/^ON$/i){

	print LOG "** Starting stats on count syntenies / singletons\n";

	# check in table genes when ( stop - start - 2 ) / 3 not int
	print LOG "\n\n";
	print LOG "** Starting check in table genes when nbr_codons != (lenght residues + 1): \n";
	my $count_failed_genes_stop_start_diff_res_length = 0;
	my $count_JOIN = 0;
	my $count_Ribosomal_Slippage = 0;
	my $count_Insertion = 0;
	my $count_nbr_codons_eq_lenght_residues = 0;
	my $count_left_unexplained = 0;
	$st = $ORIGAMI::dbh->prepare("SELECT genes.gene_id, genes.start, genes.stop, prot_feat.proteine, genes.element_id, genes.feature_id, elements.accession FROM genes, elements, micado.prot_feat WHERE genes.organism_id IN (".join(",", @list_orga_ids_to_analyse).") AND genes.element_id = elements.element_id AND genes.accession = micado.prot_feat.accession AND genes.feature_id = micado.prot_feat.code_feat");
	$st->execute or die("Pb execute $!");
	while ( my $res = $st->fetchrow_hashref() ) {
		my $gene_id = $res->{gene_id};
		my $start = $res->{start};
		my $stop = $res->{stop};
		my $residues = $res->{residues};
		my $lenght_residues = length($residues);
		my $element_id = $res->{element_id};
		my $feature_id = $res->{feature_id};
		my $accession = $res->{accession};

		my $nbr_codons;
		if($stop > $start){

			$nbr_codons = ($stop - $start + 1 ) / 3;

			if($nbr_codons  != ( $lenght_residues + 1) ){

				$count_failed_genes_stop_start_diff_res_length++;
				my $inner_st = $ORIGAMI::dbh->prepare("select * from micado.locations where code_feat = $feature_id AND accession = '$accession'");
				$inner_st->execute or die("Pb execute $!");
				my $count_locations_rows = 0;
				while ( my $inner_res = $inner_st->fetchrow_hashref() ) {
					$count_locations_rows++;
				}
				my $inner_st_bis = $ORIGAMI::dbh->prepare("select * from micado.qualifiers where code_feat = $feature_id AND accession = '$accession' AND type_qual = 'ribosomal_slippage'");
				$inner_st_bis->execute or die("Pb execute $!");
				my $flag_ribosomal_slippage = 0;
				if ( my $inner_res_bis = $inner_st_bis->fetchrow_hashref() ) {
					$flag_ribosomal_slippage = 1;
				}
				my $inner_st_ter = $ORIGAMI::dbh->prepare("select * from micado.qualifiers where code_feat = $feature_id AND accession = '$accession' AND type_qual = 'insertion'");
				$inner_st_ter->execute or die("Pb execute $!");
				my $flag_insertion = 0;
				if ( my $inner_res_ter = $inner_st_ter->fetchrow_hashref() ) {
					$flag_insertion = 1;
				}

				if($count_locations_rows <= 0){
					print LOG ("no row returned for select * from micado.locations where code_feat = $feature_id AND accession = '$accession'\n");
					die("no row returned for select * from micado.locations where code_feat = $feature_id AND accession = '$accession'\n");
				}

				if($count_locations_rows > 1){
					#ok JOIN
					$count_JOIN++;
				} elsif ($count_locations_rows == 1 && $flag_ribosomal_slippage == 1) {
					#ok ribosomal_slippage
					$count_Ribosomal_Slippage++;
				} elsif($count_locations_rows == 1 && $flag_insertion == 1) {
					#ok insertion
					$count_Insertion++;
				} elsif ($nbr_codons == $lenght_residues) {
					$count_nbr_codons_eq_lenght_residues++;
				} else {
					$count_left_unexplained++;
					if ( $PRINT_DETAILS_TEST_ANNOT_STRUCT =~ m/^ON$/i ) {
						print LOG ("no Join or ribosomal_slippage or insertion despite nbr_codons != (lenght residues + 1) => $nbr_codons = ($stop - $start + 1 ) / 3) != $lenght_residues for gene id $gene_id\n\tselect * from micado.locations where code_feat = $feature_id AND accession = '$accession'\n\tselect * from micado.qualifiers where code_feat = $feature_id AND accession = '$accession'\n");
					}
				}
			}

		}else{
			#TODO gene across ORI, get element size...
		}

	}

	#TODO other type qual to integrate as test: exception
	print LOG "** Done test check rows in table genes where nbr_codons >= (lenght residues + 1).\nNumber of genes that do not meet this formula = $count_failed_genes_stop_start_diff_res_length. Of which:\n";
	print LOG "\t- number of CDS with JOIN location = $count_JOIN\n";
	print LOG "\t- number of CDS with flag Ribosomal Slippage = $count_Ribosomal_Slippage\n";
	print LOG "\t- number of CDS with flag Insertion = $count_Insertion\n";
	print LOG "\t- number of CDS with nbr_codons eq lenght_residues (probably missing stop codon in structural annotation ?) = $count_nbr_codons_eq_lenght_residues \n";
	print LOG "\t=> left unexplained = $count_left_unexplained\n";
	print LOG "\n";

}

=for comment


#TODO test gene fusion
#SELECT alignment_pairs.q_gene_id, count(alignment_pairs.alignment_id), homologies.q_first, homologies.q_last, homologies.s_first, homologies.s_last FROM alignment_pairs, genes, homologies WHERE alignment_pairs.s_gene_id = genes.gene_id AND alignment_pairs.q_gene_id = homologies.q_gene_id AND alignment_pairs.s_gene_id = homologies.s_gene_id GROUP BY alignment_pairs.q_gene_id, genes.element_id HAVING count(alignment_id) > 1;


print LOG "** Starting test gene fusion\n";
my %duplicates_q_gene_id2count = ();
my $count_test_gene_fusion_match_duplicates_q_gene_id = 0;
my $count_test_gene_fusion_match_identical_s_gene_id = 0;
my $count_test_gene_fusion_match_different_s_gene_id_from_same_element = 0;
my $count_test_gene_fusion_duplicates_q_gene_id_non_overlapping = 0;
my $count_test_gene_fusion_duplicates_q_gene_id_overlapping_less_30 = 0;
my $count_test_gene_fusion_duplicates_q_gene_id_overlapping_greater_30 = 0;

$st = $ORIGAMI::dbh->prepare("SELECT alignment_pairs.q_gene_id, count(alignment_pairs.alignment_id) FROM alignment_pairs, genes WHERE alignment_pairs.s_gene_id = genes.gene_id AND alignment_pairs.q_gene_id > 0 GROUP BY alignment_pairs.q_gene_id, genes.element_id HAVING count(alignment_id) > 1");
$st->execute  or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	$duplicates_q_gene_id2count{$res->{q_gene_id}} = 1;
}
foreach my $key_duplicates_q_gene_id2count_IT (keys %duplicates_q_gene_id2count) {
	$count_test_gene_fusion_match_duplicates_q_gene_id++;

	my %inner_s_element_id2array_s_gene_id = ();
	my $inner_st1 = $ORIGAMI::dbh->prepare("SELECT alignment_pairs.s_gene_id, genes.element_id FROM alignment_pairs, genes WHERE alignment_pairs.q_gene_id = $key_duplicates_q_gene_id2count_IT AND alignment_pairs.s_gene_id = genes.gene_id");
	$inner_st1->execute  or die("Pb execute $!");
	while ( my $inner_res1 = $inner_st1->fetchrow_hashref() ) {
		my $s_element_id_IT =  $inner_res1->{element_id};
		my $s_gene_id_IT =  $inner_res1->{s_gene_id};

		if ( exists $inner_s_element_id2array_s_gene_id{$s_element_id_IT} ) {
			# add $s_gene_id_IT to array
			my @list_s_gene_ids = $inner_s_element_id2array_s_gene_id{$s_element_id_IT};
			push(@list_s_gene_ids, $s_gene_id_IT);
			$inner_s_element_id2array_s_gene_id{$s_element_id_IT} = @list_s_gene_ids;
		}else{
			# create entry
			my @list_s_gene_ids = ($s_gene_id_IT);
			$inner_s_element_id2array_s_gene_id{$s_element_id_IT} = @list_s_gene_ids;
		}
	}

	while (my ($key_element_id, @value_array_gene_ids) = each %inner_s_element_id2array_s_gene_id) {
		#TODO
	}

}


print LOG "** Test gene fusion done. Here are the results : \n";
print LOG "${count_test_gene_fusion_match_duplicates_q_gene_id}  duplicates q_gene_id are found having mutliples s_gene from the same element in different alignments. Of which : \n";
print LOG "\t- ${count_test_gene_fusion_match_identical_s_gene_id} match identical s_gene_id\n";
print LOG "\t- ${count_test_gene_fusion_match_different_s_gene_id_from_same_element} match different s_gene_id from the same element\n";
print LOG "Regarding the overlap of the alignment regions on the query,\n";
print LOG "\t- ${count_test_gene_fusion_duplicates_q_gene_id_non_overlapping} duplicates q_gene_id have non overlapping matching regions\n";
print LOG "\t- ${count_test_gene_fusion_duplicates_q_gene_id_overlapping_less_30} duplicates q_gene_id have overlapping matching regions where the percentage of overlap is less than 30 percent of each individual matching region\n";
print LOG "\t- ${count_test_gene_fusion_duplicates_q_gene_id_overlapping_greater_30} duplicates q_gene_id have overlapping matching regions where the percentage of overlap is greater than 30 percent of each individual matching region\n";
=cut



#TODO synteny that are perfect reverse of each other (test consistency alignment without orientation_conserved)



#TODO tuples in table alignment_pairs that are duplcated  within the same element (both internal and external duplication)
#SELECT q_gene_id, count(alignment_id) FROM alignment_pairs, genes WHERE alignment_pairs.s_gene_id = genes.gene_id GROUP BY alignment_pairs.q_gene_id, genes.element_id HAVING count(alignment_id) > 1;



#end of script
print LOG
"\n---------------------------------------------------\n\n\n analyse_stats_db.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);






