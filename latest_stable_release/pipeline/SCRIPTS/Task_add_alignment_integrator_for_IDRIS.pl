#!/usr/local/bin/perl
#
# Task_add_alignment_integrator_for_IDRIS
#
#
# Pipeline origami Copyright - INRA - 2012-2025
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#


use strict;
use Time::HiRes qw(sleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;
use BlastConfig;


# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_integrator_for_IDRIS.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/Task_add_alignment_integrator_for_IDRIS.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/Task_add_alignment_integrator_for_IDRIS.error";
my $output_backtick = "";
my @alignment_params_table_sql_file            = ();
my $VERBOSE = "ON";
my $SQL_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sql";
my $TRIM_UNUSED_ALIGNMENT_PARAMS_DATA = "ON";
my $TABLE_ALIGNMENT_PARAMS_ONLY = "OFF";
my $FILE_BLACKLISTED_ORGANISM_IDS = undef;
my $MAX_FILES_TO_CHARGE_IN_THIS_RUN = -1;
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $molecule_type;
my $FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT = -1;
my $counter_loop = 0;
my $in; # infile to be read
my $out; # outfile to be written
my %HASH_BLACKLISTED_ORGANISM_IDS = ();
my $output_cmd = "";

sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


foreach my $argnum ( 0 .. $#ARGV ) {

	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl Task_add_alignment_integrator_for_IDRIS.pl -VERBOSE {ON, OFF}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-TRIM_UNUSED_ALIGNMENT_PARAMS_DATA$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$TRIM_UNUSED_ALIGNMENT_PARAMS_DATA = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -TRIM_UNUSED_ALIGNMENT_PARAMS_DATA argument ; usage : perl Task_add_alignment_integrator_for_IDRIS.pl -TRIM_UNUSED_ALIGNMENT_PARAMS_DATA {ON, OFF}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-TABLE_ALIGNMENT_PARAMS_ONLY$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$TABLE_ALIGNMENT_PARAMS_ONLY = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -TABLE_ALIGNMENT_PARAMS_ONLY argument ; usage : perl Task_add_alignment_integrator_for_IDRIS.pl -TABLE_ALIGNMENT_PARAMS_ONLY {ON, OFF}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^(\d)$/i )
		{
			$FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT = $1;
			print LOG "\nmode FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT activated : $FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT\n" unless $VERBOSE =~ m/^OFF$/i;
		}
		else {
			die_with_error_mssg ("incorrect -FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT argument ; usage : perl Task_add_alignment_integrator_for_IDRIS.pl -FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT {DIGIT}");
		}
	}elsif ( $ARGV[$argnum] =~ m/^-SQL_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$SQL_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -SQL_INPUT_DIR argument ; usage : perl Task_add_alignment_integrator_for_IDRIS.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-MAX_FILES_TO_CHARGE_IN_THIS_RUN/ ) {

		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_FILES_TO_CHARGE_IN_THIS_RUN = $ARGV[ $argnum + 1 ];
			if($MAX_FILES_TO_CHARGE_IN_THIS_RUN < 1){
				die_with_error_mssg ("incorrect -MAX_FILES_TO_CHARGE_IN_THIS_RUN argument ; usage : perl Task_add_alignment_integrator_for_IDRIS.pl  -MAX_FILES_TO_CHARGE_IN_THIS_RUN {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAX_FILES_TO_CHARGE_IN_THIS_RUN argument ; usage : perl Task_add_alignment_integrator_for_IDRIS.pl  -MAX_FILES_TO_CHARGE_IN_THIS_RUN {int > 0}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-FILE_BLACKLISTED_ORGANISM_IDS$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$FILE_BLACKLISTED_ORGANISM_IDS = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -FILE_BLACKLISTED_ORGANISM_IDS argument ; usage : perl Task_add_alignment_integrator_for_IDRIS.pl -FILE_BLACKLISTED_ORGANISM_IDS {PATH_TO_DIRECTORY}");
		}
	}

}

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
if ($FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT >= 0){
	if (-e "$SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_integrator_for_IDRIS_${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}.log"){
		die_with_error_mssg ("The log file $SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_integrator_for_IDRIS_${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}.log already exists and mode FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT is activated ; please make sure no other process are using the same FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT and is running in parrallele, else it will cause duplicated data.");
	} else {
		open( LOG, ">$SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_integrator_for_IDRIS_${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}.log"); #| perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
		#open( LOG, ">${path_log_file}_${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
	}
} else {
	open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
}


if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
	#die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}

print LOG
"\n---------------------------------------------------\n Task_add_alignments_integrator_for_IDRIS.pl started at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/i;

# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}


if (! defined $SQL_INPUT_DIR) {
	die_with_error_mssg("Undefined SQL_INPUT_DIR ; usage : perl Task_add_alignment_integrator_for_IDRIS.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}");
}

my $count_files = 0;
my $count_gz_files = 0;
my $count_todo_gzip_files = 0;
my $count_files_skipped_because_of_FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT = 0;
sub sub_list_alignment_params_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die_with_error_mssg("Unable to open $path: $!");
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_alignment_params_files_recursively ($_);
        } elsif ($_ =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table\.sql$/ ) {
		my $master_molecule_id_IT = $1;
		$count_files++;

		if ( $MAX_FILES_TO_CHARGE_IN_THIS_RUN > 0 && scalar(@alignment_params_table_sql_file) == $MAX_FILES_TO_CHARGE_IN_THIS_RUN ) {

		} else {
			if ($FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT >= 0) {
				#mode FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT ON
				if ($master_molecule_id_IT  =~ m/^\d*${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}$/){
					push ( @alignment_params_table_sql_file, $_);
				} else {
					$count_files_skipped_because_of_FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT++;
				}
			} else {
           			push ( @alignment_params_table_sql_file, $_);
			}
		}
        } elsif ($_ =~ m/^.+_alignment_params_table_TRIMED\.sql$/ ) {
		#do nothing
        } elsif ($_ =~ m/^.+_alignment_params_table\.sql\.gz$/ ) {
		$count_gz_files++;
        } elsif ($_ =~ m/^.+_alignment_params_table_TRIMED\.sql\.gz$/ ) {
		#do nothing
        } elsif ($_ =~ m/^.+_alignment_params_table\.sql\.todo_gzip$/ ) {
		$count_todo_gzip_files++;
        } else {
            die_with_error_mssg("Error in sub_list_alignment_params_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
#get all *_alignment_params_table.sql file
sub_list_alignment_params_files_recursively("$SQL_INPUT_DIR/alignment_params/");
#@alignment_params_table_sql_file = <$SQL_INPUT_DIR/*_alignment_params_table.sql>;


print LOG "$count_gz_files alignment params table sql.gz file(s) have been found and skipped.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "$count_todo_gzip_files alignment params table sql.todo_gzip file(s) have been found and skipped.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "$count_files_skipped_because_of_FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT alignment params table file(s) skipped because of FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT = ${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "$count_files alignment params table sql file(s) found in total.\n";
print LOG "\n".scalar(@alignment_params_table_sql_file)." alignment params table sql file(s) found to be treated.\n" unless $VERBOSE =~ m/^OFF$/i;
if ( $MAX_FILES_TO_CHARGE_IN_THIS_RUN > 0 ) {
	print LOG "You chose the option -MAX_FILES_TO_CHARGE_IN_THIS_RUN $MAX_FILES_TO_CHARGE_IN_THIS_RUN\n" unless $VERBOSE =~ m/^OFF$/i;
}


if ( defined $FILE_BLACKLISTED_ORGANISM_IDS && -e "$FILE_BLACKLISTED_ORGANISM_IDS" ) {

	open  ($in,  '<',  $FILE_BLACKLISTED_ORGANISM_IDS ) or do {
	    die_with_error_mssg("Can't read FILE_BLACKLISTED_ORGANISM_IDS file $FILE_BLACKLISTED_ORGANISM_IDS : $!");
	};	
	while( my $line = <$in> ) {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		if ( $line =~ m/^(\d+)$/ ) {
			my $organism_id_to_blacklist = $1;
			$HASH_BLACKLISTED_ORGANISM_IDS {$organism_id_to_blacklist} = undef ;
		}else{
			die_with_error_mssg("error parsing line\n$line\n in file $FILE_BLACKLISTED_ORGANISM_IDS.\n");
		}
	}
	close $in;
	print LOG scalar(keys %HASH_BLACKLISTED_ORGANISM_IDS)." element_id found to be blacklisted from file $FILE_BLACKLISTED_ORGANISM_IDS.\n" unless $VERBOSE =~ m/^OFF$/i;
}


# import alignment_param and other files in db
print LOG "\nStarting import files in db...\n" unless $VERBOSE =~ m/^OFF$/i;
$counter_loop = 0;
FOREACH_alignment_params_table_sql_file : foreach my $alignment_params_table_sql_file_IT (@alignment_params_table_sql_file) {

	$counter_loop++;

	my $master_molecule_id;
	my $sub_molecule_id;
	#my $core_file_name;
	my %ALIGN_PARAM_PRIMARY_ID_TO_KEEP = ();

	if ( $alignment_params_table_sql_file_IT =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table.sql$/ ) {
		$master_molecule_id = $1;
		$sub_molecule_id = $2;
	} else {
		die_with_error_mssg("could not parse alignment_params sql file $alignment_params_table_sql_file_IT.");
	}

	my $alignments_input_file_IT = "$SQL_INPUT_DIR/alignments/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_table.sql";
	my $alignment_pairs_input_file_IT = "$SQL_INPUT_DIR/alignment_pairs/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_pairs_table.sql";
	my $homologies_input_file_IT = "$SQL_INPUT_DIR/homologies/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_homologies_table.sql";
	my $trimed_alignment_params_table = "$SQL_INPUT_DIR/alignment_params/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_params_table_TRIMED.sql";
	my $associated_params_scores_algo_syntenies_sql_file = "$SQL_INPUT_DIR/params_scores_algo_syntenies.sql";
	my $associated_tandem_dups_table_sql_file = "$SQL_INPUT_DIR/tandem_dups/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_tandem_dups_table.sql";
	my $associated_isBranchedToAnotherSynteny_table_sql_file = "$SQL_INPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_isBranchedToAnotherSynteny_table.sql";
	my $associated_prot_fusion_table_sql_file = "$SQL_INPUT_DIR/protFusion/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_protFusion_table.sql";
	my $associated_closeBestMatchs_table_sql_file = "$SQL_INPUT_DIR/closeBestMatchs/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_closeBestMatchs_table.sql";

	my $IS_BLACKLISTED = undef;
	if ( exists $HASH_BLACKLISTED_ORGANISM_IDS {$master_molecule_id} || exists $HASH_BLACKLISTED_ORGANISM_IDS {$sub_molecule_id} ) {
		$IS_BLACKLISTED = 1;
	}

	if( ! -e "$alignments_input_file_IT" ) {
		die_with_error_mssg("could not associate file $alignments_input_file_IT.");
	}

	if ( $TABLE_ALIGNMENT_PARAMS_ONLY =~ m/^ON$/i ) {
		# do not need $alignment_pairs_input_file_IT and $homologies_input_file_IT files
	} elsif ( ! -e "$alignment_pairs_input_file_IT" || ! -e "$homologies_input_file_IT" ) {
		die_with_error_mssg("could not associate file $alignment_pairs_input_file_IT or $homologies_input_file_IT.");
	}


	if (defined $IS_BLACKLISTED) {
		print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : Do not import blacklisted master_molecule_id = $master_molecule_id and sub_molecule_id = $sub_molecule_id... \n" unless $VERBOSE =~ m/^OFF$/i;
	} else {

		print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : Starting import of master_molecule_id = $master_molecule_id and sub_molecule_id = $sub_molecule_id... \n" unless $VERBOSE =~ m/^OFF$/i;

		# import alignment_param file
		if ($TRIM_UNUSED_ALIGNMENT_PARAMS_DATA =~ m/^ON$/i) {

			open  ($in,  '<',  $alignments_input_file_IT ) or do {
			    die_with_error_mssg("Can't read alignments_input_file_IT file $alignments_input_file_IT : $!");
			};	
			while( my $line = <$in> ) {
				chomp($line);
				$line =~ s/^\s+//;
				$line =~ s/\s+$//;
				if ( $line eq "BEGIN WORK;" 
					|| $line eq "\\\." 
					|| $line eq "COMMIT WORK;" 
				) {
				    	# do nothing
				} elsif ( $line eq "COPY alignments \(alignment_id, alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_size_kb, s_size_kb, q_start, q_stop, s_start, s_stop\) FROM stdin;"
				){
				    	# do nothing
				} elsif ( $line =~ m/^\d+\t(\d+)\t\d+\t\d+\t[TF]\t\d+\t\d+\t\d+\t\d+\t\d+\t[\d\.]+\t[\d\.]+\t\d+\t\d+\t\d+\t\d+$/ ) {
					my $ALIGN_PARAM_PRIMARY_ID = $1;
					$ALIGN_PARAM_PRIMARY_ID_TO_KEEP {$ALIGN_PARAM_PRIMARY_ID} = undef ;
				}else{
					die_with_error_mssg("error parsing line\n$line\n in file $trimed_alignment_params_table.\n");
				}
			}
			close $in;

			open ( $in,  '<',  $alignment_params_table_sql_file_IT ) or do {
			    die_with_error_mssg("Can't read alignment_params_table_sql_file_IT file $alignment_params_table_sql_file_IT : $!");
			};
			#`$SiteConfig::CMDDIR/mkdir -p $SQL_INPUT_DIR/alignment_params/${molecule_type}_${master_molecule_id}/`;
			open ( $out, '>', "$trimed_alignment_params_table" ) or do {
			    die_with_error_mssg("Can't write $trimed_alignment_params_table : $!");
			};

			while( my $line = <$in> ) {
				chomp($line);
				$line =~ s/^\s+//;
				$line =~ s/\s+$//;
				if ( $line eq "BEGIN WORK;" 
					|| $line eq "\\\." 
					|| $line eq "COMMIT WORK;" 
				) {
					print $out "$line\n";
				} elsif ( $line eq "COPY alignment_params \(alignment_param_id, q_organism_id, q_element_id, s_organism_id, s_element_id\) FROM stdin;" ) {
					#do not store info on params_scores_algo_syntenies, has its own table
					print $out  "$line\n";
				} elsif ($line =~ m/^(\d+)\t\d+\t\d+\t\d+\t\d+$/ ) {
					my $alignment_param_id_IT = $1;
					if ( exists $ALIGN_PARAM_PRIMARY_ID_TO_KEEP {$alignment_param_id_IT} ) {
						print $out  "$line\n";
					}
				}else{
					die_with_error_mssg("error parsing line\n$line\n in file $alignment_params_table_sql_file_IT : does not match the main regex\n");
				}
			}
			close $out;
			close $in;
			$output_cmd = `psql -f $trimed_alignment_params_table -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
			if ( $? == -1 )
			{
				die_with_error_mssg("command psql -f $trimed_alignment_params_table -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!");
			}
			if($output_cmd eq ''
				|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/
			){
				print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $trimed_alignment_params_table.\n" unless $VERBOSE =~ m/^OFF$/i;
			}else{
				die_with_error_mssg("Error psql -f $trimed_alignment_params_table -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
		
			}

		} else {

			$output_cmd = `psql -f $alignment_params_table_sql_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
			if ( $? == -1 )
			{
				die_with_error_mssg("command psql -f $alignment_params_table_sql_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!");
			}
			if($output_cmd eq ''
				|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/
			){
				print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $alignment_params_table_sql_file_IT.\n" unless $VERBOSE =~ m/^OFF$/i;
			}else{
				die_with_error_mssg("Error psql -f $alignment_params_table_sql_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
		
			}
		}


		if ( $TABLE_ALIGNMENT_PARAMS_ONLY =~ m/^ON$/i ) {
			#do not insert other tables than alignment_params
			print LOG "Flag TABLE_ALIGNMENT_PARAMS_ONLY is $TABLE_ALIGNMENT_PARAMS_ONLY : do not insert other tables than alignment_params.\n" unless $VERBOSE =~ m/^OFF$/i;
		} else {

			# import alignments_input_file_IT
			$output_cmd = `psql -f $alignments_input_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
			if ( $? == -1 )
			{
				die_with_error_mssg("command psql -f $alignments_input_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!");
			}
			if($output_cmd eq ''
				|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/
			){
				print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $alignments_input_file_IT.\n" unless $VERBOSE =~ m/^OFF$/i;
			}else{
				die_with_error_mssg("Error psql -f $alignments_input_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
			}


			# import alignment_pairs_input_file_IT
			$output_cmd = `psql -f $alignment_pairs_input_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
			if ( $? == -1 )
			{
				die_with_error_mssg("command psql -f $alignment_pairs_input_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!");
			}
			if($output_cmd eq ''
				|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/
			){
				print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $alignment_pairs_input_file_IT.\n" unless $VERBOSE =~ m/^OFF$/i;
			}else{
				die_with_error_mssg("Error psql -f $alignment_pairs_input_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
			}



			# import homologies_input_file_IT
			$output_cmd = `psql -f $homologies_input_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
			if ( $? == -1 )
			{
				die("command output_cmd = `psql -f $homologies_input_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!\n");
			}
			if($output_cmd eq ''
				|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/g
			){
				print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $homologies_input_file_IT.\n" unless $VERBOSE =~ m/^OFF$/i;
			}else{
				die_with_error_mssg("Error psql -f $homologies_input_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
			}

			# import associated_params_scores_algo_syntenies_sql_file
			if( -e "$associated_params_scores_algo_syntenies_sql_file" ){

=pod
				my $in;
				open ( $in,  '<',  $associated_params_scores_algo_syntenies_sql_file ) or do {
				    die_with_error_mssg("Can't read params_scores_algo_syntenies_sql file $associated_params_scores_algo_syntenies_sql_file : $!");
				};

				my $ortho_score_FromFile = undef;
				my $homo_score_FromFile = undef;
				my $mismatch_penalty_FromFile = undef;
				my $gap_creation_penalty_FromFile = undef;
				my $max_gap_size_for_creation_penalty_FromFile = undef;
				my $gap_extension_penalty_FromFile = undef;
				my $min_align_size_FromFile = undef;
				my $min_score_FromFile = undef;
				my $orthologs_included_FromFile = undef;
				my $ortho_min_prot_frac_FromFile = undef;
				my $geneFusions_minPctLenghtProtThres_FromFile = undef;
				my $closeBestMatchs_minPctThres_FromFile = undef;
				my $tandemDups_minPctWithinBestPidThres_FromFile = undef;

				while( my $line = <$in> ) {
					chomp($line);
					$line =~ s/^\s+//;
					$line =~ s/\s+$//;
					if ( $line eq "BEGIN WORK;" 
						|| $line eq "\\\." 
						|| $line eq "COMMIT WORK;" 
					) {
						#do nothing
					} elsif ( $line eq "COPY params_scores_algo_syntenies (ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, max_gap_size_for_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac, geneFusions_minPctLenghtProtThres, closeBestMatchs_minPctThres, tandemDups_minPctWithinBestPidThres) FROM stdin;" ) {
						#do nothing
					} elsif ($line =~ m/^([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([TF])\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)$/ ) {
						$ortho_score_FromFile = $1;
						$homo_score_FromFile = $2;
						$mismatch_penalty_FromFile = $3;
						$gap_creation_penalty_FromFile = $4;
						$max_gap_size_for_creation_penalty_FromFile = $5;
						$gap_extension_penalty_FromFile = $6;
						$min_align_size_FromFile = $7;
						$min_score_FromFile = $8;
						$orthologs_included_FromFile = $9;
						$ortho_min_prot_frac_FromFile = $10;
						$geneFusions_minPctLenghtProtThres_FromFile = $11;
						$closeBestMatchs_minPctThres_FromFile = $12;
						$tandemDups_minPctWithinBestPidThres_FromFile = $13;
					}else{
						die_with_error_mssg("error parsing line\n$line\n in file $associated_params_scores_algo_syntenies_sql_file : does not match the main regex\n");
					}
				}
=cut

				my $sql_query = $ORIGAMI::dbh->prepare("SELECT ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac FROM params_scores_algo_syntenies");
				$sql_query->execute or die_with_error_mssg("Pb execute SELECT ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac FROM params_scores_algo_syntenies: $!");
				my $count_row_FromDB = 0;
				while ( my $res = $sql_query->fetchrow_hashref() ) {
					$count_row_FromDB++;
=pod
					my $ortho_score_FromDB = $res->{ortho_score};
					my $homo_score_FromDB = $res->{homo_score};
					my $mismatch_penalty_FromDB = $res->{mismatch_penalty};
					my $gap_creation_penalty_FromDB = $res->{gap_creation_penalty};
					my $gap_extension_penalty_FromDB = $res->{gap_extension_penalty};
					my $min_align_size_FromDB = $res->{min_align_size};
					my $min_score_FromDB = $res->{min_score};
					my $orthologs_included_FromDB = $res->{orthologs_included};
					my $ortho_min_prot_frac_FromDB = $res->{ortho_min_prot_frac};
=cut

				}
				if ($count_row_FromDB == 0) {
					#do insert row
					$output_cmd = `psql -f $associated_params_scores_algo_syntenies_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
					if ( $? == -1 )
					{
						die("command output_cmd = `psql -f $associated_params_scores_algo_syntenies_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!\n");
					}
					if($output_cmd eq ''
						|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/g
					){
						print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $associated_params_scores_algo_syntenies_sql_file.\n" unless $VERBOSE =~ m/^OFF$/i;
					}else{
						die_with_error_mssg("Error psql -f $associated_params_scores_algo_syntenies_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
					}
				} elsif ($count_row_FromDB == 1) {
					#ok already inserted, nothing to do
				} else {
					die_with_error_mssg("Error : more than 1 row inserted in table params_scores_algo_syntenies");
				}

			}


			if( -e "$associated_tandem_dups_table_sql_file" ){
				$output_cmd = `psql -f $associated_tandem_dups_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
				if ( $? == -1 )
				{
					die("command output_cmd = `psql -f $associated_tandem_dups_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!\n");
				}
				if($output_cmd eq ''
					|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/g
				){
					print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $associated_tandem_dups_table_sql_file.\n" unless $VERBOSE =~ m/^OFF$/i;
				}else{
					die_with_error_mssg("Error psql -f $associated_tandem_dups_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
				}
			}



			if( -e "$associated_isBranchedToAnotherSynteny_table_sql_file" ){
				$output_cmd = `psql -f $associated_isBranchedToAnotherSynteny_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
				if ( $? == -1 )
				{
					die("command output_cmd = `psql -f $associated_isBranchedToAnotherSynteny_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!\n");
				}
				if($output_cmd eq ''
					|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/g
				){
					print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $associated_isBranchedToAnotherSynteny_table_sql_file.\n" unless $VERBOSE =~ m/^OFF$/i;
				}else{
					die_with_error_mssg("Error psql -f $associated_isBranchedToAnotherSynteny_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
				}
			}


			if( -e "$associated_prot_fusion_table_sql_file" ){
				$output_cmd = `psql -f $associated_prot_fusion_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
				if ( $? == -1 )
				{
					die("command output_cmd = `psql -f $associated_prot_fusion_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!\n");
				}
				if($output_cmd eq ''
					|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/g
				){
					print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $associated_prot_fusion_table_sql_file.\n" unless $VERBOSE =~ m/^OFF$/i;
				}else{
					die_with_error_mssg("Error psql -f $associated_prot_fusion_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
				}
			}


			if( -e "$associated_closeBestMatchs_table_sql_file" ){
				$output_cmd = `psql -f $associated_closeBestMatchs_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
				if ( $? == -1 )
				{
					die("command output_cmd = `psql -f $associated_closeBestMatchs_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!\n");
				}
				if($output_cmd eq ''
					|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/g
				){
					print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : done import of file $associated_closeBestMatchs_table_sql_file.\n" unless $VERBOSE =~ m/^OFF$/i;
				}else{
					die_with_error_mssg("Error psql -f $associated_closeBestMatchs_table_sql_file -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
				}
			}
		} # else of if ( $TABLE_ALIGNMENT_PARAMS_ONLY =~ m/^ON$/i ) {
	} # blacklisted or not

	# gzip file
	`gzip $alignment_params_table_sql_file_IT`;
	if( -e "$trimed_alignment_params_table" ){
		`gzip $trimed_alignment_params_table`;
	}
	`gzip $alignments_input_file_IT`;
	if ( -e "$alignment_pairs_input_file_IT" ) {
		`gzip $alignment_pairs_input_file_IT`;
	}
	if ( -e "$homologies_input_file_IT" ) {
		`gzip $homologies_input_file_IT`;
	}
	if( -e "$associated_params_scores_algo_syntenies_sql_file" ){
		`gzip $associated_params_scores_algo_syntenies_sql_file`;
	}
	if( -e "$associated_tandem_dups_table_sql_file" ){
		`gzip $associated_tandem_dups_table_sql_file`;
	}
	if( -e "$associated_isBranchedToAnotherSynteny_table_sql_file" ){
		`gzip $associated_isBranchedToAnotherSynteny_table_sql_file`;
	}
	if( -e "$associated_prot_fusion_table_sql_file" ){
		`gzip $associated_prot_fusion_table_sql_file`;
	}
	if( -e "$associated_closeBestMatchs_table_sql_file" ){
		`gzip $associated_closeBestMatchs_table_sql_file`;
	}
	print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_file)." ) : Done dealing with master_molecule_id = $master_molecule_id and sub_molecule_id = $sub_molecule_id. \n" unless $VERBOSE =~ m/^OFF$/i;

}

print LOG "Finished importing files in db.\n\n" unless $VERBOSE =~ m/^OFF$/i;

#end of script
print LOG
"\n---------------------------------------------------\n\n\n Task_add_alignments_integrator_for_IDRIS.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/i;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);



