#!/usr/local/bin/perl

package Object_element;

use strict;
use roman2int;
use Carp qw(confess carp);

my $DO_NOT_PRINT_WARNING = "ON";

# modify also object fields
my @list_supported_qualifiers_conserved_within_similar_orga = ("isolation_source","country","collection_date","culture_collection","serovar","serotype","collected_by","isolate","host",
"organism",
"strain",
"sub_strain",
"altitude",
"lat_lon",
"sub_species");
my @list_supported_qualifiers_not_conserved_within_similar_orga = ("chromosome","plasmid");
my @list_supported_qualifiers = ();
push (@list_supported_qualifiers, @list_supported_qualifiers_conserved_within_similar_orga);
push (@list_supported_qualifiers, @list_supported_qualifiers_not_conserved_within_similar_orga);
my @list_supported_element_types = ("complete_genome","chromosome","plasmid","draft","synthetic","transfer_element","satellite","genomic_region","contig","scaffold","probable_contig", "undef", "genomic", "scaffold_contig","contig_scaffold","supercontig","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid");
my %compatible_element_types = (
        "complete_genome" => ["plasmid","transfer_element","extrachrom"],
        "chromosome" => ["chromosome","plasmid","transfer_element","extrachrom"],
        "plasmid" => ["complete_genome","chromosome","plasmid","draft","synthetic","transfer_element","satellite","genomic_region","contig","scaffold","probable_contig","scaffold_contig","contig_scaffold",
"supercontig","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid"],
        "draft" => ["draft","plasmid","transfer_element","extrachrom"],
        "synthetic" => ["plasmid","transfer_element","extrachrom"],
        "transfer_element" => ["complete_genome","chromosome","plasmid","draft","synthetic","transfer_element","satellite","genomic_region","contig","scaffold","probable_contig","scaffold_contig","contig_scaffold",
"supercontig","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid"],
        "satellite" => ["satellite","plasmid","transfer_element","extrachrom"],
        "genomic_region" => ["genomic_region","plasmid","transfer_element","extrachrom"],
        "contig" => ["contig","plasmid","transfer_element","supercontig","extrachrom","scaffold","scaffold_contig","contig_scaffold","supercontig","supercontig_scaffold","scaffold_supercontig","contig_scaffold_plasmid"],
        "scaffold" => ["scaffold","plasmid","transfer_element","scaffold_contig","contig_scaffold","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid","contig"],
        "probable_contig" => ["probable_contig","plasmid","transfer_element","extrachrom"],
	"genomic" => ["genomic"],
        "scaffold_contig" => ["scaffold","plasmid","transfer_element","scaffold_contig","contig_scaffold","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid","contig"],
        "contig_scaffold" => ["scaffold","plasmid","transfer_element","contig_scaffold","contig_scaffold","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid","contig"],
	"supercontig" => ["contig","plasmid","transfer_element","supercontig","extrachrom","contig"],
	"supercontig_scaffold" => ["scaffold","plasmid","transfer_element","scaffold_contig","contig_scaffold","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid","contig"],
	"scaffold_supercontig" => ["scaffold","plasmid","transfer_element","scaffold_contig","contig_scaffold","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid","contig"],
	"extrachrom" => ["complete_genome","chromosome","plasmid","draft","synthetic","transfer_element","satellite","genomic_region","contig","scaffold","probable_contig","scaffold_contig","contig_scaffold",
"supercontig","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid"],
	"contig_scaffold_plasmid" => ["scaffold","plasmid","transfer_element","scaffold_contig","contig_scaffold","supercontig_scaffold","scaffold_supercontig","extrachrom","contig_scaffold_plasmid","contig"],
    );

sub new {
    my $class = shift;

    my $self = {

	_organisms_organism_id => "",
	_organisms_taxon_id => "",
	_elements_element_id => "",
	_elements_accession => "",
	_elements_type => "",
	_elements_description => "",
	_elements_date_seq => "",

	# qualifiers
	# modify also sublist_supported_qualifiers
	# conserved qualifiers
	_micado_qualifiers_isolation_source => "",
	_micado_qualifiers_country => "",
	_micado_qualifiers_collection_date => "",
	_micado_qualifiers_culture_collection => "",
	_micado_qualifiers_serovar => "",
	_micado_qualifiers_serotype => "",
	_micado_qualifiers_collected_by => "",
	_micado_qualifiers_isolate => "",
	_micado_qualifiers_host => "",
	_micado_qualifiers_organism => "",
	_micado_qualifiers_strain => "",
	_micado_qualifiers_sub_strain => "",
	_micado_qualifiers_altitude => "",
	_micado_qualifiers_lat_lon => "",
	_micado_qualifiers_sub_species => "",
	# not conserved qualifiers
	_micado_qualifiers_chromosome => "",
	_micado_qualifiers_plasmid => "",

	# special
	_suggested_type => "",
	_chromosome_number => "",
	_accession_prefix_word => "",
	_accession_postfix_digit => "",
	_suggested_organism_id => "",
    };

    bless $self, $class;
    return $self;
}


sub init {

    my $class = shift;
    my $organism_id_IT = shift;
    my $taxon_id_IT = shift;
    my $element_id_IT = shift;
    my $accession_IT = shift;
    my $type_IT = shift;
    my $description_IT = shift;
    my $date_seq_IT = shift;

    set_organism_id($class, $organism_id_IT);
    set_taxon_id($class, $taxon_id_IT);
    set_element_id($class, $element_id_IT);
    set_accession($class, $accession_IT);
    set_type($class, $type_IT);
    set_description($class, $description_IT);
    set_date_seq($class, $date_seq_IT);

}


# _organisms_organism_id
sub set_organism_id {
    my ( $self, $organism_id ) = @_;
    if ( defined($organism_id) ) {
	if ($organism_id =~ /^\d+$/) {
		$self->{_organisms_organism_id} = $organism_id;
		return $self->{_organisms_organism_id};
	} else {
		confess "ERROR in set_organism_id : could not parse $organism_id\n";
	}
    } else {
	confess "ERROR in set_organism_id : undefined organism_id\n";
    }
}

sub get_organism_id {
    my( $self ) = @_;
    return $self->{_organisms_organism_id};
}

# _organisms_taxon_id ; taxon_id
sub set_taxon_id {
    my ( $self, $taxon_id ) = @_;
    $self->{_organisms_taxon_id} = $taxon_id if defined($taxon_id);
    return $self->{_organisms_taxon_id};
}

sub get_taxon_id {
    my( $self ) = @_;
    return $self->{_organisms_taxon_id};
}

# _elements_element_id ; element_id
sub set_element_id {
    my ( $self, $element_id ) = @_;
    $self->{_elements_element_id} = $element_id if defined($element_id);
    return $self->{_elements_element_id};
}

sub get_element_id {
    my( $self ) = @_;
    return $self->{_elements_element_id};
}

# _elements_accession ; accession
sub set_accession {
	my ( $self, $accession ) = @_;

	if (defined($accession)) {
		if ($accession =~ /^(.+?)(\d+).*?$/) {
			# ok parse
			$self->{_accession_prefix_word} = $1;
			$self->{_accession_postfix_digit} = $2;
			$self->{_elements_accession} = $accession;
			return $self->{_elements_accession};
		} else {
			confess "ERROR in set_accession : could not parse $accession\n";
		}
	} else {
		confess "ERROR in set_accession : undefined accession $accession\n";
	}
}

sub get_accession {
    my( $self ) = @_;
    return $self->{_elements_accession};
}

# _elements_type ; type
sub set_type {
    my ( $self, $type ) = @_;
    if ( ! defined $type || $type eq "") {
	    $type = "undef";
    }
    if ( check_that_element_type_is_supported($type) == 1 ){
	    $self->{_elements_type} = $type if defined($type);
	    return $self->{_elements_type};
    } else {
	confess "ERROR in Object_element.pm -> set_type : unsupported type $type\n";
    }
}

sub get_type {
    my( $self ) = @_;
    return $self->{_elements_type};
}

# _elements_description ; description
sub set_description {
    my ( $self, $description ) = @_;
    $self->{_elements_description} = $description if defined($description);
    return $self->{_elements_description};
}

sub get_description {
    my( $self ) = @_;
    return $self->{_elements_description};
}

# _elements_date_seq ; date_seq
sub set_date_seq {
    my ( $self, $date_seq ) = @_;

    if ( defined($date_seq) ) {
	$self->{_elements_date_seq} = $date_seq;
	return $self->{_elements_date_seq};
=pod
	if ($date_seq =~ /^\d+-\w{3}-\d{4}$/) {
		$self->{_elements_date_seq} = $date_seq;
		return $self->{_elements_date_seq};
	} else {
		confess "ERROR in set_date_seq : could not parse $date_seq\n";
	}
=cut
    } else {
	confess "ERROR in set_date_seq : undefined date_seq\n";
    }

}

sub get_date_seq {
    my( $self ) = @_;
    return $self->{_elements_date_seq};
}


sub set_qualifiers {
    my ( $self, $type, $qualifier ) = @_;
    if ( check_that_qualifier_is_supported($type) == 1 ){
	    my $LONG = "_micado_qualifiers_".$type;
	    $self->{$LONG} = $qualifier if defined($qualifier);
	    return $self->{$LONG};
    } else {
	confess "ERROR in Object_element.pm -> set_qualifiers : unsupported type ".$type;
    }

}

sub get_qualifiers {
    my( $self, $type ) = @_;
    if ( check_that_qualifier_is_supported($type) == 1 ){
	    my $LONG = "_micado_qualifiers_".$type;
	    return $self->{$LONG};
    } else {
	confess "ERROR in Object_element.pm -> get_qualifiers : unsupported type ".$type;
    }
}



sub set_chromosome_number {
    my ( $self, $chromosome_number ) = @_;
    if ( defined($chromosome_number) ) {

	if ($chromosome_number =~ /^\d+$/i ) {
		#ok only digit
	} elsif ($chromosome_number =~ /^[IVXLCDM]+$/i ) {
		my $arabic_convert = roman2int->arabic($chromosome_number);
		$chromosome_number = $arabic_convert;
	} elsif ($chromosome_number =~ /^main$/i ) {
		#ok 
	} elsif ($chromosome_number =~ /^circular$/i ) {
		#ok 
	} elsif ($chromosome_number =~ /^linear$/i ) {
		#ok 
	} elsif ($chromosome_number =~ /^chromosome$/i ) {
		#ok 
	} else {
		print "\n** WARNING unconventional chromosome_number : $chromosome_number for accnum ".$self->get_accession()." ; ".$self->get_description()."\n" unless ( $DO_NOT_PRINT_WARNING =~ m/^ON$/i );;
	}
	

	if ( $self->{_chromosome_number} ne "" ) {
		if ($self->{_chromosome_number} eq $chromosome_number) {
			# ok similar, do nothing
		} else {
			# error
			confess "ERROR in Object_element.pm -> set_chromosome_number : conflicting chromosome_number : $chromosome_number different from ".$self->{_chromosome_number}."\n";
		}
	} else {
		$self->{_chromosome_number} = $chromosome_number;
		return $self->{_chromosome_number};
	}
    } else {
	    return $self->{_chromosome_number};
    }
}

sub get_chromosome_number {
    my ( $self ) = @_;
    return $self->{_chromosome_number};
}


#_accession_prefix_word
sub set_accession_prefix_word {
    my ( $self, $accession_prefix_word ) = @_;
    $self->{_accession_prefix_word} = $accession_prefix_word if defined($accession_prefix_word);
    return $self->{_accession_prefix_word};
}

sub get_accession_prefix_word {
    my( $self ) = @_;
    return $self->{_accession_prefix_word};
}


#_accession_postfix_digit
sub set_accession_postfix_digit {
    my ( $self, $accession_postfix_digit ) = @_;
    $self->{_accession_postfix_digit} = $accession_postfix_digit if defined($accession_postfix_digit);
    return $self->{_accession_postfix_digit};
}

sub get_accession_postfix_digit {
    my( $self ) = @_;
    return $self->{_accession_postfix_digit};
}


# return static

sub get_list_supported_qualifiers {
    return @list_supported_qualifiers;
}

sub get_list_supported_element_types {
    return @list_supported_element_types;
}

sub get_list_supported_qualifiers_conserved_within_similar_orga {
    return @list_supported_qualifiers_conserved_within_similar_orga;
}

sub get_list_supported_qualifiers_not_conserved_within_similar_orga {
    return @list_supported_qualifiers_not_conserved_within_similar_orga;
}

sub get_hash_compatible_element_types {
    return %compatible_element_types;
}

# special

sub get_qualifiers_conserved_within_similar_orga_as_string {
	my( $self ) = @_;
	my $string_to_return = "";
	foreach my $supported_qualifiers_conserved_within_similar_orga_IT (@list_supported_qualifiers_conserved_within_similar_orga) { 
		$string_to_return = $string_to_return."_".$self->get_qualifiers($supported_qualifiers_conserved_within_similar_orga_IT);
	}
	return $string_to_return;
}

sub check_that_qualifier_is_supported {
	my( $type_qual ) = @_;
	foreach my $supported_qualifiers_IT (@list_supported_qualifiers) { 
		if ($type_qual eq $supported_qualifiers_IT){
			return 1;
		}
	}
	return 0;
}

sub check_that_element_type_is_supported {
	my( $element_types ) = @_;
	foreach my $element_types_IT (@list_supported_element_types) { 
		if ($element_types eq $element_types_IT){
			return 1;
		}
	}
	return 0;
}

# _suggested_type ; suggested_type
sub set_suggested_type {
    my ( $self, $suggested_type ) = @_;
    $self->{_suggested_type} = $suggested_type if defined($suggested_type);
    return $self->{_suggested_type};
}

sub get_suggested_type {
	my( $self ) = @_;
	if ($self->{_suggested_type} eq "") {
		return $self->{_elements_type};
	} else {
		return $self->{_suggested_type};
	}
}

sub set_suggested_organism_id {
    my ( $self, $suggested_organism_id ) = @_;
    $self->{_suggested_organism_id} = $suggested_organism_id if defined($suggested_organism_id);
    return $self->{_suggested_organism_id};
}

sub get_suggested_organism_id {
	my( $self ) = @_;
	if ($self->{_suggested_organism_id} eq "") {
		return $self->{_organisms_organism_id};
	} else {
		return $self->{_suggested_organism_id};
	}
}



1;

