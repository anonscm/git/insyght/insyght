#!/usr/local/bin/perl

# Script de construction et mise à jour de ORIGAMI
# Permet d'intégrer les fichiers .sql généré par generate_sorted_list_comp_elements.pl dans la table q_element_id_2_sorted_list_comp_orga_whole
# Date : 06/2015
#args :
# -DO_abundance_homologs
# -DO_synteny_score
# -DO_alignemnt_score
# -DO_abundance_homo_annotations
# -DO_ALL
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) :  Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-...), Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr),
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 
use strict;
use DBI;
use SiteConfig;
use ORIGAMI;


# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/compute/integrate_sorted_list_comp_orga_whole.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/integrate_sorted_list_comp_orga_whole.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/integrate_sorted_list_comp_orga_whole.error";
my $output_backtick = "";
my $VERBOSE = "ON";
my $DO_abundance_homologs = 0;
my $DO_synteny_score = 0;
my $DO_alignemnt_score = 0;
my $DO_abundance_homo_annotations = 0;
my $INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sorted_list_comp_orga_whole";
my @al_files_to_treat_abundance_homologs = ();
my @al_files_to_treat_synteny_score = ();
my @al_files_to_treat_alignemnt_score = ();


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


#args
foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-DO_abundance_homologs$/ ) {
		$DO_abundance_homologs = 1;
	} elsif ( $ARGV[$argnum] =~ m/^-DO_synteny_score$/ ) {
		$DO_synteny_score = 1;
	} elsif ( $ARGV[$argnum] =~ m/^-DO_alignemnt_score$/ ) {
		$DO_alignemnt_score = 1;
	} elsif ( $ARGV[$argnum] =~ m/^-DO_abundance_homo_annotations$/ ) {
		$DO_abundance_homo_annotations = 1;
	} elsif ( $ARGV[$argnum] =~ m/^-DO_ALL$/ ) {
		$DO_abundance_homologs = 1;
		$DO_synteny_score = 1;
		$DO_alignemnt_score = 1;
		$DO_abundance_homo_annotations = 1;
	}
	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl integrate_sorted_list_comp_orga_whole.pl -VERBOSE {ON, OFF}");
		}

	}
	if ( $ARGV[$argnum] =~ m/^-INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -INPUT_DIR argument ; usage : perl integrate_sorted_list_comp_orga_whole.pl  -INPUT_DIR {PATH_TO_DIRECTORY}");
		}
	}
}


# on créé le répertoire pour les fichiers de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/compute`;
# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG "\n\n***Starting integrate_sorted_list_comp_orga_whole.pl at ",scalar(localtime),"\n\n" unless $VERBOSE =~ m/^OFF$/;

# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}

#check attribute number_fragment_hit is in table homologies
$ORIGAMI::dbh->selectall_arrayref( "SELECT element_id, abundance_homologs, synteny_score, alignemnt_score, abundance_homo_annotations FROM q_element_id_2_sorted_list_comp_orga_whole LIMIT 1") or die_with_error_mssg ("no columns element_id, abundance_homologs, synteny_score text, alignemnt_score, abundance_homo_annotations in table q_element_id_2_sorted_list_comp_orga_whole");


# truncate table q_element_id_2_sorted_list_comp_orga_whole
#my $sth = $ORIGAMI::dbh->prepare("TRUNCATE TABLE q_element_id_2_sorted_list_comp_orga_whole");
#$sth->execute();

sub check_if_insert_or_update {
	my ($file_to_check, $column_to_check) = @_;
	#print LOG "\tRunning check_if_insert_or_update\n" unless $VERBOSE =~ m/^OFF$/;
	open( FILE_READ, "<$file_to_check" ) or die_with_error_mssg("Can not open $file_to_check");
	open( FILE_WRITE, ">$file_to_check.checked") or die_with_error_mssg("Can not open $file_to_check.checked");
	while ( my $line = <FILE_READ> ) {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		if ( $line =~ m/^INSERT INTO q_element_id_2_sorted_list_comp_orga_whole \(element_id,$column_to_check\) VALUES \((\d+),(\'.+\')\)\s*;$/ ) {
			my $q_elet_id = $1;
			my $value = $2;
			my $query = "SELECT COUNT(*) FROM q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = ?";
			my $sth = $ORIGAMI::dbh->prepare($query);
			$sth->execute($q_elet_id);
			my $numRows = $sth->fetchrow_arrayref()->[0];
			if ($numRows == 0) {
				#ok, should be insert query, do not change
				#print LOG "q_elet_id = $q_elet_id => ok, should be insert query, do not change\n";
				print FILE_WRITE "$line\n";
			} elsif ($numRows == 1) {
				#modify, should be update query
				#print LOG "q_elet_id = $q_elet_id => modify, should be update query\n";
				print FILE_WRITE "UPDATE q_element_id_2_sorted_list_comp_orga_whole SET $column_to_check = $value WHERE element_id = $q_elet_id;\n";
			} else {
				die_with_error_mssg("ERROR check_if_insert_or_update : more than 1 count retrieve for query:\nSELECT COUNT(*) FROM q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = $q_elet_id");
			}
		} elsif ( $line =~ m/^INSERT INTO q_element_id_2_sorted_list_comp_orga_whole \(element_id,$column_to_check\) VALUES \(\d+,\'\'\)\s*;$/ ) {
			#empty line, do not print
		} elsif ( $line =~ m/^BEGIN WORK;$/ || $line =~ m/^COMMIT WORK;$/ ) {
			#ok header and footer
			print FILE_WRITE "$line\n";
		} else {
			die_with_error_mssg("ERROR parsing file $file_to_check : the line\n$line\ncould not be parsed.");
		}
	}
	close (FILE_READ);
	close (FILE_WRITE);
	#print LOG "\tDone check_if_insert_or_update\n" unless $VERBOSE =~ m/^OFF$/;
}



#todo
my $count_gz_files = 0;
sub sub_list_sql_files_recursively {
    my ( $path, $type_criteria ) = @_;
    #my $path = shift;
    opendir (DIR, $path) or die_with_error_mssg("Unable to open $path: $!");
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_sql_files_recursively ($_);
# accessions articles comments dna_loc dna_seq features keywords locations prot_feat qualifiers sequences elements genes
# %add_accessions_table_sql_files
        #} elsif ($_ =~ m/^.*\/(.+)\/(accessions|articles|comments|dna_loc|dna_seq|features|keywords|locations|prot_feat|qualifiers|sequences|elements|genes)\.sql$/ ) {
        } elsif ( $_ =~ m/^.+\/abundance_homologs.*\.sql$/ ) {
		if ($type_criteria eq "abundance_homologs") {
			push ( @al_files_to_treat_abundance_homologs, $_ ) ;
		} elsif ($type_criteria eq "synteny_score") {
			#
		} elsif ($type_criteria eq "alignemnt_score") {
			#
		} else {
			die_with_error_mssg("ERROR sub_list_sql_files_recursively unrecognized type_criteria $type_criteria.");
		}
        } elsif ( $_ =~ m/^.+\/abundance_homologs.*\.sql\.gz$/ ) {
		if ($type_criteria eq "abundance_homologs") {
			$count_gz_files++;
		} elsif ($type_criteria eq "synteny_score") {
			#
		} elsif ($type_criteria eq "alignemnt_score") {
			#
		} else {
			die_with_error_mssg("ERROR sub_list_sql_files_recursively unrecognized type_criteria $type_criteria.");
		}
        } elsif ( $_ =~ m/^.+\/synteny_score.*\.sql$/ ) {
		if ($type_criteria eq "abundance_homologs") {
			#push ( @al_files_to_treat_abundance_homologs, $_ ) ;
		} elsif ($type_criteria eq "synteny_score") {
			push ( @al_files_to_treat_synteny_score, $_ ) ;
		} elsif ($type_criteria eq "alignemnt_score") {
			#push ( @al_files_to_treat_alignemnt_score, $_ ) ;
		} else {
			die_with_error_mssg("ERROR sub_list_sql_files_recursively unrecognized type_criteria $type_criteria.");
		}

        } elsif ( $_ =~ m/^.+\/synteny_score.*\.sql\.gz$/ ) {
		if ($type_criteria eq "abundance_homologs") {
			#
		} elsif ($type_criteria eq "synteny_score") {
			$count_gz_files++;
		} elsif ($type_criteria eq "alignemnt_score") {
			#
		} else {
			die_with_error_mssg("ERROR sub_list_sql_files_recursively unrecognized type_criteria $type_criteria.");
		}
	} elsif ( $_ =~ m/^.+\/alignemnt_score.*\.sql$/ ) {
		if ($type_criteria eq "abundance_homologs") {
			#push ( @al_files_to_treat_abundance_homologs, $_ ) ;
		} elsif ($type_criteria eq "synteny_score") {
			#push ( @al_files_to_treat_synteny_score, $_ ) ;
		} elsif ($type_criteria eq "alignemnt_score") {
			push ( @al_files_to_treat_alignemnt_score, $_ ) ;
		} else {
			die_with_error_mssg("ERROR sub_list_sql_files_recursively unrecognized type_criteria $type_criteria.");
		}

        } elsif ( $_ =~ m/^.+\/alignemnt_score.*\.sql\.gz$/ ) {
		if ($type_criteria eq "abundance_homologs") {
			#
		} elsif ($type_criteria eq "synteny_score") {
			#
		} elsif ($type_criteria eq "alignemnt_score") {
			$count_gz_files++;
		} else {
			die_with_error_mssg("ERROR sub_list_sql_files_recursively unrecognized type_criteria $type_criteria.");
		}
	} elsif ( $_ =~ m/^.+\/abundance_homo_annotations.*\.sql$/ ) {
		if ($type_criteria eq "abundance_homologs") {
			#push ( @al_files_to_treat_abundance_homologs, $_ ) ;
		} elsif ($type_criteria eq "synteny_score") {
			#push ( @al_files_to_treat_synteny_score, $_ ) ;
		} elsif ($type_criteria eq "alignemnt_score") {
			#push ( @al_files_to_treat_alignemnt_score, $_ ) ;
		} else {
			die_with_error_mssg("ERROR sub_list_sql_files_recursively unrecognized type_criteria $type_criteria.");
		}

        } elsif ( $_ =~ m/^.+\/abundance_homo_annotations.*\.sql\.gz$/ ) {
		if ($type_criteria eq "abundance_homologs") {
			#
		} elsif ($type_criteria eq "synteny_score") {
			#
		} elsif ($type_criteria eq "alignemnt_score") {
			#
		} else {
			die_with_error_mssg("ERROR sub_list_sql_files_recursively unrecognized type_criteria $type_criteria.");
		}
	} elsif ($_ =~ m/.sql.checked.gz$/ ) {
		#files from a previous aborted run
	} elsif ($_ =~ m/.sql.checked$/ ) {
		#files from a previous aborted run
	} elsif ($_ =~ m/~$/ ) {
		#tmp files to ignore
        } else {
            die_with_error_mssg("Error in sub_list_sql_files_recursively : the file $_ does not match the appropriate regex with type criteria $type_criteria");
        }
    }
}


#liste file to insert
if ($DO_abundance_homologs == 1) {
	$count_gz_files = 0;
	sub_list_sql_files_recursively($INPUT_DIR, "abundance_homologs");
	print LOG scalar(@al_files_to_treat_abundance_homologs)." abundance homologs file(s) have been scheduled for integration.\n" unless $VERBOSE =~ m/^OFF$/i;
	print LOG "$count_gz_files abundance homologs sql.gz file(s) have been found and skipped.\n" unless $VERBOSE =~ m/^OFF$/i;
}
if ($DO_synteny_score == 1) {
	$count_gz_files = 0;
	sub_list_sql_files_recursively($INPUT_DIR, "synteny_score");
	print LOG scalar(@al_files_to_treat_synteny_score)." synteny score file(s) have been scheduled for integration.\n" unless $VERBOSE =~ m/^OFF$/i;
	print LOG "$count_gz_files synteny score sql.gz file(s) have been found and skipped.\n" unless $VERBOSE =~ m/^OFF$/i;
}
if ($DO_alignemnt_score == 1) {
	$count_gz_files = 0;
	sub_list_sql_files_recursively($INPUT_DIR, "alignemnt_score");
	print LOG scalar(@al_files_to_treat_alignemnt_score)." alignemnt score file(s) have been scheduled for integration.\n" unless $VERBOSE =~ m/^OFF$/i;
	print LOG "$count_gz_files alignemnt score sql.gz file(s) have been found and skipped.\n" unless $VERBOSE =~ m/^OFF$/i;
}
if ($DO_abundance_homo_annotations == 1) {
	print LOG "\tabundance_homo_annotations is not yet implemented.\n" unless $VERBOSE =~ m/^OFF$/;
	#run_integration_of_sql_file("abundance_homo_annotations");
}


my $total_files_to_integrate = scalar(@al_files_to_treat_abundance_homologs) + scalar(@al_files_to_treat_synteny_score) + scalar(@al_files_to_treat_alignemnt_score) ;


my $counter_run_integration_of_sql_file = 0;
sub run_integration_of_sql_file {
	my ( $file_to_treat_IT, $type_criteria ) = @_;
	$counter_run_integration_of_sql_file++;
	if ( -e "$file_to_treat_IT" ){
		print LOG "( $counter_run_integration_of_sql_file / $total_files_to_integrate ) : Starting integration of file $file_to_treat_IT\n" unless $VERBOSE =~ m/^OFF$/;

		check_if_insert_or_update($file_to_treat_IT, $type_criteria);
		#integrate sql file
		#print LOG "( $counter_run_integration_of_sql_file / $total_files_to_integrate ) : Running integrate sql file\n" unless $VERBOSE =~ m/^OFF$/;
		if ( -e "$file_to_treat_IT.checked" ) {
			my $psql_out = `psql -f $file_to_treat_IT.checked -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname`;
			if ($psql_out eq ''
				|| $psql_out  =~ m/^BEGIN\s*[UPDATEINSERT\s\d\n]*COMMIT\s*$/
			) {
				#ok went well
				`gzip -f $file_to_treat_IT.checked`;
				`gzip -f $file_to_treat_IT`;
			} else {
				die_with_error_mssg("ERROR command\npsql -f $file_to_treat_IT.checked -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname\nreturned an error :\n$psql_out");
			}
		} else {
			die_with_error_mssg("ERROR run_integration_of_sql_file : file $file_to_treat_IT.checked does not exists.");
		}
		print LOG "( $counter_run_integration_of_sql_file / $total_files_to_integrate ) : Done integration of file $file_to_treat_IT\n" unless $VERBOSE =~ m/^OFF$/;
	} else {
		die_with_error_mssg("ERROR run_integration_of_sql_file : the file $file_to_treat_IT was not found.");
	}
}

if ($DO_abundance_homologs == 1) {
	foreach my $file_to_treat_IT ( @al_files_to_treat_abundance_homologs ) {
		run_integration_of_sql_file($file_to_treat_IT, "abundance_homologs");
	}
}
if ($DO_synteny_score == 1) {
	foreach my $file_to_treat_IT ( @al_files_to_treat_synteny_score ) {
		run_integration_of_sql_file($file_to_treat_IT, "synteny_score");
	}
}
if ($DO_alignemnt_score == 1) {
	foreach my $file_to_treat_IT ( @al_files_to_treat_alignemnt_score ) {
		run_integration_of_sql_file($file_to_treat_IT, "alignemnt_score");
	}
}
if ($DO_abundance_homo_annotations == 1) {
	#print LOG "\tabundance_homo_annotations is not yet implemented.\n" unless $VERBOSE =~ m/^OFF$/;
	#run_integration_of_sql_file("abundance_homo_annotations");
}


#end of script
print LOG
"\n---------------------------------------------------\n\n\nintegrate_sorted_list_comp_orga_whole.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);







