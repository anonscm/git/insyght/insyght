#!/usr/local/bin/perl
#
# perl count_inserted_data.pl
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2016)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use SiteConfig;
use ORIGAMI;


# whole script scoped variable
my $st;
my @list_tables_to_count = (
"ncbi_taxonomy_tree",
#"taxo_names",
"qualifiers",
"sequences",
"features",
"alignment_pairs",
"alignment_params",
"alignments",
"elements",
"genes",
"homologies",
"organisms",
"q_element_id_2_sorted_list_comp_orga_whole",
"params_scores_algo_syntenies",
"close_best_match",
"prot_fusion",
"isBranchedToAnotherSynteny",
"tandem_dups"
);


# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log 
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/count_inserted_data.log" );
#print LOG "\n---------------------------------------------------\n count_inserted_data.pl started at :", scalar(localtime), "\n\n\n";


foreach my $table_to_count (@list_tables_to_count) {

	#print "\n** VACUUMing table $table_to_count\n";
	$st = $ORIGAMI::dbh->prepare("VACUUM $table_to_count");
	$st->execute or die("Pb execute $!");

	print "\n\t\tCount table $table_to_count";
	# fast but not accurate $st = $ORIGAMI::dbh->prepare("SELECT reltuples FROM pg_class WHERE relname=\'$table_to_count\'");
	$st = $ORIGAMI::dbh->prepare("SELECT count(*) FROM $table_to_count");
	$st->execute or die("Pb execute $!");
	while ( my @row = $st->fetchrow_array(  ) ) {
		my ($count_IT) = @row;
		print "\t=> $count_IT tuples";
	}
#	while ( my $res = $st->fetchrow_hashref() ) {
#		my $count_IT = $res->{COUNT};
#		print LOG "\t=> $count_IT tuples\n";
#	}

}


# count special

print "\n\t\tCount number of new organisms with genes";
$st = $ORIGAMI::dbh->prepare("select count(*) from (select organisms.organism_id, count(genes.gene_id) FROM organisms, genes WHERE genes.organism_id = organisms.organism_id AND organisms.organism_id NOT IN (select distinct q_organism_id FROM alignment_params) GROUP BY organisms.organism_id ORDER BY count(genes.gene_id) DESC) as foo");
$st->execute or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	my $count_IT = $res->{count};
	print "\t=> $count_IT tuples";
}


print "\n\t\tCount number of previously inserted organisms with genes";
$st = $ORIGAMI::dbh->prepare("select count(*) from (select organisms.organism_id, count(genes.gene_id) FROM organisms, genes WHERE genes.organism_id = organisms.organism_id AND organisms.organism_id IN (select distinct q_organism_id FROM alignment_params) GROUP BY organisms.organism_id ORDER BY count(genes.gene_id) DESC) as foo");
$st->execute or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	my $count_IT = $res->{count};
	print "\t=> $count_IT tuples";
}



print "\n\t\tCount number of total number of organisms with genes";
$st = $ORIGAMI::dbh->prepare("select count(*) from (select organisms.organism_id, count(genes.gene_id) FROM organisms, genes WHERE genes.organism_id = organisms.organism_id GROUP BY organisms.organism_id ORDER BY count(genes.gene_id) DESC) as foo");
$st->execute or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	my $count_IT = $res->{count};
	print "\t=> $count_IT tuples";
}


print "\n";

#end of script
#print LOG "\n---------------------------------------------------\n\n\n count_inserted_data.pl successfully completed at :", scalar(localtime), "\n\n---------------------------------------------------\n";
#close(LOG);





