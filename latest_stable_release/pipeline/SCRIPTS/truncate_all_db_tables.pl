#!/usr/local/bin/perl
#
# truncate_all_db_tables.pl
#
# Pipeline origami Copyright - INRA - 2012-2025
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#




use strict;
use SiteConfig;
use ORIGAMI;


# whole script scoped variable
my $VERBOSE = "ON";
my $sql_query = "";

foreach my $argnum ( 0 .. $#ARGV ) {

	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			print "incorrect -VERBOSE argument ; usage : perl truncate_all_db_tables.pl -VERBOSE {ON, OFF}";
			die "incorrect -VERBOSE argument ; usage : perl truncate_all_db_tables.pl -VERBOSE {ON, OFF}";
		}

	}
}


# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/truncate_all_db_tables`;
open( LOG,"> $SiteConfig::LOGDIR/truncate_all_db_tables/truncate_all_db_tables.log");
#open( LOG,"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/truncate_all_db_tables/truncate_all_db_tables.log");


print LOG
"---------------------------------------------------\n truncate_all_db_tables.pl started at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/i;

$sql_query = $ORIGAMI::dbh->prepare("SET client_min_messages TO WARNING");
$sql_query->execute or die("SET client_min_messages TO WARNING");

$sql_query = $ORIGAMI::dbh->prepare("SELECT truncate_tables_public('".$ORIGAMI::user."')");
$sql_query->execute or die("SELECT truncate_tables_public('".$ORIGAMI::user."')");

$sql_query = $ORIGAMI::dbh->prepare("SELECT truncate_tables_micado('".$ORIGAMI::user."')");
$sql_query->execute or die("SELECT truncate_tables_micado('".$ORIGAMI::user."')");

print `perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/count_inserted_data.pl`."\n";

#end of script
print LOG
"---------------------------------------------------\n\n truncate_all_db_tables.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/i;
close(LOG);

