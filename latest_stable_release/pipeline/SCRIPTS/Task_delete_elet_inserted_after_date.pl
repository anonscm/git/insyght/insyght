#!/usr/local/bin/perl
#
# perl Task_delete_elet_inserted_after_date.pl -DEL_AFTER_DATE 2014-11-12
#
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2018)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use SiteConfig;
use ORIGAMI;
use lib '$SiteConfig::SCRIPTSDIR';


# on créé le répertoire pour les fichiers de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Delete_entries`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open(LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Delete_entries/Task_delete_elet_inserted_after_date.log");
print LOG "***Starting Task_delete_elet_inserted_after_date at ", scalar(localtime),"\n\n";

# script scoped variables
my %accession_to_delete = ();
my $DEL_AFTER_DATE = undef;

if ( $ARGV[$argnum] =~ m/^-DEL_AFTER_DATE$/ ) {

	if (   $ARGV[ $argnum + 1 ] =~ m/^\d{4}\-(\d{2})\-(\d{2})$/i ) {
		$DEL_AFTER_DATE = $ARGV[ $argnum + 1 ];
		if ($1 > 12) {
			print LOG
"incorrect -DEL_AFTER_DATE argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -DEL_AFTER_DATE {YEAR-MO-DA} ; The Month parameter should not be greater than 12.\n";
			die
"incorrect -DEL_AFTER_DATE argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -DEL_AFTER_DATE {YEAR-MO-DA} ; The Month parameter should not be greater than 12.\n";
		}
		if ($2 > 31) {
			print LOG
"incorrect -DEL_AFTER_DATE argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -DEL_AFTER_DATE {YEAR-MO-DA} ; The Day parameter should not be greater than 31.\n";
			die
"incorrect -DEL_AFTER_DATE argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -DEL_AFTER_DATE {YEAR-MO-DA} ; The Day parameter should not be greater than 31.\n";
		}
	} else {
		print LOG
"incorrect -DEL_AFTER_DATE argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -DEL_AFTER_DATE {YEAR-MO-DA}\n";
		die
"incorrect -DEL_AFTER_DATE argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -DEL_AFTER_DATE {YEAR-MO-DA}\n";
	}

}


# query db to find accession with an entry in table elements but without an entry in the table alignment_params
my $st = $ORIGAMI::dbh->prepare("select accession from elements where mdate >= '$DEL_AFTER_DATE'");
$st->execute or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	$accession_to_delete { $res->{accession} } = 1;
}

print LOG scalar (keys %accession_to_delete) ." accessions have been found to be deleted.\n";

foreach $accession_to_delete_IT (keys %accession_to_delete) {
	print LOG `perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/delete_entry.pl $accession_to_delete_IT`;
}



# end of script
print LOG "Task_delete_elet_inserted_after_date finished at : ",scalar(localtime),"\n";
print LOG "----------------------------------------------------------------------\n";
close(LOG);













