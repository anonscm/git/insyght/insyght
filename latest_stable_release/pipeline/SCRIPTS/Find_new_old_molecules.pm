package Find_new_old_molecules;

#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use SiteConfig;
use BlastConfig;
use ORIGAMI;
use lib '$SiteConfig::SCRIPTSDIR';

my %q_molecule_id_in_table_alignment_params	            = ();
my $st;
my @list_ordered_new_molecule_id		            = ();
my @list_ordered_old_molecule_id		            = ();
#my %element_id_2_accnum		            = ();
my %molecule_id_2_size		            = ();
my %element_id2orga_id		            = ();

sub find_new_old_molecules_from_db_data {
	#print "BLAST_TAXO_GRANULARITY $BLAST_TAXO_GRANULARITY\n";
        #my @array1 = ("a", "b", "c", "d");
        #my @array2 = (1, 2, 3, 4);
        #return (\@array1, \@array2);

        my $MOLECULE_TYPE = shift;
	if ($MOLECULE_TYPE =~ m/^ORGANISM_VS_ALL$/  || $MOLECULE_TYPE =~ m/^CLUSTER_ORGANISM$/ ) {
		$MOLECULE_TYPE = "ORGANISM";
	} elsif ($MOLECULE_TYPE =~ m/^ELEMENT_VS_ELEMENT$/) {
		$MOLECULE_TYPE = "ELEMENT";
	}


	if ($MOLECULE_TYPE =~ m/^ORGANISM$/) {

		# is orga new or old ?
		$st = $ORIGAMI::dbh->prepare("select distinct q_organism_id FROM alignment_params");
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {
			$q_molecule_id_in_table_alignment_params{ $res->{q_organism_id} } = 1;
		}

		#QUERRY ALL elements_id order by size: Origami database query : all rows of table elements, showing columns accession, element_id and store in list_ordered_new_element_id and list_ordered_old_element_id
		$st = $ORIGAMI::dbh->prepare("select organisms.organism_id, count(genes.gene_id) FROM organisms, genes WHERE genes.organism_id = organisms.organism_id GROUP BY organisms.organism_id ORDER BY count(genes.gene_id) DESC");
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {

			if (exists $q_molecule_id_in_table_alignment_params{ $res->{organism_id} }) {
				#print LOG "The orga id = ${orga_id_IT} is considered as inserted in a previous run\n";
				push( @list_ordered_old_molecule_id, $res->{organism_id} );
				$molecule_id_2_size{ $res->{organism_id} } = $res->{count};
			} else {
				#print LOG "The orga id = ${orga_id_IT} is considered as new\n";
				push( @list_ordered_new_molecule_id, $res->{organism_id} );
				$molecule_id_2_size{ $res->{organism_id} } = $res->{count};
			}

		}

		#print @list_ordered_new_molecule_id." new organisms found.\n";
		#print @list_ordered_old_molecule_id." old organisms found.\n";


	} elsif ($MOLECULE_TYPE =~ m/^ELEMENT$/) {

		# is orga new or old ?
		$st = $ORIGAMI::dbh->prepare("select distinct q_element_id FROM alignment_params");
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {
			$q_molecule_id_in_table_alignment_params{ $res->{q_element_id} } = 1;
		}

		#QUERRY ALL elements_id order by size: Origami database query : all rows of table elements, showing columns accession, element_id and store in list_ordered_new_element_id and list_ordered_old_element_id
		$st = $ORIGAMI::dbh->prepare("select genes.element_id, elements.organism_id, count(genes.gene_id) FROM genes, elements WHERE genes.element_id = elements.element_id GROUP BY genes.element_id, elements.accession, elements.organism_id ORDER BY count(genes.gene_id) DESC, elements.accession");
		$st->execute or die("Pb execute $!");
		while ( my $res = $st->fetchrow_hashref() ) {
			#$element_id_2_accnum{ $res->{element_id} } = $res->{accession};
			$element_id2orga_id{ $res->{element_id} } = $res->{organism_id};

			if (exists $q_molecule_id_in_table_alignment_params{ $res->{element_id} }) {
				#print LOG "The orga id = ${orga_id_IT} is considered as inserted in a previous run\n";
				push( @list_ordered_old_molecule_id, $res->{element_id} );
				$molecule_id_2_size{ $res->{element_id} } = $res->{count};
			} else {
				#print LOG "The orga id = ${orga_id_IT} is considered as new\n";
				push( @list_ordered_new_molecule_id, $res->{element_id} );
				$molecule_id_2_size{ $res->{element_id} } = $res->{count};
			}

		}

		#print @list_ordered_new_molecule_id." new elements found.\n";
		#print @list_ordered_old_molecule_id." old elements found.\n";

	} else {
		print "MOLECULE_TYPE not recognized : $MOLECULE_TYPE.\n";
		die("MOLECULE_TYPE not recognized : $MOLECULE_TYPE.\n");
	}

#	return (\@list_ordered_new_molecule_id, \@list_ordered_old_molecule_id, \%element_id_2_accnum, \%molecule_id_2_size, \%element_id2orga_id);
	return (\@list_ordered_new_molecule_id, \@list_ordered_old_molecule_id, \%molecule_id_2_size, \%element_id2orga_id);

}



1;
