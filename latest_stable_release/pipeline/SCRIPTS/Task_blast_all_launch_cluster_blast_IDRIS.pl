#!/usr/local/bin/perl
#
# Task_blast_all_launch_cluster_blast_IDRIS.pl : lance les fichier blast.ll sur le cluster
#
# Date : 01/2014
# LT
#
#LOG FILE : $SiteConfig::LOGDIR/Blast/Task_blast_all_launch_cluster_blast_IDRIS.log
#LAUNCHED AS : perl Task_blast_all_launch_cluster_blast_IDRIS.pl  -MAX_JOBS = {INTEGER} -VERBOSE = {ON, OFF} -CMD_CLUSTER [optional]
#ARGUMENTS :
# -MAX_JOBS = {INTEGER}
# -VERBOSE : if ON print ant details at each step, if OFF only print critical information
# -CMD_CLUSTER : [optional], if this argument is present or 0 then the command are executed in the bash locally. Example of command for cluster: -CMD_CLUSTER "qsub -m ea -q short.q"
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use Time::HiRes qw(usleep);
use SiteConfig;
#use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;
use BlastConfig;


#script level variables
my $path_log_file = "$SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_launch_cluster_blast_IDRIS.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/Task_blast_all_launch_cluster_blast_IDRIS.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/Task_blast_all_launch_cluster_blast_IDRIS.error";
my $output_backtick = "";
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $MAX_JOBS = 1;
my $VERBOSE = "ON";
my $CMD_SUBMIT_CLUSTER = "$SiteConfig::CMDDIR/bash";
#my @launch_fasta_files_unsorted   = ();
my @launch_blast_batch_files_unsorted   = ();
my @launch_blast_batch_files            = ();
my @blast_output_unsorted    = ();
my @blast_output             = ();
my $flag_RECOVER_FROM_FAILURE = 0;
my $output_rm = "";
my $DO_NOT_STOP_ON_BLAST_FAILURE = 0;
my $molecule_type;
my $EMAIL_NOTIFICATION = "";
my $FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = "ON";


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	if ( length ($EMAIL_NOTIFICATION) > 0 ) {
		my $email_subject = "Inysght notification : Error Task_blast_all_launch_cluster_blast_IDRIS.pl";
		my $email_body = "Hello,\nThis is a notification from Insyght, your job Task_blast_all_launch_cluster_blast_IDRIS.pl did not complete because of an error :\n$error_mssg\nThe Insyght team\n";
		`echo "$email_body" | mailx -r Inysght_notification_no_reply -s "$email_subject" $EMAIL_NOTIFICATION`;
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


#set the variables according to the argument or exit with usage if the arguments are incorrect
foreach my $argnum ( 0 .. $#ARGV ) {

	if ( $ARGV[$argnum] =~ m/^-MAX_JOBS$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_JOBS = $ARGV[ $argnum + 1 ];
			if($MAX_JOBS < 1){
				die_with_error_mssg ("incorrect -MAX_JOBS argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl  -MAX_JOBS {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAX_JOBS argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -MAX_JOBS = {INTEGER}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -VERBOSE argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -VERBOSE = {ON, OFF}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-CMD_CLUSTER/ ) {
		if ( $ARGV[$argnum] =~ m/^0$/i ) {

			#do nothing
		}
		else {
			if ( $ARGV[$argnum] =~ m/^.+$/i ) {
				$CMD_SUBMIT_CLUSTER = $ARGV[ $argnum + 1 ];
			}
		}

	} elsif ( $ARGV[$argnum] =~ m/^-RECOVER_FROM_FAILURE$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^ON$/i )
		{
			$flag_RECOVER_FROM_FAILURE = 1;
			print LOG "\nmode RECOVER_FROM_FAILURE activated.\n\n" unless $VERBOSE =~ m/^OFF$/;
		}
		else {
			die_with_error_mssg ("incorrect -RECOVER_FROM_FAILURE argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -RECOVER_FROM_FAILURE ON ; omit the -RECOVER_FROM_FAILURE flag to turn off");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-DO_NOT_STOP_ON_BLAST_FAILURE$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^ON$/i )
		{
			$DO_NOT_STOP_ON_BLAST_FAILURE = 1;
			print LOG "\nmode DO_NOT_STOP_ON_BLAST_FAILURE activated.\n\n" unless $VERBOSE =~ m/^OFF$/;
		}
		else {
			die_with_error_mssg ("incorrect -DO_NOT_STOP_ON_BLAST_FAILURE argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -DO_NOT_STOP_ON_BLAST_FAILURE ON ; omit the -DO_NOT_STOP_ON_BLAST_FAILURE flag to turn off");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-EMAIL_NOTIFICATION/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i ) {
			$EMAIL_NOTIFICATION = $1;
			print LOG "\nmode EMAIL_NOTIFICATION activated, notifications will be sent to : $EMAIL_NOTIFICATION\n\n" unless $VERBOSE =~ m/^OFF$/;
		}
		else {
			die_with_error_mssg( "incorrect -EMAIL_NOTIFICATION argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -EMAIL_NOTIFICATION {your_email_address} ; omit the -EMAIL_NOTIFICATION flag to turn off");
		}
	} elsif  ( $ARGV[$argnum] =~ m/^-FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK argument ; usage : perl Task_blast_all_launch_cluster_blast_IDRIS.pl -FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK {ON, OFF}");
		}

	}

}

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers des blast
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_launch_cluster_blast_IDRIS.log" );
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG "\n---------------------------------------------------\n Task_blast_all_launch_cluster_blast_IDRIS.pl started at :", scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}



print LOG "\nYou choose the following options :\n\tMAX_JOBS : $MAX_JOBS\n\tVERBOSE : $VERBOSE\n" unless $VERBOSE =~ m/^OFF$/;
if ( $CMD_SUBMIT_CLUSTER =~ m/^0$/i ) {
	#do nothing
}
else {
	print LOG "CMD_CLUSTER : $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/;
}



if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/ ) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	$molecule_type = "element";
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}

#rm files
$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/*`;
$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_*.err`;
$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_*.out`;
if($flag_RECOVER_FROM_FAILURE == 0){
	$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f -r $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/*`;
}
if ($output_rm eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm files : $output_rm");
}

# list files under $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb
sub sub_list_faa_files_recursively {
    my ( $path, $ref_array_to_push ) = @_;
    #my $path = shift;
    opendir (DIR, $path) or do {
		die_with_error_mssg( "Unable to open $path: $!");
    };

    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);

    for (@files) {
        if (-d $_) {
            # directory, run recursively
            #print "$_ is a dir\n";
            sub_list_faa_files_recursively ($_);
        } elsif ($_ =~ m/^.+\/${molecule_type}_([\d_]+)\.faa$/ ) {
	    push ( @$ref_array_to_push, $1);
        }
=pod
	 elsif ($_ =~ m/\.faa~$/ ) {
		#del ~ files
		`$SiteConfig::CMDDIR/rm -f $_`;
	} else {
		print LOG "Error in sub_list_faa_files_recursively : the file $_ does not match the regex *.faa\n";
		die("Error in sub_list_faa_files_recursively : the file $_ does not match the regex *.faa\n");
        }
=cut
    }
}
#NO NEED TO LIST fasta files
#if ( -d "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/" ) {
#	sub_list_faa_files_recursively ("$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/", \@launch_fasta_files_unsorted);
#}
#@launch_fasta_files_unsorted = <$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/*.faa>;
#print LOG scalar(@launch_fasta_files_unsorted) . " .faa files found under $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/\n";


my $count_already_existing_output_files = 0;
# Accepts one argument: the full path to a directory.
sub sub_list_blast_ll_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or do {
		die_with_error_mssg( "Unable to open $path: $!");
    };


    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);

    for (@files) {
        if (-d $_) {
            # directory, run recursively
            #print "$_ is a dir\n";
            sub_list_blast_ll_files_recursively ($_);
        } elsif ($_ =~ m/^.+\/batch_(\w+)_([\d_]+)_VS_(\w+)_([\d_al]+)\.blast\.ll$/ ) {
            # File *.blast.ll, store it in @launch_blast_batch_files_unsorted
            #print "$_ is a file\n";
           my $molecule_id_parsed = $2;
           my $core_name_parsed = "$2_$1_VS_$4_$3";
           if($flag_RECOVER_FROM_FAILURE == 1){
		#check if output file already here
		#TODO NOT WORKING flag_RECOVER_FROM_FAILURE
		if ( -e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${molecule_type}_id_${molecule_id_parsed}/${core_name_parsed}.blast") {
			#output file exists, skip
			print LOG "Output file $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${molecule_type}_id_${molecule_id_parsed}/${core_name_parsed}.blast already exists, skipping batch command file $_\n" unless $VERBOSE =~ m/^OFF$/;
			$count_already_existing_output_files++;
		} else {
			push ( @launch_blast_batch_files_unsorted, $_);
		}
           } else {
		push ( @launch_blast_batch_files_unsorted, $_);
           }
         } elsif ($_ =~ m/^.+\/batch_(\w+)_([\d_]+)_VS_(\w+)_([\d_al]+)\.blast\.ll~$/ ) {
		#del ~ files
		`$SiteConfig::CMDDIR/rm -f $_`;
	 } else {
		die_with_error_mssg("Error in sub_list_blast_ll_files_recursively : the file $_ does not match the regex *.blast.ll");
        }
    }
}
sub_list_blast_ll_files_recursively ("$SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/");
@launch_blast_batch_files = sort @launch_blast_batch_files_unsorted;
print LOG scalar(@launch_blast_batch_files) . " .blast.ll files found under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/\n" unless $VERBOSE =~ m/^OFF$/;
if($flag_RECOVER_FROM_FAILURE == 1){
	print LOG "Skipped $count_already_existing_output_files batch command files because the corresponding blast output file already exists\n" unless $VERBOSE =~ m/^OFF$/;
}


# launch blast
my $number_blast_started  = 0;
my $number_blast_done = 0;
my $count_blast_failure = 0;
my $number_blast_started_before_force_submit_check = 0;

sub sub_count_blast_done_files {


	if ( $FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK =~ m/^ON$/i && ( $number_blast_started - $number_blast_done < $MAX_JOBS ) && $number_blast_started > $number_blast_started_before_force_submit_check ) {
		$number_blast_started_before_force_submit_check = $number_blast_started;
		print LOG "( $number_blast_done / ".scalar(@launch_blast_batch_files).") : FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK is ON ; Continuing to submit as there is $number_blast_started - $number_blast_done = ".($number_blast_started - $number_blast_done)." simulteneous process ;  MAX_JOBS =  $MAX_JOBS\n" unless $VERBOSE =~ m/^OFF$/i;
	
	} else {

		#my @list_new_files_blast_done = <$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/*.blast_done>;
		opendir (DIR, "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/") or do {
			die_with_error_mssg( "Unable to open $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/: $!");
		};

		my @list_new_files_blast_done =
			# Third: Prepend the full path
			map { "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/" . $_ }
			# Second: take out '.' and '..'
			grep { !/^\.{1,2}$/ }
			# First: get all files
			readdir (DIR);
		closedir (DIR);


		# check err files are empty
		my $check_err_files_are_empty = `cat $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_*.err 2>/dev/null`;
		if ($check_err_files_are_empty eq ""){
			#ok continue
		} else {
			#TODO NOT WORKING WITH WARNING FROM BLAST...
			my @split = split( '\n', $check_err_files_are_empty );
			my $not_only_Selenocysteine = 0;
			foreach my $line_split ( @split ) {
				if ($line_split =~ m/^Selenocysteine \(U\) at position \d+ replaced by X$/
					|| $line_split =~ m/Warning: One or more U or O characters replaced by X for alignment score calculations/
				) {
					#ok
				} else {
					$not_only_Selenocysteine = 1;
				}
			}
			if ($not_only_Selenocysteine) {
				die_with_error_mssg("Error cat $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_*.err 2>/dev/null is not empty : $check_err_files_are_empty");
			} else {
				#ok do nothing		
			}
		}


		foreach my $list_new_files_blast_done_IT (@list_new_files_blast_done) {

			#parse file name
			my $core_name_list_new_files_blast_done_IT = "";
			if ( $list_new_files_blast_done_IT =~ m/^.+\/(.+)\.blast_done$/i ) {
				$core_name_list_new_files_blast_done_IT = $1;
			} else {
				die_with_error_mssg("Could not parse core name in file $list_new_files_blast_done_IT");
			}

			# if .err not empty
			if (! $CMD_SUBMIT_CLUSTER eq "$SiteConfig::CMDDIR/bash") {
				if (! -z "$SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_list_new_files_blast_done_IT}.err" ) {
					if ($DO_NOT_STOP_ON_BLAST_FAILURE == 1) {
						$count_blast_failure++;
						`$SiteConfig::CMDDIR/mv $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_list_new_files_blast_done_IT}.err $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_list_new_files_blast_done_IT}.err_DO_NOT_STOP_ON_BLAST_FAILURE`;
						`$SiteConfig::CMDDIR/mv $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_list_new_files_blast_done_IT}.out $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_list_new_files_blast_done_IT}.out_DO_NOT_STOP_ON_BLAST_FAILURE`;
						`$SiteConfig::CMDDIR/rm -f $list_new_files_blast_done_IT`;
						$number_blast_done++;
					} else {
						die_with_error_mssg("The file $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_list_new_files_blast_done_IT}.err is not empty : $!");
					}
			
				} else {

					# rm .err
					$output_rm = `$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_list_new_files_blast_done_IT}.err`;
					# rm .out
					$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_list_new_files_blast_done_IT}.out`;

					if ($output_rm eq "") {
						#ok
					} else {
						die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm -f $list_new_files_blast_done_IT : $output_rm");
					}
				}
			}



			# rm marker file
			`$SiteConfig::CMDDIR/rm -f $list_new_files_blast_done_IT`;

			# increment counter
			$number_blast_done++;
		}

	}
}


LOOP: foreach my $launch_blast_batch_files_IT (@launch_blast_batch_files) {

	# wait a bit if there more than $MAX_JOBS child process that have been launched simultaneously
	my $count_sleep = 0;
	while ( $count_sleep < 2 ) {

		sub_count_blast_done_files();

		#if ($MAX_JOBS == 1) {
		#	last;
		#} else {
			if( $number_blast_started - $number_blast_done < $MAX_JOBS ){
				#wait a bit so that there is a shifting in time and exit loop
				#usleep(10000);

				my $number_jobs_being_processed = $number_blast_started - $number_blast_done;
				my $number_jobs_left_to_be_submitted = scalar(@launch_blast_batch_files) - $number_blast_started;
				print LOG "( $number_blast_done / ".scalar(@launch_blast_batch_files).") : Continuing to submit ; MAX_JOBS = $MAX_JOBS ; Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_blast_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
				last;
			}
		
			if ($count_sleep == 0 ) {
				my $number_jobs_being_processed = $number_blast_started - $number_blast_done;
				my $number_jobs_left_to_be_submitted = scalar(@launch_blast_batch_files) - $number_blast_started;
				print LOG "( $number_blast_done / ".scalar(@launch_blast_batch_files).") : waiting a bit, $MAX_JOBS jobs launched simultenously ; MAX_JOBS = $MAX_JOBS ; Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_blast_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
				$count_sleep = 1;
				usleep(10000);
			} else {
				usleep(10000);
			}
		#}
	}

	# check the state of blast failure
	if ($DO_NOT_STOP_ON_BLAST_FAILURE == 1 && $count_blast_failure > 0) {
		print LOG "There are $count_blast_failure blast failures so far ; DO_NOT_STOP_ON_BLAST_FAILURE mode on.\n" unless $VERBOSE =~ m/^OFF$/i;
	}
	if ($count_blast_failure > 500) {
		die_with_error_mssg("Stopping script as there is more than 500 blast failures, something is wrong, please check.");
	}


	# make sure the ll file is executable
	my $chmod_output = `chmod +x $launch_blast_batch_files_IT`;
	if ( ! $chmod_output eq "") {
		die_with_error_mssg("ERROR Could not chmod +x $launch_blast_batch_files_IT: $!");
	}

	#parse file name batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast.ll
	#my $master_elet_id;
	#my $sub_elet_id;
	my $core_name_ll_file_IT = "";
	if ( $launch_blast_batch_files_IT =~ m/^.+\/batch_(.+)\.blast\.ll$/i ) {
		#$master_elet_id = $1;
		#$sub_elet_id = $2;
		$core_name_ll_file_IT = $1;
	} else {
		die_with_error_mssg("Could not parse core name in file $launch_blast_batch_files_IT");
	}


	# launch of the bash file
	# case no cluster, no paralelisation
	if ( $CMD_SUBMIT_CLUSTER eq "$SiteConfig::CMDDIR/bash") {

#TODO $QSUB_LOG_DIR/launch_blast_for_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.err
# my $check_err_files_are_empty = `cat $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_*.err 2>/dev/null`


		if($MAX_JOBS > 1){
			system("$CMD_SUBMIT_CLUSTER $launch_blast_batch_files_IT >> $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_ll_file_IT}.err 2>&1 &");
			if ( $? != 0 ) {
				die_with_error_mssg("command $CMD_SUBMIT_CLUSTER $launch_blast_batch_files_IT failed: $!");
			}
			#print LOG $output_cmd;
			$number_blast_started++;
			#print LOG "file $launch_blast_batch_files_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
			my $number_jobs_being_processed = $number_blast_started - $number_blast_done;
			my $number_jobs_left_to_be_submitted = scalar(@launch_blast_batch_files) - $number_blast_started;
			print LOG "( $number_blast_done / ".scalar(@launch_blast_batch_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_blast_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		} else {
			$number_blast_started++;
			my $number_jobs_being_processed = $number_blast_started - $number_blast_done;
			my $number_jobs_left_to_be_submitted = scalar(@launch_blast_batch_files) - $number_blast_started;
			print LOG "( $number_blast_done / ".scalar(@launch_blast_batch_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_blast_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
			system("$CMD_SUBMIT_CLUSTER $launch_blast_batch_files_IT >> $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${core_name_ll_file_IT}.err 2>&1");
			#print LOG "file $launch_blast_batch_files_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
			print LOG "( $number_blast_done / ".scalar(@launch_blast_batch_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_blast_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		}


	} else {
		# use of cluster
		my $output_cmd = `$CMD_SUBMIT_CLUSTER $launch_blast_batch_files_IT 2>&1`;
		if ( $? != 0 )
		{
			die_with_error_mssg("command $CMD_SUBMIT_CLUSTER $launch_blast_batch_files_IT 2>&1 failed: $!");
		}
		if($output_cmd =~ m/^Your job .+ has been submitted$/i
			|| $output_cmd eq ""
		){
			$number_blast_started++;
			#print LOG "file $launch_blast_batch_files_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
			my $number_jobs_being_processed = $number_blast_started - $number_blast_done;
			my $number_jobs_left_to_be_submitted = scalar(@launch_blast_batch_files) - $number_blast_started;
			print LOG "( $number_blast_done / ".scalar(@launch_blast_batch_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_blast_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		}else{
			die_with_error_mssg("Error launch of the bash on cluster: $CMD_SUBMIT_CLUSTER $launch_blast_batch_files_IT :\n$output_cmd");
		}
	}

}    #foreach my $launch_blast_batch_files_IT (@launch_blast_batch_files) {

print LOG "done submitting all commands\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "waiting for last commands to complete...\n" unless $VERBOSE =~ m/^OFF$/;
$FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = "OFF";

my $count_finish = 0;
my $count_wait_hours = 0;

while ( $count_finish >= 0 ) {

	sub_count_blast_done_files();
	
	if($number_blast_done == scalar(@launch_blast_batch_files) ){
		last;
	}

	usleep(1000000);#sleep 1s
	$count_finish++;
	if ($count_finish > 3600){#1 hour
		#print LOG "Error, waited for blast output file for more than 30 minutes\n";
		#die ("Error, waited for blast output file for more than 30 minutes\n");
		$count_wait_hours++;
		print LOG "( $number_blast_done / ".scalar(@launch_blast_batch_files).") : All jobs have been launched. Waiting for blast output files ; Total waiting time = $count_wait_hours hours ; Total output files found yet = $number_blast_done. (This message will display every hour until all output files are found).\n" unless $VERBOSE =~ m/^OFF$/;
		$count_finish = 0;
	}

}

#rm file under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/*
$output_rm = `$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/*`;
if ($output_rm eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/* : $!");
}

# check the state of blast failure
if ($DO_NOT_STOP_ON_BLAST_FAILURE == 1 && $count_blast_failure > 0) {

	print LOG "Script has finished submitting blast ($number_blast_done blast submitted total) but there have been $count_blast_failure blast failure ; DO_NOT_STOP_ON_BLAST_FAILURE mode was on.\nPlease re-run Task_blast_all_launch_cluster_blast_IDRIS.pl with the mode RECOVER_FROM_FAILURE ON to complete this step.\n If some blast keep failing, please run them manuallly to check what is wrong.\n";

} else {
	print LOG "$number_blast_done blast have been successfully submitted in this run.\n" unless $VERBOSE =~ m/^OFF$/;
	if($flag_RECOVER_FROM_FAILURE == 1){
		print LOG "$count_already_existing_output_files blast were skipped because RECOVER_FROM_FAILURE mode was ON and the blast output file was already present.\n" unless $VERBOSE =~ m/^OFF$/;
	}

	print LOG
	"\n---------------------------------------------------\n\n\nTask_blast_all_launch_cluster_blast_IDRIS.pl successfully completed at :",
	  scalar(localtime),
	  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;

}

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}


close(LOG);

if ( length ($EMAIL_NOTIFICATION) > 0 ) {
	my $email_subject = "Inysght notification : Task_blast_all_launch_cluster_blast_IDRIS.pl complete";
	my $email_body = "Hello,\nThis is a notification from Insyght, your job Task_blast_all_launch_cluster_blast_IDRIS.pl is complete!\nYou can now proceed with the rest of the pipeline.\nSincerely,\nThe Insyght team\n";
	`echo "$email_body" | mailx -r Inysght_notification_no_reply -s "$email_subject" $EMAIL_NOTIFICATION`;
}




