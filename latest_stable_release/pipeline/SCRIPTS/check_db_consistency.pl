#!/usr/local/bin/perl
#
# perl check_db_consistency.pl
#
# TODO more tests, implemented tests are incomplete
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use Time::HiRes qw(sleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;

# whole script scoped variable
my %element_id_2_accnum            = ();
my $st;
my $OVERALL_FATAL_PB = "";
my $OVERALL_WARN_PB = "";
my $FORCE_DELETE_DUPLICATE_ALIGN_PARAM = "OFF";
my %DUPLICATE_TO_DELETE_FROM_ALIGN_PARAM = ();
my $DB_VERSION = 1;

#turn on autoflush
$| = 1;

foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-FORCE_DELETE_DUPLICATE_ALIGN_PARAM$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			||  $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FORCE_DELETE_DUPLICATE_ALIGN_PARAM = $ARGV[ $argnum + 1 ];
		}
		else {
			print "incorrect -FORCE_DELETE_DUPLICATE_ALIGN_PARAM argument ; usage : perl check_db_consistency.pl -FORCE_DELETE_DUPLICATE_ALIGN_PARAM {ON / OFF}";
			die "incorrect -FORCE_DELETE_DUPLICATE_ALIGN_PARAM argument ; usage : perl check_db_consistency.pl -FORCE_DELETE_DUPLICATE_ALIGN_PARAM {ON / OFF}";
		}
	}
	if ( $ARGV[$argnum] =~ m/^-DB_VERSION$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i )
		{
			$DB_VERSION = $ARGV[ $argnum + 1 ];
		}
		else {
			print "incorrect -FORCE_DELETE_DUPLICATE_ALIGN_PARAM argument ; usage : perl check_db_consistency.pl -DB_VERSION {POSITIVE DIGIT}";
			die "incorrect -FORCE_DELETE_DUPLICATE_ALIGN_PARAM argument ; usage : perl check_db_consistency.pl -DB_VERSION {POSITIVE DIGIT}";
		}
	}
}

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log 
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_alignments/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_add_alignments/check_db_consistency.log" );
#print LOG "\n---------------------------------------------------\n check_db_consistency.pl started at :",  scalar(localtime), "\n\n\n";
print "\n";

# filling in element_id_2_accnum
$st = $ORIGAMI::dbh->prepare("select element_id, accession from elements");
$st->execute or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	$element_id_2_accnum { $res->{element_id} } = $res->{accession};
}


# check all rows in alignment_params have no redundancy
# alignment_params_id is defined as primary key and will throw an exception upon insertion of duplicate key
print "\t\tChecking redundancies in table alignment_params for q_element_id, s_element_id";
$st = $ORIGAMI::dbh->prepare("select * from (
  SELECT alignment_param_id, q_element_id, s_element_id,
  ROW_NUMBER() OVER(PARTITION BY q_element_id, s_element_id ORDER BY alignment_param_id asc) AS Row
  FROM alignment_params
) dups
where 
dups.Row > 1");
my $count_redundancies_in_alignment_params = 0;
$st->execute or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	$count_redundancies_in_alignment_params++;
	print "The alignment_param_id = ".$res->{alignment_param_id}." in table alignment_params is a duplicate regarding the columns q_element_id and s_element_id. To have the details, use request :\nSELECT * FROM alignment_params WHERE q_element_id = ".$res->{q_element_id}." AND s_element_id = ".$res->{s_element_id}."\n";
	$OVERALL_FATAL_PB = $OVERALL_FATAL_PB."\n - The alignment_param_id = ".$res->{alignment_param_id}." in table alignment_params is a duplicate regarding the columns q_element_id and s_element_id. To have the details, use request :\nSELECT * FROM alignment_params WHERE q_element_id = ".$res->{q_element_id}." AND s_element_id = ".$res->{s_element_id}."\n";
	if ($FORCE_DELETE_DUPLICATE_ALIGN_PARAM =~ m/^ON$/i) {
		$DUPLICATE_TO_DELETE_FROM_ALIGN_PARAM { $res->{alignment_param_id} } = 1;
	}
}


my $count_deleted_row_align_params = 0;
if ($FORCE_DELETE_DUPLICATE_ALIGN_PARAM =~ m/^ON$/i) {
	foreach my $DUPLICATE_TO_DELETE_FROM_ALIGN_PARAM_IT (keys %DUPLICATE_TO_DELETE_FROM_ALIGN_PARAM) {
		$ORIGAMI::dbh->do("DELETE FROM alignment_params WHERE alignment_param_id = $DUPLICATE_TO_DELETE_FROM_ALIGN_PARAM_IT");
		#$st_del->execute or die("Pb execute $!");
		#print "DELETE FROM alignment_params WHERE alignment_param_id = $DUPLICATE_TO_DELETE_FROM_ALIGN_PARAM_IT\n";
		$count_deleted_row_align_params++;
	}
}

#print "\n\t\tDone\n";
if ($count_redundancies_in_alignment_params == 0) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> $count_redundancies_in_alignment_params problems were detected\n";
	if ($FORCE_DELETE_DUPLICATE_ALIGN_PARAM =~ m/^ON$/i) {
		print ("$count_deleted_row_align_params duplicated rows were deleted.\n\n");
		die ("$count_deleted_row_align_params duplicated rows were deleted\n\n");
	}
}


print "\t\tChecking redundancies for synteny parameters";
my $table_synteny_parameters = "";
if ($DB_VERSION >= 3) {
	$table_synteny_parameters = "params_scores_algo_syntenies";
} else {
	$table_synteny_parameters = "alignment_params";
}
$st = $ORIGAMI::dbh->prepare("SELECT DISTINCT ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac FROM $table_synteny_parameters");
my $count_redundancies_in_alignment_params_parameters = 0;
$st->execute or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	$count_redundancies_in_alignment_params_parameters++;
}
if ($count_redundancies_in_alignment_params_parameters > 1) {
	print "There are multiple different set of parameters in the table alignment_params, comparison between different pairwise comparison is not advised.\n Please run the command\nSELECT DISTINCT ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac FROM alignment_params\n to have a full list of the different parameters.\n";
	$OVERALL_WARN_PB = $OVERALL_WARN_PB."\n - There are multiple different set of parameters in the table alignment_params, comparison between different pairwise comparison is not advised.\n Please run the command\nSELECT DISTINCT ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac FROM alignment_params\n to have a full list of the different parameters.\n";
}
#print "** Done check redundancies in table alignment_params for parameters.\n";
if ($count_redundancies_in_alignment_params <= 1) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> A warning was detected\n";
}


# check all rows in alignments have no redundancy
# alignment_id is defined as primary key and will throw an exception upon insertion of duplicate key
print "\t\tChecking redundancies in table alignments";
$st = $ORIGAMI::dbh->prepare("select * from (
  SELECT alignment_id, alignment_param_id, q_start, q_stop, s_start, s_stop, score,
  ROW_NUMBER() OVER(PARTITION BY alignment_param_id, q_start, q_stop, s_start, s_stop, score, orientation_conserved ORDER BY alignment_id asc) AS Row
  FROM alignments
) dups
where 
dups.Row > 1");
$st->execute or die("Pb execute $!");
my $count_redundancies_table_alignments = 0;
while ( my $res = $st->fetchrow_hashref() ) {
	$count_redundancies_table_alignments++;
	print "The alignment_id = ".$res->{alignment_id}."in table alignments is a duplicate regarding the columns alignment_param_id, q_start, q_stop, s_start, s_stop, score, orientation_conserved. For more details, please run the command :\nSELECT * FROM alignments WHERE alignment_param_id = ".$res->{alignment_param_id}." AND q_start = ".$res->{q_start}." AND q_stop = ".$res->{q_stop}." AND s_start = ".$res->{s_start}." AND s_stop = ".$res->{s_stop}." AND score = ".$res->{score}." AND orientation_conserved = ".$res->{orientation_conserved}." \n";
	$OVERALL_FATAL_PB = $OVERALL_FATAL_PB."\n - The alignment_id = ".$res->{alignment_id}."in table alignments is a duplicate regarding the columns alignment_param_id, q_start, q_stop, s_start, s_stop, score, orientation_conserved. For more details, please run the command :\nSELECT * FROM alignments WHERE alignment_param_id = ".$res->{alignment_param_id}." AND q_start = ".$res->{q_start}." AND q_stop = ".$res->{q_stop}." AND s_start = ".$res->{s_start}." AND s_stop = ".$res->{s_stop}." AND score = ".$res->{score}." AND orientation_conserved = ".$res->{orientation_conserved}." \n";
}
#print "** Done check redundancies in table alignments.\n\n";
if ($count_redundancies_table_alignments == 0) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> $count_redundancies_table_alignments problems were detected\n";
}




#check all rows in alignment_pairs have no redundancy
print "\t\tChecking all rows in alignment_pairs have no redundancy";

$st = $ORIGAMI::dbh->prepare("select * from (
  SELECT alignment_id, q_gene_id, s_gene_id, type, 
  ROW_NUMBER() OVER(PARTITION BY alignment_id, q_gene_id, s_gene_id ORDER BY alignment_id asc) AS Row
  FROM alignment_pairs WHERE type = 1 OR type = 2
) dups
where 
dups.Row > 1");
$st->execute or die("Pb execute $!");
my $count_redundancies_alignment_pairs = 0;
while ( my $res = $st->fetchrow_hashref() ) {
	$count_redundancies_alignment_pairs++;
	print "The alignment_id = ".$res->{alignment_id}."in table alignment_pairs is a duplicate regarding the columns alignment_id, q_gene_id, s_gene_id. For more details, please run the command :\nSELECT * FROM alignment_pairs WHERE alignment_id = ".$res->{alignment_id}." AND q_gene_id = ".$res->{q_gene_id}." AND s_gene_id = ".$res->{s_gene_id}."\n";
	$OVERALL_FATAL_PB = $OVERALL_FATAL_PB."\n - The alignment_id = ".$res->{alignment_id}."in table alignment_pairs is a duplicate regarding the columns alignment_id, q_gene_id, s_gene_id. For more details, please run the command :\nSELECT * FROM alignment_pairs WHERE alignment_id = ".$res->{alignment_id}." AND q_gene_id = ".$res->{q_gene_id}." AND s_gene_id = ".$res->{s_gene_id}."\n";
}

#print "** Test done with success: check all rows in alignment_pairs have no redundancy\n\n";
if ($count_redundancies_alignment_pairs == 0) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> $count_redundancies_alignment_pairs problems were detected\n";
}


#check all rows in homologies have no redundancy
print "\t\tChecking all rows in homologies have no redundancy";

$st = $ORIGAMI::dbh->prepare("select * from (
  SELECT q_gene_id, s_gene_id, 
  ROW_NUMBER() OVER(PARTITION BY q_gene_id, s_gene_id) AS Row
  FROM homologies
) dups
where 
dups.Row > 1");

$st->execute or die("Pb execute $!");
my $count_redundancy_table_homo = 0;
while ( my $res = $st->fetchrow_hashref() ) {
	$count_redundancy_table_homo++;
	print "The q_gene_id = ".$res->{q_gene_id}." and s_gene_id = ".$res->{s_gene_id}." in table homologies is a duplicate regarding the columns q_gene_id, s_gene_id. For more details, please run the command :\nSELECT * FROM homologies WHERE q_gene_id = ".$res->{q_gene_id}." AND s_gene_id = ".$res->{s_gene_id}."\n";
	$OVERALL_FATAL_PB = $OVERALL_FATAL_PB."\n - The q_gene_id = ".$res->{q_gene_id}." and s_gene_id = ".$res->{s_gene_id}." in table homologies is a duplicate regarding the columns q_gene_id, s_gene_id. For more details, please run the command :\nSELECT * FROM homologies WHERE q_gene_id = ".$res->{q_gene_id}." AND s_gene_id = ".$res->{s_gene_id}."\n";
}
#print "** Test done with success: check all rows in homologies have no redundancy\n\n";
if ($count_redundancy_table_homo == 0) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> $count_redundancy_table_homo problems were detected\n";
}

my $count_found_link_table_align_pairs_2_homo = 0;
#TODO RETAB
=pod
#check each rows in table alignment_pairs have a correspondance in table homologies
print "\t\tChecking each rows type 1 or 2 in table alignment_pairs does exist in table homologies";
$st = $ORIGAMI::dbh->prepare("SELECT  a.*, b.q_gene_id, b.s_gene_id FROM alignment_pairs a LEFT JOIN homologies b on a.q_gene_id = b.q_gene_id AND a.s_gene_id = b.s_gene_id WHERE (a.type = 1 OR a.type = 2) AND ( b.q_gene_id IS NULL OR b.s_gene_id IS NULL)");
$st->execute or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	$count_found_link_table_align_pairs_2_homo++;
	print "\nThe q_gene_id = ".$res->{q_gene_id}." and s_gene_id = ".$res->{s_gene_id}." in table alignment_pairs are not found in the table homologies. For more details, please run the command :\nSELECT  a.* FROM alignment_pairs a LEFT JOIN homologies b on a.q_gene_id = b.q_gene_id AND a.s_gene_id = b.s_gene_id WHERE b.q_gene_id IS NULL OR b.s_gene_id IS NULL\n";
	$OVERALL_FATAL_PB = $OVERALL_FATAL_PB."\n - The q_gene_id = ".$res->{q_gene_id}." and s_gene_id = ".$res->{s_gene_id}." in table alignment_pairs are not found in the table homologies. For more details, please run the command :\nSELECT  a.* FROM alignment_pairs a LEFT JOIN homologies b on a.q_gene_id = b.q_gene_id AND a.s_gene_id = b.s_gene_id WHERE b.q_gene_id IS NULL OR b.s_gene_id IS NULL\n";
}
#print "** Test done with success: check each rows type 1 or 2 in table alignment_pairs does exist in table homologies.\n\n";
if ($count_found_link_table_align_pairs_2_homo == 0) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> $count_found_link_table_align_pairs_2_homo problems were detected\n";
}
=cut

#check each rows type 3, 4, 5 in table alignment_pairs does not exist in table homologies
print "\t\tChecking each rows type 3 in table alignment_pairs does not exist in table homologies";
$st = $ORIGAMI::dbh->prepare("SELECT  a.*, b.q_gene_id, b.s_gene_id FROM alignment_pairs a LEFT JOIN homologies b on a.q_gene_id = b.q_gene_id AND a.s_gene_id = b.s_gene_id WHERE a.type = 3 AND ( b.q_gene_id IS NOT NULL OR b.s_gene_id IS NOT NULL)");
$st->execute or die("Pb execute $!");
$count_found_link_table_align_pairs_2_homo = 0;
while ( my $res = $st->fetchrow_hashref() ) {
	$count_found_link_table_align_pairs_2_homo++;
	print "\nThe q_gene_id = ".$res->{q_gene_id}." and s_gene_id = ".$res->{s_gene_id}." in table alignment_pairs are found in the table homologies. For more details, please run the command :\nSELECT  a.*, b.q_gene_id, b.s_gene_id FROM alignment_pairs a LEFT JOIN homologies b on a.q_gene_id = b.q_gene_id AND a.s_gene_id = b.s_gene_id WHERE a.type = 3 AND ( b.q_gene_id IS NOT NULL OR b.s_gene_id IS NOT NULL)\n";
	$OVERALL_WARN_PB = $OVERALL_WARN_PB."\n - The q_gene_id = ".$res->{q_gene_id}." and s_gene_id = ".$res->{s_gene_id}." in table alignment_pairs are found in the table homologies. For more details, please run the command :\nSELECT  a.*, b.q_gene_id, b.s_gene_id FROM alignment_pairs a LEFT JOIN homologies b on a.q_gene_id = b.q_gene_id AND a.s_gene_id = b.s_gene_id WHERE a.type = 3 AND ( b.q_gene_id IS NOT NULL OR b.s_gene_id IS NOT NULL)\n";
}

#print "** Test done with success: check each rows type 3 in table alignment_pairs does not exist in table homologies.\n\n";
if ($count_found_link_table_align_pairs_2_homo == 0) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> $count_found_link_table_align_pairs_2_homo warnings were detected\n";
}


#test every element has an associated organism_id
print "\t\tChecking every element has an associated organism_id";
$st = $ORIGAMI::dbh->prepare("SELECT organism_id, accession FROM elements");
$st->execute or die("Pb execute $!");
my $count_pb_every_element_has_organism_id = 0;
while ( my $res = $st->fetchrow_hashref() ) {
	my $organism_id = $res->{organism_id};
	my $accession = $res->{accession};
	if ($organism_id == undef
	 || $organism_id < 0) {
		$count_pb_every_element_has_organism_id++;
		print "The accession = $accession is not associated with an organism_id in the table elements. For more details, please run the command :\nSELECT * FROM elements WHERE accnum = '$accession'\n";
		$OVERALL_FATAL_PB = $OVERALL_FATAL_PB."\n - The accession = $accession is not associated with an organism_id in the table elements. For more details, please run the command :\nSELECT * FROM elements WHERE accnum = '$accession'\n";
	}

}
#print "** Test done with success: every element has an associated organism_id.\n\n";
if ($count_pb_every_element_has_organism_id == 0) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> $count_pb_every_element_has_organism_id problems were detected\n";
}


# check if each gene has a locus_tag
print "\t\tChecking if each gene has a locus_tag";
my $count_pb_each_gene_has_locus_tag = 0;
foreach my $element_id_IT (keys(%element_id_2_accnum)) {
	my $accnum_IT = $element_id_2_accnum{$element_id_IT};
	$st = $ORIGAMI::dbh->prepare("SELECT a.gene_id, b.qualifier FROM genes a LEFT JOIN micado.qualifiers b on a.feature_id = b.code_feat WHERE a.element_id = $element_id_IT AND b.accession = '$accnum_IT' AND b.type_qual = 'locus_tag' AND b.qualifier IS NULL");
	$st->execute or die("Pb execute $!");
	while ( my $res = $st->fetchrow_hashref() ) {
		$count_pb_each_gene_has_locus_tag++;
		print "The gene_id = ".$res->{gene_id}." in table gene has no locus tag. For more details, please run the command :\nSELECT a.gene_id, b.qualifier FROM genes a LEFT JOIN micado.qualifiers b on a.feature_id = b.code_feat WHERE a.element_id = $element_id_IT AND b.accession = '$accnum_IT' AND b.type_qual = 'locus_tag' AND b.qualifier IS NULL\n";
		$OVERALL_WARN_PB = $OVERALL_WARN_PB."\n - The gene_id = ".$res->{gene_id}." in table gene has no locus tag. For more details, please run the command :\nSELECT a.gene_id, b.qualifier FROM genes a LEFT JOIN micado.qualifiers b on a.feature_id = b.code_feat WHERE a.element_id = $element_id_IT AND b.accession = '$accnum_IT' AND b.type_qual = 'locus_tag' AND b.qualifier IS NULL\n";
	}
}
#print "** Test done with success: each gene has a locus_tag.\n\n";
if ($count_pb_each_gene_has_locus_tag == 0) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> $count_pb_each_gene_has_locus_tag warning(s) were detected\n";
}

#check organisms that are not present in table alignment_params
print "\t\tChecking organisms that are not present in table alignment_params";
$st = $ORIGAMI::dbh->prepare("SELECT  a.*, b.q_organism_id, b.s_organism_id FROM organisms a LEFT JOIN alignment_params b on a.organism_id = b.q_organism_id WHERE b.q_organism_id IS NULL");
$st->execute or die("Pb execute $!");
my $count_pb_organisms_not_present_in_alignment_params = 0;
while ( my $res = $st->fetchrow_hashref() ) {
	$count_pb_organisms_not_present_in_alignment_params++;
	print "The organism_id = ".$res->{organism_id}." in table organisms is not found in the table alignment_params. For more details, please run the command :\nSELECT  a.*, b.q_organism_id, b.s_organism_id FROM organisms a LEFT JOIN alignment_params b on a.organism_id = b.q_organism_id WHERE b.q_organism_id IS NULL\n";
	$OVERALL_WARN_PB = $OVERALL_WARN_PB."\n - The organism_id = ".$res->{organism_id}." in table organisms is not found in the table alignment_params. For more details, please run the command :\nSELECT  a.*, b.q_organism_id, b.s_organism_id FROM organisms a LEFT JOIN alignment_params b on a.organism_id = b.q_organism_id WHERE b.q_organism_id IS NULL\n";
}
#print "** Test done with success: check organisms that are not present in table alignment_params.\n\n";
if ($count_pb_organisms_not_present_in_alignment_params == 0) {
	print "\t=> No problem detected\n";
} else {
	print "\t=> $count_pb_organisms_not_present_in_alignment_params warnings were detected\n";
}

#TODO test that there is no redundoncy in accession numbers


#end of script
#print  "\n---------------------------------------------------\n\n\n check_db_consistency.pl successfully completed at :",  scalar(localtime),  "\n\n---------------------------------------------------\n";
print "\t\t** Summary :\n";
if (length ($OVERALL_FATAL_PB) > 0 ) {
	print "\t\t=> The following major problem(s) were detected :\n$OVERALL_FATAL_PB\n. Your database is ** NOT ** stable\n.";
} else {
	print "\t\t=> No major problem was detected, your database is stable.\n";
}
if (length ($OVERALL_WARN_PB) > 0 ) {
	print "\t\t=> The following warning(s) were detected :\n$OVERALL_WARN_PB\n.";
} else {
	print "\t\t=> No warning was detected.\n";
}

#print LOG "\n\n";
#close(LOG);



