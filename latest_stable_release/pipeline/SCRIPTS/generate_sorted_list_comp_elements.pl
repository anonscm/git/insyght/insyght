#!/usr/local/bin/perl
#
# Script de construction et mise à jour de ORIGAMI
# Permet de calculer des listes ordonnés d'elements comparés selon différent critères (abundance_homologs, synteny_score, alignemnt_score, abundance_homo_annotations)
# et de générer les fichiers .sql qui serviront à peupler la table q_element_id_2_sorted_list_comp_orga_whole via le script integrate_sorted_list_comp_orga_whole.pl
# Date : 06/2015
#args :
# -DO_abundance_homologs
# -DO_synteny_score
# -DO_alignemnt_score
# -DO_abundance_homo_annotations
# -DO_ALL
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) :  Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-...), Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr),
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 
use strict;
use DBI;
use SiteConfig;
use ORIGAMI;

# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/compute/generate_sorted_list_comp_elements";#.log
my $path_step_done_file = "$SiteConfig::LOGDIR/generate_sorted_list_comp_elements.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/generate_sorted_list_comp_elements.error";
my $output_backtick = "";
my @list_elements_ids = ();
my %alignmentParamId2selementId = ();
my $VERBOSE = "ON";
my $DO_abundance_homologs = 0;
my $DO_synteny_score = 0;
my $DO_alignemnt_score = 0;
my $DO_abundance_homo_annotations = 0;
my $init_DO_abundance_homologs = 0;
my $init_DO_synteny_score = 0;
my $init_DO_alignemnt_score = 0;
my $init_DO_abundance_homo_annotations = 0;
my $SYNTENY_DATA_ARE_NO_MIRROR = "ON";
my $FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = -1;
my $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN = -1;
my $OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sorted_list_comp_orga_whole";
my $CONTINUE_MULTI_RUNS_COMPUTATION = "OFF";


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}



#args


foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-DO_abundance_homologs$/ ) {
		$init_DO_abundance_homologs = 1;
	} elsif ( $ARGV[$argnum] =~ m/^-DO_synteny_score$/ ) {
		$init_DO_synteny_score = 1;
	} elsif ( $ARGV[$argnum] =~ m/^-DO_alignemnt_score$/ ) {
		$init_DO_alignemnt_score = 1;
	} elsif ( $ARGV[$argnum] =~ m/^-DO_abundance_homo_annotations$/ ) {
		#$init_DO_abundance_homo_annotations = 1;
	} elsif ( $ARGV[$argnum] =~ m/^-DO_ALL$/ ) {
		$init_DO_abundance_homologs = 1;
		$init_DO_synteny_score = 1;
		$init_DO_alignemnt_score = 1;
		#$init_DO_abundance_homo_annotations = 1;
	}
	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl generate_sorted_list_comp_elements.pl -VERBOSE {ON, OFF}");
		}
	}
	if  ( $ARGV[$argnum] =~ m/^-CONTINUE_MULTI_RUNS_COMPUTATION$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$CONTINUE_MULTI_RUNS_COMPUTATION = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -CONTINUE_MULTI_RUNS_COMPUTATION argument ; usage : perl generate_sorted_list_comp_elements.pl -CONTINUE_MULTI_RUNS_COMPUTATION {ON, OFF}");
		}
	}
	if  ( $ARGV[$argnum] =~ m/^-SYNTENY_DATA_ARE_NO_MIRROR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$SYNTENY_DATA_ARE_NO_MIRROR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -SYNTENY_DATA_ARE_NO_MIRROR argument ; usage : perl generate_sorted_list_comp_elements.pl -SYNTENY_DATA_ARE_NO_MIRROR {ON, OFF}");
		}
	}
	if ( $ARGV[$argnum] =~ m/^-FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^(\d)$/i )
		{
			$FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = $1;
		}
		else {
			die_with_error_mssg ("incorrect -FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT argument ; usage : perl generate_sorted_list_comp_elements.pl -FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT {DIGIT}");
		}
	}
	if ( $ARGV[$argnum] =~ m/^-MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN = $ARGV[ $argnum + 1 ];
			if($MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN < 1){
				die_with_error_mssg ("incorrect -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN argument ; usage : perl generate_sorted_list_comp_elements.pl  -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN {int > 0}");
			}
		}
		else {
			die("incorrect -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN argument ; usage : perl generate_sorted_list_comp_elements.pl  -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN {int > 0}");
		}
	}
	if ( $ARGV[$argnum] =~ m/^-OUTPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -OUTPUT_DIR argument ; usage : perl generate_sorted_list_comp_elements.pl  -OUTPUT_DIR {PATH_TO_DIRECTORY}");
		}
	}

}


# on créé le répertoire pour les fichiers de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/compute`;
# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0){
	if (-e "${path_log_file}_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log"){
		die_with_error_mssg ("The log file ${path_log_file}_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log already exists and mode FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT is activated ; please make sure no other process are using the same FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT and is running in parrallele, else it will cause duplicated data. Then remove the log file  ${path_log_file}_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log\n");
	} else {
		open( LOG, ">${path_log_file}_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log"); #| perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
	}
} else {
	open( LOG, ">$path_log_file.log" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
}


print LOG "\n\n***Starting task generate_sorted_list_comp_elements at ",scalar(localtime),"\n\n" unless $VERBOSE =~ m/^OFF$/;

# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}


#sub init_DO_abundance_homologs {
sub init_DO_type_of_analysis { # abundance_homologs, synteny_score, alignemnt_score, abundance_homo_annotations

   	my ( $type_of_analysis ) = @_;

	# deal with dir
	`$SiteConfig::CMDDIR/mkdir -p $OUTPUT_DIR/`;
	if ($CONTINUE_MULTI_RUNS_COMPUTATION =~ m/^ON$/i) {
	} else {
		`$SiteConfig::CMDDIR/rm -f $OUTPUT_DIR/${type_of_analysis}*.sql`;
	}
	my $type_of_analysis_file = undef;
	if ( $FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0 ) {
		my $number_file = 0;
		while ( ! defined $type_of_analysis_file ) {
			if ( ! -e "$OUTPUT_DIR/${type_of_analysis}_ELET_ID_END_WITH_DIGIT_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}_INCRE_${number_file}.sql" ) {
				$type_of_analysis_file = "$OUTPUT_DIR/${type_of_analysis}_ELET_ID_END_WITH_DIGIT_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}_INCRE_${number_file}.sql";
			}
			$number_file++;
		}
	} else {
		my $number_file = 0;
		while ( ! defined $type_of_analysis_file ) {
			if ( ! -e "$OUTPUT_DIR/${type_of_analysis}_INCRE_${number_file}.sql" ) {
				$type_of_analysis_file = "$OUTPUT_DIR/${type_of_analysis}_INCRE_${number_file}.sql";
			}
			$number_file++;
		}
	}
	# open file
	if ( $type_of_analysis eq "abundance_homologs" ) {
		open( SQL_FILE_abundance_homologs, "> $type_of_analysis_file") or die_with_error_mssg("Can not open $type_of_analysis_file");
		print SQL_FILE_abundance_homologs "BEGIN WORK;\n";
		$DO_abundance_homologs = 1;
	} elsif ( $type_of_analysis eq "synteny_score" ) {
		open( SQL_FILE_synteny_score, "> $type_of_analysis_file") or die_with_error_mssg("Can not open $type_of_analysis_file");
		print SQL_FILE_synteny_score "BEGIN WORK;\n";
		$DO_synteny_score = 1;
	} elsif ( $type_of_analysis eq "alignemnt_score" ) {
		open( SQL_FILE_alignemnt_score, "> $type_of_analysis_file") or die_with_error_mssg("Can not open $type_of_analysis_file");
		print SQL_FILE_alignemnt_score "BEGIN WORK;\n";
		$DO_alignemnt_score = 1;
	} elsif ( $type_of_analysis eq "abundance_homo_annotations" ) {
		open( SQL_FILE_abundance_homo_annotations, "> $type_of_analysis_file") or die_with_error_mssg("Can not open $type_of_analysis_file");
		print SQL_FILE_abundance_homo_annotations "BEGIN WORK;\n";
		$DO_abundance_homo_annotations = 1;
	} else {
		die_with_error_mssg("incorrect type_of_analysis argument in method init_DO_type_of_analysis ; Should be : abundance_homologs, synteny_score, alignemnt_score, or abundance_homo_annotations");
	}

	
}

if ( $init_DO_abundance_homologs == 1) {
	#init_DO_abundance_homologs();
	init_DO_type_of_analysis("abundance_homologs");
}
if ( $init_DO_synteny_score == 1) {
	#init_DO_synteny_score();
	init_DO_type_of_analysis("synteny_score");
}
if ( $init_DO_alignemnt_score == 1) {
	#init_DO_alignemnt_score();
	init_DO_type_of_analysis("alignemnt_score");
}
if ( $init_DO_abundance_homo_annotations == 1) {
	#init_DO_abundance_homo_annotations();
	init_DO_type_of_analysis("abundance_homo_annotations");
}




sub sum_from_table_alignments {
	my ($column_table_alignments, $element_id) = @_;
	my @keys_alignmentParamId = keys %alignmentParamId2selementId;

	my $rs_list_alignment_param_id = $ORIGAMI::dbh->selectall_arrayref("SELECT alignment_param_id, SUM(".$column_table_alignments.") AS SUM FROM alignments WHERE alignment_param_id IN (".join(",", @keys_alignmentParamId).") GROUP BY alignment_param_id");
	my %alignmentParamId2cummulativeScore = ();
	foreach my $rs_alignment_param_id ( @{$rs_list_alignment_param_id} ) {
		my ( $alignment_param_id_2nd, $sum_pairs)= @{$rs_alignment_param_id};
		$alignmentParamId2cummulativeScore { $alignment_param_id_2nd } = $sum_pairs;
	}
	#crunch score by element
	my %eletId2finalScore = ();
        foreach my $keyAlignmentParamIdWithScore (keys %alignmentParamId2cummulativeScore) {
		my $cumScoreIT = $alignmentParamId2cummulativeScore { $keyAlignmentParamIdWithScore };
		if (exists $alignmentParamId2selementId { $keyAlignmentParamIdWithScore } ) {
			my $eletIdIT = $alignmentParamId2selementId { $keyAlignmentParamIdWithScore };
			if ( exists $eletId2finalScore { $eletIdIT } ) {
				my $currScoreForEletId = $eletId2finalScore { $eletIdIT };
				$eletId2finalScore { $eletIdIT } = ($cumScoreIT + $currScoreForEletId);
			} else {
				$eletId2finalScore { $eletIdIT } = $cumScoreIT;
			}
		} else {
			die_with_error_mssg("ERROR in sum_from_table_alignments colonne $column_table_alignments : no alignmentParamId2selementId.containsKey ". $keyAlignmentParamIdWithScore);
		}
        }
	#sortHashMapByValues
	my $sortedOrigamiIdAsText = "";
	my $count_sortedOrigamiIdAsText = 0;
    	foreach my $sorted_eletId (sort { $eletId2finalScore{$b} <=> $eletId2finalScore{$a} } ( keys %eletId2finalScore ) ) {
		if ($sorted_eletId == $element_id) {
			next;
		}
		my $sorted_eletId_then_score = $sorted_eletId."(s=".$eletId2finalScore{$sorted_eletId}.")";
		if ($count_sortedOrigamiIdAsText == 0) {
			$count_sortedOrigamiIdAsText = 1;
			$sortedOrigamiIdAsText = $sorted_eletId_then_score;
		} else {
			$sortedOrigamiIdAsText = $sortedOrigamiIdAsText.",".$sorted_eletId_then_score;
		}
    	}
	return $sortedOrigamiIdAsText;

}

#get list of all possible q_element_id
my $elements = $ORIGAMI::dbh->selectall_arrayref("SELECT element_id FROM elements");
my $count_skipped_does_not_end_filter_digit = 0;
my $count_skipped_max_accnum_to_charge = 0;
foreach my $element ( @{$elements} ) {
	my ( $element_id )= @{$element};
	if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0) {
		if ($element_id =~ m/^\d*$FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT$/) {
			if ( $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN  >= 0 ) {
				if ( scalar(@list_elements_ids) >= $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN) {
					$count_skipped_max_accnum_to_charge++;
				} else {
					push (@list_elements_ids, $element_id) ;
				}
			} else {
				push (@list_elements_ids, $element_id) ;
			}
		} else {
			$count_skipped_does_not_end_filter_digit++;
		}
	} else {
		if ( $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN  >= 0 ) {
			if ( scalar(@list_elements_ids) >= $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN) {
				$count_skipped_max_accnum_to_charge++;
			} else {
				push (@list_elements_ids, $element_id) ;
			}
		} else {
			push (@list_elements_ids, $element_id) ;
		}
	}
}

print LOG scalar(@list_elements_ids)." accnum / element_id will be treated in this run\n" unless $VERBOSE =~ m/^OFF$/;
if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0) {
	print LOG "$count_skipped_does_not_end_filter_digit accnum / element_id will be skipped because they do not match the -FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT option $FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT\n" unless $VERBOSE =~ m/^OFF$/;
}
if ( $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN  >= 0 ) {
	print LOG "$count_skipped_max_accnum_to_charge accnum / element_id will be skipped because the number of -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN has been reached.\n" unless $VERBOSE =~ m/^OFF$/;
}

my $count_elet_done = 0;
foreach my $element_id ( @list_elements_ids ) {
	$count_elet_done++;
	my $skip_abundance_homologs_for_this_accnum = undef;
	my $skip_synteny_score_for_this_accnum = undef;
	my $skip_alignemnt_score_for_this_accnum = undef;
	my $skip_abundance_homo_annotations_for_this_accnum = undef;

	print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : Starting element_id = $element_id\n" unless $VERBOSE =~ m/^OFF$/;

	if ($CONTINUE_MULTI_RUNS_COMPUTATION =~ m/^ON$/i) {
		my $output_cmd_grep = "";
		if ($DO_abundance_homologs == 1) {
			my ($abundance_homologs_already_inseted_in_db_for_elet_id) = $ORIGAMI::dbh->selectrow_array("select count(*) from q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = ".$element_id." AND abundance_homologs IS NOT NULL");
			if ($abundance_homologs_already_inseted_in_db_for_elet_id == 1) {
				print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : skipping computation of abundance_homologs for element_id $element_id because the flag CONTINUE_MULTI_RUNS_COMPUTATION is activated and this entry has been found in the database : select count(*) from q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = ".$element_id." AND abundance_homologs IS NOT NULL\n" unless $VERBOSE =~ m/^OFF$/;
				$skip_abundance_homologs_for_this_accnum = 1;
			} else {
				$output_cmd_grep = `grep \'INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,abundance_homologs) VALUES ($element_id,\' $OUTPUT_DIR/abundance_homologs*.sql`;
				chomp($output_cmd_grep);
				$output_cmd_grep =~ s/^\s+//;
				$output_cmd_grep =~ s/\s+$//;
				if($output_cmd_grep eq ""){
				} else {
					print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : skipping computation of abundance_homologs for element_id $element_id because the flag CONTINUE_MULTI_RUNS_COMPUTATION is activated and this entry has been found in $OUTPUT_DIR/. grep \'INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,abundance_homologs) VALUES ($element_id,\' $OUTPUT_DIR/abundance_homologs*.sql*\n" unless $VERBOSE =~ m/^OFF$/;
					$skip_abundance_homologs_for_this_accnum = 1;
				}
			}
		}
		if ( $DO_synteny_score == 1) {
			my ($synteny_score_already_inseted_in_db_for_elet_id) = $ORIGAMI::dbh->selectrow_array("select count(*) from q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = ".$element_id." AND synteny_score IS NOT NULL");
			if ($synteny_score_already_inseted_in_db_for_elet_id == 1) {
				print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : skipping computation of synteny_score for element_id $element_id because the flag CONTINUE_MULTI_RUNS_COMPUTATION is activated and this entry has been found in the database : select count(*) from q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = ".$element_id." AND synteny_score IS NOT NULL\n" unless $VERBOSE =~ m/^OFF$/;
				$skip_synteny_score_for_this_accnum = 1;
			} else {
				$output_cmd_grep = `grep \'INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,synteny_score) VALUES ($element_id,\' $OUTPUT_DIR/synteny_score*.sql`;
				chomp($output_cmd_grep);
				$output_cmd_grep =~ s/^\s+//;
				$output_cmd_grep =~ s/\s+$//;
				if($output_cmd_grep eq ""){
				} else {
					print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : skipping computation of synteny_score for element_id = $element_id because the flag CONTINUE_MULTI_RUNS_COMPUTATION is activated and this entry has been found in $OUTPUT_DIR/. grep \'INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,synteny_score) VALUES ($element_id,\' $OUTPUT_DIR/synteny_score*.sql\n" unless $VERBOSE =~ m/^OFF$/;
					$skip_synteny_score_for_this_accnum = 1;
				}
			}
			
		}
		if ( $DO_alignemnt_score == 1) {
			my ($alignemnt_score_already_inseted_in_db_for_elet_id) = $ORIGAMI::dbh->selectrow_array("select count(*) from q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = ".$element_id." AND alignemnt_score IS NOT NULL");
			if ($alignemnt_score_already_inseted_in_db_for_elet_id == 1) {
				print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : skipping computation of alignemnt_score for element_id $element_id because the flag CONTINUE_MULTI_RUNS_COMPUTATION is activated and this entry has been found in the database : select count(*) from q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = ".$element_id." AND alignemnt_score IS NOT NULL\n" unless $VERBOSE =~ m/^OFF$/;
				$skip_alignemnt_score_for_this_accnum = 1;
			} else {
				$output_cmd_grep = `grep \'INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,alignemnt_score) VALUES ($element_id,\' $OUTPUT_DIR/alignemnt_score*.sql`;
				chomp($output_cmd_grep);
				$output_cmd_grep =~ s/^\s+//;
				$output_cmd_grep =~ s/\s+$//;
				if($output_cmd_grep eq ""){
				} else {
					print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : skipping computation of alignemnt_score for element_id = $element_id because the flag CONTINUE_MULTI_RUNS_COMPUTATION is activated and this entry has been found in $OUTPUT_DIR/. grep \'INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,alignemnt_score) VALUES ($element_id,\' $OUTPUT_DIR/alignemnt_score*.sql\n" unless $VERBOSE =~ m/^OFF$/;
					$skip_alignemnt_score_for_this_accnum = 1;
				}
			}
		}
		if ( $DO_abundance_homo_annotations == 1) {
			my ($abundance_homo_annotations_already_inseted_in_db_for_elet_id) = $ORIGAMI::dbh->selectrow_array("select count(*) from q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = ".$element_id." AND abundance_homo_annotations IS NOT NULL");
			if ($abundance_homo_annotations_already_inseted_in_db_for_elet_id == 1) {
				print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : skipping computation of abundance_homo_annotations for element_id $element_id because the flag CONTINUE_MULTI_RUNS_COMPUTATION is activated and this entry has been found in the database : select count(*) from q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = ".$element_id." AND abundance_homo_annotations IS NOT NULL\n" unless $VERBOSE =~ m/^OFF$/;
				$skip_abundance_homo_annotations_for_this_accnum = 1;
			} else {
				$output_cmd_grep = `grep \'INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,abundance_homo_annotations) VALUES ($element_id,\' $OUTPUT_DIR/abundance_homo_annotations*.sql`;
				chomp($output_cmd_grep);
				$output_cmd_grep =~ s/^\s+//;
				$output_cmd_grep =~ s/\s+$//;
				if($output_cmd_grep eq ""){
				} else {
					print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : skipping computation of abundance_homo_annotations for element_id = $element_id because the flag CONTINUE_MULTI_RUNS_COMPUTATION is activated and this entry has been found in $OUTPUT_DIR/. grep \'INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,abundance_homo_annotations) VALUES ($element_id,\' $OUTPUT_DIR/abundance_homo_annotations*.sql\n" unless $VERBOSE =~ m/^OFF$/;
					$skip_abundance_homo_annotations_for_this_accnum = 1;
				}
			}
		}
	}


	%alignmentParamId2selementId = ();
	my $list_rs_alignment_param_id = $ORIGAMI::dbh->selectall_arrayref("SELECT alignment_params.s_element_id, alignment_params.alignment_param_id FROM alignment_params WHERE alignment_params.q_element_id=$element_id");
	foreach my $rs_alignment_param_id ( @{$list_rs_alignment_param_id} ) {
		my ( $s_element_id, $alignment_param_id)= @{$rs_alignment_param_id};
		if (exists $alignmentParamId2selementId { $alignment_param_id } ) {
			if ( $alignmentParamId2selementId { $alignment_param_id } != $s_element_id ) {
				die_with_error_mssg(" \%alignmentParamId2selementId  {  $alignment_param_id } already exists : ".$alignmentParamId2selementId { $alignment_param_id }." ; could not set value to $s_element_id ");
			}
		} else {
			$alignmentParamId2selementId { $alignment_param_id } = $s_element_id;
		}
	}

	if ($SYNTENY_DATA_ARE_NO_MIRROR =~ m/^ON$/i) {
		my $list_rs_alignment_param_id_mirror = $ORIGAMI::dbh->selectall_arrayref("SELECT alignment_params.q_element_id, alignment_params.alignment_param_id FROM alignment_params WHERE alignment_params.s_element_id=$element_id");
		foreach my $rs_alignment_param_id ( @{$list_rs_alignment_param_id_mirror} ) {
			my ( $s_element_id, $alignment_param_id)= @{$rs_alignment_param_id};
			if (exists $alignmentParamId2selementId { $alignment_param_id } ) {
				if ( $alignmentParamId2selementId { $alignment_param_id } != $s_element_id ) {
					die_with_error_mssg("SYNTENY_DATA_ARE_NO_MIRROR \%alignmentParamId2selementId  {  $alignment_param_id } already exists : ".$alignmentParamId2selementId { $alignment_param_id }." ; could not set value to $s_element_id ");
				}
			} else {
				$alignmentParamId2selementId { $alignment_param_id } = $s_element_id;
			}
		}
	}

	if ( ! %alignmentParamId2selementId) {
		print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : skipping computation of $element_id because no entry has been found in the table alignment_params.\n" unless $VERBOSE =~ m/^OFF$/;
		next ;
	}

	# deal with abundance_homologs
	if ($DO_abundance_homologs == 1 && ! defined $skip_abundance_homologs_for_this_accnum ) {
		print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : dealing with abundance_homologs.\n" unless $VERBOSE =~ m/^OFF$/;
		my $sortedOrigamiIdAsText_abundance_homologs = sum_from_table_alignments("pairs", $element_id);
		#print SQL_FILE_abundance_homologs "sortedOrigamiIdAsText abundance_homologs for elet $element_id = $sortedOrigamiIdAsText_abundance_homologs\n";
		print SQL_FILE_abundance_homologs "INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,abundance_homologs) VALUES ($element_id,'$sortedOrigamiIdAsText_abundance_homologs') ;\n";
	}


	# deal with synteny_score
	if ( $DO_synteny_score == 1 && ! defined $skip_synteny_score_for_this_accnum ) {
		print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : dealing with synteny_score.\n" unless $VERBOSE =~ m/^OFF$/;
		my $sortedOrigamiIdAsText_synteny_score = sum_from_table_alignments("score", $element_id);
		#print "sortedOrigamiIdAsText synteny_score for elet $element_id = $sortedOrigamiIdAsText_synteny_score\n";
		print SQL_FILE_synteny_score "INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,synteny_score) VALUES ($element_id,'$sortedOrigamiIdAsText_synteny_score') ;\n";
	}


	# deal with alignemnt_score
	if ( $DO_alignemnt_score == 1 && ! defined $skip_alignemnt_score_for_this_accnum ) {
		print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : dealing with alignemnt_score.\n" unless $VERBOSE =~ m/^OFF$/;
		my %s_element_id2alignemnt_score = ();
		my $rs_list_elet_id = $ORIGAMI::dbh->selectall_arrayref("SELECT s_element_id, SUM(homologies.score) as sum_score FROM homologies WHERE q_element_id = $element_id GROUP BY s_element_id");# ORDER BY SUM(homologies.score) DESC
		foreach my $rs_elet_id ( @{$rs_list_elet_id} ) {
			my ( $elet_id_2nd, $sum_score)= @{$rs_elet_id};
			if ($elet_id_2nd == $element_id) {
				next;
			}

			if ( exists $s_element_id2alignemnt_score{$elet_id_2nd} ) {
				my $previous_score = $s_element_id2alignemnt_score{$elet_id_2nd};
				$s_element_id2alignemnt_score{$elet_id_2nd} = $previous_score + $sum_score;
			} else {
				$s_element_id2alignemnt_score{$elet_id_2nd} = $sum_score;
			}
		}

		if ($SYNTENY_DATA_ARE_NO_MIRROR =~ m/^ON$/i) {
			my $rs_list_elet_id_bis = $ORIGAMI::dbh->selectall_arrayref("SELECT q_element_id, SUM(homologies.score) as sum_score FROM homologies WHERE s_element_id = $element_id GROUP BY q_element_id");# ORDER BY SUM(homologies.score) DESC
			foreach my $rs_elet_id ( @{$rs_list_elet_id_bis} ) {
				my ( $elet_id_2nd, $sum_score)= @{$rs_elet_id};
				if ($elet_id_2nd == $element_id) {
					next;
				}
				if ( exists $s_element_id2alignemnt_score{$elet_id_2nd} ) {
					my $previous_score = $s_element_id2alignemnt_score{$elet_id_2nd};
					$s_element_id2alignemnt_score{$elet_id_2nd} = $previous_score + $sum_score;
				} else {
					$s_element_id2alignemnt_score{$elet_id_2nd} = $sum_score;
				}
			}
		}

		# sort hash by value
		my @keys_sorted = reverse sort { $s_element_id2alignemnt_score{$a} <=> $s_element_id2alignemnt_score{$b} } keys %s_element_id2alignemnt_score;

		# print result
		my $sortedOrigamiIdAsText_alignemnt_score = "";
		my $count_sortedOrigamiIdAsText_alignemnt_score = 0;
		foreach my $s_element_id_IT2 ( @keys_sorted ) {
			my $alignemnt_score_IT2 = $s_element_id2alignemnt_score{$s_element_id_IT2};
			my $sorted_eletId_then_score = $s_element_id_IT2."(s=".$alignemnt_score_IT2.")";
			if ($count_sortedOrigamiIdAsText_alignemnt_score == 0) {
				$count_sortedOrigamiIdAsText_alignemnt_score = 1;
				$sortedOrigamiIdAsText_alignemnt_score = $sorted_eletId_then_score;
			} else {
				$sortedOrigamiIdAsText_alignemnt_score = $sortedOrigamiIdAsText_alignemnt_score.",".$sorted_eletId_then_score;
			}
		}
		print SQL_FILE_alignemnt_score "INSERT INTO q_element_id_2_sorted_list_comp_orga_whole (element_id,alignemnt_score) VALUES ($element_id,'$sortedOrigamiIdAsText_alignemnt_score') ;\n";
	}


	# deal with abundance_homo_annotations
	if ( $DO_abundance_homo_annotations == 1 && ! defined $skip_abundance_homo_annotations_for_this_accnum ) {
		print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : abundance_homo_annotations is not yet implemented.\n" unless $VERBOSE =~ m/^OFF$/;
		#TODO future
	}


	print LOG "( $count_elet_done / ".scalar(@list_elements_ids).") : Finished with element_id = $element_id\n" unless $VERBOSE =~ m/^OFF$/;


} #foreach my $element_id ( @list_elements_ids ) {


# print COMMIT WORK
if ($DO_abundance_homologs == 1) {
	print SQL_FILE_abundance_homologs "COMMIT WORK;\n";
	close(SQL_FILE_abundance_homologs);
}
if ( $DO_synteny_score == 1) {
	print SQL_FILE_synteny_score "COMMIT WORK;\n";
	close(SQL_FILE_synteny_score);
}
if ( $DO_alignemnt_score == 1) {
	print SQL_FILE_alignemnt_score "COMMIT WORK;\n";
	close(SQL_FILE_alignemnt_score);
}
if ( $DO_abundance_homo_annotations == 1) {
	print SQL_FILE_abundance_homo_annotations "COMMIT WORK;\n";
	close(SQL_FILE_abundance_homo_annotations);
}




#end of script
print LOG
"\n---------------------------------------------------\n\n\ngenerate_sorted_list_comp_elements.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);






