#!/usr/local/bin/perl
#
# full name = generate_correct_primary_key_launcher_files.pl
#
# This script generate bash script file that will be used to calculate primary keys
#
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use SiteConfig;
use BlastConfig;
use ORIGAMI;

# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/generate_correct_primary_key_launcher_files.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/generate_correct_primary_key_launcher_files.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/generate_correct_primary_key_launcher_files.error";
my $output_backtick = "";
my $VERBOSE = "ON";
my $TSV_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv";
my $LL_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit";
my $COUNT_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/count_files";
my $SQL_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sql";
my $LOG_DIR = "$SiteConfig::LOGDIR/Task_add_alignments";
my $DO_NOT_DEL_SQL_OUTPUT_DIR = "OFF";
my $INCREMENTAL_RUNS = "OFF";
my $CHECK_FIRST_LINE_SCORE_PARAMS = "ON";
my $MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN = undef;
my @input_count_files = ();
my $max_curr_alignment_param_id = undef;
my $max_curr_alignment_id = undef;
my $max_curr_tandem_dups_id = undef;
my $max_curr_prot_fusion_id = undef;
my $max_curr_close_best_match_id = undef;
my $max_next_alignment_param_id = undef;
my $max_next_alignment_id = undef;
my $max_next_tandem_dups_id = undef;
my $max_next_prot_fusion_id = undef;
my $max_next_close_best_match_id = undef;
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $molecule_type;
my $st;
my $res;

sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}



foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl generate_correct_primary_key_launcher_files.pl -VERBOSE = {ON, OFF}");
		}
#my $TSV_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv";
	} elsif ( $ARGV[$argnum] =~ m/^-TSV_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$TSV_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -TSV_INPUT_DIR argument ; usage : perl generate_correct_primary_key_launcher_files.pl  -TSV_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
#my $LL_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit";
	} elsif ( $ARGV[$argnum] =~ m/^-LL_OUTPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$LL_OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -LL_OUTPUT_DIR argument ; usage : perl generate_correct_primary_key_launcher_files.pl  -LL_OUTPUT_DIR {PATH_TO_DIRECTORY}");
		}
#my $COUNT_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/count_files";
	} elsif ( $ARGV[$argnum] =~ m/^-COUNT_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$COUNT_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -COUNT_INPUT_DIR argument ; usage : perl generate_correct_primary_key_launcher_files.pl  -COUNT_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
#my $SQL_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sql";
	} elsif ( $ARGV[$argnum] =~ m/^-SQL_OUTPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$SQL_OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -SQL_OUTPUT_DIR argument ; usage : perl generate_correct_primary_key_launcher_files.pl  -SQL_OUTPUT_DIR {PATH_TO_DIRECTORY}");
		}
#my $LOG_DIR = "$SiteConfig::LOGDIR/Task_add_alignments";
	} elsif ( $ARGV[$argnum] =~ m/^-LOG_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$LOG_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -LOG_DIR argument ; usage : perl generate_correct_primary_key_launcher_files.pl  -LOG_DIR {PATH_TO_DIRECTORY}");
		}
#my $DO_NOT_DEL_SQL_OUTPUT_DIR = "OFF"
	} elsif ( $ARGV[$argnum] =~ m/^-DO_NOT_DEL_SQL_OUTPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DO_NOT_DEL_SQL_OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -DO_NOT_DEL_SQL_OUTPUT_DIR argument ; usage : perl generate_correct_primary_key_launcher_files.pl -DO_NOT_DEL_SQL_OUTPUT_DIR = {ON, OFF}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-INCREMENTAL_RUNS$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$INCREMENTAL_RUNS = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -INCREMENTAL_RUNS argument ; usage : perl generate_correct_primary_key_launcher_files.pl -INCREMENTAL_RUNS = {ON, OFF}");
		}
#MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN
	} elsif ( $ARGV[$argnum] =~ m/^-MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN = $ARGV[ $argnum + 1 ];
			if($MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN < 1){
				die_with_error_mssg("incorrect -MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN argument ; usage : perl launch_count_primary_key_increment.pl  -MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN {int > 0}");
			}
		}
		else {
			die_with_error_mssg("incorrect -MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN argument ; usage : perl launch_count_primary_key_increment.pl  -MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN {int > 0}");
		}
# $CHECK_FIRST_LINE_SCORE_PARAMS = "ON";
	} elsif ( $ARGV[$argnum] =~ m/^-CHECK_FIRST_LINE_SCORE_PARAMS$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$CHECK_FIRST_LINE_SCORE_PARAMS = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -CHECK_FIRST_LINE_SCORE_PARAMS argument ; usage : perl launch_count_primary_key_increment.pl -CHECK_FIRST_LINE_SCORE_PARAMS = {ON, OFF}");
		}
	}
}
my $INCREMENTAL_RUNS_MASTER_FILE = "$LL_OUTPUT_DIR/incremental_runs_master_file.txt";


# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $LOG_DIR/generate_correct_primary_key_launcher_files.log");
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG "\n---------------------------------------------------\n generate_correct_primary_key_launcher_files.pl started at :", scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# print in and out directory
print LOG "---------------------------------------------------\nYou choose the following options : 
\t VERBOSE = $VERBOSE
\t TSV_INPUT_DIR = $TSV_INPUT_DIR
\t LL_OUTPUT_DIR = $LL_OUTPUT_DIR
\t COUNT_INPUT_DIR = $COUNT_INPUT_DIR
\t SQL_OUTPUT_DIR = $SQL_OUTPUT_DIR
\t LOG_DIR = $LOG_DIR
\t INCREMENTAL_RUNS = $INCREMENTAL_RUNS
\t DO_NOT_DEL_SQL_OUTPUT_DIR = $DO_NOT_DEL_SQL_OUTPUT_DIR
\t INCREMENTAL_RUNS_MASTER_FILE = $INCREMENTAL_RUNS_MASTER_FILE
\t MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN = $MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN
\n" unless $VERBOSE =~ m/^OFF$/;



if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
	#die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.\n");
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately\n");
}

# preparing directories
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::LOGDIR/Task_blast_all/`;
`$SiteConfig::CMDDIR/mkdir -p $LL_OUTPUT_DIR`;
`$SiteConfig::CMDDIR/mkdir -p $SQL_OUTPUT_DIR`;
`$SiteConfig::CMDDIR/mkdir -p $LOG_DIR`;
# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($INCREMENTAL_RUNS  =~ m/^ON$/ ) {
	print LOG "Do not delete files under $LL_OUTPUT_DIR/ as defined INCREMENTAL_RUNS = $INCREMENTAL_RUNS \n" unless $VERBOSE =~ m/^OFF$/;
} else {
	print LOG "Deleting files under $LL_OUTPUT_DIR/* \n" unless $VERBOSE =~ m/^OFF$/;
	$output_backtick .= `rm -rf $LL_OUTPUT_DIR/*`;
}
if ( $DO_NOT_DEL_SQL_OUTPUT_DIR  =~ m/^ON$/ ) {
	print LOG "Do not delete files under $SQL_OUTPUT_DIR/ as defined SQL_OUTPUT_DIR = $SQL_OUTPUT_DIR \n" unless $VERBOSE =~ m/^OFF$/;
} else {
	print LOG "Deleting files under $SQL_OUTPUT_DIR/* \n" unless $VERBOSE =~ m/^OFF$/;
	$output_backtick .= `rm -rf $SQL_OUTPUT_DIR/*`;
}

if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete preparing directories :\n$output_backtick \n $!");
}


my $count_input_count_files = 0;
my $count_input_count_gz_files = 0;
sub list_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die_with_error_mssg("Unable to open $path: $!");
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);

    for (@files) {
	if (-d $_) {
		# directory
		list_files_recursively($_);
        } elsif ($_ =~ m/^.+\/count_primary_key_increment_.+\.out$/ ) {
		if ( defined $MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN && scalar(@input_count_files) == $MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN ) {
			$count_input_count_files++;
		} else {
			push (@input_count_files, $_);
			$count_input_count_files++;
		}
        } elsif ($_ =~ m/^.+\/count_primary_key_increment_.+\.out\.gz$/ ) {
		# alraedy traeted, do nothing
		$count_input_count_gz_files++;
        } else {
            die_with_error_mssg("Error in list_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
print LOG "Listing input count files under $COUNT_INPUT_DIR/ ...\n" unless $VERBOSE =~ m/^OFF$/;
list_files_recursively("$COUNT_INPUT_DIR/");
print LOG "$count_input_count_files input count files found under $COUNT_INPUT_DIR/\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "$count_input_count_gz_files input count files gz found under $COUNT_INPUT_DIR/\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "\n".scalar(@input_count_files) . " .ll files will be treated...\n" unless $VERBOSE =~ m/^OFF$/;
if ( defined $MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN ) {
	print LOG "You chose the option -MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN $MAX_COUNT_INPUT_FILES_TO_TREAT_IN_THIS_RUN\n" unless $VERBOSE =~ m/^OFF$/;
}



print LOG "Calculating starter primary keys...\n" unless $VERBOSE =~ m/^OFF$/;
# get max id from the db
# get max current alignment_param_id
$st = $ORIGAMI::dbh->prepare("SELECT max(alignment_param_id) FROM alignment_params");
$st->execute or die_with_error_mssg("Pb execute $!");
while ( $res = $st->fetchrow_hashref() ) {
$max_curr_alignment_param_id = $res->{max};
}
if($max_curr_alignment_param_id <= 0){
$max_curr_alignment_param_id = 0;
}
$max_curr_alignment_param_id++;
#get max current alignments_id
$st = $ORIGAMI::dbh->prepare("SELECT max(alignment_id) FROM alignments");
$st->execute or die_with_error_mssg("Pb execute $!");
while ( $res = $st->fetchrow_hashref() ) {
$max_curr_alignment_id = $res->{max};
}
if($max_curr_alignment_id <= 0){
$max_curr_alignment_id = 0;
}
$max_curr_alignment_id++;
# $max_curr_tandem_dups_id
# -REPLACEMENT_TANDEM_DUPS_PRIMARY_ID
# "COPY tandem_dups (tandem_dups_id,
$st = $ORIGAMI::dbh->prepare("SELECT max(tandem_dups_id) FROM tandem_dups");
$st->execute or die_with_error_mssg("Pb execute $!");
while ( $res = $st->fetchrow_hashref() ) {
$max_curr_tandem_dups_id = $res->{max};
}
if($max_curr_tandem_dups_id <= 0){
$max_curr_tandem_dups_id = 0;
}
$max_curr_tandem_dups_id++;
# $max_curr_prot_fusion_id
# -REPLACEMENT_PROT_FUSION_PRIMARY_ID
# COPY prot_fusion ("prot_fusion_id
$st = $ORIGAMI::dbh->prepare("SELECT max(prot_fusion_id) FROM prot_fusion");
$st->execute or die_with_error_mssg("Pb execute $!");
while ( $res = $st->fetchrow_hashref() ) {
$max_curr_prot_fusion_id = $res->{max};
}
if($max_curr_prot_fusion_id <= 0){
$max_curr_prot_fusion_id = 0;
}
$max_curr_prot_fusion_id++;
# $max_curr_close_best_match_id
# -REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID
# COPY close_best_match (close_best_match_id
$st = $ORIGAMI::dbh->prepare("SELECT max(close_best_match_id) FROM close_best_match");
$st->execute or die_with_error_mssg("Pb execute $!");
while ( $res = $st->fetchrow_hashref() ) {
$max_curr_close_best_match_id = $res->{max};
}
if($max_curr_close_best_match_id <= 0){
$max_curr_close_best_match_id = 0;
}
$max_curr_close_best_match_id++;


# read INCREMENTAL_RUNS_MASTER_FILE
if ( -e "$INCREMENTAL_RUNS_MASTER_FILE" ) {
	# read file
	open( INCREMENTAL_RUNS_MASTER_FILE_FS, "<$INCREMENTAL_RUNS_MASTER_FILE" ) or die_with_error_mssg("Can not open $INCREMENTAL_RUNS_MASTER_FILE\n");
	while ( my $line = <INCREMENTAL_RUNS_MASTER_FILE_FS> ) {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		if ($line =~ m/^max_curr_alignment_param_id=(\d+)$/) {
			my $max_curr_alignment_param_id_from_master_file = $1;
			if ($max_curr_alignment_param_id_from_master_file < $max_curr_alignment_param_id) {
				die_with_error_mssg("max_curr_alignment_param_id_from_master_file $max_curr_alignment_param_id_from_master_file from file $INCREMENTAL_RUNS_MASTER_FILE is less than initial max_curr_alignment_param_id $max_curr_alignment_param_id.");
			}
			$max_curr_alignment_param_id = $max_curr_alignment_param_id_from_master_file;
		}
		if ($line =~ m/^max_curr_alignment_id=(\d+)$/) {
			my $max_curr_alignment_id_from_master_file = $1;
			if ($max_curr_alignment_id_from_master_file < $max_curr_alignment_id) {
				die_with_error_mssg("max_curr_alignment_id_from_master_file $max_curr_alignment_id_from_master_file from file $INCREMENTAL_RUNS_MASTER_FILE is less than initial max_curr_alignment_id $max_curr_alignment_id.");
			}
			$max_curr_alignment_id = $max_curr_alignment_id_from_master_file;
		}
		if ($line =~ m/^max_curr_tandem_dups_id=(\d+)$/) {
			my $max_curr_tandem_dups_id_from_master_file = $1;
			if ($max_curr_tandem_dups_id_from_master_file < $max_curr_tandem_dups_id) {
				die_with_error_mssg("max_curr_tandem_dups_id_from_master_file $max_curr_tandem_dups_id_from_master_file from file $INCREMENTAL_RUNS_MASTER_FILE is less than initial max_curr_tandem_dups_id $max_curr_tandem_dups_id.");
			}
			$max_curr_tandem_dups_id = $max_curr_tandem_dups_id_from_master_file;
		}
		if ($line =~ m/^max_curr_prot_fusion_id=(\d+)$/) {
			my $max_curr_prot_fusion_id_from_master_file = $1;
			if ($max_curr_prot_fusion_id_from_master_file < $max_curr_prot_fusion_id) {
				die_with_error_mssg("max_curr_prot_fusion_id_from_master_file $max_curr_prot_fusion_id_from_master_file from file $INCREMENTAL_RUNS_MASTER_FILE is less than initial max_curr_prot_fusion_id $max_curr_prot_fusion_id.");
			}
			$max_curr_prot_fusion_id = $max_curr_prot_fusion_id_from_master_file;
		}
		if ($line =~ m/^max_curr_close_best_match_id=(\d+)$/) {
			my $max_curr_close_best_match_id_from_master_file = $1;
			if ($max_curr_close_best_match_id_from_master_file < $max_curr_close_best_match_id) {
				die_with_error_mssg("max_curr_close_best_match_id_from_master_file $max_curr_close_best_match_id_from_master_file from file $INCREMENTAL_RUNS_MASTER_FILE is less than initial max_curr_close_best_match_id $max_curr_close_best_match_id.");
			}
			$max_curr_close_best_match_id = $max_curr_close_best_match_id_from_master_file;
		}
	}
	close(INCREMENTAL_RUNS_MASTER_FILE_FS);
} else {
	`touch $INCREMENTAL_RUNS_MASTER_FILE`;
}


sub write_update_incremental_runs_master_file {
	#my ($blabla) = @_; 

	open( RUN_MASTER_FILE_TMP, "> $INCREMENTAL_RUNS_MASTER_FILE.tmp" ) or die_with_error_mssg("Can not open $INCREMENTAL_RUNS_MASTER_FILE.tmp");
	print RUN_MASTER_FILE_TMP "max_curr_alignment_param_id=$max_curr_alignment_param_id\n";
	print RUN_MASTER_FILE_TMP "max_curr_alignment_id=$max_curr_alignment_id\n";
	print RUN_MASTER_FILE_TMP "max_curr_tandem_dups_id=$max_curr_tandem_dups_id\n";
	print RUN_MASTER_FILE_TMP "max_curr_prot_fusion_id=$max_curr_prot_fusion_id\n";
	print RUN_MASTER_FILE_TMP "max_curr_close_best_match_id=$max_curr_close_best_match_id\n";
	close(RUN_MASTER_FILE_TMP);
	$output_backtick = `mv $INCREMENTAL_RUNS_MASTER_FILE.tmp $INCREMENTAL_RUNS_MASTER_FILE`;
	if ($output_backtick eq "") {
		#ok
	} else {
		die_with_error_mssg("Error could not complete mv $INCREMENTAL_RUNS_MASTER_FILE.tmp $INCREMENTAL_RUNS_MASTER_FILE\n $!");#
	}

}

print LOG "Generating bash file...\n" unless $VERBOSE =~ m/^OFF$/;
my $count_loop_generate = 0;
foreach my $input_count_file_IT (@input_count_files) {

	$count_loop_generate++;
	print LOG "bash file $count_loop_generate / ".scalar(@input_count_files)."\n" unless $VERBOSE =~ m/^OFF$/;

	my $master_elet_id;
	my $sub_elet_id;
	if ( $input_count_file_IT =~ m/^.*\/count_primary_key_increment_(\d+)_and_(\d+)\.out$/ ) {
		$master_elet_id = $1;
		$sub_elet_id = $2;
	} else {
		die_with_error_mssg("could not parse both elet_id for file $input_count_file_IT.");
	}

	my $core_name_file = "${master_elet_id}_and_${sub_elet_id}";

	#parse input_count_file_IT
	my $count_alignment_params_file = 0;
	my $count_alignment_file = 0;
	my $count_tandem_dups_file = 0;
	my $count_prot_fusion_file = 0;
	my $count_closeBestMatchs_file = 0;
	my $marker_input_count_file_section = "";
	open( INPUT_COUNT_FILE_FS, "<$input_count_file_IT" ) or die_with_error_mssg("Can not open $input_count_file_IT\n");
	while ( my $line = <INPUT_COUNT_FILE_FS> ) {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		if ( $marker_input_count_file_section eq "count_alignment_params_file" ) {
			if ($line =~ m/^\d+$/) {
				$count_alignment_params_file = $line;
				$marker_input_count_file_section = "";
			} else {
				die_with_error_mssg("Unrecognized section count_alignment_params_file for line = $line\nin file $input_count_file_IT.\n");
			}
		} elsif ( $marker_input_count_file_section eq "count_alignment_file" ) {
			if ($line =~ m/^\d+$/) {
				$count_alignment_file = $line;
				$marker_input_count_file_section = "";
			} else {
				die_with_error_mssg("Unrecognized section count_alignment_file for line = $line\nin file $input_count_file_IT.\n");
			}
		} elsif ( $marker_input_count_file_section eq "count_tandem_dups_file" ) {
			if ($line =~ m/^\d+$/) {
				$count_tandem_dups_file = $line;
				$marker_input_count_file_section = "";
			} else {
				die_with_error_mssg("Unrecognized section count_tandem_dups_file for line = $line\nin file $input_count_file_IT.\n");
			}
		} elsif ( $marker_input_count_file_section eq "count_prot_fusion_file" ) {
			if ($line =~ m/^\d+$/) {
				$count_prot_fusion_file = $line;
				$marker_input_count_file_section = "";
			} else {
				die_with_error_mssg("Unrecognized section count_prot_fusion_file for line = $line\nin file $input_count_file_IT.\n");
			}
		} elsif ( $marker_input_count_file_section eq "count_closeBestMatchs_file" ) {
			if ($line =~ m/^\d+$/) {
				$count_closeBestMatchs_file = $line;
				$marker_input_count_file_section = "";
			} else {
				die_with_error_mssg("Unrecognized section count_closeBestMatchs_file for line = $line\nin file $input_count_file_IT.\n");
			}
		} elsif ( $marker_input_count_file_section eq "" ) {
			if ( $line eq "" ) {
				#print LOG "skipping line $line in $input_count_file_IT as it starts with a comment or empty\n" unless $VERBOSE =~ m/^OFF$/;
				next;
			} elsif ( $line eq "count_alignment_params_file :") {
				$marker_input_count_file_section = "count_alignment_params_file";
			} elsif ( $line eq "count_alignment_file :") {
				$marker_input_count_file_section = "count_alignment_file";
			} elsif ( $line eq "count_tandem_dups_file :") {
				$marker_input_count_file_section = "count_tandem_dups_file";
			} elsif ( $line eq "count_prot_fusion_file :") {
				$marker_input_count_file_section = "count_prot_fusion_file";
			} elsif ( $line eq "count_closeBestMatchs_file :") {
				$marker_input_count_file_section = "count_closeBestMatchs_file";
			} else {
				die_with_error_mssg("Unrecognized marker_input_count_file_section for line = $line\nin file $input_count_file_IT.\n");
			}
		} else {
			die_with_error_mssg("Unrecognized marker_input_count_file_section = $marker_input_count_file_section\n");
		}

	}
	close(INPUT_COUNT_FILE_FS);

	# calculate max next
	$max_next_alignment_param_id = $max_curr_alignment_param_id + $count_alignment_params_file - 4; #-4 for header and footer
	$max_next_alignment_id = $max_curr_alignment_id + $count_alignment_file - 4; #-4 for header and footer
	$max_next_tandem_dups_id = $max_curr_tandem_dups_id + $count_tandem_dups_file - 4; #-4 for header and footer
	$max_next_prot_fusion_id = $max_curr_prot_fusion_id + $count_prot_fusion_file - 4; #-4 for header and footer
	$max_next_close_best_match_id = $max_curr_close_best_match_id + $count_closeBestMatchs_file - 4; #-4 for header and footer
	if ($max_next_alignment_param_id < 1) {
		$max_next_alignment_param_id = 1;
	}
	if ($max_next_alignment_id < 1) {
		$max_next_alignment_id = 1;
	}
	if ($max_next_tandem_dups_id < 1) {
		$max_next_tandem_dups_id = 1;
	}
	if ($max_next_prot_fusion_id < 1) {
		$max_next_prot_fusion_id = 1;
	}
	if ($max_next_close_best_match_id < 1) {
		$max_next_close_best_match_id = 1;
	}


	#get associated alignment_params_table.tsv file
	my $associated_alignment_params_table_tsv_file = "$TSV_INPUT_DIR/alignment_params/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_params_table.tsv";
	if ( -e "$associated_alignment_params_table_tsv_file" ) {
		# do nothing
	} elsif ( -e "${associated_alignment_params_table_tsv_file}.gz" ) {
		$associated_alignment_params_table_tsv_file .= ".gz";
	} else {
		die_with_error_mssg("could not associate file $associated_alignment_params_table_tsv_file (or its gzip version).");
	}

	#get associated alignment_table.tsv file
	my $associated_alignment_table_tsv_file = "$TSV_INPUT_DIR/alignments/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_table.tsv";
	if ( -e "$associated_alignment_table_tsv_file" ) {
		# do nothing
	} elsif ( -e "${associated_alignment_table_tsv_file}.gz" ) {
		$associated_alignment_table_tsv_file .= ".gz";
	} else {
		die_with_error_mssg("could not associate file $associated_alignment_table_tsv_file (or its gzip version).");
	}


	#get associated alignment_pairs_table_tsv_file file
	my $associated_alignment_pairs_table_tsv_file = "$TSV_INPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_pairs_table.tsv";
	if ( -e "$associated_alignment_pairs_table_tsv_file" ) {
		# do nothing
	} elsif ( -e "${associated_alignment_pairs_table_tsv_file}.gz" ) {
		$associated_alignment_pairs_table_tsv_file .= ".gz";
	} else {
		die_with_error_mssg("could not associate file $associated_alignment_pairs_table_tsv_file (or its gzip version).");
	}


	# *_tandem_dups_table.tsv.gz
	# "COPY tandem_dups (tandem_dups_id, alignment_param_id, gene_id_single_is_q, single_organims_id, single_element_id, single_gene_id, tandem_organims_id, tandem_element_id, tandem_gene_id, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, idx_in_dup) FROM stdin;\n"
	# alignment_param_id
	# -REPLACEMENT_TANDEM_DUPS_PRIMARY_ID
	#get associated alignment_table.tsv file
	my $associated_tandem_dups_table_tsv_file = "$TSV_INPUT_DIR/tandem_dups/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_tandem_dups_table.tsv";
	my $option_tandem_dups_table_tsv_file = "";
	if ( -e "$associated_tandem_dups_table_tsv_file" ) {
		$option_tandem_dups_table_tsv_file = "-TANDEM_DUPS_INPUT_FILE $associated_tandem_dups_table_tsv_file";
	} elsif ( -e "${associated_tandem_dups_table_tsv_file}.gz" ) {
		$associated_tandem_dups_table_tsv_file .= ".gz";
		$option_tandem_dups_table_tsv_file = "-TANDEM_DUPS_INPUT_FILE ${associated_tandem_dups_table_tsv_file}";
	} else {
		$associated_tandem_dups_table_tsv_file = "";
		# not mandatory 
	}


	# _isBranchedToAnotherSynteny_table.tsv
	# COPY isBranchedToAnotherSynteny (alignment_id_branched, alignment_param_id, branche_on_q_gene_id, branche_on_s_gene_id) FROM stdin;\n";
	# alignment_id_branched
	# alignment_param_id
	my $associated_isBranchedToAnotherSynteny_table_tsv_file = "$TSV_INPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_isBranchedToAnotherSynteny_table.tsv";
	my $option_isBranchedToAnotherSynteny_table_tsv_file = "";
	if ( -e "$associated_isBranchedToAnotherSynteny_table_tsv_file" ) {
		$option_isBranchedToAnotherSynteny_table_tsv_file = "-ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE $associated_isBranchedToAnotherSynteny_table_tsv_file";
	} elsif ( -e "${associated_isBranchedToAnotherSynteny_table_tsv_file}.gz" ) {
		$associated_isBranchedToAnotherSynteny_table_tsv_file .= ".gz";
		$option_isBranchedToAnotherSynteny_table_tsv_file = "-ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE ${associated_isBranchedToAnotherSynteny_table_tsv_file}";
	} else {
		$associated_isBranchedToAnotherSynteny_table_tsv_file = "";
		# not mandatory
	}


	# output gene fusion (_protFusion_table.tsv)  
	# COPY prot_fusion ("prot_fusion_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, isMirrorData, s_rank_among_all_s_fusions, lengthTotalCoverageSOnQ, lenghtNewContribCoverageSOnQ, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, geneIdBestSingleLongMatchHidingProtFusion, numberSingleLongMatchHidingProtFusion, numberProtFusionDetectedBeforeThisOne
	# -REPLACEMENT_PROT_FUSION_PRIMARY_ID
	my $associated_prot_fusion_table_tsv_file = "$TSV_INPUT_DIR/protFusion/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_protFusion_table.tsv";
	my $option_prot_fusion_table_tsv_file = "";
	if ( -e "$associated_prot_fusion_table_tsv_file" ) {
		$option_prot_fusion_table_tsv_file = "-PROT_FUSION_INPUT_FILE $associated_prot_fusion_table_tsv_file";
	} elsif ( -e "${associated_prot_fusion_table_tsv_file}.gz" ) {
		$associated_prot_fusion_table_tsv_file .= ".gz";
		$option_prot_fusion_table_tsv_file = "-PROT_FUSION_INPUT_FILE ${associated_prot_fusion_table_tsv_file}";
	} else {
		$associated_prot_fusion_table_tsv_file = "";
		# not mandatory
	}


	# print out gene families where multiple match within 1% of best bits score (_closeBestMatchs_table.tsv)  
	# COPY close_best_match (close_best_match_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, is_mirror_data, number_of_close_matchs, contain_bdbh, pid_best_match, evalue_best_match, bits_best_match, list_close_match_gene_ids") FROM stdin;
	# -REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID
	my $associated_closeBestMatchs_table_tsv_file = "$TSV_INPUT_DIR/closeBestMatchs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_closeBestMatchs_table.tsv";
	my $option_closeBestMatchs_table_tsv_file = "";
	if ( -e "$associated_closeBestMatchs_table_tsv_file" ) {
		$option_closeBestMatchs_table_tsv_file = "-CLOSEBESTMATCHS_INPUT_FILE $associated_closeBestMatchs_table_tsv_file";
	} elsif ( -e "${associated_closeBestMatchs_table_tsv_file}.gz" ) {
		$associated_closeBestMatchs_table_tsv_file .= ".gz";
		$option_closeBestMatchs_table_tsv_file = "-CLOSEBESTMATCHS_INPUT_FILE ${associated_closeBestMatchs_table_tsv_file}";
	} else {
		$associated_closeBestMatchs_table_tsv_file = "";
		# not mandatory
	}



	`$SiteConfig::CMDDIR/mkdir -p $LL_OUTPUT_DIR/${molecule_type}_${master_elet_id}/`;

	#print LOG "print batchfile\n" unless $VERBOSE =~ m/^OFF$/;

	open( BATCHFILE,
	"> $LL_OUTPUT_DIR/${molecule_type}_${master_elet_id}/correct_primary_key_${core_name_file}.ll"
	  )
	  or die_with_error_mssg("Can not open $LL_OUTPUT_DIR/${molecule_type}_${master_elet_id}/correct_primary_key_${core_name_file}.ll");


	print BATCHFILE "#!/bin/bash
#\$ -S /bin/bash
# (n) No mail is sent ; Avertir au début (b)egin, à la fin (e)nd, à l'éliminaton (a)bort et
# à la suspension (s)uspend d'un job
#\$ -m n
# Adresse mail à laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#\$ -o $LOG_DIR/correct_primary_key_${core_name_file}.out
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -e $LOG_DIR/correct_primary_key_${core_name_file}.error
# Lance la commande depuis le répertoire où est lancé le script
#\$ -cwd
\n";

	print BATCHFILE "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -CORE_INPUT_DIR $TSV_INPUT_DIR -ALIGN_PARAM_INPUT_FILE $associated_alignment_params_table_tsv_file -ALIGNMENT_INPUT_FILE $associated_alignment_table_tsv_file -ALIGN_PAIRS_INPUT_FILE $associated_alignment_pairs_table_tsv_file $option_tandem_dups_table_tsv_file $option_isBranchedToAnotherSynteny_table_tsv_file $option_prot_fusion_table_tsv_file $option_closeBestMatchs_table_tsv_file -REPLACEMENT_ALIGN_PARAM_PRIMARY_ID $max_curr_alignment_param_id -REPLACEMENT_ALIGNMENT_PRIMARY_ID $max_curr_alignment_id -REPLACEMENT_TANDEM_DUPS_PRIMARY_ID $max_curr_tandem_dups_id -REPLACEMENT_PROT_FUSION_PRIMARY_ID $max_curr_prot_fusion_id -REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID $max_curr_close_best_match_id -CORE_OUTPUT_DIR $SQL_OUTPUT_DIR -LOG_DIR $LOG_DIR -CHECK_FIRST_LINE_SCORE_PARAMS $CHECK_FIRST_LINE_SCORE_PARAMS && $SiteConfig::CMDDIR/touch $LOG_DIR/correct_primary_key_${core_name_file}.done\n";

	#close BATCHFILE
	close(BATCHFILE);

	# $max_curr becomes $max_next
	$max_curr_alignment_param_id = $max_next_alignment_param_id;
	$max_curr_alignment_id = $max_next_alignment_id;
	$max_curr_tandem_dups_id = $max_next_tandem_dups_id;
	$max_curr_prot_fusion_id = $max_next_prot_fusion_id;
	$max_curr_close_best_match_id = $max_next_close_best_match_id;
	$max_next_alignment_param_id = undef;
	$max_next_alignment_id = undef;
	$max_next_tandem_dups_id = undef;
	$max_next_prot_fusion_id = undef;
	$max_next_close_best_match_id = undef;

	write_update_incremental_runs_master_file();

	# gziping file input_count_file_IT
	`gzip $input_count_file_IT`;


}


#end of script
print LOG
"\n---------------------------------------------------\n generate_correct_primary_key_launcher_files.pl successfully completed at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);



