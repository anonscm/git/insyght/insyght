#!/usr/local/bin/perl
#
# apply_cmd_to_files_recursively_in_dir.pl
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014-2015)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use Time::HiRes qw(sleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;

# whole script scoped variable
my $CMD = undef;
my $INPUT_DIR = undef;
my $EXT_FILE = undef;
my $REMOVE_EXT_FROM_FILE_NAME = "OFF";
my $output_backtick = "";

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/apply_cmd_to_files_recursively_in_dir.log"
);
print LOG
"\n---------------------------------------------------\n apply_cmd_to_files_recursively_in_dir.pl started at :",
  scalar(localtime), "\n";



foreach my $argnum ( 0 .. $#ARGV ) {

	if ( $ARGV[$argnum] =~ m/^-INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -INPUT_DIR argument ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -INPUT_DIR {PATH_TO_DIRECTORY}\n";
			die
"incorrect -INPUT_DIR argument ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -INPUT_DIR {PATH_TO_DIRECTORY}";
		}
	} elsif ( $ARGV[$argnum] =~ m/^-CMD$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$CMD = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -CMD argument ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -CMD {CMD}\n";
			die
"incorrect -CMD argument ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -CMD {CMD}";
		}
	} elsif ( $ARGV[$argnum] =~ m/^-EXT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$EXT_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -EXT_FILE argument ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -EXT_FILE {EXT_FILE}\n";
			die
"incorrect -EXT_FILE argument ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -EXT_FILE {EXT_FILE}";
		}
	} elsif  ( $ARGV[$argnum] =~ m/^-REMOVE_EXT_FROM_FILE_NAME$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$REMOVE_EXT_FROM_FILE_NAME = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -REMOVE_EXT_FROM_FILE_NAME argument ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -REMOVE_EXT_FROM_FILE_NAME {ON, OFF}";
			die
"incorrect -REMOVE_EXT_FROM_FILE_NAME argument ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -REMOVE_EXT_FROM_FILE_NAME {ON, OFF}";
		}
	}
}



if (! defined $INPUT_DIR) {
	print LOG "Undefined INPUT_DIR ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -INPUT_DIR {PATH_TO_DIRECTORY}\n";
	die "Undefined INPUT_DIR ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -INPUT_DIR {PATH_TO_DIRECTORY}";
}
if (! defined $CMD) {
	print LOG "Undefined CMD ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -CMD {CMD}\n";
	die "Undefined CMD ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -CMD {CMD}";
}
if (! defined $EXT_FILE) {
	print LOG "Undefined EXT_FILE ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -EXT_FILE {EXT_FILE}\n";
	die "Undefined EXT_FILE ; usage : perl apply_cmd_to_files_recursively_in_dir.pl -EXT_FILE {EXT_FILE}";
}
if ($CMD eq "gzip" || $CMD eq "rm" || $CMD eq "mv"  || $CMD eq "gunzip" ) {
	#ok supported
} else {
        print LOG "Unsupported CMD : $CMD ; Supported CMD so far are gzip or rm or mv or gunzip\n";
        die("Unsupported CMD : $CMD ; Supported CMD so far are gzip or rm or mv or gunzip\n");
}

sub sub_apply_cmd_to_files_recursively_in_dir {
    my $path = shift;
    opendir (DIR, $path) or die "Unable to open $path: $!";
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_apply_cmd_to_files_recursively_in_dir ($_);
        } elsif ($_ =~ m/^(.+\/.+)\.${EXT_FILE}$/ ) {
		my $file_name_IT_pre = $1;
		my $file_name_IT;
		if ($REMOVE_EXT_FROM_FILE_NAME =~ m/^ON$/i) {
			#TODO DEL
			#print "mv $_ $file_name_IT_pre\n";
			$output_backtick = $output_backtick.`mv $_ $file_name_IT_pre`;
			if ($output_backtick eq "") {
				#ok
			} else {
				print LOG "Error could not complete $SiteConfig::CMDDIR/mv files : $output_backtick\n";
				die("Error could not complete $SiteConfig::CMDDIR/mv files : $output_backtick\n");
			}
			$file_name_IT = "${file_name_IT_pre}";
		} else {
			$file_name_IT = "${file_name_IT_pre}\.${EXT_FILE}";
		}


		if ($CMD eq "gzip" || $CMD eq "gunzip" ) {
			$output_backtick = `$CMD $file_name_IT`;
			if ($output_backtick eq "") {
				print LOG "done $CMD $file_name_IT\n";
			} else {
				print LOG "Error $CMD $file_name_IT : $output_backtick\n";
				die ( "Error $CMD $file_name_IT : $output_backtick\n" );
			}

		} elsif ($CMD eq "rm") {
			$output_backtick = `rm -f $file_name_IT`;
			if ($output_backtick eq "") {
				print LOG "done rm -f  $file_name_IT\n";
			} else {
				print LOG "Error rm -f  $file_name_IT : $output_backtick\n";
				die ( "Error rm -f  $file_name_IT : $output_backtick\n" );
			}
		} elsif ($CMD eq "mv") {
			print LOG "mv $file_name_IT done\n";
			# do nothing, remove extension of file name already done if asked for
		} else {
            		print LOG "Unsupported CMD : $CMD ; Supported CMD are gzip or rm or mv or gunzip\n";
            		die("Unsupported CMD : $CMD ; Supported CMD are gzip or rm or mv or gunzip\n");
		}
        } else {
	    #do nothing
            #print LOG "Error in sub_list_alignment_params_files_recursively : the file $_ does not match the appropriate regex";
            #die("Error in sub_list_alignment_params_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}


sub_apply_cmd_to_files_recursively_in_dir("$INPUT_DIR");

#end of script
print LOG
"\n---------------------------------------------------\n\n\n apply_cmd_to_files_recursively_in_dir.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);



