#!/usr/bin/perl
#
# Task_blast_all_generator_for_IDRIS.pl : Script principal permettant de créer tous les scripts shell pour l'IDRIS concernant le blast de Origami
#
# Example command generate file for IDRIS : perl Task_blast_all_generator_for_IDRIS.pl -GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE ON
#
#
# Date : 01/2014
# LT
#
#LOG FILE : $SiteConfig::LOGDIR/Blast/generate_script_IDRIS_Task_blast_all.log
#LAUNCHED AS : perl Task_blast_all_generator_for_IDRIS.pl -VERBOSE {ON, OFF}
#ARGUMENTS :
#VERBOSE : if ON print ant details at each step, if OFF only print critical information
#
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 


use strict;
use Time::HiRes qw(sleep);
use SiteConfig;
use lib '$SiteConfig::SCRIPTSDIR';
use BlastConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;
use Find_new_old_molecules;

# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR//Task_blast_all/Task_blast_all_generator_for_IDRIS.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/Task_blast_all_generator_for_IDRIS.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/Task_blast_all_generator_for_IDRIS.error";
my $output_backtick = "";
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $st;
my $VERBOSE = "ON";
my @list_new_molecule_ids		            = ();
my @list_old_molecule_ids		            = ();
my %molecule_id_2_size		            = ();
my %element_id2orga_id		            = ();
my $molecule_type;
my $prefix_new_old_fasta_query = "";
my $prefix_new_old_blast_bank = "";
my %print_line_MAKEBLAST_BATCHFILE_FOR_IDRIS             = ();
my $GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE = "OFF";
my $NO_PARALOG = "OFF";
my $cmd_out = "";
my $program_to_use = "BLAST"; # can be BLAST or PLAST ; set by option -PROGRAM_TO_USE
my $number_processors_to_use_per_blast = 1;# -a for plast ; -num_threads for blastp ; set by option -NUM_PROC_PER_BLAST_PS
my $GENERATE_WITH_ENVIRONENT_VARIABLES = "OFF";
my $EXPORT_QSUB_LOG_DIR = "";
my $EXPORT_PLAST_EXEC_PATH = "";
my $EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR = "";
my $EXPORT_BLAST_OUTPUT_DIR = "";
my $EXPORT_MARKER_DONE_BLAST_DIR = "";



sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


#set the variables according to the argument or exit with usage if the arguments are incorrect
foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE {ON, OFF}";
			die_with_error_mssg ("incorrect -GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE {ON, OFF}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-PROGRAM_TO_USE/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^BLAST$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^PLAST$/i )
		{
			$program_to_use = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -PROGRAM_TO_USE argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -PROGRAM_TO_USE {BLAST, PLAST}";
			die_with_error_mssg ("incorrect -PROGRAM_TO_USE argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -PROGRAM_TO_USE {BLAST, PLAST}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-NUM_PROC_PER_BLAST_PS/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^\d+$/i )
		{
			$number_processors_to_use_per_blast = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -NUM_PROC_PER_BLAST_PS argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -NUM_PROC_PER_BLAST_PS {Integer}";
			die_with_error_mssg ("incorrect -NUM_PROC_PER_BLAST_PS argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -NUM_PROC_PER_BLAST_PS {Integer}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -VERBOSE argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl  -VERBOSE = {ON, OFF}";
			die_with_error_mssg ("incorrect -VERBOSE argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl  -VERBOSE {ON, OFF}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-NO_PARALOG$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$NO_PARALOG = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -NO_PARALOG argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl  -NO_PARALOG = {ON, OFF}";
			die_with_error_mssg ("incorrect -NO_PARALOG argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl  -NO_PARALOG {ON, OFF}");
		}
	}
# GENERATE_WITH_ENVIRONENT_VARIABLES
	if ( $ARGV[$argnum] =~ m/^-GENERATE_WITH_ENVIRONENT_VARIABLES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$GENERATE_WITH_ENVIRONENT_VARIABLES = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -GENERATE_WITH_ENVIRONENT_VARIABLES argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -GENERATE_WITH_ENVIRONENT_VARIABLES {ON, OFF}");
		}
	}
# EXPORT_QSUB_LOG_DIR
	if ( $ARGV[$argnum] =~ m/^-EXPORT_QSUB_LOG_DIR$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_QSUB_LOG_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_QSUB_LOG_DIR argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -EXPORT_QSUB_LOG_DIR {ENVIRONENT_VARIABLE}");
		}
	}
# EXPORT_PLAST_EXEC_PATH
	if ( $ARGV[$argnum] =~ m/^-EXPORT_PLAST_EXEC_PATH$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_PLAST_EXEC_PATH = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_PLAST_EXEC_PATH argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -EXPORT_PLAST_EXEC_PATH {ENVIRONENT_VARIABLE}");
		}
	}
# EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR
	if ( $ARGV[$argnum] =~ m/^-EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR {ENVIRONENT_VARIABLE}");
		}
	}
# EXPORT_BLAST_OUTPUT_DIR
	if ( $ARGV[$argnum] =~ m/^-EXPORT_BLAST_OUTPUT_DIR$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_BLAST_OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_BLAST_OUTPUT_DIR argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -EXPORT_BLAST_OUTPUT_DIR {ENVIRONENT_VARIABLE}");
		}
	}
# EXPORT_MARKER_DONE_BLAST_DIR = "";
	if ( $ARGV[$argnum] =~ m/^-EXPORT_MARKER_DONE_BLAST_DIR$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^[^-].+$/i )
		{
			$EXPORT_MARKER_DONE_BLAST_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -EXPORT_MARKER_DONE_BLAST_DIR argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl -EXPORT_MARKER_DONE_BLAST_DIR {ENVIRONENT_VARIABLE}");
		}
	}
}




# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/launch_blast/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_generator_for_IDRIS.log");
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG "\n---------------------------------------------------\n Task_blast_all_generator_for_IDRIS.pl started at :",  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}


if (defined $VERBOSE ) {
	print LOG "\nYou choose the following options :\n\tVERBOSE : $VERBOSE\n" unless $VERBOSE =~ m/^OFF$/;
}
else {
	#print LOG "incorrect arguments ; usage : perl Task_blast_all_generator_for_IDRIS.pl -VERBOSE = {ON, OFF}";
	die_with_error_mssg ("incorrect arguments ; usage : perl Task_blast_all_generator_for_IDRIS.pl -VERBOSE = {ON, OFF}");
}


#create the following empty directories if they do not exists : DATADIR/Task_blast_all/tmp/,  DATADIR/Task_blast_all/tmp/fasta_formatdb/, DATADIR/Task_blast_all/tmp/blast_output/, DATADIR/Task_blast_all/tmp/launch_blast_batch_files/, DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/, ....


print LOG "Deleting files under $SiteConfig::DATADIR/Task_blast_all/tmp/ \n" unless $VERBOSE =~ m/^OFF$/;
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/*`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/`;
if($GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE =~ m/^ON$/i){
	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/cmd_file_idris/`;# if cmd -GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE ON
}else{
	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/`;
	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/`;
	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/`;
	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/blast/`;
}



if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/ ) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	$molecule_type = "element";
} else {
	#print LOG "BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately\n";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}

if ( $NO_PARALOG =~ m/^ON$/i && $BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/ ) {
	#print LOG "Incompatible arguments -NO_PARALOG ON and \$BLAST_TAXO_GRANULARITY = CLUSTER_ORGANISM\n";
	die_with_error_mssg("Incompatible arguments -NO_PARALOG ON and \$BLAST_TAXO_GRANULARITY = CLUSTER_ORGANISM");
}


#check attribute number_fragment_hit is in table homologies
$ORIGAMI::dbh->selectall_arrayref( "SELECT number_fragment_hit FROM homologies LIMIT 1") or die_with_error_mssg("no column number_fragment_hit in table homologies");

if($GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE =~ m/^ON$/i){

	open( BLAST_BATCHFILE_FOR_IDRIS,
	"> $SiteConfig::DATADIR/Task_blast_all/tmp/cmd_file_idris/blast_cmd_file_for_IDRIS.ll"
	  )
	  or die_with_error_mssg(
	"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/cmd_file_idris/blast_cmd_file_for_IDRIS.ll"
	  );

	open( MAKEBLAST_BATCHFILE_FOR_IDRIS,
	"> $SiteConfig::DATADIR/Task_blast_all/tmp/cmd_file_idris/makeblast_cmd_file_for_IDRIS.ll"
	  )
	  or die_with_error_mssg(
	"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/cmd_file_idris/makeblast_cmd_file_for_IDRIS.ll"
	  );
}



=pod OLD
my ($list_new_molecule_ids_ref, $list_old_molecule_ids_ref, $molecule_id_2_size_ref, $element_id2orga_id_ref) = Find_new_old_molecules::find_new_old_molecules_from_db_data($BLAST_TAXO_GRANULARITY);
@list_new_molecule_ids = @$list_new_molecule_ids_ref;
@list_old_molecule_ids = @$list_old_molecule_ids_ref;
%molecule_id_2_sizemolecule_id_2_size = %$molecule_id_2_size_ref;
%element_id2orga_id = %$element_id2orga_id_ref;
=cut
# list *faa files under $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/new_molecules/ and $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/old_molecules/ and store only the molecule ids
sub sub_list_faa_files_recursively {
    my ( $path, $ref_array_to_push ) = @_;
    #my $path = shift;
    opendir (DIR, $path) or do {
		#print LOG "Unable to open $path: $!";
		die_with_error_mssg ("Unable to open $path: $!");
    };

    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);

    for (@files) {
        if (-d $_) {
            # directory, run recursively
            #print "$_ is a dir\n";
            sub_list_faa_files_recursively ($_);
        } elsif ($_ =~ m/^.+\/${molecule_type}_([\d_]+)\.faa$/ ) {
	    push ( @$ref_array_to_push, $1);
        }
=pod
	 elsif ($_ =~ m/\.faa~$/ ) {
		#del ~ files
		`$SiteConfig::CMDDIR/rm -f $_`;
	} else {
		#print LOG "Error in sub_list_faa_files_recursively : the file $_ does not match the regex *.faa\n";
		die_with_error_mssg("Error in sub_list_faa_files_recursively : the file $_ does not match the regex *.faa\n");
        }
=cut
    }
}
if ( -d "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/new_molecules/" ) {
	sub_list_faa_files_recursively ("$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/new_molecules/", \@list_new_molecule_ids);
}
if ( -d "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/old_molecules/" ) {
	sub_list_faa_files_recursively ("$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/old_molecules/", \@list_old_molecule_ids);
}
print LOG @list_new_molecule_ids." new $BLAST_TAXO_GRANULARITY found.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG @list_old_molecule_ids." old $BLAST_TAXO_GRANULARITY found.\n" unless $VERBOSE =~ m/^OFF$/;

if($GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE =~ m/^ON$/i){
	#EXPORT_QSUB_LOG_DIR
	if ($EXPORT_QSUB_LOG_DIR ne "") {
		print BLAST_BATCHFILE_FOR_IDRIS "export QSUB_LOG_DIR=$EXPORT_QSUB_LOG_DIR\n";
	}
	#EXPORT_PLAST_EXEC_PATH
	if ($EXPORT_PLAST_EXEC_PATH ne "") {
		print BLAST_BATCHFILE_FOR_IDRIS "export PLAST_EXEC_PATH=$EXPORT_PLAST_EXEC_PATH\n";
	}
	#EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR
	if ($EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR ne "") {
		print BLAST_BATCHFILE_FOR_IDRIS "export FASTA_MAKEBLASTDB_INPUT_DIR=$EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR\n";
	}
	#EXPORT_BLAST_OUTPUT_DIR
	if ($EXPORT_BLAST_OUTPUT_DIR ne "") {
		print BLAST_BATCHFILE_FOR_IDRIS "export BLAST_OUTPUT_DIR=$EXPORT_BLAST_OUTPUT_DIR\n";
	}
	#EXPORT_MARKER_DONE_BLAST_DIR
	if ($EXPORT_MARKER_DONE_BLAST_DIR ne "") {
		print BLAST_BATCHFILE_FOR_IDRIS "export MARKER_DONE_BLAST_DIR=$EXPORT_MARKER_DONE_BLAST_DIR\n";
	}
}

#print blast command for new mol against new
if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$prefix_new_old_fasta_query = "new_molecules";
	$prefix_new_old_blast_bank = "EMPTY";
} else {
	$prefix_new_old_fasta_query = "new_molecules";
	$prefix_new_old_blast_bank = "new_molecules";
}
for my $i (0 .. $#list_new_molecule_ids){
	my $master_mol_id = $list_new_molecule_ids[$i];

	for my $j ($i .. $#list_new_molecule_ids){
		my $sub_mol_id = $list_new_molecule_ids[$j];

		if ($NO_PARALOG =~ m/^ON$/i
		) {
			if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
				if ( $master_mol_id == $sub_mol_id ) {
					next;
				}
			} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/ ) {
				if ( $element_id2orga_id{$master_mol_id} == $element_id2orga_id{$sub_mol_id} ) {
					next;
				}
			} else {
				die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized 1 : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
			}

		}

		if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
			$sub_mol_id = "all";
		}



		if($GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE =~ m/^ON$/i){

			# GENERATE_WITH_ENVIRONENT_VARIABLES
				# generate_individual_bash_file_blast_IDRIS ${QSUB_LOG_DIR}
				# BlastConfig.pm ${PLAST_EXEC_PATH}
				# ${FASTA_MAKEBLASTDB_INPUT_DIR}
				# ${BLAST_OUTPUT_DIR}
				# generate_individual_bash_file_blast_IDRIS ${MARKER_DONE_BLAST_DIR}
			# BlastConfig::generate_command($program_to_use
							#, $number_processors_to_use
							#, $database_file
							#, $query_file
							#, $out_file
			my $query_file = "";
			if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
				$query_file = "\${FASTA_MAKEBLASTDB_INPUT_DIR}/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id}.faa";
			} else {
				$query_file = "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id}.faa";
			}
			my $database_file = "";
			if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
				$database_file = "\${FASTA_MAKEBLASTDB_INPUT_DIR}/${prefix_new_old_blast_bank}/${molecule_type}_${sub_mol_id}";
			} else {
				$database_file = "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${prefix_new_old_blast_bank}/${molecule_type}_${sub_mol_id}";
			}
			my $out_file = "";
			if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
				$out_file = "\${BLAST_OUTPUT_DIR}/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id}_VS_${molecule_type}_${sub_mol_id}.blast";
			} else {
				$out_file = "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id}_VS_${molecule_type}_${sub_mol_id}.blast";
			}
			my $string_command_IT = BlastConfig::generate_command(
							$program_to_use,
							$number_processors_to_use_per_blast,
							$database_file,
							$query_file,
							$out_file,
							$GENERATE_WITH_ENVIRONENT_VARIABLES
							);
			print BLAST_BATCHFILE_FOR_IDRIS "$string_command_IT\n";

		}else{

			# generate bash script to launch blast for molecule_id_accepted_as_new and current_molecule_id

			# BlastConfig::generate_command($program_to_use
							#, $number_processors_to_use
							#, $database_file
							#, $query_file
							#, $out_file
			# generate_individual_bash_file_blast_IDRIS.pl ($program_to_use,
							# $number_processors_to_use_per_blast,
							# "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${prefix_new_old_blast_bank}/${molecule_type}_${sub_mol_id_IT}",
							# "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}.faa",
							# "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast"

			$cmd_out = `perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/generate_individual_bash_file_blast_IDRIS.pl $master_mol_id $sub_mol_id $VERBOSE OFF $molecule_type $program_to_use $number_processors_to_use_per_blast $prefix_new_old_fasta_query $prefix_new_old_blast_bank $GENERATE_WITH_ENVIRONENT_VARIABLES $EXPORT_QSUB_LOG_DIR $EXPORT_PLAST_EXEC_PATH $EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR $EXPORT_BLAST_OUTPUT_DIR $EXPORT_MARKER_DONE_BLAST_DIR`;



			chomp ($cmd_out);
			$cmd_out =~ s/^\s+//;
			$cmd_out =~ s/\s+$//;
			if($cmd_out eq ""){
				#ok
				print LOG "step gen_faa_db and generate_individual_bash_file_blast_IDRIS succeded for ${molecule_type}_$master_mol_id VS ${molecule_type}_$sub_mol_id\n" unless $VERBOSE =~ m/^OFF$/i;
			} else {
				die_with_error_mssg ("ERROR : perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/generate_individual_bash_file_blast_IDRIS.pl $master_mol_id $sub_mol_id $VERBOSE OFF $molecule_type $program_to_use $number_processors_to_use_per_blast $prefix_new_old_fasta_query $prefix_new_old_blast_bank $GENERATE_WITH_ENVIRONENT_VARIABLES $EXPORT_QSUB_LOG_DIR $EXPORT_PLAST_EXEC_PATH $EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR $EXPORT_BLAST_OUTPUT_DIR $EXPORT_MARKER_DONE_BLAST_DIR failed: $?");
				exit(1);
			}

			if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
				last;
			}
		}
	}
}


#print main_Align command for new mol against old
$prefix_new_old_fasta_query = "old_molecules";
$prefix_new_old_blast_bank = "new_molecules";
for my $i (0 .. $#list_new_molecule_ids){

	# already done for ORGANISM_VS_ALL in new mol against new
	if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
		last;
	}

	my $new_mol_id = $list_new_molecule_ids[$i];

	for my $j (0 .. $#list_old_molecule_ids){
		my $old_mol_id = $list_old_molecule_ids[$j];



		if($GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE =~ m/^ON$/i){

			# GENERATE_WITH_ENVIRONENT_VARIABLES
				# generate_individual_bash_file_blast_IDRIS ${QSUB_LOG_DIR}
				# BlastConfig.pm ${PLAST_EXEC_PATH}
				# ${FASTA_MAKEBLASTDB_INPUT_DIR}
				# ${BLAST_OUTPUT_DIR}
				# generate_individual_bash_file_blast_IDRIS ${MARKER_DONE_BLAST_DIR}
			my $query_file = "";
			if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
				$query_file = "\${FASTA_MAKEBLASTDB_INPUT_DIR}/${prefix_new_old_fasta_query}/${molecule_type}_${old_mol_id}.faa";
			} else {
				$query_file = "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${prefix_new_old_fasta_query}/${molecule_type}_${old_mol_id}.faa";
			}
			my $database_file = "";
			if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
				$database_file = "\${FASTA_MAKEBLASTDB_INPUT_DIR}/${prefix_new_old_blast_bank}/${molecule_type}_${new_mol_id}";
			} else {
				$database_file = "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${prefix_new_old_blast_bank}/${molecule_type}_${new_mol_id}";
			}
			my $out_file = "";
			if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
				$out_file = "\${BLAST_OUTPUT_DIR}/${prefix_new_old_fasta_query}/${molecule_type}_${old_mol_id}_VS_${molecule_type}_${new_mol_id}.blast";
			} else {
				$out_file = "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${prefix_new_old_fasta_query}/${molecule_type}_${old_mol_id}_VS_${molecule_type}_${new_mol_id}.blast";
			}
			my $string_command_IT = BlastConfig::generate_command(
							$program_to_use,
							$number_processors_to_use_per_blast,
							$database_file,
							$query_file,
							$out_file,
							$GENERATE_WITH_ENVIRONENT_VARIABLES
							);
			print BLAST_BATCHFILE_FOR_IDRIS "$string_command_IT\n";

		}else{

			# generate bash script to launch blast for molecule_id_accepted_as_new and current_molecule_id
			$cmd_out = `perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/generate_individual_bash_file_blast_IDRIS.pl $old_mol_id $new_mol_id $VERBOSE OFF $molecule_type $program_to_use $number_processors_to_use_per_blast $prefix_new_old_fasta_query $prefix_new_old_blast_bank $GENERATE_WITH_ENVIRONENT_VARIABLES $EXPORT_QSUB_LOG_DIR $EXPORT_PLAST_EXEC_PATH $EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR $EXPORT_BLAST_OUTPUT_DIR $EXPORT_MARKER_DONE_BLAST_DIR`;
			chomp ($cmd_out);
			$cmd_out =~ s/^\s+//;
			$cmd_out =~ s/\s+$//;
			if($cmd_out eq ""){
				#ok
				print LOG "step gen_faa_db and generate_individual_bash_file_blast_IDRIS succeded for ${molecule_type}_$old_mol_id VS ${molecule_type}_$new_mol_id\n" unless $VERBOSE =~ m/^OFF$/i;
			} else {
				die_with_error_mssg ("ERROR : perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/generate_individual_bash_file_blast_IDRIS.pl $old_mol_id $new_mol_id $VERBOSE OFF $molecule_type $program_to_use $number_processors_to_use_per_blast $prefix_new_old_fasta_query $prefix_new_old_blast_bank $GENERATE_WITH_ENVIRONENT_VARIABLES $EXPORT_QSUB_LOG_DIR $EXPORT_PLAST_EXEC_PATH $EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR $EXPORT_BLAST_OUTPUT_DIR $EXPORT_MARKER_DONE_BLAST_DIR failed: $?");
				exit(1);
			}

		}

	}
}


#close, file, cleaning up archive
if($GENERATE_SINGLE_CONSOLIDATED_LAUNCH_FILE =~ m/^ON$/i){
	close(BLAST_BATCHFILE_FOR_IDRIS);
	close(MAKEBLAST_BATCHFILE_FOR_IDRIS);
}

#end of script
print LOG
"\n---------------------------------------------------\n\n\nTask_blast_all_generator_for_IDRIS.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/i;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);




