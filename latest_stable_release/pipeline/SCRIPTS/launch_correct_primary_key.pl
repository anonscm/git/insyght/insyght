#!/usr/local/bin/perl
#
# full name = launch_correct_primary_key.pl
#
# This script launch bash files that will be used to calculate primary keys
#
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use SiteConfig;
use Time::HiRes qw(usleep);
use POSIX ":sys_wait_h";
use BlastConfig;

# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/launch_correct_primary_key.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/launch_correct_primary_key.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/launch_correct_primary_key.error";
my $output_backtick = "";
my $VERBOSE = "ON";
my $LL_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit";
my $LOG_DIR = "$SiteConfig::LOGDIR/Task_add_alignments"; # should be the same as the one for script generate_correct_primary_key_launcher_files
my $CMD_SUBMIT_CLUSTER = "$SiteConfig::CMDDIR/bash";
my $MAX_FILES_TO_SUBMIT_IN_THIS_RUN = undef;
my $MAX_JOBS = 1;
my $FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = "ON";
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $molecule_type;
my @ll_files = ();


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}

foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl launch_correct_primary_key.pl -VERBOSE = {ON, OFF}");
		}
	}
	if ( $ARGV[$argnum] =~ m/^-LL_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$LL_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -LL_INPUT_DIR argument ; usage : perl launch_correct_primary_key.pl  -LL_INPUT_DIR {PATH_TO_DIRECTORY}";
			die_with_error_mssg("incorrect -LL_INPUT_DIR argument ; usage : perl launch_correct_primary_key.pl  -LL_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-LOG_DIR$/ ) {
		if (  $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$LOG_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -LOG_DIR argument ; usage : perl launch_correct_primary_key.pl -LOG_DIR {PATH_TO_DIRECTORY}";
			die_with_error_mssg("incorrect -LOG_DIR argument ; usage : perl launch_correct_primary_key.pl -LOG_DIR {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-CMD_CLUSTER/ ) {
		if ( $ARGV[$argnum] =~ m/^0$/i ) {

			#do nothing
		} 
		else {
			if ( $ARGV[$argnum] =~ m/^.+$/i ) {
				$CMD_SUBMIT_CLUSTER = $ARGV[ $argnum + 1 ];
			}
		}

	} elsif ( $ARGV[$argnum] =~ m/^-MAX_JOBS$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_JOBS = $ARGV[ $argnum + 1 ];
			if($MAX_JOBS < 1){
				die_with_error_mssg ("incorrect -MAX_JOBS argument ; usage : perl launch_correct_primary_key.pl  -MAX_JOBS {int > 0}");
			}
		}
		else {
			die_with_error_mssg("incorrect -MAX_JOBS argument ; usage : perl launch_correct_primary_key.pl  -MAX_JOBS {int > 0}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-MAX_FILES_TO_SUBMIT_IN_THIS_RUN/ ) {

		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_FILES_TO_SUBMIT_IN_THIS_RUN = $ARGV[ $argnum + 1 ];
			if($MAX_FILES_TO_SUBMIT_IN_THIS_RUN < 1){
				die_with_error_mssg("incorrect -MAX_FILES_TO_SUBMIT_IN_THIS_RUN argument ; usage : perl launch_correct_primary_key.pl  -MAX_FILES_TO_SUBMIT_IN_THIS_RUN {int > 0}");
			}
		}
		else {
			die_with_error_mssg("incorrect -MAX_FILES_TO_SUBMIT_IN_THIS_RUN argument ; usage : perl launch_correct_primary_key.pl  -MAX_FILES_TO_SUBMIT_IN_THIS_RUN {int > 0}");
		}
	} elsif  ( $ARGV[$argnum] =~ m/^-FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK argument ; usage : perl launch_correct_primary_key.pl -FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK {ON, OFF}");
		}

	}
}

# ouvre fichier de log
open( LOG, ">$path_log_file" );
print LOG "\n---------------------------------------------------\n launch_correct_primary_key.pl started at :", scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# print in and out directory
print LOG "---------------------------------------------------\nThe LL input directory is : $LL_INPUT_DIR\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "\nYou choose the following options :\n\tMAX_JOBS : $MAX_JOBS\n\tVERBOSE : $VERBOSE\n" unless $VERBOSE =~ m/^OFF$/;
if ( $CMD_SUBMIT_CLUSTER =~ m/^0$/i ) {
	#do nothing
}
else {
	print LOG "CMD_CLUSTER : $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/;
}


if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
	#die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}


# deal with dir
`$SiteConfig::CMDDIR/mkdir -p $LOG_DIR`;
# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
`$SiteConfig::CMDDIR/rm -f $LOG_DIR/correct_primary_key_*.error`;
`$SiteConfig::CMDDIR/rm -f $LOG_DIR/correct_primary_key_*.out`;
`$SiteConfig::CMDDIR/rm -f $LOG_DIR/correct_primary_key_*.done`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}



my $count_ll_files = 0;
my $count_ll_gz_files = 0;
sub list_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die_with_error_mssg("Unable to open $path: $!");
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);

    for (@files) {
        
	if (-d $_) {
		# directory
		list_files_recursively($_);
        } elsif ($_ =~ m/^.+\/correct_primary_key_.+\.ll$/ ) {
		if ( defined $MAX_FILES_TO_SUBMIT_IN_THIS_RUN && scalar(@ll_files) == $MAX_FILES_TO_SUBMIT_IN_THIS_RUN ) {
			$count_ll_files++;
		} else {
			push (@ll_files, $_);
			$count_ll_files++;
		}
        } elsif ($_ =~ m/^.+\/correct_primary_key_.+\.ll\.gz$/ ) {
		# alraedy submitted, do nothing
		$count_ll_gz_files++;
        } elsif ($_ =~ m/^.+\/incremental_runs_master_file.txt$/ ) {
		# do nothing
        } elsif ($_ =~ m/^.+\/incremental_runs_master_file.txt.tmp$/ ) {
		# do nothing
        } elsif ($_ =~ m/^.+\/\..+$/ ) {
		# do nothing, hidden file
        } else {
            die_with_error_mssg("Error in list_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
print LOG "gathering ll files under $LL_INPUT_DIR/ ...\n" unless $VERBOSE =~ m/^OFF$/;
list_files_recursively("$LL_INPUT_DIR/");
print LOG "$count_ll_files .ll files found under $LL_INPUT_DIR/\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "$count_ll_gz_files .ll.gz files found under $LL_INPUT_DIR/\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "\n".scalar(@ll_files) . " .ll files will be treated...\n" unless $VERBOSE =~ m/^OFF$/;
if ( defined $MAX_FILES_TO_SUBMIT_IN_THIS_RUN ) {
	print LOG "You chose the option -MAX_FILES_TO_SUBMIT_IN_THIS_RUN $MAX_FILES_TO_SUBMIT_IN_THIS_RUN\n" unless $VERBOSE =~ m/^OFF$/;
}


my $number_files_started  = 0;
my $number_files_done = 0;
my $number_files_started_before_force_submit_check = 0;
print LOG "Start submit ll files\n" unless $VERBOSE =~ m/^OFF$/;

sub sub_count_output_and_del_tmp_files {

	if ( $FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK =~ m/^ON$/i && ( $number_files_started - $number_files_done < $MAX_JOBS ) && $number_files_started > $number_files_started_before_force_submit_check ) {
		$number_files_started_before_force_submit_check = $number_files_started;
		print LOG "( $number_files_done / ".scalar(@ll_files).") : FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK is ON ; Continuing to submit as there is $number_files_started - $number_files_done = ".($number_files_started - $number_files_done)." simulteneous process ;  MAX_JOBS =  $MAX_JOBS\n" unless $VERBOSE =~ m/^OFF$/i;
	} else {

		#my @list_new_files_alignment_done = <$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/*.main_Align_done>;
		#$LOG_DIR/launch_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse_${core_name_file}.done`;
		my @list_new_files_done = ();
		opendir(my $dh, "$LOG_DIR/") or die_with_error_mssg("opendir($LOG_DIR/): $!");
		while (my $de = readdir($dh)) {
		  next if $de =~ /^\./ or $de !~ /correct_primary_key_.+\.done$/;
		  push ( @list_new_files_done, "$LOG_DIR/$de");
		}
		closedir($dh);

		# check err files are empty
		#print "cat $LOG_DIR/process_parse_tsv_output_Replacing_Forward_Reverse_*.error 2>/dev/null\n";
		my $check_err_files_are_empty = `cat $LOG_DIR/correct_primary_key*.err* 2>/dev/null`;
		if ($check_err_files_are_empty eq "" ){
			#ok continue
			$output_backtick = `rm -f $LOG_DIR/correct_primary_key*.err*`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("rm -f $LOG_DIR/correct_primary_key*.err*\n $!");#
			}
		} else {
			die_with_error_mssg("Error cat $LOG_DIR/correct_primary_key*.err* is not empty : $check_err_files_are_empty");
		}


		foreach my $list_new_files_done_IT (@list_new_files_done) {

			#parse file name
			my $master_elet_id;
			my $sub_elet_id;
			my $core_name_list_new_files_done_IT = "";
			if ( $list_new_files_done_IT =~ m/^.+\/correct_primary_key_(\d+)_and_(\d+)\.done$/i ) {
				$master_elet_id = $1;
				$sub_elet_id = $2;
				$core_name_list_new_files_done_IT = "${master_elet_id}_and_${sub_elet_id}";
			} else {
				die_with_error_mssg("Could not parse core name in file $list_new_files_done_IT");
			}

			# if .error not empty
			if ( $CMD_SUBMIT_CLUSTER ne "$SiteConfig::CMDDIR/bash" ) {
=pod
				if (! -z "$LOG_DIR/correct_primary_key_${core_name_list_new_files_done_IT}.error" ) {
					die_with_error_mssg("The file $LOG_DIR/correct_primary_key_${core_name_list_new_files_done_IT}.error is not empty : $!");
				}
				`$SiteConfig::CMDDIR/rm -f $LOG_DIR/correct_primary_key_${core_name_list_new_files_done_IT}.error`;
=cut
				`$SiteConfig::CMDDIR/rm -f $LOG_DIR/correct_primary_key_${core_name_list_new_files_done_IT}.out`;
			}

			# rm and gzip marker file
			my $ll_file_IT = "$LL_INPUT_DIR/${molecule_type}_${master_elet_id}/correct_primary_key_${core_name_list_new_files_done_IT}.ll";

			if ( -e "$ll_file_IT" ) {
				`gzip $ll_file_IT`;
			} else {
				die_with_error_mssg("ERROR : the ll file $ll_file_IT does not exists or is empty.");
			}
			`$SiteConfig::CMDDIR/rm -f $list_new_files_done_IT`;

			# increment counter
			$number_files_done++;

		}
	}
}



foreach my $ll_file_IT (@ll_files) {

	# wait a bit if there more than $MAX_JOBS child process that have been launched simultaneously
	my $count_sleep = 0;
	while ( $count_sleep < 2 ) {

		sub_count_output_and_del_tmp_files();

		#if ($MAX_JOBS == 1) {
		#	last;
		#} else {
			if( $number_files_started - $number_files_done < $MAX_JOBS ){
				#wait a bit so that there is a shifting in time and exit loop
				#usleep(10000);
				my $number_jobs_being_processed = $number_files_started - $number_files_done;
				my $number_jobs_left_to_be_submitted = scalar(@ll_files) - $number_files_started;
				print LOG "( $number_files_done / ".scalar(@ll_files).") : Continuing to submit ; MAX_JOBS = $MAX_JOBS ; Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_files_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
				last;
			}

			if ($count_sleep == 0 ) {
				my $number_jobs_being_processed = $number_files_started - $number_files_done;
				my $number_jobs_left_to_be_submitted = scalar(@ll_files) - $number_files_started;
				print LOG "( $number_files_done / ".scalar(@ll_files).") : waiting a bit, $MAX_JOBS jobs launched simultenously ; MAX_JOBS = $MAX_JOBS ; Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_files_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
				$count_sleep = 1;
				usleep(10000);
			} else {
				usleep(10000);
			}
		#}
	}#while ( $count_sleep < 2 ) {


	# make sure the ll file is executable
	my $chmod_output = `chmod +x $ll_file_IT`;
	if ( ! $chmod_output eq "") {
		die_with_error_mssg("ERROR Could not chmod +x $ll_file_IT: $!");
	}

	#parse file name
	my $master_elet_id;
	my $sub_elet_id;
	my $core_name_ll_file_IT = "";
	if ( $ll_file_IT =~ m/^.+\/correct_primary_key_(\d+)_and_(\d+)\.ll$/i ) {
		$master_elet_id = $1;
		$sub_elet_id = $2;
		$core_name_ll_file_IT = "${master_elet_id}_and_${sub_elet_id}";
	} else {
		die_with_error_mssg("Could not parse core name in file $ll_file_IT");
	}

	# launch of the bash file
	# case no cluster, no paralelisation
	if ( $CMD_SUBMIT_CLUSTER eq "$SiteConfig::CMDDIR/bash") {

		if($MAX_JOBS > 1){
			system("$CMD_SUBMIT_CLUSTER $ll_file_IT >> $LOG_DIR/correct_primary_key_${core_name_ll_file_IT}.error 2>&1 &");
			if ( $? != 0 ) {
				die_with_error_mssg("command $CMD_SUBMIT_CLUSTER $ll_file_IT 2>&1 failed: $!");
			}
			$number_files_started++;
			#print LOG "file $ll_file_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i; # (Total submitted = $number_files_started ; Total done = $number_files_done ; Total Expected = ".scalar(@ll_files).")
			my $number_jobs_being_processed = $number_files_started - $number_files_done;
			my $number_jobs_left_to_be_submitted = scalar(@ll_files) - $number_files_started;
			print LOG "( $number_files_done / ".scalar(@ll_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_files_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		} else {
			$number_files_started++;
			my $number_jobs_being_processed = $number_files_started - $number_files_done;
			my $number_jobs_left_to_be_submitted = scalar(@ll_files) - $number_files_started;
			print LOG "( $number_files_done / ".scalar(@ll_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_files_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
			system("$CMD_SUBMIT_CLUSTER $ll_file_IT >> $LOG_DIR/correct_primary_key_${core_name_ll_file_IT}.error 2>&1");
			#print LOG "file $ll_file_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i; # (Total submitted = $number_files_started ; Total done = $number_files_done ; Total Expected = ".scalar(@ll_files).")
			print LOG "( $number_files_done / ".scalar(@ll_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_files_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		}
	} else {
		# use of cluster
		my $output_cmd = `$CMD_SUBMIT_CLUSTER $ll_file_IT 2>&1`;
		if ( $? != 0 )
		{
			die_with_error_mssg("command $CMD_SUBMIT_CLUSTER $ll_file_IT 2>&1 failed: $!");
		}
		if($output_cmd =~ m/^Your job .+ has been submitted$/i
			|| $output_cmd eq ""
		){
			$number_files_started++;
			#print LOG "file $ll_file_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i; #  (Total submitted = $number_files_started ; Total done = $number_files_done ; Total Expected = ".scalar(@ll_files).")
			my $number_jobs_being_processed = $number_files_started - $number_files_done;
			my $number_jobs_left_to_be_submitted = scalar(@ll_files) - $number_files_started;
			print LOG "( $number_files_done / ".scalar(@ll_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_files_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		}else{
			die_with_error_mssg("Error launch of : $CMD_SUBMIT_CLUSTER $ll_file_IT :\n$output_cmd");
		}
	}
} #foreach my $ll_file_IT (@ll_files) {

print LOG "done submitting commands on cluster\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "waiting for last commands to complete...\n" unless $VERBOSE =~ m/^OFF$/;
$FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = "OFF";

#wait for all files
my $count_finish = 0;
my $count_wait_hours = 0;
while ( $count_finish >= 0 ) {

	sub_count_output_and_del_tmp_files();

	if($number_files_done == scalar(@ll_files) ){
		last;
	}

	usleep(1000000);#sleep 1s
	$count_finish++;
	if ($count_finish > 3600){#1 H
		#print LOG "Error, waited for files output file for more than 1 hour\n";
		#die_with_error_mssg ("Error, waited for blast files file for more than 1 hour\n");
		$count_wait_hours++;
		print LOG "( $number_files_done / ".scalar(@ll_files).") : All jobs have been launched. Waiting for output files ; Total waiting time = $count_wait_hours hours ; Total output files found yet = $number_files_done. (This message will display every hour until all output files are found).\n" unless $VERBOSE =~ m/^OFF$/;
		$count_finish = 0;
	}
}

#end of script
print LOG "$number_files_done jobs have been successfully submitted in this run.\n" unless $VERBOSE =~ m/^OFF$/;

print LOG
"\n---------------------------------------------------\n\n\n launch_correct_primary_key.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);


