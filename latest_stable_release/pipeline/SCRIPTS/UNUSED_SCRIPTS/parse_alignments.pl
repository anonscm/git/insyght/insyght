#!/usr/local/bin/perl

#Auteurs: Michael Alaux, Mark Hoebeke
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use Time::HiRes qw(sleep);
use DBI;

use XML::Writer;
use IO;

use SiteConfig;
use lib '$SiteConfig::SCRIPTSDIR';
use AlignConfig;
use ORIGAMI;

my $accession = shift;

die "Usage: $0 accession"
  unless ( defined $accession );

my $SUBDIR   = $AlignConfig::params->{-datasubdir};
my @txtfiles = <$SiteConfig::DATADIR/$accession/Alignments/$SUBDIR/*.aligned>;

foreach my $txtfile (@txtfiles) {

	$txtfile =~ s/.aligned/.txt/;
	die "$0: wrong name pattern for input file, required pattern: x_y.txt"
	  if ( $txtfile !~ /(\d+)_(\d+)\.txt$/ );
	my $qelementid = $1;
	my $selementid = $2;

	my $outfile = $txtfile;
	$outfile =~ s/.txt/.xml/;
	if ( !-r "$outfile.ok" ) {

		my %homologies = ();

		my $rows = $ORIGAMI::dbh->selectall_arrayref(
"select g1.name as qname, g2.name as sname, q_first,q_last,s_first,s_last from homologies,genes as g1, genes as g2 where homologies.q_element_id=$qelementid and homologies.s_element_id=$selementid and g1.gene_id=homologies.q_gene_id and g2.gene_id=homologies.s_gene_id"
		);
		foreach my $row ( @{$rows} ) {
			$homologies{ $row->[0] . " " . $row->[1] } = {
				qstart => $row->[2],
				qend   => $row->[3]
			};
		}

		my $output = new IO::File(">$outfile");
		my $writer = new XML::Writer(
			OUTPUT      => $output,
			DATA_MODE   => 1,
			DATA_INDENT => 4
		);

		$writer->xmlDecl( "ISO-8859-1", "no" );
		$writer->doctype( "alignments", undef, "Alignments.dtd" );

		#Parcours des lignes et cr�ation de variables locales
		my $etat = 0;
		open FICENTREE, "<$txtfile";

		my ( $desc_query,            $val_query );
		my ( $desc_subject,          $val_subject );
		my ( $desc_ortholog_score,   $val_ortholog_score );
		my ( $desc_homolog_score,    $val_homolog_score );
		my ( $desc_mismatch_penalty, $val_mismatch_penalty );
		my ( $desc_gapcreation,      $val_gapcreation );
		my ( $desc_gapextension,     $val_gapextension );
		my ( $desc_minalign,         $val_minalign );
		my ( $desc_minscore,         $val_minscore );
		my ( $desc_include,          $val_include );
		my (@booleen);
		my ( $desc_score,       $val_score );
		my ( $desc_size,        $val_size );
		my ( $desc_orientation, $val_orientation );
		my ( $desc_ortho,       $val_ortho );
		my ( $desc_homo,        $val_homo );
		my (@sens);
		my ($gaps);
		my ($total_gap_size);
		my ($mismatches);
		my ($query_position_actuelle);
		my ($subject_position_actuelle);
		my ($query_name);
		my ($subject_name);
		my ($local_score);
		my ($global_score);
		my ($query_start);
		my ($query_end);
		my ($subject_start);
		my ($subject_end);
		my (@tab_paires);
		my ($pair);

		my $compteur = 0
		  ; #creation d'un compteur pour obtenir la ligne lors de message d'erreur
		while ( my $line = <FICENTREE> )    #boucle lisant chaque ligne
		{
			$compteur++;

			chomp($line);

			#  print "$etat : $line\n";

			if ( $line =~ /Run parameters.*/ ) {
				$etat = 1;                  #on entre dans alignments
				next;
			}

			if ( $line =~ /Query element.*/ && $etat == 1 ) {
				( $desc_query, $val_query ) = split( /: /, $line );
				next;
			}

			if ( $line =~ /Subject element.*/ && $etat == 1 ) {
				( $desc_subject, $val_subject ) = split( /: /, $line );

				$writer->startTag(
					'alignments',
					'query_organism'   => $val_query,
					'subject_organism' => $val_subject
				);

				$etat = 2;    #on va entrer dans run_parameters
				next;
			}

			if ( $line =~ /Ortholog\s+Score.*/ && $etat == 2 ) {
				( $desc_ortholog_score, $val_ortholog_score ) =
				  split( /: /, $line );
				next;
			}

			if ( $line =~ /Homolog\s+Score.*/ && $etat == 2 ) {
				( $desc_homolog_score, $val_homolog_score ) =
				  split( /: /, $line );
				next;
			}

			if ( $line =~ /Mismatch\s+Penalty.*/ && $etat == 2 ) {
				( $desc_mismatch_penalty, $val_mismatch_penalty ) =
				  split( /: /, $line );
				next;
			}

			if ( $line =~ /Gap Creation Penalty.*/ && $etat == 2 ) {
				( $desc_gapcreation, $val_gapcreation ) = split( /: /, $line );
				next;
			}

			if ( $line =~ /Gap Extension Penalty.*/ && $etat == 2 ) {
				( $desc_gapextension, $val_gapextension ) =
				  split( /: /, $line );
				next;
			}

			if ( $line =~ /Min align.*/ && $etat == 2 ) {
				( $desc_minalign, $val_minalign ) = split( /: /, $line );
				next;
			}

			if ( $line =~ /Min  score.*/ && $etat == 2 ) {
				( $desc_minscore, $val_minscore ) = split( /: /, $line );
				next;
			}

			if ( $line =~ /Include All Orthologs.*/ && $etat == 2 ) {
				( $desc_include, $val_include ) = split( /: /, $line );
				@booleen[ 0, 1 ] = ( 'false', 'true' );

				$writer->emptyTag(
					"run_parameters",
					"ortholog_score"        => "$val_ortholog_score",
					"homolog_score"         => "$val_homolog_score",
					"mismatch_penalty"      => "$val_mismatch_penalty",
					"gap_creation_penalty"  => "$val_gapcreation",
					"gap_extension_penalty" => "$val_gapextension",
					"min_alignment_size"    => "$val_minalign",
					"min_score"             => "$val_minscore",
					"include_all_orthologs" => "$booleen[$val_include]"
				);

				$etat = 3;  #on va enter dans alignment (sauf mismatchs et gaps)
				next;
			}

			if ( $line =~ /Score.*/ && $etat == 3 ) {
				( $desc_score, $val_score ) = split( /: /, $line );
				next;
			}

			if ( $line =~ /Size.*/ && $etat == 3 ) {
				( $desc_size, $val_size ) = split( /: /, $line );
				next;
			}

			if ( $line =~ /Orientation.*/ && $etat == 3 ) {
				( $desc_orientation, $val_orientation ) = split( /: /, $line );
				@sens[ 0, 1 ] = ( 'conserved', 'inverted' );
				next;
			}

			if ( $line =~ /Orthologs.*/ && $etat == 3 ) {
				( $desc_ortho, $val_ortho ) = split( /: /, $line );
				next;
			}

			if ( $line =~ /Homologs.*/ && $etat == 3 ) {
				( $desc_homo, $val_homo ) = split( /: /, $line );
				$etat = 4;    #on va enter dans query
				next;
			}

			#postion des g�nes du query
			if ( $line =~ /\s*Start Gene : (\d*).*/ && $etat == 4 ) {
				$query_position_actuelle = $1 - 1;

				$etat = 5;    #on va enter dans subject
				next;
			}

			#position des g�nes du subject (attention au sens)
			if ( $line =~ /Subject :.*/ && $etat == 5 ) {
				$etat = 6;    #on entre dans subject
				next;
			}

			if (   $line =~ /\s*Start Gene : (\d*).*/
				&& $etat == 6
				&& $val_orientation == 0 )
			{
				$subject_position_actuelle = $1 - 1;
				$etat = 7;    #on va verifier la presence de la ligne de -------
				next;
			}

			if (   $line =~ /\s*End Gene   : (\d*).*/
				&& $etat == 6
				&& $val_orientation == 1 )
			{
				$subject_position_actuelle = $1 + 1;
				$etat = 7;    #on va verifier la presence de la ligne de -------
				next;
			}

			if ( $line =~ /---.*/ && $etat == 7 ) {
				@tab_paires =
				  ();         #initialisation du tableau associatif des paires
				$gaps           = 0;    #initialisation des gaps et mismatches
				$total_gap_size = 0;
				$mismatches     = 0;
				$etat           = 8
				  ; #on va entrer dans les paires et calculs des mismatchs et gaps
				next;
			}

			if ( $etat == 8 ) {

				#condition de sortie
				if ( $line =~ /===.*/ ) {
					$writer->startTag(
						"alignment",
						"score"          => "$val_score",
						"pairs"          => "$val_size",
						"orientation"    => "$sens[$val_orientation]",
						"orthologs"      => "$val_ortho",
						"homologs"       => "$val_homo",
						"mismatches"     => "$mismatches",
						"gaps"           => "$gaps",
						"total_gap_size" => "$total_gap_size"
					);

					foreach $pair (@tab_paires) {
						$writer->startTag(
							"pair",
							"local_score"  => "$pair->{local_score}",
							"global_score" => "$pair->{global_score}"
						);

						if ( $pair->{query_name} eq '-' ) {
							$writer->emptyTag("gap");
						}

						else {
							$writer->emptyTag(
								"query",
								"name" => "$pair->{query_name}",
								"position" =>
								  "$pair->{query_position_actuelle}",
								"start" => "$pair->{query_start}",
								"end"   => "$pair->{query_end}"
							);
						}

						if ( $pair->{subject_name} eq '-' ) {
							$writer->emptyTag("gap");
						}

						else {
							$writer->emptyTag(
								"subject",
								"name" => "$pair->{subject_name}",
								"position" =>
								  "$pair->{subject_position_actuelle}",
								"start" => "$pair->{subject_start}",
								"end"   => "$pair->{subject_end}"
							);
						}
						my $key =
						  $pair->{query_name} . " " . $pair->{subject_name};
						if ( defined $homologies{$key} ) {
							$writer->emptyTag(
								"location",
								"qstart" => $homologies{$key}->{qstart},
								"qend"   => $homologies{$key}->{qend}
							);
						}

						$writer->endTag("pair");
					}
					$writer->endTag("alignment");

					$etat = 3;    #on retourne � alignement
				}

				else {

					#decoupage des champs en fonction des tabulations
					my (@fields) = split( /\t/, $line );
					$query_name   = $fields[0];
					$subject_name = $fields[1];
					$local_score  = $fields[2];

					#comptabilisation des gaps et mismatchs
					
					if ( $local_score == -2 ) {
						$mismatches++;
					}
					if ( $local_score == -4 ) {
						$gaps++;
						$total_gap_size++;
					}

					if ( $local_score == -1 ) {
						$total_gap_size++;
					}

					$global_score = $fields[3];

					if ( $fields[4] =~ /\[(\d+),\s(\d+)\]/ ) {
						$query_start = $1;
						$query_end   = $2;
					}

					else {
						print STDERR "Erreur de format � la ligne: $compteur";
					}

					if ( $fields[5] =~ /\[(\d+),\s(\d+)\]/ ) {
						$subject_start = $1;
						$subject_end   = $2;
					}

					else {
						print STDERR "Erreur de format � la ligne: $compteur";
					}

					#evolution des positions des genes
					if ( $query_name ne '-' ) {
						$query_position_actuelle++;
					}

					if ( $subject_name ne '-' && $val_orientation == 0 ) {
						$subject_position_actuelle++;
					}

					if ( $subject_name ne '-' && $val_orientation == 1 ) {
						$subject_position_actuelle--;
					}

					#on met chaque variable dans un tableau de hachage
					push @tab_paires,
					  {
						query_name                => $query_name,
						subject_name              => $subject_name,
						local_score               => $local_score,
						global_score              => $global_score,
						query_start               => $query_start,
						query_end                 => $query_end,
						subject_start             => $subject_start,
						subject_end               => $subject_end,
						query_position_actuelle   => $query_position_actuelle,
						subject_position_actuelle => $subject_position_actuelle
					  };

				}
				next;
			}
		}

		close FICENTREE;

		$writer->endTag("alignments");
		$writer->end();
		$output->close();
`$SiteConfig::BINDIR/bzip2 $txtfile && /$SiteConfig::CMDDIR/touch $outfile.ok`;
	}
}

