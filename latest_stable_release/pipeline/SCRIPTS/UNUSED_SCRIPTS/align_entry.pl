#!/usr/local/bin/perl

# script de construction et de miise a jour de SeqDB n�3.1
# permet le lancement du programme d'alignement sur le cluster
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;

use DBI;
use Benchmark;
use Time::HiRes qw(sleep);

use SiteConfig;
use lib '$SiteConfig::SCRIPTSDIR';
use AlignConfig;
use ORIGAMI;

my $ORTHOLOGS = $AlignConfig::params->{-allorthologs};
my $CUTOFF    = $AlignConfig::params->{-minscore};
my $OS        = $AlignConfig::params->{-orthoscore};
my $HS        = $AlignConfig::params->{-homoscore};
my $MP        = $AlignConfig::params->{-mismatch};
my $GC        = $AlignConfig::params->{-gcpenalty};
my $GE        = $AlignConfig::params->{-gepenalty};
my $SUBDIR    = $AlignConfig::params->{-datasubdir};

my $accession         = shift;
my $cluster           = shift;
my $DELETE_TEMP_FILES = shift;

# usage
die
"wrong 1st argument ; usage : align_entry.pl ACCESSION SEND_TO_CLUSTER=[0 for no cluster, 1 for cluster] DELETE_TEMP_FILES=[ON,OFF]"
  unless ( defined $accession );

if ( $cluster != 0 && $cluster != 1 ) {
	die(
"wrong 2nd argument ; usage : align_entry.pl ACCESSION SEND_TO_CLUSTER=[0 for no cluster, 1 for cluster] DELETE_TEMP_FILES=[ON,OFF]"
	);
}

die(
"wrong 3rd argument ; usage : align_entry.pl ACCESSION SEND_TO_CLUSTER=[0 for no cluster, 1 for cluster] DELETE_TEMP_FILES=[ON,OFF]"
  )
  unless ( $DELETE_TEMP_FILES =~ m/^ON$/i
	|| $DELETE_TEMP_FILES =~ m/^OFF$/i );

# creation des scripts permettant le lancement en parall�le des alignements
sub open_new_sub {
	my $nbba = shift;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_alignment/tmp/cluster_batchfiles/`;
	open BATCHFILE,
"> $SiteConfig::DATADIR/Task_add_alignment/tmp/cluster_batchfiles/Align_$nbba.ll";
	print BATCHFILE "#!/bin/bash
#\$ -S /bin/bash  
# Avertir au d�but (b)egin, � la fin (e)nd, � l'�liminaton (a)bort et
# � la suspension (s)uspend d'un job
#\$ -m as 
# Adresse mail � laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#\$ -o $SiteConfig::LOGDIR/Align/Align_$nbba.out
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -e $SiteConfig::LOGDIR/Align/Align_$nbba.err  
# Lance la commande depuis le r�pertoire o� est lanc� le script
#\$ -cwd

export LD_LIBRARY_PATH=\"$SiteConfig::ALIGN/lib/\"

";
}

my $query_ids = $ORIGAMI::dbh->selectall_arrayref(
	"select element_id from elements where accession='$accession'");

die "Unknown accession $accession.\n"
  unless ( scalar(@$query_ids) );

my $RESDIR = "$SiteConfig::DATADIR/$accession/Alignments/$SUBDIR";

`mkdir -p $RESDIR`;

# nombre de fichier d�ja aligned
my @aligned = <$RESDIR/*.aligned>;
my $nbalign = @aligned;

my $query_ids_string = '(';
my $nbb              = 0;
my $query_element_id;
my $subject_element_id;

# ouvre en pipant sur tee.pl pour �crire � la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Align/Align_entry.log"
);

foreach my $query_id (@$query_ids) {
	my $query_element_id = $query_id->[0];
	my $subject_ids      = $ORIGAMI::dbh->selectall_arrayref(
"select distinct h.s_element_id from homologies as h where h.q_element_id=$query_element_id"
	);
	foreach my $subject_id ( @{$subject_ids} ) {
		my $subject_element_id = $subject_id->[0];
		if ( !-r "$RESDIR/${query_element_id}_${subject_element_id}.aligned" ) {
			my $t0 = new Benchmark;
			print LOG
			  "Aligning $query_element_id with $subject_element_id at :",
			  scalar(localtime), ").\n";
			my $cmdstring =
"$SiteConfig::ALIGN/align -o $ORTHOLOGS -c $CUTOFF -os $OS -hs $HS -mp $MP -gc $GC -ge $GE $query_element_id $subject_element_id > $RESDIR/${query_element_id}_${subject_element_id}.txt && touch $RESDIR/${query_element_id}_${subject_element_id}.aligned";
			if ( $cluster == 1 ) {
				$nbb++;
				open_new_sub($nbb);
				print BATCHFILE "$cmdstring";
				close BATCHFILE;
				
#				`qsub -m ea -q short.q $SiteConfig::DATADIR/Task_add_alignment/tmp/cluster_batchfiles/Align_$nbb.ll`;

				if (
					system(
"qsub -m ea -q short.q $SiteConfig::DATADIR/Task_add_alignment/tmp/cluster_batchfiles/Align_$nbb.ll"
					) != 0
				  )
				{
					print LOG
"system qsub -m ea -q short.q $SiteConfig::DATADIR/Task_add_alignment/tmp/cluster_batchfiles/Align_$nbb.ll failed: $?\n";
					die(
"system qsub -m ea -q short.q $SiteConfig::DATADIR/Task_add_alignment/tmp/cluster_batchfiles/Align_$nbb.ll failed: $?\n"
					);
				}
				else {
					print LOG
					  "qsub -m ea Align_$nbb.ll finished successfuly\n";
				}


			}
			else {
				`$cmdstring`;
				my $t1 = new Benchmark;
				print LOG "Completed in ", timestr( timediff( $t1, $t0 ) ),
				  "\n";
			}

#	    `./res2xml.pl $RESDIR/align-${query_element_id}-${subject_element_id}.txt $RESDIR/align-${query_element_id}-${subject_element_id}.xml`;
#	    `./alignxml2boxplot.pl -expanded $RESDIR $RESDIR ${query_element_id} ${subject_element_id}`;
		}
	}
}

#close(FIC);

if ( $cluster == 1 ) {

	# attente de tous les r�sultats
	my $nbaligning;
	while ( $nbaligning != $nbb ) {

		# nombre de fichier aligned au total
		my @aligned = <$RESDIR/*.aligned>;
		$nbaligning = @aligned;

		# on retranche les alignement ant�rieur
		$nbaligning -= $nbalign;
		sleep(60);
	}
}

#del temp files if asked for
if ( $DELETE_TEMP_FILES =~ m/^ON$/i ) {
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_add_alignment/tmp/cluster_batchfiles/`;
}
