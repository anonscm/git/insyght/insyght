#!/usr/local/bin/perl
#
# Scripts permettant de remplir les collonnes q_start, q_stop, s_start et s_stop de la table $table_to_update.
# lancer ce script après Task_add_alignments.pl
# Date : 05/20012
# LT
##LAUNCHED BY : user, manually on a console as a perl scipt
#AS : perl Compute_Start_Stop_Syntenies.pl -FORCE_RECOMPUTE_ALL {ON, OFF} -VERBOSE {ON, OFF}
#Example avec arguments : perl Compute_Start_Stop_Syntenies.pl -FORCE_RECOMPUTE_ALL OFF -VERBOSE ON
#Example in daily production : perl Compute_Start_Stop_Syntenies.pl
#ARGUMENTS : -FORCE_RECOMPUTE_ALL : si pas présent ou OFF, alors ne calcule le q start et stop unqiuement si la valeur de la colonne est nulle
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 


use strict;
use SiteConfig;
use ORIGAMI;

#script level variables
my $FORCE_RECOMPUTE_ALL = "OFF";
my $VERBOSE = "OFF";
my $success = 1;
my $dbhIT = $ORIGAMI::dbh;
my $table_to_update = "alignments";
my $count_row_table_alignments = 0;

$dbhIT->{AutoCommit} = 0; 

#prepare cached
my $update_handle_q_start_IT = $dbhIT->prepare_cached('UPDATE alignments 
                                    SET q_start = ?
                                  WHERE alignment_id = ?');
die "Couldn't prepare queries update_handle_q_start_IT; aborting"
           		unless defined $update_handle_q_start_IT;
my $update_handle_q_stop_IT = $dbhIT->prepare_cached('UPDATE alignments 
                                    SET q_stop = ?
                                  WHERE alignment_id = ?');
 die "Couldn't prepare queries update_handle_q_stop_IT; aborting"
           		unless defined $update_handle_q_stop_IT;
my $update_handle_s_start_IT = $dbhIT->prepare_cached('UPDATE alignments 
                                    SET s_start = ?
                                  WHERE alignment_id = ?');
die "Couldn't prepare queries update_handle_s_start_IT; aborting"
           		unless defined $update_handle_s_start_IT;
my $update_handle_s_stop_IT = $dbhIT->prepare_cached('UPDATE alignments 
                                    SET s_stop = ?
                                  WHERE alignment_id = ?');
die "Couldn't prepare queries; aborting"
           		unless defined $update_handle_s_stop_IT;

#set the variables according to the argument or exit with usage if the arguments are incorrect

foreach my $argnum ( 0 .. $#ARGV ) {

	if ( $ARGV[$argnum] =~ m/^-FORCE_RECOMPUTE_ALL$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FORCE_RECOMPUTE_ALL = $ARGV[ $argnum + 1 ];
		}
		else {
			#print "incorrect -FORCE_RECOMPUTE_ALL argument ; usage : perl Compute_Start_Stop_Syntenies.pl -FORCE_RECOMPUTE_ALL {ON, OFF} -VERBOSE = {ON, OFF}\n";
			die
"incorrect -FORCE_RECOMPUTE_ALL argument ; usage : perl Compute_Start_Stop_Syntenies.pl -FORCE_RECOMPUTE_ALL {ON, OFF} -VERBOSE = {ON, OFF}\n";
		}

	}
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			# print "incorrect -VERBOSE argument ; usage : perl Compute_Start_Stop_Syntenies.pl -FORCE_RECOMPUTE_ALL {ON, OFF} -VERBOSE = {ON, OFF}\n";
			die
"incorrect -VERBOSE argument ; usage : perl Compute_Start_Stop_Syntenies.pl -FORCE_RECOMPUTE_ALL {ON, OFF} -VERBOSE = {ON, OFF}\n";
		}

	}
}

print "\nStarting Compute_Start_Stop_Syntenies.pl\nYou choose the following options :\n   FORCE_RECOMPUTE_ALL : $FORCE_RECOMPUTE_ALL\n   VERBOSE : $VERBOSE\n";




#check collones exist is in table homologies
$dbhIT->selectall_arrayref(
	"SELECT q_start FROM $table_to_update LIMIT 1")
  or die "no column q_start in table $table_to_update";
$dbhIT->selectall_arrayref(
	"SELECT q_stop FROM $table_to_update LIMIT 1")
  or die "no column q_stop in table $table_to_update";
$dbhIT->selectall_arrayref(
	"SELECT s_start FROM $table_to_update LIMIT 1")
  or die "no column s_start in table $table_to_update";
$dbhIT->selectall_arrayref(
	"SELECT s_stop FROM $table_to_update LIMIT 1")
  or die "no column s_stop in table $table_to_update";

print "\n Step 1 : gathering correct information about syntenies start and stop\n";

# loop over all rows of table $table_to_update
my $st = undef;
if($FORCE_RECOMPUTE_ALL =~ m/^ON$/i){
	$st = $dbhIT->prepare("SELECT alignment_id, q_start, q_stop, s_start, s_stop FROM $table_to_update");
}else{
	$st = $dbhIT->prepare("SELECT alignment_id, q_start, q_stop, s_start, s_stop FROM $table_to_update WHERE q_start is null OR q_start = 0 OR q_stop is null OR q_stop = 0 OR s_start is null OR s_start = 0 OR s_stop is null OR s_stop = 0");
}

$st->execute
  or die("Pb execute $!");

while ( my $res = $st->fetchrow_hashref() ) {

	
	my $alignment_id_IT = $res->{alignment_id};
	my $q_start_IT = $res->{q_start};
	my $q_stop_IT = $res->{q_stop};
	my $s_start_IT = $res->{s_start};
	my $s_stop_IT = $res->{s_stop};
	my $minimal_q_start_IT = -1;
	my $maximal_q_stop_IT = -1;
	my $minimal_s_start_IT = -1;
	my $maximal_s_stop_IT = -1;
	my $DO_update_handle_q_start_IT = undef;
	my $DO_update_handle_q_stop_IT = undef;
	my $DO_update_handle_s_start_IT = undef;
	my $DO_update_handle_s_stop_IT = undef;

	if (defined $q_start_IT && $FORCE_RECOMPUTE_ALL =~ m/^OFF$/i && $q_start_IT > 0){
		#do nothing for $q_start_IT
		#if($VERBOSE =~ m/^ON$/i){
		#	print "alignment_id $alignment_id_IT : q_start already defined and not force recompute\n";
		#}
	}else{
		if($VERBOSE =~ m/^ON$/i){
			print "alignment_id $alignment_id_IT : q_start not already defined or force recompute\n";
		}
		# calculate minimal $q_start_IT
		my $st_q_start_IT = $dbhIT->prepare("SELECT genes.start FROM alignment_pairs, genes WHERE alignment_pairs.q_gene_id = genes.gene_id AND alignment_pairs.alignment_id = ?")
			or die "Couldn't prepare statement: " . $dbhIT->errstr;
		$st_q_start_IT->execute($alignment_id_IT)
  			or die("Pb execute $!");
		while ( my $res_q_start_IT = $st_q_start_IT->fetchrow_hashref() ) {
			if($minimal_q_start_IT == -1){
				$minimal_q_start_IT = $res_q_start_IT->{start};
			}
			if($minimal_q_start_IT > $res_q_start_IT->{start}){
				$minimal_q_start_IT = $res_q_start_IT->{start};
			}
		}
		$DO_update_handle_q_start_IT = 1;
	}

	if (defined $q_stop_IT && $FORCE_RECOMPUTE_ALL =~ m/^OFF$/i && $q_stop_IT > 0){
		#do nothing for $q_start_IT
		#if($VERBOSE =~ m/^ON$/i){
		#	print "alignment_id $alignment_id_IT : q_stop already defined and not force recompute\n";
		#}
	}else{
		if($VERBOSE =~ m/^ON$/i){
			print "alignment_id $alignment_id_IT : q_stop not already defined or force recompute\n";
		}
		# calculate minimal $q_start_IT
		my $st_q_stop_IT = $dbhIT->prepare("SELECT genes.stop FROM alignment_pairs, genes WHERE alignment_pairs.q_gene_id = genes.gene_id AND alignment_pairs.alignment_id = ?")
			or die "Couldn't prepare statement: " . $dbhIT->errstr;
		$st_q_stop_IT->execute($alignment_id_IT)
  			or die("Pb execute $!");
		while ( my $res_q_stop_IT = $st_q_stop_IT->fetchrow_hashref() ) {
			if($maximal_q_stop_IT == -1){
				$maximal_q_stop_IT = $res_q_stop_IT->{stop};
			}
			if($maximal_q_stop_IT < $res_q_stop_IT->{stop}){
				$maximal_q_stop_IT = $res_q_stop_IT->{stop};
			}
		}
		$DO_update_handle_q_stop_IT = 1;
		
	}
	
	if (defined $s_start_IT && $FORCE_RECOMPUTE_ALL =~ m/^OFF$/i && $s_start_IT > 0){
		#do nothing for $s_start_IT
		#if($VERBOSE =~ m/^ON$/i){
		#	print "alignment_id $alignment_id_IT : s_start already defined and not force recompute\n";
		#}
	}else{
		if($VERBOSE =~ m/^ON$/i){
			print "alignment_id $alignment_id_IT : s_start not already defined or force recompute\n";
		}
		# calculate minimal $s_start_IT
		my $st_s_start_IT = $dbhIT->prepare("SELECT genes.start FROM alignment_pairs, genes WHERE alignment_pairs.s_gene_id = genes.gene_id AND alignment_pairs.alignment_id = ?")
			or die "Couldn't prepare statement: " . $dbhIT->errstr;
		$st_s_start_IT->execute($alignment_id_IT)
  			or die("Pb execute $!");
		while ( my $res_s_start_IT = $st_s_start_IT->fetchrow_hashref() ) {
			if($minimal_s_start_IT == -1){
				$minimal_s_start_IT = $res_s_start_IT->{start};
			}
			if($minimal_s_start_IT > $res_s_start_IT->{start}){
				$minimal_s_start_IT = $res_s_start_IT->{start};
			}
		}
		$DO_update_handle_s_start_IT = 1;
	}

	if (defined $s_stop_IT && $FORCE_RECOMPUTE_ALL =~ m/^OFF$/i && $s_stop_IT > 0){
		#do nothing for $s_stop_IT
		#if($VERBOSE =~ m/^ON$/i){
		#	print "alignment_id $alignment_id_IT : s_stop already defined and not force recompute\n";
		#}
	}else{
		if($VERBOSE =~ m/^ON$/i){
			print "alignment_id $alignment_id_IT : s_stop not already defined or force recompute\n";
		}
		# calculate maximal $s_stop_IT
		my $st_s_stop_IT = $dbhIT->prepare("SELECT genes.stop FROM alignment_pairs, genes WHERE alignment_pairs.s_gene_id = genes.gene_id AND alignment_pairs.alignment_id = ?")
			or die "Couldn't prepare statement: " . $dbhIT->errstr;
		$st_s_stop_IT->execute($alignment_id_IT)
  			or die("Pb execute $!");
		while ( my $res_s_stop_IT = $st_s_stop_IT->fetchrow_hashref() ) {
			if($maximal_s_stop_IT == -1){
				$maximal_s_stop_IT = $res_s_stop_IT->{stop};
			}
			if($maximal_s_stop_IT < $res_s_stop_IT->{stop}){
				$maximal_s_stop_IT = $res_s_stop_IT->{stop};
			}
		}
		$DO_update_handle_s_stop_IT = 1;
	}

	#update row in db
	if(defined $DO_update_handle_q_start_IT){
		#print "$minimal_q_start_IT, $alignment_id_IT : $success\n";
		$success &&= $update_handle_q_start_IT->execute($minimal_q_start_IT, $alignment_id_IT);
		#print "$minimal_q_start_IT, $alignment_id_IT : $success\n";
		$count_row_table_alignments++;
	}
	if(defined $DO_update_handle_q_stop_IT){
		#print "$maximal_q_stop_IT, $alignment_id_IT : $success\n";
		$success &&= $update_handle_q_stop_IT->execute($maximal_q_stop_IT, $alignment_id_IT);
		#print "$maximal_q_stop_IT, $alignment_id_IT : $success\n";
		$count_row_table_alignments++;
	}
	if(defined $DO_update_handle_s_start_IT){
		#print "$minimal_s_start_IT, $alignment_id_IT : $success\n";
		$success &&= $update_handle_s_start_IT->execute($minimal_s_start_IT, $alignment_id_IT);
		#print "$minimal_s_start_IT, $alignment_id_IT : $success\n";
		$count_row_table_alignments++;
	}
	if(defined $DO_update_handle_s_stop_IT){
		#print "$maximal_s_stop_IT, $alignment_id_IT : $success\n";
		$success &&= $update_handle_s_stop_IT->execute($maximal_s_stop_IT, $alignment_id_IT);
		#print "$maximal_s_stop_IT, $alignment_id_IT : $success\n";
		$count_row_table_alignments++;
	}

	#don't do it at once, do it
	if($count_row_table_alignments > 28000000){
		print "memory limit reached : commiting current operation and exiting. Please re-run the script to update rows that have been left out in this run\n";
		last;
	}else{
		print "counter memory : $count_row_table_alignments out of 28000000\n";
	}

}


# commit to db
print "Step2 : commit to db\n";

#($success ? print "commit\n" : print "rollback\n");
#$dbhIT->rollback;
my $result = ($success ? $dbhIT->commit : $dbhIT->rollback);
unless ($result) { 
	die "Couldn't finish transaction : " . $dbhIT->errstr 
}

if($VERBOSE =~ m/^ON$/i){
	print "\ncommit completed to database\n";
}

print
"\n---------------------------------------------------\n\n\Compute_Start_Stop_Syntenies.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";



