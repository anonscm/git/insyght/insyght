#!/usr/local/bin/perl

#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

package AlignmentHandler;

use strict;
use ORIGAMI;

sub new {
	my $class = shift;
	my $self  = {};
	bless $self, $class;
	return $self;
}

sub start_alignments {
	my $self       = shift;
	my $attributes = shift;

	#$self->{aligments} = [];
	$self->{alignments} = [];
}

sub start_run_parameters {
	my $self       = shift;
	my $attributes = shift;

	$self->{run_parameters} = {
		-ortho_score           => $attributes->{ortholog_score},
		-homo_score            => $attributes->{homolog_score},
		-mismatch_penalty      => $attributes->{mismatch_penalty},
		-gap_creation_penalty  => $attributes->{gap_creation_penalty},
		-gap_extension_penalty => $attributes->{gap_extension_penalty},
		-min_align_size        => $attributes->{min_alignment_size},
		-min_score             => $attributes->{min_score},
		-orthologs_included    => $attributes->{include_all_orthologs}
	};
}

sub start_alignment {
	my $self       = shift;
	my $attributes = shift;

	my $orientation = $attributes->{orientation};
	$self->{cur_alignment} = {
		-score                 => $attributes->{score},
		-pairs                 => $attributes->{pairs},
		-orientation_conserved => 't',
		-orthologs             => $attributes->{orthologs},
		-homologs              => $attributes->{homologs},
		-mismatches            => $attributes->{mismatches},
		-gaps                  => $attributes->{gaps},
		-total_gap_size        => $attributes->{total_gap_size}
	};
	$self->{cur_alignment}->{-orientation_conserved} = 'f'
	  if ( $orientation eq 'inverted' );

}

sub end_alignment {
	my $self       = shift;
	my $attributes = shift;

	my $first_pair = $self->{cur_alignment}->{pairs}->[0];
	my $last_pair  = $self->{cur_alignment}->{pairs}->[-1];
	$self->{cur_alignment}->{-q_first_gene_position} =
	  $first_pair->{-q_position};
	$self->{cur_alignment}->{-s_first_gene_position} =
	  $first_pair->{-s_position};
	$self->{cur_alignment}->{-q_last_gene_position} = $last_pair->{-q_position};
	$self->{cur_alignment}->{-s_last_gene_position} = $last_pair->{-s_position};

	my $q_size_kb = $last_pair->{-q_end} - $first_pair->{-q_start} + 1;
	$self->{cur_alignment}->{-q_size_kb} = $q_size_kb / 1000;

	my $s_size_kb = $last_pair->{-s_end} - $first_pair->{-s_start} + 1;
	$s_size_kb = $first_pair->{-s_end} - $last_pair->{-s_start} + 1
	  if ( $self->{cur_alignment}->{-orientation_conserved} eq 'f' );
	$self->{cur_alignment}->{-s_size_kb} = $s_size_kb / 1000;

	push @{ $self->{alignments} }, $self->{cur_alignment};
}

sub start_pair {
	my $self       = shift;
	my $attributes = shift;

	$self->{cur_pair} = {
		-q_gene   => undef,
		-s_gene   => undef,
		-type     => undef,
		-score    => $attributes->{local_score},
		-location => undef,
	};
}

sub end_pair {
	my $self       = shift;
	my $attributes = shift;

	if ( !defined $self->{cur_pair}->{-q_gene} ) {
		$self->{cur_pair}->{-type} = 'q_gap';
	}
	if ( !defined $self->{cur_pair}->{-s_gene} ) {
		$self->{cur_pair}->{-type} = 's_gap';
	}
	if (   defined $self->{cur_pair}->{-q_gene}
		&& defined $self->{cur_pair}->{-s_gene} )
	{

		#if ( !defined( $self->{cur_pair}->{-location} ) ) {
		if ( $self->{cur_pair}->{-score} ==
			$self->{run_parameters}->{-mismatch_penalty} )
		{
			$self->{cur_pair}->{-type} = 'mismatch';
		}
		else {
			if ( $self->{cur_pair}->{-score} ==
				$self->{run_parameters}->{-ortho_score} )
			{
				$self->{cur_pair}->{-type} = 'orthologs';
			}
			else {
				$self->{cur_pair}->{-type} = 'homologs';
			}
		}
	}

	push @{ $self->{cur_alignment}->{pairs} }, $self->{cur_pair};
}

sub start_query {
	my $self       = shift;
	my $attributes = shift;

	$self->{cur_pair}->{-q_gene}     = $attributes->{name};
	$self->{cur_pair}->{-q_position} = $attributes->{position};
	$self->{cur_pair}->{-q_start}    = $attributes->{start};
	$self->{cur_pair}->{-q_end}      = $attributes->{end};

}

sub start_subject {
	my $self       = shift;
	my $attributes = shift;

	$self->{cur_pair}->{-s_gene}     = $attributes->{name};
	$self->{cur_pair}->{-s_position} = $attributes->{position};
	$self->{cur_pair}->{-s_start}    = $attributes->{start};
	$self->{cur_pair}->{-s_end}      = $attributes->{end};

}

sub start_location {
	my $self       = shift;
	my $attributes = shift;

	$self->{cur_pair}->{-location} = 1;
}

sub start_element {
	my $self = shift;
	my $tag  = shift;

	my $name       = $tag->{Name};
	my $attributes = $tag->{Attributes};

	my $code = $self->can("start_$name");
	$self->$code($attributes)
	  if ( defined $code );

}

sub end_element {
	my $self = shift;
	my $tag  = shift;

	my $name       = $tag->{Name};
	my $attributes = $tag->{Attributes};

	my $code = $self->can("end_$name");
	$self->$code($attributes)
	  if ( defined $code );

}

package main;

use strict;

use DBI;
use XML::Parser::PerlSAX;

use ORIGAMI;
use SiteConfig;
use lib '$SiteConfig::SCRIPTSDIR';
use AlignConfig;
use CompAnalResult;

my $accession = shift;

die "Usage: $0 accession"
  unless ( defined $accession );

my $DESCRIPTION =
  'Context Conservations: ' . $AlignConfig::params->{-description};
my $SUBDIR = $AlignConfig::params->{-datasubdir};

my $resultid =
  CompAnalResult::get_comp_anal_result_description_id($DESCRIPTION);

my @infiles = <$SiteConfig::DATADIR/$accession/Alignments/$SUBDIR/*.xml.ok>;

foreach my $infile (@infiles) {

	$infile =~ s/.ok//;

	my $outfile = $infile;
	$outfile =~ s/\.xml/.sql/;

	if ( !-r "$outfile.ok" ) {
		my $q_element_id = undef;
		my $s_element_id = undef;

		if ( $infile =~ /(\d+)_(\d+).xml/ ) {
			$q_element_id = $1;
			$s_element_id = $2;

#my $ORIGAMI::dbh=DBI->connect('dbi:Pg:dbname=origami;host=bdd;port=5433','origami_admin','oriroot01');

  #			my %q_genes       = ();
  #			my $q_organism_id = undef;
  #			my $rows          = $ORIGAMI::dbh->selectall_arrayref(
  #"SELECT name, gene_id, organism_id FROM genes WHERE element_id=$q_element_id"
  #			);
  #			$q_organism_id = $rows->[0]->[2];
  #			foreach my $row (@$rows) {
  #				$q_genes{ $row->[0] } = $row->[1];
  #			}
			my $q_organism_id = undef;
			my $rows          = $ORIGAMI::dbh->selectall_arrayref(
"SELECT organism_id FROM elements WHERE element_id=$q_element_id"
			);
			$q_organism_id = $rows->[0]->[0];

  #			my %s_genes       = ();
  #			my $s_organism_id = undef;
  #			my $rows2         = $ORIGAMI::dbh->selectall_arrayref(
  #"SELECT name, gene_id, organism_id FROM genes WHERE element_id=$s_element_id"
  #			);
  #			$s_organism_id = $rows2->[0]->[2];
  #			foreach my $row (@$rows2) {
  #				$s_genes{ $row->[0] } = $row->[1];
  #			}
			my $s_organism_id = undef;
			my $rows2         = $ORIGAMI::dbh->selectall_arrayref(
"SELECT organism_id FROM elements WHERE element_id=$s_element_id"
			);
			$s_organism_id = $rows2->[0]->[0];

			my %location_types = (
				"orthologs" => 1,
				"homologs"  => 2,
				"mismatch"  => 3,
				"q_gap"     => 4,
				"s_gap"     => 5
			);

			my $handler = AlignmentHandler->new();

			my $parser =
			  XML::Parser::PerlSAX->new( DocumentHandler => $handler );

			my $result = $parser->parse( Source => { SystemId => $infile } );

			my $rows3 = $ORIGAMI::dbh->selectall_arrayref(
				"SELECT max(alignment_param_id) FROM alignment_params");
			my $alignment_param_id = $rows3->[0]->[0];
			if ( $alignment_param_id < 1 ) {
				$alignment_param_id = 1;
			}
			else {
				$alignment_param_id++;
			}

			my $rows4 = $ORIGAMI::dbh->selectall_arrayref(
				"SELECT max(alignment_id) FROM alignments");
			my $alignment_id = $rows4->[0]->[0];
			if ( $alignment_id < 1 ) {
				$alignment_id = 1;
			}
			else {
				$alignment_id++;
			}

			open SQLFILE, "> $outfile";

			my $run_parameters = $handler->{run_parameters};

			print SQLFILE << "EOSQL";
DELETE FROM comp_anal_results_elements WHERE comp_anal_result_description_id=$resultid AND organism_id=$q_organism_id AND element_id=$q_element_id;
INSERT INTO alignment_params(alignment_param_id,q_organism_id,q_element_id,s_organism_id,s_element_id,ortho_score,homo_score,mismatch_penalty,gap_creation_penalty,gap_extension_penalty,min_align_size,min_score,orthologs_included) VALUES ($alignment_param_id,$q_organism_id,$q_element_id,$s_organism_id,$s_element_id,$run_parameters->{-ortho_score},$run_parameters->{-homo_score},$run_parameters->{-mismatch_penalty},$run_parameters->{-gap_creation_penalty},$run_parameters->{-gap_extension_penalty},$run_parameters->{-min_align_size},$run_parameters->{-min_score},'$run_parameters->{-orthologs_included}');
SELECT setval('alignment_param_id_seq',$alignment_param_id);
INSERT INTO comp_anal_results_elements(comp_anal_result_description_id,organism_id,element_id) VALUES ($resultid,$q_organism_id,$q_element_id);
EOSQL

			my $alignments = $handler->{alignments};

			#	    die "No alignments found for $infile."
			#		unless (defined $alignments && scalar(@{$alignments})>0);

			if ( $alignments == undef ) {
				print "No alignments found for : $infile\n";
			}
			else {
				print SQLFILE << "EOSQL";
COPY alignments FROM stdin WITH DELIMITER '|';
EOSQL

				foreach my $alignment ( @{$alignments} ) {
					$alignment->{id} = $alignment_id;
# maj agl 02/07/2012
# AJOUT des 4 | en fin de ligne suite ajouts 4 attributs suppl. table alignments
# q_stat, q_stop, s_start, s_stop qui seront remplis par Compute_Start_Stop_Syntenies.pl 
					print SQLFILE << "EOSQL";
$alignment_id|$alignment_param_id|$alignment->{-score}|$alignment->{-pairs}|$alignment->{-orientation_conserved}|$alignment->{-orthologs}|$alignment->{-homologs}|$alignment->{-mismatches}|$alignment->{-gaps}|$alignment->{-total_gap_size}|$alignment->{-q_first_gene_position}|$alignment->{-q_last_gene_position}|$alignment->{-q_size_kb}|$alignment->{-s_first_gene_position}|$alignment->{-s_last_gene_position}|$alignment->{-s_size_kb}|0|0|0|0
EOSQL
					$alignment_id++;
				}

				$alignment_id--;
				print SQLFILE << "EOSQL";
\\.
SELECT setval('alignment_id_seq',$alignment_id);
COPY alignment_pairs FROM stdin WITH DELIMITER '|';
EOSQL
				foreach my $alignment ( @{$alignments} ) {
					foreach my $pair ( @{ $alignment->{pairs} } ) {

						#my $q_gene_id = $q_genes{ $pair->{-q_gene} };
						my $q_gene_id = $pair->{-q_gene};
						$q_gene_id = '\N'
						  if ( !defined $q_gene_id );

						#my $s_gene_id = $s_genes{ $pair->{-s_gene} };
						my $s_gene_id = $pair->{-s_gene};
						$s_gene_id = '\N'
						  if ( !defined $s_gene_id );

						my $type = $location_types{ $pair->{-type} };

						print SQLFILE << "EOSQL";
$alignment->{id}|$q_gene_id|$s_gene_id|$type
EOSQL
					}
				}

				print SQLFILE << "EOSQL";
\\.
EOSQL
				close SQLFILE;
`cd $SiteConfig::DATADIR/$accession/Alignments/$SUBDIR && $SiteConfig::BINDIR/psql -f $outfile -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname && $SiteConfig::BINDIR/bzip2 $infile && $SiteConfig::BINDIR/bzip2 $outfile && $SiteConfig::CMDDIR/touch $outfile.ok`;
			}
		}
	}
}
