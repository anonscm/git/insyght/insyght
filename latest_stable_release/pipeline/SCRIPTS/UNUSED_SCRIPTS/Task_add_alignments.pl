#!/usr/local/bin/perl
# script de mise a jour et construction de SeqDB , n� 3
# permet le lancement des taches permettant le calcul des alignements de g�nomes
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use DBI;
use Time::HiRes qw(sleep);
use SiteConfig;
use lib '$SiteConfig::SCRIPTSDIR';
use ORIGAMI;

my $CLUSTER;
my $DELETE_TEMP_FILES;

#get the arguments or exit with usage if the arguments are incorrect

foreach my $argnum ( 0 .. $#ARGV ) {

	if ( $ARGV[$argnum] =~ m/^-CLUSTER$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$CLUSTER = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -CLUSTER argument ; usage : perl Task_add_alignments.pl -CLUSTER {ON, OFF} -DELETE_TEMP_FILES {ON, OFF} \n\n";
			die
"incorrect -CLUSTER argument ; usage : perl Task_add_alignments.pl -CLUSTER {ON, OFF} -DELETE_TEMP_FILES {ON, OFF} \n\n";
		}
	}
	if ( $ARGV[$argnum] =~ m/^-DELETE_TEMP_FILES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DELETE_TEMP_FILES = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -DELETE_TEMP_FILES argument ; usage : perl Task_add_alignments.pl -CLUSTER {ON, OFF} -DELETE_TEMP_FILES {ON, OFF} \n\n";
			die
"incorrect -DELETE_TEMP_FILES argument ; usage : perl Task_add_alignments.pl -CLUSTER {ON, OFF} -DELETE_TEMP_FILES {ON, OFF} \n\n";
		}
	}

}
if (   !defined($CLUSTER)
	|| !defined($DELETE_TEMP_FILES) )
{
	print
"incorrect arguments : usage : perl Task_add_alignments.pl -CLUSTER {ON, OFF} -DELETE_TEMP_FILES {ON, OFF}";
	die
"incorrect arguments : usage : perl Task_add_alignments.pl -CLUSTER {ON, OFF} -DELETE_TEMP_FILES {ON, OFF}";

}

my $rows = $ORIGAMI::dbh->selectall_arrayref("SELECT accession FROM elements");

my $send_to_cluster;
if ( $CLUSTER =~ m/^ON$/i ) {
	$send_to_cluster = 1;
}
else {
	$send_to_cluster = 0;
}

# s'il n'existe pas, on cr�� le r�pertoire pour les fichiers de log
if ( !-e "$SiteConfig::LOGDIR" ) { mkdir("$SiteConfig::LOGDIR"); }

# s'il n'existe pas, on cr�� le sous-r�pertoire pour les fichiers de log des align
if ( !-e "$SiteConfig::LOGDIR/Align" ) { mkdir("$SiteConfig::LOGDIR/Align"); }

# ouvre en pipant sur tee.pl pour �crire � la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Align/Align.log"
);

print LOG "***Starting task $0 at ", scalar(localtime), "\n";

# parcours des genomes un par un
foreach my $row (@$rows) {

	my $accession = $row->[0];

	print LOG "***Accession: $accession. Starting task $0 at ",
	  scalar(localtime), "\n";

	print LOG
	  "***Accession: $accession. Starting alignements (align_entry.pl) at ",
	  scalar(localtime), "\n";

	my @args_for_align_entry = (
		"$SiteConfig::SCRIPTSDIR/align_entry.pl",
		"$accession",
		"$send_to_cluster",
		"$DELETE_TEMP_FILES"
	);
	if ( system(@args_for_align_entry) != 0 ) {
		print LOG "system @args_for_align_entry failed: $?\n";
		die("system @args_for_align_entry failed: $?\n");
	}
	#"print `$SiteConfig::SCRIPTSDIR/align_entry.pl $accession $send_to_cluster $DELETE_TEMP_FILES`;

	print LOG
	  "\n***Accession: $accession. Starting alignement result parsing at ",
	  scalar(localtime), "\n";
	  	my @args_for_parse_alignments = (
		"$SiteConfig::SCRIPTSDIR/parse_alignments.pl",
		"$accession"
	);
	if ( system(@args_for_parse_alignments) != 0 ) {
		print LOG "system @args_for_parse_alignments failed: $?\n";
		die("system @args_for_parse_alignments failed: $?\n");
	}
	#print `$SiteConfig::SCRIPTSDIR/parse_alignments.pl $accession`;

	print LOG
	  "\n***Accession: $accession. Starting alignement result import at ",
	  scalar(localtime), "\n";
	  	  	my @args_for_import_alignments = (
		"$SiteConfig::SCRIPTSDIR/import_alignments.pl",
		"$accession"
	);
	if ( system(@args_for_import_alignments) != 0 ) {
		print LOG "system @args_for_import_alignments failed: $?\n";
		die("system @args_for_import_alignments failed: $?\n");
	}
	#print `$SiteConfig::SCRIPTSDIR/import_alignments.pl $accession`;

	print LOG "\n***Accession: $accession. Task $0 completed at ",
	  scalar(localtime), "\n";
}

print LOG "All alignments added at : ", scalar(localtime), "\n";
print LOG
  "----------------------------------------------------------------------\n";
close(LOG);
