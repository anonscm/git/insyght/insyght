#!/usr/local/bin/perl
#
# Scripts permettant de reprendre Task_blastall en cas d'interuption pendant l'insertion des donn�es
# perl recover_insertion_db_Task_blast_all.pl -DELETE_TEMP_FILES OFF
# Date : 2010
# LT
#
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use SiteConfig;
use ORIGAMI;
my $DELETE_TEMP_FILES;
my %hash_of_all_premier_accnum_to_import = ();

# ouvre en pipant sur tee.pl pour �crire � la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/Task_blast_all.log"
);
print LOG
"\n---------------------------------------------------\nrecover_insertion_db_Task_blast_all.pl started at :",
  scalar(localtime), "\n";

foreach my $argnum ( 0 .. $#ARGV ) {

	if ( $ARGV[$argnum] =~ m/^-DELETE_TEMP_FILES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DELETE_TEMP_FILES = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -DELETE_TEMP_FILES argument ; usage : perl recover_insertion_db_Task_blast_all.pl -DELETE_TEMP_FILES = {ON, OFF}\n";
			die
"incorrect -DELETE_TEMP_FILES argument ; usage : perl recover_insertion_db_Task_blast_all.pl -DELETE_TEMP_FILES = {ON, OFF}\n";
		}
	}

}

if ( defined $DELETE_TEMP_FILES ) {
	print LOG
"\nYou choose the following options :\nDELETE_TEMP_FILES : $DELETE_TEMP_FILES\n\n";
}
else {
	die(
"incorrect arguments ; usage : perl recover_insertion_db_Task_blast_all.pl-DELETE_TEMP_FILES = {ON, OFF}"
	);
}

open( ACCNUM_ALREADY_IMPORTED_IN_DB,
	">> $SiteConfig::DATADIR/Task_blast_all/tmp/accnum_already_imported_in_db" )
  or die(
"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/accnum_already_imported_in_db\n"
  );
  
  print ACCNUM_ALREADY_IMPORTED_IN_DB "\nRECOVERING DB IMPORT...\n";
  
open( TO_FINISH_MASTER_SQL,
	">$SiteConfig::DATADIR/Task_blast_all/tmp/to_finish_master_sql" )
  or die(
"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/to_finish_master_sql\n"
  );
print TO_FINISH_MASTER_SQL "\\.\nCOMMIT WORK;\n";

#into a hashtable hash_of_all_premier_accnum_to_import
my @directories_basis_for_cat =
  <$SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/*>;
foreach my $file_basis_for_cat_in_test (@directories_basis_for_cat) {
	my $premier_accnum_in_test;
	if ( $file_basis_for_cat_in_test =~ m/^.+\/(\w+)VS.+$/ ) {
		$premier_accnum_in_test = $1;
		$hash_of_all_premier_accnum_to_import{$premier_accnum_in_test} = 1;
	}
	else {
		die(
"Error while attempting to parse outup files in $file_basis_for_cat_in_test, it didn't match the regex so the corresponding accnum couldn't be extracted. No changes in db. Contact a developper\n"
		);
	}
}

#cat every files under ORIGAMI_data/Task_blast_all/tmp/parse_blast_tabular_output into proper
LOOP_ACCNUM_TO_IMPORT:
foreach my $premier_accnum_to_import_in_test (
	keys %hash_of_all_premier_accnum_to_import )
{


	if (
		!-e "$SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql"
	  )
	{

		open( SQL_IMPORT_FILE,
"> $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql"
		  )
		  or die(
"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql\n"
		  );

	}
	else {
		
		if(-e "$SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql_import_done"){
			print ACCNUM_ALREADY_IMPORTED_IN_DB "The file $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql already exists, skipping insertion in the db as this file should have already been inserted into the db. Please check by doing a grep 'The file $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql has been imported in the db' du fichier $SiteConfig::DATADIR/Task_blast_all/tmp/accnum_already_imported_in_db.\n";
			print LOG "The file $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql already exists, skipping insertion in the db as this file should have already been inserted into the db. Please check by doing a grep 'The file $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql has been imported in the db' du fichier $SiteConfig::DATADIR/Task_blast_all/tmp/accnum_already_imported_in_db.\n";
			next LOOP_ACCNUM_TO_IMPORT;	
		}else{
			die(
"The file $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql already exists but not the file $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql_import_done, please check wether or not this file has already been inserted into the db and touch $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql_import_done if so or rm $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql if not.\n"
			);			
		}
		

	}

	print SQL_IMPORT_FILE "BEGIN WORK;\n";
	print SQL_IMPORT_FILE "COPY homologies FROM stdin;\n";
	my $count_line_sql_files = 0;
	my @sql_files_to_cat =
<$SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/${premier_accnum_to_import_in_test}*.sql>;
	foreach my $sql_file (@sql_files_to_cat) {
		print LOG
"concatenating $sql_file into $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql\n";
`cat $sql_file >> $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql`;
		my $count_line_this_sql_file = `$SiteConfig::CMDDIR/cat $sql_file | wc`;
		$count_line_this_sql_file =~ s/^\s*(\d+)\s+.*$/$1/;
		$count_line_sql_files =
		  $count_line_sql_files + $count_line_this_sql_file;
	}

`cat $SiteConfig::DATADIR/Task_blast_all/tmp/to_finish_master_sql >> $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql`;
	close(SQL_IMPORT_FILE);

	if ( $count_line_sql_files == 0 ) {

#die(
#"count_line_sql_files for ${premier_accnum_to_import_in_test} is null. Please contact the developper, changes may have been carried out to the database for other elements that ${premier_accnum_to_import_in_test}.\n"
#);
		print LOG
"***WARNING : count_line_sql_files for ${premier_accnum_to_import_in_test} is null. This mean this accnum do not have a single blast match with any other species. This is very unusual...\n";
		next LOOP_ACCNUM_TO_IMPORT;

	}
	my $count_line_master_sql_file =
`$SiteConfig::CMDDIR/cat $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql | wc`;

	$count_line_master_sql_file =~ s/^\s*(\d+)\s+.*$/$1/;
	print LOG
"count_line_master_sql_file = $count_line_master_sql_file\ncount_line_sql_files = $count_line_sql_files\n";

	if ( ( $count_line_sql_files + 4 ) != $count_line_master_sql_file ) {
		die(
"the file $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql has not the same number of lines (= $count_line_master_sql_file) as the cumul of every .sql files matching the pattern ${premier_accnum_to_import_in_test}*.sql under $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/ ( = $count_line_sql_files) + the header. No changes has been performed on the database.\n"
		);
	}
	else {
`$SiteConfig::BINDIR/psql -f $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname && $SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql_import_done`;

		if (
			!-e "$SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql_import_done"
		  )
		{
`$SiteConfig::CMDDIR/touch $SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED`;
			print LOG
"system $SiteConfig::BINDIR/psql -f $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname && $SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql_import_done failed: $?\n";
			exit(1);
		}

		print ACCNUM_ALREADY_IMPORTED_IN_DB
"The file $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql has been imported in the db\n";
		print
"The file $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/${premier_accnum_to_import_in_test}.sql has been imported in the db\n";
	}
}

close(TO_FINISH_MASTER_SQL);
close(ACCNUM_ALREADY_IMPORTED_IN_DB);

#delete files under DATADIR/Task_blast_all/tmp/XXX as needed unless stated otherwise as -DELETE_TEMP_FILES

if ( $DELETE_TEMP_FILES =~ m/^ON$/i ) {
	print LOG "Deleting temporary files under 
	$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb 
	and $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done 
	and $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files 
	and $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output 
	and $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output 
\n";
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb`;
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done`;
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files`;
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output`;
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output`;
}

#delete the file DATADIR/Task_blast_all/tmp/import_of_[list of new_elt_id].sql unless stated otherwise as -DELETE_TEMP_FILES argument
if ( $DELETE_TEMP_FILES =~ m/^ON$/i ) {
	print LOG
"Deleting temporary files under $SiteConfig::DATADIR/Task_blast_all/tmp/\n";
	`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/`;
}


print LOG
"\n---------------------------------------------------\n\n\n recover_insertion_db_Task_blast_all.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);
