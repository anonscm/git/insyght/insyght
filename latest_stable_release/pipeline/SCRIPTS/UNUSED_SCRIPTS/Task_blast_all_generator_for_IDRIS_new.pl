#!/usr/local/bin/perl
#
# Task_blast_all_generator_for_IDRIS.pl : Script principal permettant de créer tous les scripts shell pour l'IDRIS concernant le blast de Origami
#
# Date : 01/2014
# LT
#
#LOG FILE : $SiteConfig::LOGDIR/Blast/generate_script_IDRIS_Task_blast_all.log
#LAUNCHED AS : perl Task_blast_all_generator_for_IDRIS.pl -VERBOSE {ON, OFF}
#ARGUMENTS :
#VERBOSE : if ON print ant details at each step, if OFF only print critical information
#
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use Time::HiRes qw(sleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;

use lib '$SiteConfig::SCRIPTSDIR'; #AGJ
use BlastConfig; #AGJ

my $MAX_JOBS;
my $DELETE_TEMP_FILES;
my $VERBOSE;
my %all_element_id_from_table_elements            = ();
my %new_accnum_from_listing_dat_files             = ();
my %new_accnum_from_gr2species_migale             = ();
my %new_elements_id_missing_from_table_homologies = ();
my @element_id_accepted_as_new                    = ();
my @element_id_accepted_as_new_unsorted           = ();

my $THRESH   = $BlastConfig::params->{-thresh};  #AGJ

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::LOGDIR/Task_blast_all/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_generator_for_IDRIS.log"
);
print LOG
"\n---------------------------------------------------\n Task_blast_all_generator_for_IDRIS.pl started at :",
  scalar(localtime), "\n";

#set the variables according to the argument or exit with usage if the arguments are incorrect

foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -VERBOSE argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl  -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF}";
			die
"incorrect -VERBOSE argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl  -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF}";
		}

	}

	if ( $ARGV[$argnum] =~ m/^-FAA$/ ) {
	    if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FAA = $ARGV[ $argnum + 1 ];
		}
		else {
			print 
"incorrect -FAA argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl  -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -FAA = {ON, OFF}";
			die
"incorrect -FAA argument ; usage : perl Task_blast_all_generator_for_IDRIS.pl  -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -FAA = {ON, OFF}";
		}


	    $FAA = $ARGV[ $argnum + 1 ];
        }


}
if (defined $VERBOSE ) {
	print LOG
"\nYou choose the following options :\n\tVERBOSE : $VERBOSE\n"
	  unless $VERBOSE =~ m/^OFF$/;
}
else {
	print LOG
"incorrect arguments ; usage : perl Task_blast_all_generator_for_IDRIS.pl -VERBOSE = {ON, OFF}";
	die
"incorrect arguments ; usage : perl Task_blast_all_generator_for_IDRIS.pl -VERBOSE = {ON, OFF}";
}

if (defined $FAA ) {
	print LOG
"\nYou choose the following options :\n\tFAA : $FAA\n"
	  unless $VERBOSE =~ m/^OFF$/;
}
else {
	print LOG
"incorrect arguments -FAA; usage : perl Task_blast_all_generator_for_IDRIS.pl -VERBOSE = {ON, OFF} -FAA = {ON, OFF}";
	die
"incorrect arguments -FAA; usage : perl Task_blast_all_generator_for_IDRIS.pl -VERBOSE = {ON, OFF} -FAA {ON, OFF}";
}


#create the following empty directories if they do not exists : DATADIR/Task_blast_all/tmp/,  DATADIR/Task_blast_all/tmp/fasta_formatdb/, DATADIR/Task_blast_all/tmp/blast_output/, DATADIR/Task_blast_all/tmp/launch_blast_batch_files/, DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/, ....

print LOG "Deleting files under $SiteConfig::DATADIR/Task_blast_all/tmp/ \n";
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/`;


#check attribute number_fragment_hit is in table homologies
$ORIGAMI::dbh->selectall_arrayref(
	"SELECT number_fragment_hit FROM homologies LIMIT 1")
  or die "no column number_fragment_hit in table homologies";

#QUERRY ALL elements_id : Origami database query : all rows of table elements, showing columns accession, element_id and store in hashtable all_element_id_from_table_elements = accnum
my $st = $ORIGAMI::dbh->prepare("SELECT accession, element_id FROM elements");
$st->execute
  or die("Pb execute $!");
while ( my $res = $st->fetchrow_hashref() ) {
	$all_element_id_from_table_elements{ $res->{element_id} } =
	  $res->{accession};
}

#List files under $WORKDIR/ORIGAMI_bank/gr_mirror with extension *.dat or .gbk and add accnum to hashtable  new_accnum_from_listing_dat_files{accnum} ++. If the value of  hashtable  new_accnum_from_listing_dat_files{accnum} > 1 then write it in LOG as duplication.
my @started         = <$SiteConfig::BANKDIR/*.dat>;
my @started_for_gbk = <$SiteConfig::BANKDIR/*.gbk>;
push( @started, @started_for_gbk );

foreach my $file (@started) {

	#print "file : $file\n";
	if ( $file =~ m/^.+\/(.+)\.dat$/ || $file =~ m/^.+\/(.+)\.gbk$/ ) {
		my $accnum = $1;
		$new_accnum_from_listing_dat_files{$accnum} = 1;
	}
	else {
		die(
"the file $file do not match the m/.+\/(.+)\.dat/ or m/.+\/(.+)\.gbk/ pattern, please contact the developper.\n"
		);
	}
}

#read file $WORKDIR/ORIGAMI_bank/gr_mirror/gr2species_migale.txt and parse its lines to add the accnum to hashtable  new_accnum_from_gr2species_migale{accnum}++ (skip line starting with comment #). If the value of  hashtable  new_accnum_from_listing_dat_files{accnum} > 1 then write it in LOG as duplication.

open( GR2SPECIES_MIGALE, "<$SiteConfig::BANKDIR/gr2species_migale.txt" )
  or die("Can not open $SiteConfig::BANKDIR/gr2species_migale.txt\n");
LINE_GR2SPECIES_MIGALE: while ( my $line = <GR2SPECIES_MIGALE> ) {
	chomp($line);
	my @split = split( '\s+', $line );
	if ( $line =~ m/^#/ ) {
		print
"skipping line $line in $SiteConfig::BANKDIR/gr2species_migale.txt as it starts with a comment\n";
		next LINE_GR2SPECIES_MIGALE;

	}
	$new_accnum_from_gr2species_migale{ $split[1] } =
	  $new_accnum_from_gr2species_migale{ $split[1] } + 1;
	if ( $new_accnum_from_gr2species_migale{ $split[1] } > 1 ) {
		die(
"the file $SiteConfig::BANKDIR/gr2species_migale.txt has a redundancy for the accession number $split[1], please check this file.\n"
		);
	}
}

#for each results from QUERRY ALL elements_id (current_element_id)

my $flag_diff_table_elet_homologies = 0;
foreach my $current_element_id ( keys %all_element_id_from_table_elements ) {

#Origami database query : rows of table homologies where q_element_id =  current_element_id or s_element_id =  current_element_id, showing columns q_element_id
	my $res2 = $ORIGAMI::dbh->selectall_arrayref(
"SELECT q_element_id FROM homologies where q_element_id = $current_element_id LIMIT 1"
	);
	my $res3 = $ORIGAMI::dbh->selectall_arrayref(
"SELECT q_element_id FROM homologies where s_element_id = $current_element_id LIMIT 1"
	);

#if there isn't any row returned by the previous querry, add the current_element_id into hasatble new_elements_id_missing_from_table_homologies{current_element_id}=1.

	if ( !scalar(@$res2) && !scalar(@$res3) ) {
		$new_elements_id_missing_from_table_homologies{$current_element_id} = 1;
		$flag_diff_table_elet_homologies = 1;
	}

	$res2 = 0;
	$res3 = 0;
}

#die if no difference between table elements and homologies
if ( $flag_diff_table_elet_homologies == 0 ) {

	die
"All the element_id from table elements have at least one entry in the table homologies, therefore there is nothing to do. Have you run Task_add_entry on the entry you wish to add ? If the entry is an update, have the rows for this elements_id have been deleted in the table homologies ?\n";

}

#check that the listing of the .dat or .gbk files and what is in the file  gr2species_migale.txt are coherent : for each keys of  hashtable  new_accnum_from_listing_dat_files
foreach my $current_accnum_from_listing_dat_files (
	keys %new_accnum_from_listing_dat_files )
{

#if it is present in  hashtable new_accnum_from_gr2species_migale then delete the key in new_accnum_from_gr2species_migale
	if (
		exists $new_accnum_from_gr2species_migale{
			$current_accnum_from_listing_dat_files} )
	{
		delete $new_accnum_from_gr2species_migale{
			$current_accnum_from_listing_dat_files};
	}
	else {

#else write it in LOG as WARNING that this .dat or .gbk file has no equivalent in the file gr2species_migale.txt
		print LOG
"***WARNING : the .dat or .gbk file corresponding to the accession number $current_accnum_from_listing_dat_files has no equivalent in the file $SiteConfig::BANKDIR/gr2species_migale.txt\n";
	}
}

#if hashtable new_element_id_from_gr2species_migale.txt is not empty, write in LOG all the remaining keys as not being found under $WORKDIR/ORIGAMI_bank/gr_mirror as a file with extension .dat or .gbk
if ( scalar( keys %new_accnum_from_gr2species_migale ) > 0 ) {
	foreach my $current_new_accnum_from_gr2species_migale (
		keys %new_accnum_from_gr2species_migale )
	{
		print LOG
"***WARNING : the accession number $current_new_accnum_from_gr2species_migale listed as new in the file $SiteConfig::BANKDIR/gr2species_migale.txt is not being found under $SiteConfig::BANKDIR/ as a file with extension .dat or .gbk\n";
	}
}

#for each keys of  hashtable new_elements_id_missing_from_table_homologies
foreach my $current_new_elements_id_missing_from_table_homologies (
	keys %new_elements_id_missing_from_table_homologies )
{

#if it is present in hashtable new_element_id_from_listing_dat_files, then delete the key in new_element_id_from_listing_dat_files and add it to the array  element_id_accepted_as_new
	my $accnum_correspondance;
	if (
		exists $all_element_id_from_table_elements{
			$current_new_elements_id_missing_from_table_homologies} )
	{
		$accnum_correspondance =
		  $all_element_id_from_table_elements{
			$current_new_elements_id_missing_from_table_homologies};
	}
	else {
		die(
"the element id $current_new_elements_id_missing_from_table_homologies is not found in the table elements. Please see the developper.\n"
		);
	}

	if ( !defined($accnum_correspondance) ) {
		die(
"the element id $current_new_elements_id_missing_from_table_homologies could not be associated with an accession number in the table elements. Please see the developper.\n"
		);
	}

	if ( exists $new_accnum_from_listing_dat_files{$accnum_correspondance} ) {
		delete $new_accnum_from_listing_dat_files{$accnum_correspondance};
		push( @element_id_accepted_as_new_unsorted,
			$current_new_elements_id_missing_from_table_homologies );
		print LOG
"The element id $current_new_elements_id_missing_from_table_homologies (accession number $accnum_correspondance ) has been added as new or updated genome and blast will be launched fo it\n";
	}
	else {

#else write it in LOG as WARNING that this accnum (elet id) isn't present in the table homologies in the database but as no equivalent .dat or .gbk file under directory $WORKDIR/ORIGAMI_bank/gr_mirror/.
		print LOG
"*** WARNING : the element id $current_new_elements_id_missing_from_table_homologies (accession number $accnum_correspondance ) is not present in the table homologie in the database but as no equivalent .dat or .gbk file under directory $SiteConfig::BANKDIR/ . Therefore it is not considered as being a new or updated genome and no blast will be launched for it.\n";

	}

}

#if hashtable new_element_id_from_listing_dat_files is not empty, write all keys as being as .dat or .gbk file under directory $WORKDIR/ORIGAMI_bank/gr_mirror/ but having already an entry into the table homologies of the database as q_element_id or s_element_id, if you think this is an error please manually delete those rows from the table homologies
if ( scalar( keys %new_accnum_from_listing_dat_files ) > 0 ) {
	foreach my $current_new_accnum_from_listing_dat_files (
		keys %new_accnum_from_listing_dat_files )
	{

		die(
"the accession number $current_new_accnum_from_listing_dat_files listed is found under $SiteConfig::BANKDIR/ as a file with extension .dat or .gbk but is already having at least one entry into the table homologies of the database as q_element_id or s_element_id. Therefore it is not considered as being a new or updated genome and no blast will be launched for it. Exiting, please manually delete those rows from the table homologies.\n"
		);
		
	}
}
@element_id_accepted_as_new = sort @element_id_accepted_as_new_unsorted;

#LOOP1 : For each elements_id from array accepted_as_new (new_elt_id)

# AGJ
	open( BLAST_BATCHFILE,
	"> $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast.ll"
	  )
	  or die(
	"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast.ll\n"
	  );
#AGJ

LOOP1:
foreach my $curr_element_id_accepted_as_new (@element_id_accepted_as_new) {


	# get the corresponding accession number
	my $accnum_correspondance_curr_element_id_accepted_as_new;
	if (
		exists
		$all_element_id_from_table_elements{$curr_element_id_accepted_as_new} )
	{
		$accnum_correspondance_curr_element_id_accepted_as_new =
		  $all_element_id_from_table_elements{$curr_element_id_accepted_as_new};
	}
	else {
		die(
"the element id $curr_element_id_accepted_as_new is not found in the table elements. Please see the developper.\n"
		);
	}
	if ( !defined($accnum_correspondance_curr_element_id_accepted_as_new) ) {
		die(
"the element id $curr_element_id_accepted_as_new could not be associated with an accession number in the table elements. Please see the developper.\n"
		);
	}

	my $num_keys_all_element_id_from_table_elements =
	  keys %all_element_id_from_table_elements;

	#LOOP2 : for each results from QUERRY ALL elements_id (curr_elt_id)

LOOP2:	foreach my $current_element_id ( keys %all_element_id_from_table_elements )
	{


		# get the corresponding accession number
		my $accnum_correspondance_current_element_id;
		if ( exists $all_element_id_from_table_elements{$current_element_id} ) {
			$accnum_correspondance_current_element_id =
			  $all_element_id_from_table_elements{$current_element_id};
		}
		else {
			die(
"the element id $current_element_id is not found in the table elements. Please see the developper.\n"
			);
		}
		if ( !defined($accnum_correspondance_current_element_id) ) {
			die(
"the element id $current_element_id could not be associated with an accession number in the table elements. Please see the developper.\n"
			);
		}

if ($FAA =~ m/^ON/ ) {  ## AGJ Debut fichier FAA
 
#launch perl gen_faa_db.pl  $new_elet_id  $curr_elet_id to generate the fasta file prepared with formatdb both for new_elet_id and curr_elt_id if they aren't already there
		my @args_for_gen_faa_db = (
			"$SiteConfig::SCRIPTSDIR/gen_faa_db.pl",
			"$curr_element_id_accepted_as_new",
			"$current_element_id",
			"$VERBOSE",
			"OFF" # do not perform the formatdb
		);
		if ( system(@args_for_gen_faa_db) != 0 ) {
			print LOG "ERROR : system @args_for_gen_faa_db failed: $?\n";
			exit(1);
		}

		#wait until file $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}_fasta_done is here
		my $count_go_for_blast = 0;
		while (
			!-e "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${curr_element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_fasta_done"
			&& !
			-e "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id}_fasta_done"
		  )
		{
			if ( $count_go_for_blast == 0 ) {
				print LOG "waiting for files 
				$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${curr_element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_fasta_done
				 and $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id}_fasta_done
				 to appear\n" unless $VERBOSE =~ m/^OFF$/i;
			}
			sleep(1);
			$count_go_for_blast++;
			if ( $count_go_for_blast > 900 ) {
				print LOG "waited more than 15 minutes for files 
				$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${curr_element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_fasta_done
				 and $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id}_fasta_done
				 to appear\n";
				exit(1);
			}
		}
		if ( $count_go_for_blast > 0 ) {
			print LOG "waited $count_go_for_blast seconds for files 
				$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${curr_element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_fasta_done
				 and $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id}_fasta_done
				 to appear\n" unless $VERBOSE =~ m/^OFF$/i;
		}

	    }  ##AGJ Fin fichier FAA




#AGJ
# print formatdb ${current_element_id}_${accnum_correspondance_current_element_id} if not done already
if( !-e "$SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${current_element_id}_${accnum_correspondance_current_element_id}.makeblastdb.ll"){
	open( MAKEBLASTDB_BATCHFILE_CURR,
	"> $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${current_element_id}_${accnum_correspondance_current_element_id}.makeblastdb.ll"
	  )
	  or die(
	"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${current_element_id}_${accnum_correspondance_current_element_id}.makeblastdb.ll\n"
	  );
	
	print MAKEBLASTDB_BATCHFILE_CURR "$SiteConfig::FORMATDB -in $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id}.faa -input_type fasta -dbtype prot -out $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id};	
	\n";
	close(MAKEBLASTDB_BATCHFILE_CURR);
}
#AGJ
#AGJ deb generate batch file 


	print BLAST_BATCHFILE
	"time blastp -outfmt 6 -evalue $THRESH -db $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id} -query $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${curr_element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.faa -out $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${curr_element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast\n";
	print BLAST_BATCHFILE
"time blastp -outfmt 6 -evalue $THRESH -db $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${curr_element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new} -query $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id}.faa -out $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${curr_element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast\n";
#AGJ


	}#foreach loop2
}#foreach loop1


#end of script
print LOG
"\n---------------------------------------------------\n\n\ngenerate_script_IDRIS_Task_blast_all.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);

close(BLAST_BATCHFILE);




