#!/usr/local/bin/perl

# Script de construction et mise � jour de Origami - version cluster
# Permet de lancer les blasts sur le cluster sous forme de batch
#
# Date : 04/2009
# LT
# Please have a look at the documentation (algorithmics) for this script in ORIGAMI/DOC/Algorithmics_of_scripts
#
#GOAL : This script does the following tasks :
#create a batchfile to perform the following blast on the cluster :
#new_elt_id VS curr_elt_id if needed
#and curr_elt_id VS new_elt_id if new_elt_id != curr_elt_id if needed
#send the batchfile to be executed by the cluster
#wait for it to complete and then launch parse_blast.pl on the output
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use SiteConfig;
use lib '$SiteConfig::SCRIPTSDIR';
use BlastConfig;
use Time::HiRes qw(sleep);

#script level variable
my $BLASTCMD = $BlastConfig::params->{-command};
my $THRESH   = $BlastConfig::params->{-thresh};
my $element_id_accepted_as_new;
my $accnum_correspondance_curr_element_id_accepted_as_new;
my $current_element_id;
my $accnum_correspondance_current_element_id;
my $VERBOSE;
my $submit_to_cluster            = 0;
my @registered_files_to_wait_for = ();
my @registered_files_to_parse    = ();
my $RECOVER_FROM_FAILURE;
my $do_batchfile                 = 1;
my $marker1_of_dont_do_batchfile = 0;
my $marker2_of_dont_do_batchfile = 0;
my $do_the_parsing_of_blast_file = 0;
my $cmd_cluster_or_bash = "$SiteConfig::CMDDIR/bash";

#get the arguments or exit with usage if the arguments are incorrect

foreach my $argnum ( 0 .. $#ARGV ) {

	if ( $argnum == 0 ) {
		if ( $ARGV[$argnum] =~ m/^\d+$/ ) {
			$element_id_accepted_as_new = $ARGV[$argnum];
		}
		else {
			print
"incorrect 1st argument (element_id_accepted_as_new) : must be integer.\n";
			die
"incorrect 1st argument (element_id_accepted_as_new) : must be integer.\n";
		}

	}
	elsif ( $argnum == 1 ) {
		if ( $ARGV[$argnum] =~ m/^\w+$/ ) {
			$accnum_correspondance_curr_element_id_accepted_as_new =
			  $ARGV[$argnum];
		}
		else {
			print
"incorrect 2nd argument (accnum_correspondance_curr_element_id_accepted_as_new) : must be alphanumeric plus _.\n";
			die
"incorrect 2nd argument (accnum_correspondance_curr_element_id_accepted_as_new) : must be alphanumeric plus _.\n";
		}

	}
	elsif ( $argnum == 2 ) {
		if ( $ARGV[$argnum] =~ m/^\d+$/ ) {
			$current_element_id = $ARGV[$argnum];
		}
		else {
			print
"incorrect 3rd argument (current_element_id) : must be integer.\n";
			die
"incorrect 3rd argument (current_element_id) : must be integer.\n";
		}

	}
	elsif ( $argnum == 3 ) {
		if ( $ARGV[$argnum] =~ m/^\w+$/ ) {
			$accnum_correspondance_current_element_id = $ARGV[$argnum];
		}
		else {
			print
"incorrect 4th argument (accnum_correspondance_current_element_id) : must be alphanumeric plus _.\n";
			die
"incorrect 4th argument (accnum_correspondance_current_element_id) : must be alphanumeric plus _.\n";
		}

	}
	elsif ( $argnum == 4 ) {
		if ( $ARGV[$argnum] =~ m/^ON$/i || $ARGV[$argnum] =~ m/^OFF$/i ) {
			$VERBOSE = $ARGV[$argnum];
		}
		else {
			print "incorrect 5th argument (VERBOSE) : must be ON or OFF.\n";
			die "incorrect 5th argument (VERBOSE) : must be ON or OFF.\n";
		}

	}
	elsif ( $argnum == 5 ) {
		if ( $ARGV[$argnum] =~ m/^ON$/i || $ARGV[$argnum] =~ m/^OFF$/i ) {
			$RECOVER_FROM_FAILURE = $ARGV[$argnum];
		}
		else {
			print
"incorrect 6th argument (RECOVER_FROM_FAILURE) : must be ON or OFF.\n";
			die
"incorrect 6th argument (RECOVER_FROM_FAILURE) : must be ON or OFF.\n";
		}

	}
	elsif ( $argnum == 6 ) {
		if ( $ARGV[$argnum] =~ m/^0$/i) {
			#do nothing
			print "not using the cluster, command is bash.\n";
		}else{
			if ( $ARGV[$argnum] =~ m/^.+$/i) {
				$cmd_cluster_or_bash = $ARGV[$argnum];
				print "using the cluster, command is $cmd_cluster_or_bash.\n";
			}
		}
		
	}
	else {
		print
		  "The argument $ARGV[ $argnum ] has not been taking into account\n";
	}

}

if (   !defined($element_id_accepted_as_new)
	|| !defined($current_element_id)
	|| !defined($VERBOSE)
	|| !defined($RECOVER_FROM_FAILURE)
	|| !defined($accnum_correspondance_curr_element_id_accepted_as_new)
	|| !defined($accnum_correspondance_current_element_id) )
{
	print
"incorrect arguments : usage launch_blast.pl [INTEGER -> elet_id of 1st element] [ACCNUM -> accession number of 1st element] [INTEGER -> elet_id of 2nd element] [ACCNUM -> accession number of 2nd element] [ON,OFF] [ON,OFF]\n";
	die
"incorrect arguments : usage launch_blast.pl [INTEGER -> elet_id of 1st element] [ACCNUM -> accession number of 1st element] [INTEGER -> elet_id of 2nd element] [ACCNUM -> accession number of 2nd element] [ON,OFF] [ON,OFF]\n";
}

# ouvre en pipant sur tee.pl pour �crire � la fois sur la sortie standard et dans le fichier de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/launch_blast`;
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.log"
);
print LOG
"\n---------------------------------------------------\nlaunch_blast.pl started at :",
  scalar(localtime),
" for new element id = $element_id_accepted_as_new VS elet id = $current_element_id\n"
  unless $VERBOSE =~ m/^OFF$/i;
print LOG "command for bash / cluster is $cmd_cluster_or_bash.\n";

# set everything right if $RECOVER_FROM_FAILURE MODE ON
if ( $RECOVER_FROM_FAILURE =~ m/^ON$/i ) {
	if (
		!-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_done"
		&& !
		-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_started_RECUP"

	  )
	{
`$SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_started_RECUP`;
		print LOG
"File $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_done do not exists\n";

		if (
			-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_started"
		  )
		{
`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_started`;
		}

		if (
			-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast"
		  )
		{
`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast`;
		}

		if (
			-e "$SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.parsing_done"
		  )
		{
`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.parsing_done`;
		}

	}
	else {
		$marker1_of_dont_do_batchfile = 1;
		print LOG
"The files $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_done
have been found.\n";
	}

	if (
		$current_element_id != $element_id_accepted_as_new
		&& !
		-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_done"
		&& !
		-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_started_RECUP"

	  )
	{
`$SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_started_RECUP`;
		print LOG
"File $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_done do not exists\n";

		if (
			-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_started"
		  )
		{
`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_started`;
		}

		if (
			-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast"
		  )
		{
`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast`;
		}
		if (
			-e "$SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.parsing_done"
		  )
		{
`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.parsing_done`;
		}
	}
	else {
		$marker2_of_dont_do_batchfile = 1;
		print LOG
"The files $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_done
have been found.\n";
	}

	if (   $marker1_of_dont_do_batchfile == 1
		&& $marker2_of_dont_do_batchfile == 1 )
	{
		$do_batchfile = 0;
	}

	if (

		!-e "$SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.parsing_done"
		&& !
		-e "$SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.parsing_started_RECUP"

	  )
	{
`$SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.parsing_started_RECUP`;
		print LOG
"File $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.parsing_done do not exists.\n";
		$registered_files_to_wait_for[0] =
"$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_done";
		$registered_files_to_parse[0] =
"$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast";
		$do_the_parsing_of_blast_file = 1;
		if (
			-e "$SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.sql"
		  )
		{
			print LOG
"deleting $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.sql\n";
`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.sql`;
		}

	}
	else {
		print LOG
"The files $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.parsing_done
have been found\n";
	}

	if (
		$current_element_id != $element_id_accepted_as_new
		&& !
		-e "$SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.parsing_done"
		&& !
		-e "$SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.parsing_started_RECUP"

	  )
	{
`$SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.parsing_started_RECUP`;
		print LOG
"File $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.parsing_done do not exists\n";

		$registered_files_to_wait_for[1] =
"$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_done";
		$registered_files_to_parse[1] =
"$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast";
		$do_the_parsing_of_blast_file = 1;

		if (
			-e "$SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.sql"
		  )
		{
			print LOG
"deleting $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.sql\n";
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.sql`;
		}

	}
	else {
		print LOG
"The files $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.parsing_done
have been found\n";
	}

}

if ( $do_batchfile == 1 ) {

#open a new BATCHFILE with name DATADIR/Task_blast_all/tmp/launch_blast_batch_files/$new_elet_id_$corresponding_accnum_in_hashtable_VS_$curr_elet_id_$corresponding_accnum_in_hashtable.ll (make sure to open the file in mode truncated, not appending to an existing file with that name) and print the header

	open( BATCHFILE,
"> $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll"
	  )
	  or die(
"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll\n"
	  );
	print BATCHFILE "#!/bin/bash
#\$ -S /bin/bash
# Avertir au d�but (b)egin, � la fin (e)nd, � l'�liminaton (a)bort et
# � la suspension (s)uspend d'un job
#\$ -m as 
# Adresse mail � laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#\$ -o $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.out
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -e $SiteConfig::LOGDIR/Task_blast_all/launch_blast/launch_blast_for_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.err
# Lance la commande depuis le r�pertoire o� est lanc� le script
#\$ -cwd

export BLASTMAT=\"$SiteConfig::BLASTMAT\"

BLAST=$BLASTCMD\n";

#print on BATCHFILE to perform a blastp of the file DATADIR/Task_blast_all/tmp/fasta_formatdb/$new_elet_id_$accnum.faa against the file DATADIR/Task_blast_all/tmp/fasta_formatdb/$curr_elet_id_$accnum.faa and direct the output file to be  DATADIR/Task_blast_all/tmp/blast_output/$new_elet_id_$corresponding_accnum_in_hashtable_VS_$curr_elet_id_$corresponding_accnum_in_hashtable ; specify output as tabular format and evalue
	if (
		!-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_started"
	  )
	{
`$SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_started`;
		$submit_to_cluster = 1;
		$registered_files_to_wait_for[0] =
"$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_done";
		$registered_files_to_parse[0] =
"$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast";

#		#TEST
#		if($element_id_accepted_as_new == 6){ #5 ou 6 && $current_element_id == 3
#		print BATCHFILE
#"time \$BLAST -p blastp -m 8 -e $THRESH -d $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id} -i $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.faa -o $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast\n";
#		}else{
		print BATCHFILE
"time \$BLAST -p blastp -m 8 -e $THRESH -d $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id} -i $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.faa -o $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast && touch $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_done\n";

#		}

	}
	else {
		print LOG
"The command time \$BLAST -p blastp -m 8 -e $THRESH -d $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id} -i $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.faa -o $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast has already been started as the marker file $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast_started already exists.\n"
		  unless $VERBOSE =~ m/^OFF$/i;
	}

#if $new_elt_id != $curr_elt_id then print on BATCHFILE to perform a blastp of the file DATADIR/Task_blast_all/tmp/fasta_formatdb/$curr_elet_id_$accnum.faa against the file DATADIR/Task_blast_all/tmp/fasta_formatdb/$new_elet_id_$accnum.faa and direct the output file to be DATADIR/Task_blast_all/tmp/blast_output/$curr_elet_id_$corresponding_accnum_in_hashtable_VS_$new_elet_id_$corresponding_accnum_in_hashtable ; specify output as tabular format and evalue
	if ( $current_element_id != $element_id_accepted_as_new
		&& !
		-e "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_started"
	  )
	{
`$SiteConfig::CMDDIR/touch $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_started`;
		$submit_to_cluster = 1;
		$registered_files_to_wait_for[1] =
"$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_done";
		$registered_files_to_parse[1] =
"$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast";

#		#TEST
#		if($element_id_accepted_as_new == 5){ #5 ou 6
#		print BATCHFILE
#"time \$BLAST -p blastpTESTESTEST -m 8 -e $THRESH -d $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new} -i $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id}.faa -o $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast && touch $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_done\n";
# ou blastpTESTESTEST
#		}else{
		print BATCHFILE
"time \$BLAST -p blastp -m 8 -e $THRESH -d $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new} -i $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id}.faa -o $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast && touch $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_done\n";

#		}

	}
	else {
		if ( $current_element_id != $element_id_accepted_as_new ) {
			print LOG
"The command time \$BLAST -p blastp -m 8 -e $THRESH -d $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new} -i $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${current_element_id}_${accnum_correspondance_current_element_id}.faa -o $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast has already been started as the marker file $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/${current_element_id}_${accnum_correspondance_current_element_id}_VS_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}.blast_started already exists\n"
			  unless $VERBOSE =~ m/^OFF$/i;
		}
	}

	#close BATCHFILE and qsub -m ea the BATCHFILE ON the cluster
	close(BATCHFILE);
}

#`qsub -m ea -q short.q $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll` if there is any blast to submit;
if ( $submit_to_cluster == 1 ) {
	if (
		system(
"$cmd_cluster_or_bash $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll"
		) != 0
	  )
	{
`$SiteConfig::CMDDIR/touch $SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED`;
		print LOG
"$cmd_cluster_or_bash $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll failed: $?\n";
		exit(1);
	}
	$do_the_parsing_of_blast_file = 1;
}
else {

	print LOG
"No blast to launch in $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll\n"
	  unless $VERBOSE =~ m/^OFF$/i;
}

if ( $do_the_parsing_of_blast_file == 1 ) {

#wait that the job is finished on the cluster by checking the empty marker file DATADIR/Task_blast_all/tmp/blast_done/$new_elet_id_$corresponding_accnum_in_hashtable_VS_$curr_elet_id_$corresponding_accnum_in_hashtable.blast_done. But if the waiting time is too long (more than 40 minutes), then consider it a failure : add into array launch_blast_failure = command that did fail is perl launch_blast.pl $new_elet_id  $curr_elet_id as it took more than 40 minute for this process to complete on the cluster

	my $flag_first_parsing_done;
	my $flag_second_parsing_done;

	if ( defined $registered_files_to_wait_for[0] ) {
		print LOG
"The file $registered_files_to_parse[0] is registered to be parsed out at position 0\n"
		  unless $VERBOSE =~ m/^OFF$/i;
		$flag_first_parsing_done = 0;
	}
	else {
		print LOG "no registered file at position 0 to be parsed out\n"
		  unless $VERBOSE =~ m/^OFF$/i;
		$flag_first_parsing_done = 1;
	}

	if ( defined $registered_files_to_wait_for[1] ) {
		print LOG
"The file $registered_files_to_parse[1] is registered to be parsed out at position 1\n"
		  unless $VERBOSE =~ m/^OFF$/i;
		$flag_second_parsing_done = 0;
	}
	else {
		print LOG "No registered file at position 1 to be parsed out\n"
		  unless $VERBOSE =~ m/^OFF$/i;
		$flag_second_parsing_done = 1;
	}

	my $count_wait_for_parsing = 0;

	while ($flag_first_parsing_done == 0
		|| $flag_second_parsing_done == 0 )
	{
		if ( $flag_first_parsing_done == 0 ) {
			if (   -e "$registered_files_to_wait_for[0]"
				&& -e "$registered_files_to_parse[0]" )
			{

# parse it if the file exists and is not empty by launching perl parse_blast_tabular.pl $registered_files_to_wait_for[0]
				my @args_for_parse_blast_tabular = (
					"$SiteConfig::SCRIPTSDIR/parse_blast_tabular.pl",
					"$registered_files_to_parse[0]",
					"$element_id_accepted_as_new",
					"$accnum_correspondance_curr_element_id_accepted_as_new",
					"$current_element_id",
					"$accnum_correspondance_current_element_id",
					"$VERBOSE",
					"OFF",
					"OFF"
				);
				print LOG
"launching parsing for file $registered_files_to_parse[0] at position 0\n"
				  unless $VERBOSE =~ m/^OFF$/i;
				if ( system(@args_for_parse_blast_tabular) != 0 ) {
`$SiteConfig::CMDDIR/touch $SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED`;
					print LOG
					  "system @args_for_parse_blast_tabular failed: $?\n";
					exit(1);
				}
				else {
					print LOG
" parsing for file $registered_files_to_parse[0] at position 0 is finished with\n"
					  unless $VERBOSE =~ m/^OFF$/i;
					$flag_first_parsing_done = 1;
				}
			}
		}
		if ( $flag_second_parsing_done == 0 ) {
			if (   -e "$registered_files_to_wait_for[1]"
				&& -e "$registered_files_to_parse[1]" )
			{

# parse it if the file exists and is not empty by launching perl parse_blast_tabular.pl $registered_files_to_wait_for[1]
				my @args_for_parse_blast_tabular = (
					"$SiteConfig::SCRIPTSDIR/parse_blast_tabular.pl",
					"$registered_files_to_parse[1]",
					"$current_element_id",
					"$accnum_correspondance_current_element_id",
					"$element_id_accepted_as_new",
					"$accnum_correspondance_curr_element_id_accepted_as_new",
					"$VERBOSE",
					"OFF",
					"OFF"
				);
				print LOG
"launching parsing for file $registered_files_to_parse[1] at position 1\n"
				  unless $VERBOSE =~ m/^OFF$/i;
				if ( system(@args_for_parse_blast_tabular) != 0 ) {
`$SiteConfig::CMDDIR/touch $SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED`;
					print LOG
					  "system @args_for_parse_blast_tabular failed: $?\n";
					exit(1);
				}
				else {
					print LOG
"parsing for file $registered_files_to_parse[1] at position 1 is finished\n"
					  unless $VERBOSE =~ m/^OFF$/i;
					$flag_second_parsing_done = 1;
				}

#				}
#				else {
#`$SiteConfig::CMDDIR/touch $SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED`;
#					print LOG
#"***WARNING : The blast output file $registered_files_to_parse[1] generated by the shell script $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll does not exists\n";
#					exit(1);
#				}
			}
		}
		sleep(3);
		$count_wait_for_parsing++;

		# if wait more than 90 minutes in this loop, exite with errors
		if ( $count_wait_for_parsing > 2700 ) {
`$SiteConfig::CMDDIR/touch $SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED`;
			print LOG
"The waiting time for the generation of the blast output by the shell script $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll is greater than 90 minutes.\nExiting with errors.\n";
			exit(1);
		}
	}

	print LOG
"The waiting time for the generation of the blast output by the shell script $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/batch_${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.ll took ",
	  $count_wait_for_parsing * 20, " seconds\n"
	  unless $VERBOSE =~ m/^OFF$/i;

}

print LOG "launch_blast.pl completed succesfully at :", scalar(localtime),
  "\n---------------------------------------------------\n"
  unless $VERBOSE =~ m/^OFF$/i;
close(LOG);
