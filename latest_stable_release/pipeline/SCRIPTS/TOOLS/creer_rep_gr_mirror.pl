#!/usr/bin/perl -w
#
# Script permettant de copier les fichiers genome_reviews (.dat)
# dans le r�pertoire $SiteConfig::BANKDIR (...ORIGAMI_bank/gr_mirror)
# � partir du fichier $BANKDIR/gr2species_migale.txt pr�par�
# en vue de la nouvelle entr�e
#
# Date : 10/2009
# AGJ
#
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use SiteConfig;

my $rep ="$SiteConfig::BANKDIR";

open (acces_in, "$rep/gr2species_migale.txt") || die ("pb ouverture fichier gr2species_migale.txt: $!");


my @fields;
my $i=0;
while (my $ligne = <acces_in>) {
    chomp ($ligne);
    @fields = split(/\t/,$ligne);
   print "$fields[1]\n";
    system `cp /db/genome_reviews/current/flat/dat/cellular/$fields[1].dat $rep`;
    $i++;
}
print " NB files copi�s = $i \n";
