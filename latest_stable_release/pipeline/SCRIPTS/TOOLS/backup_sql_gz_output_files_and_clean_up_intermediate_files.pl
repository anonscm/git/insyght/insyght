#!/usr/local/bin/perl
#
#perl TOOLS/backup_sql_gz_output_files_and_clean_up_intermediate_files.pl
#
# Pipeline origami Copyright - INRA - 2012-2025
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#


use strict;
use SiteConfig;
use BlastConfig;


# whole script scoped variable
my $path_log_file = "/home/tlacroix/montages_reseau/migale_work/pipeline_origami_bacteria_2017/insyght/latest_stable_release/pipeline/TMP_DATA/ORIGAMI_log/backup_sql_gz_output_files_and_clean_up_intermediate_files.log";
my $path_step_done_file = "/home/tlacroix/montages_reseau/migale_work/pipeline_origami_bacteria_2017/insyght/latest_stable_release/pipeline/TMP_DATA/ORIGAMI_log/backup_sql_gz_output_files_and_clean_up_intermediate_files.done";
my $path_step_error_file = "/home/tlacroix/montages_reseau/migale_work/pipeline_origami_bacteria_2017/insyght/latest_stable_release/pipeline/TMP_DATA/ORIGAMI_log/backup_sql_gz_output_files_and_clean_up_intermediate_files.error";
my $output_backtick = "";
my @alignment_params_table_sql_gz_file            = ();
my $VERBOSE = "ON";
my $MAX_FILES_TO_BACKUP_IN_THIS_RUN = -1;
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $molecule_type;
my $counter_loop = 0;
my $LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_BASH_SCRIPT_SUBMIT = "$SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/bash_script_submit";
my $LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_COUNT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/count_files";
my $LL_DIR_CORRECT_PRIMARY_KEY_BASH_SCRIPT_SUBMIT = "$SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit";
my $SQL_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sql";
my $TSV_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv";
my $MOUNTING_POINT_BACKUP_MAIAGE = "/home/tlacroix/montages_reseau/backup_maiage/bacteria_2017/ORIGAMI_data/sql_files";
my $FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT = -1;
my $LOG_DIR = "/home/tlacroix/montages_reseau/migale_work/pipeline_origami_bacteria_2017/insyght/latest_stable_release/pipeline/TMP_DATA/ORIGAMI_log/";


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}



foreach my $argnum ( 0 .. $#ARGV ) {

	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -VERBOSE {ON, OFF}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-MAX_FILES_TO_BACKUP_IN_THIS_RUN/ ) {

		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_FILES_TO_BACKUP_IN_THIS_RUN = $ARGV[ $argnum + 1 ];
			if($MAX_FILES_TO_BACKUP_IN_THIS_RUN < 1){
				die_with_error_mssg ("incorrect -MAX_FILES_TO_BACKUP_IN_THIS_RUN argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl  -MAX_FILES_TO_BACKUP_IN_THIS_RUN {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAX_FILES_TO_BACKUP_IN_THIS_RUN argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl  -MAX_FILES_TO_BACKUP_IN_THIS_RUN {int > 0}");
		}
#my $LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_BASH_SCRIPT_SUBMIT = "$SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/bash_script_submit";
	}elsif ( $ARGV[$argnum] =~ m/^-LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_BASH_SCRIPT_SUBMIT$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_BASH_SCRIPT_SUBMIT = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_BASH_SCRIPT_SUBMIT argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_BASH_SCRIPT_SUBMIT {PATH_TO_DIRECTORY}");
		}
#my $LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_COUNT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/count_files";
	}elsif ( $ARGV[$argnum] =~ m/^-LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_COUNT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_COUNT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_COUNT_DIR argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_COUNT_DIR {PATH_TO_DIRECTORY}");
		}
#my $LL_DIR_CORRECT_PRIMARY_KEY_BASH_SCRIPT_SUBMIT = "$SiteConfig::DATADIR/Task_add_alignments/tmp/correct_primary_key_launcher_files/bash_script_submit";
	}elsif ( $ARGV[$argnum] =~ m/^-LL_DIR_CORRECT_PRIMARY_KEY_BASH_SCRIPT_SUBMIT$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$LL_DIR_CORRECT_PRIMARY_KEY_BASH_SCRIPT_SUBMIT = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -LL_DIR_CORRECT_PRIMARY_KEY_BASH_SCRIPT_SUBMIT argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -LL_DIR_CORRECT_PRIMARY_KEY_BASH_SCRIPT_SUBMIT {PATH_TO_DIRECTORY}");
		}
#my $SQL_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sql";
	}elsif ( $ARGV[$argnum] =~ m/^-SQL_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$SQL_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -SQL_INPUT_DIR argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
#my $TSV_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv";
	}elsif ( $ARGV[$argnum] =~ m/^-TSV_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$TSV_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -TSV_INPUT_DIR argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -TSV_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
# MOUNTING_POINT_BACKUP_MAIAGE
	}elsif ( $ARGV[$argnum] =~ m/^-MOUNTING_POINT_BACKUP_MAIAGE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$MOUNTING_POINT_BACKUP_MAIAGE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -MOUNTING_POINT_BACKUP_MAIAGE argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -MOUNTING_POINT_BACKUP_MAIAGE {PATH_TO_DIRECTORY}");
		}
#LOG_DIR
	}elsif ( $ARGV[$argnum] =~ m/^-LOG_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$LOG_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -LOG_DIR argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -MOUNTING_POINT_BACKUP_MAIAGE {PATH_TO_DIRECTORY}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^(\d)$/i )
		{
			$FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT = $1;
			print LOG "\nmode FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT activated : $FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT\n" unless $VERBOSE =~ m/^OFF$/i;
		}
		else {
			die_with_error_mssg ("incorrect -FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT argument ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT {DIGIT}");
		}
	}

}

if ($FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT >= 0){
	if (-e "$LOG_DIR/backup_sql_gz_output_files_and_clean_up_intermediate_files_${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}.log"){
		die_with_error_mssg ("The log file $LOG_DIR/backup_sql_gz_output_files_and_clean_up_intermediate_files_${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}.log already exists and mode FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT is activated ; please make sure no other process are using the same FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT and is running in parrallele, else it will cause duplicated data.");
	} else {
		open( LOG, ">$LOG_DIR/backup_sql_gz_output_files_and_clean_up_intermediate_files_${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}.log");#| perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
		#open( LOG, ">${path_log_file}_${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
	}
} else {
	open( LOG, ">$path_log_file" );
}


if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
	#die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}

print LOG
"\n---------------------------------------------------\n backup_sql_gz_output_files_and_clean_up_intermediate_files.pl started at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/i;

# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}

#/home/tlacroix/montages_reseau/backup_maiage/bacteria_2017/ORIGAMI_data/sql_files/
if ( -e "$MOUNTING_POINT_BACKUP_MAIAGE" ) {
	$output_backtick = `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/alignment_params`;
	$output_backtick .= `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/alignments`;
	$output_backtick .= `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/alignment_pairs`;
	$output_backtick .= `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/homologies`;
	$output_backtick .= `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/tandem_dups`;
	$output_backtick .= `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/isBranchedToAnotherSynteny`;
	$output_backtick .= `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/protFusion`;
	$output_backtick .= `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/closeBestMatchs`;
	if ($output_backtick eq "") {
		#ok
	} else {
		die_with_error_mssg("Error could not complete mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/... :\n$output_backtick \n $!");
	}
} else {
	die_with_error_mssg("Error can not find backup directory $MOUNTING_POINT_BACKUP_MAIAGE\n $!");
}

if (! defined $SQL_INPUT_DIR) {
	die_with_error_mssg("Undefined SQL_INPUT_DIR ; usage : perl backup_sql_gz_output_files_and_clean_up_intermediate_files.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}");
}

my $count_files = 0;
my $count_not_gz_files = 0;
my $count_files_skipped_because_of_FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT = 0;
sub sub_list_alignment_params_gz_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die_with_error_mssg("Unable to open $path: $!");
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_alignment_params_gz_files_recursively ($_);
        } elsif ($_ =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table\.sql\.gz$/ ) {
		my $master_molecule_id_IT = $1;
		$count_files++;

		if ( $MAX_FILES_TO_BACKUP_IN_THIS_RUN > 0 && scalar(@alignment_params_table_sql_gz_file) == $MAX_FILES_TO_BACKUP_IN_THIS_RUN ) {

		} else {
			if ($FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT >= 0) {
				#mode FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT ON
				if ($master_molecule_id_IT  =~ m/^\d*${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}$/){
					push ( @alignment_params_table_sql_gz_file, $_);
				} else {
					$count_files_skipped_because_of_FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT++;
				}
			} else {
           			push ( @alignment_params_table_sql_gz_file, $_);
			}
		}
        } elsif ($_ =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table_TRIMED\.sql\.gz$/ ) {
		#trimed file
        } elsif ($_ =~ m/^.+_alignment_params_table\.sql$/ ) {
		$count_not_gz_files++;
        } else {
            die_with_error_mssg("Error in sub_list_alignment_params_gz_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
#get all *_alignment_params_table.sql file
sub_list_alignment_params_gz_files_recursively("$SQL_INPUT_DIR/alignment_params/");
#@alignment_params_table_sql_gz_file = <$SQL_INPUT_DIR/*_alignment_params_table.sql>;

print LOG "$count_files alignment params table sql.gz file(s) have been found.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "$count_not_gz_files alignment params table sql file(s) have been found and will be skipped.\n";
print LOG "$count_files_skipped_because_of_FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT file(s) skipped because of FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT = ${FILTER_BY_MASTER_MOLECULE_ID_END_WITH_DIGIT}.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "\n".scalar(@alignment_params_table_sql_gz_file)." alignment params table sql gz file(s) found to be treated.\n" unless $VERBOSE =~ m/^OFF$/i;
if ( $MAX_FILES_TO_BACKUP_IN_THIS_RUN > 0 ) {
	print LOG "You chose the option -MAX_FILES_TO_BACKUP_IN_THIS_RUN $MAX_FILES_TO_BACKUP_IN_THIS_RUN\n" unless $VERBOSE =~ m/^OFF$/i;
}

# backup or clean alignment_param and other files
print LOG "\nStarting backup or clean files...\n" unless $VERBOSE =~ m/^OFF$/i;
$counter_loop = 0;
foreach my $alignment_params_table_sql_gz_file_IT (@alignment_params_table_sql_gz_file) {

	my $master_molecule_id;
	my $sub_molecule_id;
	#my $core_file_name;

	if ( $alignment_params_table_sql_gz_file_IT =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table\.sql\.gz$/ ) {
		$master_molecule_id = $1;
		$sub_molecule_id = $2;
	} else {
		die_with_error_mssg("could not parse alignment_params sql gz file $alignment_params_table_sql_gz_file_IT.");
	}

	my $alignment_params_table_TRIMED_gz_input_file_IT = "$SQL_INPUT_DIR/alignment_params/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_params_table_TRIMED.sql.gz";
	my $alignments_gz_input_file_IT = "$SQL_INPUT_DIR/alignments/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_table.sql.gz";
	my $alignment_pairs_gz_input_file_IT = "$SQL_INPUT_DIR/alignment_pairs/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_pairs_table.sql.gz";
	my $homologies_gz_input_file_IT = "$SQL_INPUT_DIR/homologies/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_homologies_table.sql.gz";
#find /postgres/tmp/data_dir/Task_add_alignments/count_primary_key_increment/bash_script_submit -type f -name '*.ll.gz' | wc -l
	my $count_primary_key_increment_bash_script_submit_ll_gz = "$LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_BASH_SCRIPT_SUBMIT/${molecule_type}_${master_molecule_id}/count_primary_key_increment_${master_molecule_id}_and_${sub_molecule_id}.ll.gz";
#find /postgres/tmp/data_dir/Task_add_alignments/count_primary_key_increment/count_files -type f -name '*.out.gz' | wc -l
	my $count_primary_key_increment_count_files_out_gz = "$LL_DIR_COUNT_PRIMARY_KEY_INCREMENT_COUNT_DIR/${molecule_type}_${master_molecule_id}/count_primary_key_increment_${master_molecule_id}_and_${sub_molecule_id}.out.gz";
#find /postgres/tmp/data_dir/Task_add_alignments/correct_primary_key_launcher/bash_script_submit -type f -name '*.ll.gz' | wc -l
	my $correct_primary_key_launcher_bash_script_submit_ll_gz = "$LL_DIR_CORRECT_PRIMARY_KEY_BASH_SCRIPT_SUBMIT/${molecule_type}_${master_molecule_id}/correct_primary_key_${master_molecule_id}_and_${sub_molecule_id}.ll.gz";
#find /postgres/tmp/align_output_dir/alignment_params/ -type f -name '*_alignment_params_table.tsv.gz' | wc -l
	my $alignment_params_table_tsv_gz = "$TSV_INPUT_DIR/alignment_params/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_params_table.tsv.gz";
#find /postgres/tmp/align_output_dir/alignments/ -type f -name '*_alignment_table.tsv.gz' | wc -l
	my $alignment_table_tsv_gz = "$TSV_INPUT_DIR/alignments/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_table.tsv.gz";
#find /postgres/tmp/align_output_dir/alignment_pairs/ -type f -name '*_alignment_pairs_table.tsv.gz' | wc -l
	my $alignment_pairs_table_tsv_gz = "$TSV_INPUT_DIR/alignment_pairs/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_pairs_table.tsv.gz";
#find /postgres/tmp/align_output_dir/homologies/ -type f -name '*_homologies_table.tsv.gz' | wc -l
	my $homologies_table_tsv_gz = "$TSV_INPUT_DIR/homologies/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_homologies_table.tsv.gz";

	if( -e "$alignments_gz_input_file_IT"
	 && -e "$alignment_pairs_gz_input_file_IT"
	 && -e "$homologies_gz_input_file_IT"
	 && -e "$count_primary_key_increment_bash_script_submit_ll_gz"
	 && -e "$count_primary_key_increment_count_files_out_gz"
	 && -e "$correct_primary_key_launcher_bash_script_submit_ll_gz"
	 && -e "$alignment_params_table_tsv_gz"
	 && -e "$alignment_table_tsv_gz"
	 && -e "$alignment_pairs_table_tsv_gz"
	 && -e "$homologies_table_tsv_gz"
	 ) {

		$counter_loop++;
		print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_gz_file)." ) : Starting backup or clean of master_molecule_id = $master_molecule_id and sub_molecule_id = $sub_molecule_id... \n" unless $VERBOSE =~ m/^OFF$/i;


		#alignment_params_table_sql_gz_file_IT
		$output_backtick = `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/alignment_params/${molecule_type}_${master_molecule_id}/`;
		$output_backtick .= `mv $alignment_params_table_sql_gz_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/alignment_params/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_params_table.sql.gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete mv $alignment_params_table_sql_gz_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/alignment_params/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_params_table.sql.gz :\n$output_backtick \n $!");
		}

		if( -e "$alignment_params_table_TRIMED_gz_input_file_IT" ){
			$output_backtick = `mv $alignment_params_table_TRIMED_gz_input_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/alignment_params/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_params_table_TRIMED.sql.gz`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete mv $alignment_params_table_TRIMED_gz_input_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/alignment_params/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_params_table_TRIMED.sql.gz :\n$output_backtick \n $!");
			}
		}

		#alignments_gz_input_file_IT
		$output_backtick = `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/alignments/${molecule_type}_${master_molecule_id}/`;
		$output_backtick .= `mv $alignments_gz_input_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/alignments/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_table.sql.gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete mv $alignments_gz_input_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/alignments/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_table.sql.gz :\n$output_backtick \n $!");
		}

		#alignment_pairs_gz_input_file_IT
		$output_backtick = `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/alignment_pairs/${molecule_type}_${master_molecule_id}/`;
		$output_backtick .= `mv $alignment_pairs_gz_input_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/alignment_pairs/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_pairs_table.sql.gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete mv $alignment_pairs_gz_input_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/alignment_pairs/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_alignment_pairs_table.sql.gz :\n$output_backtick \n $!");
		}

		#homologies_gz_input_file_IT
		$output_backtick = `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/homologies/${molecule_type}_${master_molecule_id}/`;
		$output_backtick .= `mv $homologies_gz_input_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/homologies/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_homologies_table.sql.gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete mv $homologies_gz_input_file_IT $MOUNTING_POINT_BACKUP_MAIAGE/homologies/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_homologies_table.sql.gz :\n$output_backtick \n $!");
		}


		# rm -f
		$output_backtick = `rm -f $count_primary_key_increment_bash_script_submit_ll_gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete rm -f $count_primary_key_increment_bash_script_submit_ll_gz :\n$output_backtick \n $!");
		}
		$output_backtick = `rm -f $count_primary_key_increment_count_files_out_gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete rm -f $count_primary_key_increment_count_files_out_gz :\n$output_backtick \n $!");
		}
		$output_backtick = `rm -f $correct_primary_key_launcher_bash_script_submit_ll_gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete rm -f $correct_primary_key_launcher_bash_script_submit_ll_gz :\n$output_backtick \n $!");
		}
		$output_backtick = `rm -f $alignment_params_table_tsv_gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete rm -f $alignment_params_table_tsv_gz :\n$output_backtick \n $!");
		}
		$output_backtick = `rm -f $alignment_table_tsv_gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete rm -f $alignment_table_tsv_gz :\n$output_backtick \n $!");
		}
		$output_backtick = `rm -f $alignment_pairs_table_tsv_gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete rm -f $alignment_pairs_table_tsv_gz :\n$output_backtick \n $!");
		}
		$output_backtick = `rm -f $homologies_table_tsv_gz`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete rm -f $homologies_table_tsv_gz :\n$output_backtick \n $!");
		}


		my $associated_tandem_dups_gz_sql_file = "$SQL_INPUT_DIR/tandem_dups/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_tandem_dups_table.sql.gz";
		if( -e "$associated_tandem_dups_gz_sql_file" ){
			#find /postgres/tmp/align_output_dir/tandem_dups/ -type f -name '*_tandem_dups_table.tsv.gz' | wc -l
			#associated_tandem_dups_gz_sql_file
			$output_backtick = `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/tandem_dups/${molecule_type}_${master_molecule_id}/`;
			$output_backtick .= `mv $associated_tandem_dups_gz_sql_file $MOUNTING_POINT_BACKUP_MAIAGE/tandem_dups/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_tandem_dups_table.sql.gz`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete mv $associated_tandem_dups_gz_sql_file $MOUNTING_POINT_BACKUP_MAIAGE/tandem_dups/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_tandem_dups_table.sql.gz :\n$output_backtick \n $!");
			}

		}
		my $associated_isBranchedToAnotherSynteny_gz_sql_file = "$SQL_INPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_isBranchedToAnotherSynteny_table.sql.gz";
		if( -e "$associated_isBranchedToAnotherSynteny_gz_sql_file" ){
			#find /postgres/tmp/align_output_dir/isBranchedToAnotherSynteny/ -type f -name '*_isBranchedToAnotherSynteny_table.tsv.gz' | wc -l
			#associated_isBranchedToAnotherSynteny_gz_sql_file
			$output_backtick = `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/isBranchedToAnotherSynteny/${molecule_type}_${master_molecule_id}/`;
			$output_backtick .= `mv $associated_isBranchedToAnotherSynteny_gz_sql_file $MOUNTING_POINT_BACKUP_MAIAGE/isBranchedToAnotherSynteny/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_isBranchedToAnotherSynteny_table.sql.gz`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete mv $associated_isBranchedToAnotherSynteny_gz_sql_file $MOUNTING_POINT_BACKUP_MAIAGE/isBranchedToAnotherSynteny/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_isBranchedToAnotherSynteny_table.sql.gz :\n$output_backtick \n $!");
			}
		}
		my $associated_prot_fusion_gz_sql_file = "$SQL_INPUT_DIR/protFusion/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_protFusion_table.sql.gz";
		if( -e "$associated_prot_fusion_gz_sql_file" ){
			#find /postgres/tmp/align_output_dir/protFusion/ -type f -name '*_protFusion_table.tsv.gz' | wc -l
			#associated_prot_fusion_gz_sql_file
			$output_backtick = `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/protFusion/${molecule_type}_${master_molecule_id}/`;
			$output_backtick .= `mv $associated_prot_fusion_gz_sql_file $MOUNTING_POINT_BACKUP_MAIAGE/protFusion/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_protFusion_table.sql.gz`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete mv $associated_prot_fusion_gz_sql_file $MOUNTING_POINT_BACKUP_MAIAGE/protFusion/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_protFusion_table.sql.gz :\n$output_backtick \n $!");
			}
		}
		my $associated_closeBestMatchs_gz_sql_file = "$SQL_INPUT_DIR/closeBestMatchs/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_closeBestMatchs_table.sql.gz";
		if( -e "$associated_closeBestMatchs_gz_sql_file" ){
			#find /postgres/tmp/align_output_dir/closeBestMatchs/ -type f -name '*_closeBestMatchs_table.tsv.gz' | wc -l
			#associated_closeBestMatchs_gz_sql_file
			$output_backtick = `mkdir -p $MOUNTING_POINT_BACKUP_MAIAGE/closeBestMatchs/${molecule_type}_${master_molecule_id}/`;
			$output_backtick .= `mv $associated_closeBestMatchs_gz_sql_file $MOUNTING_POINT_BACKUP_MAIAGE/closeBestMatchs/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_closeBestMatchs_table.sql.gz`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete mv $associated_closeBestMatchs_gz_sql_file $MOUNTING_POINT_BACKUP_MAIAGE/closeBestMatchs/${molecule_type}_${master_molecule_id}/${master_molecule_id}_and_${sub_molecule_id}_closeBestMatchs_table.sql.gz :\n$output_backtick \n $!");
			}
		}


		print LOG "( $counter_loop / ".scalar(@alignment_params_table_sql_gz_file)." ) : Done backup or clean of master_molecule_id = $master_molecule_id and sub_molecule_id = $sub_molecule_id. \n" unless $VERBOSE =~ m/^OFF$/i;


	} else {
		die_with_error_mssg("could not associate file $alignments_gz_input_file_IT or $alignment_pairs_gz_input_file_IT or $homologies_gz_input_file_IT or $count_primary_key_increment_bash_script_submit_ll_gz or $count_primary_key_increment_count_files_out_gz or $correct_primary_key_launcher_bash_script_submit_ll_gz or $alignment_params_table_tsv_gz or $alignment_table_tsv_gz or $alignment_pairs_table_tsv_gz or $homologies_table_tsv_gz");
	}
}

print LOG "Finished backup or clean files.\n\n" unless $VERBOSE =~ m/^OFF$/i;

#end of script
print LOG
"\n---------------------------------------------------\n\n\n backup_sql_gz_output_files_and_clean_up_intermediate_files.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/i;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);





