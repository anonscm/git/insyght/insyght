#!/usr/bin/perl

# script de nettoyage.
# permet la suppression des alignements
# d'une entr�e de la base et du depot de fichiers
# Date : 07/2009
# Author VL.
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use DBI;
use lib "../";
use SiteConfig;
use ORIGAMI;
use AlignConfig;


die "Usage: $0 accession1 accession2 accession ..."
    unless (scalar(@ARGV) >=1);


my @accessions = @ARGV;

$ORIGAMI::dbh->begin_work();



# on recup�re l'element_id et l'organism_id

foreach my $accession (@accessions){
print STDERR $SiteConfig::DATADIR."/".$accession."/Alignments/".$AlignConfig::params->{-datasubdir}."/\n";



    my $rows = $ORIGAMI::dbh->selectall_arrayref("SELECT element_id, organism_id FROM elements WHERE accession='$accession'");
    my $element_id = $rows->[0]->[0];
    my $organism_id = $rows->[0]->[1];

    print "ACCESSION : $accession Elt : $element_id ORG : $organism_id\n";




    if ($element_id == undef){die "$accession unknown \n";}


## on recup�re les gene_id
my $gene_id = $ORIGAMI::dbh->selectall_arrayref("SELECT gene_id FROM genes WHERE element_id = $element_id");

## nettoyage de la table alignments et alignment_pairs
print "Cleaning alignments and alignment_pairs tables...\n";
my @alignment_id;
foreach my $row (@{$gene_id}) {
#    print STDERR "$row->[0]\n";
	my $alignment_ids = $ORIGAMI::dbh->selectall_arrayref("SELECT alignment_id FROM alignment_pairs WHERE q_gene_id = $row->[0] OR s_gene_id = $row->[0]");
	push(@alignment_id,$alignment_ids);
}

foreach my $row (@alignment_id) {
    if(exists($row->[0]->[0]) && $row>-[0]->[0] ne ""){
	my $req = $ORIGAMI::dbh->prepare("DELETE FROM alignment_pairs WHERE alignment_id = $row->[0]->[0]");
	$req->execute() or die ($ORIGAMI::dbh->errstr,"DELETE FROM alignment_pairs WHERE alignment_id =", $row->[0]->[0]);

	my $req = $ORIGAMI::dbh->prepare("DELETE FROM alignments WHERE alignment_id = $row->[0]->[0]");
	$req->execute()or die ($ORIGAMI::dbh->errstr,"DELETE FROM alignments WHERE alignment_id =", $row->[0]->[0]);
    }
}




# nettoyage de la table alignment_params
print "Cleaning alignment_params table...\n";
my $req = $ORIGAMI::dbh->prepare("DELETE FROM alignment_params WHERE s_element_id = $element_id OR q_element_id =$element_id");
$req->execute();

# nettoyage de la table comp_anal_results_elements
print "Cleaning comp_anal_results_elements table...\n";
my $req = $ORIGAMI::dbh->prepare("DELETE FROM comp_anal_results_elements WHERE element_id = $element_id");
$req->execute();

#Les fichiers
my @files=`find $SiteConfig::DATADIR -name "*\_${element_id}\.*"`;
for(@files){
    print `rm -f $_`;
}

}
$ORIGAMI::dbh->commit();




