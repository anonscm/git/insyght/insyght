#!/usr/local/bin/perl
#
#remove_mirror_rows_synteny.pl
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2017)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use FindBin;
use SiteConfig "$FindBin::Bin/../SiteConfig";
use BlastConfig "$FindBin::Bin/../BlastConfig";
use ORIGAMI "$FindBin::Bin/../ORIGAMI";

# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/Task_add_alignments/remove_mirror_rows_synteny.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/remove_mirror_rows_synteny.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/remove_mirror_rows_synteny.error";
my $VERBOSE = "ON";
my $GUNZIP_INTERMEDIATE_FILES = "ON";
my $SQL_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sql";
my $SQL_NO_MIRROR_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/sql_no_mirror";
my @alignment_params_table_sql_file            = ();
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $molecule_type;
my $output_backtick = "";
my $FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = -1;
my $DO_NOT_RM_OUTPUT_DIR_BEFORE = "OFF";
my $MAX_FILES_TO_CHARGE_IN_THIS_RUN = -1;
my %alignment_param_id_to_delete = ();
my %alignment_id_to_delete = ();
my $ortho_score = -1;
my $homo_score = -1;
my $mismatch_penalty = -1;
my $gap_creation_penalty = -1;
my $gap_extension_penalty = -1;
my $min_align_size = -1;
my $min_score = -1;
my $orthologs_included = "";
my $ortho_min_prot_frac = -1;
my $sth = "";
my $sql_command = "";
my $total_rows_sql_returned = "";
my $do_update_table_params_scores_algo_syntenies = -1;
my $counter_loop = 0;

#
sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


foreach my $argnum ( 0 .. $#ARGV ) {
	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl remove_mirror_rows_synteny.pl -VERBOSE {ON, OFF}");
		}
# GUNZIP_INTERMEDIATE_FILES
	} elsif ( $ARGV[$argnum] =~ m/^-GUNZIP_INTERMEDIATE_FILES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$GUNZIP_INTERMEDIATE_FILES = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -GUNZIP_INTERMEDIATE_FILES argument ; usage : perl remove_mirror_rows_synteny.pl -GUNZIP_INTERMEDIATE_FILES {ON, OFF}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^(\d)$/i )
		{
			$FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = $1;
			print "\nmode FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT activated : $FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT\n" unless $VERBOSE =~ m/^OFF$/i;
		}
		else {
			die_with_error_mssg("incorrect -FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT argument ; usage : perl remove_mirror_rows_synteny.pl -FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT {DIGIT}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-SQL_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$SQL_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -SQL_INPUT_DIR argument ; usage : perl remove_mirror_rows_synteny.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-MAX_FILES_TO_CHARGE_IN_THIS_RUN/ ) {

		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_FILES_TO_CHARGE_IN_THIS_RUN = $ARGV[ $argnum + 1 ];
			if($MAX_FILES_TO_CHARGE_IN_THIS_RUN < 1){
				die_with_error_mssg("incorrect -MAX_FILES_TO_CHARGE_IN_THIS_RUN argument ; usage : perl remove_mirror_rows_synteny.pl  -MAX_FILES_TO_CHARGE_IN_THIS_RUN {int > 0}");
			}
		}
		else {
			die_with_error_mssg("incorrect -MAX_FILES_TO_CHARGE_IN_THIS_RUN argument ; usage : perl remove_mirror_rows_synteny.pl  -MAX_FILES_TO_CHARGE_IN_THIS_RUN {int > 0}");
		}
	}
#DO_NOT_RM_OUTPUT_DIR_BEFORE
	 elsif ( $ARGV[$argnum] =~ m/^-DO_NOT_RM_OUTPUT_DIR_BEFORE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DO_NOT_RM_OUTPUT_DIR_BEFORE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -DO_NOT_RM_OUTPUT_DIR_BEFORE argument ; usage : perl remove_mirror_rows_synteny.pl -DO_NOT_RM_OUTPUT_DIR_BEFORE {ON, OFF}");
		}

	}

}



# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_alignments/`;
# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0){
	if (-e "$SiteConfig::LOGDIR/Task_add_alignments/remove_mirror_rows_synteny_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log"){
		die_with_error_mssg ("The log file $SiteConfig::LOGDIR/Task_add_alignments/remove_mirror_rows_synteny_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log already exists and mode FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT is activated ; please make sure no other process are using the same FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT and is running in parrallele, else it will cause duplicated data.");
	} else {
		open( LOG, "> $SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_integrator_for_IDRIS_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log");# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
	}
} else {
	open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
	#open( LOG,"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_add_alignments/remove_mirror_rows_synteny.log");
}
print LOG
"\n---------------------------------------------------\n remove_mirror_rows_synteny.pl started at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/i;


print LOG "\nYou choose the following options :
\t VERBOSE : $VERBOSE
\t GUNZIP_INTERMEDIATE_FILES : $GUNZIP_INTERMEDIATE_FILES
\t FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT : $FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT
\t SQL_INPUT_DIR : $SQL_INPUT_DIR
" unless $VERBOSE =~ m/^OFF$/;

# mandatory args
if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	#$molecule_type = "orga";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element_id";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}

if (! defined $SQL_INPUT_DIR) {
	die_with_error_mssg("mandatory SQL_INPUT_DIR argument ; usage : perl remove_mirror_rows_synteny.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}");
}

# mkdir and rm files
$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mkdir -p $SQL_NO_MIRROR_OUTPUT_DIR`;
if ($DO_NOT_RM_OUTPUT_DIR_BEFORE =~ m/^ON$/) {
	#do nothing
} else {
	$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/rm -rf $SQL_NO_MIRROR_OUTPUT_DIR/*`;
}
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm files : $output_backtick");
}
# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}


# MODIFY STRUCT DB (suivit_modif_db.txt) TEST get params_scores_algo_syntenies info from db
$sql_command = "SELECT scores_algo_syntenies_id, ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac FROM params_scores_algo_syntenies";
$sth = $ORIGAMI::dbh->prepare($sql_command);
$total_rows_sql_returned = $sth->execute or die_with_error_mssg("Problel executing command :\nSELECT scores_algo_syntenies_id, ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac FROM params_scores_algo_syntenies\n: $!");
if ($total_rows_sql_returned == 0) {
	print LOG "Table params_scores_algo_syntenies is empty, need to update it\n" unless $VERBOSE =~ m/^OFF$/i;
	$do_update_table_params_scores_algo_syntenies = 1;
} elsif ($total_rows_sql_returned == 1) {
	#ok parse
	while ( my $res = $sth->fetchrow_hashref() ) {
		#$scores_algo_syntenies_id = $res->{scores_algo_syntenies_id};
		$ortho_score = $res->{ortho_score};
		$homo_score = $res->{homo_score};
		$mismatch_penalty = $res->{mismatch_penalty};
		$gap_creation_penalty = $res->{gap_creation_penalty};
		$gap_extension_penalty = $res->{gap_extension_penalty};
		$min_align_size = $res->{min_align_size};
		$min_score = $res->{min_score};
		my $orthologs_included_pre = $res->{orthologs_included};
		if ($orthologs_included_pre == 0) {
			$orthologs_included = "F";
		} elsif ($orthologs_included_pre == 1) {
			$orthologs_included = "T";
		} else {
			die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm files : $output_backtick");
		}
		$ortho_min_prot_frac = $res->{ortho_min_prot_frac};
	}
	$do_update_table_params_scores_algo_syntenies = 0;
} else {
	die_with_error_mssg("Error more than 1 row returned for $sql_command");
}



my $count_files = 0;
my $count_gz_files = 0;
my $count_todo_gzip_files = 0;
my $count_files_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = 0;
sub sub_list_alignment_params_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die_with_error_mssg("Unable to open $path: $!");
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_alignment_params_files_recursively ($_);
        } elsif ($_ =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table.sql$/ ) {
		my $master_elet_id_IT = $1;
		$count_files++;

		if ( $MAX_FILES_TO_CHARGE_IN_THIS_RUN > 0 && scalar(@alignment_params_table_sql_file) == $MAX_FILES_TO_CHARGE_IN_THIS_RUN ) {

		} else {
			if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0) {
				#mode FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT ON
				if ($master_elet_id_IT  =~ m/^\d*${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}$/){
					push ( @alignment_params_table_sql_file, $_);
				} else {
					$count_files_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT++;
				}
			} else {
           			push ( @alignment_params_table_sql_file, $_);
			}
		}
        } elsif ($_ =~ m/^.+_alignment_params_table\.sql\.gz$/ ) {
		$count_gz_files++;
        } elsif ($_ =~ m/^.+_alignment_params_table\.sql\.todo_gzip$/ ) {
		$count_todo_gzip_files++;
        } else {
            die_with_error_mssg("Error in sub_list_alignment_params_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
#get all *_alignment_params_table.sql file
sub_list_alignment_params_files_recursively("$SQL_INPUT_DIR/alignment_params/");
print LOG "$count_gz_files alignment params table sql.gz file(s) found (skipped).\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "$count_todo_gzip_files alignment params table sql.todo_gzip file(s) found initialy.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "$count_files_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT alignment params table file(s) skipped because of FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = ${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT} found initialy.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "$count_files alignment params table sql file(s) found in total.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "\n".scalar(@alignment_params_table_sql_file)." alignment params table sql file(s) found to be treated.\n" unless $VERBOSE =~ m/^OFF$/i;



print LOG "\nStarting parsing files to remove dups...\n" unless $VERBOSE =~ m/^OFF$/i;
$counter_loop = 0;
foreach my $alignment_params_table_sql_file_IT (@alignment_params_table_sql_file) {

	my $master_elet_id;
	my $sub_elet_id;
	%alignment_param_id_to_delete = ();
	%alignment_id_to_delete = ();
	$counter_loop++;
	if ( $alignment_params_table_sql_file_IT =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table.sql$/ ) {
		$master_elet_id = $1;
		$sub_elet_id = $2;
		#$core_file_name = "${master_elet_id}_and_${sub_elet_id}";
	} else {
		die_with_error_mssg("could not parse alignment_params sql file $alignment_params_table_sql_file_IT.");
	}


	my $alignments_input_file_IT = "$SQL_INPUT_DIR/alignments/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql";
	my $alignment_pairs_input_file_IT = "$SQL_INPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_pairs_table.sql";
	my $homologies_input_file_IT = "$SQL_INPUT_DIR/homologies/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_homologies_table.sql";

	if( -e "$alignments_input_file_IT" && -e "$alignment_pairs_input_file_IT" && -e "$homologies_input_file_IT" ) {

		print LOG "Starting parsing of master_elet_id = $master_elet_id and sub_elet_id = $sub_elet_id ($counter_loop / ".scalar(@alignment_params_table_sql_file).") \n" unless $VERBOSE =~ m/^OFF$/i;

		#alignment_params_table
		$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mkdir -p $SQL_NO_MIRROR_OUTPUT_DIR/alignment_params/${molecule_type}_${master_elet_id}/`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete mkdir -p $SQL_NO_MIRROR_OUTPUT_DIR/alignment_params/${molecule_type}_${master_elet_id}/ : $output_backtick");
		}
		open( ALIGNMENT_PARAMS_TABLE_OUTPUT, "> $SQL_NO_MIRROR_OUTPUT_DIR/alignment_params/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_params_table.sql") or die_with_error_mssg("Can not open $SQL_NO_MIRROR_OUTPUT_DIR/alignment_params/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_params_table.sql");
		open( ALIGNMENT_PARAMS_TABLE_INPUT, "<$alignment_params_table_sql_file_IT" ) or die_with_error_mssg("Can not open $alignment_params_table_sql_file_IT");
=pod
BEGIN WORK;
COPY alignment_params (alignment_param_id, q_organism_id, q_element_id, s_organism_id, s_element_id, ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac) FROM stdin;
9	20	38	19	37	4	2	-4	-3	-3	1	8	T	0.5
10	19	37	20	38	4	2	-4	-3	-3	1	8	T	0.5
\.
COMMIT WORK;
=cut
		while ( my $line = <ALIGNMENT_PARAMS_TABLE_INPUT> ) {
			chomp($line);
			$line =~ s/^\s+//;
			$line =~ s/\s+$//;
			my @split = split( '\s+', $line );
			if ( $line =~ m/^BEGIN WORK;$/ 
				|| $line =~ m/^\\\.$/ 
				|| $line =~ m/^COMMIT WORK;$/ 
			) {
				print ALIGNMENT_PARAMS_TABLE_OUTPUT "$line\n";
			} elsif ( $line =~ m/^COPY alignment_params \(alignment_param_id, q_organism_id, q_element_id, s_organism_id, s_element_id, ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac\) FROM stdin;$/ ) {
				#do not store info on params_scores_algo_syntenies
				print ALIGNMENT_PARAMS_TABLE_OUTPUT "COPY alignment_params (alignment_param_id, q_organism_id, q_element_id, s_organism_id, s_element_id) FROM stdin;\n";
			} elsif ($line =~ m/^(\d+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t([\-\d]+)\t([\-\d]+)\t([\-\d]+)\t(\d+)\t(\d+)\t([TF])\t([\d\.]+)$/ ) {
				my $alignment_param_id_IT = $1;
				my $q_organism_id_IT = $2;
				my $q_element_id_IT = $3;
				my $s_organism_id_IT = $4;
				my $s_element_id_IT = $5;
				my $ortho_score_IT = $6;
				my $homo_score_IT = $7;
				my $mismatch_penalty_IT = $8;
				my $gap_creation_penalty_IT = $9;
				my $gap_extension_penalty_IT = $10;
				my $min_align_size_IT = $11;
				my $min_score_IT = $12;
				my $orthologs_included_IT = $13;
				my $ortho_min_prot_frac_IT = $14;

				if ($do_update_table_params_scores_algo_syntenies == 0) {
					if ( $ortho_score == $ortho_score_IT && $homo_score == $homo_score_IT && $mismatch_penalty == $mismatch_penalty_IT && $gap_creation_penalty == $gap_creation_penalty_IT && $gap_extension_penalty == $gap_extension_penalty_IT && $min_align_size == $min_align_size_IT && $min_score == $min_score_IT && $orthologs_included == $orthologs_included_IT && $ortho_min_prot_frac == $ortho_min_prot_frac_IT ) {
						#ok same params synteny
					} else {
						die_with_error_mssg("Error params scores parsed in line \n$line\n are not identical to the standard from table params scores algo syntenies: ortho_score = $ortho_score VS $ortho_score_IT, homo_score = $homo_score VS $homo_score_IT, mismatch_penalty = $mismatch_penalty VS $mismatch_penalty_IT, gap_creation_penalty = $gap_creation_penalty VS $gap_creation_penalty_IT, gap_extension_penalty = $gap_extension_penalty VS $gap_extension_penalty_IT, min_align_size = $min_align_size VS $min_align_size_IT, min_score = $min_score VS $min_score_IT, orthologs_included = $orthologs_included VS $orthologs_included_IT, ortho_min_prot_frac = $ortho_min_prot_frac VS $ortho_min_prot_frac_IT");
					}
				} elsif ($do_update_table_params_scores_algo_syntenies == 1) {
					my $sql_command = "INSERT INTO params_scores_algo_syntenies (scores_algo_syntenies_id, ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac) VALUES (1, $ortho_score_IT, $homo_score_IT, $mismatch_penalty_IT, $gap_creation_penalty_IT, $gap_extension_penalty_IT, $min_align_size_IT, $min_score_IT, \'$orthologs_included_IT\', $ortho_min_prot_frac_IT)";
					$ORIGAMI::dbh -> do($sql_command) || die_with_error_mssg("ERROR $sql_command");
					#$ORIGAMI::dbh -> commit();
					#$scores_algo_syntenies_id = 
					$ortho_score = $ortho_score_IT;
					$homo_score = $homo_score_IT;
					$mismatch_penalty = $mismatch_penalty_IT;
					$gap_creation_penalty = $gap_creation_penalty_IT;
					$gap_extension_penalty = $gap_extension_penalty_IT;
					$min_align_size = $min_align_size_IT;
					$min_score = $min_score_IT;
					$orthologs_included = $orthologs_included_IT;
					$ortho_min_prot_frac = $ortho_min_prot_frac_IT;

					$do_update_table_params_scores_algo_syntenies = 0;
				} else {
					die_with_error_mssg("Error \$do_update_table_params_scores_algo_syntenies is nither 0 or 1 : $do_update_table_params_scores_algo_syntenies.");
				}

				if ($q_organism_id_IT <= $s_organism_id_IT) {
					#store this tuple in db without info on params_scores_algo_syntenies
					print ALIGNMENT_PARAMS_TABLE_OUTPUT "$alignment_param_id_IT\t$q_organism_id_IT\t$q_element_id_IT\t$s_organism_id_IT\t$s_element_id_IT\n";
				} else {
					$alignment_param_id_to_delete{$alignment_param_id_IT} = undef;
				}

			} else {
				die_with_error_mssg("could not parse line\n$line\n of sql file $alignment_params_table_sql_file_IT.");
			}
		}
		close (ALIGNMENT_PARAMS_TABLE_INPUT);
		close (ALIGNMENT_PARAMS_TABLE_OUTPUT);


		#alignments
		$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mkdir -p $SQL_NO_MIRROR_OUTPUT_DIR/alignments/${molecule_type}_${master_elet_id}/`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete mkdir -p $SQL_NO_MIRROR_OUTPUT_DIR/alignments/${molecule_type}_${master_elet_id}/ : $output_backtick");
		}
		open( ALIGNMENTS_OUTPUT, "> $SQL_NO_MIRROR_OUTPUT_DIR/alignments/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql") or die_with_error_mssg("Can not open $SQL_NO_MIRROR_OUTPUT_DIR/alignments/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql");
		open( ALIGNMENTS_INPUT, "<$alignments_input_file_IT" ) or die_with_error_mssg("Can not open $alignments_input_file_IT");
=pod
BEGIN WORK;
COPY alignments (alignment_id, alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_size_kb, s_size_kb, q_start, q_stop, s_start, s_stop) FROM stdin;
27	5	94	31	T	26	1	0	4	4	16.278	14.461	124972	141249	203082	217542
28	5	30	11	T	9	0	0	2	2	12.708	11.855	109861	122568	242499	254353
...
661	6	4	1	T	1	0	0	0	0	1.281	1.275	708529	709809	20228	21502
662	6	4	1	T	1	0	0	0	0	1.914	1.923	1095009	1096922	4469	6391
\.
COMMIT WORK;
=cut

		while ( my $line = <ALIGNMENTS_INPUT> ) {
			chomp($line);
			$line =~ s/^\s+//;
			$line =~ s/\s+$//;
			my @split = split( '\s+', $line );
			if (  $line =~ m/^BEGIN WORK;$/ 
				|| $line =~ m/^COPY alignments \(alignment_id, alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_size_kb, s_size_kb, q_start, q_stop, s_start, s_stop\) FROM stdin;$/ 
				|| $line =~ m/^\\\.$/ 
				|| $line =~ m/^COMMIT WORK;$/ 
			) {
				print ALIGNMENTS_OUTPUT "$line\n";
			} elsif ( $line =~ m/^(\d+)\t(\d+)\t\d+\t\d+\t[TF]\t\d+\t\d+\t\d+\t\d+\t\d+\t[\d\.]+\t[\d\.]+\t\d+\t\d+\t\d+\t\d+$/ ) {
				my $alignment_id_IT = $1;
				my $alignment_param_id_IT = $2;
				if ( exists $alignment_param_id_to_delete{$alignment_param_id_IT} ) {
					$alignment_id_to_delete{$alignment_id_IT} = undef;
				} else {
					print ALIGNMENTS_OUTPUT "$line\n";
				}
			} else {
				die_with_error_mssg("could not parse line\n$line\n of sql file $alignments_input_file_IT.");
			}
		}

		close (ALIGNMENTS_INPUT);
		close (ALIGNMENTS_OUTPUT);


		#alignment_pairs
		$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mkdir -p $SQL_NO_MIRROR_OUTPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete mkdir -p $SQL_NO_MIRROR_OUTPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/ : $output_backtick");
		}
		open( ALIGNMENT_PAIRS_OUTPUT, "> $SQL_NO_MIRROR_OUTPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_pairs_table.sql") or die_with_error_mssg("Can not open $SQL_NO_MIRROR_OUTPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_pairs_table.sql");
		open( ALIGNMENT_PAIRS_INPUT, "<$alignment_pairs_input_file_IT" ) or die_with_error_mssg("Can not open $alignment_pairs_input_file_IT");
=pod
BEGIN WORK;
COPY alignment_pairs (alignment_id, q_gene_id, s_gene_id, type) FROM stdin;
4097	51481	24667	1
4097	51482	24666	1
4097	51483	24665	1
...
5447	24330	51382	1
5448	24740	51381	1
\.
COMMIT WORK;
=cut

		while ( my $line = <ALIGNMENT_PAIRS_INPUT> ) {
			chomp($line);
			$line =~ s/^\s+//;
			$line =~ s/\s+$//;
			my @split = split( '\s+', $line );
			if ( $line =~ m/^BEGIN WORK;$/ 
				|| $line =~ m/^COPY alignment_pairs \(alignment_id, q_gene_id, s_gene_id, type\) FROM stdin;$/ 
				|| $line =~ m/^\\\.$/ 
				|| $line =~ m/^COMMIT WORK;$/ 
			) {
				print ALIGNMENT_PAIRS_OUTPUT "$line\n";
			} elsif ( $line =~ m/^(\d+)\t[\d\\nN]+\t[\d\\nN]+\t\d+$/ ) {
				my $alignment_id_IT = $1;
				if ( exists $alignment_id_to_delete{$alignment_id_IT} ) {
					# do not print
				} else {
					print ALIGNMENT_PAIRS_OUTPUT "$line\n";
				}
			} else {
				die_with_error_mssg("could not parse line\n$line\n of sql file $alignment_pairs_input_file_IT");
			}
		}

		close (ALIGNMENT_PAIRS_INPUT);
		close (ALIGNMENT_PAIRS_OUTPUT);


		#homologies
		$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mkdir -p $SQL_NO_MIRROR_OUTPUT_DIR/homologies/${molecule_type}_${master_elet_id}/`;
		if ($output_backtick eq "") {
			#ok
		} else {
			die_with_error_mssg("Error could not complete mkdir -p $SQL_NO_MIRROR_OUTPUT_DIR/homologies/${molecule_type}_${master_elet_id}/ : $output_backtick");
		}
		open( HOMOLOGIES_OUTPUT, "> $SQL_NO_MIRROR_OUTPUT_DIR/homologies/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_homologies_table.sql") or die("Can not open $SQL_NO_MIRROR_OUTPUT_DIR/homologies/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_homologies_table.sql");
		open( HOMOLOGIES_INPUT, "<$homologies_input_file_IT" ) or die_with_error_mssg("Can not open $homologies_input_file_IT");
=pod
BEGIN WORK;
COPY homologies (q_organism_id,q_element_id,q_gene_id,q_length,s_organism_id,s_element_id, s_gene_id, s_length, identity, score, e_value, rank, q_first, q_first_frac, q_last, q_last_frac, q_align_length, q_align_frac, s_first, s_first_frac, s_last, s_last_frac, s_align_length, s_align_frac) FROM stdin;
1	4	2370	145	5	14	11424	153	33.57	85.9	1e-17	1	3	0.0137931	142	0.97931	140	0.965517	9	0.0522876	136	0.888889	128	0.836601
1	4	2371	60	5	14	11423	62	48.39	50.4	4e-07	1	1	0	60	1	60	1	1061	0.983871	61	0.983871
...
5	14	11394	293	1	3	755	280	33.2	117.1	1e-26	1	35	0.116041	286	0.976109	252	0.860068	46	0.160714	276	0.985714	231	0.825
\.
COMMIT WORK;
=cut
		my %homologs_ids2fields = ();
# for each line of INPUT file
		while ( my $line = <HOMOLOGIES_INPUT> ) {
			chomp($line);
			$line =~ s/^\s+//;
			$line =~ s/\s+$//;
			my @split = split( '\s+', $line );
			if ( $line =~ m/^BEGIN WORK;$/ 
				|| $line =~ m/^\\\.$/ 
				|| $line =~ m/^COMMIT WORK;$/ 
			) {
				#do nothing
			} elsif ( $line =~ m/^COPY homologies \(q_organism_id,q_element_id,q_gene_id,q_length,s_organism_id,s_element_id, s_gene_id, s_length, identity, score, e_value, rank, q_first, q_first_frac, q_last, q_last_frac, q_align_length, q_align_frac, s_first, s_first_frac, s_last, s_last_frac, s_align_length, s_align_frac\) FROM stdin;$/ ) {
				#do nothing
			} elsif ($line =~ m/^(\d+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t([\.\d]+)\t([\.\d]+)\t([eE\.\-\d]+)\t(\d+)\t(\d+)\t([eE\.\-\d]+)\t(\d+)\t([eE\.\-\d]+)\t(\d+)\t([eE\.\-\d]+)\t(\d+)\t([eE\.\-\d]+)\t(\d+)\t([eE\.\-\d]+)\t(\d+)\t([eE\.\-\d]+)$/ ) {
				my $q_organism_id_IT = $1;
				my $q_element_id_IT = $2;
				my $q_gene_id_IT = $3;
				my $q_length_IT = $4;
				my $s_organism_id_IT = $5;
				my $s_element_id_IT = $6;
				my $s_gene_id_IT = $7;
				my $s_length_IT = $8;
				my $identity_IT = $9;
				my $score_IT = $10;
				my $e_value_IT = $11;
				my $rank_IT = $12;
				my $q_first_IT = $13;
				my $q_first_frac_IT = $14;
				my $q_last_IT = $15;
				my $q_last_frac_IT = $16;
				my $q_align_length_IT = $17;
				my $q_align_frac_IT = $18;
				my $s_first_IT = $19;
				my $s_first_frac_IT = $20;
				my $s_last_IT = $21;
				my $s_last_frac_IT = $22;
				my $s_align_length_IT = $23;
				my $s_align_frac_IT = $24;

				# store data in %homologs_ids2fields{ ${q_organism_id}_${q_element_id}_${q_gene_id}_${s_organism_id}_${s_element_id}_${s_gene_id} } { **fields** } = values 
				# if q_orga_id >= s_orga_id like other tables above store everything + rank_forward
				# else store rank_reverse
				my $is_rank_forward = undef;
				if ($q_organism_id_IT < $s_organism_id_IT) {
					$is_rank_forward = 1;
				} elsif ($q_organism_id_IT == $s_organism_id_IT && $q_gene_id_IT <= $s_gene_id_IT) {
					$is_rank_forward = 1;
				} else {
					$is_rank_forward = 0;
				}

				if ($is_rank_forward == 1) {
					my $key_hash = "${q_organism_id_IT}_${q_element_id_IT}_${q_gene_id_IT}_${s_organism_id_IT}_${s_element_id_IT}_${s_gene_id_IT}";
					if ( exists $homologs_ids2fields{$key_hash} ) {
						my $ref_tmp_hash = $homologs_ids2fields{$key_hash};
						if ( $$ref_tmp_hash{"rank_forward"} >= 0 ) {
							die_with_error_mssg("error in parsing homologie file $homologies_input_file_IT : key_hash $key_hash already stored with rank_forward >= 0 in \%homologs_ids2fields for line:\n$line");
						} else {
							$$ref_tmp_hash {"rank_forward"} = $rank_IT;
						}
					} else {
						my %tmp_hash = ();
						$tmp_hash {"q_length"} = $q_length_IT;
						$tmp_hash {"s_length"} = $s_length_IT;
						$tmp_hash {"identity"} = $identity_IT;
						$tmp_hash {"score"} = $score_IT;
						$tmp_hash {"e_value"} = $e_value_IT;
						$tmp_hash {"rank_forward"} = $rank_IT;
						$tmp_hash {"rank_reverse"} = -1; # in case reverse blast does not exists in file
						$tmp_hash {"q_first"} = $q_first_IT;
						$tmp_hash {"q_first_frac"} = $q_first_frac_IT;
						$tmp_hash {"q_last"} = $q_last_IT;
						$tmp_hash {"q_last_frac"} = $q_last_frac_IT;
						$tmp_hash {"q_align_length"} = $q_align_length_IT;
						$tmp_hash {"q_align_frac"} = $q_align_frac_IT;
						$tmp_hash {"s_first"} = $s_first_IT;
						$tmp_hash {"s_first_frac"} = $s_first_frac_IT;
						$tmp_hash {"s_last"} = $s_last_IT;
						$tmp_hash {"s_last_frac"} = $s_last_frac_IT;
						$tmp_hash {"s_align_length"} = $s_align_length_IT;
						$tmp_hash {"s_align_frac"} = $s_align_frac_IT;
						$homologs_ids2fields{$key_hash} = \%tmp_hash;
					}

				} elsif ($is_rank_forward == 0) {
					my $key_hash = "${s_organism_id_IT}_${s_element_id_IT}_${s_gene_id_IT}_${q_organism_id_IT}_${q_element_id_IT}_${q_gene_id_IT}";
					if ( exists $homologs_ids2fields{$key_hash} ) {
						my $ref_tmp_hash = $homologs_ids2fields{$key_hash};
						if ($$ref_tmp_hash{"rank_reverse"} >= 0 ) {
							die_with_error_mssg("error in parsing homologie file $homologies_input_file_IT : key_hash $key_hash already stored with rank_reverse >= 0 in \%homologs_ids2fields for line:\n$line");
						} else {
							$$ref_tmp_hash {"rank_reverse"} = $rank_IT;
						}
					} else {
						my %tmp_hash = ();
						$tmp_hash {"q_length"} = $s_length_IT;
						$tmp_hash {"s_length"} = $q_length_IT;
						$tmp_hash {"identity"} = $identity_IT;
						$tmp_hash {"score"} = $score_IT;
						$tmp_hash {"e_value"} = $e_value_IT;
						$tmp_hash {"rank_forward"} = -1; # in case reverse blast does not exists in file
						$tmp_hash {"rank_reverse"} = $rank_IT;
						$tmp_hash {"q_first"} = $s_first_IT;
						$tmp_hash {"q_first_frac"} = $s_first_frac_IT;
						$tmp_hash {"q_last"} = $s_last_IT;
						$tmp_hash {"q_last_frac"} = $s_last_frac_IT;
						$tmp_hash {"q_align_length"} = $s_align_length_IT;
						$tmp_hash {"q_align_frac"} = $s_align_frac_IT;
						$tmp_hash {"s_first"} = $q_first_IT;
						$tmp_hash {"s_first_frac"} = $q_first_frac_IT;
						$tmp_hash {"s_last"} = $q_last_IT;
						$tmp_hash {"s_last_frac"} = $q_last_frac_IT;
						$tmp_hash {"s_align_length"} = $q_align_length_IT;
						$tmp_hash {"s_align_frac"} = $q_align_frac_IT;
						$homologs_ids2fields{$key_hash} = \%tmp_hash;
					}
				}


			} else {
				die_with_error_mssg("could not parse line\n$line\n of sql file $homologies_input_file_IT");
			}
		}

# print header in sql OUTPUT file non redundant
		print HOMOLOGIES_OUTPUT "BEGIN WORK;\n";
		print HOMOLOGIES_OUTPUT "COPY homologies (q_organism_id,q_element_id,q_gene_id,q_length,s_organism_id,s_element_id, s_gene_id, s_length, identity, score, e_value, rank_forward, rank_reverse, q_first, q_first_frac, q_last, q_last_frac, q_align_length, q_align_frac, s_first, s_first_frac, s_last, s_last_frac, s_align_length, s_align_frac) FROM stdin;\n";
# for each entries in %homologs_ids2fields, print a line to insert in sql OUTPUT file non redundant
		foreach my $key_hash_IT (keys %homologs_ids2fields) {
			# parse key
			my @split = split( '_', $key_hash_IT );
			my $q_organism_id_IT = $split[0];
			my $q_element_id_IT = $split[1];
			my $q_gene_id_IT = $split[2];
			my $s_organism_id_IT = $split[3];
			my $s_element_id_IT = $split[4];
			my $s_gene_id_IT = $split[5];
			# get other values
			my $ref_tmp_hash = $homologs_ids2fields {$key_hash_IT};
			my $q_length_IT = $$ref_tmp_hash {"q_length"};
			my $s_length_IT = $$ref_tmp_hash {"s_length"};
			my $identity_IT = $$ref_tmp_hash {"identity"};
			my $score_IT = $$ref_tmp_hash {"score"};
			my $e_value_IT = $$ref_tmp_hash {"e_value"};
			my $rank_forward_IT = $$ref_tmp_hash {"rank_forward"};
			my $rank_reverse_IT = $$ref_tmp_hash {"rank_reverse"};
			my $q_first_IT = $$ref_tmp_hash {"q_first"};
			my $q_first_frac_IT = $$ref_tmp_hash {"q_first_frac"};
			my $q_last_IT = $$ref_tmp_hash {"q_last"};
			my $q_last_frac_IT = $$ref_tmp_hash {"q_last_frac"};
			my $q_align_length_IT = $$ref_tmp_hash {"q_align_length"};
			my $q_align_frac_IT = $$ref_tmp_hash {"q_align_frac"};
			my $s_first_IT = $$ref_tmp_hash {"s_first"};
			my $s_first_frac_IT = $$ref_tmp_hash {"s_first_frac"};
			my $s_last_IT = $$ref_tmp_hash {"s_last"};
			my $s_last_frac_IT = $$ref_tmp_hash {"s_last_frac"};
			my $s_align_length_IT = $$ref_tmp_hash {"s_align_length"};
			my $s_align_frac_IT = $$ref_tmp_hash {"s_align_frac"};
			print HOMOLOGIES_OUTPUT "$q_organism_id_IT\t$q_element_id_IT\t$q_gene_id_IT\t$q_length_IT\t$s_organism_id_IT\t$s_element_id_IT\t$s_gene_id_IT\t$s_length_IT\t$identity_IT\t$score_IT\t$e_value_IT\t$rank_forward_IT\t$rank_reverse_IT\t$q_first_IT\t$q_first_frac_IT\t$q_last_IT\t$q_last_frac_IT\t$q_align_length_IT\t$q_align_frac_IT\t$s_first_IT\t$s_first_frac_IT\t$s_last_IT\t$s_last_frac_IT\t$s_align_length_IT\t$s_align_frac_IT\n";
		}
# print footer in sql OUTPUT file non redundant
		print HOMOLOGIES_OUTPUT "\\.\n";
		print HOMOLOGIES_OUTPUT "COMMIT WORK;\n";

		close (HOMOLOGIES_INPUT);
		close (HOMOLOGIES_OUTPUT);


		#option to gunzip or just mark file to do gzip
		if ( $GUNZIP_INTERMEDIATE_FILES  =~ m/^ON$/i ) {
			$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/gzip $alignment_params_table_sql_file_IT`;
			$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/gzip $alignments_input_file_IT`;
			$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/gzip $alignment_pairs_input_file_IT`;
			$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/gzip $homologies_input_file_IT`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/gzip  files : $output_backtick");
			}
		} else {
			$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mv $alignment_params_table_sql_file_IT $alignment_params_table_sql_file_IT\.todo_gzip`;
			$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mv $alignments_input_file_IT $alignments_input_file_IT\.todo_gzip`;
			$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mv $alignment_pairs_input_file_IT $alignment_pairs_input_file_IT\.todo_gzip`;
			$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mv $homologies_input_file_IT $homologies_input_file_IT\.todo_gzip`;
			if ($output_backtick eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/mv files : $output_backtick");
			}
		}
		print LOG "Done parsing of master_elet_id = $master_elet_id and sub_elet_id = $sub_elet_id... \n" unless $VERBOSE =~ m/^OFF$/i;

	}else{
		die_with_error_mssg("could not associate file $alignments_input_file_IT or $alignment_pairs_input_file_IT or $homologies_input_file_IT.");
	}

}
print LOG "Finished parsing files to remove dups.\n" unless $VERBOSE =~ m/^OFF$/i;


print LOG "\n---------------------------------------------------\n remove_mirror_rows_synteny.pl successfully completed at :", scalar(localtime), "\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/i;


# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);



