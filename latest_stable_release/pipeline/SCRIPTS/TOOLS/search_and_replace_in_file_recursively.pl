#!/usr/local/bin/perl
#
# search_and_replace_in_file_recursively.pl
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2017)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#


use strict;
use FindBin;
use SiteConfig "$FindBin::Bin/../SiteConfig";
use BlastConfig "$FindBin::Bin/../BlastConfig";
use ORIGAMI "$FindBin::Bin/../ORIGAMI";
use Data::Dumper;

# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/TOOLS/search_and_replace_in_file_recursively.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/search_and_replace_in_file_recursively.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/search_and_replace_in_file_recursively.error";
my $VERBOSE = "ON";
my $GUNZIP_INPUT_FILES = "OFF";
my $INPUT_DIR = undef;
my $RESTRICT_TO_FILE_EXTENSION = "";
my $OUTPUT_DIR = undef;
my %STRING_TO_REPLACE2REPLACEMENT_STRING = ();
my $output_backtick = "";
my @files_to_modify = ();


#
sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


foreach my $argnum ( 0 .. $#ARGV ) {

	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl search_and_replace_in_file_recursively.pl -VERBOSE {ON, OFF}");
		}
	}
	if  ( $ARGV[$argnum] =~ m/^-GUNZIP_INPUT_FILES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$GUNZIP_INPUT_FILES = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -GUNZIP_INPUT_FILES argument ; usage : perl search_and_replace_in_file_recursively.pl -GUNZIP_INPUT_FILES {ON, OFF}");
		}
	}
	if  ( $ARGV[$argnum] =~ m/^-INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -INPUT_DIR argument ; usage : perl search_and_replace_in_file_recursively.pl -INPUT_DIR {**PATH_TO_DIR**}");
		}
	}
	if  ( $ARGV[$argnum] =~ m/^-RESTRICT_TO_FILE_EXTENSION$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$RESTRICT_TO_FILE_EXTENSION = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -RESTRICT_TO_FILE_EXTENSION argument ; usage : perl search_and_replace_in_file_recursively.pl -RESTRICT_TO_FILE_EXTENSION {**EXTENSION**}");
		}
	}
	if  ( $ARGV[$argnum] =~ m/^-OUTPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -OUTPUT_DIR argument ; usage : perl search_and_replace_in_file_recursively.pl -OUTPUT_DIR {**PATH_TO_DIR**}");
		}
	}
	if  ( $ARGV[$argnum] =~ m/^-REPLACE_STRING$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			my $STRING_TO_REPLACE = $ARGV[ $argnum + 1 ];
			if (exists $STRING_TO_REPLACE2REPLACEMENT_STRING{$STRING_TO_REPLACE}) {
				die_with_error_mssg("multiple -REPLACE_STRING argument can not duplicate of the same term to replace ; usage : perl search_and_replace_in_file_recursively.pl -REPLACE_STRING {**STRING_REGEX**} BY {**STRING_REGEX**}");
			}
			if ( $ARGV[ $argnum + 2 ] =~ m/^BY$/i ) {
				if (   $ARGV[ $argnum + 3 ] =~ m/^.+$/i ) {
					my $REPLACEMENT_STRING = $ARGV[ $argnum + 3 ];
					$STRING_TO_REPLACE2REPLACEMENT_STRING{$STRING_TO_REPLACE} = $REPLACEMENT_STRING;
				}
			} else {
				die_with_error_mssg("incorrect -REPLACE_STRING argument ; usage : perl search_and_replace_in_file_recursively.pl -REPLACE_STRING {**STRING_REGEX**} BY {**STRING_REGEX**}");
			}
		}
		else {
			die_with_error_mssg("incorrect -REPLACE_STRING argument ; usage : perl search_and_replace_in_file_recursively.pl -REPLACE_STRING {**STRING_REGEX**} BY {**STRING_REGEX**}");
		}
	}

}


# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/TOOLS/`;
# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 


print LOG
"\n---------------------------------------------------\n search_and_replace_in_file_recursively.pl started at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/i;



print LOG "\nYou choose the following options :
\t VERBOSE : $VERBOSE
\t GUNZIP_INPUT_FILES : $GUNZIP_INPUT_FILES
\t INPUT_DIR : $INPUT_DIR
\t RESTRICT_TO_FILE_EXTENSION : $RESTRICT_TO_FILE_EXTENSION
\t OUTPUT_DIR : $OUTPUT_DIR
\t REPLACE_STRING : "+print Dumper(\%STRING_TO_REPLACE2REPLACEMENT_STRING)+"
" unless $VERBOSE =~ m/^OFF$/;

# mandatory args
if (! defined $INPUT_DIR) {
	die_with_error_mssg("mandatory -INPUT_DIR argument ; usage : perl search_and_replace_in_file_recursively.pl -INPUT_DIR {**PATH_TO_DIR**}");
}
if (! defined $OUTPUT_DIR) {
	die_with_error_mssg("mandatory -OUTPUT_DIR argument ; usage : perl search_and_replace_in_file_recursively.pl -OUTPUT_DIR {**PATH_TO_DIR**}");
}
if (scalar(keys %STRING_TO_REPLACE2REPLACEMENT_STRING) == 0) {
	die_with_error_mssg("mandatory -REPLACE_STRING argument ; usage : perl search_and_replace_in_file_recursively.pl -REPLACE_STRING {**STRING_REGEX**} BY {**STRING_REGEX**}");
}

# check that no %STRING_TO_REPLACE2REPLACEMENT_STRING replacement are interacting and potential conflicts
foreach my $keys_string_to_replace_IT (keys %STRING_TO_REPLACE2REPLACEMENT_STRING) {
	my $value_replacement_string_IT = $STRING_TO_REPLACE2REPLACEMENT_STRING{$keys_string_to_replace_IT};
	foreach my $keys_string_to_replace_BIS (keys %STRING_TO_REPLACE2REPLACEMENT_STRING) {
		my $value_replacement_string_BIS = $STRING_TO_REPLACE2REPLACEMENT_STRING{$keys_string_to_replace_BIS};
		if ($keys_string_to_replace_IT eq $keys_string_to_replace_BIS) {
			next;
		}
		if ($keys_string_to_replace_IT =~ m/^.+$keys_string_to_replace_BIS.+$/
			|| $keys_string_to_replace_BIS =~ m/^.+$keys_string_to_replace_IT.+$/
			|| $keys_string_to_replace_IT =~ m/^.+$value_replacement_string_IT.+$/
			|| $value_replacement_string_IT =~ m/^.+$keys_string_to_replace_IT.+$/
			|| $keys_string_to_replace_IT =~ m/^.+$value_replacement_string_BIS.+$/
			|| $value_replacement_string_BIS =~ m/^.+$keys_string_to_replace_IT.+$/
			|| $value_replacement_string_IT =~ m/^.+$keys_string_to_replace_BIS.+$/
			|| $keys_string_to_replace_BIS =~ m/^.+$value_replacement_string_IT.+$/
			|| $value_replacement_string_IT =~ m/^.+$value_replacement_string_BIS.+$/
			|| $value_replacement_string_BIS =~ m/^.+$value_replacement_string_IT.+$/
			|| $keys_string_to_replace_BIS =~ m/^.+$value_replacement_string_BIS.+$/
			|| $value_replacement_string_BIS =~ m/^.+$keys_string_to_replace_BIS.+$/
		) {
			die_with_error_mssg("Error iteration order is not constent so there is a potential conflict when replacing\n$keys_string_to_replace_IT\nwith\n$value_replacement_string_IT\nand\n$keys_string_to_replace_BIS\nwith\n$value_replacement_string_BIS\n");
		}
	}
}

# mkdir and rm files
$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/mkdir -p $OUTPUT_DIR`;
$output_backtick = $output_backtick.`$SiteConfig::CMDDIR/rm -rf $OUTPUT_DIR/*`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm files OUTPUT_DIR = $OUTPUT_DIR : $output_backtick");
}
# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}



my $count_files_skipped = 0;
sub sub_list_input_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die_with_error_mssg("Unable to open $path: $!");
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_input_files_recursively ($_);
        } elsif ($_ =~ m/^.*$RESTRICT_TO_FILE_EXTENSION$/ ) {
		push ( @files_to_modify, $_);
        } else {
		$count_files_skipped++;
        }
    }
}
#get all *_alignment_params_table.sql file
sub_list_input_files_recursively("$INPUT_DIR");
print LOG scalar(@files_to_modify)." file(s) have been added to be treated.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "$count_files_skipped file(s) have been skipped.\n" unless $VERBOSE =~ m/^OFF$/i;



#TODO NOT FINISHED, BUT NEEDED ??


