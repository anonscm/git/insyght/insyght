#!/usr/local/bin/perl

use strict;

my $pattern_accession;
my $accession = 0;


$pattern_accession = '^ID\s+(\w+);*';

my $files_SVA = shift;
die "Usage : $0 file_name\n"
	unless(defined $files_SVA);	

open(TEXTLIST,$files_SVA) || die "impossible ouvrir fichier $files_SVA \n";

	while (my $ligne = <TEXTLIST>){
if($ligne=~ /$pattern_accession/){

  if ($accession ne 0) {close (textout);}
      $accession = $1;
      open (textout,">$accession.dat") || die "impossible ouvrir fichier $accession.dat \n";}
  
print textout "$ligne";
}
