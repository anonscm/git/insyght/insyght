#!/usr/local/bin/perl
#
# integrate_changes_element_types_and_reorganize_elements_into_organisms.pl
#
# Script de construction et mise à jour de ORIGAMI
# Permet d'intégrer les fichiers .sql généré par check_for_element_types_and_reorganize_elements_into_organisms.pl
# Date : 07/2015
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) :  Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-...), Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr),
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 
use strict;
use DBI;
use SiteConfig;
use ORIGAMI;


# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::LOGDIR/Task_blast_all/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/check_for_element_types_and_reorganize_elements_into_organisms/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/check_for_element_types_and_reorganize_elements_into_organisms/integrate_changes_element_types_and_reorganize_elements_into_organisms.log"
);
print LOG
"\n\n---------------------------------------------------\n integrate_changes_element_types_and_reorganize_elements_into_organisms.pl started at :",
  scalar(localtime), "\n\n\n";


my $psql_out = `psql -f $SiteConfig::DATADIR/check_reorganize_elements/check_reorganize_elements.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname`;
if ($psql_out eq '' || $psql_out  =~ m/^BEGIN\s*[UPDATEINSERTDELETE\s\d\n]*COMMIT\s*$/
) {
	#ok went well
} else {
	print LOG "ERROR command\npsql -f $SiteConfig::DATADIR/check_reorganize_elements/check_reorganize_elements.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname\nreturned an error :\n$psql_out\n";
	die("ERROR command\npsql -f $SiteConfig::DATADIR/check_reorganize_elements/check_reorganize_elements.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname\nreturned an error :\n$psql_out\n");
}


# end of script
print LOG
"\n---------------------------------------------------\n\n\nintegrate_changes_element_types_and_reorganize_elements_into_organisms.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);


