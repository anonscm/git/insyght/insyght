#!/usr/local/bin/perl
#
# Task_blast_all_integrator_IDRIS_results.pl : Script principal permettant de parser et d'intégrer dans la base de donnée tous les résultats de blast venant l'IDRIS concernant le blast de Origami
#
# Date : 01/2014
# LT
#
#LOG FILE : $SiteConfig::LOGDIR/Blast/Task_blast_all_integrator_IDRIS_results.log
#LAUNCHED AS : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER : [optional]
#ARGUMENTS :
# -MAX_JOBS = {INTEGER}
# -RECOVER_FAILURE = {ON, OFF}
# -VERBOSE : if ON print ant details at each step, if OFF only print critical information
# -DELETE_TEMP_FILES = {ON, OFF}
# -CMD_CLUSTER : [optional], if this argument is present or 0 then the command are executed in the bash locally. Example of command for cluster: -CMD_CLUSTER "qsub -m ea -q short.q"
#
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use Time::HiRes qw(usleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;

#script level variables
my $MAX_JOBS;
my $DELETE_TEMP_FILES;
my $VERBOSE;
my @childs_pid                          = ();
my $RECOVER_FROM_FAILURE                = "OFF";
my $CMD_SUBMIT_CLUSTER                  = 0;
my @blast_output_from_IDRIS_unsorted    = ();
my @blast_output_from_IDRIS             = ();
my @launch_blast_batch_files_unsorted   = ();
my @launch_blast_batch_files            = ();
my @parse_blast_tabular_output_unsorted = ();
my @parse_blast_tabular_output          = ();
my @sql_import_done_unsorted            = ();
my @sql_import_done                     = ();

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::LOGDIR/Task_blast_all/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_integrator_IDRIS_results.log"
);
print LOG
"\n---------------------------------------------------\n Task_blast_all_integrator_IDRIS_results.pl started at :",
  scalar(localtime), "\n";

#set the variables according to the argument or exit with usage if the arguments are incorrect

foreach my $argnum ( 0 .. $#ARGV ) {

	if ( $ARGV[$argnum] =~ m/^-MAX_JOBS$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_JOBS = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -MAX_JOBS argument ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}\n";
			die
"incorrect -MAX_JOBS argument ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}\n";
		}

	}
	if ( $ARGV[$argnum] =~ m/^-DELETE_TEMP_FILES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DELETE_TEMP_FILES = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -DELETE_TEMP_FILES argument ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0\"/qsub -m ea -q short.q\"}\n";
			die
"incorrect -DELETE_TEMP_FILES argument ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}\n";
		}
	}
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -VERBOSE argument ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}";
			die
"incorrect -VERBOSE argument ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}";
		}

	}
	if ( $ARGV[$argnum] =~ m/^-RECOVER_FAILURE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$RECOVER_FROM_FAILURE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG
"incorrect -RECOVER_FAILURE argument ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}\n";
			die
"incorrect -RECOVER_FAILURE argument ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}\n";
		}

	}
	if ( $ARGV[$argnum] =~ m/^-CMD_CLUSTER/ ) {
		if ( $ARGV[$argnum] =~ m/^0$/i ) {

			#do nothing
		}
		else {
			if ( $ARGV[$argnum] =~ m/^.+$/i ) {
				$CMD_SUBMIT_CLUSTER = $ARGV[ $argnum + 1 ];
			}
		}

	}
}
if ( defined $MAX_JOBS && defined $DELETE_TEMP_FILES && defined $VERBOSE ) {
	print LOG
"\nYou choose the following options :\n\tMAX_JOBS : $MAX_JOBS\n\tDELETE_TEMP_FILES : $DELETE_TEMP_FILES\n\tVERBOSE : $VERBOSE\t\nRECOVER_FAILURE : $RECOVER_FROM_FAILURE\n"
	  unless $VERBOSE =~ m/^OFF$/;
	if ( $CMD_SUBMIT_CLUSTER =~ m/^0$/i ) {

		#do nothing
	}
	else {
		print LOG "CMD_CLUSTER : $CMD_SUBMIT_CLUSTER\n"
		  unless $VERBOSE =~ m/^OFF$/;
	}
}
else {
	print LOG
"incorrect arguments ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}";
	die
"incorrect arguments ; usage : perl Task_blast_all_integrator_IDRIS_results.pl -MAX_JOBS = {INTEGER} -DELETE_TEMP_FILES = {ON, OFF} -VERBOSE = {ON, OFF} -RECOVER_FAILURE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}";
}

#checking for directory DATADIR/Task_blast_all/tmp/blast_output/ (sent by IDRIS)
#create the following empty directories if they do not exists : DATADIR/Task_blast_all/tmp/, DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/, ....
@blast_output_from_IDRIS_unsorted =
  <$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/*.blast>;
@blast_output_from_IDRIS = sort @blast_output_from_IDRIS_unsorted;
print LOG scalar(@blast_output_from_IDRIS)
  . " .blast files found under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/\n";
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/`;

if ( $RECOVER_FROM_FAILURE =~ m/^ON$/i ) {
	print
"You have entered the mode RECOVER_FROM_FAILURE. Files under $SiteConfig::DATADIR/Task_blast_all/tmp/ will not be deleted. A check for the presence of output files will be performed at each steps. If the file exists the step will be skiped.\n";
	print LOG "mode RECOVER_FROM_FAILURE ON\n";
	if ( -e "$SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED" ) {
`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED`;
	}
}
else {
	print LOG
"Deleting files under $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/ \n";
`$SiteConfig::CMDDIR/rm $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/**`;
	print LOG
"Deleting files under $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/ \n";
`$SiteConfig::CMDDIR/rm $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/**`;
	print LOG
"Deleting files under $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/ \n";
`$SiteConfig::CMDDIR/rm $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/**`;
}

# list files under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/ as @launch_blast_batch_files
@launch_blast_batch_files_unsorted =
  <$SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/*.blast.ll>;
@launch_blast_batch_files = sort @launch_blast_batch_files_unsorted;
print LOG scalar(@launch_blast_batch_files)
  . " .blast.ll files found under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/\n";

# Are all the blast results here ? compare list @blast_output_from_IDRIS with @launch_blast_batch_files
if ( scalar(@blast_output_from_IDRIS) != scalar(@launch_blast_batch_files) ) {
	print LOG
"The number of files found under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/ ("
	  . scalar(@blast_output_from_IDRIS)
	  . ") is different from the number of files under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/ ("
	  . scalar(@launch_blast_batch_files) . ")\n";
	die
"The number of files found under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/ ("
	  . scalar(@blast_output_from_IDRIS)
	  . ") is different from the number of files under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/ ("
	  . scalar(@launch_blast_batch_files) . ")\n";
}

LOOP_blast_output_from_IDRIS:
foreach my $blast_output_from_IDRIS_IT (@blast_output_from_IDRIS) {
	$blast_output_from_IDRIS_IT =~ s/^.+\/(.+?)\..*$/$1/;
	my $index = 0;
	foreach my $launch_blast_batch_files_IT (@launch_blast_batch_files) {
		$launch_blast_batch_files_IT =~ s/^.+\/(.+?)\..*$/$1/;

#print "launch : $launch_blast_batch_files_IT blast : $blast_output_from_IDRIS_IT\n"; #AGJ
		if ( $launch_blast_batch_files_IT =~ m/$blast_output_from_IDRIS_IT/i ) {
			splice( @launch_blast_batch_files, $index, 1 );
			next LOOP_blast_output_from_IDRIS;
		}
		$index++;
	}
	print LOG
"The file ${blast_output_from_IDRIS_IT}.blast under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/ has no equivalent under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/\n";
	die
"The file ${blast_output_from_IDRIS_IT}.blast under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/ has no equivalent under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/\n";
}

if ( scalar(@launch_blast_batch_files) != 0 ) {
	print LOG
"There is a discrepancy between files under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/ and $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/. Here are the files under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/ that have no match : @launch_blast_batch_files\n";
	die
"There is a discrepancy between files under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/ and $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/. Here are the files under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/ that have no match : @launch_blast_batch_files\n";
}

# parse all blast output
foreach
  my $blast_output_from_IDRIS_IT_unsorted (@blast_output_from_IDRIS_unsorted)
{

#file name of blast output is
#$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/${element_id_accepted_as_new}_${accnum_correspondance_curr_element_id_accepted_as_new}_VS_${current_element_id}_${accnum_correspondance_current_element_id}.blast
#split by _VS_
	my $elementIdNew_accnumNew;
	my $elementIdCurr_accnumCurr;
	if ( $blast_output_from_IDRIS_IT_unsorted =~ m/^.+\/(.+)_VS_(.+)\.blast$/ )
	{
		$elementIdNew_accnumNew   = $1;
		$elementIdCurr_accnumCurr = $2;
	}
	else {
		print LOG
"ERROR Task_blast_all_integrator_IDRIS_results.pl : output blast file $blast_output_from_IDRIS_IT_unsorted do not match the regex ^(.+)_VS_(.+).blast\n";
		die
"ERROR Task_blast_all_integrator_IDRIS_results.pl : output blast file $blast_output_from_IDRIS_IT_unsorted do not match the regex ^(.+)_VS_(.+).blast\n";
	}

	my $elementIdNew;
	my $accnumNew;
	if ( $elementIdNew_accnumNew =~ m/^(.+?)_(.+)$/ ) {
		$elementIdNew = $1;
		$accnumNew    = $2;
	}
	else {
		print LOG
"ERROR Task_blast_all_integrator_IDRIS_results.pl : output blast file $blast_output_from_IDRIS_IT_unsorted do not match the regex for elementIdNew_accnumNew ^(.+?)_(.+)\n";
		die
"ERROR Task_blast_all_integrator_IDRIS_results.pl : output blast file $blast_output_from_IDRIS_IT_unsorted do not match the regex for elementIdNew_accnumNew ^(.+?)_(.+)\n";
	}

	my $elementIdCurr;
	my $accnumCurr;
	if ( $elementIdCurr_accnumCurr =~ m/^(.+?)_(.+)$/ ) {
		$elementIdCurr = $1;
		$accnumCurr    = $2;
	}
	else {
		print LOG
"ERROR Task_blast_all_integrator_IDRIS_results.pl : output blast file $blast_output_from_IDRIS_IT_unsorted do not match the regex for elementIdCurr_accnumCurr ^(.+?)_(.+)\n";
		die
"ERROR Task_blast_all_integrator_IDRIS_results.pl : output blast file $blast_output_from_IDRIS_IT_unsorted do not match the regex for elementIdCurr_accnumCurr ^(.+?)_(.+)\n";
	}

	my @args_for_parse_blast_tabular = (
		"$SiteConfig::SCRIPTSDIR/parse_blast_tabular.pl",
		"$blast_output_from_IDRIS_IT_unsorted",
		"$elementIdNew",
		"$accnumNew",
		"$elementIdCurr",
		"$accnumCurr",
		"$VERBOSE",
		"ON",
		$RECOVER_FROM_FAILURE
	);
	print LOG
	  "launching parsing for file $blast_output_from_IDRIS_IT_unsorted\n"
	  unless $VERBOSE =~ m/^OFF$/i;
	if ( system(@args_for_parse_blast_tabular) != 0 ) {
		print LOG "system @args_for_parse_blast_tabular failed: $?\n";
		exit(1);
	}
	else {
		print LOG
		  " parsing for file $blast_output_from_IDRIS_IT_unsorted is finished\n"
		  unless $VERBOSE =~ m/^OFF$/i;
	}
}

# list files under $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/
@parse_blast_tabular_output_unsorted =
  <$SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/*.sql>;
@parse_blast_tabular_output = sort @parse_blast_tabular_output_unsorted;
print LOG scalar(@parse_blast_tabular_output)
  . " .sql files found under $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/\n";

# Are all the blast results here ? compare list @blast_output_from_IDRIS with @launch_blast_batch_files
if ( scalar(@blast_output_from_IDRIS) != scalar(@parse_blast_tabular_output) ) {
	print LOG
"The number of files found under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/ ("
	  . scalar(@blast_output_from_IDRIS)
	  . ") is different from the number of files under $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/ ("
	  . scalar(@parse_blast_tabular_output) . ")\n";
	die
"The number of files found under $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/ ("
	  . scalar(@blast_output_from_IDRIS)
	  . ") is different from the number of files under $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/ ("
	  . scalar(@parse_blast_tabular_output) . ")\n";
}

#list files already imported in db and if recover failure, say which are skipped
@sql_import_done_unsorted =
  <$SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/*.sql_import_done>;
@sql_import_done = sort @sql_import_done_unsorted;
if ( $RECOVER_FROM_FAILURE =~ m/^ON$/i ) {
	print LOG
"WARNING : Some data have already been imported in the database during the previous run. Import of those files will be skipped:\n";
  LOOP_sql_import_done: foreach my $sql_import_done_IT (@sql_import_done) {
		$sql_import_done_IT =~ s/^(.+?)\..*$/$1/;
		my $index = 0;
		foreach my $parse_blast_tabular_output_IT (@parse_blast_tabular_output)
		{

			#$parse_blast_tabular_output_IT =~ s/^(.+?)\..*$/$1/;
			if ( $parse_blast_tabular_output_IT =~
				m/^$sql_import_done_IT\.sql$/i )
			{
				print LOG "$sql_import_done_IT\n";
				splice( @parse_blast_tabular_output, $index, 1 );
				next LOOP_sql_import_done;
			}
			$index++;
		}
		print LOG
"The file $sql_import_done_IT.sql_import_done under $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/ has no match for its .sql file/\n";
		die
"The file $sql_import_done_IT.sql_import_done under $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/ has no match for its .sql file/\n";
	}
}
else {
	if ( scalar(@sql_import_done) != 0 ) {
		print LOG
"The number of files .sql_import_done found under $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/ ("
		  . scalar(@sql_import_done)
		  . ") is different from 0 while RECOVER_FROM_FAILURE is not ON.\n";
		die
"The number of files .sql_import_done found under $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/ ("
		  . scalar(@sql_import_done)
		  . ") is different from 0 while RECOVER_FROM_FAILURE is not ON.\n";
	}
}

##check consistyancy between sql to import and data in the db
#foreach my $parse_blast_tabular_output_IT (@parse_blast_tabular_output) {
#
##file name of sql output is
##$SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/${element_id_querry}_${accnum_correspondance_elt_id_querry}_VS_${element_id_subject}_${accnum_correspondance_elt_id_subject}.sql
##split by _VS_
#	my $elementIdNew_accnumNew;
#	my $elementIdCurr_accnumCurr;
#	if ( $parse_blast_tabular_output_IT =~ m/^.+\/(.+)_VS_(.+)\.sql$/ ) {
#		$elementIdNew_accnumNew   = $1;
#		$elementIdCurr_accnumCurr = $2;
#	}
#	else {
#		print LOG
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : output sql file $parse_blast_tabular_output_IT do not match the regex ^(.+)_VS_(.+).sql\n";
#		die
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : output sql file $parse_blast_tabular_output_IT do not match the regex ^(.+)_VS_(.+).sql\n";
#	}
#
#	my $elementIdNew;
#	my $accnumNew;
#	if ( $elementIdNew_accnumNew =~ m/^(.+?)_(.+)$/ ) {
#		$elementIdNew = $1;
#		$accnumNew    = $2;
#	}
#	else {
#		print LOG
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : output sql file $parse_blast_tabular_output_IT do not match the regex for elementIdNew_accnumNew ^(.+?)_(.+)\n";
#		die
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : output sql file $parse_blast_tabular_output_IT do not match the regex for elementIdNew_accnumNew ^(.+?)_(.+)\n";
#	}
#
#	my $elementIdCurr;
#	my $accnumCurr;
#	if ( $elementIdCurr_accnumCurr =~ m/^(.+?)_(.+)$/ ) {
#		$elementIdCurr = $1;
#		$accnumCurr    = $2;
#	}
#	else {
#		print LOG
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : output sql file $parse_blast_tabular_output_IT do not match the regex for elementIdCurr_accnumCurr ^(.+?)_(.+)\n";
#		die
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : output sql file $parse_blast_tabular_output_IT do not match the regex for elementIdCurr_accnumCurr ^(.+?)_(.+)\n";
#	}
#
##check Origami database query : rows of table homologies where q_element_id =  elementIdNew and s_element_id = elementIdCurr
#	my $res =
#	  $ORIGAMI::dbh->selectall_arrayref(
#"SELECT q_element_id, s_element_id FROM homologies where q_element_id = $elementIdNew AND s_element_id = $elementIdCurr"
#	  );
#
#	#if there isn't any row returned by the previous querry = good; else error
#	if ( scalar(@$res) > 0 ) {
#		print LOG
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : The table homologies already contain data for q_element_id = $elementIdNew AND s_element_id = $elementIdCurr.\n";
#		die
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : The table homologies already contain data for q_element_id = $elementIdNew AND s_element_id = $elementIdCurr.\n";
#	}
#
##check Origami database query : rows of table homologies where q_element_id =  elementIdNew and s_element_id = elementIdCurr
#	my $res1 =
#	  $ORIGAMI::dbh->selectall_arrayref(
#		"SELECT element_id FROM elements where element_id = $elementIdNew" );
#
#	#if there isn't any row returned by the previous querry, error
#	if ( !scalar(@$res1) ) {
#		print LOG
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : The table elements do not contain data for element_id = $elementIdNew.\n";
#		die
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : The table elements do not contain data for element_id = $elementIdNew.\n";
#	}
#
##check Origami database query : rows of table homologies where q_element_id =  elementIdNew and s_element_id = elementIdCurr
#	my $res2 =
#	  $ORIGAMI::dbh->selectall_arrayref(
#		"SELECT element_id FROM elements where element_id = $elementIdCurr" );
#
#	#if there isn't any row returned by the previous querry, error
#	if ( !scalar(@$res2) ) {
#		print LOG
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : The table elements do not contain data for element_id = $elementIdCurr.\n";
#		die
#"ERROR Task_blast_all_integrator_IDRIS_results.pl : The table elements do not contain data for element_id = $elementIdCurr.\n";
#	}
#
#}

# make multiple child process to do the psl command for each file to import
LOOP: foreach my $parse_blast_tabular_output_IT (@parse_blast_tabular_output) {

	#exit if failure
	if ( -e "$SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED" ) {
		die(
"Exiting script as a failure occured.\nSome changes may have been committed to the database.\nPlease manually check files under$SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/. You can use the option to recover from failure mode available in this script.\n"
		);
	}
# wait a bit if there more than $MAX_JOBS child process that have been launched simultaneously
	my $do_sleep    = 0;
	my $count_sleep = 0;
	while ( scalar(@childs_pid) > $MAX_JOBS ) {
		$do_sleep = 1;
		$count_sleep++;

		#print LOG "count_sleep : $count_sleep\n";
		foreach my $index ( 0 .. $#childs_pid ) {

			if ( waitpid( $childs_pid[$index], WNOHANG ) == -1 ) {
				delete $childs_pid[$index];
				$do_sleep = 0;
			}

		}
		if ( $do_sleep == 0 ) {

			#				print LOG "waited for ", $count_sleep * 5,
			#" seconds, but now one process is over so continuing blast...\n"
			#				  unless $VERBOSE =~ m/^OFF$/i;

		}
		elsif ( $do_sleep == 1 && $count_sleep == 1 ) {
			print
"waiting a bit as there is more than $MAX_JOBS processes that have been launched simultenously\n"
			  unless $VERBOSE =~ m/^OFF$/i;

			print LOG
"waiting a bit as there is more than $MAX_JOBS processes that have been launched simultenously...\n"
			  unless $VERBOSE =~ m/^OFF$/i;
			usleep(50000);
		}
		else {
			usleep(50000);
		}
	}

	#wait a bit so that there is a shifting in time for the different forks
	usleep(30000);

	#fork the script
	my $pid = fork();
	if ($pid) {

		# if parent process, register the child process in array childs_pid
		push( @childs_pid, $pid );
		next LOOP;

	}
	elsif ( $pid == 0 ) {

		# if child process, then pilot the launching of the scripts
		$CMD_SUBMIT_CLUSTER = "\'" . $CMD_SUBMIT_CLUSTER . "\'";
		my @args_for_import_individual_blast_result = (
			"$SiteConfig::SCRIPTSDIR/import_individual_blast_result.pl",
			"$parse_blast_tabular_output_IT",
			"$VERBOSE", "$CMD_SUBMIT_CLUSTER"
		);
		if ( system(@args_for_import_individual_blast_result) != 0 ) {
`$SiteConfig::CMDDIR/touch $SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED`;
			print LOG
			  "system @args_for_import_individual_blast_result failed: $?\n";
			exit(1);
		}

		#exit child process
		exit(0);

	}
	else {
		die("couldn't fork : $!\n");
	}

}    #foreach my $parse_blast_tabular_output_IT (@parse_blast_tabular_output) {

#wait for all child processes that are still running to complete
foreach (@childs_pid) {
	waitpid( $_, 0 );
}

#exit if failure
if ( -e "$SiteConfig::LOGDIR/Task_blast_all/ERROR_OCCURED" ) {
	die(
"Exiting script as a failure occured.\nSome changes may have been committed to the database.\nPlease manually check files under$SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/. You can use the option to recover from failure mode available in this script.\n"
	);
}

#delete files under DATADIR/Task_blast_all/tmp/XXX as needed unless stated otherwise as -DELETE_TEMP_FILES

if ( $DELETE_TEMP_FILES =~ m/^ON$/i ) {
	print LOG "Deleting temporary files under 
	$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb 
	and $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done 
	and $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files 
	and $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output 
	and $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output 
\n"
	  unless $VERBOSE =~ m/^OFF$/i;
	`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/`;

#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done`;
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files`;
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output`;
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output`;

#RQ : mkdir
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_import_db_batch_files/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/blast_output/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/blast_done/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/parse_blast_tabular_output/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/parsing_done/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/sql_import/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/`;

}

#Check if redundancy for a given q_gene_id and s_gene_id in the table homologies (more than one entry for one couple q_gene_id - s_gene_id)
#MANUAL CHECK TO DO : select q_gene_id, s_gene_id, count(*) from homologies group by q_gene_id, s_gene_id having count(*) > 1

#Check redundancy for a given rank - q_gene_id - s_organism_id in the table homologies (there is more than one entry for a similar rank - q_gene_id - s_organism_id)
#MANUAL CHECK TO DO : select rank, q_gene_id, s_organism_id, count(*) from homologies group by rank, q_gene_id, s_organism_id having count(*) > 2

print LOG
"\n---------------------------------------------------\n\n\nTask_blast_all_integrator_IDRIS_results.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);

