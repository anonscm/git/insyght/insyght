#!/usr/local/bin/perl

# script de mise a jour de la base
# permet la suppression d'une entr�e de la base
#
# Date : 09/2008
# (reprise du script delete_entry.pl de Seqdb)
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2050)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use DBI;
use SiteConfig;
use ORIGAMI;

my $accession = shift;
my $req = undef;

eval {

	$ORIGAMI::dbh->begin_work;

	# on recup�re l'element_id et l'organism_id
	my $rows = $ORIGAMI::dbh->selectall_arrayref("SELECT element_id, organism_id FROM elements WHERE accession='$accession'");
	my $element_id = $rows->[0]->[0];
	my $organism_id = $rows->[0]->[1];
	# Ajout (AGJ)
	#print "ACC : $accession Elt : $element_id ORG : $organism_id\n";
	## on recup�re les gene_id
	my $gene_id = $ORIGAMI::dbh->selectall_arrayref("SELECT gene_id FROM genes WHERE element_id = $element_id");

	if ($element_id == undef){die "$accession already removed\n";}



	# nettoyage de la table elements
	#print "Cleaning elements table...\n";
	#my $ele = $ORIGAMI::dbh->selectall_arrayref("SELECT element_id FROM elements WHERE accession = '$accession'");
	$req = $ORIGAMI::dbh->prepare("DELETE FROM elements WHERE element_id = $element_id");
	$req->execute();
	# nettoyage des tables organims 
	my $nborg = $ORIGAMI::dbh->selectall_arrayref("SELECT COUNT(organism_id) FROM elements WHERE organism_id = $organism_id");

	#print "Cleaning organism table...\n";
	if ($nborg->[0]->[0] == 0){
		$req = $ORIGAMI::dbh->prepare("DELETE FROM organisms WHERE organism_id = $organism_id");
		$req->execute();
	}

	# nettoyage de la table alignment_params
	#print "Cleaning alignment_params table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM alignment_params WHERE s_element_id = $element_id OR q_element_id = $element_id");
	$req->execute();

	## nettoyage de la table alignments et alignment_pairs
	#print "Cleaning alignments and alignment_pairs and isbranchedtoanothersynteny tables...\n";
	my @alignment_id;
	foreach my $row (@{$gene_id}) {
		my $alignment_ids = $ORIGAMI::dbh->selectall_arrayref("SELECT alignment_id FROM alignment_pairs WHERE q_gene_id = $row->[0] OR s_gene_id = $row->[0]");
		push(@alignment_id,$alignment_ids);
	}
	foreach my $row (@{$gene_id}) {
		$req = $ORIGAMI::dbh->prepare("DELETE FROM alignment_pairs WHERE q_gene_id = $row->[0] OR s_gene_id = $row->[0]");
		$req->execute();
	}
	foreach my $col (@alignment_id) {
		if($col->[0]->[0] != undef){
			$req = $ORIGAMI::dbh->prepare("DELETE FROM isbranchedtoanothersynteny WHERE alignment_id_branched = $col->[0]->[0]");
			$req->execute();
			$req = $ORIGAMI::dbh->prepare("DELETE FROM alignments WHERE alignment_id = $col->[0]->[0]");
			$req->execute();
		}
	}	


	# nettoyage de la table genes
	#print "Cleaning genes table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM genes WHERE element_id = $element_id");
	$req->execute();

	# nettoyage de la table comp_anal_results_elements
	#print "Cleaning comp_anal_results_elements table...\n";
	#$req = $ORIGAMI::dbh->prepare("DELETE FROM comp_anal_results_elements WHERE element_id = $element_id");
	#$req->execute();

	## nettoyage de la table neighbors
	#print "Cleaning neighbors table...\n";
	#$req = $ORIGAMI::dbh->prepare("DELETE FROM neighbors WHERE element_id = $element_id");
	#$req->execute();

	## nettoyage de la table homologies
	#print "Cleaning homologies table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM homologies WHERE s_element_id = $element_id OR q_element_id = $element_id");
	$req->execute();


	## nettoyage de la table prot_fusion
	#print "Cleaning prot_fusion table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM prot_fusion WHERE s_element_id = $element_id OR q_element_id = $element_id");
	$req->execute();

	## nettoyage de la table tandem_dups
	#print "Cleaning tandem_dups table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM tandem_dups WHERE single_element_id = $element_id OR tandem_element_id = $element_id");
	$req->execute();


	# nettoyage de la table q_element_id_2_sorted_list_comp_orga_whole
	#print "Cleaning q_element_id_2_sorted_list_comp_orga_whole table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM q_element_id_2_sorted_list_comp_orga_whole WHERE element_id = $element_id");
	$req->execute();

	# nettoyage de la table close_best_match
	#print "Cleaning close_best_match table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM close_best_match WHERE q_element_id = $element_id");
	$req->execute();

	# nettoyage tables du mod�le MICADO

	# nettoyage de la table qualifiers 
	#print "Cleaning qualifiers table...\n";

	$req = $ORIGAMI::dbh->prepare("DELETE FROM qualifiers WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table prot_feat
	#print "Cleaning  prot_feat table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM prot_feat WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table  dna_loc
	#print "Cleaning  dna_loc table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM dna_loc WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table  locations
	#print "Cleaning locations table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM locations WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table  dna_seq
	#print "Cleaning dna_seq table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM dna_seq WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table  keywords
	#print "Cleaning keywords table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM keywords WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table  comments
	#print "Cleaning comments table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM comments WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table  accession
	#print "Cleaning accession table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM accessions WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table  articles
	#print "Cleaning articles table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM articles WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table features ; RQ must be cleaned before table sequences
	#print "Cleaning features table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM features WHERE accession = '$accession'");
	$req->execute();

	# nettoyage de la table sequences
	#print "Cleaning sequence table...\n";
	$req = $ORIGAMI::dbh->prepare("DELETE FROM sequences WHERE accession = '$accession'");
	$req->execute();

	$ORIGAMI::dbh->commit;
};

if ($@) {
	$ORIGAMI::dbh->rollback;
	die ("Error delete_entry.pl : $@");
}


