##########################################################
# Module qui permet de se connecter et de se déconnecter #
# de la base de données                                  #
##########################################################

package ORIGAMI_ncbi_taxonomy;

#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use DBI;
use CGI::Carp qw(fatalsToBrowser);
my $dbh;
my ( $dbname, $port, $host, $user, $passwd );

BEGIN {
	my $DBNAME = "ncbi_taxonomy"; 
	my $USER   = "origami_admin";
	my $HOST   = "bddor.jouy.inra.fr";
	my $PASSWD = "XXX";
	my $PORT   = "5432";


	### Connexion
	$ORIGAMI_ncbi_taxonomy::dbh = DBI->connect( "dbi:Pg:host=$HOST;dbname=$DBNAME;port=$PORT",
		$USER, $PASSWD )

	  or die
	  "ORIGAMI_ncbi_taxonomy.pm : Impossible de se connecter à la base de données : DBI::errstr\n";
	$ORIGAMI_ncbi_taxonomy::dbh->do('set search_path to public, micado');
	$ORIGAMI_ncbi_taxonomy::user   = $USER;
	$ORIGAMI_ncbi_taxonomy::passwd = $PASSWD;
	$ORIGAMI_ncbi_taxonomy::host   = $HOST;
	$ORIGAMI_ncbi_taxonomy::port   = $PORT;
	$ORIGAMI_ncbi_taxonomy::dbname = $DBNAME;

}

END {
	### déconnexion
	$ORIGAMI_ncbi_taxonomy::dbh->disconnect()
	  or die "ORIGAMI_ncbi_taxonomy.pm : Echec de la deconnection : DBI::errstr\n";
}

1
