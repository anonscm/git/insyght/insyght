#!/usr/local/bin/perl
#
# Task_generate_fasta.pl Permet de générer les fichier fasta de tous les éléments ou organismes de la base de données
#
# Pipeline origami v1.0 Copyright - INRA - 2017-2025
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2017)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use SiteConfig;
use lib '$SiteConfig::SCRIPTSDIR';
use BlastConfig;
use ORIGAMI;
use Find_new_old_molecules;
use POSIX;

#script level variable
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $BlastConfig_number_clusters_organism_best_adjust = $BlastConfig::number_clusters_organism_best_adjust; 
my $BlastConfig_avg_number_proteines_per_cluster_best_adjust = $BlastConfig::avg_number_proteines_per_cluster_best_adjust;
my $VERBOSE = "ON";
my $st;
my $GENERATE_FOR_IDRIS = "OFF";
my @list_ordered_new_molecule_id;
my @list_ordered_old_molecule_id;
#my %element_id_2_accnum;
my %molecule_id_2_size;
my %element_id2orga_id;
my $molecule_type;
my $path_log_file = "$SiteConfig::LOGDIR/Task_generate_fasta/Task_generate_fasta.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/generate_fasta_proteome.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/generate_fasta_proteome.error";
my $output_backtick = "";

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log 
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_generate_fasta/`;

#
sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}




#set the variables according to the argument or exit with usage if the arguments are incorrect
foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		} else {
			#print LOG "incorrect -VERBOSE argument ; usage : perl Task_generate_fasta.pl  -VERBOSE = {ON, OFF}";
			die_with_error_mssg ("incorrect -VERBOSE argument ; usage : perl Task_generate_fasta.pl  -VERBOSE = {ON, OFF}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-GENERATE_FOR_IDRIS$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$GENERATE_FOR_IDRIS = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -GENERATE_FOR_IDRIS argument ; usage : Task_generate_fasta.pl -GENERATE_FOR_IDRIS {ON, OFF}";
			die_with_error_mssg ("incorrect -GENERATE_FOR_IDRIS argument ; usage : Task_generate_fasta.pl -GENERATE_FOR_IDRIS {ON, OFF}");
		}

	}
}

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
# ouvre fichier de log, no tee.pl
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG
"\n---------------------------------------------------\n Task_generate_fasta.pl started at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;


if (defined $VERBOSE ) {
	print LOG
"\nYou choose the following options :\n\tVERBOSE : $VERBOSE\n"
	  unless $VERBOSE =~ m/^OFF$/;
}
else {
	#print LOG "incorrect arguments ; usage : Task_generate_fasta.pl -VERBOSE = {ON, OFF}";
	die_with_error_mssg ("incorrect arguments ; usage : Task_generate_fasta.pl -VERBOSE = {ON, OFF}");
}



print LOG "creating output directory and cleaning up\n" unless $VERBOSE =~ m/^OFF$/;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/`;
if($GENERATE_FOR_IDRIS =~ m/^ON$/i){
	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/cmd_file_idris/`;
}else{
	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/`;
}
`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/*`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/new_molecules/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/old_molecules/`;
if($GENERATE_FOR_IDRIS =~ m/^ON$/i){
	`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/cmd_file_idris/*`;
}else{
	`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/*`;
}
# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}



if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/ ) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
	`mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/list_cluster/`;
	open( LIST_CLUSTER, ">$SiteConfig::DATADIR/Task_blast_all/tmp/list_cluster/list_cluster.txt") or die_with_error_mssg("Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/list_cluster/list_cluster.txt");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	$molecule_type = "element_id";
} else {
	#print LOG "BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately\n";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately\n");
}


if($GENERATE_FOR_IDRIS =~ m/^ON$/i){
	open( MAKEBLAST_BATCHFILE_FOR_IDRIS, "> $SiteConfig::DATADIR/Task_blast_all/tmp/cmd_file_idris/makeblast_cmd_file_for_IDRIS.ll") or die_with_error_mssg("Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/cmd_file_idris/makeblast_cmd_file_for_IDRIS.ll");
}
if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	open( FASTA_ALL, ">$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${molecule_type}_all.faa") or die_with_error_mssg("Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${molecule_type}_all.faa");
}


my ($list_ordered_new_molecule_id_ref, $list_ordered_old_molecule_id_ref, $molecule_id_2_size_ref, $element_id2orga_id_ref) = Find_new_old_molecules::find_new_old_molecules_from_db_data($BLAST_TAXO_GRANULARITY);
#odering is by desc size of proteome
@list_ordered_new_molecule_id = @$list_ordered_new_molecule_id_ref;
@list_ordered_old_molecule_id = @$list_ordered_old_molecule_id_ref;
#%element_id_2_accnum = %$element_id_2_accnum_ref;
%molecule_id_2_size = %$molecule_id_2_size_ref;
%element_id2orga_id = %$element_id2orga_id_ref;

print LOG @list_ordered_new_molecule_id." new $molecule_type found.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG @list_ordered_old_molecule_id." old $molecule_type found.\n" unless $VERBOSE =~ m/^OFF$/;


sub create_makedb_file_list_molecules {

	my ( $ref_list_molecule_id_IT, $is_new_molecule ) = @_;
	my $molecule_ids_IT_as_string = "";
	if ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
		$molecule_ids_IT_as_string = $ref_list_molecule_id_IT->[0];
	} else {
		$molecule_ids_IT_as_string = join("_", @$ref_list_molecule_id_IT);
	}

	print LOG "Generating makedb file command for ${molecule_type} = $molecule_ids_IT_as_string\n" unless $VERBOSE =~ m/^OFF$/;

	my $prefix_new_old = "";
	if ($is_new_molecule == 1) {
		$prefix_new_old = "new_molecules";
	} elsif ($is_new_molecule == 0) {
		$prefix_new_old = "old_molecules";
	} else {
		#print LOG "error in create_makedb_file_list_molecules : argument \$is_new_molecule is not recognized: $is_new_molecule\n";
		die_with_error_mssg("error in create_makedb_file_list_molecules : argument \$is_new_molecule is not recognized: $is_new_molecule");
	}
	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/$prefix_new_old/`;


	if ($GENERATE_FOR_IDRIS =~ m/^ON$/) {
		print MAKEBLAST_BATCHFILE_FOR_IDRIS "$SiteConfig::FORMATDB -in \$fasta_formatdb_dir/$prefix_new_old/${molecule_type}_${molecule_ids_IT_as_string}.faa -input_type fasta -dbtype prot -out \$fasta_formatdb_dir/$prefix_new_old/${molecule_type}_${molecule_ids_IT_as_string}.faa\n";
	} else {

		open( MAKEBLASTDB_BATCHFILE_NEW, "> $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/$prefix_new_old/batch_${molecule_type}_${molecule_ids_IT_as_string}.makeblastdb.ll")
			or die_with_error_mssg("Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/$prefix_new_old/batch_${molecule_type}_${molecule_ids_IT_as_string}.makeblastdb.ll");

		print MAKEBLASTDB_BATCHFILE_NEW "#!/bin/bash
#\$ -S /bin/bash
# (n) No mail is sent ; Avertir au début (b)egin, à la fin (e)nd, à l'éliminaton (a)bort et
# à la suspension (s)uspend d'un job
#\$ -m n
# Adresse mail à laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#\$ -o $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${molecule_type}_${molecule_ids_IT_as_string}.out
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -e $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${molecule_type}_${molecule_ids_IT_as_string}.err
# Lance la commande depuis le répertoire où est lancé le script
#\$ -cwd\n";
		print MAKEBLASTDB_BATCHFILE_NEW "$SiteConfig::FORMATDB -in $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/$prefix_new_old/${molecule_type}_${molecule_ids_IT_as_string}.faa -input_type fasta -dbtype prot -out $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/$prefix_new_old/${molecule_type}_${molecule_ids_IT_as_string} && touch $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/${molecule_type}_${molecule_ids_IT_as_string}.makeblastdb_done\n";
		close (MAKEBLASTDB_BATCHFILE_NEW);
	}

	print LOG "Done generating makedb file command for ${molecule_type} = $molecule_ids_IT_as_string\n\n" unless $VERBOSE =~ m/^OFF$/;

}


sub print_fasta_list_molecules {

	my ( $ref_list_molecule_id_IT, $is_new_molecule ) = @_;
	my $molecule_ids_IT_as_string = "";
	if ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
		$molecule_ids_IT_as_string = $ref_list_molecule_id_IT->[0];
	} else {
		$molecule_ids_IT_as_string = join("_", @$ref_list_molecule_id_IT);
	}


	print LOG "Generating fasta for ${molecule_type} = $molecule_ids_IT_as_string\n" unless $VERBOSE =~ m/^OFF$/;

	my $column_to_request_in_table_db = "";
	if($molecule_type eq "orga" || $molecule_type eq "orgaCluster") {
		$column_to_request_in_table_db = "organism_id";
	} elsif ($molecule_type eq "element") {
		$column_to_request_in_table_db = "element_id";
	} else {
		#print LOG "error in print_fasta_list_molecules : \$molecule_type is not recognized: $molecule_type\n";
		die_with_error_mssg("error in print_fasta_list_molecules : \$molecule_type is not recognized: $molecule_type");
	}


	my $prefix_new_old = "";
	if ($is_new_molecule == 1) {
		$prefix_new_old = "new_molecules";
	} elsif ($is_new_molecule == 0) {
		$prefix_new_old = "old_molecules";
	} else {
		#print LOG "error in print_fasta_list_molecules : argument \$is_new_molecule is not recognized: $is_new_molecule\n";
		die_with_error_mssg("error in print_fasta_list_molecules : argument \$is_new_molecule is not recognized: $is_new_molecule");
	}
	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/$prefix_new_old/`;

	open( FASTA, ">$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/$prefix_new_old/${molecule_type}_${molecule_ids_IT_as_string}.faa") or die_with_error_mssg("Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/$prefix_new_old/${molecule_type}_${molecule_ids_IT_as_string}.faa");

	#Origami database query : all rows of table genes that have element_id == $elt_id, showing column gene_id, name, residues
	my $genes = $ORIGAMI::dbh->selectall_arrayref(
		"SELECT genes.gene_id, genes.name, micado.prot_feat.proteine, genes.organism_id, genes.element_id FROM genes, micado.prot_feat where $column_to_request_in_table_db IN (".join(",", @$ref_list_molecule_id_IT).") AND genes.accession = micado.prot_feat.accession AND genes.feature_id = micado.prot_feat.code_feat ORDER BY $column_to_request_in_table_db"
	);
	# INNER JOIN AND genes.accession = micado.prot_feat.accession AND genes.feature_id = micado.prot_feat.code_feat because we want to exclude pseudogene with no aa sequences

	# pour chaques genes
	foreach my $gene ( @{$genes} ) {
		my ( $gene_id_QUERY, $name_QUERY, $residues_QUERY, $organism_id_QUERY, $element_id_QUERY ) = @{$gene};
		$name_QUERY =~ s/\s/=/g;
		$name_QUERY =~ tr/()/==/;

		print FASTA ">${name_QUERY}_${organism_id_QUERY}_${element_id_QUERY}_${gene_id_QUERY}\n${residues_QUERY}\n";
	}

	close(FASTA);

	print LOG "Done generating fasta for ${molecule_type} = $molecule_ids_IT_as_string\n\n" unless $VERBOSE =~ m/^OFF$/;

}



sub calculate_cumulated_proteome_size {
	my ( $ref_al_molecule_ids ) = @_;
	my $cumulated_proteome_size = 0;
	foreach my $molecule_id_IT ( @$ref_al_molecule_ids ) {
		if (exists $molecule_id_2_size{$molecule_id_IT}) {
			$cumulated_proteome_size += $molecule_id_2_size{$molecule_id_IT};
		} else {
			#print LOG "error in calculate_cumulated_proteome_size : \$molecule_id_IT $molecule_id_IT doesn't exists in \%molecule_id_2_size\n";
			die_with_error_mssg("error in calculate_cumulated_proteome_size : \$molecule_id_IT $molecule_id_IT doesn't exists in \%molecule_id_2_size");
		}
	}
	return $cumulated_proteome_size;
}


sub find_index_with_lowsest_cumulated_proteome_size {
	my ( $ref_cumulated_proteome_size_per_index ) = @_;
	my $index_with_lowsest_cumulated_proteome_size = 0;

	for (my $index_IT=1; $index_IT < scalar(@$ref_cumulated_proteome_size_per_index); $index_IT++) {
		my $cumulated_proteome_size_IT = $$ref_cumulated_proteome_size_per_index[$index_IT];
		if ($cumulated_proteome_size_IT < $$ref_cumulated_proteome_size_per_index[$index_with_lowsest_cumulated_proteome_size]) {
			$index_with_lowsest_cumulated_proteome_size = $index_IT;
		}
	}
	return $index_with_lowsest_cumulated_proteome_size;

}


#return reference of arraylist of arraylist of integer
sub get_cluster_organisms {
	my ( $number_clusters, $ref_list_ordered_molecule_ids ) = @_;

	if ($number_clusters <= 0) {
		my @empty_array = ();
		return \@empty_array;
	}

	my $size_array_to_return = 0;
	if (scalar(@$ref_list_ordered_molecule_ids) < $number_clusters) {
		$size_array_to_return = scalar(@$ref_list_ordered_molecule_ids);
	} else {
		$size_array_to_return = $number_clusters;
	}

	my @array_to_return = ();
	#filling the array with references to empty array
	for (my $i=0; $i < $size_array_to_return; $i++) {
		my @empty_sub_array = ();
		$array_to_return[$i] = \@empty_sub_array;
	}
	my @cumulated_proteome_size_per_index_in_array_to_return = (0) x $size_array_to_return;

	foreach my $molecule_ids_IT ( @$ref_list_ordered_molecule_ids ) {
		
		#get the size of the proteome
		my $proteome_size_IT = 0;
		if (exists $molecule_id_2_size{$molecule_ids_IT} ) {
			$proteome_size_IT = $molecule_id_2_size{$molecule_ids_IT};
		} else {
			#print LOG "error in get_cluster_organisms : \$molecule_ids_IT $molecule_ids_IT doesn't exists in \%molecule_id_2_size\n";
			die_with_error_mssg("error in get_cluster_organisms : \$molecule_ids_IT $molecule_ids_IT doesn't exists in \%molecule_id_2_size");
		}

		#find the index with lowsest cumulated proteome size to add this molecule to
		my $index_with_lowsest_cumulated_proteome_size = find_index_with_lowsest_cumulated_proteome_size(\@cumulated_proteome_size_per_index_in_array_to_return);


		# add the genome to the index above
		$cumulated_proteome_size_per_index_in_array_to_return[$index_with_lowsest_cumulated_proteome_size] += $proteome_size_IT;
		my $ref_sub_array_IT = $array_to_return[$index_with_lowsest_cumulated_proteome_size];
		push(@$ref_sub_array_IT,$molecule_ids_IT);
		
	} # foreach my $molecule_ids_IT ( @$ref_list_ordered_molecule_ids ) {

	if ($VERBOSE =~ m/^ON$/) {
		print LOG "\nSummary of the cluster created:\n";
		for(my $index=0;$index<=$#array_to_return;$index++){
			my $ref_sub_array_IT = $array_to_return[$index];
			print LOG "cluster number = $index ; molecules ids = ".join(", ", @$ref_sub_array_IT);
			print LOG " ; cumulated proteome size = ".$cumulated_proteome_size_per_index_in_array_to_return[$index]."\n";
			print LIST_CLUSTER "cluster number = $index ; molecules ids = ".join(", ", @$ref_sub_array_IT)."\n";
		}
		print LOG "\n";
	}

	return \@array_to_return;

} #sub get_cluster_organisms {



if ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {

	print LOG "Starting generating fasta and makedb file command for the mode CLUSTER_ORGANISM\n\n" unless $VERBOSE =~ m/^OFF$/;

	my $cumulated_proteome_size_new_molecules = calculate_cumulated_proteome_size(\@list_ordered_new_molecule_id);
	my $cumulated_proteome_size_old_molecules = calculate_cumulated_proteome_size(\@list_ordered_old_molecule_id);

	# if $BlastConfig::number_clusters_organism_best_adjust > 0
	if($BlastConfig_number_clusters_organism_best_adjust > 0) {

		my $percent_new_proteome_size_out_of_total_proteome_size = $cumulated_proteome_size_new_molecules / ( $cumulated_proteome_size_new_molecules + $cumulated_proteome_size_old_molecules );
		my $number_clusters_new_molecule_percent = $BlastConfig_number_clusters_organism_best_adjust * $percent_new_proteome_size_out_of_total_proteome_size;
		my $number_clusters_new_molecule = sprintf "%.0f", $number_clusters_new_molecule_percent;
		my $number_clusters_old_molecule = $BlastConfig_number_clusters_organism_best_adjust - $number_clusters_new_molecule;
		my $ref_cluster_new_organism_ids = get_cluster_organisms(
			$number_clusters_new_molecule,
			\@list_ordered_new_molecule_id
			);
		foreach my $ref_al_molecule_ids_IT ( @$ref_cluster_new_organism_ids ) {
			print_fasta_list_molecules($ref_al_molecule_ids_IT, 1);
			create_makedb_file_list_molecules($ref_al_molecule_ids_IT, 1);
		}
		my $ref_cluster_old_organism_ids = get_cluster_organisms(
			$number_clusters_old_molecule,
			\@list_ordered_old_molecule_id
			);
		foreach my $ref_al_molecule_ids_IT ( @$ref_cluster_old_organism_ids ) {
			print_fasta_list_molecules($ref_al_molecule_ids_IT, 0);
			# no need to create makedb for old, will blast against new
			#create_makedb_file_list_molecules($ref_al_molecule_ids_IT, 0);
		}

	} elsif ($BlastConfig_avg_number_proteines_per_cluster_best_adjust > 0) {

		#calculate number cluster new
		my $number_cluster_new_percent = ( $cumulated_proteome_size_new_molecules / $BlastConfig_avg_number_proteines_per_cluster_best_adjust ) ;
		my $number_cluster_new = sprintf "%.0f", $number_cluster_new_percent;
		my $ref_cluster_new_organism_ids = get_cluster_organisms(
			$number_cluster_new,
			\@list_ordered_new_molecule_id
			);
		foreach my $ref_al_molecule_ids_IT ( @$ref_cluster_new_organism_ids ) {
			print_fasta_list_molecules($ref_al_molecule_ids_IT, 1);
			create_makedb_file_list_molecules($ref_al_molecule_ids_IT, 1);
		}

		my $number_cluster_old_percent = ( $cumulated_proteome_size_old_molecules / $BlastConfig_avg_number_proteines_per_cluster_best_adjust ) ;
		my $number_cluster_old = sprintf "%.0f", $number_cluster_old_percent;
		my $ref_cluster_old_organism_ids = get_cluster_organisms(
			$number_cluster_old,
			\@list_ordered_old_molecule_id
			);
		foreach my $ref_al_molecule_ids_IT ( @$ref_cluster_old_organism_ids ) {
			print_fasta_list_molecules($ref_al_molecule_ids_IT, 0);
			# no need to create makedb for old, will blast against new
			#create_makedb_file_list_molecules($ref_al_molecule_ids_IT, 0);
		}


	} else {
		#print LOG "BLAST_TAXO_GRANULARITY is CLUSTER_ORGANISM but neither BlastConfig_number_clusters_organism_best_adjust or BlastConfig_avg_number_proteines_per_cluster_best_adjust are set.\nPlease modify the file BlastConfig appropriately\n";
		die_with_error_mssg("BLAST_TAXO_GRANULARITY is CLUSTER_ORGANISM but neither BlastConfig_number_clusters_organism_best_adjust or BlastConfig_avg_number_proteines_per_cluster_best_adjust are set.\nPlease modify the file BlastConfig appropriately");
	}

} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {

	print LOG "Starting generating fasta and makedb file command for the mode ORGANISM_VS_ALL\n\n" unless $VERBOSE =~ m/^OFF$/;

	open( FASTA_ALL, ">$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/orga_all.faa"
	) or die_with_error_mssg(
		"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/orga_all.faa"
	);

	for my $i (0 .. $#list_ordered_new_molecule_id){
		my $orga_id_IT = $list_ordered_new_molecule_id[$i];

		print LOG "Generating fasta and makedb file command for orga_id = ${orga_id_IT}\n" unless $VERBOSE =~ m/^OFF$/;

		open( FASTA,
			">$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/new_molecules/orga_${orga_id_IT}.faa"
		) or die_with_error_mssg(
			"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/new_molecules/orga_${orga_id_IT}.faa"
		);
		#Origami database query : all rows of table genes that have element_id == $elt_id, showing column gene_id, name, residues
		my $genes = $ORIGAMI::dbh->selectall_arrayref(
			"SELECT genes.gene_id, genes.name, micado.prot_feat.proteine, genes.element_id FROM genes, micado.prot_feat where organism_id=$orga_id_IT AND genes.accession = micado.prot_feat.accession AND genes.feature_id = micado.prot_feat.code_feat"
		);
		# pour chaques genes
		foreach my $gene ( @{$genes} ) {
			my ( $gene_id, $name, $residues, $element_id ) = @{$gene};
			$name =~ s/\s/=/g;
			$name =~ tr/()/==/;

			print FASTA_ALL ">${name}_${orga_id_IT}_${element_id}_${gene_id}\n$residues\n";
			print FASTA ">${name}_${orga_id_IT}_${element_id}_${gene_id}\n$residues\n";


		}
		#close  FASTA
		close(FASTA);

	}

	for my $i (0 .. $#list_ordered_old_molecule_id){
		my $orga_id_IT = $list_ordered_old_molecule_id[$i];

		print LOG "Generating fasta and makedb file command for orga_id = ${orga_id_IT}\n" unless $VERBOSE =~ m/^OFF$/;

		#Origami database query : all rows of table genes that have element_id == $elt_id, showing column gene_id, name, residues
		my $genes = $ORIGAMI::dbh->selectall_arrayref(
			"SELECT genes.gene_id, genes.name, micado.prot_feat.proteine, genes.element_id FROM genes, micado.prot_feat where organism_id=$orga_id_IT AND genes.accession = micado.prot_feat.accession AND genes.feature_id = micado.prot_feat.code_feat"
		);

		# pour chaques genes
		foreach my $gene ( @{$genes} ) {
			my ( $gene_id, $name, $residues, $element_id ) = @{$gene};
			$name =~ s/\s/=/g;
			$name =~ tr/()/==/;

			print FASTA_ALL ">${name}_${orga_id_IT}_${element_id}_${gene_id}\n$residues\n";

		}

	}

	#close  FASTA_ALL
	close(FASTA_ALL);


	if ($GENERATE_FOR_IDRIS =~ m/^ON$/) {
		print MAKEBLAST_BATCHFILE_FOR_IDRIS "$SiteConfig::FORMATDB -in \$fasta_formatdb_dir/orga_all.faa -input_type fasta -dbtype prot -out \$fasta_formatdb_dir/orga_all\n";
	} else {

		open( MAKEBLASTDB_BATCHFILE_NEW, "> $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/batch_orga_all.makeblastdb.ll")
			or die_with_error_mssg("Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/batch_orga_all.makeblastdb.ll");

		print MAKEBLASTDB_BATCHFILE_NEW "#!/bin/bash
#\$ -S /bin/bash
# (n) No mail is sent ; Avertir au début (b)egin, à la fin (e)nd, à l'éliminaton (a)bort et
# à la suspension (s)uspend d'un job
#\$ -m n
# Adresse mail à laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#\$ -o $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_orga_all.out
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -e $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_orga_all.err
# Lance la commande depuis le répertoire où est lancé le script
#\$ -cwd\n";
		print MAKEBLASTDB_BATCHFILE_NEW "$SiteConfig::FORMATDB -in $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/orga_all.faa -input_type fasta -dbtype prot -out $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/orga_all && touch $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/orga_all.makeblastdb_done\n";
		close (MAKEBLASTDB_BATCHFILE_NEW);
	}

	print LOG "Done generating fasta and makedb file command for the mode ORGANISM_VS_ALL\n\n" unless $VERBOSE =~ m/^OFF$/;


} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {

	print LOG "Starting generating fasta and makedb file command for the mode ELEMENT_VS_ELEMENT\n\n" unless $VERBOSE =~ m/^OFF$/;

	foreach my $element_id_IT (@list_ordered_new_molecule_id) {
		my @list_molecules_IT = ();
		push ( @list_molecules_IT, $element_id_IT) ;
		print_fasta_list_molecules(\@list_molecules_IT, 1);
		create_makedb_file_list_molecules(\@list_molecules_IT, 1);
	}
	foreach my $element_id_IT (@list_ordered_old_molecule_id) {
		my @list_molecules_IT = ();
		push ( @list_molecules_IT, $element_id_IT) ;
		print_fasta_list_molecules(\@list_molecules_IT, 0);
		# no need to create makedb for old, will blast against new
		#create_makedb_file_list_molecules(\@list_molecules_IT, 0);
	}

	print LOG "Done generating fasta and makedb file command for the mode ELEMENT_VS_ELEMENT\n\n" unless $VERBOSE =~ m/^OFF$/;

} else {
	#print LOG "BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately\n";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}

#close
if($GENERATE_FOR_IDRIS =~ m/^ON$/i){
	close (MAKEBLAST_BATCHFILE_FOR_IDRIS);
}

#end of script
print LOG
"\n---------------------------------------------------\n\n\n Task_generate_fasta.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;


# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);
if ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	close(LIST_CLUSTER);
}




