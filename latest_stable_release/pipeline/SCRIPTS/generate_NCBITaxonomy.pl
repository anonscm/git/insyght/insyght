#!/usr/local/bin/perl
#
# generate_NCBITaxonomy.pl
#
# Options :
# if you wish to insert all the taxonomic nodes regardless of their presence or not in the database, use the option -TAKE_INTO_ACCOUNT_TABLE_SEQUENCES OFF ; By default, this option is ON therefore only the minimum set of taxonomic nodes that are needed for the database is kept
# if you wish to keep not only the scientific name of the taxonomic node but also the synonymous etc... present in the file names.dmp, use the option -ONLY_SCIENTIFIC_NAME OFF ; by default, this option is ON and only keep the scientific names
#
# Pipeline origami Copyright - INRA - 2012-2025
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2015), Valentin Loux
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use SiteConfig;
use ORIGAMI;


# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/Update_taxo/generate_NCBITaxonomy.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/generate_NCBITaxonomy.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/generate_NCBITaxonomy.error";
my $output_backtick = "";
my $VERBOSE = "ON";
my $file_names = ""; # default but can be set with option NAMES_DMP_INPUT_FILE was $SiteConfig::DATADIR/Update_taxo/names.dmp
my $file_nodes = ""; # default but can be set with option NODES_DMP_INPUT_FILE was $SiteConfig::DATADIR/Update_taxo/nodes.dmp
my $DIR_TMP_DOWNLOAD = "$SiteConfig::BANKDIR/../DIR_TMP_DOWNLOAD/";
my $DO_NOT_DELETE_DIR_TMP_DOWNLOAD = "OFF";
my $taxo_sql_file = "$SiteConfig::DATADIR/Update_taxo/ncbi_taxonomy.dat";
#my $taxo_names_sql_file = "$SiteConfig::DATADIR/Update_taxo/taxo_names.dat";
my $TAKE_INTO_ACCOUNT_TABLE_SEQUENCES = "ON";
#my $ONLY_SCIENTIFIC_NAME = "ON";
my %taxonId2scientificName = ();
my %tax_id2parent_tax_id = ();
my %parent_tax_id2presence = ();
my %tax_id2rank = ();
my %list_leaf_taxon_id_to_restrict_to = ();
my %tax_id_in_tree2lineage = ();


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}

foreach my $argnum ( 0 .. $#ARGV ) {
	if  ( $ARGV[$argnum] =~ m/^-TAKE_INTO_ACCOUNT_TABLE_SEQUENCES$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$TAKE_INTO_ACCOUNT_TABLE_SEQUENCES = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -TAKE_INTO_ACCOUNT_TABLE_SEQUENCES argument ; usage : perl generate_NCBITaxonomy.pl -TAKE_INTO_ACCOUNT_TABLE_SEQUENCES {ON, OFF}");
		}
=pod
	} elsif  ( $ARGV[$argnum] =~ m/^-ONLY_SCIENTIFIC_NAME$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$ONLY_SCIENTIFIC_NAME = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -ONLY_SCIENTIFIC_NAME argument ; usage : perl generate_NCBITaxonomy.pl -ONLY_SCIENTIFIC_NAME {ON, OFF}");
		}
=cut
	} elsif  ( $ARGV[$argnum] =~ m/^-NAMES_DMP_INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$file_names = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -NAMES_DMP_INPUT_FILE argument ; usage : perl generate_NCBITaxonomy.pl -NAMES_DMP_INPUT_FILE {path_to_file}");
		}
	} elsif  ( $ARGV[$argnum] =~ m/^-NODES_DMP_INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$file_nodes = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -NODES_DMP_INPUT_FILE argument ; usage : perl generate_NCBITaxonomy.pl -NODES_DMP_INPUT_FILE {path_to_file}");
		}
	}
#DO_NOT_DELETE_DIR_TMP_DOWNLOAD
	elsif ( $ARGV[$argnum] =~ m/^-DO_NOT_DELETE_DIR_TMP_DOWNLOAD$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$DO_NOT_DELETE_DIR_TMP_DOWNLOAD = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -DO_NOT_DELETE_DIR_TMP_DOWNLOAD argument ; usage : perl generate_NCBITaxonomy.pl -DO_NOT_DELETE_DIR_TMP_DOWNLOAD = {ON, OFF}");
		}
	}
	elsif  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl generate_NCBITaxonomy.pl -VERBOSE {ON, OFF}");
		}
	}
}

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log 
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Update_taxo/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG "\n---------------------------------------------------\n generate_NCBITaxonomy.pl started at :", scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# mkdir and rm file
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Update_taxo`;
`$SiteConfig::CMDDIR/rm -rf $SiteConfig::DATADIR/Update_taxo/*`;
`$SiteConfig::CMDDIR/mkdir -p $DIR_TMP_DOWNLOAD`;
if ( $DO_NOT_DELETE_DIR_TMP_DOWNLOAD =~ m/^OFF$/i ) {
	$output_backtick = `$SiteConfig::CMDDIR/rm -rf $DIR_TMP_DOWNLOAD/*`;
}
# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}


if ($file_nodes eq "" && $file_names eq "") {
	# download from NCBI
	print LOG "Downloading taxdump.tar.gz...\n" unless $VERBOSE =~ m/^OFF$/;
	$output_backtick = `curl https://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz -o "$DIR_TMP_DOWNLOAD/taxdump.tar.gz" --create-dirs --remote-time --retry 4 --retry-delay 40 --silent --show-error`;
	if ($output_backtick eq "" && -e "$DIR_TMP_DOWNLOAD/taxdump.tar.gz" ) {
		#ok
	} else {
		die_with_error_mssg("Error could not complete download taxdump : curl https://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz -o \"$DIR_TMP_DOWNLOAD/taxdump.tar.gz\" --create-dirs --remote-time --retry 2 --retry-delay 5 --silent --show-error: $!");
	}
	
	print LOG "Exctracting taxdump.tar.gz...\n" unless $VERBOSE =~ m/^OFF$/;
	$output_backtick = `tar xzf $DIR_TMP_DOWNLOAD/taxdump.tar.gz -C $DIR_TMP_DOWNLOAD`;
	if ($output_backtick eq "") {
		#ok
	} else {
		die_with_error_mssg("Error could not exctract taxdump : tar xzf $DIR_TMP_DOWNLOAD/taxdump.tar.gz -C $DIR_TMP_DOWNLOAD: $!");
	}
	if (-e "$DIR_TMP_DOWNLOAD/nodes.dmp") {
		$file_nodes = "$DIR_TMP_DOWNLOAD/nodes.dmp";
	} else {
		die_with_error_mssg("Error file $DIR_TMP_DOWNLOAD/nodes.dmp does not exists");
	}
	if (-e "$DIR_TMP_DOWNLOAD/names.dmp") {
		$file_names = "$DIR_TMP_DOWNLOAD/names.dmp";
	} else {
		die_with_error_mssg("Error file $DIR_TMP_DOWNLOAD/names.dmp does not exists");
	}

} elsif ($file_nodes ne "" && $file_names ne "") {
	# set through args, do nothing
} else {
	die_with_error_mssg ("incorrect arguments : both -NAMES_DMP_INPUT_FILE and -NODES_DMP_INPUT_FILE needs to be set through arguments or else should not be set at all ; usage : perl generate_NCBITaxonomy.pl OR perl generate_NCBITaxonomy.pl -NAMES_DMP_INPUT_FILE {path_to_file} -NODES_DMP_INPUT_FILE {path_to_file}");
}




=pod
names.dmp
Taxonomy names file (names.dmp):
        tax_id                                  -- the id of node associated with this name
        name_txt                                -- name itself
        unique name                             -- the unique variant of this name if name not unique
        name class                              -- (synonym, common name, ...)

1	|	all	|		|	synonym	|
1	|	root	|		|	scientific name	|
2	|	Bacteria	|	Bacteria <prokaryotes>	|	scientific name	|
2	|	Monera	|	Monera <Bacteria>	|	in-part	|
2	|	Procaryotae	|	Procaryotae <Bacteria>	|	in-part	|
2	|	Prokaryota	|	Prokaryota <Bacteria>	|	in-part	|
2	|	Prokaryotae	|	Prokaryotae <Bacteria>	|	in-part	|
2	|	bacteria	|	bacteria <blast2>	|	blast name	|
2	|	eubacteria	|		|	genbank common name	|
2	|	not Bacteria Haeckel 1894	|		|	synonym	|
...
2109352	|	Aestuariimonas	|		|	scientific name	|
2109356	|	'Falsarthrobacter '	|		|	synonym	|
2109356	|	Falsarthrobacter	|		|	scientific name	|
2109360	|	'Aestuariibius'	|		|	synonym	|
2109360	|	Aestuariibius	|		|	scientific name	|
2109368	|	Photinia arguta	|		|	scientific name	|
2109368	|	Photinia arguta Wall. ex Lindl., 1837	|		|	authority	|
2109369	|	'Pseudomaribius'	|		|	synonym	|
2109369	|	Pseudomaribius	|		|	scientific name	|
2109526	|	unclassified Nymphulinae	|		|	scientific name	|



SELECT ncbi_taxid, name FROM micado.taxo_names WHERE ncbi_taxid IN (...) AND type = 'scientific name'


test_origami=> \d micado.taxo_names 
            Table "micado.taxo_names"
   Column    |          Type          | Modifiers 
-------------+------------------------+-----------
 ncbi_taxid  | integer                | not null
 name        | character varying(256) | not null
 type        | character varying(50)  | not null
 linked_name | character varying(150) | not null
Indexes:
    "pk_taxo_names" PRIMARY KEY, btree (ncbi_taxid, name, type)
Foreign-key constraints:
    "fk_taxo_names_ncbi_taxid" FOREIGN KEY (ncbi_taxid) REFERENCES taxo(ncbi_taxid)

=cut

# slurp in names.dmp
print LOG "slurp in names.dmp...\n" unless $VERBOSE =~ m/^OFF$/;
my $names_dmp_infile;
open  ($names_dmp_infile,  '<',  $file_names ) or do {
    die_with_error_mssg("Can't read names.dmp file $file_names : $!");
};	
while( my $line = <$names_dmp_infile> ) {
	chomp($line);
	#$line =~ s/^\s+//;
	#$line =~ s/\s+$//;
	my @split = split(/\|/, $line);
	my $tax_id;#-- the id of node associated with this name
	my $name_txt;#-- name itself
	#my $unique_name;#-- the unique variant of this name if name not unique
	my $name_class;#-- (synonym, common name, ...)
	my $count_column = 0;
	foreach my $line_split ( @split ) {
		$line_split =~ s/^\s+//;
		$line_split =~ s/\s+$//;
		#print "count_column=$count_column ; line_split=$line_split\n";
		if ($count_column == 0) {
			# tax_id
			if ( $line_split =~ m/^(\d+)$/ ) {
				$tax_id = $line_split;
			} else {
				die_with_error_mssg("Error in names.dmp file $file_names\nline : $line\ntax_id does not contain digit only");
			}
		} elsif ($count_column == 1) {
			# name_txt
			$name_txt = $line_split;
		} elsif ($count_column == 2) {
			# unique_name
		} elsif ($count_column == 3) {
			# name_class
			$name_class = $line_split;
		#} elsif ($count_column == 4) {
		} else {
			die_with_error_mssg("Error in names.dmp file $file_names\nline : $line\nMore than 4 column");
		}

		$count_column++;
	}
	if ($name_class eq "scientific name" ) {
		$taxonId2scientificName{$tax_id} = $name_txt;
	}


}
close $names_dmp_infile;

#System time (seconds): 0.14 ; Maximum resident set size (kbytes): 315544 = 315 MB



=pod

 nodes.dmp

nodes.dmp file consists of taxonomy nodes. The description for each node includes the following
fields:
        tax_id                                  -- node id in GenBank taxonomy database
        parent tax_id                           -- parent node id in GenBank taxonomy database
        rank                                    -- rank of this node (superkingdom, kingdom, ...) 
        embl code                               -- locus-name prefix; not unique
        division id                             -- see division.dmp file
        inherited div flag  (1 or 0)            -- 1 if node inherits division from parent
        genetic code id                         -- see gencode.dmp file
        inherited GC  flag  (1 or 0)            -- 1 if node inherits genetic code from parent
        mitochondrial genetic code id           -- see gencode.dmp file
        inherited MGC flag  (1 or 0)            -- 1 if node inherits mitochondrial gencode from parent
        GenBank hidden flag (1 or 0)            -- 1 if name is suppressed in GenBank entry lineage
        hidden subtree root flag (1 or 0)       -- 1 if this subtree has no sequence data yet
        comments                                -- free-text comments and citations


1	|	1	|	no rank	|		|	8	|	0	|	1	|	0	|	0	|	0	|	0	|	0	|	|
2	|	131567	|	superkingdom	|		|	0	|	0	|	11	|	0	|	0	|	0	|	0	|	0	||
6	|	335928	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|	|
7	|	6	|	species	|	AC	|	0	|	1	|	11	|	1	|	0	|	1	|	1	|	0	|	|
9	|	32199	|	species	|	BA	|	0	|	1	|	11	|	1	|	0	|	1	|	1	|	0	|	|
10	|	1706371	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|	|
11	|	1707	|	species	|	CG	|	0	|	1	|	11	|	1	|	0	|	1	|	1	|	0	|	|
13	|	203488	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|	|
14	|	13	|	species	|	DT	|	0	|	1	|	11	|	1	|	0	|	1	|	1	|	0	|	|
16	|	32011	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|	|
....
2109287	|	1920660	|	genus	|		|	1	|	1	|	1	|	1	|	5	|	1	|	0	|	0	|	|
2109339	|	450425	|	species	|	FS	|	4	|	1	|	1	|	1	|	4	|	1	|	1	|	0	|	|
2109340	|	450425	|	species	|	FS	|	4	|	1	|	1	|	1	|	4	|	1	|	1	|	0	|	|
2109346	|	506	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|	|
2109352	|	49546	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|	|
2109356	|	1268	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|	|
2109360	|	31989	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|	|
2109368	|	23199	|	species	|	PA	|	4	|	1	|	1	|	1	|	1	|	1	|	1	|	0	|	|
2109369	|	31989	|	genus	|		|	0	|	1	|	11	|	1	|	0	|	1	|	0	|	0	|	|
2109526	|	299362	|	no rank	|		|	1	|	1	|	1	|	1	|	5	|	1	|	0	|	0	|	|

SELECT ncbi_taxid, lineage FROM micado.taxo WHERE ncbi_taxid IN (...);

test_origami=> \d micado.taxo
                    Table "micado.taxo"
      Column      |          Type          |   Modifiers   
------------------+------------------------+---------------
 ncbi_taxid       | integer                | not null
 in_micado        | boolean                | not null
 father_id        | integer                | not null
 lineage          | integer[]              | 
 rank             | character varying(25)  | not null
 taxid            | character varying(100) | 
 depth            | integer                | 
 leaf             | boolean                | default false
 sequences        | integer                | 
 sequences_length | bigint                 | 
 lft              | integer                | not null
 rgt              | integer                | not null
Indexes:
    "pk_taxo" PRIMARY KEY, btree (ncbi_taxid)
Check constraints:
    "order_okay_taxo" CHECK (lft < rgt)
    "taxo_lft_check" CHECK (lft > '-1'::integer)
    "taxo_rgt_check" CHECK (rgt > 0)
Referenced by:
    TABLE "taxo_names" CONSTRAINT "fk_taxo_names_ncbi_taxid" FOREIGN KEY (ncbi_taxid) REFERENCES taxo(ncbi_taxid)


=cut


# slurp in taxo.dmp
print LOG "slurp in taxo.dmp...\n" unless $VERBOSE =~ m/^OFF$/;
my $nodes_dmp_infile;
open  ($nodes_dmp_infile,  '<',  $file_nodes ) or do {
    die_with_error_mssg("Can't read nodes.dmp file $file_nodes : $!");
};	
while( my $line = <$nodes_dmp_infile> ) {
	chomp($line);
	#$line =~ s/^\s+//;
	#$line =~ s/\s+$//;

	my @split = split(/\|/, $line);
	my $tax_id; #-- node id in GenBank taxonomy database
	my $parent_tax_id; #-- parent node id in GenBank taxonomy database
	my $rank; #-- rank of this node (superkingdom, kingdom, ...) 
=pod
	my $embl_code; #-- locus-name prefix; not unique
	my $division_id; #-- see division.dmp file
	my $inherited_div_flag; #(1 or 0)            -- 1 if node inherits division from parent
	my $genetic_code_id; #-- see gencode.dmp file
	my $inherited_GC_flag; #(1 or 0)            -- 1 if node inherits genetic code from parent
	my $mitochondrial_genetic_code_id; #-- see gencode.dmp file
	my $inherited_MGC_flag; #(1 or 0)            -- 1 if node inherits mitochondrial gencode from parent
	my $GenBank_hidden_flag; #(1 or 0)            -- 1 if name is suppressed in GenBank entry lineage
	my $hidden_subtree_root_flag; #(1 or 0)       -- 1 if this subtree has no sequence data yet
	my $comments; #-- free-text comments and citations
=cut
	my $count_column = 0;
	foreach my $line_split ( @split ) {
		$line_split =~ s/^\s+//;
		$line_split =~ s/\s+$//;
		if ($count_column == 0) {
			# tax_id
			if ( $line_split =~ m/^(\d+)$/ ) {
				$tax_id = $line_split;
			} else {
				die_with_error_mssg("Error in nodes.dmp file $file_nodes\nline : $line\ntax_id does not contain digit only");
			}
		} elsif ($count_column == 1) {
			# parent_tax_id
			if ( $line_split =~ m/^(\d+)$/ ) {
				$parent_tax_id = $line_split;
			} else {
				die_with_error_mssg("Error in nodes.dmp file $file_nodes\nline : $line\nparent_tax_id does not contain digit only");
			}
		} elsif ($count_column == 2) {
			# rank
			$rank = $line_split;
		}

		$count_column++;
	}
	$tax_id2parent_tax_id{$tax_id} = $parent_tax_id; #Elapsed (wall clock) time (h:mm:ss or m:ss): 0:43.35 ; Maximum resident set size (kbytes): 491928 = 491 MB
	$parent_tax_id2presence{$parent_tax_id} = undef; # Maximum resident set size (kbytes): = 499 MB
	if ($rank =~ m/^\s*no\s+rank\s*$/ || $rank eq "" ) {
	} else {
		$tax_id2rank{$tax_id} = $rank; # Elapsed (wall clock) time (h:mm:ss or m:ss): 0:35.53 ; Maximum resident set size (kbytes): 655864 = 655 MB
	}

}
close $nodes_dmp_infile;

# Elapsed (wall clock) time (h:mm:ss or m:ss): 0:35.53 ; Maximum resident set size (kbytes): 655864 = 655 MB

# set list_leaf_taxon_id_to_restrict_to
print LOG "Finding taxon id leaves of interest...\n" unless $VERBOSE =~ m/^OFF$/;
if ($TAKE_INTO_ACCOUNT_TABLE_SEQUENCES =~ m/^ON$/i) {
	#my $st = $ORIGAMI::dbh->prepare("SELECT DISTINCT ncbi_taxid FROM micado.sequences");
	my $st = $ORIGAMI::dbh->prepare("SELECT DISTINCT taxon_id FROM organisms WHERE computation_in_process IS FALSE");
	$st->execute or die_with_error_mssg("Pb execute $!");
	while ( my $res = $st->fetchrow_hashref() ) {
		my $ncbi_taxid_IT = $res->{taxon_id};
		if (defined $ncbi_taxid_IT && $ncbi_taxid_IT != "") {
			$list_leaf_taxon_id_to_restrict_to{$ncbi_taxid_IT} = undef ;
		}

	}
} else {
	# find leaves in whole tree
	foreach my $tax_id (keys %tax_id2parent_tax_id) {
		if ( ! exists $parent_tax_id2presence{$tax_id} ) {
			if (defined $tax_id && $tax_id != "") {
				$list_leaf_taxon_id_to_restrict_to{$tax_id} = undef ;
			}
		}
	}
}
print LOG scalar(keys %list_leaf_taxon_id_to_restrict_to)." taxon id leaves of interest found\n" unless $VERBOSE =~ m/^OFF$/;
# Elapsed (wall clock) time (h:mm:ss or m:ss): 0:36.23 ; Maximum resident set size (kbytes): 647068


sub buildPathTaxoTree {

	my ($tax_id_IT) = @_;
	if (defined $tax_id_IT && $tax_id_IT != "" ) {
		if (exists $tax_id_in_tree2lineage{$tax_id_IT} ) {
			# already stored
			return $tax_id_in_tree2lineage{$tax_id_IT};
		} else {
			my $build_up_lineage = "";
			my $parent_tax_id = $tax_id2parent_tax_id{$tax_id_IT};
			if ($parent_tax_id != $tax_id_IT && $parent_tax_id != 0 && $parent_tax_id ne "" ) {
				# not beyond root of the tree
				$build_up_lineage = buildPathTaxoTree($parent_tax_id);
			} else {
				# beyond root of the tree, stop
			}

			if ($build_up_lineage eq "") {
				# root
				$build_up_lineage = $tax_id_IT;
			} else {
				$build_up_lineage .= ",".$tax_id_IT;
			}

			$tax_id_in_tree2lineage{$tax_id_IT} = $build_up_lineage;

			return $build_up_lineage;
		}
	}

}


print LOG "building up taxo tree for leaves of interest...\n" unless $VERBOSE =~ m/^OFF$/;
foreach my $tax_id_leaf (keys %list_leaf_taxon_id_to_restrict_to) {
	buildPathTaxoTree($tax_id_leaf);
}

print LOG "printing taxo tree for leaves of interest...\n" unless $VERBOSE =~ m/^OFF$/;
open(FICOUT,">$taxo_sql_file") 
	or die_with_error_mssg("Problem opening data storage file taxo_sql_file : ".$taxo_sql_file);

# header
print FICOUT "BEGIN WORK;\n";
print FICOUT "COPY micado.ncbi_taxonomy_tree (ncbi_taxid, parent_taxid, lineage, rank, depth, is_leaf, scientific_name) FROM stdin;\n";

foreach my $ncbi_taxid (keys %tax_id_in_tree2lineage) {

	my $parent_taxid = $tax_id2parent_tax_id{$ncbi_taxid};

	if ($parent_taxid == $ncbi_taxid || $parent_taxid eq "") {
		#root
		$parent_taxid = 0;
	}
	my $rank = $tax_id2rank{$ncbi_taxid};
	my $lineage_tmp = "";
	my @split_lineage = split(/,/, $tax_id_in_tree2lineage{$ncbi_taxid});
	for my $i (0 .. $#split_lineage) {
		if ($i == $#split_lineage) {
			#print "skipping last : ".$split_lineage[$i]."\n";
		} elsif ($i == 0) {
			$lineage_tmp = $split_lineage[$i];
		} else {
			$lineage_tmp .= ",".$split_lineage[$i];
		}
	}
	my $lineage = "{".$lineage_tmp."}";
	my $depth = scalar(@split_lineage) - 1;
	my $is_leaf = "FALSE";
	if (exists $list_leaf_taxon_id_to_restrict_to{$ncbi_taxid} ) {
		$is_leaf = "TRUE";
	}
	my $scientific_name = $taxonId2scientificName{$ncbi_taxid};
	if (defined $ncbi_taxid && $ncbi_taxid != "" ) {
		print FICOUT "$ncbi_taxid\t$parent_taxid\t$lineage\t$rank\t$depth\t$is_leaf\t$scientific_name\n";
	}

}


# footer
print FICOUT "\\\.\n";
print FICOUT "COMMIT WORK;\n";


=pod
#WAS .dat file like this :
taxo.dat

SELECT ncbi_taxid, lineage FROM micado.taxo WHERE ncbi_taxid IN (...);

BEGIN WORK;
COPY micado.taxo (ncbi_taxid, in_micado, father_id, lineage, rank, taxid, depth, leaf, sequences, sequences_length, lft, rgt) FROM stdin;
1	true	0	{}	no rank	0	0	false	36	76671701	0	3390179
131567	true	1	{1}	no rank	0.5	1	false	36	76671701	408327	3390178
2	true	131567	{1,131567}	superkingdom	0.5.1	2	false	36	76671701	408328	1366427
1224	true	2	{1,131567,2}	phylum	0.5.1.1	3	false	2	9359042	408329	816196
1236	true	1224	{1,131567,2,1224}	class	0.5.1.1.1	4	false	2	9359042	408330	630797
91347	true	1236	{1,131567,2,1224,1236}	order	0.5.1.1.1.4	5	false	2	9359042	479523	533588
543	true	91347	{1,131567,2,1224,1236,91347}	family	0.5.1.1.1.4.1	6	false	2	9359042	479524	517887
561	true	543	{1,131567,2,1224,1236,91347,543}	genus	0.5.1.1.1.4.1.3	7	false	2	9359042	491395	499522
...
1350	true	81852	{1,131567,2,1783272,1239,91061,186826,81852}	genus	0.5.1.19.1.3.3.4.1	8	false	23	35470290	1138528	1143457
1351	true	1350	{1,131567,2,1783272,1239,91061,186826,81852,1350}	species	0.5.1.19.1.3.3.4.1.1	9	false	23	35470290	1138529	1139292
226185	true	1351	{1,131567,2,1783272,1239,91061,186826,81852,1350,1351}	no rank	0.5.1.19.1.3.3.4.1.1.2	10	true	4	3359974	1138532	1138533
474186	true	1351	{1,131567,2,1783272,1239,91061,186826,81852,1350,1351}	no rank	0.5.1.19.1.3.3.4.1.1.3	10	true	1	2739625	1138534	1138535
1201292	true	1351	{1,131567,2,1783272,1239,91061,186826,81852,1350,1351}	no rank	0.5.1.19.1.3.3.4.1.1.291	10	true	3	3048131	1139110	1139111
1206105	true	1351	{1,131567,2,1783272,1239,91061,186826,81852,1350,1351}	no rank	0.5.1.19.1.3.3.4.1.1.292	10	true	3	3062505	1139112	1139113
1261557	true	1351	{1,131567,2,1783272,1239,91061,186826,81852,1350,1351}	no rank	0.5.1.19.1.3.3.4.1.1.313	10	true	1	2810675	1139154	1139155
1287066	true	1351	{1,131567,2,1783272,1239,91061,186826,81852,1350,1351}	no rank	0.5.1.19.1.3.3.4.1.1.316	10	true	1	2961043	1139160	1139161
\.
COMMIT WORK;

wc -l = 47 ; 4,8K 
=cut



=pod
taxo_names.dat

BEGIN WORK;
COPY micado.taxo_names (ncbi_taxid, name, type, linked_name) FROM stdin;
1	root	scientific name	root
131567	cellular organisms	scientific name	cellular organisms
2	Bacteria	scientific name	Bacteria
1224	Proteobacteria	scientific name	Proteobacteria
1236	Gammaproteobacteria	scientific name	Gammaproteobacteria
91347	Enterobacterales	scientific name	Enterobacterales
543	Enterobacteriaceae	scientific name	Enterobacteriaceae
561	Escherichia	scientific name	Escherichia
...
1350	Enterococcus	scientific name	Enterococcus
1351	Enterococcus faecalis	scientific name	Enterococcus faecalis
226185	Enterococcus faecalis V583	scientific name	Enterococcus faecalis V583
474186	Enterococcus faecalis OG1RF	scientific name	Enterococcus faecalis OG1RF
1201292	Enterococcus faecalis ATCC 29212	scientific name	Enterococcus faecalis ATCC 29212
1206105	Enterococcus faecalis D32	scientific name	Enterococcus faecalis D32
1261557	Enterococcus faecalis str. Symbioflor 1	scientific name	Enterococcus faecalis str. Symbioflor 1
1287066	Enterococcus faecalis DENG1	scientific name	Enterococcus faecalis DENG1
\.
COMMIT WORK;

wc -l = 47 ; 2.9K 

=cut



# end of script
print LOG "---------------------------------------------------\n generate_NCBITaxonomy.pl successfully completed at :", scalar(localtime), "\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;

if ( $DO_NOT_DELETE_DIR_TMP_DOWNLOAD =~ m/^OFF$/i ) {
	$output_backtick = `$SiteConfig::CMDDIR/rm -rf $DIR_TMP_DOWNLOAD/*`;
}

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);

