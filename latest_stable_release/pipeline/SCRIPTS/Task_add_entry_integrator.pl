#!/usr/local/bin/perl
#
# Task_add_entry_integrator.pl
#
# Pipeline origami Copyright - INRA - 2012-2025
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#




use strict;
use Time::HiRes qw(sleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;
use BlastConfig;


# whole script scoped variable
my %add_table_sql_files = ();
my %add_table_sql_files_gz = ();
my %add_table_sql_files_todo_gzip = ();
my $VERBOSE = "ON";
my $SQL_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_entry";
my $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN = -1;
my %accnum_skipped_because_of_MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN = ();
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = -1;
my %accnum_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = ();
my $MARK_TREATED_FILES_AS = "gzip"; #can be gzip or mv_todo_gzip
my $output_backtick = "";
my $counter_loop = 0;
my @ordered_tables_to_import = ("sequences","features","accessions","articles","comments","dna_loc","dna_seq","keywords","locations","prot_feat","qualifiers","organisms","elements","genes");
my $regex_tables = join("|", @ordered_tables_to_import);


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $SiteConfig::LOGDIR/manage_primary_data_integrator.error`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $SiteConfig::LOGDIR/manage_primary_data_integrator.error :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $SiteConfig::LOGDIR/manage_primary_data_integrator.error :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


foreach my $argnum ( 0 .. $#ARGV ) {

	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			#print "incorrect -VERBOSE argument ; usage : perl Task_add_entry_integrator.pl -VERBOSE {ON, OFF}";
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl Task_add_entry_integrator.pl -VERBOSE {ON, OFF}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^(\d)$/i )
		{
			$FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = $1;
			print "\nmode FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT activated : $FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT\n" unless $VERBOSE =~ m/^OFF$/i;
		}
		else {
			#print "incorrect -FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT argument ; usage : perl Task_add_entry_integrator.pl -FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT {DIGIT}\n";
			die_with_error_mssg ("incorrect -FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT argument ; usage : perl Task_add_entry_integrator.pl -FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT {DIGIT}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-SQL_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$SQL_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			#print "incorrect -SQL_INPUT_DIR argument ; usage : perl Task_add_entry_integrator.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}\n";
			die ("incorrect -SQL_INPUT_DIR argument ; usage : perl Task_add_entry_integrator.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN/ ) {

		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN = $ARGV[ $argnum + 1 ];
			if($MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN < 1){
				#print "incorrect -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN argument ; usage : perl Task_add_entry_integrator.pl  -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN {int > 0}\n";
				die_with_error_mssg ("incorrect -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN argument ; usage : perl Task_add_entry_integrator.pl  -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN {int > 0}");
			}
		}
		else {
			#print "incorrect -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN argument ; usage : perl Task_add_entry_integrator.pl  -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN {int > 0}\n";
			die("incorrect -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN argument ; usage : perl Task_add_entry_integrator.pl  -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN {int > 0}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-MARK_TREATED_FILES_AS$/ ) {
		if (  $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$MARK_TREATED_FILES_AS = $ARGV[ $argnum + 1 ];
		}
		else {
			#print "incorrect -MARK_TREATED_FILES_AS argument ; usage : perl Task_add_entry_integrator.pl -MARK_TREATED_FILES_AS {gzip | mv_todo_gzip}";
			die_with_error_mssg("incorrect -MARK_TREATED_FILES_AS argument ; usage : perl Task_add_entry_integrator.pl -MARK_TREATED_FILES_AS {gzip | mv_todo_gzip}");
		}
	}
}

if ( $MARK_TREATED_FILES_AS =~ m/^gzip$/i || $MARK_TREATED_FILES_AS =~ m/^mv_todo_gzip$/i ) {
	#ok
} else {
	#print "incorrect -MARK_TREATED_FILES_AS argument ; usage : perl Task_add_entry_integrator.pl -MARK_TREATED_FILES_AS {gzip | mv_todo_gzip}";
	die_with_error_mssg("incorrect -MARK_TREATED_FILES_AS argument ; usage : perl Task_add_entry_integrator.pl -MARK_TREATED_FILES_AS {gzip | mv_todo_gzip}");
}

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_entry`;
if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0){
	if (-e "$SiteConfig::LOGDIR/Task_add_entry/Task_add_entry_integrator_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log"){
		die_with_error_mssg ("The log file $SiteConfig::LOGDIR/Task_add_entry/Task_add_entry_integrator_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log already exists and mode FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT is activated ; please make sure no other process are using the same FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT and is running in parrallele, else it will cause duplicated data.");
	} else {
		open( LOG, ">$SiteConfig::LOGDIR/Task_add_entry/Task_add_entry_integrator_${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.log"); #| perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
	}
} else {
	open( LOG,">$SiteConfig::LOGDIR/Task_add_entry/Task_add_entry_integrator.log");#| perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
}


#del files beforehands
$output_backtick = `$SiteConfig::CMDDIR/rm -rf $SQL_INPUT_DIR/chargebd.sql`;
# rm marker file done script
$output_backtick .= `rm -f $SiteConfig::LOGDIR/manage_primary_data_integrator.done`;
$output_backtick .= `rm -f $SiteConfig::LOGDIR/manage_primary_data_integrator.error`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $SQL_INPUT_DIR/chargebd.sql or $SiteConfig::LOGDIR/manage_primary_data_integrator.done or .error :\n$output_backtick \n $!");
}

if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	#$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	#$molecule_type = "orgaCluster";
	#die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element";
	#print LOG "BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.\n";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} else {
	#print LOG "BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately\n";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}

print LOG
"\n---------------------------------------------------\n Task_add_entry_integrator.pl started at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/i;


if (! defined $SQL_INPUT_DIR) {
	#print LOG "Undefined SQL_INPUT_DIR ; usage : perl Task_add_entry_integrator.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}\n";
	die_with_error_mssg("Undefined SQL_INPUT_DIR ; usage : perl Task_add_entry_integrator.pl -SQL_INPUT_DIR {PATH_TO_DIRECTORY}");
}


my $count_sql_files = 0;
my $count_gz_files = 0;
my $count_todo_gzip_files = 0;
my $count_files_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = 0;
sub sub_list_sql_files_recursively {
    my ( $path ) = @_;
    #my $path = shift;
    opendir (DIR, $path) or die_with_error_mssg("Unable to open $path: $!");
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);

    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_sql_files_recursively ($_);
# accessions articles comments dna_loc dna_seq features keywords locations prot_feat qualifiers sequences elements genes
# %add_accessions_table_sql_files
        #} elsif ($_ =~ m/^.*\/(.+)\/(accessions|articles|comments|dna_loc|dna_seq|features|keywords|locations|prot_feat|qualifiers|sequences|elements|genes)\.sql$/ ) {
        } elsif ($_ =~ m/^.*\/(.+)\/($regex_tables)\.sql$/ ) {
		my $accnum_IT = $1;
		my $table_IT = $2;
		my $last_digit_accnum_if_any = -1;
		if ($accnum_IT =~ m/^.+(\d).*?$/) {
			$last_digit_accnum_if_any = $1;
		}
		if ( $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN > 0 && scalar( keys %add_table_sql_files ) == $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN ) {
			$accnum_skipped_because_of_MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN{$accnum_IT} = undef;
		} else {
			if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0) {
				if ($last_digit_accnum_if_any == ${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}){
					$count_sql_files++;
					$add_table_sql_files{$accnum_IT}{$table_IT} = $_;
				} else {
					$accnum_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT{$accnum_IT} = undef;
					$count_files_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT++;
				}
			} else {
				$count_sql_files++;
		   		$add_table_sql_files{$accnum_IT}{$table_IT} = $_;
			}
		}
        #} elsif ($_ =~ m/^.*\/(.+)\/(accessions|articles|comments|dna_loc|dna_seq|features|keywords|locations|prot_feat|qualifiers|sequences|elements|genes)\.sql\.gz$/ ) {
        } elsif ($_ =~ m/^.*\/(.+)\/($regex_tables)\.sql\.gz$/ ) {
		$count_gz_files++;
=pod
		my $accnum_IT = $1;
		my $table_IT = $2;
		my $last_digit_accnum_if_any = -1;
		if ($accnum_IT =~ m/^.+(\d).*?$/) {
			$last_digit_accnum_if_any = $1;
		}
		if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0) {
			if ($last_digit_accnum_if_any == ${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}){
				$add_table_sql_files_gz{$accnum_IT}{$table_IT} = $_;
			} else {
				$count_files_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT++;
			}
		} else {
           		$add_table_sql_files_gz{$accnum_IT}{$table_IT} = $_;
		}
=cut
	#} elsif ($_ =~ m/^.*\/(.+)\/(accessions|articles|comments|dna_loc|dna_seq|features|keywords|locations|prot_feat|qualifiers|sequences|elements|genes)\.sql\.todo_gzip$/ ) {
	} elsif ($_ =~ m/^.*\/(.+)\/($regex_tables)\.sql\.todo_gzip$/ ) {
		$count_todo_gzip_files++;
=pod
		my $accnum_IT = $1;
		my $table_IT = $2;
		my $last_digit_accnum_if_any = -1;
		if ($accnum_IT =~ m/^.+(\d).*?$/) {
			$last_digit_accnum_if_any = $1;
		}
		if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0) {
			if ($last_digit_accnum_if_any == ${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}){
				$add_table_sql_files_todo_gzip{$accnum_IT}{$table_IT} = $_;
			} else {
				$count_files_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT++;
			}
		} else {
           		$add_table_sql_files_todo_gzip{$accnum_IT}{$table_IT} = $_;
		}
=cut
	} elsif ($_ =~ m/~$/ ) {
		#tmp files to ignore
        } else {
            #print LOG "Error in sub_list_sql_files_recursively : the file $_ does not match the appropriate regex\n";
            die_with_error_mssg("Error in sub_list_sql_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}

sub_list_sql_files_recursively("$SQL_INPUT_DIR/");
print LOG "$count_gz_files sql.gz file(s) have been found and skipped.\n" unless $VERBOSE =~ m/^OFF$/i;
print LOG "$count_todo_gzip_files sql.todo_gzip file(s) have been found and skipped.\n" unless $VERBOSE =~ m/^OFF$/i;
if ($FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT >= 0) {
	print LOG "".scalar (keys %accnum_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT)." accession number (equivalent to $count_files_skipped_because_of_FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT sql files) have been skipped because of FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT = ${FILTER_BY_MASTER_ELET_ID_END_WITH_DIGIT}.\n" unless $VERBOSE =~ m/^OFF$/i;
}
if ( $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN > 0 ) {
	print LOG "You chose the option -MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN $MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN\n" unless $VERBOSE =~ m/^OFF$/i;
	print LOG "".scalar ( keys %accnum_skipped_because_of_MAX_ACCNUMS_TO_CHARGE_IN_THIS_RUN )." accnum have been skipped because of this option.\n" unless $VERBOSE =~ m/^OFF$/i;
}
print LOG "".scalar(keys %add_table_sql_files)." accession number (equivalent to $count_sql_files sql files) have been found and will be treated by this script.\n" unless $VERBOSE =~ m/^OFF$/i;



sub check_all_sql_files_are_present {
	my ( $ref_add_table_sql_files ) = @_;
	foreach my $accnum_IT (keys %{$ref_add_table_sql_files}) {
		foreach my $tables_to_import_IT (@ordered_tables_to_import) {
			if ( ! exists ${$ref_add_table_sql_files}{$accnum_IT}{$tables_to_import_IT}) {
				    #print LOG "Error in check_all_sql_files_are_present : the sql file corresponding to table $tables_to_import_IT does not exists for the accnum $accnum_IT";
				    die_with_error_mssg("Error in check_all_sql_files_are_present : the sql file corresponding to table $tables_to_import_IT does not exists for the accnum $accnum_IT");
			}
		}
	}
}
check_all_sql_files_are_present(\%add_table_sql_files);





#import db

# import alignment_param and other files in db
print LOG "\nStarting import files in db...\n" unless $VERBOSE =~ m/^OFF$/i;
$counter_loop = 0;
foreach my $accession_to_import_IT ( keys %add_table_sql_files) {
	$counter_loop++;
	print LOG "( $counter_loop / ".scalar(keys %add_table_sql_files)." ) : Start import of accession $accession_to_import_IT\n" unless $VERBOSE =~ m/^OFF$/i;

	# Création fichier de chargement de la base
	open (CHARGEBD,"> $SQL_INPUT_DIR/chargebd.sql") || die_with_error_mssg("Sorry, I can't open $SQL_INPUT_DIR/chargebd.sql");
	print CHARGEBD "BEGIN WORK;\n";
	print CHARGEBD "set search_path to micado,public;\n";
	foreach my $tables_to_import_IT (@ordered_tables_to_import) {
		if ( -e "$SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql" ) {
			print CHARGEBD "\\COPY $tables_to_import_IT FROM $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql WITH NULL AS 'NULL'\n";
		} else {
			#print LOG "Error while importing accession $accession_to_import_IT : the sql file $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql does not exists";
			die_with_error_mssg("Error while importing accession $accession_to_import_IT : the sql file $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql does not exists");
		}
	}
=pod
	print CHARGEBD "\\COPY sequences FROM $outdir/sequences.dat WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY accessions FROM '$outdir/accessions.dat' WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY articles FROM '$outdir/articles.dat' WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY comments FROM '$outdir/comments.dat' WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY dna_seq FROM '$outdir/dna_seq.dat' WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY features FROM '$outdir/features.dat' WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY keywords FROM '$outdir/keywords.dat' WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY locations FROM '$outdir/locations.dat' WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY prot_feat FROM '$outdir/prot_feat.dat' WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY qualifiers FROM '$outdir/qualifiers.dat' WITH NULL AS 'NULL'\n";
	print CHARGEBD "\\COPY dna_loc FROM '$outdir/dna_loc.dat' WITH NULL AS 'NULL'\n";
=cut
	print CHARGEBD "COMMIT WORK;\n";

	# Chargement de la base avec les fichiers générés
	$output_backtick = `psql -f $SQL_INPUT_DIR/chargebd.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1`;
	#$output_backtick = `psql -f $sql_file_IT -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
	if ( $? == -1 )
	{
		#print LOG "command psql -f $SQL_INPUT_DIR/chargebd.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1 failed: $!\n";
		die_with_error_mssg("command psql -f $SQL_INPUT_DIR/chargebd.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1 failed: $!");
	}
	if($output_backtick eq ''
		|| $output_backtick =~ m/^BEGIN\nSET\n(COPY\s+\d+\n)*COMMIT$/
	){
		
	}else{
		#print LOG "Error psql -f $SQL_INPUT_DIR/chargebd.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1 :\n$output_backtick\n";
		die_with_error_mssg("Error psql -f $SQL_INPUT_DIR/chargebd.sql -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host $ORIGAMI::dbname 2>&1 :\n$output_backtick");		
	}


	# mark file to do gzip
	if ( $MARK_TREATED_FILES_AS =~ m/^gzip$/i ) {
		foreach my $tables_to_import_IT (@ordered_tables_to_import) {
			if ( -e "$SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql" ) {
				$output_backtick = `gzip -f $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql`;
				if($output_backtick eq ''
					){
				} else {
					#print LOG "Error gzip $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql :\n$output_backtick\n";
					die_with_error_mssg("Error gzip $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql :\n$output_backtick");		
				}
			} else {
				#print LOG "Error while gzip accession $accession_to_import_IT : the sql file $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql does not exists";
				die_with_error_mssg("Error while gzip accession $accession_to_import_IT : the sql file $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql does not exists");
			}
		}
	} elsif ($MARK_TREATED_FILES_AS =~ m/^mv_todo_gzip$/i) {
		foreach my $tables_to_import_IT (@ordered_tables_to_import) {
			if ( -e "$SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql" ) {
				#$output_backtick = `$SiteConfig::CMDDIR/mv $sql_file_IT $sql_file_IT\.todo_gzip`;
				$output_backtick = `$SiteConfig::CMDDIR/mv $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql.todo_gzip`;
				if($output_backtick eq ''
					){
				} else {
					#print LOG "Error $SiteConfig::CMDDIR/mv $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql.todo_gzip :\n$output_backtick\n";
					die_with_error_mssg("Error $SiteConfig::CMDDIR/mv $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql.todo_gzip :\n$output_backtick");		
				}
			} else {
				#print LOG "Error while gzip accession $accession_to_import_IT : the sql file $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql does not exists";
				die_with_error_mssg("Error while gzip accession $accession_to_import_IT : the sql file $SQL_INPUT_DIR/$accession_to_import_IT/$tables_to_import_IT.sql does not exists");
			}
		}
	} else {
		#print LOG "incorrect -MARK_TREATED_FILES_AS argument ; usage : perl Task_add_entry_integrator.pl -MARK_TREATED_FILES_AS {gzip | mv_todo_gzip}";
		die_with_error_mssg("incorrect -MARK_TREATED_FILES_AS argument ; usage : perl Task_add_entry_integrator.pl -MARK_TREATED_FILES_AS {gzip | mv_todo_gzip}");
	}
	
	print LOG "( $counter_loop / ".scalar(keys %add_table_sql_files)." ) : done import of accnum $accession_to_import_IT.\n" unless $VERBOSE =~ m/^OFF$/i;


}
print LOG "Finished importing sql files in db.\n\n" unless $VERBOSE =~ m/^OFF$/i;

#end of script
print LOG
"\n---------------------------------------------------\n\n\n Task_add_entry_integrator.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/i;
close(LOG);

# touch marker file done script when script done
$output_backtick = `touch $SiteConfig::LOGDIR/manage_primary_data_integrator.done`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $SiteConfig::LOGDIR/manage_primary_data_integrator.done :\n$output_backtick \n $!");#
}


