#!/usr/local/bin/perl
#
# full name = process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl
#
# This suite of scripts parse the tsv output from align into .sql files suitable to be inserted in the database
#
# core name =
# Task_add_alignment_parse_tsv_output
#
# prefix =
# generate_
# launch_
# process_
#
# suffix =
# _Replacing_Forward_Reverse.pl
# _Replacing_Tmp_Iary_key.pl
# _Deleting_redundant_lines_homologies.pl
#
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
#use FindBin;                 # locate this script # no need because of perl -I script
#use lib "$FindBin::Bin/..";  # use the parent directory # no need because of perl -I script
use SiteConfig;
use BlastConfig;

# whole script scoped variable
#my $path_log_file = "$SiteConfig::LOGDIR/generate_correct_primary_key_launcher_files.log"; # no log file created, print on stdout
#my $path_step_done_file = "$SiteConfig::LOGDIR/generate_correct_primary_key_launcher_files.done"; # taking care off in bash script
my $path_step_error_file = undef; # defined below with meaningful core name
my $VERBOSE = "OFF";
my $output_backtick = "";
my $CORE_INPUT_DIR = undef;
my $ALIGN_PARAM_INPUT_FILE = undef;
my $ALIGNMENT_INPUT_FILE = undef;
my $ALIGN_PAIRS_INPUT_FILE = undef;
my $HOMOLOGIES_INPUT_FILE = undef;
my $TANDEM_DUPS_INPUT_FILE = undef;
my $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE = undef;
my $PROT_FUSION_INPUT_FILE = undef;
my $CLOSEBESTMATCHS_INPUT_FILE = undef;
my $params_scores_algo_syntenies_sql_file = undef;
my $REPLACEMENT_ALIGN_PARAM_PRIMARY_ID = undef;
my $REPLACEMENT_ALIGNMENT_PRIMARY_ID = undef;
my $REPLACEMENT_TANDEM_DUPS_PRIMARY_ID = undef;
my $REPLACEMENT_PROT_FUSION_PRIMARY_ID = undef;
my $REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID = undef;
my $CORE_OUTPUT_DIR = undef;
my $LOG_DIR = "$SiteConfig::LOGDIR/Task_add_alignments";
my $MARK_TREATED_FILES_AS = "gzip"; #can be gzip or mv_todo_gzip
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $molecule_type;
my %TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT = ();
my %TMP_ALIGN_PRIMARY_ID_TO_PERM_REPLACEMENT = ();
my %TMP_PROTFUS_PRIMARY_ID_TO_PERM_REPLACEMENT = ();
my %TMP_TANDEMDUPS_PRIMARY_ID_TO_PERM_REPLACEMENT = ();
my $in; # infile to be read
my $out; # outfile to be written
my $CHECK_FIRST_LINE_SCORE_PARAMS = "ON";


#special process need to write mssg in error file
sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	#$output_backtick = `touch $path_step_error_file`;
	#if ($output_backtick eq "") {
	#	#ok
	#} else {
	#	#print "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
	#	die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	#}
	open ( ERR_FILE, "> $path_step_error_file") or die("Can not open error file $path_step_error_file to print error message:\n$error_mssg\n");
	print ERR_FILE "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] eq "ON"
			|| $ARGV[ $argnum + 1 ] eq "OFF" )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -VERBOSE = {ON, OFF}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-CORE_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$CORE_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -CORE_INPUT_DIR argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-ALIGN_PARAM_INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$ALIGN_PARAM_INPUT_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -ALIGN_PARAM_INPUT_FILE argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -ALIGN_PARAM_INPUT_FILE {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-ALIGNMENT_INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$ALIGNMENT_INPUT_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -ALIGNMENT_INPUT_FILE argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -ALIGNMENT_INPUT_FILE {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-ALIGN_PAIRS_INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$ALIGN_PAIRS_INPUT_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -ALIGN_PAIRS_INPUT_FILE argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -ALIGN_PAIRS_INPUT_FILE {PATH_TO_DIRECTORY}");
		}

#my $TANDEM_DUPS_INPUT_FILE = undef;
	} elsif ( $ARGV[$argnum] =~ m/^-TANDEM_DUPS_INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$TANDEM_DUPS_INPUT_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -TANDEM_DUPS_INPUT_FILE argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -TANDEM_DUPS_INPUT_FILE {PATH_TO_DIRECTORY}");
		}
#my $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE = undef;
	} elsif ( $ARGV[$argnum] =~ m/^-ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE {PATH_TO_DIRECTORY}");
		}
#my $PROT_FUSION_INPUT_FILE = undef;
	} elsif ( $ARGV[$argnum] =~ m/^-PROT_FUSION_INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$PROT_FUSION_INPUT_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -PROT_FUSION_INPUT_FILE argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -PROT_FUSION_INPUT_FILE {PATH_TO_DIRECTORY}");
		}
#my $CLOSEBESTMATCHS_INPUT_FILE = undef;
	} elsif ( $ARGV[$argnum] =~ m/^-CLOSEBESTMATCHS_INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$CLOSEBESTMATCHS_INPUT_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -CLOSEBESTMATCHS_INPUT_FILE argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -CLOSEBESTMATCHS_INPUT_FILE {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-REPLACEMENT_ALIGN_PARAM_PRIMARY_ID$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(\d+)$/i)
		{
			$REPLACEMENT_ALIGN_PARAM_PRIMARY_ID = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -REPLACEMENT_ALIGN_PARAM_PRIMARY_ID argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_ALIGN_PARAM_PRIMARY_ID {int}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-REPLACEMENT_ALIGNMENT_PRIMARY_ID$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(\d+)$/i)
		{
			$REPLACEMENT_ALIGNMENT_PRIMARY_ID = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -REPLACEMENT_ALIGNMENT_PRIMARY_ID argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_ALIGNMENT_PRIMARY_ID {int}");
		}
# REPLACEMENT_TANDEM_DUPS_PRIMARY_ID
	} elsif ( $ARGV[$argnum] =~ m/^-REPLACEMENT_TANDEM_DUPS_PRIMARY_ID$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(\d+)$/i)
		{
			$REPLACEMENT_TANDEM_DUPS_PRIMARY_ID = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -REPLACEMENT_TANDEM_DUPS_PRIMARY_ID argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_TANDEM_DUPS_PRIMARY_ID {int}");
		}
# REPLACEMENT_PROT_FUSION_PRIMARY_ID
	} elsif ( $ARGV[$argnum] =~ m/^-REPLACEMENT_PROT_FUSION_PRIMARY_ID$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(\d+)$/i)
		{
			$REPLACEMENT_PROT_FUSION_PRIMARY_ID = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -REPLACEMENT_PROT_FUSION_PRIMARY_ID argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_PROT_FUSION_PRIMARY_ID {int}");
		}
# REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID
	} elsif ( $ARGV[$argnum] =~ m/^-REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(\d+)$/i)
		{
			$REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID {int}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-CORE_OUTPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i )
		{
			$CORE_OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -CORE_OUTPUT_DIR argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -CORE_OUTPUT_DIR {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-MARK_TREATED_FILES_AS$/ ) {
		if (  $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$MARK_TREATED_FILES_AS = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -MARK_TREATED_FILES_AS argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -MARK_TREATED_FILES_AS {gzip | mv_todo_gzip}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-LOG_DIR$/ ) {
		if (  $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$LOG_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -LOG_DIR argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -LOG_DIR {PATH_TO_DIRECTORY}");
		}
# $CHECK_FIRST_LINE_SCORE_PARAMS = "ON";
	} elsif ( $ARGV[$argnum] =~ m/^-CHECK_FIRST_LINE_SCORE_PARAMS$/ ) {
		if (   $ARGV[ $argnum + 1 ] eq "ON"
			|| $ARGV[ $argnum + 1 ] eq "OFF" )
		{
			$CHECK_FIRST_LINE_SCORE_PARAMS = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -CHECK_FIRST_LINE_SCORE_PARAMS argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -CHECK_FIRST_LINE_SCORE_PARAMS = {ON, OFF}");
		}
	}
}

if ( $MARK_TREATED_FILES_AS =~ m/^gzip$/i || $MARK_TREATED_FILES_AS =~ m/^mv_todo_gzip$/i ) {
	#ok
} else {
	die_with_error_mssg("incorrect -MARK_TREATED_FILES_AS argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -MARK_TREATED_FILES_AS {gzip | mv_todo_gzip}");
}



if (! defined $CORE_INPUT_DIR) {
	die_with_error_mssg("Undefined CORE_INPUT_DIR ; usage : perl process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}");
}
if (! defined $ALIGN_PARAM_INPUT_FILE) {
	die_with_error_mssg("Undefined ALIGN_PARAM_INPUT_FILE ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -ALIGN_PARAM_INPUT_FILE {PATH_TO_DIRECTORY}");
} elsif ( -e "$ALIGNMENT_INPUT_FILE" ) {
}
if (! defined $ALIGNMENT_INPUT_FILE) {
	die_with_error_mssg("Undefined ALIGNMENT_INPUT_FILE ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -ALIGNMENT_INPUT_FILE {PATH_TO_DIRECTORY}");
}
if (! defined $ALIGN_PAIRS_INPUT_FILE) {
	die_with_error_mssg("Undefined ALIGN_PAIRS_INPUT_FILE ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -ALIGN_PAIRS_INPUT_FILE {PATH_TO_DIRECTORY}");
}
if (! defined $REPLACEMENT_ALIGN_PARAM_PRIMARY_ID) {
	die_with_error_mssg("Undefined REPLACEMENT_ALIGN_PARAM_PRIMARY_ID ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_ALIGN_PARAM_PRIMARY_ID {int}");
}
if (! defined $REPLACEMENT_ALIGNMENT_PRIMARY_ID) {
	die_with_error_mssg("Undefined REPLACEMENT_ALIGNMENT_PRIMARY_ID ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_ALIGNMENT_PRIMARY_ID {int}");
}
if (! defined $REPLACEMENT_TANDEM_DUPS_PRIMARY_ID) {
	die_with_error_mssg("Undefined REPLACEMENT_TANDEM_DUPS_PRIMARY_ID ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_TANDEM_DUPS_PRIMARY_ID {int}");
}
if (! defined $REPLACEMENT_PROT_FUSION_PRIMARY_ID) {
	die_with_error_mssg("Undefined REPLACEMENT_PROT_FUSION_PRIMARY_ID ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_PROT_FUSION_PRIMARY_ID {int}");
}
if (! defined $REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID) {
	die_with_error_mssg("Undefined REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID {int}");
}
if (! defined $CORE_OUTPUT_DIR) {
	die_with_error_mssg("Undefined CORE_OUTPUT_DIR ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -CORE_OUTPUT_DIR {PATH_TO_DIRECTORY}");
}


#parse file name
my $core_name_file = "";
my $master_elet_id;
my $sub_elet_id;
if ( $ALIGN_PARAM_INPUT_FILE =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table\.tsv(\.gz)?$/ ) {
	$master_elet_id = $1;
	$sub_elet_id = $2;
	$core_name_file = "${master_elet_id}_and_${sub_elet_id}";
} else {
	die_with_error_mssg("Could not parse core name in file $ALIGN_PARAM_INPUT_FILE\n");
}


if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
	#die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}

# correct global var if needed
$path_step_error_file = "$LOG_DIR/correct_primary_key_${core_name_file}.error";
$HOMOLOGIES_INPUT_FILE = "$CORE_INPUT_DIR/homologies/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_homologies_table.tsv";
if( -e "$HOMOLOGIES_INPUT_FILE" || -e "$HOMOLOGIES_INPUT_FILE\.gz" ) {
	# ok
} else {
	die_with_error_mssg("couldn't find associate file HOMOLOGIES_INPUT_FILE : $HOMOLOGIES_INPUT_FILE\n");
}
$params_scores_algo_syntenies_sql_file = "$CORE_OUTPUT_DIR/params_scores_algo_syntenies.sql";

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
`$SiteConfig::CMDDIR/mkdir -p $LOG_DIR/`;
#`$SiteConfig::CMDDIR/mkdir -p $LOG_DIR/tmp/launch_process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse_files/`;

#open( LOG, "$LOG_DIR/process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.log"); # | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print "\n---------------------------------------------------\n process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl started at :",  scalar(localtime),  "\n\n---------------------------------------------------\n" unless $VERBOSE eq "OFF";




=pod NOT needed anymore
# if .tsv file does not exists, check for .gz or .todo_gzip
if (! -e "$ALIGN_PARAM_INPUT_FILE" ) {
	if ( -e "$ALIGN_PARAM_INPUT_FILE\.gz" ) {
		`gunzip $ALIGN_PARAM_INPUT_FILE\.gz`;
	}
	if ( -e "$ALIGN_PARAM_INPUT_FILE\.todo_gzip" ) {
		`mv $ALIGN_PARAM_INPUT_FILE\.todo_gzip $ALIGN_PARAM_INPUT_FILE`;
	}
}
if (! -e "$ALIGNMENT_INPUT_FILE" ) {
	if ( -e "$ALIGNMENT_INPUT_FILE\.gz" ) {
		`gunzip $ALIGNMENT_INPUT_FILE\.gz`;
	}
	if ( -e "$ALIGNMENT_INPUT_FILE\.todo_gzip" ) {
		`mv $ALIGNMENT_INPUT_FILE\.todo_gzip $ALIGNMENT_INPUT_FILE`;
	}
}
if (! -e "$ALIGN_PAIRS_INPUT_FILE" ) {
	if ( -e "$ALIGN_PAIRS_INPUT_FILE\.gz" ) {
		`gunzip $ALIGN_PAIRS_INPUT_FILE\.gz`;
	}
	if ( -e "$ALIGN_PAIRS_INPUT_FILE\.todo_gzip" ) {
		`mv $ALIGN_PAIRS_INPUT_FILE\.todo_gzip $ALIGN_PAIRS_INPUT_FILE`;
	}
}
if (! -e "$HOMOLOGIES_INPUT_FILE" ) {
	if ( -e "$HOMOLOGIES_INPUT_FILE\.gz" ) {
		`gunzip $HOMOLOGIES_INPUT_FILE\.gz`;
	}
	if ( -e "$HOMOLOGIES_INPUT_FILE\.todo_gzip" ) {
		`mv $HOMOLOGIES_INPUT_FILE\.todo_gzip $HOMOLOGIES_INPUT_FILE`;
	}
}
=cut


# get associated file
#if( -e "$ALIGNMENT_INPUT_FILE" && -e "$ALIGN_PAIRS_INPUT_FILE" && -e "$HOMOLOGIES_INPUT_FILE" ) { # not needed anymore


sub compare_params_scores_IT_with_file {
	my ( $params_scores_IT ) = @_;
	my $MARKER_SAME_PARAMS_SCORES = 0;
	my $count_lines = 0;
	my $not_first_iteration = 0;
	while ( $count_lines != 5 ) {
		if ($not_first_iteration == 1 ) {
			sleep 1;
		}
		open(PARAM_SCORE_FILE, "< $params_scores_algo_syntenies_sql_file") || die_with_error_mssg("Can not open $params_scores_algo_syntenies_sql_file");
		#flock(PARAM_SCORE_FILE, 1) || die_with_error_mssg("Can not flock shared $params_scores_algo_syntenies_sql_file");
		$MARKER_SAME_PARAMS_SCORES = 0;
		$count_lines = 0;
		while( my $line = <PARAM_SCORE_FILE>) {
			$count_lines++;
			chomp($line);
			$line =~ s/^\s+//;
			$line =~ s/\s+$//;
			if ($line =~ m/^${params_scores_IT}$/) {
				$MARKER_SAME_PARAMS_SCORES = 1;
			}
		}
		close(PARAM_SCORE_FILE);
		$not_first_iteration = 1;
	}
	if ($MARKER_SAME_PARAMS_SCORES == 0) {
		die_with_error_mssg("Error checking against existing params_scores_algo_syntenies : The file $params_scores_algo_syntenies_sql_file does not contain ${params_scores_IT}");
	}
}

# deal with alignment_param_file
if ($ALIGN_PARAM_INPUT_FILE =~ m/^(.+)\.gz$/ ) {
	if ( -e "$ALIGN_PARAM_INPUT_FILE") { # file can be already gunzip if re-run script after error
		`gunzip $ALIGN_PARAM_INPUT_FILE`;
	}
	$ALIGN_PARAM_INPUT_FILE = $1;
}
open ( $in,  '<',  $ALIGN_PARAM_INPUT_FILE ) or do {
    die_with_error_mssg("Can't read ALIGN_PARAM_INPUT_FILE file $ALIGN_PARAM_INPUT_FILE : $!");
};
`$SiteConfig::CMDDIR/mkdir -p $CORE_OUTPUT_DIR/alignment_params/${molecule_type}_${master_elet_id}/`;
open ( $out, '>', "$CORE_OUTPUT_DIR/alignment_params/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_params_table.sql" ) or do {
    die_with_error_mssg("Can't write $CORE_OUTPUT_DIR/alignment_params/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_params_table.sql : $!");
};

while( my $line = <$in> ) {
	chomp($line);
	$line =~ s/^\s+//;
	$line =~ s/\s+$//;
	#my @split = split( '\s+', $line );
=pod
	if ( $line =~ m/^BEGIN WORK;$/ 
		|| $line =~ m/^\\\.$/ 
		|| $line =~ m/^COMMIT WORK;$/ 
	) {
=cut
	if ( $line eq "BEGIN WORK;" 
		|| $line eq "\\\." 
		|| $line eq "COMMIT WORK;" 
	) {
		print $out "$line\n";
	} elsif ( $line eq "COPY alignment_params \(alignment_param_id, q_organism_id, q_element_id, s_organism_id, s_element_id, ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, max_gap_size_for_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac, geneFusions_minPctLenghtProtThres, closeBestMatchs_minPctThres, tandemDups_minPctWithinBestPidThres\) FROM stdin;" ) {
		#do not store info on params_scores_algo_syntenies, has its own table
		print $out "COPY alignment_params (alignment_param_id, q_organism_id, q_element_id, s_organism_id, s_element_id) FROM stdin;\n";
	#} elsif ($line =~ m/^([\w_]+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([TF])\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)\t([\-\d\.]+)$/ ) {
	} elsif ($line =~ m/^([\w_]+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t([\-\d\.\sTF]+)$/ ) {
		my $alignment_param_id_IT = $1;
		my $q_organism_id_IT = $2;
		my $q_element_id_IT = $3;
		my $s_organism_id_IT = $4;
		my $s_element_id_IT = $5;
=pod
		my $ortho_score_IT = $6;
		my $homo_score_IT = $7;
		my $mismatch_penalty_IT = $8;
		my $gap_creation_penalty_IT = $9;
		my $max_gap_size_for_creation_penalty_IT = $10;
		my $gap_extension_penalty_IT = $11;
		my $min_align_size_IT = $12;
		my $min_score_IT = $13;
		my $orthologs_included_IT = $14;
		my $ortho_min_prot_frac_IT = $15;
		my $geneFusions_minPctLenghtProtThres_IT = $16;
		my $closeBestMatchs_minPctThres_IT = $17;
		my $tandemDups_minPctWithinBestPidThres_IT = $18;
=cut

		if ($CHECK_FIRST_LINE_SCORE_PARAMS eq "ON") {
			my $params_scores_IT = $6;
			# file locking properly http://www.perlmonks.org/?node_id=7058
			if ( -e "$params_scores_algo_syntenies_sql_file" ) {
				compare_params_scores_IT_with_file($params_scores_IT);
			} else {
				open( PARAM_SCORE_FILE, "> $params_scores_algo_syntenies_sql_file" ) or die_with_error_mssg("Can not open $params_scores_algo_syntenies_sql_file");
				flock(PARAM_SCORE_FILE, 2) || die_with_error_mssg("Can not exclusive flock $params_scores_algo_syntenies_sql_file.tmp"); # exclusive lock
				my $count_lines = 0;
				while( my $line = <PARAM_SCORE_FILE>) {
					$count_lines++;
				}
				if ( $count_lines == 0 ) {
					# create file
					print PARAM_SCORE_FILE "BEGIN WORK;\n";
					print PARAM_SCORE_FILE "COPY params_scores_algo_syntenies (ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, max_gap_size_for_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac, geneFusions_minPctLenghtProtThres, closeBestMatchs_minPctThres, tandemDups_minPctWithinBestPidThres) FROM stdin;\n";
					print PARAM_SCORE_FILE "$params_scores_IT\n";
					print PARAM_SCORE_FILE "\\.\n";
					print PARAM_SCORE_FILE "COMMIT WORK;\n";
					close(PARAM_SCORE_FILE);

				} else {
					close(PARAM_SCORE_FILE);
					compare_params_scores_IT_with_file($params_scores_IT);
=pod
					open(PARAM_SCORE_FILE, "< $params_scores_algo_syntenies_sql_file") || die_with_error_mssg("Can not open $params_scores_algo_syntenies_sql_file");
					flock(PARAM_SCORE_FILE, 1) || die_with_error_mssg("Can not flock shared $params_scores_algo_syntenies_sql_file"); # shared lock
					my $MARKER_SAME_PARAMS_SCORES = 0;
					while( my $line = <PARAM_SCORE_FILE>) {
						chomp($line);
						$line =~ s/^\s+//;
						$line =~ s/\s+$//;
						if ($line =~ m/^${params_scores_IT}$/) {
							$MARKER_SAME_PARAMS_SCORES = 1;
						}
					}
					close(PARAM_SCORE_FILE);
					if ($MARKER_SAME_PARAMS_SCORES == 0) {
						die_with_error_mssg("Error checking for params_scores_algo_syntenies : The file $params_scores_algo_syntenies_sql_file does not contain ${params_scores_IT}");
					}
=cut
				}
			}
			$CHECK_FIRST_LINE_SCORE_PARAMS = "OFF";
		}
		if ( exists $TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT {$alignment_param_id_IT}) {
			# redondance, die
			die_with_error_mssg("Error in file $ALIGN_PARAM_INPUT_FILE\n Duplicated entry $alignment_param_id_IT.\n");
		} else {
			$TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT {$alignment_param_id_IT} = $REPLACEMENT_ALIGN_PARAM_PRIMARY_ID ;
			#$_ =~ s/${TMP_ALIGN_PARAM_PRIMARY_ID}/${REPLACEMENT_ALIGN_PARAM_PRIMARY_ID}/;
	    		#print $out $_;
			print $out "${REPLACEMENT_ALIGN_PARAM_PRIMARY_ID}\t$q_organism_id_IT\t$q_element_id_IT\t$s_organism_id_IT\t$s_element_id_IT\n";
			$REPLACEMENT_ALIGN_PARAM_PRIMARY_ID++;
		}
	}else{
		die_with_error_mssg("error parsing line\n$line\n in file $ALIGN_PARAM_INPUT_FILE : does not match the main regex\n");
	}

}
close $out;
close $in;

# deal with alignments file
if ($ALIGNMENT_INPUT_FILE =~ m/^(.+)\.gz$/ ) {
	if ( -e "$ALIGNMENT_INPUT_FILE") { # file can be already gunzip if re-run script after error
		`gunzip $ALIGNMENT_INPUT_FILE`;
	}
	$ALIGNMENT_INPUT_FILE = $1;
}
open  ($in,  '<',  $ALIGNMENT_INPUT_FILE ) or do {
    die_with_error_mssg("Can't read ALIGNMENT_INPUT_FILE file $ALIGNMENT_INPUT_FILE : $!");
};	
`$SiteConfig::CMDDIR/mkdir -p $CORE_OUTPUT_DIR/alignments/${molecule_type}_${master_elet_id}/`;
open ( $out, '>', "$CORE_OUTPUT_DIR/alignments/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql" ) or do {
    die_with_error_mssg("Can't write $CORE_OUTPUT_DIR/alignments/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_table.sql : $!");
};
while( my $line = <$in> ) {
	chomp($line);
	$line =~ s/^\s+//;
	$line =~ s/\s+$//;
	if ( $line eq "BEGIN WORK;" 
		|| $line eq "\\\." 
		|| $line eq "COMMIT WORK;" 
	) {
	    	print $out "$line\n";
	} elsif ( $line eq "COPY alignments \(alignment_id, alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_size_kb, s_size_kb, q_start, q_stop, s_start, s_stop\) FROM stdin;"
	){
	    	print $out "$line\n";
	} elsif ( $line =~ m/^(\d+)\t([\w_]+)\t(\d+\t\d+\t[TF]\t\d+\t\d+\t\d+\t\d+\t\d+\t[\d\.]+\t[\d\.]+\t\d+\t\d+\t\d+\t\d+)$/ ) {
		my $TMP_ALIGN_PRIMARY_ID = $1;
		my $TMP_ALIGN_PARAM_PRIMARY_ID = $2;
		my $unchanged_portion = $3;

		# deal with ALIGN_PRIMARY_ID
		$TMP_ALIGN_PRIMARY_ID_TO_PERM_REPLACEMENT {$TMP_ALIGN_PRIMARY_ID} = $REPLACEMENT_ALIGNMENT_PRIMARY_ID ;
		#$line =~ s/${TMP_ALIGN_PRIMARY_ID}/${REPLACEMENT_ALIGNMENT_PRIMARY_ID}/;

		# deal with ALIGN_PARAM_PRIMARY_ID
		if ( exists $TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT {$TMP_ALIGN_PARAM_PRIMARY_ID}) {
			my $replacement_ID_IT = $TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT {$TMP_ALIGN_PARAM_PRIMARY_ID};
			#$line =~ s/${TMP_ALIGN_PARAM_PRIMARY_ID}/${replacement_ID_IT}/;
			# print line
		    	print $out "$REPLACEMENT_ALIGNMENT_PRIMARY_ID\t$replacement_ID_IT\t$unchanged_portion\n";
			$REPLACEMENT_ALIGNMENT_PRIMARY_ID++;
		} else {
			die_with_error_mssg("key TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT{ TMP_ALIGN_PARAM_PRIMARY_ID } does not exists for TMP_ALIGN_PARAM_PRIMARY_ID = TMP_ALIGN_PARAM_PRIMARY_ID.");
		}
	}else{
		die_with_error_mssg("error parsing line\n$line\n in file $ALIGNMENT_INPUT_FILE.\n");
	}
}
close $out;
close $in;


# deal with alignment_pairs_table file
if ($ALIGN_PAIRS_INPUT_FILE =~ m/^(.+)\.gz$/ ) {
	if ( -e "$ALIGN_PAIRS_INPUT_FILE") { # file can be already gunzip if re-run script after error
		`gunzip $ALIGN_PAIRS_INPUT_FILE`;
	}
	$ALIGN_PAIRS_INPUT_FILE = $1;
}
open ( $in,  '<',  $ALIGN_PAIRS_INPUT_FILE ) or do {
    die_with_error_mssg("Can't read $ALIGN_PAIRS_INPUT_FILE : $!");
};
`$SiteConfig::CMDDIR/mkdir -p $CORE_OUTPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/`;
open ( $out, '>', "$CORE_OUTPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_pairs_table.sql" ) or do {
    die_with_error_mssg("Can't write $CORE_OUTPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_pairs_table.sql : $!");
};
while( my $line = <$in> ) {
	chomp($line);
	$line =~ s/^\s+//;
	$line =~ s/\s+$//;
	if ( $line eq "BEGIN WORK;" 
		|| $line eq "\\\." 
		|| $line eq "COMMIT WORK;" 
	) {
	    	print $out "$line\n";
	} elsif ( $line eq "COPY alignment_pairs \(alignment_id, q_gene_id, s_gene_id, type\) FROM stdin;" ){
		print $out "$line\n";
	} elsif ( $line =~ m/^(\d+)\t([-\d]+)\t([-\d]+)\t(\d+)$/ ) {
		if (exists $TMP_ALIGN_PRIMARY_ID_TO_PERM_REPLACEMENT{ $1 }) {
			my $replacement_alignment_id = $TMP_ALIGN_PRIMARY_ID_TO_PERM_REPLACEMENT{ $1 };
			my $replacement_q_gene_id = $2;
			my $replacement_s_gene_id = $3;
			my $replacement_type = $4;
			#$line =~ s/$1/$replac/;
			#if($line =~ m/-/){ $line =~ s/-/\\N/g; }
			if($replacement_q_gene_id eq "-" || $replacement_q_gene_id <= 0){
				#if($replacement_type == 4){
					$replacement_q_gene_id = "\\N";
				#} else {
				#	die_with_error_mssg("failed checking for replacement_q_gene_id in deal with alignment_pairs_table file. Pb with line:\n$line\n");
				#}	
			}
			if($replacement_s_gene_id eq "-" || $replacement_s_gene_id <= 0){
				#if($4 == 5){
					$replacement_s_gene_id = "\\N";
				#} else {
				#	die_with_error_mssg("failed checking for replacement_s_gene_id in deal with alignment_pairs_table file. Pb with line:\n$line\n");
				#}	
			}
			my $modified_line = "${replacement_alignment_id}\t${replacement_q_gene_id}\t${replacement_s_gene_id}\t${replacement_type}\n";
			print $out $modified_line;
		#} elsif ($master_elet_id == $sub_elet_id) {
		    #can happen as we do not duplicate forward - reverse for element against itself
		} else {
			die_with_error_mssg("key TMP_ALIGN_PRIMARY_ID_TO_PERM_REPLACEMENT{ dollar1 } does not exists for dollar1 = $1.\n");
		}	
	}else{
		die_with_error_mssg("error parsing line\n$line\n in file $ALIGN_PAIRS_INPUT_FILE.\n");
	}
}
close $out;
close $in;


# HOMOLOGIES_INPUT_FILE
if (! -e "$HOMOLOGIES_INPUT_FILE" ) {
	if ( -e "$HOMOLOGIES_INPUT_FILE\.gz" ) {
		`gunzip $HOMOLOGIES_INPUT_FILE\.gz`;
	}
}
if (! -e "$HOMOLOGIES_INPUT_FILE" ) {
    die_with_error_mssg("$HOMOLOGIES_INPUT_FILE does not exists.");
}
my $output_duplicates_lines_in_file = `sort $HOMOLOGIES_INPUT_FILE | uniq -d`;
my @array_output_duplicates_lines_in_file = split("\n",$output_duplicates_lines_in_file);
my %hash_duplicates_lines_in_file            = ();
foreach my $line_duplicate_IT (@array_output_duplicates_lines_in_file) {
	$hash_duplicates_lines_in_file{$line_duplicate_IT} = 0;
}
open ( $in, '<', $HOMOLOGIES_INPUT_FILE ) or do {
    die_with_error_mssg("Can't read $HOMOLOGIES_INPUT_FILE : $!");
};
`$SiteConfig::CMDDIR/mkdir -p $CORE_OUTPUT_DIR/homologies/${molecule_type}_${master_elet_id}/`;
open ( $out, '>', "$CORE_OUTPUT_DIR/homologies/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_homologies_table.sql" ) or do {
    die_with_error_mssg("Can't write $CORE_OUTPUT_DIR/homologies/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_homologies_table.sql : $!");
};
while( my $line = <$in> ) {
	chomp($line);
	#$line =~ s/^\s+//;
	#$line =~ s/\s+$//;
	#my @split = split( '\s+', $line );
	if( exists $hash_duplicates_lines_in_file{$line} ){
		if($hash_duplicates_lines_in_file{$line} > 0){
			#already printed, do not print a second time
		}else{
			#not already printed
			print $out "$line\n";
			$hash_duplicates_lines_in_file{$line} = 1;
		}
	} else {
		print $out "$line\n";
	}
}
close $out;
close $in;


# -REPLACEMENT_TANDEM_DUPS_PRIMARY_ID
# -TANDEM_DUPS_INPUT_FILE 
	# -> tandem_dups_id BY REPLACEMENT_TANDEM_DUPS_PRIMARY_ID
	# -> alignment_param_id <-> ALIGN_PARAM_PRIMARY_ID
	# "COPY tandem_dups (tandem_dups_id, alignment_param_id, gene_id_single_is_q, single_organims_id, single_element_id, single_gene_id, tandem_organims_id, tandem_element_id, tandem_gene_id, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, idx_in_dup) FROM stdin;\n"
if ( defined $TANDEM_DUPS_INPUT_FILE ) {
	if ($TANDEM_DUPS_INPUT_FILE =~ m/^(.+)\.gz$/ ) {
		if ( -e "$TANDEM_DUPS_INPUT_FILE") { # file can be already gunzip if re-run script after error
			`gunzip $TANDEM_DUPS_INPUT_FILE`;
		}
		$TANDEM_DUPS_INPUT_FILE = $1;
	}
	open ( $in,  '<',  $TANDEM_DUPS_INPUT_FILE ) or do {
	    die_with_error_mssg("Can't read TANDEM_DUPS_INPUT_FILE file $TANDEM_DUPS_INPUT_FILE : $!");
	};
	`$SiteConfig::CMDDIR/mkdir -p $CORE_OUTPUT_DIR/tandem_dups/${molecule_type}_${master_elet_id}/`;
	open ( $out, '>', "$CORE_OUTPUT_DIR/tandem_dups/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_tandem_dups_table.sql" ) or do {
	    die_with_error_mssg("Can't write $CORE_OUTPUT_DIR/tandem_dups/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_tandem_dups_table.sql : $!");
	};
	while( my $line = <$in> ) {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		#my @split = split( '\s+', $line );
		if ( $line eq "BEGIN WORK;" 
			|| $line eq "\\\." 
			|| $line eq "COMMIT WORK;" 
		) {
		    	print $out "$line\n";
		} elsif ( $line eq "COPY tandem_dups \(tandem_dups_id, alignment_param_id, gene_id_single_is_q, single_organims_id, single_element_id, single_gene_id, tandem_organims_id, tandem_element_id, tandem_gene_id, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, idx_in_dup\) FROM stdin;" ) {
			print $out "$line\n";
		} elsif ($line =~ m/^(\d+)\t([\w_]+)\t(.+)$/ ) {
			my $tmp_tandem_dups_primary_id = $1;
			my $TMP_ALIGN_PARAM_PRIMARY_ID = $2;
			my $unchanged_portion = $3;
			if ( exists $TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT {$TMP_ALIGN_PARAM_PRIMARY_ID}) {
				my $replacement_align_param_ID_IT = $TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT {$TMP_ALIGN_PARAM_PRIMARY_ID};
				if (exists $TMP_TANDEMDUPS_PRIMARY_ID_TO_PERM_REPLACEMENT {$tmp_tandem_dups_primary_id} ) {
					my $perm_repla_tandem_dups_stored = $TMP_TANDEMDUPS_PRIMARY_ID_TO_PERM_REPLACEMENT {$tmp_tandem_dups_primary_id};
					print $out "$perm_repla_tandem_dups_stored\t$replacement_align_param_ID_IT\t$unchanged_portion\n";
				} else {
					print $out "$REPLACEMENT_TANDEM_DUPS_PRIMARY_ID\t$replacement_align_param_ID_IT\t$unchanged_portion\n";
					$TMP_TANDEMDUPS_PRIMARY_ID_TO_PERM_REPLACEMENT {$tmp_tandem_dups_primary_id} = $REPLACEMENT_TANDEM_DUPS_PRIMARY_ID;
					$REPLACEMENT_TANDEM_DUPS_PRIMARY_ID++;
				}
			} else {
				die_with_error_mssg("key TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT{ TMP_ALIGN_PARAM_PRIMARY_ID } does not exists for TMP_ALIGN_PARAM_PRIMARY_ID = TMP_ALIGN_PARAM_PRIMARY_ID.");
			}
		}else{
			die_with_error_mssg("error parsing line\n$line\n in file $TANDEM_DUPS_INPUT_FILE : does not match the main regex\n");
		}
	}
	close $out;
	close $in;
}

# -ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE
	# -> alignment_id_branched <-> ALIGNMENT_PRIMARY_ID
	# -> alignment_param_id <-> ALIGN_PARAM_PRIMARY_ID
	# COPY isBranchedToAnotherSynteny (alignment_id_branched, alignment_param_id, branche_on_q_gene_id, branche_on_s_gene_id) FROM stdin;\n";
if ( defined $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE ) {
	if ($ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE =~ m/^(.+)\.gz$/ ) {
		if ( -e "$ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE") { # file can be already gunzip if re-run script after error
			`gunzip $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE`;
		}
		$ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE = $1;
	}
	open ( $in,  '<',  $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE ) or do {
	    die_with_error_mssg("Can't read ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE file $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE : $!");
	};
	`$SiteConfig::CMDDIR/mkdir -p $CORE_OUTPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_${master_elet_id}/`;
	open ( $out, '>', "$CORE_OUTPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_isBranchedToAnotherSynteny_table.sql" ) or do {
	    die_with_error_mssg("Can't write $CORE_OUTPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_isBranchedToAnotherSynteny_table.sql : $!");
	};
	while( my $line = <$in> ) {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		#my @split = split( '\s+', $line );
		if ( $line eq "BEGIN WORK;" 
			|| $line eq "\\\." 
			|| $line eq "COMMIT WORK;" 
		) {
		    	print $out "$line\n";
		} elsif ( $line eq "COPY isBranchedToAnotherSynteny \(alignment_id_branched, alignment_param_id, branche_on_q_gene_id, branche_on_s_gene_id\) FROM stdin;" ) {
			print $out "$line\n";
		} elsif ($line =~ m/^(\d+)\t([\w_]+)\t(.+)$/ ) {
			my $TMP_ALIGN_PRIMARY_ID = $1;
			my $TMP_ALIGN_PARAM_PRIMARY_ID = $2;
			my $unchanged_portion = $3;
			if ( exists $TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT {$TMP_ALIGN_PARAM_PRIMARY_ID}) {
				my $replacement_ALIGN_PARAM_PRIMARY_ID_IT = $TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT {$TMP_ALIGN_PARAM_PRIMARY_ID};
				if ( exists $TMP_ALIGN_PRIMARY_ID_TO_PERM_REPLACEMENT {$TMP_ALIGN_PRIMARY_ID}) {
					my $replacement_ALIGN_PRIMARY_ID_IT = $TMP_ALIGN_PRIMARY_ID_TO_PERM_REPLACEMENT {$TMP_ALIGN_PRIMARY_ID};
				    	print $out "$replacement_ALIGN_PRIMARY_ID_IT\t$replacement_ALIGN_PARAM_PRIMARY_ID_IT\t$unchanged_portion\n";
				} else {
					die_with_error_mssg("key TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT{ TMP_ALIGN_PARAM_PRIMARY_ID } does not exists for TMP_ALIGN_PARAM_PRIMARY_ID = TMP_ALIGN_PARAM_PRIMARY_ID.");
				}
			} else {
				die_with_error_mssg("key TMP_ALIGN_PARAM_PRIMARY_ID_TO_PERM_REPLACEMENT{ TMP_ALIGN_PARAM_PRIMARY_ID } does not exists for TMP_ALIGN_PARAM_PRIMARY_ID = TMP_ALIGN_PARAM_PRIMARY_ID.");
			}
		}else{
			die_with_error_mssg("error parsing line\n$line\n in file $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE : does not match the main regex\n");
		}
	}
	close $out;
	close $in;
}

# -REPLACEMENT_PROT_FUSION_PRIMARY_ID
# -PROT_FUSION_INPUT_FILE
	# -> prot_fusion_id BY REPLACEMENT_PROT_FUSION_PRIMARY_ID
	# COPY prot_fusion ("prot_fusion_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, isMirrorData, s_rank_among_all_s_fusions, lengthTotalCoverageSOnQ, lenghtNewContribCoverageSOnQ, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, geneIdBestSingleLongMatchHidingProtFusion, numberSingleLongMatchHidingProtFusion, numberProtFusionDetectedBeforeThisOne
if ( defined $PROT_FUSION_INPUT_FILE ) {
	if ($PROT_FUSION_INPUT_FILE =~ m/^(.+)\.gz$/ ) {
		if ( -e "$PROT_FUSION_INPUT_FILE") { # file can be already gunzip if re-run script after error
			`gunzip $PROT_FUSION_INPUT_FILE`;
		}
		$PROT_FUSION_INPUT_FILE = $1;
	}
	open ( $in,  '<',  $PROT_FUSION_INPUT_FILE ) or do {
	    die_with_error_mssg("Can't read PROT_FUSION_INPUT_FILE file $PROT_FUSION_INPUT_FILE : $!");
	};
	`$SiteConfig::CMDDIR/mkdir -p $CORE_OUTPUT_DIR/protFusion/${molecule_type}_${master_elet_id}/`;
	open ( $out, '>', "$CORE_OUTPUT_DIR/protFusion/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_protFusion_table.sql" ) or do {
	    die_with_error_mssg("Can't write $CORE_OUTPUT_DIR/protFusion/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_protFusion_table.sql : $!");
	};
	while( my $line = <$in> ) {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		#my @split = split( '\s+', $line );
		if ( $line eq "BEGIN WORK;" 
			|| $line eq "\\\." 
			|| $line eq "COMMIT WORK;" 
		) {
			print $out "$line\n";
		} elsif ( $line eq "COPY prot_fusion (prot_fusion_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, isMirrorData, s_rank_among_all_s_fusions, lengthTotalCoverageSOnQ, lenghtNewContribCoverageSOnQ, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, geneIdBestSingleLongMatchHidingProtFusion, numberSingleLongMatchHidingProtFusion, numberProtFusionDetectedBeforeThisOne) FROM stdin;" ) {
			print $out "$line\n";
		} elsif ($line =~ m/^(\d+)\t(.+)$/ ) {
			my $tmp_prot_fusion_primary_id = $1;
			my $unchanged_portion = $2;
			if (exists $TMP_PROTFUS_PRIMARY_ID_TO_PERM_REPLACEMENT {$tmp_prot_fusion_primary_id} ) {
				my $perm_repla_protfus_stored = $TMP_PROTFUS_PRIMARY_ID_TO_PERM_REPLACEMENT {$tmp_prot_fusion_primary_id};
				print $out "$perm_repla_protfus_stored\t$unchanged_portion\n";
			} else {
				print $out "$REPLACEMENT_PROT_FUSION_PRIMARY_ID\t$unchanged_portion\n";
				$TMP_PROTFUS_PRIMARY_ID_TO_PERM_REPLACEMENT {$tmp_prot_fusion_primary_id} = $REPLACEMENT_PROT_FUSION_PRIMARY_ID;
				$REPLACEMENT_PROT_FUSION_PRIMARY_ID++;
			} 
		}else{
			die_with_error_mssg("error parsing line\n$line\n in file $PROT_FUSION_INPUT_FILE : does not match the main regex\n");
		}
	}
	close $out;
	close $in;
}


# -REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID
# -CLOSEBESTMATCHS_INPUT_FILE
	# -> close_best_match_id BY REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID
if ( defined $CLOSEBESTMATCHS_INPUT_FILE ) {
	if ($CLOSEBESTMATCHS_INPUT_FILE =~ m/^(.+)\.gz$/ ) {
		if ( -e "$CLOSEBESTMATCHS_INPUT_FILE") { # file can be already gunzip if re-run script after error
			`gunzip $CLOSEBESTMATCHS_INPUT_FILE`;
		}
		$CLOSEBESTMATCHS_INPUT_FILE = $1;
	}
	open ( $in,  '<',  $CLOSEBESTMATCHS_INPUT_FILE ) or do {
	    die_with_error_mssg("Can't read CLOSEBESTMATCHS_INPUT_FILE file $CLOSEBESTMATCHS_INPUT_FILE : $!");
	};
	`$SiteConfig::CMDDIR/mkdir -p $CORE_OUTPUT_DIR/closeBestMatchs/${molecule_type}_${master_elet_id}/`;
	open ( $out, '>', "$CORE_OUTPUT_DIR/closeBestMatchs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_closeBestMatchs_table.sql" ) or do {
	    die_with_error_mssg("Can't write $CORE_OUTPUT_DIR/closeBestMatchs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_closeBestMatchs_table.sql : $!");
	};
	while( my $line = <$in> ) {
		chomp($line);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		#my @split = split( '\s+', $line );
		if ( $line eq "BEGIN WORK;" 
			|| $line eq "\\\." 
			|| $line eq "COMMIT WORK;" 
		) {
			print $out "$line\n";
		} elsif ( $line eq "COPY close_best_match (close_best_match_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, is_mirror_data, number_of_close_matchs, contain_bdbh, pid_best_match, evalue_best_match, bits_best_match, list_close_match_gene_ids) FROM stdin;" ) {
			print $out "$line\n";
		} elsif ($line =~ m/^\d+\t(.+)$/ ) {
			my $unchanged_portion = $1;
			print $out "$REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID\t$unchanged_portion\n";
			$REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID++;
		}else{
			die_with_error_mssg("error parsing line\n$line\n in file $CLOSEBESTMATCHS_INPUT_FILE : does not match the main regex\n");
		}
	}
	close $out;
	close $in;
}

#}else{
#	die_with_error_mssg("could not associate file $ALIGNMENT_INPUT_FILE or $ALIGN_PAIRS_INPUT_FILE or $HOMOLOGIES_INPUT_FILE.\n");
#}


# gzip all input files
if ( $MARK_TREATED_FILES_AS =~ m/^gzip$/i ) {
	`gzip $ALIGN_PARAM_INPUT_FILE`;
	`gzip $ALIGNMENT_INPUT_FILE`;
	`gzip $ALIGN_PAIRS_INPUT_FILE`;
	`gzip $HOMOLOGIES_INPUT_FILE`;
	if ( defined $TANDEM_DUPS_INPUT_FILE ) {
		`gzip $TANDEM_DUPS_INPUT_FILE`;
	}
	if ( defined $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE ) {
		`gzip $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE`;
	}
	if ( defined $PROT_FUSION_INPUT_FILE ) {
		`gzip $PROT_FUSION_INPUT_FILE`;
	}
	if ( defined $CLOSEBESTMATCHS_INPUT_FILE ) {
		`gzip $CLOSEBESTMATCHS_INPUT_FILE`;
	}
} elsif ($MARK_TREATED_FILES_AS =~ m/^mv_todo_gzip$/i) {
	`mv $ALIGN_PARAM_INPUT_FILE $ALIGN_PARAM_INPUT_FILE\.todo_gzip`;
	`mv $ALIGNMENT_INPUT_FILE $ALIGNMENT_INPUT_FILE\.todo_gzip`;
	`mv $ALIGN_PAIRS_INPUT_FILE $ALIGN_PAIRS_INPUT_FILE\.todo_gzip`;
	`mv $HOMOLOGIES_INPUT_FILE $HOMOLOGIES_INPUT_FILE\.todo_gzip`;
	if ( defined $TANDEM_DUPS_INPUT_FILE ) {
		`mv $TANDEM_DUPS_INPUT_FILE $TANDEM_DUPS_INPUT_FILE\.todo_gzip`;
	}
	if ( defined $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE ) {
		`mv $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE $ISBRANCHEDTOANOTHERSYNTENY_INPUT_FILE\.todo_gzip`;
	}
	if ( defined $PROT_FUSION_INPUT_FILE ) {
		`mv $PROT_FUSION_INPUT_FILE $PROT_FUSION_INPUT_FILE\.todo_gzip`;
	}
	if ( defined $CLOSEBESTMATCHS_INPUT_FILE ) {
		`mv $CLOSEBESTMATCHS_INPUT_FILE $CLOSEBESTMATCHS_INPUT_FILE\.todo_gzip`;
	}
} else {
	die_with_error_mssg("incorrect -MARK_TREATED_FILES_AS argument ; usage : perl process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl -MARK_TREATED_FILES_AS {gzip | mv_todo_gzip}");
}


#`touch $LOG_DIR/launch_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse_${core_name_file}.done`; taken care of by bash file ll


#end of script
print "\n---------------------------------------------------\n\n\n process_Task_add_alignment_parse_tsv_output_Replacing_Forward_Reverse.pl successfully completed at :",  scalar(localtime), "\n\n---------------------------------------------------\n" unless $VERBOSE eq "OFF";
#close(LOG);


