#!/usr/local/bin/perl
#
# perl List_check_and_split_genomes_files.pl
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use Time::HiRes qw(sleep);
use SiteConfig;
use POSIX ":sys_wait_h";
use Term::ReadKey;
use BlastConfig;
use File::Basename;

# whole script scoped variable
my @bankdir         = ();
my $output_backtick = "";
my $output_cmd_grep = "";
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $TREAT_TXT_FILE_AS = "";
my %genome_files_to_do_individual_checking  = ();
my $VERBOSE = "ON";


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $SiteConfig::LOGDIR/check_and_prepare_genomes_files.error`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $SiteConfig::LOGDIR/check_and_prepare_genomes_files.error :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $SiteConfig::LOGDIR/check_and_prepare_genomes_files.error :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}

foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-TREAT_TXT_FILE_AS$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^embl$/i ) {
			$TREAT_TXT_FILE_AS = "embl";
		} elsif (   $ARGV[ $argnum + 1 ] =~ m/^gbk$/i ) {
			$TREAT_TXT_FILE_AS = "gbk";
		} else {
			die_with_error_mssg("incorrect -TREAT_TXT_FILE_AS argument ; usage : perl List_check_and_split_genomes_files.pl -TREAT_TXT_FILE_AS {embl, gbk}");
			#print LOG "incorrect -TREAT_TXT_FILE_AS argument ; usage : perl List_check_and_split_genomes_files.pl -TREAT_TXT_FILE_AS {embl, gbk}";
			#die "incorrect -TREAT_TXT_FILE_AS argument ; usage : perl List_check_and_split_genomes_files.pl -TREAT_TXT_FILE_AS {embl, gbk}";
		}
	} elsif ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i ) {
			#ok
		} elsif ( $ARGV[ $argnum + 1 ] =~ m/^OFF$/i ) {
			$VERBOSE = "OFF";
		} else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl List_check_and_split_genomes_files.pl -VERBOSE {ON, OFF}");
			#print LOG "incorrect -VERBOSE argument ; usage : perl List_check_and_split_genomes_files.pl -VERBOSE {ON, OFF}";
			#die "incorrect -VERBOSE argument ; usage : perl List_check_and_split_genomes_files.pl -VERBOSE {ON, OFF}";
		}
	}
}

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log 
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Add_entries/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG, ">$SiteConfig::LOGDIR/Add_entries/List_check_and_split_genomes_files.log" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG
"\n---------------------------------------------------\nList_check_and_split_genomes_files.pl started at :",
  scalar(localtime), "\n" unless ($VERBOSE eq "OFF");


# rm marker file done script
$output_backtick .= `rm -f $SiteConfig::LOGDIR/check_and_prepare_genomes_files.done`;
$output_backtick .= `rm -f $SiteConfig::LOGDIR/check_and_prepare_genomes_files.error`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $SiteConfig::LOGDIR/check_and_prepare_genomes_files.done or .error :\n$output_backtick \n $!");
	#print LOG "Error could not complete rm -f $SiteConfig::LOGDIR/check_and_prepare_genomes_files.done or .error :\n$output_backtick \n $! \n";#
	#die("Error could not complete rm -f $SiteConfig::LOGDIR/check_and_prepare_genomes_files.done or .error :\n$output_backtick \n $! \n");#
}




if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/ ) {
 	# ok continue
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	if ($BlastConfig::number_clusters_organism_best_adjust > 0 || $BlastConfig::avg_number_proteines_per_cluster_best_adjust > 0) {
		# ok continue
	} else {
		die_with_error_mssg("The option BLAST_TAXO_GRANULARITY CLUSTER_ORGANISM was detected but neither number_clusters_organism_best_adjust or avg_number_proteines_per_cluster_best_adjust are set, please modify your BlastConfig.pm file accordingly");
	}
} else {
	die_with_error_mssg("This option of BLAST_TAXO_GRANULARITY ($BLAST_TAXO_GRANULARITY) is not yet supported, please modify your BlastConfig.pm file accordingly");
	#print LOG "This option of BLAST_TAXO_GRANULARITY ($BLAST_TAXO_GRANULARITY) is not yet supported, please modify your BlastConfig.pm file accordingly\n";
	#die ("This option of BLAST_TAXO_GRANULARITY ($BLAST_TAXO_GRANULARITY) is not yet supported, please modify your BlastConfig.pm file accordingly\n");
}

# deleting current file gr2species_migale.txt if present
if ( -e "$SiteConfig::BANKDIR/gr2species_migale.txt" ) {
	my $output_rm = `$SiteConfig::CMDDIR/rm -f $SiteConfig::BANKDIR/gr2species_migale.txt`;
	if ($output_rm eq "") {
		#ok
	} else {
		die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm -f $SiteConfig::BANKDIR/gr2species_migale.txt : $!");
		#print LOG "Error could not complete $SiteConfig::CMDDIR/rm -f $SiteConfig::BANKDIR/gr2species_migale.txt : $!\n";
		#die("Error could not complete $SiteConfig::CMDDIR/rm -f $SiteConfig::BANKDIR/gr2species_migale.txt : $!\n");
	}
}
open( GR2SPECIES_MIGALE, ">$SiteConfig::BANKDIR/gr2species_migale.txt" ) or die_with_error_mssg("Can not open $SiteConfig::BANKDIR/gr2species_migale.txt");


# gunziping .gz files
print LOG "\nStarting gunziping .gz files...\n" unless ($VERBOSE eq "OFF");
my $count_gzipped_treated = 0;
my $count_gzipped_gzipped = 0;
@bankdir         = <$SiteConfig::BANKDIR/*>;
foreach my $file (@bankdir) {
	$count_gzipped_treated++;
	if ($file =~ m/^.+\/.+\.gz$/) {
		#my $output_cmd_gunzip = `$SiteConfig::CMDDIR/gunzip $file`; gzip -d --force 
		my $output_cmd_gunzip = `$SiteConfig::CMDDIR/gzip -d --force $file`;  # there can be symlink in the directory too
		chomp($output_cmd_gunzip);
		if($output_cmd_gunzip eq ""){
			#ok, expected
			$count_gzipped_gzipped++;
		}else{
			die_with_error_mssg("Error while gunziping file $file : $output_cmd_gunzip");
			#print LOG "Error while gunziping file $file : $output_cmd_gunzip\n";
			#die("Error while gunziping file $file : $output_cmd_gunzip\n");
		}
	}
}
print LOG "Done gunzipping .gz files.\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_gzipped_treated file(s) have been treated\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_gzipped_gzipped file(s) have been gunzipped\n" unless ($VERBOSE eq "OFF");


print LOG "\nStarting expanding .gbff and other files containing multiple accession into individual files...\n" unless ($VERBOSE eq "OFF");
my $count_file_split_treated = 0;
my $count_file_split_gbff = 0;
my $count_file_split_gbff_into_gbk = 0;
my $count_file_split_multiple_dats = 0;
my $count_file_split_multiple_dats_into_dat = 0;
my $count_file_split_single_gbk = 0;
my $count_file_split_single_dat = 0;

sub split_file_according_to_slash_slash {

	my ($file, $extension) = @_; 
	my $regexIT;
	if ($extension eq "gbk") {
		$regexIT = "ACCESSION";
		$count_file_split_gbff++;
	} elsif ($extension eq "dat") {
		$regexIT = "AC";
		$count_file_split_multiple_dats++;
	} else {
		die_with_error_mssg("Error in split_file_according_to_slash_slash: The extension $extension is not recognized");
		#print LOG "Error in split_file_according_to_slash_slash: The extension $extension is not recognized\n";
		#die("Error in split_file_according_to_slash_slash: The extension $extension is not recognized\n");
	}

	#Read file line by line
	my $curr_accnum = "";
	open( GBFF_FILE_IT, "<$file" ) or die_with_error_mssg("Can not open gbff file $file");
	my $file_name_without_path = basename($file);
	my $count_contig = 0;

	my $open_new_SPLIT_GBK_FILE_IT = 1;
	while ( my $line = <GBFF_FILE_IT> ) {
		if($open_new_SPLIT_GBK_FILE_IT == 1){
			open( SPLIT_GBK_FILE_IT, ">$SiteConfig::BANKDIR/tmp_accnum_split.$extension" ) or die_with_error_mssg("Can not open file $SiteConfig::BANKDIR/tmp_accnum_split.$extension");
			$open_new_SPLIT_GBK_FILE_IT = 0;
		}
		print SPLIT_GBK_FILE_IT $line;
		chomp($line);
		if($line =~ m/^$regexIT\s+(.+?);?(\s+.+)?$/){
			#store curr accnum and open file
			$curr_accnum = $1;
			chomp($curr_accnum);
			$curr_accnum =~ s/^\s+//;
			$curr_accnum =~ s/\s+$//;
		}
		if($line =~ m/^\/\/\s*$/){
			#end of file
			close(SPLIT_GBK_FILE_IT);
			$open_new_SPLIT_GBK_FILE_IT = 1;
			if ($curr_accnum eq ""
				 || $curr_accnum =~ m/^unknown$/i ) {
				#print LOG "Error no accnum detected for chunk in file $file.\n";
				#die("Error no accnum detected for chunk in file $file.\n");
				$count_contig++;
				$curr_accnum = "${file_name_without_path}_contig${count_contig}";
			}
			my $mv_output = `$SiteConfig::CMDDIR/mv $SiteConfig::BANKDIR/tmp_accnum_split.$extension $SiteConfig::BANKDIR/${curr_accnum}.$extension 2>&1`;
			if($mv_output eq ""){
				#ok no problem
				$genome_files_to_do_individual_checking{"$SiteConfig::BANKDIR/${curr_accnum}.$extension"} = 1;
				if ($extension eq "gbk") {
					$count_file_split_gbff_into_gbk++;
				} elsif ($extension eq "dat") {
					$count_file_split_multiple_dats_into_dat++;
				} else {
					die_with_error_mssg("Error 2 in split_file_according_to_slash_slash: The extension $extension is not recognized");
					#print LOG "Error 2 in split_file_according_to_slash_slash: The extension $extension is not recognized\n";
					#die("Error 2 in split_file_according_to_slash_slash: The extension $extension is not recognized\n");
				}
			} else {
				die_with_error_mssg("Error with the command $SiteConfig::CMDDIR/mv $SiteConfig::BANKDIR/tmp_accnum_split.$extension $SiteConfig::BANKDIR/${curr_accnum}.$extension 2>&1 :\n$mv_output");
				#print LOG "Error with the command $SiteConfig::CMDDIR/mv $SiteConfig::BANKDIR/tmp_accnum_split.$extension $SiteConfig::BANKDIR/${curr_accnum}.$extension 2>&1 :\n$mv_output.\n";
				#die("Error with the command $SiteConfig::CMDDIR/mv $SiteConfig::BANKDIR/tmp_accnum_split.$extension $SiteConfig::BANKDIR/${curr_accnum}.$extension 2>&1 :\n$mv_output.\n");
			}
			$curr_accnum = "";
		}
	}
	close(GBFF_FILE_IT);
}

@bankdir         = <$SiteConfig::BANKDIR/*>;
# split and do preliminary check for serious error in files
foreach my $file (@bankdir) {

	$count_file_split_treated++;

	#deal with .gb or .genbank files to be renamed .gbk
	if ($file =~ m/^(.+)\.gb$/
		|| $file =~ m/^(.+)\.genbank$/) {
		#print "HERE : mv '$file' '$1.gbk'\n";
		my $mv_output = `mv '$file' '$1.gbk'`;
		if ($mv_output eq "") {
			#ok went well
			$file = "$1.gbk";
		} else {
			die_with_error_mssg("Error mv $file $1.gbk :\n$mv_output");
			#print LOG "Error mv $file $1.gbk :\n$mv_output.\n";
			#die("Error mv $file $1.gbk :\n$mv_output.\n");
		}
	#deal with .embl files to be renamed .dat
	} elsif ($file =~ m/^(.+)\.embl$/) {
		my $mv_output = `mv '$file' '$1.dat'`;
		if ($mv_output eq "") {
			#ok went well
			$file = "$1.dat";
		} else {
			die_with_error_mssg("Error mv $file $1.dat :\n$mv_output");
			#print LOG "Error mv $file $1.dat :\n$mv_output.\n";
			#die("Error mv $file $1.dat :\n$mv_output.\n");
		}
	} elsif ($file =~ m/^(.+)\.txt$/ && $TREAT_TXT_FILE_AS eq "embl") {
		my $mv_output = `mv '$file' '$1.dat'`;
		if ($mv_output eq "") {
			#ok went well
			$file = "$1.dat";
		} else {
			die_with_error_mssg("Error mv $file $1.dat :\n$mv_output");
			#print LOG "Error mv $file $1.dat :\n$mv_output.\n";
			#die("Error mv $file $1.dat :\n$mv_output.\n");
		}
	} elsif ($file =~ m/^(.+)\.txt$/ && $TREAT_TXT_FILE_AS eq "gbk") {
		my $mv_output = `mv '$file' '$1.gbk'`;
		if ($mv_output eq "") {
			#ok went well
			$file = "$1.gbk";
		} else {
			die_with_error_mssg("Error mv $file $1.gbk :\n$mv_output");
			#print LOG "Error mv $file $1.gbk :\n$mv_output.\n";
			#die("Error mv $file $1.gbk :\n$mv_output.\n");
		}
	}

	# deal with .gbff
	if ($file =~ m/^.+\/.+\.gbff$/) {
		split_file_according_to_slash_slash($file, "gbk");
	# deal with .multiple_dats
	} elsif ($file =~ m/^.+\/.+\.multiple_dats$/) {
		split_file_according_to_slash_slash($file, "dat");
	} elsif ($file =~ m/^.+\/.+\.gbk$/) {
		$output_cmd_grep = `$SiteConfig::CMDDIR/grep -E \'^//\\s*\$\' $file`;
		chomp($output_cmd_grep);
		if($output_cmd_grep =~ m/^\/\/\s*$/){
			#ok, expected, do nothing but store
			$genome_files_to_do_individual_checking{"$file"} = 1;
			$count_file_split_single_gbk++;
		}else{
			# file with multiple //, treat as .gbff
			print LOG "Warn: the file $file has multiple // and will be treated as containing multiple accession\n" unless ($VERBOSE eq "OFF");
			split_file_according_to_slash_slash($file, "gbk");
		}
	} elsif ($file =~ m/^.+\/.+\.dat$/) {
		$output_cmd_grep = `$SiteConfig::CMDDIR/grep -E \'^//\\s*\$\' $file`;
		chomp($output_cmd_grep);
		if($output_cmd_grep =~ m/^\/\/\s*$/){
			#ok, expected, do nothing but store
			$genome_files_to_do_individual_checking{"$file"} = 1;
			$count_file_split_single_dat++;
		}else{
			# file with multiple //, treat as .gbff
			print LOG "Warn: the file $file has multiple // and will be treated as containing multiple accession\n" unless ($VERBOSE eq "OFF");
			split_file_according_to_slash_slash($file, "dat");
		}
	} elsif ($file eq "$SiteConfig::BANKDIR/gr2species_migale.txt") {
		#ok, do nothing
	} else {
		die_with_error_mssg("Error: The file $file does not seem to be a genome file");
		#print LOG "Error: The file $file does not seem to be a genome file\n";
		#die("Error: The file $file does not seem to be a genome file\n");
	}
}
print LOG "Done expanding .gbff and other files containing multiple accession into individual files.\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_file_split_treated file(s) have been treated\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_file_split_gbff gbff file(s) have been splitted into $count_file_split_gbff_into_gbk gbk file(s)\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_file_split_multiple_dats multiple dat/embl file(s) have been splitted into $count_file_split_multiple_dats_into_dat dat/embl file(s)\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_file_split_single_gbk gbk file(s) have been found with one accession entry.\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_file_split_single_dat dat/embl file(s) have been found with one accession entry.\n" unless ($VERBOSE eq "OFF");
print LOG "A total of ".scalar(keys %genome_files_to_do_individual_checking)." file(s) will be checked in the next step of this script.\n" unless ($VERBOSE eq "OFF");


print LOG "\nStarting checking individual genome files and creating the file gr2species_migale with all accnum...\n" unless ($VERBOSE eq "OFF");

my $count_no_CDS = 0;
my $count_no_locus_tag = 0;
my $count_no_protein_sequence = 0;
my $count_no_sequence = 0;
my $count_files_ok = 0;
my $count_files_hash = 0;

sub checkForMissingTagsAndPrintGR2SPECIES_MIGALE {

	my ($file, $prefix_string, $accnum_from_file_name) = @_; 
  	# $prefix_string should be blank or FT
	my $string_cmd_grep;

	$output_cmd_grep = `$SiteConfig::CMDDIR/grep -E \'^//\\s*\$\' $file`;
	chomp($output_cmd_grep);
	if($output_cmd_grep =~ m/^\/\/\s*$/){
		#ok, expected
	}else{
		die_with_error_mssg("Error: Multiple // in file $file : $output_cmd_grep");
		#print LOG "Error: Multiple // in file $file : $output_cmd_grep\n";
		#die("Error: Multiple // in file $file : $output_cmd_grep\n");
	}


	# check sequence dna is present at end of file
	my $no_sequence = 0;
	if($prefix_string eq ""){
		$string_cmd_grep = "$SiteConfig::CMDDIR/grep -m 2 -E \'^ +1 +\'";
	} else {
		$string_cmd_grep = "$SiteConfig::CMDDIR/grep -m 2 -E \'^SQ +Sequence\'";
	}
	$output_cmd_grep = `$string_cmd_grep $file`;
	if($output_cmd_grep eq ""){
		$no_sequence = 1;
		$count_no_sequence++;
		#print LOG "Error: The file $file seems to not have any field of type DNA Sequence. Please check that this file includes the dna sequence.\n";
		#`$SiteConfig::CMDDIR/rm -f $SiteConfig::BANKDIR/gr2species_migale.txt`;
		#die("Error: The file $file seems to not have any field of type DNA Sequence. Please check that this file includes the dna sequence.\n");
	}

	# CDS available ?
	my $no_CDS = 0;
	$string_cmd_grep = "$SiteConfig::CMDDIR/grep -m 2 -E \'^".$prefix_string." +CDS +\'";
	$output_cmd_grep = `$string_cmd_grep $file`;
	if($output_cmd_grep eq ""){
		$no_CDS = 1;
		$count_no_CDS++;
	}

	# check at least one protein sequence available
	my $no_locus_tag = 0;
	$string_cmd_grep = "$SiteConfig::CMDDIR/grep -m 2 -E \'^".$prefix_string." +/locus_tag=\'";
	$output_cmd_grep = `$string_cmd_grep $file`;
	if($output_cmd_grep eq ""){
		$no_locus_tag = 1;
		$count_no_locus_tag++;
	}

        # protein sequence available ?
	my $no_protein_sequence = 0;
	$string_cmd_grep = "$SiteConfig::CMDDIR/grep -m 2 -E \'^".$prefix_string." +/translation=\'";
	$output_cmd_grep = `$string_cmd_grep $file`;
	if($output_cmd_grep eq ""){
		#print LOG "Warn: The file $file seems to not have any field of type /translation=. Please check that this file includes the protein sequences.\n";
		#die("Error: The file $file seems to not have any field of type /translation=. Please check that this file includes the protein sequences.\n");
		$no_protein_sequence = 1;
		$count_no_protein_sequence++;
	}


	if ($no_CDS == 1) {
		print LOG "Warn: The file $file seems to not have any CDS. This file has been ignored.\n" unless ($VERBOSE eq "OFF");
		print GR2SPECIES_MIGALE "#Bacteria\t$accnum_from_file_name\n";
		$count_files_hash++;
	} elsif ($no_locus_tag == 1) {
		print LOG "Warn: The file $file seems to not have any locus tag. This file has been ignored.\n" unless ($VERBOSE eq "OFF");
		print GR2SPECIES_MIGALE "#Bacteria\t$accnum_from_file_name\n";
		$count_files_hash++;
	} elsif ($no_protein_sequence == 1) {
		print LOG "Warn: The file $file seems to not have any protein sequence. This file has been ignored.\n" unless ($VERBOSE eq "OFF");
		print GR2SPECIES_MIGALE "#Bacteria\t$accnum_from_file_name\n";
		$count_files_hash++;
	} elsif ($no_sequence == 1) {
		print LOG "Warn: The file $file seems to not have any dna sequence at the end of the file. It will be treated by the pipeline however.\n" unless ($VERBOSE eq "OFF");
		print GR2SPECIES_MIGALE "Bacteria\t$accnum_from_file_name\n";
		$count_files_ok++;
	} else {
		print GR2SPECIES_MIGALE "Bacteria\t$accnum_from_file_name\n";
		$count_files_ok++;
	}

}

my $count_genome_files_treated = 0;
foreach my $file (keys %genome_files_to_do_individual_checking) {
	$count_genome_files_treated++;
	if ( $file =~ m/^(.+)\/(.+)(\..+)*\.dat$/ ) {
		my $root_dir = $1;
		my $accnum_from_file_name = $2;
		my $rest_of_file_name = $3;
		checkForMissingTagsAndPrintGR2SPECIES_MIGALE($file, "FT", $accnum_from_file_name);
	} elsif ($file =~ m/^(.+)\/(.+)\.gbk$/) {
		# check no multiple // in file		
		my $root_dir = $1;
		my $accnum_from_file_name = $2;
		checkForMissingTagsAndPrintGR2SPECIES_MIGALE($file, "", $accnum_from_file_name);
	} else {
		die_with_error_mssg("Error in checking individual genome file: the extension of the file $file is not supported.");
		#print LOG "Error in checking individual genome file: the extension of the file $file is not supported.\n";
		#die("Error in checking individual genome file: the extension of the file $file is not supported.\n");
	}
}

print LOG "Done checking individual genome file(s) and creating the file gr2species_migale with all accnum.\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_genome_files_treated file(s) have been treated\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_files_hash file(s) will been ignored because of the following reasons:\n" unless ($VERBOSE eq "OFF");
print LOG "\t$count_no_CDS file(s) have been found without CDS and have been ignored\n" unless ($VERBOSE eq "OFF");
print LOG "\t$count_no_locus_tag file(s) have been found without locus tags and have been ignored\n" unless ($VERBOSE eq "OFF");
print LOG "\t$count_no_protein_sequence file(s) have been found without protein sequence and have been ignored\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_no_sequence file(s) have been found without dna sequence at the end of the file. They will be treated by the pipeline however.\n" unless ($VERBOSE eq "OFF");
print LOG "A total of $count_files_ok file(s) will be treated by the next steps of the pipeline.\n" unless ($VERBOSE eq "OFF");

close(GR2SPECIES_MIGALE);


# end of script
print LOG
"\n---------------------------------------------------\n\n\nList_check_and_split_genomes_files.pl successfully completed at :",
  scalar(localtime),
  "\n\n=> No major problem was detected.\n\n---------------------------------------------------\n\n" unless ($VERBOSE eq "OFF");


# touch marker file done script
$output_backtick = `touch $SiteConfig::LOGDIR/check_and_prepare_genomes_files.done`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $SiteConfig::LOGDIR/check_and_prepare_genomes_files.done :\n$output_backtick \n $!");
	#print LOG "Error could not complete touch $SiteConfig::LOGDIR/check_and_prepare_genomes_files.done :\n$output_backtick \n $! \n";#
	#die("Error could not complete touch $SiteConfig::LOGDIR/check_and_prepare_genomes_files.done :\n$output_backtick \n $! \n");#
}

close(LOG);
















