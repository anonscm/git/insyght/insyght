#!/usr/local/bin/perl
#
# perl integrate_taxo_data.pl : automatic management of the integration of the data for the tables micado.taxo and micado.ncbi_taxonomy_tree
# default files to integrate are $SiteConfig::DATADIR/Update_taxo/taxo.dat and $SiteConfig::DATADIR/Update_taxo/ncbi_taxonomy.dat
#
# Date : 01/2016
# LT
#
#LOG FILE : $SiteConfig::LOGDIR/Update_taxo/integrate_taxo_data.log
#
# Pipeline origami v1.7.0 Copyright - INRA - 2016-2026
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2016)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use SiteConfig;
use ORIGAMI;

#script level variables
my $path_log_file = "$SiteConfig::LOGDIR/Update_taxo/integrate_taxo_data.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/integrate_taxo_data.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/integrate_taxo_data.error";
my $output_backtick = "";
my $VERBOSE = "ON";
my $TAXO_FILE_TO_LOAD = "$SiteConfig::DATADIR/Update_taxo/ncbi_taxonomy.dat";
#my $TAXO_NAMES_FILE_TO_LOAD = "$SiteConfig::DATADIR/Update_taxo/taxo_names.dat";
my $sth;
my $output_cmd;

sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


#set the variables according to the argument or exit with usage if the arguments are incorrect
foreach my $argnum ( 0 .. $#ARGV ) {
	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl integrate_taxo_data.pl -VERBOSE {ON, OFF}");
		}

	}
}

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Update_taxo/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG "\n---------------------------------------------------\n integrate_taxo_data.pl started at :", scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}



#checking for output files to integrate
if ( -e "$TAXO_FILE_TO_LOAD" ) {
	print LOG "taxo file that will be loaded in the table micado.taxo: $TAXO_FILE_TO_LOAD\n" unless $VERBOSE =~ m/^OFF$/;
} else {
	die_with_error_mssg("Error: taxo file that should be loaded in the table micado.taxo is not found: $TAXO_FILE_TO_LOAD");
}

#if ( -e "$TAXO_NAMES_FILE_TO_LOAD" ) {
#	print LOG "taxo_names file that will be loaded in the table micado.taxo_names: $TAXO_NAMES_FILE_TO_LOAD\n" unless $VERBOSE =~ m/^OFF$/;
#} else {
#	die_with_error_mssg("Error: taxo_names file that should be loaded in the table micado.taxo_names is not found: $TAXO_NAMES_FILE_TO_LOAD");
#}

# SET client_min_messages TO WARNING
$sth= $ORIGAMI::dbh->prepare("SET client_min_messages TO WARNING");
$sth->execute() or die_with_error_mssg("Impossible d'executer l'instruction SQL SET client_min_messages TO WARNING : $DBI::errstr");

# truncate taxo_names and taxo tables
print LOG "starting TRUNCATE micado.ncbi_taxonomy_tree\n" unless $VERBOSE =~ m/^OFF$/;
$sth= $ORIGAMI::dbh->prepare("TRUNCATE micado.ncbi_taxonomy_tree");
$sth->execute() or die_with_error_mssg("Impossible d'executer l'instruction SQL TRUNCATE micado.ncbi_taxonomy_tree : $DBI::errstr");
#print LOG "starting TRUNCATE micado.taxo CASCADE\n" unless $VERBOSE =~ m/^OFF$/;
#$sth= $ORIGAMI::dbh->prepare("TRUNCATE micado.taxo CASCADE");
#$sth->execute() or die_with_error_mssg("Impossible d'executer l'instruction SQL TRUNCATE micado.taxo CASCADE : $DBI::errstr");

# integrate files $TAXO_FILE_TO_LOAD and $TAXO_NAMES_FILE_TO_LOAD
print LOG "starting integration of file $TAXO_FILE_TO_LOAD\n" unless $VERBOSE =~ m/^OFF$/;
$output_cmd = `psql --quiet -f $TAXO_FILE_TO_LOAD -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
if ( $? == -1 )
{
	die_with_error_mssg("command psql --quiet -f $TAXO_FILE_TO_LOAD -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!");
}
if($output_cmd eq ''
	|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/
){
	print LOG "done import of file $TAXO_FILE_TO_LOAD.\n" unless $VERBOSE =~ m/^OFF$/;
}else{
	die_with_error_mssg("Error psql --quiet -f $TAXO_FILE_TO_LOAD -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
}

=pod
print LOG "starting integration of file $TAXO_NAMES_FILE_TO_LOAD\n" unless $VERBOSE =~ m/^OFF$/;
$output_cmd = `psql --quiet -f $TAXO_NAMES_FILE_TO_LOAD -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1`;
if ( $? == -1 )
{
	die_with_error_mssg("command psql --quiet -f $TAXO_NAMES_FILE_TO_LOAD -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname 2>&1 failed: $!");
}
if($output_cmd eq ''
	|| $output_cmd =~ m/^BEGIN\n(COPY\s+\d+\n)?COMMIT$/
){
	print LOG "done import of file $TAXO_NAMES_FILE_TO_LOAD.\n" unless $VERBOSE =~ m/^OFF$/;
}else{
	die_with_error_mssg("Error psql --quiet -f $TAXO_NAMES_FILE_TO_LOAD -U $ORIGAMI::user -p $ORIGAMI::port -h $ORIGAMI::host -d $ORIGAMI::dbname :\n$output_cmd");
}
=cut


# counting inserted tuples
print LOG "** VACUUMing table micado.ncbi_taxonomy_tree\n" unless $VERBOSE =~ m/^OFF$/;
$sth = $ORIGAMI::dbh->prepare("VACUUM micado.ncbi_taxonomy_tree");
$sth->execute or die_with_error_mssg("Pb execute $!");
print LOG "** Counting table micado.ncbi_taxonomy_tree" unless $VERBOSE =~ m/^OFF$/;
$sth = $ORIGAMI::dbh->prepare("SELECT count(*) from micado.ncbi_taxonomy_tree");
$sth->execute or die_with_error_mssg("Pb execute $!");
while ( my $res = $sth->fetchrow_hashref() ) {
	my $count_IT = $res->{count};
	print LOG "\t=> $count_IT tuples\n";
}

=pod
print LOG "** VACUUMing table micado.taxo_names\n" unless $VERBOSE =~ m/^OFF$/;
$sth = $ORIGAMI::dbh->prepare("VACUUM micado.taxo_names");
$sth->execute or die_with_error_mssg("Pb execute $!");
print LOG "** Counting table micado.taxo_names" unless $VERBOSE =~ m/^OFF$/;
$sth = $ORIGAMI::dbh->prepare("SELECT count(*) from micado.taxo_names");
$sth->execute or die_with_error_mssg("Pb execute $!");
while ( my $res = $sth->fetchrow_hashref() ) {
	my $count_IT = $res->{count};
	print LOG "\t=> $count_IT tuples\n" unless $VERBOSE =~ m/^OFF$/;
}
=cut
print LOG
"---------------------------------------------------\n integrate_taxo_data.pl successfully completed at :",
  scalar(localtime),
  "\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);





