#!/usr/local/bin/perl
#
# process_organize_tar_output_from_IDRIS.pl# Date : 01/2015
# LT
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
#use FindBin;                 # locate this script # no need because of perl -I script
#use lib "$FindBin::Bin/..";  # use the parent directory # no need because of perl -I script
use SiteConfig;


# whole script scoped variable
my $INPUT_FILE = undef;
my $CORE_INPUT_DIR = undef;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_alignments/`;

open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS.log");
print LOG "\n---------------------------------------------------\n\n\n process_organize_tar_output_from_IDRIS.pl started at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";

foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-INPUT_FILE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$INPUT_FILE = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG "incorrect -INPUT_FILE argument ; usage : perl process_organize_tar_output_from_IDRIS.pl -INPUT_DIR {PATH_TO_DIRECTORY}";
			die "incorrect -INPUT_FILE argument ; usage : perl process_organize_tar_output_from_IDRIS.pl -INPUT_DIR {PATH_TO_DIRECTORY}";
		}
	} elsif ( $ARGV[$argnum] =~ m/^-CORE_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$CORE_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG "incorrect -CORE_INPUT_DIR argument ; usage : perl process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}";
			die "incorrect -CORE_INPUT_DIR argument ; usage : perl process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}";
		}
	}
}

if (! defined $INPUT_FILE) {
	print LOG "Undefined INPUT_FILE ; usage : perl process_organize_tar_output_from_IDRIS.pl -INPUT_FILE {PATH_TO_DIRECTORY}";
	die "Undefined INPUT_FILE ; usage : perl process_organize_tar_output_from_IDRIS.pl -INPUT_FILE {PATH_TO_DIRECTORY}";
}
if (! defined $CORE_INPUT_DIR) {
	print LOG "Undefined CORE_INPUT_DIR ; usage : perl process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}";
	die "Undefined CORE_INPUT_DIR ; usage : perl process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR {PATH_TO_DIRECTORY}";
}


#parse file name
my $core_name_file = "";
if ( $INPUT_FILE =~ m/^.+\/rep_align_2_align_1_(.+)\.tar$/i ) {
	$core_name_file = $1;
} else {
	print LOG "Could not parse core name in file $INPUT_FILE\n";
	die("Could not parse core name in file $INPUT_FILE\n");
}

`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_alignments/tmp/launch_process_organize_files/`;
open( ERR_FILE, "> $SiteConfig::LOGDIR/Task_add_alignments/tmp/launch_process_organize_files/ERR_process_organize_${core_name_file}.ERR") or die(
"Can not open $SiteConfig::LOGDIR/Task_add_alignments/tmp/launch_process_organize_files/ERR_process_organize_${core_name_file}.ERR\n"
 );


print LOG "\nStarting to untar the input file $INPUT_FILE\n";
my $tar_output = `$SiteConfig::CMDDIR/tar -P -xf $INPUT_FILE -C $CORE_INPUT_DIR 2>&1`;
if(! $tar_output eq ""){
	print LOG "Error $SiteConfig::CMDDIR/tar -P -xvf $INPUT_FILE -C $CORE_INPUT_DIR 2>&1\ntar output = $tar_output\n";
	print ERR_FILE "Error $SiteConfig::CMDDIR/tar -P -xvf $INPUT_FILE -C $CORE_INPUT_DIR 2>&1\ntar output = $tar_output\n";
	die ("Error $SiteConfig::CMDDIR/tar -P -xvf $INPUT_FILE -C $CORE_INPUT_DIR 2>&1\ntar output = $tar_output\n");
}
print LOG "Done untar the input file\n";



print LOG "\nStarting to mv the untared files...\n";
#sub_untar_and_bzip_input_dir("$INPUT_FILE");

#list files in tar
my $tar_output = `$SiteConfig::CMDDIR/tar -tf $INPUT_FILE`;
my @untared_files = split(/\n/, $tar_output);

# mv untared files
foreach my $file_IT (@untared_files) {
	#print "$CORE_INPUT_DIR/$file_IT\n";
	my $full_path_file_IT = "$CORE_INPUT_DIR/$file_IT";
	my $output_mv = "";

	if ($full_path_file_IT =~ m/^.+\/(\d+)_(.+_alignment_params_table\.tsv)$/ ) {
		#`$SiteConfig::CMDDIR/tar -P --append --file=$CORE_INPUT_DIR/alignments/$core_tar_name.tar $full_path_file_IT`;
		my $file_name_IT = "${1}_${2}";
		#print LOG "$SiteConfig::CMDDIR/mv $full_path_file_IT $CORE_INPUT_DIR/alignment_params/element_id_${1}/${file_name_IT}\n";
		`$SiteConfig::CMDDIR/mkdir -p $CORE_INPUT_DIR/alignment_params/element_id_${1}/`;
    		$output_mv = `$SiteConfig::CMDDIR/mv $full_path_file_IT $CORE_INPUT_DIR/alignment_params/element_id_${1}/${file_name_IT} 2>&1`;
	#_alignment_table
        } elsif ($full_path_file_IT =~ m/^.+\/(\d+)_(.+_alignment_table\.tsv)$/ ) {
		my $file_name_IT = "${1}_${2}";
		#print LOG "$SiteConfig::CMDDIR/mv $full_path_file_IT $CORE_INPUT_DIR/alignments/element_id_${1}/${file_name_IT}\n";
		`$SiteConfig::CMDDIR/mkdir -p $CORE_INPUT_DIR/alignments/element_id_${1}/`;
		$output_mv = `$SiteConfig::CMDDIR/mv $full_path_file_IT $CORE_INPUT_DIR/alignments/element_id_${1}/${file_name_IT} 2>&1`;
	#_alignment_pairs_table
        } elsif ($full_path_file_IT =~ m/^.+\/(\d+)_(.+_alignment_pairs_table\.tsv)$/ ) {
		my $file_name_IT = "${1}_${2}";
		#print LOG "$SiteConfig::CMDDIR/mv $full_path_file_IT $CORE_INPUT_DIR/alignment_pairs/element_id_${1}/${file_name_IT}\n";
		`$SiteConfig::CMDDIR/mkdir -p $CORE_INPUT_DIR/alignment_pairs/element_id_${1}/`;
		$output_mv = `$SiteConfig::CMDDIR/mv $full_path_file_IT $CORE_INPUT_DIR/alignment_pairs/element_id_${1}/${file_name_IT} 2>&1`;
	#_homologies_table
        } elsif ($full_path_file_IT =~ m/^.+\/(\d+)_(.+_homologies_table\.tsv)$/ ) {
		my $file_name_IT = "${1}_${2}";
		#print LOG "$SiteConfig::CMDDIR/mv $full_path_file_IT $CORE_INPUT_DIR/homologies/element_id_${1}/${file_name_IT}\n";
		`$SiteConfig::CMDDIR/mkdir -p $CORE_INPUT_DIR/homologies/element_id_${1}/`;
		$output_mv = `$SiteConfig::CMDDIR/mv $full_path_file_IT $CORE_INPUT_DIR/homologies/element_id_${1}/${file_name_IT} 2>&1`;
        } else {
            print LOG "Error mv untared files : the file $full_path_file_IT does not match the appropriate regex\n";
            print ERR_FILE "Error mv untared files : the file $full_path_file_IT does not match the appropriate regex\n";
            die("Error mv untared files : the file $full_path_file_IT does not match the appropriate regex\n");
        }

	if(! $output_mv eq ""){
		print LOG "Error mv $full_path_file_IT\n mv output = $output_mv\n";
		print ERR_FILE "Error mv $full_path_file_IT\n mv output = $output_mv\n";
		die ("Error mv $full_path_file_IT\n mv output = $output_mv\n");
	}

}

print LOG "Done mv the untared files\n";

=pod
print LOG "\nStarting to bzip2 the old tar file\n";
my $bzip_output = `bzip2 $INPUT_FILE 2>&1`;
if(! $bzip_output eq ""){
	print LOG "Error bzip2 $INPUT_FILE 2>&1\nbzip output = $bzip_output\n";
	print ERR_FILE "Error bzip2 $INPUT_FILE 2>&1\nbzip output = $bzip_output\n";
	die ("Error bzip2 $INPUT_FILE 2>&1\nbzip output = $bzip_output\n");
}
print LOG "Done bzip2 the old tar file\n";
=cut

print LOG "\nStarting touch done file...\n";
`$SiteConfig::CMDDIR/touch $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_file}.done`;
print LOG "Done touch done file\n";


#end of script
print LOG
"\n---------------------------------------------------\n\n\n process_organize_tar_output_from_IDRIS.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);


