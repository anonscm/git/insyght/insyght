#!/usr/local/bin/perl
#
# generate_organize_tar_output_from_IDRIS.pl
# Date : 01/2015
# LT
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use SiteConfig;

# whole script scoped variable
my $INPUT_DIR = undef;
my @list_tar_files = ();


# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_alignments/`;
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_add_alignments/generate_organize_tar_output_from_IDRIS.log"
);
print LOG 
"\n---------------------------------------------------\n\n\n generate_organize_tar_output_from_IDRIS.pl started at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";

foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i)
		{
			$INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			print LOG "incorrect -INPUT_DIR argument ; usage : perl generate_organize_tar_output_from_IDRIS.pl -INPUT_DIR {PATH_TO_DIRECTORY}";
			die "incorrect -INPUT_DIR argument ; usage : perl generate_organize_tar_output_from_IDRIS.pl -INPUT_DIR {PATH_TO_DIRECTORY}";
		}
	}
}

if (! defined $INPUT_DIR) {
	print LOG "Undefined INPUT_DIR ; usage : perl generate_organize_tar_output_from_IDRIS.pl -INPUT_DIR {PATH_TO_DIRECTORY}";
	die "Undefined INPUT_DIR ; usage : perl generate_organize_tar_output_from_IDRIS.pl -INPUT_DIR {PATH_TO_DIRECTORY}";
}

# deal with dir
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/launch_process_organize_files/`;
`$SiteConfig::CMDDIR/rm -f -r $SiteConfig::DATADIR/Task_add_alignments/tmp/launch_process_organize_files/*`;

sub list_tar_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die "Unable to open $path: $!";
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);

    for (@files) {
        
	if (-d $_) {
		list_tar_files_recursively($_);
        } elsif ($_ =~ m/^.+\/(.+)\.tar$/ ) {
		push (@list_tar_files, $_);
        } else {
            print LOG "Error in list_tar_files_recursively : the file $_ does not match the appropriate regex\n";
            die("Error in list_tar_files_recursively : the file $_ does not match the appropriate regex\n");
        }
    }
}
list_tar_files_recursively($INPUT_DIR);


print LOG "\n".scalar(@list_tar_files)." tar files found under $INPUT_DIR\n";

print LOG "\n Generating qsub script for each file...\n";
foreach my $tar_file_IT (@list_tar_files) {

	my $core_name_file;
	if ( $tar_file_IT =~ m/^.+\/rep_align_2_align_1_(.+)\.tar$/ ) {
		$core_name_file = $1;
	} else {
		print LOG "could not parse core_name_file for file $tar_file_IT.\n";
		die("could not parse core_name_file for file $tar_file_IT.\n");
	}

	open( MV_BATCHFILE,
	"> $SiteConfig::DATADIR/Task_add_alignments/tmp/launch_process_organize_files/batch_process_organize_${core_name_file}.ll"
	  )
	  or die(
	"Can not open $SiteConfig::DATADIR/Task_add_alignments/tmp/launch_process_organize_files/batch_process_organize_${core_name_file}.ll\n"
	  );

	print MV_BATCHFILE "#!/bin/bash
#\$ -S /bin/bash
# (n) No mail is sent ; Avertir au début (b)egin, à la fin (e)nd, à l'éliminaton (a)bort et
# à la suspension (s)uspend d'un job
#\$ -m n
# Adresse mail à laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#\$ -o $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_file}.out
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -e $SiteConfig::LOGDIR/Task_add_alignments/process_organize_tar_output_from_IDRIS_${core_name_file}.err
# Lance la commande depuis le répertoire où est lancé le script
#\$ -cwd
\n";

	print MV_BATCHFILE "perl -I $SiteConfig::SCRIPTSDIR $SiteConfig::SCRIPTSDIR/process_organize_tar_output_from_IDRIS.pl -CORE_INPUT_DIR $INPUT_DIR -INPUT_FILE $tar_file_IT\n";

	#close MV_BATCHFILE
	close(MV_BATCHFILE);

}
print LOG "\n Done generating qsub script for each file.\n";

#end of script
print LOG
"\n---------------------------------------------------\n\n\n generate_organize_tar_output_from_IDRIS.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n";
close(LOG);







