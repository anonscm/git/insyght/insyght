#!/usr/local/bin/perl
#
# Task_add_alignment_launch_jobs.pl
#
# Example command run on cluster mig : perl Task_add_alignment_launch_jobs.pl -MAX_JOBS 200 -CMD_CLUSTER "qsub -m ea -q short.q"
# Example command run on cluster mig : perl Task_add_alignment_launch_jobs.pl -MAX_JOBS 200 -CMD_CLUSTER "qsub -m ea -q short.q" -RECOVER_FROM_FAILURE ON
#
# -CMD_CLUSTER {STRING} : -CMD_CLUSTER "qsub -m ea -q short.q". must be last option given as argument.
# -MAX_JOBS {INTEGER}
# -RECOVER_FROM_FAILURE ON
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#


use strict;
use Time::HiRes qw(usleep);
use SiteConfig;
#use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;
use BlastConfig;


# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_launch_jobs.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/Task_add_alignment_launch_jobs.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/Task_add_alignment_launch_jobs.error";
my $output_backtick = "";
my $VERBOSE = "ON";
my $CMD_SUBMIT_CLUSTER = "$SiteConfig::CMDDIR/bash";
my $MAX_JOBS = 1;
my $flag_RECOVER_FROM_FAILURE = 0;
my $FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = "ON";
my $output_rm = "";
my $output_mv = "";
my @files_cluster_mig = ();
my $EMAIL_NOTIFICATION = "";
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $molecule_type;


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	if ( length ($EMAIL_NOTIFICATION) > 0 ) {
		my $email_subject = "Inysght notification : Error Task_add_alignment_launch_jobs.pl";
		my $email_body = "Hello,\nThis is a notification from Insyght, your job Task_add_alignment_launch_jobs.pl did not complete because of an error :\n$error_mssg\nSincerely,\nThe Insyght team\n";
		`echo "$email_body" | mailx -r Inysght_notification_no_reply -s "$email_subject" $EMAIL_NOTIFICATION`;
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


foreach my $argnum ( 0 .. $#ARGV ) {
	if  ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg ("incorrect -VERBOSE argument ; usage : perl Task_add_alignment_launch_jobs.pl -VERBOSE {ON, OFF}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-CMD_CLUSTER/ ) {
		if ( $ARGV[$argnum] =~ m/^0$/i ) {

			#do nothing
		} 
		else {
			if ( $ARGV[$argnum] =~ m/^.+$/i ) {
				$CMD_SUBMIT_CLUSTER = $ARGV[ $argnum + 1 ];
			}
		}
	} elsif ( $ARGV[$argnum] =~ m/^-MAX_JOBS$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_JOBS = $ARGV[ $argnum + 1 ];
			if($MAX_JOBS < 1){
				die_with_error_mssg ("incorrect -MAX_JOBS argument ; usage : perl Task_add_alignment_launch_jobs.pl  -MAX_JOBS {int > 0}");
			}
		}
		else {
			die_with_error_mssg ("incorrect -MAX_JOBS argument ; usage : perl Task_add_alignment_launch_jobs.pl  -MAX_JOBS {int > 0}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-RECOVER_FROM_FAILURE$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^ON$/i )
		{
			$flag_RECOVER_FROM_FAILURE = 1;
			print "\nmode RECOVER_FROM_FAILURE activated.\n\n" unless $VERBOSE =~ m/^OFF$/;
		}
		else {
			die_with_error_mssg("incorrect -RECOVER_FROM_FAILURE argument ; usage : perl Task_add_alignment_launch_jobs.pl -RECOVER_FROM_FAILURE ON ; omit the -RECOVER_FROM_FAILURE flag to turn off");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-EMAIL_NOTIFICATION/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^(.+)$/i ) {
			$EMAIL_NOTIFICATION = $1;
			print "\nmode EMAIL_NOTIFICATION activated, notifications will be sent to : $EMAIL_NOTIFICATION\n\n" unless $VERBOSE =~ m/^OFF$/;
		}
		else {
			die_with_error_mssg("incorrect -EMAIL_NOTIFICATION argument ; usage : perl Task_add_alignment_launch_jobs.pl -EMAIL_NOTIFICATION {your_email_address} ; omit the -EMAIL_NOTIFICATION flag to turn off");
		}
	} elsif  ( $ARGV[$argnum] =~ m/^-FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK argument ; usage : perl Task_add_alignment_launch_jobs.pl -FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK {ON, OFF}");
		}
	}
}


# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_add_alignments/`;


# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_add_alignments/Task_add_alignment_launch_jobs.log");
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG "\n---------------------------------------------------\n Task_add_alignment_launch_jobs.pl started at :", scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;


print LOG "\nYou choose the following options :\n\tMAX_JOBS : $MAX_JOBS\n\tVERBOSE : $VERBOSE\n" unless $VERBOSE =~ m/^OFF$/;
if ( $CMD_SUBMIT_CLUSTER =~ m/^0$/i ) {
	#do nothing
}
else {
	print LOG "CMD_CLUSTER : $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/;
}


if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
	#die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately");
}

# mkdir -p
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/align_done/`;

# rm files
# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}

$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_*.err`;
$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_*.out`;
$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/*.main_Align_done`;
if($flag_RECOVER_FROM_FAILURE == 0){
	$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f -r $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/*`;
}
if ($output_rm eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm files : $output_rm");
}


# list files under $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb
my @archive_files = <$SiteConfig::DATADIR/Task_add_alignments/tmp/archive_files/*.archive>;
print LOG scalar(@archive_files) . " .archive files found under $SiteConfig::DATADIR/Task_add_alignments/tmp/archive_files/\n" unless $VERBOSE =~ m/^OFF$/;



my $count_already_existing_output_files = 0;
# Accepts one argument: the full path to a directory.
sub sub_list_alignment_ll_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or do {
	die_with_error_mssg("Unable to open $path: $!");
    };

    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);

    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_alignment_ll_files_recursively ($_);
        } elsif ($_ =~ m/^.+\/batch_(\w+)_(\d+)_VS_(\w+)_(\d+)\.ll$/ ) {
		my $element_id_parsed = $2;
		my $core_name_parsed = "$2_and_$4";
		if($flag_RECOVER_FROM_FAILURE == 1){
			# check if output file already here
			if ( -e "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignment_pairs/${molecule_type}_${element_id_parsed}/${core_name_parsed}_alignment_pairs_table.tsv"
				&& -e "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignment_params/${molecule_type}_${element_id_parsed}/${core_name_parsed}_alignment_params_table.tsv"
				&& -e "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignments/${molecule_type}_${element_id_parsed}/${core_name_parsed}_alignment_table.tsv"
			) {
				# output file exists, skip
				print LOG "Output file $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignment_pairs/${molecule_type}_${element_id_parsed}/${core_name_parsed}_alignment_pairs_table.tsv && _alignment_params_table.tsv && _alignment_table already exist, skipping batch command file $_\n" unless $VERBOSE =~ m/^OFF$/;
				$count_already_existing_output_files++;
			} else {
				push ( @files_cluster_mig, $_);
			}
		} else {
			push ( @files_cluster_mig, $_);
	        }
        } elsif ($_ =~ m/^.+\/batch_(\w+)_(\d+)_(.+)\.ll~$/ ) {
		# del ~ files
		`$SiteConfig::CMDDIR/rm -f $_`;
        } else {
            	die_with_error_mssg("Error in sub_list_alignment_ll_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
sub_list_alignment_ll_files_recursively ("$SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/");
print LOG scalar(@files_cluster_mig)." batch_XXX.ll files found \n" unless $VERBOSE =~ m/^OFF$/;
if($flag_RECOVER_FROM_FAILURE == 1){
	print LOG "Skipped $count_already_existing_output_files batch command files because the corresponding alignment output file already exists\n" unless $VERBOSE =~ m/^OFF$/;
}

my $number_alignment_started  = 0;
my $number_alignment_done = 0;
#my $count_alignment_failure = 0;
my $number_alignment_started_before_force_submit_check = 0;

sub sub_count_and_mv_output_files {

	if ( $FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK =~ m/^ON$/i && ( $number_alignment_started - $number_alignment_done < $MAX_JOBS ) && $number_alignment_started > $number_alignment_started_before_force_submit_check ) {
		$number_alignment_started_before_force_submit_check = $number_alignment_started;
		print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK is ON ; Continuing to submit as there is $number_alignment_started - $number_alignment_done = ".($number_alignment_started - $number_alignment_done)." simulteneous process ;  MAX_JOBS =  $MAX_JOBS\n" unless $VERBOSE =~ m/^OFF$/i;
	
	} else {

		#my @list_new_files_alignment_done = <$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/*.main_Align_done>;
		my @list_new_files_alignment_done = ();
		opendir(my $dh, "$SiteConfig::DATADIR/Task_add_alignments/tmp/align_done/") or do {
			die_with_error_mssg("opendir($SiteConfig::DATADIR/Task_add_alignments/tmp/align_done/): $!");
		};
		while (my $de = readdir($dh)) {
		  next if $de =~ /^\./ or $de !~ /.+\.main_Align_done$/;
		  push ( @list_new_files_alignment_done, "$SiteConfig::DATADIR/Task_add_alignments/tmp/align_done/$de");
		}
		closedir($dh);


		# check err files are empty
		my $check_err_files_are_empty = `cat $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_*.err 2>/dev/null`;
		if ($check_err_files_are_empty eq "" ){
			#ok continue
		} else {
			die_with_error_mssg("Error cat $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_*.err is not empty : $check_err_files_are_empty");
		}
		$check_err_files_are_empty = `cat $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/sge_err_out/batch_*.err 2>/dev/null`;
		if ($check_err_files_are_empty eq "" ){
			#ok continue
		} else {
			die_with_error_mssg("Error cat $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/sge_err_out/batch_*.err is not empty : $check_err_files_are_empty");
		}


		foreach my $list_new_files_alignment_done_IT (@list_new_files_alignment_done) {

=pod

			#parse file name
			my $master_element_id = "";
			my $core_name_list_new_files_alignment_done_IT = "";
			my $output_name_from_mainAlign = "";
			if ( $list_new_files_alignment_done_IT =~ m/^.+\/(\w+)_(\d+)_VS_(\w+)_(\d+)\.main_Align_done$/i ) {
				$master_element_id = $2;
				$core_name_list_new_files_alignment_done_IT = "$1_$2_VS_$3_$4";
				$output_name_from_mainAlign = "$2_and_$4";
			} else {
				die_with_error_mssg("Could not parse core name in file $list_new_files_alignment_done_IT");
			}

			# if .err not empty
			if ($CMD_SUBMIT_CLUSTER eq "$SiteConfig::CMDDIR/bash" ) {
				# do nothing
			} else {
				if (! -z "$SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_list_new_files_alignment_done_IT}.err" ) {

					if ($DO_NOT_STOP_ON_MINOR_JOB_FAILURE == 1) {
						$count_alignment_failure++;
						`$SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_list_new_files_alignment_done_IT}.err $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_list_new_files_alignment_done_IT}.err_DO_NOT_STOP_ON_MINOR_JOB_FAILURE`;
						`$SiteConfig::CMDDIR/mv $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_list_new_files_alignment_done_IT}.out $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_list_new_files_alignment_done_IT}.out_DO_NOT_STOP_ON_MINOR_JOB_FAILURE`;
						`$SiteConfig::CMDDIR/rm -f $list_new_files_alignment_done_IT`;
						$number_alignment_done++;
					} else {
						die_with_error_mssg("The file $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_list_new_files_alignment_done_IT}.err is not empty : $!");
					}
				} else {

					#rm .err, .out, and marker file
					$output_rm = `$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_list_new_files_alignment_done_IT}.err`;
					$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_list_new_files_alignment_done_IT}.out`;
			
					if ($output_rm eq "") {
						#ok
					} else {
						die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm .err, .out file : $output_rm");
					}
				}
			}

			# mv output files to correct final directory
			# no homologies files if no protein match...
			#if ( -e "${output_name_from_mainAlign}_homologies_table.tsv" ) {
			$output_mv = `$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/homologies/${molecule_type}_${master_element_id}/`;
			$output_mv = $output_mv.`$SiteConfig::CMDDIR/mv ${output_name_from_mainAlign}_homologies_table.tsv $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/homologies/${molecule_type}_${master_element_id}/${output_name_from_mainAlign}_homologies_table.tsv`;
		
			#else{
			#	die_with_error_mssg("the file ${output_name_from_mainAlign}_homologies_table.tsv does not exists.\n");
			#}

			#if ( -e "${output_name_from_mainAlign}_alignment_params_table.tsv" ) {
			$output_mv = $output_mv.`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignment_params/${molecule_type}_${master_element_id}/`;
			$output_mv = $output_mv.`$SiteConfig::CMDDIR/mv ${output_name_from_mainAlign}_alignment_params_table.tsv $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignment_params/${molecule_type}_${master_element_id}/${output_name_from_mainAlign}_alignment_params_table.tsv`;
			#}else{
			#	die_with_error_mssg("the file ${output_name_from_mainAlign}_alignment_params_table.tsv does not exists.\n");
			#}

			#if ( -e "${output_name_from_mainAlign}_alignment_table.tsv" ) {
			$output_mv = $output_mv.`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignments/${molecule_type}_${master_element_id}/`;
			$output_mv = $output_mv.`$SiteConfig::CMDDIR/mv ${output_name_from_mainAlign}_alignment_table.tsv $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignments/${molecule_type}_${master_element_id}/${output_name_from_mainAlign}_alignment_table.tsv`;
			#}else{
			#	die_with_error_mssg("the file ${output_name_from_mainAlign}_alignment_table.tsv does not exists.\n");
			#}

			#if ( -e "${output_name_from_mainAlign}_alignment_pairs_table.tsv" ) {
			$output_mv = $output_mv.`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignment_pairs/${molecule_type}_${master_element_id}/`;
			$output_mv = $output_mv.`$SiteConfig::CMDDIR/mv ${output_name_from_mainAlign}_alignment_pairs_table.tsv $SiteConfig::DATADIR/Task_add_alignments/tmp/tsv/alignment_pairs/${molecule_type}_${master_element_id}/${output_name_from_mainAlign}_alignment_pairs_table.tsv`;
			#}else{
			#	die_with_error_mssg("the file ${output_name_from_mainAlign}_alignment_pairs_table.tsv does not exists.\n");
			#}

			if ($output_mv eq "") {
				#ok
			} else {
				die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/mv output files : $output_mv");
			}
=cut

			#`$SiteConfig::CMDDIR/rm -f $list_new_files_alignment_done_IT`;
			`$SiteConfig::CMDDIR/mv $list_new_files_alignment_done_IT ${list_new_files_alignment_done_IT}_checked`;
			# increment counter
			$number_alignment_done++;

		}
	}
}

print LOG "Start run main_Align command and mv files\n" unless $VERBOSE =~ m/^OFF$/;

foreach my $files_cluster_mig_IT (@files_cluster_mig) {

	# wait a bit if there more than $MAX_JOBS child process that have been launched simultaneously
	my $count_sleep = 0;
	while ( $count_sleep < 2 ) {

		sub_count_and_mv_output_files();

		#if ($MAX_JOBS == 1) {
		#	last;
		#} else {
			if( $number_alignment_started - $number_alignment_done < $MAX_JOBS ){
				#wait a bit so that there is a shifting in time and exit loop
				#usleep(10000);
				my $number_jobs_being_processed = $number_alignment_started - $number_alignment_done;
				my $number_jobs_left_to_be_submitted = scalar(@files_cluster_mig) - $number_alignment_started;
				print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : Continuing to submit ; MAX_JOBS = $MAX_JOBS ; Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_alignment_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
				last;
			}

			if ($count_sleep == 0 ) {
				my $number_jobs_being_processed = $number_alignment_started - $number_alignment_done;
				my $number_jobs_left_to_be_submitted = scalar(@files_cluster_mig) - $number_alignment_started;
				print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : waiting a bit, $MAX_JOBS jobs launched simultenously ; MAX_JOBS = $MAX_JOBS ; Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_alignment_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
				$count_sleep = 1;
				usleep(10000);
			} else {
				usleep(10000);
			}
		#}
	}#while ( $count_sleep < 2 ) {


=pod
	# check the state of blast failure
	if ($DO_NOT_STOP_ON_MINOR_JOB_FAILURE == 1 && $count_alignment_failure > 0) {
		print LOG "There are $count_alignment_failure job failures so far ; DO_NOT_STOP_ON_MINOR_JOB_FAILURE mode on.\n" unless $VERBOSE =~ m/^OFF$/i;
	}
	if ($count_alignment_failure > 500) {
		die_with_error_mssg("Stopping script as there is more than 500 jobs failures, something is wrong, please check.");
	}
=cut

	# make sure the ll file is executable
	my $chmod_output = `chmod +x $files_cluster_mig_IT`;
	if ( ! $chmod_output eq "") {
		die_with_error_mssg("ERROR Could not chmod +x $files_cluster_mig_IT: $!");
	}

	#parse file name batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.ll
	#my $master_elet_id;
	#my $sub_elet_id;
	my $core_name_ll_file_IT = "";
	if ( $files_cluster_mig_IT =~ m/^.+\/batch_(.+)\.ll$/i ) {
		#$master_elet_id = $1;
		#$sub_elet_id = $2;
		$core_name_ll_file_IT = $1;
	} else {
		die_with_error_mssg("Could not parse core name in file $files_cluster_mig_IT");
	}


	# launch of the bash file
	if ( $CMD_SUBMIT_CLUSTER eq "$SiteConfig::CMDDIR/bash") {

		if($MAX_JOBS > 1){
			system("$CMD_SUBMIT_CLUSTER $files_cluster_mig_IT >> $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_ll_file_IT}.err 2>&1 &");
			if ( $? != 0 ) {
				die_with_error_mssg("command $CMD_SUBMIT_CLUSTER $files_cluster_mig_IT failed: $!");
			}
			$number_alignment_started++;
			#print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : file $files_cluster_mig_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
			my $number_jobs_being_processed = $number_alignment_started - $number_alignment_done;
			my $number_jobs_left_to_be_submitted = scalar(@files_cluster_mig) - $number_alignment_started;
			print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_alignment_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		} else {
			$number_alignment_started++;
			my $number_jobs_being_processed = $number_alignment_started - $number_alignment_done;
			my $number_jobs_left_to_be_submitted = scalar(@files_cluster_mig) - $number_alignment_started;
			print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_alignment_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
			system("$CMD_SUBMIT_CLUSTER $files_cluster_mig_IT >> $SiteConfig::DATADIR/Task_add_alignments/tmp/cmd_files_cluster_mig/batch_${core_name_ll_file_IT}.err 2>&1");
			print LOG "file $files_cluster_mig_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
			print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_alignment_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		}

	} else {
		# use of cluster
		my $output_cmd = `$CMD_SUBMIT_CLUSTER $files_cluster_mig_IT 2>&1`;
		if ( $? != 0 )
		{
			die_with_error_mssg("command $CMD_SUBMIT_CLUSTER $files_cluster_mig_IT 2>&1 failed: $!");
		}
		if($output_cmd =~ m/^Your job .+ has been submitted$/i
			|| $output_cmd eq ""
		){
			$number_alignment_started++;
			#print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : file $files_cluster_mig_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
			my $number_jobs_being_processed = $number_alignment_started - $number_alignment_done;
			my $number_jobs_left_to_be_submitted = scalar(@files_cluster_mig) - $number_alignment_started;
			print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_alignment_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		}else{
			die_with_error_mssg("Error launch of the bash on cluster: $CMD_SUBMIT_CLUSTER $files_cluster_mig_IT :\n$output_cmd");
		}
	}
}#foreach my $files_cluster_mig_IT (@files_cluster_mig) {

print LOG "done submitting main_Align command and mv files\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "waiting for last commands to complete...\n" unless $VERBOSE =~ m/^OFF$/;
$FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = "OFF";

#wait for all files
my $count_finish = 0;
my $count_wait_hours = 0;
while ( $count_finish >= 0 ) {

	sub_count_and_mv_output_files();

	if($number_alignment_done == scalar(@files_cluster_mig) ){
		last;
	}

	usleep(1000000);#sleep 1s
	$count_finish++;
	if ($count_finish > 1800){#30 min
		#die_with_error_mssg ("Error, waited for blast alignment file for more than 30 minutes");
		$count_wait_hours++;
		print LOG "( $number_alignment_done / ".scalar(@files_cluster_mig).") : All jobs have been launched. Waiting for blast output files ; Total waiting time = $count_wait_hours hours ; Total output files found yet = $number_alignment_done. (This message will display every hour until all output files are found).\n" unless $VERBOSE =~ m/^OFF$/;
		$count_finish = 0;
	}
}


#check no more .tsv file in current directory
my @leftover_tsv_file_in_current_dir = <./*.tsv>;
if(@leftover_tsv_file_in_current_dir){
	die_with_error_mssg(@leftover_tsv_file_in_current_dir." leftover tsv files in current directory found, should be empty.");
}

#end of script
=pod
if ($DO_NOT_STOP_ON_MINOR_JOB_FAILURE == 1 && $count_alignment_failure > 0) {

	print LOG "Script has finished submitting jobs ($number_alignment_done jobs submitted total) but there have been $count_alignment_failure failures ; DO_NOT_STOP_ON_MINOR_JOB_FAILURE mode was on.\nPlease re-run Task_add_alignment_launch_jobs.pl with the mode RECOVER_FROM_FAILURE ON to complete this step.\n If some jobs keep failing, please run them manuallly to check what is wrong with them.\n" unless $VERBOSE =~ m/^OFF$/;

} else {
=cut
	print LOG "$number_alignment_done jobs have been successfully submitted in this run.\n" unless $VERBOSE =~ m/^OFF$/;
	if($flag_RECOVER_FROM_FAILURE == 1){
		print LOG "$count_already_existing_output_files jobs were skipped because RECOVER_FROM_FAILURE mode was ON and the corresponding output file was already present.\n" unless $VERBOSE =~ m/^OFF$/;
	}

	print LOG
	"\n---------------------------------------------------\n\n\n Task_add_alignment_launch_jobs.pl successfully completed at :",
	  scalar(localtime),
	  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;

#}
# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}
close(LOG);


if ( length ($EMAIL_NOTIFICATION) > 0 ) {
	my $email_subject = "Inysght notification : Task_add_alignment_launch_jobs.pl complete";
	my $email_body = "Hello,\nThis is a notification from Insyght, your job Task_add_alignment_launch_jobs.pl is complete!\nYou can now proceed with the rest of the pipeline.\nSincerely,\nThe Insyght team\n";
	`echo "$email_body" | mailx -r Inysght_notification_no_reply -s "$email_subject" $EMAIL_NOTIFICATION`;
}











