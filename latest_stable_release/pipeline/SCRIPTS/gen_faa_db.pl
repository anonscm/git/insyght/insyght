#!/usr/local/bin/perl

# Script de construction et mise � jour de Origami
# Permet le g�n�ration des fichier de prot�ines au format FASTA
# et de la banque BLAST associ�e
#
# Date : 04/2009
# LT
#
# Please have a look at the documentation (algorithmics) for this script in ORIGAMI/DOC/Algorithmics_of_scripts
#
#GOAL : This script does the following tasks :
#create the fasta files corresponding to new_elt_id and curr_elt_id given in argument, each holding the sequences for all the proteins of its organism. Then those 2 fasta files are prepared to be blasted against with the command Formatdb if specified
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Franc�ois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use DBI;
use SiteConfig;
use ORIGAMI;
use lib '$SiteConfig::SCRIPTSDIR';
use Time::HiRes qw(sleep);

#script level variable
my $element_id_accepted_as_new;
my $current_element_id;
my $VERBOSE;
my $MAKE_FORMATDB;

#get the arguments $new_elt_id and $curr_elt_id or exit with usage if the arguments are incorrect

foreach my $argnum ( 0 .. $#ARGV ) {

	#print "$ARGV[$argnum]\n";

	if ( $argnum == 0 ) {
		if ( $ARGV[$argnum] =~ m/^\d+$/ ) {
			$element_id_accepted_as_new = $ARGV[$argnum];
		}
		else {
			print "incorrect 1st argument (element_id_accepted_as_new) : must be integer.\n";
			die "incorrect 1st argument (element_id_accepted_as_new) : must be integer.\n";
		}

	}
	elsif ( $argnum == 1 ) {
		if ( $ARGV[$argnum] =~ m/^\d+$/ ) {
			$current_element_id = $ARGV[$argnum];
		}
		else {
			print "incorrect 2nd argument (current_element_id) : must be integer.\n";
			die "incorrect 2nd argument (current_element_id) : must be integer.\n";
		}

	}
	elsif ( $argnum == 2 ) {
		if ( $ARGV[$argnum] =~ m/^ON$/i || $ARGV[$argnum] =~ m/^OFF$/i) {
			$VERBOSE = $ARGV[$argnum];
		}
		else {
			print "incorrect 3rd argument (VERBOSE) : must be ON or OFF.\n";
			die "incorrect 3rd argument (VERBOSE) : must be ON or OFF.\n";
		}

	}
	elsif ( $argnum == 3 ) {
		if ( $ARGV[$argnum] =~ m/^ON$/i || $ARGV[$argnum] =~ m/^OFF$/i) {
			$MAKE_FORMATDB = $ARGV[$argnum];
		}
		else {
			print "incorrect 3rd argument (MAKE_FORMATDB) : must be ON or OFF.\n";
			die "incorrect 3rd argument (MAKE_FORMATDB) : must be ON or OFF.\n";
		}
	}
	else {
		print
		  "The argument $ARGV[ $argnum ] has not been taking into account\n";
	}

}

if ( !defined($element_id_accepted_as_new) || !defined($current_element_id) || !defined($VERBOSE) || !defined($MAKE_FORMATDB)) {
	print "incorrect arguments : usage gen_faa_db.pl [INTEGER -> elet_id of 1st element] [INTEGER -> elet_id of 2nd element] [ON,OFF -> VERBOSE] [ON,OFF -> MAKE_FORMATDB]\n";
	die "incorrect arguments : usage gen_faa_db.pl [INTEGER -> elet_id of 1st element] [INTEGER -> elet_id of 2nd element] [ON,OFF -> VERBOSE] [ON,OFF -> MAKE_FORMATDB]\n";

}

# ouvre en pipant sur tee.pl pour �crire � la fois sur la sortie standard et dans le fichier de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db`;
open( LOG,
"| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db/gen_faa_db_for_${element_id_accepted_as_new}_VS_${current_element_id}.log"
);
print LOG
"\n---------------------------------------------------\ngen_faa_db.pl started at :",
  scalar(localtime),
" for new element id = $element_id_accepted_as_new VS elet id = $current_element_id\n" unless $VERBOSE =~ m/^OFF$/i;

my @array_of_elt_id = ();
if ( $element_id_accepted_as_new == $current_element_id ) {
	push( @array_of_elt_id, $element_id_accepted_as_new );
}
else {
	push( @array_of_elt_id, $element_id_accepted_as_new, $current_element_id );
}

#Loop : For $new_elt_id then $curr_elt_id

foreach my $elt_id (@array_of_elt_id) {
	my $accession   = undef;
	my $organism_id = undef;

#Origami database query : table elements where element_Id = $elt_id showing column element_Id, accession and organism_Id
	my $st = $ORIGAMI::dbh->prepare(
"SELECT element_id, accession, organism_id FROM elements WHERE element_id = $elt_id"
	);
	$st->execute
	  or die("Pb execute $!");
	while ( my $res = $st->fetchrow_hashref() ) {
		$accession   = $res->{accession};
		$organism_id = $res->{organism_id};
	}

	if ( !defined($accession) || !defined($organism_id) ) {
		die
"The element id $elt_id has no associated accession or organism_id in table elements\n";
	}

#if file DATADIR/Task_blast_all/tmp/fasta_formatdb/$elet_id_$accnum.faa do not exists
	if (
		!-e "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa"
	  )
	{    #-z File has zero size (is empty). -e File exists.

   #Open FASTA >  DATADIR/Task_blast_all/tmp/fasta_formatdb/$elet_id_$accnum.faa
		open( FASTA,
">$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa"
		  )
		  or die(
"Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa\n"
		  );

#Origami database query : all rows of table genes that have element_id == $elt_id, showing column gene_id, name, residues
		my $genes = $ORIGAMI::dbh->selectall_arrayref(
			"SELECT gene_id, name, residues FROM genes where element_id=$elt_id"
		);

		# pour chaques genes
		foreach my $gene ( @{$genes} ) {
			my ( $gene_id, $name, $residues ) = @{$gene};
			$name =~ s/\s/=/g;
			$name =~ tr/()/==/;

   #print FASTA ">${name}_${organism_id}_${element_id}_${gene_id}\n$residues\n";
			print FASTA
			  ">${name}_${organism_id}_${elt_id}_${gene_id}\n$residues\n";

		}

		#close  FASTA
		close(FASTA);

#if file DATADIR/Task_blast_all/tmp/fasta_formatdb/$elet_id_$accnum.faa exists and is not empty (zero size), print LOG creation of this file went well else if the file exists but has zero size, print LOG "***WARNING Accession: $accession. MAY BE Deleted entry with no genes information(script gen_faa_db.pl) and delete this file, else if the file do not exists write LOG creation of this file failed and exit script
		if (
			!-z "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa"
		  )
		{

			#checking if the number of lines looks good
			my $wc_output_unparsed =
`$SiteConfig::CMDDIR/more $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa | wc`;
			chomp($wc_output_unparsed);
			my @wc_output_parsed = split( /\s+/, $wc_output_unparsed );
			my $rowCount = $ORIGAMI::dbh->selectrow_array(
				qq{
                                      SELECT count(*)
                                         FROM genes
                                       WHERE element_id = ?
                }, undef,
				$elt_id
			);
			if ( $wc_output_parsed[1] == 0 ) {
				print LOG
"***WARNING counting the number of lines in the file $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa indicates that there are 0 line.\nPlease see LOG file $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db/gen_faa_db_for_${element_id_accepted_as_new}_VS_${current_element_id}.log for all the details.\n";
				exit(1);
			}
			if ( $rowCount == 0 ) {
				print LOG
"***WARNING counting the number of rows in the table genes for the element $elt_id indicates that there are 0 row.\nPlease see LOG file $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db/gen_faa_db_for_${element_id_accepted_as_new}_VS_${current_element_id}.log for all the details.\n";
				exit(1);
			}
			if ( $wc_output_parsed[1] != ( $rowCount * 2 ) ) {
				print LOG
"***WARNING counting the number of lines in the file $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa do NOT give same number as the number of rows (= $wc_output_parsed[1]) as in the table genes for the element $elt_id multiplied by 2 (= $rowCount).\nPlease see LOG file $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db/gen_faa_db_for_${element_id_accepted_as_new}_VS_${current_element_id}.log for all the details.\n";
				exit(1);
			}
			else {
				print LOG
"creation of fasta file for element_id = $elt_id ; Accession: $accession went well\n" unless $VERBOSE =~ m/^OFF$/i;
			}

			if($MAKE_FORMATDB =~ m/^ON$/i){

				#`$SiteConfig::FORMATDB -i DATADIR/Task_blast_all/tmp/fasta_formatdb/$elet_id_$accnum.faa` !!!!! RQ : do not use the option -o T as the header of the fasta file is not of the style of the NCBI.  In order to use the formatdb option -o T, the FASTA defline must follow a specific format. !!!!
				`$SiteConfig::FORMATDB -i $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa  -n $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}`;

				#if the files DATADIR/Task_blast_all/tmp/fasta_formatdb/$elet_id_$accnum.phr, DATADIR/Task_blast_all/tmp/fasta_formatdb/$elet_id_$accnum.pin, DATADIR/Task_blast_all/tmp/fasta_formatdb/$elet_id_$accnum.psq, DATADIR/Task_blast_all/tmp/fasta_formatdb/$elet_id_$accnum.psd, and  DATADIR/Task_blast_all/tmp/fasta_formatdb/$elet_id_$accnum.psi exists and are not empty, write LOG formatdb was successful
				if (
					!-z "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.phr"
					&& !
					-z "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.pin"
					&& !
					-z "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.psq"
					&& !
					-z "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.psd"
					&& !
					-z "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.psi"
				  )
				{
					`touch $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}_fasta_done`;
					print LOG
	"formadb on file $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa went well\n" unless $VERBOSE =~ m/^OFF$/i;
				}
				else {
					print LOG
	"***WARNING formadb on file $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa didn't went well as not all .phr, .pin, .psq, .psd and .psi files have not zero size.Please check what went wrong.\nPlease see LOG file $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db/gen_faa_db${element_id_accepted_as_new}_VS_${current_element_id}.log for all the details.\n";
					exit(1);
				}

			}else{
				print LOG
				"\nSkipping formatdb as requested in arguments\n";
				`touch $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}_fasta_done`;
			}


		} else {
			if (-z "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa"){

				print LOG
	"***WARNING Accession: $accession ; element_id = $elt_id resulted in an empty fasta file. It may be because this element_id is a deleted entry with no genes information.\nPlease see LOG file $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db/gen_faa_db${element_id_accepted_as_new}_VS_${current_element_id}.log for all the details.\n";
			
				print 
	"***WARNING Accession: $accession ; element_id = $elt_id resulted in an empty fasta file. It may be because this element_id is a deleted entry with no genes information.\nPlease see LOG file $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db/gen_faa_db${element_id_accepted_as_new}_VS_${current_element_id}.log for all the details.\n";
			
				`touch $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}_fasta_done`;

			} else {

				print LOG
	"***WARNING Accession: $accession ; element_id = $elt_id resulted in no fasta file.\nPlease see LOG file $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db/gen_faa_db${element_id_accepted_as_new}_VS_${current_element_id}.log for all the details.\n";
				print 
	"***WARNING Accession: $accession ; element_id = $elt_id resulted in no fasta file.\nPlease see LOG file $SiteConfig::LOGDIR/Task_blast_all/gen_faa_db/gen_faa_db${element_id_accepted_as_new}_VS_${current_element_id}.log for all the details.\n";
				exit(1);

			}
		}
	}
	else {
		print LOG
"The file $SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb/${elt_id}_${accession}.faa already exists, creation of the fasta file for the element_id $elt_id is skipped.\n";
	}

}

print LOG "gen_faa_db.pl completed succesfully at :", scalar(localtime),
  "\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/i;
close(LOG);
