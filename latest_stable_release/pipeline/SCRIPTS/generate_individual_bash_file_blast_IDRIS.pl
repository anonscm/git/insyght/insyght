#!/usr/local/bin/perl
#
# generate_individual_bash_file_blast_IDRIS.pl Permet de générer les fichier bash de Blast pour l'IDRIS pour 2 élément donnés
#
# Date : 01/2014
# LT
######
#GOAL : This script does the following tasks :
#create a batchfile to perform the following blast on the cluster :
#formatdb of the new_elt_id.faa if needed
#formatdb of the curr_elt_id.faa if needed
#new_elt_id VS curr_elt_id if needed
#and curr_elt_id VS new_elt_id if new_elt_id != curr_elt_id if needed
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
# 
# This file is part of Pipeline origami
# 
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
# 
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# 

use strict;
use SiteConfig;
use lib '$SiteConfig::SCRIPTSDIR';
use BlastConfig;
use Time::HiRes qw(sleep);

#script level variable
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $master_mol_id = undef;
#my $accnum_correspondance_curr_molecule_id_accepted_as_new;
my $sub_mol_id = undef;
#my $accnum_correspondance_current_molecule_id;
my $VERBOSE = "OFF";
my $BI_DIRECTIONAL_BLAST = "OFF";
my $molecule_type = undef;
my $program_to_use = undef; # can be BLAST or PLAST ; set by option -PROGRAM_TO_USE in Task_blast_all_generator_for_IDRIS
my $number_processors_to_use_per_blast = 1;# -a for plast ; -num_threads for blastp ; set by option -NUM_PROC_PER_BLAST_PS in Task_blast_all_generator_for_IDRIS
my $prefix_new_old_fasta_query = undef;
my $prefix_new_old_blast_bank = ""; # can be empty for database all
my $GENERATE_WITH_ENVIRONENT_VARIABLES = "OFF";
my $EXPORT_QSUB_LOG_DIR = "";
my $EXPORT_PLAST_EXEC_PATH = "";
my $EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR = "";
my $EXPORT_BLAST_OUTPUT_DIR = "";
my $EXPORT_MARKER_DONE_BLAST_DIR = "";

#get the arguments or exit with usage if the arguments are incorrect
foreach my $argnum ( 0 .. $#ARGV ) {

	if ( $argnum == 0 ) {
		if ( $ARGV[$argnum] =~ m/^[\d_]+$/ ) {
			$master_mol_id = $ARGV[$argnum];
		}
		else {
			print
"incorrect 1st argument (master_mol_id) : must be integer or _\n";
			die
"incorrect 1st argument (master_mol_id) : must be integer or _\n";
		}

	}
	elsif ( $argnum == 1 ) {
		if ( $ARGV[$argnum] =~ m/^[\d_]+$/ ) {
			$sub_mol_id = $ARGV[$argnum];
		} elsif ( $ARGV[$argnum] =~ m/^all$/ ) {
			$sub_mol_id = $ARGV[$argnum];
		}
		else {
			print
"incorrect 2nd argument (sub_mol_id) : must be integer or _ or all\n";
			die
"incorrect 2nd argument (sub_mol_id) : must be integer or _ or all\n";
		}

	}
	elsif ( $argnum == 2 ) {
		if ( $ARGV[$argnum] =~ m/^ON$/i || $ARGV[$argnum] =~ m/^OFF$/i ) {
			$VERBOSE = $ARGV[$argnum];
		}
		else {
			print "incorrect 3rd argument (VERBOSE) : must be ON or OFF.\n";
			die "incorrect 3rd argument (VERBOSE) : must be ON or OFF.\n";
		}
	}
	elsif ( $argnum == 3 ) {
		if ( $ARGV[$argnum] =~ m/^ON$/i || $ARGV[$argnum] =~ m/^OFF$/i ) {
			$BI_DIRECTIONAL_BLAST = $ARGV[$argnum];
		}
		else {
			print "incorrect 4th argument (BI_DIRECTIONAL_BLAST) : must be ON or OFF.\n";
			die "incorrect 4th argument (BI_DIRECTIONAL_BLAST) : must be ON or OFF.\n";
		}
	}
	elsif ( $argnum == 4 ) {
		if ( $ARGV[$argnum] =~ m/^.+$/i ) {
			$molecule_type = $ARGV[$argnum];
		}
		else {
			print "incorrect 5th argument (molecule_type) : $ARGV[$argnum]\n";
			die "incorrect 5th argument (molecule_type) : $ARGV[$argnum]\n";
		}
	}
	elsif ( $argnum == 5 ) {
		if (	$ARGV[ $argnum ] =~ m/^BLAST$/i
			|| $ARGV[ $argnum ] =~ m/^PLAST$/i 
		) {
			$program_to_use = $ARGV[$argnum];
		}
		else {
			print "incorrect 6th argument (program_to_use) : $ARGV[ $argnum ]\n";
			die "incorrect 6th argument (program_to_use) : $ARGV[ $argnum ]\n";
		}
	}
	elsif ( $argnum == 6 ) {
		if (	$ARGV[ $argnum ] =~ m/^\d+$/i
		) {
			$number_processors_to_use_per_blast = $ARGV[$argnum];
		}
		else {
			print "incorrect 7th argument (number_processors_to_use_per_blast) : $ARGV[ $argnum ] but must be digit\n";
			die "incorrect 7th argument (number_processors_to_use_per_blast) : $ARGV[ $argnum ] but must be digit\n";
		}
	}
#my $prefix_new_old_fasta_query = undef;
	elsif ( $argnum == 7 ) {
		if (	$ARGV[ $argnum ] =~ m/^.+$/i
		) {
			$prefix_new_old_fasta_query = $ARGV[$argnum];
		}
		else {
			print "incorrect 8th argument (prefix_new_old_fasta_query) : $ARGV[ $argnum ]\n";
			die "incorrect 8th argument (prefix_new_old_fasta_query) : $ARGV[ $argnum ]\n";
		}
	}
#my $prefix_new_old_blast_bank = undef;
	elsif ( $argnum == 8 ) {
		if (	$ARGV[ $argnum ] =~ m/^.+$/i
		) {
			$prefix_new_old_blast_bank = $ARGV[$argnum];
			if ( $prefix_new_old_blast_bank eq "EMPTY" ) {
				$prefix_new_old_blast_bank = "";
			}
		}
		else {
			print "incorrect 9th argument (prefix_new_old_blast_bank) : $ARGV[ $argnum ]\n";
			die "incorrect 9th argument (prefix_new_old_blast_bank) : $ARGV[ $argnum ]\n";
		}
	}
# GENERATE_WITH_ENVIRONENT_VARIABLES
	elsif ( $argnum == 9 ) {

		if ( $ARGV[$argnum] =~ m/^ON$/i || $ARGV[$argnum] =~ m/^OFF$/i ) {
			$GENERATE_WITH_ENVIRONENT_VARIABLES = $ARGV[$argnum];
		}
		else {
			print "incorrect 9th argument (GENERATE_WITH_ENVIRONENT_VARIABLES) : must be ON or OFF.\n";
			die "incorrect 9th argument (GENERATE_WITH_ENVIRONENT_VARIABLES) : must be ON or OFF.\n";
		}
	}
# EXPORT_QSUB_LOG_DIR
	elsif ( $argnum == 10 ) {
		if ( $ARGV[$argnum] =~ m/^[^-].+$/i ) {
			$EXPORT_QSUB_LOG_DIR = $ARGV[$argnum];
		}
		else {
			print "incorrect 10th argument (EXPORT_QSUB_LOG_DIR) : ".$ARGV[$argnum].".\n";
			die "incorrect 10th argument (EXPORT_QSUB_LOG_DIR) : ".$ARGV[$argnum].".\n";
		}
	}
# EXPORT_PLAST_EXEC_PATH
	elsif ( $argnum == 11 ) {
		if ( $ARGV[$argnum] =~ m/^[^-].+$/i ) {
			$EXPORT_PLAST_EXEC_PATH = $ARGV[$argnum];
		}
		else {
			print "incorrect 11th argument (EXPORT_PLAST_EXEC_PATH) : ".$ARGV[$argnum].".\n";
			die "incorrect 11th argument (EXPORT_PLAST_EXEC_PATH) : ".$ARGV[$argnum].".\n";
		}
	}
# EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR
	elsif ( $argnum == 12 ) {
		if ( $ARGV[$argnum] =~ m/^[^-].+$/i ) {
			$EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR = $ARGV[$argnum];
		}
		else {
			print "incorrect 12th argument (EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR) : ".$ARGV[$argnum].".\n";
			die "incorrect 12th argument (EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR) : ".$ARGV[$argnum].".\n";
		}
	}
# EXPORT_BLAST_OUTPUT_DIR
	elsif ( $argnum == 13 ) {
		if ( $ARGV[$argnum] =~ m/^[^-].+$/i ) {
			$EXPORT_BLAST_OUTPUT_DIR = $ARGV[$argnum];
		}
		else {
			print "incorrect 13th argument (EXPORT_BLAST_OUTPUT_DIR) : ".$ARGV[$argnum].".\n";
			die "incorrect 13th argument (EXPORT_BLAST_OUTPUT_DIR) : ".$ARGV[$argnum].".\n";
		}
	}
# EXPORT_MARKER_DONE_BLAST_DIR
	elsif ( $argnum == 14 ) {
		if ( $ARGV[$argnum] =~ m/^[^-].+$/i ) {
			$EXPORT_MARKER_DONE_BLAST_DIR = $ARGV[$argnum];
		}
		else {
			print "incorrect 14th argument (EXPORT_MARKER_DONE_BLAST_DIR) : ".$ARGV[$argnum].".\n";
			die "incorrect 14th argument (EXPORT_MARKER_DONE_BLAST_DIR) : ".$ARGV[$argnum].".\n";
		}
	}
# end of args
	else {
		print "The argument $ARGV[ $argnum ] has not been taking into account\n";
		die ( "The argument $ARGV[ $argnum ] has not been taking into account\n" );
	}

}


if (   !defined($master_mol_id)
	|| !defined($sub_mol_id)
	|| !defined($molecule_type) 
	|| !defined($program_to_use)
	|| !defined($prefix_new_old_fasta_query)
#	|| !defined($prefix_new_old_blast_bank)
)
{
	print
"incorrect arguments : usage generate_individual_bash_file_blast_IDRIS.pl [INTEGER -> master_mol_id] [INTEGER -> sub_mol_id] [ON,OFF -> VERBOSE] [ON,OFF -> BI_DIRECTIONAL_BLAST] [ALPHANUM -> molecule_type] [BLAST or PLAST -> program to use] [INTEGER -> number processors to use per blast] [prefix_new_old_fasta_query] [OPTIONAL prefix_new_old_blast_bank]\n";
	die
"incorrect arguments : usage generate_individual_bash_file_blast_IDRIS.pl [INTEGER -> master_mol_id] [INTEGER -> sub_mol_id] [ON,OFF -> VERBOSE] [ON,OFF -> BI_DIRECTIONAL_BLAST] [ALPHANUM -> molecule_type] [BLAST or PLAST -> program to use] [INTEGER -> number processors to use per blast] [prefix_new_old_fasta_query] [OPTIONAL prefix_new_old_blast_bank]\n";

}


# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/launch_blast`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/`;


open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/launch_blast/generate_individual_bash_file_blast_IDRIS_for_${molecule_type}_${master_mol_id}_VS_${molecule_type}_${sub_mol_id}.log" );

# NO print if no problem, we want a silent output if everything runs smooth
#print LOG "\n---------------------------------------------------\ngenerate_individual_bash_file_blast_IDRIS.pl started at :", scalar(localtime), " for ${molecule_type}_${master_mol_id}_VS_${molecule_type}_${sub_mol_id}\n" unless $VERBOSE =~ m/^OFF$/i;


sub sub_print_batch_file {
	my $master_mol_id_IT = shift;
	my $sub_mol_id_IT = shift;

	`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/`;

	if( ! -e "$SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast.ll"){
		
		open( BLAST_BATCHFILE, "> $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast.ll" ) or die ("Can not open $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast.ll\n");

		# GENERATE_WITH_ENVIRONENT_VARIABLES
			# ${QSUB_LOG_DIR}
			# ${PLAST_EXEC_PATH}
			# ${FASTA_MAKEBLASTDB_INPUT_DIR}
			# ${BLAST_OUTPUT_DIR}
			# ${MARKER_DONE_BLAST_DIR}

		# BlastConfig::generate_command($program_to_use, $number_processors_to_use, $database_file, $query_file, $out_file)
		my $QSUB_LOG_DIR = "$SiteConfig::LOGDIR/Task_blast_all/launch_blast";
		if ($EXPORT_QSUB_LOG_DIR ne "") {
			$QSUB_LOG_DIR = $EXPORT_QSUB_LOG_DIR;
		}
		my $FASTA_MAKEBLASTDB_INPUT_DIR = "$SiteConfig::DATADIR/Task_blast_all/tmp/fasta_formatdb";
		if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
			$FASTA_MAKEBLASTDB_INPUT_DIR = "\${FASTA_MAKEBLASTDB_INPUT_DIR}";
		}
		my $BLAST_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_output";
		if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
			$BLAST_OUTPUT_DIR = "\${BLAST_OUTPUT_DIR}";
		}
		my $MARKER_DONE_BLAST_DIR = "$SiteConfig::DATADIR/Task_blast_all/tmp/blast_done";
		if ($GENERATE_WITH_ENVIRONENT_VARIABLES =~ m/^ON$/i) {
			$MARKER_DONE_BLAST_DIR = "\${MARKER_DONE_BLAST_DIR}";
		}

		`mkdir -p $QSUB_LOG_DIR`;

		my $string_command_IT = BlastConfig::generate_command(
							$program_to_use,
							$number_processors_to_use_per_blast,
							"$FASTA_MAKEBLASTDB_INPUT_DIR/${prefix_new_old_blast_bank}/${molecule_type}_${sub_mol_id_IT}",
							"$FASTA_MAKEBLASTDB_INPUT_DIR/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}.faa",
							"$BLAST_OUTPUT_DIR/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast",
							# FOR cluster that need writting on local disk before copying "/tmp/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast",
							$GENERATE_WITH_ENVIRONENT_VARIABLES
							);


		print BLAST_BATCHFILE "#!/bin/bash\n";
		#EXPORT_PLAST_EXEC_PATH
		if ($EXPORT_PLAST_EXEC_PATH ne "") {
			print BLAST_BATCHFILE "export PLAST_EXEC_PATH=$EXPORT_PLAST_EXEC_PATH\n";
		}
		#EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR
		if ($EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR ne "") {
			print BLAST_BATCHFILE "export FASTA_MAKEBLASTDB_INPUT_DIR=$EXPORT_FASTA_MAKEBLASTDB_INPUT_DIR\n";
		}
		#EXPORT_BLAST_OUTPUT_DIR
		if ($EXPORT_BLAST_OUTPUT_DIR ne "") {
			print BLAST_BATCHFILE "export BLAST_OUTPUT_DIR=$EXPORT_BLAST_OUTPUT_DIR\n";
		}
		#EXPORT_MARKER_DONE_BLAST_DIR
		if ($EXPORT_MARKER_DONE_BLAST_DIR ne "") {
			print BLAST_BATCHFILE "export MARKER_DONE_BLAST_DIR=$EXPORT_MARKER_DONE_BLAST_DIR\n";
		}
		print BLAST_BATCHFILE "#\$ -S /bin/bash
# (n) No mail is sent ; Avertir au début (b)egin, à la fin (e)nd, à l'éliminaton (a)bort et
# à la suspension (s)uspend d'un job
#\$ -m n
# Adresse mail à laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#\$ -o $QSUB_LOG_DIR/launch_blast_for_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.out
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -e $QSUB_LOG_DIR/launch_blast_for_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.err
# Lance la commande depuis le répertoire où est lancé le script
#\$ -cwd \n";

		# print blast command
		print BLAST_BATCHFILE "$SiteConfig::CMDDIR/mkdir -p $BLAST_OUTPUT_DIR/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT} && $string_command_IT && touch $MARKER_DONE_BLAST_DIR/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast_done\n";
		# RQ : gzip to put before marker file done
		# && gzip $BLAST_OUTPUT_DIR/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast
		# FOR cluster that need writting on local disk before copying print BLAST_BATCHFILE "$SiteConfig::CMDDIR/mkdir -p $BLAST_OUTPUT_DIR/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT} && $string_command_IT && touch $MARKER_DONE_BLAST_DIR/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast_done && gzip /tmp/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast && mv /tmp/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast.gz $BLAST_OUTPUT_DIR/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast.gz\n";

		close(BLAST_BATCHFILE);

	}else{
		print LOG "file $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast.ll already exists, skipping generation of blast command\n";
		die ( "file $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/blast/${prefix_new_old_fasta_query}/${molecule_type}_${master_mol_id_IT}/batch_${molecule_type}_${master_mol_id_IT}_VS_${molecule_type}_${sub_mol_id_IT}.blast.ll already exists, skipping generation of blast command\n" );
	}

}

sub_print_batch_file ($master_mol_id, $sub_mol_id);
if ( $BI_DIRECTIONAL_BLAST =~ m/^ON$/i ) {
	sub_print_batch_file ($sub_mol_id, $master_mol_id);
}



# NO print if no problem, we want a silent output if everything runs smooth
#print LOG "generate_individual_bash_file_blast_IDRIS.pl completed succesfully at : ", scalar(localtime), "\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/i;
close(LOG);






