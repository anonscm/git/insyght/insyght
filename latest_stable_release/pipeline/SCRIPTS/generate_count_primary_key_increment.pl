#!/usr/local/bin/perl
#
# full name = generate_count_primary_key_increment.pl
#
# This script generate bash script file that will be used to calculate primary keys
#
#
# Pipeline origami Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2014)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use SiteConfig;
use BlastConfig;


# whole script scoped variable
my $path_log_file = "$SiteConfig::LOGDIR/generate_count_primary_key_increment.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/generate_count_primary_key_increment.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/generate_count_primary_key_increment.error";
my $output_backtick = "";
my $VERBOSE = "ON";
my $TSV_INPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/tsv";
my $LL_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/bash_script_submit";
my $COUNT_OUTPUT_DIR = "$SiteConfig::DATADIR/Task_add_alignments/tmp/count_primary_key_increment/count_files";
my $LOG_DIR = "$SiteConfig::LOGDIR/Task_add_alignments";
my @alignment_params_table_tsv_file            = ();
my $BLAST_TAXO_GRANULARITY = $BlastConfig::taxonomic_granularity;
my $molecule_type;


sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		} else {
			die_with_error_mssg("incorrect -VERBOSE argument ; usage : perl generate_count_primary_key_increment.pl -VERBOSE = {ON, OFF}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-TSV_INPUT_DIR$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^.+$/i)
		{
			$TSV_INPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -TSV_INPUT_DIR argument ; usage : perl generate_count_primary_key_increment.pl  -TSV_INPUT_DIR {PATH_TO_DIRECTORY}";
			die_with_error_mssg("incorrect -TSV_INPUT_DIR argument ; usage : perl generate_count_primary_key_increment.pl  -TSV_INPUT_DIR {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-LL_OUTPUT_DIR$/ ) {
		if (  $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$LL_OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -LL_OUTPUT_DIR argument ; usage : perl generate_count_primary_key_increment.pl -LL_OUTPUT_DIR {PATH_TO_DIRECTORY}";
			die_with_error_mssg("incorrect -LL_OUTPUT_DIR argument ; usage : perl generate_count_primary_key_increment.pl -LL_OUTPUT_DIR {PATH_TO_DIRECTORY}");
		}
# COUNT_OUTPUT_DIR
	} elsif ( $ARGV[$argnum] =~ m/^-COUNT_OUTPUT_DIR$/ ) {
		if (  $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$COUNT_OUTPUT_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -COUNT_OUTPUT_DIR argument ; usage : perl generate_count_primary_key_increment.pl -COUNT_OUTPUT_DIR {PATH_TO_DIRECTORY}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-LOG_DIR$/ ) {
		if (  $ARGV[ $argnum + 1 ] =~ m/^.+$/i )
		{
			$LOG_DIR = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -LOG_DIR argument ; usage : perl generate_count_primary_key_increment.pl -LOG_DIR {PATH_TO_DIRECTORY}";
			die_with_error_mssg("incorrect -LOG_DIR argument ; usage : perl generate_count_primary_key_increment.pl -LOG_DIR {PATH_TO_DIRECTORY}");
		}
	}
}


# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $LOG_DIR/generate_count_primary_key_increment.log");
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
print LOG "\n---------------------------------------------------\n generate_count_primary_key_increment.pl started at :", scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# print in and out directory
print LOG "---------------------------------------------------\nThe TSV input directory is : $TSV_INPUT_DIR\nThe LL_OUTPUT_DIR output directory is :$LL_OUTPUT_DIR \n" unless $VERBOSE =~ m/^OFF$/;


if ($BLAST_TAXO_GRANULARITY =~ m/^ORGANISM_VS_ALL$/) {
	$molecule_type = "orga";
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^CLUSTER_ORGANISM$/) {
	$molecule_type = "orgaCluster";
	#die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.");
} elsif ($BLAST_TAXO_GRANULARITY =~ m/^ELEMENT_VS_ELEMENT$/) {
	#$molecule_type = "element";
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not supported : $BLAST_TAXO_GRANULARITY.\nSorry but at the current time this granularity is not supported by the rest of the pipeline. Please re-run the pipeline with a granularity that is supported.\n");
} else {
	die_with_error_mssg("BLAST_TAXO_GRANULARITY not recognized : $BLAST_TAXO_GRANULARITY.\nPlease modify the file BlastConfig appropriately\n");
}

# preparing directories
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::LOGDIR/Task_blast_all/`;
`$SiteConfig::CMDDIR/mkdir -p $COUNT_OUTPUT_DIR`;
`$SiteConfig::CMDDIR/mkdir -p $LL_OUTPUT_DIR`;
`$SiteConfig::CMDDIR/mkdir -p $LOG_DIR`;
# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
print LOG "Deleting files under $LL_OUTPUT_DIR/* \n" unless $VERBOSE =~ m/^OFF$/;
$output_backtick .= `rm -rf $LL_OUTPUT_DIR/*`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete preparing directories :\n$output_backtick \n $!");
}




print LOG "Listing input files...\n" unless $VERBOSE =~ m/^OFF$/;

my $count_skipped_files = 0;
sub sub_list_alignment_params_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or die_with_error_mssg("Unable to open $path: $!");
    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);
    for (@files) {
        if (-d $_) {
            # directory, run recursively
            sub_list_alignment_params_files_recursively ($_);
        } elsif ( $_ =~ m/^(.*)\/(\d+)_and_(\d+)_alignment_params_table\.tsv$/ ) {
		push ( @alignment_params_table_tsv_file, $_);
        } elsif ( $_ =~ m/^(.*)\/(\d+)_and_(\d+)_alignment_params_table\.tsv\.gz$/ ) {
		my $dir_IT = $1;
		my $master_elet_id = $2;
		my $sub_elet_id = $3;
		my $core_name_file = "${master_elet_id}_and_${sub_elet_id}";
		if ( -e "${dir_IT}/${core_name_file}_alignment_params_table.tsv" ) {
			# exists both as not gzip and gzip, ignore
		} else {
			push ( @alignment_params_table_tsv_file, $_);
		}
	} else {
            die_with_error_mssg("Error in sub_list_alignment_params_files_recursively : the file $_ does not match the appropriate regex");
        }
    }
}
sub_list_alignment_params_files_recursively("$TSV_INPUT_DIR/alignment_params/");
print LOG scalar(@alignment_params_table_tsv_file)." alignment_params_table_tsv_file found to be treated.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "Done listing input files\n" unless $VERBOSE =~ m/^OFF$/;


# for each *_alignment_params_table.tsv file and its associate *_alignment_table.tsv file, replace Forward and Reverse by appropriate Iary key alignment_param_id
print LOG "Generating bash file...\n" unless $VERBOSE =~ m/^OFF$/;
my $count_loop_generate = 0;
foreach my $alignment_params_table_tsv_file_IT (@alignment_params_table_tsv_file) {

	$count_loop_generate++;
	print LOG "bash file $count_loop_generate / ".scalar(@alignment_params_table_tsv_file)."\n" unless $VERBOSE =~ m/^OFF$/;

	my $master_elet_id;
	my $sub_elet_id;
	if ( $alignment_params_table_tsv_file_IT =~ m/^.*\/(\d+)_and_(\d+)_alignment_params_table\.tsv(?:\.gz)?$/ ) {
		$master_elet_id = $1;
		$sub_elet_id = $2;
	} else {
		die_with_error_mssg("could not parse both elet_id for file $alignment_params_table_tsv_file_IT.");
	}

	my $core_name_file = "${master_elet_id}_and_${sub_elet_id}";

	#print LOG "get associated files\n" unless $VERBOSE =~ m/^OFF$/;

	#get associated alignment_table.tsv file
	my $associated_alignment_table_tsv_file = "$TSV_INPUT_DIR/alignments/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_table.tsv";
	if ( -e "$associated_alignment_table_tsv_file" ) {
		# do nothing
	} elsif ( -e "${associated_alignment_table_tsv_file}.gz" ) {
		$associated_alignment_table_tsv_file .= ".gz";
	} else {
		die_with_error_mssg("could not associate file $associated_alignment_table_tsv_file (or its gzip version).");
	}

	#get associated alignment_pairs_table_tsv_file file
	my $associated_alignment_pairs_table_tsv_file = "$TSV_INPUT_DIR/alignment_pairs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_alignment_pairs_table.tsv";
	if ( -e "$associated_alignment_pairs_table_tsv_file" ) {
		# do nothing
	} elsif ( -e "${associated_alignment_pairs_table_tsv_file}.gz" ) {
		$associated_alignment_pairs_table_tsv_file .= ".gz";
	} else {
		die_with_error_mssg("could not associate file $associated_alignment_pairs_table_tsv_file (or its gzip version).");
	}


	# *_tandem_dups_table.tsv.gz
	# "COPY tandem_dups (tandem_dups_id, alignment_param_id, gene_id_single_is_q, single_organims_id, single_element_id, single_gene_id, tandem_organims_id, tandem_element_id, tandem_gene_id, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, idx_in_dup) FROM stdin;\n"
	# alignment_param_id
	# -REPLACEMENT_TANDEM_DUPS_PRIMARY_ID
	#get associated alignment_table.tsv file
	my $associated_tandem_dups_table_tsv_file = "$TSV_INPUT_DIR/tandem_dups/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_tandem_dups_table.tsv";
	if ( -e "$associated_tandem_dups_table_tsv_file" ) {
		# do nothing
	} elsif ( -e "${associated_tandem_dups_table_tsv_file}.gz" ) {
		$associated_tandem_dups_table_tsv_file .= ".gz";
	} else {
		$associated_tandem_dups_table_tsv_file = "";
		# not mandatory 
	}


	# _isBranchedToAnotherSynteny_table.tsv
	# COPY isBranchedToAnotherSynteny (alignment_id_branched, alignment_param_id, branche_on_q_gene_id, branche_on_s_gene_id) FROM stdin;\n";
	# alignment_id_branched
	# alignment_param_id
	my $associated_isBranchedToAnotherSynteny_table_tsv_file = "$TSV_INPUT_DIR/isBranchedToAnotherSynteny/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_isBranchedToAnotherSynteny_table.tsv";
	if ( -e "$associated_isBranchedToAnotherSynteny_table_tsv_file" ) {
		# do nothing
	} elsif ( -e "${associated_isBranchedToAnotherSynteny_table_tsv_file}.gz" ) {
		$associated_isBranchedToAnotherSynteny_table_tsv_file .= ".gz";
	} else {
		$associated_isBranchedToAnotherSynteny_table_tsv_file = "";
		# not mandatory
	}


	# output gene fusion (_protFusion_table.tsv)  
	# COPY prot_fusion ("prot_fusion_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, isMirrorData, s_rank_among_all_s_fusions, lengthTotalCoverageSOnQ, lenghtNewContribCoverageSOnQ, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, geneIdBestSingleLongMatchHidingProtFusion, numberSingleLongMatchHidingProtFusion, numberProtFusionDetectedBeforeThisOne
	# -REPLACEMENT_PROT_FUSION_PRIMARY_ID
	my $associated_prot_fusion_table_tsv_file = "$TSV_INPUT_DIR/protFusion/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_protFusion_table.tsv";
	if ( -e "$associated_prot_fusion_table_tsv_file" ) {
		# do nothing
	} elsif ( -e "${associated_prot_fusion_table_tsv_file}.gz" ) {
		$associated_prot_fusion_table_tsv_file .= ".gz";
	} else {
		$associated_prot_fusion_table_tsv_file = "";
		# not mandatory
	}


	# print out gene families where multiple match within 1% of best bits score (_closeBestMatchs_table.tsv)  
	# COPY close_best_match (close_best_match_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, is_mirror_data, number_of_close_matchs, contain_bdbh, pid_best_match, evalue_best_match, bits_best_match, list_close_match_gene_ids") FROM stdin;
	# -REPLACEMENT_CLOSEBESTMATCHS_PRIMARY_ID
	my $associated_closeBestMatchs_table_tsv_file = "$TSV_INPUT_DIR/closeBestMatchs/${molecule_type}_${master_elet_id}/${master_elet_id}_and_${sub_elet_id}_closeBestMatchs_table.tsv";
	if ( -e "$associated_closeBestMatchs_table_tsv_file" ) {
		# do nothing
	} elsif ( -e "${associated_closeBestMatchs_table_tsv_file}.gz" ) {
		$associated_closeBestMatchs_table_tsv_file .= ".gz";
	} else {
		$associated_closeBestMatchs_table_tsv_file = "";
		# not mandatory
	}


	`$SiteConfig::CMDDIR/mkdir -p $LL_OUTPUT_DIR/${molecule_type}_${master_elet_id}/`;

	#print LOG "print batchfile\n" unless $VERBOSE =~ m/^OFF$/;

	open( BATCHFILE,
	"> $LL_OUTPUT_DIR/${molecule_type}_${master_elet_id}/count_primary_key_increment_${core_name_file}.ll"
	  )
	  or die_with_error_mssg("Can not open $LL_OUTPUT_DIR/${molecule_type}_${master_elet_id}/count_primary_key_increment_${core_name_file}.ll");

	print BATCHFILE "#!/bin/bash
#\$ -S /bin/bash
# (n) No mail is sent ; Avertir au début (b)egin, à la fin (e)nd, à l'éliminaton (a)bort et
# à la suspension (s)uspend d'un job
#\$ -m n
# Adresse mail à laquelle envoyer ces informations
#\$ -M $SiteConfig::USERMAIL
# Sortie standard
# Vous pouvez utiliser '-j y' pour ajouter stderr avec stdout
#\$ -o $LOG_DIR/count_primary_key_increment_${core_name_file}.out
# Sortie d'erreur (ne pas utiliser cette option avec '-j y')
#\$ -e $LOG_DIR/count_primary_key_increment_${core_name_file}.error
# Lance la commande depuis le répertoire où est lancé le script
#\$ -cwd
\n";

	# create output count file
	my $output_count_file = "$COUNT_OUTPUT_DIR/${molecule_type}_${master_elet_id}/count_primary_key_increment_${core_name_file}.out";
	print BATCHFILE "mkdir -p $COUNT_OUTPUT_DIR/${molecule_type}_${master_elet_id}/\n";
	print BATCHFILE "echo \"\" > $output_count_file\n";

	# alignment_params_table_tsv_file_IT
	my $gzip_flag = 0;
	if ( $alignment_params_table_tsv_file_IT ne "") {
		if ($alignment_params_table_tsv_file_IT =~ m/^(.*)\.gz$/ ) {
			$gzip_flag = 1;
			print BATCHFILE "gunzip $alignment_params_table_tsv_file_IT\n";
			$alignment_params_table_tsv_file_IT = $1;
		}
		print BATCHFILE "echo \"count_alignment_params_file : \" >> $output_count_file\n";
		print BATCHFILE "wc -l $alignment_params_table_tsv_file_IT | cut -f 1 -d \" \" >> $output_count_file\n";
		#print BATCHFILE "echo \"\\n\" >> $output_count_file\n";
		if ( $gzip_flag == 1 ) {
			print BATCHFILE "gzip $alignment_params_table_tsv_file_IT\n";
		}
	} else {
		die_with_error_mssg("file alignment_params_table_tsv_file_IT is empty : $alignment_params_table_tsv_file_IT");
	}

	# $associated_alignment_table_tsv_file
	$gzip_flag = 0;
	if ( $associated_alignment_table_tsv_file ne "") {
		if ($associated_alignment_table_tsv_file =~ m/^(.*)\.gz$/ ) {
			$gzip_flag = 1;
			print BATCHFILE "gunzip $associated_alignment_table_tsv_file\n";
			$associated_alignment_table_tsv_file = $1;
		}
		print BATCHFILE "echo \"count_alignment_file : \" >> $output_count_file\n";
		print BATCHFILE "wc -l $associated_alignment_table_tsv_file | cut -f 1 -d \" \" >> $output_count_file\n";
		#print BATCHFILE "echo \"\\n\" >> $output_count_file\n";
		if ( $gzip_flag == 1 ) {
			print BATCHFILE "gzip $associated_alignment_table_tsv_file\n";
		}
	} else {
		die_with_error_mssg("file associated_alignment_table_tsv_file is empty : $associated_alignment_table_tsv_file");
	}

	# associated_tandem_dups_table_tsv_file
	$gzip_flag = 0;
	if ( $associated_tandem_dups_table_tsv_file ne "") {
		if ($associated_tandem_dups_table_tsv_file =~ m/^(.*)\.gz$/ ) {
			$gzip_flag = 1;
			print BATCHFILE "gunzip $associated_tandem_dups_table_tsv_file\n";
			$associated_tandem_dups_table_tsv_file = $1;
		}
		print BATCHFILE "echo \"count_tandem_dups_file : \" >> $output_count_file\n";
		print BATCHFILE "cut -f 1 $associated_tandem_dups_table_tsv_file | sort -u | wc -l | cut -f 1 -d \" \" >> $output_count_file\n";
		#print BATCHFILE "echo \"\\n\" >> $output_count_file\n";
		if ( $gzip_flag == 1 ) {
			print BATCHFILE "gzip $associated_tandem_dups_table_tsv_file\n";
		}
	} else {
		#die_with_error_mssg("file associated_tandem_dups_table_tsv_file is empty : $associated_tandem_dups_table_tsv_file");
		# not mandatory
	}


	# $associated_prot_fusion_table_tsv_file
	$gzip_flag = 0;
	if ( $associated_prot_fusion_table_tsv_file ne "") {
		if ($associated_prot_fusion_table_tsv_file =~ m/^(.*)\.gz$/ ) {
			$gzip_flag = 1;
			print BATCHFILE "gunzip $associated_prot_fusion_table_tsv_file\n";
			$associated_prot_fusion_table_tsv_file = $1;
		}
		print BATCHFILE "echo \"count_prot_fusion_file : \" >> $output_count_file\n";
		print BATCHFILE "cut -f 1 $associated_prot_fusion_table_tsv_file | sort -u | wc -l | cut -f 1 -d \" \" >> $output_count_file\n";
		#print BATCHFILE "echo \"\\n\" >> $output_count_file\n";
		if ( $gzip_flag == 1 ) {
			print BATCHFILE "gzip $associated_prot_fusion_table_tsv_file\n";
		}
	} else {
		#die_with_error_mssg("file associated_prot_fusion_table_tsv_file is empty : $associated_prot_fusion_table_tsv_file");
		# not mandatory
	}


	# $associated_closeBestMatchs_table_tsv_file
	$gzip_flag = 0;
	if ( $associated_closeBestMatchs_table_tsv_file ne "") {
		if ($associated_closeBestMatchs_table_tsv_file =~ m/^(.*)\.gz$/ ) {
			$gzip_flag = 1;
			print BATCHFILE "gunzip $associated_closeBestMatchs_table_tsv_file\n";
			$associated_closeBestMatchs_table_tsv_file = $1;
		}
		print BATCHFILE "echo \"count_closeBestMatchs_file : \" >> $output_count_file\n";
		print BATCHFILE "wc -l $associated_closeBestMatchs_table_tsv_file | cut -f 1 -d \" \" >> $output_count_file\n";
		#print BATCHFILE "echo \"\\n\" >> $output_count_file\n";
		if ( $gzip_flag == 1 ) {
			print BATCHFILE "gzip $associated_closeBestMatchs_table_tsv_file\n";
		}
	} else {
		#die_with_error_mssg("file associated_closeBestMatchs_table_tsv_file is empty : $associated_closeBestMatchs_table_tsv_file");
		# not mandatory
	}

	#print BATCHFILE "touch $LOG_DIR/count_primary_key_increment_${core_name_file}.done\n";
	# -s FILE exists and has a size greater than zero
	print BATCHFILE "if [ -s \"$output_count_file\" ]
then
\ttouch $LOG_DIR/count_primary_key_increment_${core_name_file}.done
else
\ttouch $LOG_DIR/count_primary_key_increment_${core_name_file}.error
\techo \"file $output_count_file does not exists or has a size zero.\" >> $LOG_DIR/count_primary_key_increment_${core_name_file}.error
\texit 1
fi
";

	#close BATCHFILE
	close(BATCHFILE);

}
print LOG "Done generating bash file.\n" unless $VERBOSE =~ m/^OFF$/;

#end of script
print LOG
"\n---------------------------------------------------\n generate_count_primary_key_increment.pl successfully completed at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}

close(LOG);

