#!/usr/local/bin/perl
#
# Task_blast_all_launch_makeblastdb_IDRIS.pl : lance les fichier makeblastdb.ll sur le cluster
#
# Date : 01/2014
# LT
#
#LOG FILE : $SiteConfig::LOGDIR/Blast/Task_blast_all_launch_makeblastdb_IDRIS.log
#LAUNCHED AS : perl Task_blast_all_launch_makeblastdb_IDRIS.pl  -MAX_JOBS = {INTEGER} -VERBOSE = {ON, OFF} -CMD_CLUSTER [optional]
#ARGUMENTS :
# -MAX_JOBS = {INTEGER}
# -VERBOSE : if ON print ant details at each step, if OFF only print critical information
# -CMD_CLUSTER : [optional], if this argument is present or 0 then the command are executed in the bash locally. Example of command for cluster: -CMD_CLUSTER "qsub -m ea -q short.q"
#
#
# Pipeline origami v1.0 Copyright - INRA - 2012-2020
# Auteurs: INRA
# Contributor(s) : Mark Hoebeke (mark.hoebeke[AT]sb-roscoff.fr), Valentin Loux (valentin.loux[AT]jouy.inra.fr), Annie Gendrault (annie.gendrault[AT]jouy.inra.fr), Jean-Francçois Gibrat (jean-francois.gibrat[AT]jouy.inra.fr), Thomas Lacroix (thomas.lacroix[AT]jouy.inra.fr) (2004-2012)
#
# This file is part of Pipeline origami
#
# This software is a computer program whose purpose is to insert relavant data about genomes, genes, homologies and syntenies in the origami database. It uses Blast, dynamic programming and files in embl or gbk formats.
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
#

use strict;
use Time::HiRes qw(usleep);
use SiteConfig;
use ORIGAMI;
use POSIX ":sys_wait_h";
use Term::ReadKey;

#script level variables
my $path_log_file = "$SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_launch_makeblastdb_IDRIS.log";
my $path_step_done_file = "$SiteConfig::LOGDIR/Task_blast_all_launch_makeblastdb_IDRIS.done";
my $path_step_error_file = "$SiteConfig::LOGDIR/Task_blast_all_launch_makeblastdb_IDRIS.error";
my $MAX_JOBS = 1;
my $VERBOSE = "ON";
my $CMD_SUBMIT_CLUSTER = "$SiteConfig::CMDDIR/bash";
my @makeblastdb_ll_files = ();
my $output_backtick = "";
my $FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = "ON";

#
sub die_with_error_mssg {
	my ($error_mssg) = @_; 
	# touch marker file error
	$output_backtick = `touch $path_step_error_file`;
	if ($output_backtick eq "") {
		#ok
	} else {
		print LOG "Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n";
		die("Error could not complete touch $path_step_error_file :\n$output_backtick \n $error_mssg \n $! \n");
	}
	print LOG "$error_mssg\n";
	die("$error_mssg\n");
	exit -1;
}


#set the variables according to the argument or exit with usage if the arguments are incorrect
foreach my $argnum ( 0 .. $#ARGV ) {
	if ( $ARGV[$argnum] =~ m/^-MAX_JOBS$/ ) {
		if ( $ARGV[ $argnum + 1 ] =~ m/^\d+$/ ) {
			$MAX_JOBS = $ARGV[ $argnum + 1 ];
			if($MAX_JOBS < 1){
				#print LOG "incorrect -MAX_JOBS argument ; usage : perl TasTask_blast_all_launch_makeblastdb_IDRIS.pl  -MAX_JOBS {int > 0}";
				die_with_error_mssg ("incorrect -MAX_JOBS argument ; usage : perl Task_blast_all_launch_makeblastdb_IDRIS.pl  -MAX_JOBS {int > 0}");
			}
		}
		else {
			#print LOG "incorrect -MAX_JOBS argument ; usage : perl Task_blast_all_launch_makeblastdb_IDRIS.pl -MAX_JOBS = {INTEGER} -VERBOSE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}\n";
			die_with_error_mssg ("incorrect -MAX_JOBS argument ; usage : perl Task_blast_all_launch_makeblastdb_IDRIS.pl -MAX_JOBS = {INTEGER} -VERBOSE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}");
		}
	} elsif ( $ARGV[$argnum] =~ m/^-VERBOSE$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$VERBOSE = $ARGV[ $argnum + 1 ];
		}
		else {
			#print LOG "incorrect -VERBOSE argument ; usage : perl Task_blast_all_launch_makeblastdb_IDRIS.pl -MAX_JOBS = {INTEGER} -VERBOSE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}";
			die_with_error_mssg ("incorrect -VERBOSE argument ; usage : perl Task_blast_all_launch_makeblastdb_IDRIS.pl -MAX_JOBS = {INTEGER} -VERBOSE = {ON, OFF} -CMD_CLUSTER {0/\"qsub -m ea -q short.q\"}");
		}

	} elsif ( $ARGV[$argnum] =~ m/^-CMD_CLUSTER/ ) {
		if ( $ARGV[$argnum + 1] =~ m/^0$/i ) {
			#do nothing
		}
		else {
			$CMD_SUBMIT_CLUSTER = $ARGV[ $argnum + 1 ];
		}
	} elsif ( $ARGV[$argnum] =~ m/^-FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK$/ ) {
		if (   $ARGV[ $argnum + 1 ] =~ m/^ON$/i
			|| $ARGV[ $argnum + 1 ] =~ m/^OFF$/i )
		{
			$FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = $ARGV[ $argnum + 1 ];
		}
		else {
			die_with_error_mssg("incorrect -FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK argument ; usage : perl Task_blast_all_launch_makeblastdb_IDRIS.pl -FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK {ON, OFF}");
		}
	}

}

# s'il n'existe pas, on créé le sous-répertoire pour les fichiers de log des blast
#`$SiteConfig::CMDDIR/rm -r -f $SiteConfig::LOGDIR/Task_blast_all/`;
#`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/launch_blast/`;
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/`;

# ouvre en pipant sur tee.pl pour écrire à la fois sur la sortie standard et dans le fichier de log
open( LOG, ">$path_log_file" );# | perl $SiteConfig::SCRIPTSDIR/tee.pl -a 
#open( LOG, "| perl $SiteConfig::SCRIPTSDIR/tee.pl -a $SiteConfig::LOGDIR/Task_blast_all/Task_blast_all_launch_makeblastdb_IDRISS.log");
print LOG
"\n---------------------------------------------------\n Task_blast_all_launch_makeblastdb_IDRIS.pl started at :",
  scalar(localtime), "\n" unless $VERBOSE =~ m/^OFF$/;

# rm marker file done script
$output_backtick = `rm -f $path_step_done_file`;
$output_backtick .= `rm -f $path_step_error_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete rm -f $path_step_done_file or $path_step_error_file :\n$output_backtick \n $!");
}



print LOG "\nYou choose the following options :\n\tMAX_JOBS : $MAX_JOBS\n\tVERBOSE : $VERBOSE\n" unless $VERBOSE =~ m/^OFF$/;
if ( $CMD_SUBMIT_CLUSTER =~ m/^0$/i ) {
	#do nothing
}
else {
	print LOG "CMD_CLUSTER : $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/;
}


#rm file under $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/
`$SiteConfig::CMDDIR/mkdir -p $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/`;
my $output_rm = `$SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/*`;
$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/*.err`;
$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/*.out`;
if ($output_rm eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/* or $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/*.err or $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/*.out : $!\n");
}

# list *.makeblastdb.ll files under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/
# Accepts one argument: the full path to a directory.
sub sub_list_makeblastdb_ll_files_recursively {
    my $path = shift;
    opendir (DIR, $path) or do {
		die_with_error_mssg ("Unable to open $path: $!");
    };

    my @files =
        # Third: Prepend the full path
        map { $path . '/' . $_ }
        # Second: take out '.' and '..'
        grep { !/^\.{1,2}$/ }
        # First: get all files
        readdir (DIR);
    closedir (DIR);

    for (@files) {
        if (-d $_) {
            # directory, run recursively
            #print "$_ is a dir\n";
            sub_list_makeblastdb_ll_files_recursively ($_);
        } elsif ($_ =~ m/\.makeblastdb\.ll$/ ) {
		push ( @makeblastdb_ll_files, $_);
         } elsif ($_ =~ m/\.makeblastdb\.ll~$/ ) {
		#del ~ files
		`$SiteConfig::CMDDIR/rm -f $_`;
	 } else {
		die_with_error_mssg("Error in sub_list_blast_ll_files_recursively : the file $_ does not match the regex *.makeblastdb.ll");
        }
    }
}
sub_list_makeblastdb_ll_files_recursively ("$SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/");
#@makeblastdb_ll_files = <$SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/*.makeblastdb.ll>;
print LOG scalar(@makeblastdb_ll_files) . " .makeblastdb.ll files found under $SiteConfig::DATADIR/Task_blast_all/tmp/launch_blast_batch_files/makeblastdb/\n" unless $VERBOSE =~ m/^OFF$/;


my $number_makeblastdb_started    = 0;
my $number_makeblastdb_done = 0;
my $number_makeblastdb_started_before_force_submit_check = 0;
print LOG "Start submit ll files\n" unless $VERBOSE =~ m/^OFF$/;

sub sub_count_makeblastdb_done_files {

	if ( $FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK =~ m/^ON$/i && ( $number_makeblastdb_started - $number_makeblastdb_done < $MAX_JOBS ) && $number_makeblastdb_started > $number_makeblastdb_started_before_force_submit_check ) {
		$number_makeblastdb_started_before_force_submit_check = $number_makeblastdb_started;
		print LOG "( $number_makeblastdb_done / ".scalar(@makeblastdb_ll_files).") : FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK is ON ; Continuing to submit as there is $number_makeblastdb_started - $number_makeblastdb_done = ".($number_makeblastdb_started - $number_makeblastdb_done)." simulteneous process ;  MAX_JOBS =  $MAX_JOBS\n" unless $VERBOSE =~ m/^OFF$/i;
	
	} else {

		#my @list_new_files_makeblastdb_done = <$SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/*.makeblastdb_done>;
		opendir (DIR, "$SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/") or do {
			die_with_error_mssg( "Unable to open $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/: $!");
		};

		my @list_new_files_makeblastdb_done =
			# Third: Prepend the full path
			map { "$SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/" . $_ }
			# Second: take out '.' and '..'
			grep { !/^\.{1,2}$/ }
			# First: get all files
			readdir (DIR);
		closedir (DIR);


		# check err files are empty
		my $check_err_files_are_empty = `cat $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_*.err 2>/dev/null`;
		if ($check_err_files_are_empty eq "" ){
			#ok continue
		} else {
			die_with_error_mssg("Error cat $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_*.err 2>/dev/null is not empty : $check_err_files_are_empty");
		}


		foreach my $list_new_files_makeblastdb_done_IT (@list_new_files_makeblastdb_done) {

			#parse file name
			my $core_name_list_new_files_makeblastdb_done_IT = "";
			if ( $list_new_files_makeblastdb_done_IT =~ m/^.+\/(.+)\.makeblastdb_done$/i ) {
				$core_name_list_new_files_makeblastdb_done_IT = $1;
			} else {
				#print LOG "Could not parse core name in file $list_new_files_makeblastdb_done_IT\n";
				die_with_error_mssg("Could not parse core name in file $list_new_files_makeblastdb_done_IT");
			}

			# rm .err if empty
			if ($CMD_SUBMIT_CLUSTER eq "$SiteConfig::CMDDIR/bash") {
				# do nothing
				#print LOG "Bash do nothing\n";
			} else {

				if (! -z "$SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_list_new_files_makeblastdb_done_IT}.err" ) {
					#print LOG "The file $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_list_new_files_makeblastdb_done_IT}.err is not empty : $!\n";
					die_with_error_mssg("The file $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_list_new_files_makeblastdb_done_IT}.err is not empty : $!");
			
				} else {
					$output_rm = `$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_list_new_files_makeblastdb_done_IT}.err`;
					$output_rm = $output_rm.`$SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_list_new_files_makeblastdb_done_IT}.out`;
					if ($output_rm eq "") {
						#ok
						#print LOG "Done $SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_list_new_files_makeblastdb_done_IT}.err and out : $!\n";
					} else {
						#print LOG "Error could not complete $SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_list_new_files_makeblastdb_done_IT}.err or out : $!\n";
						die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm -f $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_list_new_files_makeblastdb_done_IT}.err or out : $!");
					}
				}
			}


			# rm marker file
			$output_rm = `$SiteConfig::CMDDIR/rm -f $list_new_files_makeblastdb_done_IT`;
			if ($output_rm eq "") {
				#ok
			} else {
				#print LOG "Error could not complete $SiteConfig::CMDDIR/rm -f $list_new_files_makeblastdb_done_IT : $!\n";
				die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm -f $list_new_files_makeblastdb_done_IT : $!");
			}

			# increment counter
			$number_makeblastdb_done++;

		}
	}
}

# launch all makeblastdb
LOOP: foreach my $makeblastdb_ll_file_IT (@makeblastdb_ll_files) {

	# wait a bit if there more than $MAX_JOBS child process that have been launched simultaneously
	my $count_sleep = 0;
	while ( $count_sleep < 2 ) {

		sub_count_makeblastdb_done_files();
		if ($MAX_JOBS == 1) {
			last;
		} else {
			if( $number_makeblastdb_started - $number_makeblastdb_done < $MAX_JOBS ){
				#wait a bit so that there is a shifting in time and exit loop
				#usleep(10000);
				print LOG "( $number_makeblastdb_done / ".scalar(@makeblastdb_ll_files).") : Continuing to submit as there is $number_makeblastdb_started - $number_makeblastdb_done = ".($number_makeblastdb_started - $number_makeblastdb_done)." simulteneous process ;  MAX_JOBS =  $MAX_JOBS\n" unless $VERBOSE =~ m/^OFF$/i;
				last;
			}


			if ($count_sleep == 0 ) {
				print LOG
	"waiting a bit as there is more than $MAX_JOBS processes that have been launched simultenously...\n"
				  unless $VERBOSE =~ m/^OFF$/i;
				$count_sleep = 1;
				usleep(10000);
			} else {
				usleep(10000);
			}
		}
	}


	# make sure the ll file is executable
	my $chmod_output = `chmod +x $makeblastdb_ll_file_IT`;
	if ( ! $chmod_output eq "") {
		die_with_error_mssg("ERROR Could not chmod +x $makeblastdb_ll_file_IT: $!");
	}


	#parse file name  \.makeblastdb\.ll
	#my $master_elet_id;
	#my $sub_elet_id;
	my $core_name_ll_file_IT = "";
	if ( $makeblastdb_ll_file_IT =~ m/^.+\/(.+)\.makeblastdb\.ll$/i ) {
		#$master_elet_id = $1;
		#$sub_elet_id = $2;
		$core_name_ll_file_IT = $1;
	} else {
		die_with_error_mssg("Could not parse core name in file $makeblastdb_ll_file_IT");
	}


	# launch of the bash file
	# case no cluster, no paralelisation
	if ( $CMD_SUBMIT_CLUSTER eq "$SiteConfig::CMDDIR/bash") {

		if($MAX_JOBS > 1){
			system("$CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT 2>> $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_ll_file_IT}.err 1>>$path_log_file &");
			if ( $? != 0 ) {
				#print LOG "command $CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT failed: $!\n";
				die_with_error_mssg("command $CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT failed: $!");
			}
			#print LOG $output_cmd;
			$number_makeblastdb_started++;
			#print LOG "file $makeblastdb_ll_file_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
			my $number_jobs_being_processed = $number_makeblastdb_started - $number_makeblastdb_done;
			my $number_jobs_left_to_be_submitted = scalar(@makeblastdb_ll_files) - $number_makeblastdb_started;
			print LOG "( $number_makeblastdb_done / ".scalar(@makeblastdb_ll_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_makeblastdb_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;

		} else {
			$number_makeblastdb_started++;
			#print LOG "file $makeblastdb_ll_file_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
			my $number_jobs_being_processed = $number_makeblastdb_started - $number_makeblastdb_done;
			my $number_jobs_left_to_be_submitted = scalar(@makeblastdb_ll_files) - $number_makeblastdb_started;
			print LOG "( $number_makeblastdb_done / ".scalar(@makeblastdb_ll_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_makeblastdb_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
			system("$CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT 2>> $SiteConfig::LOGDIR/Task_blast_all/launch_makeblastdb/launch_makeblastdb_for_${core_name_ll_file_IT}.err 1>>$path_log_file");
			print LOG "( $number_makeblastdb_done / ".scalar(@makeblastdb_ll_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_makeblastdb_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
			#if ( $? != 0 ) {
			#	#print LOG "command $CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT failed: $!\n";
			#	die_with_error_mssg("command $CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT failed: $!");
			#}
			#print LOG $output_cmd;
			#print LOG "file $makeblastdb_ll_file_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
		}



	} else {
		# use of cluster

		print LOG "doing : $CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT 2>&1\n" unless $VERBOSE =~ m/^OFF$/i;

		my $output_cmd = `$CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT 2>&1`;
		if ( $? != 0 )
		{
			#print LOG "command $CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT 2>&1 failed: $!\n";
			die_with_error_mssg("command $CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT 2>&1 failed: $!");
		}
		if($output_cmd =~ m/^Your job .+ has been submitted$/i
			|| $output_cmd eq ""
		){
			$number_makeblastdb_started++;
			#print LOG "file $makeblastdb_ll_file_IT successfuly submited by $CMD_SUBMIT_CLUSTER\n" unless $VERBOSE =~ m/^OFF$/i;
			my $number_jobs_being_processed = $number_makeblastdb_started - $number_makeblastdb_done;
			my $number_jobs_left_to_be_submitted = scalar(@makeblastdb_ll_files) - $number_makeblastdb_started;
			print LOG "( $number_makeblastdb_done / ".scalar(@makeblastdb_ll_files).") : Jobs being processed : $number_jobs_being_processed ; Jobs finished : $number_makeblastdb_done ; Jobs left to be submitted : $number_jobs_left_to_be_submitted\n" unless $VERBOSE =~ m/^OFF$/i;
		}else{
			#print LOG "Error launch of the bash on cluster: $CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT :\n$output_cmd\n";
			die_with_error_mssg("Error launch of the bash on cluster: $CMD_SUBMIT_CLUSTER $makeblastdb_ll_file_IT :\n$output_cmd");
		}
	}

}    #foreach my $makeblastdb_ll_file_IT (@makeblastdb_ll_files) {


print LOG "done submitting all commands\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "waiting for last commands to complete...\n" unless $VERBOSE =~ m/^OFF$/;
$FORCE_MAX_SUBMIT_CLUSTER_BEFORE_CHECK = "OFF";

my $count_finish = 0;
my $count_wait_hours = 0;
while ( $count_finish >= 0 ) {

	sub_count_makeblastdb_done_files();

	if($number_makeblastdb_done == scalar(@makeblastdb_ll_files) ){
		last;
	}

	usleep(1000000);#sleep 1s
	$count_finish++;
	if ($count_finish > 1800){#30 min
		#print LOG "Error, waited for makeblastdb output file for more than 30 minutes\n";
		#die ("Error, waited for makeblastdb output file for more than 30 minutes\n");
		$count_wait_hours++;
		print LOG "( $number_makeblastdb_done / ".scalar(@makeblastdb_ll_files).") : All jobs have been launched. Waiting for blast output files ; Total waiting time = $count_wait_hours hours ; Total output files found yet = $number_makeblastdb_done. (This message will display every hour until all output files are found).\n";
		$count_finish = 0;
	}

}

sub remove_trailing_space_and_chomp_var {
	my ( $var_to_chomp ) = @_;
	chomp ($var_to_chomp);
	$var_to_chomp =~ s/^\s+//;
	$var_to_chomp =~ s/\s+$//;
	return $var_to_chomp;
}

my $number_fasta_files = `find $SiteConfig::DATADIR//Task_blast_all/tmp/fasta_formatdb/ -type f -name '*.faa' | wc -l`;
$number_fasta_files = remove_trailing_space_and_chomp_var($number_fasta_files);
my $number_phr_files = `find $SiteConfig::DATADIR//Task_blast_all/tmp/fasta_formatdb/ -type f -name '*.phr' | wc -l`;
$number_phr_files = remove_trailing_space_and_chomp_var($number_phr_files);
my $number_pin_files = `find $SiteConfig::DATADIR//Task_blast_all/tmp/fasta_formatdb/ -type f -name '*.pin' | wc -l`;
$number_pin_files = remove_trailing_space_and_chomp_var($number_pin_files);
my $number_psq_files = `find $SiteConfig::DATADIR//Task_blast_all/tmp/fasta_formatdb/ -type f -name '*.psq' | wc -l`;
$number_psq_files = remove_trailing_space_and_chomp_var($number_psq_files);

print LOG "\n$number_fasta_files .faa have been found.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "$number_phr_files .phr have been found.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "$number_pin_files .pin have been found.\n" unless $VERBOSE =~ m/^OFF$/;
print LOG "$number_psq_files .psq have been found.\n" unless $VERBOSE =~ m/^OFF$/;

#rm file under $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/
$output_rm = `$SiteConfig::CMDDIR/rm -rf $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/*`;
if ($output_rm eq "") {
	#ok
} else {
	#print LOG "Error could not complete $SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/* : $!\n";
	die_with_error_mssg("Error could not complete $SiteConfig::CMDDIR/rm -f $SiteConfig::DATADIR/Task_blast_all/tmp/makeblastdb_done/* : $!");
}

print LOG
"\n---------------------------------------------------\n\n\nTask_blast_all_launch_makeblastdb_IDRIS.pl successfully completed at :",
  scalar(localtime),
  "\n\n---------------------------------------------------\n" unless $VERBOSE =~ m/^OFF$/;

# touch marker file done script when script done
$output_backtick = `touch $path_step_done_file`;
if ($output_backtick eq "") {
	#ok
} else {
	die_with_error_mssg("Error could not complete touch $path_step_done_file :\n$output_backtick \n $!");#
}
close(LOG);





