SET client_min_messages TO WARNING; -- do not print NOTICE messages
-- SET search_path
SET search_path TO micado, public;

-- index table alignment_params
CREATE UNIQUE INDEX alignment_params_pkey ON alignment_params (alignment_param_id);-- index fast access
ALTER TABLE alignment_params ADD PRIMARY KEY USING INDEX alignment_params_pkey;-- index fast access
CREATE INDEX alignment_params_q_element_id ON alignment_params (q_element_id);-- index fast access
CREATE INDEX alignment_params_s_element_id ON alignment_params (s_element_id);-- index fast access
CREATE INDEX alignment_params_q_organism_id ON alignment_params (q_organism_id);-- index fast access
CREATE INDEX alignment_params_s_organism_id ON alignment_params (s_organism_id);-- index fast access
-- index slow access not necessary : CREATE INDEX alignment_params_q_element_id_s_element_id ON alignment_params (q_element_id, s_element_id);

-- index table alignments
CREATE UNIQUE INDEX alignments_pkey ON alignments (alignment_id);-- index fast access
ALTER TABLE alignments ADD PRIMARY KEY USING INDEX alignments_pkey;-- index fast access
CREATE INDEX alignments_alignment_param_id ON alignments (alignment_param_id);-- index fast access

-- index table alignment_pairs
CREATE INDEX alignment_pairs_alignment_id ON alignment_pairs (alignment_id);-- index fast access
CREATE INDEX alignment_pairs_q_gene_id ON alignment_pairs (q_gene_id);-- index fast access
CREATE INDEX alignment_pairs_s_gene_id ON alignment_pairs (s_gene_id);-- index slow access

-- index table homologies
CREATE INDEX homologies_q_gene_id_s_element_id ON homologies (q_gene_id, s_element_id);-- index fast access
CREATE INDEX homologies_s_gene_id ON homologies (s_gene_id);-- index fast access
CREATE INDEX homologies_s_organism_id ON homologies (s_organism_id);-- index slow access
CREATE INDEX homologies_q_element_id ON homologies (q_element_id);-- index slow access
CREATE INDEX homologies_q_organism_id ON homologies (q_organism_id);-- index slow access
CREATE INDEX homologies_s_element_id ON homologies (s_element_id);-- index slow access
CREATE INDEX homologies_e_value_q_first_frac_s_align_frac ON homologies (e_value, q_first_frac, s_align_frac);-- index slow access
-- index slow access not necessary : CREATE INDEX homologies_rank ON homologies (rank);

-- index table tandem_dups
CREATE INDEX tandem_dups_tandem_dups_id ON tandem_dups (tandem_dups_id);-- index slow access
CREATE INDEX tandem_dups_alignment_param_id ON tandem_dups (alignment_param_id);-- index slow access

-- index table isBranchedToAnotherSynteny
CREATE INDEX isBranchedToAnotherSynteny_alignment_id_branched ON isBranchedToAnotherSynteny (alignment_id_branched);-- index slow access
CREATE INDEX isBranchedToAnotherSynteny_alignment_param_id ON isBranchedToAnotherSynteny (alignment_param_id);-- index slow access

-- index table prot_fusion
CREATE INDEX prot_fusion_prot_fusion_id ON prot_fusion (prot_fusion_id);-- index fast access
CREATE INDEX prot_fusion_q_organims_id ON prot_fusion (q_organims_id);-- index fast access
CREATE INDEX prot_fusion_q_gene_id ON prot_fusion (q_gene_id);-- index fast access
CREATE INDEX prot_fusion_s_organims_id ON prot_fusion (s_organims_id);-- index fast access
CREATE INDEX prot_fusion_s_gene_id ON prot_fusion (s_gene_id);-- index fast access

-- index table close_best_match
CREATE UNIQUE INDEX close_best_match_q_gene_id_s_organims_id_pkey ON close_best_match (q_gene_id, s_organims_id);
ALTER TABLE close_best_match ADD PRIMARY KEY USING INDEX close_best_match_q_gene_id_s_organims_id_pkey;
--CREATE UNIQUE INDEX close_best_match_pkey ON close_best_match (close_best_match_id);-- index slow access
--ALTER TABLE close_best_match ADD PRIMARY KEY USING INDEX close_best_match_pkey;-- index slow access
--CREATE INDEX close_best_match_q_gene_id ON close_best_match (q_gene_id);-- index slow access
--CREATE INDEX close_best_match_s_organims_id ON close_best_match (s_organims_id);-- index slow access


-- constraint table alignment_params
ALTER TABLE alignment_params ADD CONSTRAINT q_organism_id_fk FOREIGN KEY (q_organism_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE alignment_params ADD CONSTRAINT q_element_id_fk FOREIGN KEY (q_element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE alignment_params ADD CONSTRAINT s_organism_id_fk FOREIGN KEY (s_organism_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE alignment_params ADD CONSTRAINT s_element_id_fk FOREIGN KEY (s_element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;--

-- constraint table alignments
ALTER TABLE alignments ADD CONSTRAINT alignment_param_id_fk FOREIGN KEY (alignment_param_id) REFERENCES alignment_params (alignment_param_id) MATCH FULL ON DELETE CASCADE;--

-- constraint table alignment_pairs
ALTER TABLE alignment_pairs ADD CONSTRAINT alignment_id_fk FOREIGN KEY (alignment_id) REFERENCES alignments (alignment_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE alignment_pairs ADD CONSTRAINT q_gene_id_fk FOREIGN KEY (q_gene_id) REFERENCES genes (gene_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE alignment_pairs ADD CONSTRAINT s_gene_id_fk FOREIGN KEY (s_gene_id) REFERENCES genes (gene_id) MATCH FULL ON DELETE CASCADE;--

-- constraint table homologies
ALTER TABLE homologies ADD CONSTRAINT check_lengths CHECK (q_length > 0 AND s_length > 0);--
ALTER TABLE homologies ADD CONSTRAINT check_q_first CHECK (q_first > 0 AND q_first <= q_length);--
ALTER TABLE homologies ADD CONSTRAINT check_q_last CHECK (q_last > 0 AND q_last <= q_length);--
ALTER TABLE homologies ADD CONSTRAINT check_s_first CHECK (s_first > 0 AND s_first <= s_length);--
ALTER TABLE homologies ADD CONSTRAINT check_s_last CHECK (s_last > 0 AND s_last <= s_length);--
ALTER TABLE homologies ADD CONSTRAINT q_organism_id_fk FOREIGN KEY (q_organism_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE homologies ADD CONSTRAINT q_element_id_fk FOREIGN KEY (q_element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE homologies ADD CONSTRAINT q_gene_id_fk FOREIGN KEY (q_gene_id) REFERENCES genes (gene_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE homologies ADD CONSTRAINT s_organism_id_fk FOREIGN KEY (s_organism_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE homologies ADD CONSTRAINT s_element_id_fk FOREIGN KEY (s_element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;--
ALTER TABLE homologies ADD CONSTRAINT s_gene_id_fk FOREIGN KEY (s_gene_id) REFERENCES genes (gene_id) MATCH FULL ON DELETE CASCADE;--

-- constraint tandem_dups
-- ALTER TABLE tandem_dups ADD CONSTRAINT alignment_param_id_fk FOREIGN KEY (alignment_param_id) REFERENCES alignment_params (alignment_param_id) MATCH FULL ON DELETE CASCADE; -- alignment_param_id not necessary present if no synteny or ortholog are reported
ALTER TABLE tandem_dups ADD CONSTRAINT single_organims_id_fk FOREIGN KEY (single_organims_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE tandem_dups ADD CONSTRAINT single_element_id_fk FOREIGN KEY (single_element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE tandem_dups ADD CONSTRAINT single_gene_id_fk FOREIGN KEY (single_gene_id) REFERENCES genes (gene_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE tandem_dups ADD CONSTRAINT tandem_organims_id_fk FOREIGN KEY (tandem_organims_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE tandem_dups ADD CONSTRAINT tandem_element_id_fk FOREIGN KEY (tandem_element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE tandem_dups ADD CONSTRAINT tandem_gene_id_fk FOREIGN KEY (tandem_gene_id) REFERENCES genes (gene_id) MATCH FULL ON DELETE CASCADE;


-- constraint isBranchedToAnotherSynteny
ALTER TABLE isBranchedToAnotherSynteny ADD CONSTRAINT alignment_id_branched_fk FOREIGN KEY (alignment_id_branched) REFERENCES alignments (alignment_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE isBranchedToAnotherSynteny ADD CONSTRAINT alignment_param_id_fk FOREIGN KEY (alignment_param_id) REFERENCES alignment_params (alignment_param_id) MATCH FULL ON DELETE CASCADE;


-- constraint prot_fusion
ALTER TABLE prot_fusion ADD CONSTRAINT q_organims_id_fk FOREIGN KEY (q_organims_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE prot_fusion ADD CONSTRAINT q_element_id_fk FOREIGN KEY (q_element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE prot_fusion ADD CONSTRAINT q_gene_id_fk FOREIGN KEY (q_gene_id) REFERENCES genes (gene_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE prot_fusion ADD CONSTRAINT s_organims_id_fk FOREIGN KEY (s_organims_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE prot_fusion ADD CONSTRAINT s_element_id_fk FOREIGN KEY (s_element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE prot_fusion ADD CONSTRAINT s_gene_id_fk FOREIGN KEY (s_gene_id) REFERENCES genes (gene_id) MATCH FULL ON DELETE CASCADE;


-- constraint close_best_match
ALTER TABLE close_best_match ADD CONSTRAINT q_organims_id_fk FOREIGN KEY (q_organims_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE close_best_match ADD CONSTRAINT q_element_id_fk FOREIGN KEY (q_element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE close_best_match ADD CONSTRAINT q_gene_id_fk FOREIGN KEY (q_gene_id) REFERENCES genes (gene_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE close_best_match ADD CONSTRAINT s_organims_id_fk FOREIGN KEY (s_organims_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;

