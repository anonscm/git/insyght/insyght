-- Creation des tables pour la gestion des donn�es priv�/public
-- AGJ Mai 2011

create sequence seq_groupe_id;
create sequence seq_user_id;

create table user_info (user_id integer default (nextval('seq_user_id')) primary key,
labo varchar(255),
nom varchar(50) not null,
prenom varchar(50) not null,
session_id varchar(100),
login varchar(100) not null,
password varchar(100) not null);

alter table user_info add constraint unique_login UNIQUE (login);

create table groupe_info (groupe_id integer default (nextval('seq_groupe_id')) primary key,
                    
organism_id integer,
libelle varchar(50),
acces_type varchar(10));

alter table groupe_info ADD CONSTRAINT FK_organisms foreign key (organism_id) references organisms(organism_id);

create table user_groupe (groupe_id integer not null, user_id integer not null);alter table user_groupe add constraint fk_groupe foreign key (groupe_id) references groupe_info(groupe_id);
alter table user_groupe add constraint fk_user foreign key (user_id) references user_info(user_id);

-- alter table organisms add column ispublic boolean;

grant select on user_info to origami_read;
grant select on groupe_info to origami_read;
grant select on user_groupe to origami_read;

grant select on user_info to origami_anonyme;
grant select on groupe_info to origami_anonyme;
grant select on user_groupe to origami_anonyme;
