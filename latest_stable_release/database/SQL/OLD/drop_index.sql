-- Script de cr�ation des index et foreign key
-- Cr�� par AGJ Juillet
-- � partir du script create_index_origami.sql

\echo "IDX homologies_q_organism_id"
DROP INDEX homologies_q_organism_id ;
\echo "IDX homologies_s_organism_id"
DROP INDEX homologies_s_organism_id ;
\echo "IDX homologies_s_gene_id"
DROP INDEX homologies_s_gene_id;
\echo "IDX homologies_q_gene_id"
DROP INDEX homologies_q_gene_id ;
\echo "IDX homologies_element_id"
DROP INDEX genes_element_id ;
\echo "IDX genes_names"
DROP INDEX genes_names ;
\echo "IDX genes_organism_id"
DROP INDEX genes_organism_id ;
\echo "IDX homologies_q_element_id"
DROP INDEX homologies_q_element_id ;
\echo "IDX homologies_s_element_id"
DROP INDEX homologies_s_element_id ;
\echo "IDX homologies_rank"
DROP INDEX homologies_rank ;


-- Mise en commentaire pour version1 Juin 2007
\echo "alignment_pairs_alignment_id"
DROP INDEX alignment_pairs_alignment_id ON alignment_pairs USING btree (alignment_id);

-- CREATE INDEX alignments_alignment_param_id ON alignments USING btree (alignment_param_id);

-- CREATE INDEX alignments_q_first_gene_positions ON alignments USING btree (q_first_gene_position);

-- CREATE INDEX alignments_q_last_gene_positions ON alignments USING btree (q_last_gene_position);
\echo "alignment_pairs_alignment_id"
DROP INDEX alignment_pairs_q_gene_id ON alignment_pairs USING btree (q_gene_id);
\echo "alignment_pairs_alignment_id"
DROP INDEX alignment_pairs_s_gene_id ON alignment_pairs USING btree (s_gene_id);
\echo "IDX homologies_q_gene_id_s_element_id"
DROP INDEX homologies_q_gene_id_s_element_id ;
\echo "IDX homologies_e_value_q_first_frac_s_align_frac"
DROP INDEX homologies_e_value_q_first_frac_s_align_frac ;


-- Mise en commentaire pour version1 Juin 2007
--ALTER TABLE ONLY organisms
--    ADD CONSTRAINT "organisms_fk1" FOREIGN KEY (taxon_id) REFERENCES taxons(taxon_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY elements
--    ADD CONSTRAINT "elements_fk1" FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY genes
--    ADD CONSTRAINT "genes_fk1" FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY genes
--    ADD CONSTRAINT "genes_fk2" FOREIGN KEY (element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk1" FOREIGN KEY (gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk2" FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk3" FOREIGN KEY (element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk4" FOREIGN KEY (left_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk5" FOREIGN KEY (right_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY alignment_params
--    ADD CONSTRAINT "alignment_params_fk1" FOREIGN KEY (q_organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE CASCADE;

--ALTER TABLE ONLY alignment_params
--    ADD CONSTRAINT "alignment_params_fk2" FOREIGN KEY (q_element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE CASCADE;

--ALTER TABLE ONLY alignment_params
--    ADD CONSTRAINT "alignment_params_fk3" FOREIGN KEY (s_organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE CASCADE;

--ALTER TABLE ONLY alignment_params
--    ADD CONSTRAINT "alignment_param_fk4" FOREIGN KEY (s_element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE CASCADE;

--ALTER TABLE ONLY alignments
--    ADD CONSTRAINT "aligments_fk1" FOREIGN KEY (alignment_param_id) REFERENCES alignment_params(alignment_param_id) ON UPDATE NO ACTION ON DELETE CASCADE;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk1" FOREIGN KEY (q_organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk2" FOREIGN KEY (q_element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk3" FOREIGN KEY (q_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk4" FOREIGN KEY (s_organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk5" FOREIGN KEY (s_element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk6" FOREIGN KEY (s_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY state_descriptions
--    ADD CONSTRAINT state_descriptions_description_key UNIQUE (description);

--ALTER TABLE ONLY entry_state_info
--    ADD CONSTRAINT "entry_state_info_fk1" FOREIGN KEY (entry_id) REFERENCES entries(entry_id) ON UPDATE NO ACTION ON DELETE CASCADE;

--ALTER TABLE ONLY gene_states
--    ADD CONSTRAINT "gene_states_fk1" FOREIGN KEY (entry_state_info_id) REFERENCES entry_state_info(entry_state_info_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY gene_states
--    ADD CONSTRAINT "gene_states_fk2" FOREIGN KEY (feature_id) REFERENCES features(feature_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY gene_states
--    ADD CONSTRAINT "gene_states_fk3" FOREIGN KEY (dominant_state_id) REFERENCES state_descriptions(state_description_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY comp_anal_result_descriptions
--  ADD CONSTRAINT comp_anal_result_descriptions_description_key UNIQUE (description);

--ALTER TABLE ONLY comp_anal_results_elements
--    ADD CONSTRAINT "comp_anal_results_elements_fk1" FOREIGN KEY (comp_anal_result_description_id) REFERENCES comp_anal_result_descriptions(comp_anal_result_description_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY comp_anal_results_elements
--    ADD CONSTRAINT "comp_anal_results_elements_fk2" FOREIGN KEY (element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY comp_anal_results_elements
--    ADD CONSTRAINT "comp_anal_results_elements_fk3" FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY alignment_pairs
--    ADD CONSTRAINT "alignment_pairs_fk1" FOREIGN KEY (alignment_id) REFERENCES alignments(alignment_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY alignment_pairs
--    ADD CONSTRAINT "alignment_pairs_fk2" FOREIGN KEY (q_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY alignment_pairs
--    ADD CONSTRAINT "alignment_pairs_fk3" FOREIGN KEY (s_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE ONLY comp_anal_results_elements
--    ADD CONSTRAINT unique_desc_el_org UNIQUE (comp_anal_result_description_id, element_id, organism_id);
