 ------------------------------------------------------------
 --  Nom de la base   : PARTIE SEQUENCE GENERIQUE de MICADO                
 --  Nom de SGBD      : POSTGRESQL                   
 --  Date de creation : 11/02/03
 --  maj      : 13/11/03 (maj taxo)
 --  maj      : 11/09/06 (maj complete_genome ajout strain et reorg attributs)
 --  Version	      : 2
 --  Version simplifiee et modifiee a charger avec le parser BioPerl
 --  Version avec les tables FEATURES et LOCATIONS modifiees
 --  Version avec la taxonomie du NCBI
 --  Version avec la BIBLIO non parsee (1 table ARTICLES)
 --  Version avec table DNA_LOC modifiee (cle CODE_LOC viree)
 --  13 Tables crees : SEQUENCES, ACCESSIONS, COMMENTS, KEYWORDS
 --  DNA_SEQ, ARTICLES, FEATURES, QUALIFIERS, LOCATIONS, DNA_LOC, 
 --  PROT_FEAT, TAXO, COMPLETE_GENOMES
 --
 --  Maj	: 19/01/2007 (AGJ)
 --  Table qualifiers, attribut qualifiers modif varchar(5000) en TEXT
 ------------------------------------------------------------
 -- Maj Mars2007 (AGJ) pour ORIGAMI
 -- ajout 2 dans prot_feat : CRC(chechsum) et LENGTH longueur de la seq
 ------------------------------------------------------------
 -- SEQUENCEURS                                            
 ------------------------------------------------------------
 drop sequence num_code_loc;
 create sequence num_code_loc start 1;

 ------------------------------------------------------------
-- Drop des tables
-------------------------------------------------------------
drop table TAXO;
drop table SEQUENCES CASCADE;
drop table ACCESSIONS CASCADE;
drop table COMMENTS CASCADE;
drop table KEYWORDS CASCADE;
drop table DNA_SEQ CASCADE;
drop table ARTICLES CASCADE;
drop table FEATURES CASCADE;
drop table QUALIFIERS CASCADE;
drop table LOCATIONS CASCADE;
drop table DNA_LOC CASCADE;
drop table PROT_FEAT CASCADE;
drop table COMPLETE_GENOMES CASCADE;

-------------------------------------------------------------
-- TABLE TAXO 
-------------------------------------------------------------
create table TAXO
(
	NCBI_TAXID	INTEGER		not null,
	NAME 		VARCHAR(100)	not null,
	COMMON_NAME	VARCHAR(100)	,
	SYNONYMS	VARCHAR(2000)	,
	FATHER_ID	INTEGER		not null,
	RANK		VARCHAR(25)	not null,
	TAXID		VARCHAR(100)	,
	DEPTH		INTEGER		,
	LEAF		BOOLEAN		DEFAULT false,
	SEQUENCES	INTEGER		,
	SEQUENCES_LENGTH		INTEGER,
	LFT		INTEGER		not null UNIQUE CHECK (lft > -1),
	RGT		INTEGER		not null UNIQUE CHECK (rgt > 0),
	constraint pk_taxo
		primary key(ncbi_taxid),	
	constraint order_okay_taxo 
		CHECK (lft < rgt)	
);

 ------------------------------------------------------------
 --  Table : SEQUENCES                                               
 ------------------------------------------------------------
create table SEQUENCES
(
     ACCESSION			VARCHAR(25)		not null,
     NAME_SEQ			VARCHAR(15)		not null,
     SPECIES			VARCHAR(200)		not null,
     NCBI_TAXID			INTEGER,
     LENGTH			INTEGER			not null,
     MOLECULE			VARCHAR(30),
     SUBBANK			VARCHAR(30),
     DATE_SEQ			VARCHAR(100)		not null,
     DEFINITION			VARCHAR(2000),
     VERSION			INTEGER,
	constraint pk_sequences 
		primary key(accession)
--	constraint fk_sequences
--		foreign key(ncbi_taxid)
--		references TAXO(ncbi_taxid) 
);

 ------------------------------------------------------------
 --  Table : ACCESSIONS
 ------------------------------------------------------------
create table ACCESSIONS
(
    ACCESSION			VARCHAR(25)		not null,
    OLD_ACCESSION		VARCHAR(25)		not null,
	constraint pk_accessions 
		primary key(accession,old_accession),
	constraint fk_accessions
		foreign key(accession)
		references SEQUENCES(accession) 
);

 ------------------------------------------------------------
 --  Table : COMMENTS
 ------------------------------------------------------------
create table COMMENTS
(
    ACCESSION                   VARCHAR(25)             not null,
    COMMENTS                    TEXT                            ,
        constraint pk_comments
                primary key(accession),
        constraint fk_comments
                foreign key(accession)
                references SEQUENCES(accession)
);

 ------------------------------------------------------------
 --  Table : KEYWORDS                                           
 ------------------------------------------------------------
create table KEYWORDS                                           
(
    ACCESSION			VARCHAR(25)		not null, 
    LST_KEYWORD			TEXT 			,
	constraint pk_keywords 
		primary key(accession),
	constraint fk_keywords
		foreign key(accession)
		references SEQUENCES(accession)            
);

 ------------------------------------------------------------
 --  Table : DNA_SEQ                                         
 ------------------------------------------------------------
create table DNA_SEQ
(
     ACCESSION			VARCHAR(25)		not null,
     SEQUENCES			text,
	constraint pk_dna_seq 
		primary key(accession),
	constraint fk_dna_seq
		foreign key(accession)
		references SEQUENCES(accession) 
);



 ------------------------------------------------------------
 --  Table : ARTICLES                                       
 ------------------------------------------------------------
create table ARTICLES
(
    ACCESSION			VARCHAR(50)		not null,
    NUM_ART			INTEGER			not null,	
    TITLE			VARCHAR(2000)			,
    AUTHOR_LIST			VARCHAR(2000)		not null,
    LOCATION			VARCHAR(1000)			,
    MEDLINE			VARCHAR(20)			,
	constraint pk_articles
		primary key(accession, num_art),
	constraint fk_articles
		foreign key(accession)
		references SEQUENCES(accession)              
);


 ------------------------------------------------------------
 --  Table : FEATURES                                         
 ------------------------------------------------------------
create table FEATURES
(
    ACCESSION			VARCHAR(25)		not null,
    CODE_FEAT			INTEGER			not null,
    TYPE_FEAT			VARCHAR(50)		not null,
    LOCATION			TEXT			not null,
    OPERATOR_FEAT		VARCHAR(50)			,
    POSMIN			INTEGER				,
    POSMAX			INTEGER				,
    STRAND			INTEGER				,
  	constraint pk_features 
		primary key(accession,code_feat),
	constraint fk_features
		foreign key(accession)
		references SEQUENCES(accession) 
);

 ------------------------------------------------------------
 -- Table : QUALIFIERS
 ------------------------------------------------------------
create table QUALIFIERS
(
    ACCESSION VARCHAR(25) not null,
    CODE_FEAT INTEGER not null,
    CODE_QUAL INTEGER not null, 
    TYPE_QUAL VARCHAR(50) not null,
    QUALIFIER TEXT,
	constraint pk_qualifiers 
		primary key(accession,code_feat,code_qual),
	constraint fk_qualifiers
		foreign key(accession,code_feat)
		references FEATURES(accession,code_feat)                         );

 ------------------------------------------------------------
 --  Table : LOCATIONS
 ------------------------------------------------------------
create table LOCATIONS
(
    ACCESSION			VARCHAR(25)		not null,
    CODE_FEAT			INTEGER			not null,
    CODE_LOC			INTEGER			not null,
    STARTPOS_BEGIN		INTEGER			not null,
    STARTPOS_END		INTEGER			not null,
    STARTPOS_TYPE		VARCHAR(30)		not null,
    STOPPOS_BEGIN		INTEGER			not null,
    STOPPOS_END	        	INTEGER			not null,
    STOPPOS_TYPE		VARCHAR(30)		not null,		
    STRAND			INTEGER			not null,
    LINK_ACCESS			VARCHAR(50)			,
  	constraint pk_locations 
		primary key(accession,code_feat,code_loc),
	constraint fk_locations
		foreign key(accession,code_feat)
		references FEATURES(accession,code_feat)        
);

 ------------------------------------------------------------
 --  Table : DNA_LOC
 ------------------------------------------------------------
create table DNA_LOC
(
    ACCESSION			VARCHAR(25)		not null,
    CODE_FEAT			INTEGER			not null,
    SEQUENCES			TEXT			not null,
 	constraint pk_dnaloc 
		primary key(accession,code_feat),
	constraint fk_dnaloc
		foreign key(accession,code_feat)
		references FEATURES(accession,code_feat)
);

 ------------------------------------------------------------
 --  Table : PROT_FEAT                                        
 ------------------------------------------------------------
create table PROT_FEAT
(
     ACCESSION			VARCHAR(25)		not null,	
     CODE_FEAT			INTEGER			not null,
     PROTEINE			TEXT			not null,
     LENGTH			INTEGER			not null,
     CRC			CHAR(16)		not null,
	constraint pk_protfeat 
		primary key(accession,code_feat),
	constraint fk_protfeat
		foreign key(accession, code_feat)
		references FEATURES(accession,code_feat)        
);


create table COMPLETE_GENOMES
(
    ACCESSION                   VARCHAR(25)             not null,
    SPECIES                     VARCHAR(100)             not null,
    STRAIN                     VARCHAR(150)             ,
    TYPE                        VARCHAR(250)             not null,
        constraint pk_comp_gen
                primary key(accession),
        constraint fk_com_gen
                foreign key(accession)
                references SEQUENCES(accession)
);
