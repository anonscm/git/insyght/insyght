--
-- Grant des acc�s pour genopage
-- Creation AGJ Decembre 2007
--
-- origami_read
grant select on genopage.genes to origami_read;
grant select on genopage.bestortho to origami_read;
grant select on genopage.modules to origami_read;
grant select on genopage.organisms to origami_read;
grant select on genopage.bloc to origami_read;
grant select on genopage.replicons to origami_read;
grant select on genopage.soh to origami_read;
grant select on genopage.neighborpairs to origami_read;
grant select on genopage.version to origami_read;

-- origami_admin
grant select on genopage.genes to origami_admin;
grant select on genopage.bestortho to origami_admin;
grant select on genopage.modules to origami_admin;
grant select on genopage.organisms to origami_admin;
grant select on genopage.bloc to origami_admin;
grant select on genopage.replicons to origami_admin;
grant select on genopage.soh to origami_admin;
grant select on genopage.neighborpairs to origami_admin;
grant select on genopage.version to origami_admin;

-- orimagi_lri
grant select on genopage.genes to origami_lri;
grant select on genopage.bestortho to origami_lri;
grant select on genopage.modules to origami_lri;
grant select on genopage.organisms to origami_lri;
grant select on genopage.bloc to origami_lri;
grant select on genopage.replicons to origami_lri;
grant select on genopage.soh to origami_lri;
grant select on genopage.neighborpairs to origami_lri;
grant select on genopage.version to origami_lri;

-- origami_igm
grant select on genopage.genes to origami_igm;
grant select on genopage.bestortho to origami_igm;
grant select on genopage.modules to origami_igm;
grant select on genopage.organisms to origami_igm;
grant select on genopage.bloc to origami_igm;
grant select on genopage.replicons to origami_igm;
grant select on genopage.soh to origami_igm;
grant select on genopage.neighborpairs to origami_igm;
grant select on genopage.version to origami_igm;
