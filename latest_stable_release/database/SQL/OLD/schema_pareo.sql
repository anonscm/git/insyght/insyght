alter table compound set schema pareo;
alter table compound_equiv set schema pareo;
alter table compound_syn set schema pareo;
alter table enzyme set schema pareo;
alter table enzyme_ref set schema pareo;
alter table enzyme_syn set schema pareo;
alter table gen_enz set schema pareo;
alter table gen_ort set schema pareo;
alter table gene set schema pareo;
alter table gene_seq set schema pareo;
alter table gene_syn set schema pareo;
alter table item set schema pareo; 
alter table link set schema pareo;
alter table note set schema pareo;
alter table nte_cmp set schema pareo;
alter table nte_enz set schema pareo;
alter table nte_gen set schema pareo;
alter table nte_ptw set schema pareo;
alter table nte_rct set schema pareo;
alter table ort_enz set schema pareo;
alter table ortholog set schema pareo;
alter table pathway set schema pareo;
alter table ptw_cmp set schema pareo;
alter table ptw_enz set schema pareo;
alter table ptw_gen set schema pareo;
alter table ptw_ort set schema pareo;
alter table ptw_ptw set schema pareo;
alter table ptw_rct set schema pareo;
alter table rct_cmp set schema pareo;
alter table rct_enz set schema pareo;
alter table reaction set schema pareo;
alter table specie set schema pareo;
alter table version set schema pareo;



