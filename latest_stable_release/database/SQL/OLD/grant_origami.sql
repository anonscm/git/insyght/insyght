-- Script de g�n�ration des droits
-- Cr�� par AGJ D�cembre 2006
-- Maj Mars 2007 
--
-- pour origami_read
GRANT select ON elements TO origami_read;
GRANT select ON organisms TO origami_read;
GRANT select ON genes TO origami_read;
GRANT select ON neighbors TO origami_read;
GRANT select ON alignment_params TO origami_read;
GRANT select ON alignments TO origami_read;
GRANT select ON alignment_pairs TO origami_read;
GRANT select ON homologies TO origami_read;
GRANT select ON state_descriptions TO origami_read;
GRANT select ON entry_state_info TO origami_read;
GRANT select ON gene_states TO origami_read;
GRANT select ON comp_anal_result_descriptions TO origami_read;
GRANT select ON comp_anal_results_elements TO origami_read;
GRANT select ON suivi_MAJ TO origami_read;
-- pour micado
GRANT select ON elements TO micado;
GRANT select ON organisms TO micado;
GRANT select ON genes TO micado;
GRANT select ON neighbors TO micado;
GRANT select ON alignment_params TO micado;
GRANT select ON alignments TO micado;
GRANT select ON alignment_pairs TO micado;
GRANT select ON homologies TO micado;
GRANT select ON state_descriptions TO micado;
GRANT select ON entry_state_info TO micado;
GRANT select ON gene_states TO micado;
GRANT select ON comp_anal_result_descriptions TO micado;
GRANT select ON comp_anal_results_elements TO micado;
GRANT select ON suivi_MAJ TO micado;
-- pour IGM
GRANT select ON elements TO origami_igm;
GRANT select ON organisms TO origami_igm;
GRANT select ON genes TO origami_igm;
GRANT select ON neighbors TO origami_igm;
GRANT select ON alignment_params TO origami_igm;
GRANT select ON alignments TO origami_igm;
GRANT select ON alignment_pairs TO origami_igm;
GRANT select ON homologies TO origami_igm;
GRANT select ON state_descriptions TO origami_igm;
GRANT select ON entry_state_info TO origami_igm;
GRANT select ON gene_states TO origami_igm;
GRANT select ON comp_anal_result_descriptions TO origami_igm;
GRANT select ON comp_anal_results_elements TO origami_igm;
GRANT select ON suivi_MAJ TO origami_igm;
-- pour LRI
GRANT select ON elements TO origami_lri;
GRANT select ON organisms TO origami_lri;
GRANT select ON genes TO origami_lri;
GRANT select ON neighbors TO origami_lri;
GRANT select ON alignment_params TO origami_lri;
GRANT select ON alignments TO origami_lri;
GRANT select ON alignment_pairs TO origami_lri;
GRANT select ON homologies TO origami_lri;
GRANT select ON state_descriptions TO origami_lri;
GRANT select ON entry_state_info TO origami_lri;
GRANT select ON gene_states TO origami_lri;
GRANT select ON comp_anal_result_descriptions TO origami_lri;
GRANT select ON comp_anal_results_elements TO origami_lri;
GRANT select ON suivi_MAJ TO origami_lri;
