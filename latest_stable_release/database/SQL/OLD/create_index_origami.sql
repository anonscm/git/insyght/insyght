 -- Script de cr�ation des index et foreign key
-- Cr�� par AGJ Septembre 2006
-- � partir du script seqdb_schema.sql
-- Maj AGJ Juin 2007
-- suite modif sch�ma reprise Micado
-- Maj AGJ Aout 2010
-- Maj AGJ NOvembre 2010
---
---
-- Index � garder pour les Majs : ajout de nouveaux genomes
-- sinon
-- suppression pour la bd maj � remettre en prod (AGJ 11/10)-
---
-- \echo "IDX homologies_q_organism_id"
-- CREATE INDEX homologies_q_organism_id ON homologies USING btree (q_organism_id) tablespace idx;
-- \echo "IDX homologies_s_organism_id"
-- CREATE INDEX homologies_s_organism_id ON homologies USING btree (s_organism_id) tablespace idx;
-- \echo "IDX homologies_s_gene_id"
-- CREATE INDEX homologies_s_gene_id ON homologies USING btree (s_gene_id) tablespace idx;
\echo "IDX homologies_q_element_id"
CREATE INDEX homologies_q_element_id ON homologies USING btree (q_element_id) tablespace idx;
\echo "IDX homologies_s_element_id"
CREATE INDEX homologies_s_element_id ON homologies USING btree (s_element_id) tablespace idx;
-- \echo "IDX homologies_rank"
-- CREATE INDEX homologies_rank ON homologies USING btree (rank) tablespace idx;
-- \echo "IDX homologies_q_gene_id_s_element_id"
-- CREATE INDEX homologies_q_gene_id_s_element_id ON homologies USING btree (q_gene_id, s_element_id) tablespace idx;
-- \echo "IDX homologies_e_value_q_first_frac_s_align_frac"
-- CREATE INDEX homologies_e_value_q_first_frac_s_align_frac ON homologies USING btree (e_value, q_first_frac, s_align_frac) tablespace idx;
--
--
-- supression doublon avec IDX homologies_q_gene_id_s_element_id (AGJ 08/10)
-- \echo "IDX homologies_q_gene_id"
-- CREATE INDEX homologies_q_gene_id ON homologies USING btree (q_gene_id) tablespace idx;
\echo "IDX genes_element_id"
CREATE INDEX genes_element_id ON genes USING btree (element_id) tablespace idx;
\echo "IDX genes_names"
CREATE INDEX genes_names ON genes USING btree (name) tablespace idx;
\echo "IDX genes_organism_id"
CREATE INDEX genes_organism_id ON genes USING btree (organism_id) tablespace idx;

-- Mise en commentaire pour version1 Juin 2007
\echo "alignment_pairs_alignment_id"
CREATE INDEX alignment_pairs_alignment_id ON alignment_pairs USING btree (alignment_id) tablespace idx;

-- CREATE INDEX alignments_alignment_param_id ON alignments USING btree (alignment_param_id) tablespace idx;

-- CREATE INDEX alignments_q_first_gene_positions ON alignments USING btree (q_first_gene_position) tablespace idx;

-- CREATE INDEX alignments_q_last_gene_positions ON alignments USING btree (q_last_gene_position) tablespace idx;
\echo "alignment_pairs_q_gene_id"
CREATE INDEX alignment_pairs_q_gene_id ON alignment_pairs USING btree (q_gene_id) tablespace idx;
\echo "alignment_pairs_s_gene_id"
CREATE INDEX alignment_pairs_s_gene_id ON alignment_pairs USING btree (s_gene_id) tablespace idx;

-- Mise en commentaire pour version1 Juin 2007
--ALTER TABLE ONLY organisms
--    ADD CONSTRAINT "organisms_fk1" FOREIGN KEY (taxon_id) REFERENCES taxons(taxon_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY elements
--    ADD CONSTRAINT "elements_fk1" FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY genes
--    ADD CONSTRAINT "genes_fk1" FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY genes
--    ADD CONSTRAINT "genes_fk2" FOREIGN KEY (element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk1" FOREIGN KEY (gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk2" FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk3" FOREIGN KEY (element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk4" FOREIGN KEY (left_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY neighbors
--    ADD CONSTRAINT "neighbors_fk5" FOREIGN KEY (right_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY alignment_params
--    ADD CONSTRAINT "alignment_params_fk1" FOREIGN KEY (q_organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE CASCADE tablespace idx;

--ALTER TABLE ONLY alignment_params
--    ADD CONSTRAINT "alignment_params_fk2" FOREIGN KEY (q_element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE CASCADE tablespace idx;

--ALTER TABLE ONLY alignment_params
--    ADD CONSTRAINT "alignment_params_fk3" FOREIGN KEY (s_organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE CASCADE tablespace idx;

--ALTER TABLE ONLY alignment_params
--    ADD CONSTRAINT "alignment_param_fk4" FOREIGN KEY (s_element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE CASCADE tablespace idx;

--ALTER TABLE ONLY alignments
--    ADD CONSTRAINT "aligments_fk1" FOREIGN KEY (alignment_param_id) REFERENCES alignment_params(alignment_param_id) ON UPDATE NO ACTION ON DELETE CASCADE tablespace idx;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk1" FOREIGN KEY (q_organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk2" FOREIGN KEY (q_element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk3" FOREIGN KEY (q_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk4" FOREIGN KEY (s_organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk5" FOREIGN KEY (s_element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY homologies
--    ADD CONSTRAINT "homologies_fk6" FOREIGN KEY (s_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY state_descriptions
--    ADD CONSTRAINT state_descriptions_description_key UNIQUE (description) tablespace idx;

--ALTER TABLE ONLY entry_state_info
--    ADD CONSTRAINT "entry_state_info_fk1" FOREIGN KEY (entry_id) REFERENCES entries(entry_id) ON UPDATE NO ACTION ON DELETE CASCADE tablespace idx;

--ALTER TABLE ONLY gene_states
--    ADD CONSTRAINT "gene_states_fk1" FOREIGN KEY (entry_state_info_id) REFERENCES entry_state_info(entry_state_info_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY gene_states
--    ADD CONSTRAINT "gene_states_fk2" FOREIGN KEY (feature_id) REFERENCES features(feature_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY gene_states
--    ADD CONSTRAINT "gene_states_fk3" FOREIGN KEY (dominant_state_id) REFERENCES state_descriptions(state_description_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY comp_anal_result_descriptions
--  ADD CONSTRAINT comp_anal_result_descriptions_description_key UNIQUE (description) tablespace idx;

--ALTER TABLE ONLY comp_anal_results_elements
--    ADD CONSTRAINT "comp_anal_results_elements_fk1" FOREIGN KEY (comp_anal_result_description_id) REFERENCES comp_anal_result_descriptions(comp_anal_result_description_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY comp_anal_results_elements
--    ADD CONSTRAINT "comp_anal_results_elements_fk2" FOREIGN KEY (element_id) REFERENCES elements(element_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY comp_anal_results_elements
--    ADD CONSTRAINT "comp_anal_results_elements_fk3" FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY alignment_pairs
--    ADD CONSTRAINT "alignment_pairs_fk1" FOREIGN KEY (alignment_id) REFERENCES alignments(alignment_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY alignment_pairs
--    ADD CONSTRAINT "alignment_pairs_fk2" FOREIGN KEY (q_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY alignment_pairs
--    ADD CONSTRAINT "alignment_pairs_fk3" FOREIGN KEY (s_gene_id) REFERENCES genes(gene_id) ON UPDATE NO ACTION ON DELETE NO ACTION tablespace idx;

--ALTER TABLE ONLY comp_anal_results_elements
--    ADD CONSTRAINT unique_desc_el_org UNIQUE (comp_anal_result_description_id, element_id, organism_id) tablespace idx;
