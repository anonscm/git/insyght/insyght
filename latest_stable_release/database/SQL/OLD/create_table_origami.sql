-- Script de creation des tables et primary key
-- Cr�� par AGJ Septembre 2006
-- � partir du script seqdb_schema.sql
--
-- Maj par AGJ Novembre 2006 :--> ORIGAMI (Microbiogenomics)
--  -modif table elements pour fusion avec entries
--   (ne garder que 1 description = ancien entries.description)
--  -modif table organisms ajout des attributs strain, substrain, taxon_id
--     et name=species , suppression de shortname
--
-- Maj par AGJ Mars 2007:
-- -suppression des tabls features, features_types, sublocations,
--   qualifiers, qualifier_type, sequences
--  (reprise du mod�le Micado) script: MICADO/ADMIN/SQL/CreeTableSeqNp.sql
--
-- Maj par thomas Mars 2009:
-- ajout de l'attribut number_fragment_hit dans table homologies
-- 
-- Maj par Annie Mai 2012
-- ajout des 4 attributs q_start, q_stop, s_start, s_stop table alignments
--

CREATE SEQUENCE element_seq
    START 1
    INCREMENT 1
    MAXVALUE 9223372036854775807
    MINVALUE 1
    CACHE 1;

--    organism_id integer NOT NULL,
--    "type" character varying(16) DEFAULT 'chromosome' NOT NULL,
--    size integer NOT NULL,
CREATE TABLE elements (
    element_id integer DEFAULT nextval('element_seq'::text) NOT NULL,
    organism_id integer,
    "type" character varying(16) DEFAULT 'chromosome',
    size integer ,
    accession character varying(32) NOT NULL,
    description character varying(512) ,
    mdate timestamp without time zone DEFAULT now() NOT NULL,
    date_seq varchar(20),
    version integer
    CONSTRAINT element_size CHECK ((size > 0)),
    CONSTRAINT element_type CHECK ((("type" = 'chromosome'::character varying) OR ("type" = 'plasmid'::character varying)))
);


CREATE TABLE organisms (
    organism_id integer NOT NULL,
    species character varying(128) NOT NULL,
    strain character varying (255),
    substrain character varying (255),
    taxon_id integer,
    ispublic boolean
);


CREATE TABLE genes (
    gene_id integer NOT NULL,
    organism_id integer NOT NULL,
    element_id integer NOT NULL,
    name character varying(32) NOT NULL,
    strand integer NOT NULL,
    "start" integer NOT NULL,
    stop integer NOT NULL,
    residues text NOT NULL,
    feature_id integer,
    CONSTRAINT gene_size CHECK ((abs((stop - "start")) >= 1)),
    CONSTRAINT strand_values CHECK (((strand = -1) OR (strand = 1)))
);



CREATE TABLE neighbors (
    gene_id integer NOT NULL,
    organism_id integer NOT NULL,
    element_id integer NOT NULL,
    left_gene_id integer,
    left_distance integer,
    right_gene_id integer,
    right_distance integer
);


CREATE TABLE alignment_params (
    alignment_param_id integer DEFAULT nextval('alignment_param_id_seq'::text) NOT NULL,
    q_organism_id integer NOT NULL,
    q_element_id integer NOT NULL,
    s_organism_id integer NOT NULL,
    s_element_id integer NOT NULL,
    ortho_score double precision NOT NULL,
    homo_score double precision NOT NULL,
    mismatch_penalty double precision NOT NULL,
    gap_creation_penalty double precision NOT NULL,
    gap_extension_penalty double precision NOT NULL,
    min_align_size integer NOT NULL,
    min_score double precision NOT NULL,
    orthologs_included boolean NOT NULL,
    mdate timestamp without time zone DEFAULT now() NOT NULL,
    comp_anal_result_description_id integer
);


CREATE SEQUENCE alignment_param_id_seq
    START 1
    INCREMENT 1
    MAXVALUE 9223372036854775807
    MINVALUE 1
    CACHE 1;

CREATE TABLE alignments (
    alignment_id integer DEFAULT nextval('alignment_id_seq'::text) NOT NULL,
    alignment_param_id integer NOT NULL,
    score double precision NOT NULL,
    pairs integer NOT NULL,
    orientation_conserved boolean NOT NULL,
    orthologs integer NOT NULL,
    homologs integer NOT NULL,
    mismatches integer NOT NULL,
    gaps integer NOT NULL,
    total_gap_size integer NOT NULL,
    q_first_gene_position integer NOT NULL,
    q_last_gene_position integer NOT NULL,
    q_size_kb double precision NOT NULL,
    s_first_gene_position integer NOT NULL,
    s_last_gene_position integer NOT NULL,
    s_size_kb double precision NOT NULL,
    q_start integer,
    q_stop integer,
    s_start integer,
    s_stop integer
);


CREATE SEQUENCE alignment_id_seq
    START 1
    INCREMENT 1
    MAXVALUE 9223372036854775807
    MINVALUE 1
    CACHE 1;


CREATE TABLE alignment_pairs (
    alignment_id integer NOT NULL,
    q_gene_id integer,
    s_gene_id integer,
    "type" integer NOT NULL
);


CREATE TABLE homologies (
    q_organism_id integer NOT NULL,
    q_element_id integer NOT NULL,
    q_gene_id integer NOT NULL,
    q_length integer NOT NULL,
    s_organism_id integer NOT NULL,
    s_element_id integer NOT NULL,
    s_gene_id integer NOT NULL,
    s_length integer NOT NULL,
    identity double precision NOT NULL,
    score double precision NOT NULL,
    e_value double precision NOT NULL,
    rank integer NOT NULL,
    q_first integer NOT NULL,
    q_first_frac double precision NOT NULL,
    q_last integer NOT NULL,
    q_last_frac double precision NOT NULL,
    q_align_length integer NOT NULL,
    q_align_frac double precision NOT NULL,
    s_first integer NOT NULL,
    s_first_frac double precision NOT NULL,
    s_last integer NOT NULL,
    s_last_frac double precision NOT NULL,
    s_align_length integer NOT NULL,
    s_align_frac double precision NOT NULL,
    number_fragment_hit integer,
    CONSTRAINT check_lengths CHECK (((q_length > 0) AND (s_length > 0))),
    CONSTRAINT check_q_first CHECK (((q_first > 0) AND (q_first <= q_length))),
    CONSTRAINT check_q_last CHECK (((q_last > 0) AND (q_last <= q_length))),
    CONSTRAINT check_s_first CHECK (((s_first > 0) AND (s_first <= s_length))),
    CONSTRAINT check_s_last CHECK (((s_last > 0) AND (s_last <= s_length)))
);



CREATE SEQUENCE state_description_id_seq
    START 1
    INCREMENT 1
    MAXVALUE 9223372036854775807
    MINVALUE 1
    CACHE 1;


CREATE TABLE state_descriptions (
    state_description_id integer DEFAULT nextval('state_description_id_seq'::text) NOT NULL,
    description character varying(256) NOT NULL
);


CREATE SEQUENCE entry_state_info_id_seq
    START 1
    INCREMENT 1
    MAXVALUE 9223372036854775807
    MINVALUE 1
    CACHE 1;


CREATE TABLE entry_state_info (
    entry_state_info_id integer DEFAULT nextval('entry_state_info_id_seq'::text) NOT NULL,
    element_id integer,
    genes_per_state integer[],
    gene_ratios_per_state double precision[],
    state_description_ids integer[]
);


CREATE TABLE gene_states (
    entry_state_info_id integer NOT NULL,
    feature_id integer NOT NULL,
    state_ratios double precision[],
    dominant_state_id integer NOT NULL
);


CREATE SEQUENCE comp_anal_result_description_seq
    START 1
    INCREMENT 1
    MAXVALUE 9223372036854775807
    MINVALUE 1
    CACHE 1;


CREATE TABLE comp_anal_result_descriptions (
    comp_anal_result_description_id integer DEFAULT nextval('comp_anal_result_description_seq'::text) NOT NULL,
    description character varying(512) NOT NULL
);


CREATE TABLE comp_anal_results_elements (
    comp_anal_result_description_id integer NOT NULL,
    element_id integer NOT NULL,
    organism_id integer NOT NULL
);


CREATE SEQUENCE pair_id_seq
    START 1
    INCREMENT 1
    MAXVALUE 9223372036854775807
    MINVALUE 1
    CACHE 1;

CREATE TABLE suivi_MAJ (
    date_MAJ timestamp without time zone DEFAULT now() NOT NULL,
    accession character varying(32) NOT NULL,
    type_MAJ character NOT NULL
);

ALTER TABLE ONLY elements
    ADD CONSTRAINT elements_pkey PRIMARY KEY (element_id);

--ALTER TABLE ONLY feature_types
--    ADD CONSTRAINT feature_types_pkey PRIMARY KEY (feature_type_id);

--ALTER TABLE ONLY features
--    ADD CONSTRAINT features_pkey PRIMARY KEY (feature_id);

--ALTER TABLE ONLY qualifier_types
--    ADD CONSTRAINT qualifier_types_pkey PRIMARY KEY (qualifier_type_id);

ALTER TABLE ONLY organisms
    ADD CONSTRAINT organisms_pkey PRIMARY KEY (organism_id);

ALTER TABLE ONLY genes
    ADD CONSTRAINT genes_pkey PRIMARY KEY (gene_id);

ALTER TABLE ONLY alignment_params
    ADD CONSTRAINT alignment_params_pkey PRIMARY KEY (alignment_param_id);

ALTER TABLE ONLY alignments
    ADD CONSTRAINT alignments_pkey PRIMARY KEY (alignment_id);

ALTER TABLE ONLY state_descriptions
    ADD CONSTRAINT state_descriptions_pkey PRIMARY KEY (state_description_id);

ALTER TABLE ONLY entry_state_info
    ADD CONSTRAINT entry_state_info_pkey PRIMARY KEY (entry_state_info_id);

ALTER TABLE ONLY comp_anal_result_descriptions
    ADD CONSTRAINT comp_anal_result_descriptions_pkey PRIMARY KEY (comp_anal_result_description_id)
