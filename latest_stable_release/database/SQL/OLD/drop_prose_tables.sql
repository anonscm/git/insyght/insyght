-- Cr�ation Juillet 2007 par AGJ
-- Sch�ma prose

 drop table  ac_numbers                    ;
 drop table  author_list                   ;
 drop table  biblio_db_name_list           ;
 drop table  book                          ;
 drop table  comment_entry                 ;
 drop table  content                       ;
 drop table  electronic_publication        ;
 drop table  feature_entry                 ;
 drop table  feature_list                  ;
 drop table  gazette                       ;
 drop table  gene                          ;
 drop table  journal                       ;
 drop table  journal_list                  ;
 drop table  keyword_entry                 ;
 drop table  keyword_list                  ;
 drop table  patent                        ;
 drop table  protein                       ;
 drop table  ref_cross_ref                 ;
 drop table  ref_database_entry            ;
 drop table  ref_database_list             ;
 drop table  reference_authors             ;
 drop table  reference_comment             ;
 drop table  reference_entry               ;
 drop table  seq_data                      ;
 drop table  seq_header                    ;
 drop table  sp_entry                      ;
 drop table  species                       ;
 drop table  submission                    ;
 drop table  thesis                        ;
 drop table  topic_list                    ;
 drop table  unpublished_observation       ;
 drop table  unpublished_results           ;
 drop table version;
