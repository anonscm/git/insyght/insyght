--###########################################################
--# Nom de la base   : PAREO                                #
--# Description      : Drop of the tables from Pareo        #
--# Maj : Nov 2007 (AGJ) : ajout de 3 tables manquantes     #
--#                        et sequence note_serial       #
--# SGBD      	   : POSTGRESQL                           #
--###########################################################    

 DROP TABLE 
 compound,
 compound_equiv,
 compound_syn,
 enzyme,
 enzyme_ref,
 enzyme_syn,
 gene,
 gen_enz,
 gene_seq,
 gene_syn,
 ortholog,
 gen_ort,
 item,
 link,
 note,
 nte_cmp,
 nte_enz,
 nte_gen,
 nte_ptw,
 nte_rct,
 pathway,
 ptw_cmp,
 ptw_enz,
 ptw_rct,
 ptw_gen,
 rct_cmp,
 rct_enz,
 reaction,
 specie,
ort_enz , 
ptw_ort, 
ptw_ptw
 version
 cascade;


-- Drop des sequences 
drop sequence note_serial ;

