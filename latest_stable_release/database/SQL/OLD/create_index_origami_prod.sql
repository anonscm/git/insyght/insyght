 -- Script de cr�ation des index et foreign key
-- Cr�� par AGJ Novembre 2010
-- Maj Juin 2011
-- index + pour la BD production
\echo "IDX homologies_q_organism_id"
CREATE INDEX homologies_q_organism_id ON homologies USING btree (q_organism_id) tablespace idx;
\echo "IDX homologies_s_organism_id"
CREATE INDEX homologies_s_organism_id ON homologies USING btree (s_organism_id) tablespace idx;
\echo "IDX homologies_s_gene_id"
CREATE INDEX homologies_s_gene_id ON homologies USING btree (s_gene_id) tablespace idx;
\echo "IDX homologies_rank"
CREATE INDEX homologies_rank ON homologies USING btree (rank) tablespace idx;
\echo "IDX homologies_q_gene_id_s_element_id"
CREATE INDEX homologies_q_gene_id_s_element_id ON homologies USING btree (q_gene_id, s_element_id) tablespace idx;
\echo "IDX homologies_e_value_q_first_frac_s_align_frac"
CREATE INDEX homologies_e_value_q_first_frac_s_align_frac ON homologies USING btree (e_value, q_first_frac, s_align_frac) tablespace idx;
\echo "IDX alignment_pairs_q_gene_id"
CREATE INDEX alignment_pairs_q_gene_id ON alignment_pairs USING btree (q_gene_id) tablespace idx;
\echo " IDX alignment_pairs_s_gene_id"
 CREATE INDEX alignment_pairs_s_gene_id ON alignment_pairs USING btree (s_gene_id) tablespace idx;
-- Ajout pour performance de Insyght (AGJ avril 2011)
CREATE INDEX genes_start ON genes (start) tablespace idx;
VACUUM ANALYZE genes;
-- Ajout pour performance de Insyght (AGJ Mars 2012)
CREATE INDEX alignment_params_q_organism_id ON alignment_params (q_organism_id) tablespace idx;
CREATE INDEX alignment_params_q_element_id ON alignment_params (q_element_id) tablespace idx;
CREATE INDEX alignment_params_s_organism_id ON alignment_params (s_organism_id) tablespace idx;
--Ajout pour performance de Insyght (AGJ janvier 2014)
CREATE INDEX alignments_alignment_param_id ON alignments USING btree (alignment_param_id) tablespace idx;