alter table accessions set schema micado;
alter table articles set schema micado;
alter table comments set schema micado;  
alter table complete_genomes set schema micado;
alter table dna_loc set schema micado;
alter table dna_seq set schema micado;
alter table features set schema micado;
alter table keywords set schema micado;
alter table locations set schema micado;
alter table prot_feat set schema micado;
alter table qualifiers set schema micado;
alter table sequences set schema micado;
alter table taxo set schema micado;
alter table version set schema micado;

