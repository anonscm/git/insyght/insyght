\echo "IDX homologies_q_element_id"
CREATE INDEX homologies_q_element_id ON homologies USING btree (q_element_id);
\echo "IDX homologies_s_element_id"
CREATE INDEX homologies_s_element_id ON homologies USING btree (s_element_id);
\echo "IDX homologies_rank"
CREATE INDEX homologies_rank ON homologies USING btree (rank);
\echo "IDX homologies_q_gene_id_s_element_id"
CREATE INDEX homologies_q_gene_id_s_element_id ON homologies USING btree (q_gene_id, s_element_id);
\echo "IDX homologies_e_value_q_first_frac_s_align_frac"
CREATE INDEX homologies_e_value_q_first_frac_s_align_frac ON homologies USING btree (e_value, q_first_frac, s_align_frac);


