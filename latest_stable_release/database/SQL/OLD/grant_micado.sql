-- Script de g�n�ration des droits
-- Cr�� par AGJ Mars 2007
-- 
-- pour origami_admin
GRANT select, update,insert,delete ON sequences TO origami_admin;
GRANT select, update,insert,delete ON features TO origami_admin;
GRANT select, update,insert,delete ON qualifiers TO origami_admin;
GRANT select, update,insert,delete ON prot_feat TO origami_admin;
GRANT select, update,insert,delete ON dna_loc TO origami_admin;
GRANT select, update,insert,delete ON locations TO origami_admin;
GRANT select, update,insert,delete ON dna_seq TO origami_admin;
GRANT select, update,insert,delete ON keywords TO origami_admin;
GRANT select, update,insert,delete ON comments TO origami_admin;
GRANT select, update,insert,delete ON accessions TO origami_admin;
GRANT select, update,insert,delete ON articles TO origami_admin;
GRANT select, update,insert,delete ON taxo TO origami_admin;
GRANT select, update,insert,delete ON taxo_names TO origami_admin;
GRANT select, update,insert,delete ON version TO origami_admin;

-- pour origami_read
GRANT select ON sequences TO origami_read;
GRANT select ON features TO origami_read;
GRANT select ON qualifiers TO origami_read;
GRANT select ON prot_feat TO origami_read;
GRANT select ON dna_loc TO origami_read;
GRANT select ON locations TO origami_read;
GRANT select ON dna_seq TO origami_read;
GRANT select ON keywords TO origami_read;
GRANT select ON comments TO origami_read;
GRANT select ON accessions TO origami_read;
GRANT select ON articles TO origami_read;
GRANT select ON taxo TO origami_read;
GRANT select ON taxo_names TO origami_read;
GRANT select ON version TO origami_read;
-- pour IGM
GRANT select ON sequences TO origami_igm;
GRANT select ON features TO origami_igm;
GRANT select ON qualifiers TO origami_igm;
GRANT select ON prot_feat TO origami_igm;
GRANT select ON dna_loc TO origami_igm;
GRANT select ON locations TO origami_igm;
GRANT select ON dna_seq TO origami_igm;
GRANT select ON keywords TO origami_igm;
GRANT select ON comments TO origami_igm;
GRANT select ON accessions TO origami_igm;
GRANT select ON articles TO origami_igm;
GRANT select ON taxo TO origami_igm;
GRANT select ON taxo_names TO origami_igm;
GRANT select ON version TO origami_igm;
-- pour lri
GRANT select ON sequences TO origami_lri;
GRANT select ON features TO origami_lri;
GRANT select ON qualifiers TO origami_lri;
GRANT select ON prot_feat TO origami_lri;
GRANT select ON dna_loc TO origami_lri;
GRANT select ON locations TO origami_lri;
GRANT select ON dna_seq TO origami_lri;
GRANT select ON keywords TO origami_lri;
GRANT select ON comments TO origami_lri;
GRANT select ON accessions TO origami_lri;
GRANT select ON articles TO origami_lri;
GRANT select ON taxo TO origami_lri;
GRANT select ON taxo_names TO origami_lri;
GRANT select ON version TO origami_lri;

