SET client_min_messages TO WARNING; -- do not print NOTICE messages
-- SET search_path
SET search_path TO micado, public;

-- idx and constraint micado.accessions
ALTER TABLE micado.accessions DROP CONSTRAINT IF EXISTS pk_accessions CASCADE;
ALTER TABLE micado.accessions DROP CONSTRAINT IF EXISTS fk_accessions CASCADE;

-- idx and constraint micado.articles
ALTER TABLE micado.articles DROP CONSTRAINT IF EXISTS pk_articles CASCADE;
ALTER TABLE micado.articles DROP CONSTRAINT IF EXISTS fk_articles CASCADE;

-- idx and constraint micado.comments
ALTER TABLE micado.comments DROP CONSTRAINT IF EXISTS pk_comments CASCADE;
ALTER TABLE micado.comments DROP CONSTRAINT IF EXISTS fk_comments CASCADE;

-- idx and constraint micado.dna_loc
ALTER TABLE micado.dna_loc DROP CONSTRAINT IF EXISTS pk_dnaloc CASCADE;
ALTER TABLE micado.dna_loc DROP CONSTRAINT IF EXISTS fk_dnaloc CASCADE;

-- idx and constraint micado.dna_seq
ALTER TABLE micado.dna_seq DROP CONSTRAINT IF EXISTS pk_dna_seq CASCADE;
ALTER TABLE micado.dna_seq DROP CONSTRAINT IF EXISTS fk_dna_seq CASCADE;


-- idx and constraint micado.keywords
ALTER TABLE micado.keywords DROP CONSTRAINT IF EXISTS pk_keywords CASCADE;
ALTER TABLE micado.keywords DROP CONSTRAINT IF EXISTS fk_keywords CASCADE;

-- idx and constraint micado.locations
ALTER TABLE micado.locations DROP CONSTRAINT IF EXISTS pk_locations CASCADE;
ALTER TABLE micado.locations DROP CONSTRAINT IF EXISTS fk_locations CASCADE;

-- idx and constraint micado.prot_feat
ALTER TABLE micado.prot_feat DROP CONSTRAINT IF EXISTS pk_protfeat CASCADE;
ALTER TABLE micado.prot_feat DROP CONSTRAINT IF EXISTS fk_protfeat CASCADE;

-- idx and constraint micado.qualifiers
ALTER TABLE micado.qualifiers DROP CONSTRAINT IF EXISTS pk_qualifiers CASCADE;
ALTER TABLE micado.qualifiers DROP CONSTRAINT IF EXISTS fk_qualifiers CASCADE;

-- idx and constraint micado.sequences
ALTER TABLE micado.sequences DROP CONSTRAINT IF EXISTS pk_sequences CASCADE;

-- idx and constraint micado.groupe_info
--ALTER TABLE micado.groupe_info DROP CONSTRAINT IF EXISTS groupe_info_pkey CASCADE;
--ALTER TABLE micado.groupe_info DROP CONSTRAINT IF EXISTS fk_organisms CASCADE;

-- idx and constraint micado.user_groupe
--ALTER TABLE micado.user_groupe DROP CONSTRAINT IF EXISTS fk_groupe CASCADE;
--ALTER TABLE micado.user_groupe DROP CONSTRAINT IF EXISTS fk_user CASCADE;

-- idx and constraint micado.user_info
--ALTER TABLE micado.user_info DROP CONSTRAINT IF EXISTS user_info_pkey CASCADE;

-- idx and constraint micado.features
ALTER TABLE micado.features DROP CONSTRAINT IF EXISTS pk_features CASCADE;
ALTER TABLE micado.features DROP CONSTRAINT IF EXISTS fk_features CASCADE;

-- idx and constraint elements
ALTER TABLE elements DROP CONSTRAINT IF EXISTS elements_pkey CASCADE;
ALTER TABLE elements DROP CONSTRAINT IF EXISTS element_size CASCADE;
ALTER TABLE elements DROP CONSTRAINT IF EXISTS organism_id_fk CASCADE;
ALTER TABLE elements DROP CONSTRAINT IF EXISTS accession_fk CASCADE;

-- idx and constraint genes
ALTER TABLE genes DROP CONSTRAINT IF EXISTS genes_pkey CASCADE;
DROP INDEX IF EXISTS genes_element_id;
DROP INDEX IF EXISTS genes_names;
DROP INDEX IF EXISTS genes_organism_id;
DROP INDEX IF EXISTS genes_start;
ALTER TABLE genes DROP CONSTRAINT IF EXISTS gene_size CASCADE;
ALTER TABLE genes DROP CONSTRAINT IF EXISTS strand_values CASCADE;
ALTER TABLE genes DROP CONSTRAINT IF EXISTS organism_id_fk CASCADE;
ALTER TABLE genes DROP CONSTRAINT IF EXISTS element_id_fk CASCADE;
ALTER TABLE genes DROP CONSTRAINT IF EXISTS feature_id_fk CASCADE;

-- idx and constraint organisms
ALTER TABLE organisms DROP CONSTRAINT IF EXISTS organisms_pkey CASCADE;

-- constraint q_element_id_2_sorted_list_comp_orga_whole
ALTER TABLE q_element_id_2_sorted_list_comp_orga_whole DROP CONSTRAINT IF EXISTS q_element_id_2_sorted_list_comp_orga_whole_pkey CASCADE;
ALTER TABLE q_element_id_2_sorted_list_comp_orga_whole DROP CONSTRAINT IF EXISTS element_id_fk CASCADE;


