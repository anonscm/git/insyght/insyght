SET client_min_messages TO WARNING; -- do not print NOTICE messages
-- SET search_path
SET search_path TO micado, public;

-- idx and constraint micado.sequences
CREATE UNIQUE INDEX pk_sequences ON micado.sequences (accession);-- index fast access
ALTER TABLE micado.sequences ADD PRIMARY KEY USING INDEX pk_sequences;-- index fast access

-- idx and constraint micado.features
CREATE UNIQUE INDEX pk_features ON micado.features (accession, code_feat);-- index fast access
ALTER TABLE micado.features ADD PRIMARY KEY USING INDEX pk_features;-- index fast access
ALTER TABLE micado.features ADD CONSTRAINT fk_features FOREIGN KEY (accession) REFERENCES sequences(accession) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.accessions
CREATE UNIQUE INDEX pk_accessions ON micado.accessions (accession, old_accession);-- index fast access
ALTER TABLE micado.accessions ADD PRIMARY KEY USING INDEX pk_accessions;-- index fast access
ALTER TABLE micado.accessions ADD CONSTRAINT fk_accessions FOREIGN KEY (accession) REFERENCES sequences(accession) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.articles
CREATE UNIQUE INDEX pk_articles ON micado.articles (accession, num_art);-- index fast access
ALTER TABLE micado.articles ADD PRIMARY KEY USING INDEX pk_articles;-- index fast access
ALTER TABLE micado.articles ADD CONSTRAINT fk_articles FOREIGN KEY (accession) REFERENCES sequences(accession) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.comments
CREATE UNIQUE INDEX pk_comments ON micado.comments (accession);-- index fast access
ALTER TABLE micado.comments ADD PRIMARY KEY USING INDEX pk_comments;-- index fast access
ALTER TABLE micado.comments ADD CONSTRAINT fk_comments FOREIGN KEY (accession) REFERENCES sequences(accession) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.dna_loc
CREATE UNIQUE INDEX pk_dnaloc ON micado.dna_loc (accession, code_feat);-- index fast access
ALTER TABLE micado.dna_loc ADD PRIMARY KEY USING INDEX pk_dnaloc;-- index fast access
ALTER TABLE micado.dna_loc ADD CONSTRAINT fk_dnaloc FOREIGN KEY (accession, code_feat) REFERENCES features(accession, code_feat) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.dna_seq
CREATE UNIQUE INDEX pk_dna_seq ON micado.dna_seq (accession);-- index fast access
ALTER TABLE micado.dna_seq ADD PRIMARY KEY USING INDEX pk_dna_seq;-- index fast access
ALTER TABLE micado.dna_seq ADD CONSTRAINT fk_dna_seq FOREIGN KEY (accession) REFERENCES sequences(accession) MATCH FULL ON DELETE CASCADE;


-- idx and constraint micado.keywords
CREATE UNIQUE INDEX pk_keywords ON micado.keywords (accession);-- index fast access
ALTER TABLE micado.keywords ADD PRIMARY KEY USING INDEX pk_keywords;-- index fast access
ALTER TABLE micado.keywords ADD CONSTRAINT fk_keywords FOREIGN KEY (accession) REFERENCES sequences(accession) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.locations
CREATE UNIQUE INDEX pk_locations ON micado.locations (accession, code_feat, code_loc);-- index fast access
ALTER TABLE micado.locations ADD PRIMARY KEY USING INDEX pk_locations;-- index fast access
ALTER TABLE micado.locations ADD CONSTRAINT fk_locations FOREIGN KEY (accession, code_feat) REFERENCES features(accession, code_feat) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.prot_feat
CREATE UNIQUE INDEX pk_protfeat ON micado.prot_feat (accession, code_feat);-- index fast access
ALTER TABLE micado.prot_feat ADD PRIMARY KEY USING INDEX pk_protfeat;-- index fast access
ALTER TABLE micado.prot_feat ADD CONSTRAINT fk_protfeat FOREIGN KEY (accession, code_feat) REFERENCES features(accession, code_feat) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.qualifiers
CREATE UNIQUE INDEX pk_qualifiers ON micado.qualifiers (accession, code_feat, code_qual);-- index fast access
ALTER TABLE micado.qualifiers ADD PRIMARY KEY USING INDEX pk_qualifiers;-- index fast access
ALTER TABLE micado.qualifiers ADD CONSTRAINT fk_qualifiers FOREIGN KEY (accession, code_feat) REFERENCES features(accession, code_feat) MATCH FULL ON DELETE CASCADE;


-- idx and constraint micado.groupe_info
-- CREATE UNIQUE INDEX groupe_info_pkey ON micado.groupe_info (groupe_id);-- index fast access
-- ALTER TABLE micado.groupe_info ADD PRIMARY KEY USING INDEX groupe_info_pkey;-- index fast access
-- ALTER TABLE micado.groupe_info ADD CONSTRAINT fk_organisms FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.user_groupe
-- ALTER TABLE micado.user_groupe ADD CONSTRAINT fk_groupe FOREIGN KEY (groupe_id) REFERENCES groupe_info(groupe_id) MATCH FULL ON DELETE CASCADE;
-- ALTER TABLE micado.user_groupe ADD CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES user_info(user_id) MATCH FULL ON DELETE CASCADE;

-- idx and constraint micado.user_info
-- CREATE UNIQUE INDEX user_info_pkey ON micado.user_info (user_id);-- index fast access
-- ALTER TABLE micado.user_info ADD PRIMARY KEY USING INDEX user_info_pkey;-- index fast access

-- idx and constraint organisms
CREATE UNIQUE INDEX organisms_pkey ON organisms (organism_id);-- index fast access
ALTER TABLE organisms ADD PRIMARY KEY USING INDEX organisms_pkey;-- index fast access

-- idx and constraint elements
CREATE UNIQUE INDEX elements_pkey ON elements (element_id);-- index fast access
ALTER TABLE elements ADD PRIMARY KEY USING INDEX elements_pkey;-- index fast access
ALTER TABLE elements ADD CONSTRAINT element_size CHECK (size > 0);--
ALTER TABLE elements ADD CONSTRAINT organism_id_fk FOREIGN KEY (organism_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE elements ADD CONSTRAINT accession_fk FOREIGN KEY (accession) REFERENCES micado.sequences (accession) MATCH FULL ON DELETE CASCADE;

-- idx and constraint genes
CREATE UNIQUE INDEX genes_pkey ON genes (gene_id);-- index fast access
ALTER TABLE genes ADD PRIMARY KEY USING INDEX genes_pkey;-- index fast access
CREATE INDEX genes_element_id ON genes (element_id);-- index fast access
CREATE INDEX genes_names ON genes (name);-- index fast access
CREATE INDEX genes_organism_id ON genes (organism_id);-- index fast access
CREATE INDEX genes_start ON genes (start);-- index fast access
ALTER TABLE genes ADD CONSTRAINT gene_size CHECK (abs(stop - start) >= 1);--
ALTER TABLE genes ADD CONSTRAINT strand_values CHECK (strand = '-1'::integer OR strand = 1);--
ALTER TABLE genes ADD CONSTRAINT organism_id_fk FOREIGN KEY (organism_id) REFERENCES organisms (organism_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE genes ADD CONSTRAINT element_id_fk FOREIGN KEY (element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;
ALTER TABLE genes ADD CONSTRAINT feature_id_fk FOREIGN KEY (accession, feature_id) REFERENCES micado.features (accession, code_feat) MATCH FULL;



-- constraint q_element_id_2_sorted_list_comp_orga_whole
CREATE UNIQUE INDEX q_element_id_2_sorted_list_comp_orga_whole_pkey ON q_element_id_2_sorted_list_comp_orga_whole (element_id);-- index fast access
ALTER TABLE q_element_id_2_sorted_list_comp_orga_whole ADD PRIMARY KEY USING INDEX q_element_id_2_sorted_list_comp_orga_whole_pkey;-- index fast access
ALTER TABLE q_element_id_2_sorted_list_comp_orga_whole ADD CONSTRAINT element_id_fk FOREIGN KEY (element_id) REFERENCES elements (element_id) MATCH FULL ON DELETE CASCADE;



