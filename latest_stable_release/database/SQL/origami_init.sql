--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: micado; Type: SCHEMA; Schema: -; Owner: origami_admin
--

CREATE SCHEMA micado;


ALTER SCHEMA micado OWNER TO origami_admin;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = micado, pg_catalog;

--
-- Name: truncate_tables_micado(text); Type: FUNCTION; Schema: micado; Owner: origami_admin
--

CREATE FUNCTION truncate_tables_micado(_username text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
tbl text;
BEGIN
FOR tbl IN
SELECT t.tablename
FROM pg_tables t
WHERE t.tableowner = _username
AND t.schemaname = 'micado'
LOOP
EXECUTE 'TRUNCATE TABLE ' || quote_ident(tbl) || ' CASCADE';
END LOOP;
END
$$;


ALTER FUNCTION micado.truncate_tables_micado(_username text) OWNER TO origami_admin;

--
-- Name: truncate_tables_public(text); Type: FUNCTION; Schema: micado; Owner: origami_admin
--

CREATE FUNCTION truncate_tables_public(_username text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
tbl text;
BEGIN
FOR tbl IN
SELECT t.tablename
FROM pg_tables t
WHERE t.tableowner = _username
AND t.schemaname = 'public'
LOOP
EXECUTE 'TRUNCATE TABLE ' || quote_ident(tbl) || ' CASCADE';
END LOOP;
END
$$;


ALTER FUNCTION micado.truncate_tables_public(_username text) OWNER TO origami_admin;

SET search_path = public, pg_catalog;

--
-- Name: truncate_tables_micado(text); Type: FUNCTION; Schema: public; Owner: origami_admin
--

CREATE FUNCTION truncate_tables_micado(_username text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
tbl text;
BEGIN
FOR tbl IN
SELECT t.tablename
FROM pg_tables t
WHERE t.tableowner = _username
AND t.schemaname = 'micado'
LOOP
EXECUTE 'TRUNCATE TABLE ' || quote_ident(tbl) || ' CASCADE';
END LOOP;
END
$$;


ALTER FUNCTION public.truncate_tables_micado(_username text) OWNER TO origami_admin;

--
-- Name: truncate_tables_public(text); Type: FUNCTION; Schema: public; Owner: origami_admin
--

CREATE FUNCTION truncate_tables_public(_username text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
tbl text;
BEGIN
FOR tbl IN
SELECT t.tablename
FROM pg_tables t
WHERE t.tableowner = _username
AND t.schemaname = 'public'
LOOP
EXECUTE 'TRUNCATE TABLE ' || quote_ident(tbl) || ' CASCADE';
END LOOP;
END
$$;


ALTER FUNCTION public.truncate_tables_public(_username text) OWNER TO origami_admin;

SET search_path = micado, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accessions; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE accessions (
    accession character varying(100) NOT NULL,
    old_accession character varying(100) NOT NULL
);


ALTER TABLE accessions OWNER TO origami_admin;

--
-- Name: articles; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE articles (
    accession character varying(100) NOT NULL,
    num_art integer NOT NULL,
    title character varying(2000),
    author_list character varying(2000) NOT NULL,
    location character varying(1000),
    medline character varying(20)
);


ALTER TABLE articles OWNER TO origami_admin;

--
-- Name: comments; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE comments (
    accession character varying(100) NOT NULL,
    comments text
);


ALTER TABLE comments OWNER TO origami_admin;

--
-- Name: dna_loc; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE dna_loc (
    accession character varying(100) NOT NULL,
    code_feat integer NOT NULL,
    sequences text NOT NULL
);


ALTER TABLE dna_loc OWNER TO origami_admin;

--
-- Name: dna_seq; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE dna_seq (
    accession character varying(100) NOT NULL,
    sequences text
);


ALTER TABLE dna_seq OWNER TO origami_admin;

--
-- Name: features; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE features (
    accession character varying(100) NOT NULL,
    code_feat integer NOT NULL,
    type_feat character varying(50) NOT NULL,
    location text NOT NULL,
    operator_feat character varying(50),
    posmin integer,
    posmax integer,
    strand integer
);


ALTER TABLE features OWNER TO origami_admin;

--
-- Name: keywords; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE keywords (
    accession character varying(100) NOT NULL,
    lst_keyword text
);


ALTER TABLE keywords OWNER TO origami_admin;

--
-- Name: locations; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE locations (
    accession character varying(100) NOT NULL,
    code_feat integer NOT NULL,
    code_loc integer NOT NULL,
    startpos_begin integer NOT NULL,
    startpos_end integer NOT NULL,
    startpos_type character varying(30) NOT NULL,
    stoppos_begin integer NOT NULL,
    stoppos_end integer NOT NULL,
    stoppos_type character varying(30) NOT NULL,
    strand integer NOT NULL,
    link_access character varying(50)
);


ALTER TABLE locations OWNER TO origami_admin;

--
-- Name: ncbi_taxonomy_tree; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE ncbi_taxonomy_tree (
    ncbi_taxid integer NOT NULL,
    parent_taxid integer NOT NULL,
    lineage integer[],
    rank character varying(40),
    depth integer NOT NULL,
    is_leaf boolean NOT NULL,
    scientific_name character varying(500) NOT NULL
);


ALTER TABLE ncbi_taxonomy_tree OWNER TO origami_admin;

--
-- Name: prot_feat; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE prot_feat (
    accession character varying(100) NOT NULL,
    code_feat integer NOT NULL,
    proteine text NOT NULL,
    length integer NOT NULL,
    crc character(16) NOT NULL
);


ALTER TABLE prot_feat OWNER TO origami_admin;

--
-- Name: qualifiers; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE qualifiers (
    accession character varying(100) NOT NULL,
    code_feat integer NOT NULL,
    code_qual integer NOT NULL,
    type_qual character varying(50) NOT NULL,
    qualifier text
);


ALTER TABLE qualifiers OWNER TO origami_admin;

--
-- Name: seq_group_id; Type: SEQUENCE; Schema: micado; Owner: origami_admin
--

CREATE SEQUENCE seq_group_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_group_id OWNER TO origami_admin;

--
-- Name: seq_user_id; Type: SEQUENCE; Schema: micado; Owner: origami_admin
--

CREATE SEQUENCE seq_user_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_user_id OWNER TO origami_admin;

--
-- Name: sequences; Type: TABLE; Schema: micado; Owner: origami_admin
--

CREATE TABLE sequences (
    accession character varying(100) NOT NULL,
    name_seq character varying(100) NOT NULL,
    species character varying(200) NOT NULL,
    ncbi_taxid integer,
    length integer NOT NULL,
    molecule character varying(30),
    subbank character varying(30),
    date_seq character varying(100) NOT NULL,
    definition character varying(2000),
    version integer
);


ALTER TABLE sequences OWNER TO origami_admin;

SET search_path = public, pg_catalog;

--
-- Name: alignment_id_seq; Type: SEQUENCE; Schema: public; Owner: origami_admin
--

CREATE SEQUENCE alignment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alignment_id_seq OWNER TO origami_admin;

--
-- Name: alignment_pairs; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE alignment_pairs (
    alignment_id bigint NOT NULL,
    q_gene_id integer,
    s_gene_id integer,
    type integer NOT NULL
);


ALTER TABLE alignment_pairs OWNER TO origami_admin;

--
-- Name: alignment_param_id_seq; Type: SEQUENCE; Schema: public; Owner: origami_admin
--

CREATE SEQUENCE alignment_param_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alignment_param_id_seq OWNER TO origami_admin;

--
-- Name: alignment_params; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE alignment_params (
    alignment_param_id bigint DEFAULT nextval(('alignment_param_id_seq'::text)::regclass) NOT NULL,
    q_organism_id integer NOT NULL,
    q_element_id integer NOT NULL,
    s_organism_id integer NOT NULL,
    s_element_id integer NOT NULL,
    mdate timestamp without time zone DEFAULT now() NOT NULL,
    comp_anal_result_description_id integer
);


ALTER TABLE alignment_params OWNER TO origami_admin;

--
-- Name: alignments; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE alignments (
    alignment_id bigint DEFAULT nextval(('alignment_id_seq'::text)::regclass) NOT NULL,
    alignment_param_id bigint NOT NULL,
    score double precision NOT NULL,
    pairs integer NOT NULL,
    orientation_conserved boolean NOT NULL,
    orthologs integer NOT NULL,
    homologs integer NOT NULL,
    mismatches integer NOT NULL,
    gaps integer NOT NULL,
    total_gap_size integer NOT NULL,
    q_size_kb double precision NOT NULL,
    s_size_kb double precision NOT NULL,
    q_start integer,
    q_stop integer,
    s_start integer,
    s_stop integer
);


ALTER TABLE alignments OWNER TO origami_admin;

--
-- Name: close_best_match; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE close_best_match (
    close_best_match_id integer NOT NULL,
    q_organims_id integer NOT NULL,
    q_element_id integer NOT NULL,
    q_gene_id integer NOT NULL,
    s_organims_id integer NOT NULL,
    is_mirror_data boolean NOT NULL,
    number_of_close_matchs integer NOT NULL,
    contain_bdbh boolean NOT NULL,
    pid_best_match double precision NOT NULL,
    evalue_best_match double precision NOT NULL,
    bits_best_match double precision NOT NULL,
    list_close_match_gene_ids character varying(5000) NOT NULL
);


ALTER TABLE close_best_match OWNER TO origami_admin;

--
-- Name: element_seq; Type: SEQUENCE; Schema: public; Owner: origami_admin
--

CREATE SEQUENCE element_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE element_seq OWNER TO origami_admin;

--
-- Name: elements; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE elements (
    element_id integer DEFAULT nextval(('element_seq'::text)::regclass) NOT NULL,
    organism_id integer,
    type character varying(100) DEFAULT 'chromosome'::character varying,
    size integer,
    accession character varying(100) NOT NULL,
    mdate timestamp without time zone DEFAULT now() NOT NULL,
    ncbi_internal_id character varying(64),
    ncbi_sourcedb character varying(32),
    ncbi_tech character varying(100),
    ncbi_geneticcode character varying(32),
    ncbi_topology character varying(32),
    ncbi_completeness character varying(32),
    ncbi_status character varying(32),
    ncbi_comment character varying(2000),
    CONSTRAINT element_size CHECK ((size > 0))
);


ALTER TABLE elements OWNER TO origami_admin;

--
-- Name: genes; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE genes (
    gene_id integer NOT NULL,
    organism_id integer NOT NULL,
    element_id integer NOT NULL,
    name character varying(300),
    strand integer NOT NULL,
    start integer NOT NULL,
    stop integer NOT NULL,
    feature_id integer,
    locus_tag character varying(300),
    accession character varying(100) NOT NULL,
    is_pseudo boolean NOT NULL,
    length_residues integer NOT NULL,
    CONSTRAINT gene_size CHECK ((abs((stop - start)) >= 1)),
    CONSTRAINT strand_values CHECK (((strand = '-1'::integer) OR (strand = 1)))
);


ALTER TABLE genes OWNER TO origami_admin;

--
-- Name: seq_group_id; Type: SEQUENCE; Schema: public; Owner: origami_admin
--

CREATE SEQUENCE seq_group_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_group_id OWNER TO origami_admin;

--
-- Name: groupe_info; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE groupe_info (
    groupe_id integer DEFAULT nextval('seq_group_id'::regclass) NOT NULL,
    organism_id integer,
    libelle character varying(50),
    acces_type character varying(10)
);


ALTER TABLE groupe_info OWNER TO origami_admin;

--
-- Name: homologies; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE homologies (
    q_organism_id integer NOT NULL,
    q_element_id integer NOT NULL,
    q_gene_id integer NOT NULL,
    q_length integer NOT NULL,
    s_organism_id integer NOT NULL,
    s_element_id integer NOT NULL,
    s_gene_id integer NOT NULL,
    s_length integer NOT NULL,
    identity double precision NOT NULL,
    score double precision NOT NULL,
    e_value double precision NOT NULL,
    rank integer NOT NULL,
    q_first integer NOT NULL,
    q_first_frac double precision NOT NULL,
    q_last integer NOT NULL,
    q_last_frac double precision NOT NULL,
    q_align_length integer NOT NULL,
    q_align_frac double precision NOT NULL,
    s_first integer NOT NULL,
    s_first_frac double precision NOT NULL,
    s_last integer NOT NULL,
    s_last_frac double precision NOT NULL,
    s_align_length integer NOT NULL,
    s_align_frac double precision NOT NULL,
    number_fragment_hit integer,
    CONSTRAINT check_lengths CHECK (((q_length > 0) AND (s_length > 0))),
    CONSTRAINT check_q_first CHECK (((q_first > 0) AND (q_first <= q_length))),
    CONSTRAINT check_q_last CHECK (((q_last > 0) AND (q_last <= q_length))),
    CONSTRAINT check_s_first CHECK (((s_first > 0) AND (s_first <= s_length))),
    CONSTRAINT check_s_last CHECK (((s_last > 0) AND (s_last <= s_length)))
);


ALTER TABLE homologies OWNER TO origami_admin;

--
-- Name: isbranchedtoanothersynteny; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE isbranchedtoanothersynteny (
    alignment_id_branched bigint NOT NULL,
    alignment_param_id bigint NOT NULL,
    branche_on_q_gene_id integer NOT NULL,
    branche_on_s_gene_id integer NOT NULL
);


ALTER TABLE isbranchedtoanothersynteny OWNER TO origami_admin;

--
-- Name: num_code_loc; Type: SEQUENCE; Schema: public; Owner: origami_admin
--

CREATE SEQUENCE num_code_loc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE num_code_loc OWNER TO origami_admin;

--
-- Name: organisms; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE organisms (
    organism_id integer NOT NULL,
    species character varying(128) NOT NULL,
    strain character varying(255),
    substrain character varying(255),
    taxon_id integer,
    ispublic boolean DEFAULT true,
    ncbi_internal_id_it character varying(64),
    ncbi_assemblyaccession_it character varying(64),
    ncbi_assemblyname_it character varying(300),
    ncbi_lastupdatedate_it timestamp without time zone,
    ncbi_seqreleasedate_it timestamp without time zone,
    ncbi_isolate_it character varying(64),
    ncbi_speciestaxid_it character varying(32),
    ncbi_biosampleid_it character varying(64),
    ncbi_biosampleaccn_it character varying(64),
    ncbi_list_bioprojectid_it character varying(200),
    ncbi_list_bioprojectaccn_it character varying(200),
    ncbi_assemblyclass_it character varying(32),
    ncbi_assemblystatus_it character varying(32),
    alt_strain character varying(255),
    serotype character varying(255),
    computation_in_process boolean DEFAULT true NOT NULL
);


ALTER TABLE organisms OWNER TO origami_admin;

--
-- Name: pair_id_seq; Type: SEQUENCE; Schema: public; Owner: origami_admin
--

CREATE SEQUENCE pair_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pair_id_seq OWNER TO origami_admin;

--
-- Name: params_scores_algo_syntenies; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE params_scores_algo_syntenies (
    ortho_score double precision NOT NULL,
    homo_score double precision NOT NULL,
    mismatch_penalty double precision NOT NULL,
    gap_creation_penalty double precision NOT NULL,
    gap_extension_penalty double precision NOT NULL,
    min_align_size integer NOT NULL,
    min_score double precision NOT NULL,
    orthologs_included boolean NOT NULL,
    ortho_min_prot_frac double precision NOT NULL,
    genefusions_minpctlenghtprotthres double precision NOT NULL,
    closebestmatchs_minpctthres double precision NOT NULL,
    tandemdups_minpctwithinbestpidthres double precision NOT NULL,
    max_gap_size_for_creation_penalty integer NOT NULL
);


ALTER TABLE params_scores_algo_syntenies OWNER TO origami_admin;

--
-- Name: prot_fusion; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE prot_fusion (
    prot_fusion_id integer NOT NULL,
    q_organims_id integer NOT NULL,
    q_element_id integer NOT NULL,
    q_gene_id integer NOT NULL,
    s_organims_id integer NOT NULL,
    s_element_id integer NOT NULL,
    s_gene_id integer NOT NULL,
    ismirrordata boolean NOT NULL,
    s_rank_among_all_s_fusions integer NOT NULL,
    lengthtotalcoveragesonq integer NOT NULL,
    lenghtnewcontribcoveragesonq integer NOT NULL,
    pid double precision NOT NULL,
    hsp_len integer NOT NULL,
    mismatches integer NOT NULL,
    gaps integer NOT NULL,
    qstart integer NOT NULL,
    qend integer NOT NULL,
    qlength integer NOT NULL,
    hstart integer NOT NULL,
    hend integer NOT NULL,
    hlength integer NOT NULL,
    evalue double precision NOT NULL,
    bits double precision NOT NULL,
    bdbh integer NOT NULL,
    rank integer NOT NULL,
    geneidbestsinglelongmatchhidingprotfusion integer NOT NULL,
    numbersinglelongmatchhidingprotfusion integer NOT NULL,
    numberprotfusiondetectedbeforethisone integer NOT NULL
);


ALTER TABLE prot_fusion OWNER TO origami_admin;

--
-- Name: q_element_id_2_sorted_list_comp_orga_whole; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE q_element_id_2_sorted_list_comp_orga_whole (
    element_id integer NOT NULL,
    abundance_homologs text,
    synteny_score text,
    alignemnt_score text,
    abundance_homo_annotations text
);


ALTER TABLE q_element_id_2_sorted_list_comp_orga_whole OWNER TO origami_admin;

--
-- Name: seq_user_id; Type: SEQUENCE; Schema: public; Owner: origami_admin
--

CREATE SEQUENCE seq_user_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_user_id OWNER TO origami_admin;

--
-- Name: suivi_maj; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE suivi_maj (
    date_maj timestamp without time zone DEFAULT now() NOT NULL,
    accession character varying(100) NOT NULL,
    type_maj character(1) NOT NULL
);


ALTER TABLE suivi_maj OWNER TO origami_admin;

--
-- Name: tandem_dups; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE tandem_dups (
    tandem_dups_id integer NOT NULL,
    alignment_param_id bigint NOT NULL,
    gene_id_single_is_q boolean NOT NULL,
    single_organims_id integer NOT NULL,
    single_element_id integer NOT NULL,
    single_gene_id integer NOT NULL,
    tandem_organims_id integer NOT NULL,
    tandem_element_id integer NOT NULL,
    tandem_gene_id integer NOT NULL,
    pid double precision NOT NULL,
    hsp_len integer NOT NULL,
    mismatches integer NOT NULL,
    gaps integer NOT NULL,
    qstart integer NOT NULL,
    qend integer NOT NULL,
    qlength integer NOT NULL,
    hstart integer NOT NULL,
    hend integer NOT NULL,
    hlength integer NOT NULL,
    evalue double precision NOT NULL,
    bits double precision NOT NULL,
    bdbh integer NOT NULL,
    rank integer NOT NULL,
    idx_in_dup integer NOT NULL
);


ALTER TABLE tandem_dups OWNER TO origami_admin;

--
-- Name: user_groupe; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE user_groupe (
    groupe_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE user_groupe OWNER TO origami_admin;

--
-- Name: user_info; Type: TABLE; Schema: public; Owner: origami_admin
--

CREATE TABLE user_info (
    user_id integer DEFAULT nextval('seq_user_id'::regclass) NOT NULL,
    labo character varying(255),
    nom character varying(50) NOT NULL,
    prenom character varying(50) NOT NULL,
    session_id character varying(100),
    login character varying(100) NOT NULL,
    password character varying(100) NOT NULL
);


ALTER TABLE user_info OWNER TO origami_admin;

SET search_path = micado, pg_catalog;

--
-- Data for Name: accessions; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY accessions (accession, old_accession) FROM stdin;
\.


--
-- Data for Name: articles; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY articles (accession, num_art, title, author_list, location, medline) FROM stdin;
\.


--
-- Data for Name: comments; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY comments (accession, comments) FROM stdin;
\.


--
-- Data for Name: dna_loc; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY dna_loc (accession, code_feat, sequences) FROM stdin;
\.


--
-- Data for Name: dna_seq; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY dna_seq (accession, sequences) FROM stdin;
\.


--
-- Data for Name: features; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY features (accession, code_feat, type_feat, location, operator_feat, posmin, posmax, strand) FROM stdin;
\.


--
-- Data for Name: keywords; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY keywords (accession, lst_keyword) FROM stdin;
\.


--
-- Data for Name: locations; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY locations (accession, code_feat, code_loc, startpos_begin, startpos_end, startpos_type, stoppos_begin, stoppos_end, stoppos_type, strand, link_access) FROM stdin;
\.


--
-- Data for Name: ncbi_taxonomy_tree; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY ncbi_taxonomy_tree (ncbi_taxid, parent_taxid, lineage, rank, depth, is_leaf, scientific_name) FROM stdin;
\.


--
-- Data for Name: prot_feat; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY prot_feat (accession, code_feat, proteine, length, crc) FROM stdin;
\.


--
-- Data for Name: qualifiers; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY qualifiers (accession, code_feat, code_qual, type_qual, qualifier) FROM stdin;
\.


--
-- Name: seq_group_id; Type: SEQUENCE SET; Schema: micado; Owner: origami_admin
--

SELECT pg_catalog.setval('seq_group_id', 1, false);


--
-- Name: seq_user_id; Type: SEQUENCE SET; Schema: micado; Owner: origami_admin
--

SELECT pg_catalog.setval('seq_user_id', 1, false);


--
-- Data for Name: sequences; Type: TABLE DATA; Schema: micado; Owner: origami_admin
--

COPY sequences (accession, name_seq, species, ncbi_taxid, length, molecule, subbank, date_seq, definition, version) FROM stdin;
\.


SET search_path = public, pg_catalog;

--
-- Name: alignment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: origami_admin
--

SELECT pg_catalog.setval('alignment_id_seq', 1, false);


--
-- Data for Name: alignment_pairs; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY alignment_pairs (alignment_id, q_gene_id, s_gene_id, type) FROM stdin;
\.


--
-- Name: alignment_param_id_seq; Type: SEQUENCE SET; Schema: public; Owner: origami_admin
--

SELECT pg_catalog.setval('alignment_param_id_seq', 1, false);


--
-- Data for Name: alignment_params; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY alignment_params (alignment_param_id, q_organism_id, q_element_id, s_organism_id, s_element_id, mdate, comp_anal_result_description_id) FROM stdin;
\.


--
-- Data for Name: alignments; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY alignments (alignment_id, alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_size_kb, s_size_kb, q_start, q_stop, s_start, s_stop) FROM stdin;
\.


--
-- Data for Name: close_best_match; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY close_best_match (close_best_match_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, is_mirror_data, number_of_close_matchs, contain_bdbh, pid_best_match, evalue_best_match, bits_best_match, list_close_match_gene_ids) FROM stdin;
\.


--
-- Name: element_seq; Type: SEQUENCE SET; Schema: public; Owner: origami_admin
--

SELECT pg_catalog.setval('element_seq', 1, false);


--
-- Data for Name: elements; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY elements (element_id, organism_id, type, size, accession, mdate, ncbi_internal_id, ncbi_sourcedb, ncbi_tech, ncbi_geneticcode, ncbi_topology, ncbi_completeness, ncbi_status, ncbi_comment) FROM stdin;
\.


--
-- Data for Name: genes; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY genes (gene_id, organism_id, element_id, name, strand, start, stop, feature_id, locus_tag, accession, is_pseudo, length_residues) FROM stdin;
\.


--
-- Data for Name: groupe_info; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY groupe_info (groupe_id, organism_id, libelle, acces_type) FROM stdin;
\.


--
-- Data for Name: homologies; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY homologies (q_organism_id, q_element_id, q_gene_id, q_length, s_organism_id, s_element_id, s_gene_id, s_length, identity, score, e_value, rank, q_first, q_first_frac, q_last, q_last_frac, q_align_length, q_align_frac, s_first, s_first_frac, s_last, s_last_frac, s_align_length, s_align_frac, number_fragment_hit) FROM stdin;
\.


--
-- Data for Name: isbranchedtoanothersynteny; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY isbranchedtoanothersynteny (alignment_id_branched, alignment_param_id, branche_on_q_gene_id, branche_on_s_gene_id) FROM stdin;
\.


--
-- Name: num_code_loc; Type: SEQUENCE SET; Schema: public; Owner: origami_admin
--

SELECT pg_catalog.setval('num_code_loc', 1, false);


--
-- Data for Name: organisms; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY organisms (organism_id, species, strain, substrain, taxon_id, ispublic, ncbi_internal_id_it, ncbi_assemblyaccession_it, ncbi_assemblyname_it, ncbi_lastupdatedate_it, ncbi_seqreleasedate_it, ncbi_isolate_it, ncbi_speciestaxid_it, ncbi_biosampleid_it, ncbi_biosampleaccn_it, ncbi_list_bioprojectid_it, ncbi_list_bioprojectaccn_it, ncbi_assemblyclass_it, ncbi_assemblystatus_it, alt_strain, serotype, computation_in_process) FROM stdin;
\.


--
-- Name: pair_id_seq; Type: SEQUENCE SET; Schema: public; Owner: origami_admin
--

SELECT pg_catalog.setval('pair_id_seq', 1, false);


--
-- Data for Name: params_scores_algo_syntenies; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY params_scores_algo_syntenies (ortho_score, homo_score, mismatch_penalty, gap_creation_penalty, gap_extension_penalty, min_align_size, min_score, orthologs_included, ortho_min_prot_frac, genefusions_minpctlenghtprotthres, closebestmatchs_minpctthres, tandemdups_minpctwithinbestpidthres, max_gap_size_for_creation_penalty) FROM stdin;
\.


--
-- Data for Name: prot_fusion; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY prot_fusion (prot_fusion_id, q_organims_id, q_element_id, q_gene_id, s_organims_id, s_element_id, s_gene_id, ismirrordata, s_rank_among_all_s_fusions, lengthtotalcoveragesonq, lenghtnewcontribcoveragesonq, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, geneidbestsinglelongmatchhidingprotfusion, numbersinglelongmatchhidingprotfusion, numberprotfusiondetectedbeforethisone) FROM stdin;
\.


--
-- Data for Name: q_element_id_2_sorted_list_comp_orga_whole; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY q_element_id_2_sorted_list_comp_orga_whole (element_id, abundance_homologs, synteny_score, alignemnt_score, abundance_homo_annotations) FROM stdin;
\.


--
-- Name: seq_group_id; Type: SEQUENCE SET; Schema: public; Owner: origami_admin
--

SELECT pg_catalog.setval('seq_group_id', 1, false);


--
-- Name: seq_user_id; Type: SEQUENCE SET; Schema: public; Owner: origami_admin
--

SELECT pg_catalog.setval('seq_user_id', 1, false);


--
-- Data for Name: suivi_maj; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY suivi_maj (date_maj, accession, type_maj) FROM stdin;
\.


--
-- Data for Name: tandem_dups; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY tandem_dups (tandem_dups_id, alignment_param_id, gene_id_single_is_q, single_organims_id, single_element_id, single_gene_id, tandem_organims_id, tandem_element_id, tandem_gene_id, pid, hsp_len, mismatches, gaps, qstart, qend, qlength, hstart, hend, hlength, evalue, bits, bdbh, rank, idx_in_dup) FROM stdin;
\.


--
-- Data for Name: user_groupe; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY user_groupe (groupe_id, user_id) FROM stdin;
\.


--
-- Data for Name: user_info; Type: TABLE DATA; Schema: public; Owner: origami_admin
--

COPY user_info (user_id, labo, nom, prenom, session_id, login, password) FROM stdin;
\.


SET search_path = micado, pg_catalog;

--
-- Name: ncbi_taxonomy_tree ncbi_taxonomy_tree_pkey; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY ncbi_taxonomy_tree
    ADD CONSTRAINT ncbi_taxonomy_tree_pkey PRIMARY KEY (ncbi_taxid);


--
-- Name: accessions pk_accessions; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY accessions
    ADD CONSTRAINT pk_accessions PRIMARY KEY (accession, old_accession);


--
-- Name: articles pk_articles; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY articles
    ADD CONSTRAINT pk_articles PRIMARY KEY (accession, num_art);


--
-- Name: comments pk_comments; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT pk_comments PRIMARY KEY (accession);


--
-- Name: dna_seq pk_dna_seq; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY dna_seq
    ADD CONSTRAINT pk_dna_seq PRIMARY KEY (accession);


--
-- Name: dna_loc pk_dnaloc; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY dna_loc
    ADD CONSTRAINT pk_dnaloc PRIMARY KEY (accession, code_feat);


--
-- Name: features pk_features; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY features
    ADD CONSTRAINT pk_features PRIMARY KEY (accession, code_feat);


--
-- Name: keywords pk_keywords; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY keywords
    ADD CONSTRAINT pk_keywords PRIMARY KEY (accession);


--
-- Name: locations pk_locations; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT pk_locations PRIMARY KEY (accession, code_feat, code_loc);


--
-- Name: prot_feat pk_protfeat; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY prot_feat
    ADD CONSTRAINT pk_protfeat PRIMARY KEY (accession, code_feat);


--
-- Name: qualifiers pk_qualifiers; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY qualifiers
    ADD CONSTRAINT pk_qualifiers PRIMARY KEY (accession, code_feat, code_qual);


--
-- Name: sequences pk_sequences; Type: CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY sequences
    ADD CONSTRAINT pk_sequences PRIMARY KEY (accession);


SET search_path = public, pg_catalog;

--
-- Name: alignment_params alignment_params_pkey; Type: CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY alignment_params
    ADD CONSTRAINT alignment_params_pkey PRIMARY KEY (alignment_param_id);


--
-- Name: alignments alignments_pkey; Type: CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY alignments
    ADD CONSTRAINT alignments_pkey PRIMARY KEY (alignment_id);


--
-- Name: close_best_match close_best_match_pkey; Type: CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY close_best_match
    ADD CONSTRAINT close_best_match_pkey PRIMARY KEY (close_best_match_id);


--
-- Name: elements elements_pkey; Type: CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY elements
    ADD CONSTRAINT elements_pkey PRIMARY KEY (element_id);


--
-- Name: genes genes_pkey; Type: CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY genes
    ADD CONSTRAINT genes_pkey PRIMARY KEY (gene_id);


--
-- Name: groupe_info groupe_info_pkey; Type: CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY groupe_info
    ADD CONSTRAINT groupe_info_pkey PRIMARY KEY (groupe_id);


--
-- Name: organisms organisms_pkey; Type: CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY organisms
    ADD CONSTRAINT organisms_pkey PRIMARY KEY (organism_id);


--
-- Name: q_element_id_2_sorted_list_comp_orga_whole q_element_id_2_sorted_list_comp_orga_whole_pkey; Type: CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY q_element_id_2_sorted_list_comp_orga_whole
    ADD CONSTRAINT q_element_id_2_sorted_list_comp_orga_whole_pkey PRIMARY KEY (element_id);


--
-- Name: user_info user_info_pkey; Type: CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY user_info
    ADD CONSTRAINT user_info_pkey PRIMARY KEY (user_id);


--
-- Name: alignment_pairs_alignment_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX alignment_pairs_alignment_id ON alignment_pairs USING btree (alignment_id);


--
-- Name: alignment_pairs_q_gene_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX alignment_pairs_q_gene_id ON alignment_pairs USING btree (q_gene_id);


--
-- Name: alignment_pairs_s_gene_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX alignment_pairs_s_gene_id ON alignment_pairs USING btree (s_gene_id);


--
-- Name: alignment_params_q_element_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX alignment_params_q_element_id ON alignment_params USING btree (q_element_id);


--
-- Name: alignment_params_q_organism_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX alignment_params_q_organism_id ON alignment_params USING btree (q_organism_id);


--
-- Name: alignment_params_s_organism_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX alignment_params_s_organism_id ON alignment_params USING btree (s_organism_id);


--
-- Name: alignments_alignment_param_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX alignments_alignment_param_id ON alignments USING btree (alignment_param_id);


--
-- Name: genes_element_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX genes_element_id ON genes USING btree (element_id);


--
-- Name: genes_names; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX genes_names ON genes USING btree (name);


--
-- Name: genes_organism_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX genes_organism_id ON genes USING btree (organism_id);


--
-- Name: genes_start; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX genes_start ON genes USING btree (start);


--
-- Name: homologies_e_value_q_first_frac_s_align_frac; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX homologies_e_value_q_first_frac_s_align_frac ON homologies USING btree (e_value, q_first_frac, s_align_frac);


--
-- Name: homologies_q_gene_id_s_element_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX homologies_q_gene_id_s_element_id ON homologies USING btree (q_gene_id, s_element_id);


--
-- Name: homologies_q_organism_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX homologies_q_organism_id ON homologies USING btree (q_organism_id);


--
-- Name: homologies_rank; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX homologies_rank ON homologies USING btree (rank);


--
-- Name: homologies_s_gene_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX homologies_s_gene_id ON homologies USING btree (s_gene_id);


--
-- Name: homologies_s_organism_id; Type: INDEX; Schema: public; Owner: origami_admin
--

CREATE INDEX homologies_s_organism_id ON homologies USING btree (s_organism_id);


SET search_path = micado, pg_catalog;

--
-- Name: accessions fk_accessions; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY accessions
    ADD CONSTRAINT fk_accessions FOREIGN KEY (accession) REFERENCES sequences(accession);


--
-- Name: articles fk_articles; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY articles
    ADD CONSTRAINT fk_articles FOREIGN KEY (accession) REFERENCES sequences(accession);


--
-- Name: comments fk_comments; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT fk_comments FOREIGN KEY (accession) REFERENCES sequences(accession);


--
-- Name: dna_seq fk_dna_seq; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY dna_seq
    ADD CONSTRAINT fk_dna_seq FOREIGN KEY (accession) REFERENCES sequences(accession);


--
-- Name: dna_loc fk_dnaloc; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY dna_loc
    ADD CONSTRAINT fk_dnaloc FOREIGN KEY (accession, code_feat) REFERENCES features(accession, code_feat);


--
-- Name: features fk_features; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY features
    ADD CONSTRAINT fk_features FOREIGN KEY (accession) REFERENCES sequences(accession);


--
-- Name: keywords fk_keywords; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY keywords
    ADD CONSTRAINT fk_keywords FOREIGN KEY (accession) REFERENCES sequences(accession);


--
-- Name: locations fk_locations; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT fk_locations FOREIGN KEY (accession, code_feat) REFERENCES features(accession, code_feat);


--
-- Name: prot_feat fk_protfeat; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY prot_feat
    ADD CONSTRAINT fk_protfeat FOREIGN KEY (accession, code_feat) REFERENCES features(accession, code_feat);


--
-- Name: qualifiers fk_qualifiers; Type: FK CONSTRAINT; Schema: micado; Owner: origami_admin
--

ALTER TABLE ONLY qualifiers
    ADD CONSTRAINT fk_qualifiers FOREIGN KEY (accession, code_feat) REFERENCES features(accession, code_feat);


SET search_path = public, pg_catalog;

--
-- Name: elements accession_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY elements
    ADD CONSTRAINT accession_fk FOREIGN KEY (accession) REFERENCES micado.sequences(accession) MATCH FULL;


--
-- Name: isbranchedtoanothersynteny alignment_id_branched_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY isbranchedtoanothersynteny
    ADD CONSTRAINT alignment_id_branched_fk FOREIGN KEY (alignment_id_branched) REFERENCES alignments(alignment_id) MATCH FULL;


--
-- Name: tandem_dups alignment_param_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY tandem_dups
    ADD CONSTRAINT alignment_param_id_fk FOREIGN KEY (alignment_param_id) REFERENCES alignment_params(alignment_param_id) MATCH FULL;


--
-- Name: isbranchedtoanothersynteny alignment_param_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY isbranchedtoanothersynteny
    ADD CONSTRAINT alignment_param_id_fk FOREIGN KEY (alignment_param_id) REFERENCES alignment_params(alignment_param_id) MATCH FULL;


--
-- Name: genes element_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY genes
    ADD CONSTRAINT element_id_fk FOREIGN KEY (element_id) REFERENCES elements(element_id) MATCH FULL;


--
-- Name: q_element_id_2_sorted_list_comp_orga_whole element_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY q_element_id_2_sorted_list_comp_orga_whole
    ADD CONSTRAINT element_id_fk FOREIGN KEY (element_id) REFERENCES elements(element_id) MATCH FULL;


--
-- Name: genes feature_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY genes
    ADD CONSTRAINT feature_id_fk FOREIGN KEY (accession, feature_id) REFERENCES micado.features(accession, code_feat) MATCH FULL;


--
-- Name: user_groupe fk_groupe; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY user_groupe
    ADD CONSTRAINT fk_groupe FOREIGN KEY (groupe_id) REFERENCES groupe_info(groupe_id);


--
-- Name: groupe_info fk_organisms; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY groupe_info
    ADD CONSTRAINT fk_organisms FOREIGN KEY (organism_id) REFERENCES organisms(organism_id);


--
-- Name: user_groupe fk_user; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY user_groupe
    ADD CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES user_info(user_id);


--
-- Name: elements organism_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY elements
    ADD CONSTRAINT organism_id_fk FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) MATCH FULL;


--
-- Name: genes organism_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY genes
    ADD CONSTRAINT organism_id_fk FOREIGN KEY (organism_id) REFERENCES organisms(organism_id) MATCH FULL;


--
-- Name: prot_fusion q_element_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY prot_fusion
    ADD CONSTRAINT q_element_id_fk FOREIGN KEY (q_element_id) REFERENCES elements(element_id) MATCH FULL;


--
-- Name: close_best_match q_element_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY close_best_match
    ADD CONSTRAINT q_element_id_fk FOREIGN KEY (q_element_id) REFERENCES elements(element_id) MATCH FULL;


--
-- Name: prot_fusion q_gene_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY prot_fusion
    ADD CONSTRAINT q_gene_id_fk FOREIGN KEY (q_gene_id) REFERENCES genes(gene_id) MATCH FULL;


--
-- Name: close_best_match q_gene_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY close_best_match
    ADD CONSTRAINT q_gene_id_fk FOREIGN KEY (q_gene_id) REFERENCES genes(gene_id) MATCH FULL;


--
-- Name: prot_fusion q_organims_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY prot_fusion
    ADD CONSTRAINT q_organims_id_fk FOREIGN KEY (q_organims_id) REFERENCES organisms(organism_id) MATCH FULL;


--
-- Name: close_best_match q_organims_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY close_best_match
    ADD CONSTRAINT q_organims_id_fk FOREIGN KEY (q_organims_id) REFERENCES organisms(organism_id) MATCH FULL;


--
-- Name: prot_fusion s_element_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY prot_fusion
    ADD CONSTRAINT s_element_id_fk FOREIGN KEY (s_element_id) REFERENCES elements(element_id) MATCH FULL;


--
-- Name: prot_fusion s_gene_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY prot_fusion
    ADD CONSTRAINT s_gene_id_fk FOREIGN KEY (s_gene_id) REFERENCES genes(gene_id) MATCH FULL;


--
-- Name: prot_fusion s_organims_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY prot_fusion
    ADD CONSTRAINT s_organims_id_fk FOREIGN KEY (s_organims_id) REFERENCES organisms(organism_id) MATCH FULL;


--
-- Name: close_best_match s_organims_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY close_best_match
    ADD CONSTRAINT s_organims_id_fk FOREIGN KEY (s_organims_id) REFERENCES organisms(organism_id) MATCH FULL;


--
-- Name: tandem_dups single_element_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY tandem_dups
    ADD CONSTRAINT single_element_id_fk FOREIGN KEY (single_element_id) REFERENCES elements(element_id) MATCH FULL;


--
-- Name: tandem_dups single_gene_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY tandem_dups
    ADD CONSTRAINT single_gene_id_fk FOREIGN KEY (single_gene_id) REFERENCES genes(gene_id) MATCH FULL;


--
-- Name: tandem_dups single_organims_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY tandem_dups
    ADD CONSTRAINT single_organims_id_fk FOREIGN KEY (single_organims_id) REFERENCES organisms(organism_id) MATCH FULL;


--
-- Name: tandem_dups tandem_element_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY tandem_dups
    ADD CONSTRAINT tandem_element_id_fk FOREIGN KEY (tandem_element_id) REFERENCES elements(element_id) MATCH FULL;


--
-- Name: tandem_dups tandem_gene_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY tandem_dups
    ADD CONSTRAINT tandem_gene_id_fk FOREIGN KEY (tandem_gene_id) REFERENCES genes(gene_id) MATCH FULL;


--
-- Name: tandem_dups tandem_organims_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: origami_admin
--

ALTER TABLE ONLY tandem_dups
    ADD CONSTRAINT tandem_organims_id_fk FOREIGN KEY (tandem_organims_id) REFERENCES organisms(organism_id) MATCH FULL;


--
-- Name: micado; Type: ACL; Schema: -; Owner: origami_admin
--

REVOKE ALL ON SCHEMA micado FROM origami_admin;
GRANT ALL ON SCHEMA micado TO origami_admin WITH GRANT OPTION;
GRANT USAGE ON SCHEMA micado TO origami_read;


--
-- Name: public; Type: ACL; Schema: -; Owner: origami_admin
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;
GRANT USAGE ON SCHEMA public TO origami_read;
GRANT ALL ON SCHEMA public TO origami_admin WITH GRANT OPTION;


SET search_path = micado, pg_catalog;

--
-- Name: accessions; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE accessions TO origami_read;


--
-- Name: articles; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE articles TO origami_read;


--
-- Name: comments; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE comments TO origami_read;


--
-- Name: dna_loc; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE dna_loc TO origami_read;


--
-- Name: dna_seq; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE dna_seq TO origami_read;


--
-- Name: features; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE features TO origami_read;


--
-- Name: keywords; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE keywords TO origami_read;


--
-- Name: locations; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE locations TO origami_read;


--
-- Name: ncbi_taxonomy_tree; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT,UPDATE ON TABLE ncbi_taxonomy_tree TO origami_read;


--
-- Name: prot_feat; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE prot_feat TO origami_read;


--
-- Name: qualifiers; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE qualifiers TO origami_read;


--
-- Name: sequences; Type: ACL; Schema: micado; Owner: origami_admin
--

GRANT SELECT ON TABLE sequences TO origami_read;


SET search_path = public, pg_catalog;

--
-- Name: alignment_id_seq; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON SEQUENCE alignment_id_seq TO origami_read;


--
-- Name: alignment_pairs; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON TABLE alignment_pairs TO origami_read;


--
-- Name: alignment_param_id_seq; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON SEQUENCE alignment_param_id_seq TO origami_read;


--
-- Name: alignment_params; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON TABLE alignment_params TO origami_read;


--
-- Name: alignments; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON TABLE alignments TO origami_read;


--
-- Name: close_best_match; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,UPDATE ON TABLE close_best_match TO origami_read;


--
-- Name: element_seq; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON SEQUENCE element_seq TO origami_read;


--
-- Name: elements; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON TABLE elements TO origami_read;


--
-- Name: genes; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON TABLE genes TO origami_read;


--
-- Name: seq_group_id; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,UPDATE ON SEQUENCE seq_group_id TO origami_read;


--
-- Name: groupe_info; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,INSERT,UPDATE ON TABLE groupe_info TO origami_read;


--
-- Name: homologies; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON TABLE homologies TO origami_read;


--
-- Name: isbranchedtoanothersynteny; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,UPDATE ON TABLE isbranchedtoanothersynteny TO origami_read;


--
-- Name: num_code_loc; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON SEQUENCE num_code_loc TO origami_read;


--
-- Name: organisms; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON TABLE organisms TO origami_read;


--
-- Name: pair_id_seq; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON SEQUENCE pair_id_seq TO origami_read;


--
-- Name: params_scores_algo_syntenies; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,UPDATE ON TABLE params_scores_algo_syntenies TO origami_read;


--
-- Name: prot_fusion; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,UPDATE ON TABLE prot_fusion TO origami_read;


--
-- Name: q_element_id_2_sorted_list_comp_orga_whole; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,UPDATE ON TABLE q_element_id_2_sorted_list_comp_orga_whole TO origami_read;


--
-- Name: seq_user_id; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,UPDATE ON SEQUENCE seq_user_id TO origami_read;


--
-- Name: suivi_maj; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT ON TABLE suivi_maj TO origami_read;


--
-- Name: tandem_dups; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,UPDATE ON TABLE tandem_dups TO origami_read;


--
-- Name: user_groupe; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,INSERT,UPDATE ON TABLE user_groupe TO origami_read;


--
-- Name: user_info; Type: ACL; Schema: public; Owner: origami_admin
--

GRANT SELECT,INSERT,UPDATE ON TABLE user_info TO origami_read;


--
-- PostgreSQL database dump complete
--

