SET client_min_messages TO WARNING; -- do not print NOTICE messages

VACUUM FULL ANALYSE micado.accessions;
VACUUM FULL ANALYSE micado.articles;
VACUUM FULL ANALYSE micado.comments;
VACUUM FULL ANALYSE micado.dna_loc;
VACUUM FULL ANALYSE micado.dna_seq;
VACUUM FULL ANALYSE micado.features;
VACUUM FULL ANALYSE micado.keywords;
VACUUM FULL ANALYSE micado.locations;
VACUUM FULL ANALYSE micado.ncbi_taxonomy_tree;
VACUUM FULL ANALYSE micado.prot_feat;
VACUUM FULL ANALYSE micado.qualifiers;
VACUUM FULL ANALYSE micado.sequences;
VACUUM FULL ANALYSE alignment_pairs;
VACUUM FULL ANALYSE alignment_params;
VACUUM FULL ANALYSE alignments;
VACUUM FULL ANALYSE close_best_match;
VACUUM FULL ANALYSE elements;
VACUUM FULL ANALYSE genes;
VACUUM FULL ANALYSE groupe_info;
VACUUM FULL ANALYSE homologies;
VACUUM FULL ANALYSE isbranchedtoanothersynteny;
VACUUM FULL ANALYSE organisms;
VACUUM FULL ANALYSE params_scores_algo_syntenies;
VACUUM FULL ANALYSE prot_fusion;
VACUUM FULL ANALYSE q_element_id_2_sorted_list_comp_orga_whole;
VACUUM FULL ANALYSE suivi_maj;
VACUUM FULL ANALYSE tandem_dups;
VACUUM FULL ANALYSE user_groupe;
VACUUM FULL ANALYSE user_info;


