import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;



public class RemoveDoublonTableAlignments {

	private static String DB_URL = "jdbc:postgresql://";
	private static String DB_USERNAME = "";
	private static String DB_PSSD = "";
	public static final String DRIVER = "org.postgresql.Driver";
	
	/**
	 * @param args
	 * -db_url [db url]
	 * -db_user [db user]
	 * -db_pssd [db pssd]
	 * @throws Exception 
	 * 
	 */
	public static void main(String[] args) throws Exception {
		
		long milli = System.currentTimeMillis();
		
		for(int i=0;i<args.length;i++){
			
			if(args[i].compareTo("-db_url")==0){
				i++;
				DB_URL += args[i];
			}else if(args[i].compareTo("-db_user")==0){
				i++;
				DB_USERNAME = args[i];
			}else if(args[i].compareTo("-db_pssd")==0){
				i++;
				DB_PSSD = args[i];
			}
			
		}

		
		Connection conn = null;


		//SELECT alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_first_gene_position, q_last_gene_position, q_size_kb, s_first_gene_position, s_last_gene_position, s_size_kb, count(alignment_id)
		// FROM alignments
		// GROUP BY alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_first_gene_position, q_last_gene_position, q_size_kb, s_first_gene_position, s_last_gene_position, s_size_kb
		// HAVING count(alignment_id) > 1
		
		//System.out.println("command :"+command);
		
		Statement statement = null;
		ResultSet rs = null;
		
		ArrayList<AlignmentObj> listAO = new ArrayList<AlignmentObj>();

		try {

			String myDateString = DateFormat.getDateTimeInstance().format(new Date()).replaceAll("[\\s,:]", "_");
			FileWriter fstream = new FileWriter("log_RemoveDoublonTableAlignments_"+myDateString+".txt");
			BufferedWriter out = new BufferedWriter(fstream);

			System.out.println("\nStarting RemoveDoublonTableAlignments with args :\n\tdb url="+DB_URL+"\n\tusername="+DB_USERNAME+"\n\tdb pssd="+DB_PSSD+"\n");
			out.write("\nStarting RemoveDoublonTableAlignments with args :\n\tdb url="+DB_URL+"\n\tusername="+DB_USERNAME+"\n\tdb pssd="+DB_PSSD+"\n");

			
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PSSD);

			

			statement = conn.createStatement();
			
			
			//count tuples in alignments before starting script
			//long milli1 = System.currentTimeMillis();
			String commandCount = "select count(*) from alignments";
			rs = null;
			rs = statement.executeQuery(commandCount);
			if(rs.next()){
				System.out.println("\nselect count(*) from alignments before script start : "+rs.getInt("count")+"\n");
				out.write("\nselect count(*) from alignments before script start : "+rs.getInt("count")+"\n");
			}
			
			//count tuples in alignment_pairs before starting script
			String commandCountAlignment_pairs = "select count(*) from alignment_pairs";
			rs = null;
			rs = statement.executeQuery(commandCountAlignment_pairs);
			if(rs.next()){
				System.out.println("\nselect count(*) from alignment_pairs before script start : "+rs.getInt("count")+"\n");
				out.write("\nselect count(*) from alignment_pairs before script start : "+rs.getInt("count")+"\n");
			}
			//System.out.println("Finished count tuples before starting script in " + (System.currentTimeMillis() - milli1) + " milliseconds");
			//out.write("Finished count tuples before starting script in " + (System.currentTimeMillis() - milli1) + " milliseconds");
			

			
			//1st step : get the duplicate in table alignments
			System.out.println("\nStep 1 : get the duplicate in table alignments");
			out.write("\nStep 1 : get the duplicate in table alignments");
			//long milli2 = System.currentTimeMillis();
			
			String command = "SELECT alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_first_gene_position, q_last_gene_position, q_size_kb, s_first_gene_position, s_last_gene_position, s_size_kb, count(alignment_id)"
				+ " FROM alignments"
				+ " GROUP BY alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_first_gene_position, q_last_gene_position, q_size_kb, s_first_gene_position, s_last_gene_position, s_size_kb"
				+ " HAVING count(alignment_id) > 1";
			rs = null;
			rs = statement.executeQuery(command);
			
			while (rs.next()) {
				
				AlignmentObj ao = new AlignmentObj();
				//ao.setAlignmentId(alignmentId)
				ao.setAlignmentParamId(rs.getInt("alignment_param_id"));
				ao.setGaps(rs.getInt("gaps"));
				ao.setHomologs(rs.getInt("homologs"));
				ao.setMismatches(rs.getInt("mismatches"));
				ao.setOrientationConserved(rs.getBoolean("orientation_conserved"));
				ao.setOrthologs(rs.getInt("orthologs"));
				ao.setPairs(rs.getInt("pairs"));
				ao.setqFirstGenePosition(rs.getInt("q_first_gene_position"));
				ao.setqLastGenePosition(rs.getInt("q_last_gene_position"));
				ao.setqSizeKb(rs.getDouble("q_size_kb"));
				ao.setScore(rs.getDouble("score"));
				ao.setsFirstGenePosition(rs.getInt("s_first_gene_position"));
				ao.setsLastGenePosition(rs.getInt("s_last_gene_position"));
				ao.setsSizeKb(rs.getDouble("s_size_kb"));
				ao.setTotalGapSize(rs.getInt("total_gap_size"));
				listAO.add(ao);
				
			}

			//System.out.println("\nFinished 1st step : get the duplicate in table alignments in " + (System.currentTimeMillis() - milli2) + " milliseconds\n"); //TEST
			//out.write("\nFinished 1st step : get the duplicate in table alignments in " + (System.currentTimeMillis() - milli2) + " milliseconds\n"); //TEST

			System.out.println("\nStep 1 : Detected "+listAO.size()+" duplicated rows in table Alignments\n");
			out.write("Step 1 : Detected "+listAO.size()+" duplicated rows in table Alignments\n");
			
			//2nd step : for each duplicate get the differents alignment_id and delete all but the 1 one in table alignments and alignment_pairs
			System.out.println("\nStep 2 : for each duplicate get the differents alignment_id and delete all but the 1 one in table alignments and alignment_pairs");
			out.write("\nStep 2 : for each duplicate get the differents alignment_id and delete all but the 1 one in table alignments and alignment_pairs");
			
			for(int i=0; i<listAO.size();i++){
				AlignmentObj ao = listAO.get(i);
				
				//long milli3 = System.currentTimeMillis();
				
				String command2 = "SELECT alignment_id" +
						" FROM alignments" +
						" WHERE alignment_param_id = "+ao.getAlignmentParamId()+" AND score = "+ao.getScore()+" AND pairs = "+ao.getPairs()+" AND orientation_conserved = "+ao.isOrientationConserved()+" AND orthologs = "+ao.getOrthologs()+" AND homologs = "+ao.getHomologs()+" AND mismatches = "+ao.getMismatches()+" AND gaps =" +ao.getGaps()+
						" AND total_gap_size = "+ao.getTotalGapSize()+" AND q_first_gene_position = "+ao.getqFirstGenePosition()+" AND q_last_gene_position = "+ao.getqLastGenePosition()+" AND q_size_kb = "+ao.getqSizeKb()+" AND s_first_gene_position = "+ao.getsFirstGenePosition()+" AND s_last_gene_position = "+ao.getsLastGenePosition()+" AND s_size_kb = "+ao.getsSizeKb();
				rs = null;
				rs = statement.executeQuery(command2);
				
				//System.out.println("Finished step get alignment id of 1 delete iteration in " + (System.currentTimeMillis() - milli3) + " milliseconds"); //TEST
				//out.write("Finished step get alignment id of 1 delete iteration in " + (System.currentTimeMillis() - milli3) + " milliseconds"); //TEST
				
				ArrayList<Integer> listAlignmentIdDuplicated = new ArrayList<Integer>();
				while (rs.next()) {
					listAlignmentIdDuplicated.add(rs.getInt("alignment_id"));
				}
				
				for(int i1=0;i1<listAlignmentIdDuplicated.size();i1++){
					if(i1==0){
						//do noting, keep this AlignmentId
						System.out.println("keeping alignment_id "+listAlignmentIdDuplicated.get(i1)+"\n");
						out.write("keeping alignment_id "+listAlignmentIdDuplicated.get(i1)+"\n");
					}else{
						int AlignmentIdToRemove = listAlignmentIdDuplicated.get(i1);
						


						//long milli4 = System.currentTimeMillis();
						
						
						String command3 = "DELETE FROM alignment_pairs WHERE alignment_id = "+AlignmentIdToRemove;
						String command4 = "DELETE FROM alignments WHERE alignment_id = "+AlignmentIdToRemove;
						int rowAlignmentPairsDeleted = statement.executeUpdate(command3);
						int rowAlignmentsDeleted = statement.executeUpdate(command4);
						System.out.println("Removed alignment_id "+AlignmentIdToRemove+" successfully from table alignment_pairs ("+rowAlignmentPairsDeleted+" rows deleted) and alignments ("+rowAlignmentsDeleted+" rows deleted)\n");
						out.write("Removed alignment_id "+AlignmentIdToRemove+" successfully from table alignment_pairs ("+rowAlignmentPairsDeleted+" rows deleted) and alignments ("+rowAlignmentsDeleted+" rows deleted)\n");
						
						/*
						statement.addBatch("DELETE FROM alignment_pairs WHERE alignment_id = "+AlignmentIdToRemove);
						statement.addBatch("DELETE FROM alignments WHERE alignment_id = "+AlignmentIdToRemove);
						int[] arraysCountDeletion = statement.executeBatch();
						System.out.println("Removed alignment_id "+AlignmentIdToRemove+" successfully from table alignment_pairs ("+arraysCountDeletion[0]+" rows deleted) and alignments ("+arraysCountDeletion[1]+" rows deleted)\n");
						out.write("Removed alignment_id "+AlignmentIdToRemove+" successfully from table alignment_pairs ("+arraysCountDeletion[0]+" rows deleted) and alignments ("+arraysCountDeletion[1]+" rows deleted)\n");
						statement.clearBatch();
						*/

						//System.out.println("Finished step delete alignment id of 1 delete iteration in " + (System.currentTimeMillis() - milli4) + " milliseconds"); //TEST
						//out.write("Finished step delete alignment id of 1 delete iteration in " + (System.currentTimeMillis() - milli4) + " milliseconds"); //TEST
					
						
					}
				}
				
				out.flush();
				
			}
			

			//count tuples in alignments after script
			String commandCountAfter = "select count(*) from alignments";
			rs = null;
			rs = statement.executeQuery(commandCountAfter);
			if(rs.next()){
				System.out.println("\nselect count(*) from alignments after script finished : "+rs.getInt("count")+"\n");
				out.write("\nselect count(*) from alignments after script finished : "+rs.getInt("count")+"\n");
			}
			
			//count tuples in alignment_pairs after script
			String commandCountAlignmentPairsAfter = "select count(*) from alignment_pairs";
			rs = null;
			rs = statement.executeQuery(commandCountAlignmentPairsAfter);
			if(rs.next()){
				System.out.println("\nselect count(*) from alignment_pairs after script finished : "+rs.getInt("count")+"\n");
				out.write("\nselect count(*) from alignment_pairs after script finished : "+rs.getInt("count")+"\n");
			}
			
			//finished!
			System.out.println("Finished RemoveDoublonTableAlignments in " + (System.currentTimeMillis() - milli) + " milliseconds");
			out.write("Finished RemoveDoublonTableAlignments in " + (System.currentTimeMillis() - milli) + " milliseconds");
			
			 //Close the output stream
			 out.close();
			 
		} catch (Exception ex) {
			// probleme
			System.err.println("problem in RemoveDoublonTableAlignments");
			System.err.println(ex + "\n" + ex.getMessage());
			ex.printStackTrace();
			throw new Exception(ex + "\n" + ex.getMessage());

		} finally {
			
			try {
				rs.close();
			} catch (Exception ex) {
				System.err.println("problem in RemoveDoublonTableAlignments rs.close"+ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
			} catch (Exception ex) {
				System.err.println("problem in RemoveDoublonTableAlignments statement.close"+ex + "\n" + ex.getMessage());
			}
			try {
				conn.close();
			} catch (Exception ex) {
				System.err.println("problem in RemoveDoublonTableAlignments conn.close"+ex + "\n" + ex.getMessage());
			}
		}//try
		
		
		
	}

}

