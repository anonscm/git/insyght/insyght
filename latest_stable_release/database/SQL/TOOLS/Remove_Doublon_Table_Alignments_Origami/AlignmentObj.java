public class AlignmentObj {

	private int alignmentId;
	private int alignmentParamId;
	private double score;
	private int pairs;
	private boolean orientationConserved;
	private int orthologs;
	private int homologs;
	private int mismatches;
	private int gaps;
	private int totalGapSize;
	private int qFirstGenePosition;
	private int qLastGenePosition;
	private double qSizeKb;
	private int sFirstGenePosition;
	private int sLastGenePosition;
	private double sSizeKb;
	
	
	
	public AlignmentObj() {
		super();
	}
	
	public int getAlignmentId() {
		return alignmentId;
	}
	public void setAlignmentId(int alignmentId) {
		this.alignmentId = alignmentId;
	}
	public int getAlignmentParamId() {
		return alignmentParamId;
	}
	public void setAlignmentParamId(int alignmentParamId) {
		this.alignmentParamId = alignmentParamId;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public int getPairs() {
		return pairs;
	}
	public void setPairs(int pairs) {
		this.pairs = pairs;
	}
	public boolean isOrientationConserved() {
		return orientationConserved;
	}
	public void setOrientationConserved(boolean orientationConserved) {
		this.orientationConserved = orientationConserved;
	}
	public int getOrthologs() {
		return orthologs;
	}
	public void setOrthologs(int orthologs) {
		this.orthologs = orthologs;
	}
	public int getHomologs() {
		return homologs;
	}
	public void setHomologs(int homologs) {
		this.homologs = homologs;
	}
	public int getMismatches() {
		return mismatches;
	}
	public void setMismatches(int mismatches) {
		this.mismatches = mismatches;
	}
	public int getGaps() {
		return gaps;
	}
	public void setGaps(int gaps) {
		this.gaps = gaps;
	}
	public int getTotalGapSize() {
		return totalGapSize;
	}
	public void setTotalGapSize(int totalGapSize) {
		this.totalGapSize = totalGapSize;
	}
	public int getqFirstGenePosition() {
		return qFirstGenePosition;
	}
	public void setqFirstGenePosition(int qFirstGenePosition) {
		this.qFirstGenePosition = qFirstGenePosition;
	}
	public int getqLastGenePosition() {
		return qLastGenePosition;
	}
	public void setqLastGenePosition(int qLastGenePosition) {
		this.qLastGenePosition = qLastGenePosition;
	}
	public double getqSizeKb() {
		return qSizeKb;
	}
	public void setqSizeKb(double qSizeKb) {
		this.qSizeKb = qSizeKb;
	}
	public int getsFirstGenePosition() {
		return sFirstGenePosition;
	}
	public void setsFirstGenePosition(int sFirstGenePosition) {
		this.sFirstGenePosition = sFirstGenePosition;
	}
	public int getsLastGenePosition() {
		return sLastGenePosition;
	}
	public void setsLastGenePosition(int sLastGenePosition) {
		this.sLastGenePosition = sLastGenePosition;
	}
	public double getsSizeKb() {
		return sSizeKb;
	}
	public void setsSizeKb(double sSizeKb) {
		this.sSizeKb = sSizeKb;
	}
	
	
	
	

}
