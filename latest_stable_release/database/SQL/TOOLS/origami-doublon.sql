-- Fichier de requete pour verif cohérence de la base
-- Création Juin 2011
--
--Requete verif de doublon sur alignment_params
select q_organism_id, q_element_id, s_organism_id, s_element_id,count(alignment_param_id)  from alignment_params
group by q_organism_id, q_element_id, s_organism_id, s_element_id having count(alignment_param_id)>1 order by count(alignment_param_id) desc

--Requete test doublon sur alignments
select alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_first_gene_position, q_last_gene_position, q_size_kb, s_first_gene_position, s_last_gene_position, s_size_kb, count(alignment_id) FROM alignments GROUP BY alignment_param_id, score, pairs, orientation_conserved, orthologs, homologs, mismatches, gaps, total_gap_size, q_first_gene_position, q_last_gene_position, q_size_kb, s_first_gene_position, s_last_gene_position, s_size_kb HAVING count(alignment_id) > 1;
 
--Requete de verif pour alignments
select * from alignments where alignment_param_id = AND score = AND pairs = AND orientation_conserved = AND orthologs = AND homologs = AND mismatches = AND gaps = AND total_gap_size = AND q_first_gene_position = AND q_last_gene_position = AND q_size_kb = AND s_first_gene_position = AND s_last_gene_position = AND s_size_kb =
--**

--Requete de doublon dans la table homologies
select q_organism_id, q_element_id, q_gene_id, s_organism_id, s_element_id, s_gene_id from homologies group by q_organism_id, q_element_id, q_gene_id, s_organism_id, s_element_id, s_gene_id having count(q_length)>1;

--Requete test organisms sans elements correspondant
select organism_id from organisms where organism_id not in (select distinct organism_id from elements);


--Requete test doublon table genes
select  count(gene_id), organism_id, element_id, name, strand, start, stop, residues, feature_id FROM genes GROUP BY organism_id, element_id, name, strand, start, stop, residues, feature_id HAVING count(gene_id)>1;


-- Requete test alignment_id présent dans table alignments mias pas dans table alignments_pairs
SELECT alignment_id FROM alignments WHERE alignment_id NOT IN (select distinct alignment_id from alignment_pairs);



----------------------------------------------------------------------------
Virer les doublons de la table homologies
=========================================
create table temp_doublon_homo tablespace idx as (select q_organism_id, q_element_id, q_gene_id, s_organism_id, s_element_id, s_gene_id from homologies group by q_organism_id, q_element_id, q_gene_id, s_organism_id, s_element_id, s_gene_id having count(q_length)>1);

create table homologies2 (q_organism_id integer NOT NULL,q_element_id integer NOT NULL,
q_gene_id integer NOT NULL,
q_length integer,
s_organism_id integer NOT NULL,
s_element_id integer NOT NULL,
s_gene_id integer NOT NULL,
s_length integer,
identity double precision,
score double precision,
e_value double precision,
rank integer,
q_first integer,
q_first_frac double precision,
q_last integer,
q_last_frac double precision,
q_align_length integer,
q_align_frac double precision,
s_first integer,
s_first_frac double precision,
s_last integer,
s_last_frac double precision,
s_align_length integer,
s_align_frac double precision,
number_fragment_hit integer);



insert into homologies2 (q_organism_id, q_element_id, q_gene_id, s_organism_id, s_element_id, s_gene_id) select q_organism_id, q_element_id, q_gene_id, s_organism_id, s_element_id, s_gene_id from temp_doublon_homo;

update homologies2  h2 set (q_length, s_length, identity, score, e_value,rank,q_first,q_first_frac,q_last,q_last_frac,q_align_length,q_align_frac,s_first,s_first_frac,s_last,s_last_frac,s_align_length,s_align_frac,number_fragment_hit) = (h.q_length, h.s_length, h.identity, h.score, h.e_value,h.rank,h.q_first,h.q_first_frac,h.q_last,h.q_last_frac,h.q_align_length,h.q_align_frac,h.s_first,h.s_first_frac,h.s_last,h.s_last_frac,h.s_align_length,h.s_align_frac, h.number_fragment_hit) from homologies h where h.q_organism_id=h2.q_organism_id and h.q_element_id=h2.q_element_id and h.q_gene_id=h2.q_gene_id and h.s_organism_id=h2.s_organism_id and h.s_element_id=h2.s_element_id and h.s_gene_id=h2.s_gene_id;
 
-->virer les index sur la table homologies

delete from homologies where (q_organism_id, q_element_id, q_gene_id, s_organism_id, s_element_id, s_gene_id) in (select q_organism_id, q_element_id, q_gene_id, s_organism_id, s_element_id, s_gene_id from temp_doublon_homo );

insert into homologies select * from homologies2;

-->remettre les index sur la table homologies