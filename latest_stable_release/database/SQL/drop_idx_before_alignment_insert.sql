SET client_min_messages TO WARNING; -- do not print NOTICE messages
-- SET search_path
SET search_path TO micado, public;

-- constraint table alignment_params
ALTER TABLE alignment_params DROP CONSTRAINT IF EXISTS q_organism_id_fk CASCADE;--
ALTER TABLE alignment_params DROP CONSTRAINT IF EXISTS q_element_id_fk CASCADE;--
ALTER TABLE alignment_params DROP CONSTRAINT IF EXISTS s_organism_id_fk CASCADE;--
ALTER TABLE alignment_params DROP CONSTRAINT IF EXISTS s_element_id_fk CASCADE;--

-- constraint table alignments
ALTER TABLE alignments DROP CONSTRAINT IF EXISTS alignment_param_id_fk CASCADE;--

-- constraint table alignment_pairs
ALTER TABLE alignment_pairs DROP CONSTRAINT IF EXISTS alignment_id_fk CASCADE;--
ALTER TABLE alignment_pairs DROP CONSTRAINT IF EXISTS q_gene_id_fk CASCADE;--
ALTER TABLE alignment_pairs DROP CONSTRAINT IF EXISTS s_gene_id_fk CASCADE;--

-- constraint table homologies
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS check_lengths CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS check_q_first CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS check_q_last CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS check_s_first CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS check_s_last CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS q_organism_id_fk CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS q_element_id_fk CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS q_gene_id_fk CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS s_organism_id_fk CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS s_element_id_fk CASCADE;--
ALTER TABLE homologies DROP CONSTRAINT IF EXISTS s_gene_id_fk CASCADE;--

-- constraint tandem_dups
ALTER TABLE tandem_dups DROP CONSTRAINT IF EXISTS alignment_param_id_fk CASCADE;-- shouldn't exist any more
ALTER TABLE tandem_dups DROP CONSTRAINT IF EXISTS single_organims_id_fk CASCADE;
ALTER TABLE tandem_dups DROP CONSTRAINT IF EXISTS single_element_id_fk CASCADE;
ALTER TABLE tandem_dups DROP CONSTRAINT IF EXISTS single_gene_id_fk CASCADE;
ALTER TABLE tandem_dups DROP CONSTRAINT IF EXISTS tandem_organims_id_fk CASCADE;
ALTER TABLE tandem_dups DROP CONSTRAINT IF EXISTS tandem_element_id_fk CASCADE;
ALTER TABLE tandem_dups DROP CONSTRAINT IF EXISTS tandem_gene_id_fk CASCADE;


-- constraint isBranchedToAnotherSynteny
ALTER TABLE isBranchedToAnotherSynteny DROP CONSTRAINT IF EXISTS alignment_id_branched_fk CASCADE;
ALTER TABLE isBranchedToAnotherSynteny DROP CONSTRAINT IF EXISTS alignment_param_id_fk CASCADE;


-- constraint prot_fusion
ALTER TABLE prot_fusion DROP CONSTRAINT IF EXISTS q_organims_id_fk CASCADE;
ALTER TABLE prot_fusion DROP CONSTRAINT IF EXISTS q_element_id_fk CASCADE;
ALTER TABLE prot_fusion DROP CONSTRAINT IF EXISTS q_gene_id_fk CASCADE;
ALTER TABLE prot_fusion DROP CONSTRAINT IF EXISTS s_organims_id_fk CASCADE;
ALTER TABLE prot_fusion DROP CONSTRAINT IF EXISTS s_element_id_fk CASCADE;
ALTER TABLE prot_fusion DROP CONSTRAINT IF EXISTS s_gene_id_fk CASCADE;


-- constraint close_best_match
ALTER TABLE close_best_match DROP CONSTRAINT IF EXISTS q_organims_id_fk CASCADE;
ALTER TABLE close_best_match DROP CONSTRAINT IF EXISTS q_element_id_fk CASCADE;
ALTER TABLE close_best_match DROP CONSTRAINT IF EXISTS q_gene_id_fk CASCADE;
ALTER TABLE close_best_match DROP CONSTRAINT IF EXISTS s_organims_id_fk CASCADE;


-- index table alignment_params
ALTER TABLE alignment_params DROP CONSTRAINT IF EXISTS alignment_params_pkey CASCADE;--
DROP INDEX IF EXISTS alignment_params_q_element_id CASCADE;--
DROP INDEX IF EXISTS alignment_params_s_element_id CASCADE;--
DROP INDEX IF EXISTS alignment_params_q_organism_id CASCADE;--
DROP INDEX IF EXISTS alignment_params_s_organism_id CASCADE;--
DROP INDEX IF EXISTS alignment_params_q_element_id_s_element_id CASCADE;--

-- index table alignments
ALTER TABLE alignments DROP CONSTRAINT IF EXISTS alignments_pkey CASCADE;--
DROP INDEX IF EXISTS alignments_alignment_param_id CASCADE;--

-- index table alignment_pairs
DROP INDEX IF EXISTS alignment_pairs_alignment_id CASCADE;--
DROP INDEX IF EXISTS alignment_pairs_q_gene_id CASCADE;--
DROP INDEX IF EXISTS alignment_pairs_s_gene_id CASCADE;--

-- index table homologies
DROP INDEX IF EXISTS homologies_q_gene_id_s_element_id CASCADE;--
DROP INDEX IF EXISTS homologies_s_gene_id CASCADE;--
DROP INDEX IF EXISTS homologies_s_organism_id CASCADE;--
DROP INDEX IF EXISTS homologies_q_element_id CASCADE;--
DROP INDEX IF EXISTS homologies_q_organism_id CASCADE;--
DROP INDEX IF EXISTS homologies_s_element_id CASCADE;--
DROP INDEX IF EXISTS homologies_e_value_q_first_frac_s_align_frac CASCADE;--
DROP INDEX IF EXISTS homologies_rank CASCADE;--

-- index table tandem_dups
DROP INDEX IF EXISTS tandem_dups_tandem_dups_id CASCADE;
DROP INDEX IF EXISTS tandem_dups_alignment_param_id CASCADE;

-- index table isBranchedToAnotherSynteny
DROP INDEX IF EXISTS isBranchedToAnotherSynteny_alignment_id_branched CASCADE;
DROP INDEX IF EXISTS isBranchedToAnotherSynteny_alignment_param_id CASCADE;

-- index table prot_fusion
DROP INDEX IF EXISTS prot_fusion_prot_fusion_id CASCADE;
DROP INDEX IF EXISTS prot_fusion_q_organims_id CASCADE;
DROP INDEX IF EXISTS prot_fusion_q_gene_id CASCADE;
DROP INDEX IF EXISTS prot_fusion_s_organims_id CASCADE;
DROP INDEX IF EXISTS prot_fusion_s_gene_id CASCADE;

-- index table close_best_match
--ALTER TABLE close_best_match DROP CONSTRAINT IF EXISTS close_best_match_id_pkey CASCADE;
ALTER TABLE close_best_match DROP CONSTRAINT IF EXISTS close_best_match_q_gene_id_s_organims_id_pkey CASCADE;
ALTER TABLE close_best_match DROP CONSTRAINT IF EXISTS close_best_match_pkey CASCADE;
--DROP INDEX IF EXISTS close_best_match_q_gene_id CASCADE;
--DROP INDEX IF EXISTS close_best_match_s_organims_id CASCADE;




