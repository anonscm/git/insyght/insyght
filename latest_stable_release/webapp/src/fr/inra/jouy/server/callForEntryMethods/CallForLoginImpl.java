package fr.inra.jouy.server.callForEntryMethods;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Random;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.inra.jouy.client.RPC.CallForLogin;
import fr.inra.jouy.server.BCrypt;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.users.GroupUsersObj;
import fr.inra.jouy.shared.pojos.users.PersonsObj;

import javax.mail.*;
import javax.mail.internet.*;

public class CallForLoginImpl extends RemoteServiceServlet implements
CallForLogin {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	
	
	/* for Java 1.6 JDK et compiler compliance
	@Override
	 */
	public PersonsObj authentificationByLogin(String loginSent, String pssdSent)
	throws Exception {
		
		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForLoginImpl getOrigamiOrgaIdWithSpeciesStrainSubstrain",
				loginSent, "login", false, false);
		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForLoginImpl getOrigamiOrgaIdWithSpeciesStrainSubstrain",
				pssdSent, "password", false, false);
		
		PersonsObj personObj = new PersonsObj();

		String sessionId = "";
		Connection conn = null;

		String command = "SELECT user_info.user_id, user_info.labo, user_info.nom, user_info.prenom, user_info.password, user_groupe.groupe_id"
			+ " FROM user_info LEFT JOIN user_groupe ON user_info.user_id = user_groupe.user_id  WHERE user_info.login = '" + loginSent + "'";
		
		Statement statement = null;
		ResultSet rs = null;

		try {

			conn = DatabaseConf.getConnection_db();

			statement = conn.createStatement();
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForLoginImpl authentificationByLogin");
			
			boolean firstIter = true;
			boolean loginFound = false;
			int checkUserId = -1;

			while (rs.next()) {
				
				if(firstIter){
					
					firstIter = false;
					checkUserId = rs.getInt("user_id");

					String hashFromDB = rs.getString("password");
					boolean valid = BCrypt.checkpw(pssdSent, hashFromDB);
					if (valid) {
						
						loginFound = true;
						
						Random randomGenerator = new Random();
						for (int i = 0; i < loginSent.length(); i++) {
							sessionId += loginSent.charAt(i);
							if (i > 3) {
								break;
							}
						}
						//generate random int
						sessionId += randomGenerator.nextInt(100000);
						if (loginSent.length() > 7) {
							for (int i = 4; i < loginSent.length(); i++) {
								sessionId += loginSent.charAt(i);
								if (i > 7) {
									break;
								}
							}
						}

						sessionId += randomGenerator.nextInt(10000);
						sessionId += randomGenerator.nextInt(10000);

						String command2 = "UPDATE user_info SET session_id = '"
							+ sessionId + "' WHERE login = '" + loginSent + "'";
						Statement statement2 = conn.createStatement();
						
						statement2.executeUpdate(command2);

						personObj.setSessionId(sessionId);
						int idPersonFromDB = rs.getInt("user_id");
						personObj.setIdPerson(idPersonFromDB);
						String nomFromDB = rs.getString("nom");
						personObj.setNom(nomFromDB);
						String prenomFromDB = rs.getString("prenom");
						personObj.setPrenom(prenomFromDB);
						String affiliationFromDB = rs.getString("labo");
						personObj.setLabo(affiliationFromDB);
						personObj.setLogin(loginSent);

					}else{
						UtilitiesMethodsServer.reportException("CallForLoginImpl authentificationByLogin",new Exception("Wrong username or password"));
					}
				}else{
					if(checkUserId != rs.getInt("user_id")){
						UtilitiesMethodsServer.reportException("CallForLoginImpl authentificationByLogin",new Exception("Multiple user_id returned for login "+loginSent));
					}
				}
				
				if(loginFound){
					//get list of private project user has access to
					int groupeIdIT = rs.getInt("groupe_id");
					if(groupeIdIT > 0){
						GroupUsersObj gi = getGroupUsersObjWithGroupeId(conn, groupeIdIT);
						personObj.getListGroupsSubscribed().add(gi);
					}
				}else{
					UtilitiesMethodsServer.reportException("CallForLoginImpl authentificationByLogin",new Exception("No entry found, maybe you used a wrong username or password?"));
				}

				
			}//while
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForLoginImpl authentificationByLogin",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			DatabaseConf.freeConnection(conn, "CallForLoginImpl authentificationByLogin");
		}//try

		if (sessionId.length() > 0) {

			return personObj;
		} else {
			UtilitiesMethodsServer.reportException("CallForLoginImpl authentificationByLogin",new Exception("Wrong Username or Password"));
			return null;
		}

	}

	private GroupUsersObj getGroupUsersObjWithGroupeId(Connection conn, int groupeId) throws Exception {

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		GroupUsersObj guo = new GroupUsersObj();

		try {


			if(conn == null){
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}


			String command = "SELECT groupe_info.organism_id, groupe_info.libelle, groupe_info.acces_type, organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic, elements.element_id, elements.type, elements.accession, elements.size" +
			" FROM user_groupe, groupe_info, organisms, elements" +
			" WHERE elements.organism_id = organisms.organism_id" +
			" AND user_groupe.groupe_id = groupe_info.groupe_id" +
			" AND groupe_info.organism_id = organisms.organism_id" +
			" AND groupe_info.groupe_id = "+groupeId +"ORDER BY organisms.organism_id";

			//System.out.println("getGroupUsersObjWithGroupeId :"+command);
			
			statement = conn.createStatement();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForLoginImpl getGroupUsersObjWithGroupeId");
			
			
			int refOrgaId = -1;
			
			while (rs.next()) {

				if(rs.getBoolean("ispublic")){
					UtilitiesMethodsServer.reportException("CallForLoginImpl getGroupUsersObjWithGroupeId",new Exception("the following project is public where it should be private ; organism id = "+rs.getInt("organism_id")));
				}else{

					int organismId = rs.getInt("organism_id");
					
					if(refOrgaId == -1){
						refOrgaId = organismId;
					}else if(refOrgaId != organismId){
						UtilitiesMethodsServer.reportException("CallForLoginImpl getGroupUsersObjWithGroupeId",new Exception("There are more than 1 organism Id assigned to group id = "+groupeId));
					}
					
					guo.setGroupeId(groupeId);
					guo.setLibelle(rs.getString("libelle"));
					guo.setAccesType(rs.getString("acces_type"));
					guo.setOrganismId(organismId);
					guo.setSpecies(rs.getString("species"));
					guo.setStrain(rs.getString("strain"));
					guo.setSubstrain(rs.getString("substrain"));
					guo.setTaxonId(rs.getInt("taxon_id"));
					guo.setPublic(rs.getBoolean("ispublic"));
					LightElementItem lei = new LightElementItem();
					lei.setElementId(rs.getInt("element_id"));
					lei.setOrganismId(organismId);
					lei.setType(rs.getString("type"));
					lei.setAccession(rs.getString("accession"));
					lei.setSize(rs.getInt("size"));
					lei.setSpecies(rs.getString("species"));
					lei.setStrain(rs.getString("strain"));
					guo.getListAllLightElementItem().add(lei);
					
				}
				
			}


		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForLoginImpl getGroupUsersObjWithGroupeId",ex);

		} finally {

			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println("problem in CallForLoginImpl getGroupUsersObjWithGroupeId rs.close() "+ex + "\n" + ex.getMessage());
			}

			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println("problem in CallForLoginImpl getGroupUsersObjWithGroupeId statement.close() "+ex + "\n" + ex.getMessage());
			}


			if(closeConn){
				DatabaseConf.freeConnection(conn, "CallForLoginImpl getGroupUsersObjWithGroupeId");
			}
		}//try

		return guo;
	}

	/* for Java 1.6 JDK et compiler compliance
	@Override
	 */
	public PersonsObj checkWithServerIfSessionIdIsStillLegal(
			String session_idSent) throws Exception {

		PersonsObj personObj = new PersonsObj();

		Connection conn = null;

		String command = "SELECT user_info.user_id, user_info.login, user_info.labo, user_info.nom, user_info.prenom, user_groupe.groupe_id"
			+ " FROM user_info LEFT JOIN user_groupe ON user_info.user_id = user_groupe.user_id  WHERE user_info.session_id = '" + session_idSent + "'";
		
		Statement statement = null;
		ResultSet rs = null;

		try {

			conn = DatabaseConf.getConnection_db();
			statement = conn.createStatement();
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForLoginImpl checkWithServerIfSessionIdIsStillLegal");
			

			boolean firstIter = true;
			boolean loginFound = false;
			int checkUserId = -1;
			
			while (rs.next()) {

				if(firstIter){
					
					firstIter = false;
					checkUserId = rs.getInt("user_id");
					
					personObj.setSessionId(session_idSent);
					int idPersonFromDB = rs.getInt("user_id");
					personObj.setIdPerson(idPersonFromDB);
					String nomFromDB = rs.getString("nom");
					personObj.setNom(nomFromDB);
					String prenomFromDB = rs.getString("prenom");
					personObj.setPrenom(prenomFromDB);
					String affiliationFromDB = rs.getString("labo");
					personObj.setLabo(affiliationFromDB);
					personObj.setLogin(rs.getString("login"));
					loginFound = true;
					
				}else{
					if(checkUserId != rs.getInt("user_id")){
						UtilitiesMethodsServer.reportException("CallForLoginImpl checkWithServerIfSessionIdIsStillLegal",new Exception("Multiple user_id returned for session_id "+session_idSent));
					}
				}
				
				if(loginFound){
					//get list of private project user has access to
					int groupeIdIT = rs.getInt("groupe_id");
					if(groupeIdIT > 0){
						GroupUsersObj gi = getGroupUsersObjWithGroupeId(conn, groupeIdIT);
						personObj.getListGroupsSubscribed().add(gi);
					}
				}else{
					UtilitiesMethodsServer.reportException("CallForLoginImpl checkWithServerIfSessionIdIsStillLegal",new Exception("No entry found for session_id "+session_idSent));
				}

			}//while

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForLoginImpl checkWithServerIfSessionIdIsStillLegal",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			DatabaseConf.freeConnection(conn, "CallForLoginImpl checkWithServerIfSessionIdIsStillLegal");
		}//try

		if (personObj.getLogin().length() > 0) {
			return personObj;
		} else {
			UtilitiesMethodsServer.reportException("CallForLoginImpl checkWithServerIfSessionIdIsStillLegal", new Exception("unable to login from cookies"));
			return null;
		}

	}

	/* for Java 1.6 JDK et compiler compliance
	@Override
	 */
	public String registerNewUser(String loginSent) throws Exception {

		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForLoginImpl registerNewUser",
				loginSent, "login", false, false);
		
		Connection conn = null;
		
		String command = "SELECT user_info.user_id"
			+ " FROM user_info WHERE user_info.login = '" + loginSent + "'";
		
		//SELECT micado.user_info.user_id FROM micado.user_info WHERE micado.user_info.login = 'frdel_1@yahoo.fr';
		
		//System.out.println(command);

		Statement statement = null;
		ResultSet rs = null;

		try {
			conn = DatabaseConf.getConnection_db();

			statement = conn.createStatement();
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForLoginImpl registerNewUser");
			

			if (rs.next()) {
				UtilitiesMethodsServer.reportException("CallForLoginImpl registerNewUser",new Exception("login exists already. Please choose a different login or report this to the administrator of the web site if you think your email is being usurpated."));
			} else {
				String randomNewPssd = generateRandomNewPssd(loginSent);

				//Add new account to database example for java using jBCrypt:
				String hash = BCrypt.hashpw(randomNewPssd, BCrypt.gensalt());
				//System.out.println("hash : "+hash);
				//(create new user entry in db storing ONLY username and hash, NOT the password).
				String command2 = "INSERT INTO user_info (user_id,login,password,nom,prenom) VALUES (nextval('seq_user_id'::regclass),'"
					+ loginSent + "','" + hash + "','','')";
				
				
				statement.executeUpdate(command2);
				
				
				String recipient = loginSent;
				String subject = "[Insyght] Your account Insyght has been activated";
				String content = "Following your request at the website http://genome.jouy.inra.fr/Insyght/, your account has been activated." +
						" A randomly generated password has been generated :\n"+randomNewPssd+
						"\nYou can now log in with it and change it as your convenience in the admin tab." +
						" If you experience any problem, please contact us at insyght@jouy.inra.fr.";
				sendEmail(recipient, subject, content);


			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForLoginImpl registerNewUser",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			DatabaseConf.freeConnection(conn, "CallForLoginImpl registerNewUser");
		}//try

		return null;

	}

	public String forgotPssd(String emailAddress) throws Exception {
		
		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForLoginImpl forgotPssd",
				emailAddress, "email address", false, false);

		
		String randomNewPssd = generateRandomNewPssd(emailAddress);

		//update info in db

		Connection conn = null;
		
		String command = "SELECT user_info.user_id"
			+ " FROM user_info WHERE user_info.login = '" + emailAddress + "'";
		/*String command = "SELECT login, pssd, nom" + " FROM persons"
		+ " WHERE login = '" + emailAddress + "'";*/

		Statement statement = null;
		ResultSet rs = null;

		try {

			conn = DatabaseConf.getConnection_db();
			statement = conn.createStatement();
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForLoginImpl forgotPssd");
			

			if (rs.next()) {

				//Add new account to database example for java using jBCrypt:
				String hash = BCrypt.hashpw(randomNewPssd, BCrypt.gensalt());
				//System.out.println("hash : "+hash);
				String command2 = "UPDATE user_info SET password = '" + hash + "' WHERE login = '" + emailAddress + "'";

				statement.executeUpdate(command2);
				
				
			} else {
				UtilitiesMethodsServer.reportException("CallForLoginImpl forgotPssd",new Exception("Login does not exists. Please register a new account with this login"));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForLoginImpl forgotPssd",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			DatabaseConf.freeConnection(conn, "CallForLoginImpl forgotPssd");
		}//try

		String recipient = emailAddress;
		String subject = "[Insyght] New Random generated password";
		String content = "Following your request from the web application Insyght, a new password has been generated for your account :\n"+randomNewPssd+
				"\nYou can now log in with it and change it as your convenience in the admin tab.";
		sendEmail(recipient, subject, content);


		return null;
	}

	
	private String generateRandomNewPssd(String emailAddress) throws Exception{
		
		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForLoginImpl generateRandomNewPssd",
				emailAddress, "email address", false, false);
		
		// generate random new pssd
		String newPssd = "I2_2";
		Random randomGenerator = new Random();
		newPssd += randomGenerator.nextInt(100000);
		for (int i = 2; i < emailAddress.length(); i++) {
			newPssd += emailAddress.charAt(i);
			if (i > 5) {
				break;
			}
		}
		newPssd += randomGenerator.nextInt(100000);
		if (newPssd.length() > 9) {
			for (int i = 5; i < emailAddress.length(); i++) {
				newPssd += emailAddress.charAt(i);
				if (i > 9) {
					break;
				}
			}
		}
		newPssd += randomGenerator.nextInt(1000);
		return newPssd;

	}

	
	
	public String sendEmail(String recipient, String subject, String content) throws Exception{
		// https://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/
		
		// smtp.jouy.inra.fr works
		/*
		String host="smtp.jouy.inra.fr"; 
		final String smtp_username_auth="insyght@inra.fr"; // insyght@inra.fr marche
		final String smtp_password_auth="XXXX";
		final String from="insyght@inra.fr"; // thomas.lacroix@inra.fr et insyght@jouy.inra.fr works
		Properties props = new Properties();  
		props.put("mail.smtp.host", host);  
		props.put("mail.smtp.auth", "false"); // true, false marche
		props.put("mail.smtp.port", 25);// 587 ne marche pas, 25 marche
		*/
		
		/**/
		//smtp.inra.fr NOT WORKING: com.sun.mail.util.MailConnectException: Couldn't connect to host, port: smtp.inra.fr, 25; timeout -1; nested exception is: java.net.SocketException: Permission denied: connect
		final String smtp_username_auth="Insyght_dev";
		final String smtp_password_auth="ypY9<99D;z";
		final String from="Insyght_dev@inra.fr"; //insyght@inra.fr, tlacroix@inra.fr ne marche pas
		Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.inra.fr");
        props.put("mail.smtp.starttls.enable","true"); // ne marche pas avec ou sans
        props.put("mail.smtp.localhost", "inra.fr"); // ne marche pas avec ou sans
        props.put("mail.smtp.port", 587);//587 et 25 ne marche pas
        props.put("mail.smtp.auth", "true"); // true, false ne marche pas
		
		Session session = Session.getDefaultInstance(props,  
		 new javax.mail.Authenticator() {  
		   protected PasswordAuthentication getPasswordAuthentication() {  
		 return new PasswordAuthentication(smtp_username_auth, smtp_password_auth);  //password
		   }  
		 });  
		  
		//Compose the message  
		try {  
		  MimeMessage message = new MimeMessage(session);  
		  message.setFrom(new InternetAddress(from));  
		  message.addRecipient(Message.RecipientType.TO,new InternetAddress(recipient));  
		  message.setSubject(subject);  
		  message.setText(content);  
		       
		  //send the message  
		  Transport.send(message);  
		  
		  //System.out.println("message sent successfully...");  // print in apache log sdout
		   
		 } catch (MessagingException e) {e.printStackTrace();}  
		
		
		
		/* OLD
		
		// send email
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		//props.setProperty("mail.user", "Insyght@inra.fr"); // Insyght@inra.fr include me, JF et Val
		//props.setProperty("mail.user", "thomas.lacroix@inra.fr");
		props.setProperty("mail.user", "thomaslacroix_2003@yahoo.fr");
		props.setProperty("mail.password", "");
		
		// old smtp server INRA not working ??
		//props.setProperty("mail.host", "smtp.jouy.inra.fr");
		// smtp.jouy.inra.fr NOT WORKING
		//props.setProperty("mail.host", "138.102.162.52");
		// 138.102.162.52 ??

		
		// new smtp server INRA not working
		//props.put("mail.from", "Insyght@inra.fr");
		//props.put("mail.host", "smtp.inra.fr");
		//props.put("mail.smtp.host", "smtp.inra.fr");
		// smtp.inra.fr NOT WORKING
		// 147.100.174.11 ??
		//props.put("mail.protocol.port", "25");
		
		// server gmail
		//props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		

		Session mailSession = Session.getDefaultInstance(props, null);
		Transport transport = mailSession.getTransport();

		MimeMessage message = new MimeMessage(mailSession);
		message.setSubject(subject);
		message.setContent(content, "text/plain");
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
				recipient));

		transport.connect();
		transport.sendMessage(message, message
				.getRecipients(Message.RecipientType.TO));
		transport.close();
		*/
	      
		return "email sent!";
		
	}

	public String voidifySessionIdIndb(String sessionId) throws Exception {
		
		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForLoginImpl voidifySessionIdIndb",
				sessionId, "sessionId", true, true);
		
		Connection conn = null;
		Statement statement = null;
		//ResultSet rs = null;

		try {

			conn = DatabaseConf.getConnection_db();
			
			String command2 = "UPDATE user_info SET session_id = '' WHERE session_id = '" + sessionId + "'";
			statement = conn.createStatement();

			int numberRowsModified = statement.executeUpdate(command2);	
			
			if (numberRowsModified < 1) {
				UtilitiesMethodsServer.reportException("CallForLoginImpl voidifySessionIdIndb",new Exception("Login does not exists. Please register a new account with this login."));
			} else if (numberRowsModified > 1) {
				UtilitiesMethodsServer.reportException("CallForLoginImpl voidifySessionIdIndb",new Exception("Login does not exists. Please register a new account with this login."));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForLoginImpl voidifySessionIdIndb",ex);
		} finally {
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println("ERROR in CallForLoginImpl voidifySessionIdIndb statement.close()() : " + ex + "\n" + ex.getMessage());
			}
			DatabaseConf.freeConnection(conn, "CallForLoginImpl voidifySessionIdIndb");
		}//try
		
		return null;
	}

}
