package fr.inra.jouy.server.callForEntryMethods;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentPairs;
import fr.inra.jouy.server.queriesTable.QueriesTableGenes;
import fr.inra.jouy.server.queriesTable.QueriesTableProtAndNucSequences;

public class DownloadServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	//protected void doGet( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {

		/*System.err.println("getParameterValues Reference CDS = "+req.getParameterValues("Reference CDS"));
		System.err.println("getParameterValues Compared CDS = "+req.getParameterValues("Compared CDS"));
		System.err.println("getParameterValues All reference CDSs in the selected synteny = "+req.getParameterValues("All reference CDSs in the selected synteny"));
		System.err.println("getParameterValues All compared CDSs in the selected synteny = "+req.getParameterValues("All compared CDSs in the selected synteny"));
		System.err.println("getParameterValues All reference CDSs in the selected genomic region without homologs = "+req.getParameterValues("All reference CDSs in the selected genomic region without homologs"));
		System.err.println("getParameterValues All compared CDSs in the selected genomic region without homologs = "+req.getParameterValues("All compared CDSs in the selected genomic region without homologs"));
		System.err.println("getParameterValues All reference CDSs in the selected gene set = "+req.getParameterValues("All reference CDSs in the selected gene set"));
		System.err.println("getParameterValues All compared CDSs in the selected gene set = "+req.getParameterValues("All compared CDSs in the selected gene set"));
		System.err.println("getParameterValues lbExportType = "+req.getParameterValues("lbExportType"));
		System.err.println("getParameterValues lbExportFormat = "+req.getParameterValues("lbExportFormat"));*/
		
		String[] paramReferenceCDS = req.getParameterValues("Reference CDS");
		String[] paramComparedCDS = req.getParameterValues("Compared CDS");
		String[] paramAllReferenceCDSsSelectedSynteny = req.getParameterValues("All reference CDSs in the selected synteny");
		String[] paramAllComparedCDSsSelectedSynteny = req.getParameterValues("All compared CDSs in the selected synteny");
		String[] paramAllReferenceCDSsSelectedGenomicRegionWithoutHomologs = req.getParameterValues("All reference CDSs in the selected genomic region without homologs");
		String[] paramAllComparedCDSsSelectedGenomicRegionWithoutHomologs = req.getParameterValues("All compared CDSs in the selected genomic region without homologs");
		String[] paramAllReferenceCDSsSelectedGeneSet = req.getParameterValues("All reference CDSs in the selected gene set");
		String[] paramAllComparedCDSsSelectedGeneSet = req.getParameterValues("All compared CDSs in the selected gene set");
		String exportTypeParam = req.getParameterValues("lbExportType")[0];
		String exportFormatParam = req.getParameterValues("lbExportFormat")[0];
		
		//correspondance old with new parameters
//        String[] alGeneIdParam = req.getParameterValues("geneId");
//        // <-> paramReferenceCDS
//        // <-> paramComparedCDS
//        String[] alQAlignmentIdParam = req.getParameterValues("qAlignmentId");
//        // <-> paramAllReferenceCDSsSelectedSynteny
//        String[] alSAlignmentIdParam = req.getParameterValues("sAlignmentId");
//        // <-> paramAllComparedCDSsSelectedSynteny
//        String[] alElementIdStartStopParam = req.getParameterValues("elementId_pbStartInElement_pbStopInElement");
//        // <-> paramAllReferenceCDSsSelectedGenomicRegionWithoutHomologs
//        // <-> paramAllComparedCDSsSelectedGenomicRegionWithoutHomologs
//        String[] alGeneSetIdsParam = req.getParameterValues("geneSetIds");
//        // <-> paramAllReferenceCDSsSelectedGeneSet
//        // <-> paramAllComparedCDSsSelectedGeneSet
//        String exportTypeParam = req.getParameterValues("exportType")[0];
//        String exportFormatParam = req.getParameterValues("exportFormat")[0];
        
        ServletOutputStream out = resp.getOutputStream();
        try {
        	String stToReturn = "";
        	
        	Connection conn = DatabaseConf.getConnection_db();
        	
        	HashMap<Integer,Integer> alGeneIds2Marker = new HashMap<Integer,Integer>();
        	
        	//geneIdParam
        	/*if(alGeneIdParam != null){
        		for(int i=0;i<alGeneIdParam.length;i++){
            		alGeneIds2Marker.put(Integer.parseInt(alGeneIdParam[i]),1);
            	}
        	}*/
        	if(paramReferenceCDS != null){
        		for(int i=0;i<paramReferenceCDS.length;i++){
            		alGeneIds2Marker.put(Integer.parseInt(paramReferenceCDS[i]),1);
            	}
        	}
        	if(paramComparedCDS != null){
        		for(int i=0;i<paramComparedCDS.length;i++){
            		alGeneIds2Marker.put(Integer.parseInt(paramComparedCDS[i]),1);
            	}
        	}
        	
        	//alQAlignmentIdParam
        	/*if(alQAlignmentIdParam != null){
        		if(alQAlignmentIdParam.length > 0){
        			HashSet<Long> hsAlignmentIdsFromParam = new HashSet<>();
        			for(int i=0;i<alQAlignmentIdParam.length;i++){
        				hsAlignmentIdsFromParam.add(Long.parseLong(alQAlignmentIdParam[i]));
        			}*/
        	//paramAllReferenceCDSsSelectedSynteny
        	if(paramAllReferenceCDSsSelectedSynteny != null){
        		if(paramAllReferenceCDSsSelectedSynteny.length > 0){
        			HashSet<Long> hsAlignmentIdsFromParam = new HashSet<>();
        			for(int i=0;i<paramAllReferenceCDSsSelectedSynteny.length;i++){
        				hsAlignmentIdsFromParam.add(Long.parseLong(paramAllReferenceCDSsSelectedSynteny[i]));
        			}
        			// RQ type for genes
        			// type 1 : strong homolog BDBH with avg score 220, average evalue
        			// 5.76e-5
        			// type 2 : weaker homolog with avg score 137, average evalue
        			// 2.2e-4
        			// type 3 mismatch
        			// type 4 s insertion
        			// type 5 q insertion
        			HashSet<Integer> hsTypeToRetrieve = new HashSet<>();
        			hsTypeToRetrieve.add(1);
        			hsTypeToRetrieve.add(2);
        			hsTypeToRetrieve.add(3);
        			//hsTypeToRetrieve.add(4);
        			hsTypeToRetrieve.add(5);
        			ArrayList<Integer> geneIdsFromAlQAlignmentId = QueriesTableAlignmentPairs.getListQGenesIdsWithAlAlignmentId(
        					conn
        					, hsAlignmentIdsFromParam
        					, true
        					, hsTypeToRetrieve
        					);
        			if(!geneIdsFromAlQAlignmentId.isEmpty()){
        				for(int i=0;i<geneIdsFromAlQAlignmentId.size();i++){
        					int intIT = geneIdsFromAlQAlignmentId.get(i);
        					alGeneIds2Marker.put(intIT, 1);
        				}
        			}
        		}
        	}
        	
        	//alSAlignmentIdParam
        	/*if(alSAlignmentIdParam != null){
        		if(alSAlignmentIdParam.length > 0){
        			HashSet<Long> hsAlignmentIdsFromParam = new HashSet<>();
        			for(int i=0;i<alSAlignmentIdParam.length;i++){
        				hsAlignmentIdsFromParam.add(Long.parseLong(alSAlignmentIdParam[i]));
        			}*/
        	//paramAllComparedCDSsSelectedSynteny
        	if(paramAllComparedCDSsSelectedSynteny != null){
        		if(paramAllComparedCDSsSelectedSynteny.length > 0){
        			HashSet<Long> hsAlignmentIdsFromParam = new HashSet<>();
        			for(int i=0;i<paramAllComparedCDSsSelectedSynteny.length;i++){
        				hsAlignmentIdsFromParam.add(Long.parseLong(paramAllComparedCDSsSelectedSynteny[i]));
        			}
        			// RQ type for genes
        			// type 1 : strong homolog BDBH with avg score 220, average evalue
        			// 5.76e-5
        			// type 2 : weaker homolog with avg score 137, average evalue
        			// 2.2e-4
        			// type 3 mismatch
        			// type 4 s insertion
        			// type 5 q insertion
        			HashSet<Integer> hsTypeToRetrieve = new HashSet<>();
        			hsTypeToRetrieve.add(1);
        			hsTypeToRetrieve.add(2);
        			hsTypeToRetrieve.add(3);
        			hsTypeToRetrieve.add(4);
        			//alTypeToRetrieve.add(5);
        			ArrayList<Integer> geneIdsFromAlSAlignmentId = QueriesTableAlignmentPairs.getListSGenesIdsWithAlAlignmentId(
        					conn
        					, hsAlignmentIdsFromParam
        					, true
        					, hsTypeToRetrieve
        					);
        			if(!geneIdsFromAlSAlignmentId.isEmpty()){
        				for(int i=0;i<geneIdsFromAlSAlignmentId.size();i++){
        					int intIT = geneIdsFromAlSAlignmentId.get(i);
        					alGeneIds2Marker.put(intIT, 1);
        				}
        			}
        		}
        	}
        			
			
        	//alElementIdStartStopParam
        	/*if(alElementIdStartStopParam != null){
        		if(alElementIdStartStopParam.length > 0){
        			ArrayList<Integer> ListElementIdsThenStartPbthenStopPBLooped = new ArrayList<Integer>();
        			for(int i=0;i<alElementIdStartStopParam.length;i++){
        				String alElementIdStartStopParamIT = alElementIdStartStopParam[i];*/
////        String[] alElementIdStartStopParam = req.getParameterValues("elementId_pbStartInElement_pbStopInElement");
//        		        // <-> paramAllReferenceCDSsSelectedGenomicRegionWithoutHomologs
//        		        // <-> paramAllComparedCDSsSelectedGenomicRegionWithoutHomologs
        	if(paramAllReferenceCDSsSelectedGenomicRegionWithoutHomologs != null){
        		if(paramAllReferenceCDSsSelectedGenomicRegionWithoutHomologs.length > 0){
        			ArrayList<Integer> ListElementIdsThenStartPbthenStopPBLooped = new ArrayList<Integer>();
        			for(int i=0;i<paramAllReferenceCDSsSelectedGenomicRegionWithoutHomologs.length;i++){
        				String alElementIdStartStopParamIT = paramAllReferenceCDSsSelectedGenomicRegionWithoutHomologs[i];
        				
        				String[] splitedIT = alElementIdStartStopParamIT.split("_");
        				int elementIdIT = Integer.parseInt(splitedIT[0]);
        				int startPbIT = Integer.parseInt(splitedIT[1]);
        				int stopPbIT = Integer.parseInt(splitedIT[2]);
        				ListElementIdsThenStartPbthenStopPBLooped.add(elementIdIT);
        				ListElementIdsThenStartPbthenStopPBLooped.add(startPbIT);
        				ListElementIdsThenStartPbthenStopPBLooped.add(stopPbIT);
        			}
        			
        			ArrayList<Integer> geneIdsFromAlElementIdStartStopParam = 
        					QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
        							conn
        							, ListElementIdsThenStartPbthenStopPBLooped
        							, null
        							, true // grabCDSWithAtLeastOnePbInThisStartStopLocus
        							//, true // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
        							, 10 // ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch
									, true // orderCDSByStartAsc
        							, false // shouldBePresentInTable
        							);
        			//ArrayList<Integer> geneIdsFromAlElementIdStartStopParam = getGeneIdsWithAlElementIdStartStop(alElementIdStartStopParam, conn);
        			if(!geneIdsFromAlElementIdStartStopParam.isEmpty()){
        				for(int i=0;i<geneIdsFromAlElementIdStartStopParam.size();i++){
        					int intIT = geneIdsFromAlElementIdStartStopParam.get(i);
        					alGeneIds2Marker.put(intIT, 1);
        				}
        			}
        		}
        	}

        	
        	/*if(alElementIdStartStopParam != null){
        		if(alElementIdStartStopParam.length > 0){
        			ArrayList<Integer> ListElementIdsThenStartPbthenStopPBLooped = new ArrayList<Integer>();
        			for(int i=0;i<alElementIdStartStopParam.length;i++){
        				String alElementIdStartStopParamIT = alElementIdStartStopParam[i];*/
////        	        String[] alElementIdStartStopParam = req.getParameterValues("elementId_pbStartInElement_pbStopInElement");
//        		        // <-> paramAllReferenceCDSsSelectedGenomicRegionWithoutHomologs
//        		        // <-> paramAllComparedCDSsSelectedGenomicRegionWithoutHomologs
        	if(paramAllComparedCDSsSelectedGenomicRegionWithoutHomologs != null){
        		if(paramAllComparedCDSsSelectedGenomicRegionWithoutHomologs.length > 0){
        			ArrayList<Integer> ListElementIdsThenStartPbthenStopPBLooped = new ArrayList<Integer>();
        			for(int i=0;i<paramAllComparedCDSsSelectedGenomicRegionWithoutHomologs.length;i++){
        				String alElementIdStartStopParamIT = paramAllComparedCDSsSelectedGenomicRegionWithoutHomologs[i];
        				
        				String[] splitedIT = alElementIdStartStopParamIT.split("_");
        				int elementIdIT = Integer.parseInt(splitedIT[0]);
        				int startPbIT = Integer.parseInt(splitedIT[1]);
        				int stopPbIT = Integer.parseInt(splitedIT[2]);
        				ListElementIdsThenStartPbthenStopPBLooped.add(elementIdIT);
        				ListElementIdsThenStartPbthenStopPBLooped.add(startPbIT);
        				ListElementIdsThenStartPbthenStopPBLooped.add(stopPbIT);
        			}
        			
        			ArrayList<Integer> geneIdsFromAlElementIdStartStopParam = 
        					QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
        							conn
        							, ListElementIdsThenStartPbthenStopPBLooped
        							, null
        							, true // grabCDSWithAtLeastOnePbInThisStartStopLocus
        							//, true // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
        							, 10 // ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch
									, true // orderCDSByStartAsc
        							, false // shouldBePresentInTable
        							);
        			//ArrayList<Integer> geneIdsFromAlElementIdStartStopParam = getGeneIdsWithAlElementIdStartStop(alElementIdStartStopParam, conn);
        			if(!geneIdsFromAlElementIdStartStopParam.isEmpty()){
        				for(int i=0;i<geneIdsFromAlElementIdStartStopParam.size();i++){
        					int intIT = geneIdsFromAlElementIdStartStopParam.get(i);
        					alGeneIds2Marker.put(intIT, 1);
        				}
        			}
        		}
        	}

        				
        	//alGeneSetIdsParam
        	/*if(alGeneSetIdsParam != null){
        		for(int i=0;i<alGeneSetIdsParam.length;i++){
        			String[] alGeneSetIdsIT = alGeneSetIdsParam[i].split(",");
        			for(int j=0;j<alGeneSetIdsIT.length;j++){
        				alGeneIds2Marker.put(Integer.parseInt(alGeneSetIdsIT[j].trim()),1);
        			}
            	}
        	}*/
//          String[] alGeneSetIdsParam = req.getParameterValues("geneSetIds");
//          // <-> paramAllReferenceCDSsSelectedGeneSet
//          // <-> paramAllComparedCDSsSelectedGeneSet
        	if(paramAllReferenceCDSsSelectedGeneSet != null){
        		for(int i=0;i<paramAllReferenceCDSsSelectedGeneSet.length;i++){
        			
        			//System.err.println("paramAllReferenceCDSsSelectedGeneSet["+i+"] = "+paramAllReferenceCDSsSelectedGeneSet[i]);
        			
        			String[] alGeneSetIdsIT = paramAllReferenceCDSsSelectedGeneSet[i].split(",");
        			for(int j=0;j<alGeneSetIdsIT.length;j++){
        				alGeneIds2Marker.put(Integer.parseInt(alGeneSetIdsIT[j].trim()),1);
        			}
            	}
        	}
        	if(paramAllComparedCDSsSelectedGeneSet != null){
        		for(int i=0;i<paramAllComparedCDSsSelectedGeneSet.length;i++){
        			String[] alGeneSetIdsIT = paramAllComparedCDSsSelectedGeneSet[i].split(",");
        			for(int j=0;j<alGeneSetIdsIT.length;j++){
        				alGeneIds2Marker.put(Integer.parseInt(alGeneSetIdsIT[j].trim()),1);
        			}
            	}
        	}
        	
        	
        	String fileName = "data_exported_from_insyght.txt";

        	if(!alGeneIds2Marker.isEmpty()
        			&& exportTypeParam.compareTo("proteinSequence")==0
        			&& exportFormatParam.compareTo("fasta")==0
        			){
        		        		
        		stToReturn = QueriesTableProtAndNucSequences.getProteinSequenceAsFastaWithAlGeneIds(conn, alGeneIds2Marker);
        		fileName = "exported_protein_sequences.fasta";
        		
        	}else if(!alGeneIds2Marker.isEmpty()
        			&& exportTypeParam.compareTo("dnaSequence")==0
        			&& exportFormatParam.compareTo("fasta")==0){
        		stToReturn = QueriesTableProtAndNucSequences.getNucleicAcidSequenceAsFastaWithAlGeneIds(
        				conn
        				, alGeneIds2Marker.keySet()
        				, true //withHeader
        				, true //lineSeparatorInSeq
        				);
        		fileName = "exported_dna_sequences.fasta";
        		
        	}else{
        		UtilitiesMethodsServer.reportException("DownloadServlet doGet",new Exception("do not match !alExportEntitiesAsString.isEmpty() && exportType.compareTo(proteinSequence)==0 && exportFormat.compareTo(fasta)==0\n"));
        	}
        	
        	if(stToReturn == null){
        		UtilitiesMethodsServer.reportException("DownloadServlet doGet",new Exception("stToReturn == null"));
        	}
        	if(stToReturn.isEmpty()){
        		UtilitiesMethodsServer.reportException("DownloadServlet doGet",new Exception("stToReturn.isEmpty"));
        	}
        	
        	//force chrome respect formating
        	//stToReturn = "<pre>" + stToReturn + "</pre>";
        	
            byte[] bytes = stToReturn.getBytes();          
            //resp.setContentType("text/plain; charset=utf-8"); //text/html
            resp.setContentType("text/html");
            //resp.setContentType("application/x-download"); // not in use anymore
            //resp.setContentType("application/force-download"); // not in use anymore
            resp.setHeader( "Content-Disposition", " attachment; filename=" + fileName );
            resp.setContentLength(bytes.length);
            
            //set cookie as a marker client side to know when the download is done
            Cookie userCookie = new Cookie("testDownloadDone", "testDownloadDone");
            userCookie.setPath("/");
            //userCookie.setDomain(pattern);
            //userCookie.setMaxAge(-1); //A negative value means that the cookie is not stored persistently and will be deleted when the Web browser exits. 60*60*24*365 = Store cookie for 1 year
            userCookie.setMaxAge(60*60); //A negative value means that the cookie is not stored persistently and will be deleted when the Web browser exits. 60*60*24*365 = Store cookie for 1 hour
            resp.addCookie(userCookie);
            
            
            out.write(bytes, 0, bytes.length);
        } catch (Exception e) {
            e.printStackTrace();
            byte[] bytes = e.toString().getBytes();
            resp.setContentType("text/plain; charset=utf-8"); //text/html
            //resp.setHeader( "Content-Disposition:", "attachment;filename=" + "\"data_exported_from_insyght.txt\"" );
            resp.setContentLength(bytes.length);
            out.write(bytes, 0, bytes.length);
        } finally {
            out.flush();
            out.close();
        }
    }



	
}
