package fr.inra.jouy.server.callForEntryMethods;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import fr.inra.jouy.client.RPC.CallForUpdateDB;
import fr.inra.jouy.server.BCrypt;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.shared.pojos.users.PersonsObj;

public class CallForUpdateDBImpl extends RemoteServiceServlet implements
		CallForUpdateDB {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public PersonsObj updateNameAndAffiInfo(String textLastName,
			String textFirstName, String labo, String sessionId)
			throws Exception {

		UtilitiesMethodsServer.checkValidFreeStringInput(
					"CallForUpdateDBImpl updateNameAndAffiInfo",
					textFirstName, "textFirstName", false, false);
		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForUpdateDBImpl updateNameAndAffiInfo",
				textLastName, "textLastName", false, false);
		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForUpdateDBImpl updateNameAndAffiInfo",
				labo, "labo", false, false);
		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForUpdateDBImpl updateNameAndAffiInfo",
				sessionId, "sessionId", false, false);
	
		
		PersonsObj personObjIT = new PersonsObj();

		Connection conn = null;
		String command = "UPDATE user_info SET nom = '" + textLastName
				+ "', prenom = '" + textFirstName + "', labo = '"
				+ labo + "' WHERE session_id = '" + sessionId + "'";

		Statement statement = null;
		//ResultSet rs = null;

		try {
			conn = DatabaseConf.getConnection_db();

			statement = conn.createStatement();
			
			int numberRowsUpdated = statement.executeUpdate(command);
			
			if (numberRowsUpdated != 1) {
				UtilitiesMethodsServer.reportException("CallForUpdateDBImpl updateNameAndAffiInfo",new Exception(" expected 1 row updated but "
								+ numberRowsUpdated + " row(s) were updated."));
			}

			personObjIT.setLabo(labo);
			personObjIT.setNom(textLastName);
			personObjIT.setPrenom(textFirstName);

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForUpdateDBImpl updateNameAndAffiInfo",ex);
		} finally {
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			DatabaseConf.freeConnection(conn, "CallForUpdateDBImpl updateNameAndAffiInfo");
		}//try

		return personObjIT;

	}

	
	public String updatePssdInfo(String newPssd, String oldPssd,
			String sessionId) throws Exception {

		
		Connection conn = null;

		String command = "SELECT password FROM user_info WHERE session_id = '"
				+ sessionId + "'";

		Statement statement = null;
		ResultSet rs = null;

		try {
			conn = DatabaseConf.getConnection_db();

			statement = conn.createStatement();
			rs = statement.executeQuery(command);

			
			rs.next();

			//Check if an incoming user/pass combo is valid for java using jBCrypt:
			String hashFromDB = rs.getString("password");
			boolean valid = BCrypt.checkpw(oldPssd, hashFromDB);
			if (valid) {

				//update pssd

				String hash = BCrypt.hashpw(newPssd, BCrypt.gensalt());
				//System.out.println("hash : "+hash);
				String command2 = "UPDATE user_info SET password = '" + hash
						+ "' WHERE session_id = '" + sessionId + "'";
				
				int numberRowsUpdated = statement.executeUpdate(command2);
				
				if (numberRowsUpdated != 1) {
					UtilitiesMethodsServer.reportException("CallForUpdateDBImpl updatePssdInfo",new Exception("expected 1 row updated but "
									+ numberRowsUpdated
									+ " row(s) were updated."));
				}

			} else {
				UtilitiesMethodsServer.reportException("CallForUpdateDBImpl updatePssdInfo",new Exception("The old password you entered is incorrect : "+oldPssd));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForUpdateDBImpl updatePssdInfo",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err.println(ex + "\n" + ex.getMessage());
			}
			DatabaseConf.freeConnection(conn, "CallForUpdateDBImpl updatePssdInfo");
		}//try

		return null;
	}

}
