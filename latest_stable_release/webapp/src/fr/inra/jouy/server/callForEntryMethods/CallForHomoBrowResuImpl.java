package fr.inra.jouy.server.callForEntryMethods;

import java.text.DecimalFormat;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import fr.inra.jouy.client.RPC.CallForHomoBrowResu;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentPairs;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentParams;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignments;
import fr.inra.jouy.server.queriesTable.QueriesTableElements;
import fr.inra.jouy.server.queriesTable.QueriesTableGenes;
import fr.inra.jouy.server.queriesTable.QueriesTableHomologs;
import fr.inra.jouy.server.queriesTable.QueriesTableOrganisms;
import fr.inra.jouy.server.queriesTable.QueriesTableProtAndNucSequences;
import fr.inra.jouy.server.queriesTable.QueriesTableQElementId2SortedListCompOrgaWhole;
import fr.inra.jouy.shared.RefGeneSetForAnnotCompa;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotCategoryCompa;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotClasses;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotComparedGene;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotNames;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotOrganisms;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotRefGenes;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotRoot;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_scope;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortType;
import fr.inra.jouy.shared.pojos.applicationItems.SharedAppParams;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
//import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.EnumResultListSortScopeType;
import fr.inra.jouy.shared.pojos.databaseMapping.AlignmentParametersForSynteny;
import fr.inra.jouy.shared.pojos.databaseMapping.GeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneMatchItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.TransAlignmPairs;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.TransAbsoPropQSSyntItem;
import fr.inra.jouy.shared.TransAbsoPropResuGeneSet;
import fr.inra.jouy.shared.TransAbsoPropResuGeneSetHold;
import fr.inra.jouy.shared.TransAbsoPropResuHold;
import fr.inra.jouy.shared.TransStartStopGeneInfo;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.comparators.TransientAbsoluteProportionResultGeneSetComparator;
import fr.inra.jouy.shared.comparators.TransientStartStopGeneInfoComparator;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import javax.activation.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang3.StringUtils;



public class CallForHomoBrowResuImpl extends RemoteServiceServlet
		implements CallForHomoBrowResu {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static Pattern sortedListWholeOrgaPatt = Pattern.compile("^(\\d+)\\(s=(\\d+)\\.?\\d*\\)$");
	
	@Override
	public RefGenoPanelAndListOrgaResu getRefGenomePanelAndListOrgaResult(
			SearchItem searchItemSent, String sessionId) throws Exception {

		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForHomoBrowResulImpl getRefGenomePanelAndListOrgaResult",
				sessionId, "sessionId", true, true);
		

		RefGenoPanelAndListOrgaResu refGenomePanelAndListOrgaResultToReturn = new RefGenoPanelAndListOrgaResu();
		refGenomePanelAndListOrgaResultToReturn
				.setExcludeAutomaticallyComputedResults(searchItemSent
						.isExcludeAutomaticallyComputedResults());
		refGenomePanelAndListOrgaResultToReturn
				.setAlignmentParametersForSynteny(searchItemSent
						.getAlignmentParametersForSynteny());
		refGenomePanelAndListOrgaResultToReturn
				.setListExcludedGenome(searchItemSent.getListExcludedGenome());

		Connection conn = null;

		try {

			conn = DatabaseConf.getConnection_db();
			
			if (searchItemSent.getReferenceOrganism().getOrganismId() < 0) {
				if (!searchItemSent.getReferenceOrganism().getListAllLightElementItem().isEmpty()) {
					String accnumIT = searchItemSent.getReferenceOrganism().getListAllLightElementItem().get(0).getAccession();
					int orgaIdReturned = QueriesTableElements.getOrgaIdWithAccnum(conn, accnumIT);
					if(orgaIdReturned > 0){
						searchItemSent.getReferenceOrganism().setOrganismId(orgaIdReturned);
					}
				} else if (searchItemSent.getReferenceOrganism().getSpecies() != null){
					int orgaIdReturned = QueriesTableOrganisms.getOrgaIdWithSpeciesStrainSubstrain(
							conn, 
							searchItemSent.getReferenceOrganism().getSpecies(),
							searchItemSent.getReferenceOrganism().getStrain(),
							searchItemSent.getReferenceOrganism().getSubstrain()
							);
					if(orgaIdReturned > 0){
						searchItemSent.getReferenceOrganism().setOrganismId(orgaIdReturned);
					}
				}
			}
			if (searchItemSent.getReferenceOrganism().getOrganismId() < 0) {
				UtilitiesMethodsServer.reportException("CallForHomoBrowResulImpl getRefGenomePanelAndListOrgaResult", new Exception("ERROR : could not find organism, ids and species name are null"));
			}

			checkIfRefOrganismIsPrivateAndIfValidSessionId(conn, searchItemSent
					.getReferenceOrganism().getOrganismId(), sessionId);
			checkIfPrivateGenomeAmongOptions(conn,
					searchItemSent.getListExcludedGenome(),
					searchItemSent.getListUserSelectedOrgaToBeResults());

			/*
			 * 
			 * 1. get completed LightGeneItem with intial gene set LightGeneItem
			 */
			ArrayList<Integer> lstReferenceGenesSet = new ArrayList<Integer>();
			if (!searchItemSent.getListReferenceGeneSet().isEmpty()) {
				for (int i = 0; i < searchItemSent.getListReferenceGeneSet()
						.size(); i++) {
					int giIT = searchItemSent.getListReferenceGeneSet().get(i)
							.getGeneId();

					if (giIT < 0) {
						if (searchItemSent.getListReferenceGeneSet().get(i)
								.getLocusTagAsRawString() != null) {
							if (!searchItemSent.getListReferenceGeneSet()
									.get(i).getLocusTagAsRawString().isEmpty()) {
								giIT = QueriesTableGenes.getGeneIdWithLocusTagAndOrganismId(
										conn,
										searchItemSent
												.getListReferenceGeneSet()
												.get(i)
												.getLocusTagAsRawString(),
										searchItemSent.getReferenceOrganism()
												.getOrganismId(),
										true);
							}
						}
						if (giIT < 0
								&& searchItemSent.getListReferenceGeneSet()
										.get(i).getNameAsRawString() != null) {
							if (!searchItemSent.getListReferenceGeneSet()
									.get(i).getNameAsRawString().isEmpty()) {
								giIT = QueriesTableGenes.getGeneIdWithGeneNameAndOrganismId_checkTablesGenesThenQualifiers_firstMatchOnly(
										conn,
										searchItemSent
												.getListReferenceGeneSet()
												.get(i).getNameAsRawString(),
										searchItemSent.getReferenceOrganism()
												.getOrganismId(),
												true);
							}
						}
						if (giIT < 0) {
							UtilitiesMethodsServer.reportException("CallForHomoBrowResulImpl getRefGenomePanelAndListOrgaResult(SearchItem, String)",
									new Exception(
											"ERROR : could not find gene id for gene locus tag = "
											+searchItemSent.getListReferenceGeneSet().get(i).getLocusTagAsRawString()
											+ " name = "
											+ searchItemSent.getListReferenceGeneSet().get(i).getNameAsRawString()
									)
							);
						}
					}
					lstReferenceGenesSet.add(giIT);
				}
				
				refGenomePanelAndListOrgaResultToReturn.getRefGeneSetForAnnotationsComparator().setListGeneIds(lstReferenceGenesSet);
				refGenomePanelAndListOrgaResultToReturn.getRefGeneSetForAnnotationsComparator().setListElementIdsThenStartPbthenStopPBLooped(null);
			} else if(!searchItemSent.getReferenceOrigamiElementIdsThenStartPbthenStopPBLooped().isEmpty()){
				lstReferenceGenesSet = QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
						conn
						, searchItemSent.getReferenceOrigamiElementIdsThenStartPbthenStopPBLooped()
						, null
						, true // grabCDSWithAtLeastOnePbInThisStartStopLocus
						//, true // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
						, 10 // ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch
						, true // orderCDSByStartAsc
						, false // shouldBePresentInTable
						);
				
				refGenomePanelAndListOrgaResultToReturn.getRefGeneSetForAnnotationsComparator().setListGeneIds(null);
				refGenomePanelAndListOrgaResultToReturn.getRefGeneSetForAnnotationsComparator().setListElementIdsThenStartPbthenStopPBLooped(searchItemSent.getReferenceOrigamiElementIdsThenStartPbthenStopPBLooped());
			} else {
				
				//lstReferenceGenesSet = CallForInfoDBImpl.getAllGeneIdsWithOrigamiOrganismId(conn, searchItemSent.getReferenceOrganism().getOrganismId());
				ArrayList<Integer> arrayListElementIdOrderedBySizeDesc = QueriesTableElements.getAlElementIdWithOrganismId_optionalOrderBySizeDesc(
						conn
						, searchItemSent.getReferenceOrganism().getOrganismId()
						, true, true);
				lstReferenceGenesSet = QueriesTableGenes.getAlGenesIdsOrderedByAlElementIdWithAlElementId_optionalOrderedByStart(
						conn
						, arrayListElementIdOrderedBySizeDesc
						, true
						, true);
				refGenomePanelAndListOrgaResultToReturn.setCountTotalNumberCDSForRefOrganismIfRefGeneSetIsWholeOrga(lstReferenceGenesSet.size());
				refGenomePanelAndListOrgaResultToReturn.getRefGeneSetForAnnotationsComparator().setListGeneIds(null);
				ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedForOrga = QueriesTableElements.getAlElementIdsThenStartPbthenStopPBLoopedWithOrgaId(conn, searchItemSent.getReferenceOrganism().getOrganismId());
				refGenomePanelAndListOrgaResultToReturn.getRefGeneSetForAnnotationsComparator().setListElementIdsThenStartPbthenStopPBLooped(listElementIdsThenStartPbthenStopPBLoopedForOrga);
				
			}

			refGenomePanelAndListOrgaResultToReturn
					.setListReferenceGeneSetForHomologsTable(lstReferenceGenesSet);
			// System.err.println("lstReferenceGenesSet : "+lstReferenceGenesSet.toString());

			/*
			 * 
			 * 3. get the referenceGenomePanelItem
			 */

			//LightOrganismItem refOrgaItam = CallForInfoDBImpl.getLightOrganismItemWithOrgaId(conn, searchItemSent.getReferenceOrganism().getOrganismId());
			LightOrganismItem refOrgaItam = QueriesTableOrganisms.getLightOrganismItemWithOrgaId(
					conn
					, searchItemSent.getReferenceOrganism().getOrganismId()
					, false
					, true
			);
			
			GenomePanelItem referenceGenomePanelItem = new GenomePanelItem(
					refOrgaItam);
			// referenceGenomePanelItem.setResultPosition(resultPos + 1);
			referenceGenomePanelItem
					.setSizeState(GenomePanelItem.GpiSizeState.MIN);
			referenceGenomePanelItem.setReferenceGenome(true);
			refGenomePanelAndListOrgaResultToReturn
					.setReferenceGenomePanelItem(referenceGenomePanelItem);

			/*ArrayList<AbsoPropQElemItem> alITElementItemQ = getListAbsoluteProportionQElementItemWithOrigamiOrgaIdOrderedByLenghtDesc(
					conn,
					searchItemSent.getReferenceOrganism().getOrganismId(),
					false);*/
			ArrayList<AbsoPropQElemItem> alITElementItemQ = QueriesTableElements.getAlAbsoPropQElemItemWithOrgaId_orderByLenghtDesc(
					conn
					, searchItemSent.getReferenceOrganism().getOrganismId()
					, false
					, true
					);
			
			
			
			referenceGenomePanelItem.setListAbsoluteProportionElementItemQ(alITElementItemQ);

			// System.out.println("Finished get the paralogs for the ref gene set in "
			// + (System.currentTimeMillis() - milli3) + " milliseconds");
			// //test

			/*
			 * 
			 * 4. get the result list
			 */

			// long milli4 = System.currentTimeMillis(); // TEST remove test
			// time

			ArrayList<LightOrganismItem> lstOrgaResult = null;

			// set View type
			if(searchItemSent.getViewTypeDesired() != null){
				refGenomePanelAndListOrgaResultToReturn
				.setViewTypeInsyght(searchItemSent.getViewTypeDesired());
			}else{
				//default view type
				if(refGenomePanelAndListOrgaResultToReturn.getListReferenceGeneSetForHomologsTable().size() > 1){
					refGenomePanelAndListOrgaResultToReturn
					.setViewTypeInsyght(EnumResultViewTypes.homolog_table);
				}else{
					refGenomePanelAndListOrgaResultToReturn
					.setViewTypeInsyght(EnumResultViewTypes.genomic_organization);
				}
			}
			
			// set firstRowShownId
			if(searchItemSent.getFirstRowShownId() >0){
				refGenomePanelAndListOrgaResultToReturn.setFirstRowShownId(searchItemSent.getFirstRowShownId());
			}
			
			// set LookUp element and genes
			if (refGenomePanelAndListOrgaResultToReturn
					.getListReferenceGeneSetForHomologsTable().size() == 1) {
				// System.err.println("here");
				int geneIdForLookUp = refGenomePanelAndListOrgaResultToReturn
						.getListReferenceGeneSetForHomologsTable().get(0);
				
				ArrayList<Integer> geneStartStopElementIdToLookUp = QueriesTableGenes.getAlStartStopElementIdWithGeneId(conn, geneIdForLookUp);
				int geneStartToLookUp = geneStartStopElementIdToLookUp.get(0);//CallForInfoDBImpl.getGeneStartWithGeneId(conn, geneIdForLookUp);
				int geneStopToLookUp = geneStartStopElementIdToLookUp.get(1);//CallForInfoDBImpl.getGeneStopWithGeneId(conn, geneIdForLookUp);
				int geneElementIdToLookUp = geneStartStopElementIdToLookUp.get(2);//QueriesTableGenes.getElementIdWithGeneId(conn, geneIdForLookUp, true);
				refGenomePanelAndListOrgaResultToReturn
						.setGlobalLookUpForGeneWithQStart(geneStartToLookUp);
				refGenomePanelAndListOrgaResultToReturn
						.setGlobalLookUpForGeneWithQStop(geneStopToLookUp);
				refGenomePanelAndListOrgaResultToReturn
						.setGlobalLookUpForGeneOnQElementId(geneElementIdToLookUp);
				referenceGenomePanelItem
						.setLookUpForGeneWithQStart(geneStartToLookUp, EnumResultViewTypes.genomic_organization);
				referenceGenomePanelItem
						.setLookUpForGeneWithQStop(geneStopToLookUp, EnumResultViewTypes.genomic_organization);
				referenceGenomePanelItem
						.setLookUpForGeneOnQElementId(geneElementIdToLookUp, EnumResultViewTypes.genomic_organization);
			}

			// default sort type
			//refGenomePanelAndListOrgaResultToReturn.setResultListSortScopeType(searchItemSent.getResultListSortScopeType());
			refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_scope(searchItemSent.getSortResultListBy_scope());
			refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortType(searchItemSent.getSortResultListBy_sortType());
			refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortOrder(searchItemSent.getSortResultListBy_sortOrder());
			
			
			//if (refGenomePanelAndListOrgaResultToReturn.getResultListSortScopeType() == null) {
			if (refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_scope() == null) {
				if (refGenomePanelAndListOrgaResultToReturn.getViewTypeInsyght().compareTo(
								EnumResultViewTypes.homolog_table) == 0) {
					if (refGenomePanelAndListOrgaResultToReturn
							.getListReferenceGeneSetForHomologsTable().size() > SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED) {
						//refGenomePanelAndListOrgaResultToReturn.setResultListSortScopeType(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_scope(SearchItem.Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortType(SearchItem.Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortOrder(SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder.Descending);
					} else {
						//refGenomePanelAndListOrgaResultToReturn.setResultListSortScopeType(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_scope(SearchItem.Enum_comparedGenomes_SortResultListBy_scope.RefGeneSet);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortType(SearchItem.Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortOrder(SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder.Descending);
					}
				} else {
					//refGenomePanelAndListOrgaResultToReturn.setResultListSortScopeType(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC);
					//Default
					refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_scope(SearchItem.Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism);
					refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortType(SearchItem.Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs);
					refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortOrder(SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder.Descending);

					
					
				}
				// refGenomePanelAndListOrgaResultToReturn.setResultListSortScopeType(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS);
			} else {
				if (
						//refGenomePanelAndListOrgaResultToReturn.getResultListSortScopeType().toString().startsWith("SCOPE_GENESET_")
						refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_scope().compareTo(Enum_comparedGenomes_SortResultListBy_scope.RefGeneSet) == 0
						|| 
						//refGenomePanelAndListOrgaResultToReturn.getResultListSortScopeType().toString().startsWith("SCOPE_GENE_")
						refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_scope().compareTo(Enum_comparedGenomes_SortResultListBy_scope.SelectedRefGene) == 0
						) {
					if (refGenomePanelAndListOrgaResultToReturn.getListReferenceGeneSetForHomologsTable().size() < 1) {
						//refGenomePanelAndListOrgaResultToReturn.setResultListSortScopeType(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_scope(SearchItem.Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortType(SearchItem.Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortOrder(SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder.Descending);
					} else if (refGenomePanelAndListOrgaResultToReturn.getListReferenceGeneSetForHomologsTable().size() > SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED) {
						//refGenomePanelAndListOrgaResultToReturn.setResultListSortScopeType(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_scope(SearchItem.Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortType(SearchItem.Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs);
						refGenomePanelAndListOrgaResultToReturn.setSortResultListBy_sortOrder(SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder.Descending);
					}
				}
			}

			
			
			
			/*if (
					//refGenomePanelAndListOrgaResultToReturn.getResultListSortScopeType().compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
					refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_scope().compareTo(Enum_comparedGenomes_SortResultListBy_scope.RefGeneSet) == 0
					&& refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_sortType().compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs) == 0
					&& refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_sortOrder().compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending) == 0
					) {
				lstOrgaResult = getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
						conn
						, searchItemSent.getReferenceOrganism().getOrganismId()
						, refGenomePanelAndListOrgaResultToReturn.getListReferenceGeneSetForHomologsTable()
						, searchItemSent.getAlignmentParametersForSynteny()
						, searchItemSent.getListUserSelectedOrgaToBeResults()
						, searchItemSent.getListExcludedGenome()
						, SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC
						);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						refGenomePanelAndListOrgaResultToReturn.getListReferenceGeneSetForHomologsTable(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						refGenomePanelAndListOrgaResultToReturn.getListReferenceGeneSetForHomologsTable(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_ASC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						refGenomePanelAndListOrgaResultToReturn.getListReferenceGeneSetForHomologsTable(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_DESC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						refGenomePanelAndListOrgaResultToReturn
								.getListReferenceGeneSetForHomologsTable(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						refGenomePanelAndListOrgaResultToReturn
								.getListReferenceGeneSetForHomologsTable(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_ASC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_DESC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC);
			} else if (refGenomePanelAndListOrgaResultToReturn
					.getResultListSortScopeType()
					.compareTo(
							SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0) {
				lstOrgaResult = getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
						conn,
						searchItemSent.getReferenceOrganism().getOrganismId(),
						searchItemSent.getAlignmentParametersForSynteny(),
						searchItemSent.getListUserSelectedOrgaToBeResults(),
						searchItemSent.getListExcludedGenome(),
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC);

			*/


			if ( refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_scope().compareTo(Enum_comparedGenomes_SortResultListBy_scope.RefGeneSet) == 0 ) {
				lstOrgaResult = getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
						conn
						, searchItemSent.getReferenceOrganism().getOrganismId()
						, refGenomePanelAndListOrgaResultToReturn.getListReferenceGeneSetForHomologsTable()
						, searchItemSent.getAlignmentParametersForSynteny()
						, searchItemSent.getListUserSelectedOrgaToBeResults()
						, searchItemSent.getListExcludedGenome()
						//, SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC
						, refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_sortType()
						, refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_sortOrder()
						);
			} else if ( refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_scope().compareTo(Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism) == 0 ) {
				lstOrgaResult = getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
						conn
						, searchItemSent.getReferenceOrganism().getOrganismId()
						, searchItemSent.getAlignmentParametersForSynteny()
						, searchItemSent.getListUserSelectedOrgaToBeResults()
						, searchItemSent.getListExcludedGenome()
						//, SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC
						, refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_sortType()
						, refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_sortOrder()
						);
				
				//TODO
				//implement getListPublicOrgaResultScopeGeneWithRefOrgaIdAndSlectedGeneAndParamForSynteny(conn, qOrgaId, qGeneId, alignmentParametersForSynteny, listFeaturedOrga, listExcludedOrga, sortScopeType_sortType, sortScopeType_sortOrder)

			} else {
				UtilitiesMethodsServer.reportException("CallForHomoBrowResulImpl getRefGenomePanelAndListOrgaResult(SearchItem, String)", new Exception("ERROR IN getRefGenomePanelAndListOrgaResult : Unrecognized getSortResultListBy_scope : "
								+ refGenomePanelAndListOrgaResultToReturn.getSortResultListBy_scope().toString()));
			}
			
			
			// deal with featured orga
			ArrayList<LightOrganismItem> alFeaturedOrgaToBuild = new ArrayList<LightOrganismItem>();
			for (LightOrganismItem loiToBeResultsIT : searchItemSent.getListUserSelectedOrgaToBeResults()) {
				for (int i=0;i<lstOrgaResult.size();i++) {
					LightOrganismItem loiFromLstOrgaResultIT = lstOrgaResult.get(i);
					if (loiToBeResultsIT.getOrganismId() == loiFromLstOrgaResultIT.getOrganismId()) {
						alFeaturedOrgaToBuild.add(loiFromLstOrgaResultIT);
						lstOrgaResult.remove(i);
						break;
					}
				}
			}
			refGenomePanelAndListOrgaResultToReturn.setListUserSelectedOrgaToBeResults(alFeaturedOrgaToBuild);

			refGenomePanelAndListOrgaResultToReturn.setLstOrgaResult(lstOrgaResult);

			// System.out.println("Finished get the result list in " +
			// (System.currentTimeMillis() - milli4) + " milliseconds"); // test

			// System.out.println(
			// "************************ Finished getRefGenomePanelAndListOrgaResult in "
			// + (System.currentTimeMillis() - milli0) + " milliseconds");//test

			return refGenomePanelAndListOrgaResultToReturn;

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForHomoBrowResulImpl getRefGenomePanelAndListOrgaResult(SearchItem, String)", ex);
		} finally {
			DatabaseConf.freeConnection(conn, "CallForHomoBrowResulImpl getRefGenomePanelAndListOrgaResult");
		}// try
		return null;

	}



	@Override
	public ArrayList<LightOrganismItem> getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
			int qOrgaId
			, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<LightOrganismItem> listFeaturedOrga
			, ArrayList<LightOrganismItem> listExcludedOrga
			//, SearchItem.EnumResultListSortScopeType sortScopeType
			//, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			)
			throws Exception {

		ArrayList<LightOrganismItem> al = getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
				null
				, qOrgaId
				, alignmentParametersForSynteny
				, listFeaturedOrga
				, listExcludedOrga
				//, sortScopeType
				//, sortScopeType_scope
				, sortScopeType_sortType
				, sortScopeType_sortOrder
				);

		return al;

	}

	/* alignmentParametersForSynteny can be null as it is never used*/
	private ArrayList<LightOrganismItem> getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
			Connection conn
			, int qOrgaId
			, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<LightOrganismItem> listFeaturedOrga
			, ArrayList<LightOrganismItem> listExcludedOrga
			//, SearchItem.EnumResultListSortScopeType sortScopeType
			//, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			) throws Exception {

		// type can be abundanceHomologs, syntenyScore or alignemntScore
		String methodNameToReport = "CallForHomoBrowResulImpl getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny"
				+ " ; qOrgaId="+qOrgaId
				+ " ; alignmentParametersForSynteny="+alignmentParametersForSynteny
				+ " ; listFeaturedOrga=" + ( (listFeaturedOrga != null ) ? listFeaturedOrga.toString() : "NULL" )	
				+ " ; listExcludedOrga=" + ( (listExcludedOrga != null ) ? listExcludedOrga.toString() : "NULL" )	
				//+ " ; sortScopeType="+sortScopeType.toString()
				+ " ; sortScopeType_sortType="+sortScopeType_sortType.toString()
				+ " ; sortScopeType_sortOrder="+sortScopeType_sortOrder.toString()
				;
		
		if (sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologsAnnotations) == 0) {
			UtilitiesMethodsServer.reportException(methodNameToReport
					, new Exception("Unsuported sortType : "+sortScopeType_sortType.toString())
					);
		}
		
		//Statement statement = null;
		//ResultSet rs = null;
		boolean closeConn = false;
		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();
			
			boolean insertZeroAtBeginningOfArray = false;

			ArrayList<LightOrganismItem> lstOrgaResult = new ArrayList<LightOrganismItem>();
			ArrayList<Integer> alOrgaIdsToExclude = new ArrayList<Integer>();
			for (int i = 0; i < listExcludedOrga.size(); i++) {
				int idFeaturedIT = listExcludedOrga.get(i).getOrganismId();
				alOrgaIdsToExclude.add(idFeaturedIT);
			}

//			if (
//					sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0
//					
//					) {

			String columnNameTable_q_orga_2_precomputed_score = "";
			String columnNameTable_alignments = "";
			if (
					//sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
					//|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0
					sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs) == 0
					) {
				columnNameTable_q_orga_2_precomputed_score = "abundance_homologs";
				columnNameTable_alignments = "pairs";
			} else if (
					//sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
					//|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0
					sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.SyntenyScore) == 0
					) {
				columnNameTable_q_orga_2_precomputed_score = "synteny_score";
				columnNameTable_alignments = "score";
			} else if (
					//sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
					//|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0
					sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AlignemntScore) == 0
					) {
				columnNameTable_q_orga_2_precomputed_score = "alignemnt_score";
			} else {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("sortScopeType_sortType not recognized columnNameTable_q_orga_2_precomputed_score : "+sortScopeType_sortType)
						);
			}
			
			//ArrayList<Integer> listElementIdIT = CallForInfoDBImpl.getListElementIdOrderedBySizeDescWithOrganismId(conn, qOrgaId);
			ArrayList<Integer> listElementIdIT = QueriesTableElements.getAlElementIdWithOrganismId_optionalOrderBySizeDesc(conn, qOrgaId, false, true);
			ArrayList<String> listOrgaPrecomputed = QueriesTableQElementId2SortedListCompOrgaWhole.
					getAlStringOrgaPrecomputedWithAlElementId(
							conn
							, columnNameTable_q_orga_2_precomputed_score
							, listElementIdIT
							);
			
			


			//fork according to presence table q_element_id_2_sorted_list_comp_orga_whole
//				System.err.println("new listOrgaPrecomputed = "+listOrgaPrecomputed);
			//listOrgaPrecomputed.clear();
//				System.err.println("bis listOrgaPrecomputed = "+listOrgaPrecomputed);
			ArrayList<Integer> listSOrgaIds = new ArrayList<Integer>();
			HashMap<Integer, Integer> orgaId2finalScore = new HashMap<Integer, Integer>();
			if (listOrgaPrecomputed.isEmpty()) {
				
				if (
//						sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
//						|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0
//						|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
//						|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0
						sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs) == 0
						|| sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.SyntenyScore) == 0
						) {
					

					HashMap<Long, Integer> alignmentParamId2sOrganismId = QueriesTableAlignmentParams
							.getHMAalignmentParamId2sOrganismIdWithQOrganismId(
									conn
									, qOrgaId
									, false
									, false
									);
					
					HashMap<Long, Integer> alignmentParamId2cummulativeScore = QueriesTableAlignments
							.getHMAlignmentParamId2cummulativeSumAsIntWithSetAlignmentParamId(
									conn
									, alignmentParamId2sOrganismId.keySet()
									, true
									, columnNameTable_alignments
									, false
									, false
									);
					
					// crunch score by organism
					for (Long keyAlignmentParamIdWithScore : alignmentParamId2cummulativeScore
							.keySet()) {
						int cumScoreIT = alignmentParamId2cummulativeScore
								.get(keyAlignmentParamIdWithScore);
						if (alignmentParamId2sOrganismId
								.containsKey(keyAlignmentParamIdWithScore)) {
							int orgaIdIT = alignmentParamId2sOrganismId
									.get(keyAlignmentParamIdWithScore);
							if (orgaId2finalScore.containsKey(orgaIdIT)) {
								int currScoreForOrgaId = orgaId2finalScore
										.get(orgaIdIT);
								orgaId2finalScore.put(orgaIdIT, currScoreForOrgaId
										+ cumScoreIT);
							} else {
								orgaId2finalScore.put(orgaIdIT, cumScoreIT);
							}
						} else {
							UtilitiesMethodsServer.reportException(
									methodNameToReport
									, new Exception("no alignmentParamId2sOrganismId.containsKey "
									+ keyAlignmentParamIdWithScore));
						}
					}

				} else if (
//						sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
//						|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0
						sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AlignemntScore) == 0
						) {

					
					orgaId2finalScore = QueriesTableHomologs.getHMorgaId2cummulativeSumAsIntWithQOrganismId(
							conn
							, qOrgaId
							, "score"
							, false
							, false
							);
					
				} else {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("sortScopeType_sortType not recognized QueriesTableHomologs query : "+sortScopeType_sortType)
							);
				}
				
				
			} else {
				// ok got result through table q_orga_id_2_list_sorted...
				Matcher m = null;
				HashMap<Integer, Integer> ElementId2OrganismId = QueriesTableElements.getHashElementId2OrgaId(conn);
				for (String listEletIdWithScoreIT : listOrgaPrecomputed) {
					ArrayList<String> listSEletIdsWithScoreAsString = new ArrayList<String>(Arrays.asList(listEletIdWithScoreIT.replaceAll("''", "").split(",")));
					for (String sEletIdsWithScoreIT : listSEletIdsWithScoreAsString) {
						m = sortedListWholeOrgaPatt.matcher(sEletIdsWithScoreIT);
						if (m.matches()) {
							int sElementIdIT = Integer.parseInt(m.group(1));
							int scoreIT = Integer.parseInt(m.group(2));
							int sOrgaIdIT = -1;
							if (ElementId2OrganismId.get(sElementIdIT) == null) {
								UtilitiesMethodsServer.reportException(
										methodNameToReport
										, new Exception("ElementId2OrganismId.get(sElementIdIT) is null for : "+sElementIdIT));

							} else {
								sOrgaIdIT = ElementId2OrganismId.get(sElementIdIT);
							}
							if (orgaId2finalScore.containsKey(sOrgaIdIT)) {
								int currScoreForOrgaId = orgaId2finalScore
										.get(sOrgaIdIT);
								orgaId2finalScore.put(sOrgaIdIT, currScoreForOrgaId
										+ scoreIT);
							} else {
								orgaId2finalScore.put(sOrgaIdIT, scoreIT);
							}
						} else {
							UtilitiesMethodsServer.reportException(
									methodNameToReport
									, new Exception("could not sortedListWholeOrgaPatt the following string : "+sEletIdsWithScoreIT));
						}
					}
				}
			}
			
			
			//order orga_id DESC
			//LinkedHashMap<Integer, Integer> sortedLhm = UtilitiesMethodsShared.sortMapByIntegerValues(
			//		orgaId2finalScore,
			//		true);
			// store in listSOrgaIds
			listSOrgaIds = new ArrayList<Integer>(orgaId2finalScore.keySet());
			listSOrgaIds.removeAll(alOrgaIdsToExclude);
			
			
			//get the real orga object
			ArrayList<Integer> excludeQOrgaId = new ArrayList<>();
			excludeQOrgaId.add(qOrgaId);
			lstOrgaResult.addAll(QueriesTableOrganisms.getAlLightOrganismItems_orderByScoreAscIfProvided(
					conn
					, listSOrgaIds
					, excludeQOrgaId
					, orgaId2finalScore
					, false
					, true
					, true
					, false
					)
				);
			


			

			// order array asc or desc
			//boolean isDesc = true;
			if (
//					sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
					sortScopeType_sortOrder.compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending) == 0
					) {
				insertZeroAtBeginningOfArray = true;
			} else if (
//					sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0
					sortScopeType_sortOrder.compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Descending) == 0
					) {
				Collections.reverse(lstOrgaResult);
			} else {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("sortScopeType_sortOrder not recognized Collections.reverse : "+sortScopeType_sortOrder));
			}

			
			// get orga with score = 0
			alOrgaIdsToExclude.addAll(excludeQOrgaId);
			for (int i = 0; i < lstOrgaResult.size(); i++) {
				alOrgaIdsToExclude.add(lstOrgaResult.get(i)
						.getOrganismId());
			}
			ArrayList<LightOrganismItem> alLOIScoreZero = QueriesTableOrganisms.getAlLightOrganismItems_orderByScoreAscIfProvided(
					conn
					, null
					, alOrgaIdsToExclude
					, null
					, true
					, false
					, true
					, true
					);
			
			if (insertZeroAtBeginningOfArray) {
				lstOrgaResult.addAll(0, alLOIScoreZero);
			} else {
				lstOrgaResult.addAll(alLOIScoreZero);
			}
			UtilitiesMethodsShared.setLOIPositionInResultListAccordingToArrayIndex(lstOrgaResult);

			return lstOrgaResult;

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			//UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return null;
		
	}




	@Override
	public ArrayList<LightOrganismItem> getListPublicOrgaResultScopeGeneWithRefOrgaIdAndSlectedGeneAndParamForSynteny(
			int orgaId
			, int referenceGeneId
			, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<LightOrganismItem> listFeaturedOrga
			, ArrayList<LightOrganismItem> listExcludedOrga
			//, SearchItem.EnumResultListSortScopeType sortScopeType
			//, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			)
			throws Exception {

		ArrayList<LightOrganismItem> al = getListPublicOrgaResultScopeGeneWithRefOrgaIdAndSlectedGeneAndParamForSynteny(
				null
				, orgaId
				, referenceGeneId
				, alignmentParametersForSynteny
				, listFeaturedOrga
				, listExcludedOrga
				//, sortScopeType
				//, sortScopeType_scope
				, sortScopeType_sortType
				, sortScopeType_sortOrder
				);
		return al;
	}

	
	private ArrayList<LightOrganismItem> getListPublicOrgaResultScopeGeneWithRefOrgaIdAndSlectedGeneAndParamForSynteny(
			Connection conn
			, int qOrgaId
			, int qGeneId
			, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<LightOrganismItem> listFeaturedOrga
			, ArrayList<LightOrganismItem> listExcludedOrga
			//, SearchItem.EnumResultListSortScopeType sortScopeType
			//, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			) throws Exception {

		// type can be abundanceHomologs, syntenyScore, alignemntScore,
		// abundanceHomologsAnnotations
		String methodNameToReport = "CallForHomoBrowResuImpl getListPublicOrgaResultScopeGeneWithRefOrgaIdAndSlectedGeneAndParamForSynteny"
				+ " ; qOrgaId="+qOrgaId
				+ " ; qGeneId="+qGeneId
				+ " ; alignmentParametersForSynteny="+alignmentParametersForSynteny.toString()
				+ " ; listFeaturedOrga=" + ( (listFeaturedOrga != null ) ? listFeaturedOrga.toString() : "NULL" )
				+ " ; listExcludedOrga=" + ( (listExcludedOrga != null ) ? listExcludedOrga.toString() : "NULL" )
				//+ " ; sortScopeType="+sortScopeType
				+ " ; sortScopeType_sortType="+sortScopeType_sortType.toString()
				+ " ; sortScopeType_sortOrder="+sortScopeType_sortOrder.toString()
				;

		//Statement statement = null;
		//ResultSet rs = null;
		boolean closeConn = false;
		
		ArrayList<LightOrganismItem> lstOrgaResult = new ArrayList<LightOrganismItem>();
		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();
			
			
			boolean insertZeroAtBeginningOfArray = false;
			if (
//					sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGSANNOTATIONS_ORDER_ASC) == 0
					sortScopeType_sortOrder.compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending) == 0
					) {
				insertZeroAtBeginningOfArray = true;
			}
			
			//String listOrgaIdToExclude = "";
			ArrayList<Integer> alOrgaIdsToExclude = new ArrayList<Integer>();
			/* NOT anymore, featured orga dealt with latter on
			 * for (int i = 0; i < listFeaturedOrga.size(); i++) {
				int idFeaturedIT = listFeaturedOrga.get(i).getOrganismId();
				alOrgaIdsToExclude.add(idFeaturedIT);
			}*/
			for (int i = 0; i < listExcludedOrga.size(); i++) {
				int idFeaturedIT = listExcludedOrga.get(i).getOrganismId();
				alOrgaIdsToExclude.add(idFeaturedIT);
			}
			
			HashMap<Integer, LightOrganismItem> intermediateResultsOrgaId2LOI = QueriesTableOrganisms.getHmOrgaId2LOIWithQGeneIdAndQOrgaIdAndSortScopeType_scopeGene(
					conn
					, qGeneId
					, qOrgaId
					//, sortScopeType
					, sortScopeType_sortType
					, sortScopeType_sortOrder
					, alOrgaIdsToExclude
					);
			
			//turn hash intermediateResultsOrgaId2LOI into arraylist lstOrgaResult
			lstOrgaResult = new ArrayList<LightOrganismItem>(intermediateResultsOrgaId2LOI.values());
			
			//sort arraylist lstOrgaResult
			Comparator<LightOrganismItem> compareLOIByScoreIT = new QueriesTableOrganisms.compareLOIByScoreAsc();
			Collections.sort(lstOrgaResult, compareLOIByScoreIT);
			
			if (
//					sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGSANNOTATIONS_ORDER_ASC) == 0
					sortScopeType_sortOrder.compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending) == 0
					) {
				//do nothing, already sorted asc
			} else if (
//					sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGSANNOTATIONS_ORDER_DESC) == 0
					sortScopeType_sortOrder.compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Descending) == 0
					) {
				Collections.reverse(lstOrgaResult);
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("sortScopeType_sortOrder not recognized Collections.reverse : "+sortScopeType_sortOrder.toString()));
			}
			
			
			// get orga with score = 0
			ArrayList<Integer> excludeQOrgaId = new ArrayList<>();
			excludeQOrgaId.add(qOrgaId);
			alOrgaIdsToExclude.addAll(excludeQOrgaId);
			for (int i = 0; i < lstOrgaResult.size(); i++) {
				alOrgaIdsToExclude.add(lstOrgaResult.get(i)
						.getOrganismId());
			}
			ArrayList<LightOrganismItem> alLOIScoreZero = QueriesTableOrganisms.getAlLightOrganismItems_orderByScoreAscIfProvided(
					conn
					, null
					, alOrgaIdsToExclude
					, null
					, true
					, false
					, true
					, true
					);
			if (insertZeroAtBeginningOfArray) {
				lstOrgaResult.addAll(0, alLOIScoreZero);
			} else {
				lstOrgaResult.addAll(alLOIScoreZero);
			}
			UtilitiesMethodsShared.setLOIPositionInResultListAccordingToArrayIndex(lstOrgaResult);
			

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			//UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return lstOrgaResult;
	}
	



	@Override
	public ArrayList<LightOrganismItem> getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
			int qOrgaIdSent
			, ArrayList<Integer> lstReferenceGenesSet
			, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<LightOrganismItem> listFeaturedOrga
			, ArrayList<LightOrganismItem> listExcludedOrga
			//, SearchItem.EnumResultListSortScopeType sortScopeType
			//, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			)
			throws Exception {

		ArrayList<LightOrganismItem> al = getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
				null
				, qOrgaIdSent
				, lstReferenceGenesSet
				, alignmentParametersForSynteny
				, listFeaturedOrga
				, listExcludedOrga
				//, sortScopeType
				//, sortScopeType_scope
				, sortScopeType_sortType
				, sortScopeType_sortOrder
				);

		return al;

	}

	private ArrayList<LightOrganismItem> getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
			Connection conn
			, int qOrgaId
			, ArrayList<Integer> alQGeneIds
			, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<LightOrganismItem> listFeaturedOrga
			, ArrayList<LightOrganismItem> listExcludedOrga
			//, SearchItem.EnumResultListSortScopeType sortScopeType
			//, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			) throws Exception {

		if (alQGeneIds.size() > SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED) {
			return getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
					conn
					, qOrgaId
					, alignmentParametersForSynteny
					, listFeaturedOrga
					, listExcludedOrga
					//, sortScopeType
					, sortScopeType_sortType
					, sortScopeType_sortOrder
					);
		}
		
		String methodNameToReport = "CallForHomoBrowResuImpl getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny"
				+ " ; qOrgaId="+qOrgaId
				+ " ; alQGeneIds=" + ( (alQGeneIds != null ) ? alQGeneIds.toString() : "NULL" )
				+ " ; alignmentParametersForSynteny=" + ( (alignmentParametersForSynteny != null ) ? alignmentParametersForSynteny.toString() : "NULL" )
				+ " ; listFeaturedOrga=" + ( (listFeaturedOrga != null ) ? listFeaturedOrga.toString() : "NULL" )
				+ " ; listExcludedOrga=" + ( (listExcludedOrga != null ) ? listExcludedOrga.toString() : "NULL" )
				//+ " ; sortScopeType="+sortScopeType
				+ " ; sortScopeType_sortType="+sortScopeType_sortType.toString()
				+ " ; sortScopeType_sortOrder="+sortScopeType_sortOrder.toString()
				;

		if (sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologsAnnotations) == 0) {
			UtilitiesMethodsServer.reportException(methodNameToReport
					, new Exception("Unsuported sortType : "+sortScopeType_sortType.toString())
					);
		}
		
		
		//Statement statement = null;
		//ResultSet rs = null;
		boolean closeConn = false;

		ArrayList<LightOrganismItem> lstOrgaResult = new ArrayList<LightOrganismItem>();
		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();
			
			boolean insertZeroAtBeginningOfArray = false;
			if (
//					sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
					sortScopeType_sortOrder.compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending) == 0
					) {
				insertZeroAtBeginningOfArray = true;
			}

			ArrayList<Integer> alOrgaIdsToExclude = new ArrayList<Integer>();
			for (int i = 0; i < listExcludedOrga.size(); i++) {
				int idFeaturedIT = listExcludedOrga.get(i).getOrganismId();
				alOrgaIdsToExclude.add(idFeaturedIT);
			}

			HashMap<Integer, LightOrganismItem> intermediateResultsOrgaId2LOI = QueriesTableOrganisms.getHmOrgaId2LOIWithAlQGeneIdAndQOrgaIdAndSortScopeType_scopeGeneSet(
					conn
					, alQGeneIds
					, qOrgaId
					//, sortScopeType
					, sortScopeType_sortType
					, sortScopeType_sortOrder
					, alOrgaIdsToExclude
					);
			
			//turn hash intermediateResultsOrgaId2LOI into arraylist lstOrgaResult
			lstOrgaResult = new ArrayList<LightOrganismItem>(intermediateResultsOrgaId2LOI.values());

			//sort arraylist lstOrgaResult
			//Comparator<LightOrganismItem> compareLOIByScoreIT = new QueriesTableOrganisms.compareLOIByScoreAsc();
			Collections.sort(lstOrgaResult, new QueriesTableOrganisms.compareLOIByScoreAsc());
			
			if (
//					sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
					sortScopeType_sortOrder.compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending) == 0
					) {
				//do nothing, already sorted asc
			} else if (
//					sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0
//					|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0
					sortScopeType_sortOrder.compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Descending) == 0
					) {
				Collections.reverse(lstOrgaResult);
			} else {
				UtilitiesMethodsServer.reportException(
						methodNameToReport, new Exception("sortScopeType_sortOrder not recognized Collections.reverse : "+sortScopeType_sortOrder.toString()));
			}

			// get orga with score = 0
			ArrayList<Integer> excludeQOrgaId = new ArrayList<>();
			excludeQOrgaId.add(qOrgaId);
			alOrgaIdsToExclude.addAll(excludeQOrgaId);
			for (int i = 0; i < lstOrgaResult.size(); i++) {
				alOrgaIdsToExclude.add(lstOrgaResult.get(i)
						.getOrganismId());
			}

			//TODO find faster technique ?
			//can not count on table homology or alignment_pairs
			//try other techno like nosql ?
			ArrayList<LightOrganismItem> alLOIScoreZero = QueriesTableOrganisms.getAlLightOrganismItems_orderByScoreAscIfProvided(
					conn
					, null
					, alOrgaIdsToExclude
					, null
					, true
					, false
					, true
					, true
					);
			if (insertZeroAtBeginningOfArray) {
				lstOrgaResult.addAll(0, alLOIScoreZero);
			} else {
				lstOrgaResult.addAll(alLOIScoreZero);
			}
			UtilitiesMethodsShared.setLOIPositionInResultListAccordingToArrayIndex(lstOrgaResult);
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);

		} finally {
			//UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return lstOrgaResult;
	}

	



	private void checkIfRefOrganismIsPrivateAndIfValidSessionId(
			Connection conn
			, int orgaId
			, String sessionId
			) throws Exception {

		String methodNameToReport = "CallForHomoBrowResulImpl checkIfRefOrganismIsPrivateAndIfValidSessionId"
				+ " ; orgaId="+orgaId
				+ " ; sessionId="+sessionId
				;
		
		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport,
				sessionId, "sessionId", true, true);
		

		Statement statement = null; //TODO mv to appropriate QueriesTable class
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			// 1. check if private or public ?
			boolean orgaIsPublic = QueriesTableOrganisms.getIsPublicWithOrganismId(conn, orgaId);
			if (orgaIsPublic) {
				return;
			}
			

			if (sessionId.isEmpty()) {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Error while accessing private data, you are not authorized to visualise result for the requested private genome. Please log in to access those data."));
			}

			// if private, get user via sessionId
			String commandGetUserGroupeId = "SELECT user_info.user_id, user_groupe.groupe_id"
					+ " FROM user_info LEFT JOIN user_groupe ON user_info.user_id = user_groupe.user_id  WHERE user_info.session_id = '"
					+ sessionId + "'";
			// System.out.println("commandGetUserGroupeId = "+commandGetUserGroupeId);
			//rs.close();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandGetUserGroupeId, "CallForHomoBrowResulImpl checkIfRefOrganismIsPrivateAndIfValidSessionId");
			
			
			boolean firstIter = true;
			boolean loginFound = false;
			int checkUserId = -1;
			ArrayList<Integer> listGroupeId = new ArrayList<Integer>();

			while (rs.next()) {

				if (firstIter) {

					firstIter = false;
					checkUserId = rs.getInt("user_id");
					loginFound = true;

				} else {
					if (checkUserId != rs.getInt("user_id")) {
						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Multiple user_id returned for session_id"+sessionId));
					}
				}

				if (loginFound) {
					// get list of private project user has access to
					int groupeIdIT = rs.getInt("groupe_id");
					if (groupeIdIT > 0) {
						listGroupeId.add(groupeIdIT);
					}
				} else {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("No entry found for session_id"+sessionId));

				}

			}// while

			if (listGroupeId.isEmpty()) {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Error while accessing private data, you are not authorized to visualise result for the requested private genome. Please log in to access those data."));

			}

			// check that this private element id is allowed for this user

			String commandGetGroupeInfo = "SELECT organism_id FROM groupe_info WHERE groupe_id IN (";
			for (int i = 0; i < listGroupeId.size(); i++) {
				if (i == 0) {
					commandGetGroupeInfo += listGroupeId.get(i);
				} else {
					commandGetGroupeInfo += "," + listGroupeId.get(i);
				}

			}
			commandGetGroupeInfo += ")";

			// System.out.println("commandGetGroupeInfo = "+commandGetGroupeInfo);

			if(rs != null){
				rs.close();
			}
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandGetGroupeInfo, methodNameToReport);
			
			
			boolean foundAllowingAccess = false;
			while (rs.next()) {
				if (rs.getInt("organism_id") == orgaId) {
					foundAllowingAccess = true;
				}
			}

			if (!foundAllowingAccess) {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Error while accessing private data, you are not authorized to visualise result for the requested private genome. Please log in to access those data."));

			} else {
				return;
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try

	}


	private void checkIfPrivateGenomeAmongOptions(Connection conn
			, ArrayList<LightOrganismItem> listExcludedGenome
			, ArrayList<LightOrganismItem> listUserSelectedOrgaToBeResults
			) throws Exception {

		String methodNameToReport = "CallForHomoBrowResulImpl checkIfPrivateGenomeAmongOptions"
				+ " ; listExcludedGenome=" + ( (listExcludedGenome != null ) ? listExcludedGenome.toString() : "NULL" )
				+ " ; listUserSelectedOrgaToBeResults=" + ( (listUserSelectedOrgaToBeResults != null ) ? listUserSelectedOrgaToBeResults.toString() : "NULL" )
				;
		
		if (listExcludedGenome.isEmpty()
				&& listUserSelectedOrgaToBeResults.isEmpty()) {
			return;
		}

		Statement statement = null; //TODO mv to appropriate QueriesTable class
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			// 1. check if private or public ?

			String commandGetPrivateOrPublic = "SELECT organism_id, ispublic FROM organisms WHERE organism_id IN (";
			for (int i = 0; i < listExcludedGenome.size(); i++) {
				if (i == 0) {
					commandGetPrivateOrPublic += listExcludedGenome.get(i)
							.getOrganismId();
				} else {
					commandGetPrivateOrPublic += ","
							+ listExcludedGenome.get(i).getOrganismId();
				}
			}
			for (int i = 0; i < listUserSelectedOrgaToBeResults.size(); i++) {
				if (i == 0) {
					if (!listExcludedGenome.isEmpty()) {
						commandGetPrivateOrPublic += ",";
					}
					commandGetPrivateOrPublic += listUserSelectedOrgaToBeResults
							.get(i).getOrganismId();
				} else {
					commandGetPrivateOrPublic += ","
							+ listUserSelectedOrgaToBeResults.get(i)
									.getOrganismId();
				}
			}

			commandGetPrivateOrPublic += ")";

			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandGetPrivateOrPublic, methodNameToReport);
			
			
			while (rs.next()) {
				if (!rs.getBoolean("ispublic")) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("At least one genome among listExcludedGenome or listUserSelectedOrgaToBeResults is private. This operation is not allowed for security reasons."));
				}
			}

			return;

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);

		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try

	}


	

	@Override
	public int getNumberGenesBetweenGenomicRegionWithOrigamiElementIDAndStartPbAndStopPb(
			int elementId, int pbStartInElement, int pbStopInElement)
			throws Exception {
		
		String methodNameToReport = "CallForHomoBrowResulImpl getNumberGenesBetweenGenomicRegionWithOrigamiElementIDAndStartPbAndStopPb"
				+ " ; elementId="+elementId
				+ " ; pbStartInElement="+pbStartInElement
				+ " ; pbStopInElement="+pbStopInElement
				;
		
		int numberGenes = -1;

		Connection conn = null;
		//Statement statement = null;
		//ResultSet rs = null;

		try {
			conn = DatabaseConf.getConnection_db();
			//statement = conn.createStatement();
			
			numberGenes = QueriesTableGenes.getNumberGenesWithElementId_optionalStartStopBoundaries(
					conn
					, elementId
					, pbStartInElement
					, pbStopInElement
					);
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			//UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try

		return numberGenes;

	}

	
	@Override
	public int getNumberGenesWithOrigamiElementId(int elementId)
			throws Exception {
		String methodNameToReport = "CallForHomoBrowResulImpl getNumberGenesWithOrigamiElementId"
				+ " ; elementId="+elementId
				;
		int numberGenes = -1;
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();
			numberGenes = QueriesTableGenes.getNumberGenesWithElementId_optionalStartStopBoundaries(
					conn,
					elementId
					, -1
					, -1
					);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try
		return numberGenes;
	}


	@Override
	public TransAbsoPropResuHold getItemAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertionWithQArrayListOrigamiElementIdsAndSOrigamiOrgaId (
			//ArrayList<AbsoPropQElemItem> arrayListAbsoProQelementItem,
			int qOrigamiOrgaIdSent
			, int sOrigamiOrgaIdSent
			//, AlignmentParametersForSynteny alignmentParametersForSynteny
			, boolean listAbsoluteProportionElementItemSIsEmpty
			) throws Exception {

		String methodNameToReport = "CallForHomoBrowResulImpl getItemAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertionWithQArrayListOrigamiElementIdsAndSOrigamiOrgaId"
				//+ " ; arrayListAbsoProQelementItem=" + ( (arrayListAbsoProQelementItem != null ) ? arrayListAbsoProQelementItem.toString() : "NULL" )
				+ " ; qOrigamiOrgaIdSent=" + qOrigamiOrgaIdSent
				+ " ; sOrigamiOrgaIdSent=" + sOrigamiOrgaIdSent
				//+ " ; alignmentParametersForSynteny=" + alignmentParametersForSynteny.toString()
				+ "listAbsoluteProportionElementItemSIsEmpty=" + listAbsoluteProportionElementItemSIsEmpty
				;
		
		TransAbsoPropResuHold hirsToReturn = new TransAbsoPropResuHold();

		Connection conn = null;

//		long milli = System.currentTimeMillis();
//		long milliPrint2 = -1;
//		if (sOrigamiOrgaIdSent == 43) {
//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("Start method "+methodNameToReport);
//		}

		
		try {
			

			conn = DatabaseConf.getConnection_db();


			ArrayList<TransAbsoPropQSSyntItem> alQSSyntenyTotal = new ArrayList<TransAbsoPropQSSyntItem>();
			// get list of AlignmentParamId along with q_element_ids and s_element_ids for the specific q versus s organisms
			HashMap<Long, QueriesTableAlignmentParams.AlignParamsTmpObjQElementIdAndSElementId> alignmentParamId2QElementIdAndSElementId = 
					QueriesTableAlignmentParams.getHMalignmentParamId2QElementIdAndSElementId_WithQOrgaIdAndSOrgaId(
							conn
							, qOrigamiOrgaIdSent
							, sOrigamiOrgaIdSent
							, false
							, false
							);
			
			if ( ! alignmentParamId2QElementIdAndSElementId.isEmpty()) {
				alQSSyntenyTotal = QueriesTableAlignments.getAlTransAbsoPropQSSyntItem_WithalignmentParamId2QElementIdAndSElementId(
						conn
						, alignmentParamId2QElementIdAndSElementId
						, true
						//, true
						);
			}


			/*OLD WAY
			// get list of AbsoProSelementItem
			//ArrayList<AbsoPropSElemItem> alAPSelementItem = getListAbsoluteProportionSElementItemWithOrigamiOrgaIdOrderedByLenghtDesc(conn, sOrigamiOrgaIdSent);
			ArrayList<AbsoPropSElemItem> alAPSelementItem = QueriesTableElements.getAlAbsoPropSElemItemWithOrgaId_orderByLenghtDesc(
					conn
					, sOrigamiOrgaIdSent
					, false
					, true
					);
			
			ArrayList<TransAbsoPropQSSyntItem> alQSSyntenyTotal = new ArrayList<TransAbsoPropQSSyntItem>();

			// get the synteny against sOrigamiOrgaIdSent
			for (int i = 0; i < arrayListAbsoProQelementItem.size(); i++) {
				AbsoPropQElemItem apqeiIT = arrayListAbsoProQelementItem.get(i);

				for (int j = 0; j < alAPSelementItem.size(); j++) {
					AbsoPropSElemItem apseiIT = alAPSelementItem.get(j);

					// get all alignment in array
					Long alignmentParamIdIT = QueriesTableAlignmentParams.getAlignmentParamIdWithQandSElementIdAndParamForSynteny(
							conn, apqeiIT.getqOrigamiElementId(),
							apseiIT.getsOrigamiElementId()
							//, alignmentParametersForSynteny
							, false
							, false
							);
					
					if (alignmentParamIdIT == 0) {
						continue;
					}
					
					ArrayList<TransAbsoPropQSSyntItem> alITQSSynteny = QueriesTableAlignments.getListAlignmentsWithAlignmentParamId(
							conn, alignmentParamIdIT
							, apqeiIT.getqOrigamiElementId()
							, apseiIT.getsOrigamiElementId());
					//System.err.println("alignmentParamIdIT = "+alignmentParamIdIT+" ; alITQSSynteny = "+alITQSSynteny.toString());
					// for each alignment,
					// Do not support this anymore, throw error in method alignment if did not find the data
					for (int k = 0; k < alITQSSynteny.size(); k++) {
						TransAbsoPropQSSyntItem apqsSyntenyIT = alITQSSynteny
								.get(k);

						if (apqsSyntenyIT.getPbQStartSyntenyInElement() <= 0
								|| apqsSyntenyIT.getPbQStopSyntenyInElement() <= 0) {
							
							// get q start and stop
							
							// RQ type for genes
							// type 1 : strong homolog BDBH with avg score 220, average evalue
							// 5.76e-5
							// type 2 : weaker homolog with avg score 137, average evalue
							// 2.2e-4
							// type 3 mismatch
							// type 4 s insertion
							// type 5 q insertion
							ArrayList<Integer> alTypeToRetrieve = new ArrayList<>();
							alTypeToRetrieve.add(1);
							alTypeToRetrieve.add(2);
							alTypeToRetrieve.add(3);
							//alTypeToRetrieve.add(4);
							alTypeToRetrieve.add(5);
							ArrayList<Integer> alQGeneIdsIT = QueriesTableAlignmentPairs.getListQGenesIdsWithAlignmentId(
									conn
									, apqsSyntenyIT.getSyntenyOrigamiAlignmentId()
									, alTypeToRetrieve
									);

							ArrayList<Integer> alQStartStopIT = QueriesTableGenes.getFirstGeneStartAndLastGeneStopWithListOrigamiGeneIds(
									conn, alQGeneIdsIT);
							int pbqStartSyntenyInElement = alQStartStopIT
									.get(0);
							int pbqStopSyntenyInElement = alQStartStopIT.get(1);
							apqsSyntenyIT.setPbQStartSyntenyInElement(pbqStartSyntenyInElement);
							apqsSyntenyIT.setPbQStopSyntenyInElement(pbqStopSyntenyInElement);

						}

						if (apqsSyntenyIT.getPbSStartSyntenyInElement() <= 0
								|| apqsSyntenyIT.getPbSStopSyntenyInElement() <= 0) {
							// get s start and stop

							// RQ type for genes
							// type 1 : strong homolog BDBH with avg score 220, average evalue
							// 5.76e-5
							// type 2 : weaker homolog with avg score 137, average evalue
							// 2.2e-4
							// type 3 mismatch
							// type 4 s insertion
							// type 5 q insertion
							ArrayList<Integer> alTypeToRetrieve = new ArrayList<>();
							alTypeToRetrieve.add(1);
							alTypeToRetrieve.add(2);
							alTypeToRetrieve.add(3);
							alTypeToRetrieve.add(4);
							//alTypeToRetrieve.add(5);

							ArrayList<Integer> alSGeneIdsIT = QueriesTableAlignmentPairs.getListSGenesIdsWithAlignmentId(
									conn
									, apqsSyntenyIT.getSyntenyOrigamiAlignmentId()
									, alTypeToRetrieve
									);
							ArrayList<Integer> alSStartStopIT = QueriesTableGenes.getFirstGeneStartAndLastGeneStopWithListOrigamiGeneIds(
									conn, alSGeneIdsIT);
							int pbsStartSyntenyInElement = alSStartStopIT
									.get(0);
							int pbsStopSyntenyInElement = alSStartStopIT.get(1);
							apqsSyntenyIT
									.setPbSStartSyntenyInElement(pbsStartSyntenyInElement);
							apqsSyntenyIT
									.setPbSStopSyntenyInElement(pbsStopSyntenyInElement);
						}

					}
					// alAPSyntenyAndQInsertionItem.addAll(alITQSSynteny);

					alQSSyntenyTotal.addAll(alITQSSynteny);
					
				}// for each selet

			}// for each qelet
			*/
			hirsToReturn.setAlAPSyntenyItem(alQSSyntenyTotal);
			//hirsToReturn.setAlAPSEI(alAPSelementItem);
			if ( listAbsoluteProportionElementItemSIsEmpty) {
				ArrayList<AbsoPropSElemItem> alAPSelementItem = QueriesTableElements.getAlAbsoPropSElemItemWithOrgaId_orderByLenghtDesc(
						conn
						, sOrigamiOrgaIdSent
						, false
						, true
						);
				hirsToReturn.setAlAPSEI(alAPSelementItem);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try


//		if (sOrigamiOrgaIdSent == 43) {
//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("Overall it took\t" + milliPrint2 + "\tmilliseconds");
//		}
		
		return hirsToReturn;

	}


	@Override
	public ArrayList<TransAlignmPairs> getListTransientAlignmentPairsWithMainSyntenyAndRelated(
			long mainOrigamiSyntenyId
			, ArrayList<Long> listOrigamiAlignmentIdsContainedOtherSyntenies
			, ArrayList<Long> listOrigamiAlignmentIdsMotherSyntenies
			, ArrayList<Long> listOrigamiAlignmentIdsSprungOffSyntenies
			, boolean isContainedWithinAnotherMotherSynteny
			, boolean isMotherOfContainedOtherSyntenies
			, boolean isMotherOfSprungOffSyntenies
			, boolean isSprungOffAnotherMotherSynteny
			) throws Exception {


		Connection conn = null;
		ArrayList<TransAlignmPairs> alToReturn = new ArrayList<>();
		try {
			conn = DatabaseConf.getConnection_db();
			alToReturn = QueriesTableAlignmentPairs.getListTransientAlignmentPairsWithMainSyntenyAndRelated(conn
					, mainOrigamiSyntenyId
					, listOrigamiAlignmentIdsContainedOtherSyntenies
					, listOrigamiAlignmentIdsMotherSyntenies
					, listOrigamiAlignmentIdsSprungOffSyntenies
					, isContainedWithinAnotherMotherSynteny
					, isMotherOfContainedOtherSyntenies
					, isMotherOfSprungOffSyntenies
					, isSprungOffAnotherMotherSynteny
					);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForHomoBrowResulImpl getListTransientAlignmentPairsWithMainSyntenyAndRelated mainOrigamiSyntenyId="+mainOrigamiSyntenyId,ex); //getListTransientAlignmentPairsWithListOrigamiSyntenyId
		} finally {
			DatabaseConf.freeConnection(conn, "CallForHomoBrowResulImpl getListTransientAlignmentPairsWithMainSyntenyAndRelated mainOrigamiSyntenyId="+mainOrigamiSyntenyId); //getListTransientAlignmentPairsWithListOrigamiSyntenyId
		}// try
		return alToReturn;


	}

	

	@Override
	public ArrayList<TransStartStopGeneInfo> getListTransientStartStopGeneInfoWithListGeneIds(
			ArrayList<Integer> listSGeneIds) throws Exception {

		Connection conn = null;
		ArrayList<TransStartStopGeneInfo> alToReturn = null;
		try {
			conn = DatabaseConf.getConnection_db();
			alToReturn = QueriesTableGenes.getAlTransientStartStopGeneInfoWithListGeneIds(conn
					, listSGeneIds
					);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForHomoBrowResulImpl getListTransientStartStopGeneInfoWithListGeneIds listSGeneIds="+listSGeneIds.toString(),ex);
		} finally {
			DatabaseConf.freeConnection(conn, "CallForHomoBrowResulImpl getListTransientStartStopGeneInfoWithListGeneIds listSGeneIds="+listSGeneIds.toString());
		}// try
		return alToReturn;

	}

	
	@Override
	public ArrayList<TransStartStopGeneInfo> getListTransientStartStopGeneInfoWithOrigamiElementIdAndStartPbAndStopPb(
			int elementId
			, int startPb
			, int stopPb
			) throws Exception {
		String methodNameToReport = "CallForHomoBrowResulImpl getListTransientStartStopGeneInfoWithOrigamiElementIdAndStartPbAndStopPb"
				+ " ; elementId="+elementId
				+ " ; startPb="+startPb
				+ " ; stopPb="+stopPb
				;
		Connection conn = null;
		//Statement statement = null;
		//ResultSet rs = null;
		ArrayList<TransStartStopGeneInfo> alToReturn = new ArrayList<TransStartStopGeneInfo>();
		try {
			conn = DatabaseConf.getConnection_db();
			//statement = conn.createStatement();
			alToReturn = QueriesTableGenes.getAlTransStartStopGeneInfoWithElementId_optionalStartStopBoundaries(
					conn
					, elementId
					, startPb
					, stopPb
					);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			//UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try
		return alToReturn;
	}

	// get the orthologs and homologs for the ortholog table view
	@Override
	public TransAbsoPropResuGeneSetHold getTransientAbsoluteProportionType1Or2ResultGeneSetHolderWithOrgaIdAndSOrgaIdAndALQGeneIds(
			//ArrayList<AbsoPropQElemItem> listAbsoluteProportionElementItemQ
			int qOrigamiOrgaIdSent
			, int sOrigamiOrgaIdSent
			//, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<Integer> alQGeneIds
			, boolean listAbsoluteProportionElementItemSIsEmpty
			) throws Exception {

		String methodNameToReport = "CallForHomoBrowResuImpl getTransientAbsoluteProportionType1Or2ResultGeneSetHolderWithOrgaIdAndSOrgaIdAndALQGeneIds"
				//+ " ; listAbsoluteProportionElementItemQ="+ ( (listAbsoluteProportionElementItemQ != null ) ? listAbsoluteProportionElementItemQ.stream().map(AbsoPropQElemItem::getqOrigamiElementId).collect(Collectors.toList()).toString() : "NULL" )
				+ " ; qOrigamiOrgaIdSent="+qOrigamiOrgaIdSent
				+ " ; sOrigamiOrgaIdSent="+sOrigamiOrgaIdSent
				//+ " ; alignmentParametersForSynteny="+alignmentParametersForSynteny.toString()
				+ " ; alQGeneIds="+ ( (alQGeneIds != null ) ? alQGeneIds.toString() : "NULL" )
				+ "listAbsoluteProportionElementItemSIsEmpty=" + listAbsoluteProportionElementItemSIsEmpty
				;
		
		
		TransAbsoPropResuGeneSetHold hirsToReturn = new TransAbsoPropResuGeneSetHold();

		Connection conn = null;

//		long milli = System.currentTimeMillis();
//		long milliPrint2 = -1;
//		if (sOrigamiOrgaIdSent == 134) {
//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("Start method "+methodNameToReport);
//		}

		
		try {

			conn = DatabaseConf.getConnection_db();
			
//			if (sOrigamiOrgaIdSent == 134) {
//				milliPrint2 = System.currentTimeMillis() - milli;
//				System.out.println("HERE1\t" + milliPrint2 + "\tmilliseconds");
//			}
			
			
			ArrayList<TransAbsoPropResuGeneSet> alQSGH = new ArrayList<>();
			// get list of AlignmentParamId along with q_element_ids and s_element_ids for the specific q versus s organisms
			HashMap<Long, QueriesTableAlignmentParams.AlignParamsTmpObjQElementIdAndSElementId> alignmentParamId2QElementIdAndSElementId	= 
					QueriesTableAlignmentParams.getHMalignmentParamId2QElementIdAndSElementId_WithQOrgaIdAndSOrgaId(
						conn
						, qOrigamiOrgaIdSent
						, sOrigamiOrgaIdSent
						, false
						, false
					);
			
			
			
			if ( ! alignmentParamId2QElementIdAndSElementId.isEmpty()) {
				// get list of AlignmentIds along with pairs and alignmentParamId with the list of alAlignmentParamId from above
				
//				if (sOrigamiOrgaIdSent == 134) {
//					milliPrint2 = System.currentTimeMillis() - milli;
//					System.out.println("HERE2\t" + milliPrint2 + "\tmilliseconds "
//					+ " ; qOrigamiOrgaIdSent="+qOrigamiOrgaIdSent
//					+ " ; sOrigamiOrgaIdSent="+sOrigamiOrgaIdSent
//					+ " ; keySet().size = "+alignmentParamId2QElementIdAndSElementId.keySet().size());
//				}
				
				
				HashMap<Long, QueriesTableAlignments.AlignmentsTmpObjAlignmentParamIdAndPairs> alignmentId2AlignmentParamIdAndPairs = 
						QueriesTableAlignments.getHMalignmentId2AlignmentParamIdAndPairs_WithSetAlignmentParamId(
								conn
								, alignmentParamId2QElementIdAndSElementId.keySet()
								, true
								, false // some database like origami_cg still have some alignment_para_id that do not have any row in the table alignments
								);
				// get ArrayList<TransAbsoPropResuGeneSet> with raw data from table alignmentPairs for q_orga VS s_orga
				// and fill setNumberGeneInSynteny = pairs, qElementId, sElementId for each TransAbsoPropResuGeneSet
				

//				if (sOrigamiOrgaIdSent == 134) {
//					milliPrint2 = System.currentTimeMillis() - milli;
//					System.out.println("HERE3\t" + milliPrint2 + "\tmilliseconds");
//				}
				

				//HashSet<Integer> hsQGeneIds = new HashSet<>(alQGeneIds);
				if ( ! alignmentId2AlignmentParamIdAndPairs.isEmpty()) {
					alQSGH = QueriesTableAlignmentPairs.getAlTransAbsoPropResuGeneSetType1Or2WithHSAlignmentId2AlignmentParamIdAndPairsANDHSAlignmentParamId2QElementIdAndSElementIdAndAlQGeneIds(
							conn
							, alignmentId2AlignmentParamIdAndPairs
							, true
							, alignmentParamId2QElementIdAndSElementId
							, alQGeneIds
							//, hsQGeneIds
							);
				}

				
				// not Order result so element ids are by orderByLenghtDesc, look like not important
			}
			
			

			/* OLD
			ArrayList<AbsoPropSElemItem> alAPSelementItem = QueriesTableElements.getAlAbsoPropSElemItemWithOrgaId_orderByLenghtDesc(
						conn
						, sOrigamiOrgaIdSent
						, false
						, true
						);
			
			
			ArrayList<TransAbsoPropResuGeneSet> alQSGH = new ArrayList<TransAbsoPropResuGeneSet>();
			// get the synteny against sOrigamiOrgaIdSent ; fill up alQSGH
			for (int i = 0; i < listAbsoluteProportionElementItemQ.size(); i++) {
				AbsoPropQElemItem apqeiIT = listAbsoluteProportionElementItemQ
						.get(i);

				for (int j = 0; j < alAPSelementItem.size(); j++) {
					AbsoPropSElemItem apseiIT = alAPSelementItem
							.get(j);

					// get all alignment in array
					Long alignmentParamIdIT = QueriesTableAlignmentParams.getAlignmentParamIdWithQandSElementIdAndParamForSynteny(
							conn, apqeiIT.getqOrigamiElementId(),
							apseiIT.getsOrigamiElementId()
							//, alignmentParametersForSynteny
							, false
							, false
							);

					
					if (alignmentParamIdIT == 0) {
						continue;
					}

					ArrayList<Long> alAlignmentIds = QueriesTableAlignments.getListAlignmentIdsWithAlignmentParamId(
							conn
							, alignmentParamIdIT
							//, apqeiIT.getqOrigamiElementId()
							//, apseiIT.getsOrigamiElementId()
							, false
							);


					if (!alAlignmentIds.isEmpty()) {

						HashMap<Long, Integer> alignmentIds2pairs = QueriesTableAlignments.getHashOfAssociatedPairsNumberWithListAlignmentIds(
								conn, alAlignmentIds);
						
						
						QueriesTableAlignmentPairs.addToAlTransientAbsoluteProportionResultGeneSetWithAlAlignmentIdsAndAlQGeneIds(
								conn, alAlignmentIds, alQGeneIds, alQSGH,
								apqeiIT.getqOrigamiElementId(),
								apseiIT.getsOrigamiElementId(),
								alignmentIds2pairs
								);
					}

				}
			}
			*/


			// s gene first
			ArrayList<Integer> alSGeneIds = new ArrayList<Integer>();
			for (int k = 0; k < alQSGH.size(); k++) {
				TransAbsoPropResuGeneSet taprgsIT = alQSGH
						.get(k);
				alSGeneIds.add(taprgsIT.getsGeneId());
			}
			//Comparator<TransStartStopGeneInfo> compaByGeneIdTransientStartStopGeneInfoComparatorIT = new ByGeneIdTransientStartStopGeneInfoComparator();
			if (!alSGeneIds.isEmpty()) {
				
				ArrayList<TransStartStopGeneInfo> alStartStopInfoForSGene = QueriesTableGenes.getAlTransientStartStopGeneInfoWithListGeneIds(
						conn, alSGeneIds);

				
				// sort alStartStopInfoForQGene
				Collections.sort(alStartStopInfoForSGene, TransientStartStopGeneInfoComparator.byGeneIdTransientStartStopGeneInfoComparator);
				// sort alQSGH
				//Comparator<TransAbsoPropResuGeneSet> compaBySGeneIdTransientAbsoluteProportionResultGeneSetIT = new BySGeneIdTransientAbsoluteProportionResultGeneSet();
				Collections.sort(alQSGH, TransientAbsoluteProportionResultGeneSetComparator.bySGeneIdTransientAbsoluteProportionResultGeneSetComparator);
				// fill getTmpListAPQSGeneHomo with new data and compute other
				// info
				int m = -1;
				for (int l = 0; l < alQSGH.size(); l++) {
					m++;
					if (m > alStartStopInfoForSGene.size() - 1) {
						m = alStartStopInfoForSGene.size() - 1;
					}
					TransAbsoPropResuGeneSet apqsgiiIT = alQSGH
							.get(l);
					TransStartStopGeneInfo tssgiIT = alStartStopInfoForSGene
							.get(m);
					// check right one
					if (apqsgiiIT.getsGeneId() == tssgiIT.getOrigamiGeneId()) {
						// ok
						// fill new data
						apqsgiiIT.setsStart(tssgiIT.getStart());
						apqsgiiIT.setsStop(tssgiIT.getStop());
					} else {
						// patine j
						l = l - 1;
						m = m - 2;
						if (m < -1) {
							UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("s: different apsgiiIT and tssgiIT : "
											+ apqsgiiIT.getsGeneId()
											+ " ; "
											+ tssgiIT.getOrigamiGeneId()));

						} else {
							continue;
						}
					}
				}

			}
			
			
			
			// add q gene insertion
			HashMap<Integer, Integer> crunchAlQGeneIds = new HashMap<Integer, Integer>();
			for (int i = 0; i < alQGeneIds.size(); i++) {
				int qGeneIdIT = alQGeneIds.get(i);
				crunchAlQGeneIds.put(qGeneIdIT, 1);
			}
			HashMap<Integer, Integer> crunchAlQSGHQGeneIds = new HashMap<Integer, Integer>();
			for (int i = 0; i < alQSGH.size(); i++) {
				int qGeneIdIT = alQSGH.get(i).getqGeneId();
				crunchAlQSGHQGeneIds.put(qGeneIdIT, 1);
			}
			
			HashSet<Integer> alQInserQGeneId = new HashSet<>();
			for (int keyCrunchAlQGeneIds : crunchAlQGeneIds.keySet()) {
				if (!crunchAlQSGHQGeneIds.containsKey(keyCrunchAlQGeneIds)) {
					// keyCrunchAlQGeneIds is q insertion
					alQInserQGeneId.add(keyCrunchAlQGeneIds);

				}
			}
			if ( ! alQInserQGeneId.isEmpty()) {
				HashMap<Integer, Integer> hmGeneId2ElementId = QueriesTableGenes.getHMGeneId2ElementId_WithSetGeneId(
						conn
						, alQInserQGeneId
						, true
						);
				for (int qInserQGeneId : alQInserQGeneId ) {
					TransAbsoPropResuGeneSet newTAPRS = new TransAbsoPropResuGeneSet();
					newTAPRS.setqGeneId(qInserQGeneId);
					int qOrigamiElementId = hmGeneId2ElementId.get(qInserQGeneId);
					newTAPRS.setqOrigamiElementId(qOrigamiElementId);
					//newTAPRS.setqOrigamiElementId(QueriesTableGenes.getElementIdWithGeneId( conn, qInserQGeneId, true));
					alQSGH.add(newTAPRS);
				}
			}

			
			
			// then do q gene fill start and stop
			ArrayList<TransStartStopGeneInfo> alStartStopInfoForQGene = QueriesTableGenes.getAlTransientStartStopGeneInfoWithListGeneIds(
					conn, alQGeneIds);
			// sort alStartStopInfoForQGene
			Collections.sort(alStartStopInfoForQGene, TransientStartStopGeneInfoComparator.byGeneIdTransientStartStopGeneInfoComparator);
			// sort alQSGH
			//Comparator<TransAbsoPropResuGeneSet> compaByQGeneIdTransientAbsoluteProportionResultGeneSetIT = new ByQGeneIdTransientAbsoluteProportionResultGeneSet();
			Collections.sort(alQSGH, TransientAbsoluteProportionResultGeneSetComparator.byQGeneIdTransientAbsoluteProportionResultGeneSet);
			// fill getTmpListAPQSGeneHomo with new data and compute other info
			int n = -1;
			for (int l = 0; l < alQSGH.size(); l++) {
				n++;
				if (n > alStartStopInfoForQGene.size() - 1) {
					n = alStartStopInfoForQGene.size() - 1;
				}
				TransAbsoPropResuGeneSet apqsgiiIT = alQSGH
						.get(l);
				TransStartStopGeneInfo tssgiIT = alStartStopInfoForQGene
						.get(n);
				// check right one
				if (apqsgiiIT.getqGeneId() == tssgiIT.getOrigamiGeneId()) {
					// ok
					// fill new data
					apqsgiiIT.setqStart(tssgiIT.getStart());
					apqsgiiIT.setqStop(tssgiIT.getStop());
				} else {
					// patine j
					l = l - 1;
					n = n - 2;
					if (n < -1) {
						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("q: different apsgiiIT and tssgiIT : "
										+ apqsgiiIT.getqGeneId()
										+ " ; "
										+ tssgiIT.getOrigamiGeneId()));

					} else {
						continue;
					}
				}
			}

			
			
			hirsToReturn.setAlAPGeneSet(alQSGH);
			if ( listAbsoluteProportionElementItemSIsEmpty) {
				ArrayList<AbsoPropSElemItem> alAPSelementItem = QueriesTableElements.getAlAbsoPropSElemItemWithOrgaId_orderByLenghtDesc(
						conn
						, sOrigamiOrgaIdSent
						, false
						, true
						);
				hirsToReturn.setAlAPSEI(alAPSelementItem);
			}
			
//			if (sOrigamiOrgaIdSent == 134) {
//				milliPrint2 = System.currentTimeMillis() - milli;
//				System.out.println("Overall it took\t" + milliPrint2 + "\tmilliseconds");
//			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try
		
		
		return hirsToReturn;
	}



	@Override
	public CompaAnnotRoot getCompaAnnotRootWithAlGeneId(
			RefGeneSetForAnnotCompa referenceGeneSetForAnnotationsComparatorSent
	// ArrayList<Integer> refAlGeneId,
	// ArrayList<Integer> listElementIdsThenStartPbthenStopPBLooped
	// int pbQStartInElement,
	// int pbQStopInElement,
	// int origamiElementId
	) throws Exception {

		CompaAnnotRoot compaAnnotRootToReturn = new CompaAnnotRoot();
		// header
		CompaAnnotRefGenes headerCompaAnnotRefGenes = new CompaAnnotRefGenes();
		headerCompaAnnotRefGenes.setTextAkaGeneIdentifier("Reference genes");
		compaAnnotRootToReturn.getChildrens().add(headerCompaAnnotRefGenes);

		//System.err.println(referenceGeneSetForAnnotationsComparatorSent.getListGeneIds().toString());
		
		ArrayList<GeneItem> refAlGeneItem = new ArrayList<GeneItem>();
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();
			ArrayList<ArrayList<Integer>> refAlAlGeneIds = getMostSignificantAlGeneIdsWithReferenceGeneSetForAnnotationsComparator(
					conn,
					referenceGeneSetForAnnotationsComparatorSent);
			for(ArrayList<Integer> alGeneIds : refAlAlGeneIds){
				refAlGeneItem.addAll(QueriesTableGenes.getAllGeneItemWithListGeneIds(conn, alGeneIds));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForHomoBrowResulImpl getCompaAnnotRootWithAlGeneId",ex);

		} finally {
			DatabaseConf.freeConnection(conn, "CallForHomoBrowResulImpl getCompaAnnotRootWithAlGeneId");
		}// try

		if (refAlGeneItem.isEmpty()) {
			UtilitiesMethodsServer.reportException("CallForHomoBrowResulImpl getCompaAnnotRootWithAlGeneId",new Exception("refAlGeneItem is empty : "
							+ referenceGeneSetForAnnotationsComparatorSent
									.getListGeneIds().toString()
							+ " ; "
							+ referenceGeneSetForAnnotationsComparatorSent
									.getListElementIdsThenStartPbthenStopPBLooped()
									.toString()));
		}

		for (int i = 0; i < refAlGeneItem.size(); i++) {
			GeneItem giIT = refAlGeneItem.get(i);
			CompaAnnotRefGenes compaAnnotRefGenesIT = new CompaAnnotRefGenes();
			compaAnnotRefGenesIT.setTextAkaGeneIdentifier(giIT
					.getMostSignificantGeneNameAsStrippedText()
					+ " ["
					+ giIT.getLocusTagAsStrippedString() + "]");
			compaAnnotRefGenesIT.setGeneId(giIT.getGeneId());
			compaAnnotRootToReturn.getChildrens().add(compaAnnotRefGenesIT);
		}
		return compaAnnotRootToReturn;

	}



	private ArrayList<ArrayList<Integer>> getMostSignificantAlGeneIdsWithReferenceGeneSetForAnnotationsComparator(
			Connection conn,
			RefGeneSetForAnnotCompa referenceGeneSetForAnnotationsComparatorSent)
			throws Exception {
		
		ArrayList<ArrayList<Integer>> alAlToReturn = new ArrayList<ArrayList<Integer>>();
		
		if (referenceGeneSetForAnnotationsComparatorSent.getListGeneIds() != null) {
			if (!referenceGeneSetForAnnotationsComparatorSent.getListGeneIds()
					.isEmpty()) {
				alAlToReturn.add(referenceGeneSetForAnnotationsComparatorSent
						.getListGeneIds());
				return alAlToReturn;
			}
		}
		if (referenceGeneSetForAnnotationsComparatorSent
				.getListElementIdsThenStartPbthenStopPBLooped() != null) {
			if (!referenceGeneSetForAnnotationsComparatorSent
					.getListElementIdsThenStartPbthenStopPBLooped().isEmpty()) {	
				return QueriesTableGenes.getAlAlGeneIdWithElementIdAndAlGenomicBoundaries(
								conn
								, referenceGeneSetForAnnotationsComparatorSent.getListElementIdsThenStartPbthenStopPBLooped()
								, false
								, true
								);
			}
		}

		return null;
	}

	private ArrayList<CompaAnnotCategoryCompa> getCompaAnnotRefGenesWithGeneIdAndParams(
			Connection conn
			, Integer qGeneId
			, Double paramMaxEvalue
			, int paramMinPercentIdentity
			, int paramMinPercentAlignLenght
			, ArrayList<Integer> listComparedOrgaIdsSent
			, boolean bbdhOnly
			) throws Exception {

		String methodName = "CallForHomoBrowResuImpl getCompaAnnotRefGenesWithGeneIdAndParams"
				+ " ; qGeneId="+qGeneId
				+ " ; paramMaxEvalue="+paramMaxEvalue
				+ " ; paramMinPercentIdentity="+paramMinPercentIdentity
				+ " ; paramMinPercentAlignLenght="+paramMinPercentAlignLenght
				+ " ; listComparedOrgaIdsSent=" + ( (listComparedOrgaIdsSent != null ) ? listComparedOrgaIdsSent.toString() : "NULL" )
				+ " ; paramBdbhOnlySent="+bbdhOnly
				;
		
		//Statement statement = null;
		//ResultSet rs = null;
		boolean closeConn = false;

		ArrayList<CompaAnnotCategoryCompa> compaAnnotCategoryCompaToReturn = new ArrayList<CompaAnnotCategoryCompa>();

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			// step 1
			// get the list of s_gene_ids from table alignment_pairs that
			// correspond to q_gene_id = refGeneId
			ArrayList<Integer> alSGeneIds = QueriesTableAlignmentPairs.getAlSGeneIdsWithQGeneId(
					conn
					, qGeneId
					, bbdhOnly
					, false
					);

			if (alSGeneIds.isEmpty()) {
				return new ArrayList<CompaAnnotCategoryCompa>();
			}

			// optional step: filter s_genes by organisms
			if (listComparedOrgaIdsSent != null && ! listComparedOrgaIdsSent.isEmpty() ) {
				ArrayList<Integer> alBisSGeneIds = QueriesTableGenes.getAlGenesIdsWithAlOrganismIds_optionalAlGeneIds(
						conn
						, listComparedOrgaIdsSent
						, alSGeneIds
						, false
						);
				alSGeneIds.clear();
				alSGeneIds = alBisSGeneIds;

				if (alSGeneIds.isEmpty()) {
					return new ArrayList<CompaAnnotCategoryCompa>();
				}

			}

			// System.err.println("paramMaxEvalue="+paramMaxEvalue+" ; paramMinPercentAlignLenght="
			// +paramMinPercentAlignLenght+" ; paramMinPercentIdentity = "+paramMinPercentIdentity);

			// optional step: filter s_genes by homlogies criteria
			if (paramMaxEvalue >= 0.01 && paramMinPercentAlignLenght == 0
					&& paramMinPercentIdentity == 0) {
				// default param, do nothing
			} else {
				// Double paramMaxEvalue
				// int paramMinPercentIdentity,
				// int paramMinPercentAlignLenght
				
				ArrayList<Integer> alTerSGeneIds = QueriesTableHomologs
						.getAlSGeneIdWithQGeneIdAndAlSGeneIdAndMaxdEValueMinQAlignMinFracIdentity(
								conn
								, qGeneId
								, alSGeneIds
								, paramMaxEvalue
								, paramMinPercentAlignLenght
								, paramMinPercentIdentity
								, false
						);
				alSGeneIds.clear();
				alSGeneIds = alTerSGeneIds;

				if (alSGeneIds.isEmpty()) {
					return new ArrayList<CompaAnnotCategoryCompa>();
				}

			}

			// get the list of annotation features for the reference gene
			GeneItem refGeneItem = QueriesTableGenes.getGeneItemWithGeneId(conn, qGeneId);
			
			// get the list of annotation features for the homologs
			ArrayList<GeneItem> listHomologGenes = QueriesTableGenes.getAllGeneItemWithListGeneIds(conn, alSGeneIds);

			// header
			CompaAnnotCategoryCompa headercompaAnnotCategoryCompa = new CompaAnnotCategoryCompa();
			headercompaAnnotCategoryCompa
					.setTextAkaCategoryOfCompa("Comparison categories");
			compaAnnotCategoryCompaToReturn.add(headercompaAnnotCategoryCompa);

			CompaAnnotCategoryCompa compaAnnotCategoryCompaShared = new CompaAnnotCategoryCompa();
			compaAnnotCategoryCompaShared
					.setTextAkaCategoryOfCompa("[Shared] Annotations present in the reference gene and at least in one homolog");
			compaAnnotCategoryCompaToReturn.add(compaAnnotCategoryCompaShared);
			// headers
			CompaAnnotClasses headerAnnotationTypeShared = new CompaAnnotClasses();
			headerAnnotationTypeShared
					.setTextAkaAnnotationClass("Annotation classes");
			compaAnnotCategoryCompaShared.getChildrens().add(
					headerAnnotationTypeShared);

			CompaAnnotCategoryCompa compaAnnotCategoryCompaMissing = new CompaAnnotCategoryCompa();
			compaAnnotCategoryCompaMissing
					.setTextAkaCategoryOfCompa("[Missing] Annotations present in at least one homolog but missing in the reference gene");
			compaAnnotCategoryCompaToReturn.add(compaAnnotCategoryCompaMissing);
			// headers
			CompaAnnotClasses headerAnnotationTypeMissing = new CompaAnnotClasses();
			headerAnnotationTypeMissing
					.setTextAkaAnnotationClass("Annotation classes");
			compaAnnotCategoryCompaMissing.getChildrens().add(
					headerAnnotationTypeMissing);

			CompaAnnotCategoryCompa compaAnnotCategoryCompaUnique = new CompaAnnotCategoryCompa();
			compaAnnotCategoryCompaUnique
					.setTextAkaCategoryOfCompa("[Unique] Annotations present in the reference gene but missing in homologs");
			compaAnnotCategoryCompaToReturn.add(compaAnnotCategoryCompaUnique);
			// headers
			CompaAnnotClasses headerAnnotationTypeUnique = new CompaAnnotClasses();
			headerAnnotationTypeUnique
					.setTextAkaAnnotationClass("Annotation classes");
			compaAnnotCategoryCompaUnique.getChildrens().add(
					headerAnnotationTypeUnique);

			// 0 = getListFunction
			// CompaAnnotRoot compaRootFunction =
			getCompaOfThisAnnotType("Molecular Function", refGeneItem, listHomologGenes,
					compaAnnotCategoryCompaShared,
					compaAnnotCategoryCompaMissing,
					compaAnnotCategoryCompaUnique);
			// alToReturn.add(alCompaFunction);
			// compaAnnotPreRootToReturn.getChildrens().add(compaRootFunction);

			// 1 = getListBiologicalProcess
			// CompaAnnotRoot compaRootBiologicalProcess =
			getCompaOfThisAnnotType("Biological Process", refGeneItem,
					listHomologGenes, compaAnnotCategoryCompaShared,
					compaAnnotCategoryCompaMissing,
					compaAnnotCategoryCompaUnique);
			// alToReturn.add(alCompaBiologicalProcess);
			// compaAnnotPreRootToReturn.getChildrens().add(compaRootBiologicalProcess);

			// 2 = getListCellularComponent
			// CompaAnnotRoot compaRootCellularComponent =
			getCompaOfThisAnnotType("Cellular Component", refGeneItem,
					listHomologGenes, compaAnnotCategoryCompaShared,
					compaAnnotCategoryCompaMissing,
					compaAnnotCategoryCompaUnique);
			// alToReturn.add(alCompaCellularComponent);
			// compaAnnotPreRootToReturn.getChildrens().add(compaRootCellularComponent);

			// 4 = getListECNumber
			// CompaAnnotRoot compaRootECNumber =
			getCompaOfThisAnnotType("EC Number", refGeneItem, listHomologGenes,
					compaAnnotCategoryCompaShared,
					compaAnnotCategoryCompaMissing,
					compaAnnotCategoryCompaUnique);
			// alToReturn.add(alCompaECNumber);
			// compaAnnotPreRootToReturn.getChildrens().add(compaRootECNumber);

			getCompaOfThisAnnotType("Product", refGeneItem, listHomologGenes,
					compaAnnotCategoryCompaShared,
					compaAnnotCategoryCompaMissing,
					compaAnnotCategoryCompaUnique);
			
			getCompaOfThisAnnotType("Note", refGeneItem, listHomologGenes,
					compaAnnotCategoryCompaShared,
					compaAnnotCategoryCompaMissing,
					compaAnnotCategoryCompaUnique);
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodName,ex);

		} finally {
			//UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodName);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodName);
			}
		}// try
		return compaAnnotCategoryCompaToReturn;
	}
	

	@Override
	public ArrayList<CompaAnnotCategoryCompa> getCompaAnnotRefGenesWithGeneIdAndParams(
			Integer refGeneId, Double paramMaxEvalue,
			int paramMinPercentIdentity, int paramMinPercentAlignLenght,
			ArrayList<Integer> listComparedOrgaIdsSent,
			boolean paramBdbhOnlySent) throws Exception {

		Connection conn = null;
		
		try {
			conn = DatabaseConf.getConnection_db();

			return getCompaAnnotRefGenesWithGeneIdAndParams(conn, refGeneId,
					paramMaxEvalue, paramMinPercentIdentity,
					paramMinPercentAlignLenght, listComparedOrgaIdsSent,
					paramBdbhOnlySent);

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForHomoBrowResulImpl getComparaisonAnnotationWithGeneIdAndParams",ex);
			return null;
		} finally {
			DatabaseConf.freeConnection(conn, "CallForHomoBrowResulImpl getComparaisonAnnotationWithGeneIdAndParams");
		}// try
		

	}

	private void getCompaOfThisAnnotType(
			String typeOfAnnotation
			, GeneItem refGeneItem
			, ArrayList<GeneItem> listHomologGenes
			, CompaAnnotCategoryCompa compaAnnotCompaCategoShared
			, CompaAnnotCategoryCompa compaAnnotCompaCategoMissing
			, CompaAnnotCategoryCompa compaAnnotCompaCategoUnique
			) throws Exception {

		String methodNameToReport = "CallForHomoBrowResulImpl getCompaOfThisAnnotType"
				+ " ; typeOfAnnotation="+typeOfAnnotation
				+ " ; refGeneItem="+refGeneItem.stringifyToJSON(true, false)
				+ " ; listHomologGenes="+ ( (listHomologGenes != null ) ? listHomologGenes.toString() : "NULL" )
				+ " ; compaAnnotCompaCategoShared="+compaAnnotCompaCategoShared.toString()
				+ " ; compaAnnotCompaCategoMissing="+compaAnnotCompaCategoMissing.toString()
				+ " ; compaAnnotCompaCategoUnique="+compaAnnotCompaCategoUnique.toString()
				;
		
		// for each BiologicalProcess feature of the refGeneItem, compare it
		// with other homologs
		HashMap<String, HashMap<Integer, ArrayList<Integer>>> sharedAnnotString2HashMapOrgaIds2CompGeneIds = new HashMap<String, HashMap<Integer, ArrayList<Integer>>>();
		HashMap<String, HashMap<Integer, ArrayList<Integer>>> missingInRefAnnotString2HashMapOrgaIds2CompGeneIds = new HashMap<String, HashMap<Integer, ArrayList<Integer>>>();
		HashMap<String, HashMap<Integer, ArrayList<Integer>>> uniqueRefAnnotString2HashMapOrgaIds2CompGeneIds = new HashMap<String, HashMap<Integer, ArrayList<Integer>>>();

		RegExp pUniProtKb = RegExp.compile("^(.*)\\s\\{UniProtKB.+\\}$");
		RegExp pGO = RegExp.compile("^(.*)\\s\\{(GO\\:.+)\\}$");

		ArrayList<String> annotRef = null;
		if (typeOfAnnotation.compareTo("Molecular Function") == 0) {
			annotRef = refGeneItem.getListFunction();
		} else if (typeOfAnnotation.compareTo("Biological Process") == 0) {
			annotRef = refGeneItem.getListBiologicalProcess();
		} else if (typeOfAnnotation.compareTo("Cellular Component") == 0) {
			annotRef = refGeneItem.getListCellularComponent();
		} else if (typeOfAnnotation.compareTo("EC Number") == 0) {
			annotRef = refGeneItem.getListECNumber();
		} else if (typeOfAnnotation.compareTo("Product") == 0) {
			annotRef = refGeneItem.getListProduct();
		} else if (typeOfAnnotation.compareTo("Note") == 0) {
			annotRef = refGeneItem.getListNote();
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("typeOfAnnotation not found = "+typeOfAnnotation)
					);
			return;
		}
		
		if(annotRef == null){
			return;
		}

		for (int i = 0; i < annotRef.size(); i++) {
			String preAnnotTypeRefIT = annotRef.get(i);
			String annotTypeRefNameIT = preAnnotTypeRefIT;
			String annotTypeRefGOIT = "";
			MatchResult matcherPUniProtKb = pUniProtKb.exec(preAnnotTypeRefIT);
			if (matcherPUniProtKb != null) {
				annotTypeRefNameIT = matcherPUniProtKb.getGroup(1);
			}
			MatchResult matcherPGO = pGO.exec(preAnnotTypeRefIT);
			if (matcherPGO != null) {
				annotTypeRefNameIT = matcherPGO.getGroup(1);
				annotTypeRefGOIT = matcherPGO.getGroup(2);
			}
			if (annotTypeRefNameIT == null && annotTypeRefGOIT.isEmpty()) {
				continue;
			}

			for (int j = 0; j < listHomologGenes.size(); j++) {
				GeneItem compGeneItemIT = listHomologGenes.get(j);

				ArrayList<String> annotToCompare = null;
				if (typeOfAnnotation.compareTo("Molecular Function") == 0) {
					annotToCompare = compGeneItemIT.getListFunction();
				} else if (typeOfAnnotation.compareTo("Biological Process") == 0) {
					annotToCompare = compGeneItemIT.getListBiologicalProcess();
				} else if (typeOfAnnotation.compareTo("Cellular Component") == 0) {
					annotToCompare = compGeneItemIT.getListCellularComponent();
				} else if (typeOfAnnotation.compareTo("EC Number") == 0) {
					annotToCompare = compGeneItemIT.getListECNumber();
				} else if (typeOfAnnotation.compareTo("Product") == 0) {
					annotToCompare = compGeneItemIT.getListProduct();
				} else if (typeOfAnnotation.compareTo("Note") == 0) {
					annotToCompare = compGeneItemIT.getListNote();
				} else {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("typeOfAnnotation not found = "+typeOfAnnotation)
							);
					return;
				}
				
				if(annotToCompare == null){
					continue;
				}
				
				for (int k = 0; k < annotToCompare.size(); k++) {
					String preAnnotTypeCompIT = annotToCompare.get(k);
					String annotTypeCompNameIT = preAnnotTypeCompIT;
					String annotTypeCompGOIT = "";
					MatchResult matcherPUniProtKbComp = pUniProtKb
							.exec(preAnnotTypeCompIT);
					if (matcherPUniProtKbComp != null) {
						annotTypeCompNameIT = matcherPUniProtKbComp.getGroup(1);
					}
					MatchResult matcherPGOComp = pGO.exec(preAnnotTypeCompIT);
					if (matcherPGOComp != null) {
						annotTypeCompNameIT = matcherPGOComp.getGroup(1);
						annotTypeCompGOIT = matcherPGOComp.getGroup(2);
					}
					if (annotTypeCompNameIT == null
							&& annotTypeCompGOIT.isEmpty()) {
						continue;
					}

					boolean isMatch = false;
					if (annotTypeRefNameIT != null
							&& annotTypeCompNameIT != null) {
						if(!annotTypeRefNameIT.isEmpty()
								&& !annotTypeCompNameIT.isEmpty()){
									if (annotTypeRefNameIT.compareTo(annotTypeCompNameIT) == 0) {
										isMatch = true;
									}
						}
					}
					if (!isMatch) {
						//System.err.println("comparing "+annotTypeRefGOIT+" to "+annotTypeCompGOIT);
						if (annotTypeRefGOIT != null
								&& annotTypeCompGOIT != null) {
							if (!annotTypeRefGOIT.isEmpty()
									&& !annotTypeCompGOIT.isEmpty()) {
								if (annotTypeRefGOIT.compareTo(annotTypeCompGOIT) == 0) {
									//System.err.println("match!!!");
									isMatch = true;
								}
							}
						}
					}

					if (isMatch) {
						// sharedAnnotString2HashMapOrgaIds2CompGeneIds
						// System.err.println("similar BiologicalProcess ref et comp: "+biologicalProcessRefNameIT+" ; GO="+biologicalProcessCompGOIT);

						if (sharedAnnotString2HashMapOrgaIds2CompGeneIds
								.containsKey(annotTypeRefNameIT + " "
										+ annotTypeRefGOIT)) {
							// annot already in
							HashMap<Integer, ArrayList<Integer>> hashOrgaIds2CompGeneIds = sharedAnnotString2HashMapOrgaIds2CompGeneIds
									.get(annotTypeRefNameIT + " "
											+ annotTypeRefGOIT);
							if (hashOrgaIds2CompGeneIds
									.containsKey(compGeneItemIT.getOrganismId())) {
								// org id already in
								ArrayList<Integer> alGeneIds = hashOrgaIds2CompGeneIds
										.get(compGeneItemIT.getOrganismId());
								alGeneIds.add(compGeneItemIT.getGeneId());
							} else {
								// new orga
								ArrayList<Integer> alGeneIds = new ArrayList<Integer>();
								alGeneIds.add(compGeneItemIT.getGeneId());
								hashOrgaIds2CompGeneIds.put(
										compGeneItemIT.getOrganismId(),
										alGeneIds);
							}
						} else {
							// new annot to put in
							// sharedAnnotString2HashMapOrgaIds2CompGeneIds
							HashMap<Integer, ArrayList<Integer>> hashOrgaIds2CompGeneIds = new HashMap<Integer, ArrayList<Integer>>();
							ArrayList<Integer> alGeneIds = new ArrayList<Integer>();
							alGeneIds.add(compGeneItemIT.getGeneId());
							hashOrgaIds2CompGeneIds.put(
									compGeneItemIT.getOrganismId(), alGeneIds);
							sharedAnnotString2HashMapOrgaIds2CompGeneIds
									.put(annotTypeRefNameIT + " "
											+ annotTypeRefGOIT,
											hashOrgaIds2CompGeneIds);
						}

					} else {
						// System.err.println("not similar BiologicalProcess REF: "+biologicalProcessRefNameIT+" ; GO="+biologicalProcessRefGOIT+" ; COMP: "+biologicalProcessCompNameIT+" ; GO="+biologicalProcessCompGOIT);
						// do nothing
					}
				}
			}
		}

		// mark as uniqueRefAnnotString2HashMapOrgaIds2CompGeneIds from
		// refGeneItem.getListBiologicalProcess() that did not match
		for (int i = 0; i < annotRef.size(); i++) {
			String preAnnotTypeRefIT = annotRef.get(i);
			String annotTypeRefNameIT = preAnnotTypeRefIT;
			String annotTypeRefGOIT = "";
			MatchResult matcherPUniProtKb = pUniProtKb.exec(preAnnotTypeRefIT);
			if (matcherPUniProtKb != null) {
				annotTypeRefNameIT = matcherPUniProtKb.getGroup(1);
			}
			MatchResult matcherPGO = pGO.exec(preAnnotTypeRefIT);
			if (matcherPGO != null) {
				annotTypeRefNameIT = matcherPGO.getGroup(1);
				annotTypeRefGOIT = matcherPGO.getGroup(2);
			}
			if (annotTypeRefNameIT == null && annotTypeRefGOIT.isEmpty()) {
				continue;
			}
			if (!sharedAnnotString2HashMapOrgaIds2CompGeneIds
					.containsKey(annotTypeRefNameIT + " " + annotTypeRefGOIT)) {
				// new annot to put in
				// uniqueRefAnnotString2HashMapOrgaIds2CompGeneIds
				uniqueRefAnnotString2HashMapOrgaIds2CompGeneIds.put(
						annotTypeRefNameIT + " " + annotTypeRefGOIT, null);
			}
		}

		// mark as missingInRefAnnotString2HashMapOrgaIds2CompGeneIds from
		// compGeneItem.getListBiologicalProcess() that did match
		for (int j = 0; j < listHomologGenes.size(); j++) {
			GeneItem compGeneItemIT = listHomologGenes.get(j);

			ArrayList<String> annotToCompare = null;
			// 0 = getListFunction
			// 1 = getListBiologicalProcess
			// 2 = getListCellularComponent
			// 4 = getListECNumber
			if (typeOfAnnotation.compareTo("Molecular Function") == 0) {
				annotToCompare = compGeneItemIT.getListFunction();
			} else if (typeOfAnnotation.compareTo("Biological Process") == 0) {
				annotToCompare = compGeneItemIT.getListBiologicalProcess();
			} else if (typeOfAnnotation.compareTo("Cellular Component") == 0) {
				annotToCompare = compGeneItemIT.getListCellularComponent();
			} else if (typeOfAnnotation.compareTo("EC Number") == 0) {
				annotToCompare = compGeneItemIT.getListECNumber();
			} else if (typeOfAnnotation.compareTo("Product") == 0) {
				annotToCompare = compGeneItemIT.getListProduct();
			} else if (typeOfAnnotation.compareTo("Note") == 0) {
				annotToCompare = compGeneItemIT.getListNote();
			} else {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("typeOfAnnotation not found = "+typeOfAnnotation)
						);
				return;
			}
			
			if(annotToCompare == null){
				continue;
			}
			
			for (int k = 0; k < annotToCompare.size(); k++) {
				String preAnnotTypeCompIT = annotToCompare.get(k);
				String annotTypeCompNameIT = preAnnotTypeCompIT;
				String annotTypeCompGOIT = "";
				MatchResult matcherPUniProtKbComp = pUniProtKb
						.exec(preAnnotTypeCompIT);
				if (matcherPUniProtKbComp != null) {
					annotTypeCompNameIT = matcherPUniProtKbComp.getGroup(1);
				}
				MatchResult matcherPGOComp = pGO.exec(preAnnotTypeCompIT);
				if (matcherPGOComp != null) {
					annotTypeCompNameIT = matcherPGOComp.getGroup(1);
					annotTypeCompGOIT = matcherPGOComp.getGroup(2);
				}
				if (annotTypeCompNameIT == null && annotTypeCompGOIT.isEmpty()) {
					continue;
				}

				if (!sharedAnnotString2HashMapOrgaIds2CompGeneIds
						.containsKey(annotTypeCompNameIT + " "
								+ annotTypeCompGOIT)) {
					// new in missingInRefAnnotString2HashMapOrgaIds2CompGeneIds
					if (missingInRefAnnotString2HashMapOrgaIds2CompGeneIds
							.containsKey(annotTypeCompNameIT + " "
									+ annotTypeCompGOIT)) {
						// annot already in
						HashMap<Integer, ArrayList<Integer>> hashOrgaIds2CompGeneIds = missingInRefAnnotString2HashMapOrgaIds2CompGeneIds
								.get(annotTypeCompNameIT + " "
										+ annotTypeCompGOIT);
						if (hashOrgaIds2CompGeneIds.containsKey(compGeneItemIT
								.getOrganismId())) {
							// org id already in
							ArrayList<Integer> alGeneIds = hashOrgaIds2CompGeneIds
									.get(compGeneItemIT.getOrganismId());
							alGeneIds.add(compGeneItemIT.getGeneId());
						} else {
							// new orga
							ArrayList<Integer> alGeneIds = new ArrayList<Integer>();
							alGeneIds.add(compGeneItemIT.getGeneId());
							hashOrgaIds2CompGeneIds.put(
									compGeneItemIT.getOrganismId(), alGeneIds);
						}
					} else {
						// new annot to put in
						// sharedAnnotString2HashMapOrgaIds2CompGeneIds
						HashMap<Integer, ArrayList<Integer>> hashOrgaIds2CompGeneIds = new HashMap<Integer, ArrayList<Integer>>();
						ArrayList<Integer> alGeneIds = new ArrayList<Integer>();
						alGeneIds.add(compGeneItemIT.getGeneId());
						hashOrgaIds2CompGeneIds.put(
								compGeneItemIT.getOrganismId(), alGeneIds);
						missingInRefAnnotString2HashMapOrgaIds2CompGeneIds.put(
								annotTypeCompNameIT + " " + annotTypeCompGOIT,
								hashOrgaIds2CompGeneIds);
					}
				}
			}
		}

		turnHashMapIntoCompaAnnotFirstLevel(typeOfAnnotation,
				sharedAnnotString2HashMapOrgaIds2CompGeneIds,
				compaAnnotCompaCategoShared);
		// compaAnnotRootToReturn.getChildrens().add(compaAnnotFirstLevelShared);

		turnHashMapIntoCompaAnnotFirstLevel(typeOfAnnotation,
				missingInRefAnnotString2HashMapOrgaIds2CompGeneIds,
				compaAnnotCompaCategoMissing);
		// compaAnnotRootToReturn.getChildrens().add(compaAnnotFirstLevelMissing);

		turnHashMapIntoCompaAnnotFirstLevel(typeOfAnnotation,
				uniqueRefAnnotString2HashMapOrgaIds2CompGeneIds,
				compaAnnotCompaCategoUnique);
		// compaAnnotRootToReturn.getChildrens().add(compaAnnotFirstLevelUnique);

		// return compaAnnotRootToReturn;

	}

	private void turnHashMapIntoCompaAnnotFirstLevel(
			String typeOfAnnotation,
			HashMap<String, HashMap<Integer, ArrayList<Integer>>> AnnotString2HashMapOrgaIds2CompGeneIds,
			CompaAnnotCategoryCompa compaAnnotCategoryCompaIT) {

		// CompaAnnotFirstLevel compaAnnotFirstLevelToReturn = new
		// CompaAnnotFirstLevel();
		// compaAnnotFirstLevelToReturn.setTextAkaCategoryOfCompa(categoryOfCompa);

		CompaAnnotClasses compaAnnotClassesIT = new CompaAnnotClasses();
		compaAnnotClassesIT.setTextAkaAnnotationClass(typeOfAnnotation);

		// header
		CompaAnnotNames headerCompaAnnotName = new CompaAnnotNames();
		headerCompaAnnotName.setTextAkaAnnotationName("Gene annotations");
		compaAnnotClassesIT.getChildrens().add(headerCompaAnnotName);

		Set<Entry<String, HashMap<Integer, ArrayList<Integer>>>> entries = AnnotString2HashMapOrgaIds2CompGeneIds
				.entrySet();
		Iterator<Entry<String, HashMap<Integer, ArrayList<Integer>>>> it = entries
				.iterator();
		while (it.hasNext()) {
			Map.Entry<String, HashMap<Integer, ArrayList<Integer>>> entry = it
					.next();
			CompaAnnotNames compaAnnotSecondLevelIT = new CompaAnnotNames();
			compaAnnotSecondLevelIT.setTextAkaAnnotationName(entry.getKey());

			// header
			CompaAnnotOrganisms headerCompaAnnotThirdLevel = new CompaAnnotOrganisms();
			headerCompaAnnotThirdLevel.setReplacementString("Compared organisms");
			compaAnnotSecondLevelIT.getChildrens().add(
					headerCompaAnnotThirdLevel);

			if (entry.getValue() != null) {
				Set<Entry<Integer, ArrayList<Integer>>> subEntries = entry
						.getValue().entrySet();
				Iterator<Entry<Integer, ArrayList<Integer>>> subIt = subEntries
						.iterator();
				while (subIt.hasNext()) {
					Map.Entry<Integer, ArrayList<Integer>> subEntry = subIt
							.next();
					CompaAnnotOrganisms compaAnnotThirdLevelIT = new CompaAnnotOrganisms();
					compaAnnotThirdLevelIT.setOrgaId(subEntry.getKey());
					compaAnnotThirdLevelIT.setChildrensAkaGeneIds(subEntry
							.getValue());
					compaAnnotSecondLevelIT.getChildrens().add(
							compaAnnotThirdLevelIT);
				}

			} else {
				// Unique, no other orga listed
			}
			compaAnnotClassesIT.getChildrens().add(compaAnnotSecondLevelIT);
		}

		compaAnnotCategoryCompaIT.getChildrens().add(compaAnnotClassesIT);

	}

	@Override
	public ArrayList<CompaAnnotComparedGene> getAllCompaAnnotComparedGeneWithRefGeneIdAndListGeneIds(
			int currClickedRefGeneId, ArrayList<Integer> listGenesIds)
			throws Exception {

		String methodNameToReport = "CallForHomoBrowResuImpl getAllCompaAnnotComparedGeneWithRefGeneIdAndListGeneIds"
				+ " ; currClickedRefGeneId="+currClickedRefGeneId
				+ " ; listGenesIds=" + ( (listGenesIds != null ) ? listGenesIds.toString() : "NULL" )
				;

		
		if (listGenesIds.isEmpty()) {
			return new ArrayList<CompaAnnotComparedGene>();
		}

		ArrayList<CompaAnnotComparedGene> arrayListCACG = new ArrayList<CompaAnnotComparedGene>();

		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();

			ArrayList<LightGeneItem> alLGI = QueriesTableGenes.getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(conn, listGenesIds, true);
			for (int i = 0; i < alLGI.size(); i++) {
				LightGeneItem lgiIT = alLGI.get(i);
				ArrayList<LightGeneMatchItem> alLGMIIT = QueriesTableHomologs.getLightGeneMatchItemWithQGeneIdAndSGeneId(
						conn
						, currClickedRefGeneId
						, lgiIT.getGeneId()
						, false
						);
				CompaAnnotComparedGene cacgIT = new CompaAnnotComparedGene();
				cacgIT.setAlAlignmentDataWithRefGene(alLGMIIT);
				cacgIT.setComparedGeneId(lgiIT.getGeneId());
				cacgIT.setTextAkaComparedGeneIdentifier(lgiIT
						.getMostSignificantGeneNameAsStrippedText()
						+ " ["
						+ lgiIT.getLocusTagAsStrippedString() + "]");
				arrayListCACG.add(cacgIT);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);

		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
			
		}// try

		return arrayListCACG;

	}

	
	@Override
	public String exportToEmailHomoBroTable(
			String emailAddress
			, ArrayList<Integer> listReferenceGeneSetForHomologsTable
			, int referenceOrgaId
			, HashSet<Integer> hsFeaturedOrganismIds 
			, HashSet<Integer> hsMainListOrganismIds 
			//, EnumResultListSortScopeType sortScopeType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			, boolean summaryConservationData
			, String fieldDelimiter
			)
			throws Exception {
		
		new NewThreadMasterExportToEmailHomoBroTable(
				emailAddress
				, listReferenceGeneSetForHomologsTable
				, referenceOrgaId
				, hsFeaturedOrganismIds
				, hsMainListOrganismIds
				//, sortScopeType
				, sortScopeType_scope
				, sortScopeType_sortType
				, sortScopeType_sortOrder
				, summaryConservationData
				, fieldDelimiter
				); // create a new thread
		return emailAddress;
		
	}
	
	// Create a new master thread for compa annot.
	class NewThreadMasterExportToEmailHomoBroTable implements
			Runnable {
		Thread t;
		String emailAddress;
		ArrayList<Integer> listReferenceGeneSetForHomologsTable;
		int referenceOrgaId;
		HashSet<Integer> hsFeaturedOrganismIds;
		HashSet<Integer> hsMainListOrganismIds;
		//EnumResultListSortScopeType sortScopeType;
		SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope;
		SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType;
		SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder;
		boolean summaryConservationData;
		String fieldDelimiter;
		
		NewThreadMasterExportToEmailHomoBroTable(
				String emailAddressSent
				, ArrayList<Integer> listReferenceGeneSetForHomologsTableSent
				, int referenceOrgaIdSent
				, HashSet<Integer> hsFeaturedOrganismIdsSent 
				, HashSet<Integer> hsMainListOrganismIdsSent 
				//, EnumResultListSortScopeType sortScopeTypeSent
				, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scopeSent
				, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortTypeSent
				, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrderSent
				, boolean summaryConservationDataSent
				, String fieldDelimiterSent
				) {
			
			// Create a new, second thread
			t = new Thread(this, "Master Compa Annot Thread");
			// System.out.println("Child thread: " + t);
			emailAddress = emailAddressSent;
			listReferenceGeneSetForHomologsTable = listReferenceGeneSetForHomologsTableSent;
			referenceOrgaId = referenceOrgaIdSent;
			hsFeaturedOrganismIds = hsFeaturedOrganismIdsSent;
			hsMainListOrganismIds = hsMainListOrganismIdsSent;
			//sortScopeType = sortScopeTypeSent;
			sortScopeType_scope = sortScopeType_scopeSent;
			sortScopeType_sortType = sortScopeType_sortTypeSent;
			sortScopeType_sortOrder = sortScopeType_sortOrderSent;
			summaryConservationData = summaryConservationDataSent;
			fieldDelimiter = fieldDelimiterSent;
			t.start(); // Start the thread
		}

		// This is the entry point for the second thread.
		public void run() {
			

			//here
			
//			System.err.println(emailAddress);
//			System.err.println(listReferenceGeneSetForHomologsTable.toString());
//			System.err.println(referenceOrgaId);
//			System.err.println(hsFeaturedOrganismIds.toString());
//			System.err.println(hsMainListOrganismIds.toString());
//			System.err.println(sortScopeType_scope.toString());
//			System.err.println(sortScopeType_sortType.toString());
//			System.err.println(sortScopeType_sortOrder.toString());
//			System.err.println(summaryConservationData);
			
			String bodyToReturn = "";
			String headerToReturn = "";
			Connection conn = null;
			
			
			try {
				conn = DatabaseConf.getConnection_db();

				if ( ! summaryConservationData) {
					//header
					headerToReturn += "The layout of this table is a reference gene per column and a compared organism per line."
							+ " The number shown in each cell represents the number of homologs for a given combination \"reference gene / compared organism\"."
							+ "\n";

					ArrayList<LightOrganismItem> listPublicOrgaResultScopeGeneSet = getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
							conn
							, referenceOrgaId
							, listReferenceGeneSetForHomologsTable
							, null //alignmentParametersForSynteny can be null as it is never used
							, new ArrayList<LightOrganismItem>() // listFeaturedOrga,
							, new ArrayList<LightOrganismItem>() // listExcludedOrga,
							//, sortScopeType
							//, sortScopeType_scope
							, sortScopeType_sortType
							, sortScopeType_sortOrder
							);
					
					//print for ref orga
					//LightOrganismItem refLoi = CallForInfoDBImpl.getLightOrganismItemWithOrgaId(conn, referenceOrgaId);
					LightOrganismItem refLoi = QueriesTableOrganisms.getLightOrganismItemWithOrgaId(
							conn
							, referenceOrgaId
							, false
							, true
					);
					
					
					//print column 1: orga full name
					bodyToReturn += refLoi.getFullName()+fieldDelimiter;
					//print subsequent column: gene name
					for (int geneIdIT : listReferenceGeneSetForHomologsTable){
						//for each gene, print its name or locus tag
						LightGeneItem lgiIT = QueriesTableGenes.getLightGeneItemWithGeneId(conn, geneIdIT);
						bodyToReturn += lgiIT.getMostSignificantGeneNameAsStrippedText()+fieldDelimiter;
					}
					//next line
					bodyToReturn += "\n";
					
					//for each orga, print the line
					

					/*ArrayList<AbsoPropQElemItem> listAbsoluteProportionElementItemQ = new ArrayList<AbsoPropQElemItem>();
					ArrayList<Integer> listElementIdIT = QueriesTableElements
							.getAlElementIdWithOrganismId_optionalOrderBySizeDesc(conn, referenceOrgaId, true, true);
					for (int eletIdIT : listElementIdIT){
						AbsoPropQElemItem apqeIIT = new AbsoPropQElemItem();
						apqeIIT.setqOrigamiElementId(eletIdIT);
						listAbsoluteProportionElementItemQ.add(apqeIIT);
					}*/
					//RQ : removed sort elment by size in the new way of dealing with getTransientAbsoluteProportionResultGeneSetHolderWith..., should be no big deal
					
					for (LightOrganismItem loiIT : listPublicOrgaResultScopeGeneSet){
						
						//print column 1: orga full name
						bodyToReturn += loiIT.getFullName();
						if (loiIT.getPositionInResultList() > 0) {
							bodyToReturn +=  " #"+loiIT.getPositionInResultList()+")";
						}
						if (loiIT.getScore() > 0) {
							bodyToReturn +=  " (s="+loiIT.getScore()+")";
						}
						bodyToReturn += fieldDelimiter;
						
						TransAbsoPropResuGeneSetHold taprgsh = getTransientAbsoluteProportionType1Or2ResultGeneSetHolderWithOrgaIdAndSOrgaIdAndALQGeneIds(
								//listAbsoluteProportionElementItemQ
								referenceOrgaId
								, loiIT.getOrganismId()
							 	//, null //alignmentParametersForSynteny can be null as it is never used
								, listReferenceGeneSetForHomologsTable
								, false
								);
						HashMap<Integer, Integer> hashcountOrtho = new HashMap<Integer, Integer>();
						for (TransAbsoPropResuGeneSet taprgsIT : taprgsh.getAlAPGeneSet() ){
								if(hashcountOrtho.containsKey(taprgsIT.getqGeneId())){
									if(taprgsIT.getsGeneId() >= 0){
										int currVal = hashcountOrtho.get(taprgsIT.getqGeneId());
										currVal++;
										hashcountOrtho.put(taprgsIT.getqGeneId(), currVal);
									}
								} else {
									if(taprgsIT.getsGeneId() >= 0){
										hashcountOrtho.put(taprgsIT.getqGeneId(), 1);
									}
								}
							
						}
						for (int qGeneIDIT : listReferenceGeneSetForHomologsTable ){
							if (hashcountOrtho.containsKey(qGeneIDIT)) {
								bodyToReturn += hashcountOrtho.get(qGeneIDIT)+"\t";
							} else {
								bodyToReturn += fieldDelimiter;
							}
						}
						//next line
						bodyToReturn += "\n";
					}
				} else {

					//TODO export via not email
					headerToReturn += "Summary conservation statistics for "+listReferenceGeneSetForHomologsTable.size()
				    		//+ " (gene id = "+qGeneIdIT+" ; organism id = "+qOrganismIdIT+")"
				    		+ " reference CDS." + System.getProperty("line.separator");
					headerToReturn += "Comparison with "+hsFeaturedOrganismIds.size()+" genomes from the featured list." + System.getProperty("line.separator");
					headerToReturn += "Comparison with "+hsMainListOrganismIds.size()+" genomes from the main list." + System.getProperty("line.separator");
					headerToReturn += "Gene_id"
							+ fieldDelimiter+"Locus_tag"
							//+ fieldDelimiter+"Gene_name"
							+ fieldDelimiter+"Protein_id"
							+ fieldDelimiter+"List_products"
							+ fieldDelimiter+"Length_residues"
							+ fieldDelimiter+"Gene_percent_GC"
							+ fieldDelimiter+"Accession"
							+ fieldDelimiter+"Organism_id"
							+ fieldDelimiter+"Number_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_featuredGenomes"
							+ fieldDelimiter+"Percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_featuredGenomes"
							+ fieldDelimiter+"Number_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_featuredGenomes"
							+ fieldDelimiter+"Percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_featuredGenomes"
							+ fieldDelimiter+"Average_percent_identity_alignment_featuredGenomes"
							+ fieldDelimiter+"Average_percent_query_and_subject_lenght_coverage_alignment_featuredGenomes"
							+ fieldDelimiter+"Median_Evalue_alignment_featuredGenomes"
							+ fieldDelimiter+"Number_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_mainGenomesList"
							+ fieldDelimiter+"Percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_mainGenomesList"
							+ fieldDelimiter+"Number_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_mainGenomesList"
							+ fieldDelimiter+"Percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_mainGenomesList"
							+ fieldDelimiter+"Average_percent_identity_alignment_mainGenomesList"
							+ fieldDelimiter+"Average_percent_query_and_subject_lenght_coverage_alignment_mainGenomesList"
							+ fieldDelimiter+"Median_Evalue_alignment_mainGenomesList"
							;
					
					for (int qGeneIdIT : listReferenceGeneSetForHomologsTable) {

						GeneItem geneItemIT = QueriesTableGenes.getGeneItemWithGeneId(conn, qGeneIdIT);
						String accnumIT = "";
						if (geneItemIT.getAccession() != null && ! geneItemIT.getAccession().isEmpty()) {
							accnumIT = geneItemIT.getAccession();
						} else {
							if (geneItemIT.getElementId() > 0) {
								accnumIT = QueriesTableElements.getAccessionWithElementId(conn, geneItemIT.getElementId(), true);
							}
						}
						
						HashSet<Integer> hsQGeneIdIT = new HashSet<>();
						hsQGeneIdIT.add(qGeneIdIT);


						HashSet<Long> hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList = new HashSet<>();
						if ( ! hsFeaturedOrganismIds.isEmpty()) {
							hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList = QueriesTableAlignmentParams.getHsAlignmentParamId_withQOrganismIdAndHsSOrganismId(
									conn
									, referenceOrgaId
									, hsFeaturedOrganismIds
									, false //isMirrorRequest
									);
						}


						//							HashSet<Long> HsAlignmentIdRelatedToQOrganismIdITAndFeaturedGenomesList = 
						//									QueriesTableAlignments.getListAlignmentIdsWithAlAlignmentParamId(
						//											conn
						//											, hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList
						//											, true //convertSetLongToSortedRangeItem_allowGapWithinRange
						//											, false //shouldBePresentInTable
						//											);

						HashSet<Integer> hsTypesIT = new HashSet<>();
						hsTypesIT.add(1);
						hsTypesIT.add(2);

						HashMap<Integer, HashSet<Long>> hmQGeneId2HsAlignmentId = 
								QueriesTableAlignmentPairs.getHmQGeneId2HsAlignmentId_withHsGeneId(
										conn
										, hsQGeneIdIT
										, hsTypesIT
										, false //isMirrorRequest
										//, true //convertSetIntegerToSortedRangeItem_allowGapWithinRange
										//, hsOrgaIdsFeaturedGenomes
										);
						HashSet<Long> consolidatedHsAlignmentIdForAllQueryGenes = new HashSet<>();
						for (Map.Entry<Integer, HashSet<Long>> entryHmQGeneId2 : hmQGeneId2HsAlignmentId.entrySet()) {
							//int qGeneIdIT = entryHmQGeneId2.getKey();
							//HashSet<Long> HsAlignmentIdIT = entryHmQGeneId2.getValue();
							consolidatedHsAlignmentIdForAllQueryGenes.addAll(entryHmQGeneId2.getValue());
						}


						//System.err.println("consolidatedHsAlignmentIdForAllQueryGenes="+consolidatedHsAlignmentIdForAllQueryGenes.size());

						//al idx0 = consolidatedHsAlignmentParamsIdForAllQueryGenes_singleton
						//al idx0 = consolidatedHsAlignmentParamsIdForAllQueryGenes_synteny
						ArrayList<HashSet<Long>> alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1 = 
								//HashSet<Long> consolidatedHsAlignmentParamsIdForAllQueryGenes = 
								QueriesTableAlignments.getAlHsAlignmentParamId_SingletonIdx0SyntenyIdx1_withHsAlignmentId(
										conn
										, consolidatedHsAlignmentIdForAllQueryGenes
										, true // convertSetLongToSortedRangeItem_allowGapWithinRange
										, false // shouldBePresentInTable
										);

						//System.err.println("consolidatedHsAlignmentParamsIdForAllQueryGenes="+consolidatedHsAlignmentParamsIdForAllQueryGenes.size());
						HashSet<Long> alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1.get(0));
						alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all.addAll(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1.get(1));
						HashSet<Long> alConsolidatedHsAlignmentParamsIdForAllQueryGenes_syntenyOnly = alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1.get(1);

						HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all);
						HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all);
						hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.retainAll(hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList);
						hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.removeAll(hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList);

						int featuredGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.size();
						int mainGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.size();

						HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_syntenyOnly);
						HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_mainGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_syntenyOnly);
						hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList.retainAll(hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList);
						hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_mainGenomesList.removeAll(hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList);

						int featuredGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship = hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList.size();
						int mainGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship = hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_mainGenomesList.size();



						Double featuredGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution = 0D;
						Double featuredGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = 0D;
						Double featuredGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution = 0D;
						if ( ! hsFeaturedOrganismIds.isEmpty()) {
							// idx 0 : Avg % identity of alignments
							// idx 1 : Stdev % identity of alignments
							// idx 2 : Avg % query and subject lenght coverage of alignments
							// idx 3 : Stdev % query and subject lenght coverage of alignments
							// idx 4 : Median Evalue of alignments
							// idx 5 : Min Evalue of alignments
							// idx 6 : Max Evalue of alignments
							ArrayList<Double> alAvgStdevBasicAlignementStats_featuredGenomesList = 
									QueriesTableHomologs.getAvgStdevBasicAlignementStats_withHsQGeneId_optionalHsSOrgaIdsAndRank(
											conn
											, hsQGeneIdIT
											, hsFeaturedOrganismIds
											, null //hsRank
											, false //calculateStdev
											, false //calculateMinMaxEvalue
											//, false //isMirrorRequest
											);
							featuredGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(0);
							//Double featuredGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(1);
							featuredGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(2);
							//Double featuredGenomesList_stdevQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(3);
							featuredGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(4);
							//Double featuredGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(5);
							//Double featuredGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(6);

						}


						// idx 0 : Avg % identity of alignments
						// idx 1 : Stdev % identity of alignments
						// idx 2 : Avg % query and subject lenght coverage of alignments
						// idx 3 : Stdev % query and subject lenght coverage of alignments
						// idx 4 : Median Evalue of alignments
						// idx 5 : Min Evalue of alignments
						// idx 6 : Max Evalue of alignments
						ArrayList<Double> alAvgStdevBasicAlignementStats_mainGenomesList = 
								QueriesTableHomologs.getAvgStdevBasicAlignementStats_withHsQGeneId_optionalHsSOrgaIdsAndRank(
										conn
										, hsQGeneIdIT
										, hsMainListOrganismIds
										, null //hsRank
										, false //calculateStdev
										, false //calculateMinMaxEvalue
										//, false //isMirrorRequest
										);
						Double mainGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(0);
						//Double mainGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(1);
						Double mainGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(2);
						//Double mainGenomesList_stdevQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(3);
						Double mainGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(4);
						//Double mainGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(5);
						//Double mainGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(6);


						String nucSequencesHsGenes = QueriesTableProtAndNucSequences.getNucleicAcidSequenceAsFastaWithAlGeneIds(
								conn
								, hsQGeneIdIT
								, false //withHeader
								, false //lineSeparatorInSeq
								);
						int countSizeSeq = nucSequencesHsGenes.length();
						int countG = StringUtils.countMatches(nucSequencesHsGenes, "G");
						int countC = StringUtils.countMatches(nucSequencesHsGenes, "C");
						int countg = StringUtils.countMatches(nucSequencesHsGenes, "g");
						int countc = StringUtils.countMatches(nucSequencesHsGenes, "c");
						int countGC = countG + countC + countg + countc;
//						System.err.println(nucSequencesHsGenes);
//						System.err.println("countG="+countG);
//						System.err.println("countC="+countC);
//						System.err.println("countGC="+countGC);
//						System.err.println("nucSequencesHsGenes.length()="+nucSequencesHsGenes.length());
						
						Double percentGCrefCDS = (double) ( ( (double) countGC / (double) countSizeSeq ) * (double) 100 ) ;

						DecimalFormat df = new DecimalFormat("##.##");
						
//						headerToReturn += "Gene_id"
//								+ fieldDelimiter+"Locus_tag"
//								//+ fieldDelimiter+"Gene_name"
//								+ fieldDelimiter+"Protein_id"
//								+ fieldDelimiter+"List_products"
//								+ fieldDelimiter+"Length_residues"
//								+ fieldDelimiter+"Gene_percent_GC"
//								+ fieldDelimiter+"Accession"
//								+ fieldDelimiter+"Organism_id"
//								+ fieldDelimiter+"Number_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_featuredGenomes"
//								+ fieldDelimiter+"Percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_featuredGenomes"
//								+ fieldDelimiter+"Number_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_featuredGenomes"
//								+ fieldDelimiter+"Percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_featuredGenomes"
//								+ fieldDelimiter+"Average_percent_identity_alignment_featuredGenomes"
//								+ fieldDelimiter+"Average_percent_query_and_subject_lenght_coverage_alignment_featuredGenomes"
//								+ fieldDelimiter+"Median_Evalue_alignment_featuredGenomes"
//								+ fieldDelimiter+"Number_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_mainGenomesList"
//								+ fieldDelimiter+"Percent_genomes_with_presence_of_at_least_1_CDS_conserved_during_evolution_mainGenomesList"
//								+ fieldDelimiter+"Number_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_mainGenomesList"
//								+ fieldDelimiter+"Percent_genomes_with_presence_of_at_least_1_CDS_in_synteny_relationship_mainGenomesList"
//								+ fieldDelimiter+"Average_percent_identity_alignment_mainGenomesList"
//								+ fieldDelimiter+"Average_percent_query_and_subject_lenght_coverage_alignment_mainGenomesList"
//								+ fieldDelimiter+"Median_Evalue_alignment_mainGenomesList"
//								;

						bodyToReturn += System.getProperty("line.separator");
						bodyToReturn += 
								qGeneIdIT
								+ fieldDelimiter + geneItemIT.getLocusTagAsStrippedString()
								//+ fieldDelimiter + geneItemIT.getNameAsStrippedString()
								+ fieldDelimiter + geneItemIT.getListProteinId().toString()
								+ fieldDelimiter + geneItemIT.getListProduct().toString()
								+ fieldDelimiter + geneItemIT.getLengthResidues()
								+ fieldDelimiter + df.format(percentGCrefCDS)
								+ fieldDelimiter + accnumIT
								+ fieldDelimiter + referenceOrgaId
								;

						if ( ! hsFeaturedOrganismIds.isEmpty()) {
							bodyToReturn += fieldDelimiter + featuredGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution;
							double featuredGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = ( (double) featuredGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution / (double) hsFeaturedOrganismIds.size() ) * (double) 100;
							bodyToReturn += fieldDelimiter + df.format(featuredGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution);
							bodyToReturn += fieldDelimiter + featuredGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship;
							double featuredGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = ( (double) featuredGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship / (double) hsFeaturedOrganismIds.size() ) * (double) 100;
							bodyToReturn += fieldDelimiter + df.format(featuredGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship);
							bodyToReturn += fieldDelimiter + df.format(featuredGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution);
							bodyToReturn += fieldDelimiter + df.format(featuredGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution);
							bodyToReturn += fieldDelimiter + featuredGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution;
						} else {
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
						}

						if ( ! hsMainListOrganismIds.isEmpty()) {
							bodyToReturn += fieldDelimiter + mainGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution;
							double mainGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = ( (double) mainGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution / (double) hsMainListOrganismIds.size() ) * (double) 100;
							bodyToReturn += fieldDelimiter + df.format(mainGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution);
							bodyToReturn += fieldDelimiter + mainGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship;
							double mainGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = ( (double) mainGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship / (double) hsMainListOrganismIds.size() ) * (double) 100;
							bodyToReturn += fieldDelimiter + df.format(mainGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship);
							bodyToReturn += fieldDelimiter + df.format(mainGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution);
							bodyToReturn += fieldDelimiter + df.format(mainGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution);
							bodyToReturn += fieldDelimiter + mainGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution;
						}else {
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
							bodyToReturn += fieldDelimiter + "0";
						}

					} // for (int qGeneIdIT : listReferenceGeneSetForHomologsTable) {
					
					
				} //else summaryConservationData
				
				
				String resultString = headerToReturn + bodyToReturn;
				
				//
				//System.err.println(resultString);
				
				sendEmail(emailAddress, resultString);
				
			} catch (InterruptedException e) {
				System.err
						.println("Problem in CallForHomoBrowResulImpl NewThreadMasterExportToEmailHomoBroTable: Child interrupted");
				bodyToReturn = "Problem in CallForHomoBrowResulImpl NewThreadMasterExportToEmailHomoBroTable: Child interrupted";
				sendEmail(emailAddress, bodyToReturn);
			} catch (Exception e) {
				// probleme
				System.err.println("Problem in CallForHomoBrowResulImpl NewThreadMasterExportToEmailHomoBroTable");
				System.err.println(e + "\n" + e.getMessage());
				e.printStackTrace();
				bodyToReturn = "Problem in CallForHomoBrowResulImpl NewThreadMasterExportToEmailHomoBroTable";
				bodyToReturn += e + "\n" + e.getMessage();
				sendEmail(emailAddress, bodyToReturn);
			} finally {
				DatabaseConf.freeConnection(conn, "CallForHomoBrowResulImpl NewThreadMasterExportToEmailHomoBroTable");
			}// try

		}
	}
	
	@Override
	public String exportToEmailCompaAnnotRefGenesWithListGeneIdAndParams(
			String emailAddress,
			RefGeneSetForAnnotCompa referenceGeneSetForAnnotationsComparatorSent,
			Double paramMaxEvalueSent, int paramMinPercentIdentitySent,
			int paramMinPercentAlignLenghtSent,
			ArrayList<Integer> alOrgaIdsSent, boolean paramBdbhOnlySent)
			throws Exception {

		new NewThreadMasterCompaAnnotRefGenesWithListGeneIdAndParams(
				emailAddress, referenceGeneSetForAnnotationsComparatorSent, paramMaxEvalueSent,
				paramMinPercentIdentitySent, paramMinPercentAlignLenghtSent,
				alOrgaIdsSent, paramBdbhOnlySent); // create a new thread
		return emailAddress;

	}

	// Create a new master thread for compa annot.
	class NewThreadMasterCompaAnnotRefGenesWithListGeneIdAndParams implements Runnable {
		Thread t;
		String emailAddress;
		RefGeneSetForAnnotCompa refGeneSetForAnnotCompa;
		Double paramMaxEvalue;
		int paramMinPercentIdentity;
		int paramMinPercentAlignLenght;
		ArrayList<Integer> alOrgaIds;
		boolean paramBdbhOnly;

		NewThreadMasterCompaAnnotRefGenesWithListGeneIdAndParams(
				String emailAddressSent, RefGeneSetForAnnotCompa referenceGeneSetForAnnotationsComparatorSent,
				Double paramMaxEvalueSent, int paramMinPercentIdentitySent,
				int paramMinPercentAlignLenghtSent,
				ArrayList<Integer> alOrgaIdsSent, boolean paramBdbhOnlySent) {
			// Create a new, second thread
			t = new Thread(this, "Master Compa Annot Thread");
			// System.out.println("Child thread: " + t);
			emailAddress = emailAddressSent;
			refGeneSetForAnnotCompa = referenceGeneSetForAnnotationsComparatorSent;
			paramMaxEvalue = paramMaxEvalueSent;
			paramMinPercentIdentity = paramMinPercentIdentitySent;
			paramMinPercentAlignLenght = paramMinPercentAlignLenghtSent;
			alOrgaIds = alOrgaIdsSent;
			paramBdbhOnly = paramBdbhOnlySent;
			t.start(); // Start the thread
		}

		// This is the entry point for the second thread.
		public void run() {
			String csvToSend = "";
			Connection conn = null;
			try {
				conn = DatabaseConf.getConnection_db();

				// for (int i = 0; i < refAlGeneId.size(); i++) {
				// System.out.println("Child Thread: " + i);
				// // Let the thread sleep for a while.
				// Thread.sleep(500);
				// }
				HashMap<Integer, ArrayList<CompaAnnotCategoryCompa>> rootAlAlCACC = new HashMap<Integer, ArrayList<CompaAnnotCategoryCompa>>();

				ArrayList<ArrayList<Integer>> refAlAlGeneId = getMostSignificantAlGeneIdsWithReferenceGeneSetForAnnotationsComparator(
						conn, refGeneSetForAnnotCompa);

				ArrayList<Integer> refAlGeneId = new ArrayList<Integer>();
				for(ArrayList<Integer> refAlGeneIdIT : refAlAlGeneId){
					refAlGeneId.addAll(refAlGeneIdIT);
				}
				
				for (int i = 0; i < refAlGeneId.size(); i++) {
					rootAlAlCACC.put(
							refAlGeneId.get(i),
							getCompaAnnotRefGenesWithGeneIdAndParams(conn,
									refAlGeneId.get(i), paramMaxEvalue,
									paramMinPercentIdentity,
									paramMinPercentAlignLenght, alOrgaIds,
									paramBdbhOnly));
				}
				int listOrgaSize = alOrgaIds.size();
				if (listOrgaSize == 0) {
					listOrgaSize = QueriesTableOrganisms.getCountOrganisms(
							conn
							, null
							, null
							, null
							, false
							, true
							, true
							);
					//listOrgaSize = CallForInfoDBImpl.getAllPublicLightOrganismItem(conn).size();
				}
				csvToSend = convertRootAlCACCIntoStringCsv(conn, rootAlAlCACC, listOrgaSize);

				sendEmail(emailAddress, csvToSend);

			} catch (InterruptedException e) {
				System.err.println("Problem in CallForHomoBrowResulImpl NewThreadMasterCompaAnnotRefGenesWithListGeneIdAndParams: Child interrupted");
				csvToSend = "Problem in NewThreadMasterCompaAnnotRefGenesWithListGeneIdAndParams: Child interrupted";
				sendEmail(emailAddress, csvToSend);
			} catch (Exception e) {
				// probleme
				System.err
						.println("Problem in CallForHomoBrowResulImpl NewThreadMasterCompaAnnotRefGenesWithListGeneIdAndParams");
				System.err.println(e + "\n" + e.getMessage());
				e.printStackTrace();
				// throw new Exception(e + "\n" + e.getMessage());
				csvToSend = "Problem in CallForHomoBrowResulImpl NewThreadMasterCompaAnnotRefGenesWithListGeneIdAndParams";
				csvToSend += e + "\n" + e.getMessage();
				sendEmail(emailAddress, csvToSend);
			} finally {
				DatabaseConf.freeConnection(conn, "CallForHomoBrowResulImpl NewThreadMasterCompaAnnotRefGenesWithListGeneIdAndParams");
			}// try

			// System.out.println("Exiting child thread.");
		}
	}

	public void sendEmail(String emailAddress, String csvToSend) {
		// final String username = "your.mail.id@gmail.com";
		// final String password = "your.password";
		// Properties props = new Properties();
		// props.put("mail.smtp.auth", true);
		// props.put("mail.smtp.starttls.enable", true);
		// props.put("mail.smtp.host", "smtp.gmail.com");
		// props.put("mail.smtp.port", "587");

		// send email
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", "smtp.jouy.inra.fr");
		// props.setProperty("mail.host", "jouy.inra.fr");
		// props.setProperty("mail.user", "thomas.lacroix");
		props.setProperty("mail.user", "insyght");
		props.setProperty("mail.password", "");

		// Session session = Session.getInstance(props,
		// new javax.mail.Authenticator() {
		// protected PasswordAuthentication getPasswordAuthentication() {
		// return new PasswordAuthentication(username, password);
		// }
		// });

		Session session = Session.getDefaultInstance(props, null);

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("insyght@jouy.inra.fr"));// from.mail.id@gmail.com
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailAddress));// "to.mail.id@gmail.com"
			message.setSubject("[insyght] Your exported table is here");
			// message.setText("PFA");

			Multipart multipart = new MimeMultipart();

			MimeBodyPart contentBodyPart = new MimeBodyPart();
			contentBodyPart
					.setText("Dear user,\nPlease find enclosed the file you requested.\nPlease let us know if there is any problem.\nBest wishes,\nThe insyght team");
			multipart.addBodyPart(contentBodyPart);

			MimeBodyPart attachementBodyPart = new MimeBodyPart();
			// MimeBodyPart(java.io.InputStream is)
			// String file =
			// "C:\\Users\\thomas\\Desktop\\LEFTOVER_use_case.docx";
			// DataSource source = new FileDataSource(file);
			DataSource source = new ByteArrayDataSource(csvToSend, "text/csv");
			// DataSource source = new ByteArrayDataSource(csvToSend,
			// "text/csv");
			attachementBodyPart.setDataHandler(new DataHandler(source));
			attachementBodyPart.setFileName("insyght_data.csv");
			multipart.addBodyPart(attachementBodyPart);

			message.setContent(multipart);
			// System.out.println("Sending");
			Transport.send(message);
			// System.out.println("Done");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String convertRootAlCACCIntoStringCsv(Connection conn,
			HashMap<Integer, ArrayList<CompaAnnotCategoryCompa>> rootAlAlCACC,
			int totalNumberOfOrganismsAnalysed) throws Exception {

		// use of StringBuilder, but need to use StringBuffer si multiple
		// threads
		StringBuilder sb = new StringBuilder(
				"\"Reference gene\","
						+ "\"Reference gene start\","
						+ "\"Reference gene stop\","
						+ "\"Annotation category\","
						+ "\"Annotation class\","
						+ "\"Annotation name\","
						+ "\"Number of compared organisms that have at least one homolog with a similar annotation\","
						+ "\"Total number of compared organisms analysed\","
						+ "\"Ratio number of compared organisms that have at least one homolog with a similar annotation by total number of compared organisms analysed\""
						+ "\n");
		Iterator<Entry<Integer, ArrayList<CompaAnnotCategoryCompa>>> it = rootAlAlCACC
				.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Integer, ArrayList<CompaAnnotCategoryCompa>> pairs = it
					.next();
			// System.out.println(pairs.getKey() + " = " + pairs.getValue());
			// new gene
			GeneItem giIT = QueriesTableGenes.getGeneItemWithGeneId(conn, pairs.getKey());
			for (CompaAnnotCategoryCompa caccIT : pairs.getValue()) {
				for (CompaAnnotClasses cacIT : caccIT.getChildrens()) {
					for (CompaAnnotNames canIT : cacIT.getChildrens()) {
						if(canIT.getChildrens().isEmpty()){
							continue;
						}
						if (canIT.getTextAkaAnnotationName().compareTo(
								"Annotations") == 0) {
							continue;
						}
						double ratio = (double) ((double) (canIT.getChildrens()
								.size() - 1) / (double) totalNumberOfOrganismsAnalysed);
						NumberFormat percentFormat = NumberFormat
								.getPercentInstance();
						percentFormat.setMaximumFractionDigits(3);
						String resultRatio = percentFormat.format(ratio);
						sb.append("\""
								+ giIT.getMostSignificantGeneNameAsStrippedText()
								+ " [" + giIT.getLocusTagAsStrippedString()
								+ "]" + "\",\"" + giIT.getStart() + "\",\""
								+ giIT.getStop() + "\",\""
								+ caccIT.getTextAkaCategoryOfCompa() + "\",\""
								+ cacIT.getTextAkaAnnotationClass() + "\",\""
								+ canIT.getTextAkaAnnotationName() + "\",\""
								+ (canIT.getChildrens().size() - 1) + "\",\""
								+ totalNumberOfOrganismsAnalysed + "\",\""
								+ resultRatio + "\"\n");
					}
				}
			}
		}

		return sb.toString();
		// return "data0.1,data0.2,data0.3\ndata1.1,data1.2,data1.3";

	}

	



}
