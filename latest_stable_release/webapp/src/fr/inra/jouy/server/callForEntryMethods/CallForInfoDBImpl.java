package fr.inra.jouy.server.callForEntryMethods;


/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import fr.inra.jouy.client.RPC.CallForInfoDB;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.objects.ObjAssociationAsMap;
import fr.inra.jouy.server.queriesTable.QueriesDatabaseMetaData;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentPairs;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentParams;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignments;
import fr.inra.jouy.server.queriesTable.QueriesTableElements;
import fr.inra.jouy.server.queriesTable.QueriesTableGenes;
import fr.inra.jouy.server.queriesTable.QueriesTableHomologs;
import fr.inra.jouy.server.queriesTable.QueriesTableMicadoQualifiers;
import fr.inra.jouy.server.queriesTable.QueriesTableOrganisms;
import fr.inra.jouy.server.queriesTable.mutlipleTables.ComparativeGenomics;
import fr.inra.jouy.server.stat.FisherExactTest;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneMatchItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;
import fr.inra.jouy.shared.pojos.transitComposite.AlItemsOfIntPrimaryIdAttributePlusScore;
import fr.inra.jouy.shared.TransAPMissGeneHomoInfo;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;


public class CallForInfoDBImpl extends RemoteServiceServlet implements
		CallForInfoDB {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	//global Pattern
	
	//dbxref pattern
//	static Pattern EnsemblGenomesPatt = Pattern.compile("^EnsemblGenomes-.+:(.+?)(\\s+.+)?$"); 
//	static Pattern GOAPatt = Pattern.compile("^GOA:(.+?)(\\s+.+)?$");
//	static Pattern InterProPatt = Pattern.compile("^InterPro:(.+?)(\\s+.+)?$");
//	static Pattern UniProtKBPatt = Pattern.compile("^UniProtKB.*?:(.+?)(\\s+.+)?$");
//	static Pattern GOPatt = Pattern.compile("^GO:(.+?)(\\s+.+)?$");
//	static Pattern UniParcPatt = Pattern.compile("^UniParc:(.+?)(\\s+.+)?$");
//	static Pattern KEGGPatt = Pattern.compile("^KEGG:\\s..+[;$]"); // KEGG:\s..+[;$]
//	static Pattern PFAMPatt = Pattern.compile("^PFAM:PF.+[\\s;$]"); // PFAM:PF.+[\s;$]
	static Pattern NCBIGIPatt = Pattern.compile("^[Gg][Ii]:(.+?)(\\s+.+)?$");
//	static Pattern SubtiListPatt = Pattern.compile("^SubtiList:(.+?)(\\s+.+)?$");
//	static Pattern PDBPatt = Pattern.compile("^PDB:(.+?)(\\s+.+)?$"); //http://www.ebi.ac.uk/pdbe/entry/search/index?text:4DDQ // db_xref : PDB:4DDQ
	
	  /**
	   * Setup our connection pool when this servlet is started.
	   * Note that this servlet must be started before any other servlet that tries to
	   * use our database connections.
	   */
	public void init() throws ServletException {
		DatabaseConf.initDBContext();
	}



	
	
	@Override
	public ArrayList<LightOrganismItem> loadAllPublicLightOrganisms() throws Exception {
		String methodNameToReport = "CallForInfoDBImpl loadAllPublicLightOrganisms";
//		long milli = System.currentTimeMillis();
//		long milliPrint2 = -1;
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Start method "+methodNameToReport);
		ArrayList<LightOrganismItem> alToReturn = null;
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();
			alToReturn = QueriesTableOrganisms.getAlLightOrganismItems_orderByScoreAscIfProvided(
					conn
					, null
					, null
					, null
					, false
					, false
					, true
					, true);

			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Overall it took\t" + milliPrint2 + "\tmilliseconds for method "+methodNameToReport);
		return alToReturn;
	}


	
	@Override
	public ArrayList<LightElementItem> loadAllPublicMoleculesOfOrganisms() throws Exception {
		String methodNameToReport = "CallForInfoDBImpl loadAllPublicMoleculesOfOrganisms";
//		long milli = System.currentTimeMillis();
//		long milliPrint2 = -1;
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Start method "+methodNameToReport);
		

		ArrayList<LightElementItem> alToReturn = null;
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();
			alToReturn = QueriesTableElements.getAlLightElementItemsPlusOrganismIdFacultatifWithSetOrganismIdsFacultatif(
					conn
					, true
					, null
					, true
					);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Overall it took\t" + milliPrint2 + "\tmilliseconds for method "+methodNameToReport);

		
		return alToReturn;
	}

	
	
	
//	@Override
//	public ArrayList<OrganismItem> getAllPublicOrganismItem() throws Exception {
//		
//		String methodNameToReport = "CallForInfoDBImpl getAllPublicOrganismItem";
//		
//		long milli = System.currentTimeMillis();
//		long milliPrint2 = -1;
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Start method "+methodNameToReport);
//		
//		ArrayList<OrganismItem> alToReturn = null;
//
//		Connection conn = null;
//		
//		try {
//			
//			conn = DatabaseConf.getConnection_db();
//			alToReturn = QueriesTableOrganisms.getAllOrganismItems(conn, true, true, true);
//		} catch (Exception ex) {
//			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
//
//		} finally {
//			DatabaseConf.freeConnection(conn, methodNameToReport);
//		}
//		
//
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Overall it took\t" + milliPrint2 + "\tmilliseconds for method "+methodNameToReport);
//		
//		return alToReturn;
//
//	}


	
//	@Override
//	public OLD_AlOrganismItemAndTaxoItem getAllPublicOrganismItemAndTaxoTree() throws Exception {
//
//		String methodNameToReport = "CallForInfoDBImpl getAllPublicOrganismItemAndTaxoTree";
//
//		OLD_AlOrganismItemAndTaxoItem aoiatiToReturn = new OLD_AlOrganismItemAndTaxoItem();
//
//		Connection conn = null;
//		
//		try {
//			
//			conn = DatabaseConf.getConnection_db();
//			
//			ArrayList<OrganismItem> alOrganismItemsIT = QueriesTableOrganisms.getAllOrganismItems(conn, true, true, true);
//			aoiatiToReturn.setAlOrganismItem(alOrganismItemsIT);
//			
//			
//			HashSet<Integer> hsTaxIdsIT = new HashSet<Integer>();
//			for (int i = 0; i < alOrganismItemsIT.size(); i++) {
//				hsTaxIdsIT.add(alOrganismItemsIT.get(i).getTaxonId());
//			}
//			
//			conn = DatabaseConf.getConnection_taxo();
//			TaxoItem tiToReturn = QueriesTableTaxonomy.getTaxoTree(conn, hsTaxIdsIT);
//			aoiatiToReturn.setTaxoItem(tiToReturn);
//			
//		} catch (Exception ex) {
//			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
//
//		} finally {
//			DatabaseConf.freeConnection(conn, methodNameToReport);
//		}
//		
//		return aoiatiToReturn;
//			
//	}



	
	@Override
	public ArrayList<LightOrganismItem> getAllPublicLightOrganismItemWithFilterNameOrId(
			String searchString
			, boolean wrapAround
			, boolean caseInsensitive
			) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getAllPublicLightOrganismItemWithFilterNameOrId"
				+ " ; searchString="+searchString
				+ " ; wrapAround="+wrapAround
				+ " ; caseInsensitive="+caseInsensitive
				;
		Connection conn = null;
		ArrayList<LightOrganismItem> alToReturn = new ArrayList<>();
		try {
			conn = DatabaseConf.getConnection_db();
			alToReturn = QueriesTableOrganisms.getAllPublicLightOrganismItemWithFilterNameOrId(conn, searchString, wrapAround, caseInsensitive);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try
		
		return alToReturn;
		
	}
	
	@Override
	public ArrayList<OrganismItem> getAllPublicOrganismItemWithFilterNameOrId(
			String searchString, boolean wrapAround, boolean caseInsensitive)
			throws Exception {

		String methodNameToReport = "CallForInfoDBImpl getAllPublicOrganismItemWithFilterNameOrId"
				+ " ; searchString="+searchString
				+ " ; wrapAround="+wrapAround
				+ " ; caseInsensitive="+caseInsensitive
				;
		Connection conn = null;
		ArrayList<OrganismItem> alToReturn = new ArrayList<>();
		try {
			conn = DatabaseConf.getConnection_db();
			alToReturn = QueriesTableOrganisms.getAllPublicOrganismItemWithFilterNameOrId(conn, searchString, wrapAround, caseInsensitive);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try
		
		return alToReturn;
		
		
		/*
		UtilitiesMethodsServer.checkValidFreeStringInput(
				"CallForInfoDBImpl getAllPublicOrganismItemWithFilterNameOrId",
				searchString, "searchString", false, false);

		ArrayList<OrganismItem> arrayListLOI = new ArrayList<OrganismItem>();
		HashMap<Integer, OrganismItem> hm = new HashMap<Integer, OrganismItem>();

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		String stringToMatch = UtilitiesMethodsServer.prepareStringToBeRegexSearchString(searchString);

		String stringToMatchInQuery = null;
		if (!wrapAround) {
			stringToMatchInQuery = stringToMatch;
		} else {

			if (stringToMatch.matches("^\\.\\*.+")
					&& stringToMatch.matches(".+\\.\\*$")) {
				stringToMatchInQuery = stringToMatch;
			} else if (stringToMatch.matches("^\\.\\*.+")) {
				stringToMatchInQuery = stringToMatch + ".*";
			} else if (stringToMatch.matches(".+\\.\\*$")) {
				stringToMatchInQuery = ".*" + stringToMatch;
			} else {
				stringToMatchInQuery = ".*" + stringToMatch + ".*";
			}

		}
		String command = "SELECT organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic, elements.element_id, elements.type, elements.accession, elements.size"
				+ " FROM organisms, elements"
				+ " WHERE elements.organism_id = organisms.organism_id";
		if (searchString.matches("^\\d+$")) {
			command += " AND organisms.taxon_id = " + searchString;
		} else {
			command += " AND (organisms.species ~ E'^" + stringToMatchInQuery
					+ "$' OR organisms.strain ~ E'^" + stringToMatchInQuery
					+ "$' OR organisms.substrain ~ E'^" + stringToMatchInQuery
					+ "$' OR elements.accession ~ E'^" + stringToMatchInQuery
					+ "$')";
		}

		command += " ORDER BY organisms.organism_id";

		if (caseInsensitive) {
			// System.out.println("case insensitive");
			command = command.replaceAll("~ E\'", "~* E\'");
		}

		// System.out.println("command getAllOrganismItemWithFilterNameOrId : "+command);

		try {
			conn = DatabaseConf.getConnection_db();

			statement = conn.createStatement();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "getAllPublicOrganismItemWithFilterNameOrId");

			while (rs.next()) {

				if (rs.getBoolean("ispublic")) {

					int organismId = rs.getInt("organism_id");
					String strain = rs.getString("strain");
					String species = rs.getString("species");
					OrganismItem loi = null;

					if (hm.containsKey(organismId)) {
						loi = hm.get(organismId);
					} else {

						loi = new OrganismItem();
						loi.setOrganismId(organismId);
						loi.setSpecies(species);
						loi.setStrain(strain);
						loi.setSubstrain(rs.getString("substrain"));
						loi.setTaxonId(rs.getInt("taxon_id"));
						loi.setPublic(rs.getBoolean("ispublic"));
						hm.put(organismId, loi);
						arrayListLOI.add(loi);
					}

					LightElementItem lei = new LightElementItem();
					lei.setElementId(rs.getInt("element_id"));
					lei.setOrganismId(organismId);
					lei.setType(rs.getString("type"));
					lei.setAccession(rs.getString("accession"));
					lei.setSize(rs.getInt("size"));
					lei.setSpecies(species);
					lei.setStrain(strain);
					loi.getListAllLightElementItem().add(lei);

				}
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getAllOrganismItemWithFilterNameOrId",ex);

		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in getAllOrganismItemWithFilterNameOrId rs.close : "
								+ ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in getAllOrganismItemWithFilterNameOrId statement.close : "
								+ ex + "\n" + ex.getMessage());
			}
			DatabaseConf.freeConnection(conn, "getAllOrganismItemWithFilterNameOrId");
		}// try

		return arrayListLOI;
		*/
	}






	/* OLD
	public static ArrayList<Integer> getListElementIdOrderedBySizeDescWithOrganismId(
			Connection conn, int origamiOrgaId) throws Exception {

		ArrayList<Integer> arrayListElementIdOrderedBySizeDesc = new ArrayList<Integer>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			statement = conn.createStatement();

			String command = "SELECT element_id FROM elements WHERE organism_id = "+origamiOrgaId+" ORDER BY size DESC";

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForInfoDBImpl getListElementIdOrderedBySizeDescWithOrganismId");

			while (rs.next()) {
				arrayListElementIdOrderedBySizeDesc.add(rs.getInt("element_id"));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getListElementIdOrderedBySizeDescWithOrganismId",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in getListElementIdOrderedBySizeDescWithOrganismId for rs.close() : "
								+ ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in getListElementIdOrderedBySizeDescWithOrganismId for statement.close() : "
								+ ex + "\n" + ex.getMessage());
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, "getListElementIdOrderedBySizeDescWithOrganismId");
			}

		}// try

		return arrayListElementIdOrderedBySizeDesc;

	}
	*/
	

	@Override
	public String getDetailledElementInfoAsStringWithGeneId(int geneId) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getDetailledElementInfoAsStringWithGeneId"
				+ " ; geneId="+geneId
				;
		
		 Connection conn = null;
		 String stToReturn = null;
		 try {
		
			 if (conn == null) {
				 conn = DatabaseConf.getConnection_db();
			 }
			 
			 int elementIdIT = QueriesTableGenes.getElementIdWithGeneId(conn, geneId, true);
			 stToReturn = QueriesTableElements.getDetailledElementInfoAsStringWithOrigamiElementId(conn, elementIdIT);

		 } catch (Exception ex) {
			 UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		 } finally {
				 DatabaseConf.freeConnection(conn, methodNameToReport);
		 }// try
		 return stToReturn;
	}
	
	@Override
	public String getBasicElementInfoAsStringWithGeneId(int geneId) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getBasicElementInfoAsStringWithGeneId"
				+ " ; geneId="+geneId
				;
		
		 Connection conn = null;
		 
		 String stToReturn = null;
		 try {
		
			 if (conn == null) {
				 conn = DatabaseConf.getConnection_db();
			 }
			 
			 int elementIdIT = QueriesTableGenes.getElementIdWithGeneId(conn, geneId, true);
			 LightElementItem leiIT = QueriesTableElements.getLightElementItemWithElementId(conn, elementIdIT);
			 stToReturn = "<big><i>Accnum : </i></big>"
								+ leiIT.getAccession()
								+ "<br/><big><i>Type : </i></big>"
								+ leiIT.getType()
								+ "<br/><big><i>Size in pb : </i></big>"
								+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(leiIT.getSize()+ " pb")
								+ "<br/>";

		 } catch (Exception ex) {
			 UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		 } finally {
				 DatabaseConf.freeConnection(conn, methodNameToReport);
		 }// try
		 return stToReturn;
	}
	
	

	@Override
	public String getDetailledElementInfoAsStringWithOrigamiElementId(
			int elementId) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getDetailledElementInfoAsStringWithOrigamiElementId"
				+ " ; elementId="+elementId
				;
		
		 Connection conn = null;
		 String stToReturn = null;
		 try {
			 if (conn == null) {
				 conn = DatabaseConf.getConnection_db();
			 }
			 stToReturn =  QueriesTableElements.getDetailledElementInfoAsStringWithOrigamiElementId(conn, elementId);
		 } catch (Exception ex) {
			 UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		 } finally {
				 DatabaseConf.freeConnection(conn, methodNameToReport);
		 }// try
		 return stToReturn;
	}
	
	
	

	

	/*
	@Override
	public String getSyntenyInfoAsStringForHTMLWithQGeneIdAndSGeneId(
			int qGeneId, int sGeneId) throws Exception {

		String stringToReturn = "";

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		try {
			conn = DatabaseConf.getConnection_db();
			statement = conn.createStatement();

			String command = "SELECT alignments.alignment_id, alignments.pairs, alignments.orientation_conserved, alignments.orthologs, alignments.homologs, alignments.mismatches, alignments.gaps"
					+ " FROM alignments, alignment_pairs"
					+ " WHERE alignments.alignment_id = alignment_pairs.alignment_id"
					+ " AND alignment_pairs.q_gene_id = "
					+ qGeneId
					+ " AND alignment_pairs.s_gene_id = " + sGeneId;
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForInfoDBImpl getSyntenyInfoAsStringForHTMLWithQGeneIdAndSGeneId");
			
			if (rs.next()) {

				stringToReturn += "<big><i>Synteny Id : </i></big>"
						+ rs.getLong("alignment_id");
				stringToReturn += "<br/><big><i>Number of pairs : </i></big>"
						+ rs.getInt("pairs");
				stringToReturn += "<br/><big><i>Orientation conserved : </i></big>"
						+ rs.getBoolean("orientation_conserved");
				stringToReturn += "<br/><big><i>Number of orthologs : </i></big>"
						+ rs.getInt("orthologs");
				stringToReturn += "<br/><big><i>Number of homologs : </i></big>"
						+ rs.getInt("homologs");
				stringToReturn += "<br/><big><i>Number of mismatches : </i></big>"
						+ rs.getInt("mismatches");
				stringToReturn += "<br/><big><i>Number of gaps : </i></big>"
						+ rs.getInt("gaps");
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getSyntenyInfoAsStringForHTMLWithQGeneIdAndSGeneId",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in getSyntenyInfoAsStringForHTMLWithQGeneIdAndSGeneId rs.close : "
								+ ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in getSyntenyInfoAsStringForHTMLWithQGeneIdAndSGeneId statement.close : "
								+ ex + "\n" + ex.getMessage());
			}
			DatabaseConf.freeConnection(conn, "getSyntenyInfoAsStringForHTMLWithQGeneIdAndSGeneId");
		}// try

		return stringToReturn;
	}*/


	@Override
	public String getDetailledSyntenyInfoAsStringForHTMLWithAlignmentId(
			long alignmentId) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getDetailledSyntenyInfoAsStringForHTMLWithAlignmentId"
				+ " ; alignmentId="+alignmentId
				;
		
		String stringToReturn = "";
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();
			stringToReturn = QueriesTableAlignments.getDetailledSyntenyInfoAsStringForHTMLWithAlignmentId(conn, alignmentId);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try
		return stringToReturn;

	}

	
	/*
	static ArrayList<Integer> getAllGeneIdsWithOrigamiOrganismId(
			Connection conn, int origamiOrgaId) throws Exception {


		ArrayList<Integer> arrayListLGI = new ArrayList<Integer>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			statement = conn.createStatement();

			//ArrayList<Integer> arrayListElementIdOrderedBySizeDesc = getListElementIdOrderedBySizeDescWithOrganismId(conn, origamiOrgaId);
			ArrayList<Integer> arrayListElementIdOrderedBySizeDesc = QueriesTableElements.getAlElementIdWithOrganismId_optionalOrderBySizeDesc(conn, origamiOrgaId, true, true);

			for(int eletIdIT : arrayListElementIdOrderedBySizeDesc){

				String command = "SELECT gene_id" + " FROM genes"
						+ " WHERE element_id = " + eletIdIT
						+ " ORDER BY start";

				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForInfoDBImpl getAllGeneIdsWithOrigamiOrganismId");
				
				while (rs.next()) {
					arrayListLGI.add(rs.getInt("gene_id"));
				}
			}
			

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getAllGeneIdsWithOrigamiOrganismId",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in getAllGeneIdsWithOrigamiOrganismId for rs.close() : "
								+ ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in getAllGeneIdsWithOrigamiOrganismId for statement.close() : "
								+ ex + "\n" + ex.getMessage());
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, "getAllGeneIdsWithOrigamiOrganismId");
			}

		}// try

		return arrayListLGI;

	}*/
	
	@Override
	public ArrayList<Integer> getGeneSetIdsWithListElementIdsThenStartPbthenStopPBLooped(
			ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent
			, boolean grabCDSWithAtLeastOnePbInThisStartStopLocus
			) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getGeneSetIdsWithListElementIdsThenStartPbthenStopPBLooped"
				+ " ; listElementIdsThenStartPbthenStopPBLoopedSent=" + ( (listElementIdsThenStartPbthenStopPBLoopedSent != null ) ? listElementIdsThenStartPbthenStopPBLoopedSent.toString() : "NULL" )
				;
		
		ArrayList<Integer> alToReturn = new ArrayList<Integer>();
		Connection conn = null;
		try {
			alToReturn.addAll(QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
														conn
														, listElementIdsThenStartPbthenStopPBLoopedSent
														, null
														, grabCDSWithAtLeastOnePbInThisStartStopLocus // grabCDSWithAtLeastOnePbInThisStartStopLocus
														//, grabCDSWithAtLeastOnePbInThisStartStopLocus // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch 
														, 10 // ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch
														, true // orderCDSByStartAsc
														, false // shouldBePresentInTable
														)
					);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}
		return alToReturn;
	}
	
	
//	static ArrayList<Integer> getListGenesIdsWithElementIdAndPbStartAndPbStop(
//			Connection conn,
//			ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent
//			) throws Exception {
//		
//		todel replaced by QueriesTableGenes.getListGenesIdsWithElementId_optionalPbStartAndPbStop
//
//		ArrayList<Integer> alQGeneIds = new ArrayList<Integer>();
//
//		Statement statement = null;
//		ResultSet rs = null;
//		boolean closeConn = false;
//
//		try {
//			if (conn == null) {
//				conn = DatabaseConf.getConnection_db();
//				closeConn = true;
//			}
//			statement = conn.createStatement();
//			
//			for (int i = 0; i < listElementIdsThenStartPbthenStopPBLoopedSent
//					.size(); i += 3) {
//
//				// String command =
//				String command = "SELECT gene_id FROM genes WHERE element_id = "
//						+ listElementIdsThenStartPbthenStopPBLoopedSent.get(i)
//						+ " AND start < "
//						+ listElementIdsThenStartPbthenStopPBLoopedSent.get(i+2)
//						+ " AND stop > "
//						+ listElementIdsThenStartPbthenStopPBLoopedSent.get(i+1)
//						;
//				
//				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForInfoDBImpl getListGenesIdsWithElementIdAndPbStartAndPbStop");
//							
//				while (rs.next()) {
//					int geneIdIt = rs.getInt("gene_id");
//					if (geneIdIt > 0) {
//						alQGeneIds.add(geneIdIt);
//					}
//				}
//			}
//			
//		} catch (Exception ex) {
//			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getListGenesIdsWithElementIdAndPbStartAndPbStop",ex);
//		} finally {
//			try {
//				rs.close();
//				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
//			} catch (Exception ex) {
//				System.err
//						.println("problem in getListGenesIdsWithElementIdAndPbStartAndPbStop for rs.close() : "
//								+ ex + "\n" + ex.getMessage());
//			}
//			try {
//				statement.close();
//				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
//			} catch (Exception ex) {
//				System.err
//						.println("problem in getListGenesIdsWithElementIdAndPbStartAndPbStop for statement.close() : "
//								+ ex + "\n" + ex.getMessage());
//			}
//			if (closeConn) {
//				DatabaseConf.freeConnection(conn, "getListGenesIdsWithElementIdAndPbStartAndPbStop");
//			}
//
//		}// try
//
//		return alQGeneIds;
//		
//	}





	/*
	static int getGeneStartWithGeneId(Connection conn, int geneId) throws Exception {

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		int geneStartToReturn = -1;
		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT start" + " FROM genes"
					+ " WHERE genes.gene_id = " + geneId;
			
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForInfoDBImpl getGeneStartWithGeneId");
			

			if (rs.next()) {
				geneStartToReturn = rs.getInt("start");

				if (rs.next()) {
					UtilitiesMethodsServer.reportException("CallForInfoDBImpl getGeneStartWithGeneId",new Exception("multiples entries found for gene_id "
									+ geneId));
				}
			} else {
				UtilitiesMethodsServer.reportException("CallForInfoDBImpl getGeneStartWithGeneId",new Exception("No entry found for gene_id "+geneId));
			}


		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getGeneStartWithGeneId",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in CallForInfoDBImpl getGeneStartWithGeneId for rs.close() : "
								+ ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in CallForInfoDBImpl getGeneStartWithGeneId for statement.close() : "
								+ ex + "\n" + ex.getMessage());
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, "CallForInfoDBImpl getGeneStartWithGeneId");
			}

		}// try
		
		if (geneStartToReturn >= 0) {
			return geneStartToReturn;
		} else {
			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getGeneStartWithGeneId",new Exception("start is negative for gene_id "+geneId));
			return -1;
		}
		
	}*/


	/*
	static int getGeneStopWithGeneId(Connection conn, int geneId)
			throws Exception {

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		int geneStopToReturn = -1;
		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT stop" + " FROM genes"
					+ " WHERE genes.gene_id = " + geneId;
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "CallForInfoDBImpl getGeneStopWithGeneId");
			
			if (rs.next()) {
				geneStopToReturn = rs.getInt("stop");
				if (rs.next()) {
					UtilitiesMethodsServer.reportException("CallForInfoDBImpl getGeneStopWithGeneId",new Exception("multiples entries found for gene_id "
									+ geneId));
				}
			} else {
				UtilitiesMethodsServer.reportException("CallForInfoDBImpl getGeneStopWithGeneId",new Exception("No entry found for gene_id "
								+ geneId));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getGeneStopWithGeneId",ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in CallForInfoDBImpl getGeneStopWithGeneId for rs.close() : "
								+ ex + "\n" + ex.getMessage());
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				System.err
						.println("problem in CallForInfoDBImpl getGeneStopWithGeneId for statement.close() : "
								+ ex + "\n" + ex.getMessage());
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, "CallForInfoDBImpl getGeneStopWithGeneId");
			}

		}// try
		
		if (geneStopToReturn >= 0) {
			return geneStopToReturn;
		} else {
			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getGeneStopWithGeneId",new Exception("start is negative for gene_id "
							+ geneId));
			return -1;
		}
		
	}*/
	
	
	@Override
	public ArrayList<Integer> getGeneSetIdsWithOrigamiSyntenyId(
			Long mainOrigamiSyntenyId
			, HashSet<Integer> hsTypeToRetrieve
			) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getGeneSetIdsWithOrigamiSyntenyId"
				+ " ; mainOrigamiSyntenyId="+mainOrigamiSyntenyId
				;
		
		ArrayList<Integer> alToReturn = new ArrayList<Integer>();

		Connection conn = null;

		try {

			conn = DatabaseConf.getConnection_db();
			alToReturn = QueriesTableAlignmentPairs.getListQGenesIdsWithAlignmentId(
					conn
					, mainOrigamiSyntenyId
					, hsTypeToRetrieve
					);

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try

		return alToReturn;

	}

	
	
	
	@Override
	public ArrayList<String> getDetailledGeneInfoAsListStringForHTMLWithSingletonAlignmentId(
			Long alignmentId, boolean referenceGene) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getDetailledGeneInfoAsListStringForHTMLWithSingletonAlignmentId"
				+ " ; alignmentId="+alignmentId
				+ " ; referenceGene="+referenceGene
				;
		
		ArrayList<String> AlToReturn = null;
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();
			int geneIdIT = QueriesTableAlignmentPairs.getGeneIdWithSingletonAlignmentIdAndBooleanReferenceGene(conn, alignmentId, referenceGene);
			AlToReturn = QueriesTableGenes.getDetailledGeneInfoAsListStringForHTMLWithGeneId(conn, geneIdIT);
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try
		return AlToReturn;
	}


	
	@Override
	public ArrayList<String> getDetailledGeneInfoAsListStringForHTMLWithGeneId(
			Integer geneIdSent) throws Exception {
		return QueriesTableGenes.getDetailledGeneInfoAsListStringForHTMLWithGeneId(
				null, geneIdSent);
	}
	


	
	@Override
	public ArrayList<LightGeneItem> getListQLightGeneItemWithOrigamiSyntenyAlignmentId(
			Long origamiSyntenyAlignmentId
			) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getListQLightGeneItemWithOrigamiSyntenyAlignmentId"
				+ " ; origamiSyntenyAlignmentId="+origamiSyntenyAlignmentId
				;
		
		Connection conn = null;
		ArrayList<LightGeneItem> alToReturn = null;
		
		try {

			// RQ type for genes
			// type 1 : strong homolog BDBH with avg score 220, average evalue
			// 5.76e-5
			// type 2 : weaker homolog with avg score 137, average evalue
			// 2.2e-4
			// type 3 mismatch
			// type 4 s insertion
			// type 5 q insertion
			HashSet<Integer> hsTypeToRetrieve = new HashSet<>();
			hsTypeToRetrieve.add(1);
			hsTypeToRetrieve.add(2);
			hsTypeToRetrieve.add(3);
			//hsTypeToRetrieve.add(4);
			hsTypeToRetrieve.add(5);
			
			
			conn = DatabaseConf.getConnection_db();
			// statement = conn.createStatement();
			alToReturn = QueriesTableGenes.getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(
					conn
					, QueriesTableAlignmentPairs.getListQGenesIdsWithAlignmentId(
							conn
							, origamiSyntenyAlignmentId
							, hsTypeToRetrieve
							)
					, true
					);
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {

			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try
		
		return alToReturn;
		
	}

			

	@Override
	public ArrayList<LightGeneItem> getListLightGeneItemWithWithAlElementIdAndStartPbAndStopPbLooped(
			ArrayList<Integer> alElementIdTenStartThenStopLooped
			, boolean grabCDSWithAtLeastOnePbInThisStartStopLocus
			)
			throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getListLightGeneItemWithWithAlElementIdAndStartPbAndStopPbLooped"
				+ " ; alElementIdTenStartThenStopLooped="+ ( (alElementIdTenStartThenStopLooped != null ) ? alElementIdTenStartThenStopLooped.toString() : "NULL" )
				;
		
		Connection conn = null;
		ArrayList<LightGeneItem> alToReturn = null;
		
		try {

			conn = DatabaseConf.getConnection_db();
			
			ArrayList<Integer> alGeneIdsIT = QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
					conn
					, alElementIdTenStartThenStopLooped
					, null
					, grabCDSWithAtLeastOnePbInThisStartStopLocus // grabCDSWithAtLeastOnePbInThisStartStopLocus
					//, grabCDSWithAtLeastOnePbInThisStartStopLocus // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
					, 10 // ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch
					, true // orderCDSByStartAsc
					, false // shouldBePresentInTable
					);
			alToReturn = QueriesTableGenes.getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(conn, alGeneIdsIT, true);
			
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try
		return alToReturn;
	}

	
//	@Override
//	public ArrayList<LightGeneItem> getAllLightGeneItemWithOrgaIdAndListGeneName(
//			int organismId, ArrayList<String> listGeneNameDNARep)
//			throws Exception {
//
//		if (listGeneNameDNARep.isEmpty()) {
////			throw new Exception(
////					"Pb in getAllLightGeneItemWithOrgaIdAndListGeneName, listGeneNameDNARep is empty");
//			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getAllLightGeneItemWithOrgaIdAndListGeneName",new Exception("listGeneNameDNARep is empty"));
//		}
//
//		ArrayList<LightGeneItem> arrayListLGI = new ArrayList<LightGeneItem>();
//
//		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
//
//		String command = "SELECT genes.gene_id, genes.organism_id, genes.element_id, genes.name, genes.strand, genes.start, genes.stop, elements.type, micado.features.accession, micado.features.code_feat"
//				+ " FROM genes, elements, micado.features"
//				+ " WHERE elements.element_id=genes.element_id AND elements.accession = micado.features.accession AND genes.feature_id = micado.features.code_feat"
//				+ " AND genes.organism_id = " + organismId + " AND (";
//		for (int i = 0; i < listGeneNameDNARep.size(); i++) {
//			if (i > 0) {
//				command += " OR ";
//			}
//			command += "genes.name = '" + listGeneNameDNARep.get(i) + "'";
//		}
//		command += " ) " + " ORDER BY genes.start";
//
//		//System.err.println(command);
//
//		try {
//
//			conn = DatabaseConf.getConnection_db();
//			statement = conn.createStatement();
//			
//			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "getAllLightGeneItemWithOrgaIdAndListGeneName");
//
//
//			int count = 0;
//			while (rs.next()) {
//				count++;
//				if (count > MAX_GENE_ITEM_RETURNED_ALLOWED_FOR_SEARCH) {
////					throw new Exception(
////							"Your query returned a large number of gene, please narrow your search down");
//					UtilitiesMethodsServer.reportException("CallForInfoDBImpl getAllLightGeneItemWithOrgaIdAndListGeneName",new Exception("Your query returned a large number of gene, please narrow your search down : "+command));
//				}
//				LightGeneItem geneToDW = new LightGeneItem();
//				// fillInThisGeneItemWithBasicsAndLocusTag(geneToDW, rs);
//				fillInThisLightGeneItemWithBasicsExceptLocusTagAndName(geneToDW, rs);
//				ArrayList<String> alGeneNameAndLocusTag = getGeneNameAndLocusTagWithAccessionAndCodeFeat(
//						rs.getString("accession"), rs.getInt("code_feat"), conn);
//				if (alGeneNameAndLocusTag.get(1).length() > 0) {
//					geneToDW.setLocusTag(alGeneNameAndLocusTag.get(1));
//				}
//				if (alGeneNameAndLocusTag.get(0).length() > 0) {
//					geneToDW.setName(alGeneNameAndLocusTag.get(0));
//				}
//				arrayListLGI.add(geneToDW);
//
//			}// while
//
//
//		} catch (Exception ex) {
//			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getAllLightGeneItemWithOrgaIdAndListGeneName",ex);
//		} finally {
//			try {
//				rs.close();
//			} catch (Exception ex) {
//				System.err
//						.println("problem in CallForInfoDBImpl getAllLightGeneItemWithOrgaIdAndListGeneName rs.close : "
//								+ ex + "\n" + ex.getMessage());
//			}
//			try {
//				statement.close();
//			} catch (Exception ex) {
//				System.err
//						.println("problem in CallForInfoDBImpl getAllLightGeneItemWithOrgaIdAndListGeneName statement.close : "
//								+ ex + "\n" + ex.getMessage());
//			}
//			DatabaseConf.freeConnection(conn, "getAllLightGeneItemWithOrgaIdAndListGeneName");
//		}// try
//
//		return arrayListLGI;
//		
//	}


	@Override
	public ArrayList<LightGeneItem> getAllLightGeneItemWithElementId(
			int elementId) throws Exception {
		return QueriesTableGenes.getAllLightGeneItemWithElementId(null, elementId);
	}


	
	@Override
	public LightGeneItem getLightGeneItemWithGeneId(
			int geneId) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getLightGeneItemWithGeneId"
				+ " ; geneId="+geneId
				;
		
		ArrayList<Integer> alGeneIdsIT = new ArrayList<Integer>();
		alGeneIdsIT.add(geneId);
		ArrayList<LightGeneItem> alLgiIT = getAllLightGeneItemWithListGeneIds_sortedByGeneId(alGeneIdsIT);
		if(alLgiIT != null){
			if(!alLgiIT.isEmpty()){
				return alLgiIT.get(0);
			}else{
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("alLgiIT.isEmpty()"));
				return null;
			}
		}else{
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("alLgiIT is null"));
			return null;
		}
	}

	
	
	
	@Override
	public ArrayList<LightGeneItem> getAllLightGeneItemWithListGeneIds_sortedByGeneId(
			ArrayList<Integer> alGeneIds) throws Exception {
			return QueriesTableGenes.getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(null, alGeneIds, true);
			
	}

	
	/*public static ArrayList<LightGeneItem> getAllLightGeneItemWithListGeneIds_sortedByGeneId(
			Connection conn, ArrayList<Integer> listGEneIds) throws Exception {

		//early return if no data sent
		if (listGEneIds.isEmpty()) {
			return new ArrayList<LightGeneItem>();
		}

		boolean closeConn = false;
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			return QueriesTableGenes.getALLightGeneItemWithCollectionGeneIds_sortedByGeneId(conn,
					listGEneIds,
					true);

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException("CallForInfoDBImpl getAllLightGeneItemWithListGeneIds",ex);
		} finally {
			if (closeConn) {
				DatabaseConf.freeConnection(conn, "getAllLightGeneItemWithListGeneIds");
			}
		}// try

		return new ArrayList<LightGeneItem>();
	}*/


	@Override
	public ArrayList<LightGeneItem> getAllLightGeneItemWithListFiltersAndAlElementIds(
			ArrayList<FilterGeneItem> listFilters
			//, ArrayList<Integer> alRefOrganismIds
			, ArrayList<Integer> alElementIds
			) throws Exception {

		String methodNameToReport = "CallForInfoDBImpl getAllLightGeneItemWithListFilters"
				+ " ; listFilters="+ ( (listFilters != null ) ? listFilters.toString() : "NULL" )
				//+ " ; alRefOrganismIds="+ ( (alRefOrganismIds != null ) ? alRefOrganismIds.toString() : "NULL" )
				+ " ; alElementIds="+ ( (alElementIds != null ) ? alElementIds.toString() : "NULL" )
				;
		
		Connection conn = null;

		ArrayList<LightGeneItem> alLgiToReturn = new ArrayList<LightGeneItem>();
		
		try {
			
//			long milli = System.currentTimeMillis();
//			long milliPrint2 = -1;
//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("Start method "+methodNameToReport);
			
			HashSet<Integer> hsElementIds = new HashSet<>(alElementIds);
			alLgiToReturn.addAll(QueriesTableGenes.getAllLightGeneItemWithListFiltersAndAlRefElementIds(conn, listFilters, hsElementIds));
			
			
			// OLD WAY
//			conn = DatabaseConf.getConnection_db();
//			ArrayList<Integer> alRefElementIdsFromAlOrgaIdsAndEletIdsSent = QueriesTableElements.getUniqListEletIdFromAlOrgaAndEletIds(
//					conn,
//					alRefOrganismIds, alRefElementIds);
//			for(int eletIdIT : alRefElementIdsFromAlOrgaIdsAndEletIdsSent){
//				alLgiToReturn.addAll(QueriesTableGenes.getAllLightGeneItemWithListFiltersAndRefEletId(conn, listFilters, eletIdIT));
//			}
			
//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("Overall it took\t" + milliPrint2 + "\tmilliseconds");
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}// try

		return alLgiToReturn;
		
	}

	
	@Override
	public TransAPMissGeneHomoInfo getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem(
			TransAPMissGeneHomoInfo tapmghiSent, boolean getMatchInfo)
			throws Exception {

		String methodName = "CallForInfoDBImpl getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem"
				+ " ; tapmghiSent="+tapmghiSent.stringifyToJSON(true, true)
				+ " ; getMatchInfo="+getMatchInfo
				;
		
		Connection conn = null;
		//Statement statement = null;
		//ResultSet rs = null;

		try {
			
			conn = DatabaseConf.getConnection_db();
			//statement = conn.createStatement();

			// if missing q_gene id and s gene id, probably a singleton synteny,
			// get them
			if (tapmghiSent.getqGeneId() < 0 && tapmghiSent.getsGeneId() < 0) {
				
				tapmghiSent = QueriesTableAlignmentPairs.getTransAPMissGeneHomoInfosWithAlignmentId(
						conn
						, tapmghiSent.getAlignmentId()
						, null
						, null
						, false
						);

				/*
				String commandPre = "SELECT alignment_id, q_gene_id, s_gene_id, type"
						+ " FROM alignment_pairs"
						+ " WHERE alignment_id = "
						+ tapmghiSent.getOrigamiAlignmentId();
				
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandPre, "CallForInfoDBImpl getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem");
				
				
				// RQ type for genes
				// type 1 : strong homolog with avg score 220, average evalue
				// 5.76e-5
				// type 2 : weaker homolog with avg score 137, average evalue
				// 2.2e-4
				// type 3 mismatch
				// type 4 s insertion
				// type 5 q insertion
				if (rs.next()) {
					int type = rs.getInt("type");
					tapmghiSent.setOrigamiAlignmentPairsType(type);
					int qGeneId = rs.getInt("q_gene_id");
					if (type != 4 && qGeneId < 1) {
						UtilitiesMethodsServer.reportException("CallForInfoDBImpl getListTransientAlignmentPairsWithListOrigamiSyntenyId",new Exception("type != 4 has qgene id < 1 for alignment_id = "
										+ rs.getLong("alignment_id")));
					}
					tapmghiSent.setqGeneId(qGeneId);
					int sGeneId = rs.getInt("s_gene_id");
					if (type != 5 && sGeneId < 1) {
						UtilitiesMethodsServer.reportException("CallForInfoDBImpl getListTransientAlignmentPairsWithListOrigamiSyntenyId",new Exception("type != 5 has sgene id < 1 for alignment_id = "
										+ rs.getLong("alignment_id")));
					}
					tapmghiSent.setsGeneId(sGeneId);
					if (rs.next()) {
						UtilitiesMethodsServer.reportException("CallForInfoDBImpl getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem",new Exception("there is more than 1 row returned for alignment_id = "
										+ tapmghiSent.getOrigamiAlignmentId()));
					}
				} else {
					UtilitiesMethodsServer.reportException("CallForInfoDBImpl getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem",new Exception("there is no row returned for alignment_id = "
									+ tapmghiSent.getOrigamiAlignmentId()));
				}*/
			}

			if (tapmghiSent.getqGeneId() >= 0) {
				// for query : get gene name, locus tag and strand
				
				ObjAssociationAsMap oaamGeneNameLocusTagStrandIT = QueriesTableGenes.getGeneNameLocusTagStrandWithGeneId(conn, tapmghiSent.getqGeneId());
				if (oaamGeneNameLocusTagStrandIT.getRsColumn2String().get("name") != null) {
					tapmghiSent.setqName(oaamGeneNameLocusTagStrandIT.getRsColumn2String().get("name"));
				}
				if (oaamGeneNameLocusTagStrandIT.getRsColumn2String().get("locus_tag") != null) {
					tapmghiSent.setqLocusTag(oaamGeneNameLocusTagStrandIT.getRsColumn2String().get("locus_tag"));
				}
				if (oaamGeneNameLocusTagStrandIT.getRsColumn2Int().get("strand") != null) {
					tapmghiSent.setqStrand(oaamGeneNameLocusTagStrandIT.getRsColumn2Int().get("strand"));
				}
				/* OLD
				String command1 = "SELECT genes.name, genes.strand, micado.features.accession, micado.features.code_feat"
						+ " FROM genes, elements, micado.features"
						+ " WHERE elements.element_id=genes.element_id AND elements.accession = micado.features.accession AND genes.feature_id = micado.features.code_feat"
						+ " AND genes.gene_id = " + tapmghiSent.getqGeneId();

				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command1, "CallForInfoDBImpl getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem");
				
				if (rs.next()) {
					tapmghiSent.setqName(rs.getString("name"));
					tapmghiSent.setqStrand(rs.getInt("strand"));

					ArrayList<String> alGeneNameAndLocusTag = getGeneNameAndLocusTagWithAccessionAndCodeFeat(
							rs.getString("accession"), rs.getInt("code_feat"),
							conn);

					if (alGeneNameAndLocusTag.get(1).length() > 0) {
						// System.out.println(alGeneNameAndLocusTag.get(1));
						tapmghiSent.setqLocusTag(alGeneNameAndLocusTag.get(1));
					}
					if (alGeneNameAndLocusTag.get(0).length() > 0) {
						// System.out.println(alGeneNameAndLocusTag.get(0));
						tapmghiSent.setqName(alGeneNameAndLocusTag.get(0));
					}
				} else {
					UtilitiesMethodsServer.reportException("CallForInfoDBImpl getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem",new Exception("there is more than row returned for qgeneid = "
									+ tapmghiSent.getqGeneId()));
				}*/

			}

			if (tapmghiSent.getsGeneId() >= 0) {
				// for query : get gene name, locus tag and strand
				
				ObjAssociationAsMap oaamGeneNameLocusTagStrandIT = QueriesTableGenes.getGeneNameLocusTagStrandWithGeneId(conn, tapmghiSent.getsGeneId());
				if (oaamGeneNameLocusTagStrandIT.getRsColumn2String().get("name") != null) {
					tapmghiSent.setsName(oaamGeneNameLocusTagStrandIT.getRsColumn2String().get("name"));
				}
				if (oaamGeneNameLocusTagStrandIT.getRsColumn2String().get("locus_tag") != null) {
					tapmghiSent.setsLocusTag(oaamGeneNameLocusTagStrandIT.getRsColumn2String().get("locus_tag"));
				}
				if (oaamGeneNameLocusTagStrandIT.getRsColumn2Int().get("strand") != null) {
					tapmghiSent.setsStrand(oaamGeneNameLocusTagStrandIT.getRsColumn2Int().get("strand"));
				}
				
				/*
				String command2 = "SELECT genes.name, genes.strand, micado.features.accession, micado.features.code_feat"
						+ " FROM genes, elements, micado.features"
						+ " WHERE elements.element_id=genes.element_id AND elements.accession = micado.features.accession AND genes.feature_id = micado.features.code_feat"
						+ " AND genes.gene_id = " + tapmghiSent.getsGeneId();

				if (rs != null){
					rs.close();
				}
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command2, "CallForInfoDBImpl getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem");
				
				

				if (rs.next()) {
					tapmghiSent.setsName(rs.getString("name"));
					tapmghiSent.setsStrand(rs.getInt("strand"));
					ArrayList<String> alGeneNameAndLocusTag = getGeneNameAndLocusTagWithAccessionAndCodeFeat(
							rs.getString("accession"), rs.getInt("code_feat"),
							conn);
					if (alGeneNameAndLocusTag.get(1).length() > 0) {
						tapmghiSent.setsLocusTag(alGeneNameAndLocusTag.get(1));
					}
					if (alGeneNameAndLocusTag.get(0).length() > 0) {
						tapmghiSent.setsName(alGeneNameAndLocusTag.get(0));
					}
				} else {
					UtilitiesMethodsServer.reportException("CallForInfoDBImpl getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem",new Exception("there is more than row returned for sgeneid = "
									+ tapmghiSent.getsGeneId()));
				}*/

			}

			if (getMatchInfo) {
				// get homology match info
				ArrayList<LightGeneMatchItem> listLightGeneMatchItemIT = QueriesTableHomologs.getLightGeneMatchItemWithQGeneIdAndSGeneId(
						conn
						, tapmghiSent.getqGeneId()
						, tapmghiSent.getsGeneId()
						, false
						);
				tapmghiSent.setListLightGeneMatchItem(listLightGeneMatchItemIT);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodName, ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodName);

		}// try

		return tapmghiSent;

	}




	@Override
	public ArrayList<String> getIdentifiersForVisuAlignmentWithQAndSGeneId(
			int qGeneIdForVisuAlignment, int sGeneIdForVisuAlignment)
			throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getIdentifiersForVisuAlignmentWithQAndSGeneId"
				+ " ; qGeneIdForVisuAlignment="+qGeneIdForVisuAlignment
				+ " ; sGeneIdForVisuAlignment="+sGeneIdForVisuAlignment
				;
		
		ArrayList<String> alToReturn = new ArrayList<String>();
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();

			ArrayList<String> alTypeQualProteinId = new ArrayList<String>();
			alTypeQualProteinId.add("protein_id");
			alTypeQualProteinId.add("prot_id");
			
			ArrayList<String> alTypeQualGI = new ArrayList<String>();
			alTypeQualGI.add("db_xref"); // + Pattern to match NCBIGIPatt
			
			
			ArrayList<Integer> alIntGeneId = new ArrayList<Integer>();
			alIntGeneId.add(qGeneIdForVisuAlignment);
			alIntGeneId.add(sGeneIdForVisuAlignment);
			
			for (int geneIdIT : alIntGeneId) {

				// return ObjAssociationAsMap.getRsColumn2Int().get("element_id")
				// return ObjAssociationAsMap.getRsColumn2Int().get("feature_id")
				ObjAssociationAsMap oaam_elementIdAndFeatureIdIT = QueriesTableGenes.getElementIdAndFeatureIdWithGeneId(conn, geneIdIT, true);
				String accnumIT = QueriesTableElements.getAccessionWithElementId(
						conn,
						oaam_elementIdAndFeatureIdIT.getRsColumn2Int().get("element_id"),
						true);
				ArrayList<String> alQualProteinId = QueriesTableMicadoQualifiers.getAlGeneQualifiers_withAccnumAndAlTypeQual_optionalFeatId(
						conn,
						accnumIT,
						oaam_elementIdAndFeatureIdIT.getRsColumn2Int().get("feature_id"),
						alTypeQualProteinId,
						false,
						false);
				if(alQualProteinId.size() == 1){
					alToReturn.add(alQualProteinId.get(0));
				} else {
					ArrayList<String> alQualDbXrefIT = QueriesTableMicadoQualifiers.getAlGeneQualifiers_withAccnumAndAlTypeQual_optionalFeatId(
							conn,
							accnumIT,
							oaam_elementIdAndFeatureIdIT.getRsColumn2Int().get("feature_id"),
							alTypeQualGI,
							false,
							false);
					ArrayList<String> alQualGIIT = new ArrayList<String>();
					for(String dbXrefIT : alQualDbXrefIT){
						Matcher m = NCBIGIPatt.matcher(dbXrefIT);
						if (m.matches()) {
							alQualGIIT.add(m.group(1));
						}
					}
					if(alQualGIIT.size() == 1){
						alToReturn.add(alQualGIIT.get(0));
					} else {
						String protSeq = QueriesTableGenes.getProteinSequenceWithGeneId(conn, geneIdIT, true);
						if( ! protSeq.isEmpty()){
							alToReturn.add(protSeq);
						} else {
							return new ArrayList<String>();
						}
					}
				}
			}
			
			
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}

		return alToReturn;

	}


	@Override
	public AlItemsOfIntPrimaryIdAttributePlusScore getReferenceGenesAndOrthologsOverRepresentatedInPhenotype(
			Integer referenceOrgaId,
			ArrayList<Integer> alSelectedPhenoPlusOrgaIds,
			ArrayList<Integer> alSelectedPhenoMinusOrgaIds
			//ArrayList<Integer> alSelectedEitherPresenceOrAbsenceOrgaIds
			)
			throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getReferenceGenesAndOrthologsOverRepresentatedInPhenotype"
				+ " ; referenceOrgaId="+referenceOrgaId
				+ " ; alSelectedPhenoPlusOrgaIds="+ ( (alSelectedPhenoPlusOrgaIds != null ) ? alSelectedPhenoPlusOrgaIds.toString() : "NULL" )
				+ " ; alSelectedPhenoMinusOrgaIds="+ ( (alSelectedPhenoMinusOrgaIds != null ) ? alSelectedPhenoMinusOrgaIds.toString() : "NULL" )
				;
		
		AlItemsOfIntPrimaryIdAttributePlusScore objToReturn = new AlItemsOfIntPrimaryIdAttributePlusScore();
		//LinkedHashMap<Integer, Double> sortedQGeneId2probability = new LinkedHashMap<Integer, Double>();
		
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();

			int populationSize = alSelectedPhenoPlusOrgaIds.size() + alSelectedPhenoMinusOrgaIds.size() + 1;
			if (populationSize <= 0){
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("populationSize is less or equal to 0"));
			}
			else if (populationSize > UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined){
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("populationSize is greater than "+UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined));
			}
			
			
//					//+ alSelectedEitherPresenceOrAbsenceOrgaIds.size(); not taken into account, treat as discarded sample
//			int numberOfSuccessesInPopulation = alSelectedPresenceOrgaIds.size();
//			Double probaSuccessInPopulation = ( (double) numberOfSuccessesInPopulation / (double) populationSize );
//			int numberOfFailuresInPopulation = alSelectedAbsenceOrgaIds.size();
//					// + alSelectedEitherPresenceOrAbsenceOrgaIds.size(); not taken into account, treat as discarded sample
//			Double probaFailuresInPopulation = ( (double) numberOfFailuresInPopulation / (double) populationSize );
			
			ArrayList<Integer> alSorgaIdToRestrictSearch = new ArrayList<Integer>();
			alSorgaIdToRestrictSearch.addAll(alSelectedPhenoPlusOrgaIds);
			alSorgaIdToRestrictSearch.addAll(alSelectedPhenoMinusOrgaIds);
			HashMap<Integer, HashSet<Integer>> qGeneId2HsSOrgaIdHaveHomolog = 
					QueriesTableHomologs
						.getHmqGeneId2HsSOrgaIdWithAlQGeneIdsOrQOrgaId_optionalRestrictSorgaId(
								conn
								, referenceOrgaId
								, null
								, alSorgaIdToRestrictSearch
								, true
								, false
							);
			
			HashMap<Integer, Double> qGeneId2probability = new HashMap<Integer, Double>();
			
			
			for (Entry<Integer, HashSet<Integer>> entry : qGeneId2HsSOrgaIdHaveHomolog.entrySet()) {
			    int qGeneIdIT = entry.getKey();
			    HashSet<Integer> hsSOrgaIdHaveHomolog = entry.getValue();
			    HashSet<Integer> hsSOrgaIdHaveHomolog_bis = new HashSet<Integer>(hsSOrgaIdHaveHomolog);
			    // int sampleSize 
			    //int initSizeHsSOrgaIdHaveHomolog = hsSOrgaIdHaveHomolog.size();
				// compute intersection result getListSOrgaIdFromTableHomologsWithQOrgaId(referenceOrgaId) with alSelectedPresenceOrgaIds 
				hsSOrgaIdHaveHomolog.retainAll(alSelectedPhenoPlusOrgaIds);
				int numberOfSorgaFromPhenotypePresenceFoundWithOrtholog = hsSOrgaIdHaveHomolog.size();
				hsSOrgaIdHaveHomolog_bis.retainAll(alSelectedPhenoMinusOrgaIds);
				int numberOfSorgaFromPhenotypeAbsenceFoundWithOrtholog = hsSOrgaIdHaveHomolog_bis.size();
						
				// if alSelectedPresenceOrgaIds is empty or alSelectedAbsenceOrgaIds is empty
				//, use another method (count abundance) because Fisher's exact test
				// will always give proba 1.0 no matter on how many orthologs are recovered
				
				
				// else, use the Fisher's exact test:
				// Fisher's exact test is used to detect group differences using frequency (count) data
				// Fet is used when members of two independent groups can fall into one of two mutually exclusive categories.
				// The test is used to determine whether the proportions of those falling into each category differ by group.
				// The chi-square test of independence can also be used in such situations, but not for small sample (all values in cell must be > 5 to be considered not small sample) and it is only an approximation
				// When the observed number of successes and the observed number of failures are greater than or equal to 5 for both populations
				// then the sampling distribution of is approximately normal and we can use z-methods
				// The test is used when you want to see if there is a relationship between two categorical variables
				// For example, is there a relationship between the type of school attended (public / private) and students' gender (female / male)
				// Test requirements :
				// - categorical variables are Dichotomous (jointly exhaustive + mutually exclusive)
				// - individuals in the test are independant from each other
				// - The two classifications (ex : gender and type of school) are not associated
				// in our case two classifications are : Phenotype + / - and Presence / Absence gene of interest
				
				// hypoteses H0 : two population proportions are the same; in other words, their difference is equal to 0
				// or the distribution of the orthologs among the 2 phenotypes groups is homogeneous
				// Hypothesse H1 : P1 > P2 (One tailed test - Right Tailed Probability = PValue)
				// in other word, the proportion of orthologs in the phenotype group "presence" is significatively higher than in the phenotype group "absence"
				// build the ContingencyTable2x2 with
				//				| Presence gene of interest	| Absence gene of interest	| Total
				// Phenotype +	| 				(a)			|			(b)				| 	alSelectedPresenceOrgaIds.size()
				// Phenotype -	| 				(c)			|			(d)				| 	alSelectedAbsenceOrgaIds.size()
				// Total		|hsSOrgaIdHaveHomolog.size()|							| 	
				//
				
				//online calclator : http://quantpsy.org/fisher/fisher.htm ; http://graphpad.com/quickcalcs/contingency1.cfm ; http://vassarstats.net/tab2x2.html
				

				// a
				int numberOrgaPhenotypePresenceThatHaveGeneIT = numberOfSorgaFromPhenotypePresenceFoundWithOrtholog + 1; // +1 because add orga de reference
				// b
				int numberOrgaPhenotypePresenceThatDoNotHaveGeneIT = alSelectedPhenoPlusOrgaIds.size() - numberOfSorgaFromPhenotypePresenceFoundWithOrtholog;
				// c
				int numberOrgaPhenotypeAbsenceThatHaveGeneIT = numberOfSorgaFromPhenotypeAbsenceFoundWithOrtholog ;
				// d
				int numberOrgaPhenotypeAbsenceThatDoNotHaveGeneIT = alSelectedPhenoMinusOrgaIds.size() - numberOfSorgaFromPhenotypeAbsenceFoundWithOrtholog;
				
				
				// for test purposes
//				// a
//				int numberOrgaPhenotypePresenceThatHaveGeneIT = 20; // +1 because add orga de reference
//				// b
//				int numberOrgaPhenotypePresenceThatDoNotHaveGeneIT = 12;
//				// c
//				int numberOrgaPhenotypeAbsenceThatHaveGeneIT = 12;
//				// d
//				int numberOrgaPhenotypeAbsenceThatDoNotHaveGeneIT = 10;
			
				
//				System.err.println(
//						" ** For geneId "+qGeneIdIT);
//				System.err.println("\tContingencyTable2x2 : "
//						+ " a (PhenoPresHaveGene) = "+numberOrgaPhenotypePresenceThatHaveGeneIT
//						+ " ; b (PhenoPresNotGene) = "+numberOrgaPhenotypePresenceThatDoNotHaveGeneIT
//						+ " ; c (PhenoAbsHaveGene) "+ numberOrgaPhenotypeAbsenceThatHaveGeneIT
//						+ " ; d (PhenoAbsNotGene) "+numberOrgaPhenotypeAbsenceThatDoNotHaveGeneIT
//						+ ")"
//						);
				FisherExactTest fisherExact = new FisherExactTest(
						numberOrgaPhenotypePresenceThatHaveGeneIT //int a,
						+ numberOrgaPhenotypePresenceThatDoNotHaveGeneIT//int b,
						+ numberOrgaPhenotypeAbsenceThatHaveGeneIT//int c,
						+ numberOrgaPhenotypeAbsenceThatDoNotHaveGeneIT//int d
						+ 10);
				//we want right-tail
			    double rightTailedP = fisherExact.getRightTailedP(
						numberOrgaPhenotypePresenceThatHaveGeneIT, //int a,
						numberOrgaPhenotypePresenceThatDoNotHaveGeneIT,//int b,
						numberOrgaPhenotypeAbsenceThatHaveGeneIT,//int c,
						numberOrgaPhenotypeAbsenceThatDoNotHaveGeneIT//int d
						);
			    
			    
			    // limit threshold is 0.05
			    if(rightTailedP < 0.05){
			    	qGeneId2probability.put(qGeneIdIT,rightTailedP);
			    }
				
				
				
				
				// OLD OR FLAWED
//			    // use of the binomial law
//				// RQ ifconsider not only success / failure but also unnown, use the Multinomial distribution ?
//			    // https://en.wikipedia.org/wiki/Binomial_distribution
//				// calculate 2 P-value 
//				// P-value = probability, under the assumption of hypothesis H, of obtaining a result
//				// equal to or more extreme than what was actually observed:
//				// the null hypothesis is that two categories are equally likely to occur (such as a coin toss).
//				// a one-tailed test is used if only deviations in one direction are considered possible
//				// https://en.wikipedia.org/wiki/One-_and_two-tailed_tests
//				// we consider each "Presence" organism as an independant experiment,
//				// and calculate P-Value (cumulativeProba P(X >= x)) using the BinomialDistribution
//				// The total number of success for the experiments is the number of organisms that have an homolog to the q_gene in test
//				// The proba of success is number of "Presence" organisms / total number of organism
//				// a significant "presence" result tends toward 0, a non significant result tends toward 1
//				// then same think for the "absence" group, excpet that we want the (1 - P-Value) so that a significant "absence" result tends toward 0
//				// then we take the average of the 2 P-value
//				// could not multiply the 2 proba because gives too much importance to very low proba
//				// for ex if 1 proba is 0 and other is 1, then multiply by give 0
//			    // Considering numberOfSuccessesInPopulation experiment,
//			    // with proba of success numberOfSuccessesInPopulation / populationSize
//			    // what is the proba of numberSuccessesInExperiments successes ??
//				// http://commons.apache.org/proper/commons-math/apidocs/org/apache/commons/math3/distribution/BinomialDistribution.html
//				//BinomialDistribution(int trials, double p)
//				//Create a binomial distribution with the given number of trials and probability of success.
//				Double probaSuccessesInExperiments = -1.0;
//				if(numberOfSuccessesInPopulation > 0){
//					
//					BinomialDistribution distribSuccesses = new BinomialDistribution(numberOfSuccessesInPopulation, probaSuccessInPopulation);
//					if(numberSuccessesInExperiments == 0){
//						probaSuccessesInExperiments = 1.0; // needed because we calculate ( numberSuccessesInExperiments - 1 ) so that we have P(X >= x)
//					} else {
//						// Rq use of numberSuccessesInExperiments - 1 because we want P(X >= x)
//						// and the cumulativeProbability method return P(X <= x)
//						probaSuccessesInExperiments = 1.0 - distribSuccesses.cumulativeProbability( ( numberSuccessesInExperiments - 1 ) );
//					}
//
//				}
//
//				
//			    // multiply by
//			    // proba inverse to not get any orga from the failure group
//			    // Considering numberOfFailuresInPopulation experiment 
//				// with proba of success ( numberOfFailuresInPopulation ) / populationSize
//				// what is 1 - proba of numberFailuresInExperiments successes ??
//				Double probaFailuresInExperiments = -1.0;
//				if(numberOfFailuresInPopulation > 0){
//					BinomialDistribution distribFailures = new BinomialDistribution(numberOfFailuresInPopulation, probaFailuresInPopulation);
//					probaFailuresInExperiments = distribFailures.cumulativeProbability( numberFailuresInExperiments );
//				}
//			
//				// then we take the average of the 2 P-value if both are defined
//				Double probIT = -1.0;
//				if(probaSuccessesInExperiments >= 0
//						&& probaFailuresInExperiments >= 0){
//					// then we take the average of the 2 P-value if both are defined
//					probIT = ( probaSuccessesInExperiments + probaFailuresInExperiments ) / 2 ;
//				// T O D O we want to multiply both cumultative proba
//					// pb 0 is too strong and change the other proba too much
//					// 1 on the contrary is neuttrl en do not chane other proba
//					// then do 1 - what I have right now and multiply htem ??
//					// or do a multiplication on each proba before cumulating them ?
//					// or write a question to a stat forum on my problem ?
//					
//					// https://www.probabilitycourse.com/chapter5/5_1_2_joint_cdf.php
//					
//					
//				} else if (probaSuccessesInExperiments >= 0) {
//					probIT = probaSuccessesInExperiments;
//				} else if (probaFailuresInExperiments >= 0) {
//					probIT = probaFailuresInExperiments;
//				} else {
//					UtilitiesMethodsServer.reportException("CallForInfoDBImpl getReferenceGenesAndOrthologsOverRepresentatedInPhenotype",
//							new Exception("both probaSuccessesInExperiments and  less or equal to 0"));
//				}
//				
//				
////				Double probIT = ( ( probaSuccessesInExperiments * numberOfSuccessesInPopulation )
////						+ ( (1.0 - probaFailuresInExperiments) * numberOfFailuresInPopulation ) )
////						/ (numberOfSuccessesInPopulation + numberOfFailuresInPopulation) ;
//
//				//System.err.println(
//				//		" * For geneId "+qGeneIdIT
//				//		+" => "+probIT+ " ( "
//				//		+ "numberSuccessesInExperiments = "+numberSuccessesInExperiments
//				//		+ " => probaSuccessesInExperiments = "+probaSuccessesInExperiments
//				//		+" numberFailuresInExperiments = "+ numberFailuresInExperiments
//				//		+ " => probaFailuresInExperiments = "+probaFailuresInExperiments
//				//		+ ")"
//				//		);
//			    
//				
//				
				
//				qGeneId2probability.put(qGeneIdIT, ??);
				
			}

//			System.err.println("\npopulationSize : "+populationSize
//				+ " ; number orga Pheno Presence : "+alSelectedPresenceOrgaIds.size()
//				+ " ; number orga Pheno Absence : "+alSelectedAbsenceOrgaIds.size()
//				);
			
			//RQ : need to do the sorting and convert to String seerver side as Double does not seem to be sent correctly via RPC ??
			LinkedHashMap<Integer, Double> sortedQGeneId2probability = UtilitiesMethodsShared.sortMapByDoubleValues(
					qGeneId2probability,
					false);
			LinkedHashMap<Integer, String> sortedQGeneId2probabilityAsString = new LinkedHashMap<>();
			for (Map.Entry<Integer, Double> entry : sortedQGeneId2probability.entrySet()) {
				sortedQGeneId2probabilityAsString.put(
						entry.getKey(),
						UtilitiesMethodsShared.formatNumberMax6DigitsAfterPointIfPossible(entry.getValue())
						);
			}
			objToReturn.setIntPrimaryIdAttributed2stScore(sortedQGeneId2probabilityAsString);
			
			ArrayList<LightGeneItem> alLGI = QueriesTableGenes.getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(conn,
					qGeneId2probability.keySet(),
					true);
			objToReturn.setAlItemsOfIntPrimaryIdAttribute(alLGI);
			
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}

		return objToReturn;
		
	}



	@Override
	public AlItemsOfIntPrimaryIdAttributePlusScore getReferenceGenesMostRepresentedInAlOrganisms(
			Integer referenceOrgaId
			, ArrayList<Integer> alSelectedPhenoPlusOrgaIds
			) throws Exception {
		
		String methodNameToReport = "CallForInfoDBImpl getReferenceGenesMostRepresentedInAlOrganisms"
				+ " ; referenceOrgaId="+referenceOrgaId
				+ " ; alSelectedPhenoPlusOrgaIds="+ ( (alSelectedPhenoPlusOrgaIds != null ) ? alSelectedPhenoPlusOrgaIds.toString() : "NULL" )
				;
		
		AlItemsOfIntPrimaryIdAttributePlusScore objToReturn = new AlItemsOfIntPrimaryIdAttributePlusScore();
		
		
//		long milli = System.currentTimeMillis();
//		long milliPrint2 = -1;
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Start method "+methodNameToReport);
		
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();

			//boolean doNotIncludeRefOrgaInResultsAkaNoParalogs = true;
			if (alSelectedPhenoPlusOrgaIds.isEmpty()){
				//doNotIncludeRefOrgaInResultsAkaNoParalogs = false; // get count paralogs
				alSelectedPhenoPlusOrgaIds.add(referenceOrgaId);
			}
			else if (alSelectedPhenoPlusOrgaIds.size() + 1 > UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined){
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("alSelectedPhenoPlusOrgaIds.size() + 1 is greater than "+UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined));
			}

			//get alAlignment_params_id with q orga id and al s orga ids
			ArrayList<Integer> alQElementIdsIT = QueriesTableElements.getAlElementIdWithOrganismId_optionalOrderBySizeDesc(
					conn
					, referenceOrgaId
					, false
					, true
					);
			HashSet<Integer> hsQElementIdsIT = new HashSet<>(alQElementIdsIT);
			

//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("intermediate 1 took\t" + milliPrint2 + "\tmilliseconds");
			
			
			HashSet<Long> hsAlignmentParamIdIT = QueriesTableAlignmentParams.getHsAlignmentParamId_withAlQElementIdsAndAlSOrgaIds(
					conn
					, hsQElementIdsIT
					, alSelectedPhenoPlusOrgaIds
					, false
					, true
					);
			
//			//hsQElementIdsIT.add(referenceOrgaId);
//			HashMap<Integer, HashSet<Long>> hmSOrgaId2AlAlignmentParamIdIT = QueriesTableAlignmentParams.getHmSOrgaId2AlAlignmentParamId_withAlQElementIdsAndAlSOrgaIds(
//					conn
//					, hsQElementIdsIT
//					, alSelectedPhenoPlusOrgaIds
//					, false
//					, true
//					);
//			HashSet<Long> hsAlignmentParamIdIT = new HashSet<>();
//			for (Entry<Integer, HashSet<Long>> entrySOrgaId2 : hmSOrgaId2AlAlignmentParamIdIT.entrySet()) {
//				hsAlignmentParamIdIT.addAll(entrySOrgaId2.getValue());
//			}
			
			
			//get list alignment_id with alAlignment_params_id
			HashSet<Long> hsAlignmentIdsIT = QueriesTableAlignments.getListAlignmentIdsWithAlAlignmentParamId(
					conn
					, hsAlignmentParamIdIT
					, true
					, false
					);
			//getHmqGeneId2HsGeneIdsHomologsWithAlAlignmentsids
			HashMap<Integer, HashSet<Integer>> qGeneId2hsGeneIdsHomologs =	QueriesTableAlignmentPairs.getHmQGeneId2HsGeneIdsHomologs_WithHsAlignmentIds(
					conn
					, hsAlignmentIdsIT
					, true
					);
						
			// OLD TOO SLOW ??
//			HashMap<Integer, HashSet<Integer>> qGeneId2hsGeneIdsHomologs = QueriesTableHomologs.getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId(
//								conn
//								, referenceOrgaId // referenceOrgaId
//								, null // optionalListQGeneIdsToRestrictSearch
//								, alSelectedPhenoPlusOrgaIds // optionalListSOrgaIdsToRestrictSearch
//								, true //doNotIncludeRefOrgaInResultsAkaNoParalogs
//								, false
//							);
			

//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("intermediate 1 took\t" + milliPrint2 + "\tmilliseconds");
			
			//create the count
			LinkedHashMap<Integer, Integer> qGeneId2countHomologs = new LinkedHashMap<>();
			for (Entry<Integer, HashSet<Integer>> entry : qGeneId2hsGeneIdsHomologs.entrySet()) {
				qGeneId2countHomologs.put(
						entry.getKey(),
						entry.getValue().size()
						);
			}
			objToReturn.setIntPrimaryIdAttributed2intScore(qGeneId2countHomologs);
			
			ArrayList<LightGeneItem> alLGI = QueriesTableGenes.getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(conn,
					qGeneId2countHomologs.keySet(),
					true);
			
			objToReturn.setAlItemsOfIntPrimaryIdAttribute(alLGI);

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}


//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Overall it took\t" + milliPrint2 + "\tmilliseconds");
		
		return objToReturn;
		
	}

	@Override
	public int getDatabaseIsGenesElementsGenomeAssemblyConvenience() throws Exception {
		String methodNameToReport = "CallForInfoDBImpl getDatabaseIsGenesElementsGenomeAssemblyConvenience";
		if (QueriesDatabaseMetaData.DATABASE_IS_genes_elements_genome_assembly_convenience == 0) {
			QueriesDatabaseMetaData.getDatabaseIsGenesElementsGenomeAssemblyConvenience(null);
		}
		if (QueriesDatabaseMetaData.DATABASE_IS_genes_elements_genome_assembly_convenience == 1
				|| QueriesDatabaseMetaData.DATABASE_IS_genes_elements_genome_assembly_convenience == -1) {
			return QueriesDatabaseMetaData.DATABASE_IS_genes_elements_genome_assembly_convenience;
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("DATABASE_IS_genes_elements_genome_assembly_convenience is neither 1 or -1")
					);
			return 0;
		}
	}

	@Override
	public int getDatabaseIsNoMirror() throws Exception {
		String methodNameToReport = "CallForInfoDBImpl getDatabaseIsNoMirror";
		if (QueriesDatabaseMetaData.DATABASE_IS_NO_MIRROR == 0) {
			QueriesDatabaseMetaData.getDatabaseIsNoMirror(null);
		}
		if (QueriesDatabaseMetaData.DATABASE_IS_NO_MIRROR == 1
				|| QueriesDatabaseMetaData.DATABASE_IS_NO_MIRROR == -1) {
			return QueriesDatabaseMetaData.DATABASE_IS_NO_MIRROR;
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("DATABASE_IS_NO_MIRROR is neither 1 or -1")
					);
			return 0;
		}
	}



	@Override
	public String getStatSummaryRefCDS(
			HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneName //contains only 1 qOrganismId, 1 qGeneId and 1 qGeneName
			, HashSet<Integer> hsOrgaIdsFeaturedGenomes
			//, HashSet<Integer> hsOrgaIdsMainList // no main list, only featured ; cause problem for private genome for main list
			) throws Exception {

		String listQOrganismId2GeneId = "[";
		String separator = "";
		for (Map.Entry<Integer, HashMap<Integer, String>> entryqOrganismId2 : qOrganismId2qGeneId2qGeneName.entrySet()) {
			Integer qOrganismId = entryqOrganismId2.getKey();
			for (Map.Entry<Integer, String> entryqGeneId2 : entryqOrganismId2.getValue().entrySet()) {
				Integer qGeneId = entryqGeneId2.getKey();
				listQOrganismId2GeneId += separator+qOrganismId+"-"+qGeneId;
				separator = ",";
			}
		}
		listQOrganismId2GeneId += "]";
		
		String methodNameToReport = "CallForInfoDBImpl getStatSummaryRefCDS"
				+ " ; qOrganismId-GeneId="+listQOrganismId2GeneId
				+ " ; hsOrgaIdsFeaturedGenomes.size()=" + ( (hsOrgaIdsFeaturedGenomes != null ) ? hsOrgaIdsFeaturedGenomes.size() : "NULL" )
				//+ " ; hsOrgaIdsMainList.size()=" + ( (hsOrgaIdsMainList != null ) ? hsOrgaIdsMainList.size() : "NULL" )		
				+ " ; hsOrgaIdsFeaturedGenomes=" + ( (hsOrgaIdsFeaturedGenomes != null ) ? hsOrgaIdsFeaturedGenomes.toString() : "NULL" )
				//+ " ; hsOrgaIdsMainList=" + ( (hsOrgaIdsMainList != null ) ? hsOrgaIdsMainList.toString() : "NULL" )
				;
		
		//System.err.println(methodNameToReport);

		String stToReturn = "";
		
		Connection conn = null;
		try {
			conn = DatabaseConf.getConnection_db();
			
			stToReturn = ComparativeGenomics.getStatSummaryRefCDS_asString(
					conn
					, qOrganismId2qGeneId2qGeneName //contains only 1 qOrganismId, 1 qGeneId and 1 qGeneName
					, hsOrgaIdsFeaturedGenomes
					);
			
//
//			for (Map.Entry<Integer, HashMap<Integer, String>> entryQOrganismId2 : qOrganismId2qGeneId2qGeneName.entrySet()) {
//			    int qOrganismIdIT = entryQOrganismId2.getKey();
//			    HashMap<Integer, String> qGeneId2qGeneNameIT = entryQOrganismId2.getValue();
//			    
//			    for (Map.Entry<Integer, String> entryQGeneId2 : qGeneId2qGeneNameIT.entrySet()) {
//				    int qGeneIdIT = entryQGeneId2.getKey();
//				    String qGeneNameIT = entryQGeneId2.getValue();
//				   
//				    stToReturn += "Summary conservation statistics for the <b>reference CDS "+qGeneNameIT
//				    		//+ " (gene id = "+qGeneIdIT+" ; organism id = "+qOrganismIdIT+")"
//				    		+ ":</b>";
//				    
//					GeneItem geneItemIT = QueriesTableGenes.getGeneItemWithGeneId(conn, qGeneIdIT);
////					String accnumIT = "";
////					if (geneItemIT.getAccession() != null && ! geneItemIT.getAccession().isEmpty()) {
////						accnumIT = geneItemIT.getAccession();
////					} else {
////						if (geneItemIT.getElementId() > 0) {
////							accnumIT = QueriesTableElements.getAccessionWithElementId(conn, geneItemIT.getElementId(), true);
////						}
////					}
//					
//				    HashSet<Integer> hsQGeneIdIT = new HashSet<>();
//				    hsQGeneIdIT.add(qGeneIdIT);
//
//					HashSet<Long> hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList = new HashSet<>();
//					if ( ! hsOrgaIdsFeaturedGenomes.isEmpty()) {
//						hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList = QueriesTableAlignmentParams.getHsAlignmentParamId_withQOrganismIdAndHsSOrganismId(
//								conn
//								, qOrganismIdIT
//								, hsOrgaIdsFeaturedGenomes
//								, false //isMirrorRequest
//								);
//					}
//					
//					//System.err.println("hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList.size()="+hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList.size());
//					//System.err.println("hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList="+hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList.toString());
////					Long toTEST = -170631L;
////					if (hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList.contains(toTEST)) {
////						System.err.println("ok contain toTEST="+toTEST);
////					} else {
////						System.err.println("NOT ok DO NOT contain toTEST="+toTEST);
////					}
//										
////					HashSet<Long> HsAlignmentIdRelatedToQOrganismIdITAndFeaturedGenomesList = 
////							QueriesTableAlignments.getListAlignmentIdsWithAlAlignmentParamId(
////									conn
////									, hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList
////									, true //convertSetLongToSortedRangeItem_allowGapWithinRange
////									, false //shouldBePresentInTable
////									);
//					
//					HashSet<Integer> hsTypesIT = new HashSet<>();
//					hsTypesIT.add(1);
//					hsTypesIT.add(2);
//
//					HashMap<Integer, HashSet<Long>> hmQGeneId2HsAlignmentId = 
//							QueriesTableAlignmentPairs.getHmQGeneId2HsAlignmentId_withHsGeneId(
//									conn
//									, hsQGeneIdIT
//									, hsTypesIT
//									, false //isMirrorRequest
//									);
//					HashSet<Long> consolidatedHsAlignmentIdForAllQueryGenes = new HashSet<>();
//					for (Map.Entry<Integer, HashSet<Long>> entryHmQGeneId2 : hmQGeneId2HsAlignmentId.entrySet()) {
//					    //int qGeneIdIT = entryHmQGeneId2.getKey();
//						//HashSet<Long> HsAlignmentIdIT = entryHmQGeneId2.getValue();
//						consolidatedHsAlignmentIdForAllQueryGenes.addAll(entryHmQGeneId2.getValue());
//					}
//
//
//					//System.err.println("consolidatedHsAlignmentIdForAllQueryGenes="+consolidatedHsAlignmentIdForAllQueryGenes.size());
//					
//					//al idx0 = consolidatedHsAlignmentParamsIdForAllQueryGenes_singleton
//					//al idx0 = consolidatedHsAlignmentParamsIdForAllQueryGenes_synteny
//					ArrayList<HashSet<Long>> alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1 = 
//					//HashSet<Long> consolidatedHsAlignmentParamsIdForAllQueryGenes = 
//							QueriesTableAlignments.getAlHsAlignmentParamId_SingletonIdx0SyntenyIdx1_withHsAlignmentId(
//									conn
//									, consolidatedHsAlignmentIdForAllQueryGenes
//									, true // convertSetLongToSortedRangeItem_allowGapWithinRange
//									, false // shouldBePresentInTable
//									);
//
//					//System.err.println("consolidatedHsAlignmentParamsIdForAllQueryGenes="+consolidatedHsAlignmentParamsIdForAllQueryGenes.size());
//					HashSet<Long> alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1.get(0));
//					alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all.addAll(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1.get(1));
//					//System.err.println("alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all.size()="+alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all.size());
//					//System.err.println("alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all="+alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all.toString());
//					HashSet<Long> alConsolidatedHsAlignmentParamsIdForAllQueryGenes_syntenyOnly = alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1.get(1);
//					
//					HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all);
//					///HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all);
//					hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.retainAll(hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList);
//					//hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.removeAll(hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList);
//					
//					//System.err.println("hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.size()="+hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.size());
//					//System.err.println("hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.size()="+hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.size());
//					//System.err.println("hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList="+hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.toString());
//					//System.err.println("hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList="+hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.toString());
//					
//					int featuredGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.size();
//					//int mainGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.size();
//
//					HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_syntenyOnly);
//					//HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_mainGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_syntenyOnly);
//					hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList.retainAll(hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList);
//					//hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_mainGenomesList.removeAll(hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList);
//					
//					int featuredGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship = hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList.size();
//					//int mainGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship = hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_mainGenomesList.size();
//					
//					Double featuredGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution = 0D;
//					Double featuredGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = 0D;
//					Double featuredGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution = 0D;
//					if ( ! hsOrgaIdsFeaturedGenomes.isEmpty() ) {
//						// idx 0 : Avg % identity of alignments
//						// idx 1 : Stdev % identity of alignments
//						// idx 2 : Avg % query and subject lenght coverage of alignments
//						// idx 3 : Stdev % query and subject lenght coverage of alignments
//						// idx 4 : Median Evalue of alignments
//						// idx 5 : Min Evalue of alignments
//						// idx 6 : Max Evalue of alignments
//						ArrayList<Double> alAvgStdevBasicAlignementStats_featuredGenomesList = 
//								QueriesTableHomologs.getAvgStdevBasicAlignementStats_withHsQGeneId_optionalHsSOrgaIdsAndRank(
//									conn
//									, hsQGeneIdIT
//									, hsOrgaIdsFeaturedGenomes
//									, null //hsRank
//									, false //calculateStdev
//									, false //calculateMinMaxEvalue
//									//, false //isMirrorRequest
//								);
//						featuredGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(0);
//						//Double featuredGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(1);
//						featuredGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(2);
//						//Double featuredGenomesList_stdevQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(3);
//						featuredGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(4);
//						//Double featuredGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(5);
//						//Double featuredGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(6);
//						
//					}
//					
//					
////					// idx 0 : Avg % identity of alignments
////					// idx 1 : Stdev % identity of alignments
////					// idx 2 : Avg % query and subject lenght coverage of alignments
////					// idx 3 : Stdev % query and subject lenght coverage of alignments
////					// idx 4 : Median Evalue of alignments
////					// idx 5 : Min Evalue of alignments
////					// idx 6 : Max Evalue of alignments
////					ArrayList<Double> alAvgStdevBasicAlignementStats_mainGenomesList = 
////							QueriesTableHomologs.getAvgStdevBasicAlignementStats_withHsQGeneId_optionalHsSOrgaIdsAndRank(
////								conn
////								, hsQGeneIdIT
////								, hsOrgaIdsMainList
////								, null //hsRank
////								, false //calculateStdev
////								, false //calculateMinMaxEvalue
////								//, false //isMirrorRequest
////							);
////					Double mainGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(0);
////					//Double mainGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(1);
////					Double mainGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(2);
////					//Double mainGenomesList_stdevQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(3);
////					Double mainGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(4);
////					//Double mainGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(5);
////					//Double mainGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(6);
//					
//					
//					String nucSequencesHsGenes = QueriesTableProtAndNucSequences.getNucleicAcidSequenceAsFastaWithAlGeneIds(
//							conn
//							, hsQGeneIdIT
//							, false //withHeader
//							, false //lineSeparatorInSeq
//							);
//					int countSizeSeq = nucSequencesHsGenes.length();
//					int countG = StringUtils.countMatches(nucSequencesHsGenes, "G");
//					int countC = StringUtils.countMatches(nucSequencesHsGenes, "C");
//					int countg = StringUtils.countMatches(nucSequencesHsGenes, "g");
//					int countc = StringUtils.countMatches(nucSequencesHsGenes, "c");
//					int countGC = countG + countC + countg + countc;
////					System.err.println(nucSequencesHsGenes);
////					System.err.println("countG="+countG);
////					System.err.println("countC="+countC);
////					System.err.println("countGC="+countGC);
////					System.err.println("nucSequencesHsGenes.length()="+nucSequencesHsGenes.length());
//					
//					Double percentGCrefCDS = (double) ( ( (double) countGC / (double) countSizeSeq ) * (double) 100 ) ;
//					
//					DecimalFormat df = new DecimalFormat("##.##");
//					//stToReturn += "<br/>";
//					stToReturn += ""
//							//+ "<br/>"
//							//+ "<br/><b><i>Locus tag:</i></b> " + geneItemIT.getLocusTagAsStrippedString()
//							//+ "<br/><b><i>Protein id:</i></b> " + geneItemIT.getListProteinId().toString()
//							+ "<br/><b><i>List product:</i></b> " + geneItemIT.getListProduct().toString()
//							+ "<br/><b><i>Length residues:</i></b> " + geneItemIT.getLengthResidues()
//							+ "<br/><b><i>% GC:</i></b> " + df.format(percentGCrefCDS)
//							//+ "<br/><b><i>Accession:</i></b> " + accnumIT
//							+ "<br/>"
//							;
//					//stToReturn += "<br/><b><i>% GC reference CDS:</i></b> "+df.format(percentGCrefCDS) + "%";
//					
//					if ( ! hsOrgaIdsFeaturedGenomes.isEmpty()) {
//						stToReturn += "<br/><b><i>Comparison with "+hsOrgaIdsFeaturedGenomes.size()+" genomes from the featured list:</i></b><ul>";
//						double featuredGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = ( (double) featuredGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution / (double) hsOrgaIdsFeaturedGenomes.size() ) * (double) 100;
//						stToReturn += "<li>"
//								+ "Number of genomes with presence of at least 1 CDS conserved during evolution: "+featuredGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution 
//								+ " (" + df.format(featuredGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution) + "%)"
//								+ "</li>" ;
//						double featuredGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = ( (double) featuredGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship / (double) hsOrgaIdsFeaturedGenomes.size() ) * (double) 100;
//						stToReturn += "<li>"
//								+ "Number of genomes with presence of at least 1 CDS in synteny relationship: "+ featuredGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship 
//								+ " (" + df.format(featuredGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship) + "%)"
//								+ "</li>" ;
//						stToReturn += "<li>Average % identity for the alignment(s): "+df.format(featuredGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution) + "%</li>";
////						stToReturn += "<li>Standard deviation % identity for the alignment(s): "+df.format(featuredGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution) + "%</li>";
//						stToReturn += "<li>Average % query and subject lenght coverage for the alignment(s): "+df.format(featuredGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
////						stToReturn += "<li>Standard deviation % query and subject lenght coverage for the alignment(s): "+df.format(featuredGenomesList_stdevQueryLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
////						stToReturn += "<li>Average % subject lenght coverage for the alignment(s): "+df.format(featuredGenomesList_avgSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
////						stToReturn += "<li>Standard deviation % subject lenght coverage for the alignment(s): "+df.format(featuredGenomesList_stdevSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
//						stToReturn += "<li>Median Evalue for the alignment(s): "+featuredGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
////						stToReturn += "<li>Min Evalue for the alignment(s): "+featuredGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
////						stToReturn += "<li>Max Evalue for the alignment(s): "+featuredGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
//						stToReturn += "</ul>";
//					} else {
//						stToReturn += "<br/><b><i>There is no genome in your featured list. Please add to your featured list the genomes you would like to calculate the summary conservation statistics for.</i></b>";
//					}
//	
//					//if ( ! hsOrgaIdsMainList.isEmpty()) {
//						//stToReturn += "<br/><b><i>Comparison with "+hsOrgaIdsMainList.size()+" genomes from the main list:</i></b><ul>";
//						//double mainGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = ( (double) mainGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution / (double) hsOrgaIdsMainList.size() ) * (double) 100;
//						//stToReturn += "<li>Number of genomes with presence of at least 1 CDS conserved during evolution: "+mainGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution + " (" + df.format(mainGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution) + "%)</li>" ; // give count and %
//						//double mainGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = ( (double) mainGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship / (double) hsOrgaIdsMainList.size() ) * (double) 100;
//						//stToReturn += "<li>Number of genomes with presence of at least 1 CDS in synteny relationship: "+ mainGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship + " (" + df.format(mainGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship) + "%)</li>" ; // give count and %
//						//stToReturn += "<li>Average % identity for the alignment(s): "+df.format(mainGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution) + "%</li>";
//	//					stToReturn += "<li>Standard deviation % identity for the alignment(s): "+df.format(mainGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution) + "%</li>";
//						//stToReturn += "<li>Average % query and subject lenght coverage for the alignment(s): "+df.format(mainGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
//	//					stToReturn += "<li>Standard deviation % query and subject lenght coverage for the alignment(s): "+df.format(mainGenomesList_stdevQueryLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
//	//					stToReturn += "<li>Average % subject lenght coverage for the alignment(s): "+df.format(mainGenomesList_avgSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
//	//					stToReturn += "<li>Standard deviation % subject lenght coverage for the alignment(s): "+df.format(mainGenomesList_stdevSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
//						//stToReturn += "<li>Median Evalue for the alignment(s): "+mainGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
//	//					stToReturn += "<li>Min Evalue for the alignment(s): "+mainGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
//	//					stToReturn += "<li>Max Evalue for the alignment(s): "+mainGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
//						//stToReturn += "</ul>";
//					//}
//
//
//									    
//				}
//			}
			
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}

		return stToReturn;
		
	}





}
