package fr.inra.jouy.server;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import com.google.gwt.core.client.GWT;
import fr.inra.jouy.server.queriesTable.QueriesDatabaseMetaData;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import fr.inra.jouy.server.objects.ObjSQLCommand;
//import fr.inra.jouy.shared.interfaceIs.IsMappingKeyValueToFields;

public class UtilitiesMethodsServer {
	
	public static int PRINT_SQL_QUERY_LONGUER_THAN_XX_MILLISECONDS = -1; //set to -1 to not print any, set to 0 to print all
	
	public static void reportException(String method, Exception ex) throws Exception {
		String textFromThrowable = "Error in "+method+": \n";
		StackTraceElement[] stackTraceElements = ex.getStackTrace();
		textFromThrowable += ex.toString() + "\n";
		for (int i = 0; i < stackTraceElements.length; i++) {
			textFromThrowable += "    at " + stackTraceElements[i] + "\n";
		}
		System.err.println(textFromThrowable + "\nwith message :\n"+ ex.getMessage());
		throw new Exception(textFromThrowable);
	}


	public static ResultSet executeAndLogSQLQuery(Statement statement, ResultSet rs, String command,
			String methodFrom) throws Exception {
		return executeAndLogSQLQuery(statement, rs, command, methodFrom, false);
	}
	
	public static ResultSet executeAndLogSQLQuery(Statement statement, ResultSet rs, String command,
			String methodFrom, boolean forcePrint) throws Exception {
		long milliPrint1 = 0;
		if(PRINT_SQL_QUERY_LONGUER_THAN_XX_MILLISECONDS >= 0 || forcePrint){
			milliPrint1 = System.currentTimeMillis();
		}
		if (command.contains(";")) {
			reportException(methodFrom, new Exception("executeAndLogSQLQuery found that the string "+command+" contains a semi-colon. This is not allowed."));
		} else {
			rs = statement.executeQuery(command);
		}
		if(PRINT_SQL_QUERY_LONGUER_THAN_XX_MILLISECONDS >= 0 || forcePrint){
			long milliPrint2 = System.currentTimeMillis() - milliPrint1;
			if(milliPrint2 >= PRINT_SQL_QUERY_LONGUER_THAN_XX_MILLISECONDS || forcePrint){
				System.out.println(methodFrom+"\t"+command+"\ttook\t" + milliPrint2 + "\tmilliseconds");
				GWT.log(methodFrom+"\t"+command+"\ttook\t" + milliPrint2 + "\tmilliseconds");
			}
		}
		return rs;
	}
	

	public static String getGeneNameAndLocusTagQualifiers() {
		return "micado.qualifiers.type_qual = 'locus_tag' OR micado.qualifiers.type_qual = 'gene' OR micado.qualifiers.type_qual = 'gene_name'";
	}
	

	public static String prepareStringToBeRegexSearchString(String stringToDW) {

		stringToDW = stringToDW.replaceAll("\\[", "\\~");
		stringToDW = stringToDW.replaceAll("\\]", "\\~");
		stringToDW = stringToDW.replaceAll("\\^", "\\~");
		stringToDW = stringToDW.replaceAll("\\\\", "\\~");

		stringToDW = stringToDW.replaceAll("\\.", "[.]");
		stringToDW = stringToDW.replaceAll("\\+", "[+]");
		stringToDW = stringToDW.replaceAll("\\?", "[?]");
		stringToDW = stringToDW.replaceAll("\\*", "[*]");
		stringToDW = stringToDW.replaceAll("\\{", "[{]");
		stringToDW = stringToDW.replaceAll("\\}", "[}]");
		stringToDW = stringToDW.replaceAll("\\,", "[,]");
		stringToDW = stringToDW.replaceAll("\\(", "[(]");
		stringToDW = stringToDW.replaceAll("\\)", "[)]");

		// tilda character substitute for any one char
		stringToDW = stringToDW.replaceAll("~", ".");
		// $ char substitute for any string of any length including 0 length
		stringToDW = stringToDW.replaceAll("\\$", ".*");

		return stringToDW;

	}


	public static String getParamsOrthoHomoGapToUseAsStringForSQLQuery() {
		
		//initial params pour 408 genomes 2013 : 
		//ortho_score = 4.0
		//homo_score = 2.0
		//mismatch_penalty = -4.0
		//gap_creation_penalty = -8.0
		//gap_extension_penalty = -2.0
		//min_align_size = 1
		//min_score = 8.0
		//orthologs_included = true
		
//		return " AND ( alignment_params.ortho_score = 4.0" +
//				" AND alignment_params.homo_score = 2.0" +
//				" AND alignment_params.mismatch_penalty = -4.0" +
//				" AND alignment_params.gap_creation_penalty = -8.0" +
//				" AND alignment_params.gap_extension_penalty = -2.0" +
//				" AND alignment_params.min_align_size = 1" +
//				" AND alignment_params.min_score = 8.0" +
//				" AND alignment_params.orthologs_included = true )";
		

		return "";
		
	}


	public static boolean checkValidFreeStringInput(String methodFrom, String stringToTest, String typeOfStringToReport,
			boolean allowNull, boolean allowEmpty) throws Exception{
		boolean stringIsValidAndNotNullOrEmpty = false;
		if (stringToTest == null) {
			if(!allowNull){
				reportException(methodFrom, new Exception("checkValidFreeStringInput found that the string "+typeOfStringToReport+" is null. This is not allowed."));
			}
		} else {
			if (stringToTest.isEmpty()) {
				if(!allowEmpty){
					reportException(methodFrom, new Exception("checkValidFreeStringInput found that the string "+typeOfStringToReport+" is empty. This is not allowed."));
				}
			} else {
				if (stringToTest.contains(";")) {
					reportException(methodFrom, new Exception("checkValidFreeStringInput found that the string "+typeOfStringToReport+" contains a semi-colon. This is not allowed."));
				} else {
					stringIsValidAndNotNullOrEmpty = true;
				}
			}
		}
		return stringIsValidAndNotNullOrEmpty;
	}

	// can cause limit overhead problems
	/*
	public static <T extends IsMappingKeyValueToFields> T excecuteCommandAndParseUniqueRowRS(Connection conn,
	//			ArrayList<String> stringColumns,
	//			ArrayList<String> intColumns,
	//			String command,
				Class<T> classToInstanciate,
				ObjSQLCommand objSQLCommandIT,
				boolean shouldBePresentInTable,
				boolean shouldBeNotEmptyOrZero,
				String methodNameToReport) throws Exception {
			
			ArrayList<T> alIntermediate = UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS(
					conn,
	//				ArrayList<String> stringColumns,
	//				ArrayList<String> intColumns,
	//				String command,
					classToInstanciate,
					objSQLCommandIT,
					shouldBePresentInTable,
					shouldBeNotEmptyOrZero,
					methodNameToReport
					);
			
			if(alIntermediate.size() == 1){
				return alIntermediate.get(0);
			} else if (alIntermediate.isEmpty() && !shouldBePresentInTable){
				T emptyOAAM = classToInstanciate.newInstance();
				return emptyOAAM;
			} else {
				reportException(methodNameToReport,
						new Exception("Not a unique result for query : " + objSQLCommandIT.getSQLCommand())
				);
				return null;
			}
			
	}

	
	public static <T,U> HashMap<T, U> excecuteCommandAndParseMultipleRowRS_HM(
			Connection conn
			, String keyColumnName
			, Class<T> keyClassToInstanciate
			, String valueColumnName
			, Class<U> valueClassToInstanciate
			, ObjSQLCommand objSQLCommandIT
			, boolean shouldBePresentInTable
			, boolean shouldBeNotEmptyOrZero
			, boolean shouldNotFindDuplicateKeys
			, boolean storeKeysAsNegativeIfNumber
			, boolean storeValuesAsNegativeIfNumber
			, String methodNameToReport
			) throws Exception {

		methodNameToReport += "("+objSQLCommandIT.getSQLCommand()+")";
		HashMap<T, U> hmToReturn = new HashMap<>();
		
		checkValidFreeStringInput(
				methodNameToReport,
				objSQLCommandIT.getSQLCommand(), "command", false, false);

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			rs = executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);

			hmToReturn = objSQLCommandIT.parseRsIntoHM(
					keyColumnName
					, keyClassToInstanciate
					, valueColumnName
					, valueClassToInstanciate
					, rs
					, shouldBeNotEmptyOrZero
					, shouldNotFindDuplicateKeys
					, storeKeysAsNegativeIfNumber
					, storeValuesAsNegativeIfNumber
					, methodNameToReport
					);
			
			if (shouldBePresentInTable && hmToReturn.isEmpty()) {
				reportException(methodNameToReport,
						new Exception("No row returned for " + objSQLCommandIT.getSQLCommand())
				);
			}
			
		} catch (Exception ex) {
			reportException(methodNameToReport, ex);
		} finally {
			closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return hmToReturn;
		
	}
	
		
		
		
	public static <T,U> HashMap<T, HashSet<U>> excecuteCommandAndParseMultipleRowRS_HMofHS(
			Connection conn,
			String keyColumnName,
			Class<T> keyClassToInstanciate,
			String valueColumnName,
			Class<U> valueClassToInstanciate,
			ObjSQLCommand objSQLCommandIT,
			boolean shouldBePresentInTable,
			boolean shouldBeNotEmptyOrZero,
			String methodNameToReport) throws Exception {
		
		methodNameToReport += "("+objSQLCommandIT.getSQLCommand()+")";
		HashMap<T, HashSet<U>> hmToReturn = new HashMap<>();
		
		checkValidFreeStringInput(
				methodNameToReport,
				objSQLCommandIT.getSQLCommand(), "command", false, false);

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			rs = executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);

			hmToReturn = objSQLCommandIT.parseRsIntoHMofHS(
					keyColumnName,
					keyClassToInstanciate,
					valueColumnName,
					valueClassToInstanciate,
					rs,
					shouldBeNotEmptyOrZero,
					methodNameToReport);
			
			if (shouldBePresentInTable && hmToReturn.isEmpty()) {
				reportException(methodNameToReport,
						new Exception("No row returned for " + objSQLCommandIT.getSQLCommand())
				);
			}
			
		} catch (Exception ex) {
			reportException(methodNameToReport, ex);
		} finally {
			closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return hmToReturn;
		
	}

	

	public static ArrayList<Long> excecuteCommandAndParseMultipleRowRS_alLong(
			Connection conn
			, String columnIntToParse
			, ObjSQLCommand objSQLCommandIT
			, boolean shouldBePresentInTable
			, boolean shouldBeNotEmptyOrZero
			, boolean storeAsNegativeValue
			, String methodNameToReport
			) throws Exception {

		ArrayList<Long> alToReturn = new ArrayList<>();

		methodNameToReport += "("+objSQLCommandIT.getSQLCommand()+")";

		checkValidFreeStringInput(
				methodNameToReport,
				objSQLCommandIT.getSQLCommand(), "command", false, false);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			rs = executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			alToReturn = objSQLCommandIT.parseRsIntoAlLong(
					rs
					, columnIntToParse
					, shouldBeNotEmptyOrZero
					, storeAsNegativeValue
					, methodNameToReport
					);
		} catch (Exception ex) {
			reportException(methodNameToReport, ex);
		} finally {
			closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		if (shouldBePresentInTable && alToReturn.isEmpty()) {
			reportException(methodNameToReport,
					new Exception("No row returned for " + objSQLCommandIT.getSQLCommand())
			);
		}
		return alToReturn;
	}
	
	
	public static ArrayList<Integer> excecuteCommandAndParseMultipleRowRS_alInt(
			Connection conn,
			String columnIntToParse,
			ObjSQLCommand objSQLCommandIT,
			boolean shouldBePresentInTable,
			boolean shouldBeNotEmptyOrZero,
			String methodNameToReport
			) throws Exception {

		ArrayList<Integer> alToReturn = new ArrayList<>();

		methodNameToReport += "("+objSQLCommandIT.getSQLCommand()+")";

		checkValidFreeStringInput(
				methodNameToReport,
				objSQLCommandIT.getSQLCommand(), "command", false, false);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			rs = executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			alToReturn = objSQLCommandIT.parseRsIntoAlInt(rs,
					columnIntToParse,
					shouldBeNotEmptyOrZero,
					methodNameToReport);
		} catch (Exception ex) {
			reportException(methodNameToReport, ex);
		} finally {
			closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		if (shouldBePresentInTable && alToReturn.isEmpty()) {
			reportException(methodNameToReport,
					new Exception("No row returned for " + objSQLCommandIT.getSQLCommand())
			);
		}
		return alToReturn;
	}
	
	public static ArrayList<String> excecuteCommandAndParseMultipleRowRS_alString(
			Connection conn,
			String columnStringToParse,
			ObjSQLCommand objSQLCommandIT,
			boolean shouldBePresentInTable,
			boolean shouldBeNotEmptyOrZero,
			String methodNameToReport
			) throws Exception {

		ArrayList<String> alToReturn = new ArrayList<>();

		methodNameToReport += "("+objSQLCommandIT.getSQLCommand()+")";

		checkValidFreeStringInput(
				methodNameToReport,
				objSQLCommandIT.getSQLCommand(), "command", false, false);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			rs = executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			alToReturn = objSQLCommandIT.parseRsIntoAlString(rs,
					columnStringToParse,
					shouldBeNotEmptyOrZero,
					methodNameToReport);
		} catch (Exception ex) {
			reportException(methodNameToReport, ex);
		} finally {
			closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		if (shouldBePresentInTable && alToReturn.isEmpty()) {
			reportException(methodNameToReport,
					new Exception("No row returned for " + objSQLCommandIT.getSQLCommand())
			);
		}
		return alToReturn;
	}

	

	// general methods to query db
	public static <T extends IsMappingKeyValueToFields> ArrayList<T> excecuteCommandAndParseMultipleRowRS(Connection conn,
				Class<T> classToInstanciate,
				ObjSQLCommand objSQLCommandIT,
				boolean shouldBePresentInTable,
				boolean shouldBeNotEmptyOrZero,
				String methodNameToReport) throws Exception {

		ArrayList<T> alToReturn = new ArrayList<T>();

		methodNameToReport += "("+objSQLCommandIT.getSQLCommand()+")";

		checkValidFreeStringInput(
				methodNameToReport,
				objSQLCommandIT.getSQLCommand(), "command", false, false);
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			rs = executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);

			alToReturn = objSQLCommandIT.parseRsIntoClassExtendsAbstractFieldsMapper(
					classToInstanciate,
					rs,
					shouldBeNotEmptyOrZero,
					methodNameToReport);
			
		} catch (Exception ex) {
			reportException(methodNameToReport, ex);
		} finally {
			closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		if (shouldBePresentInTable && alToReturn.isEmpty()) {
			reportException(methodNameToReport,
					new Exception("No row returned for " + objSQLCommandIT.getSQLCommand())
			);
		}
		return alToReturn;
			
	}
*/
	
	
	public static void closeRsAndStatement(ResultSet rs, Statement statement, String methodNameToReport) throws Exception {
		try {
			if(rs != null){
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			}
		} catch (Exception ex) {
			reportException(methodNameToReport+" rs.close", ex);
		}
		try {
			if(statement != null){
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			}
		} catch (Exception ex) {
			reportException(methodNameToReport+" statement.close", ex);
		}
	}


	public static Exception dealWithServerSideThrowable(Throwable e) {
		
		e.printStackTrace();
		
		String textFromThrowable = "";
		StackTraceElement[] ste = e.getStackTrace();
		textFromThrowable += "\n" + e.toString() + "\n";
		for (int i = 0; i < ste.length; i++) {
			textFromThrowable += "    at " + ste[i] + "\n";
		}
		int loopCount = 0;
		while (e.getCause() != null) {
			loopCount++;
			e = e.getCause();
			textFromThrowable += "\n" + e.toString() + "\n";
			StackTraceElement[] steCause = e.getStackTrace();
			for (int i = 0; i < steCause.length; i++) {
				textFromThrowable += "    at " + steCause[i] + "\n";
			}
			if (loopCount > 20) {
				break;
			}
		}
		return new Exception(textFromThrowable);
	}


	public static boolean isDatabaseGenesElementsGenomeAssemblyConvenience(Connection conn) throws Exception {
		String methodNameToReport = "UtilitiesMethodsServer isDatabaseGenesElementsGenomeAssemblyConvenience";
		
		if (QueriesDatabaseMetaData.DATABASE_IS_genes_elements_genome_assembly_convenience == 0) {
			QueriesDatabaseMetaData.getDatabaseIsGenesElementsGenomeAssemblyConvenience(conn);
		}
		if (QueriesDatabaseMetaData.DATABASE_IS_genes_elements_genome_assembly_convenience == 1) {
			return true;
		} else if (QueriesDatabaseMetaData.DATABASE_IS_genes_elements_genome_assembly_convenience == -1) {
			return false;
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("DATABASE_IS_genes_elements_genome_assembly_convenience is neither 1 or -1")
					);
			return false;
		}
	}

	public static boolean isDatabaseNoMirror(Connection conn) throws Exception {
		String methodNameToReport = "UtilitiesMethodsServer isDatabaseNoMirror";
		
		if (QueriesDatabaseMetaData.DATABASE_IS_NO_MIRROR == 0) {
			QueriesDatabaseMetaData.getDatabaseIsNoMirror(conn);
		}
		if (QueriesDatabaseMetaData.DATABASE_IS_NO_MIRROR == 1) {
			return true;
		} else if (QueriesDatabaseMetaData.DATABASE_IS_NO_MIRROR == -1) {
			return false;
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("DATABASE_IS_NO_MIRROR is neither 1 or -1")
					);
			return false;
		}
	}


	
	
}

