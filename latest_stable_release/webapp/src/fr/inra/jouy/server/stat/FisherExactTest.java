package fr.inra.jouy.server.stat;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */



//adapted for Insyght from the initial free code source from Ed Buckler in 2001
/**
* This does a Fisher Exact test.  The Fisher's Exact test procedure calculates an exact probability value
* for the relationship between two dichotomous variables, as found in a two by two crosstable. The program
* calculates the difference between the data observed and the data expected, considering the given marginal
* and the assumptions of the model of independence. It works in exactly the same way as the Chi-square test
* for independence; however, the Chi-square gives only an estimate of the true probability value, an estimate
* which might not be very accurate if the marginal is very uneven or if there is a small value (less than five)
* in one of the cells.
* <p/>
* It uses an array of factorials initialized at the beginning to provide speed.
* There could be better ways to do this.
*
* @author Ed Buckler
* @version $Id: FisherExact.java,v 1
*/

public class FisherExactTest {
 private static final boolean DEBUG = false;
 private double[] f;
 int maxSize;


 /**
  * constructor for FisherExact table
  *
  * @param maxSize is the maximum sum that will be encountered by the table (a+b+c+d)
  */
 public FisherExactTest(int maxSize) {
     this.maxSize = maxSize;
     //double cf = 1.0;
     f = new double[maxSize + 1];
     f[0] = 0.0;
     for (int i = 1; i <= this.maxSize; i++) {
         f[i] = f[i - 1] + Math.log(i);
     }
 }

 public FisherExactTest(int maxSize, boolean useLookup) {
     this.maxSize = maxSize;
     //double cf = 1.0;
     f = new double[maxSize + 1];
     f[0] = 0.0;
     for (int i = 1; i <= this.maxSize; i++) {
         f[i] = f[i - 1] + Math.log(i);
     }
     int count = 0;
     double minP = 0.05;
     for (int i = 1; i < maxSize; i++) {
         for (int j = 1; j < maxSize; j++) {
             for (int k = 1; k < maxSize; k++) {
                 for (int m = 1; m < maxSize; m++) {
                     if (getTwoTailedP(i, j, k, m) < minP) {
                         count++;
                     }
                 }
             }
         }

     }
     System.out.printf("MaxSize %d minP: %g  Count: %d %n", maxSize, minP, count);
 }

 /**
  * calculates the P-value for this specific state
  *
  * @param a a, b, c, d are the four cells in a 2x2 matrix
  * @param b
  * @param c
  * @param d
  * @return the P-value
  */
 public final double getP(int a, int b, int c, int d) {
     int n = a + b + c + d;
     if (n > maxSize) {
         return Double.NaN;
     }
     double p;
     p = (f[a + b] + f[c + d] + f[a + c] + f[b + d]) - (f[a] + f[b] + f[c] + f[d] + f[n]);
     return Math.exp(p);
 }

 /**
  * Calculates the one-tail P-value for the Fisher Exact test.  Determines whether to calculate the right- or left-
  * tail, thereby always returning the smallest p-value.
  *
  * @param a a, b, c, d are the four cells in a 2x2 matrix
  * @param b
  * @param c
  * @param d
  * @return one-tailed P-value (right or left, whichever is smallest)
  */
 public final double getCumlativeP(int a, int b, int c, int d) {
     int min, i;
     int n = a + b + c + d;
     if (n > maxSize) {
         return Double.NaN;
     }
     double p = 0;

     p += getP(a, b, c, d);
     if (DEBUG) {
         System.out.println("p = " + p);
     }
     if ((a * d) >= (b * c)) {
         if (DEBUG) {
             System.out.println("doing R-tail: a=" + a + " b=" + b + " c=" + c + " d=" + d);
         }
         min = (c < b) ? c : b;
         for (i = 0; i < min; i++) {
             if (DEBUG) {
                 System.out.print("doing round " + i);
             }
             p += getP(++a, --b, --c, ++d);
             if (DEBUG) {
                 System.out.println("\ta=" + a + " b=" + b + " c=" + c + " d=" + d);
             }
         }
         //           System.out.println("");
     }
     if ((a * d) < (b * c)) {
         if (DEBUG) {
             System.out.println("doing L-tail: a=" + a + " b=" + b + " c=" + c + " d=" + d);
         }
         min = (a < d) ? a : d;
         for (i = 0; i < min; i++) {
             if (DEBUG) {
                 System.out.print("doing round " + i);
             }
             double pTemp = getP(--a, ++b, ++c, --d);
             if (DEBUG) {
                 System.out.print("\tpTemp = " + pTemp);
             }
             p += pTemp;
             if (DEBUG) {
                 System.out.println("\ta=" + a + " b=" + b + " c=" + c + " d=" + d);
             }
         }
     }
     return p;
 }

 /**
  * Calculates the right-tail P-value for the Fisher Exact test.
  *
  * @param a a, b, c, d are the four cells in a 2x2 matrix
  * @param b
  * @param c
  * @param d
  * @return one-tailed P-value (right-tail)
  */
 public final double getRightTailedP(int a, int b, int c, int d) {
     int min, i;
     int n = a + b + c + d;
     if (n > maxSize) {
         return Double.NaN;
     }
     double p = 0;

     p += getP(a, b, c, d);
     if (DEBUG) {
         System.out.println("p = " + p);
     }
     if (DEBUG) {
         System.out.println("doing R-tail: a=" + a + " b=" + b + " c=" + c + " d=" + d);
     }
     min = (c < b) ? c : b;
     for (i = 0; i < min; i++) {
         p += getP(++a, --b, --c, ++d);

     }
     return p;
 }

 /**
  * Calculates the right-tail P-value for the Fisher Exact test.
  *
  * @param a a, b, c, d are the four cells in a 2x2 matrix
  * @param b
  * @param c
  * @param d
  * @return one-tailed P-value (right-tail)
  */
 public final double getRightTailedPQuick(int a, int b, int c, int d, double maxP) {
     int min, i;
     double p = 0;

     p += getP(a, b, c, d);
     min = (c < b) ? c : b;
     for (i = 0; (i < min) && (p < maxP); i++) {
         p += getP(++a, --b, --c, ++d);

     }
     return p;
 }

 /**
  * Calculates the left-tail P-value for the Fisher Exact test.
  *
  * @param a a, b, c, d are the four cells in a 2x2 matrix
  * @param b
  * @param c
  * @param d
  * @return one-tailed P-value (left-tail)
  */
 public final double getLeftTailedP(int a, int b, int c, int d) {
     int min, i;
     int n = a + b + c + d;
     if (n > maxSize) {
         return Double.NaN;
     }
     double p = 0;

     p += getP(a, b, c, d);
     if (DEBUG) {
         System.out.println("p = " + p);
     }
     if (DEBUG) {
         System.out.println("doing L-tail: a=" + a + " b=" + b + " c=" + c + " d=" + d);
     }
     min = (a < d) ? a : d;
     for (i = 0; i < min; i++) {
         if (DEBUG) {
             System.out.print("doing round " + i);
         }
         double pTemp = getP(--a, ++b, ++c, --d);
         if (DEBUG) {
             System.out.print("\tpTemp = " + pTemp);
         }
         p += pTemp;
         if (DEBUG) {
             System.out.println("\ta=" + a + " b=" + b + " c=" + c + " d=" + d);
         }
     }


     return p;
 }

 /**
  * Calculates the two-tailed P-value for the Fisher Exact test.
  * <p/>
  * In order for a table under consideration to have its p-value included
  * in the final result, it must have a p-value less than the original table's P-value, i.e.
  * Fisher's exact test computes the probability, given the observed marginal
  * frequencies, of obtaining exactly the frequencies observed and any configuration more extreme.
  * By "more extreme," we mean any configuration (given observed marginals) with a smaller probability of
  * occurrence in the same direction (one-tailed) or in both directions (two-tailed).
  *
  * @param a a, b, c, d are the four cells in a 2x2 matrix
  * @param b
  * @param c
  * @param d
  * @return two-tailed P-value
  */
 public final double getTwoTailedP(int a, int b, int c, int d) {
     int min, i;
     int n = a + b + c + d;
     if (n > maxSize) {
         return Double.NaN;
     }
     double p = 0;

     double baseP = getP(a, b, c, d);
//      in order for a table under consideration to have its p-value included
//      in the final result, it must have a p-value less than the baseP, i.e.
//      Fisher's exact test computes the probability, given the observed marginal
//      frequencies, of obtaining exactly the frequencies observed and any configuration more extreme.
//      By "more extreme," we mean any configuration (given observed marginals) with a smaller probability of
//      occurrence in the same direction (one-tailed) or in both directions (two-tailed).

     if (DEBUG) {
         System.out.println("baseP = " + baseP);
     }
     int initialA = a, initialB = b, initialC = c, initialD = d;
     p += baseP;
     if (DEBUG) {
         System.out.println("p = " + p);
     }
     if (DEBUG) {
         System.out.println("Starting with R-tail: a=" + a + " b=" + b + " c=" + c + " d=" + d);
     }
     min = (c < b) ? c : b;
     for (i = 0; i < min; i++) {
         if (DEBUG) {
             System.out.print("doing round " + i);
         }
         double tempP = getP(++a, --b, --c, ++d);
         if (tempP <= baseP) {
             if (DEBUG) {
                 System.out.print("\ttempP (" + tempP + ") is less than baseP (" + baseP + ")");
             }
             p += tempP;
         }
         if (DEBUG) {
             System.out.println(" a=" + a + " b=" + b + " c=" + c + " d=" + d);
         }
     }

     // reset the values to their original so we can repeat this process for the other side
     a = initialA;
     b = initialB;
     c = initialC;
     d = initialD;

     if (DEBUG) {
         System.out.println("Now doing L-tail: a=" + a + " b=" + b + " c=" + c + " d=" + d);
     }
     min = (a < d) ? a : d;
     if (DEBUG) {
         System.out.println("min = " + min);
     }
     for (i = 0; i < min; i++) {
         if (DEBUG) {
             System.out.print("doing round " + i);
         }
         double pTemp = getP(--a, ++b, ++c, --d);
         if (DEBUG) {
             System.out.println("  pTemp = " + pTemp);
         }
         if (pTemp <= baseP) {
             if (DEBUG) {
                 System.out.print("\ttempP (" + pTemp + ") is less than baseP (" + baseP + ")");
             }
             p += pTemp;
         }
         if (DEBUG) {
             System.out.println(" a=" + a + " b=" + b + " c=" + c + " d=" + d);
         }
     }
     return p;
 }

// public static void main(String[] args) {
//     int a = 1;
//     int b = 1;
//     int c = 0;
//     int d = 50;
//     FisherExact fisherExact = new FisherExact(a + b + c + d + 10);
//     double twoTailedP = fisherExact.getTwoTailedP(a, b, c, d);
//     System.out.println(twoTailedP);
//
//     double tmp = 0;
//     System.out.println((-1) * 10 * Math.log10(tmp));
// }
 
}