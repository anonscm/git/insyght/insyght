package fr.inra.jouy.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.sql.DataSource;


/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/


public class DatabaseConf {

	public static DataSource datasource_db = null;
	public static DataSource datasource_taxo = null;
	public static final String CONF_DRIVER = "org.postgresql.Driver";
	public static final String CONF_USERNAME = "xxx";
	public static boolean POOL_CONNECTIONS = true;
	public static final String CONF_PASSWORD = "xxx";
	public static final String CONF_DB_URL = "xxx";
	private static final String DB_URL_TAXO = "xxx";
	private static final String DRIVER_TAXO = "org.postgresql.Driver";
	private static final String USERNAME_TAXO = "xxx";
	private static final String PASSWORD_TAXO = "xxx";

	
	/**
	 * Dole out the connections here.
	 * @throws ClassNotFoundException 
	 */
	public static synchronized Connection getConnection_db() throws Exception
	{
		if(DatabaseConf.POOL_CONNECTIONS){
			try {
				return datasource_db.getConnection();
			} catch (SQLException e) {
				throw UtilitiesMethodsServer.dealWithServerSideThrowable(e);
			}
		}else {
			try {
				Class.forName(CONF_DRIVER);
			} catch (ClassNotFoundException e) {
				throw UtilitiesMethodsServer.dealWithServerSideThrowable(e);
			}
			Connection conn = null;
			try {
				conn = DriverManager.getConnection(CONF_DB_URL, CONF_USERNAME, CONF_PASSWORD);
			} catch (Throwable e) {
				throw UtilitiesMethodsServer.dealWithServerSideThrowable(e);
			}
			return conn;
		}
		
	}

	public static synchronized Connection getConnection_taxo() throws Exception {
		if(DatabaseConf.POOL_CONNECTIONS){
			try {
				return datasource_taxo.getConnection();
			} catch (SQLException e) {
				throw UtilitiesMethodsServer.dealWithServerSideThrowable(e);
			}
		}else {
			try {
				Class.forName(DRIVER_TAXO);
			} catch (ClassNotFoundException e) {
				throw UtilitiesMethodsServer.dealWithServerSideThrowable(e);
			}
			Connection conn;
			try {
				conn = DriverManager.getConnection(DB_URL_TAXO, USERNAME_TAXO, PASSWORD_TAXO);
			} catch (SQLException e) {
				throw UtilitiesMethodsServer.dealWithServerSideThrowable(e);
			}
			return conn;
		}
	}
	

	/**
	 * Must close the database connection to return it to the pool.
	 * @throws Exception 
	 */
	public static synchronized void freeConnection(Connection connection, String method) {
	    try
	    {
	    	if (connection != null) {
	  	      connection.close();
		      connection = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
	    	}
	    }
	    catch (Throwable e)
	    {
	    	UtilitiesMethodsServer.dealWithServerSideThrowable(e);
	    }
	}


	public static void initDBContext() throws ServletException {
		if(DatabaseConf.POOL_CONNECTIONS){
			try
		    {
		      InitialContext initialContext = new InitialContext();
	
		      // actual jndi name is "jdbc/postgres"
		      datasource_db = (DataSource) initialContext.lookup( "java:/comp/env/jdbc/postgres_db" );
		      if ( datasource_db == null )
		      {
		    	  UtilitiesMethodsServer.reportException("init",new Exception("DatabaseConf initDBContext : Could not find our DataSource_db in DB_Insyght. We're about to have problems"));
		      }
		      
		      datasource_taxo = (DataSource) initialContext.lookup( "java:/comp/env/jdbc/ncbi_taxonomy" );
		      if ( datasource_taxo == null )
		      {
		    	  UtilitiesMethodsServer.reportException("init",new Exception("DatabaseConf initDBContext : Could not find our DataSource taxo in DB_Insyght. We're about to have problems"));
		      }
		      
		      
		    }
		    catch (Throwable e)
		    {
		      throw new ServletException(UtilitiesMethodsServer.dealWithServerSideThrowable(e));
		    }
		}
	}
	
	
}



