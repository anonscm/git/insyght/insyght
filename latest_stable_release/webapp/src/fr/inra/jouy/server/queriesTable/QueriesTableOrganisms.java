package fr.inra.jouy.server.queriesTable;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem;
//import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_scope;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortType;
//import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder;
//import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.EnumResultListSortScopeType;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;

public class QueriesTableOrganisms {

	// table organisms
	
	//method that submit queries
	
	public static int getOrgaIdWithSpeciesStrainSubstrain(
			Connection conn
			, String species
			, String strain
			, String substrain
			) throws Exception {

		String methodNameToReport = "QueriesTableOrganisms getOrgaIdWithSpeciesStrainSubstrain"
				+ " ; species="+species
				+ " ; strain="+strain
				+ " ; substrain="+substrain
				;

		int origamiOrgaId = -1;
		
		//args to check
		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport,
				species, "species", false, false);
		boolean strainIsValidAndNotNullOrEmpty = UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport,
				strain, "strain", true, true
				);
		boolean substrainIsValidAndNotNullOrEmpty = UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport,
				substrain, "substrain", true, true
				);

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			String command = "SELECT organism_id FROM organisms WHERE species ILIKE '%" + species + "%'";
			if (strainIsValidAndNotNullOrEmpty) {
				command += " AND strain ILIKE '%" + strain + "%'";
			}
			if(substrainIsValidAndNotNullOrEmpty) {
				command += " AND substrain ILIKE '%" + substrain + "%'";
			}
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				command += " AND computation_in_process IS FALSE";
			}
			
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			if (rs.next()) {
				origamiOrgaId = rs.getInt("organism_id");
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("Error : more than 1 result when looking for organism with "
							+ "species = " + species
							+ " AND strain = " + strain
							+ " AND substrain = " + substrain
							)
					);
				}
			}
			if (origamiOrgaId < 0) {
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception(" no row returned when looking for organism with "
							+ "species = " + species
							+ " AND strain = " + strain
							+ " AND substrain = " + substrain
							)
				);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return origamiOrgaId;
		
	}
	
	/*
	public static HashMap<Integer, LightOrganismItem> getHMOrgaId2LightOrganismItem(
			Connection conn
			, ArrayList<Integer> limitToAlOrgaIds 
			, ArrayList<Integer> excludeAlOrgaIds
			, HashMap<Integer, Integer> hmOrgaId2fScoreToStore
			, boolean onlyPublic
			, boolean onlyWithElement
			) throws Exception {
		

		String methodNameToReport = "QueriesTableOrganisms getHMOrgaId2LightOrganismItem"
				+ " ; limitToAlOrgaIds =" + ( (limitToAlOrgaIds != null ) ? limitToAlOrgaIds.toString() : "NULL" )	
				+ " ; excludeAlOrgaIds =" + ( (excludeAlOrgaIds != null ) ? excludeAlOrgaIds.toString() : "NULL" )	
				+ " ; onlyPublic="+onlyPublic
				+ " ; onlyWithElement="+onlyWithElement
				;
		
		HashMap<Integer, LightOrganismItem> hmToReturn = new HashMap<Integer, LightOrganismItem>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT organism_id, species, strain, substrain, taxon_id, ispublic"
					+ " FROM organisms";
			String whereClause = "";
			if( limitToAlOrgaIds != null && ! limitToAlOrgaIds.isEmpty() ){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(limitToAlOrgaIds)
						+ ")";
			}
			
			if( excludeAlOrgaIds != null && ! excludeAlOrgaIds.isEmpty() ){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(excludeAlOrgaIds)
						+ ")";
			}
			if(onlyPublic){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " ispublic is true";
			}
			if(onlyWithElement){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id IN (select distinct(organism_id) FROM elements)";
			}
			command += whereClause;

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				LightOrganismItem loi = new LightOrganismItem();
				loi.setOrganismId(rs.getInt("organism_id"));
				loi.setSpecies(rs.getString("species"));
				loi.setStrain(rs.getString("strain"));
				loi.setSubstrain(rs.getString("substrain"));
				loi.setTaxonId(rs.getInt("taxon_id"));
				loi.setPublic(rs.getBoolean("ispublic"));
				if (hmOrgaId2fScoreToStore != null && hmOrgaId2fScoreToStore.containsKey(rs.getInt("organism_id"))) {
					loi.setScore(hmOrgaId2fScoreToStore.get(rs.getInt("organism_id")));
				}
				// lstOrgaResult.add(loi);
				hmToReturn.put(loi.getOrganismId(), loi);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return hmToReturn;
		
	}*/
	
	
	public static int getCountOrganisms(
			Connection conn
			, ArrayList<Integer> limitToAlOrgaIds 
			, ArrayList<Integer> excludeAlOrgaIds
			, HashMap<Integer, Integer> hmOrgaId2fScoreToStore
			, boolean setScoreTo0IfNotProvided
			, boolean onlyPublic
			, boolean onlyWithElement
			) throws Exception {

		String methodNameToReport = "QueriesTableOrganisms getCountOrganismItems"
				+ " ; limitToAlOrgaIds =" + ( (limitToAlOrgaIds != null ) ? limitToAlOrgaIds.toString() : "NULL" )
				+ " ; excludeAlOrgaIds =" + ( (excludeAlOrgaIds != null ) ? excludeAlOrgaIds.toString() : "NULL" )
				+ " ; hmOrgaId2fScoreToStore =" + ( (hmOrgaId2fScoreToStore != null ) ? hmOrgaId2fScoreToStore.toString() : "NULL" )
				+ " ; setScoreTo0IfNotProvided="+setScoreTo0IfNotProvided
				+ " ; onlyPublic="+onlyPublic
				+ " ; onlyWithElement="+onlyWithElement
				;
		
		int intToReturn = -1;

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT count(organism_id) AS COUNT"
					+ " FROM organisms";
			String whereClause = "";
			if( limitToAlOrgaIds != null && ! limitToAlOrgaIds.isEmpty() ){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(limitToAlOrgaIds)
						+ ")";
			}
			
			if( excludeAlOrgaIds != null && ! excludeAlOrgaIds.isEmpty() ){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(excludeAlOrgaIds)
						+ ")";
			}
			if(onlyPublic){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " ispublic is true";
			}
			if(onlyWithElement){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id IN (select distinct(organism_id) FROM elements)";
			}
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " computation_in_process IS FALSE";
			}
			
			command += whereClause;

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			if (rs.next()) {
				intToReturn = rs.getInt("COUNT");
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return intToReturn;
		
	}
	
	

	public static LightOrganismItem getLightOrganismItemWithOrgaId(
			Connection conn
			, int organismId
			, boolean onlyPublic
			, boolean onlyWithElement
			) throws Exception {
		
		String methodNameToReport = "QueriesTableOrganisms getLightOrganismItemWithOrgaId"
				+ " ; organismId="+organismId
				+ " ; onlyPublic="+onlyPublic
				+ " ; onlyWithElement="+onlyWithElement
				;
		//args to check
		//no
		//mandatory args
		if (organismId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("organismId <= 0")
					);
		}
		
		ArrayList<Integer> limitToAlOrgaIds = new ArrayList<>();
		limitToAlOrgaIds.add(organismId);
		ArrayList<LightOrganismItem> alLightOrganismItems = getAlLightOrganismItems_orderByScoreAscIfProvided(
				conn
				, limitToAlOrgaIds
				, null
				, null
				, false
				, false
				, onlyPublic
				, onlyWithElement
				);
		if (alLightOrganismItems.size() == 1) {
			return alLightOrganismItems.get(0);
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT alLightOrganismItems.size() == 1 : "+alLightOrganismItems.size())
					);
			return null;
		}
		
	}
	
	

	public static HashSet<Integer> getHsTaxonIds_withIspublicAndIsOnlyWithElement(
			Connection conn
			, boolean onlyPublic
			, boolean onlyWithElement
			) throws Exception {

		String methodNameToReport = "QueriesTableOrganisms getHsTaxonIds_withIspublicAndIsOnlyWithElement"
				+ " ; onlyPublic="+onlyPublic
				+ " ; onlyWithElement="+onlyWithElement
				;

		HashSet<Integer> hsToReturn = new HashSet<>();
		
		//args to check
		//no
		//mandatory args
		//no

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT taxon_id"
					+ " FROM organisms";
			String whereClause = "";
			if(onlyPublic){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " ispublic is true";
			}
			if(onlyWithElement){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id IN (select distinct(organism_id) FROM elements)";
			}
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " computation_in_process IS FALSE";
			}
			
			command += whereClause;

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				int taxonIdIT = rs.getInt("taxon_id");
				hsToReturn.add(taxonIdIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return hsToReturn;
		
	}	
	
	

	public static ArrayList<Integer> getAlOrganismIds(
			Connection conn
			, boolean onlyPublic
			, boolean onlyWithElement
			) throws Exception {
		

		String methodNameToReport = "QueriesTableOrganisms getAlOrganismIds"
				+ " ; onlyPublic="+onlyPublic
				+ " ; onlyWithElement="+onlyWithElement
				;
		
		ArrayList<Integer> alToReturn = new ArrayList<>();
		

		//args to check
		//no
		//mandatory args
		//no
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			String command = "SELECT organism_id"
					+ " FROM organisms";
			String whereClause = "";
			if(onlyPublic){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " ispublic is true";
			}
			if(onlyWithElement){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id IN (select distinct(organism_id) FROM elements)";
			}
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " computation_in_process IS FALSE";
			}
			
			command += whereClause;

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				alToReturn.add(rs.getInt("organism_id"));
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return alToReturn;

	}
	
	
	public static ArrayList<LightOrganismItem> getAlLightOrganismItems_orderByScoreAscIfProvided(
			Connection conn
			, ArrayList<Integer> limitToAlOrgaIds
			, ArrayList<Integer> excludeAlOrgaIds
			, HashMap<Integer, Integer> hmOrgaId2fScoreToStore
			, boolean setScoreTo0IfNotProvided
			, boolean orderByScoreAsc
			, boolean onlyPublic
			, boolean onlyWithElement
			) throws Exception {

		String methodNameToReport = "QueriesTableOrganisms getAlLightOrganismItems_orderByScoreAscIfProvided"
				+ " ; limitToAlOrgaIds =" + ( (limitToAlOrgaIds != null ) ? limitToAlOrgaIds.toString() : "NULL" )
				+ " ; excludeAlOrgaIds =" + ( (excludeAlOrgaIds != null ) ? excludeAlOrgaIds.toString() : "NULL" )
				+ " ; hmOrgaId2fScoreToStore =" + ( (hmOrgaId2fScoreToStore != null ) ? hmOrgaId2fScoreToStore.toString() : "NULL" )
				+ " ; setScoreTo0IfNotProvided="+setScoreTo0IfNotProvided
				+ " ; orderByScoreAsc="+orderByScoreAsc
				+ " ; onlyPublic="+onlyPublic
				+ " ; onlyWithElement="+onlyWithElement
				;
		

		
		
		ArrayList<LightOrganismItem> alToReturn = new ArrayList<>();
		
		//args to check
		//no
		//mandatory args
		//no
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT organism_id, species, strain, substrain, taxon_id, ispublic"
					+ " FROM organisms";
			String whereClause = "";
			if( limitToAlOrgaIds != null && ! limitToAlOrgaIds.isEmpty() ){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(limitToAlOrgaIds)
						+ ")";
			}
			
			if( excludeAlOrgaIds != null && ! excludeAlOrgaIds.isEmpty() ){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(excludeAlOrgaIds)
						+ ")";
			}
			if(onlyPublic){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " ispublic is true";
			}
			if(onlyWithElement){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organism_id IN (select distinct(organism_id) FROM elements)";
			}
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " computation_in_process IS FALSE";
			}
			
			command += whereClause;

			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				LightOrganismItem loi = new LightOrganismItem();
				loi.setOrganismId(rs.getInt("organism_id"));
				loi.setSpecies(rs.getString("species"));
				loi.setStrain(rs.getString("strain"));
				loi.setSubstrain(rs.getString("substrain"));
				loi.setTaxonId(rs.getInt("taxon_id"));
				loi.setPublic(rs.getBoolean("ispublic"));
				if (hmOrgaId2fScoreToStore != null && hmOrgaId2fScoreToStore.containsKey(rs.getInt("organism_id"))) {
					loi.setScore(hmOrgaId2fScoreToStore.get(rs.getInt("organism_id")));
				}
				if (setScoreTo0IfNotProvided && loi.getScore() < 0) {
					loi.setScore(0);
				}
				alToReturn.add(loi);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (orderByScoreAsc) {
			Comparator<LightOrganismItem> compareLOIByScoreIT = new compareLOIByScoreAsc();
			Collections.sort(alToReturn, compareLOIByScoreIT);
		}

		return alToReturn;
		
	}
	


	public static ArrayList<LightOrganismItem> getAllPublicLightOrganismItemWithFilterNameOrId(
			Connection conn
			, String searchString
			, boolean wrapAround
			, boolean caseInsensitive
			) throws Exception {
		
		String methodNameToReport = "QueriesTableOrganisms getAllPublicLightOrganismItemWithFilterNameOrId"
				+ " ; searchString="+searchString
				+ " ; wrapAround="+wrapAround
				+ " ; caseInsensitive="+caseInsensitive
				;
		
		// check args
		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport,
				searchString, "searchString", false, false);
		//mandatory args
		if (searchString != null && ! searchString.isEmpty()) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT searchString != null && ! searchString.isEmpty()")
					);
		}

		ArrayList<LightOrganismItem> alToReturn = new ArrayList<>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			String stringToMatch = UtilitiesMethodsServer.prepareStringToBeRegexSearchString(searchString);
			String stringToMatchInQuery = null;
			if (!wrapAround) {
				stringToMatchInQuery = stringToMatch;
			} else {

				if (stringToMatch.matches("^\\.\\*.+")
						&& stringToMatch.matches(".+\\.\\*$")) {
					stringToMatchInQuery = stringToMatch;
				} else if (stringToMatch.matches("^\\.\\*.+")) {
					stringToMatchInQuery = stringToMatch + ".*";
				} else if (stringToMatch.matches(".+\\.\\*$")) {
					stringToMatchInQuery = ".*" + stringToMatch;
				} else {
					stringToMatchInQuery = ".*" + stringToMatch + ".*";
				}

			}
			String command = "SELECT organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic"
					+ " FROM organisms, elements"
					+ " WHERE elements.organism_id = organisms.organism_id";

			if (searchString.matches("^\\d+$")) {
				command += " AND taxon_id = " + searchString;
			} else {
				command += " AND (species ~ E'^" + stringToMatchInQuery
						+ "$' OR strain ~ E'^" + stringToMatchInQuery
						+ "$' OR substrain ~ E'^" + stringToMatchInQuery + "$')";
			}
			
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				command += " AND computation_in_process IS FALSE";
			}

			// command += " ORDER BY organism_id";
			command += "GROUP BY organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic ORDER BY organisms.species";

			if (caseInsensitive) {
				// System.out.println("case insensitive");
				command = command.replaceAll("~ E\'", "~* E\'");
			}

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				if (rs.getBoolean("ispublic")) {
					LightOrganismItem loi = new LightOrganismItem();
					loi.setOrganismId(rs.getInt("organism_id"));
					loi.setSpecies(rs.getString("species"));
					loi.setStrain(rs.getString("strain"));
					loi.setSubstrain(rs.getString("substrain"));
					loi.setTaxonId(rs.getInt("taxon_id"));
					loi.setPublic(rs.getBoolean("ispublic"));
					alToReturn.add(loi);
				}
			}
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return alToReturn;
	}
	

	//composite, no need to test
	public static ArrayList<OrganismItem> getAllPublicOrganismItemWithFilterNameOrId(
			Connection conn
			, String searchString
			, boolean wrapAround
			, boolean caseInsensitive
			) throws Exception {
		
		ArrayList<LightOrganismItem> allLightOrganismItems = getAllPublicLightOrganismItemWithFilterNameOrId(
				conn
				, searchString
				, wrapAround
				, caseInsensitive
				);

		
		
		if (allLightOrganismItems.isEmpty()) {
			return new ArrayList<OrganismItem>();
		}
		
		//construct HashMap hmOrgaId2OrganismItem
		HashMap<Integer, OrganismItem> hmOrgaId2OrganismItem = new HashMap<Integer, OrganismItem>();
		for(LightOrganismItem loiIT : allLightOrganismItems){
			/*if (onlyCompared_TableAlignmentParam && ! hsLimitToAlOrgaIds.contains(loiIT.getOrganismId())) {
				continue;
			}*/
			hmOrgaId2OrganismItem.put(loiIT.getOrganismId(), new OrganismItem(loiIT));
		}
		
		if (hmOrgaId2OrganismItem.isEmpty()) {
			return new ArrayList<OrganismItem>();
		}
		
		ArrayList<LightElementItem> associatedLightElementItemsPlusOrganismId = QueriesTableElements.
				getAlLightElementItemsPlusOrganismIdFacultatifWithSetOrganismIdsFacultatif(conn, true, null, true);
		for(LightElementItem leiIT : associatedLightElementItemsPlusOrganismId){
			OrganismItem oiIT = hmOrgaId2OrganismItem.get(leiIT.getOrganismId());
			if(oiIT != null){
				oiIT.getListAllLightElementItem().add(leiIT);
			}
		}
		associatedLightElementItemsPlusOrganismId.clear();
		ArrayList<OrganismItem> alToReturn = new ArrayList<OrganismItem>(hmOrgaId2OrganismItem.values());
		hmOrgaId2OrganismItem.clear();
		return alToReturn;
		
	}
	
	
	//composite, no need to test
	public static ArrayList<OrganismItem> getAllOrganismItems(
			Connection conn
			, boolean onlyPublic
			, boolean onlyWithElement
			, boolean onlyCompared_TableAlignmentParam
			) throws Exception {
		
		
		ArrayList<LightOrganismItem> allLightOrganismItems = getAlLightOrganismItems_orderByScoreAscIfProvided(
				conn
				, null //alLimitToAlOrgaIds
				, null
				, null
				, false
				, false
				, onlyPublic
				, onlyWithElement
				);
		
		if (allLightOrganismItems.isEmpty()) {
			return new ArrayList<OrganismItem>();
		}
		
		//construct HashMap hmOrgaId2OrganismItem
		HashMap<Integer, OrganismItem> hmOrgaId2OrganismItem = new HashMap<Integer, OrganismItem>();
		for(LightOrganismItem loiIT : allLightOrganismItems){
			hmOrgaId2OrganismItem.put(loiIT.getOrganismId(), new OrganismItem(loiIT));
		}
		
		ArrayList<LightElementItem> associatedLightElementItemsPlusOrganismId = QueriesTableElements.
				getAlLightElementItemsPlusOrganismIdFacultatifWithSetOrganismIdsFacultatif(conn, true, null, true);
	
		for(LightElementItem leiIT : associatedLightElementItemsPlusOrganismId){
			OrganismItem oiIT = hmOrgaId2OrganismItem.get(leiIT.getOrganismId());
			if(oiIT != null){
				oiIT.getListAllLightElementItem().add(leiIT);
			}
		}
		associatedLightElementItemsPlusOrganismId.clear();
		ArrayList<OrganismItem> alToReturn = new ArrayList<OrganismItem>(hmOrgaId2OrganismItem.values());
		hmOrgaId2OrganismItem.clear();
		return alToReturn;
		
	}

	
	public static HashMap<Integer, LightOrganismItem> getHmOrgaId2LOIWithQGeneIdAndQOrgaIdAndSortScopeType_scopeGene(
			Connection conn
			, int qGeneId
			, int qOrgaId
			//, EnumResultListSortScopeType sortScopeType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			, ArrayList<Integer> alOrgaIdsToExclude
			) throws Exception {
		
		String methodNameToReport = "QueriesTableOrganisms getHmOrgaId2LOIWithQGeneIdAndQOrgaIdAndSortScopeType_scopeGene"
				+ " ; qGeneId="+qGeneId
				+ " ; qOrgaId="+qOrgaId
				//+ " ; sortScopeType="+sortScopeType
				+ " ; sortScopeType_sortType="+sortScopeType_sortType.toString()
				+ " ; sortScopeType_sortOrder="+sortScopeType_sortOrder.toString()
				+ " ; alOrgaIdsToExclude=" + ( (alOrgaIdsToExclude != null ) ? alOrgaIdsToExclude.toString() : "NULL" )
				;
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		HashMap<Integer, LightOrganismItem> hmToReturn = new HashMap<>();
		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			

			String commandGetListOrgaResult = buildCommandGetListOrgaResult_scopeGene(
					qGeneId
					, qOrgaId
					, alOrgaIdsToExclude
					//, sortScopeType
					, sortScopeType_sortType
					, sortScopeType_sortOrder
					, false
					, UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)
					);//queryDataStoredInDBAsMirror
			rs = null;
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandGetListOrgaResult
					, methodNameToReport
					);
			while (rs.next()) {
				if (rs.getBoolean("ispublic")) {
					if (rs.getInt("organism_id") != qOrgaId) {
						int organismIdIT = rs.getInt("organism_id");
						if (hmToReturn.containsKey(organismIdIT)) {
							LightOrganismItem loi = hmToReturn.get(organismIdIT);
							int currScoreIT = loi.getScore();
							int totalScoreIT = currScoreIT + rs.getInt("score_it");
							loi.setScore(totalScoreIT);
						} else {
							LightOrganismItem loi = new LightOrganismItem();
							loi.setOrganismId(organismIdIT);
							loi.setSpecies(rs.getString("species"));
							loi.setStrain(rs.getString("strain"));
							loi.setSubstrain(rs.getString("substrain"));
							loi.setTaxonId(rs.getInt("taxon_id"));
							loi.setPublic(rs.getBoolean("ispublic"));
							loi.setScore(rs.getInt("score_it"));
							//lstOrgaResult.add(loi);
							hmToReturn.put(organismIdIT, loi);
						}
					}
				}
			}
			
			//Tested
			if (UtilitiesMethodsServer.isDatabaseNoMirror(conn)) {
				commandGetListOrgaResult = buildCommandGetListOrgaResult_scopeGene(
						qGeneId
						, qOrgaId
						, alOrgaIdsToExclude
						//, sortScopeType
						, sortScopeType_sortType
						, sortScopeType_sortOrder
						, true
						, UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)
						);//queryDataStoredInDBAsMirror
				rs = null;
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandGetListOrgaResult
						, methodNameToReport
						);
				while (rs.next()) {
					if (rs.getBoolean("ispublic")) {
						if (rs.getInt("organism_id") != qOrgaId) {
							int organismIdIT = rs.getInt("organism_id");
							if (hmToReturn.containsKey(organismIdIT)) {
								LightOrganismItem loi = hmToReturn.get(organismIdIT);
								int currScoreIT = loi.getScore();
								int totalScoreIT = currScoreIT + rs.getInt("score_it");
								loi.setScore(totalScoreIT);
							} else {
								LightOrganismItem loi = new LightOrganismItem();
								loi.setOrganismId(organismIdIT);
								loi.setSpecies(rs.getString("species"));
								loi.setStrain(rs.getString("strain"));
								loi.setSubstrain(rs.getString("substrain"));
								loi.setTaxonId(rs.getInt("taxon_id"));
								loi.setPublic(rs.getBoolean("ispublic"));
								loi.setScore(rs.getInt("score_it"));
								//lstOrgaResult.add(loi);
								hmToReturn.put(organismIdIT, loi);
							}
						}
					}
				}
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return hmToReturn;
			
	}

	private static String buildCommandGetListOrgaResult_scopeGene(
			//Connection conn
			int qGeneId
			, int qorgaId
			, ArrayList<Integer> alSOrgaIdsToExclude
			//, EnumResultListSortScopeType sortScopeType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			, boolean queryDataStoredInDBAsMirror
			, boolean computation_in_process_is_false
			) throws Exception {
		
		String methodNameToReport = "QueriesTableOrganisms buildCommandGetListOrgaResult_scopeGene"
				+ " ; qGeneId="+qGeneId
				+ " ; qorgaId="+qorgaId
				+ " ; alSOrgaIdsToExclude=" + ( (alSOrgaIdsToExclude != null ) ? alSOrgaIdsToExclude.toString() : "NULL" )
				//+ " ; sortScopeType="+sortScopeType
				+ " ; sortScopeType_sortType="+sortScopeType_sortType.toString()
				+ " ; sortScopeType_sortOrder="+sortScopeType_sortOrder.toString()
				+ " ; queryDataStoredInDBAsMirror="+queryDataStoredInDBAsMirror
				;
		
		String commandStringToReturn = "";
		
		String realS = "s";
		String realQ = "q";
		if (queryDataStoredInDBAsMirror) {
			// reverse q-s
			realS = "q";
			realQ = "s";
		}
		
		if (
//				sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
//				|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0
				sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs) == 0
				) {

			commandStringToReturn += "SELECT alignment_params."+realS+"_organism_id AS organism_id,"
					+ " organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic,"
					+ " count(alignment_pairs.alignment_id) AS score_it"
					+ " FROM alignment_pairs, alignments, alignment_params, organisms"
					+ " WHERE alignment_pairs."+realQ+"_gene_id ="
					+ qGeneId;
			commandStringToReturn += " AND alignment_pairs.type IN(1,2)"
					+ " AND alignment_pairs.alignment_id = alignments.alignment_id AND alignments.alignment_param_id = alignment_params.alignment_param_id"
					+ " AND alignment_params."+realQ+"_organism_id="
					+ qorgaId
					+ UtilitiesMethodsServer.getParamsOrthoHomoGapToUseAsStringForSQLQuery()
					+ " AND alignment_params."+realS+"_organism_id = organisms.organism_id";
			if (!alSOrgaIdsToExclude.isEmpty()) {
				commandStringToReturn += " AND alignment_params."+realS+"_organism_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSOrgaIdsToExclude)
						+ ")";
			}
			if (computation_in_process_is_false) {
				commandStringToReturn += " AND organisms.computation_in_process IS FALSE";
			}
			commandStringToReturn += " GROUP BY alignment_params."+realS+"_organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic";
					

		} else if (
//				sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
//				|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0
				sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.SyntenyScore) == 0
				) {

			
			commandStringToReturn += "SELECT alignment_params."+realS+"_organism_id AS organism_id,"
					+ " organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic,"
					+ " SUM(alignments.score) AS score_it"
					+ " FROM alignment_pairs, alignments, alignment_params, organisms"
					+ " WHERE alignment_pairs."+realQ+"_gene_id ="
					+ qGeneId;
			commandStringToReturn += " AND alignment_pairs.type IN(1,2)"
					+ " AND alignment_pairs.alignment_id = alignments.alignment_id AND alignments.alignment_param_id = alignment_params.alignment_param_id"
					+ " AND alignment_params."+realQ+"_organism_id="
					+ qorgaId
					+ UtilitiesMethodsServer.getParamsOrthoHomoGapToUseAsStringForSQLQuery()
					+ " AND alignment_params."+realS+"_organism_id = organisms.organism_id";
			if (!alSOrgaIdsToExclude.isEmpty()) {
				commandStringToReturn += " AND alignment_params."+realS+"_organism_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSOrgaIdsToExclude)
						+ ")";
			}
			if (computation_in_process_is_false) {
				commandStringToReturn += " AND organisms.computation_in_process IS FALSE";
			}
			commandStringToReturn += " GROUP BY alignment_params."+realS+"_organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic";
			

		} else if (
//				sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
//				|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0
				sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AlignemntScore) == 0
				) {

			// SELECT MAX(homologies.score) AS MAX_homologies_score,
			// MAX(homologies.s_align_length) AS MAX_s_align_lenght,
			// organisms.organism_id, organisms.species, organisms.strain,
			// organisms.substrain, organisms.taxon_id
			// FROM homologies, organisms
			// WHERE homologies.q_gene_id = 1
			// AND homologies.s_organism_id = organisms.organism_id
			// GROUP BY organisms.organism_id, organisms.species,
			// organisms.strain, organisms.substrain, organisms.taxon_id
			// ORDER BY MAX(homologies.score) DESC,
			// MAX(homologies.s_align_length) DESC LIMIT 150;

			commandStringToReturn += "SELECT organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic,"
					+ " MAX(homologies.score) AS score_it"
					+ " FROM homologies, organisms"
					+ " WHERE homologies."+realQ+"_organism_id ="
					+ qorgaId
					+ " AND homologies."+realQ+"_gene_id =" + qGeneId;
			commandStringToReturn += " AND homologies."+realS+"_organism_id = organisms.organism_id";
			if (!alSOrgaIdsToExclude.isEmpty()) {
				commandStringToReturn += " AND organisms.organism_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSOrgaIdsToExclude)
						+ ")";
			}
			if (computation_in_process_is_false) {
				commandStringToReturn += " AND organisms.computation_in_process IS FALSE";
			}
			commandStringToReturn += " GROUP BY organisms.organism_id, organisms.species, organisms.strain, organisms.substrain,  organisms.taxon_id, organisms.ispublic";
			

		} else if (
//				sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGSANNOTATIONS_ORDER_ASC) == 0
//				|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGSANNOTATIONS_ORDER_DESC) == 0
				sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologsAnnotations) == 0
				) {

			commandStringToReturn += "SELECT organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic,"
					+ " count(organisms.organism_id) AS score_it"
					+ " FROM genes, micado.features, elements, micado.qualifiers, organisms"
					+ " WHERE elements.organism_id=genes.organism_id AND elements.element_id=genes.element_id AND elements.accession = micado.features.accession AND genes.feature_id = micado.features.code_feat AND micado.qualifiers.accession = micado.features.accession AND micado.qualifiers.code_feat = micado.features.code_feat"
					+ " AND (micado.qualifiers.type_qual = 'function' OR micado.qualifiers.type_qual = 'product' OR micado.qualifiers.type_qual = 'EC_number' OR micado.qualifiers.type_qual = 'biological_process' OR micado.qualifiers.type_qual = 'cellular_component' OR micado.qualifiers.type_qual = 'locus_tag' OR micado.qualifiers.type_qual = 'gene_name' OR micado.qualifiers.type_qual = 'prot_id' OR micado.qualifiers.type_qual = 'protein_id'"
					+ ") AND genes.gene_id IN ("
					+ "SELECT alignment_pairs."+realS+"_gene_id"
					+ " FROM alignment_pairs, alignments, alignment_params"
					+ " WHERE alignment_pairs."+realQ+"_gene_id = "
					+ qGeneId
					+ " AND alignment_pairs.type IN(1,2) AND alignment_pairs.alignment_id = alignments.alignment_id AND"
					+ " alignments.alignment_param_id = alignment_params.alignment_param_id"
					+ UtilitiesMethodsServer.getParamsOrthoHomoGapToUseAsStringForSQLQuery()
					//+ " AND alignment_params.ortho_score = 4.0 AND alignment_params.homo_score = 2.0 AND alignment_params.mismatch_penalty = -4.0 AND	alignment_params.gap_creation_penalty = -8.0 AND alignment_params.gap_extension_penalty = -2.0 AND alignment_params.min_align_size = 1 AND alignment_params.min_score = 8.0 AND alignment_params.orthologs_included = true"
					+ "	AND alignment_params."+realQ+"_organism_id="
					+ qorgaId
					+ "	)"
					+ " AND organisms.organism_id = genes.organism_id";
			if (!alSOrgaIdsToExclude.isEmpty()) {
				commandStringToReturn += " AND organisms.organism_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSOrgaIdsToExclude)
						+ ")";
			}
			if (computation_in_process_is_false) {
				commandStringToReturn += " AND organisms.computation_in_process IS FALSE";
			}
			commandStringToReturn += " GROUP BY organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic";

		} else {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("sortScopeType_sortType not recognized : "+sortScopeType_sortType.toString()));
		}
		return commandStringToReturn;
	}


	public static HashMap<Integer, LightOrganismItem> getHmOrgaId2LOIWithAlQGeneIdAndQOrgaIdAndSortScopeType_scopeGeneSet(
			Connection conn
			, ArrayList<Integer> alQGeneId
			, int qOrgaId
			//, EnumResultListSortScopeType sortScopeType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			, ArrayList<Integer> alOrgaIdsToExclude
			) throws Exception {


		String methodNameToReport = "QueriesTableOrganisms getHmOrgaId2LOIWithAlQGeneIdAndQOrgaIdAndSortScopeType_scopeGeneSet"
				+ " ; alQGeneId=" + ( (alQGeneId != null ) ? alQGeneId.toString() : "NULL" )
				+ " ; qOrgaId="+qOrgaId
				//+ " ; sortScopeType="+sortScopeType
				+ " ; sortScopeType_sortType="+sortScopeType_sortType.toString()
				+ " ; sortScopeType_sortOrder="+sortScopeType_sortOrder.toString()
				+ " ; alOrgaIdsToExclude=" + ( (alOrgaIdsToExclude != null ) ? alOrgaIdsToExclude.toString() : "NULL" )
				;
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		HashMap<Integer, LightOrganismItem> hmToReturn = new HashMap<>();
		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String commandGetListOrgaResult = buildCommandGetListOrgaResult_scopeGeneSet(
					alQGeneId
					, qOrgaId
					, alOrgaIdsToExclude
					//, sortScopeType
					, sortScopeType_sortType
					, sortScopeType_sortOrder
					, false
					, UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)
					);//queryDataStoredInDBAsMirror
			//System.err.println("Regular : "+commandGetListOrgaResult);
			rs = null;
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandGetListOrgaResult
					, methodNameToReport
					);
			while (rs.next()) {
				if (rs.getBoolean("ispublic")) {
					if (rs.getInt("organism_id") != qOrgaId) {
						int organismIdIT = rs.getInt("organism_id");
						if (hmToReturn.containsKey(organismIdIT)) {
							LightOrganismItem loi = hmToReturn.get(organismIdIT);
							int currScoreIT = loi.getScore();
							int totalScoreIT = currScoreIT + rs.getInt("score_it");
							loi.setScore(totalScoreIT);
						} else {
							LightOrganismItem loi = new LightOrganismItem();
							loi.setOrganismId(organismIdIT);
							loi.setSpecies(rs.getString("species"));
							loi.setStrain(rs.getString("strain"));
							loi.setSubstrain(rs.getString("substrain"));
							loi.setTaxonId(rs.getInt("taxon_id"));
							loi.setPublic(rs.getBoolean("ispublic"));
							loi.setScore(rs.getInt("score_it"));
							//lstOrgaResult.add(loi);
							hmToReturn.put(organismIdIT, loi);
						}
					}
				}
			}


			if (UtilitiesMethodsServer.isDatabaseNoMirror(conn)) {
				commandGetListOrgaResult = buildCommandGetListOrgaResult_scopeGeneSet(
						alQGeneId
						, qOrgaId
						, alOrgaIdsToExclude
						//, sortScopeType
						, sortScopeType_sortType
						, sortScopeType_sortOrder
						, true
						, UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)
						);//queryDataStoredInDBAsMirror
				//System.err.println("Mirror : "+commandGetListOrgaResult);
				rs = null;
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandGetListOrgaResult
						, methodNameToReport
						);
				while (rs.next()) {
					if (rs.getBoolean("ispublic")) {
						if (rs.getInt("organism_id") != qOrgaId) {
							int organismIdIT = rs.getInt("organism_id");
							if (hmToReturn.containsKey(organismIdIT)) {
								LightOrganismItem loi = hmToReturn.get(organismIdIT);
								int currScoreIT = loi.getScore();
								int totalScoreIT = currScoreIT + rs.getInt("score_it");
								loi.setScore(totalScoreIT);
								//System.err.println("Mirror old orga "+loi.getFullName()+" with score "+loi.getScore() + " ( old was "+currScoreIT+" + new is "+rs.getInt("score_it")+")");
							} else {
								LightOrganismItem loi = new LightOrganismItem();
								loi.setOrganismId(organismIdIT);
								loi.setSpecies(rs.getString("species"));
								loi.setStrain(rs.getString("strain"));
								loi.setSubstrain(rs.getString("substrain"));
								loi.setTaxonId(rs.getInt("taxon_id"));
								loi.setPublic(rs.getBoolean("ispublic"));
								loi.setScore(rs.getInt("score_it"));
								//lstOrgaResult.add(loi);
								hmToReturn.put(organismIdIT, loi);
								//System.err.println("Mirror new orga "+loi.getFullName()+" with score "+loi.getScore());
							}
						}
					}
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return hmToReturn;
		
	}

	private static String buildCommandGetListOrgaResult_scopeGeneSet(
			ArrayList<Integer> alQGenesSet
			, int qOrgaIdSent
			, ArrayList<Integer> alSOrgaIdsToExclude
			//, EnumResultListSortScopeType sortScopeType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			, boolean queryDataStoredInDBAsMirror
			, boolean computation_in_process_is_false
			) throws Exception {
		
		String methodNameToReport = "QueriesTableOrganisms buildCommandGetListOrgaResult_scopeGeneSet"
				+ " ; alQGenesSet=" + ( (alQGenesSet != null ) ? alQGenesSet.toString() : "NULL" )
				+ " ; qOrgaIdSent="+qOrgaIdSent
				+ " ; alSOrgaIdsToExclude=" + ( (alSOrgaIdsToExclude != null ) ? alSOrgaIdsToExclude.toString() : "NULL" )
				//+ " ; sortScopeType="+sortScopeType
				+ " ; sortScopeType_sortType="+sortScopeType_sortType.toString()
				+ " ; sortScopeType_sortOrder="+sortScopeType_sortOrder.toString()
				+ " ; queryDataStoredInDBAsMirror="+queryDataStoredInDBAsMirror
				;

		String commandGetListOrgaResult = "";
		
		String realS = "s";
		String realQ = "q";
		if (queryDataStoredInDBAsMirror) {
			// reverse q-s
			realS = "q";
			realQ = "s";
		}

		if (
//				sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0
//				|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0
				sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs) == 0
				) {
			
				commandGetListOrgaResult += "SELECT alignment_params."+realS+"_organism_id AS organism_id,"
						+ " organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic,"
						+ " count(alignment_pairs.alignment_id) AS score_it"
						+ " FROM alignment_pairs, alignments, alignment_params, organisms"
						+ " WHERE alignment_pairs."+realQ+"_gene_id IN (";
				for (int j = 0; j < alQGenesSet.size(); j++) {
					int geneIdToDw = alQGenesSet.get(j);
					if (j == 0) {
						commandGetListOrgaResult += "" + geneIdToDw;
					} else {
						commandGetListOrgaResult += "," + geneIdToDw;
					}
				}
				commandGetListOrgaResult += ") AND alignment_pairs.type IN(1,2)"
						+ " AND alignment_pairs.alignment_id = alignments.alignment_id AND alignments.alignment_param_id = alignment_params.alignment_param_id"
						+ " AND alignment_params."+realQ+"_organism_id="
						+ qOrgaIdSent
						+ UtilitiesMethodsServer.getParamsOrthoHomoGapToUseAsStringForSQLQuery()
						+ " AND alignment_params."+realS+"_organism_id = organisms.organism_id";
				if (!alSOrgaIdsToExclude.isEmpty()) {
					commandGetListOrgaResult += " AND alignment_params."+realS+"_organism_id NOT IN ("
							+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSOrgaIdsToExclude)
							+ ")";
				}
				if (computation_in_process_is_false) {
					commandGetListOrgaResult += " AND computation_in_process IS FALSE";
				}
				commandGetListOrgaResult += " GROUP BY alignment_params."+realS+"_organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic";
				

		} else if (
//				sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0
//				|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0
				sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.SyntenyScore) == 0
				) {
			commandGetListOrgaResult += "SELECT alignment_params."+realS+"_organism_id AS organism_id,"
					+ " organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic,"
					+ " SUM(alignments.score) AS score_it"
					+ " FROM alignment_pairs, alignments, alignment_params, organisms"
					+ " WHERE alignment_pairs."+realQ+"_gene_id IN (";
			for (int j = 0; j < alQGenesSet.size(); j++) {
				int geneIdToDw = alQGenesSet.get(j);
				if (j == 0) {
					commandGetListOrgaResult += "" + geneIdToDw;
				} else {
					commandGetListOrgaResult += "," + geneIdToDw;
				}
			}
			commandGetListOrgaResult += ") AND alignment_pairs.type IN(1,2)"
					+ " AND alignment_pairs.alignment_id = alignments.alignment_id AND alignments.alignment_param_id = alignment_params.alignment_param_id"
					+ " AND alignment_params."+realQ+"_organism_id="
					+ qOrgaIdSent
					//+ " AND alignment_params.ortho_score = 4.0 AND alignment_params.homo_score = 2.0 AND alignment_params.mismatch_penalty = -4.0 AND alignment_params.gap_creation_penalty = -8.0 AND alignment_params.gap_extension_penalty = -2.0 AND alignment_params.min_align_size = 1 AND alignment_params.min_score = 8.0 AND alignment_params.orthologs_included = true"
					+ UtilitiesMethodsServer.getParamsOrthoHomoGapToUseAsStringForSQLQuery()
					+ " AND alignment_params."+realS+"_organism_id = organisms.organism_id";
			if (!alSOrgaIdsToExclude.isEmpty()) {
				commandGetListOrgaResult += " AND alignment_params."+realS+"_organism_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSOrgaIdsToExclude)
						+ ")";
			}
			if (computation_in_process_is_false) {
				commandGetListOrgaResult += " AND computation_in_process IS FALSE";
			}
			commandGetListOrgaResult += " GROUP BY alignment_params."+realS+"_organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic";
			
			
		} else if (
//				sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0
//				|| sortScopeType.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0
				sortScopeType_sortType.compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AlignemntScore) == 0
				) {
			// RQ : q_gene with multiple homologs in 1 s_orga will skew this query, we should be looking for the max score per s_roga not sum
			commandGetListOrgaResult += "SELECT organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic,"
					+ " SUM(homologies.score) AS score_it"
					+ " FROM homologies, organisms"
					+ " WHERE homologies."+realQ+"_organism_id ="
					+ qOrgaIdSent
					+ " AND homologies."+realQ+"_gene_id IN (";
			for (int j = 0; j < alQGenesSet.size(); j++) {
				int geneIdToDw = alQGenesSet.get(j);
				if (j == 0) {
					commandGetListOrgaResult += "" + geneIdToDw;
				} else {
					commandGetListOrgaResult += "," + geneIdToDw;
				}
			}
			commandGetListOrgaResult += ") AND homologies."+realS+"_organism_id = organisms.organism_id";
			if (!alSOrgaIdsToExclude.isEmpty()) {
				commandGetListOrgaResult += " AND organisms.organism_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSOrgaIdsToExclude)
						+ ")";
			}
			if (computation_in_process_is_false) {
				commandGetListOrgaResult += " AND computation_in_process IS FALSE";
			}
			commandGetListOrgaResult += " GROUP BY organisms.organism_id, organisms.species, organisms.strain, organisms.substrain,  organisms.taxon_id, organisms.ispublic";
			
		} else {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("sortScopeType_sortType not recognized : "+sortScopeType_sortType.toString()));
		}
		return commandGetListOrgaResult;
	}


	public static boolean getIsPublicWithOrganismId(Connection conn
			, int orgaId
			) throws Exception {

		String methodNameToReport = "QueriesTableOrganisms getIsPublicWithOrganismId"
				+ " ; orgaId="+orgaId
				;

		boolean boolToReturn = false;
		
		// check args
		//no
		
		// mandatory args
		if (orgaId <= 0) {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("orgaId  <= 0 : "+orgaId));
		}

		// build command
		// select columns
		// Boolean
		ConcurrentHashMap<String, String> chmColumnSelectAsBoolean_aliase2columnName = new ConcurrentHashMap<String, String>();
		chmColumnSelectAsBoolean_aliase2columnName.put("ispublic", "ispublic");
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("organisms");
		String whereClause = "WHERE organism_id = " + orgaId;
		if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
			whereClause += " AND computation_in_process IS FALSE";
		}
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				null
				, null
				, chmColumnSelectAsBoolean_aliase2columnName
				, null
				, null
				, null
				, tableFrom
				, whereClause);
		
		//run command
//		ObjAssociationAsMap oaamReturned = UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//				conn, 
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				false, 
//				true, 
//				methodNameToReport);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			if (rs.next()) {
				boolToReturn = rs.getBoolean("ispublic");
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

//		if(oaamReturned.getRsColumn2Boolean().get("ispublic") != null){
//			return 	oaamReturned.getRsColumn2Boolean().get("ispublic");
//		} else {
//			return false;
//		}
		return boolToReturn;
		
	}

	// comparator
	
	public static class compareLOIByScoreAsc implements
	Comparator<LightOrganismItem> {
		public int compare(LightOrganismItem arg0, LightOrganismItem arg1) {
			return ((Integer) arg0.getScore()).compareTo(( (Integer) arg1.getScore()));
		}
	}


}













