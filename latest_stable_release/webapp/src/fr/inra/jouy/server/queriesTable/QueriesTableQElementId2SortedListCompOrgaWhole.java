package fr.inra.jouy.server.queriesTable;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.shared.UtilitiesMethodsShared;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

public class QueriesTableQElementId2SortedListCompOrgaWhole {

	
	public static ArrayList<String> getAlStringOrgaPrecomputedWithAlElementId(
			Connection conn
			, String columnNameTable_q_orga_2_precomputed_score
			, ArrayList<Integer> listElementIdIT
			) throws Exception {

		String methodNameToReport = "QueriesTableQElementId2SortedListCompOrgaWhole getAlStringOrgaPrecomputedWithAlElementId"
				+ " ; columnNameTable_q_orga_2_precomputed_score="+columnNameTable_q_orga_2_precomputed_score
				+ " ; listElementIdIT="+ ( (listElementIdIT != null ) ? listElementIdIT.toString() : "NULL" )
				;
		ArrayList<String> alToReturn = new ArrayList<String>(); // listOrgaPrecomputed
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			DatabaseMetaData metadata = conn.getMetaData();
			rs = metadata.getTables(null, null, "q_element_id_2_sorted_list_comp_orga_whole", null);
			if(rs.next()){
				rs.close();
				rs = metadata.getColumns(null, null, "q_element_id_2_sorted_list_comp_orga_whole", columnNameTable_q_orga_2_precomputed_score);
				if(rs.next()){
					// next() checks if the next table exists ...
					String cmd_is_list_orga_precomputed = "SELECT "+columnNameTable_q_orga_2_precomputed_score+" FROM q_element_id_2_sorted_list_comp_orga_whole"
							+ " WHERE element_id IN ("
							+ UtilitiesMethodsShared.getItemsAsStringFromCollection(listElementIdIT)
							+")";

					rs.close();
					rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, cmd_is_list_orga_precomputed, methodNameToReport);
					while (rs.next()) {
						String resIT = rs.getString(columnNameTable_q_orga_2_precomputed_score);
						if(resIT!=null){
							alToReturn.add(resIT);
						}
					}
				}
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			try {
				if (rs != null) {
					rs.close();
					rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				}

			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport+" rs.close()", ex);
			}
			try {
				if (statement != null) {
					statement.close();
					statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html		
				}
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport+" statement.close()", ex);
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return alToReturn;
		
	}

}
