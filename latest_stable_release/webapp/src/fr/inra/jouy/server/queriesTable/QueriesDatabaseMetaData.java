package fr.inra.jouy.server.queriesTable;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;

public class QueriesDatabaseMetaData {
	
	
	public static int DATABASE_IS_genes_elements_genome_assembly_convenience = 0;//0 not initialised, 1 true, -1 false
	/*
	 * ** "modifs table genes elements convenience and add genome assembly info to help organize molecules into living organisms"
script Task_add_entry_generator.pl AND Identifiers on assembly (to help for table organism : can be a table 1-1 with organism_id)
 - RQ : need to modify UI to deal with removal of elements.description (-> sequences.definition), elements.date_seq (-> sequences.date_seq), elements.version (-> sequences.version), genes.residue (-> prot_feat.proteine)
 - RQ : need to modify pipeline to deal with removal of elements.date_seq (-> sequences.date_seq ; Task_add_entry_generator check updates), elements.version (-> sequences.version ; Task_add_entry_generator check updates), genes.residues (-> prot_feat.proteine ; Task_generate_fasta.pl)
 - RQ : need to modify pipeline to deal with additional info in table elements: NCBI_internal_id, NCBI_sourcedb, NCBI_tech, NCBI_geneticcode, NCBI_topology, NCBI_completeness, NCBI_status, NCBI_comment
 - RQ : need to modify pipeline to deal with additional info in table organisms: NCBI_assemblyaccession_IT, NCBI_assemblyname_IT, NCBI_lastupdatedate_IT, NCBI_seqreleasedate_IT, NCBI_isolate_IT, NCBI_speciestaxid_IT, NCBI_biosampleid_IT, NCBI_biosampleaccn_IT, NCBI_list_bioprojectid_IT, NCBI_list_bioprojectaccn_IT, NCBI_assemblyclass_IT, NCBI_assemblystatus_IT
 - RQ : fix Pb long gene name and locus_tag
	 */
	
	public static int DATABASE_IS_NO_MIRROR = 0;//0 not initialised, 1 true, -1 false
	/*
	 * $SCRIPTS_DIR/TOOLS/remove_mirror_rows_synteny.pl
 - RQ : need to modify UI to deal with non redondant tables related to syntenies : alignment_params, alignments, alignment_pairs
	 */
	
	public static int DATABASE_IS_NCBI_TAXONOMY_TREE_V3 = 0;//0 not initialised, 1 true, -1 false
	
	public static int getDatabaseIsGenesElementsGenomeAssemblyConvenience(Connection conn) throws Exception {
		
		String methodNameToReport = "QueriesDatabaseMetaData getDatabaseIsGenesElementsGenomeAssemblyConvenience";
		
		if (DATABASE_IS_genes_elements_genome_assembly_convenience == 1
				|| DATABASE_IS_genes_elements_genome_assembly_convenience == -1) {
			return DATABASE_IS_genes_elements_genome_assembly_convenience;
		}

		ResultSet rs = null;
		boolean closeConn = false;
		
		if (conn == null) {
			conn = DatabaseConf.getConnection_db();
			closeConn = true;
		}
		DatabaseMetaData metadata = conn.getMetaData();
		rs = metadata.getColumns(null, "public", "genes", "residues");
		if(rs.next()){
			DATABASE_IS_genes_elements_genome_assembly_convenience = -1;
		} else {
			DATABASE_IS_genes_elements_genome_assembly_convenience = 1;
		}
		rs.close();
		metadata = null;
		if (closeConn) {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}
		if (DATABASE_IS_genes_elements_genome_assembly_convenience == 1
				|| DATABASE_IS_genes_elements_genome_assembly_convenience == -1) {
			return DATABASE_IS_genes_elements_genome_assembly_convenience;
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("DATABASE_IS_genes_elements_genome_assembly_convenience is neither 1 or -1")
					);
			return 0;
		}
	}
	
	
	
	public static int getDatabaseIsNoMirror(Connection conn) throws Exception {
		
		String methodNameToReport = "QueriesDatabaseMetaData getDatabaseIsNoMirror";
		
		if (DATABASE_IS_NO_MIRROR == 1
				|| DATABASE_IS_NO_MIRROR == -1) {
			return DATABASE_IS_NO_MIRROR;
		}
		
		ResultSet rs = null;
		boolean closeConn = false;
		
		if (conn == null) {
			conn = DatabaseConf.getConnection_db();
			closeConn = true;
		}
		DatabaseMetaData metadata = conn.getMetaData();
		rs = metadata.getTables(null, "public", "params_scores_algo_syntenies", null);
		if(rs.next()){
			DATABASE_IS_NO_MIRROR = 1;
		} else {
			DATABASE_IS_NO_MIRROR = -1;
		}
		rs.close();
		metadata = null;
		if (closeConn) {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}
		if (DATABASE_IS_NO_MIRROR == 1
				|| DATABASE_IS_NO_MIRROR == -1) {
			return DATABASE_IS_NO_MIRROR;
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("DATABASE_IS_NO_MIRROR is neither 1 or -1")
					);
			return 0;
		}
	}
	
	
	public static int getDatabaseIsNcbiTaxonomyV3(Connection conn) throws Exception {
		
		String methodNameToReport = "QueriesDatabaseMetaData getDatabaseIsNcbiTaxonomyV3";

		if (DATABASE_IS_NCBI_TAXONOMY_TREE_V3 == 1
				|| DATABASE_IS_NCBI_TAXONOMY_TREE_V3 == -1) {
			return DATABASE_IS_NCBI_TAXONOMY_TREE_V3;
		}

		ResultSet rs = null;
		boolean closeConn = false;
		
		if (conn == null) {
			conn = DatabaseConf.getConnection_taxo();
			closeConn = true;
		}
		DatabaseMetaData metadata = conn.getMetaData();
		rs = metadata.getTables(null, "micado", "ncbi_taxonomy_tree", null);
		if(rs.next()){
			DATABASE_IS_NCBI_TAXONOMY_TREE_V3 = 1;
		} else {
			DATABASE_IS_NCBI_TAXONOMY_TREE_V3 = -1;
		}
		rs.close();
		metadata = null;
		if (closeConn) {
			DatabaseConf.freeConnection(conn, methodNameToReport);
		}
		if (DATABASE_IS_NCBI_TAXONOMY_TREE_V3 == 1
				|| DATABASE_IS_NCBI_TAXONOMY_TREE_V3 == -1) {
			return DATABASE_IS_NCBI_TAXONOMY_TREE_V3;
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("DATABASE_IS_NCBI_TAXONOMY_TREE_V3 is neither 1 or -1")
					);
			return 0;
		}
	}
	
}
