package fr.inra.jouy.server.queriesTable;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.shared.UtilitiesMethodsShared;



public class QueriesTableAlignmentParams {

	// WRONG way of thinking about mirror data ; there will be some rows with s_orga_id bit different q_orga_id ; must do query twice


	// inner special object
	
	public static class AlignParamsTmpObjQElementIdAndSElementId {
		private int qElementId = 0;
		private int sElementId = 0;
		public AlignParamsTmpObjQElementIdAndSElementId(){
		}
		public int getqElementId() {
			return qElementId;
		}
		public void setqElementId(int qElementId) {
			this.qElementId = qElementId;
			
		}
		public int getsElementId() {
			return sElementId;
		}
		public void setsElementId(int sElementId) {
			this.sElementId = sElementId;
			
		}
	}

	// Tested
	public static Long getAlignmentParamIdWithQandSElementIdAndParamForSynteny(
			Connection conn
			, int qElementId
			, int sElementId
			//, AlignmentParametersForSynteny alignmentParametersForSynteny
			//, boolean shouldBePresentInTable
			, boolean isMirrorRequest
			)
			throws Exception {
		
		//boolean savedShouldBePresentInTable = false;
		
		String methodNameToReport = "QueriesTableAlignmentParams getOrigamiAlignmentParamIdWithOrigamiQandSElementIdAndParamForSynteny"
				+ "qElementId="+qElementId
				+ "sElementId="+sElementId
				//+ "shouldBePresentInTable="+shouldBePresentInTable
				+ "isMirrorRequest="+isMirrorRequest
				;

		
		Long longToReturn = (long) 0;

		// check valid input
		//no
		// check mandatory args
		//no
		
		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}
		// build command
		
		// select columns
		//int
		//ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		//chm_alColumnSelectAsInt_aliase2columnName.put("alignment_param_id", "alignment_param_id");
		//String
		//ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		//Long
		ConcurrentHashMap<String, String> chm_alColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsLong_aliase2columnName.put("alignment_param_id", "alignment_param_id");
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("alignment_params");
		String whereClause = "WHERE "+real_q+"_element_id = " + qElementId
				+ " AND "+real_s+"_element_id  = " + sElementId;
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				null
				, null
				, null
				, chm_alColumnSelectAsLong_aliase2columnName
				, null
				, null
				, tableFrom
				, whereClause
				);
		
		//System.err.println(objSQLCommandIT.getSQLCommand());
		
//		//shouldBePresentInTable
//		if (shouldBePresentInTable && UtilitiesMethodsServer.isDatabaseNoMirror(conn) &&  ! isMirrorRequest) {
//			shouldBePresentInTable = false;
//			savedShouldBePresentInTable = true;
//		}
		
		//run command
//		ObjAssociationAsMap oaamReturned = UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//				conn, 
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport);
//		
//		if(oaamReturned.getRsColumn2Long().get("alignment_param_id") != null){
//			return 	oaamReturned.getRsColumn2Long().get("alignment_param_id");
//		} else {
//			if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) &&  ! isMirrorRequest) {
//				// try mirror request
//				Long mirrorAlignmentParamId = getAlignmentParamIdWithQandSElementIdAndParamForSynteny(
//						conn
//						, sElementId
//						, qElementId
//						//, alignmentParametersForSynteny
//						, savedShouldBePresentInTable
//						, true
//						);
//				return -mirrorAlignmentParamId;
//			} else {
//				return (long) 0;
//			}
//		}

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			if (rs.next()) {
				Long alignmentParamIdIT = rs.getLong("alignment_param_id");
				if (isMirrorRequest) {
					alignmentParamIdIT = -alignmentParamIdIT;
				}
				longToReturn = alignmentParamIdIT;
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		if (longToReturn == 0) { // not found, mirror ?
			if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
				longToReturn = getAlignmentParamIdWithQandSElementIdAndParamForSynteny(
						conn
						, qElementId
						, sElementId
						//, savedShouldBePresentInTable
						, true
						);
			}
		}
		
		return longToReturn;
		
	}
	

	public static HashSet<Long> getHsAlignmentParamId_withQOrganismIdAndHsSOrganismId(
			Connection conn
			, int qOrganismId
			, HashSet<Integer> hsSOrgaIds
			, boolean isMirrorRequest
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignmentParams getHsAlignmentParamId_withQOrganismIdAndHsSOrganismId"
				+ " ; qOrganismId = "+ qOrganismId
				+ " ; hsSOrgaIds="+ ( (hsSOrgaIds != null ) ? hsSOrgaIds.toString() : "NULL" )
				+ " ; isMirrorRequest = "+ isMirrorRequest
				;

		HashSet<Long> hsToReturn = new HashSet<>();
		
		// check valid input
		//no
		// check mandatory args
		if (qOrganismId < 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("qOrganismId < 0"));
		}
//		if (hsSOrgaIds == null || hsSOrgaIds.isEmpty()) {
//			UtilitiesMethodsServer.reportException(
//					methodNameToReport
//					, new Exception("hsSOrgaIds == null || hsSOrgaIds.isEmpty()"));
//		}

		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT alignment_param_id"
					+ " FROM alignment_params"
					+ " WHERE"
					+ " "+real_q+"_organism_id = "
					+ qOrganismId
					;
			if (hsSOrgaIds != null && ! hsSOrgaIds.isEmpty()) {
				command += " AND "+real_s+"_organism_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsSOrgaIds)
						+ " )"
						;
			}

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				Long alignmentParamIdIT = rs.getLong("alignment_param_id");
				if (isMirrorRequest) {
					alignmentParamIdIT = -alignmentParamIdIT;
				}
				hsToReturn.add(alignmentParamIdIT);
			}// while
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try
		

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			hsToReturn.addAll(getHsAlignmentParamId_withQOrganismIdAndHsSOrganismId(
					conn
					, qOrganismId
					, hsSOrgaIds
					, true
					)
			);
		}
				
		return hsToReturn;
		
	}
	
	
	public static HashSet<Long> getHsAlignmentParamId_withQOrganismIdAndSOrganismId(
			Connection conn
			, Integer qOrganismId
			, Integer sOrganismId
			, boolean isMirrorRequest
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignmentParams getHsAlignmentParamId_withQOrganismIdAndSOrganismId"
				+ " ; qOrganismId = "+ qOrganismId
				+ " ; sOrganismId = "+ sOrganismId
				+ " ; isMirrorRequest = "+ isMirrorRequest
				;
		
		
		HashSet<Long> hsToReturn = new HashSet<>();
		
		// check valid input
		//no
		// check mandatory args
		//no
		
		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			String command = "SELECT alignment_param_id"
					+ " FROM alignment_params"
					+ " WHERE"
					+ " "+real_q+"_organism_id = "
					+ qOrganismId
					+ " AND "+real_s+"_organism_id = "
					+ sOrganismId
					;

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				Long alignmentParamIdIT = rs.getLong("alignment_param_id");
				if (isMirrorRequest) {
					alignmentParamIdIT = -alignmentParamIdIT;
				}
				hsToReturn.add(alignmentParamIdIT);
			}// while
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try
		

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			hsToReturn.addAll(getHsAlignmentParamId_withQOrganismIdAndSOrganismId(
					conn
					, qOrganismId
					, sOrganismId
					, true
					)
			);
		}
		
		
		
		return hsToReturn;
		
	}


	public static HashSet<Long> getHsAlignmentParamId_withAlQElementIdsAndAlSOrgaIds(
			Connection conn
			, HashSet<Integer> hsQElementIds
			, ArrayList<Integer> alSOrgaIds
			, boolean isMirrorRequest
			, boolean checkMandatoryArgs
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignmentParams getHsAlignmentParamId_withAlQElementIdsAndAlSOrgaIds"
				+ " ; hsQElementIds="+ ( (hsQElementIds != null ) ? hsQElementIds.toString() : "NULL" )
				+ " ; alSOrgaIds="+ ( (alSOrgaIds != null ) ? alSOrgaIds.toString() : "NULL" )
				+ " isMirrorRequest="+isMirrorRequest
				+ " checkMandatoryArgs="+checkMandatoryArgs
				;
		
		HashSet<Long> hsToReturn = new HashSet<>();
		
		
		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			// check valid input
			//no
			if (checkMandatoryArgs) {
				if (hsQElementIds == null || hsQElementIds.isEmpty()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("hsQElementIds == null || hsQElementIds.isEmpty()"));
				}
				if (alSOrgaIds == null || alSOrgaIds.isEmpty()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("alSOrgaIds == null || alSOrgaIds.isEmpty()"));
				}
			}
			

			String command = "SELECT alignment_param_id"//, "+real_s+"_organism_id"
					+ " FROM alignment_params"
					+ " WHERE"
					+ " "+real_q+"_element_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsQElementIds)
					+ " )"
					+ " AND "+real_s+"_organism_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSOrgaIds)
					+ " )"
					;

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				Long alignmentParamIdIT = rs.getLong("alignment_param_id");
//				int sOrganismId = rs.getInt(real_s+"_organism_id");
//				if ( ! hsSOrgaIds.contains(sOrganismId)) {
//					continue;
//				}
				
				if (isMirrorRequest) {
					alignmentParamIdIT = -alignmentParamIdIT;
				}
				hsToReturn.add(alignmentParamIdIT);
			}// while
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try
		

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			hsToReturn.addAll(getHsAlignmentParamId_withAlQElementIdsAndAlSOrgaIds(
					conn
					, hsQElementIds
					, alSOrgaIds
					, true
					, false
					)
			);
			
		}
		
		return hsToReturn;
		
	}
	
	public static HashMap<Integer, HashSet<Long>> getHmSOrgaId2AlAlignmentParamId_withAlQElementIdsAndAlSOrgaIds(
			Connection conn
			, HashSet<Integer> hsQElementIds
			, ArrayList<Integer> alSOrgaIds
			, boolean isMirrorRequest
			, boolean checkMandatoryArgs
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignmentParams getHmSOrgaId2AlAlignmentParamId_withAlQElementIdsAndAlSOrgaIds"
				+ " ; hsQElementIds="+ ( (hsQElementIds != null ) ? hsQElementIds.toString() : "NULL" )
				+ " ; alSOrgaIds="+ ( (alSOrgaIds != null ) ? alSOrgaIds.toString() : "NULL" )
				+ " isMirrorRequest="+isMirrorRequest
				+ " checkMandatoryArgs="+checkMandatoryArgs
				;
		
		HashMap<Integer, HashSet<Long>> hmToReturn = new HashMap<>();
		
		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			// check valid input
			//no
			if (checkMandatoryArgs) {
				if (hsQElementIds == null || hsQElementIds.isEmpty()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("hsQElementIds == null || hsQElementIds.isEmpty()"));
				}
				if (alSOrgaIds == null || alSOrgaIds.isEmpty()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("alSOrgaIds == null || alSOrgaIds.isEmpty()"));
				}
			}


			//can cause java.lang.OutOfMemoryError: GC overhead limit exceeded otherwise
			if (alSOrgaIds.size() > UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined) {
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("alSOrgaIds.size() = "+alSOrgaIds.size()
				+" > UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined = "+UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined
				+ " ; This is not allowed as this can cause java.lang.OutOfMemoryError: GC overhead limit exceeded"
				)
				);
			}
			

			String command = "SELECT alignment_param_id, "+real_s+"_organism_id"
					+ " FROM alignment_params"
					+ " WHERE"
					+ " "+real_q+"_element_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsQElementIds)
					+ " )"
					+ " AND "+real_s+"_organism_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSOrgaIds)
					+ " )"
					;
			
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				Long alignmentParamIdIT = rs.getLong("alignment_param_id");
				
				int sOrganismId = rs.getInt(real_s+"_organism_id");
				if (isMirrorRequest) {
					alignmentParamIdIT = -alignmentParamIdIT;
				}
				
				if (hmToReturn.containsKey(sOrganismId)) {
					HashSet<Long> alAlignmentParamIdIT = hmToReturn.get(sOrganismId);
					alAlignmentParamIdIT.add(alignmentParamIdIT);
				} else {
					HashSet<Long> alAlignmentParamIdIT = new HashSet<>();
					alAlignmentParamIdIT.add(alignmentParamIdIT);
					hmToReturn.put(sOrganismId, alAlignmentParamIdIT);
				}
			}// while
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try
		

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			HashMap<Integer, HashSet<Long>> mirrorHm = getHmSOrgaId2AlAlignmentParamId_withAlQElementIdsAndAlSOrgaIds(
					conn
					, hsQElementIds
					, alSOrgaIds
					, true
					, false
					);
			for (Map.Entry<Integer, HashSet<Long>> entryMirrorHm : mirrorHm.entrySet()) {
			    //entryMirrorHm.getKey()
				//entryMirrorHm.getValue();
				if (hmToReturn.containsKey(entryMirrorHm.getKey())) {
					HashSet<Long> hsIT = hmToReturn.get(entryMirrorHm.getKey());
					hsIT.addAll(entryMirrorHm.getValue());
				} else {
					hmToReturn.put(entryMirrorHm.getKey(), entryMirrorHm.getValue());
				}
			}
			
//			hmToReturn.putAll(
//					getHmSOrgaId2AlAlignmentParamId_withAlQElementIdsAndAlSOrgaIds(
//							conn
//							, hsQElementIds
//							, alSOrgaIds
//							, true
//							, false
//							)
//					);
		}
		
		return hmToReturn;
		
	}

	
	

	public static HashMap<Long, AlignParamsTmpObjQElementIdAndSElementId> getHMalignmentParamId2QElementIdAndSElementId_WithQOrgaIdAndSOrgaId(
			Connection conn
			, int qOrigamiOrgaIdSent
			, int sOrigamiOrgaIdSent
			, boolean shouldBePresentInTable
			, boolean isMirrorRequest
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignmentParams getHMalignmentParamId2QElementIdAndSElementId_WithQOrgaIdAndSOrgaId"
				+ " qOrigamiOrgaIdSent="+qOrigamiOrgaIdSent
				+ " sOrigamiOrgaIdSent="+sOrigamiOrgaIdSent
				+ " shouldBePresentInTable="+shouldBePresentInTable
				+ " isMirrorRequest="+isMirrorRequest
				;
		
//		if (qOrigamiOrgaIdSent == 2061 && sOrigamiOrgaIdSent == 1855) {
//			System.err.println("HERE "+methodNameToReport);
//		}
		
		HashMap<Long, AlignParamsTmpObjQElementIdAndSElementId> hmToReturn = new HashMap<>();

		
		boolean realShouldBePresentInTable = false;
		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn)) {
			realShouldBePresentInTable = false;
		} else {
			realShouldBePresentInTable = shouldBePresentInTable;
		}

		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			// check valid input
			//no
	
			// build command
			
			// select columns
			//int
			ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			//chm_alColumnSelectAsInt_aliase2columnName.put("alignment_param_id", "alignment_param_id");
			chm_alColumnSelectAsInt_aliase2columnName.put(real_q+"_element_id", real_q+"_element_id");
			chm_alColumnSelectAsInt_aliase2columnName.put(real_s+"_element_id", real_s+"_element_id");
			//String
			//ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			//empty
	//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
			ConcurrentHashMap<String, String> chm_alColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
			chm_alColumnSelectAsLong_aliase2columnName.put("alignment_param_id", "alignment_param_id");
	
			ArrayList<String> tableFrom = new ArrayList<String>();
			tableFrom.add("alignment_params");
			String whereClause = "WHERE "+real_q+"_organism_id = " + qOrigamiOrgaIdSent
					+ " AND "+real_s+"_organism_id = " + sOrigamiOrgaIdSent
					;
	
	
			ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
					chm_alColumnSelectAsInt_aliase2columnName
					, null
					, null
					, chm_alColumnSelectAsLong_aliase2columnName
					, null
					, null
					, tableFrom, whereClause);
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand()
					, methodNameToReport
					);
			
			while (rs.next()) {
				Long alignmentParamIdIT = rs.getLong("alignment_param_id");
				int qElementId = rs.getInt(real_q+"_element_id");
				int sElementId = rs.getInt(real_s+"_element_id");
				if (isMirrorRequest) {
					alignmentParamIdIT = -alignmentParamIdIT;
				}

//				if (qOrigamiOrgaIdSent == 2061 && sOrigamiOrgaIdSent == 1855) {
//					System.err.println("HERE : alignmentParamIdIT="+alignmentParamIdIT+" ; qElementId="+qElementId+" ; sElementId="+sElementId);
//				}
				
				AlignParamsTmpObjQElementIdAndSElementId newQElementIdAndSElementIdIT = new AlignParamsTmpObjQElementIdAndSElementId();
				newQElementIdAndSElementIdIT.setqElementId(qElementId);
				newQElementIdAndSElementIdIT.setsElementId(sElementId);
				hmToReturn.put(alignmentParamIdIT, newQElementIdAndSElementIdIT);
				
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try
		

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			HashMap<Long, AlignParamsTmpObjQElementIdAndSElementId> mirrorHm = getHMalignmentParamId2QElementIdAndSElementId_WithQOrgaIdAndSOrgaId(
					conn
					, qOrigamiOrgaIdSent
					, sOrigamiOrgaIdSent
					, shouldBePresentInTable
					, true
					);
			for (Map.Entry<Long, AlignParamsTmpObjQElementIdAndSElementId> entryMirrorHm : mirrorHm.entrySet()) {
			    //entryMirrorHm.getKey()
				//entryMirrorHm.getValue();
				if (hmToReturn.containsKey(entryMirrorHm.getKey())) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("hmToReturn.containsKey(entryMirrorHm.getKey()) :"
									+ " ; entryMirrorHm.getKey()="+entryMirrorHm.getKey()
									+ " ; entryMirrorHm.getValue()="+entryMirrorHm.getValue()
									+ " ; hmToReturn.getValue()="+hmToReturn.get(entryMirrorHm.getKey())
									)
							);
				} else {
					hmToReturn.put(entryMirrorHm.getKey(), entryMirrorHm.getValue());
				}
			}
			
//			hmToReturn.putAll(
//					getHMalignmentParamId2QElementIdAndSElementId_WithQOrgaIdAndSOrgaId(
//							conn
//							, qOrigamiOrgaIdSent
//							, sOrigamiOrgaIdSent
//							, shouldBePresentInTable
//							, true
//							)
//					);
		}
		if (realShouldBePresentInTable && hmToReturn.isEmpty()) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("shouldBePresentInTable is true and alToReturn.isEmpty()")
					);
		}


//		if (qOrigamiOrgaIdSent == 2061 && sOrigamiOrgaIdSent == 1855) {
//			System.err.println("HERE : hmToReturn="+hmToReturn.toString());
//		}
		return hmToReturn;
		
	}
	
	

	public static HashMap<Long, Integer> getHMAalignmentParamId2sOrganismIdWithQOrganismId(
			Connection conn
			, int qOrgaIdSent
			, boolean shouldBePresentInTable
			, boolean isMirrorRequest
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignmentParams getHMAalignmentParamId2sOrganismIdWithQOrganismId"
				+ " qOrgaIdSent="+qOrgaIdSent
				+ " shouldBePresentInTable="+shouldBePresentInTable
				+ " isMirrorRequest="+isMirrorRequest
				;
		
		HashMap<Long, Integer> hmToReturn = new HashMap<>();
		
		
//		boolean realShouldBePresentInTable = false;
//		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn)) {
//			realShouldBePresentInTable = false;
//		} else {
//			realShouldBePresentInTable = shouldBePresentInTable;
//		}
		
		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}
		
		// check valid input
		//no

		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		//chm_alColumnSelectAsInt_aliase2columnName.put("alignment_param_id", "alignment_param_id");
		chm_alColumnSelectAsInt_aliase2columnName.put(real_s+"_organism_id", real_s+"_organism_id");
		//String
		//ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		ConcurrentHashMap<String, String> chm_alColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsLong_aliase2columnName.put("alignment_param_id", "alignment_param_id");

		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("alignment_params");
		String whereClause = "WHERE "+real_q+"_organism_id = " + qOrgaIdSent;

		/*ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName
				, tableFrom, whereClause);*/

		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName
				, null
				, null
				, chm_alColumnSelectAsLong_aliase2columnName
				, null
				, null
				, tableFrom, whereClause);
		
		//run command
//		hmToReturn.putAll(
//				UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_HM(
//						conn
//						, "alignment_param_id"
//						, Long.class//, Integer.class
//						, real_s+"_organism_id"
//						, Integer.class
//						, objSQLCommandIT
//						, realShouldBePresentInTable
//						, true
//						, true
//						, isMirrorRequest
//						, false
//						, methodNameToReport
//						)
//				);

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				//HashMap<Long, Integer> getHMAalignmentParamId2sOrganismIdWithQOrganismId
				int sOrganismIdIT = rs.getInt(real_s+"_organism_id");
				Long alignmentParamIdIT = rs.getLong("alignment_param_id");
				if (isMirrorRequest) {
					alignmentParamIdIT = -alignmentParamIdIT;
				}
				hmToReturn.put(alignmentParamIdIT, sOrganismIdIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			
			HashMap<Long, Integer> mirrorHm = getHMAalignmentParamId2sOrganismIdWithQOrganismId(
					conn
					, qOrgaIdSent
					, false
					, true
					);
			for (Map.Entry<Long, Integer> entryMirrorHm : mirrorHm.entrySet()) {
			    //entryMirrorHm.getKey()
				//entryMirrorHm.getValue();
				if (hmToReturn.containsKey(entryMirrorHm.getKey())) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("hmToReturn.containsKey(entryMirrorHm.getKey()) :"
									+ " ; entryMirrorHm.getKey()="+entryMirrorHm.getKey()
									+ " ; entryMirrorHm.getValue()="+entryMirrorHm.getValue()
									+ " ; hmToReturn.getValue()="+hmToReturn.get(entryMirrorHm.getKey())
									)
							);
				} else {
					hmToReturn.put(entryMirrorHm.getKey(), entryMirrorHm.getValue());
				}
			}
			
		}
		
		if (shouldBePresentInTable && hmToReturn.isEmpty()) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("shouldBePresentInTable is true and alToReturn.isEmpty()")
					);
		}
		
		return hmToReturn;
		
	}


//	public static ArrayList<Long> getAlAlignmentParamIdWithQElementIdAndSOrgaId(
//			Connection conn
//			, int qElementId
//			, int sOrgaId
//			, boolean shouldBePresentInTable
//			, boolean isMirrorRequest
//			) throws Exception {
//		
//		String methodNameToReport = "QueriesTableAlignmentParams getAlignmentParamIdWithQElementIdAndSOrgaId"
//				+ " qElementId="+qElementId
//				+ " sOrgaId="+sOrgaId
//				+ " shouldBePresentInTable="+shouldBePresentInTable
//				+ " isMirrorRequest="+isMirrorRequest
//				;
//		ArrayList<Long> alToReturn = new ArrayList<>();
//		
//		boolean realShouldBePresentInTable = false;
//		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn)) {
//			realShouldBePresentInTable = false;
//		} else {
//			realShouldBePresentInTable = shouldBePresentInTable;
//		}
//		
//		// check valid input
//		//no
//
//		// build command
//		
//		// select columns
//		//int
//		//ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
//		//chm_alColumnSelectAsInt_aliase2columnName.put("alignment_param_id", "alignment_param_id");
//		//String
//		//ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
//		//empty
////		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
//		ConcurrentHashMap<String, String> chm_alColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
//		chm_alColumnSelectAsLong_aliase2columnName.put("alignment_param_id", "alignment_param_id");
//		
//		ArrayList<String> tableFrom = new ArrayList<String>();
//		tableFrom.add("alignment_params");
//		String whereClause = "";
//		if (isMirrorRequest) {
//			whereClause = "WHERE s_element_id = " + qElementId
//					+ " AND q_organism_id  = " + sOrgaId;
//		} else {
//			whereClause = "WHERE q_element_id = " + qElementId
//					+ " AND s_organism_id  = " + sOrgaId;
//		}
//
//		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
//				//chm_alColumnSelectAsInt_aliase2columnName,
//				//chm_alColumnSelectAsString_aliase2columnName
//				null
//				, null
//				, null
//				, chm_alColumnSelectAsLong_aliase2columnName
//				, null
//				, null
//				, tableFrom
//				, whereClause
//				);
//		
//		//run command
//		alToReturn.addAll(
//				UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_alLong(
//					conn
//					, "alignment_param_id"
//					, objSQLCommandIT
//					, realShouldBePresentInTable
//					, true
//					, isMirrorRequest
//					, methodNameToReport
//					)
//				);
//		
//		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
//			
//			alToReturn.addAll(
//					getAlAlignmentParamIdWithQElementIdAndSOrgaId(
//							conn
//							, qElementId
//							, sOrgaId
//							, false
//							, true
//							)
//					);
//
//			
//			if (shouldBePresentInTable && alToReturn.isEmpty()) {
//				UtilitiesMethodsServer.reportException(
//						methodNameToReport
//						, new Exception("shouldBePresentInTable is true and alToReturn.isEmpty()")
//						);
//			}
//		}
//
//		return alToReturn;
//	}



	//too slow to query table AlignmentParams for that
	/*
	public static HashSet<Integer> getHsAllComparedOrgaIds(
			Connection conn
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignmentParams getHsAllComparedOrgaIds"
				;
		
		HashSet<Integer> hsToReturn = new HashSet<>();
		
		//args to check
		//no
		//mandatory args
		//no
		//UtilitiesMethodsServer.reportException(methodNameToReport,new Exception(""));

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command1 = "SELECT distinct(q_organism_id) FROM alignment_params";
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command1, methodNameToReport);
			while (rs.next()) {
				hsToReturn.add(rs.getInt("q_organism_id"));
			}
			if (rs != null) {
				rs.close();
			}
			
			String command2 = "SELECT distinct(s_organism_id) FROM alignment_params";
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command2, methodNameToReport);
			while (rs.next()) {
				hsToReturn.add(rs.getInt("s_organism_id"));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return hsToReturn;
		
	}*/



	
}
