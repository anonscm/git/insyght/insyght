package fr.inra.jouy.server.queriesTable;


/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentParams.AlignParamsTmpObjQElementIdAndSElementId;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignments.AlignmentsTmpObjAlignmentParamIdAndPairs;
import fr.inra.jouy.shared.TransAPMissGeneHomoInfo;
import fr.inra.jouy.shared.TransAbsoPropResuGeneSet;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.pojos.databaseMapping.TransAlignmPairs;
import fr.inra.jouy.shared.pojos.transitComposite.ConvertSetLongToSortedRangeItem;
import fr.inra.jouy.shared.pojos.transitComposite.PairLongLowerHigherRange;


public class QueriesTableAlignmentPairs {

	
	
	// RQ : each AlignmentId can be positive or negative (require mirror data)
	
	// RQ type for genes
	// type 1 : strong homolog BDBH with avg score 220, average evalue
	// 5.76e-5
	// type 2 : weaker homolog with avg score 137, average evalue
	// 2.2e-4
	// type 3 mismatch
	// type 4 s insertion
	// type 5 q insertion

	static public ArrayList<Integer> getListQGenesIdsWithAlignmentId(
			Connection conn
			, long alignmentId
			, HashSet<Integer> hsTypeToRetrieve
			) throws Exception {
		String q_or_s = "q";
		if (alignmentId < 0) {
			alignmentId = -alignmentId;
			q_or_s = "s";
			hsTypeToRetrieve = getMirrorAlTypeToRetrieve(hsTypeToRetrieve);
		}
//		ArrayList<Long> alAlignmentIdToSend = new ArrayList<Long>();
//		alAlignmentIdToSend.add(alignmentId);
		HashSet<Long> hsAlignmentIdToSend = new HashSet<Long>();
		hsAlignmentIdToSend.add(alignmentId);
		return getAlQOrSGenesIdsWithAlPositiveAlignmentId(
				conn
				//, alAlignmentIdToSend
				, hsAlignmentIdToSend
				, new ArrayList<PairLongLowerHigherRange>()
				, new HashSet<Long>()
				, false
				, hsTypeToRetrieve
				, q_or_s
				);
	}
	

	public static HashMap<Integer, HashSet<Integer>> getHmQGeneId2HsGeneIdsHomologs_WithHsAlignmentIds(
			Connection conn
			, HashSet<Long> hsAlignmentIdsIT
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			) throws Exception {
		
		HashMap<Integer, HashSet<Integer>> hmToReturn = new HashMap<>();
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				hsAlignmentIdsIT
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);
		
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			hmToReturn.putAll(
					getHmQGeneId2HsGeneIdsHomologs_WithAlPositiveAlignmentId(
							conn
							//, alPositiveAlignmentId
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, false
							)
					);
		}

		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			
			// putAll not safe !!!
			HashMap<Integer, HashSet<Integer>> mirrorHm = getHmQGeneId2HsGeneIdsHomologs_WithAlPositiveAlignmentId(
					conn
					//, alNegativeAlignmentId
					, csltsriIT.getNegativeSingletonAsIndividual()
					, csltsriIT.getNegativeRangeAsPair()
					, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
					, convertSetLongToSortedRangeItem_allowGapWithinRange
					, true
					);
			for (Map.Entry<Integer, HashSet<Integer>> entryMirrorHm : mirrorHm.entrySet()) {
			    //entryMirrorHm.getKey()
				//entryMirrorHm.getValue();
				if (hmToReturn.containsKey(entryMirrorHm.getKey())) {
					HashSet<Integer> hsIT = hmToReturn.get(entryMirrorHm.getKey());
					hsIT.addAll(entryMirrorHm.getValue());
				} else {
					hmToReturn.put(entryMirrorHm.getKey(), entryMirrorHm.getValue());
				}
			}
			
		}
		return hmToReturn;
		
	}


	


	static public ArrayList<Integer> getListQGenesIdsWithAlAlignmentId(
			Connection conn
			, HashSet<Long> hsAlignmentId
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, HashSet<Integer> hsTypeToRetrieve
			) throws Exception {
		

		ArrayList<Integer> alToReturn = new ArrayList<>();
		
//		ArrayList<Long> alPositiveAlignmentId = new ArrayList<>();
//		ArrayList<Long> alNegativeAlignmentId = new ArrayList<>();
//		for (Long alignmentIdIT : alAlignmentId) {
//			if (alignmentIdIT < 0) {
//				alNegativeAlignmentId.add(-alignmentIdIT);
//			} else {
//				alPositiveAlignmentId.add(alignmentIdIT);
//			}
//		}
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				hsAlignmentId
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);
		
		//if ( ! alPositiveAlignmentId.isEmpty()) {
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			alToReturn.addAll(
						getAlQOrSGenesIdsWithAlPositiveAlignmentId(
							conn
							//, alPositiveAlignmentId
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, hsTypeToRetrieve
							, "q"
							)
					);
		}
		//if ( ! alNegativeAlignmentId.isEmpty()) {
		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			hsTypeToRetrieve = getMirrorAlTypeToRetrieve(hsTypeToRetrieve);
			alToReturn.addAll(
						getAlQOrSGenesIdsWithAlPositiveAlignmentId(
							conn
							//, alNegativeAlignmentId
							, csltsriIT.getNegativeSingletonAsIndividual()
							, csltsriIT.getNegativeRangeAsPair()
							, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, hsTypeToRetrieve
							, "s"
							)
					);
		}
		return alToReturn;
	}
	

	static public ArrayList<Integer> getListSGenesIdsWithAlignmentId(
			Connection conn
			, long alignmentId
			, HashSet<Integer> hsTypeToRetrieve
			) throws Exception {
		
		String q_or_s = "s";
		if (alignmentId < 0) {
			alignmentId = -alignmentId;
			q_or_s = "q";
			hsTypeToRetrieve = getMirrorAlTypeToRetrieve(hsTypeToRetrieve);
		}
//		ArrayList<Long> alAlignmentIdToSend = new ArrayList<Long>();
//		alAlignmentIdToSend.add(alignmentId);
		HashSet<Long> hsAlignmentIdToSend = new HashSet<Long>();
		hsAlignmentIdToSend.add(alignmentId);
		return getAlQOrSGenesIdsWithAlPositiveAlignmentId(
				conn
				//, alAlignmentIdToSend
				, hsAlignmentIdToSend
				, new ArrayList<PairLongLowerHigherRange>()
				, new HashSet<Long>()
				, false
				, hsTypeToRetrieve
				, q_or_s
				);
	}
	
	
	
	static public ArrayList<Integer> getListSGenesIdsWithAlAlignmentId(
			Connection conn
			, HashSet<Long> hsAlignmentId
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, HashSet<Integer> hsTypeToRetrieve
			) throws Exception {

		
//		ArrayList<Long> alPositiveAlignmentId = new ArrayList<>();
//		ArrayList<Long> alNegativeAlignmentId = new ArrayList<>();
//		for (Long alignmentIdIT : alAlignmentId) {
//			if (alignmentIdIT < 0) {
//				alNegativeAlignmentId.add(-alignmentIdIT);
//			} else {
//				alPositiveAlignmentId.add(alignmentIdIT);
//			}
//		}
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				hsAlignmentId
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);
		
		
		ArrayList<Integer> alToReturn = new ArrayList<>();
		//if ( ! alPositiveAlignmentId.isEmpty()) {
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			alToReturn.addAll(
						getAlQOrSGenesIdsWithAlPositiveAlignmentId(
							conn
							//, alPositiveAlignmentId
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, hsTypeToRetrieve
							, "s"
							)
					);
		}
		//if ( ! alNegativeAlignmentId.isEmpty()) {
		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			hsTypeToRetrieve = getMirrorAlTypeToRetrieve(hsTypeToRetrieve);
			alToReturn.addAll(
						getAlQOrSGenesIdsWithAlPositiveAlignmentId(
							conn
							//, alNegativeAlignmentId
							, csltsriIT.getNegativeSingletonAsIndividual()
							, csltsriIT.getNegativeRangeAsPair()
							, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, hsTypeToRetrieve
							, "q"
							)
					);
		}
		return alToReturn;
	}


	public static ArrayList<HashSet<Integer>> getAlHsQGeneIdAndHsSGeneIdOfAlignmentPairs_withHsAlignmentIdsAndType(
			Connection conn
			, HashSet<Long> hsAlignmentId
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, HashSet<Integer> alTypeToRetrieve
			) throws Exception {
		
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				hsAlignmentId
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);
		ArrayList<HashSet<Integer>> alToReturn = new ArrayList<>();
		HashSet<Integer> hsQGeneIdToReturn = new HashSet<>();
		HashSet<Integer> hsSGeneIdToReturn = new HashSet<>();
		alToReturn.add(hsQGeneIdToReturn);
		alToReturn.add(hsSGeneIdToReturn);
		
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			ArrayList<HashSet<Integer>> interAlToReturn = getAlQAndSGenesIdsWithAlPositiveAlignmentId(
							conn
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, alTypeToRetrieve
							, false
							);
			alToReturn.get(0).addAll(interAlToReturn.get(0));
			alToReturn.get(1).addAll(interAlToReturn.get(1));
		}

		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			alTypeToRetrieve = getMirrorAlTypeToRetrieve(alTypeToRetrieve);
			ArrayList<HashSet<Integer>> interAlToReturn = getAlQAndSGenesIdsWithAlPositiveAlignmentId(
							conn
							, csltsriIT.getNegativeSingletonAsIndividual()
							, csltsriIT.getNegativeRangeAsPair()
							, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, alTypeToRetrieve
							, true
							);
			alToReturn.get(0).addAll(interAlToReturn.get(0));
			alToReturn.get(1).addAll(interAlToReturn.get(1));

		}
		return alToReturn;
				
	}


	public static ArrayList<TransAbsoPropResuGeneSet> getAlTransAbsoPropResuGeneSetType1Or2WithHSAlignmentId2AlignmentParamIdAndPairsANDHSAlignmentParamId2QElementIdAndSElementIdAndAlQGeneIds(
			Connection conn
			, HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> alignmentId2AlignmentParamIdAndPairs
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, HashMap<Long, AlignParamsTmpObjQElementIdAndSElementId> alignmentParamId2QElementIdAndSElementId
			, ArrayList<Integer> alQGeneIds
			//, HashSet<Integer> hsQGeneIds
			) throws Exception {

		ArrayList<TransAbsoPropResuGeneSet> alToReturn = new ArrayList<>();
		
//		ArrayList<Long> alPositiveAlignmentId = new ArrayList<>();
//		ArrayList<Long> alNegativeAlignmentId = new ArrayList<>();
//		for (Long alignmentIdIT : alignmentId2AlignmentParamIdAndPairs.keySet()) {
//			if (alignmentIdIT < 0) {
//				alNegativeAlignmentId.add(-alignmentIdIT);
//			} else {
//				alPositiveAlignmentId.add(alignmentIdIT);
//			}
//		}
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				alignmentId2AlignmentParamIdAndPairs.keySet()
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);
		
		//if ( ! alPositiveAlignmentId.isEmpty()) {
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			alToReturn.addAll(
					getAlTransAbsoPropResuGeneSetType1Or2WithAlPositivetAlignmentIdsAndHSAlignmentId2AlignmentParamIdAndPairsANDHSAlignmentParamId2QElementIdAndSElementIdAndAlQGeneIds(
							conn
							//, alPositiveAlignmentId
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, alignmentId2AlignmentParamIdAndPairs
							, alignmentParamId2QElementIdAndSElementId
							, alQGeneIds
							//, hsQGeneIds
							, false //mirror_query
							)
					);
		}
		//if ( ! alNegativeAlignmentId.isEmpty()) {
		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			alToReturn.addAll(
					getAlTransAbsoPropResuGeneSetType1Or2WithAlPositivetAlignmentIdsAndHSAlignmentId2AlignmentParamIdAndPairsANDHSAlignmentParamId2QElementIdAndSElementIdAndAlQGeneIds(
							conn
							//, alNegativeAlignmentId
							, csltsriIT.getNegativeSingletonAsIndividual()
							, csltsriIT.getNegativeRangeAsPair()
							, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, alignmentId2AlignmentParamIdAndPairs
							, alignmentParamId2QElementIdAndSElementId
							, alQGeneIds
							//, hsQGeneIds
							, true //mirror_query
							)
					);
		}
		return alToReturn;

	}

	
	
	
	private static Collection<? extends TransAbsoPropResuGeneSet> getAlTransAbsoPropResuGeneSetType1Or2WithAlPositivetAlignmentIdsAndHSAlignmentId2AlignmentParamIdAndPairsANDHSAlignmentParamId2QElementIdAndSElementIdAndAlQGeneIds(
			Connection conn
			//, ArrayList<Long> alPositiveAlignmentId
			, HashSet<Long> postiveAlignmentIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> alignmentId2AlignmentParamIdAndPairs
			, HashMap<Long, AlignParamsTmpObjQElementIdAndSElementId> alignmentParamId2QElementIdAndSElementId
			, ArrayList<Integer> alQGeneIds
			//, HashSet<Integer> hsQGeneIds
			, boolean mirror_query
			) throws Exception {
		
		
		String methodNameToReport = "QueriesTableAlignmentPairs getAlTransAbsoPropResuGeneSetType1Or2WithAlPositivetAlignmentIdsAndHSAlignmentId2AlignmentParamIdAndPairsANDHSAlignmentParamId2QElementIdAndSElementIdAndAlQGeneIds"
				//+ " ; alPositiveAlignmentId="+ ( (alPositiveAlignmentId != null ) ? alPositiveAlignmentId.toString() : "NULL" )
				+ " ; postiveAlignmentIdSentSingletonAsIndividual="+ ( (postiveAlignmentIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentIdSentRangeAsPair="+ 
				( (postiveAlignmentIdSentRangeAsPair != null ) ? 
					postiveAlignmentIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; alignmentId2AlignmentParamIdAndPairs.size="+ alignmentId2AlignmentParamIdAndPairs.size()
				+ " ; alignmentParamId2QElementIdAndSElementId.size="+ alignmentParamId2QElementIdAndSElementId.size()
				+ " ; alQGeneIds="+ ( (alQGeneIds != null ) ? alQGeneIds.toString() : "NULL" )
				//+ " ; hsQGeneIds="+ ( (hsQGeneIds != null ) ? hsQGeneIds.toString() : "NULL" )
				+ " ; mirror_query="+mirror_query
				;

		ArrayList<TransAbsoPropResuGeneSet> alToReturn = new ArrayList<>();

		String realQ = "q";
		String realS = "s";
		if (mirror_query) {
			realQ = "s";
			realS = "q";
		}

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
//			String command = "";
//			Collections.sort(alPositiveAlignmentId);
//			HashSet<Long> isQueryingByRangeWorthIt = UtilitiesMethodsServer.isQueryingByRangeWorthIt(alPositiveAlignmentId);
//			if ( isQueryingByRangeWorthIt != null ) {
//				command = "SELECT alignment_id, "+realQ+"_gene_id, "+realS+"_gene_id, type FROM alignment_pairs"
//				+ " WHERE alignment_id >= "
//				+ alPositiveAlignmentId.get(0)
//				+ " AND alignment_id <= "
//				+ alPositiveAlignmentId.get(alPositiveAlignmentId.size()-1)
//				;
//				if (alQGeneIds != null && ! alQGeneIds.isEmpty()) {
//					command += " AND "+realQ+"_gene_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alQGeneIds)
//						+ ")";
//				}
//			} else {
//				command = "SELECT alignment_id, "+realQ+"_gene_id, "+realS+"_gene_id, type FROM alignment_pairs WHERE alignment_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alPositiveAlignmentId)
//						+ ")";
//				if (alQGeneIds != null && ! alQGeneIds.isEmpty()) {
//					command += " AND "+realQ+"_gene_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alQGeneIds)
//						+ ")";
//				}
//			}
			String command = "SELECT alignment_id, "+realQ+"_gene_id, "+realS+"_gene_id, type"
					+ " FROM alignment_pairs WHERE (";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}
			command += " )";
			if (alQGeneIds != null && ! alQGeneIds.isEmpty()) {
				command += " AND ( "+realQ+"_gene_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alQGeneIds)
						+ ") )";
			}
			if ( ! atLeastOneValueToSearch) {
				return alToReturn;
			}
			
			
			
//			long milli = System.currentTimeMillis();
//			long milliPrint2 = -1;
//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("Start command 2 \n"+command);
			
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);
			
//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("HERE2.1\t" + milliPrint2 + "\tmilliseconds");
			
			while (rs.next()) {
				Long alignmentIdIT = rs.getLong("alignment_id");
//				if (isQueryingByRangeWorthIt != null) {
//					if ( ! isQueryingByRangeWorthIt.contains(alignmentIdIT)) {
//						continue;
//					}
//				}
				if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentIdSentSingletonAsIndividual.contains(alignmentIdIT)
							|| cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual.contains(alignmentIdIT)
							) {
						//AlignmentIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				
				int realQGeneId = rs.getInt(realQ+"_gene_id");
				// faster querying with HashSet ?
//				if (hsQGeneIds != null && ! hsQGeneIds.isEmpty()) {
//					if ( ! hsQGeneIds.contains(realQGeneId)) {
//						continue;
//					}
//				}
				int realSGeneId = rs.getInt(realS+"_gene_id");
				int type = rs.getInt("type");
				
				if (mirror_query) {
					alignmentIdIT = -alignmentIdIT;
				}
				if (type == 1 || type == 2) {
					TransAbsoPropResuGeneSet newTAPRS = new TransAbsoPropResuGeneSet();
					newTAPRS.setAlignmentId(alignmentIdIT);
					newTAPRS.setqGeneId(realQGeneId);
					newTAPRS.setsGeneId(realSGeneId);
					newTAPRS.setType(type);
					int pairsIT = alignmentId2AlignmentParamIdAndPairs.get(alignmentIdIT).getPairs();						
					newTAPRS.setNumberGeneInSynteny(pairsIT);
					Long alignmentParamIdIT = alignmentId2AlignmentParamIdAndPairs.get(alignmentIdIT).getAlignmentParamId();

					int qElementIdIT = alignmentParamId2QElementIdAndSElementId.get(alignmentParamIdIT).getqElementId();
					newTAPRS.setqOrigamiElementId(qElementIdIT);
					int sElementIdIT = alignmentParamId2QElementIdAndSElementId.get(alignmentParamIdIT).getsElementId();
					newTAPRS.setsOrigamiElementId(sElementIdIT);
					//						newTAPRS.setqOrigamiElementId(qElementId);
					//						newTAPRS.setsOrigamiElementId(sElementId);
					//						if(alignmentIds2pairs.containsKey(alignmentIdIT)){
					//							newTAPRS.setNumberGeneInSynteny(alignmentIds2pairs.get(alignmentIdIT));
					//						}
					alToReturn.add(newTAPRS);
				}
			}
			
//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("HERE2.2\t" + milliPrint2 + "\tmilliseconds");
			
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
									methodNameToReport
									, ex);
			//return new  ArrayList<TransAbsoPropResuGeneSet>();
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try
		return alToReturn;
	}



	public static int getGeneIdWithSingletonAlignmentIdAndBooleanReferenceGene(
			Connection conn, Long alignmentId, boolean referenceGene) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignmentPairs getGeneIdWithSingletonAlignmentIdAndBooleanReferenceGene "
				+ " ; alignmentId="+alignmentId
				+ " ; referenceGene="+referenceGene
				;
		
		int geneIdToReturn = -1;

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			String command = "SELECT ";
			Long alignmentIdForQuery = alignmentId;
			if (alignmentIdForQuery < 0) {
				alignmentIdForQuery = -alignmentIdForQuery;
				if (referenceGene){
					command += "s_gene_id AS gene_id ";
				} else {
					command += "q_gene_id AS gene_id ";
				}
			} else {
				if (referenceGene){
					command += "q_gene_id AS gene_id ";
				} else {
					command += "s_gene_id AS gene_id ";
				}	
			}
			command += ", type FROM alignment_pairs WHERE alignment_id = "+alignmentIdForQuery;
			
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			if (rs.next()) {
				geneIdToReturn = rs.getInt("gene_id");
				if (rs.getInt("type") != 1){
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("Type is different than 1 for alignment_id = "+alignmentId));
				}
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("More than 1 row found for alignment_id = "+alignmentId));
				}
			}
			if(geneIdToReturn < 0){
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("gene id < 0 for alignment_id = "+alignmentId));
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {

			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return geneIdToReturn;

	}

	
	public static ArrayList<TransAlignmPairs> getListTransientAlignmentPairsWithMainSyntenyAndRelated(
			Connection conn
			, long mainOrigamiSyntenyId
			, ArrayList<Long> listOrigamiAlignmentIdsContainedOtherSyntenies
			, ArrayList<Long> listOrigamiAlignmentIdsMotherSyntenies
			, ArrayList<Long> listOrigamiAlignmentIdsSprungOffSyntenies
			, boolean isContainedWithinAnotherMotherSynteny
			, boolean isMotherOfContainedOtherSyntenies
			, boolean isMotherOfSprungOffSyntenies
			, boolean isSprungOffAnotherMotherSynteny
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignmentPairs getListTransientAlignmentPairsWithMainSyntenyAndRelated"
				+ " ; mainOrigamiSyntenyId="+mainOrigamiSyntenyId
				+ " ; listOrigamiAlignmentIdsContainedOtherSyntenies="+ ( (listOrigamiAlignmentIdsContainedOtherSyntenies != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(listOrigamiAlignmentIdsContainedOtherSyntenies) : "NULL" )
				+ " ; listOrigamiAlignmentIdsMotherSyntenies="+ ( (listOrigamiAlignmentIdsMotherSyntenies != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(listOrigamiAlignmentIdsMotherSyntenies) : "NULL" )
				+ " ; listOrigamiAlignmentIdsSprungOffSyntenies="+ ( (listOrigamiAlignmentIdsSprungOffSyntenies != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(listOrigamiAlignmentIdsSprungOffSyntenies) : "NULL" )
				+ " ; isContainedWithinAnotherMotherSynteny="+ isContainedWithinAnotherMotherSynteny
				+ " ; isMotherOfContainedOtherSyntenies="+ isMotherOfContainedOtherSyntenies
				+ " ; isMotherOfSprungOffSyntenies="+ isMotherOfSprungOffSyntenies
				+ " ; isSprungOffAnotherMotherSynteny="+ isSprungOffAnotherMotherSynteny
				;
		
		ArrayList<TransAlignmPairs> alToReturn = new ArrayList<TransAlignmPairs>();
		ArrayList<Integer> listQGeneIdsOfMainSynteny = new ArrayList<Integer>();

		//Statement statement = null;
		//ResultSet rs = null;
		boolean closeConn = false;
		
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			// deal with main synteny
			alToReturn.addAll(getListTransientAlignmentPairsWithAlignmentId (conn, mainOrigamiSyntenyId, null, listQGeneIdsOfMainSynteny, false, false));
			
			// deal with isContainedWithinAnotherMotherSynteny
			HashSet<Long> hsOrigamiAlignmentIdsMotherSyntenies = new HashSet<>(listOrigamiAlignmentIdsMotherSyntenies);
			if (isContainedWithinAnotherMotherSynteny
					|| isSprungOffAnotherMotherSynteny) {
				alToReturn.addAll(getListTransientAlignmentPairsWithAlAlignmentId (conn, hsOrigamiAlignmentIdsMotherSyntenies, false, listQGeneIdsOfMainSynteny, null, true, false));
			}

			// deal with isMotherOfContainedOtherSyntenies
			if (isMotherOfContainedOtherSyntenies
					|| isMotherOfSprungOffSyntenies) {
				
				HashSet<Long> hsAlignmentIdCatIT = new HashSet<>();
				hsAlignmentIdCatIT.addAll(listOrigamiAlignmentIdsContainedOtherSyntenies);
				hsAlignmentIdCatIT.addAll(listOrigamiAlignmentIdsSprungOffSyntenies);
				alToReturn.addAll(getListTransientAlignmentPairsWithAlAlignmentId (conn, hsAlignmentIdCatIT, false, listQGeneIdsOfMainSynteny, null, true, false));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, ex);
		} finally {
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try

		return alToReturn;
		
	}

	
	static public ArrayList<TransAlignmPairs> getListTransientAlignmentPairsWithAlignmentId(
			Connection conn
			, long alignmentId
			, ArrayList<Integer> restrictToAlQGeneIds
			, ArrayList<Integer> alQGeneIdsToFill
			, boolean restrictToOnlyType1Or2
			, boolean shouldBeNotEmptyOrNull
			) throws Exception {
		
		String methodName = "QueriesTableAlignmentPairs getListTransientAlignmentPairsWithAlignmentId"
				+ " ; alignmentId="+ alignmentId
				+ " ; restrictToAlQGeneIds="+ ( (restrictToAlQGeneIds != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(restrictToAlQGeneIds) : "NULL" )
				+ " ; alQGeneIdsToFill="+ ( (alQGeneIdsToFill != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(alQGeneIdsToFill) : "NULL" )
				+ " ; restrictToOnlyType1Or2="+restrictToOnlyType1Or2
				+ " ; shouldBeNotEmptyOrNull="+shouldBeNotEmptyOrNull
				;
		
		boolean isDataFromMirrorQuery = false;
		if (alignmentId < 0) {
			alignmentId = -alignmentId;
			isDataFromMirrorQuery = true;
		}
//		ArrayList<Long> alAlignmentIdToSend = new ArrayList<Long>();
//		alAlignmentIdToSend.add(alignmentId);
		HashSet<Long> hsAlignmentIdToSend = new HashSet<Long>();
		hsAlignmentIdToSend.add(alignmentId);
		ArrayList<TransAlignmPairs> alToReturn = getListTransientAlignmentPairsWithAlPositiveAlignmentId(
				conn
				//, alAlignmentIdToSend
				, hsAlignmentIdToSend
				, new ArrayList<PairLongLowerHigherRange>()
				, new HashSet<Long>()
				, false
				, restrictToAlQGeneIds
				, alQGeneIdsToFill
				, restrictToOnlyType1Or2
				, isDataFromMirrorQuery
				);
		if (shouldBeNotEmptyOrNull) {
			if (alToReturn == null || alToReturn.isEmpty()) {
				UtilitiesMethodsServer.reportException(
						methodName
						, new Exception("alToReturn is null or empty and it shouldn't : ")
						);
			}
		}
		return alToReturn;
	}
	
	static public ArrayList<TransAlignmPairs> getListTransientAlignmentPairsWithAlAlignmentId(
			Connection conn
			//, ArrayList<Long> alAlignmentId
			, HashSet<Long> hsAlignmentId
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, ArrayList<Integer> restrictToAlQGeneIds
			, ArrayList<Integer> alQGeneIdsToFill
			, boolean restrictToOnlyType1Or2
			, boolean shouldBeNotEmptyOrNull
			) throws Exception {
		
		String methodName = "QueriesTableAlignmentPairs getListTransientAlignmentPairsWithAlAlignmentId"
				//+ " ; alAlignmentId="+ ( (alAlignmentId != null ) ? alAlignmentId.toString() : "NULL" )
				+ " ; hsAlignmentId="+ ( (hsAlignmentId != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(hsAlignmentId) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+ convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; restrictToAlQGeneIds="+ ( (restrictToAlQGeneIds != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(restrictToAlQGeneIds) : "NULL" )
				+ " ; alQGeneIdsToFill="+ ( (alQGeneIdsToFill != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(alQGeneIdsToFill) : "NULL" )
				+ " ; restrictToOnlyType1Or2="+restrictToOnlyType1Or2
				+ " ; shouldBeNotEmptyOrNull="+shouldBeNotEmptyOrNull
				;

		ArrayList<TransAlignmPairs> alToReturn = new ArrayList<>();
		
//		ArrayList<Long> alPositiveAlignmentId = new ArrayList<>();
//		ArrayList<Long> alNegativeAlignmentId = new ArrayList<>();
//		for (Long alignmentIdIT : alAlignmentId) {
//			if (alignmentIdIT < 0) {
//				alNegativeAlignmentId.add(-alignmentIdIT);
//			} else {
//				alPositiveAlignmentId.add(alignmentIdIT);
//			}
//		}
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				hsAlignmentId
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);
		
		//if ( ! alPositiveAlignmentId.isEmpty()) {
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			alToReturn.addAll(
					getListTransientAlignmentPairsWithAlPositiveAlignmentId(
							conn
							//, alPositiveAlignmentId
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, restrictToAlQGeneIds
							, alQGeneIdsToFill
							, restrictToOnlyType1Or2
							, false
							)
					);
		}
		//if ( ! alNegativeAlignmentId.isEmpty()) {
		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			alToReturn.addAll(
					getListTransientAlignmentPairsWithAlPositiveAlignmentId(
							conn
							//, alNegativeAlignmentId
							, csltsriIT.getNegativeSingletonAsIndividual()
							, csltsriIT.getNegativeRangeAsPair()
							, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, restrictToAlQGeneIds
							, alQGeneIdsToFill
							, restrictToOnlyType1Or2
							, true
							)
					);
		}
		if (shouldBeNotEmptyOrNull) {
			if (alToReturn == null || alToReturn.isEmpty()) {
				UtilitiesMethodsServer.reportException(
						methodName
						, new Exception("alToReturn is null or empty and it shouldn't : ")
						);
			}
		}
		return alToReturn;
	}
	
	

	public static TransAPMissGeneHomoInfo getTransAPMissGeneHomoInfosWithAlignmentId(
			Connection conn
			, long alignmentId
			, ArrayList<Integer> restrictToAlQGeneIds
			, ArrayList<Integer> alQGeneIdsToFill
			, boolean restrictToOnlyType1Or2
			) throws Exception {

		
		String methodName = "QueriesTableAlignmentPairs getTransAPMissGeneHomoInfosWithAlignmentId"
				+ " ; alignmentId="+alignmentId
				+ " ; restrictToAlQGeneIds="+ ( (restrictToAlQGeneIds != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(restrictToAlQGeneIds) : "NULL" )
				+ " ; alQGeneIdsToFill="+ ( (alQGeneIdsToFill != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(alQGeneIdsToFill) : "NULL" )
				+ " ; restrictToOnlyType1Or2="+restrictToOnlyType1Or2
				;

		boolean isDataFromMirrorQuery = false;
		if (alignmentId < 0) {
			alignmentId = -alignmentId;
			isDataFromMirrorQuery = true;
		}
		
		HashSet<Long> hsAlignmentIdToSend = new HashSet<Long>();
		hsAlignmentIdToSend.add(alignmentId);
//		ArrayList<Long> alAlignmentIdToSend = new ArrayList<Long>();
//		alAlignmentIdToSend.add(alignmentId);
		ArrayList<TransAlignmPairs> alTAPIT = getListTransientAlignmentPairsWithAlPositiveAlignmentId(
				conn
				//, alAlignmentIdToSend
				, hsAlignmentIdToSend
				, new ArrayList<PairLongLowerHigherRange>()
				, new HashSet<Long>()
				, false
				, restrictToAlQGeneIds
				, alQGeneIdsToFill
				, restrictToOnlyType1Or2
				, isDataFromMirrorQuery
				);
		
		if (alTAPIT.size() == 1) {
			TransAlignmPairs tapIT = alTAPIT.get(0);
			return new TransAPMissGeneHomoInfo(
					tapIT.getAlignmentId()
					, tapIT.getqGeneId()
					, tapIT.getsGeneId()
					, tapIT.getType()
					);
		} else {
			UtilitiesMethodsServer.reportException(
					methodName
					, new Exception("The size of the arraylist returned by getListTransientAlignmentPairsWithAlPositiveAlignmentId is different than 1 : "+alTAPIT.size())
					);
			return null;
		}
		
	}

	


	public static HashMap<Integer, HashSet<Long>> getHmQGeneId2HsAlignmentId_withHsGeneId(
			Connection conn
			, HashSet<Integer> hsQGeneIdIT
			, HashSet<Integer> hsTypes
			, boolean isMirrorRequest
			//, boolean convertSetIntegerToSortedRangeItem_allowGapWithinRange
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignmentPairs getHmQGeneId2HsAlignmentId_withHsGeneId"
				+ " ; hsQGeneIdIT="+ ( (hsQGeneIdIT != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(hsQGeneIdIT) : "NULL" )
				+ " ; hsTypes="+ ( (hsTypes != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(hsTypes) : "NULL" )
				+ " ; isMirrorRequest="+ isMirrorRequest
				;

		String real_q = "q";
		//String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			//real_s = "q";
		}

		HashMap<Integer, HashSet<Long>> hmToReturn = new HashMap<>();
		// check valid input
		if (hsQGeneIdIT == null || hsQGeneIdIT.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("sQGeneIdIT == null || hsQGeneIdIT.isEmpty()"));
		}
		
		// build command
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put(real_q+"_gene_id", real_q+"_gene_id");
		//String
		//ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
		ConcurrentHashMap<String, String> chm_alColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsLong_aliase2columnName.put("alignment_id", "alignment_id");
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("alignment_pairs");
		String whereClause = "WHERE "+real_q+"_gene_id IN ("
				+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsQGeneIdIT)
				+ ")";
		if (hsTypes != null & ! hsTypes.isEmpty()) {
			whereClause += " AND type IN ("
				+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsTypes)
				+ ")";
		}
		
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName
				, null
				, null
				, chm_alColumnSelectAsLong_aliase2columnName
				, null
				, null
				, tableFrom
				, whereClause
				);
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				Long alignmentIdIT = rs.getLong("alignment_id");
				if (isMirrorRequest) {
					alignmentIdIT = -alignmentIdIT;
				}
				int qGeneIdIT = rs.getInt(real_q+"_gene_id");
				if (hmToReturn.containsKey(qGeneIdIT)) {
					HashSet<Long> hsAlignmentIdIT = hmToReturn.get(qGeneIdIT);
					hsAlignmentIdIT.add(alignmentIdIT);
				} else {
					HashSet<Long> hsAlignmentIdIT = new HashSet<>();
					hsAlignmentIdIT.add(alignmentIdIT);
					hmToReturn.put(qGeneIdIT, hsAlignmentIdIT);
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			HashMap<Integer, HashSet<Long>> hmFromMirrorRequest = getHmQGeneId2HsAlignmentId_withHsGeneId(
					conn
					, hsQGeneIdIT
					, hsTypes
					, true
					);
			// putAll not safe !!!
			for (Map.Entry<Integer, HashSet<Long>> entryMirrorHm : hmFromMirrorRequest.entrySet()) {
				if (hmToReturn.containsKey(entryMirrorHm.getKey())) {
					HashSet<Long> hsIT = hmToReturn.get(entryMirrorHm.getKey());
					hsIT.addAll(entryMirrorHm.getValue());
				} else {
					hmToReturn.put(entryMirrorHm.getKey(), entryMirrorHm.getValue());
				}
			}
		}
		
		return hmToReturn;
		
// NOT CORRECT WAY
//		HashMap<Integer, HashSet<Long>> hmToReturn = new HashMap<>();
//		ConvertSetIntegerToSortedRangeItem csitsriIT = UtilitiesMethodsShared.convertSetIntegerToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
//				hsQGeneIdIT
//				, convertSetIntegerToSortedRangeItem_allowGapWithinRange
//				);
//		if ( ! csitsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csitsriIT.getPositiveRangeAsPair().isEmpty() ) {
//			hmToReturn.putAll(
//					getHmQGeneId2HsAlignmentId_WithAlPositiveQGeneId(
//							conn
//							, csitsriIT.getPositiveSingletonAsIndividual()
//							, csitsriIT.getPositiveRangeAsPair()
//							, csitsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual()
//							, convertSetIntegerToSortedRangeItem_allowGapWithinRange
//							, false
//							)
//					);
//		}
////		geneIds can not be negative
////		if ( ! csitsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csitsriIT.getNegativeRangeAsPair().isEmpty() ) {
////			// putAll not safe !!!
////			HashMap<Integer, HashSet<Long>> mirrorHm = 
////					getHmQGeneId2HsAlignmentId_WithAlPositiveQGeneId(
////							conn
////							, csitsriIT.getNegativeSingletonAsIndividual()
////							, csitsriIT.getNegativeRangeAsPair()
////							, csitsriIT.getCumulatedNegativeIntegerWithinRangesAsIndividual()
////							, convertSetIntegerToSortedRangeItem_allowGapWithinRange
////							, true
////					);
////			for (Map.Entry<Integer, HashSet<Long>> entryMirrorHm : mirrorHm.entrySet()) {
////				if (hmToReturn.containsKey(entryMirrorHm.getKey())) {
////					HashSet<Long> hsIT = hmToReturn.get(entryMirrorHm.getKey());
////					hsIT.addAll(entryMirrorHm.getValue());
////				} else {
////					hmToReturn.put(entryMirrorHm.getKey(), entryMirrorHm.getValue());
////				}
////			}
////		}
//		return hmToReturn;
	}


	

	public static ArrayList<Integer> getAlSGeneIdsWithQGeneId(
			Connection conn
			, Integer qGeneId
			, boolean bbdhOnly
			, boolean isMirrorRequest
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignmentPairs getAlSGeneIdsWithQGeneId"
				+ " ; qGeneId="+ qGeneId
				+ " ; bbdhOnly="+ bbdhOnly
				;

		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}
		
		ArrayList<Integer> alToReturn = new ArrayList<Integer>();
		// check valid input
		//no
		// build command
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put(real_s+"_gene_id", real_s+"_gene_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
		//chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("alignment_pairs");
		String whereClause = "WHERE "+real_q+"_gene_id = " + qGeneId;
		if (bbdhOnly) {
			whereClause += " AND type = 1";
		}
		
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		//run command
//		alToReturn.addAll(UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_alInt(
//				conn, 
//				real_s+"_gene_id",
//				objSQLCommandIT, 
//				false, 
//				true, 
//				methodNameToReport));
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				int sGeneIdIT = rs.getInt(real_s+"_gene_id");
				alToReturn.add(sGeneIdIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			ArrayList<Integer> alFromMirrorRequest = getAlSGeneIdsWithQGeneId(
					conn
					, qGeneId
					, bbdhOnly
					, true
					);
			alToReturn.addAll(alFromMirrorRequest);
		}
		
		return alToReturn;
		
	}

	
	// private methods

	private static ArrayList<TransAlignmPairs> getListTransientAlignmentPairsWithAlPositiveAlignmentId(
			Connection conn
			//, ArrayList<Long> alAlignmentIds
			, HashSet<Long> postiveAlignmentIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, ArrayList<Integer> restrictToAlQGeneIds
			, ArrayList<Integer> alQGeneIdsToFill
			, boolean restrictToOnlyType1Or2
			, boolean isDataFromMirrorQuery
			) throws Exception {
		
		ArrayList<TransAlignmPairs> alToReturn = new ArrayList<>();

		String methodName = "QueriesTableAlignmentPairs getListTransientAlignmentPairsWithAlPositiveAlignmentId"
				//+ " ; alAlignmentIds="+ ( (alAlignmentIds != null ) ? alAlignmentIds.toString() : "NULL" )
				+ " ; postiveAlignmentIdSentSingletonAsIndividual="+ ( (postiveAlignmentIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentIdSentRangeAsPair="+ 
				( (postiveAlignmentIdSentRangeAsPair != null ) ? 
					postiveAlignmentIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; restrictToAlQGeneIds="+ ( (restrictToAlQGeneIds != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(restrictToAlQGeneIds) : "NULL" )
				+ " ; alQGeneIdsToFill="+ ( (alQGeneIdsToFill != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(alQGeneIdsToFill) : "NULL" )
				+ " ; restrictToOnlyType1Or2="+restrictToOnlyType1Or2
				+ " ; isDataFromMirrorQuery="+isDataFromMirrorQuery
				;
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		String realQ = "q";
		//String realS = "s";
		if (isDataFromMirrorQuery) {
			realQ = "s";
			//realS = "q";
		}
		
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

//			String command = "";
//			Collections.sort(alAlignmentIds);
//			HashSet<Long> isQueryingByRangeWorthIt = UtilitiesMethodsServer.isQueryingByRangeWorthIt(alAlignmentIds);
//			if ( isQueryingByRangeWorthIt != null ) {
//				command = "SELECT alignment_id, q_gene_id, s_gene_id, type"
//						+ " FROM alignment_pairs"
//						+ " WHERE alignment_id >= "
//						+ alAlignmentIds.get(0)
//						+ " AND alignment_id <= "
//						+ alAlignmentIds.get(alAlignmentIds.size()-1)
//						;
//				if (restrictToAlQGeneIds != null && ! restrictToAlQGeneIds.isEmpty()) {
//					command += " AND "+realQ+"_gene_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(restrictToAlQGeneIds)
//						+ ")";
//				}
//			} else {
//				command = "SELECT alignment_id, q_gene_id, s_gene_id, type"
//						+ " FROM alignment_pairs"
//						+ " WHERE alignment_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alAlignmentIds)
//						+ ")"
//						;
//				if (restrictToAlQGeneIds != null && ! restrictToAlQGeneIds.isEmpty()) {
//					command += " AND "+realQ+"_gene_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(restrictToAlQGeneIds)
//						+ ")";
//				}
//			}
			String command = "SELECT alignment_id, q_gene_id, s_gene_id, type"
					+ " FROM alignment_pairs WHERE (";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}
			command += " )";
			if (restrictToAlQGeneIds != null && ! restrictToAlQGeneIds.isEmpty()) {
				command += " AND "+realQ+"_gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(restrictToAlQGeneIds)
					+ ")";
			}
			if ( ! atLeastOneValueToSearch) {
				return alToReturn;
			}
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodName);
			
			
			// RQ type for genes
			// type 1 : strong homolog with avg score 220, average evalue
			// 5.76e-5
			// type 2 : weaker homolog with avg score 137, average evalue 2.2e-4
			// type 3 mismatch
			// type 4 s insertion
			// type 5 q insertion

			while (rs.next()) {
				Long alignmentId = rs.getLong("alignment_id");
//				if (isQueryingByRangeWorthIt != null) {
//					if ( ! isQueryingByRangeWorthIt.contains(alignment_id)) {
//						continue;
//					}
//				}
				if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentIdSentSingletonAsIndividual.contains(alignmentId)
							|| cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual.contains(alignmentId)
							) {
						//AlignmentIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				int type = rs.getInt("type");
				if (restrictToOnlyType1Or2 && type != 1) {
					if (type != 2) {
						continue;
					}
				}
				
				if (isDataFromMirrorQuery) {
					alignmentId = -alignmentId;
				}
				
				TransAlignmPairs newAPQSGHI = new TransAlignmPairs();
				newAPQSGHI.setAlignmentId(alignmentId);
				int qGeneId = rs.getInt("q_gene_id");
				if (type != 4 && qGeneId < 1) {
					UtilitiesMethodsServer.reportException(
							methodName
							, new Exception("type != 4 has qgene id < 1 for alignment_id = "+ alignmentId));
				}

				if (isDataFromMirrorQuery) {
					newAPQSGHI.setsGeneId(qGeneId);
				} else {
					newAPQSGHI.setqGeneId(qGeneId);
					if (qGeneId > 0 && alQGeneIdsToFill != null) {
						alQGeneIdsToFill.add(qGeneId);
					}
				}
				
				int sGeneId = rs.getInt("s_gene_id");
				if (type != 5 && sGeneId < 1) {
					UtilitiesMethodsServer.reportException(
							methodName
							, new Exception("type != 5 has sgene id < 1 for alignment_id = "+ alignmentId));
				}
				if (isDataFromMirrorQuery) {
					newAPQSGHI.setqGeneId(sGeneId);
					if (sGeneId > 0 && alQGeneIdsToFill != null) {
						alQGeneIdsToFill.add(sGeneId);
					}
				} else {
					newAPQSGHI.setsGeneId(sGeneId);
				}
				
				if (isDataFromMirrorQuery) {
					if (type == 4) {
						type = 5;
					} else if (type == 5) {
						type = 4;
					}
				}
				newAPQSGHI.setType(type);
				alToReturn.add(newAPQSGHI);
				
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodName
					, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodName);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, 
						methodName);
			}

		}// try

		
		if (!alToReturn.isEmpty()) {
			return alToReturn;
		} else {
			UtilitiesMethodsServer.reportException(
					methodName
					, new Exception("alToReturn is empty"));
			return alToReturn;
		}
		
		
	}

	

	// NOT CORRECT WAY TO SEARCH WITH ONLY POSITIVE Q_GENE_IDS
//	private static HashMap<Integer, HashSet<Long>> getHmQGeneId2HsAlignmentId_WithAlPositiveQGeneId(
//			Connection conn
//			, HashSet<Integer> postiveQGeneIdSentSingletonAsIndividual
//			, ArrayList<PairIntegerLowerHigherRange> postiveQGeneIdSentRangeAsPair
//			, HashSet<Integer> cumulatedPostiveQGeneIdSentWithinRangesAsIndividual
//			, boolean convertSetIntegerToSortedRangeItem_allowGapWithinRange
//			, boolean isMirrorRequest
//			) throws Exception {
//
//
//		String methodNameToReport = "QueriesTableAlignmentPairs getHmQGeneId2HsAlignmentId_WithAlPositiveGeneId"
//				//+ " ; alAlignmentId="+ ( (alAlignmentId != null ) ? alAlignmentId.toString() : "NULL" )
//				+ " ; postiveQGeneIdSentSingletonAsIndividual="+ ( (postiveQGeneIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveQGeneIdSentSingletonAsIndividual) : "NULL" )
//				+ " ; postiveQGeneIdSentRangeAsPair="+ 
//				( (postiveQGeneIdSentRangeAsPair != null ) ? 
//						postiveQGeneIdSentRangeAsPair.stream()
//					.map(pairIntegerLowerHigherRangeIT -> "[" + pairIntegerLowerHigherRangeIT.getLowerRange().toString() + ", " + pairIntegerLowerHigherRangeIT.getHigherRange().toString() +"]")
//					.collect(Collectors.toList())
//					.toString()
//				 : "NULL" )
//				+ " ; cumulatedPostiveQGeneIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveQGeneIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveQGeneIdSentWithinRangesAsIndividual) : "NULL" )
//				+ " ; convertSetIntegerToSortedRangeItem_allowGapWithinRange="+convertSetIntegerToSortedRangeItem_allowGapWithinRange
//				+ " ; isMirrorRequest="+isMirrorRequest
//				;
//
//		HashMap<Integer, HashSet<Long>> hmToReturn = new HashMap<>();
//
//		// check valid input
//		//no
//		
//		String real_q = "q";
//		//String real_s = "s";
//		if (isMirrorRequest) {
//			real_q = "s";
//			//real_s = "q";
//		}
//		Statement statement = null;
//		ResultSet rs = null;
//		boolean closeConn = false;
//		
//		try {
//			if (conn == null) {
//				conn = DatabaseConf.getConnection_db();
//				closeConn = true;
//			}
//			statement = conn.createStatement();
//
//			String command = "SELECT alignment_id, "+real_q+"_gene_id"
//					+ " FROM alignment_pairs WHERE (";
//			boolean atLeastOneValueToSearch = false;
//			if ( ! postiveQGeneIdSentSingletonAsIndividual.isEmpty()) {
//				command += " ( "+real_q+"_gene_id IN ( "
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveQGeneIdSentSingletonAsIndividual)
//						+ ") )";
//				atLeastOneValueToSearch = true;
//			}
//			if ( ! postiveQGeneIdSentRangeAsPair.isEmpty()) {
//				for (PairIntegerLowerHigherRange pilhrIT : postiveQGeneIdSentRangeAsPair) {
//					if (atLeastOneValueToSearch) {
//						command += " OR";
//					}
//					command += " ( "+real_q+"_gene_id >= "
//							+ pilhrIT.getLowerRange()
//							+ " AND "+real_q+"_gene_id <= "
//							+ pilhrIT.getHigherRange()
//							+ " )"
//							;
//					atLeastOneValueToSearch = true;
//				}
//			}
//			command += " )";
//			command += " AND type IN (1,2)";
//
//
//			if ( ! atLeastOneValueToSearch) {
//				return hmToReturn;
//			}
//
//			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
//					, methodNameToReport
//					);
//			
//			while (rs.next()) {
//				
//				int qGeneIdIT = rs.getInt(real_q+"_gene_id");
//
//				if ( ! postiveQGeneIdSentRangeAsPair.isEmpty() && convertSetIntegerToSortedRangeItem_allowGapWithinRange) {
//					if (postiveQGeneIdSentSingletonAsIndividual.contains(qGeneIdIT)
//							|| cumulatedPostiveQGeneIdSentWithinRangesAsIndividual.contains(qGeneIdIT)
//							) {
//						//AlignmentIdIsQueryInput = true;
//					} else {
//						continue;
//					}
//				}
//				
//				Long alignmentIdIT = rs.getLong("alignment_id");
//				
//				HERE negative if isMirrorRequest ???
//				
//				//HashMap<Integer, HashSet<Integer>> getHmQGeneId2HsGeneIdsHomologs
//				if (hmToReturn.containsKey(qGeneIdIT)) {
//					HashSet<Long> HsAlignmentIdIT = hmToReturn.get(qGeneIdIT);
//					HsAlignmentIdIT.add(alignmentIdIT);
//				} else {
//					HashSet<Long> HsAlignmentIdIT = new HashSet<>();
//					HsAlignmentIdIT.add(alignmentIdIT);
//					hmToReturn.put(qGeneIdIT, HsAlignmentIdIT);
//				}
//				//alGeneIds.add(geneIdIt);
//			}
//
//			
//		} catch (Exception ex) {
//			UtilitiesMethodsServer.reportException(
//					methodNameToReport
//					, ex);
//		} finally {
//			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
//			if (closeConn) {
//				DatabaseConf.freeConnection(conn
//						, methodNameToReport);
//			}
//
//		}// try
//
//		return hmToReturn;
//
//	}


	private static HashMap<Integer, HashSet<Integer>> getHmQGeneId2HsGeneIdsHomologs_WithAlPositiveAlignmentId(
			Connection conn
			, HashSet<Long> postiveAlignmentIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, boolean isMirrorRequest
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignmentPairs getHmQGeneId2HsGeneIdsHomologs_WithAlPositiveAlignmentId"
				//+ " ; alAlignmentId="+ ( (alAlignmentId != null ) ? alAlignmentId.toString() : "NULL" )
				+ " ; postiveAlignmentIdSentSingletonAsIndividual="+ ( (postiveAlignmentIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentIdSentRangeAsPair="+ 
				( (postiveAlignmentIdSentRangeAsPair != null ) ? 
					postiveAlignmentIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; isMirrorRequest="+isMirrorRequest
				;
		
		HashMap<Integer, HashSet<Integer>> hmToReturn = new HashMap<>();
		
		// check valid input
		//no
		
		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT alignment_id, q_gene_id, s_gene_id"
					+ " FROM alignment_pairs WHERE (";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}
			command += " )";
			command += " AND type IN (1,2)";

			
			if ( ! atLeastOneValueToSearch) {
				return hmToReturn;
			}
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);
			
			while (rs.next()) {
				Long alignmentIdIT = rs.getLong("alignment_id");

				if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentIdSentSingletonAsIndividual.contains(alignmentIdIT)
							|| cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual.contains(alignmentIdIT)
							) {
						//AlignmentIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				
				int qGeneIdIt = rs.getInt(real_q+"_gene_id");
				int sGeneIdIt = rs.getInt(real_s+"_gene_id");

				//HashMap<Integer, HashSet<Integer>> getHmQGeneId2HsGeneIdsHomologs
				if (hmToReturn.containsKey(qGeneIdIt)) {
					HashSet<Integer> hsGeneIdsHomologsIT = hmToReturn.get(qGeneIdIt);
					hsGeneIdsHomologsIT.add(sGeneIdIt);
				} else {
					HashSet<Integer> hsGeneIdsHomologsIT = new HashSet<>();
					hsGeneIdsHomologsIT.add(sGeneIdIt);
					hmToReturn.put(qGeneIdIt, hsGeneIdsHomologsIT);
				}
				//alGeneIds.add(geneIdIt);
			}

			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn
						, methodNameToReport);
			}

		}// try

		return hmToReturn;

	}
	
	
	// hsQGeneId idx : 0
	// hsSGeneId idx : 1
	private static ArrayList<HashSet<Integer>> getAlQAndSGenesIdsWithAlPositiveAlignmentId (
			Connection conn
			, HashSet<Long> postiveAlignmentIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, HashSet<Integer> hsTypeToRetrieve
			, boolean isMirrorRequest
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignmentPairs getAlQAndSGenesIdsWithAlPositiveAlignmentId"
				//+ " ; alAlignmentId="+ ( (alAlignmentId != null ) ? alAlignmentId.toString() : "NULL" )
				+ " ; postiveAlignmentIdSentSingletonAsIndividual="+ ( (postiveAlignmentIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentIdSentRangeAsPair="+ 
				( (postiveAlignmentIdSentRangeAsPair != null ) ? 
					postiveAlignmentIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; hsTypeToRetrieve="+ ( (hsTypeToRetrieve != null ) ? hsTypeToRetrieve.toString() : "NULL" )
				+ " ; isMirrorRequest = "+isMirrorRequest
				;
		
		ArrayList<HashSet<Integer>> alToReturn = new ArrayList<>();
		HashSet<Integer> hsQGeneIdToReturn = new HashSet<>();
		HashSet<Integer> hsSGeneIdToReturn = new HashSet<>();
		alToReturn.add(hsQGeneIdToReturn);
		alToReturn.add(hsSGeneIdToReturn);

		// check valid input
		//no
		
		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			

			String command = "SELECT alignment_id, "+real_q+"_gene_id, "+real_s+"_gene_id"
					+ " FROM alignment_pairs WHERE (";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}
			command += " )";
			if (hsTypeToRetrieve != null && ! hsTypeToRetrieve.isEmpty()) {
				command += " AND type IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsTypeToRetrieve)
						+ ")";
						;
			}
			if ( ! atLeastOneValueToSearch) {
				return alToReturn;
			}
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);

			while (rs.next()) {
				Long alignmentIdIT = rs.getLong("alignment_id");
				
				if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentIdSentSingletonAsIndividual.contains(alignmentIdIT)
							|| cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual.contains(alignmentIdIT)
							) {
						//AlignmentIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				
				int qGeneIdIt = rs.getInt(real_q+"_gene_id");
				int sGeneIdIt = rs.getInt(real_s+"_gene_id");
				hsQGeneIdToReturn.add(qGeneIdIt);
				hsSGeneIdToReturn.add(sGeneIdIt);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn
						, methodNameToReport);
			}

		}// try

		return alToReturn;
		
	}
	
	private static ArrayList<Integer> getAlQOrSGenesIdsWithAlPositiveAlignmentId (
			Connection conn
			//, ArrayList<Long> alAlignmentId
			, HashSet<Long> postiveAlignmentIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, HashSet<Integer> hsTypeToRetrieve
			, String q_or_s
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignmentPairs getAlQOrSGenesIdsWithAlPositiveAlignmentId"
				//+ " ; alAlignmentId="+ ( (alAlignmentId != null ) ? alAlignmentId.toString() : "NULL" )
				+ " ; postiveAlignmentIdSentSingletonAsIndividual="+ ( (postiveAlignmentIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentIdSentRangeAsPair="+ 
				( (postiveAlignmentIdSentRangeAsPair != null ) ? 
					postiveAlignmentIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; hsTypeToRetrieve="+ ( (hsTypeToRetrieve != null ) ? hsTypeToRetrieve.toString() : "NULL" )
				+ " ; q_or_s="+q_or_s
				;
		
		ArrayList<Integer> alGeneIds = new ArrayList<Integer>();
		
		// check valid input
		//no
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
//			String command = "";
//			Collections.sort(alAlignmentId);
//			HashSet<Long> isQueryingByRangeWorthIt = UtilitiesMethodsServer.isQueryingByRangeWorthIt(alAlignmentId);
//			if ( isQueryingByRangeWorthIt != null ) {
//				command = "SELECT alignment_id, "+q_or_s+"_gene_id"
//						+ " FROM alignment_pairs"
//						+ " WHERE alignment_id >= "
//						+ alAlignmentId.get(0)
//						+ " AND alignment_id <= "
//						+ alAlignmentId.get(alAlignmentId.size()-1)
//						;
//				if (alTypeToRetrieve != null && ! alTypeToRetrieve.isEmpty()) {
//					command += " AND type IN ("
//							+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alTypeToRetrieve)
//							+ ")";
//							;
//				}
//			} else {
//				command = "SELECT alignment_id, "+q_or_s+"_gene_id"
//						+ " FROM alignment_pairs"
//						+ " WHERE alignment_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alAlignmentId)
//						+ ")";
	//			if (alTypeToRetrieve != null && ! alTypeToRetrieve.isEmpty()) {
	//			command += " AND type IN ("
	//					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alTypeToRetrieve)
	//					+ ")";
	//					;
//			}
			
			String command = "SELECT alignment_id, "+q_or_s+"_gene_id"
					+ " FROM alignment_pairs WHERE (";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}
			command += " )";
			if (hsTypeToRetrieve != null && ! hsTypeToRetrieve.isEmpty()) {
				command += " AND type IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsTypeToRetrieve)
						+ ")";
						;
			}
			if ( ! atLeastOneValueToSearch) {
				return alGeneIds;
			}

			
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);
			
			while (rs.next()) {
				Long alignmentIdIT = rs.getLong("alignment_id");
				
//				if (isQueryingByRangeWorthIt != null) {
//					if ( ! isQueryingByRangeWorthIt.contains(alignmentIdIT)) {
//						continue;
//					}
//				}
				if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentIdSentSingletonAsIndividual.contains(alignmentIdIT)
							|| cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual.contains(alignmentIdIT)
							) {
						//AlignmentIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				
				int geneIdIt = rs.getInt(q_or_s+"_gene_id");
				alGeneIds.add(geneIdIt);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn
						, methodNameToReport);
			}

		}// try

		
		if (!alGeneIds.isEmpty()) {
			return alGeneIds;
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("alGeneIds is empty"));
			return null;
		}
		
	}


	private static HashSet<Integer> getMirrorAlTypeToRetrieve(
			Collection<Integer> collTypeToRetrieve
			) throws Exception {
		
		// RQ type for genes
		// type 1 : strong homolog BDBH with avg score 220, average evalue
		// 5.76e-5
		// type 2 : weaker homolog with avg score 137, average evalue
		// 2.2e-4
		// type 3 mismatch
		// type 4 s insertion
		// type 5 q insertion
		
		HashSet<Integer> hsToReturn = new HashSet<>();
		
		for (Integer typeToRetrieveIT : collTypeToRetrieve) {
			if (typeToRetrieveIT == 4) {
				hsToReturn.add(5);
			} else if (typeToRetrieveIT == 5) {
				hsToReturn.add(4);
			} else {
				hsToReturn.add(typeToRetrieveIT);
			}
		}
		return hsToReturn;
		
	}




	
	
}
