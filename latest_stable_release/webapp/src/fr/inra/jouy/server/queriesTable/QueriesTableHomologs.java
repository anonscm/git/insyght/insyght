package fr.inra.jouy.server.queriesTable;

/*
Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.pojos.databaseMapping.GeneMatchItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneMatchItem;


public class QueriesTableHomologs {
	
	//method that submit queries

	public static HashMap<Integer, HashSet<Integer>> getHmqGeneId2HsSOrgaIdWithAlQGeneIdsOrQOrgaId_optionalRestrictSorgaId(
			Connection conn
			, Integer qOrgaId
			, ArrayList<Integer> alQGeneIds
			, ArrayList<Integer> restrictToAlSOrgaIds
			, boolean doNotIncludeRefOrgaInResultsAkaNoParalogs
			, boolean isMirrorRequest
			) throws Exception {
		

		String methodNameToReport = "QueriesTableHomologs getHmqGeneId2HsSOrgaIdWithQOrgaId_optionalRestrictAlQGeneIdsOrSorgaId"
				+ " ; qOrgaId="+qOrgaId
				+ " ; alQGeneIds="+ ( (alQGeneIds != null ) ? alQGeneIds.toString() : "NULL" )
				+ " ; restrictToAlSOrgaIds="+ ( (restrictToAlSOrgaIds != null ) ? restrictToAlSOrgaIds.toString() : "NULL" )
				+ " ; doNotIncludeRefOrgaInResultsAkaNoParalogs="+doNotIncludeRefOrgaInResultsAkaNoParalogs
				;

		HashMap<Integer, HashSet<Integer>> hmToReturn = new HashMap<>();
		
		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}
		
		// check valid input
		//no
		//mandatory params
		if (
				( alQGeneIds != null && ! alQGeneIds.isEmpty() )
				|| qOrgaId > 0
				) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT ( alQGeneIds != null && ! alQGeneIds.isEmpty() ) || qOrgaId > 0")
					);
		}
		
		if ( ! isMirrorRequest && ! doNotIncludeRefOrgaInResultsAkaNoParalogs ) {
			if ( ! restrictToAlSOrgaIds.contains(qOrgaId)) {
				restrictToAlSOrgaIds.add(qOrgaId);
			}
		}
		
		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put(real_q+"_gene_id", real_q+"_gene_id");
		chm_alColumnSelectAsInt_aliase2columnName.put(real_s+"_organism_id", real_s+"_organism_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		//empty
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("homologies");
		String whereClause = "";
		if(alQGeneIds != null && ! alQGeneIds.isEmpty()){
			whereClause += "WHERE "+real_q+"_gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alQGeneIds)
					+ ")";
		} else {
			whereClause += "WHERE "+real_q+"_organism_id = "+qOrgaId;
		}
		if(restrictToAlSOrgaIds != null && ! restrictToAlSOrgaIds.isEmpty()){
			whereClause += " AND "+real_s+"_organism_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(restrictToAlSOrgaIds)
						+ ")";
			
		}
		if(doNotIncludeRefOrgaInResultsAkaNoParalogs && qOrgaId != null && qOrgaId > 0){
			whereClause += " AND "+real_s+"_organism_id != "+qOrgaId;
		}
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);

////		//run command
//		hmToReturn.putAll(UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_HMofHS(
//					conn, 
//					real_q+"_gene_id",
//					Integer.class,
//					real_s+"_organism_id",
//					Integer.class,
//					objSQLCommandIT,
//					false, 
//					true, 
//					methodNameToReport)
//				);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				int qGeneIdIT = rs.getInt(real_q+"_gene_id");
				int sOrganismIdIT = rs.getInt(real_s+"_organism_id");
				if (hmToReturn.containsKey(qGeneIdIT)) {
					HashSet<Integer> hsSOrganismIdIT = hmToReturn.get(qGeneIdIT);
					hsSOrganismIdIT.add(sOrganismIdIT);
				} else {
					HashSet<Integer> hsSOrganismIdIT = new HashSet<>();
					hsSOrganismIdIT.add(sOrganismIdIT);
					hmToReturn.put(qGeneIdIT, hsSOrganismIdIT);
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			
			
			if ( ! doNotIncludeRefOrgaInResultsAkaNoParalogs && restrictToAlSOrgaIds.size() == 1) {
				// get paralog only, do not do query mirror as it will result in the same query
			} else {
				HashMap<Integer, HashSet<Integer>>  mirrorToAddToHmToReturn = getHmqGeneId2HsSOrgaIdWithAlQGeneIdsOrQOrgaId_optionalRestrictSorgaId(
						conn
						, qOrgaId
						, alQGeneIds
						, restrictToAlSOrgaIds
						, doNotIncludeRefOrgaInResultsAkaNoParalogs
						, true
						);
				
				//merge two hasmap, using method addAll if key already exists, put if key does not exists
				for (Map.Entry<Integer, HashSet<Integer>> entry : mirrorToAddToHmToReturn.entrySet()) {
				    //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
					int keyQGeneId = entry.getKey();
					HashSet<Integer> valueHsSOrgaId = entry.getValue();
					if (hmToReturn.containsKey(keyQGeneId)) {
						HashSet<Integer> hsSOrgaIdToMergeIn = hmToReturn.get(keyQGeneId);
						hsSOrgaIdToMergeIn.addAll(valueHsSOrgaId);
					} else {
						hmToReturn.put(keyQGeneId, valueHsSOrgaId);
					}
				}
			}

			
		}
		
		return hmToReturn;
		
		//OLD WAY
//		//run command
//		ArrayList<ObjAssociationAsMap> alOAM = UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS(
//						conn, 
//						ObjAssociationAsMap.class,
//						objSQLCommandIT, 
//						true, 
//						true, 
//						methodNameToReport);
//		//map to object
//		HashMap<Integer, HashSet<Integer>> hmqGeneId2HsSOrgaIdToReturn = new HashMap<Integer, HashSet<Integer>>();
//		for(ObjAssociationAsMap oamIT : alOAM){
//			int qGeneId = oamIT.getRsColumn2Int().get("q_gene_id");
//			int sOrganismId = oamIT.getRsColumn2Int().get("s_organism_id");
//			if( hmqGeneId2HsSOrgaIdToReturn.containsKey(qGeneId) ){
//				HashSet<Integer> existingHs = hmqGeneId2HsSOrgaIdToReturn.get(qGeneId);
//				existingHs.add(sOrganismId);
//			} else {
//				HashSet<Integer> newHs = new HashSet<Integer>();
//				newHs.add(sOrganismId);
//				hmqGeneId2HsSOrgaIdToReturn.put(qGeneId, newHs);
//			}
//		}	
//		return hmqGeneId2HsSOrgaIdToReturn;
		
	}

	
	public static HashMap<Integer, HashSet<Integer>> getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId(
			Connection conn
			, Integer qOrgaId
			, ArrayList<Integer> alQGeneIds
			, ArrayList<Integer> restrictToAlSOrgaIds
			, boolean doNotIncludeRefOrgaInResultsAkaNoParalogs
			, boolean isMirrorRequest
			) throws Exception {
		

		String methodNameToReport = "QueriesTableHomologs getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId"
				+ " ; qOrgaId="+qOrgaId
				+ " ; alQGeneIds="+ ( (alQGeneIds != null ) ? alQGeneIds.toString() : "NULL" )
				+ " ; restrictToAlSOrgaIds="+ ( (restrictToAlSOrgaIds != null ) ? restrictToAlSOrgaIds.toString() : "NULL" )
				+ " ; doNotIncludeRefOrgaInResultsAkaNoParalogs="+doNotIncludeRefOrgaInResultsAkaNoParalogs
				;

		HashMap<Integer, HashSet<Integer>> hmToReturn = new HashMap<>();
		
		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}
		
		// check valid input
		//no
		//mandatory params
		if (
				( alQGeneIds != null && ! alQGeneIds.isEmpty() )
				|| qOrgaId > 0
				) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT ( alQGeneIds != null && ! alQGeneIds.isEmpty() ) || qOrgaId > 0")
					);
		}
		
		
		if ( ! isMirrorRequest && ! doNotIncludeRefOrgaInResultsAkaNoParalogs ) {
			if ( ! restrictToAlSOrgaIds.contains(qOrgaId)) {
				restrictToAlSOrgaIds.add(qOrgaId);
			}
		}
		
		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put(real_q+"_gene_id", real_q+"_gene_id");
		chm_alColumnSelectAsInt_aliase2columnName.put(real_s+"_gene_id", real_s+"_gene_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		//empty
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("homologies");
		String whereClause = "";
		if(alQGeneIds != null && ! alQGeneIds.isEmpty()){
			whereClause += "WHERE "+real_q+"_gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alQGeneIds)
					+ ")";
		} else {
			whereClause += "WHERE "+real_q+"_organism_id = "+qOrgaId;
		}
		if(restrictToAlSOrgaIds != null && ! restrictToAlSOrgaIds.isEmpty()){
			whereClause += " AND "+real_s+"_organism_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(restrictToAlSOrgaIds)
						+ ")";
		}
		if(doNotIncludeRefOrgaInResultsAkaNoParalogs && qOrgaId != null && qOrgaId > 0){
			whereClause += " AND "+real_s+"_organism_id != "+qOrgaId;
		}
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);		
		
//		//run command
//		hmToReturn.putAll(UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_HMofHS(
//					conn, 
//					real_q+"_gene_id",
//					Integer.class,
//					real_s+"_gene_id",
//					Integer.class,
//					objSQLCommandIT,
//					false, 
//					true, 
//					methodNameToReport)
//				);
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				int qGeneIdIT = rs.getInt(real_q+"_gene_id");
				int sGeneIdIT = rs.getInt(real_s+"_gene_id");
				//HashMap<Integer, HashSet<Integer>> getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId
				if (hmToReturn.containsKey(qGeneIdIT)) {
					HashSet<Integer> hsSOrgaIdToMergeIn = hmToReturn.get(qGeneIdIT);
					hsSOrgaIdToMergeIn.add(sGeneIdIT);
				} else {
					HashSet<Integer> hsIT = new HashSet<>();
					hsIT.add(sGeneIdIT);
					hmToReturn.put(qGeneIdIT, hsIT);
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			
			if ( ! doNotIncludeRefOrgaInResultsAkaNoParalogs && restrictToAlSOrgaIds.size() == 1) {
				// get paralog only, do not do query mirror as it will result in the same query
			} else {

				HashMap<Integer, HashSet<Integer>>  mirrorToAddToHmToReturn = getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId(
						conn
						, qOrgaId
						, alQGeneIds
						, restrictToAlSOrgaIds
						, doNotIncludeRefOrgaInResultsAkaNoParalogs
						, true
						);
				
				//merge two hasmap, using method addAll if key already exists, put if key does not exists
				for (Map.Entry<Integer, HashSet<Integer>> entry : mirrorToAddToHmToReturn.entrySet()) {
				    //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
					int keyQGeneId = entry.getKey();
					HashSet<Integer> valueHsSOrgaId = entry.getValue();
					if (hmToReturn.containsKey(keyQGeneId)) {
						HashSet<Integer> hsSOrgaIdToMergeIn = hmToReturn.get(keyQGeneId);
						hsSOrgaIdToMergeIn.addAll(valueHsSOrgaId);
					} else {
						hmToReturn.put(keyQGeneId, valueHsSOrgaId);
					}
				}
			}
		}
		
		return hmToReturn;
		
	}

	
	/*
	//tested
	public static HashMap<Integer, HashSet<Integer>> getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId(
			Connection conn,
			Integer qOrgaId,
			ArrayList<Integer> optionalListQGeneIdsToRestrictSearch,
			ArrayList<Integer> optionalListSOrgaIdsToRestrictSearch,
			boolean doNotIncludeRefOrgaInResultsAkaNoParalogs) throws Exception {
		
		String methodNameToReport = "QueriesTableHomologs getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId"
				+ " ; referenceOrgaId="+qOrgaId
				+ " ; optionalListQGeneIdsToRestrictSearch="+ ( (optionalListQGeneIdsToRestrictSearch != null ) ? optionalListQGeneIdsToRestrictSearch.toString() : "NULL" )
				+ " ; optionalListSOrgaIdsToRestrictSearch="+ ( (optionalListSOrgaIdsToRestrictSearch != null ) ? optionalListSOrgaIdsToRestrictSearch.toString() : "NULL" )
				+ " ; doNotIncludeRefOrgaInResultsAkaNoParalogs="+doNotIncludeRefOrgaInResultsAkaNoParalogs
				;
		
		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("q_gene_id", "q_gene_id");
		chm_alColumnSelectAsInt_aliase2columnName.put("s_gene_id", "s_gene_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		//empty
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("homologies");
		String whereClause = "";
		boolean queryByQGeneIds = false;
		if(optionalListQGeneIdsToRestrictSearch != null){
			if( ! optionalListQGeneIdsToRestrictSearch.isEmpty()){
				queryByQGeneIds = true;
			}
		}
		if( queryByQGeneIds ){
			whereClause += "WHERE q_gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(optionalListQGeneIdsToRestrictSearch)
					+ ")";
		} else {
			whereClause += "WHERE q_organism_id = "+qOrgaId;
		}
		if(optionalListSOrgaIdsToRestrictSearch != null){
			if( ! optionalListSOrgaIdsToRestrictSearch.isEmpty()){
				whereClause += " AND s_organism_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(optionalListSOrgaIdsToRestrictSearch)
						+ ")";
			}
		}
		if(doNotIncludeRefOrgaInResultsAkaNoParalogs){
			whereClause += " AND s_organism_id != "+qOrgaId;
		}
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
//		//run command
		return UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_HMofHS(
				conn, 
				"q_gene_id",
				Integer.class,
				"s_gene_id",
				Integer.class,
				objSQLCommandIT,
				true, 
				true, 
				methodNameToReport);
	}*/


	// JDBC can convert from Double to Int directly : https://docs.microsoft.com/en-us/sql/connect/jdbc/understanding-data-type-conversions
	public static HashMap<Integer, Integer> getHMorgaId2cummulativeSumAsIntWithQOrganismId (
			Connection conn
			, int qOrgaId
			, String columnNameTableToSumUp
			, boolean checkValidFreeStringInput
			, boolean isMirrorRequest
			) throws Exception {
		
		String methodNameToReport = "QueriesTableHomologs getHMorgaId2cummulativeSumAsIntWithQOrganismId"
				+ " qOrgaId="+qOrgaId
				+ " columnNameTableToSumUp="+columnNameTableToSumUp
				+ " checkValidFreeStringInput="+checkValidFreeStringInput
				+ " isMirrorRequest="+isMirrorRequest
				;
		HashMap<Integer, Integer> hmToReturn = new HashMap<>();

		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}
		// check valid input
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					columnNameTableToSumUp, "columnNameTableToSumUp", false, false);
		}
		//mandatory params
		if (qOrgaId > 0) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT qOrgaId > 0")
					);
		}
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put(real_s+"_organism_id", real_s+"_organism_id");
		chm_alColumnSelectAsInt_aliase2columnName.put("SUM", "SUM("+columnNameTableToSumUp+")");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
		//chm_alColumnSelectAsString_aliase2columnName.put("??", "??");

		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("homologies");
		String whereClause = "WHERE "+real_q+"_organism_id = " + qOrgaId
				+ " GROUP BY "+real_s+"_organism_id";
		;

		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);

		//run command
//		hmToReturn.putAll(
//				UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_HM(
//						conn
//						, real_s+"_organism_id"
//						, Integer.class
//						, "SUM"
//						, Integer.class
//						, objSQLCommandIT
//						, false
//						, true
//						, false
//						, false
//						, false
//						, methodNameToReport
//						)
//				);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				int sOrganismIdIT = rs.getInt(real_s+"_organism_id");
				int sumIT = rs.getInt("SUM");
				//HashMap<Integer, Integer> getHMorgaId2cummulativeSumAsIntWithQOrganismId
				hmToReturn.put(sOrganismIdIT, sumIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			HashMap<Integer, Integer> mirrorToAddToHmToReturn = getHMorgaId2cummulativeSumAsIntWithQOrganismId(
					conn
					, qOrgaId
					, columnNameTableToSumUp
					, false
					, true
					);
			
			//merge two hasmap with function Sum Int value
			//mirrorToAddToHmToReturn.forEach((k, v) -> hmToReturn.merge(k, v, Integer::sum));
			//merge two hasmap, using method addAll if key already exists, put if key does not exists
			for (Map.Entry<Integer, Integer> entry : mirrorToAddToHmToReturn.entrySet()) {
			    //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
				int keySOrganismIdIT = entry.getKey();
				int sumIT = entry.getValue();
				if (hmToReturn.containsKey(keySOrganismIdIT)) {
					int newSum = hmToReturn.get(keySOrganismIdIT) + sumIT;
					hmToReturn.put(keySOrganismIdIT, newSum);
				} else {
					hmToReturn.put(keySOrganismIdIT, sumIT);
				}
			}

		}
		
		return hmToReturn;

	}

	
	public static ArrayList<Integer> getAlSGeneIdWithQGeneIdAndAlSGeneIdAndMaxdEValueMinQAlignMinFracIdentity(
			Connection conn
			, Integer qGeneId
			, ArrayList<Integer> alSGeneIds
			, Double paramMaxEvalue
			, int paramMinPercentAlignLenght
			, int paramMinPercentIdentity
			, boolean isMirrorRequest
			) throws Exception {
		
		String methodNameToReport = "QueriesTableHomologs getAlSGeneIdWithQGeneIdAndAlSGeneIdAndMaxdEValueMinQAlignMinFracIdentity"
				+ " ; qGeneId="+qGeneId
				+ " ; alSGeneIds=" + ( (alSGeneIds != null ) ? alSGeneIds.toString() : "NULL" )
				+ " ; paramMaxEvalue="+paramMaxEvalue
				+ " ; paramMinPercentAlignLenght="+paramMinPercentAlignLenght
				+ " ; paramMinPercentIdentity="+paramMinPercentIdentity
				+ " ; isMirrorRequest="+isMirrorRequest
				;
		ArrayList<Integer> alToReturn = new ArrayList<>();

		String real_q = "q";
		String real_s = "s";
		if (isMirrorRequest) {
			real_q = "s";
			real_s = "q";
		}

		// check valid input
		//no
		//mandatory params
		if ( qGeneId > 0
				&& alSGeneIds != null && ! alSGeneIds.isEmpty()
				) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("ERROR NOT qGeneId > 0 && alSGeneIds != null && ! alSGeneIds.isEmpty()")
					);
		}
		Double minPerAlignFrac = (double) ((double) paramMinPercentAlignLenght / 100.00);
		
		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put(real_s+"_gene_id", real_s+"_gene_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//			chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("homologies");
		String whereClause = "WHERE "+real_q+"qgene_id ="+qGeneId;
		whereClause += " AND "+real_s+"_gene_id IN ("
				+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alSGeneIds)
				+ ")"
				;
		whereClause += " AND e_value < "
				+ paramMaxEvalue
				;
		whereClause += " AND q_align_frac > "
				+ minPerAlignFrac
				;
		whereClause += " AND identity > "
				+ paramMinPercentIdentity
				;

		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		//run command
//		alToReturn.addAll(UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_alInt(
//				conn, 
//				real_s+"_gene_id",
//				objSQLCommandIT, 
//				false, 
//				true, 
//				methodNameToReport));
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				int sGeneIdIT = rs.getInt(real_s+"_gene_id");
				alToReturn.add(sGeneIdIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
			ArrayList<Integer>  mirrorDataToAdd = getAlSGeneIdWithQGeneIdAndAlSGeneIdAndMaxdEValueMinQAlignMinFracIdentity(
					conn
					, qGeneId
					, alSGeneIds
					, paramMaxEvalue
					, paramMinPercentAlignLenght
					, paramMinPercentIdentity
					, true
					);
			alToReturn.addAll(mirrorDataToAdd);
		}
		
		return alToReturn;
		
	}

	public static ArrayList<LightGeneMatchItem> getLightGeneMatchItemWithQGeneIdAndSGeneId(
			Connection conn
			, int qGeneId
			, int sGeneId
			, boolean isMirrorRequest
			) throws Exception {

		String methodName = "QueriesTableHomologs getLightGeneMatchItemWithQGeneIdAndSGeneId"
				+ " ; qGeneId="+qGeneId
				+ " ; sGeneId="+sGeneId
				+ " ; isMirrorRequest="+isMirrorRequest
				;
		// mandatory args
		if (qGeneId > 0 && sGeneId > 0) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodName
					, new Exception("ERROR NOT qGeneId > 0 && sGeneId > 0")
					);
		}
		
		ArrayList<LightGeneMatchItem> alLgmiToReturn = new ArrayList<LightGeneMatchItem>();

		String realQ = "q";
		String realS = "s";
		if (isMirrorRequest) {
			realQ = "s";
			realS = "q";
		}
		
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		String commandToFillGeneMatchItem = "SELECT identity, score, e_value, "+realQ+"_first_frac, "+realQ+"_align_frac, "+realS+"_first_frac, "+realS+"_align_frac";
		commandToFillGeneMatchItem += " FROM homologies";
		commandToFillGeneMatchItem += " WHERE q_gene_id = " + qGeneId // keep query q first to take advantage of index, it means when isMirrorRequest is true, flip q and s gene ids in parameters
				+ " AND s_gene_id = " + sGeneId;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			statement = conn.createStatement();
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandToFillGeneMatchItem, methodName);

			boolean foundAtLeastOneMatchingTuple = false;
			while (rs.next()) {
				foundAtLeastOneMatchingTuple = true;
				LightGeneMatchItem lgmiToAdd = new LightGeneMatchItem();
				lgmiToAdd.setEValue(rs.getDouble("e_value"));
				lgmiToAdd.setIdentity(rs.getDouble("identity"));
				lgmiToAdd.setQAlignFrac(rs.getDouble(realQ+"_align_frac"));
				lgmiToAdd.setQFirstFrac(rs.getDouble(realQ+"_first_frac"));
				lgmiToAdd.setSAlignFrac(rs.getDouble(realS+"_align_frac"));
				lgmiToAdd.setSFirstFrac(rs.getDouble(realS+"_first_frac"));
				lgmiToAdd.setScore(rs.getDouble("score"));
				alLgmiToReturn.add(lgmiToAdd);
			}
			if ( ! foundAtLeastOneMatchingTuple && UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
				ArrayList<LightGeneMatchItem> alLgmiFromMirrorRequest = getLightGeneMatchItemWithQGeneIdAndSGeneId(
						conn
						, sGeneId
						, qGeneId
						, true
						);
				alLgmiToReturn.addAll(alLgmiFromMirrorRequest);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodName,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodName);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodName);
			}
		}// try

		return alLgmiToReturn;
	}



	// idx 0 : Avg % identity of alignments
	// idx 1 : Stdev % identity of alignments
	// idx 2 : Avg % query and subject lenght coverage of alignments
	// idx 3 : Stdev % query and subject lenght coverage of alignments
	// idx 4 : Median Evalue of alignments
	// idx 5 : Min Evalue of alignments
	// idx 6 : Max Evalue of alignments
	public static ArrayList<Double> getAvgStdevBasicAlignementStats_withHsQGeneId_optionalHsSOrgaIdsAndRank(
			Connection conn
			, HashSet<Integer> hsQGeneId
			, HashSet<Integer> hsSOrgaIds
			, HashSet<Integer> hsRank
			, boolean calculateStdev
			, boolean calculateMinMaxEvalue
			//, boolean isMirrorRequest
			) throws Exception {

		String methodName = "QueriesTableHomologs getAvgStdevBasicAlignementStats_withHsQGeneId_optionalHsSOrgaIdsAndRank"
				+ " ; hsQGeneId="+ ( (hsQGeneId != null ) ? hsQGeneId.toString() : "NULL" )
				+ " ; hsSOrgaIds="+ ( (hsSOrgaIds != null ) ? hsSOrgaIds.toString() : "NULL" )
				+ " ; hsRank="+ ( (hsRank != null ) ? hsRank.toString() : "NULL" )
				;
		// check args
		//no
		// mandatory args
		if (hsQGeneId.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodName,new Exception("hsQGeneId.isEmpty()"));
		}
		
		ArrayList<Double> alToReturn = new ArrayList<>();
		alToReturn.add(0D);// idx 0 : Avg % identity of alignments
		alToReturn.add(0D);// idx 1 : Stdev % identity of alignments
		alToReturn.add(0D);// idx 2 : Avg % query and subject lenght coverage of alignments
		alToReturn.add(0D);// idx 3 : Stdev % query and subject lenght coverage of alignments
		alToReturn.add(0D);// idx 4 : Median Evalue of alignments
		alToReturn.add(0D);// idx 5 : Min Evalue of alignments
		alToReturn.add(0D);// idx 6 : Max Evalue of alignments

		// no need uniq query deal with both ways
//		String realQ = "q";
//		String realS = "s";
//		if (isMirrorRequest) {
//			realQ = "s";
//			realS = "q";
//		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		
		String command = "SELECT ";
		//rank, count(*) AS count_overall, 
		//AVG (score) AS "Average_score", stddev_pop (score) AS "stddev_pop_score", 
		command += " AVG (identity) AS \"Average_identity\"";// idx 0 : Avg % identity of alignments
		if (calculateStdev) {
			command += ", stddev_pop (identity) AS \"stddev_pop_identity\"";// idx 1 : Stdev % identity of alignments
		}
		command += ", AVG (q_align_frac) AS \"Average_q_align_frac\"";// idx 2 : Avg % query and subject lenght coverage of alignments
		if (calculateStdev) {
			command += ", stddev_pop (q_align_frac) AS \"stddev_pop_q_align_frac\"";// idx 3 : Stdev % query and subject lenght coverage of alignments
		}
		command += ", AVG (s_align_frac) AS \"Average_s_align_frac\"";// idx 2 : Avg % query and subject lenght coverage of alignments
		if (calculateStdev) {
			command += ", stddev_pop (s_align_frac) AS \"stddev_pop_s_align_frac\"";// idx 3 : Stdev % query and subject lenght coverage of alignments
		}
		//, AVG (q_align_length) AS "Average_q_align_length"
		//, stddev_pop (q_align_length) AS "stddev_pop_q_align_length"
		//, AVG (s_align_length) AS "Average_s_align_length"
		//, stddev_pop (s_align_length) AS "stddev_pop_s_align_length"
		//, count(*) filter (where e_value = 0) as count_evalue_0
		command += ", percentile_disc(0.5) WITHIN GROUP (ORDER BY e_value) AS \"median_e_value\"";// idx 4 : Median Evalue of alignments
		if (calculateMinMaxEvalue) {
			command += ", min(e_value) AS \"min_e_value\"";// idx 5 : Min Evalue of alignments
			command += ", max(e_value) AS \"max_e_value\"";// idx 6 : Max Evalue of alignments
		}
		
		command += " FROM homologies";
//		command += " WHERE q_organism_id = " + qOrganismId // keep query q first to take advantage of index, it means when isMirrorRequest is true, flip q and s gene ids in parameters
//				+ " AND s_organism_id = " + sOrganismId;
		command += " WHERE ( ( q_gene_id IN ("
				+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsQGeneId)
				+ " )";
		if (hsSOrgaIds != null && ! hsSOrgaIds.isEmpty()) {
			command += " AND s_organism_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsSOrgaIds)
					+ " )";
		}
		command += " )";
		if ( UtilitiesMethodsServer.isDatabaseNoMirror(conn) ) {
			//do reverse s instead of q, pool query align frac and subject anyway
			command += " OR ( s_gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsQGeneId)
					+ " )";
			if (hsSOrgaIds != null && ! hsSOrgaIds.isEmpty()) {
				command += " AND q_organism_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsSOrgaIds)
						+ " )";
			}
			command += " )";
		}
		command += " )";
		if (hsRank != null && ! hsRank.isEmpty()) {
			command += " AND rank IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsRank)
					+ " )";
		}

		//GROUP BY rank
		
		//System.err.println(command);

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			statement = conn.createStatement();
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodName);

			if (rs.next()) {
				alToReturn.set(0, rs.getDouble("Average_identity"));// idx 0 : Avg % identity of alignments
				if (calculateStdev) {
					alToReturn.set(1, rs.getDouble("stddev_pop_identity"));// idx 1 : Stdev % identity of alignments
				}
				Double averageQAlignFracIT = rs.getDouble("Average_q_align_frac");// idx 2 : Avg % query and subject lenght coverage of alignments
				Double stddevPopQAlignFracIT = 0D;
				if (calculateStdev) {
					stddevPopQAlignFracIT = rs.getDouble("stddev_pop_q_align_frac");// idx 3 : Stdev % query and subject lenght coverage of alignments
				}
				Double averageSAlignFracIT = rs.getDouble("Average_s_align_frac");// idx 2 : Avg % query and subject lenght coverage of alignments
				Double stddevPopSAlignFracIT = 0D;
				if (calculateStdev) {
					stddevPopSAlignFracIT = rs.getDouble("stddev_pop_s_align_frac");// idx 3 : Stdev % query and subject lenght coverage of alignments
				}
				Double averageQAndSAlignFracIT = (double) ( ( (averageQAlignFracIT + averageSAlignFracIT) / (double) 2 ) * (double) 100);
				alToReturn.set(2, averageQAndSAlignFracIT);// idx 2 : Avg % query and subject lenght coverage of alignments
				if (calculateStdev) {
					Double stddevPopQAndSAlignFracIT = (double) ( (stddevPopQAlignFracIT + stddevPopSAlignFracIT) / (double) 2 );
					alToReturn.set(3, stddevPopQAndSAlignFracIT);// idx 3 : Stdev % query and subject lenght coverage of alignments
				}
				alToReturn.set(4, rs.getDouble("median_e_value"));// idx 4 : Median Evalue of alignments
				if (calculateMinMaxEvalue) {
					alToReturn.set(5, rs.getDouble("min_e_value"));// idx 5 : Min Evalue of alignments
					alToReturn.set(6, rs.getDouble("max_e_value"));// idx 6 : Max Evalue of alignments
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodName,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodName);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodName);
			}
		}// try

		return alToReturn;
		
	}

	public static ArrayList<GeneMatchItem> getAlGeneMatchItem_QAndS_RankPercIdentityPercLengthCoverageEValue_withQOrganismIdAndSOrganismId(
			Connection conn
			, int qOrganismId
			, int sOrganismId
			, boolean isMirrorRequest
			) throws Exception {

		String methodName = "QueriesTableHomologs getAlGeneMatchItem_QAndS_RankPercIdentityPercLengthCoverageEValue_withQOrganismIdAndSOrganismId"
				+ " ; qOrganismId="+qOrganismId
				+ " ; sOrganismId="+sOrganismId
				+ " ; isMirrorRequest="+isMirrorRequest
				;
		// mandatory args
		if (qOrganismId > 0 && sOrganismId > 0) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodName
					, new Exception("ERROR NOT qOrganismId > 0 && sOrganismId > 0")
					);
		}
		
		ArrayList<GeneMatchItem> alGmiToReturn = new ArrayList<GeneMatchItem>();

		String realQ = "q";
		String realS = "s";
		if (isMirrorRequest) {
			realQ = "s";
			realS = "q";
		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		String command = "SELECT identity, score, e_value, "+realQ+"_align_frac, "+realS+"_align_frac, rank, "+realQ+"_gene_id, "+realS+"_gene_id";
		command += " FROM homologies";
		command += " WHERE q_organism_id = " + qOrganismId // keep query q first to take advantage of index, it means when isMirrorRequest is true, flip q and s gene ids in parameters
				+ " AND s_organism_id = " + sOrganismId;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			statement = conn.createStatement();
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodName);

			boolean foundAtLeastOneMatchingTuple = false;
			while (rs.next()) {
				foundAtLeastOneMatchingTuple = true;
				GeneMatchItem gmiToAdd = new GeneMatchItem();
				gmiToAdd.setIdentity(rs.getDouble("identity"));
				gmiToAdd.setScore(rs.getDouble("score"));
				gmiToAdd.setEValue(rs.getDouble("e_value"));
				gmiToAdd.setQAlignFrac(rs.getDouble(realQ+"_align_frac"));
				gmiToAdd.setSAlignFrac(rs.getDouble(realS+"_align_frac"));
				gmiToAdd.setRank(rs.getInt("rank"));
				gmiToAdd.setQGeneId(rs.getInt(realQ+"_gene_id"));
				gmiToAdd.setSGeneId(rs.getInt(realS+"_gene_id"));
				alGmiToReturn.add(gmiToAdd);
			}
			if ( ! foundAtLeastOneMatchingTuple && UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ! isMirrorRequest) {
				ArrayList<GeneMatchItem> alGmiFromMirrorRequest = getAlGeneMatchItem_QAndS_RankPercIdentityPercLengthCoverageEValue_withQOrganismIdAndSOrganismId(
						conn
						, sOrganismId
						, qOrganismId
						, true
						);
				alGmiToReturn.addAll(alGmiFromMirrorRequest);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodName,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodName);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodName);
			}
		}// try

		return alGmiToReturn;
		
	}

	


	// method that are composites of other methods (do not submit queries directly)
	//no need Test
	

	//private method
	// no need to test
	
}
