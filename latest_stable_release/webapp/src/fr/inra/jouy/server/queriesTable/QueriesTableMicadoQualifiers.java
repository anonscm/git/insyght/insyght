package fr.inra.jouy.server.queriesTable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.objects.ObjAssociationAsMap;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.shared.UtilitiesMethodsShared;


/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

public class QueriesTableMicadoQualifiers {


	
	private static ArrayList<Pattern> alPatternNotAKnownAnnotation = new ArrayList<Pattern>();
	private static Pattern patternNotAKnownAnnotation0 = Pattern.compile("^unknown$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation05 = Pattern.compile("^not?\\s+known?$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation1 = Pattern.compile("^.*unknown\\s+function.*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation2 = Pattern.compile("^.*pseudogene.*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation3 = Pattern.compile("^.*not?\\s+known\\s+function.*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation4 = Pattern.compile("^(conserved\\s*)?hypothetical\\s*protein$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation5 = Pattern.compile("^(full=)?(conservede?)?\\s*(putative)?\\s*(conservede?)?\\s*(domain)?\\s*prote?in$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation6 = Pattern.compile("^prote?in\\s+not?\\s+characteri[zs]ed$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation7 = Pattern.compile("^(conservede?)?\\s*(putative)?\\s*unclassified\\s+(conservede?)?\\s*(domain)?\\s*protein$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation8 = Pattern.compile("^protein(\\s+not\\s+classified)?$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation9 = Pattern.compile("^(conserved\\s+)?protein(\\s+family)?(\\s+conserved)?$", Pattern.CASE_INSENSITIVE);
	//private static Pattern patternNotAKnownAnnotation10 = Pattern.compile("^uncharacteri[zs]ed\\s+protein\\s+conserved.+$", Pattern.CASE_INSENSITIVE);
	//private static Pattern patternNotAKnownAnnotation11 = Pattern.compile("^uncharacteri[zs]ed\\s+domain\\s*,?\\s*(\\w+)?$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation12 = Pattern.compile("^[\\d\\.\\,\\-]*phenotype[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation13 = Pattern.compile("^[\\d\\.\\,\\-]*putative[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation14 = Pattern.compile("^[\\d\\.\\,\\-]*No\\s+COGs+number[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation15 = Pattern.compile("^[\\d\\.\\,\\-]*enzyme[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation16 = Pattern.compile("^[\\d\\.\\,\\-]*putative\\s+enzyme[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation17 = Pattern.compile("^[\\d\\.\\,\\-]*miscellaneous[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation18 = Pattern.compile("^[\\d\\.\\,\\-]*general[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation19 = Pattern.compile("^[\\d\\.\\,\\-]*other[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation20 = Pattern.compile("^[\\d\\.\\,\\-]*conserved[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation21 = Pattern.compile("^[\\d\\.\\,\\-]*conserved\\s+domain\\s+protein[\\d\\.\\,\\-]*$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation22 = Pattern.compile("^6\\s+:\\s+no\\s+similarity$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation23 = Pattern.compile("^5\\.\\d\\s+:\\s+from\\s+.+$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation24 = Pattern.compile("^(predicted\\s*)?(conserved\\s*)?(putative\\s*)?(inner-?\\s*)?(outer-?\\s*)?(trans-?\\s*)?membrane\\s+(hypothetical\\s*)?([\\(lipo\\)])?protein$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation25 = Pattern.compile("^(putative\\s*)?protein(\\s*putative)?$", Pattern.CASE_INSENSITIVE);
	//private static Pattern patternNotAKnownAnnotation26 = Pattern.compile("^([\\w-]+\\s+)?domain\\s+protein$", Pattern.CASE_INSENSITIVE);
	private static Pattern patternNotAKnownAnnotation26 = Pattern.compile("^(putative\\s*)?(hypothetical\\s*)?(conserved\\s*)?(domain\\s+)?protein(\\s+\\w+_\\d+)?$", Pattern.CASE_INSENSITIVE); //uPF0102 protein CLONEX_03036
	private static Pattern patternNotAKnownAnnotation27 = Pattern.compile("^.*uncharacteri[zs]ed.*$", Pattern.CASE_INSENSITIVE);
	
	
	private static void intiFillUpAlPatternNotAKnownAnnotation() {
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation0);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation05);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation1);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation2);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation3);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation4);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation5);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation6);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation7);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation8);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation9);
		//alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation10);
		//alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation11);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation12);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation13);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation14);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation15);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation16);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation17);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation18);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation19);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation20);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation21);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation22);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation23);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation24);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation25);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation26);
		alPatternNotAKnownAnnotation.add(patternNotAKnownAnnotation27);
	}


		
		
	//method that submit queries

	//tested
	public static ArrayList<String> getAlGeneQualifiers_withAccnumAndAlTypeQual_optionalFeatId(
			Connection conn
			, String accession
			, int featureId
			, ArrayList<String> alTypeQualIT
			, boolean shouldBePresentInTable
			, boolean checkValidFreeStringInput
			) throws Exception {

		String methodNameToReport = "QueriesTableMicadoQualifiers getGeneQualifierWithAccnumAndFeatIdAndTypeQual"
				+ " ; accession="+accession
				+ " ; featureId="+featureId
				+ " ; alTypeQualIT="+ ( (alTypeQualIT != null ) ? alTypeQualIT.toString() : "NULL" )
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;

		ArrayList<String> alGeneQualifiersToReturn = new ArrayList<>();
		
		
		// check valid input
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					accession, "accession", false, false);
			for(String typeQualIT : alTypeQualIT){
				UtilitiesMethodsServer.checkValidFreeStringInput(
						methodNameToReport,
						typeQualIT, "typeQualIT", false, false);
			}
		}
		//mandatory args
		if (accession == null || accession.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("accession == null || accession.isEmpty() : "+accession));
		}
		if (alTypeQualIT == null || alTypeQualIT.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("alTypeQualIT == null || alTypeQualIT.isEmpty() : "+ ( (alTypeQualIT != null ) ? alTypeQualIT.toString() : "NULL" )));
		}

		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
		//chm_alColumnSelectAsInt_aliase2columnName.put("??", "??");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsString_aliase2columnName.put("qualifier", "qualifier");

		String commandClauseTypeQual = "";
		boolean firstIter = true;
		for(String typeQualIT : alTypeQualIT){
			if(firstIter){
				commandClauseTypeQual += "type_qual = '"+typeQualIT+"'";
				firstIter = false;
			} else {
				commandClauseTypeQual += " OR type_qual = '"+typeQualIT+"'";
			}
		}
		if(commandClauseTypeQual.isEmpty()){
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("Invalid parameter, alTypeQualIT is empty")
					);
		}
		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("micado.qualifiers");
		String whereClause = "WHERE accession = '" + accession+ "'";
		if (featureId >= 0) {
			whereClause += " AND code_feat = "+featureId;
		}
		whereClause += " AND ( "+commandClauseTypeQual+" )";
				
		//		+ "LIMIT 999" //hint to planer, probably not that many annotations per gene
	
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);

		//run command
//		return UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_alString(
//				conn, 
//				"qualifier",
//				objSQLCommandIT, 
//				false, 
//				true, 
//				methodNameToReport);
		

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				String qualifierIT = rs.getString("qualifier");
				alGeneQualifiersToReturn.add(qualifierIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (shouldBePresentInTable) {
			if (alGeneQualifiersToReturn.isEmpty()) {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("shouldBePresentInTable "+shouldBePresentInTable+ " and alGeneQualifiersToReturn.isEmpty()")
						);
			}
		}
		
		return alGeneQualifiersToReturn;
				
		
	}


	public static HashMap<String, HashSet<Integer>> getHmAccession2HsCodeFeatWithAtLeastOneMeaningfulFunctionalAnnotation(
			Connection conn
			, HashSet<String> hsAccessions
			) throws Exception {

		String methodNameToReport = "QueriesTableMicadoQualifiers getHmAccession2HsCodeFeatWithAtLeastOneMeaningfulFunctionalAnnotation"
				+ " ; hsAccessions="+ ( (hsAccessions != null ) ? hsAccessions.toString() : "NULL" )
				;
		//args to check
		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport
				, hsAccessions.toString()
				, "accession"
				, false
				, false);

		//mandatory args
		if (hsAccessions != null && ! hsAccessions.isEmpty()) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT setAccession != null && ! setAccession.isEmpty()")
					);
		}
		
		HashSet<String> hsMeaningfulFunctionalAnnotationTypeQualToMatch = new HashSet<>();
		hsMeaningfulFunctionalAnnotationTypeQualToMatch.add("product");
		hsMeaningfulFunctionalAnnotationTypeQualToMatch.add("cellular_component");
		hsMeaningfulFunctionalAnnotationTypeQualToMatch.add("biological_process");
		hsMeaningfulFunctionalAnnotationTypeQualToMatch.add("function");
		hsMeaningfulFunctionalAnnotationTypeQualToMatch.add("EC_number");
//		hsMeaningfulFunctionalAnnotationTypeQualToMatch.add("standard_name");
//		hsMeaningfulFunctionalAnnotationTypeQualToMatch.add("gene_synonym");
//		hsMeaningfulFunctionalAnnotationTypeQualToMatch.add("gene");
//		hsMeaningfulFunctionalAnnotationTypeQualToMatch.add("gene_name");
		
		HashMap<String, HashSet<Integer>> hmToReturn = new HashMap<>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
		
			String command = "SELECT accession, code_feat, qualifier"
					+ " FROM micado.qualifiers"
					+ " WHERE accession IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsAccessions, ",", "'")
						+ ")";
			command += " AND type_qual IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsMeaningfulFunctionalAnnotationTypeQualToMatch, ",", "'")
						+ ")";
//			command += " AND qualifier NOT IN ("
//					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsNotMeaningfulFunctionalAnnotation, ",", "'")
//					+ ")";
	
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			while (rs.next()) {
				String accessionIT = rs.getString("accession");
				int codeFeatIT = rs.getInt("code_feat");
				String qualifierIT = rs.getString("qualifier");
				
				boolean qualifierIsReallyAKnownAnnotation = checkIfQualifierIsReallyAKnownAnnotation(qualifierIT);
				if ( qualifierIsReallyAKnownAnnotation ) {
					if (hmToReturn.containsKey(accessionIT)) {
						HashSet<Integer> hsFeatureIdsIT = hmToReturn.get(accessionIT);
						hsFeatureIdsIT.add(codeFeatIT);
					} else {
						HashSet<Integer> alFeatureIdsIT = new HashSet<>();
						alFeatureIdsIT.add(codeFeatIT);
						hmToReturn.put(accessionIT, alFeatureIdsIT);
					}
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return hmToReturn;
		
	}

	
	
	//TODO TEST
	public static boolean checkIfQualifierIsReallyAKnownAnnotation(String qualifierIT) {
		if (qualifierIT.length() <= 1) {
			return false;
		}
		
		if (alPatternNotAKnownAnnotation.isEmpty()) {
			intiFillUpAlPatternNotAKnownAnnotation();
		}
		
		for (Pattern patternIt : alPatternNotAKnownAnnotation) {
			if ( patternIt.matcher(qualifierIT).matches() ) {
				return false;
			}
		}
		return true;
		
	}



	//	Return {elementId}{matchRegexString}{alFeatureIds}
	public static HashMap<Integer, HashMap<String, HashSet<Integer>>> getMultiHm_Accession2matchRegexString2alFeatureIds_WithSetAccessionAndAlTypeQualAndAlRegexStringToMatchQualifier(
			Connection conn
			, Set<String> setAccession
			, ArrayList<String> alTypeQualToMatch
			, ArrayList<String> alStringRegexToMatchQualifier
			, boolean qualifier_IS_NOT_NULL
			, HashMap<String,Integer> hmAccession2ElementId
			) throws Exception {

		String methodNameToReport = "QueriesTableMicadoQualifiers getMultiHm_Accession2matchRegexString2alFeatureIds_WithSetAccessionAndAlTypeQualAndAlRegexStringToMatchQualifier"
				+ " ; setAccession="+ ( (setAccession != null ) ? setAccession.toString() : "NULL" )
				+ " ; alTypeQualToMatch="+ ( (alTypeQualToMatch != null ) ? alTypeQualToMatch.toString() : "NULL" )
				+ " ; alStringRegexToMatchQualifier="+ ( (alStringRegexToMatchQualifier != null ) ? alStringRegexToMatchQualifier.toString() : "NULL" )
				+ " ; qualifier_IS_NOT_NULL="+qualifier_IS_NOT_NULL
				+ " ; hmAccession2ElementId="+ ( (hmAccession2ElementId != null ) ? hmAccession2ElementId.toString() : "NULL" )
				;

		//args to check
		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport
				, setAccession.toString()
				, "accession"
				, false
				, false);
		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport
				, alTypeQualToMatch.toString()
				, "alTypeQualToMatch.toString"
				, false
				, false);
		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport
				, alStringRegexToMatchQualifier.toString()
				, "alStringRegexToMatchQualifier"
				, false
				, false);
		

		//mandatory args
		if (setAccession != null && ! setAccession.isEmpty()) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT setAccession != null && ! setAccession.isEmpty()")
					);
		}
		if (alTypeQualToMatch != null && ! alTypeQualToMatch.isEmpty()) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT alTypeQualToMatch != null && ! alTypeQualToMatch.isEmpty()")
					);
		}
		if (alStringRegexToMatchQualifier != null && ! alStringRegexToMatchQualifier.isEmpty()) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT alStringRegexToMatchQualifier != null && ! alStringRegexToMatchQualifier.isEmpty()")
					);
		}

		HashMap<Integer, HashMap<String, HashSet<Integer>>> multiHmToReturn = new HashMap<>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			for ( String stRegexToMatchQualifierIT : alStringRegexToMatchQualifier) {

				String command = "SELECT accession, code_feat"
						+ " FROM micado.qualifiers"
						+ " WHERE accession IN ("
							+ UtilitiesMethodsShared.getItemsAsStringFromCollection(setAccession, ",", "'")
							+ ")";
				
				command += " AND (";
				for (int j = 0; j < alTypeQualToMatch.size(); j++) {
					String typeQIT = alTypeQualToMatch.get(j);
					if (j > 0) {
						command += " OR ";
					}
					command += "micado.qualifiers.type_qual = '"
							+ typeQIT + "'";
				}
				if (!qualifier_IS_NOT_NULL) {
					command += ") AND (micado.qualifiers.qualifier " + stRegexToMatchQualifierIT ;
				}
				command += ")";


//				if (command.matches("^.*AP011114.*$")
//						//|| command.matches("^.*rumA.*$")
//						//|| command.matches("^.*tatD.*$")
//						//|| command.matches("^.*rpsI.*$")
//						//|| command.matches("^.*GGS_1263.*$")
//						) {
//					System.err.println(command);
//					//System.exit(0);
//				}
				
				
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
				
				while (rs.next()) {
//					Return {accnum}{matchRegexString}{alFeatureIds}
					String accessionIT = rs.getString("accession");
					int element_id = -1;
					if (hmAccession2ElementId.containsKey(accessionIT)) {
						element_id = hmAccession2ElementId.get(accessionIT);
					} else {
						UtilitiesMethodsServer.reportException(methodNameToReport, 
								new Exception("accessionIT "+accessionIT+" not found in hmAccession2ElementId : "+hmAccession2ElementId.toString())
								);
						return null;
					}
					
					int codeFeatIT = rs.getInt("code_feat");
					
					if (multiHmToReturn.containsKey(element_id)) {
						// previous entry multiHmToReturn
						HashMap<String, HashSet<Integer>> matchRegexString2alFeatureIdsIT = multiHmToReturn.get(element_id);
						if (matchRegexString2alFeatureIdsIT.containsKey(stRegexToMatchQualifierIT)) {
							// previous entry stRegexToMatchQualifierIT
							HashSet<Integer> alFeatureIdsIT = matchRegexString2alFeatureIdsIT.get(stRegexToMatchQualifierIT);
							alFeatureIdsIT.add(codeFeatIT);
						} else {
							//new entry stRegexToMatchQualifierIT
							HashSet<Integer> alFeatureIdsIT = new HashSet<>();
							alFeatureIdsIT.add(codeFeatIT);
							matchRegexString2alFeatureIdsIT.put(stRegexToMatchQualifierIT, alFeatureIdsIT);
						}
					} else {
						//new accnum entry multiHmToReturn
						HashMap<String, HashSet<Integer>> matchRegexString2alFeatureIdsIT = new HashMap<>();
						HashSet<Integer> alFeatureIdsIT = new HashSet<>();
						alFeatureIdsIT.add(codeFeatIT);
						matchRegexString2alFeatureIdsIT.put(stRegexToMatchQualifierIT, alFeatureIdsIT);
						multiHmToReturn.put(element_id, matchRegexString2alFeatureIdsIT);
					}
				}
			}
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return multiHmToReturn;
	}
	
//	public static ArrayList<Integer> getAlCodeFeatWithAccessionAndAlTypeQualAndQualifier(
//			Connection conn
//			, String accnum
//			, ArrayList<String> alTypeQual
//			, String matchRegexQualifier
//			, boolean qualifier_IS_NOT_NULL
//			) throws Exception {
//
//
//		String methodNameToReport = "QueriesTableMicadoQualifiers getAlCodeFeatWithAccessionAndAlTypeQualAndQualifier"
//				+ " ; accnum="+accnum
//				+ " ; alTypeQual="+ ( (alTypeQual != null ) ? alTypeQual.toString() : "NULL" )
//				+ " ; qualifier="+matchRegexQualifier
//				+ " ; qualifier_IS_NOT_NULL="+qualifier_IS_NOT_NULL
//				;
//		
//		//args to check
//		UtilitiesMethodsServer.checkValidFreeStringInput(
//				methodNameToReport
//				, accnum
//				, "accession"
//				, false
//				, false);
//		UtilitiesMethodsServer.checkValidFreeStringInput(
//				methodNameToReport
//				, alTypeQual.toString()
//				, "alTypeQual.toString"
//				, false
//				, false);
//		UtilitiesMethodsServer.checkValidFreeStringInput(
//				methodNameToReport
//				, matchRegexQualifier
//				, "qualifier"
//				, false
//				, false);
//		
//		//mandatory args
//		if (alTypeQual != null && ! alTypeQual.isEmpty()) {
//			//ok
//		} else {
//			UtilitiesMethodsServer.reportException(
//					methodNameToReport
//					, new Exception("NOT alTypeQual != null && ! alTypeQual.isEmpty()")
//					);
//		}
//		
//		ArrayList<Integer> alToReturn = new ArrayList<>();
//		
//		Statement statement = null;
//		ResultSet rs = null;
//		boolean closeConn = false;
//
//		try {
//
//			if (conn == null) {
//				conn = DatabaseConf.getConnection_db();
//				closeConn = true;
//			}
//			statement = conn.createStatement();
//			
//			// get the feature_id
//			String command = "SELECT code_feat FROM micado.qualifiers WHERE accession = '"
//					+ accnum + "'";
//			command += " AND (";
//			for (int j = 0; j < alTypeQual.size(); j++) {
//				String typeQIT = alTypeQual.get(j);
//				if (j > 0) {
//					command += " OR ";
//				}
//				command += "micado.qualifiers.type_qual = '"
//						+ typeQIT + "'";
//			}
//			if (!qualifier_IS_NOT_NULL) {
//				command += ") AND (micado.qualifiers.qualifier " + matchRegexQualifier ;
//			}
//			command += ")";
//
//			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
//			
//			while (rs.next()) {
//				alToReturn.add(rs.getInt("code_feat"));
//			}
//			
//		} catch (Exception ex) {
//			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
//		} finally {
//			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
//			if (closeConn) {
//				DatabaseConf.freeConnection(conn, methodNameToReport);
//			}
//		}// try
//
//		return alToReturn;
//		
//	}
	
	
	

	//tested
	// return ObjAssociationAsMap.getRsColumn2String().get("accession")
	// return ObjAssociationAsMap.getRsColumn2Int().get("code_feat")
	public static ArrayList<ObjAssociationAsMap> getAlAssocAccnumCodeFeatWithAlListAccnumAndTypeQual2QueryString_MatchEqual(
			Connection conn
			, HashSet<String> listAccnum_OR
			, ArrayList<ObjAssociationAsMap> typeQual2QueryString_OR
			, boolean shouldBePresentInTable
			, boolean checkValidFreeStringInput
			) throws Exception {

		String methodNameToReport = "QueriesTableMicadoQualifiers getAlAssocAccnumCodeFeatWithAlListAccnumAndTypeQual2QueryString_MatchEqual"
				+ " ; listAccnum_OR="+ ( (listAccnum_OR != null ) ? listAccnum_OR.toString() : "NULL" )
				+ " ; typeQual2QueryString_OR="+ ( (typeQual2QueryString_OR != null ) ? typeQual2QueryString_OR.toString() : "NULL" )
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;

		ArrayList<ObjAssociationAsMap> oaamToReturn = new ArrayList<>();
		
		// check valid input
		if(checkValidFreeStringInput){
			if(listAccnum_OR.isEmpty()){
				UtilitiesMethodsServer.reportException(
						methodNameToReport,
						new Exception("Invalid parameter, listAccnum is empty"));
			}
			for(String accnumIT : listAccnum_OR){
				UtilitiesMethodsServer.checkValidFreeStringInput(
						methodNameToReport,
						accnumIT, "accnumIT", false, false);
			}
			if(typeQual2QueryString_OR.isEmpty()){
				UtilitiesMethodsServer.reportException(
						methodNameToReport,
						new Exception("Invalid parameter, typeQual2QueryString is empty"));
			}
			for(ObjAssociationAsMap oaamIT : typeQual2QueryString_OR){
				for (Map.Entry<String, String> entry : oaamIT.getRsColumn2String().entrySet()) {
				    UtilitiesMethodsServer.checkValidFreeStringInput(
							methodNameToReport,
							entry.getValue(), "TypeQual2QueryString "+entry.getKey(), false, false);
				}
			}

		}
		
		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("code_feat", "micado.qualifiers.code_feat");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsString_aliase2columnName.put("accession", "micado.qualifiers.accession");

		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("micado.qualifiers");
		
		String whereClause = "WHERE "
				+ " micado.qualifiers.accession IN ("
				+ UtilitiesMethodsShared.getItemsAsStringFromCollection(listAccnum_OR, ",", "'")
				//+ listAccnum_OR.toString().replaceAll("[\\[\\]]", "")
				+ ") AND (";
		boolean firstIter = true;
		for(ObjAssociationAsMap oaamIT : typeQual2QueryString_OR){
			if(!oaamIT.getRsColumn2Int().isEmpty()){
				UtilitiesMethodsServer.reportException(
						methodNameToReport,
						new Exception("Invalid parameter, ObjAssociationAsMap getRsColumn2Int must be empty, only getRsColumn2String is allowed"));
			}
			for (Map.Entry<String, String> entry : oaamIT.getRsColumn2String().entrySet()) {
				if(firstIter){
					whereClause += " (micado.qualifiers.type_qual = '"+entry.getKey()+"' AND micado.qualifiers.qualifier = '"+entry.getValue()+"')";
					firstIter = false;
				} else {
					whereClause += " OR (micado.qualifiers.type_qual = '"+entry.getKey()+"' AND micado.qualifiers.qualifier = '"+entry.getValue()+"')";
				}
			}
		}

		whereClause += ")";

		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		
		//run command
//		return UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS(
//				conn, 
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				int codeFeatIT = rs.getInt("code_feat");
				String accessionIT = rs.getString("accession");
				ObjAssociationAsMap oaamIT = new ObjAssociationAsMap();
				HashMap<String, Integer> hmStIntIT = new HashMap<>();
				hmStIntIT.put("code_feat", codeFeatIT);
				oaamIT.setRsColumn2Int(hmStIntIT);
				HashMap<String, String> hmStStIT = new HashMap<>();
				hmStStIT.put("accession", accessionIT);
				oaamIT.setRsColumn2String(hmStStIT);
				oaamToReturn.add(oaamIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (shouldBePresentInTable && oaamToReturn.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("shouldBePresentInTable && oaamToReturn.isEmpty()"));
		}
		
		return oaamToReturn;
		
	}


	// method that are composites of other methods (do not submit queries directly)


	//no need Test
	public static String getLocusTagWithAccnumAndFeatId_firstOnly(
			Connection conn
			, String accession
			, int featureId
			, boolean shouldBePresentInTable
			, boolean checkValidFreeStringInput
			) throws Exception {
		
		String methodNameToReport = "QueriesTableMicadoQualifiers getLocusTagWithAccnumAndFeatId_firstOnly"
				+ " ; accession="+accession
				+ " ; featureId="+featureId
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;
		
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					accession, "accession", false, false);
		}
		ArrayList<String> alTypeQualIT = new ArrayList<String>();
		alTypeQualIT.add("locus_tag");
		ArrayList<String> alTypeQualGeneName = getAlGeneQualifiers_withAccnumAndAlTypeQual_optionalFeatId(
						conn,
						accession,
						featureId,
						alTypeQualIT,
						false,
						false
						);
		if( ! alTypeQualGeneName.isEmpty()){
			//first only
			return alTypeQualGeneName.get(0);
		} else {
			return "";
		}
	}
	
	//no need Test
	public static String getGeneNameWithAccnumAndFeatId_firstOnly(
			Connection conn
			, String accession
			, int featureId
			, boolean shouldBePresentInTable
			, boolean checkValidFreeStringInput
			) throws Exception {

		String methodNameToReport = "QueriesTableMicadoQualifiers getGeneNameWithAccnumAndFeatId_firstOnly"
				+ " ; accession="+accession
				+ " ; featureId="+featureId
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;
		
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					accession, "accession", false, false);
		}
		ArrayList<String> alTypeQualIT = new ArrayList<String>();
		alTypeQualIT.add("gene_name");
		alTypeQualIT.add("gene");
		ArrayList<String> alTypeQualGeneName = getAlGeneQualifiers_withAccnumAndAlTypeQual_optionalFeatId(
						conn,
						accession,
						featureId,
						alTypeQualIT,
						false,
						false
						);
		if( ! alTypeQualGeneName.isEmpty()){
			//first only
			return alTypeQualGeneName.get(0);
		} else {
			return "";
		}
	}

	// ArrayList<String>.get(0) -> Name
	// ArrayList<String>.get(1) -> LocusTag
	// If absent array contain 2 empty strings
	public static ArrayList<String> getGeneNameAndLocusTagWithAccessionAndFeatureId_firstOnly(
			Connection conn
			, String accession
			, Integer feature_id
			) throws Exception {
		
		String methodNameToReport = "QueriesTableMicadoQualifiers getGeneNameAndLocusTagWithAccessionAndFeatureId_firstOnly"
				+ " ; accession="+accession
				+ " ; feature_id="+feature_id
				;

		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport
				, accession
				, "accession"
				, false
				, false);

		ArrayList<String> alToReturn = new ArrayList<String>();
		String geneNameToReturn = "";
		String locusTagToReturn = "";
		alToReturn.add(geneNameToReturn);
		alToReturn.add(locusTagToReturn);
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			String command = "SELECT micado.qualifiers.type_qual, micado.qualifiers.qualifier"
					+ " FROM micado.qualifiers"
					+ " WHERE micado.qualifiers.accession = '"
					+ accession
					+ "' AND micado.qualifiers.code_feat = "
					+ feature_id
					+ " AND (" + UtilitiesMethodsServer.getGeneNameAndLocusTagQualifiers() + ")";

			statement = conn.createStatement();
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			while (rs.next()) {
				QueriesTableGenes.fillAlGeneName_LocusTag(rs, alToReturn, -1, accession, feature_id, true);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try


		return alToReturn;
		
		
	}


}
