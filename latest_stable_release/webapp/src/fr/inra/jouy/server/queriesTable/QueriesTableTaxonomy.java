package fr.inra.jouy.server.queriesTable;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.sql.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.pojos.databaseMapping.TaxoItem;

public class QueriesTableTaxonomy {
	
	//static Pattern OrganismNameWithSpecies = Pattern.compile("^(.+?\\s+.+?)(\\s+.+)?$");
	

	public static HashSet<Integer> getHsTaxonIds(
			Connection conn
			, boolean onlyIsLeaf
			, boolean onlyPublic
			//, boolean onlyWithElement
			) throws Exception {
		
		String methodNameToReport = "QueriesTableTaxonomy getHsTaxonIds"
				+ " ; onlyIsLeaf="+ onlyIsLeaf
				+ " ; onlyPublic="+ onlyPublic
				//+ " ; onlyWithElement="+ onlyWithElement
				;
		HashSet<Integer> hsToReturn = new HashSet<>();

		//args to check
		// none
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_taxo();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT micado.ncbi_taxonomy_tree.ncbi_taxid"
					+ " FROM micado.ncbi_taxonomy_tree LEFT JOIN organisms ON micado.ncbi_taxonomy_tree.ncbi_taxid = organisms.taxon_id"
					;
			String whereClause = "";
			if (onlyIsLeaf) {
				if (whereClause.isEmpty()) {
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " is_leaf IS TRUE";
			}
			if (onlyPublic) {
				if (whereClause.isEmpty()) {
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organisms.ispublic IS NOT FALSE";
			}
			command += whereClause;
			
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			while (rs.next()) {
				int ncbiTaxidIT = rs.getInt("ncbi_taxid");
				hsToReturn.add(ncbiTaxidIT);
			}


		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return hsToReturn;
		
	}
	
	
	
	public static TaxoItem getTaxoTree_withHsTaxonIds(
			Connection conn
			, HashSet<Integer> hsTaxoIds
			//, boolean onlyPublic
			//, boolean onlyWithElement
			) throws Exception {
		
		String methodNameToReport = "QueriesTableTaxonomy getTaxoTreePublicOganisms"
				+ " ; listTaxoIds="+ ( (hsTaxoIds != null ) ? hsTaxoIds.toString() : "NULL" )
				//+ " ; onlyPublic="+ onlyPublic
				//+ " ; onlyWithElement="+ onlyWithElement
				;
		
		TaxoItem taxoTree = new TaxoItem("RootOfRoot", 0, null);

		//args to check
		if (hsTaxoIds == null || hsTaxoIds.isEmpty()) {
			return taxoTree;
		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_taxo();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			//taxo ids of selectable
			String listTaxoIdsOfFullPath = UtilitiesMethodsShared.getItemsAsStringFromCollection(hsTaxoIds);
			
			
			if (getDatabaseIsNcbiTaxonomyV3() == 1) {
				
				
				/*
				test_origami=> \d ncbi_taxonomy_tree
				          Table "micado.ncbi_taxonomy_tree"
				     Column      |          Type          | Modifiers 
				-----------------+------------------------+-----------
				 ncbi_taxid      | integer                | not null
				 parent_taxid    | integer                | not null
				 lineage         | integer[]              | 
				 rank            | character varying(40)  | 
				 depth           | integer                | not null
				 is_leaf         | boolean                | not null
				 scientific_name | character varying(500) | not null
				Indexes:
				    "ncbi_taxonomy_tree_pkey" PRIMARY KEY, btree (ncbi_taxid)
				*/
				
				
				String preCommand = "SELECT ncbi_taxid, lineage"
						+ " FROM micado.ncbi_taxonomy_tree"
						+ " WHERE ncbi_taxid IN ("
						+ listTaxoIdsOfFullPath
						+ ")"
						;
				
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, preCommand, "QueriesTableTaxonomy getTaxoTree");
				HashMap<Integer,TaxoItem> ncbiTaxidIT2TaxoItem = new HashMap<>();
				while (rs.next()) {
					int ncbiTaxidIT = rs.getInt("ncbi_taxid");
					Array lineageRawFromDB = rs.getArray("lineage");
					if ( ! ncbiTaxidIT2TaxoItem.containsKey(ncbiTaxidIT)) {
						ncbiTaxidIT2TaxoItem.put(ncbiTaxidIT, new TaxoItem());
					}
					Integer[] alLineageIT = (Integer[])lineageRawFromDB.getArray();
					for(int i=0;i<alLineageIT.length;i++){
						int lineageIntIT = alLineageIT[i];
						if ( ! ncbiTaxidIT2TaxoItem.containsKey(lineageIntIT)) {
							ncbiTaxidIT2TaxoItem.put(lineageIntIT, new TaxoItem());
						}
					}
				}
				
				
				String listTaxoIdsLeavesAndNodes = UtilitiesMethodsShared.getItemsAsStringFromCollection(ncbiTaxidIT2TaxoItem.keySet());
				String command = "SELECT ncbi_taxid, scientific_name, parent_taxid" // ncbi_taxid, lineage, scientific_name, depth, parent_taxid"
						+ " FROM micado.ncbi_taxonomy_tree WHERE ncbi_taxid IN ("
						+ listTaxoIdsLeavesAndNodes
						+ ")"
						;
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, "QueriesTableTaxonomy getTaxoTree");
				
				
				HashMap<Integer,String> ncbiTaxidIT2scientificName = new HashMap<>();
				HashMap<Integer,Integer> ncbiTaxidIT2parentTaxid = new HashMap<>();

				while (rs.next()) {
					int ncbiTaxidIT = rs.getInt("ncbi_taxid");
					//Array lineageRawFromDB = rs.getArray("lineage");
					//Integer[] alLineageIT = (Integer[])lineageRawFromDB.getArray();
					String scientificName = rs.getString("scientific_name");
					//int depthIT = rs.getInt("depth");
					int parentTaxid = rs.getInt("parent_taxid");
					
					ncbiTaxidIT2scientificName.put(ncbiTaxidIT, scientificName);
					ncbiTaxidIT2parentTaxid.put(ncbiTaxidIT, parentTaxid);

				}
				
				for(Entry<Integer,String> entry : ncbiTaxidIT2scientificName.entrySet()) { 
					int ncbiTaxidIT = entry.getKey();
					String scientificName = entry.getValue();
					int parentTaxid = ncbiTaxidIT2parentTaxid.get(ncbiTaxidIT);
					TaxoItem taxoItemIT = ncbiTaxidIT2TaxoItem.get(ncbiTaxidIT);
					taxoItemIT.setNcbiTaxoId(ncbiTaxidIT);
					taxoItemIT.setName(scientificName);
					if (parentTaxid != 0) {
						TaxoItem parentTaxoItemIT = ncbiTaxidIT2TaxoItem.get(parentTaxid);
						if (parentTaxoItemIT == null) {
							UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("Null parent for ncbiTaxidIT "+ncbiTaxidIT+" ; "+scientificName));
						}
						taxoItemIT.setParentTaxoItem(parentTaxoItemIT);
						parentTaxoItemIT.getTaxoChildrens().add(taxoItemIT);
					} else {
						taxoItemIT.setParentTaxoItem(taxoTree);
						taxoTree.getTaxoChildrens().add(taxoItemIT);
					}
				}
				
				
				
			} else {
				
				String commandPrePre = "SELECT ncbi_taxid, lineage"
						+ " FROM micado.taxo"
						+ " WHERE ncbi_taxid IN ("
						+ listTaxoIdsOfFullPath
						+ ")"
						;
				
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandPrePre, "QueriesTableTaxonomy getTaxoTree");
				
				HashMap<Integer,String> taxId2lineage = new HashMap<Integer,String>();
				while (rs.next()) {
					String lineageIT = rs.getString("lineage").replaceAll("[\\{\\}]", "");
					listTaxoIdsOfFullPath += ","+lineageIT;
					taxId2lineage.put(rs.getInt("ncbi_taxid"), lineageIT);
					
				}
				
				//build the taxId2ScientificName
				String commandPre = "SELECT ncbi_taxid, name"
						+ " FROM micado.taxo_names"
						+ " WHERE ncbi_taxid IN (" + listTaxoIdsOfFullPath + ")" +
						" AND "
						+ "type = 'scientific name'"
						;
				rs.close();
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandPre, "QueriesTableTaxonomy getTaxoTree");
				
				
				HashMap<Integer,String> taxId2ScientificName = new HashMap<Integer,String>();
				while (rs.next()) {
					taxId2ScientificName.put(rs.getInt("ncbi_taxid"), rs.getString("name"));
				}
				

				//while (rs.next()) {
				for(Entry<Integer,String> entry : taxId2lineage.entrySet()) {
				   
					int ncbiTaxidIT = entry.getKey();//rs.getInt("ncbi_taxid");
					String lineageIT = entry.getValue();//rs.getString("lineage").replaceAll("[\\{\\}]", "");
					String[] alLineageIT = lineageIT.split(",");
					
					TaxoItem taxoIT = taxoTree;
					for(int i=0;i<alLineageIT.length;i++){
						int lineageIntIT = Integer.parseInt(alLineageIT[i]);
						//System.out.println(Integer.parseInt(alLineageIT[i]));
						
						boolean alreadyEntered = false;
						for(int j=0;j<taxoIT.getTaxoChildrens().size();j++){
							if(taxoIT.getTaxoChildrens().get(j).getNcbiTaxoId() == lineageIntIT){
								//already entered
								taxoIT = taxoIT.getTaxoChildrens().get(j);
								alreadyEntered = true;
								break;
							}
						}
						
						if(!alreadyEntered){
							//not yet entered
							TaxoItem newTaxo = new TaxoItem(
									taxId2ScientificName.get(lineageIntIT)
									, lineageIntIT, taxoIT);
							taxoIT.getTaxoChildrens().add(newTaxo);
							taxoIT = newTaxo;
						}
					}
					
					//deal with the orga sent
					//System.out.println(rs.getInt("ncbi_taxid"));
					boolean leafAlreadyEntered = false;
					for(int j=0;j<taxoIT.getTaxoChildrens().size();j++){
						if(taxoIT.getTaxoChildrens().get(j).getNcbiTaxoId() == ncbiTaxidIT){
							//already entered
							leafAlreadyEntered = true;
							break;
						}
					}
					if(!leafAlreadyEntered){
						//not yet entered
						TaxoItem newTaxo = new TaxoItem(
								taxId2ScientificName.get(ncbiTaxidIT)
								, ncbiTaxidIT, taxoIT);
						taxoIT.getTaxoChildrens().add(newTaxo);
						taxoIT = newTaxo;
					}
				}
				//System.out.println("Finished getTaxoTree in " + (System.currentTimeMillis() - milli0) + " milliseconds"); // test		
				
			}
			
			

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return taxoTree;
		
	}
	
	

	public static int getDatabaseIsNcbiTaxonomyV3() throws Exception {
		String methodNameToReport = "QueriesTableTaxonomy getDatabaseIsNcbiTaxonomyV3";
		if (QueriesDatabaseMetaData.DATABASE_IS_NCBI_TAXONOMY_TREE_V3 == 0) {
			QueriesDatabaseMetaData.getDatabaseIsNcbiTaxonomyV3(null);
		}
		if (QueriesDatabaseMetaData.DATABASE_IS_NCBI_TAXONOMY_TREE_V3 == 1
				|| QueriesDatabaseMetaData.DATABASE_IS_NCBI_TAXONOMY_TREE_V3 == -1) {
			return QueriesDatabaseMetaData.DATABASE_IS_NCBI_TAXONOMY_TREE_V3;
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("DATABASE_IS_NCBI_TAXONOMY_TREE_V3 is neither 1 or -1")
					);
			return 0;
		}
	}


	public static HashSet<Integer> getHsTaxonIds_withScopeToConsider(
			Connection conn
			, String scopeIT // "species", "all", "none", DIGIT_taxon_id
			, int TaxIdToConsiderIfScopeIsSpecies) throws Exception {


		String methodNameToReport = "QueriesTableTaxonomy getHsTaxonIds_withScopeToConsider"
				+ " ; scopeIT="+ scopeIT
				+ " ; TaxIdToConsiderIfScopeIsSpecies="+ TaxIdToConsiderIfScopeIsSpecies
				;
		
		//args to check
		if (scopeIT == null || scopeIT.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("scopeIT == null || scopeIT.isEmpty()"));
		} else if (scopeIT.compareTo("species")==0 && TaxIdToConsiderIfScopeIsSpecies < 0) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("scopeIT.compareTo(species)==0 && TaxIdToConsiderIfScopeIsSpecies < 0"));
		}
		
		HashSet<Integer> hsToReturn = new HashSet<>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_taxo();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			if (getDatabaseIsNcbiTaxonomyV3() == 1) {	
				/*
				origami_cg=> select * from ncbi_taxonomy_tree limit 10;
				 ncbi_taxid | parent_taxid |                            lineage                             |  rank   | depth | is_leaf |                            scientific_name                     
				       
				------------+--------------+----------------------------------------------------------------+---------+-------+---------+----------------------------------------------------------------
				-------
				     370551 |       342023 | {1,131567,2,1783272,1239,91061,186826,1300,1301,1314,342023}   |         |    11 | t       | Streptococcus pyogenes MGAS9429
				    1009857 |        98795 | {1,131567,2,1224,1236,91347,1903409,32199,9,98795}             |         |    10 | t       | Buchnera aphidicola str. W106 (Myzus persicae)
				     591197 |       795750 | {1,131567,2,1783270,68336,1134404,795747,795748,795749,795750} | species |    10 | f       | Ignavibacterium album
				      44674 |        57665 | {1,131567,2,1224,68525,29547,213849,72294,57665}               | species |     9 | f       | Sulfurospirillum barnesii
				 */

				String command = "";
				
				//scopeIT // -scopeIT ["species", "all", "none", DIGIT_taxon_id]
				if (scopeIT.equalsIgnoreCase("species")) {
					String organismSpeciesAsString = "";

					String commandPre = "SELECT scientific_name FROM ncbi_taxonomy_tree WHERE ncbi_taxid = "+TaxIdToConsiderIfScopeIsSpecies;
					rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandPre, methodNameToReport);
					if (rs.next()) {
						String wholeScientificNameIT = rs.getString("scientific_name");
						String[] partWholeScientificNameIT = wholeScientificNameIT.split("\\s");
						if (partWholeScientificNameIT.length < 2) {
							UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("error partWholeScientificNameIT.length < 2 : "+wholeScientificNameIT+ " ; partWholeScientificNameIT.length = "+partWholeScientificNameIT.length));
						} else {
							organismSpeciesAsString = partWholeScientificNameIT[0] + " " + partWholeScientificNameIT[1];
						}
//						Matcher m = OrganismNameWithSpecies.matcher(wholeScientificNameIT);
//						if (m.matches()) {
//							organismSpeciesAsString = m.group(1);
//						} else {
//							UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("error matching pattern : "+wholeScientificNameIT));
//						}
						if (rs.next()) {
							UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("multiple results for commandPre="+commandPre));
						}
					} else {
						UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("no result for commandPre="+commandPre));
					}
					rs = null;
					if (organismSpeciesAsString.length() < 5) {
						UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("organismSpeciesAsString.length() < 5 for "+organismSpeciesAsString));
					}
					command = "SELECT ncbi_taxid FROM ncbi_taxonomy_tree WHERE scientific_name ILIKE '"+organismSpeciesAsString+"%'";
				} else if (scopeIT.equalsIgnoreCase("all")) {
					command = "SELECT ncbi_taxid FROM ncbi_taxonomy_tree";
				} else if (scopeIT.equalsIgnoreCase("none")) {
					return new HashSet<>();
				} else if (scopeIT.matches("^\\d+$")) {
					command = "SELECT ncbi_taxid FROM ncbi_taxonomy_tree WHERE ncbi_taxid = "+scopeIT+" OR "+scopeIT+" = ANY (lineage)";
				} else {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("unrecognized scopeIT = "+scopeIT));
				}
				
				//System.err.println(command);
				
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
				
				while (rs.next()) {
					int ncbiTaxidIT = rs.getInt("ncbi_taxid");
					hsToReturn.add(ncbiTaxidIT);
				}
				
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("failed getDatabaseIsNcbiTaxonomyV3() == 1, please update the schema of the database."));
			}
			
			

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return hsToReturn;

	}


}
