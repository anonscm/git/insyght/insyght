package fr.inra.jouy.server.queriesTable;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.objects.ObjAssociationAsMap;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.shared.TransStartStopGeneInfo;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.comparators.LightGeneItemComparators;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem.EnumOperatorFilterGeneBy;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem.EnumTypeFilterGeneBy;
import fr.inra.jouy.shared.pojos.databaseMapping.GeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;



// remark input files
//-pathToFileICEIME "/home/tlacroix/montages_reseau/home_maiage/Desktop/documents/Projets/ICE-IME/liste_ICE_IME_Dynamics/excel_file/ICE + IME_124 genomes_30_03_2017.csv - ICE_IME_w_locations.tsv"
//-pathToFileICEIME "/home/tlacroix/montages_reseau/home_maiage/Desktop/documents/Projets/ICE-IME/liste_ICE_IME_Dynamics/excel_file/ICE_+_IME_124 genomes_21_05_2019.tsv"

public class QueriesTableGenes {
	
	//static fields
	public static final int MAX_GENE_ITEM_RETURNED_ALLOWED_FOR_SEARCH = 50000;

	//global Pattern
	
	//specific qualifier pattern
	static Pattern NCBIProteinIdPatt = Pattern.compile("^(.+\\d+\\.\\d+)(\\s+.+)?$"); // type_qal protein_id or protein
	//dbxref pattern
	static Pattern EnsemblGenomesPatt = Pattern.compile("^EnsemblGenomes-.+:(.+?)(\\s+.+)?$"); 
	static Pattern GOAPatt = Pattern.compile("^GOA:(.+?)(\\s+.+)?$");
	static Pattern InterProPatt = Pattern.compile("^InterPro:(.+?)(\\s+.+)?$");
	static Pattern UniProtKBPatt = Pattern.compile("^UniProtKB.*?:(.+?)(\\s+.+)?$");
	static Pattern GOPatt = Pattern.compile("^GO:(.+?)(\\s+.+)?$");
	static Pattern UniParcPatt = Pattern.compile("^UniParc:(.+?)(\\s+.+)?$");
	static Pattern KEGGPatt = Pattern.compile("^KEGG:\\s..+[;$]"); // KEGG:\s..+[;$]
	static Pattern PFAMPatt = Pattern.compile("^PFAM:PF.+[\\s;$]"); // PFAM:PF.+[\s;$]
	static Pattern NCBIGIPatt = Pattern.compile("^[Gg][Ii]:(.+?)(\\s+.+)?$");
	static Pattern SubtiListPatt = Pattern.compile("^SubtiList:(.+?)(\\s+.+)?$");
	static Pattern PDBPatt = Pattern.compile("^PDB:(.+?)(\\s+.+)?$"); //http://www.ebi.ac.uk/pdbe/entry/search/index?text:4DDQ // db_xref : PDB:4DDQ
	
	
	// table genes
	
	//method that submit queries
	
	
	
	//Tested
	public static String getProteinSequenceWithGeneId(
			Connection conn
			, int geneId
			, boolean shouldBePresentInTable
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getProteinSequenceWithGeneId"
				+ " ; geneId="+geneId
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		//args to check
		//no
		//mandatory args
		if (geneId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("geneId <= 0")
					);
		}
		
		String stringToReturn = "";

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "";
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				command = "SELECT micado.prot_feat.proteine AS residues"
						+ " FROM genes, micado.prot_feat"
						+ " WHERE genes.gene_id = " + geneId
						+ " AND genes.accession = micado.prot_feat.accession"
						+ " AND genes.feature_id = micado.prot_feat.code_feat"
						;
			} else {
				command = "SELECT residues AS residues"
						+ " FROM genes"
						+ " WHERE gene_id = " + geneId
						;
			}
			
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			if (rs.next()) {
				stringToReturn = rs.getString("residues");
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("multiples entries found for gene_id ="+geneId));
				}
			} else {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("No entry found for gene_id = "+geneId));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try	
		
		return stringToReturn;
				
	}

	
	// return ObjAssociationAsMap.getRsColumn2String().get("name")
	// return ObjAssociationAsMap.getRsColumn2String().get("locus_tag")
	// return ObjAssociationAsMap.getRsColumn2Int().get("strand")
	public static ObjAssociationAsMap getGeneNameLocusTagStrandWithGeneId(
			Connection conn
			, int geneId
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getGeneNameLocusTagStrandWithGeneId"
				+ " ; geneId="+geneId
				;

		
		ObjAssociationAsMap oaamToReturn = new ObjAssociationAsMap();
		
		if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
			Statement statement = null;
			ResultSet rs = null;
			boolean closeConn = false;
			try {
				if (conn == null) {
					conn = DatabaseConf.getConnection_db();
					closeConn = true;
				}
				statement = conn.createStatement();
				String command = "SELECT name, strand, locus_tag"
						+ " FROM genes"
						+ " WHERE gene_id = " + geneId
						;
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
				if (rs.next()) {
					String nameIT = rs.getString("name");
					int strandIT = rs.getInt("strand");
					String locusTagIT = rs.getString("locus_tag");
					if (nameIT != null && ! nameIT.isEmpty()) {
						oaamToReturn.mapKeyStringValueToObjectField("name",nameIT);
					}
					oaamToReturn.mapKeyIntValueToObjectField("strand", strandIT);
					if (locusTagIT != null && ! locusTagIT.isEmpty()) {
						oaamToReturn.mapKeyStringValueToObjectField("locus_tag", locusTagIT);
					}
//					if (rs.next()) {
//						UtilitiesMethodsServer.reportException(
//								methodNameToReport
//								, new Exception("multiples entries found for gene_id ="+geneId));
//					}
				} else {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("No entry found for gene_id = "+geneId));
				}
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			} finally {
				UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
			}// try
			
		} else {
			ObjAssociationAsMap oaamIntermediate1 = getGeneNameStrandAccessionFeatureIdWithGeneId(conn, geneId);
			ArrayList<String> altGeneNameAndLocusTagFromTableMicadoQualifiers = QueriesTableMicadoQualifiers.getGeneNameAndLocusTagWithAccessionAndFeatureId_firstOnly(
					conn
					, oaamIntermediate1.getRsColumn2String().get("accession")
					, oaamIntermediate1.getRsColumn2Int().get("feature_id")
					);
			
			if (oaamIntermediate1.getRsColumn2String().get("name") != null && ! oaamIntermediate1.getRsColumn2String().get("name").isEmpty()) {
				oaamToReturn.mapKeyStringValueToObjectField("name", oaamIntermediate1.getRsColumn2String().get("name"));
			} else if ( ! altGeneNameAndLocusTagFromTableMicadoQualifiers.get(0).isEmpty() ) {
				oaamToReturn.mapKeyStringValueToObjectField("name", altGeneNameAndLocusTagFromTableMicadoQualifiers.get(0));
			}
			if (! altGeneNameAndLocusTagFromTableMicadoQualifiers.get(1).isEmpty()) {
				oaamToReturn.mapKeyStringValueToObjectField("locus_tag", altGeneNameAndLocusTagFromTableMicadoQualifiers.get(1));
			}
			if (oaamIntermediate1.getRsColumn2Int().get("strand") != null) {
				oaamToReturn.mapKeyIntValueToObjectField("strand", oaamIntermediate1.getRsColumn2Int().get("strand"));
			}
		}
		
		return oaamToReturn;
		
	}

	
	// return ObjAssociationAsMap.getRsColumn2String().get("name")
	// return ObjAssociationAsMap.getRsColumn2Int().get("strand")
	// return ObjAssociationAsMap.getRsColumn2String().get("accession")
	// return ObjAssociationAsMap.getRsColumn2Int().get("feature_id")
	public static ObjAssociationAsMap getGeneNameStrandAccessionFeatureIdWithGeneId(
			Connection conn
			, int geneId
			) throws Exception {
		
		String methodNameToReport = "QueriesTableGenes getGeneNameStrandAccessionFeatureIdWithGeneId"
				+ " ; geneId="+geneId
				;
		
		ObjAssociationAsMap oaamToReturn = new ObjAssociationAsMap();
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT"
					+ " genes.name"
					+ ", genes.strand"
					+ ", genes.feature_id"
					+ ", elements.accession"
					+ " FROM"
					+ " genes, elements"
					+ " WHERE"
					+ " genes.gene_id = " + geneId
					+ " AND elements.element_id=genes.element_id"
					;
					
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			if (rs.next()) {
				oaamToReturn.mapKeyStringValueToObjectField("name", rs.getString("name"));
				oaamToReturn.mapKeyIntValueToObjectField("strand", rs.getInt("strand"));
				oaamToReturn.mapKeyStringValueToObjectField("accession", rs.getString("accession"));
				oaamToReturn.mapKeyIntValueToObjectField("feature_id", rs.getInt("feature_id"));
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("multiples entries found for gene_id ="+geneId));
				}
			} else {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("No entry found for gene_id = "+geneId));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return oaamToReturn;
		
	}

	


	public static HashMap<Integer, Integer> getHMGeneId2ElementId_WithSetGeneId(
			Connection conn
			, Set<Integer> setGeneIds
			, boolean shouldBePresentInTable
			) throws Exception {

			String methodNameToReport = "QueriesTableGenes getHMGeneId2ElementId_WithAlGeneId"
						+ " ; setGeneIds="+ ( (setGeneIds != null ) ? setGeneIds.toString() : "NULL" )
						+ " shouldBePresentInTable="+shouldBePresentInTable
						;

			HashMap<Integer, Integer> hmToReturn = new HashMap<>();

			// check valid input
			//no
			if ( setGeneIds.contains(null) ) {
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception(
						"setGeneIds contain null element : "+setGeneIds.toString()
						)
						);
			}

			// build command

			Statement statement = null;
			ResultSet rs = null;
			boolean closeConn = false;

			try {

				if (conn == null) {
					conn = DatabaseConf.getConnection_db();
					closeConn = true;
				}
				statement = conn.createStatement();

				// select columns
				//int
				ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
				chm_alColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
				chm_alColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
				//String
				//ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
				//empty
	//			chm_alColumnSelectAsString_aliase2columnName.put("??", "??");

				ArrayList<String> tableFrom = new ArrayList<String>();
				tableFrom.add("genes");
				String whereClause = "WHERE gene_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(setGeneIds)
						+ ")";
	
				ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
						chm_alColumnSelectAsInt_aliase2columnName,
						null
						, tableFrom, whereClause);
	
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);

				while (rs.next()) {
					int geneIdIT = rs.getInt("gene_id");
					int elementIdIT = rs.getInt("element_id");
					hmToReturn.put(geneIdIT,elementIdIT);
				}
//				//run command
//				hmToReturn.putAll(
//						UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_HM(
//								conn
//								, "gene_id"
//								, Integer.class//, Integer.class
//								, "element_id"
//								, Integer.class
//								, objSQLCommandIT
//								, shouldBePresentInTable
//								, true
//								, true
//								, false
//								, false
//								, methodNameToReport
//								)
//						);

				if (shouldBePresentInTable) {
					if (hmToReturn.size() != setGeneIds.size() ) {
						UtilitiesMethodsServer.reportException(methodNameToReport, new Exception(
								"shouldBePresentInTable is "+shouldBePresentInTable
								+ " but hmToReturn.size() ="+hmToReturn.size()
								+ " != setGeneIds.size() ="+setGeneIds.size()
								)
								);
					}
				}
				
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			} finally {
				UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
			}// try
			
			return hmToReturn;

	}
			
			
			
	public static int getElementIdWithGeneId(
			Connection conn
			, int geneId
			, boolean shouldBePresentInTable)
			throws Exception {

		String methodNameToReport = "QueriesTableGenes getElementIdWithGeneId"
				+ " ; geneId="+geneId
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		// check valid input
		//no
		//mandatory args
		if (geneId <= 0) {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("geneId <= 0"));
		}
		
		int intToReturn = -1;
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			// build command
			
			// select columns
			//int
			ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			chm_alColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
			//String
			ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			//empty
	//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
			
			ArrayList<String> tableFrom = new ArrayList<String>();
			tableFrom.add("genes");
			String whereClause = "WHERE gene_id = " + geneId;
			ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
					chm_alColumnSelectAsInt_aliase2columnName,
					chm_alColumnSelectAsString_aliase2columnName,
					tableFrom, whereClause);
			
			//run command
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			if (rs.next()) {
				int elementIdIT = rs.getInt("element_id");
				intToReturn = elementIdIT;
			}
			
//			ObjAssociationAsMap oaamReturned = UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//					conn, 
//					ObjAssociationAsMap.class,
//					objSQLCommandIT, 
//					shouldBePresentInTable, 
//					true, 
//					methodNameToReport);

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		if (shouldBePresentInTable && intToReturn == -1) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("shouldBePresentInTable but no row returned"));
		}
		return intToReturn;
//		if(oaamReturned.getRsColumn2Int().get("element_id") != null){
//			return 	oaamReturned.getRsColumn2Int().get("element_id");
//		} else {
//			return -1;
//		}
		
	}

	
	
	//tested
	// return ObjAssociationAsMap.getRsColumn2Int().get("element_id")
	// return ObjAssociationAsMap.getRsColumn2Int().get("feature_id")
	public static ObjAssociationAsMap getElementIdAndFeatureIdWithGeneId(Connection conn,
			int geneId, boolean shouldBePresentInTable) throws Exception {

		String methodNameToReport = "QueriesTableGenes getElementIdAndFeatureIdWithGeneId"
				+ " ; geneId="+geneId
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;

		ObjAssociationAsMap oaamToReturn = new ObjAssociationAsMap();
		
		
		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
		chm_alColumnSelectAsInt_aliase2columnName.put("feature_id", "feature_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");

		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("genes");
		String whereClause = "WHERE gene_id = " + geneId;
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);

		//run command
//		return UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//				conn, 
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		boolean foundOneEntry = false;
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			
			if (rs.next()) {
				int elementIdIT = rs.getInt("element_id");
				int featureIdIT = rs.getInt("feature_id");
				// return ObjAssociationAsMap.getRsColumn2Int().get("element_id")
				// return ObjAssociationAsMap.getRsColumn2Int().get("feature_id")
				HashMap<String, Integer> hmIT = new HashMap<>();
				hmIT.put("element_id", elementIdIT);
				hmIT.put("feature_id", featureIdIT);
				oaamToReturn.setRsColumn2Int(hmIT);
				foundOneEntry = true;
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		if (shouldBePresentInTable && ! foundOneEntry) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("shouldBePresentInTable && ! foundOneEntry"));
		}
		return oaamToReturn;
		
	}


	
	public static ArrayList<Integer> getFirstGeneStartAndLastGeneStopWithListOrigamiGeneIds(
			Connection conn
			, ArrayList<Integer> alGeneIdsSent
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getFirstGeneStartAndLastGeneStopWithListOrigamiGeneIds"
				+ " ; alGeneIdsSent="+ ( (alGeneIdsSent != null ) ? alGeneIdsSent.toString() : "NULL" )
				;
		
		ArrayList<Integer> alToReturn = new ArrayList<Integer>();
		int geneStartInPbInElement = -1;
		int geneStopInPbInElement = -1;

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			String command = "SELECT start, stop from genes WHERE gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alGeneIdsSent)
			/*for (int i = 0; i < alGeneIdsSent.size(); i++) {
				if (i == 0) {
					command += alGeneIdsSent.get(i);
				} else {
					command += "," + alGeneIdsSent.get(i);
				}
			}*/
					+ ")";

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);

			while (rs.next()) {
				int currStart = rs.getInt("start");
				int currStop = rs.getInt("stop");

				if (geneStartInPbInElement == -1) {
					geneStartInPbInElement = currStart;
				} else if (currStart < geneStartInPbInElement) {
					geneStartInPbInElement = currStart;
				}

				if (geneStopInPbInElement == -1) {
					geneStopInPbInElement = currStop;
				} else if (currStop > geneStopInPbInElement) {
					geneStopInPbInElement = currStop;
				}

			}


		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);

		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try

		alToReturn.add(geneStartInPbInElement);
		alToReturn.add(geneStopInPbInElement);
		return alToReturn;

	}

	
	//Tested
	public static int getGeneIdWithGeneNameAndOrganismId_checkTableGeneOnly_firstMatchOnly (Connection conn,
			String geneName
			, int organismId
			, boolean shouldBePresentInTable
			, boolean checkValidFreeStringInput
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getGeneIdWithGeneNameAndOrganismId_checkTableGeneOnly_firstMatchOnly"
				+ " ; geneName="+geneName
				+ " ; organismId="+organismId
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;

		// check valid input
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					geneName, "gene name", false, false);
		}
		
		int intToReturn = -1;
		
		// build command
		

		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("genes");
		String whereClause = "WHERE organism_id = " + organismId
				+ " AND name  = '" + geneName + "'";
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		
		//run command
//		ObjAssociationAsMap oaamReturned = UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//				conn, 
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			if (rs.next()) {
				intToReturn = rs.getInt("gene_id");
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
//		if(oaamReturned.getRsColumn2Int().get("gene_id") != null){
//			return 	oaamReturned.getRsColumn2Int().get("gene_id");
//		} else {
//			return -1;
//		}
		if (shouldBePresentInTable && intToReturn == -1) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("shouldBePresentInTable but no row returned"));
		}
		
		return intToReturn;
		
		
	}


	//Tested
	public static int getGeneIdWithElementIdAndCodeFeat (Connection conn,
			int elementId, int codeFeat,
			boolean shouldBePresentInTable) throws Exception {

		String methodNameToReport = "QueriesTableGenes getGeneIdWithElementIdAndCodeFeat"
				+ " ; elementId="+elementId
				+ " ; codeFeat="+codeFeat
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		// check valid input
		//no
		
		int intToReturn = -1;
		
		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");

		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("genes");
		String whereClause = "WHERE element_id = " + elementId
				+ " AND feature_id  = " + codeFeat;
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		
		//run command
//		ObjAssociationAsMap oaamReturned = UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//				conn, 
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			if (rs.next()) {
				intToReturn = rs.getInt("gene_id");
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("multiple gene ids for elementId="+elementId+" and feature_id  = " + codeFeat));
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
//		if(oaamReturned.getRsColumn2Int().get("gene_id") != null){
//			return 	oaamReturned.getRsColumn2Int().get("gene_id");
//		} else {
//			return -1;
//		}
		if (shouldBePresentInTable && intToReturn == -1) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("shouldBePresentInTable but no row returned for elementId="+elementId+" and feature_id  = " + codeFeat));
		}
		
		return intToReturn;

	}
	
	


	// method composite of other methods (do not submit queries directly)
	// no need Test
	public static int getGeneIdWithAccessionAndCodeFeat (
			Connection conn, String accession, int codeFeat,
			boolean shouldBePresentInTable,
			boolean checkValidFreeStringInput
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getGeneIdWithAccessionAndCodeFeat"
				+ " ; accession="+accession
				+ " ; codeFeat="+codeFeat
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;
		
		// check valid input
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					accession, "accession", false, false);
		}

		int elementIdIT = QueriesTableElements.getElementIdWithAccession(conn, accession,
				shouldBePresentInTable, false);
		if(elementIdIT > 0){
			return QueriesTableGenes.getGeneIdWithElementIdAndCodeFeat(conn, elementIdIT, codeFeat,
					shouldBePresentInTable);
		} else {
			return -1;
		}
	}
	
	// method composite of other methods (do not submit queries directly)
	// no need to test
	public static int getGeneIdWithGeneNameAndOrganismId_checkTablesGenesThenQualifiers_firstMatchOnly(
			Connection conn
			, String geneName
			, int organismId
			, boolean checkValidFreeStringInput
			) throws Exception {


		String methodNameToReport = "QueriesTableGenes getGeneIdWithGeneNameAndOrganismId"
				+ " ; geneName="+geneName
				+ " ; organismId="+organismId
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;
		
		// check valid input
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					geneName, "geneName", false, false);
		}
		
		int geneIdToReturn = -1;
		
		geneIdToReturn = QueriesTableGenes.getGeneIdWithGeneNameAndOrganismId_checkTableGeneOnly_firstMatchOnly(
					conn,
					geneName, organismId,
					false,
					false);
			
		if (geneIdToReturn <= 0) {
			// no direct gene name but check qualifiers
			ArrayList<ObjAssociationAsMap> typeQual2QueryString_OR = new ArrayList<ObjAssociationAsMap>();
			typeQual2QueryString_OR.add(new ObjAssociationAsMap("gene_name", geneName));
			typeQual2QueryString_OR.add(new ObjAssociationAsMap("gene", geneName));
			geneIdToReturn = getGeneIdWithtypeQual2QueryStringAndOrganismId(conn, typeQual2QueryString_OR, organismId);
			
		}

		return geneIdToReturn;

	}
	

	// method composite of other methods (do not submit queries directly)
	// no need to test
	public static int getGeneIdWithLocusTagAndOrganismId(
			Connection conn
			, String locusTag
			, int organismId
			, boolean checkValidFreeStringInput
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getGeneIdWithLocusTagAndOrganismId"
				+ " ; locusTag="+locusTag
				+ " ; organismId="+organismId
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;
		
		// check valid input
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					locusTag, "locusTag", false, false);
		}
		
		ArrayList<ObjAssociationAsMap> typeQual2QueryString_OR = new ArrayList<ObjAssociationAsMap>();
		typeQual2QueryString_OR.add(new ObjAssociationAsMap("locus_tag", locusTag));
		return getGeneIdWithtypeQual2QueryStringAndOrganismId(conn, typeQual2QueryString_OR, organismId);
		
	}
	
	

	
	//private method
	// no need to test
	private static int getGeneIdWithtypeQual2QueryStringAndOrganismId(
			Connection conn, ArrayList<ObjAssociationAsMap> typeQual2QueryString_OR, int organismId) throws Exception {

		int geneIdToReturn = -1;
		
		HashSet<String> listAccnum_OR = QueriesTableElements.getHsAccessions_withOrgaId(conn, organismId);
		ArrayList<ObjAssociationAsMap> alAACFIT = 
				QueriesTableMicadoQualifiers.getAlAssocAccnumCodeFeatWithAlListAccnumAndTypeQual2QueryString_MatchEqual(conn,
				listAccnum_OR,
				typeQual2QueryString_OR,
				false,
				false
				);
		
		for(ObjAssociationAsMap aacfIT : alAACFIT ){
			//sometimes not present in table gene because not the CDS qualifiers but the gene qualifiers
			geneIdToReturn = QueriesTableGenes.getGeneIdWithAccessionAndCodeFeat(
					conn,
					aacfIT.getRsColumn2String().get("accession"),
					aacfIT.getRsColumn2Int().get("code_feat"),
					false,
					true);
			if (geneIdToReturn >= 0){
				return geneIdToReturn;
			}
		}

		return geneIdToReturn;
		
	}
	
	
	public static HashSet<Integer> getAlGeneId_WithAlElementIdAndAlPresenceAbsenceOrgaIds(
			Connection conn
			, HashSet<Integer> hsElementIds // elementids need to be same organism
			, ArrayList<String> alListOrgaIdAsString
			, ArrayList<String> alPresenceOrAbsence
			, boolean isAnd
			) throws Exception {

		String methodName = "QueriesTableGenes getAllGeneIdWithElementIdAndAlPresenceAbsenceOrgaIds"
				+ " ; hsElementIds="+ ( (hsElementIds != null ) ? hsElementIds.toString() : "NULL" )
				+ " ; alListOrgaIdAsString="+ ( (alListOrgaIdAsString != null ) ? alListOrgaIdAsString.toString() : "NULL" )
				+ " ; alListPresenceOrAbsence="+ ( (alPresenceOrAbsence != null ) ? alPresenceOrAbsence.toString() : "NULL" )
				+ " ; isAnd="+isAnd
				;
		
		if (hsElementIds == null || hsElementIds.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodName, new Exception("alElementIds == null || alElementIds.isEmpty()"));
		}
		if (alListOrgaIdAsString == null || alListOrgaIdAsString.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodName, new Exception("alListOrgaIdAsString == null || alListOrgaIdAsString.isEmpty()"));
		} else {
			for (int i = 0; i < alListOrgaIdAsString.size(); i++) {
				String sIT = alListOrgaIdAsString.get(i);
				if (sIT.isEmpty()) {
					UtilitiesMethodsServer.reportException(methodName, new Exception("the list orga is empty at position "
									+ i));
				} else if (sIT.matches("^.*;.*$")) {
					UtilitiesMethodsServer.reportException(methodName, new Exception("the list orga has a semi colon at position "
									+ i));
				}
			}
		}
		if (alPresenceOrAbsence == null || alPresenceOrAbsence.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodName, new Exception("alListPresenceOrAbsence == null || alListPresenceOrAbsence.isEmpty()"));
		} else {
			for (int i = 0; i < alPresenceOrAbsence.size(); i++) {
				String sIT = alPresenceOrAbsence.get(i);
				if (sIT.isEmpty()) {
					UtilitiesMethodsServer.reportException(methodName, new Exception("the list presence absence is empty at position "
									+ i));
				} else if (sIT.matches("^.*;.*$")) {
					UtilitiesMethodsServer.reportException(methodName, new Exception("the list presence absence has a semi colon at position "
									+ i));
				}
			}
		}

		// ArrayList<Integer> arrayListLGId = new ArrayList<Integer>();

		//Statement statement = null;
		//ResultSet rs = null;
		boolean closeConn = false;

		HashSet<Integer> hsGeneIdsToReturn = new HashSet<>();
		
		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			// get the list gene for each subfilter
			//ArrayList<ArrayList<Integer>> rootAlAlGeneIdsBeforeFusionOrIntersc = new ArrayList<ArrayList<Integer>>();
			ArrayList<HashSet<Integer>> rootAlAlGeneIdsBeforeFusionOrIntersc = new ArrayList<>();
			
			for (int i = 0; i < alListOrgaIdAsString.size(); i++) {
				String listOrgaIdAsString = alListOrgaIdAsString.get(i);
				String[] lOrgaIds = listOrgaIdAsString.split(",");
				ArrayList<Integer> alSOrgaIds = new ArrayList<>();
				for (int j = 0; j < lOrgaIds.length; j++) {
					String orgaIDAsString = lOrgaIds[j];
					try {
						int orgaIdIT = Integer.parseInt(orgaIDAsString);
						alSOrgaIds.add(orgaIdIT);
					} catch (Exception e) {
						UtilitiesMethodsServer.reportException(methodName, new Exception(
								"could not parse orga id as integer : "
								+ orgaIDAsString
								+ " within listOrgaIdAsString="+listOrgaIdAsString
								+ " ; Exception="+e.getMessage()
								)
								);
					}
				}
				
				String presenceOrAbsenceIT = alPresenceOrAbsence.get(i);
				
				boolean presence = true;
				if (presenceOrAbsenceIT.compareTo("presence") == 0) {
					presence = true;
				} else if (presenceOrAbsenceIT.compareTo("absence") == 0) {
					presence = false;
				} else {
					UtilitiesMethodsServer.reportException(methodName, new Exception("neither presence or absence : "
									+ presenceOrAbsenceIT));
				}



				//{sOrgaId}{alGeneIds}
				HashMap<Integer,ArrayList<Integer>> hmSOrgaId2AlGeneIds_BeforeIntersc = new HashMap<>();
				//{qEltId}{sOrgaId}{alGeneIds}
				//HashMap<Integer,HashMap<Integer,ArrayList<Integer>>> hmQEltId2SOrgaId2AlGeneIds_BeforeIntersc = new HashMap<>();
				
				
				//{sOrgaId}{alAlignmentParamId}
				HashMap<Integer,HashSet<Long>> HmSOrgaId2AlAlignmentParamId = QueriesTableAlignmentParams.getHmSOrgaId2AlAlignmentParamId_withAlQElementIdsAndAlSOrgaIds(
						conn
						, hsElementIds
						, alSOrgaIds
						, false //isMirrorRequest
						, false
						);
				
				//loop through each s_orga ids individually to take avantage of speed optimisation if range of alignment_params are quite close
				for (int sOrgaIdIT : alSOrgaIds) {
					
					ArrayList<Integer> alQGeneIdsBeforePresenceAbsence = new ArrayList<>();

					// new less memory used but too slow
//					ArrayList<Integer> alSOrgaIdIT = new ArrayList<>();
//					alSOrgaIdIT.add(sOrgaIdIT);
//					
//					HashSet<Long> hsAlignmentParamIdIT = QueriesTableAlignmentParams.getHsAlignmentParamId_withAlQElementIdsAndAlSOrgaIds(
//							conn
//							, hsElementIds
//							, alSOrgaIdIT
//							, false //isMirrorRequest
//							, false
//							);
//					if ( ! hsAlignmentParamIdIT.isEmpty()) {
//						HashSet<Long> hsAlignmentIds = QueriesTableAlignments.getListAlignmentIdsWithAlAlignmentParamId(
//								conn
//								, hsAlignmentParamIdIT
//								, true
//								, false
//								);
//						if ( ! hsAlignmentIds.isEmpty()) {
//							// RQ type for genes
//							// type 1 : strong homolog BDBH with avg score 220, average evalue
//							// 5.76e-5
//							// type 2 : weaker homolog with avg score 137, average evalue
//							// 2.2e-4
//							// type 3 mismatch
//							// type 4 s insertion
//							// type 5 q insertion
//							ArrayList<Integer> alTypeToRetrieve = new ArrayList<>();
//							alTypeToRetrieve.add(1);
//							alTypeToRetrieve.add(2);
//							//alTypeToRetrieve.add(3);
//							//alTypeToRetrieve.add(4);
//							//alTypeToRetrieve.add(5);
//							alQGeneIdsBeforePresenceAbsence = QueriesTableAlignmentPairs.getListQGenesIdsWithAlAlignmentId(
//									conn
//									, hsAlignmentIds
//									, true
//									, alTypeToRetrieve
//									);
//						}
//					}
					if (HmSOrgaId2AlAlignmentParamId.containsKey(sOrgaIdIT)) {
						
						HashSet<Long> hsAlignmentIds = QueriesTableAlignments.getListAlignmentIdsWithAlAlignmentParamId(
								conn
								, HmSOrgaId2AlAlignmentParamId.get(sOrgaIdIT)
								, true
								, false
								);
						if ( ! hsAlignmentIds.isEmpty()) {
							// RQ type for genes
							// type 1 : strong homolog BDBH with avg score 220, average evalue
							// 5.76e-5
							// type 2 : weaker homolog with avg score 137, average evalue
							// 2.2e-4
							// type 3 mismatch
							// type 4 s insertion
							// type 5 q insertion
							HashSet<Integer> hsTypeToRetrieve = new HashSet<>();
							hsTypeToRetrieve.add(1);
							hsTypeToRetrieve.add(2);
							//alTypeToRetrieve.add(3);
							//alTypeToRetrieve.add(4);
							//alTypeToRetrieve.add(5);
							alQGeneIdsBeforePresenceAbsence = QueriesTableAlignmentPairs.getListQGenesIdsWithAlAlignmentId(
									conn
									, hsAlignmentIds
									, true
									, hsTypeToRetrieve
									);
						}
					}
					if ( ! presence) {
						HashMap<Integer, HashSet<Integer>> hmElementId2HsMirrorAbsenceGenesIds = getHmElementId2HsGenesIds_WithAlElementId_optionalAlGeneIdsToExclude(
								conn
								, hsElementIds
								, alQGeneIdsBeforePresenceAbsence
								, false
								);
						ArrayList<Integer> mirrorAbsenceAlQGeneIds = new ArrayList<>();
						for (Map.Entry<Integer, HashSet<Integer>> entryHmElementId2 : hmElementId2HsMirrorAbsenceGenesIds.entrySet()) {
						    //int elementIdIT = entryHmElementId2.getKey();
						    HashSet<Integer> hsMirrorAbsenceGenesIdsIT = entryHmElementId2.getValue();
						    mirrorAbsenceAlQGeneIds.addAll(hsMirrorAbsenceGenesIdsIT);
						}
						hmSOrgaId2AlGeneIds_BeforeIntersc.put(sOrgaIdIT, mirrorAbsenceAlQGeneIds);
					} else {
						hmSOrgaId2AlGeneIds_BeforeIntersc.put(sOrgaIdIT, alQGeneIdsBeforePresenceAbsence);
					}
				}
				
				
				//OLD TO DEL
//				// store each result for individual orga in subAlAlGeneIdsBeforeIntersc
//				ArrayList<ArrayList<Integer>> subAlAlGeneIdsBeforeIntersc = new ArrayList<ArrayList<Integer>>();
//				String[] lOrgaIds = listOrgaIdAsString.split(",");
//				for (int j = 0; j < lOrgaIds.length; j++) {
//					String orgaIDAsString = lOrgaIds[j];
//					if (orgaIDAsString.matches("^\\d+$")) {
//						int orgaIdIT = Integer.parseInt(orgaIDAsString);
//
//						ArrayList<Long> arrayListAlignmentParamIds = QueriesTableAlignmentParams.getAlAlignmentParamIdWithQElementIdAndSOrgaId(
//								conn
//								, elementId
//								, orgaIdIT
//								, false
//								, false
//								);				
//						
//						if (arrayListAlignmentParamIds.isEmpty()) {
//							if (presence) {
//								subAlAlGeneIdsBeforeIntersc.add(new ArrayList<Integer>());
//								continue;
//							} else {
//								
//								ArrayList<Integer> listOnlyElementId = new ArrayList<>();
//								listOnlyElementId.add(elementId);
//								ArrayList<Integer> arrayListAllGIdFromElement = QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
//										conn
//										, listOnlyElementId
//										, null
//										, true
//										);
//								
//								subAlAlGeneIdsBeforeIntersc.add(arrayListAllGIdFromElement);
//								continue;
//							}
//						}
//
//						
//						ArrayList<Long> arrayListAlignmentIds = QueriesTableAlignments.getListAlignmentIdsWithAlAlignmentParamId(
//								conn
//								, arrayListAlignmentParamIds
//								, false
//								);
//
//						if (arrayListAlignmentIds.isEmpty()) {
//							if (presence) {
//								subAlAlGeneIdsBeforeIntersc.add(new ArrayList<Integer>());
//								continue;
//							} else {
//								
//								ArrayList<Integer> listOnlyElementId = new ArrayList<>();
//								listOnlyElementId.add(elementId);
//								ArrayList<Integer> arrayListAllGIdFromElement = QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
//										conn
//										, listOnlyElementId
//										, null
//										, true
//										);
//								subAlAlGeneIdsBeforeIntersc.add(arrayListAllGIdFromElement);
//								continue;
//							}
//						}
//
//						// RQ type for genes
//						// type 1 : strong homolog BDBH with avg score 220, average evalue
//						// 5.76e-5
//						// type 2 : weaker homolog with avg score 137, average evalue
//						// 2.2e-4
//						// type 3 mismatch
//						// type 4 s insertion
//						// type 5 q insertion
//						ArrayList<Integer> alTypeToRetrieve = new ArrayList<>();
//						alTypeToRetrieve.add(1);
//						alTypeToRetrieve.add(2);
//						//alTypeToRetrieve.add(3);
//						//alTypeToRetrieve.add(4);
//						//alTypeToRetrieve.add(5);
//						ArrayList<Integer> arrayListQGeneIdsBeforePresenceAbsence = QueriesTableAlignmentPairs.getListQGenesIdsWithAlAlignmentId(
//								conn
//								, arrayListAlignmentIds
//								, alTypeToRetrieve
//								);
//
//						if (arrayListQGeneIdsBeforePresenceAbsence.isEmpty()) {
//							if (presence) {
//								subAlAlGeneIdsBeforeIntersc
//										.add(new ArrayList<Integer>());
//								continue;
//							} else {
//								
//								ArrayList<Integer> listOnlyElementId = new ArrayList<>();
//								listOnlyElementId.add(elementId);
//								ArrayList<Integer> arrayListAllGIdFromElement = QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
//										conn
//										, listOnlyElementId
//										, null
//										, true
//										);
//								subAlAlGeneIdsBeforeIntersc.add(arrayListAllGIdFromElement);
//								continue;
//							}
//						}
//
//
//						
//						ArrayList<Integer> arrayListQGeneIds = new ArrayList<Integer>();
//						if (!presence) {
//							
//							ArrayList<Integer> listOnlyElementId = new ArrayList<>();
//							listOnlyElementId.add(elementId);
//							arrayListQGeneIds.addAll(
//									QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
//											conn
//											, listOnlyElementId
//											, arrayListQGeneIdsBeforePresenceAbsence
//											, false
//											)
//									);
//							
//						} else {
//							arrayListQGeneIds = arrayListQGeneIdsBeforePresenceAbsence;
//						}
//
//						subAlAlGeneIdsBeforeIntersc.add(arrayListQGeneIds);
//
//					} else {
//						UtilitiesMethodsServer.reportException(methodName, new Exception("could not parse orga id as integer : "
//										+ presenceOrAbsenceIT));
//					}
//				}

				// intersect with {sOrgaId}{alGeneIds} HashMap<Integer,ArrayList<Integer>> hmSOrgaId2AlGeneIds_BeforeIntersc
				
				// intersect subAlAlGeneIdsBeforeIntersc and store in
				// rootAlAlGeneIdsBeforeFusionOrIntersc
				// interesction
				HashSet<Integer> subHsGeneIdsFinals = new HashSet<Integer>();
				boolean firstIter = true;
				for (Map.Entry<Integer,ArrayList<Integer>> entryHmSOrgaId2 : hmSOrgaId2AlGeneIds_BeforeIntersc.entrySet()) {
				    //int sOrgaIdIT = entryHmSOrgaId2.getKey();
				    ArrayList<Integer> alGeneIdsIT = entryHmSOrgaId2.getValue();
				    if (firstIter) {
				    	subHsGeneIdsFinals.addAll(alGeneIdsIT);
				    	firstIter = false;
				    } else {
				    	subHsGeneIdsFinals.retainAll(alGeneIdsIT);
				    }
				}
				
				// OLD TO DEL
//				if (subAlAlGeneIdsBeforeIntersc.isEmpty()) {
//					subAlGeneIdsFinals = new ArrayList<Integer>();
//				} else if (subAlAlGeneIdsBeforeIntersc.size() == 1) {
//					subAlGeneIdsFinals = subAlAlGeneIdsBeforeIntersc.get(0);
//				} else {
//					for (int k = 0; k < subAlAlGeneIdsBeforeIntersc.size(); k++) {
//						if (k == 0) {
//							continue;
//						} else if (k == 1) {
//							// init intersection
//							if ((k - 1) >= 0) {
//								ArrayList<Integer> alFgiIT = subAlAlGeneIdsBeforeIntersc
//										.get(k);
//								ArrayList<Integer> alFgiPrev = subAlAlGeneIdsBeforeIntersc
//										.get(k - 1);
//								subAlGeneIdsFinals = new ArrayList<Integer>(
//										alFgiIT.size() > alFgiPrev.size() ? alFgiIT
//												.size() : alFgiPrev.size());
//								subAlGeneIdsFinals.addAll(alFgiIT);
//								subAlGeneIdsFinals.retainAll(alFgiPrev);
//							} else {
//								UtilitiesMethodsServer.reportException(methodName, new Exception("k-1 inferior at 0 when deal with sub operator cinquo"));
//							}
//						} else {
//							// continue intersection
//							if ((k - 1) >= 0) {
//								ArrayList<Integer> alFgiIT = subAlAlGeneIdsBeforeIntersc
//										.get(k);
//								ArrayList<Integer> c = new ArrayList<Integer>(
//										alFgiIT.size() > subAlGeneIdsFinals
//												.size() ? alFgiIT.size()
//												: subAlGeneIdsFinals.size());
//								c.addAll(alFgiIT);
//								c.retainAll(subAlGeneIdsFinals);
//								subAlGeneIdsFinals = c;
//							} else {
//								UtilitiesMethodsServer.reportException(methodName, new Exception("k-1 inferior at 0 when deal with sub operator sixo"));
//							}
//						}
//					}
//				}
				rootAlAlGeneIdsBeforeFusionOrIntersc.add(subHsGeneIdsFinals);

			}

			
			// make the fusion or interestion of
			// alAlGeneIdsBeforeFusionOrIntersc

			boolean firstIter = true;
			for ( HashSet<Integer> subHsGeneIdsFinals : rootAlAlGeneIdsBeforeFusionOrIntersc) {
				if (firstIter) {
					hsGeneIdsToReturn.addAll(subHsGeneIdsFinals);
			    	firstIter = false;
			    } else {
			    	if (isAnd) {
						// interesction
			    		hsGeneIdsToReturn.retainAll(subHsGeneIdsFinals);
			    	} else {
						// union
			    		hsGeneIdsToReturn.addAll(subHsGeneIdsFinals);
			    	}
			    }
			}
			
			
//			if (rootAlAlGeneIdsBeforeFusionOrIntersc.isEmpty()) {
//				return new ArrayList<Integer>();
//			} else if (rootAlAlGeneIdsBeforeFusionOrIntersc.size() == 1) {
//				return rootAlAlGeneIdsBeforeFusionOrIntersc.get(0);
//			} else {
//				if (isAnd) {
//					// interesction
//					for (int i = 0; i < rootAlAlGeneIdsBeforeFusionOrIntersc
//							.size(); i++) {
//						if (i == 0) {
//							continue;
//						} else if (i == 1) {
//							// init intersection
//							if ((i - 1) >= 0) {
//								ArrayList<Integer> alFgiIT = rootAlAlGeneIdsBeforeFusionOrIntersc
//										.get(i);
//								ArrayList<Integer> alFgiPrev = rootAlAlGeneIdsBeforeFusionOrIntersc
//										.get(i - 1);
//								alGeneIdsFinals = new ArrayList<Integer>(
//										alFgiIT.size() > alFgiPrev.size() ? alFgiIT
//												.size() : alFgiPrev.size());
//								alGeneIdsFinals.addAll(alFgiIT);
//								alGeneIdsFinals.retainAll(alFgiPrev);
//							} else {
//								UtilitiesMethodsServer.reportException(methodName, new Exception("i-1 inferior at 0 when deal with sub operator cinquo"));
//							}
//						} else {
//							// continue intersection
//							if ((i - 1) >= 0) {
//								ArrayList<Integer> alFgiIT = rootAlAlGeneIdsBeforeFusionOrIntersc
//										.get(i);
//								ArrayList<Integer> c = new ArrayList<Integer>(
//										alFgiIT.size() > alGeneIdsFinals
//												.size() ? alFgiIT.size()
//												: alGeneIdsFinals.size());
//								c.addAll(alFgiIT);
//								c.retainAll(alGeneIdsFinals);
//								alGeneIdsFinals = c;
//							} else {
//								UtilitiesMethodsServer.reportException(methodName, new Exception("i-1 inferior at 0 when deal with sub operator sixo"));
//							}
//						}
//					}
//				} else {
//					// union
//					for (int i = 0; i < rootAlAlGeneIdsBeforeFusionOrIntersc
//							.size(); i++) {
//						if (i == 0) {
//							continue;
//						} else if (i == 1) {
//							// init union
//							if ((i - 1) >= 0) {
//								ArrayList<Integer> alFgiIT = rootAlAlGeneIdsBeforeFusionOrIntersc
//										.get(i);
//								ArrayList<Integer> alFgiPrev = rootAlAlGeneIdsBeforeFusionOrIntersc
//										.get(i - 1);
//								alGeneIdsFinals = new ArrayList<Integer>(
//										alFgiIT.size() + alFgiPrev.size());
//								UtilitiesMethodsShared.addToArrayListNoDups(alGeneIdsFinals, alFgiIT);
//								UtilitiesMethodsShared.addToArrayListNoDups(alGeneIdsFinals, alFgiPrev);
//							} else {
//								UtilitiesMethodsServer.reportException(methodName, new Exception("i-1 inferior at 0 when deal with sub operator septo"));
//							}
//
//						} else {
//							// continue union
//							if ((i - 1) >= 0) {
//								ArrayList<Integer> alFgiIT = rootAlAlGeneIdsBeforeFusionOrIntersc
//										.get(i);
//								ArrayList<Integer> c = new ArrayList<Integer>(
//										alFgiIT.size()
//												+ alGeneIdsFinals.size());
//								UtilitiesMethodsShared.addToArrayListNoDups(c, alFgiIT);
//								UtilitiesMethodsShared.addToArrayListNoDups(c, alGeneIdsFinals);
//								alGeneIdsFinals = c;
//							} else {
//								UtilitiesMethodsServer.reportException(methodName, new Exception("i-1 inferior at 0 when deal with sub operator huito"));
//							}
//						}
//					}
//				}
//			}


		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodName, ex);

		} finally {
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodName);
			}
		}// try

		return hsGeneIdsToReturn;
		
	}
	
	
	//public static ArrayList<Integer> getAlGeneIdWithAlElementIdAndAlTypeQual(

	public static ArrayList<Integer> getAlGeneIdWithAlElementIdAndAlTypeQualToMatchAndAlStringToMatch(
			Connection conn
			, ArrayList<String> alMatchRegexString
			, boolean isAnd
			//, int elementId
			, HashSet<Integer> hsElementIds
			, ArrayList<String> alTypeQualToMatch
			, boolean getMirrorList
			, boolean isNotNull
			, boolean searchForGeneNameInTableGeneToo
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getAlGeneIdWithElementIdAndAlTypeQual"
				+ " ; alMatchRegexString="+ ( (alMatchRegexString != null ) ? alMatchRegexString.toString() : "NULL" )
				+ " ; isAnd="+isAnd
				//+ " ; elementId="+elementId
				+ " ; hsElementIds="+ ( (hsElementIds != null ) ? hsElementIds.toString() : "NULL" )
				+ " ; alTypeQualToMatch"+ ( (alTypeQualToMatch != null ) ? alTypeQualToMatch.toString() : "NULL" )
				+ " ; getMirrorList="+getMirrorList
				+ " ; isNotNull="+isNotNull
				+ " ; searchForGeneNameInTableGeneToo="+searchForGeneNameInTableGeneToo
				;

		//System.err.println(methodNameToReport);

		if (alMatchRegexString == null || alMatchRegexString.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("the input array of string to match is null or empty"));
		} else {
			for (int i = 0; i < alMatchRegexString.size(); i++) {
				String sIT = alMatchRegexString.get(i);
				if (sIT.isEmpty()) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("the string to match is empty at position "
									+ i));
				} else if (sIT.matches("^.*;.*$")) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("the string to match has a semi colon at position "
									+ i));
				}
			}
		}

		if (alTypeQualToMatch == null || alTypeQualToMatch.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("the input array of type qual is null or empty"));
		} else {
			for (int i = 0; i < alTypeQualToMatch.size(); i++) {
				String sIT = alTypeQualToMatch.get(i);
				if (sIT.isEmpty()) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("the type qual is empty at position "
									+ i));
				} else if (sIT.matches("^.*;.*$")) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("the type qual has a semi colon at position "
									+ i));
				}
			}
		}
		if (hsElementIds.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("hsElementIds.isEmpty()"));
		}

		ArrayList<Integer> alToReturn = new ArrayList<Integer>();

		//Statement statement = null;
		//ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			
//			String accnumIT = QueriesTableElements.getAccessionWithElementId(
//					conn
//					, elementId
//					, true);
			HashMap<String,Integer> hmAccession2ElementId = QueriesTableElements.getHmAccession2ElementId_WithAlElementId(
					conn
					, hsElementIds
					, true
					, true);
			
			//ArrayList<ArrayList<Integer>> arrayListLGIdPre = new ArrayList<>();
			
//			Structure to work with with QueriesTableMicadoQualifiers.getAlCodeFeatWithAccessionAndAlTypeQualAndQualifier
//			alAccnum + alTypeQualToMatch + alStringToMatch + isNotNull
//			=> {elementId}{matchRegexString}{alFeatureIds}
			

			HashMap<Integer,HashMap<String,HashSet<Integer>>> multiHmElementId2matchRegexString2hsFeatureIds = QueriesTableMicadoQualifiers.getMultiHm_Accession2matchRegexString2alFeatureIds_WithSetAccessionAndAlTypeQualAndAlRegexStringToMatchQualifier(
					conn
					, hmAccession2ElementId.keySet()
					, alTypeQualToMatch
					, alMatchRegexString
					, isNotNull
					, hmAccession2ElementId
					);
			

			if (multiHmElementId2matchRegexString2hsFeatureIds.isEmpty()) {
				return alToReturn;
			}

			// get hmElementId2hsConcatenatedFeatureIds => {elementId}{hsFeatureIds}
			HashMap<Integer,HashSet<Integer>> hmElementId2hsConcatenatedFeatureIds = new HashMap<>();
			for(Map.Entry<Integer,HashMap<String,HashSet<Integer>>> entryHmElementId2 : multiHmElementId2matchRegexString2hsFeatureIds.entrySet()) {
				Integer elementIdIT = entryHmElementId2.getKey();
				HashMap<String,HashSet<Integer>> hmMatchRegexString2alFeatureIdsIT = entryHmElementId2.getValue();
				
				HashSet<Integer> hsConcatenatedFeatureIds = new HashSet<>();
				hmElementId2hsConcatenatedFeatureIds.put(elementIdIT, hsConcatenatedFeatureIds);
				
				for(Map.Entry<String,HashSet<Integer>> entryHmMatchRegexString2 : hmMatchRegexString2alFeatureIdsIT.entrySet()) {
					//String matchRegexStringIT = entryHmMatchRegexString2.getKey();
					HashSet<Integer> alFeatureIdsIT = entryHmMatchRegexString2.getValue();
					
					hsConcatenatedFeatureIds.addAll(alFeatureIdsIT);
				}
			}
			
			HashMap<Integer,HashMap<Integer,Integer>> multiHmElementId2featureId2geneId = getMultiHm_ElementId2featureIds2geneId_WithHmElementId2hsFeatureIds(
					conn
					, hmElementId2hsConcatenatedFeatureIds
					);
			
			//subsitute FeatureIds by gene_id in multiHmElementId2matchRegexString2alFeatureIds
//			FROM : {elementId}{matchRegexString}{alFeatureIds}
//			Structure to work with with getAlGeneIdsWithinHmEletId2MatchRegexString_WithAccnumAndAlFeatureIds_optionalMatchRegexGeneName :
//			TO : {elementId}{matchRegexString}{hsGeneIds}
			HashMap<Integer,HashMap<String,HashSet<Integer>>> multiHmElementId2matchRegexString2hsGeneIds = new HashMap<>();
			ArrayList<Integer> alGeneIdAlreadyTakenIntoAccount = new ArrayList<>();
			for(Map.Entry<Integer,HashMap<String,HashSet<Integer>>> entryHmElementId2 : multiHmElementId2matchRegexString2hsFeatureIds.entrySet()) {
				Integer elementIdIT = entryHmElementId2.getKey();
				HashMap<String,HashSet<Integer>> hmMatchRegexString2alFeatureIdsIT = entryHmElementId2.getValue();
				
				HashMap<String,HashSet<Integer>> hmMatchRegexString2hsGeneIdsIT = new HashMap<>();
				multiHmElementId2matchRegexString2hsGeneIds.put(elementIdIT, hmMatchRegexString2hsGeneIdsIT);
				
				for(Map.Entry<String,HashSet<Integer>> entryHmMatchRegexString2 : hmMatchRegexString2alFeatureIdsIT.entrySet()) {
					String matchRegexStringIT = entryHmMatchRegexString2.getKey();
					HashSet<Integer> alFeatureIdsIT = entryHmMatchRegexString2.getValue();
					
					HashSet<Integer> hsGeneIdsIT = new HashSet<>();
					hmMatchRegexString2hsGeneIdsIT.put(matchRegexStringIT, hsGeneIdsIT);
					
					for (Integer featureIdsIT : alFeatureIdsIT) {
						int geneIdMappedFromFeatureId = -1;
						if (multiHmElementId2featureId2geneId.containsKey(elementIdIT)) {
							HashMap<Integer,Integer> featureId2geneIdIt = multiHmElementId2featureId2geneId.get(elementIdIT);
							if (featureId2geneIdIt.containsKey(featureIdsIT)) {
								geneIdMappedFromFeatureId = featureId2geneIdIt.get(featureIdsIT);
							}
						}
						if (geneIdMappedFromFeatureId < 0) {
//							UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("geneIdMappedFromFeatureId < 0 = "+geneIdMappedFromFeatureId
//									+" ; elementIdIT="+elementIdIT
//									+" ; matchRegexStringIT="+matchRegexStringIT
//									+" ; featureIdsIT="+featureIdsIT
//									));
							//can be RNA genes in table micado but not in table genes which has only CDS
						} else {
							hsGeneIdsIT.add(geneIdMappedFromFeatureId);
							alGeneIdAlreadyTakenIntoAccount.add(geneIdMappedFromFeatureId);
						}
					}
				}
			}
			
			//complement {elementId}{matchRegexString}{hsGeneIds} by searching directly in table gene column name
			if (searchForGeneNameInTableGeneToo) {

				//return {elementId}{matchRegexString}{alGeneIds}
				HashMap<Integer,HashMap<String,HashSet<Integer>>> multiHmElementId2matchRegexString2hsGeneIds_searchForGeneNameInTableGeneToo = 
						getAlGeneIds_WithAlElementIdAndAlStringToMatchInGeneName_optionalAlGeneIdsToExclude(
							conn
							//, alElementIds
							, hsElementIds
							, alMatchRegexString
							, alGeneIdAlreadyTakenIntoAccount
							, false
							, false
						);

				// merge multiHmElementId2matchRegexString2hsGeneIds_searchForGeneNameInTableGeneToo in multiHmElementId2matchRegexString2hsGeneIds
				for(Map.Entry<Integer,HashMap<String,HashSet<Integer>>> entryHmElementId2_searchForGeneNameInTableGeneToo : multiHmElementId2matchRegexString2hsGeneIds_searchForGeneNameInTableGeneToo.entrySet()) {
					Integer elementId_searchForGeneNameInTableGeneTooIT = entryHmElementId2_searchForGeneNameInTableGeneToo.getKey();
					HashMap<String,HashSet<Integer>> hmMatchRegexString2hsGeneIds_searchForGeneNameInTableGeneTooIT = entryHmElementId2_searchForGeneNameInTableGeneToo.getValue();

					HashMap<String,HashSet<Integer>> hmMatchRegexString2hsGeneIdsIT = null;
					if (multiHmElementId2matchRegexString2hsGeneIds.containsKey(elementId_searchForGeneNameInTableGeneTooIT)) {
						hmMatchRegexString2hsGeneIdsIT = multiHmElementId2matchRegexString2hsGeneIds.get(elementId_searchForGeneNameInTableGeneTooIT);
					} else {
						hmMatchRegexString2hsGeneIdsIT = new HashMap<>();
						multiHmElementId2matchRegexString2hsGeneIds.put(elementId_searchForGeneNameInTableGeneTooIT, hmMatchRegexString2hsGeneIdsIT);
					}
					
					for(Map.Entry<String,HashSet<Integer>> entryHmMatchRegexString_searchForGeneNameInTableGeneToo : hmMatchRegexString2hsGeneIds_searchForGeneNameInTableGeneTooIT.entrySet()) {
						String matchRegexString_searchForGeneNameInTableGeneTooIT = entryHmMatchRegexString_searchForGeneNameInTableGeneToo.getKey();
						HashSet<Integer> hsGeneIds_searchForGeneNameInTableGeneTooIT = entryHmMatchRegexString_searchForGeneNameInTableGeneToo.getValue();

						HashSet<Integer> hsGeneIdsIT = null;
						if (hmMatchRegexString2hsGeneIdsIT.containsKey(matchRegexString_searchForGeneNameInTableGeneTooIT)) {
							hsGeneIdsIT = hmMatchRegexString2hsGeneIdsIT.get(matchRegexString_searchForGeneNameInTableGeneTooIT);
							hsGeneIdsIT.addAll(hsGeneIds_searchForGeneNameInTableGeneTooIT);
						} else {
							hsGeneIdsIT = new HashSet<>();
							hsGeneIdsIT.addAll(hsGeneIds_searchForGeneNameInTableGeneTooIT);
							hmMatchRegexString2hsGeneIdsIT.put(matchRegexString_searchForGeneNameInTableGeneTooIT, hsGeneIdsIT);
						}
					}
				}
				
			}
			
			//OLD
//			for (int i = 0; i < alStringToMatch.size(); i++) {
//				String matchRegexString = alStringToMatch.get(i);
//				
//				ArrayList<Integer> alFeatureIds = QueriesTableMicadoQualifiers.getAlCodeFeatWithAccessionAndAlTypeQualAndQualifier(
//						conn
//						, accnumIT
//						, alTypeQualToMatch
//						, matchRegexString
//						, isNotNull
//						);
//				
//				if (alFeatureIds.isEmpty() && !getMirrorList
//						&& !searchForGeneNameInTableGeneToo) {
//					return new ArrayList<Integer>();
//				}
//
//				HashSet<Integer> hsLGIdPreTmp = getAlGeneIdsWithElementId_optionalAlFeatureIds_optionalMatchRegexGeneName (
//						conn
//						, elementId
//						, alFeatureIds
//						, searchForGeneNameInTableGeneToo
//						, matchRegexString
//						);
//				ArrayList<Integer> arrayListLGIdPreTmp = new ArrayList<>();
//				arrayListLGIdPreTmp.addAll(hsLGIdPreTmp);
//				
//				arrayListLGIdPre.add(arrayListLGIdPreTmp);
//			}
					
			
//			Structure to work with with isAnd on multiHmElementId2matchRegexString2hsGeneIds :
//			{elementId}{matchRegexString}{alGeneIds}
//			=> {elementId}{hsGeneIds}
			HashMap<Integer,HashSet<Integer>> hmElementId2mergedHsGeneIds = new HashMap<>();
			for(Map.Entry<Integer,HashMap<String,HashSet<Integer>>> entryHmElementId2 : multiHmElementId2matchRegexString2hsGeneIds.entrySet()) {
				Integer elementIdIT = entryHmElementId2.getKey();
				HashMap<String,HashSet<Integer>> hmMatchRegexString2hsGeneIdsIT = entryHmElementId2.getValue();
				HashSet<Integer> finalByElementIdHsGeneIds = new HashSet<>();
				if (isAnd) {
					// intersection
					boolean firstIter = true;
					for(Map.Entry<String,HashSet<Integer>> entryHmMatchRegexString : hmMatchRegexString2hsGeneIdsIT.entrySet()) {
						//String matchRegexStringIT = entryHmMatchRegexString.getKey();
						HashSet<Integer> hsGeneIdsIT = entryHmMatchRegexString.getValue();
						if (firstIter) {
							finalByElementIdHsGeneIds.addAll(hsGeneIdsIT);
							firstIter = false;
						} else {
							finalByElementIdHsGeneIds.retainAll(hsGeneIdsIT);
						}
					}
				} else {
					// union
					for(Map.Entry<String,HashSet<Integer>> entryHmMatchRegexString : hmMatchRegexString2hsGeneIdsIT.entrySet()) {
						//String matchRegexStringIT = entryHmMatchRegexString.getKey();
						HashSet<Integer> hsGeneIdsIT = entryHmMatchRegexString.getValue();
						finalByElementIdHsGeneIds.addAll(hsGeneIdsIT);
					}
				}
				if ( ! finalByElementIdHsGeneIds.isEmpty()) {
					hmElementId2mergedHsGeneIds.put(elementIdIT, finalByElementIdHsGeneIds);
				}
				
			}

			// OLD
//			if (arrayListLGIdPre.size() > 1 && isAnd) {
//				// System.out.println("intersection here");
//				for (int i = 0; i < arrayListLGIdPre.size(); i++) {
//					if ((i - 1) >= 0) {
//						ArrayList<Integer> alFgiIT = arrayListLGIdPre.get(i);
//						// System.out.println(alFgiIT);
//						// ArrayList<Integer> alFgiPrev =
//						// alAlGeneIdsAfterInnerLogicalOperator.get((alAlGeneIdsAfterInnerLogicalOperator.size()-1));
//						// //arrayListLGId
//						// System.out.println(arrayListLGId);
//						ArrayList<Integer> c = new ArrayList<Integer>(
//								alFgiIT.size() > alToReturn.size() ? alFgiIT
//										.size() : alToReturn.size());
//						c.addAll(alFgiIT);
//						c.retainAll(alToReturn);
//						alToReturn = c;
//					} else {
//						alToReturn = arrayListLGIdPre.get(i);
//					}
//				}
//
//			} else if (arrayListLGIdPre.size() > 1 && !isAnd) {
//				for (int i = 0; i < arrayListLGIdPre.size(); i++) {
//					if ((i - 1) >= 0) {
//						ArrayList<Integer> alFgiIT = arrayListLGIdPre.get(i);
//						// ArrayList<Integer> alFgiPrev =
//						// alAlGeneIdsAfterInnerLogicalOperator.get((alAlGeneIdsAfterInnerLogicalOperator.size()-1));//arrayListLGId
//						ArrayList<Integer> c = new ArrayList<Integer>(
//								alFgiIT.size() + alToReturn.size());
//						UtilitiesMethodsShared.addToArrayListNoDups(c, alFgiIT);
//						UtilitiesMethodsShared.addToArrayListNoDups(c, alToReturn);
//						alToReturn = c;
//					} else {
//						alToReturn = arrayListLGIdPre.get(i);
//					}
//				}
//			} else if (arrayListLGIdPre.isEmpty()) {
//				return new ArrayList<Integer>();
//			} else {
//				alToReturn = arrayListLGIdPre.get(0);
//			}


			
//			Structure to work with with getMirrorList :
//			=> {elementId}{hsGeneIds}
//			HashMap<Integer,HashSet<Integer>> hmElementId2mergedHsGeneIds
			
			for(Map.Entry<Integer,HashSet<Integer>> entryHmElementId2 : hmElementId2mergedHsGeneIds.entrySet()) {
				//Integer elementIdIT = entryHmElementId2.getKey();
				HashSet<Integer> hsGeneIdsIT = entryHmElementId2.getValue();
				alToReturn.addAll(hsGeneIdsIT);
			}
			
			if (getMirrorList) {

				HashMap<Integer,HashSet<Integer>> hmElementId2mirrorOfMergedHsGeneIds = getHmElementId2HsGenesIds_WithAlElementId_optionalAlGeneIdsToExclude(
						conn
						//, alElementIds
						, hsElementIds
						, alToReturn
						, false
						);
				alToReturn.clear();
				for(Map.Entry<Integer,HashSet<Integer>> entryHmElementId2 : hmElementId2mirrorOfMergedHsGeneIds.entrySet()) {
					//Integer elementIdIT = entryHmElementId2.getKey();
					HashSet<Integer> hsGeneIdsIT = entryHmElementId2.getValue();
					alToReturn.addAll(hsGeneIdsIT);
				}
				
				// OLD
//				ArrayList<Integer> listElementId = new ArrayList<>();
//				listElementId.add(elementId);
//				ArrayList<Integer> arrayListLGIdIsNull = getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
//						conn
//						, listElementId
//						, alToReturn
//						, false);
//				alToReturn = arrayListLGIdIsNull;

			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			//UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return alToReturn;
	}

	



	public static ArrayList<Integer> getAlGenesIdsOrderedByAlElementIdWithAlElementId_optionalOrderedByStart(
			Connection conn
			, ArrayList<Integer> arrayListElementIdOrderedBySizeDesc
			, boolean orderByGeneStart
			, boolean shouldBePresentInTable
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getAlGenesIdsOrderedByAlElementIdWithAlElementId_optionalOrderedByStart"
				+ " ; arrayListElementIdOrderedBySizeDesc="+ ( (arrayListElementIdOrderedBySizeDesc != null ) ? arrayListElementIdOrderedBySizeDesc.toString() : "NULL" )
				+ " ; orderByGeneStart="+orderByGeneStart
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		ArrayList<Integer> alToReturn = new ArrayList<>();
		
		// check valid input
		//no
		// mandatory arguments
		if (arrayListElementIdOrderedBySizeDesc == null || arrayListElementIdOrderedBySizeDesc.isEmpty()) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("arrayListElementIdOrderedBySizeDesc == null || arrayListElementIdOrderedBySizeDesc.isEmpty()")
					);
		}
		

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT gene_id, element_id FROM genes WHERE element_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(arrayListElementIdOrderedBySizeDesc)
						+ ")";
					;
			if (orderByGeneStart) {
				command += " ORDER BY start ASC";
			}

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			HashMap<Integer, ArrayList<Integer>> hmElementId2AlGeneId = new HashMap<>();
			while (rs.next()) {
				int geneIdIT = rs.getInt("gene_id");
				int elementIdIT = rs.getInt("element_id");
				if (hmElementId2AlGeneId.containsKey(elementIdIT)) {
					ArrayList<Integer> alGeneIdIT = hmElementId2AlGeneId.get(elementIdIT);
					alGeneIdIT.add(geneIdIT);
				} else {
					ArrayList<Integer> alGeneIdIT = new ArrayList<>();
					alGeneIdIT.add(geneIdIT);
					hmElementId2AlGeneId.put(elementIdIT, alGeneIdIT);
				}
			}
			for (Integer elementIdIT : arrayListElementIdOrderedBySizeDesc) {
				if (hmElementId2AlGeneId.containsKey(elementIdIT)) {
					ArrayList<Integer> alGeneIdIT = hmElementId2AlGeneId.get(elementIdIT);
					alToReturn.addAll(alGeneIdIT);
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return alToReturn;
		
	}
	
	
	public static HashSet<Integer> getHsGeneIds_withHsOrgaId(
			Connection conn
			, HashSet<Integer> hsOrgaIds
			) throws Exception {
		String methodNameToReport = "QueriesTableGenes getHsGeneIds_withHsOrgaId"
				+ " ; hsOrgaIds="+ ( (hsOrgaIds != null ) ? hsOrgaIds.toString() : "NULL" )
				;
		HashSet<Integer> hsToReturn = new HashSet<>();

		// check valid input
		//no
		// mandatory arguments
		if (hsOrgaIds == null || hsOrgaIds.isEmpty()) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("hsOrgaIds == null || hsOrgaIds.isEmpty()")
					);
		}
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT gene_id"
					+ " FROM genes";
			String whereClause = " WHERE organism_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsOrgaIds)
					+ ")"
					;
			command += whereClause;

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				int geneIdIT = rs.getInt("gene_id");
				hsToReturn.add(geneIdIT);
			}
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return hsToReturn;
	}
		

	public static ArrayList<Integer> getAlGenesIdsWithAlOrganismIds_optionalAlGeneIds(
			Connection conn
			, ArrayList<Integer> alOrgaIds
			, ArrayList<Integer> alGeneIds
			, boolean shouldBePresentInTable
			) throws Exception {
		String methodNameToReport = "QueriesTableGenes getListGenesIdsWithAlOrganismIds"
				+ " ; alOrgaIds="+ ( (alOrgaIds != null ) ? alOrgaIds.toString() : "NULL" )
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		ArrayList<Integer> alToReturn = new ArrayList<>();
		
		// check valid input
		//no
		// mandatory arguments
		if (alOrgaIds == null || alOrgaIds.isEmpty()) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("alOrgaIds == null || alOrgaIds.isEmpty()")
					);
		}
		
		// build command
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//			chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("genes");
		String whereClause = "WHERE organism_id IN ("
				+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alOrgaIds)
				+ ")"
				;
		if (alGeneIds != null && ! alGeneIds.isEmpty() ) {
			whereClause += " AND gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alGeneIds)
					+ ")"
					;
		}

		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		//run command
//		alToReturn.addAll(UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_alInt(
//				conn, 
//				"gene_id",
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport));
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				int geneIdIT = rs.getInt("gene_id");
				alToReturn.add(geneIdIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (shouldBePresentInTable && alToReturn.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("shouldBePresentInTable && alToReturn.isEmpty()"));
		}

		return alToReturn;
	}


	private static HashMap<Integer, HashMap<Integer, Integer>> getMultiHm_ElementId2featureIds2geneId_WithHmElementId2hsFeatureIds(
			Connection conn,
			HashMap<Integer,HashSet<Integer>> hmElementId2hsFeatureIds
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getMultiHm_ElementId2featureIds2geneId_WithHmElementId2hsFeatureIds"
				+ " ; hmElementId2hsFeatureIds="+ hmElementId2hsFeatureIds.entrySet().stream()
						.map(
								elementId -> "elementId=" + elementId.getKey() + " -> hsFeatureIds=" + elementId.getValue().toString()
						)
						.collect(Collectors.toList())
						.toString()
//				+ " ; multiHmElementId2matchRegexString2alFeatureIds="+ multiHmElementId2matchRegexString2alFeatureIds.entrySet().stream().flatMap(elementId ->
//				elementId.getValue().entrySet().stream().flatMap(matchRegexString ->
//				matchRegexString.getValue().stream()
//		                .map(alFeatureIds -> "alFeatureIds=" + alFeatureIds.toString() + "<-matchRegexString=" + matchRegexString.getKey() + "<-elementId=" + elementId.getKey())))
//				.collect(Collectors.toList()).toString()
		        ;
		
		//args to check
//		UtilitiesMethodsServer.checkValidFreeStringInput(
//				methodNameToReport
//				, multiHmElementId2matchRegexString2alFeatureIds.entrySet().stream().flatMap(elementId ->
//					elementId.getValue().entrySet().stream().map(matchRegexString -> matchRegexString.getKey() ))
//					.collect(Collectors.toList()).toString()
//				, "matchRegexString"
//				, true
//				, true);
		
		//mandatory args
		if (hmElementId2hsFeatureIds.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport, 
					new Exception("hmElementId2hsFeatureIds.isEmpty() : "+hmElementId2hsFeatureIds.size()));
		}
		
		HashMap<Integer, HashMap<Integer, Integer>> multiHmToReturn = new HashMap<>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT element_id, feature_id, gene_id"
					+ " FROM genes"
					+ " WHERE"
					//+ "element_id = " + elementId
					;
			
			boolean firstIter = true;
			for(Map.Entry<Integer,HashSet<Integer>> entryHmElementId2 : hmElementId2hsFeatureIds.entrySet()) {
				Integer elementIdIT = entryHmElementId2.getKey();
				HashSet<Integer> hsFeatureIdsIT = entryHmElementId2.getValue();
				if ( ! hsFeatureIdsIT.isEmpty()) {
					if (firstIter) {
						firstIter = false;
					} else {
						command += " OR";
					}
					command += " ("
								+ " element_id = " + elementIdIT
								+ " AND"
								+ " feature_id IN ("
								+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsFeatureIdsIT)
								+ " )"
								+ " )"
								;
				}
			}
			
			if (firstIter) {
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("firstIter is still true after iterating over hmElementId2hsFeatureIds"));
			}
			
			
			//UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("command="+command));
			
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			while (rs.next()) {
				int elementIdIT = rs.getInt("element_id");
				int featureIdIT = rs.getInt("feature_id");
				int geneIdIT = rs.getInt("gene_id");
				
				// MultiHm_ElementId2featureIds2geneId
				if (multiHmToReturn.containsKey(elementIdIT)) {
					HashMap<Integer, Integer> featureIds2geneIdIT = multiHmToReturn.get(elementIdIT);
					if (featureIds2geneIdIT.containsKey(featureIdIT)) {
						UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("featureIds2geneIdIT.containsKey(featureIdIT) : "
								+ " ; elementIdIT="+elementIdIT
								+ " ; featureIdIT="+featureIdIT
								+ " ; geneIdIT="+geneIdIT
								)
								);
					} else {
						featureIds2geneIdIT.put(featureIdIT, geneIdIT);
					}
				} else {
					HashMap<Integer, Integer> featureIds2geneIdIT = new HashMap<>();
					featureIds2geneIdIT.put(featureIdIT, geneIdIT);
					multiHmToReturn.put(elementIdIT, featureIds2geneIdIT);
				}
			}// while
			
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return multiHmToReturn;
		
		
	}



	private static HashMap<Integer, HashMap<String, HashSet<Integer>>> getAlGeneIds_WithAlElementIdAndAlStringToMatchInGeneName_optionalAlGeneIdsToExclude(
			Connection conn
			//, ArrayList<Integer> alElementIds
			, HashSet<Integer> hsElementIds
			, ArrayList<String> alMatchRegexString
			, ArrayList<Integer> alGeneIdAlreadyTakenIntoAccount
			, boolean checkValidFreeStringInput
			, boolean checkMandatoryArgs
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getAlGeneIds_WithAlElementIdAndAlStringToMatchInGeneName_optionalAlGeneIdsToExclude"
				+ " ; alElementIds="+ ( (hsElementIds != null ) ? hsElementIds.toString() : "NULL" )
				+ " ; alMatchRegexString="+ ( (alMatchRegexString != null ) ? alMatchRegexString.toString() : "NULL" )
				+ " ; alGeneIdAlreadyTakenIntoAccount="+ ( (alGeneIdAlreadyTakenIntoAccount != null ) ? alGeneIdAlreadyTakenIntoAccount.toString() : "NULL" )
				;
		
		if (checkValidFreeStringInput) {
			//args to check
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport
					, alMatchRegexString.toString()
					, "alMatchRegexString"
					, true
					, true);
		}
		
		//mandatory args
		if (checkMandatoryArgs) {
			if (hsElementIds.isEmpty()) {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("alElementIds.isEmpty()")
						);
			}
			if (alMatchRegexString.isEmpty()) {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("alMatchRegexString.isEmpty()")
						);
			}
		}

		HashMap<Integer, HashMap<String, HashSet<Integer>>> multiHmToReturn = new HashMap<>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			for ( String matchRegexStringIT : alMatchRegexString) {

				String command = "SELECT element_id, gene_id"
						+ " FROM genes"
						+ " WHERE element_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsElementIds)
						+ " )"
						;
				
				if (alGeneIdAlreadyTakenIntoAccount != null && ! alGeneIdAlreadyTakenIntoAccount.isEmpty()) {
					command += " AND gene_id NOT IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alGeneIdAlreadyTakenIntoAccount)
						+ " )"
						;
				}
				command += " AND name <> ''";
				command += " AND name " + matchRegexStringIT;

				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

				while (rs.next()) {
					int elementIdIT = rs.getInt("element_id");
					int geneIdIT = rs.getInt("gene_id");
					if (multiHmToReturn.containsKey(elementIdIT)) {
						// previous entry
						HashMap<String, HashSet<Integer>> matchRegexString2hsGeneIdsIT = multiHmToReturn.get(elementIdIT);
						if (matchRegexString2hsGeneIdsIT.containsKey(matchRegexStringIT)) {
							// previous entry matchRegexStringIT
							HashSet<Integer> hsGeneIdsIT = matchRegexString2hsGeneIdsIT.get(matchRegexStringIT);
							hsGeneIdsIT.add(geneIdIT);
						} else {
							//new entry matchRegexStringIT
							HashSet<Integer> hsGeneIdsIT = new HashSet<>();
							hsGeneIdsIT.add(geneIdIT);
							matchRegexString2hsGeneIdsIT.put(matchRegexStringIT, hsGeneIdsIT);
						}
					} else {
						//new accnum entry
						HashMap<String, HashSet<Integer>> matchRegexString2hsGeneIdsIT = new HashMap<>();
						HashSet<Integer> hsGeneIdsIT = new HashSet<>();
						hsGeneIdsIT.add(geneIdIT);
						matchRegexString2hsGeneIdsIT.put(matchRegexStringIT, hsGeneIdsIT);
						multiHmToReturn.put(elementIdIT, matchRegexString2hsGeneIdsIT);
					}
					
				}// while
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return multiHmToReturn;
		
	}

	
	
	public static HashSet<Integer> getAlGeneIdsWithElementId_optionalAlFeatureIds_optionalMatchRegexGeneName(
			Connection conn
			, int elementId
			, ArrayList<Integer> alFeatureIds
			, boolean searchForGeneNameInTableGeneToo
			, String matchRegexString
			) throws Exception {
		
		String methodNameToReport = "QueriesTableGenes getAlGeneIdsWithElementId_optionalAlFeatureIds_optionalMatchRegexGeneName"
				+ " ; elementId="+ elementId
				+ " ; alFeatureIds="+ ( (alFeatureIds != null ) ? alFeatureIds.toString() : "NULL" )
				+ " ; searchForGeneNameInTableGeneToo="+searchForGeneNameInTableGeneToo
				+ " ; matchRegexString="+matchRegexString
				;
		
		//args to check
		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport
				, matchRegexString
				, "matchRegexString"
				, true
				, true);
		//mandatory args
		if (elementId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("elementId <= 0")
					);
		}

		HashSet<Integer> hsToReturn = new HashSet<>();
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			String command = "SELECT gene_id FROM genes WHERE element_id = "
					+ elementId;

			if (!alFeatureIds.isEmpty() || searchForGeneNameInTableGeneToo) {
				command += " AND (";
			}

			if (!alFeatureIds.isEmpty()) {
				command += "feature_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alFeatureIds)
						+ ")";
			}
			if (!alFeatureIds.isEmpty() && searchForGeneNameInTableGeneToo) {
				command += " OR ";
			}
			if (searchForGeneNameInTableGeneToo) {
				command += " name " + matchRegexString;
			}

			if (!alFeatureIds.isEmpty() || searchForGeneNameInTableGeneToo) {
				command += " )";
			}

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			while (rs.next()) {
				hsToReturn.add(rs.getInt("gene_id"));
			}// while
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return hsToReturn;	
	}
	
	private static HashMap<Integer, ArrayList<Integer>> getHmElementId2alGeneIds_withAlElementIdsAndAlHmStartPb2StopPb(
			Connection conn
			, HashSet<Integer> hsElementIds
			, ArrayList<Map<Integer,Integer>> alMapSingletonStartPb2StopPb
			, boolean isAnd
			, boolean checkMandatoryArgs
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getHmElementId2alGeneIds_withAlElementIdsAndAlHmStartPb2StopPb"
				+ " ; hsElementIds="+ ( (hsElementIds != null ) ? hsElementIds.toString() : "NULL" )
				//+ " ; alHmStartPb2StopPb="+ ( (alHmStartPb2StopPb != null ) ? alHmStartPb2StopPb.toString() : "NULL" )
				+ " ; alHmStartPb2StopPb="+ alMapSingletonStartPb2StopPb.stream()
					    .map(map -> map.toString())
					    .collect(Collectors.toList())
						.toString()
				+ " ; isAnd="+isAnd
				+ " ; checkMandatoryArgs="+checkMandatoryArgs
				;
		
		if (checkMandatoryArgs) {
			if (hsElementIds == null || hsElementIds.isEmpty()) {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("hsElementIds == null OR hsElementIds.isEmpty()"));
			}
		}
		
		HashMap<Integer, ArrayList<Integer>> hmToReturn = new HashMap<>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT element_id, gene_id"
					+ " FROM genes"
					+ " WHERE element_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsElementIds)
					+ ")";
			boolean firstIter = true;
			if (alMapSingletonStartPb2StopPb != null && ! alMapSingletonStartPb2StopPb.isEmpty() ) {
				command += " AND (";
				for (Map<Integer,Integer> mapSingletonStartPb2StopPbIT : alMapSingletonStartPb2StopPb) {
					for (Map.Entry<Integer, Integer> entrySingletonStartPb2 : mapSingletonStartPb2StopPbIT.entrySet()) {
						Integer startPbIT = entrySingletonStartPb2.getKey();
						Integer stopPbIT = entrySingletonStartPb2.getValue();
						if (firstIter) {
							firstIter = false;
						} else {
							if (isAnd) {
								command += " AND";
							} else {
								command += " OR";
							}
						}
						command += " ( start >= " + startPbIT + " AND  stop <= " + stopPbIT + " )";
						break; // singleton Map
					}
				}

				command += " )";
			}

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			while (rs.next()) {
				int elementIdIT = rs.getInt("element_id");
				int geneIdIT = rs.getInt("gene_id");
				if (hmToReturn.containsKey(elementIdIT)) {
					ArrayList<Integer> alGeneIdsIT = hmToReturn.get(elementIdIT);
					alGeneIdsIT.add(geneIdIT);
				} else {
					ArrayList<Integer> alGeneIdsIT = new ArrayList<>();
					alGeneIdsIT.add(geneIdIT);
					hmToReturn.put(elementIdIT, alGeneIdsIT);
				}
			}// while

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return hmToReturn;
		
	}


	private static HashMap<Integer, HashSet<Integer>> getHmElementId2HsGenesIds_WithAlElementId_optionalAlGeneIdsToExclude(
			Connection conn
			, HashSet<Integer> hsElementIds
			, ArrayList<Integer> alGeneIdsToExclude
			, boolean checkMandatoryArgs
			) throws Exception {
		
		String methodNameToReport = "QueriesTableGenes getHmElementId2HsGenesIds_WithAlElementId_optionalAlGeneIdsToExclude"
				+ " ; hsElementIds="+ ( (hsElementIds != null ) ? hsElementIds.toString() : "NULL" )
				+ " ; alGeneIdsToExclude="+ ( (alGeneIdsToExclude != null ) ? alGeneIdsToExclude.toString() : "NULL" )
				;
		
		// check valid input
		//no
		//mandatory args
		if (checkMandatoryArgs) {
			if (hsElementIds != null && ! hsElementIds.isEmpty()) {
				//ok
			} else {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("NOT hsElementIds != null && ! hsElementIds.isEmpty() : "+hsElementIds)
						);
			}
		}

		HashMap<Integer, HashSet<Integer>> hmToReturn = new HashMap<>();
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT element_id, gene_id"
					+ " FROM genes"
					+ " WHERE"
					+ " element_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsElementIds)
					+ " )"
					;
			if (alGeneIdsToExclude != null && ! alGeneIdsToExclude.isEmpty()) {
				command += " AND gene_id NOT IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alGeneIdsToExclude)
					+ " )"
					;
			}
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			while (rs.next()) {
				int elementIdIT = rs.getInt("element_id");
				int geneIdIT = rs.getInt("gene_id");
				if (hmToReturn.containsKey(elementIdIT)) {
					HashSet<Integer> hsGeneIdIT = hmToReturn.get(elementIdIT);
					hsGeneIdIT.add(geneIdIT);
				} else {
					HashSet<Integer> hsGeneIdIT = new HashSet<>();
					hsGeneIdIT.add(geneIdIT);
					hmToReturn.put(elementIdIT, hsGeneIdIT);
				}
			}// while
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return hmToReturn;
		
	}

	static public ArrayList<ArrayList<Integer>> getAlAlGeneIdWithElementIdAndAlGenomicBoundaries(
			Connection conn
			, ArrayList<Integer> listElementIdsThenStartPbthenStopPBLooped
			, boolean isAnd
			, boolean grabCDSWithAtLeastOnePbInThisStartStopLocus
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getAlAlGeneIdWithElementIdAndAlGenomicBoundaries"
				+ " ; listElementIdsThenStartPbthenStopPBLooped="+ ( (listElementIdsThenStartPbthenStopPBLooped != null ) ? listElementIdsThenStartPbthenStopPBLooped.toString() : "NULL" )
				+ " ; isAnd="+isAnd
				+ " ; grabCDSWithAtLeastOnePbInThisStartStopLocus="+grabCDSWithAtLeastOnePbInThisStartStopLocus
				;
		
		if (listElementIdsThenStartPbthenStopPBLooped == null || listElementIdsThenStartPbthenStopPBLooped.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("listElementIdsThenStartPbthenStopPBLooped.isEmpty()"));
		}
		
		String startComparator = "start >=";
		String stopComparator = "stop <=";
		if ( grabCDSWithAtLeastOnePbInThisStartStopLocus ) {
			startComparator = "stop >";
			stopComparator = "start <";
		}

		ArrayList<ArrayList<Integer>> alAlLGIdToReturn = new ArrayList<ArrayList<Integer>>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			//New
			ArrayList<String> alConstructedWhereClauseByElement = new ArrayList<String>();
			boolean continueSameElement = false;
			String constructedWhereClauseByElementIT = "";
			int elementIdIT = -1;

			for (int i = 0; i < listElementIdsThenStartPbthenStopPBLooped.size(); i++) {

				if(listElementIdsThenStartPbthenStopPBLooped.get(i) == elementIdIT){
					//same element
					continueSameElement = true;
				}else{
					//new element
					continueSameElement = false;
				}
				elementIdIT = listElementIdsThenStartPbthenStopPBLooped.get(i);
				int maxStop = QueriesTableElements.getSizeElementWithElementId(conn, elementIdIT);
				int startIT = -1;
				if (i + 1 < listElementIdsThenStartPbthenStopPBLooped.size()) {
					startIT = listElementIdsThenStartPbthenStopPBLooped
							.get(i + 1);
					if (maxStop > 0) {
						if (startIT < 0) {
							startIT = 0;
						}else if(startIT > maxStop){
							i++;
							i++;
							continue;
						}
					}
				} else {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("could not determine start"));
				}
				int stopIT = -1;
				if (i + 2 < listElementIdsThenStartPbthenStopPBLooped.size()) {
					stopIT = listElementIdsThenStartPbthenStopPBLooped
							.get(i + 2);
					if (maxStop > 0) {
						if (stopIT > maxStop) {
							stopIT = maxStop;
						}else if(stopIT < 0){

							i++;
							i++;
							continue;
						}
					}
					if (startIT >= stopIT) {
						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("start "
								+ startIT + " must be lower than stop "
								+ stopIT));
					}
				} else {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("could not determine stop"));
				}

				if (continueSameElement) {
					if (isAnd) {
						constructedWhereClauseByElementIT += " AND ( "+startComparator+" " + startIT + " AND  "+stopComparator+" " + stopIT + " )";
						int currSizeAlConstructedWhereClauseByElement = alConstructedWhereClauseByElement.size();
						alConstructedWhereClauseByElement.set(currSizeAlConstructedWhereClauseByElement-1, constructedWhereClauseByElementIT);
					} else {
						constructedWhereClauseByElementIT += " OR ( "+startComparator+" " + startIT + " AND  "+stopComparator+" " + stopIT + " )";
						int currSizeAlConstructedWhereClauseByElement = alConstructedWhereClauseByElement.size();
						alConstructedWhereClauseByElement.set(currSizeAlConstructedWhereClauseByElement-1, constructedWhereClauseByElementIT);
					}
				}else{
					constructedWhereClauseByElementIT = " element_id = "+elementIdIT+" AND ( "+startComparator+" " + startIT + " AND  "+stopComparator+" " + stopIT + " )";
					alConstructedWhereClauseByElement.add(constructedWhereClauseByElementIT);
				}

				i++;
				i++;

			}
			for(String whereClauseByElementIT : alConstructedWhereClauseByElement){
				String command = "SELECT gene_id FROM genes WHERE "+whereClauseByElementIT;

				if (rs != null){
					rs.close();
				}
				

				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

				// int count = 0;
				ArrayList<Integer> alGeneIdsIT = new ArrayList<Integer>();
				while (rs.next()) {
					alGeneIdsIT.add(rs.getInt("gene_id"));
				}// while

				alAlLGIdToReturn.add(alGeneIdsIT);

			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return alAlLGIdToReturn;

	}

	

	public static ArrayList<Integer> getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
			Connection conn
			, ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent
			, ArrayList<Integer> listGeneIdsToExclude // set to null or empty if none
			, boolean grabCDSWithAtLeastOnePbInThisStartStopLocus
			//, boolean ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
			, int ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch // -1 if NA
			, boolean orderCDSByStartAsc
			, boolean shouldBePresentInTable
			) throws Exception {
		String methodNameToReport = "QueriesTableGenes getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude"
				+ " ; listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent="+ ( (listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent != null ) ? listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.toString() : "NULL" )
				+ " ; listGeneIdsToExclude="+ ( (listGeneIdsToExclude != null ) ? listGeneIdsToExclude.toString() : "NULL" )
				+ " ; grabCDSWithAtLeastOnePbInThisStartStopLocus="+grabCDSWithAtLeastOnePbInThisStartStopLocus
				//+ " ; ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch="+ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
				+ " ; ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch="+ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch
				+ " ; orderCDSByStartAsc="+orderCDSByStartAsc
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		//System.err.println(methodNameToReport);
		
		// check valid input
		//no
		//mandatory args
		if (listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent != null && ! listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.isEmpty()) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent != null && ! listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.isEmpty()")
					);
		}
		
		
		ArrayList<Integer> alToReturn = new ArrayList<>();

		// build command
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//					chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("genes");

		String startComparator = "start >=";
		String stopComparator = "stop <=";
		if ( grabCDSWithAtLeastOnePbInThisStartStopLocus ) {
			if (ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch > 0) {
				startComparator = "( stop - "+ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch+" ) >";
				stopComparator = "( start + "+ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch+" ) <";
			} else {
				startComparator = "stop >";
				stopComparator = "start <";
			}
		}

		String whereClause = "WHERE ";
		boolean firstIter = true;
		whereClause += " (";
		HashSet<Integer> loonElementIds = new HashSet<>();
		for (int i = 0; i < listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent
				.size(); i += 3) {
			Integer elementIdIT = listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i);
			if (elementIdIT == null || elementIdIT < 0) {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("elementIdIT == null || elementIdIT < 0 at index "+i));
			}
			String innerWhereClause = "";
			if(listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.size() > (i+1)
					&& listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+1) != null
					&& listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+1) > 0
					){
				innerWhereClause += " AND "+startComparator+" " + listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+1);
			}
			if(listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.size() > (i+2)
					&& listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+2) != null
					&& listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+2) < Integer.MAX_VALUE
					){
				innerWhereClause += " AND "+stopComparator+" " + listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+2);
			}
			
			if (innerWhereClause.isEmpty()) {
				loonElementIds.add(elementIdIT);
			} else {
				if (firstIter) {
					firstIter = false;
				} else {
					whereClause += " OR ";
				}
				whereClause += " ( element_id = " + elementIdIT;
				whereClause += innerWhereClause;
				whereClause += " )";
			}
			//was but was getting gene not stricly within boundaries, not good
//			if(listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.size() > (i+1)
//					&& listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+1) != null){
//				whereClause += " AND stop > " + listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+1);
//			}
//			if(listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.size() > (i+2)
//					&& listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+2) != null){
//				whereClause += " AND start < " + listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i+2);
//			}


		}
		if ( ! loonElementIds.isEmpty()) {
			if (firstIter) {
				firstIter = false;
			} else {
				whereClause += " OR ";
			}
			whereClause += " ( element_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(loonElementIds)
					+ ") )";
		}
		
		
		whereClause += " )";
		if (listGeneIdsToExclude != null && ! listGeneIdsToExclude.isEmpty()) {
			whereClause += " AND ( gene_id NOT IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(listGeneIdsToExclude)
					+ ") )";
		}
		if (orderCDSByStartAsc) {
			whereClause += " ORDER BY start ASC";
		}
		
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);

		//System.err.println(objSQLCommandIT.getSQLCommand());
		
		//run command
//		alToReturn.addAll(UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_alInt(
//				conn, 
//				"gene_id",
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport));
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				int geneIdIT = rs.getInt("gene_id");
				alToReturn.add(geneIdIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (shouldBePresentInTable && alToReturn.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("shouldBePresentInTable && alToReturn.isEmpty()"));
		}
		
		return alToReturn;
		
	}
	


	private static HashMap<Integer, ArrayList<Integer>> getHmElementId2alGeneIds_withAlElementIdsAndProtSizeAlHmGreaterThan2LowerThan(
			Connection conn
			, HashSet<Integer> hsElementIds
			, ArrayList<Map<Integer,Integer>> alMapSingletonGreaterThan2LowerThan
			, boolean isAnd
			, boolean checkMandatoryArgs
			) throws Exception {
		

		String methodNameToReport = "QueriesTableGenes getHmElementId2alGeneIds_withAlElementIdsAndProtSizeAlHmGreaterThan2LowerThan"
				+ " ; hsElementIds="+ ( (hsElementIds != null ) ? hsElementIds.toString() : "NULL" )
				//+ " ; alHmStartPb2StopPb="+ ( (alHmStartPb2StopPb != null ) ? alHmStartPb2StopPb.toString() : "NULL" )
				+ " ; alHmStartPb2StopPb="+ alMapSingletonGreaterThan2LowerThan.stream()
					    .map(map -> map.toString())
					    .collect(Collectors.toList())
						.toString()
				+ " ; isAnd="+isAnd
				+ " ; checkMandatoryArgs="+checkMandatoryArgs
				;

		if (checkMandatoryArgs) {
			if (hsElementIds == null || hsElementIds.isEmpty()) {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("alElementIds == null OR alElementIds.isEmpty()"));
			}
		}

		HashMap<Integer, ArrayList<Integer>> hmToReturn = new HashMap<>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			String command = "SELECT element_id, gene_id";
			command += " FROM genes";
			command += " WHERE";
			command += " element_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsElementIds)
					+ ")";
			String columnProtSizeToQuery = "";
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				columnProtSizeToQuery = "length_residues";
			}else {
//				command = "SELECT genes.element_id, genes.gene_id";
//				command += " FROM genes, micado.prot_feat";
//				command += " WHERE genes.accession = micado.prot_feat.accession"
//						+ " AND genes.feature_id = micado.prot_feat.code_feat"
//						;
//				command += " AND genes.element_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alElementIds)
//						+ ")";
				columnProtSizeToQuery = "length(residues)";
			}

			
			boolean firstIter = true;
			
			String buildedWhereClauseOnProtSize = "";
			
			if (alMapSingletonGreaterThan2LowerThan != null && ! alMapSingletonGreaterThan2LowerThan.isEmpty() ) {
				for (Map<Integer,Integer> mapSingletonGreaterThan2LowerThanIT : alMapSingletonGreaterThan2LowerThan) {
					for (Map.Entry<Integer, Integer> entrySingletonGreaterThan2 : mapSingletonGreaterThan2LowerThanIT.entrySet()) {
						Integer greaterThan = entrySingletonGreaterThan2.getKey();
						Integer lowerThan = entrySingletonGreaterThan2.getValue();
						if (greaterThan > 0 || lowerThan < Integer.MAX_VALUE) {
							if (firstIter) {
								firstIter = false;
							} else {
								if (isAnd) {
									buildedWhereClauseOnProtSize += " AND";
								} else {
									buildedWhereClauseOnProtSize += " OR";
								}
							}
							buildedWhereClauseOnProtSize += " ( ";
							if (greaterThan > 0) {
								buildedWhereClauseOnProtSize += " "+columnProtSizeToQuery+" >= " + greaterThan;
							}
							if (lowerThan < Integer.MAX_VALUE) {
								if (greaterThan > 0) {
									buildedWhereClauseOnProtSize += " AND ";
								}
								buildedWhereClauseOnProtSize += " "+columnProtSizeToQuery+" <= " + lowerThan ;
							}						
							buildedWhereClauseOnProtSize += " )";
							break; // singleton Map
						}
					}
				}
			}
			if ( ! buildedWhereClauseOnProtSize.isEmpty()) {
				command += " AND ( "
						+ buildedWhereClauseOnProtSize
						+ " )";
			}

			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			while (rs.next()) {
				int elementIdIT = rs.getInt("element_id");
				int geneIdIT = rs.getInt("gene_id");
				if (hmToReturn.containsKey(elementIdIT)) {
					ArrayList<Integer> alGeneIdsIT = hmToReturn.get(elementIdIT);
					alGeneIdsIT.add(geneIdIT);
				} else {
					ArrayList<Integer> alGeneIdsIT = new ArrayList<>();
					alGeneIdsIT.add(geneIdIT);
					hmToReturn.put(elementIdIT, alGeneIdsIT);
				}
			}// while

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return hmToReturn;
		
	}
	
	
	//OLD
//	static public ArrayList<ArrayList<Integer>> getAlGeneIdWithAlElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped(
//			Connection conn,
//			ArrayList<Integer> alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped,
//			boolean isAnd
//			) throws Exception {
//
//		String methodNameToReport = "QueriesTableGenes getAlGeneIdWithAlElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped"
//				+ " ; alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped="+ ( (alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped != null ) ? alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped.toString() : "NULL" )
//				;
//		
//		//args to check
//		//no
//		//mandatory args
//		if ( alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped != null && ! alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped.isEmpty() ) {
//			//ok
//		} else {
//			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("NOT alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped != null && ! alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped.isEmpty()"));
//		}
//
//		ArrayList<ArrayList<Integer>> alAlLGIdToReturn = new ArrayList<ArrayList<Integer>>();
//
//		Statement statement = null;
//		ResultSet rs = null;
//		boolean closeConn = false;
//
//		try {
//			if (conn == null) {
//				conn = DatabaseConf.getConnection_db();
//				closeConn = true;
//			}
//
//			statement = conn.createStatement();
//			
//			ArrayList<String> alConstructedWhereClauseByElement = new ArrayList<String>();
//			boolean continueSameElement = false;
//			String constructedWhereClauseByElementIT = "";
//			int elementIdIT = -1;
//
//
//			for (int i = 0; i < alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped.size(); i++) {
//				
//				if(alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped.get(i) == elementIdIT){
//					//same element
//					continueSameElement = true;
//				}else{
//					//new element
//					continueSameElement = false;
//				}
//				elementIdIT = alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped.get(i);
//				int minProtSize = -1;
//				if (i + 1 < alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped.size()) {
//					minProtSize = alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped
//							.get(i + 1);
//					if (minProtSize < 0) {
//						minProtSize = 0;
//					}
//				} else {
//					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("could not determine greater than"));
//				}
//				
//				int maxProtSize = -1;
//				if (i + 2 < alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped.size()) {
//					maxProtSize = alElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped.get(i + 2);
//					if (minProtSize >= maxProtSize) {
//						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("greater than "
//										+ minProtSize + " must be lower than lower than "
//										+ maxProtSize));
//					}
//				} else {
//					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("could not determine lower than"));
//				}
//
//				if (continueSameElement) {
//					if (isAnd) {
//						if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
//							constructedWhereClauseByElementIT += " AND ( micado.prot_feat.length >= " + minProtSize + " AND micado.prot_feat.length <= " + maxProtSize + " )";
//						} else {
//							constructedWhereClauseByElementIT += " AND ( character_length(genes.residues) >= " + minProtSize + " AND character_length(genes.residues) <= " + maxProtSize + " )";
//						}
//						int currSizeAlConstructedWhereClauseByElement = alConstructedWhereClauseByElement.size();
//						alConstructedWhereClauseByElement.set(currSizeAlConstructedWhereClauseByElement-1, constructedWhereClauseByElementIT);
//					} else {
//						if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
//							constructedWhereClauseByElementIT += " AND ( micado.prot_feat.length >= " + minProtSize + " AND micado.prot_feat.length <= " + maxProtSize + " )";
//						} else {
//							constructedWhereClauseByElementIT += " OR ( character_length(genes.residues) >= " + minProtSize + " AND character_length(genes.residues) <= " + maxProtSize + " )";
//						}
//						int currSizeAlConstructedWhereClauseByElement = alConstructedWhereClauseByElement.size();
//						alConstructedWhereClauseByElement.set(currSizeAlConstructedWhereClauseByElement-1, constructedWhereClauseByElementIT);
//					}
//				}else{
//					if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
//						constructedWhereClauseByElementIT = " genes.element_id = "+elementIdIT+" AND ( micado.prot_feat.length >= " + minProtSize + " AND micado.prot_feat.length <= " + maxProtSize + " )";
//
//					} else {
//						constructedWhereClauseByElementIT = " genes.element_id = "+elementIdIT+" AND ( character_length(genes.residues) >= " + minProtSize + " AND character_length(genes.residues) <= " + maxProtSize + " )";
//					}
//					alConstructedWhereClauseByElement.add(constructedWhereClauseByElementIT);
//				}
//
//				i++;
//				i++;
//
//			}
//			for(String whereClauseByElementIT : alConstructedWhereClauseByElement){
//				String command = "SELECT genes.gene_id";
//				if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
//					command += " FROM genes, micado.prot_feat";
//				}else {
//					command += " FROM genes";
//				}
//				if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
//					command += " WHERE "
//							+ whereClauseByElementIT
//							+ " AND genes.accession = micado.prot_feat.accession"
//							+ " AND genes.feature_id = micado.prot_feat.code_feat"
//							;
//				} else {
//					command += " WHERE " + whereClauseByElementIT;
//				}
//				
//				if (rs != null){
//					rs.close();
//				}
//				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
//				// int count = 0;
//				ArrayList<Integer> alGeneIdsIT = new ArrayList<Integer>();
//				while (rs.next()) {
//					alGeneIdsIT.add(rs.getInt("gene_id"));
//				}// while
//				alAlLGIdToReturn.add(alGeneIdsIT);
//			}
//			
//		} catch (Exception ex) {
//			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
//		} finally {
//			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
//			if (closeConn) {
//				DatabaseConf.freeConnection(conn, methodNameToReport);
//			}
//		}// try
//		return alAlLGIdToReturn;
//		
//	}
	

	//tested
	public static int getOrganismIdWithElementIdAndCodeFeat (Connection conn
			, int elementId
			, int codeFeat
			, boolean shouldBePresentInTable
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getOrganismIdWithElementIdAndCodeFeat"
				+ " ; elementId="+elementId
				+ " ; codeFeat="+codeFeat
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		int intToReturn = -1;
		
		// check valid input
		//no

		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("organism_id", "organism_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("genes");
		String whereClause = "WHERE element_id = " + elementId
				+ " AND feature_id  = " + codeFeat;
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		
		//run command
//		ObjAssociationAsMap oaamReturned = UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//				conn, 
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			if (rs.next()) {
				intToReturn = rs.getInt("organism_id");
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("multiple rows returned"));
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if (shouldBePresentInTable && intToReturn == -1) {
			UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("shouldBePresentInTable but no row returned"));
		}
		
//		if(oaamReturned.getRsColumn2Int().get("organism_id") != null){
//			return 	oaamReturned.getRsColumn2Int().get("organism_id");
//		} else {
//			return -1;
//		}
		return intToReturn;
		

	}
		
	// utilities methods
	static public boolean fillAlStringDbxrefWithDbxrefQualifiers(
			ArrayList<String> alStringDbxrefQualifiers, ResultSet rsIn) throws Exception {
		if (rsIn.getString("type_qual").compareTo("db_xref") == 0) {
			alStringDbxrefQualifiers.add(rsIn.getString("qualifier"));
			return true;
		} else {
			return false;
		}
	}

	// utilities methods
	static public boolean fillInThisGeneItemWithStructuredQualifiersExceptLocusTagAndName(
			GeneItem geneToDWIn, ResultSet rsIn) throws Exception {

//		if (rsIn.getString("type_qual").compareTo("gene_name") == 0) {
//
//			if (geneToDWIn.getNameAsRawString() != null) {
//				// System.out.println("gene name != null : "+rsIn.getString("qualifier"));
//				if (geneToDWIn.getNameAsRawString().isEmpty()) {
//					geneToDWIn.setName(rsIn.getString("qualifier"));
//				}
//			} else {
//				// System.out.println("gene name == null : "+rsIn.getString("qualifier"));
//				geneToDWIn.setName(rsIn.getString("qualifier"));
//			}
//			return true;
//		} else 
		if (rsIn.getString("type_qual").compareTo("product") == 0) {
			geneToDWIn.addToListProduct(rsIn.getString("qualifier"));
			return true;
		} else if (rsIn.getString("type_qual").compareTo("function") == 0) {
			geneToDWIn.addToListFunction(rsIn.getString("qualifier"));
			return true;
		} else if (rsIn.getString("type_qual").compareTo("old_locus_tag") == 0) {
			geneToDWIn.addToListOldLocusTag(rsIn.getString("qualifier"));
			return true;
		} else if (rsIn.getString("type_qual").compareTo("EC_number") == 0) {
			// System.out.println("EC num");
			geneToDWIn.addToListECNumber(rsIn.getString("qualifier"));
			return true;
		} else if (rsIn.getString("type_qual").compareTo("biological_process") == 0) {
			// System.out.println("bio proc");
			geneToDWIn.addToListBiologicalProcess(rsIn.getString("qualifier"));
			return true;
		} else if (rsIn.getString("type_qual").compareTo("cellular_component") == 0) {
			// System.out.println("cell comp");
			geneToDWIn.addToListCellularComponent(rsIn.getString("qualifier"));
			return true;
//		} else if (rsIn.getString("type_qual").compareTo("locus_tag") == 0) {
//			// System.out.println("loc tag");
//			if (geneToDWIn.getLocusTagAsRawString() == null) {
//				geneToDWIn.setLocusTag(rsIn.getString("qualifier"));
//			} else {
//				UtilitiesMethodsServer.reportException("CallForInfoDBImpl fillInThisGeneItemWithStructuredQualifiers",new Exception("there is more than 1 locus_tag for the gene "
//								+ geneToDWIn
//										.getMostSignificantGeneNameAsStrippedText()
//								+ ", id = " + geneToDWIn.getGeneId()
//								+ ", already saved locus tag = "
//								+ geneToDWIn.getLocusTagAsRawString()
//								+ " ; attempted insertion locus tag = "
//								+ rsIn.getString("qualifier")));
//			}
//			return true;
		} else if (rsIn.getString("type_qual").compareTo("agmial_xref") == 0) {
			// System.out.println("agmial ref");
			geneToDWIn.setAgmialXref(rsIn.getString("qualifier"));
			return true;
		} else if (rsIn.getString("type_qual").compareTo("prot_id") == 0
				|| rsIn.getString("type_qual").compareTo("protein_id") == 0
				) {
			geneToDWIn.addToListProteinId(rsIn.getString("qualifier"));
			
//			String agmialProtId = rsIn.getString("qualifier");
//			Matcher m = agmialPatt.matcher(agmialProtId);
//			if (m.matches()) {
//				String agProject = m.group(1);
//				String agSourceId = m.group(2);
//				String agProtId = m.group(3);
//				String buildXRef = "http://genome.jouy.inra.fr/"+agProject+"-prot/reader/ProteinPanel?source_id="
//						+ agSourceId
//						+ "&protein_id=" + agProtId;
//				geneToDWIn.setAgmialXref(buildXRef);
//			}else{
//				//System.err.println("Error in fillInThisGeneItemWithStructuredQualifiers : String "+agmialProtId+" does not match the Agmial Regex");
//			}
			return true;
		} else if (rsIn.getString("type_qual").compareTo("note") == 0) {
			// System.out.println("bio proc");
			geneToDWIn.addToListNote(rsIn.getString("qualifier"));
			return true;
		} else if (rsIn.getString("type_qual").compareTo("inference") == 0) {
			// System.out.println("bio proc");
			geneToDWIn.addToListInference(rsIn.getString("qualifier"));
			return true;
		} else {
			return false;
		}

	}

	// utilities methods
	public static void fillInThisLightGeneItemWithBasicsExceptLocusTagAndName(LightGeneItem geneToDWIn,
			ResultSet rsIn) throws Exception {

		geneToDWIn.setGeneId(rsIn.getInt("gene_id"));
		geneToDWIn.setStrand(rsIn.getInt("strand"));
		geneToDWIn.setStart(rsIn.getInt("start"));
		geneToDWIn.setStop(rsIn.getInt("stop"));
		geneToDWIn.setType(rsIn.getString("type"));

		geneToDWIn.setOrganismId(rsIn.getInt("organism_id"));
		geneToDWIn.setElementId(rsIn.getInt("element_id"));
		// geneToDWIn.setAccession(rsIn.getString("accession"));

	}
	
	// utilities methods
	public static void fillInThisLightGeneItemWithLocusTagAndName(LightGeneItem geneToDWIn,
			ResultSet rsIn) throws Exception {

		int geneId = geneToDWIn.getGeneId();
		int elementId = geneToDWIn.getElementId();
		
		ArrayList<String> alToFill = new ArrayList<String>();
		String geneNameToReturn = geneToDWIn.getNameAsRawString();
		String locusTagToReturn = geneToDWIn.getLocusTagAsRawString();
		alToFill.add(geneNameToReturn);
		alToFill.add(locusTagToReturn);
		
		fillAlGeneName_LocusTag(rsIn, alToFill, geneId , "element_id "+elementId, -1, true);
		
		geneToDWIn.setName(alToFill.get(0));
		geneToDWIn.setLocusTag(alToFill.get(1));
		
	}

	// utilities methods
	public static boolean fillAlGeneName_LocusTag(
			ResultSet rs, ArrayList<String> alGeneName_LocusTag, int geneId, String accession, int codeFeat, boolean doReportException
			) throws Exception {
				
		if (rs.getString("type_qual").compareTo("gene_name") == 0) {
			if(doReportException){
				if(alGeneName_LocusTag.get(0) != null){
					if( ! alGeneName_LocusTag.get(0).isEmpty()){
						if(alGeneName_LocusTag.get(0).compareTo(rs.getString("qualifier"))!=0){
							//UtilitiesMethodsServer.reportException("CallForInfoDBImpl getAlGeneName_LocusTagWithRs", new Exception("Error in getAlGeneName_LocusTagWithRs : Multiple gene name for gene id = "+geneId+" ; accession = "+accession+" ; codeFeat = "+codeFeat+" -> "+alGeneName_LocusTag.get(0)+" and "+rs.getString("qualifier")));
							//TODO better management of multiple gene name
							alGeneName_LocusTag.set(0, alGeneName_LocusTag.get(0)+", "+rs.getString("qualifier"));
							return true;
						}
					}
				}
			}
			alGeneName_LocusTag.set(0, rs.getString("qualifier"));
			return true;
		} else if (rs.getString("type_qual").compareTo("gene") == 0) {
			if(doReportException){
				if(alGeneName_LocusTag.get(0) != null){
					if( ! alGeneName_LocusTag.get(0).isEmpty()){
						if(alGeneName_LocusTag.get(0).compareTo(rs.getString("qualifier"))!=0){
							//UtilitiesMethodsServer.reportException("CallForInfoDBImpl getAlGeneName_LocusTagWithRs", new Exception("Error in getAlGeneName_LocusTagWithRs : Multiple gene name for gene id = "+geneId+" ; accession = "+accession+" ; codeFeat = "+codeFeat+" -> "+alGeneName_LocusTag.get(0)+" and "+rs.getString("qualifier")));
							//TODO better management of multiple gene name
							alGeneName_LocusTag.set(0, alGeneName_LocusTag.get(0)+", "+rs.getString("qualifier"));
							return true;
						}
					}
				}
			}
			alGeneName_LocusTag.set(0, rs.getString("qualifier"));
			return true;
		} else if (rs.getString("type_qual").compareTo("locus_tag") == 0) {
			if(doReportException){
				if(alGeneName_LocusTag.get(1) != null){
					if( ! alGeneName_LocusTag.get(1).isEmpty()){
						if(alGeneName_LocusTag.get(1).compareTo(rs.getString("qualifier"))!=0){
							UtilitiesMethodsServer.reportException("CallForInfoDBImpl getAlGeneName_LocusTagWithRs", 
									new Exception("Error in getAlGeneName_LocusTagWithRs : Multiple locus tag for gene id = "+geneId+" ; accession = "+accession+" ; codeFeat = "+codeFeat+" -> "+alGeneName_LocusTag.get(1)+" and "+rs.getString("qualifier"))
							);
						}
					}
				}
			}
			alGeneName_LocusTag.set(1, rs.getString("qualifier"));
			return true;
		}
		return false;
		
	}

	
	public static ArrayList<Integer> getAlStartStopElementIdWithGeneId(
			Connection conn
			, int geneId
			) throws Exception {
		
		String methodNameToReport = "QueriesTableGenes getAlStartStopElementIdWithGeneId"
				+ " ; geneId="+geneId
				;
		
		ArrayList<Integer> alToReturn = new ArrayList<>();
		alToReturn.add(-1);
		alToReturn.add(-1);
		alToReturn.add(-1);
		
		//args to check
		//no
		//mandatory args
		if (geneId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("geneId <= 0")
					);
		}

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			String command = "select start, stop, element_id from genes where gene_id = "
					+ geneId;

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			if (rs.next()) {
				alToReturn.set(0, rs.getInt("start"));
				alToReturn.set(1, rs.getInt("stop"));
				alToReturn.set(2, rs.getInt("element_id"));
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("no count returned for gene_id = "
								+ geneId));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try

		return alToReturn;
		
		
	}

	
	
	
	public static int getNumberGenesWithElementId_optionalStartStopBoundaries(
			Connection conn
			, int elementId
			, int pbStartInElement // set to -1 if not applicable
			, int pbStopInElement // set to -1 if not applicable
			) throws Exception {

		String methodNameToReport = "QueriesTableGenes getNumberGenesWithElementId_optionalStartStopBoundaries"
				+ " ; elementId="+elementId
				+ " ; pbStartInElement="+pbStartInElement
				+ " ; pbStopInElement="+pbStopInElement
				;
		
		int intToReturn = -1;
		
		//args to check
		//no
		//mandatory args
		if (elementId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("elementId <= 0")
					);
		}
		
		if (pbStartInElement >= 0 && pbStopInElement >= 0 && pbStartInElement >= pbStopInElement) {
//			UtilitiesMethodsServer.reportException(
//					methodNameToReport
//					, new Exception("pbStartInElement >= 0 && pbStopInElement > 0 && pbStartInElement >= pbStopInElement")
//					);
			return 0; // Overlap region between two homologous loci make such queries
		}
		
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			String command = "select count(gene_id) as counted_genes from genes where element_id = "
					+ elementId;
			//if (pbStartInElement >= 0 && pbStopInElement > 0) {
			//	command += " AND start < "
			//	+ pbStopInElement
			//	+ " AND stop > " + pbStartInElement;
			//}
			command += " AND start >= "
				+ pbStartInElement
				+ " AND stop <= " + pbStopInElement;
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			if (rs.next()) {
				intToReturn = rs.getInt("counted_genes");
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("no count returned for element id = "
								+ elementId));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);

		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try

		if (intToReturn >= 0) {
			return intToReturn;
		} else {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("number gene inferior at 0 for elementId = "+ elementId));
			return -1;
		}

	}


	public static ArrayList<TransStartStopGeneInfo> getAlTransStartStopGeneInfoWithElementId_optionalStartStopBoundaries(
			Connection conn
			, int elementId
			, int pbStartInElement
			, int pbStopInElement) throws Exception {

		String methodNameToReport = "QueriesTableGenes getAlTransStartStopGeneInfoWithElementId_optionalStartStopBoundaries"
				+ " ; elementId="+elementId
				+ " ; pbStartInElement="+pbStartInElement
				+ " ; pbStopInElement="+pbStopInElement
				;
		
		ArrayList<TransStartStopGeneInfo> alToReturn = new ArrayList<TransStartStopGeneInfo>();
		
		//args to check
		//no
		//mandatory args
		if (elementId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("elementId <= 0")
					);
		}
		if (pbStartInElement >= 0 && pbStopInElement >= 0 && pbStartInElement >= pbStopInElement) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("pbStartInElement >= 0 && pbStopInElement >= 0 && pbStartInElement >= pbStopInElement")
					);
		}

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			String command = "SELECT gene_id, start, stop FROM genes WHERE element_id ="
					+ elementId;
			if (pbStartInElement >= 0 && pbStopInElement >= 0) {
				command += " AND start < "
						+ pbStopInElement
						+ " AND stop > " + pbStartInElement;
			}

			// System.err.println(command);
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				TransStartStopGeneInfo newTSSGI = new TransStartStopGeneInfo();
				newTSSGI.setOrigamiGeneId(rs.getInt("gene_id"));
				newTSSGI.setStart(rs.getInt("start"));
				newTSSGI.setStop(rs.getInt("stop"));
				newTSSGI.setOrigamiElementId(elementId);
				alToReturn.add(newTSSGI);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return alToReturn;	
		
	}
	

	public static ArrayList<TransStartStopGeneInfo> getAlTransientStartStopGeneInfoWithListGeneIds(
			Connection conn
			, ArrayList<Integer> listSGeneIds
			) throws Exception {


		String methodNameToReport = "QueriesTableGenes getAlTransientStartStopGeneInfoWithListGeneIds"
				+ " ; listSGeneIds="+( (listSGeneIds != null ) ? listSGeneIds.toString() : "NULL" )
				;
		
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		ArrayList<TransStartStopGeneInfo> alToReturn = new ArrayList<TransStartStopGeneInfo>();

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			statement = conn.createStatement();

			String command = "SELECT gene_id, start, stop, element_id FROM genes WHERE gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(listSGeneIds)
					+ ")";
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);
			
			
			while (rs.next()) {
				TransStartStopGeneInfo newTSSGI = new TransStartStopGeneInfo();
				newTSSGI.setOrigamiGeneId(rs.getInt("gene_id"));
				newTSSGI.setStart(rs.getInt("start"));
				newTSSGI.setStop(rs.getInt("stop"));
				newTSSGI.setOrigamiElementId(rs.getInt("element_id"));
				alToReturn.add(newTSSGI);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);

		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try

		return alToReturn;

	}
	
	
	public static LightGeneItem getLightGeneItemWithGeneId(
			Connection conn, int geneId) throws Exception {
		
		String methodNameToReport = "QueriesTableGenes getLightGeneItemWithGeneId"
				+ " ; geneId="+geneId
				;
		
		ArrayList<Integer> alGeneIdsIT = new ArrayList<Integer>();
		alGeneIdsIT.add(geneId);
		ArrayList<LightGeneItem> alLgiIT = QueriesTableGenes.getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(conn, alGeneIdsIT, true);
		if(alLgiIT != null){
			if(!alLgiIT.isEmpty()){
				return alLgiIT.get(0);
			}else{
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("alLgiIT.isEmpty()"));
				return null;
			}
		}else{
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("alLgiIT is null"));
			return null;
		}
	}
	

	public static ArrayList<LightGeneItem> getAllLightGeneItemWithOrganismId(
			Connection conn, 
			int organismId
			) throws Exception {


		String methodNameToReport = "QueriesTableGenes getAllLightGeneItemWithOrganismId"
				+ " ; organismId="+ organismId
				;
		//args to check
		//no
		//mandatory args
		if (organismId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("organismId <= 0")
					);
		}
		
		ArrayList<LightGeneItem> arrayListLGI = new ArrayList<LightGeneItem>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			

			String command = "SELECT genes.gene_id, genes.organism_id, genes.element_id, genes.name, genes.strand, genes.start, genes.stop, elements.type, micado.features.accession, micado.features.code_feat"
					+ " FROM genes, elements, micado.features"
					+ " WHERE elements.element_id=genes.element_id AND elements.accession = micado.features.accession AND genes.feature_id = micado.features.code_feat"
					+ " AND genes.organism_id = "
					+ organismId
					+ " ORDER BY genes.element_id ASC, genes.start ASC";
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			int count = 0;
			while (rs.next()) {
				count++;
				if (count > QueriesTableGenes.MAX_GENE_ITEM_RETURNED_ALLOWED_FOR_SEARCH) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Your query returned a large number of gene, please narrow your search down : "+command));
				}
				LightGeneItem geneToDW = new LightGeneItem();
				// fillInThisGeneItemWithBasicsAndLocusTag(geneToDW, rs);
				QueriesTableGenes.fillInThisLightGeneItemWithBasicsExceptLocusTagAndName(geneToDW, rs);
				//ArrayList<String> alGeneNameAndLocusTag = getGeneNameAndLocusTagWithAccessionAndCodeFeat(rs.getString("accession"), rs.getInt("code_feat"), conn);
				ArrayList<String> alGeneNameAndLocusTag = QueriesTableMicadoQualifiers.getGeneNameAndLocusTagWithAccessionAndFeatureId_firstOnly(conn, rs.getString("accession"), rs.getInt("code_feat"));
				if (alGeneNameAndLocusTag.get(1).length() > 0) {
					geneToDW.setLocusTag(alGeneNameAndLocusTag.get(1));
				}
				if (alGeneNameAndLocusTag.get(0).length() > 0) {
					geneToDW.setName(alGeneNameAndLocusTag.get(0));
				}
				arrayListLGI.add(geneToDW);

			}// while

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}
		
		return arrayListLGI;
		
	}
	
	
	public static ArrayList<LightGeneItem> getAllLightGeneItemWithElementId(
			Connection conn, 
			int elementId
			) throws Exception {


		String methodNameToReport = "QueriesTableGenes getAllLightGeneItemWithElementId"
				+ " ; elementId="+ elementId
				;
		//args to check
		//no
		//mandatory args
		if (elementId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("elementId <= 0")
					);
		}
		
		ArrayList<LightGeneItem> arrayListLGI = new ArrayList<LightGeneItem>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			

			String command = "SELECT genes.gene_id, genes.organism_id, genes.element_id, genes.name, genes.strand, genes.start, genes.stop, elements.type, micado.features.accession, micado.features.code_feat"
					+ " FROM genes, elements, micado.features"
					+ " WHERE elements.element_id=genes.element_id AND elements.accession = micado.features.accession AND genes.feature_id = micado.features.code_feat"
					+ " AND genes.element_id = "
					+ elementId
					+ " ORDER BY genes.start";
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			int count = 0;
			while (rs.next()) {
				count++;
				if (count > QueriesTableGenes.MAX_GENE_ITEM_RETURNED_ALLOWED_FOR_SEARCH) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Your query returned a large number of gene, please narrow your search down : "+command));
				}
				LightGeneItem geneToDW = new LightGeneItem();
				// fillInThisGeneItemWithBasicsAndLocusTag(geneToDW, rs);
				QueriesTableGenes.fillInThisLightGeneItemWithBasicsExceptLocusTagAndName(geneToDW, rs);
				//ArrayList<String> alGeneNameAndLocusTag = getGeneNameAndLocusTagWithAccessionAndCodeFeat(rs.getString("accession"), rs.getInt("code_feat"), conn);
				ArrayList<String> alGeneNameAndLocusTag = QueriesTableMicadoQualifiers.getGeneNameAndLocusTagWithAccessionAndFeatureId_firstOnly(conn, rs.getString("accession"), rs.getInt("code_feat"));
				if (alGeneNameAndLocusTag.get(1).length() > 0) {
					geneToDW.setLocusTag(alGeneNameAndLocusTag.get(1));
				}
				if (alGeneNameAndLocusTag.get(0).length() > 0) {
					geneToDW.setName(alGeneNameAndLocusTag.get(0));
				}
				arrayListLGI.add(geneToDW);

			}// while

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}
		
		return arrayListLGI;
		
	}
	
	
	//tested
	public static <T extends Collection<Integer>> ArrayList<LightGeneItem> getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(
			Connection conn, 
			T collectionGeneIds,
			boolean shouldBePresentInTable
			) throws Exception {
		
		String methodNameToReport = "QueriesTableGenes getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId"
				+ " ; collectionGeneIds="+ ( (collectionGeneIds != null ) ? collectionGeneIds.toString() : "NULL" )
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		if (collectionGeneIds.isEmpty()) {
			return new ArrayList<LightGeneItem>();
		}

		ArrayList<LightGeneItem> arrayListLGI = new ArrayList<>();
		HashSet<Integer> alGeneIdPresentInDb = new HashSet<>();
		
		try {
			
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			String command = "SELECT genes.gene_id, genes.organism_id, genes.element_id, genes.name, genes.strand, genes.start, genes.stop,"
					+ " elements.type, micado.qualifiers.type_qual, micado.qualifiers.qualifier"
					// " FROM genes, elements, micado.qualifiers" +
					+ " FROM genes left join (elements left join micado.qualifiers on (elements.accession = micado.qualifiers.accession)) on (genes.feature_id = micado.qualifiers.code_feat)"
					+ " WHERE elements.element_id=genes.element_id"
					+ " AND genes.gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(collectionGeneIds)
					//+ listGEneIds.toString().replaceAll("[\\[\\]]", "")
					+ ")"
					+ " AND ("
					+ UtilitiesMethodsServer.getGeneNameAndLocusTagQualifiers()
					+ ")"
					+ " ORDER BY genes.gene_id";

			// System.out.println(command);

			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);


			int count = 0;
			int currGeneId = -1;
			while (rs.next()) {

				if (count > MAX_GENE_ITEM_RETURNED_ALLOWED_FOR_SEARCH) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Your query returned a large number of gene, please narrow your search down = "+command));
				}

				if (currGeneId == -1 || currGeneId != rs.getInt("gene_id")) {
					// new gene
					LightGeneItem geneToDW = new LightGeneItem();
					currGeneId = rs.getInt("gene_id");
					alGeneIdPresentInDb.add(currGeneId);
					count++;
					
					fillInThisLightGeneItemWithBasicsExceptLocusTagAndName(geneToDW, rs);
					fillInThisLightGeneItemWithLocusTagAndName(geneToDW, rs);
					arrayListLGI.add(geneToDW);
				} else {
					// same gene
					LightGeneItem geneToDW = arrayListLGI.get(arrayListLGI
							.size() - 1);
					fillInThisLightGeneItemWithLocusTagAndName(geneToDW, rs);
				}

			}// while
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}

		if(shouldBePresentInTable){
			alGeneIdPresentInDb.removeAll(collectionGeneIds);
			if( ! alGeneIdPresentInDb.isEmpty()){
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("The following gene id have not been found and should be present in the database : "+collectionGeneIds.toString()));
			}
		}
		
		
		return arrayListLGI;
		
	}
	


	public static ArrayList<LightGeneItem> getAllLightGeneItemWithListFiltersAndAlRefElementIds(
			Connection conn,
			ArrayList<FilterGeneItem> listFilters
			//, ArrayList<Integer> alRefOrganismIds
			, HashSet<Integer> hsElementIds
			) throws Exception {
		

		String methodNameToReport = "QueriesTableGenes getAllLightGeneItemWithListFiltersAndAlRefOrganismIdsAndOrAlRefElementIds"
				+ " ; listFilters="+ ( (listFilters != null ) ? listFilters.toString() : "NULL" )
				//+ " ; alRefOrganismIds="+ ( (alRefOrganismIds != null ) ? alRefOrganismIds.toString() : "NULL" )
				+ " ; hsElementIds="+ ( (hsElementIds != null ) ? hsElementIds.toString() : "NULL" )
				;

		//OLD
//	public static ArrayList<LightGeneItem> getAllLightGeneItemWithListFiltersAndRefEletId(
//				Connection conn,
//				ArrayList<FilterGeneItem> listFilters
//				, int selectedElementId
//				) throws Exception {
//			
//		String methodNameToReport = "QueriesTableGenes getAllLightGeneItemWithListFiltersAndRefEletId"
//					+ " ; listFilters="+ ( (listFilters != null ) ? listFilters.toString() : "NULL" )
//					+ " ; selectedElementId="+selectedElementId
//					;

		if (listFilters == null || listFilters.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("listFilters == null || listFilters.isEmpty() : "+listFilters));
		}
		if (hsElementIds == null || hsElementIds.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("hsElementIds == null || hsElementIds.isEmpty() : "+hsElementIds));
		}
		
		

		ArrayList<LightGeneItem> arrayListLGI = new ArrayList<LightGeneItem>();

		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
		
			ArrayList<ArrayList<Integer>> alAlGeneIdsAfterEachFilters = new ArrayList<ArrayList<Integer>>();

			// fill alAlGeneIdsToIntersectOrFusion
			for (int i = 0; i < listFilters.size(); i++) {
				FilterGeneItem fgiIT = listFilters.get(i);

//				System.err.println(fgiIT.getTypeFilterGeneBy());
//				System.err.println(fgiIT.getListSubFilter().toString());
//				System.err.println(fgiIT.getOperatorFilterGeneBy());
				
				if (fgiIT.getTypeFilterGeneBy().compareTo(
						EnumTypeFilterGeneBy.EC_number) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Function) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Biological_process) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Gene_name) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Cellular_component) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Notes_product) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.db_xref) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Evidence) == 0
				// ||
				// fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Annotated_as_pseudogene_ncRNA)==0
				) {

					ArrayList<String> alMatchRegexString = new ArrayList<String>();
					ArrayList<String> alTypeQualToMatch = new ArrayList<String>();

					boolean isAnd = true;
					boolean isNotNull = false;
					boolean getMirrorList = false;

					
					for (int j = 0; j < fgiIT.getListSubFilter().size(); j++) {
						String subFilterIT = fgiIT.getListSubFilter().get(j);
						if ((j + 1) % 2 != 0) {
							if (!subFilterIT.matches("^.*;.*$")) {
								if (subFilterIT.compareTo("IS_NOT_NULL") == 0) {
									isNotNull = true;
									getMirrorList = false;
									alMatchRegexString.add("whatever");
									break;
								} else if (subFilterIT.compareTo("IS_NULL") == 0) {
									isNotNull = true;
									getMirrorList = true;
									alMatchRegexString.add("whatever");
									break;
								} else {
									if (subFilterIT.contains("!~*")) {
										// match not
										getMirrorList = true;
										subFilterIT = subFilterIT.replaceFirst(
												"\\!\\~\\*", "\\~\\*");
									}
									alMatchRegexString.add(subFilterIT);
								}

							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("semi colon not autorized in string : "
												+ subFilterIT));
							}
							continue;
						}
						if (j == 1) {
							if (subFilterIT.compareTo("AND") == 0) {
								isAnd = true;
							} else if (subFilterIT.compareTo("OR") == 0) {
								isAnd = false;
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("neither AND or OR found : "
												+ subFilterIT));
							}
						}
					}

					boolean searchForGeneNameInTableGeneToo = false;
					if (fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.EC_number) == 0) {
						// match type_qual = 'EC_number'
						alTypeQualToMatch.add("EC_number");
					} else if (fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Function) == 0) {
						// match type_qual = 'function'
						// match type_qual = 'biological_process'
						alTypeQualToMatch.add("function");

					} else if (fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Biological_process) == 0) {
						alTypeQualToMatch.add("biological_process");
					} else if (fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Gene_name) == 0) {
						// match type_qual = 'locus_tag'
						// match type_qual = 'gene_name'
						// "gene"
						// "gene_id"
						// "gene_synonym"
						// "orf_name"
						// "protein_id"
						// "prot_id"
						searchForGeneNameInTableGeneToo = true;
						alTypeQualToMatch.add("gene");
						alTypeQualToMatch.add("gene_id");
						alTypeQualToMatch.add("gene_synonym");
						alTypeQualToMatch.add("orf_name");
						alTypeQualToMatch.add("protein_id");
						alTypeQualToMatch.add("prot_id");
						alTypeQualToMatch.add("locus_tag");
					} else if (fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Cellular_component) == 0) {
						// match type_qual = 'cellular_component'
						alTypeQualToMatch.add("cellular_component");
					} else if (fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Notes_product) == 0) {
						// match type_qual = 'product'
						// "inference"
						// "comments"
						// "note"
						alTypeQualToMatch.add("product");
						alTypeQualToMatch.add("inference");
						alTypeQualToMatch.add("comments");
						alTypeQualToMatch.add("note");
					} else if (fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.db_xref) == 0) {
						// match type_qual = 'db_xref'
						alTypeQualToMatch.add("db_xref");
					} else if (fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Evidence) == 0) {
						// match type_qual = 'evidence'
						alTypeQualToMatch.add("evidence");
					} else {
						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("unrecognized type qual : "
										+ fgiIT.getTypeFilterGeneBy()));
					}

					//alRefElementIdsFromAlOrgaIdsAndEletIdsSent
					//for(int selectedElementId : alRefElementIdsFromAlOrgaIdsAndEletIdsSent){
					alAlGeneIdsAfterEachFilters
						.add(
								getAlGeneIdWithAlElementIdAndAlTypeQualToMatchAndAlStringToMatch(
										conn
										, alMatchRegexString
										, isAnd
										, hsElementIds
										, alTypeQualToMatch
										, getMirrorList
										, isNotNull
										, searchForGeneNameInTableGeneToo
								)
							);
					//}

				} else if (fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Genomic_locations) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Lenght_protein) == 0
						) {
					ArrayList<Integer> alIntegerStartStop = new ArrayList<Integer>();
					boolean isAnd = true;
					for (int j = 0; j < fgiIT.getListSubFilter().size(); j++) {
						String subFilterIT = fgiIT.getListSubFilter().get(j);
						if ((j + 1) % 3 != 0) {
							// should be integer
							if (subFilterIT.matches("^\\d+$")) {
								alIntegerStartStop.add(Integer.parseInt(subFilterIT));
							} else {
								//max par default
								alIntegerStartStop.add(Integer.MAX_VALUE);
							}
							continue;
						}
						if (j == 2) {
							if (subFilterIT.compareTo("AND") == 0) {
								isAnd = true;
							} else if (subFilterIT.compareTo("OR") == 0) {
								isAnd = false;
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("isGenomicLocations and j%2 but neither AND or OR : " + subFilterIT));
							}
						}
					}
					
					
					ArrayList<Map<Integer,Integer>> alMapLowerBound2HigherBound = new ArrayList<>();
					for (int m = 0; m < alIntegerStartStop.size(); m++) {
						int lowerBoundIT = alIntegerStartStop.get(m);
						int higherBoundIT = -1;
						if ((m + 1) < alIntegerStartStop.size()) {
							higherBoundIT = alIntegerStartStop.get(m + 1);
							Map<Integer,Integer> lowerBound2HigherBoundIT = Collections.singletonMap(lowerBoundIT, higherBoundIT);
							alMapLowerBound2HigherBound.add(lowerBound2HigherBoundIT);
						} else {
							UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("(m+1) not < alIntegerStartStop.size(): "
											+ (m + 1)
											+ " not < "
											+ alIntegerStartStop.size()));
						}					
						m++; // double increment
					}
					
					
					if (fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Genomic_locations) == 0){
						//ArrayList<ArrayList<Integer>> alAlElementIdThenStartPbthenStopPB = new ArrayList<>();
						

						//OLD
//						ArrayList<ArrayList<Integer>> alAlGeneIdWithElementIdAndAlGenomicBoundaries = QueriesTableGenes.getAlAlGeneIdWithElementIdAndAlGenomicBoundaries(
//								conn,
//								listElementIdsThenStartPbthenStopPBLooped,
//								isAnd
//								//, sizeSelectedElement
//								);
						
						HashMap<Integer,ArrayList<Integer>> hmElementId2alGeneIds = getHmElementId2alGeneIds_withAlElementIdsAndAlHmStartPb2StopPb (
								conn
								, hsElementIds
								, alMapLowerBound2HigherBound
								, isAnd
								, false
								//, sizeSelectedElement
								);

//						for(ArrayList<Integer> alGeneIDIT : alAlGeneIdWithElementIdAndAlGenomicBoundaries){
//							alAlGeneIdsAfterEachFilters
//								.add(alGeneIDIT);
//						}
						ArrayList<Integer> alGeneIdsForFilterIT = new ArrayList<>();
						for(Map.Entry<Integer,ArrayList<Integer>> entryHmElementId2 : hmElementId2alGeneIds.entrySet()) {
							//Integer elementIdIT = entryHmElementId2.getKey();
							ArrayList<Integer> alGeneIdsIT = entryHmElementId2.getValue();
							alGeneIdsForFilterIT.addAll(alGeneIdsIT);
							//alAlGeneIdsAfterEachFilters.add(alGeneIdsIT);
						}
						alAlGeneIdsAfterEachFilters.add(alGeneIdsForFilterIT);
						
					} else if (fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Lenght_protein) == 0) {
						
//						//ArrayList<Integer> listElementIdsGreaterThanThenLowerThanITLooped = new ArrayList<Integer>();
//						HashMap<Integer,Integer> alHmGreaterThan2LowerThan = new HashMap<>();
//						for (int m = 0; m < alIntegerStartStop.size(); m++) {
//							int greaterThanIT = alIntegerStartStop.get(m);
//							int lowerThanIT = -1;
//							if ((m + 1) < alIntegerStartStop.size()) {
//								lowerThanIT = alIntegerStartStop.get(m + 1);
//								alHmGreaterThan2LowerThan.put(greaterThanIT, lowerThanIT);
//							} else {
//								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("(m+1) not < alIntegerStartStop.size(): "
//												+ (m + 1)
//												+ " not < "
//												+ alIntegerStartStop.size()));
//							}
////							listElementIdsGreaterThanThenLowerThanITLooped.add(selectedElementId);
////							listElementIdsGreaterThanThenLowerThanITLooped.add(greaterThanIT);
////							listElementIdsGreaterThanThenLowerThanITLooped.add(lowerThanIT);
//							m++; // double increment
//						}

//						ArrayList<ArrayList<Integer>> alAlGeneIdWithlistElementIdsGreaterThanThenLowerThanITLooped = QueriesTableGenes.getAlGeneIdWithAlElementIdsGreaterThanProtSizeThenLowerThanProtSizeLooped(
//								conn,
//								listElementIdsGreaterThanThenLowerThanITLooped,
//								isAnd
//								);
						HashMap<Integer,ArrayList<Integer>> hmElementId2alGeneIds = getHmElementId2alGeneIds_withAlElementIdsAndProtSizeAlHmGreaterThan2LowerThan (
										conn
										, hsElementIds
										, alMapLowerBound2HigherBound
										, isAnd
										, false
										//, sizeSelectedElement
										);
//						for(ArrayList<Integer> alGeneIDIT : alAlGeneIdWithlistElementIdsGreaterThanThenLowerThanITLooped){
//							alAlGeneIdsAfterEachFilters.add(alGeneIDIT);
//						}
						ArrayList<Integer> alGeneIdsForFilterIT = new ArrayList<>();
						for(Map.Entry<Integer,ArrayList<Integer>> entryHmElementId2 : hmElementId2alGeneIds.entrySet()) {
							//String matchRegexStringIT = entryHmMatchRegexString2.getKey();
							ArrayList<Integer> alGeneIdsIT = entryHmElementId2.getValue();
							alGeneIdsForFilterIT.addAll(alGeneIdsIT);
						}
						alAlGeneIdsAfterEachFilters.add(alGeneIdsForFilterIT);
					} else {
						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("fgiIT.getTypeFilterGeneBy not recognized : "+fgiIT.getTypeFilterGeneBy()));
					}
					
				} else if (fgiIT.getTypeFilterGeneBy().compareTo(
						EnumTypeFilterGeneBy.Gene_id) == 0) {
					if(fgiIT.getListSubFilter().size() == 2){
						String inOrNotIn = fgiIT.getListSubFilter().get(0);
						if(inOrNotIn.compareTo("IN") == 0){
							ArrayList<Integer> alGeneIDIT = new ArrayList<>();
							String stGeneIds = fgiIT.getListSubFilter().get(1);
							try {
								String[] listGeneIdsAsSt = stGeneIds.split(",");
								for(int x=0;x<listGeneIdsAsSt.length;x++){
									int geneIdIT = Integer.parseInt(listGeneIdsAsSt[x]);
									alGeneIDIT.add(geneIdIT);
								}
							} catch (Exception e) {
								UtilitiesMethodsServer.reportException(methodNameToReport,e);
							}
							alAlGeneIdsAfterEachFilters.add(alGeneIDIT);
						} else {
							UtilitiesMethodsServer.reportException(
									methodNameToReport,
									new Exception("inOrNotIn not supported : " + inOrNotIn));
						}
					} else {
						UtilitiesMethodsServer.reportException(
								methodNameToReport,
								new Exception("fgiIT.getListSubFilter().size() != 2 : "
								+ fgiIT.getListSubFilter().toString() + " ; size = "+fgiIT.getListSubFilter().size()));
					}
				} else if (fgiIT.getTypeFilterGeneBy().compareTo(
						EnumTypeFilterGeneBy.Element_id) == 0) {
					if(fgiIT.getListSubFilter().size() == 2){
						String inOrNotIn = fgiIT.getListSubFilter().get(0);
						if(inOrNotIn.compareTo("IN") == 0){
							ArrayList<Integer> alEltIDIT = new ArrayList<>();
							String stEltIds = fgiIT.getListSubFilter().get(1);
							try {
								String[] listEltIdsAsSt = stEltIds.split(",");
								for(int x=0;x<listEltIdsAsSt.length;x++){
									int eltIdIT = Integer.parseInt(listEltIdsAsSt[x]);
									alEltIDIT.add(eltIdIT);
									alEltIDIT.add(null); // start in element
									alEltIDIT.add(null); // stop in element
								}
							} catch (Exception e) {
								UtilitiesMethodsServer.reportException(methodNameToReport,e);
							}
							alAlGeneIdsAfterEachFilters.add(
									QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
											conn
											, alEltIDIT
											, null
											, false // grabCDSWithAtLeastOnePbInThisStartStopLocus
											//, false // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
											, -1
											, false // boolean orderCDSByStartAsc
											, false // shouldBePresentInTable
											)
									);
						} else {
							UtilitiesMethodsServer.reportException(
									methodNameToReport,
									new Exception("inOrNotIn not supported : " + inOrNotIn));
						}
					} else {
						UtilitiesMethodsServer.reportException(
								methodNameToReport,
								new Exception("fgiIT.getListSubFilter().size() != 2 : "
								+ fgiIT.getListSubFilter().toString()));
					}
				} else if (fgiIT.getTypeFilterGeneBy().compareTo(
						EnumTypeFilterGeneBy.Organism_id) == 0) {
					if(fgiIT.getListSubFilter().size() == 2){
						String inOrNotIn = fgiIT.getListSubFilter().get(0);
						if(inOrNotIn.compareTo("IN") == 0){
							ArrayList<Integer> alOrgaIDIT = new ArrayList<>();
							String stOrgaIds = fgiIT.getListSubFilter().get(1);
							try {
								String[] listOrgaIdsAsSt = stOrgaIds.split(",");
								for(int x=0;x<listOrgaIdsAsSt.length;x++){
									int orgaIdIT = Integer.parseInt(listOrgaIdsAsSt[x]);
									alOrgaIDIT.add(orgaIdIT);
								}
							} catch (Exception e) {
								UtilitiesMethodsServer.reportException(methodNameToReport,e);
							}
							alAlGeneIdsAfterEachFilters.add(
									QueriesTableGenes.getAlGenesIdsWithAlOrganismIds_optionalAlGeneIds(
											conn
											, alOrgaIDIT
											, null
											, false
											)
									);
						} else {
							UtilitiesMethodsServer.reportException(
									methodNameToReport,
									new Exception("inOrNotIn not supported : " + inOrNotIn));
						}
					} else {
						UtilitiesMethodsServer.reportException(
								methodNameToReport,
								new Exception("fgiIT.getListSubFilter().size() != 2 : "
								+ fgiIT.getListSubFilter().toString()));
					}

				} else if (fgiIT.getTypeFilterGeneBy().compareTo(
						EnumTypeFilterGeneBy.Presence_absence_homology) == 0) {

					ArrayList<String> alListOrgaIdAsString = new ArrayList<String>();
					ArrayList<String> alPresenceOrAbsence = new ArrayList<String>();

					boolean isAnd = true;
					for (int j = 0; j < fgiIT.getListSubFilter().size(); j++) {
						String subFilterIT = fgiIT.getListSubFilter().get(j);

						// System.out.println(subFilterIT);

						if (j % 3 == 0) {
							// Presence/absence
							if (!subFilterIT.matches("^.*;.*$")) {
								if (subFilterIT.compareTo("presence") == 0) {
									alPresenceOrAbsence.add("presence");
								} else if (subFilterIT.compareTo("absence") == 0) {
									alPresenceOrAbsence.add("absence");
								} else {
									UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("neither presence or absence : "
													+ subFilterIT));
								}
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("semi colon not autorized in string : "
												+ subFilterIT));
							}
							continue;
						}
						if ((j - 1) % 3 == 0) {
							// list orga id
							if (!subFilterIT.matches("^.*;.*$")) {
								alListOrgaIdAsString.add(subFilterIT);
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("semi colon not autorized in string : "
												+ subFilterIT));
							}
							continue;
						}
						if (j == 2) {
							if (subFilterIT.compareTo("AND") == 0) {
								isAnd = true;
							} else if (subFilterIT.compareTo("OR") == 0) {
								isAnd = false;
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("isGenomicLocations and j%2 but neither AND or OR : "
												+ subFilterIT));
							}
						}
					}
					
					//for(int selectedElementId : alRefElementIdsFromAlOrgaIdsAndEletIdsSent){
					
					ArrayList<Integer> alGeneIdPresenceAbsenceHomology = new ArrayList<>();
					alGeneIdPresenceAbsenceHomology.addAll(getAlGeneId_WithAlElementIdAndAlPresenceAbsenceOrgaIds(
							conn
							, hsElementIds
							, alListOrgaIdAsString
							, alPresenceOrAbsence
							, isAnd
						));
					alAlGeneIdsAfterEachFilters.add(alGeneIdPresenceAbsenceHomology);
//					alAlGeneIdsAfterEachFilters.add(
//							
////							getAllGeneIdWithElementIdAndAlPresenceAbsenceOrgaIds(
////								conn
////								, alOrgaIdAsString
////								, alPresenceOrAbsence
////								, isAnd
////								,selectedElementId
////							)
//							
//							
//							
//						);
//					//}
					

				} else {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("unidentified getTypeFilterGeneBy = "+fgiIT.getTypeFilterGeneBy().toString()));
				}
			}

			// intersect the different alAlGeneIdsToIntersectOrFusion with
			// operator and/or

			// deal with sub operator
			ArrayList<ArrayList<Integer>> alAlGeneIdsAfterInnerLogicalOperator = new ArrayList<ArrayList<Integer>>();
			boolean outerLogicalIsAnd = true;
			int isCurrentInner0Undef1AND2ORTypeBoolean = 0;
			
			if (alAlGeneIdsAfterEachFilters.isEmpty()) {
				return new ArrayList<LightGeneItem>();
				// }else if(alAlGeneIdsAfterEachFilters.size() == 1){
				// alAlGeneIdsAfterInnerLogicalOperator.add(alAlGeneIdsAfterEachFilters.get(0));
			} else {

				for (int i = 0; i < listFilters.size(); i++) {
					FilterGeneItem fgiIT = listFilters.get(i);
					if (fgiIT.getOperatorFilterGeneBy().compareTo(
							EnumOperatorFilterGeneBy.COMMA_DOTS_AND_DOTS_COMMA) == 0) {
						if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
								|| isCurrentInner0Undef1AND2ORTypeBoolean == 1) {
							// intersection
							isCurrentInner0Undef1AND2ORTypeBoolean = 1;
							if ((i - 1) >= 0) {
								ArrayList<Integer> alFgiIT = alAlGeneIdsAfterEachFilters
										.get(i);
								ArrayList<Integer> alFgiPrev = alAlGeneIdsAfterInnerLogicalOperator
										.get((alAlGeneIdsAfterInnerLogicalOperator
												.size() - 1));
								ArrayList<Integer> c = new ArrayList<Integer>(
										alFgiIT.size() > alFgiPrev.size() ? alFgiIT
												.size() : alFgiPrev.size());
								c.addAll(alFgiIT);
								c.retainAll(alFgiPrev);
								alAlGeneIdsAfterInnerLogicalOperator.set(
										(alAlGeneIdsAfterInnerLogicalOperator
												.size() - 1), c);
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("i-1 inferior at 0 when deal with sub operator Bis"));
							}

						} else {
							UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis"));
						}
					} else if (fgiIT.getOperatorFilterGeneBy().compareTo(
							EnumOperatorFilterGeneBy.COMMA_DOTS_OR_DOTS_COMMA) == 0) {
						if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
								|| isCurrentInner0Undef1AND2ORTypeBoolean == 2) {
							// union
							isCurrentInner0Undef1AND2ORTypeBoolean = 2;
							if ((i - 1) >= 0) {
								ArrayList<Integer> alFgiIT = alAlGeneIdsAfterEachFilters
										.get(i);
								ArrayList<Integer> alFgiPrev = alAlGeneIdsAfterInnerLogicalOperator
										.get((alAlGeneIdsAfterInnerLogicalOperator
												.size() - 1));
								ArrayList<Integer> c = new ArrayList<Integer>(
										alFgiIT.size() + alFgiPrev.size());
								UtilitiesMethodsShared.addToArrayListNoDups(c, alFgiIT);
								UtilitiesMethodsShared.addToArrayListNoDups(c, alFgiPrev);
								alAlGeneIdsAfterInnerLogicalOperator.set(
										(alAlGeneIdsAfterInnerLogicalOperator
												.size() - 1), c);
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("i-1 inferior at 0 when deal with sub operator quatro"));
							}
						} else {
							UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis"));
						}
					} else if (fgiIT.getOperatorFilterGeneBy().compareTo(
							EnumOperatorFilterGeneBy.DOTS_COMMA_AND_COMMA_DOTS) == 0) {
						isCurrentInner0Undef1AND2ORTypeBoolean = 0;
						outerLogicalIsAnd = true;
						alAlGeneIdsAfterInnerLogicalOperator
								.add(alAlGeneIdsAfterEachFilters.get(i));
					} else if (fgiIT.getOperatorFilterGeneBy().compareTo(
							EnumOperatorFilterGeneBy.DOTS_COMMA_OR_COMMA_DOTS) == 0) {
						isCurrentInner0Undef1AND2ORTypeBoolean = 0;
						outerLogicalIsAnd = false;
						alAlGeneIdsAfterInnerLogicalOperator
								.add(alAlGeneIdsAfterEachFilters.get(i));
					} else if (fgiIT.getOperatorFilterGeneBy().compareTo(
							EnumOperatorFilterGeneBy.FIRST) == 0
							&& i == 0) {
						alAlGeneIdsAfterInnerLogicalOperator
								.add(alAlGeneIdsAfterEachFilters.get(i));
					} else {
						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("unidentified EnumOperatorFilterGeneBy "
										+ fgiIT.getOperatorFilterGeneBy()));
					}

				}

			}

			// deal with root operator
			ArrayList<Integer> alAlGeneIdsFinals = new ArrayList<Integer>();

			if (alAlGeneIdsAfterInnerLogicalOperator.isEmpty()) {
				return new ArrayList<LightGeneItem>();
			} else if (alAlGeneIdsAfterInnerLogicalOperator.size() == 1) {
				alAlGeneIdsFinals = alAlGeneIdsAfterInnerLogicalOperator.get(0);
			} else {

				if (outerLogicalIsAnd) {
					// intersection
					for (int i = 0; i < alAlGeneIdsAfterInnerLogicalOperator
							.size(); i++) {
						if (i == 0) {
							continue;
						} else if (i == 1) {
							// init intersection
							if ((i - 1) >= 0) {
								ArrayList<Integer> alFgiIT = alAlGeneIdsAfterInnerLogicalOperator
										.get(i);
								ArrayList<Integer> alFgiPrev = alAlGeneIdsAfterInnerLogicalOperator
										.get(i - 1);
								alAlGeneIdsFinals = new ArrayList<Integer>(
										alFgiIT.size() > alFgiPrev.size() ? alFgiIT
												.size() : alFgiPrev.size());
								alAlGeneIdsFinals.addAll(alFgiIT);
								alAlGeneIdsFinals.retainAll(alFgiPrev);
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("i-1 inferior at 0 when deal with sub operator cinquo"));
							}

						} else {
							// continue intersection
							if ((i - 1) >= 0) {
								ArrayList<Integer> alFgiIT = alAlGeneIdsAfterInnerLogicalOperator
										.get(i);
								ArrayList<Integer> c = new ArrayList<Integer>(
										alFgiIT.size() > alAlGeneIdsFinals
												.size() ? alFgiIT.size()
												: alAlGeneIdsFinals.size());
								c.addAll(alFgiIT);
								c.retainAll(alAlGeneIdsFinals);
								alAlGeneIdsFinals = c;
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("i-1 inferior at 0 when deal with sub operator sixo"));
							}
						}
					}
				} else {
					// union
					for (int i = 0; i < alAlGeneIdsAfterInnerLogicalOperator
							.size(); i++) {
						if (i == 0) {
							continue;
						} else if (i == 1) {
							// init union
							if ((i - 1) >= 0) {
								ArrayList<Integer> alFgiIT = alAlGeneIdsAfterInnerLogicalOperator
										.get(i);
								ArrayList<Integer> alFgiPrev = alAlGeneIdsAfterInnerLogicalOperator
										.get(i - 1);
								alAlGeneIdsFinals = new ArrayList<Integer>(
										alFgiIT.size() + alFgiPrev.size());
								UtilitiesMethodsShared.addToArrayListNoDups(alAlGeneIdsFinals, alFgiIT);
								UtilitiesMethodsShared.addToArrayListNoDups(alAlGeneIdsFinals, alFgiPrev);
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("i-1 inferior at 0 when deal with sub operator septo"));
							}

						} else {
							// continue union
							if ((i - 1) >= 0) {
								ArrayList<Integer> alFgiIT = alAlGeneIdsAfterInnerLogicalOperator
										.get(i);
								ArrayList<Integer> c = new ArrayList<Integer>(
										alFgiIT.size()
												+ alAlGeneIdsFinals.size());
								UtilitiesMethodsShared.addToArrayListNoDups(c, alFgiIT);
								UtilitiesMethodsShared.addToArrayListNoDups(c, alAlGeneIdsFinals);
								alAlGeneIdsFinals = c;
							} else {
								UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("i-1 inferior at 0 when deal with sub operator huito"));
							}
						}
					}
				}

			}
			
			// get gene items with gene ids
			arrayListLGI = QueriesTableGenes.getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(conn,
					alAlGeneIdsFinals, true);

			//Comparator<LightGeneItem> compaGenesByStart = new LightGeneItem.ByStartGeneComparator();
			Collections.sort(arrayListLGI, LightGeneItemComparators.byStartGeneComparator);


		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
			
		}// try
		
		return arrayListLGI;

	}
	
	


	static public GeneItem getGeneItemWithGeneId(
			Connection conn
			, int geneId
			) throws Exception {

		ArrayList<Integer> alToSend = new ArrayList<Integer>();
		alToSend.add(geneId);

		ArrayList<GeneItem> alGIReturned = getAllGeneItemWithListGeneIds(conn,
				alToSend);
		if (alGIReturned.isEmpty()) {
			return null;
		} else {
			return alGIReturned.get(0);
		}

	}
	

	static public ArrayList<GeneItem> getAllGeneItemWithListGeneIds(
			Connection conn
			, ArrayList<Integer> listGeneIds
			) throws Exception {
		
		String methodNameToReport = "QueriesTableGenes getAllGeneItemWithListGeneIds"
				+ " ; listGeneIds="+ ( (listGeneIds != null ) ? listGeneIds.toString() : "NULL" )
				;
		
		if (listGeneIds.isEmpty()) {
			return new ArrayList<GeneItem>();
		}

		ArrayList<GeneItem> arrayListGI = new ArrayList<GeneItem>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			
			String command = "SELECT genes.gene_id"
					+ ", genes.organism_id, genes.element_id, genes.name, genes.strand, genes.start, genes.stop";
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				command += ", genes.length_residues AS length_residues";
			} else {
				command += ", length(genes.residues) AS length_residues";
			}
			command += ", elements.type"
					+ ", micado.qualifiers.type_qual, micado.qualifiers.qualifier"
					+ " FROM genes left join (elements left join micado.qualifiers on (elements.accession = micado.qualifiers.accession)) on (genes.feature_id = micado.qualifiers.code_feat)"
					+ " WHERE elements.element_id=genes.element_id"
					+ " AND genes.gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(listGeneIds)
					+ ")" +
					" ORDER BY genes.gene_id";

			// System.out.println(command);

			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
		
			int count = 0;
			int currGeneId = -1;
			while (rs.next()) {

				if (count > QueriesTableGenes.MAX_GENE_ITEM_RETURNED_ALLOWED_FOR_SEARCH) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Your query returned a large number of gene, please narrow your search down"));
				}

				if (currGeneId == -1 || currGeneId != rs.getInt("gene_id")) {
					// new gene
					GeneItem geneToDW = new GeneItem();
					currGeneId = rs.getInt("gene_id");
					count++;
					QueriesTableGenes.fillInThisLightGeneItemWithBasicsExceptLocusTagAndName(geneToDW, rs);
					QueriesTableGenes.fillInThisLightGeneItemWithLocusTagAndName(geneToDW, rs);
					QueriesTableGenes.fillInThisGeneItemWithStructuredQualifiersExceptLocusTagAndName(geneToDW, rs);
					geneToDW.setLengthResidues(rs.getInt("length_residues"));
					arrayListGI.add(geneToDW);
				} else {
					// same gene
					GeneItem geneToDW = arrayListGI.get(arrayListGI.size() - 1);
					QueriesTableGenes.fillInThisLightGeneItemWithLocusTagAndName(geneToDW, rs);
					QueriesTableGenes.fillInThisGeneItemWithStructuredQualifiersExceptLocusTagAndName(geneToDW, rs);
				}
			}// while

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return arrayListGI;
	}

	
	public static ArrayList<String> getDetailledGeneInfoAsListStringForHTMLWithGeneId(
			Connection conn, Integer geneIdSent) throws Exception {

		String methodNameToReport = "QueriesTableGenes getDetailledGeneInfoAsListStringForHTMLWithGeneId"
				+ " ; geneIdSent="+geneIdSent
				;
		
		ArrayList<String> alFullStringToReturn = new ArrayList<String>();
		
		ArrayList<String> alGeneName_LocusTag = new ArrayList<String>();
		String geneNameToReturn = "";
		String locusTagToReturn = "";
		alGeneName_LocusTag.add(geneNameToReturn);
		alGeneName_LocusTag.add(locusTagToReturn);
		ArrayList<String> alStringTypeFeatLoc = new ArrayList<String>();
		String protSize = "";
		ArrayList<String> alStringStructuredQualifiers = new ArrayList<String>();
		ArrayList<String> alStringDbxrefQualifiers = new ArrayList<String>();
		ArrayList<String> otherQualifiers = new ArrayList<String>();
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			//command to get accession and feature_id
			
			String accnumIT = "";
			int featureIdIT = -1;

			String commandGetAccessionAndFeatureId = "SELECT elements.accession, genes.feature_id"
					+ " FROM genes, elements"
					+ " WHERE elements.organism_id=genes.organism_id"
					+ " AND elements.element_id=genes.element_id"
					+ " AND genes.gene_id = " + geneIdSent;
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandGetAccessionAndFeatureId, methodNameToReport);
			
			if (rs.next()) {
				accnumIT = rs.getString("accession");
				featureIdIT = rs.getInt("feature_id");
			}
			
			if(accnumIT.isEmpty()){
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("No accession number found for gene id = "+geneIdSent));
			}
			if(featureIdIT< 0){
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("No feature_id found for gene id = "+geneIdSent));
			}
			
			// command to get TypeFeat and Location
			String command_first = "SELECT type_feat, location"
					+ " FROM micado.features"
					+ " WHERE accession = '"+accnumIT+"'"
					+ " AND code_feat = "+featureIdIT;

			rs.close();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command_first, methodNameToReport);
			
			
			if (rs.next()) {
				alStringTypeFeatLoc.add("<i>Type of feature : </i>"
						+ rs.getString("type_feat"));
				alStringTypeFeatLoc.add("<i>Location : </i>"
						+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("location"))+" on "+accnumIT);
			}


			// command to get size of protein
			String command_sizeProt = "SELECT length"
					+ " FROM micado.prot_feat"
					+ " WHERE accession = '"+accnumIT+"'"
					+ " AND code_feat = "+featureIdIT;

			rs.close();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command_sizeProt, methodNameToReport);
			
			
			if (rs.next()) {
				protSize = "<i>Protein size : </i>"+rs.getInt("length")+" aa";
			}
			
			// command to get qualifiers
			GeneItem geneToDW = new GeneItem();
			String command = "SELECT type_qual, qualifier"
					+ " FROM micado.qualifiers"
					+ " WHERE accession = '"+accnumIT+"'"
					+ " AND code_feat = "+featureIdIT;

			rs.close();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			
			while (rs.next()) {

				boolean matchGeneName_LocusTag = QueriesTableGenes.fillAlGeneName_LocusTag(rs,
						alGeneName_LocusTag, geneIdSent, accnumIT, -1, true);
				boolean matchStructuredQualifiers = QueriesTableGenes.fillInThisGeneItemWithStructuredQualifiersExceptLocusTagAndName( geneToDW, rs);
				boolean matchDbxrefQualifiers = QueriesTableGenes.fillAlStringDbxrefWithDbxrefQualifiers( alStringDbxrefQualifiers, rs);
				
				if (! matchStructuredQualifiers
						&& ! matchGeneName_LocusTag
						&& ! matchDbxrefQualifiers) {
					// unstructured qualifiers
					otherQualifiers.add("<i>"
							+ rs.getString("type_qual") + " : </i>"
							+ rs.getString("qualifier"));
				}
			}

			// add alStringStructuredQualifiers and modify html if link provided
			for (int i = 0; i < geneToDW.getListOldLocusTag().size(); i++) {
				String stringTmp = "<i>Old locus tag : </i>";
				String oldLocusTagIT = geneToDW.getListOldLocusTag().get(i);
				stringTmp += "<a href=\"http://ensemblgenomes.org/id/" // was http://ensemblgenomes.org/search/eg/
						+ oldLocusTagIT
						+ "\" title=\"link to EnsemblGenome via the old locus tag.\" target=\"_blank\">"+oldLocusTagIT+"</a>"
						;
				alStringStructuredQualifiers.add(stringTmp);
			}
			
			
			for (int i = 0; i < geneToDW.getListProteinId().size(); i++) {
				String stringTmp = "<i>Protein id : </i>";
				String proteinIdIT = geneToDW.getListProteinId().get(i);
				Matcher m = NCBIProteinIdPatt.matcher(proteinIdIT);
				if (m.matches()) {
					String ncbiProteinIdIT = m.group(1);
					stringTmp += "<a href=\"http://www.ncbi.nlm.nih.gov/protein/"
							+ ncbiProteinIdIT + "\" target=\"_blank\">" + ncbiProteinIdIT + "</a>";
					if(m.groupCount() == 2){
						if(m.group(2) != null){
							stringTmp += m.group(2);
						}
					}
				} else {
					stringTmp += proteinIdIT;
				}
				alStringStructuredQualifiers.add(stringTmp);
			}
			
			
			for (int i = 0; i < geneToDW.getListProduct().size(); i++) {
				String stringTmp = "<i>Product : </i>";
				if (geneToDW.getListProduct().get(i).contains("UniProtKB") == true) {
					String locus = geneToDW.getListProduct().get(i)
							.replaceAll("\\{UniProtKB/.+:.+", "");
					String link = geneToDW.getListProduct().get(i)
							.replaceAll(".+\\s\\{UniProtKB/.+:", "");
					link = link.replaceAll("\\}", "");
					stringTmp += "<a href=\"http://www.uniprot.org/uniprot/"
							+ link + "\" target=\"_blank\">" + locus + "</a>";
				} else {
					stringTmp += geneToDW.getListProduct().get(i);
				}
				alStringStructuredQualifiers.add(stringTmp);
			}
			for (int i = 0; i < geneToDW.getListFunction().size(); i++) {
				String stringTmp = "<i>Molecular Function : </i>";
				if (geneToDW.getListFunction().get(i).contains("GO:") == true) {
					String locus = geneToDW.getListFunction().get(i)
							.replaceAll("\\s\\{GO:.+", "");
					String link = geneToDW.getListFunction().get(i)
							.replaceAll(".+\\s\\{", "");
					link = link.replaceAll("\\}", "");
					stringTmp += "<a href=\"http://www.ebi.ac.uk/QuickGO/GTerm?id="
							+ link + "\" target=\"_blank\">" + locus + "</a>";
				} else {
					stringTmp += geneToDW.getListFunction().get(i);
				}
				alStringStructuredQualifiers.add(stringTmp);
			}
			for (int i = 0; i < geneToDW.getListBiologicalProcess().size(); i++) {
				String stringTmp = "<i>Biological Process : </i>";
				if (geneToDW.getListBiologicalProcess().get(i).contains("GO:") == true) {
					String locus = geneToDW.getListBiologicalProcess().get(i)
							.replaceAll("\\s\\{GO:.+", "");
					String link = geneToDW.getListBiologicalProcess().get(i)
							.replaceAll(".+\\s\\{", "");
					link = link.replaceAll("\\}", "");
					stringTmp += "<a href=\"http://www.ebi.ac.uk/QuickGO/GTerm?id="
							+ link + "\" target=\"_blank\">" + locus + "</a>";
				} else {
					stringTmp += geneToDW.getListBiologicalProcess().get(i);
				}
				alStringStructuredQualifiers.add(stringTmp);
			}
			for (int i = 0; i < geneToDW.getListCellularComponent().size(); i++) {
				String stringTmp = "<i>Cellular Component : </i>";
				if (geneToDW.getListCellularComponent().get(i).contains("GO:") == true) {
					String locus = geneToDW.getListCellularComponent().get(i)
							.replaceAll("\\s\\{GO:.+", "");
					String link = geneToDW.getListCellularComponent().get(i)
							.replaceAll(".+\\s\\{", "");
					link = link.replaceAll("\\}", "");
					stringTmp += "<a href=\"http://www.ebi.ac.uk/QuickGO/GTerm?id="
							+ link + "\" target=\"_blank\">" + locus + "</a>";
				} else {
					stringTmp += geneToDW.getListCellularComponent().get(i);
				}
				alStringStructuredQualifiers.add(stringTmp);
			}
			for (int i = 0; i < geneToDW.getListECNumber().size(); i++) {
				
//				*********** EC_number
//				http://enzyme.expasy.org/EC/**VAR**
//
//				** look for type_qual = EC_number
//
				String stringTmp = "<i>EC Number : </i>";
				if (geneToDW.getListECNumber().get(i).contains("UniProtKB") == true) {
					String locus = geneToDW.getListECNumber().get(i)
							.replaceAll("\\{UniProtKB/.+:.+", "");
					String uniprotLink = geneToDW.getListECNumber().get(i)
							.replaceAll(".+\\s\\{UniProtKB/.+:", "").replaceAll("\\}", "");
//					stringTmp += "<a href=\"http://www.uniprot.org/uniprot/"
//							+ link + "\" target=\"_blank\">" + locus + "</a>";
					stringTmp += "<a href=\"http://enzyme.expasy.org/EC/"
							+ locus + "\" target=\"_blank\">"
							+ locus + "</a> <a href=\"http://www.uniprot.org/uniprot/"
							+ uniprotLink + "\" target=\"_blank\">" + uniprotLink + "</a>";
				} else {
					stringTmp += "<a href=\"http://enzyme.expasy.org/EC/"
							+ geneToDW.getListECNumber().get(i) + "\" target=\"_blank\">"
							+ geneToDW.getListECNumber().get(i) + "</a>";
				}
				
				alStringStructuredQualifiers.add(stringTmp);
			}

			for (int i = 0; i < geneToDW.getListNote().size(); i++) {
				
				
				alStringStructuredQualifiers.add("<i>Note : </i>"
						+ geneToDW.getListNote().get(i));
				
			}

			for (int i = 0; i < geneToDW.getListInference().size(); i++) {
				alStringStructuredQualifiers.add("<i>Inference : </i>"
						+ geneToDW.getListInference().get(i));
			}

			if (geneToDW.getAgmialXref() != null) {
				if (!geneToDW.getAgmialXref().isEmpty()) {
					alStringStructuredQualifiers
							.add("<a href=\""
									+ geneToDW.getAgmialXref()
									+ "\" title=\"link to agmial\" target=\"_blank\">[link to agmial]</a>");
				}
			}
			
			formatStringNameANdLocusTagAccordingly(alGeneName_LocusTag);
			
			ArrayList<String> formatedAlStringDbxrefQualifiers = modifyDbxrefToAddExternalHtlmLinkToVariousDb(alStringDbxrefQualifiers);
			
			//add all qualifiers in order
			alFullStringToReturn.addAll(alGeneName_LocusTag);
			alFullStringToReturn.addAll(alStringTypeFeatLoc);
			alFullStringToReturn.add(protSize);
			alFullStringToReturn.addAll(alStringStructuredQualifiers);
			alFullStringToReturn.addAll(formatedAlStringDbxrefQualifiers);
			alFullStringToReturn.addAll(otherQualifiers);

			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return alFullStringToReturn;

	}

	private static void formatStringNameANdLocusTagAccordingly(ArrayList<String> alGeneName_LocusTag) throws Exception {
	
		String geneName = alGeneName_LocusTag.get(0);
		String locusTag = alGeneName_LocusTag.get(1);
		
		// because recF {UniProtKB/Swiss-Prot:Q5LXI7}
		//geneName = "<i>Name : </i>"+geneName;
		String[] alSplittedGeneNameIT = geneName.split("\\s+");
		String mainGeneName = alSplittedGeneNameIT[0];
		String accessoireGeneName = "";
		for(int i=1;i<alSplittedGeneNameIT.length;i++){
			String splittedGeneNameIT = alSplittedGeneNameIT[i];
			Matcher m = UniProtKBPatt.matcher(splittedGeneNameIT.replaceFirst("\\{", "").replaceAll("\\}", ""));
			if (m.matches()) {
				String value = m.group(1);
				accessoireGeneName += " "+"<a href=\"http://www.uniprot.org/uniprot/"
						+ value
						+ "\" target=\"_blank\">{uniprotKB:"+value+"}</a>"
						;
			} else {
				accessoireGeneName += " {"+splittedGeneNameIT+"}";
			}
		}
		
		geneName = "<i>Name : </i>"
					+ mainGeneName
					+ accessoireGeneName
					;
		
		
		//because of "ECTR2_2587 {UniProtKB/TrEMBL:E5AZL1}"
		String[] alSplittedLocusTagIT = locusTag.split("\\s+");
		String mainLocusTag = alSplittedLocusTagIT[0];
		String accessoireLocusTag = "";
		for(int i=1;i<alSplittedLocusTagIT.length;i++){
			String splittedLocusTagIT = alSplittedLocusTagIT[i];
			Matcher m = UniProtKBPatt.matcher(splittedLocusTagIT.replaceFirst("\\{", "").replaceAll("\\}", ""));
			if (m.matches()) {
				String value = m.group(1);
				accessoireLocusTag += " "+"<a href=\"http://www.uniprot.org/uniprot/"
						+ value
						+ "\" target=\"_blank\">{uniprotKB:"+value+"}</a>"
						;
			} else {
				accessoireLocusTag += " {"+splittedLocusTagIT+"}";
			}
		}
		
		locusTag = "<i>Locus tag : </i>"
					+ "<a href=\"http://ensemblgenomes.org/id/" // was http://ensemblgenomes.org/search/eg/
					+ mainLocusTag
					+ "\" title=\"link to EnsemblGenome. If this link doesn't work, try with the old locus tag below if it exists.\" target=\"_blank\">"+mainLocusTag+"</a>"+accessoireLocusTag
					;
		
		alGeneName_LocusTag.set(0, geneName);
		alGeneName_LocusTag.set(1, locusTag);
		
	}
	

	private static ArrayList<String> modifyDbxrefToAddExternalHtlmLinkToVariousDb(ArrayList<String> alDbxrefTags) {
		
		ArrayList<String> formattedDbxrefToReturn = new ArrayList<String>();
		
		String ensemblGenomesSt = "";
		String goaSt = "";
		String interProSt = "";
		String uniprotKBSt = "";
		String keggSt = "";
		String pfamSt = "";
		String ncbiGiSt = "";
		String SubtiListSt = "";
		String goSt = "";
		String uniParcSt = "";
		String pdbSt = "";
		
		Matcher m = null;
			
		for (String dbxrefIT : alDbxrefTags ){
			
			boolean foundAKnownDbxref = false;
			
			m = EnsemblGenomesPatt.matcher(dbxrefIT);
//			*********** link to ensembl genome browser :
//			http://ensemblgenomes.org/id/
//			was http://ensemblgenomes.org/search/eg/**VAR**
//			** look for db_xref :
//			^EnsemblGenomes-Gn:.+$
//			** if nothing, look for db_xref :
//			^EnsemblGenomes-Tr:
			if (m.matches()) {
				String value = m.group(1);
				if( ! ensemblGenomesSt.isEmpty()){
					ensemblGenomesSt += ", ";
				}
				ensemblGenomesSt += "<a href=\"http://ensemblgenomes.org/id/" // was http://ensemblgenomes.org/search/eg/
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				if (m.groupCount() == 2) {
					if(m.group(2) != null){
						ensemblGenomesSt += m.group(2);
					}
				}
				foundAKnownDbxref = true;
			}
			
			m = GOAPatt.matcher(dbxrefIT);
//			*********** link to GOA (Gene Ontology terms and annotations)
//			http://www.ebi.ac.uk/QuickGO/GProtein?ac=**VAR**
//			** look for db_xref :
//			GOA:.+$
//			** if nothing, don't show this link
			if (m.matches()) {
				String value = m.group(1);
				if( ! goaSt.isEmpty()){
					goaSt += ", ";
				}
				goaSt += "<a href=\"http://www.ebi.ac.uk/QuickGO/GProtein?ac="
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				if (m.groupCount() == 2) {
					if(m.group(2) != null){
						goaSt += m.group(2);
					}
				}
				foundAKnownDbxref = true;
			}
			
			//InterProPatt
			m = InterProPatt.matcher(dbxrefIT);
//			*********** link to InterPro:
//			http://www.ebi.ac.uk/interpro/entry/
			if (m.matches()) {
				if( ! interProSt.isEmpty()){
					interProSt += ", ";
				}
				String value = m.group(1);
				interProSt += "<a href=\"http://www.ebi.ac.uk/interpro/entry/"
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				if (m.groupCount() == 2) {
					if(m.group(2) != null){
						interProSt += m.group(2);
					}
				}
				foundAKnownDbxref = true;
			}
			
			
			//UniProtKBPatt
			m = UniProtKBPatt.matcher(dbxrefIT);
//			*********** link to UniProtKB/TrEMBL:
//			http://www.uniprot.org/uniprot/**VAR**
			if (m.matches()) {
				String value = m.group(1);
				if( ! uniprotKBSt.isEmpty()){
					uniprotKBSt += ", ";
				}
				uniprotKBSt += "<a href=\"http://www.uniprot.org/uniprot/"
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				if (m.groupCount() == 2) {
					if(m.group(2) != null){
						uniprotKBSt += m.group(2);
					}
				}
				foundAKnownDbxref = true;
			}
			
			//GO Pattern
			m = GOPatt.matcher(dbxrefIT);
			//http://amigo.geneontology.org/amigo/term/GO:0003677 for exemple db_xref : GO:0003677 {GOA:D8IGA4}
			if (m.matches()) {
				String value = m.group(1);
				if( ! goSt.isEmpty()){
					goSt += ", ";
				}
				goSt += "<a href=\"http://amigo.geneontology.org/amigo/term/GO:"
						+ value
						+ "\" target=\"_blank\">GO:"+value+"</a>"
						;
				if (m.groupCount() == 2) {
					if(m.group(2) != null){
						goSt += m.group(2);
					}
				}
				foundAKnownDbxref = true;
			}
			
			
			//UniParc Pattern
			m = UniParcPatt.matcher(dbxrefIT);
			//http://www.uniprot.org/uniparc/UPI000172D0AF for exemple db_xref : UniParc:UPI000172D0AF {EMBL:ADJ40685}
			if (m.matches()) {
				String value = m.group(1);
				if( ! uniParcSt.isEmpty()){
					uniParcSt += ", ";
				}
				uniParcSt += "<a href=\"http://www.uniprot.org/uniparc/"
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				if (m.groupCount() == 2) {
					if(m.group(2) != null){
						uniParcSt += m.group(2);
					}
				}
				foundAKnownDbxref = true;
			}
			
			
			m = KEGGPatt.matcher(dbxrefIT);
//			*********** KEGG
//			http://www.genome.jp/dbget-bin/www_bfind_sub?mode=bfind&max_hit=1000&dbkey=kegg&keywords=**VAR**
			if (m.matches()) {
				String value = m.group(1);
				if( ! keggSt.isEmpty()){
					keggSt += ", ";
				}
				keggSt += "<a href=\"http://www.genome.jp/dbget-bin/www_bfind_sub?mode=bfind&max_hit=1000&dbkey=kegg&keywords="
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				foundAKnownDbxref = true;
			}
			
			
			m = PFAMPatt.matcher(dbxrefIT);
//			*********** PFAM
//			http://pfam.xfam.org/family/**VAR**
			if (m.matches()) {
				String value = m.group(1);
				if( ! pfamSt.isEmpty()){
					pfamSt += ", ";
				}
				pfamSt += "<a href=\"http://pfam.xfam.org/family/"
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				foundAKnownDbxref = true;
			}
			
			
			m = NCBIGIPatt.matcher(dbxrefIT);
			// http://www.ncbi.nlm.nih.gov/gquery/?term=
			// ncbiGiSt
			if (m.matches()) {
				String value = m.group(1);
				if( ! ncbiGiSt.isEmpty()){
					ncbiGiSt += ", ";
				}
				ncbiGiSt += "<a href=\"http://www.ncbi.nlm.nih.gov/gquery/?term="
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				if (m.groupCount() == 2) {
					if(m.group(2) != null){
						ncbiGiSt += m.group(2);
					}
				}
				foundAKnownDbxref = true;
			}
			
			m = SubtiListPatt.matcher(dbxrefIT);
			// http://genolist.pasteur.fr/SubtiList/genome.cgi?external_query+**ID**
			if (m.matches()) {
				String value = m.group(1);
				if( ! SubtiListSt.isEmpty()){
					SubtiListSt += ", ";
				}
				SubtiListSt += "<a href=\"http://genolist.pasteur.fr/SubtiList/genome.cgi?external_query+"
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				if (m.groupCount() == 2) {
					if(m.group(2) != null){
						SubtiListSt += m.group(2);
					}
				}
				foundAKnownDbxref = true;
			}
			
			m = PDBPatt.matcher(dbxrefIT);
			//http://www.ebi.ac.uk/pdbe/entry/search/index?text:4DDQ // db_xref : PDB:4DDQ
			if (m.matches()) {
				String value = m.group(1);
				if( ! pdbSt.isEmpty()){
					pdbSt += ", ";
				}
				pdbSt += "<a href=\"http://www.ebi.ac.uk/pdbe/entry/search/index?text:"
						+ value
						+ "\" target=\"_blank\">"+value+"</a>"
						;
				if (m.groupCount() == 2) {
					if(m.group(2) != null){
						pdbSt += m.group(2);
					}
				}
				foundAKnownDbxref = true;
			}
				
			//add if not found above
			if ( ! foundAKnownDbxref){
				formattedDbxrefToReturn.add("<i>db_xref : </i>" + dbxrefIT);
			}
			
			
		}


		//Add last to show first
		if ( ! SubtiListSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref SubtiList : </i>" + SubtiListSt);
		}
		if ( ! pdbSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref PDB : </i>" + pdbSt);
		}
		if ( ! keggSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref KEGG : </i>" + keggSt);
		}
		if ( ! pfamSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref PFAM : </i>" + pfamSt);
		}
		if ( ! goSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref GO : </i>" + goSt);
		}
		if ( ! uniParcSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref uniParc : </i>" + uniParcSt);
		}
		if ( ! uniprotKBSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref uniprotKB : </i>" + uniprotKBSt);
		}
		if ( ! interProSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref Interpro : </i>" + interProSt);
		}
		if ( ! goaSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref GOA : </i>" + goaSt);
		}
		if ( ! ncbiGiSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref NCBI : </i>" + ncbiGiSt);
		}
		if ( ! ensemblGenomesSt.isEmpty()){
			formattedDbxrefToReturn.add(0,"<i>db_xref ensemblgenomes : </i>" + ensemblGenomesSt);
		}

		
		return formattedDbxrefToReturn;
					
	}

	
}

