package fr.inra.jouy.server.queriesTable;
/*
Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.shared.UtilitiesMethodsShared;

public class QueriesTableProtAndNucSequences {

	public static String getProteinSequenceAsFastaWithAlGeneIds(
			Connection conn
			, HashMap<Integer,Integer> hashGeneIds2Marker
			) throws Exception {
		
		String methodNameToReport = "QueriesTableProtAndNucSequences getProteinSequenceAsFastaWithAlGeneIds"
				+ " ; hashGeneIds2Marker"+ ( (hashGeneIds2Marker != null ) ? hashGeneIds2Marker.toString() : "NULL" )
				;
		
		StringBuilder sb = new StringBuilder();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			String command = "";
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
								
				command = "SELECT genes.gene_id, genes.name, micado.prot_feat.proteine AS residues, genes.feature_id, genes.start, genes.stop"
						+ ", organisms.species, organisms.strain, organisms.substrain, organisms.ncbi_assemblyaccession_it" //
						+ ", elements.accession"
						+ " FROM genes, organisms, elements, micado.prot_feat"
						+ " WHERE genes.gene_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hashGeneIds2Marker.keySet())
						+ ")"
						+ " AND genes.element_id = elements.element_id"
						+ " AND genes.organism_id = organisms.organism_id"
						+ " AND elements.accession = micado.prot_feat.accession"
						+ " AND genes.feature_id = micado.prot_feat.code_feat"
						+ " ORDER BY genes.organism_id, genes.element_id, genes.start";
				
				
			} else {
				command = "SELECT genes.gene_id, genes.name, genes.residues AS residues, genes.feature_id, genes.start, genes.stop," +
						" organisms.species, organisms.strain, organisms.substrain," +
						" elements.accession" +
						" FROM genes, organisms, elements WHERE genes.gene_id IN ("
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hashGeneIds2Marker.keySet())
						+ ") AND genes.element_id = elements.element_id AND genes.organism_id = organisms.organism_id ORDER BY genes.organism_id, genes.element_id, genes.start";
			}
			
			statement = conn.createStatement();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				
				ArrayList<String> alGeneNameAndLocusTag = QueriesTableMicadoQualifiers.getGeneNameAndLocusTagWithAccessionAndFeatureId_firstOnly(conn, rs.getString("accession"), rs.getInt("feature_id"));
				
				String headerIdentifiant = "";
				if(alGeneNameAndLocusTag.size() > 1){
					if(alGeneNameAndLocusTag.get(1) != null){
						if(!alGeneNameAndLocusTag.get(1).isEmpty()){
							headerIdentifiant += " "+alGeneNameAndLocusTag.get(1);
						}
					}
				}
				if( rs.getString("name") != null && !rs.getString("name").isEmpty()){
					headerIdentifiant += " "+rs.getString("name");
				}else if(alGeneNameAndLocusTag.size() > 0){
					if(alGeneNameAndLocusTag.get(0) != null){
						if(!alGeneNameAndLocusTag.get(0).isEmpty()){
							headerIdentifiant += " "+alGeneNameAndLocusTag.get(0);
						}
					}
				}
				if(headerIdentifiant.isEmpty()){
					headerIdentifiant = "OrigamiId="+rs.getString("gene_id");
				}
				headerIdentifiant += " ("+rs.getInt("start")+"-"+rs.getInt("stop")+")";
				String headerSpecies = "";
				if (rs.getString("species") != null) {
					headerSpecies += " " + rs.getString("species");
				}
				if (rs.getString("strain") != null) {
					headerSpecies += " " + rs.getString("strain");
				}
				if (rs.getString("substrain") != null) {
					headerSpecies += " " + rs.getString("substrain");
				}
				if ( UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn) && rs.getString("ncbi_assemblyaccession_it") != null) {
					headerSpecies += " " + rs.getString("ncbi_assemblyaccession_it");
				}
				//header
				sb.append(">"+headerIdentifiant+" ; "+headerSpecies);
				//sb.append("\n");
				sb.append(System.getProperty("line.separator"));
				//prot seq
				String residuesInOneString = rs.getString("residues");
				//cut seq by chunk of 120 residues
				for(int i=0;i<residuesInOneString.length();i=i+120){
					int endIndex = i+120;
					if(endIndex>residuesInOneString.length()){
						endIndex = residuesInOneString.length();
					}
					sb.append(residuesInOneString.substring(i, endIndex));
					//sb.append("\n");
					sb.append(System.getProperty("line.separator"));
				}
				//sb.append(System.getProperty("line.separator"));
				//sb.append("\n");
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return sb.toString();
		
	}

	public static String getNucleicAcidSequenceAsFastaWithAlGeneIds(
			Connection conn
			//, HashMap<Integer,Integer> hashGeneIds2Marker
			, Set<Integer> hsGeneIds
			, boolean withHeader
			, boolean lineSeparatorInSeq
			) throws Exception {


		String methodNameToReport = "QueriesTableProtAndNucSequences getNucleicAcidSequenceAsFastaWithAlGeneIds"
				+ " ; hsGeneIds="+ ( (hsGeneIds != null ) ? hsGeneIds.toString() : "NULL" )
				;
		
		StringBuilder sb = new StringBuilder();

		//check args
		if (hsGeneIds == null || hsGeneIds.isEmpty()) {
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("hsGeneIds == null || hsGeneIds.isEmpty()"));
		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			String colmunsIsDatabaseGenesElementsGenomeAssemblyConvenience = "";
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				colmunsIsDatabaseGenesElementsGenomeAssemblyConvenience = ", organisms.ncbi_assemblyaccession_it";
			}
			String command = "SELECT genes.start, genes.stop, genes.strand"
					+ ", elements.accession";
			if (withHeader) {
				command += ", genes.gene_id, genes.name, genes.feature_id, genes.strand"
						+ ", organisms.species, organisms.strain, organisms.substrain"
						+ colmunsIsDatabaseGenesElementsGenomeAssemblyConvenience;
			}
			command += " FROM genes, elements";
			if (withHeader) {
				command += ", organisms";
			}
			command += " WHERE genes.gene_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(hsGeneIds)
					+ ")"
					+ " AND genes.element_id = elements.element_id";
			if (withHeader) {
				command += " AND genes.organism_id = organisms.organism_id";
			}
			command += " ORDER BY";
			if (withHeader) {
				command += " genes.organism_id,";
			}
			command += " genes.element_id, genes.start";
			
			//System.err.println(command);
			
			statement = conn.createStatement();
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			
			while (rs.next()) {
				
				if (withHeader) {
					//ArrayList<String> alGeneNameAndLocusTag = CallForInfoDBImpl.getGeneNameAndLocusTagWithAccessionAndCodeFeat(, conn);
					ArrayList<String> alGeneNameAndLocusTag = QueriesTableMicadoQualifiers.getGeneNameAndLocusTagWithAccessionAndFeatureId_firstOnly(
							conn
							, rs.getString("accession")
							, rs.getInt("feature_id")
							);
					String headerIdentifiant = "";
					if(alGeneNameAndLocusTag.size() > 1){
						if(alGeneNameAndLocusTag.get(1) != null){
							if(!alGeneNameAndLocusTag.get(1).isEmpty()){
								headerIdentifiant += " "+alGeneNameAndLocusTag.get(1);
							}
						}
					}
					if( rs.getString("name") != null && !rs.getString("name").isEmpty() ){
						headerIdentifiant += " "+rs.getString("name");
					}else if(alGeneNameAndLocusTag.size() > 0){
						if(alGeneNameAndLocusTag.get(0) != null){
							if(!alGeneNameAndLocusTag.get(0).isEmpty()){
								headerIdentifiant += " "+alGeneNameAndLocusTag.get(0);
							}
						}
					}
					if(headerIdentifiant.isEmpty()){
						headerIdentifiant = "OrigamiId="+rs.getString("gene_id");
					}
					headerIdentifiant += " ("+rs.getInt("start")+"-"+rs.getInt("stop")+")";
					
					//String headerSpecies = rs.getString("species")+" "+rs.getString("strain")+" "+rs.getString("substrain");
					String headerSpecies = "";
					if (rs.getString("species") != null) {
						headerSpecies += " " + rs.getString("species");
					}
					if (rs.getString("strain") != null) {
						headerSpecies += " " + rs.getString("strain");
					}
					if (rs.getString("substrain") != null) {
						headerSpecies += " " + rs.getString("substrain");
					}
					if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn) && rs.getString("ncbi_assemblyaccession_it") != null) {
						headerSpecies += " " + rs.getString("ncbi_assemblyaccession_it");
					}
					//header
					sb.append(">"+headerIdentifiant+" ; "+headerSpecies);
					//sb.append("\n");
					sb.append(System.getProperty("line.separator"));
				}
				
				//prot seq
				String dnaResiduesInOneStringPre = getDnaResiduesAsString(rs.getInt("start"), rs.getInt("stop"), rs.getString("accession"), conn);
				String dnaResiduesInOneString = "";
				
				if(rs.getInt("strand")<0){
					dnaResiduesInOneString = getReverseComplement(dnaResiduesInOneStringPre);
				}else{
					dnaResiduesInOneString = dnaResiduesInOneStringPre;
				}
				
				//cut seq by chunk of 120 residues
				for(int i=0;i<dnaResiduesInOneString.length();i=i+120){
					int endIndex = i+120;
					if(endIndex>dnaResiduesInOneString.length()){
						endIndex = dnaResiduesInOneString.length();
					}
					sb.append(dnaResiduesInOneString.substring(i, endIndex));
					//sb.append("\n");
					if (lineSeparatorInSeq) {
						sb.append(System.getProperty("line.separator"));
					}
					//stToReturn += residuesInOneString.substring(i, endIndex)+"\n";
				}
				//sb.append("\n");
				//sb.append(System.getProperty("line.separator"));
				
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);

		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return sb.toString();
		
	}

	private static String getReverseComplement(String dnaResiduesInOneStringSent) {
		
		StringBuilder sbDna = new StringBuilder(dnaResiduesInOneStringSent);
		String dnaReversed = sbDna.reverse().toString().toUpperCase();
		return dnaReversed.replace('A', 'W').replace('T', 'A').replace('W', 'T').replace('C', 'Y').replace('G', 'C').replace('Y', 'G');

	}

	private static String getDnaResiduesAsString(
			int start
			, int stop
			, String accessionNb
			, Connection conn
			) throws Exception {

		String methodNameToReport = "QueriesTableProtAndNucSequences getDnaResiduesAsString"
				+ " ; start="+start
				+ " ; stop="+stop
				+ " ; accessionNb="+accessionNb
				;
		
		String stToReturn = "";

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			if(start < stop){
				String command = "SELECT substring(sequences,"+start+","+((stop-start)+1)+") AS dna_residues FROM micado.dna_seq WHERE accession = '"+accessionNb+"'";

				statement = conn.createStatement();

				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);


				if (rs.next()) {
					stToReturn = rs.getString("dna_residues");
					if(rs.next()){
						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("more than 1 tuples for accessionNb="+accessionNb+" ; start="+start+" ; stop="+stop));
					}
				}else{
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("no results for accessionNb="+accessionNb+" ; start="+start+" ; stop="+stop));
				}

			}else{
				//gene a cheval sur ORI
				//start gene
				String command = "SELECT substring(sequences,"+start+") AS dna_residues FROM micado.dna_seq WHERE accession = '"+accessionNb+"'";
				statement = conn.createStatement();

				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);


				if (rs.next()) {
					stToReturn = rs.getString("dna_residues");
					if(rs.next()){
						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("more than 1 tuples for accessionNb="+accessionNb+" ; start="+start+" ; stop="+stop));
					}
				}else{
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("no results for accessionNb="+accessionNb+" ; start="+start+" ; stop="+stop));
				}

				String commandBis = "SELECT substring(sequences,1,"+stop+") AS dna_residues FROM micado.dna_seq WHERE accession = "+accessionNb;

				if(rs != null){
					rs.close();
				}
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, commandBis, methodNameToReport);


				if (rs.next()) {
					stToReturn += rs.getString("dna_residues");
					if(rs.next()){
						UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("more than 1 tuples for accessionNb="+accessionNb+" ; start="+start+" ; stop="+stop));
					}
				}else{
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("no results for accessionNb="+accessionNb+" ; start="+start+" ; stop="+stop));
				}


			}


			if(stToReturn.isEmpty()){
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("stToReturn.isEmpty for accessionNb="+accessionNb+" ; start="+start+" ; stop="+stop));
			}


		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);

		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return stToReturn;

					
	}
	
	
}
