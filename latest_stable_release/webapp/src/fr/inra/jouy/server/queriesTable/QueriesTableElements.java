package fr.inra.jouy.server.queriesTable;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.pojos.transitComposite.ConvertSetIntegerToSortedRangeItem;
import fr.inra.jouy.shared.pojos.transitComposite.PairIntegerLowerHigherRange;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem.QEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem.SEnumBlockType;

public class QueriesTableElements {

	//tested
	//int in table element are : organism_id, size, version
	public static int getIntAttributeWithPrimaryId(
			Connection conn,
			int elementId,
			String intColumnName,
			boolean shouldBePresentInTable,
			boolean shouldBeNotEmptyOrZero,
			boolean checkValidFreeStringInput) throws Exception {
		
		String methodNameToReport = "QueriesTableElements getIntAttributetWithPrimaryId"
				+ " ; elementId="+elementId
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				+ " ; shouldBeNotEmptyOrZero="+shouldBeNotEmptyOrZero
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput	
				;

		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					intColumnName, "intColumnName", false, false);
		}

		int intToReturn = -1;
		
		// build command

		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put(intColumnName, intColumnName);
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");

		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("elements");
		String whereClause = "WHERE element_id = " + elementId;
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		
		//run command
//		ObjAssociationAsMap oaamReturned = UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//				conn,
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				shouldBeNotEmptyOrZero, 
//				methodNameToReport);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			if (rs.next()) {
				intToReturn = rs.getInt(intColumnName);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
//		if(oaamReturned.getRsColumn2Int().get(intColumnName) != null){
//			return 	oaamReturned.getRsColumn2Int().get(intColumnName);
//		} else {
//			return -1;
//		}
		return intToReturn;
		
	}


	public static int getOrgaIdWithAccnum(
			Connection conn
			, String accnum
			) throws Exception {
		
		String methodNameToReport = " QueriesTableElements getOrigamiOrgaIdWithAccnum"
				+ " ; accnum="+accnum
				;		
		
		// check valid input
		UtilitiesMethodsServer.checkValidFreeStringInput(
				methodNameToReport,
				accnum, "accession number", false, false);
		//mandatory args
		if (accnum != null && ! accnum.isEmpty()) {
			//ok
		} else {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("NOT accnum != null && ! accnum.isEmpty()")
					);
		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		int origamiOrgaId = -1;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			String command = "SELECT organism_id FROM elements WHERE accession ILIKE '%"
					+ accnum + "%'";
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);

			if (rs.next()) {
				origamiOrgaId = rs.getInt("organism_id");
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("Error : more than 1 result when looking for organism with accnum = "
									+ accnum));
				}
			}

			if (origamiOrgaId < 0) {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("getOrigamiOrgaIdWithAccnum : no row returned for accnum = "
								+ accnum));
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return origamiOrgaId;

	}

	
	// tested
	public static int getElementIdWithAccession (Connection conn,
			String accession,
			boolean shouldBePresentInTable,
			boolean checkValidFreeStringInput) throws Exception {

		String methodNameToReport = "QueriesTableElements getElementIdWithAccession"
				+ " ; accession="+accession
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;
		
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					accession, "accession", false, false);
		}
		int intToReturn = -1;


		// build command

		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
//		chm_alColumnSelectAsString_aliase2columnName.put("??", "??");

		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("elements");
		String whereClause = "WHERE accession = '" + accession + "'";
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		
		//run command
//		ObjAssociationAsMap oaamReturned = UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//				conn,
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			if (rs.next()) {
				intToReturn = rs.getInt("element_id");
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("multiple rows returnes for accession = "+accession));
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
//		if(oaamReturned.getRsColumn2Int().get("element_id") != null){
//			return 	oaamReturned.getRsColumn2Int().get("element_id");
//		} else {
//			return -1;
//		}
		return intToReturn;

		
	}


	
	public static ArrayList<Integer> getAlElementIdsThenStartPbthenStopPBLoopedWithOrgaId(
			Connection conn
			, int organismId
			) throws Exception {
		
		String methodNameToReport = "QueriesTableElements getAlElementIdsThenStartPbthenStopPBLoopedWithOrgaId"
				+ " ; organismId="+organismId
				;
		
		ArrayList<Integer> listElementIdsThenStartPbthenStopPBLooped = new ArrayList<Integer>();
		boolean closeConn = false;
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//ArrayList<Integer> arrayListElementIdOrderedBySizeDesc = getListElementIdOrderedBySizeDescWithOrganismId(conn, organismId);
			ArrayList<Integer> arrayListElementIdOrderedBySizeDesc = QueriesTableElements
					.getAlElementIdWithOrganismId_optionalOrderBySizeDesc(conn, organismId, true, true);
			
			for(int EletIdIT : arrayListElementIdOrderedBySizeDesc){
				
				listElementIdsThenStartPbthenStopPBLooped.add(EletIdIT);
				listElementIdsThenStartPbthenStopPBLooped.add(0);
				listElementIdsThenStartPbthenStopPBLooped.add(getSizeElementWithElementId(conn, EletIdIT));
				
			}
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}

		}// try

		return listElementIdsThenStartPbthenStopPBLooped;
		
	}
	
	
	public static ArrayList<Integer> getAlElementIdWithOrganismId_optionalOrderBySizeDesc(
			Connection conn
			, int orgaId
			, boolean orderBySizeDesc
			, boolean shouldBePresentInTable
			) throws Exception {

		String methodNameToReport = "QueriesTableElements getAlElementIdWithOrganismId_optionalOrderBySizeDesc"
				+ "orgaId="+orgaId
				+ "orderBySizeDesc="+orderBySizeDesc
				+ "shouldBePresentInTable="+shouldBePresentInTable
				;
		
		ArrayList<Integer> alToReturn = new ArrayList<>();
		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
		//chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("elements");
		String whereClause = "WHERE organism_id = " + orgaId;
		if (orderBySizeDesc) {
			whereClause += " ORDER BY size DESC";
		}

		
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		
		//run command
//		alToReturn.addAll(UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_alInt(
//				conn, 
//				"element_id",
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport));
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				int elementIdIT = rs.getInt("element_id");
				alToReturn.add(elementIdIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return alToReturn;
		
	}
	

	public static HashMap<Integer, Integer> getHashElementId2OrgaId(
			Connection conn) throws Exception {
		String methodNameToReport = "QueriesTableElements getHashElementId2OrgaId"
				;
		HashMap<Integer, Integer> hashToReturn = new HashMap<Integer, Integer>();
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			String command = "SELECT element_id, organism_id FROM elements";
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			while (rs.next()) {
				hashToReturn.put(rs.getInt("element_id"), rs.getInt("organism_id"));
			}
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return hashToReturn;
	}

	
	// tested
	public static HashSet<String> getHsAccessions_withOrgaId( //getAlAccnumWithOrgaId
			Connection conn,
			int orgaId
			) throws Exception {
		
		String methodNameToReport = "QueriesTableElements getHsAccessions_withOrgaId"
				+ " ; orgaId="+orgaId
				;

		HashSet<String> hsToReturn = new HashSet<>();
		
		// build command

		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
		//chm_alColumnSelectAsInt_aliase2columnName.put("??", "??");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsString_aliase2columnName.put("accession", "accession");

		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("elements");
		String whereClause = "WHERE organism_id = " + orgaId;
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		
		//run command
//		return UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_alString(
//				conn,
//				"accession",
//				objSQLCommandIT,
//				true, 
//				true, 
//				methodNameToReport
//				);
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				String accessionIdIT = rs.getString("accession");
				hsToReturn.add(accessionIdIT);
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return hsToReturn;

	}
	
	public static HashMap<String,Integer> getHmAccession2ElementId_WithAlElementId(
			Connection conn
			//, ArrayList<Integer> alElementId
			, HashSet<Integer> hsElementId
			, boolean convertSetIntegerToSortedRangeItem_allowGapWithinRange
			, boolean shouldBePresentInTable
			) throws Exception {

		String methodNameToReport = "QueriesTableElements getHmAccession2ElementId_WithAlElementId"
				//+ " ; alElementId="+ ( (alElementId != null ) ? alElementId.toString() : "NULL" )
				+ " ; hsElementId="+ ( (hsElementId != null ) ? hsElementId.toString() : "NULL" )
				+ " ; convertSetIntegerToSortedRangeItem_allowGapWithinRange="+convertSetIntegerToSortedRangeItem_allowGapWithinRange
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		HashMap<String,Integer> hmToReturn = new HashMap<>();
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
//			String command = "";
//			Collections.sort(alElementId);
//			HashSet<Integer> isQueryingByRangeWorthIt = UtilitiesMethodsServer.isQueryingByRangeWorthIt_withAlInt(alElementId);
//			if ( isQueryingByRangeWorthIt != null ) {
//				command = "SELECT element_id, accession"
//						+ " FROM elements"
//						+ " WHERE element_id >= "
//						+ alElementId.get(0)
//						+ " AND element_id <= "
//						+ alElementId.get(alElementId.size()-1)
//						;
//			} else {
//				command = "SELECT element_id, accession"
//						+ " FROM elements"
//						+ " WHERE element_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alElementId)
//						+ ")";
//			}
			ConvertSetIntegerToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetIntegerToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
					hsElementId
					, convertSetIntegerToSortedRangeItem_allowGapWithinRange
					);
			//if ( ! alPositiveElementId.isEmpty()) {
			String command = "";
			boolean atLeastOneValueToSearch = false;
			if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
				command = "SELECT element_id, accession"
						+ " FROM elements WHERE (";
				if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty()) {
					command += " ( element_id IN ( "
							+ UtilitiesMethodsShared.getItemsAsStringFromCollection(csltsriIT.getPositiveSingletonAsIndividual())
							+ ") )";
					atLeastOneValueToSearch = true;
				}
				if ( ! csltsriIT.getPositiveRangeAsPair().isEmpty()) {
					for (PairIntegerLowerHigherRange pllhrIT : csltsriIT.getPositiveRangeAsPair()) {
						if (atLeastOneValueToSearch) {
							command += " OR";
						}
						command += " ( element_id >= "
								+ pllhrIT.getLowerRange()
								+ " AND element_id <= "
								+ pllhrIT.getHigherRange()
								+ " )"
								;
						atLeastOneValueToSearch = true;
					}
				}
				command += " )";
//				if (alQGeneIds != null && ! alQGeneIds.isEmpty()) {
//					command += " AND ( "+realQ+"_gene_id IN ("
//							+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alQGeneIds)
//							+ ") )";
//				}
			}
			//if ( ! alNegativeElementId.isEmpty()) {
			if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
				UtilitiesMethodsServer.reportException(
						methodNameToReport
						, new Exception("! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() :"
								+ " ; csltsriIT.getNegativeSingletonAsIndividual() = "+csltsriIT.getNegativeSingletonAsIndividual().toString()
								+ " ; csltsriIT.getNegativeRangeAsPair().isEmpty() = "+csltsriIT.getNegativeRangeAsPair().stream()
								.map(pairIntegerLowerHigherRangeIT -> "[" + pairIntegerLowerHigherRangeIT.getLowerRange().toString() + ", " + pairIntegerLowerHigherRangeIT.getHigherRange().toString() +"]")
								.collect(Collectors.toList())
								.toString()
								));
			}
			
			
			if ( ! atLeastOneValueToSearch) {
				return hmToReturn;
			}
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);
			
			while (rs.next()) {
				int elementIdIT = rs.getInt("element_id");
//				if (isQueryingByRangeWorthIt != null) {
//					if ( ! isQueryingByRangeWorthIt.contains(elementIdIT)) {
//						continue;
//					}
//				}
				if ( ! csltsriIT.getPositiveRangeAsPair().isEmpty() && convertSetIntegerToSortedRangeItem_allowGapWithinRange) {
					if (csltsriIT.getPositiveSingletonAsIndividual().contains(elementIdIT)
							|| csltsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual().contains(elementIdIT)
							) {
						//ElementIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				String accessionIT = rs.getString("accession");
				hmToReturn.put(accessionIT,elementIdIT);
			}
			if (shouldBePresentInTable) {
				//HashSet<Integer> hsElementId = new HashSet<>(alElementId); 
//				if (isQueryingByRangeWorthIt == null) {
//					isQueryingByRangeWorthIt = new HashSet<>(alElementId);
//				}
				if (hmToReturn.size() != csltsriIT.getPositiveSingletonAsIndividual().size() + csltsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual().size() ) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception(
									"shouldBePresentInTable = "+shouldBePresentInTable
									+ " and hmToReturn.size() != csltsriIT.getPositiveSingletonAsIndividual().size() + csltsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual().size()"
									+ " ; hmToReturn.size()="+hmToReturn.size()
									+ " ; csltsriIT.getPositiveSingletonAsIndividual().size()="+csltsriIT.getPositiveSingletonAsIndividual().size()
									+ " ; csltsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual().size()="+csltsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual().size()
									));
				}
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(
									methodNameToReport
									, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return hmToReturn;
	}


	//Tested
	public static String getAccessionWithElementId(
			Connection conn
			, int elementId
			, boolean shouldBePresentInTable
			) throws Exception {

		String methodNameToReport = "QueriesTableElements getAccessionWithElementId"
				+ " ; elementId="+elementId
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		String stToReturn = "";
		
		// build command
		
		// select columns
		//int
		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		//empty
		//chm_alColumnSelectAsInt_aliase2columnName.put("??", "??");
		//String
		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		chm_alColumnSelectAsString_aliase2columnName.put("accession", "accession");

		
		ArrayList<String> tableFrom = new ArrayList<String>();
		tableFrom.add("elements");
		String whereClause = "WHERE element_id = " + elementId;
		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
				chm_alColumnSelectAsInt_aliase2columnName,
				chm_alColumnSelectAsString_aliase2columnName,
				tableFrom, whereClause);
		
		//run command
//		ObjAssociationAsMap oaamReturned = UtilitiesMethodsServer.excecuteCommandAndParseUniqueRowRS(
//				conn, 
//				ObjAssociationAsMap.class,
//				objSQLCommandIT, 
//				shouldBePresentInTable, 
//				true, 
//				methodNameToReport);
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, objSQLCommandIT.getSQLCommand(), methodNameToReport);
			
			while (rs.next()) {
				stToReturn = rs.getString("accession");
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
//		if(oaamReturned.getRsColumn2String().get("accession") != null){
//			return 	oaamReturned.getRsColumn2String().get("accession");
//		} else {
//			return "";
//		}
		return stToReturn;
	}
	

	public static LightElementItem getLightElementItemWithElementId(
				 Connection conn
				 , int elementId
				 ) throws Exception {
		
		 String methodNameToReport = "QueriesTableElements getLightElementItemWithElementId"
				+ " ; elementId="+elementId
				;
	
		 Statement statement = null;
		 ResultSet rs = null;
		 boolean closeConn = false;
		 LightElementItem lei = null;
		 
		 try {
		
			 if (conn == null) {
				 conn = DatabaseConf.getConnection_db();
				 closeConn = true;
			 }
			 statement = conn.createStatement();
			
			 String command =
			 "SELECT organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic, elements.element_id, elements.type, elements.accession, micado.sequences.length" +
			 " FROM organisms, elements, micado.sequences" +
			 " WHERE elements.organism_id = organisms.organism_id" +
			 " AND elements.accession = micado.sequences.accession AND elements.element_id = "+elementId;
			
			 rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			 lei = new LightElementItem();
			
			 if (rs.next()) {
			
				 lei.setElementId(rs.getInt("element_id"));
				 lei.setType(rs.getString("type"));
				 lei.setAccession(rs.getString("accession"));
				 lei.setSize(rs.getInt("length"));
				 lei.setSpecies(rs.getString("species"));
				 lei.setStrain(rs.getString("strain"));
				 lei.setOrganismId(rs.getInt("organism_id"));
				 lei.setSubstrain(rs.getString("substrain"));
				 lei.setTaxonId(rs.getInt("taxon_id"));
				 lei.setPublic(rs.getBoolean("ispublic"));
			
			 if (rs.next()) {
				 UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("more than 1 entry found for elet_id "+elementId));
			 }
			
			 }else{
				 UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("No entry found for elet_id "+elementId));
			 }
			
			
		 } catch (Exception ex) {
			 UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		 } finally {
			 UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			 if (closeConn) {
				 DatabaseConf.freeConnection(conn, methodNameToReport);
			 }
		 }// try

		 return lei;
	 }
	
	
	

	//tested
	public static ArrayList<LightElementItem> getAlLightElementItemsPlusOrganismIdFacultatifWithSetOrganismIdsFacultatif(
			Connection conn
			, boolean getFieldOrganismId
			, Set<Integer> keySetOrganismId
			, boolean onlyPublic
			) throws Exception {
		
		String methodNameToReport = "QueriesTableElements getAlLightElementItemsPlusOrganismIdFacultatifWithSetOrganismIdsFacultatif"
				+ " ; getFieldOrganismId="+getFieldOrganismId
				+ " ; keySetOrganismId="+ ( (keySetOrganismId != null ) ? keySetOrganismId.toString() : "NULL" )
				+ " ; onlyPublic="+ onlyPublic
				;

		ArrayList<LightElementItem> alToReturn = new ArrayList<>();
		
		//args to check
		//no
		//mandatory args
		//no
		// build command
		
		 Statement statement = null;
		 ResultSet rs = null;
		 boolean closeConn = false;
		 LightElementItem lei = null;
		 
		 try {
		
			 if (conn == null) {
				 conn = DatabaseConf.getConnection_db();
				 closeConn = true;
			 }
			 statement = conn.createStatement();

			 String command = "SELECT elements.element_id, elements.type, elements.accession, elements.size";
			 if (getFieldOrganismId) {
				 command += ", elements.organism_id";
			 }
			 command += " FROM elements";
			 String whereClause = "";
			 if ( onlyPublic || UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				 command += " , organisms";
				 whereClause = " WHERE elements.organism_id = organisms.organism_id";
			 }
			 if (onlyPublic) {
				 whereClause += " AND organisms.ispublic is true";
			 }
			 if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				 whereClause += " AND organisms.computation_in_process IS FALSE";
			 }
			 if (keySetOrganismId != null && ! keySetOrganismId.isEmpty() ) {
					if(whereClause.isEmpty()){
						whereClause += " WHERE";
					} else {
						whereClause += " AND";
					}
					whereClause += " elements.organism_id IN ("+UtilitiesMethodsShared.getItemsAsStringFromCollection(keySetOrganismId)+")";
			 }
			 command += whereClause;
			 
			 rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			 while (rs.next()) {
				 lei = new LightElementItem();
				 lei.setElementId(rs.getInt("element_id"));
				 lei.setType(rs.getString("type"));
				 lei.setAccession(rs.getString("accession"));
				 lei.setSize(rs.getInt("size"));
				 if (getFieldOrganismId) {
					 lei.setOrganismId(rs.getInt("organism_id"));
				 } 
				 alToReturn.add(lei);
			 }
			
		 } catch (Exception ex) {
			 UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		 } finally {
			 UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			 if (closeConn) {
				 DatabaseConf.freeConnection(conn, methodNameToReport);
			 }
		 }// try
		 
		 return alToReturn;
		 
		 
		// OLD
//		//args to check
//		//no
//		//mandatory args
//		//no
//		
//		// build command
//		
//		// select columns
//		//int
//		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
//		chm_alColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
//		if(getFieldOrganismId){chm_alColumnSelectAsInt_aliase2columnName.put("organism_id", "organism_id");}
//		chm_alColumnSelectAsInt_aliase2columnName.put("size", "size");
//		//String
//		ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
//		chm_alColumnSelectAsString_aliase2columnName.put("type", "type");
//		chm_alColumnSelectAsString_aliase2columnName.put("accession", "accession");
//
//		ArrayList<String> tableFrom = new ArrayList<String>();
//		tableFrom.add("elements");
//		String whereClause = "";
//		if(keySetOrganismId != null && ! keySetOrganismId.isEmpty()){
//				whereClause = "WHERE organism_id IN ("+UtilitiesMethodsShared.getItemsAsStringFromCollection(keySetOrganismId)+")";
//		}
//		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
//				chm_alColumnSelectAsInt_aliase2columnName,
//				chm_alColumnSelectAsString_aliase2columnName,
//				tableFrom, whereClause);
//
//		//run command
//		ArrayList<LightElementItem> alToReturn = UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS(
//						conn,
//						LightElementItem.class,
//						objSQLCommandIT, 
//						true, 
//						true, 
//						methodNameToReport);
//
//		return alToReturn;
		
	}


	public static ArrayList<AbsoPropQElemItem> getAlAbsoPropQElemItemWithOrgaId_orderByLenghtDesc(
			Connection conn
			, int organismId
			, boolean getNumberGenes
			, boolean orderByLenghtDesc
			) throws Exception {

		String methodNameToReport = "QueriesTableElements getAlAbsoPropQElemItemWithOrgaId_orderByLenghtDesc"
				+ " ; organismId="+organismId
				+ " ; getNumberGenes="+getNumberGenes
				+ " ; orderByLenghtDesc="+orderByLenghtDesc
				;
		
		ArrayList<AbsoPropQElemItem> alToReturn = new ArrayList<AbsoPropQElemItem>();
		
		//args to check
		//no
		//mandatory args
		if (organismId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("organismId <= 0")
					);
		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT organism_id, element_id, type, accession, size"
					+ " FROM elements"
					+ " WHERE organism_id = "
					+ organismId;
			if (orderByLenghtDesc) {
				command += " ORDER BY size DESC";
			}
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			int totalSizePbForOrga = 0;
			while (rs.next()) {
				AbsoPropQElemItem apqeiIT = new AbsoPropQElemItem();
				apqeiIT.setqAccnum(rs.getString("accession"));
				apqeiIT.setqOrigamiElementId(rs.getInt("element_id"));
				apqeiIT.setqElementType(rs.getString("type"));
				apqeiIT.setqOrigamiOrganismId(organismId);
				apqeiIT.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
				apqeiIT.setQsizeOfElementinPb(rs.getInt("size"));
				apqeiIT.setqPbStartOfElementInOrga(totalSizePbForOrga);
				totalSizePbForOrga += apqeiIT.getQsizeOfElementinPb();
				alToReturn.add(apqeiIT);
			}

			// set setQsizeOfOragnismInPb, qPercentStart et qPercentStop
			for (int i = 0; i < alToReturn.size(); i++) {
				AbsoPropQElemItem apqeiIT = alToReturn.get(i);
				apqeiIT.setQsizeOfOragnismInPb(totalSizePbForOrga);
				apqeiIT.setqPercentStart((double) apqeiIT
						.getqPbStartOfElementInOrga()
						/ (double) totalSizePbForOrga);
				apqeiIT.setqPercentStop(((double) apqeiIT
						.getqPbStartOfElementInOrga() + (double) apqeiIT
						.getQsizeOfElementinPb())
						/ (double) totalSizePbForOrga);
				// get number genes
				if (getNumberGenes) {
					apqeiIT.setqElementNumberGene(QueriesTableGenes.getNumberGenesWithElementId_optionalStartStopBoundaries(
							conn
							, apqeiIT.getqOrigamiElementId()
							, -1
							, -1
						)
					);
				}
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return alToReturn;
		
		/*
		 * Mock data ArrayList<AbsoluteProportionQElementItem> alITElementItemQ
		 * = new ArrayList<AbsoluteProportionQElementItem>();
		 * 
		 * AbsoluteProportionQElementItem apei1 = new
		 * AbsoluteProportionQElementItem(); apei1.setqAccnum("NC_000235");
		 * apei1.setqElementId(23);
		 * apei1.setqEnumBlockType(AbsoluteProportionQComparableQSpanItem
		 * .QEnumBlockType.Q_INSERTION);
		 * apei1.setqElementType(AbsoluteProportionQElementItem
		 * .ElementEnumBlockType.CHROMOSOME); apei1.setqPercentStart(0);
		 * apei1.setqPercentStop(0.67); apei1.setqElementNumberGene(5476);
		 * alITElementItemQ.add(apei1);
		 * 
		 * AbsoluteProportionQElementItem apei2 = new
		 * AbsoluteProportionQElementItem(); apei2.setqAccnum("NC_000098");
		 * apei2.setqElementId(12);
		 * apei2.setqEnumBlockType(AbsoluteProportionQComparableQSpanItem
		 * .QEnumBlockType.Q_INSERTION);
		 * apei2.setqElementType(AbsoluteProportionQElementItem
		 * .ElementEnumBlockType.PLASMID); apei2.setqPercentStart(0.67);
		 * apei2.setqPercentStop(1); apei2.setqElementNumberGene(9103);
		 * alITElementItemQ.add(apei2);
		 */
		
	}


	public static ArrayList<AbsoPropSElemItem> getAlAbsoPropSElemItemWithOrgaId_orderByLenghtDesc(
			Connection conn
			, int organismId
			, boolean getNumberGenes
			, boolean orderByLenghtDesc
			) throws Exception {


		String methodNameToReport = "QueriesTableElements getAlAbsoPropSElemItemWithOrgaId_orderByLenghtDesc"
				+ " ; organismId="+organismId
				+ " ; getNumberGenes="+getNumberGenes
				+ " ; orderByLenghtDesc="+orderByLenghtDesc
				;
		
		ArrayList<AbsoPropSElemItem> alToReturn = new ArrayList<AbsoPropSElemItem>();
		
		//args to check
		//no
		//mandatory args
		if (organismId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("organismId <= 0")
					);
		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT organism_id, element_id, type, accession, size"
					+ " FROM elements"
					+ " WHERE organism_id = "
					+ organismId;
			if (orderByLenghtDesc) {
					command += " ORDER BY size DESC";
			}
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			int totalSizePbForOrga = 0;

			while (rs.next()) {

				AbsoPropSElemItem apseiIT = new AbsoPropSElemItem();
				apseiIT.setsAccnum(rs.getString("accession"));
				apseiIT.setsOrigamiElementId(rs.getInt("element_id"));
				apseiIT.setsElementType(rs.getString("type"));
				apseiIT.setsOrigamiOrganismId(organismId);
				apseiIT.setSEnumBlockType(SEnumBlockType.S_INSERTION);
				apseiIT.setsSizeOfElementinPb(rs.getInt("size"));
				apseiIT.setsPbStartOfElementInOrga(totalSizePbForOrga);
	
				totalSizePbForOrga += apseiIT.getsSizeOfElementinPb();

				alToReturn.add(apseiIT);
			}

			// set setQsizeOfOragnismInPb, qPercentStart et qPercentStop
			for (int i = 0; i < alToReturn.size(); i++) {
				AbsoPropSElemItem apseiIT = alToReturn.get(i);
				apseiIT.setsSizeOfOragnismInPb(totalSizePbForOrga);
				apseiIT.setsPercentStart((double) apseiIT
							.getsPbStartOfElementInOrga()
							/ (double) totalSizePbForOrga);
				apseiIT.setsPercentStop(((double) apseiIT
							.getsPbStartOfElementInOrga() + (double) apseiIT
							.getsSizeOfElementinPb())
							/ (double) totalSizePbForOrga);
				
				// get number genes
				if (getNumberGenes) {
					apseiIT.setsElementNumberGene(QueriesTableGenes.getNumberGenesWithElementId_optionalStartStopBoundaries(
							conn
							, apseiIT.getsOrigamiElementId()
							, -1
							, -1
						)
					);
				}

			}
			
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return alToReturn;

	}

	

	public static ArrayList<Integer> getUniqListEletIdFromAlOrgaAndEletIds(
			Connection conn
			, ArrayList<Integer> alRefOrganismIds
			, ArrayList<Integer> alRefElementIds
			) throws Exception {
		
		ArrayList<Integer> alEletIdsFromListOrgaIds = new ArrayList<Integer>();
		HashSet<Integer> hs = new HashSet<Integer>();
		
		if(alRefOrganismIds != null){
			for(int orgaIdIT : alRefOrganismIds){
				alEletIdsFromListOrgaIds.addAll(
						getAlElementIdWithOrganismId_optionalOrderBySizeDesc(conn, orgaIdIT, true, true)
						);
			}
			hs.addAll(alEletIdsFromListOrgaIds);
		}
		if(alRefElementIds != null){
			hs.addAll(alRefElementIds);
		}
		alEletIdsFromListOrgaIds.clear();
		alEletIdsFromListOrgaIds.addAll(hs);
		return alEletIdsFromListOrgaIds;
	}
	
	
	public static String getDetailledElementInfoAsStringWithOrigamiElementId(
			Connection conn
			, int elementId
			) throws Exception {

		 String methodNameToReport = "QueriesTableElements getDetailledElementInfoAsStringWithOrigamiElementId"
				+ " ; elementId="+elementId
				;
		
		
		 String stringToReturn = "";

		 Statement statement = null;
		 ResultSet rs = null;
		 boolean closeConn = false;
		
		 try {
		
			 if (conn == null) {
				 conn = DatabaseConf.getConnection_db();
				 closeConn = true;
			 }
			 statement = conn.createStatement();

			String command = "select count(gene_id) as counted_genes from genes where element_id = "
						+ elementId;
			 rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			if (rs.next()) {
				int numberGenes = rs.getInt("counted_genes");
				if(numberGenes > 0){
					stringToReturn += "<big><i>Number of CDS in element : </i></big>"
							+ numberGenes + "<br/>";
				}
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("more than 1 row returned for count genes of element id = "+elementId));
				}
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("no count returned for element id = "+elementId));
			}

			//command = "select date_seq, version from elements where element_id = " + elementId;
			command = "select sequences.date_seq, sequences.version from elements, sequences where elements.element_id = " + elementId + " AND elements.accession = sequences.accession";
			
			rs.close();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			if (rs.next()) {
				String dateSeq = rs.getString("date_seq");
				int version = rs.getInt("version");
				if(dateSeq != null){
					stringToReturn += "<big><i>Date of sequence : </i></big>"
							+ dateSeq + "<br/>";
				}
				if(version >= 0){
					stringToReturn += "<big><i>Version of sequence : </i></big>"
							+ version + "<br/>";
				}
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("more than 1 row returned for element id = "+elementId));
				}
				
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("no row returned for element id = "+elementId));
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return stringToReturn;

	}
	
	
	
	public static Integer getSizeElementWithElementId(
			Connection conn
			, int elementId
			) throws Exception {

		String methodNameToReport = "QueriesTableElements getSizeElementWithElementId"
				+ " ; elementId="+elementId
				;
		
		//args to check
		//no
		//mandatory args
		if (elementId <= 0) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("elementId <= 0")
					);
		}
		
		int sizeElementToReturn = -1;

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}

			statement = conn.createStatement();

			String command = "SELECT size FROM elements WHERE element_id = "+elementId;

			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			if (rs.next()) {
				sizeElementToReturn = rs.getInt("size");
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
			return null;
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		if(sizeElementToReturn > 0){
			return sizeElementToReturn;
		}else{
			UtilitiesMethodsServer.reportException(methodNameToReport,new Exception("sizeElementToReturn not > 0 : "+sizeElementToReturn));
			return null;
		}

	}
	
	
	
}
