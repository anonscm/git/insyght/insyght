package fr.inra.jouy.server.queriesTable;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentParams.AlignParamsTmpObjQElementIdAndSElementId;
import fr.inra.jouy.shared.TransAbsoPropQSSyntItem;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.pojos.transitComposite.ConvertSetLongToSortedRangeItem;
import fr.inra.jouy.shared.pojos.transitComposite.PairLongLowerHigherRange;

public class QueriesTableAlignments {


	// inner special object
	
	public static class AlignmentsTmpObjAlignmentParamIdAndPairs {
		private Long alignmentParamId = (long) 0;
		private int pairs = -1;
		public AlignmentsTmpObjAlignmentParamIdAndPairs(){
		}
		public Long getAlignmentParamId() {
			return alignmentParamId;
		}
		public void setAlignmentParamId(Long alignmentParamId) {
			this.alignmentParamId = alignmentParamId;
			
		}
		public int getPairs() {
			return pairs;
		}
		public void setPairs(int pairs) {
			this.pairs = pairs;
			
		}
	}


	static public HashSet<Long> getListAlignmentIdsWithAlAlignmentParamId(
			Connection conn
			, HashSet<Long> hsAlignmentParamIdSent
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, boolean shouldBePresentInTable
			) throws Exception {

		String methodName = "QueriesTableAlignments getListAlignmentIdsWithAlAlignmentParamId"
				+ " ; hsAlignmentParamIdSent="+ ( (hsAlignmentParamIdSent != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(hsAlignmentParamIdSent) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+ convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; shouldBePresentInTable="+ shouldBePresentInTable
				;
		
		HashSet<Long> hsToReturn = new HashSet<>();
		
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				hsAlignmentParamIdSent
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);
//		ArrayList<Long> alPositiveAlignmentParamId = new ArrayList<>();
//		ArrayList<Long> alNegativeAlignmentParamId = new ArrayList<>();
//		for (Long alignmentParamIdIT : alAlignmentParamIdSent) {
//			if (alignmentParamIdIT < 0) {
//				alNegativeAlignmentParamId.add(-alignmentParamIdIT);
//			} else {
//				alPositiveAlignmentParamId.add(alignmentParamIdIT);
//			}
//		}

		//if ( ! alPositiveAlignmentParamId.isEmpty()) {
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty()
				|| ! csltsriIT.getPositiveRangeAsPair().isEmpty()
				) {
			hsToReturn.addAll(
					getListAlignmentIdsWithAlPositiveAlignmentParamId(
							conn
							//, alPositiveAlignmentParamId
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, false
							)
					);
		}
		//if ( ! alNegativeAlignmentParamId.isEmpty()) {
		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty()
				|| ! csltsriIT.getNegativeRangeAsPair().isEmpty()
				) {
			hsToReturn.addAll(
					getListAlignmentIdsWithAlPositiveAlignmentParamId(
							conn
							//, alNegativeAlignmentParamId
							, csltsriIT.getNegativeSingletonAsIndividual()
							, csltsriIT.getNegativeRangeAsPair()
							, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, true
							)
					);
		}
		if (shouldBePresentInTable && hsToReturn.isEmpty()) {
			UtilitiesMethodsServer.reportException(
					methodName
					, new Exception("shouldBePresentInTable is true && alToReturn.isEmpty()")
					);
		}
		return hsToReturn;
	}



	
	
	private static ArrayList<Long> getListAlignmentIdsWithAlPositiveAlignmentParamId(
			Connection conn
			//, ArrayList<Long> alPostiveAlignmentParamIdSent
			, HashSet<Long> postiveAlignmentParamIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentParamIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, boolean storeAsNegativeValue
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignments getListAlignmentIdsWithAlPositiveAlignmentParamId"
				+ " ; postiveAlignmentParamIdSentSingletonAsIndividual="+ ( (postiveAlignmentParamIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentParamIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentParamIdSentRangeAsPair="+ 
				( (postiveAlignmentParamIdSentRangeAsPair != null ) ? 
					postiveAlignmentParamIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; storeAsNegativeValue="+storeAsNegativeValue
				;
		
		ArrayList<Long> alToReturn = new ArrayList<Long>();
		// check valid input
		//no
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT alignment_id, alignment_param_id"
					+ " FROM alignments WHERE";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentParamIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_param_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentParamIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentParamIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentParamIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_param_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_param_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}

			
			if ( ! atLeastOneValueToSearch) {
				return alToReturn;
			}
						
			// System.out.println(command);
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				Long alignmentParamIdFromQuery = rs.getLong("alignment_param_id");
				
				if ( ! postiveAlignmentParamIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentParamIdSentSingletonAsIndividual.contains(alignmentParamIdFromQuery)
							|| cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual.contains(alignmentParamIdFromQuery)
							) {
						//alignmentParamIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				Long alignmentIdFromQuery = rs.getLong("alignment_id");
				if (storeAsNegativeValue) {
					alignmentIdFromQuery = -alignmentIdFromQuery;
					//alignmentParamIdFromQuery = -alignmentParamIdFromQuery;
				}
				alToReturn.add(alignmentIdFromQuery);
			}
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
				
		return alToReturn;
	}

	
	public static HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> getHMalignmentId2AlignmentParamIdAndPairs_WithSetAlignmentParamId(
			Connection conn
			, Set<Long> setAlignmentParamId
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, boolean shouldBePresentInTable
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignments getHMalignmentId2AlignmentParamIdAndPairs_WithSetAlignmentParamId"
				+ " ; alPostiveAlignmentParamIdSent="+ ( (setAlignmentParamId != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(setAlignmentParamId) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> hmToReturn = new HashMap<>();

//		long milli = System.currentTimeMillis();
//		long milliPrint2 = -1;
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Start getHMalignmentId2AlignmentParamIdAndPairs_WithSetAlignmentParamId ; setAlignmentParamId.size() = "+setAlignmentParamId.size());
		
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				setAlignmentParamId
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);
//		ArrayList<Long> alPositiveAlignmentParamId = new ArrayList<>();
//		ArrayList<Long> alNegativeAlignmentParamId = new ArrayList<>();
//		for (Long alignmentParamIdIT : setAlignmentParamId) {
//			if (alignmentParamIdIT == null) {
//				UtilitiesMethodsServer.reportException(
//						methodNameToReport
//						, new Exception("alignmentParamIdIT is null :"+alignmentParamIdIT)
//						);
//			} else if (alignmentParamIdIT < 0) {
//				alNegativeAlignmentParamId.add(-alignmentParamIdIT);
//			} else {
//				alPositiveAlignmentParamId.add(alignmentParamIdIT);
//			}
//		}


//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println(
//				"Done convertSetLongToSortedRange\t" + milliPrint2 + "\tmilliseconds"
//				+ " ; alNegativeAlignmentParamId.size()="+alNegativeAlignmentParamId.size()
//				+ " ; alPositiveAlignmentParamId.size()="+alPositiveAlignmentParamId.size()
//			);
		
		//if ( ! alPositiveAlignmentParamId.isEmpty()) {
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			hmToReturn.putAll(
					getHMalignmentId2AlignmentParamIdAndPairsWithAlPositiveAlignmentParamId(
							conn
							//, alPositiveAlignmentParamId
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, false
							)
					);
		}

		
		//if ( ! alNegativeAlignmentParamId.isEmpty()) {
		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> mirrorHm = getHMalignmentId2AlignmentParamIdAndPairsWithAlPositiveAlignmentParamId(
					conn
					//, alNegativeAlignmentParamId
					, csltsriIT.getNegativeSingletonAsIndividual()
					, csltsriIT.getNegativeRangeAsPair()
					, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
					, convertSetLongToSortedRangeItem_allowGapWithinRange
					, true
					);
			for (Map.Entry<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> entryMirrorHm : mirrorHm.entrySet()) {
			    //entryMirrorHm.getKey()
				//entryMirrorHm.getValue();
				if (hmToReturn.containsKey(entryMirrorHm.getKey())) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("hmToReturn.containsKey(entryMirrorHm.getKey()) :"
									+ " ; entryMirrorHm.getKey()="+entryMirrorHm.getKey()
									+ " ; entryMirrorHm.getValue().getAlignmentParamId()="+entryMirrorHm.getValue().getAlignmentParamId()
									+ " ; hmToReturn.get(entryMirrorHm.getKey()).getAlignmentParamId()="+hmToReturn.get(entryMirrorHm.getKey()).getAlignmentParamId()
									)
							);
				} else {
					hmToReturn.put(entryMirrorHm.getKey(), entryMirrorHm.getValue());
				}
			}
			
//			hmToReturn.putAll(
//					getHMalignmentId2AlignmentParamIdAndPairsWithAlPositiveAlignmentParamId(
//							conn
//							//, alNegativeAlignmentParamId
//							, csltsriIT.getNegativeSingletonAsIndividual()
//							, csltsriIT.getNegativeRangeAsPair()
//							, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
//							, convertSetLongToSortedRangeItem_allowGapWithinRange
//							, true
//							)
//					);
		}
		
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Done getHMalignmentId2AlignmentParamIdAndPairs_WithSetAlignmentParamId\t" + milliPrint2 + "\tmilliseconds");
		
		
		if (shouldBePresentInTable && hmToReturn.isEmpty()) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("shouldBePresentInTable is true && hmToReturn.isEmpty()")
					);
		}
		
		return hmToReturn;
		
	}
	
	

//	private static HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> getHMalignmentId2AlignmentParamIdAndPairsWithAlPositiveAlignmentParamIdSingletonAsIndividualAndRangeAsPair(
//			Connection conn
//			, ArrayList<Long> alPositiveAlignmentParamIdSentSingletonAsIndividual
//			, ArrayList<Long> alPositiveAlignmentParamIdSentRangeAsPair
//			, boolean storeAsNegativeValue
//			) throws Exception {
//		String methodNameToReport = "QueriesTableAlignments getHMalignmentId2AlignmentParamIdAndPairsWithAlPositiveAlignmentParamIdSingletonAsIndividualAndRangeAsPair"
//				+ " ; alPositiveAlignmentParamIdSentSingletonAsIndividual="+ ( (alPositiveAlignmentParamIdSentSingletonAsIndividual != null ) ? alPositiveAlignmentParamIdSentSingletonAsIndividual.toString() : "NULL" )
//				+ " ; alPositiveAlignmentParamIdSentRangeAsPair="+ ( (alPositiveAlignmentParamIdSentRangeAsPair != null ) ? alPositiveAlignmentParamIdSentRangeAsPair.toString() : "NULL" )
//				+ " ; storeAsNegativeValue="+storeAsNegativeValue
//				;
//
//		HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> hmToReturn = new HashMap<>();
//
//		// check valid input
//		//no
//
//		Statement statement = null;
//		ResultSet rs = null;
//		boolean closeConn = false;
//
//		try {
//			if (conn == null) {
//				conn = DatabaseConf.getConnection_db();
//				closeConn = true;
//			}
//			statement = conn.createStatement();
//
//			String command = "SELECT alignment_id, alignment_param_id, pairs"
//					+ " FROM alignments WHERE ";
//			boolean alreadyWhereCondition = false;
//			if ( ! alPositiveAlignmentParamIdSentSingletonAsIndividual.isEmpty()) {
//				command += " ( alignment_param_id IN ( "
//					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alPositiveAlignmentParamIdSentSingletonAsIndividual)
//					+ ") ) ";
//				alreadyWhereCondition = true;
//			}
//			if ( ! alPositiveAlignmentParamIdSentRangeAsPair.isEmpty()) {
//				for ( int i=0; i < alPositiveAlignmentParamIdSentRangeAsPair.size() ; i=i+2) {
//					if (i == alPositiveAlignmentParamIdSentRangeAsPair.size() - 1) {
//						UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("Error in parsing alPositiveAlignmentParamIdSentRangeAsPair : i == alPositiveAlignmentParamIdSentRangeAsPair.size() - 1"
//								+ " ; i="+i
//								+ " ; alPositiveAlignmentParamIdSentRangeAsPair.size()="+alPositiveAlignmentParamIdSentRangeAsPair.size()
//								+ " ; alPositiveAlignmentParamIdSentRangeAsPair="+alPositiveAlignmentParamIdSentRangeAsPair.toString()));
//					} else {
//						Long lowerRangeIT = alPositiveAlignmentParamIdSentRangeAsPair.get(i);
//						Long higherRangeIT = alPositiveAlignmentParamIdSentRangeAsPair.get(i+1);
//						if (lowerRangeIT >= higherRangeIT) {
//							UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("Error in parsing alPositiveAlignmentParamIdSentRangeAsPair : lowerRangeIT >= higherRangeIT"
//									+ " ; i="+i
//									+ " ; lowerRangeIT="+lowerRangeIT
//									+ " ; higherRangeIT="+higherRangeIT
//									+ " ; alPositiveAlignmentParamIdSentRangeAsPair="+alPositiveAlignmentParamIdSentRangeAsPair.toString()));
//						} else {
//							if (alreadyWhereCondition) {
//								command += " OR ";
//							}
//							command += " ( alignment_param_id >= "+lowerRangeIT+" AND alignment_param_id <= "+higherRangeIT+" ) ";
//							alreadyWhereCondition = true;
//						}
//					}
//				}
//			}
//			
//			
//			
//			// System.out.println(command);
//			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
//					, methodNameToReport
//					);
//
//			
//			
//			while (rs.next()) {
//				Long alignmentIdFromQuery = rs.getLong("alignment_id");
//				Long alignmentParamIdFromQuery = rs.getLong("alignment_param_id");
//				int numberGenesIT = rs.getInt("pairs");
//
//				if (storeAsNegativeValue) {
//					alignmentIdFromQuery = -alignmentIdFromQuery;
//					alignmentParamIdFromQuery = -alignmentParamIdFromQuery;
//				}
//				AlignmentsTmpObjAlignmentParamIdAndPairs atoapiapIT = new AlignmentsTmpObjAlignmentParamIdAndPairs();
//				atoapiapIT.setAlignmentParamId(alignmentParamIdFromQuery);
//				atoapiapIT.setPairs(numberGenesIT);
//				hmToReturn.put(alignmentIdFromQuery, atoapiapIT);
//
//			}
//
////			milliPrint2 = System.currentTimeMillis() - milli;
////			System.out.println("HERE Done ...WithAlPositiveAlignmentParamId... \t" + milliPrint2 + "\tmilliseconds"
////					+ " ; alPositiveAlignmentParamIdSentSingletonAsIndividual.size()"+alPositiveAlignmentParamIdSentSingletonAsIndividual.size()
////					+ " ; alPositiveAlignmentParamIdSentRangeAsPair.size()"+alPositiveAlignmentParamIdSentRangeAsPair.size()
////					);
//			
//		} catch (Exception ex) {
//			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
//		} finally {
//			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
//			if (closeConn) {
//				DatabaseConf.freeConnection(conn, methodNameToReport);
//			}
//		}// try
//	
//		return hmToReturn;
//	}
		
		
	
	private static HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> getHMalignmentId2AlignmentParamIdAndPairsWithAlPositiveAlignmentParamId(
			Connection conn
			//, ArrayList<Long> alPostiveAlignmentParamIdSent
			, HashSet<Long> postiveAlignmentParamIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentParamIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, boolean storeAsNegativeValue
			) throws Exception {
		String methodNameToReport = "QueriesTableAlignments getHMalignmentId2AlignmentParamIdAndPairsWithAlPositiveAlignmentParamId"
				//+ " ; alPostiveAlignmentParamIdSent="+ ( (alPostiveAlignmentParamIdSent != null ) ? alPostiveAlignmentParamIdSent.toString() : "NULL" )
				+ " ; postiveAlignmentParamIdSentSingletonAsIndividual="+ ( (postiveAlignmentParamIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentParamIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentParamIdSentRangeAsPair="+ 
				( (postiveAlignmentParamIdSentRangeAsPair != null ) ? 
					postiveAlignmentParamIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; storeAsNegativeValue="+storeAsNegativeValue
				;
		
		HashMap<Long, AlignmentsTmpObjAlignmentParamIdAndPairs> hmToReturn = new HashMap<>();

		// check valid input
		//no

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

//			String command = "";
//			Collections.sort(alPostiveAlignmentParamIdSent);
//			HashSet<Long> isQueryingByRangeWorthIt = UtilitiesMethodsServer.isQueryingByRangeWorthIt(alPostiveAlignmentParamIdSent);
//			if ( isQueryingByRangeWorthIt != null ) {
//
//				command = "SELECT alignment_id, alignment_param_id, pairs"
//				+ " FROM alignments WHERE alignment_param_id >= "
//				+ alPostiveAlignmentParamIdSent.get(0)
//				+ " AND alignment_param_id <= "
//				+ alPostiveAlignmentParamIdSent.get(alPostiveAlignmentParamIdSent.size()-1)
//				;
//			} else {
//				command = "SELECT alignment_id, alignment_param_id, pairs"
//						+ " FROM alignments WHERE alignment_param_id IN ( "
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alPostiveAlignmentParamIdSent)
//						+ ")";
//			}

			String command = "SELECT alignment_id, alignment_param_id, pairs"
					+ " FROM alignments WHERE";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentParamIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_param_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentParamIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentParamIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentParamIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_param_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_param_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}

			
			if ( ! atLeastOneValueToSearch) {
				return hmToReturn;
			}
			
//			long milli = System.currentTimeMillis();
//			long milliPrint2 = -1;
			//milliPrint2 = System.currentTimeMillis() - milli;
			//System.out.println("Start command \n"+command);
			
			
			// System.out.println(command);
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);

			
			while (rs.next()) {
				Long alignmentParamIdFromQuery = rs.getLong("alignment_param_id");
//				if (isQueryingByRangeWorthIt != null) {
//					if ( ! isQueryingByRangeWorthIt.contains(alignmentParamIdFromQuery)) {
//						continue;
//					}
//				}
				if ( ! postiveAlignmentParamIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentParamIdSentSingletonAsIndividual.contains(alignmentParamIdFromQuery)
							|| cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual.contains(alignmentParamIdFromQuery)
							) {
						//alignmentParamIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				
				Long alignmentIdFromQuery = rs.getLong("alignment_id");
				int numberGenesIT = rs.getInt("pairs");

				if (storeAsNegativeValue) {
					alignmentIdFromQuery = -alignmentIdFromQuery;
					alignmentParamIdFromQuery = -alignmentParamIdFromQuery;
				}
				AlignmentsTmpObjAlignmentParamIdAndPairs atoapiapIT = new AlignmentsTmpObjAlignmentParamIdAndPairs();
				atoapiapIT.setAlignmentParamId(alignmentParamIdFromQuery);
				atoapiapIT.setPairs(numberGenesIT);
				hmToReturn.put(alignmentIdFromQuery, atoapiapIT);

			}

//			milliPrint2 = System.currentTimeMillis() - milli;
//			System.out.println("HERE Done ...WithAlPositiveAlignmentParamId... \t" + milliPrint2 + "\tmilliseconds"
//					+ " ; alPostiveAlignmentParamIdSent.size()"+alPostiveAlignmentParamIdSent.size()
//					);
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
	
		return hmToReturn;
	}

	

	public static ArrayList<TransAbsoPropQSSyntItem> getAlTransAbsoPropQSSyntItem_WithalignmentParamId2QElementIdAndSElementId(
			Connection conn,
			HashMap<Long, AlignParamsTmpObjQElementIdAndSElementId> alignmentParamId2QElementIdAndSElementId
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			//, boolean shouldBePresentInTable //impossible to check efficiently if each alignment params yield at least 1 synteny item
			) throws Exception {
		
//		String methodNameToReport = "QueriesTableAlignments getAlTransAbsoPropQSSyntItem_WithalignmentParamId2QElementIdAndSElementId"
//				+ " ; keySet alignmentParamId2QElementIdAndSElementId="+alignmentParamId2QElementIdAndSElementId.keySet().toString()
//				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
//				//+ " ; shouldBePresentInTable="+shouldBePresentInTable
//				;

		
		ArrayList<TransAbsoPropQSSyntItem> alToReturn = new ArrayList<TransAbsoPropQSSyntItem>();
		
//		ArrayList<Long> alPositiveAlignmentParamId = new ArrayList<>();
//		ArrayList<Long> alNegativeAlignmentParamId = new ArrayList<>();
//		for (Long alignmentParamIdIT : alignmentParamId2QElementIdAndSElementId.keySet()) {
//			if (alignmentParamIdIT == null) {
//				UtilitiesMethodsServer.reportException(
//						methodNameToReport
//						, new Exception("alignmentParamIdIT is null :"+alignmentParamIdIT)
//						);
//			} else if (alignmentParamIdIT < 0) {
//				alNegativeAlignmentParamId.add(-alignmentParamIdIT);
//			} else {
//				alPositiveAlignmentParamId.add(alignmentParamIdIT);
//			}
//		}
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				alignmentParamId2QElementIdAndSElementId.keySet()
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);


//		if (alignmentParamId2QElementIdAndSElementId.keySet().toString().compareTo("[-5123846181, -5123846182]")==0) {
//			System.err.println("HERE "
//					+ "getPositiveSingletonAsIndividual="+csltsriIT.getPositiveSingletonAsIndividual().toString()
//					+ "getPositiveRangeAsPair="+csltsriIT.getPositiveRangeAsPair().toString()
//					+ "getCumulatedPositiveLongWithinRangesAsIndividual="+csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual().toString()
//					+ "getNegativeSingletonAsIndividual="+csltsriIT.getNegativeSingletonAsIndividual().toString()
//					+ "getNegativeRangeAsPair="+csltsriIT.getNegativeRangeAsPair().toString()
//					+ "getCumulatedNegativeLongWithinRangesAsIndividual="+csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().toString()
//					);
//			for (PairLongLowerHigherRange nrap : csltsriIT.getNegativeRangeAsPair()) {
//				System.err.println("HERE "
//						+ " ; getHigherRange="+nrap.getHigherRange()
//						+ " ; getLowerRange="+nrap.getLowerRange()
//						);
//			}
//		}
		
		//if ( ! alPositiveAlignmentParamId.isEmpty()) {
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			alToReturn.addAll(
					getAlTransAbsoPropQSSyntItem_WithAlPositiveAlignmentParamId(
							conn
							//, alPositiveAlignmentParamId
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, alignmentParamId2QElementIdAndSElementId
							, false
							)
					);
		}
		//if ( ! alNegativeAlignmentParamId.isEmpty()) {
		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			alToReturn.addAll(
					getAlTransAbsoPropQSSyntItem_WithAlPositiveAlignmentParamId(
							conn
							//, alNegativeAlignmentParamId
							, csltsriIT.getNegativeSingletonAsIndividual()
							, csltsriIT.getNegativeRangeAsPair()
							, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, alignmentParamId2QElementIdAndSElementId
							, true
							)
					);
		}

		return alToReturn;
		
	}

	private static ArrayList<TransAbsoPropQSSyntItem> getAlTransAbsoPropQSSyntItem_WithAlPositiveAlignmentParamId (
			Connection conn
			//, ArrayList<Long> alPositiveAlignmentParamId
			, HashSet<Long> postiveAlignmentParamIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentParamIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, HashMap<Long, AlignParamsTmpObjQElementIdAndSElementId> alignmentParamId2QElementIdAndSElementId
			, boolean storeAsNegativeValue
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignments getAlTransAbsoPropQSSyntItem_WithAlPositiveAlignmentParamId"
				//+ " ; alPositiveAlignmentParamId="+ ( (alPositiveAlignmentParamId != null ) ? alPositiveAlignmentParamId.toString() : "NULL" )
				+ " ; postiveAlignmentParamIdSentSingletonAsIndividual="+ ( (postiveAlignmentParamIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentParamIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentParamIdSentRangeAsPair="+ 
				( (postiveAlignmentParamIdSentRangeAsPair != null ) ? 
					postiveAlignmentParamIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; keySet alignmentParamId2QElementIdAndSElementId="+UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(alignmentParamId2QElementIdAndSElementId.keySet())
				+ " ; storeAsNegativeValue="+storeAsNegativeValue
				;
		
		// check valid input
		//no
		
		ArrayList<TransAbsoPropQSSyntItem> alToReturn = new ArrayList<TransAbsoPropQSSyntItem>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

//			String command = "";
//			Collections.sort(alPositiveAlignmentParamId);
//			HashSet<Long> isQueryingByRangeWorthIt = UtilitiesMethodsServer.isQueryingByRangeWorthIt(alPositiveAlignmentParamId);
//			if ( isQueryingByRangeWorthIt != null ) {
//				command = "SELECT alignment_id, alignment_param_id, pairs, orientation_conserved, score, q_start, q_stop, s_start, s_stop"
//						+ " FROM alignments WHERE alignment_param_id >= "
//						+ alPositiveAlignmentParamId.get(0)
//						+ " AND alignment_param_id <= "
//						+ alPositiveAlignmentParamId.get(alPositiveAlignmentParamId.size()-1)
//						;
//			} else {
//				command = "SELECT alignment_id, alignment_param_id, pairs, orientation_conserved, score, q_start, q_stop, s_start, s_stop"
//						+ " FROM alignments WHERE alignment_param_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alPositiveAlignmentParamId)
//						+ ")";
//			}
			
			String command = "SELECT alignment_id, alignment_param_id, pairs, orientation_conserved, score, q_start, q_stop, s_start, s_stop"
					+ " FROM alignments WHERE";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentParamIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_param_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentParamIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentParamIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentParamIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_param_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_param_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}

			
			if ( ! atLeastOneValueToSearch) {
				return alToReturn;
			}

			// System.out.println(command);
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);
			
			while (rs.next()) {
				Long alignmentParamIdIT = rs.getLong("alignment_param_id");
//				if (isQueryingByRangeWorthIt != null) {
//					if ( ! isQueryingByRangeWorthIt.contains(alignmentParamIdIT)) {
//						continue;
//					}
//				}
				if ( ! postiveAlignmentParamIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentParamIdSentSingletonAsIndividual.contains(alignmentParamIdIT)
							|| cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual.contains(alignmentParamIdIT)
							) {
						//alignmentParamIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				
				TransAbsoPropQSSyntItem apqssiIT = new TransAbsoPropQSSyntItem();
				Long alignmentIdIT = rs.getLong("alignment_id");
				
				if (storeAsNegativeValue) {
					apqssiIT.setSyntenyOrigamiAlignmentId(-alignmentIdIT);
					alignmentParamIdIT = -alignmentParamIdIT;
				} else {
					apqssiIT.setSyntenyOrigamiAlignmentId(alignmentIdIT);
				}
				int qOrigamiElementId = alignmentParamId2QElementIdAndSElementId.get(alignmentParamIdIT).getqElementId();
				apqssiIT.setQsQOrigamiElementId(qOrigamiElementId);
				int sOrigamiElementId = alignmentParamId2QElementIdAndSElementId.get(alignmentParamIdIT).getsElementId();
				apqssiIT.setQsSOrigamiElementId(sOrigamiElementId);
				int numberGenesIT = rs.getInt("pairs");
				apqssiIT.setSyntenyNumberGene(numberGenesIT);
				apqssiIT.setSyntenyScore(rs.getInt("score"));
				apqssiIT.setOrientationConserved(rs.getBoolean("orientation_conserved"));
				int q_start = rs.getInt("q_start");
				int q_stop = rs.getInt("q_stop");
				int s_start = rs.getInt("s_start");
				int s_stop = rs.getInt("s_stop");
				if (q_start <= 0) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("q_start <= 0 :"+q_start+" for alignmentId = "+alignmentIdIT)
							);
				}
				if (q_stop <= 0) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("q_stop <= 0 :"+q_stop+" for alignmentId = "+alignmentIdIT)
							);
				}
				if (s_start <= 0) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("s_start <= 0 :"+s_start+" for alignmentId = "+alignmentIdIT)
							);
				}
				if (s_stop <= 0) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("s_stop <= 0 :"+s_stop+" for alignmentId = "+alignmentIdIT)
							);
				}
				if (storeAsNegativeValue) {
					apqssiIT.setPbQStartSyntenyInElement(s_start);
					apqssiIT.setPbQStopSyntenyInElement(s_stop);
					apqssiIT.setPbSStartSyntenyInElement(q_start);
					apqssiIT.setPbSStopSyntenyInElement(q_stop);
				} else {
					apqssiIT.setPbQStartSyntenyInElement(q_start);
					apqssiIT.setPbQStopSyntenyInElement(q_stop);
					apqssiIT.setPbSStartSyntenyInElement(s_start);
					apqssiIT.setPbSStopSyntenyInElement(s_stop);
				}
				alToReturn.add(apqssiIT);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);

		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return alToReturn;
		
	}

	
	/*
	public static ArrayList<TransAbsoPropQSSyntItem> getListAlignmentsWithAlignmentParamId(
			Connection conn
			, Long alignmentParamIdSent
			, int qOrigamiElementId
			, int sOrigamiElementId
			//, boolean isDataFromMirrorQuery
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignments getListAlignmentsWithAlignmentParamId"
				+ "alignmentParamIdSent="+alignmentParamIdSent
				+ "qOrigamiElementId="+qOrigamiElementId
				+ "sOrigamiElementId="+sOrigamiElementId
				;
		
		ArrayList<TransAbsoPropQSSyntItem> alToReturn = new ArrayList<TransAbsoPropQSSyntItem>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			Long alignmentParamIdForQuery = alignmentParamIdSent;
			if (alignmentParamIdForQuery < 0) {
				alignmentParamIdForQuery = -alignmentParamIdForQuery;
			}
			
			
			String command = "SELECT alignment_id, pairs, orientation_conserved, score, q_start, q_stop, s_start, s_stop"
					+ " FROM alignments WHERE alignment_param_id = "
					+ alignmentParamIdForQuery;

			
			// System.out.println(command);
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);
			
			while (rs.next()) {
				TransAbsoPropQSSyntItem apqssiIT = new TransAbsoPropQSSyntItem();
				Long alignmentIdIT = rs.getLong("alignment_id");
				if (alignmentParamIdSent < 0) {
					apqssiIT.setSyntenyOrigamiAlignmentId(-alignmentIdIT);
				} else {
					apqssiIT.setSyntenyOrigamiAlignmentId(alignmentIdIT);
				}
				apqssiIT.setQsQOrigamiElementId(qOrigamiElementId);
				apqssiIT.setQsSOrigamiElementId(sOrigamiElementId);
				int numberGenesIT = rs.getInt("pairs");
				apqssiIT.setSyntenyNumberGene(numberGenesIT);
				apqssiIT.setSyntenyScore(rs.getInt("score"));
				apqssiIT.setOrientationConserved(rs.getBoolean("orientation_conserved"));
				if (alignmentParamIdSent < 0) {
					apqssiIT.setPbQStartSyntenyInElement(rs.getInt("s_start"));
				} else {
					apqssiIT.setPbQStartSyntenyInElement(rs.getInt("q_start"));
				}
				if (alignmentParamIdSent < 0) {
					apqssiIT.setPbQStopSyntenyInElement(rs.getInt("s_stop"));
				} else {
					apqssiIT.setPbQStopSyntenyInElement(rs.getInt("q_stop"));
				}
				if (alignmentParamIdSent < 0) {
					apqssiIT.setPbSStartSyntenyInElement(rs.getInt("q_start"));
				} else {
					apqssiIT.setPbSStartSyntenyInElement(rs.getInt("s_start"));
				}
				if (alignmentParamIdSent < 0) {
					apqssiIT.setPbSStopSyntenyInElement(rs.getInt("q_stop"));
				} else {
					apqssiIT.setPbSStopSyntenyInElement(rs.getInt("s_stop"));
				}
				//apqssiIT.setDataFromMirrorQuery(isDataFromMirrorQuery);
				alToReturn.add(apqssiIT);
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);

		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return alToReturn;
	}*/

	/*
	public static HashMap<Long, Integer> getHashOfAssociatedPairsNumberWithListAlignmentIds(
			Connection conn
			, ArrayList<Long> alAlignmentIds
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignments getHashOfAssociatedPairsNumberWithListAlignmentIds"
				+ " ; alAlignmentIds="+ ( (alAlignmentIds != null ) ? alAlignmentIds.toString() : "NULL" )
				;
		
		HashMap<Long, Integer> hashToReturn = new HashMap<Long, Integer>();

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			// deal with isDataFromMirrorQuery
			ArrayList<Long> alAlignmentIdsForQuery = new ArrayList<>();
			HashMap<Long, Boolean> negativeAlignmentIds = new HashMap<>();
			for (Long alignmentIdIT : alAlignmentIds) {
				if (alignmentIdIT < 0) {
					alignmentIdIT = -alignmentIdIT;
					alAlignmentIdsForQuery.add(alignmentIdIT);
					negativeAlignmentIds.put(alignmentIdIT, true);
				} else {
					alAlignmentIdsForQuery.add(alignmentIdIT);
				}
			}
			
			
			String command = "SELECT alignment_id, pairs FROM alignments WHERE alignment_id IN ("
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alAlignmentIdsForQuery)
					//+ alAlignmentIds.toString().replaceAll("[\\[\\]]", "")
					+ ")";
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);

			while (rs.next()) {
				Long alignmentIdFromQuery = rs.getLong("alignment_id");
				if (negativeAlignmentIds.containsKey(alignmentIdFromQuery)) {
					alignmentIdFromQuery = -alignmentIdFromQuery;
				}
				hashToReturn.put(alignmentIdFromQuery, rs.getInt("pairs"));

			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {

			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		return hashToReturn;
	}*/




	public static String getDetailledSyntenyInfoAsStringForHTMLWithAlignmentId(
			Connection conn
			, long alignmentId
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignments getDetailledSyntenyInfoAsStringForHTMLWithAlignmentId"
				+ " alignmentId="+alignmentId;
		
		String stringToReturn = "";

		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {

			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			boolean isDataFromMirrorQuery = false;
			long alignmentIdForQuery = alignmentId;
			if (alignmentIdForQuery < 0) {
				isDataFromMirrorQuery = true;
				alignmentIdForQuery = -alignmentIdForQuery;
			}

			String command = "SELECT orthologs, homologs, mismatches, gaps, q_start, q_stop, s_start, s_stop FROM alignments WHERE alignment_id = "
					+ alignmentIdForQuery;
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);

			if (rs.next()) {
				
				//stringToReturn += "<br/><big><i>Score : </i></big>"+ addThousandSeparatorToNumbersInString(rs.getString("score"));
				stringToReturn += "<big><i>Orthologs : </i></big>"
						+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("orthologs"));
				stringToReturn += "<br/><big><i>Homologs : </i></big>"
						+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("homologs"));
				stringToReturn += "<br/><big><i>Mismatches : </i></big>"
						+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("mismatches"));
				stringToReturn += "<br/><big><i>Gaps : </i></big>"
						+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("gaps"));

				if (isDataFromMirrorQuery) {
					stringToReturn += "<br/><big><i>Query start (pb) : </i></big>"+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("s_start"));
					stringToReturn += "<br/><big><i>Query stop (pb) : </i></big>"+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("s_stop"));
					stringToReturn += "<br/><big><i>Subject start (pb) : </i></big>"+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("q_start"));
					stringToReturn += "<br/><big><i>Subject stop (pb) : </i></big>"+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("q_stop"));
				} else {
					stringToReturn += "<br/><big><i>Query start (pb) : </i></big>"+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("q_start"));
					stringToReturn += "<br/><big><i>Query stop (pb) : </i></big>"+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("q_stop"));
					stringToReturn += "<br/><big><i>Subject start (pb) : </i></big>"+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("s_start"));
					stringToReturn += "<br/><big><i>Subject stop (pb) : </i></big>"+ UtilitiesMethodsShared.addThousandSeparatorToNumbersInString(rs.getString("s_stop"));
				}
				
				
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("multiple rows returned for alignment_id = "+alignmentId));

				}
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("no row returned for alignment_id = "+alignmentId));
			}


		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try

		return stringToReturn;
		
	}

	//al idx0 = hsAlignmentParamsId_singleton
	//al idx0 = hsAlignmentParamsId_synteny
	public static ArrayList<HashSet<Long>> getAlHsAlignmentParamId_SingletonIdx0SyntenyIdx1_withHsAlignmentId(
			Connection conn
			, HashSet<Long> hsAlignmentId
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, boolean shouldBePresentInTable
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignments getAlHsAlignmentParamId_SingletonIdx0SyntenyIdx1_withHsAlignmentId"
				+ " ; hsAlignmentId="+ ( (hsAlignmentId != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(hsAlignmentId) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				;
		
		ArrayList<HashSet<Long>> alToReturn = new ArrayList<>();
		HashSet<Long> hsAlignmentParamsId_singleton = new HashSet<>();
		HashSet<Long> hsAlignmentParamsId_synteny = new HashSet<>();
		alToReturn.add(hsAlignmentParamsId_singleton);
		alToReturn.add(hsAlignmentParamsId_synteny);
		
		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				hsAlignmentId
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);

		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			ArrayList<HashSet<Long>> alHsIT = getHsAlignmentParamId_withHsPositiveAlignmentId (
							conn
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, false
							);
			hsAlignmentParamsId_singleton.addAll(alHsIT.get(0));
			hsAlignmentParamsId_synteny.addAll(alHsIT.get(1));		
		}
		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			ArrayList<HashSet<Long>> alHsIT = getHsAlignmentParamId_withHsPositiveAlignmentId(
							conn
							, csltsriIT.getNegativeSingletonAsIndividual()
							, csltsriIT.getNegativeRangeAsPair()
							, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, true
							);
			hsAlignmentParamsId_singleton.addAll(alHsIT.get(0));
			hsAlignmentParamsId_synteny.addAll(alHsIT.get(1));	
		}
		if ( shouldBePresentInTable && hsAlignmentParamsId_singleton.isEmpty() && hsAlignmentParamsId_synteny.isEmpty() ) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("shouldBePresentInTable && hsAlignmentParamsId_singleton.isEmpty() && hsAlignmentParamsId_synteny.isEmpty()")
					);
		}
		

		return alToReturn;
		
	}


	//al idx0 = hsAlignmentParamsId_singleton
	//al idx0 = hsAlignmentParamsId_synteny	
	private static ArrayList<HashSet<Long>> getHsAlignmentParamId_withHsPositiveAlignmentId(
			Connection conn
			, HashSet<Long> postiveAlignmentIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, boolean storeAsNegativeValue
			) throws Exception {

		String methodNameToReport = "QueriesTableAlignments getHsAlignmentParamId_withHsPositiveAlignmentId"
				+ " ; postiveAlignmentIdSentSingletonAsIndividual="+ ( (postiveAlignmentIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentIdSentRangeAsPair="+ 
				( (postiveAlignmentIdSentRangeAsPair != null ) ? 
						postiveAlignmentIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; storeAsNegativeValue="+storeAsNegativeValue
				;
		

		ArrayList<HashSet<Long>> alToReturn = new ArrayList<>();
		HashSet<Long> hsAlignmentParamsId_singleton = new HashSet<>();
		HashSet<Long> hsAlignmentParamsId_synteny = new HashSet<>();
		alToReturn.add(hsAlignmentParamsId_singleton);
		alToReturn.add(hsAlignmentParamsId_synteny);

		
		// check valid input
		//no
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			String command = "SELECT alignment_id, alignment_param_id, pairs"
					+ " FROM alignments WHERE";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}

			
			if ( ! atLeastOneValueToSearch) {
				return alToReturn;
			}
						
			// System.out.println(command);
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			
			while (rs.next()) {
				Long alignmentIdFromQuery = rs.getLong("alignment_id");
				if ( ! postiveAlignmentIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentIdSentSingletonAsIndividual.contains(alignmentIdFromQuery)
							|| cumulatedPostiveAlignmentIdSentWithinRangesAsIndividual.contains(alignmentIdFromQuery)
							) {
					} else {
						continue;
					}
				}
				Long alignmentParamIdFromQuery = rs.getLong("alignment_param_id");
				if (storeAsNegativeValue) {
					//alignmentIdFromQuery = -alignmentIdFromQuery;
					alignmentParamIdFromQuery = -alignmentParamIdFromQuery;
				}
				int pairsIT = rs.getInt("pairs");
				if (pairsIT == 1) {
					hsAlignmentParamsId_singleton.add(alignmentParamIdFromQuery);
				} else {
					hsAlignmentParamsId_synteny.add(alignmentParamIdFromQuery);
				}
			}
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
				
		return alToReturn;
		
	}



	// JDBC can convert from Double to Int directly : https://docs.microsoft.com/en-us/sql/connect/jdbc/understanding-data-type-conversions
	public static HashMap<Long, Integer> getHMAlignmentParamId2cummulativeSumAsIntWithSetAlignmentParamId(
			Connection conn
			, Set<Long> setAlignmentParamId
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, String columnNameTableToSumUp
			, boolean shouldBePresentInTable
			, boolean checkValidFreeStringInput
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignments getHMAlignmentParamId2cummulativeSumAsIntWithSetAlignmentParamId"
				+ " ; setAlignmentParamId="+ ( (setAlignmentParamId != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(setAlignmentParamId) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; columnNameTableToSumUp="+columnNameTableToSumUp
				+ " ; shouldBePresentInTable="+shouldBePresentInTable
				+ " ; checkValidFreeStringInput="+checkValidFreeStringInput
				;
		
		HashMap<Long, Integer> hmToReturn = new HashMap<>();

		ConvertSetLongToSortedRangeItem csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
				setAlignmentParamId
				, convertSetLongToSortedRangeItem_allowGapWithinRange
				);
		
		if ( ! csltsriIT.getPositiveSingletonAsIndividual().isEmpty() || ! csltsriIT.getPositiveRangeAsPair().isEmpty() ) {
			hmToReturn.putAll(
					getHMAlignmentParamId2cummulativeSumAsIntWithSetPositiveAlignmentParamId(
							conn
							//, alPositiveAlignmentParamId
							, csltsriIT.getPositiveSingletonAsIndividual()
							, csltsriIT.getPositiveRangeAsPair()
							, csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual()
							, convertSetLongToSortedRangeItem_allowGapWithinRange
							, columnNameTableToSumUp
							, false
							, checkValidFreeStringInput
							)
					);
		}
		if ( ! csltsriIT.getNegativeSingletonAsIndividual().isEmpty() || ! csltsriIT.getNegativeRangeAsPair().isEmpty() ) {
			HashMap<Long, Integer> mirrorHm = getHMAlignmentParamId2cummulativeSumAsIntWithSetPositiveAlignmentParamId(
					conn
					//, alNegativeAlignmentParamId
					, csltsriIT.getNegativeSingletonAsIndividual()
					, csltsriIT.getNegativeRangeAsPair()
					, csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual()
					, convertSetLongToSortedRangeItem_allowGapWithinRange
					, columnNameTableToSumUp
					, true
					, checkValidFreeStringInput
					);
			for (Map.Entry<Long, Integer> entryMirrorHm : mirrorHm.entrySet()) {
			    //entryMirrorHm.getKey()
				//entryMirrorHm.getValue();
				if (hmToReturn.containsKey(entryMirrorHm.getKey())) {
					UtilitiesMethodsServer.reportException(
							methodNameToReport
							, new Exception("hmToReturn.containsKey(entryMirrorHm.getKey()) :"
									+ " ; entryMirrorHm.getKey()="+entryMirrorHm.getKey()
									+ " ; entryMirrorHm.getValue()="+entryMirrorHm.getValue()
									+ " ; hmToReturn.getValue()="+hmToReturn.get(entryMirrorHm.getKey())
									)
							);
				} else {
					hmToReturn.put(entryMirrorHm.getKey(), entryMirrorHm.getValue());
				}
			}
		}
		if (shouldBePresentInTable && hmToReturn.isEmpty()) {
			UtilitiesMethodsServer.reportException(
					methodNameToReport
					, new Exception("shouldBePresentInTable is true && hmToReturn.isEmpty()")
					);
		}
		return hmToReturn;
		
	}

	// JDBC can convert from Double to Int directly : https://docs.microsoft.com/en-us/sql/connect/jdbc/understanding-data-type-conversions
	private static HashMap<Long, Integer> getHMAlignmentParamId2cummulativeSumAsIntWithSetPositiveAlignmentParamId(
			Connection conn
			//, ArrayList<Long> alAlignmentParamId
			, HashSet<Long> postiveAlignmentParamIdSentSingletonAsIndividual
			, ArrayList<PairLongLowerHigherRange> postiveAlignmentParamIdSentRangeAsPair
			, HashSet<Long> cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual
			, boolean convertSetLongToSortedRangeItem_allowGapWithinRange
			, String columnNameTableToSumUp
			, boolean storeAsNegativeValue
			, boolean checkValidFreeStringInput
			) throws Exception {
		
		String methodNameToReport = "QueriesTableAlignments getHMAlignmentParamId2cummulativeSumAsIntWithSetPositiveAlignmentParamId"
				//+ " ; alAlignmentParamId"+ ( (alAlignmentParamId != null ) ? alAlignmentParamId.toString() : "NULL" )
				+ " ; postiveAlignmentParamIdSentSingletonAsIndividual="+ ( (postiveAlignmentParamIdSentSingletonAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(postiveAlignmentParamIdSentSingletonAsIndividual) : "NULL" )
				+ " ; postiveAlignmentParamIdSentRangeAsPair="+ 
				( (postiveAlignmentParamIdSentRangeAsPair != null ) ? 
					postiveAlignmentParamIdSentRangeAsPair.stream()
					.map(pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange().toString() + ", " + pairLongLowerHigherRangeIT.getHigherRange().toString() +"]")
					.collect(Collectors.toList())
					.toString()
				 : "NULL" )
				+ " ; cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual="+ ( (cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual != null ) ? UtilitiesMethodsShared.get10FirstItemsAsStringFromCollection(cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual) : "NULL" )
				+ " ; convertSetLongToSortedRangeItem_allowGapWithinRange="+convertSetLongToSortedRangeItem_allowGapWithinRange
				+ " ; columnNameTableToSumUp"+columnNameTableToSumUp
				+ " ; storeAsNegativeValue"+storeAsNegativeValue
				+ " ; checkValidFreeStringInput"+checkValidFreeStringInput
				;

		HashMap<Long, Integer> hmToReturn = new HashMap<>();
		
		// check valid input
		if(checkValidFreeStringInput){
			UtilitiesMethodsServer.checkValidFreeStringInput(
					methodNameToReport,
					columnNameTableToSumUp, "columnNameTableToSumUp", false, false);
		}
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

//			String command = "";
//			Collections.sort(alAlignmentParamId);
//			HashSet<Long> isQueryingByRangeWorthIt = UtilitiesMethodsServer.isQueryingByRangeWorthIt(alAlignmentParamId);
//			if ( isQueryingByRangeWorthIt != null ) {
//				command = "SELECT alignment_param_id, SUM("+columnNameTableToSumUp+") as SUM"
//						+ " FROM alignments WHERE alignment_param_id >= "
//						+ alAlignmentParamId.get(0)
//						+ " AND alignment_param_id <= "
//						+ alAlignmentParamId.get(alAlignmentParamId.size()-1)
//						+ " GROUP BY alignment_param_id";
//			} else {
//				command = "SELECT alignment_param_id, SUM("+columnNameTableToSumUp+")"
//						+ " FROM alignments WHERE alignment_param_id IN ("
//						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alAlignmentParamId)
//						+ ")"
//						+ " GROUP BY alignment_param_id"
//						;
//			}
			String command = "SELECT alignment_param_id, SUM("+columnNameTableToSumUp+") as SUM"
					+ " FROM alignments WHERE";
			boolean atLeastOneValueToSearch = false;
			if ( ! postiveAlignmentParamIdSentSingletonAsIndividual.isEmpty()) {
				command += " ( alignment_param_id IN ( "
						+ UtilitiesMethodsShared.getItemsAsStringFromCollection(postiveAlignmentParamIdSentSingletonAsIndividual)
						+ ") )";
				atLeastOneValueToSearch = true;
			}
			if ( ! postiveAlignmentParamIdSentRangeAsPair.isEmpty()) {
				for (PairLongLowerHigherRange pllhrIT : postiveAlignmentParamIdSentRangeAsPair) {
					if (atLeastOneValueToSearch) {
						command += " OR";
					}
					command += " ( alignment_param_id >= "
							+ pllhrIT.getLowerRange()
							+ " AND alignment_param_id <= "
							+ pllhrIT.getHigherRange()
							+ " )"
							;
					atLeastOneValueToSearch = true;
				}
			}
			command += " GROUP BY alignment_param_id";
			
			if ( ! atLeastOneValueToSearch) {
				return hmToReturn;
			}
			
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command
					, methodNameToReport
					);

			while (rs.next()) {
				Long alignmentParamIdFromQuery = rs.getLong("alignment_param_id");
//				if (isQueryingByRangeWorthIt != null) {
//					if ( ! isQueryingByRangeWorthIt.contains(alignmentParamIdFromQuery)) {
//						continue;
//					}
//				}
				if ( ! postiveAlignmentParamIdSentRangeAsPair.isEmpty() && convertSetLongToSortedRangeItem_allowGapWithinRange) {
					if (postiveAlignmentParamIdSentSingletonAsIndividual.contains(alignmentParamIdFromQuery)
							|| cumulatedPostiveAlignmentParamIdSentWithinRangesAsIndividual.contains(alignmentParamIdFromQuery)
							) {
						//alignmentParamIdIsQueryInput = true;
					} else {
						continue;
					}
				}
				int sumIT = rs.getInt("SUM");
				if (storeAsNegativeValue) {
					alignmentParamIdFromQuery = -alignmentParamIdFromQuery;
				}
				hmToReturn.put(alignmentParamIdFromQuery, sumIT);
			}
			
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);

		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
			
		
		// OLD WAY
//		// select columns
//		//int
//		ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
//		//chm_alColumnSelectAsInt_aliase2columnName.put("alignment_param_id", "alignment_param_id");
//		chm_alColumnSelectAsInt_aliase2columnName.put("SUM", "SUM("+columnNameTableToSumUp+")");
//		//String
//		//ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
//		//empty
//		//chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
//		ConcurrentHashMap<String, String> chm_alColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
//		chm_alColumnSelectAsLong_aliase2columnName.put("alignment_param_id", "alignment_param_id");
//		
//		ArrayList<String> tableFrom = new ArrayList<String>();
//		tableFrom.add("alignments");
//		String whereClause = " WHERE alignment_param_id IN ("
//				+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alAlignmentParamId)
//				+ ")"
//				+ " GROUP BY alignment_param_id"
//				;
//
//		ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
//				chm_alColumnSelectAsInt_aliase2columnName
//				//, chm_alColumnSelectAsString_aliase2columnName,
//				, null
//				, null
//				, chm_alColumnSelectAsLong_aliase2columnName
//				, null
//				, null
//				, tableFrom
//				, whereClause
//				);
//		
//		//run command
//		hmToReturn.putAll(
//				UtilitiesMethodsServer.excecuteCommandAndParseMultipleRowRS_HM(
//						conn
//						, "alignment_param_id"
//						, Long.class
//						, "SUM"
//						, Integer.class
//						, objSQLCommandIT
//						, false
//						, true
//						, true
//						, storeAsNegativeValue
//						, false
//						, methodNameToReport
//						)
//				);

		return hmToReturn;

	}




	
}
