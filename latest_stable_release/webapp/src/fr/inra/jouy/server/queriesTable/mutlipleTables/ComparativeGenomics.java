package fr.inra.jouy.server.queriesTable.mutlipleTables;


/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import fr.inra.jouy.server.DatabaseConf;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentPairs;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentParams;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignments;
import fr.inra.jouy.server.queriesTable.QueriesTableGenes;
import fr.inra.jouy.server.queriesTable.QueriesTableHomologs;
import fr.inra.jouy.server.queriesTable.QueriesTableProtAndNucSequences;
import fr.inra.jouy.shared.pojos.databaseMapping.GeneItem;

public class ComparativeGenomics {

	static Pattern ComparisonWithFeaturedList = Pattern.compile("^.*"
			+ "<br\\/><b><i>Comparison with \\d+ genomes from the featured list:<\\/i><\\/b><ul>"
			+ "<li>"
			+ "Number of genomes with presence of at least 1 CDS conserved during evolution: (\\d+)" // group(1)
			+ " \\((.+)" // group(2)
			+ "\\%\\)"
			+ "<\\/li>"
			+ "<li>"
			+ "Number of genomes with presence of at least 1 CDS in synteny relationship: (\\d+)" // group(3)
			+ " \\((.+)" // group(4)
			+ "\\%\\)"
			+ "<\\/li>"
			+ "<li>Average \\% identity for the alignment\\(s\\)\\: (.+)" // group(5)
			+ "\\%<\\/li>"
			+ "<li>Average \\% query and subject lenght coverage for the alignment\\(s\\): (.+)" // group(6)
			+ "\\%<\\/li>"
			+ "<li>Median Evalue for the alignment\\(s\\): (.+)" // group(7)
			+ "<\\/li>"
			+ "<\\/ul>"
			+ ".*$");
	
	// idx 0 : numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution 
	// idx 1 : percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution
	// idx 2 : numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship 
	// idx 3 : percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
	// idx 4 : avgPercentIdentityWithCDSsConservedDuringEvolution
	// idx 5 : avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
	// idx 6 : medianEvalueAlignmentsWithCDSsConservedDuringEvolution
	public static ArrayList<String> getStatSummaryRefCDS_asAlString(
			Connection conn
			, HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneName //contains only 1 qOrganismId, 1 qGeneId and 1 qGeneName
			, HashSet<Integer> hsOrgaIdsFeaturedGenomes
			) throws Exception {
		
		String listQOrganismId2GeneId = "[";
		String separator = "";
		for (Map.Entry<Integer, HashMap<Integer, String>> entryqOrganismId2 : qOrganismId2qGeneId2qGeneName.entrySet()) {
			Integer qOrganismId = entryqOrganismId2.getKey();
			for (Map.Entry<Integer, String> entryqGeneId2 : entryqOrganismId2.getValue().entrySet()) {
				Integer qGeneId = entryqGeneId2.getKey();
				listQOrganismId2GeneId += separator+qOrganismId+"-"+qGeneId;
				separator = ",";
			}
		}
		listQOrganismId2GeneId += "]";
		String methodNameToReport = "ComparativeGenomics getStatSummaryRefCDS_asAlString"
				+ " ; qOrganismId-GeneId="+listQOrganismId2GeneId
				+ " ; hsOrgaIdsFeaturedGenomes.size()=" + ( (hsOrgaIdsFeaturedGenomes != null ) ? hsOrgaIdsFeaturedGenomes.size() : "NULL" )
				//+ " ; hsOrgaIdsMainList.size()=" + ( (hsOrgaIdsMainList != null ) ? hsOrgaIdsMainList.size() : "NULL" )		
				+ " ; hsOrgaIdsFeaturedGenomes=" + ( (hsOrgaIdsFeaturedGenomes != null ) ? hsOrgaIdsFeaturedGenomes.toString() : "NULL" )
				//+ " ; hsOrgaIdsMainList=" + ( (hsOrgaIdsMainList != null ) ? hsOrgaIdsMainList.toString() : "NULL" )
				;
		
		ArrayList<String> alToReturn = new ArrayList<>();
		
		String rawOutputStatSummaryRefCDS = ComparativeGenomics.getStatSummaryRefCDS_asString(
				conn
				, qOrganismId2qGeneId2qGeneName //contains only 1 qOrganismId, 1 qGeneId and 1 qGeneName
				, hsOrgaIdsFeaturedGenomes
				);
		Matcher m = ComparisonWithFeaturedList.matcher(rawOutputStatSummaryRefCDS);
		if (m.matches()) {
			String numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = m.group(1);
			String percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = m.group(2);
			String numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship  = m.group(3);
			String percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = m.group(4);
			String avgPercentIdentityWithCDSsConservedDuringEvolution = m.group(5);
			String avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = m.group(6);
			String medianEvalueAlignmentsWithCDSsConservedDuringEvolution = m.group(7);
			if (numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution != null && ! numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution.isEmpty() ) {
				alToReturn.add(numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution);
			} else {
				throw new Exception(
						"NOT numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution != null && ! numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution.isEmpty()"
								+ " ; numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = "+numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution
								+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
								+ " ; methodToReport="+methodNameToReport);
			}
			if (percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution != null && ! percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution.isEmpty() ) {
				alToReturn.add(percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution);
			} else {
				throw new Exception(
						"NOT percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution != null && ! percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution.isEmpty()"
								+ " ; percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = "+percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution
								+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
								+ " ; methodToReport="+methodNameToReport);
			}
			if (numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship != null && ! numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship.isEmpty() ) {
				alToReturn.add(numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship);
			} else {
				throw new Exception(
						"NOT numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship != null && ! numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship.isEmpty()"
								+ " ; numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship = "+numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship
								+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
								+ " ; methodToReport="+methodNameToReport);
			}
			if (percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship != null && ! percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship.isEmpty() ) {
				alToReturn.add(percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship);
			} else {
				throw new Exception(
						"NOT percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship != null && ! percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship.isEmpty()"
								+ " ; percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = "+percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship
								+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
								+ " ; methodToReport="+methodNameToReport);
			}
			if (avgPercentIdentityWithCDSsConservedDuringEvolution != null && ! avgPercentIdentityWithCDSsConservedDuringEvolution.isEmpty() ) {
				alToReturn.add(avgPercentIdentityWithCDSsConservedDuringEvolution);
			} else {
				throw new Exception(
						"NOT avgPercentIdentityWithCDSsConservedDuringEvolution != null && ! avgPercentIdentityWithCDSsConservedDuringEvolution.isEmpty()"
								+ " ; avgPercentIdentityWithCDSsConservedDuringEvolution = "+avgPercentIdentityWithCDSsConservedDuringEvolution
								+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
								+ " ; methodToReport="+methodNameToReport);
			}
			if (avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution != null && ! avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution.isEmpty() ) {
				alToReturn.add(avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution);
			} else {
				throw new Exception(
						"NOT avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution != null && ! avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution.isEmpty()"
								+ " ; avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = "+avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution
								+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
								+ " ; methodToReport="+methodNameToReport);
			}
			if (medianEvalueAlignmentsWithCDSsConservedDuringEvolution != null && ! medianEvalueAlignmentsWithCDSsConservedDuringEvolution.isEmpty() ) {
				alToReturn.add(medianEvalueAlignmentsWithCDSsConservedDuringEvolution);
			} else {
				throw new Exception(
						"NOT medianEvalueAlignmentsWithCDSsConservedDuringEvolution != null && ! medianEvalueAlignmentsWithCDSsConservedDuringEvolution.isEmpty()"
								+ " ; medianEvalueAlignmentsWithCDSsConservedDuringEvolution = "+medianEvalueAlignmentsWithCDSsConservedDuringEvolution
								+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
								+ " ; methodToReport="+methodNameToReport);
			}
		} else {
			throw new Exception(
					"no match in rawOutputStatSummaryRefCDS for the expected regulr expression"
							+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
							+ " ; methodToReport="+methodNameToReport);
		}
		
		if (alToReturn.size() != 7) {
			throw new Exception(
					"alToReturn.size() != 7"
							+ " ; alToReturn="+alToReturn.toString()
							+ " ; rawOutputStatSummaryRefCDS = "+rawOutputStatSummaryRefCDS
							+ " ; methodToReport="+methodNameToReport);
		}
		return alToReturn;
		
	}
			
	
	public static String getStatSummaryRefCDS_asString(
			Connection conn
			, HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneName //contains only 1 qOrganismId, 1 qGeneId and 1 qGeneName
			, HashSet<Integer> hsOrgaIdsFeaturedGenomes
			) throws Exception {

		String listQOrganismId2GeneId = "[";
		String separator = "";
		for (Map.Entry<Integer, HashMap<Integer, String>> entryqOrganismId2 : qOrganismId2qGeneId2qGeneName.entrySet()) {
			Integer qOrganismId = entryqOrganismId2.getKey();
			for (Map.Entry<Integer, String> entryqGeneId2 : entryqOrganismId2.getValue().entrySet()) {
				Integer qGeneId = entryqGeneId2.getKey();
				listQOrganismId2GeneId += separator+qOrganismId+"-"+qGeneId;
				separator = ",";
			}
		}
		listQOrganismId2GeneId += "]";
		
		String methodNameToReport = "ComparativeGenomics getStatSummaryRefCDS_asString"
				+ " ; qOrganismId-GeneId="+listQOrganismId2GeneId
				+ " ; hsOrgaIdsFeaturedGenomes.size()=" + ( (hsOrgaIdsFeaturedGenomes != null ) ? hsOrgaIdsFeaturedGenomes.size() : "NULL" )
				//+ " ; hsOrgaIdsMainList.size()=" + ( (hsOrgaIdsMainList != null ) ? hsOrgaIdsMainList.size() : "NULL" )		
				+ " ; hsOrgaIdsFeaturedGenomes=" + ( (hsOrgaIdsFeaturedGenomes != null ) ? hsOrgaIdsFeaturedGenomes.toString() : "NULL" )
				//+ " ; hsOrgaIdsMainList=" + ( (hsOrgaIdsMainList != null ) ? hsOrgaIdsMainList.toString() : "NULL" )
				;
		
		//System.err.println(methodNameToReport);

		String stToReturn = "";
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			
			for (Map.Entry<Integer, HashMap<Integer, String>> entryQOrganismId2 : qOrganismId2qGeneId2qGeneName.entrySet()) {

			    int qOrganismIdIT = entryQOrganismId2.getKey();
			    HashMap<Integer, String> qGeneId2qGeneNameIT = entryQOrganismId2.getValue();
			    
			    for (Map.Entry<Integer, String> entryQGeneId2 : qGeneId2qGeneNameIT.entrySet()) {
				    int qGeneIdIT = entryQGeneId2.getKey();
				    String qGeneNameIT = entryQGeneId2.getValue();
				   
				    stToReturn += "Summary conservation statistics for the <b>reference CDS "+qGeneNameIT
				    		//+ " (gene id = "+qGeneIdIT+" ; organism id = "+qOrganismIdIT+")"
				    		+ ":</b>";
				    
					GeneItem geneItemIT = QueriesTableGenes.getGeneItemWithGeneId(conn, qGeneIdIT);
//					String accnumIT = "";
//					if (geneItemIT.getAccession() != null && ! geneItemIT.getAccession().isEmpty()) {
//						accnumIT = geneItemIT.getAccession();
//					} else {
//						if (geneItemIT.getElementId() > 0) {
//							accnumIT = QueriesTableElements.getAccessionWithElementId(conn, geneItemIT.getElementId(), true);
//						}
//					}
					
				    HashSet<Integer> hsQGeneIdIT = new HashSet<>();
				    hsQGeneIdIT.add(qGeneIdIT);

					HashSet<Long> hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList = new HashSet<>();
					if ( ! hsOrgaIdsFeaturedGenomes.isEmpty()) {
						hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList = QueriesTableAlignmentParams.getHsAlignmentParamId_withQOrganismIdAndHsSOrganismId(
								conn
								, qOrganismIdIT
								, hsOrgaIdsFeaturedGenomes
								, false //isMirrorRequest
								);
					}
					
					//System.err.println("hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList.size()="+hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList.size());
					//System.err.println("hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList="+hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList.toString());
//					Long toTEST = -170631L;
//					if (hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList.contains(toTEST)) {
//						System.err.println("ok contain toTEST="+toTEST);
//					} else {
//						System.err.println("NOT ok DO NOT contain toTEST="+toTEST);
//					}
										
//					HashSet<Long> HsAlignmentIdRelatedToQOrganismIdITAndFeaturedGenomesList = 
//							QueriesTableAlignments.getListAlignmentIdsWithAlAlignmentParamId(
//									conn
//									, hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList
//									, true //convertSetLongToSortedRangeItem_allowGapWithinRange
//									, false //shouldBePresentInTable
//									);
					
					HashSet<Integer> hsTypesIT = new HashSet<>();
					hsTypesIT.add(1);
					hsTypesIT.add(2);

					HashMap<Integer, HashSet<Long>> hmQGeneId2HsAlignmentId = 
							QueriesTableAlignmentPairs.getHmQGeneId2HsAlignmentId_withHsGeneId(
									conn
									, hsQGeneIdIT
									, hsTypesIT
									, false //isMirrorRequest
									);
					HashSet<Long> consolidatedHsAlignmentIdForAllQueryGenes = new HashSet<>();
					for (Map.Entry<Integer, HashSet<Long>> entryHmQGeneId2 : hmQGeneId2HsAlignmentId.entrySet()) {
					    //int qGeneIdIT = entryHmQGeneId2.getKey();
						//HashSet<Long> HsAlignmentIdIT = entryHmQGeneId2.getValue();
						consolidatedHsAlignmentIdForAllQueryGenes.addAll(entryHmQGeneId2.getValue());
					}


					//System.err.println("consolidatedHsAlignmentIdForAllQueryGenes="+consolidatedHsAlignmentIdForAllQueryGenes.size());
					
					//al idx0 = consolidatedHsAlignmentParamsIdForAllQueryGenes_singleton
					//al idx0 = consolidatedHsAlignmentParamsIdForAllQueryGenes_synteny
					ArrayList<HashSet<Long>> alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1 = 
					//HashSet<Long> consolidatedHsAlignmentParamsIdForAllQueryGenes = 
							QueriesTableAlignments.getAlHsAlignmentParamId_SingletonIdx0SyntenyIdx1_withHsAlignmentId(
									conn
									, consolidatedHsAlignmentIdForAllQueryGenes
									, true // convertSetLongToSortedRangeItem_allowGapWithinRange
									, false // shouldBePresentInTable
									);

					//System.err.println("consolidatedHsAlignmentParamsIdForAllQueryGenes="+consolidatedHsAlignmentParamsIdForAllQueryGenes.size());
					HashSet<Long> alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1.get(0));
					alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all.addAll(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1.get(1));
					//System.err.println("alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all.size()="+alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all.size());
					//System.err.println("alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all="+alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all.toString());
					HashSet<Long> alConsolidatedHsAlignmentParamsIdForAllQueryGenes_syntenyOnly = alConsolidatedHsAlignmentParamsIdForAllQueryGenes_SingletonIdx0SyntenyIdx1.get(1);
					
					HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all);
					///HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_all);
					hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.retainAll(hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList);
					//hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.removeAll(hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList);
					
					//System.err.println("hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.size()="+hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.size());
					//System.err.println("hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.size()="+hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.size());
					//System.err.println("hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList="+hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.toString());
					//System.err.println("hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList="+hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.toString());
					
					int featuredGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = hsAlignmentParamsIdsForAllQueryGenes_all_featuredGenomesList.size();
					//int mainGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution = hsAlignmentParamsIdsForAllQueryGenes_all_mainGenomesList.size();

					HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_syntenyOnly);
					//HashSet<Long> hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_mainGenomesList = new HashSet<>(alConsolidatedHsAlignmentParamsIdForAllQueryGenes_syntenyOnly);
					hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList.retainAll(hsAlignmentParamIdRelatedToQOrganismIdITAndFeaturedGenomesList);
					//hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_mainGenomesList.removeAll(hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList);
					
					int featuredGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship = hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_featuredGenomesList.size();
					//int mainGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship = hsAlignmentParamsIdsForAllQueryGenes_syntenyOnly_mainGenomesList.size();
					
					Double featuredGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution = 0D;
					Double featuredGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = 0D;
					Double featuredGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution = 0D;
					if ( ! hsOrgaIdsFeaturedGenomes.isEmpty() ) {
						// idx 0 : Avg % identity of alignments
						// idx 1 : Stdev % identity of alignments
						// idx 2 : Avg % query and subject lenght coverage of alignments
						// idx 3 : Stdev % query and subject lenght coverage of alignments
						// idx 4 : Median Evalue of alignments
						// idx 5 : Min Evalue of alignments
						// idx 6 : Max Evalue of alignments
						ArrayList<Double> alAvgStdevBasicAlignementStats_featuredGenomesList = 
								QueriesTableHomologs.getAvgStdevBasicAlignementStats_withHsQGeneId_optionalHsSOrgaIdsAndRank(
									conn
									, hsQGeneIdIT
									, hsOrgaIdsFeaturedGenomes
									, null //hsRank
									, false //calculateStdev
									, false //calculateMinMaxEvalue
									//, false //isMirrorRequest
								);
						featuredGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(0);
						//Double featuredGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(1);
						featuredGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(2);
						//Double featuredGenomesList_stdevQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(3);
						featuredGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(4);
						//Double featuredGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(5);
						//Double featuredGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_featuredGenomesList.get(6);
						
					}
					
					
//					// idx 0 : Avg % identity of alignments
//					// idx 1 : Stdev % identity of alignments
//					// idx 2 : Avg % query and subject lenght coverage of alignments
//					// idx 3 : Stdev % query and subject lenght coverage of alignments
//					// idx 4 : Median Evalue of alignments
//					// idx 5 : Min Evalue of alignments
//					// idx 6 : Max Evalue of alignments
//					ArrayList<Double> alAvgStdevBasicAlignementStats_mainGenomesList = 
//							QueriesTableHomologs.getAvgStdevBasicAlignementStats_withHsQGeneId_optionalHsSOrgaIdsAndRank(
//								conn
//								, hsQGeneIdIT
//								, hsOrgaIdsMainList
//								, null //hsRank
//								, false //calculateStdev
//								, false //calculateMinMaxEvalue
//								//, false //isMirrorRequest
//							);
//					Double mainGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(0);
//					//Double mainGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(1);
//					Double mainGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(2);
//					//Double mainGenomesList_stdevQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(3);
//					Double mainGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(4);
//					//Double mainGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(5);
//					//Double mainGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution = alAvgStdevBasicAlignementStats_mainGenomesList.get(6);
					
					
					String nucSequencesHsGenes = QueriesTableProtAndNucSequences.getNucleicAcidSequenceAsFastaWithAlGeneIds(
							conn
							, hsQGeneIdIT
							, false //withHeader
							, false //lineSeparatorInSeq
							);
					int countSizeSeq = nucSequencesHsGenes.length();
					int countG = StringUtils.countMatches(nucSequencesHsGenes, "G");
					int countC = StringUtils.countMatches(nucSequencesHsGenes, "C");
					int countg = StringUtils.countMatches(nucSequencesHsGenes, "g");
					int countc = StringUtils.countMatches(nucSequencesHsGenes, "c");
					int countGC = countG + countC + countg + countc;
//					System.err.println(nucSequencesHsGenes);
//					System.err.println("countG="+countG);
//					System.err.println("countC="+countC);
//					System.err.println("countGC="+countGC);
//					System.err.println("nucSequencesHsGenes.length()="+nucSequencesHsGenes.length());
					
					Double percentGCrefCDS = (double) ( ( (double) countGC / (double) countSizeSeq ) * (double) 100 ) ;
					
					DecimalFormat df = new DecimalFormat("##.##");
					//stToReturn += "<br/>";
					stToReturn += ""
							//+ "<br/>"
							//+ "<br/><b><i>Locus tag:</i></b> " + geneItemIT.getLocusTagAsStrippedString()
							//+ "<br/><b><i>Protein id:</i></b> " + geneItemIT.getListProteinId().toString()
							+ "<br/><b><i>List product:</i></b> " + geneItemIT.getListProduct().toString()
							+ "<br/><b><i>Length residues:</i></b> " + geneItemIT.getLengthResidues()
							+ "<br/><b><i>% GC:</i></b> " + df.format(percentGCrefCDS)
							//+ "<br/><b><i>Accession:</i></b> " + accnumIT
							+ "<br/>"
							;
					//stToReturn += "<br/><b><i>% GC reference CDS:</i></b> "+df.format(percentGCrefCDS) + "%";
					
					if ( ! hsOrgaIdsFeaturedGenomes.isEmpty()) {
						stToReturn += "<br/><b><i>Comparison with "+hsOrgaIdsFeaturedGenomes.size()+" genomes from the featured list:</i></b><ul>";
						double featuredGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = ( (double) featuredGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution / (double) hsOrgaIdsFeaturedGenomes.size() ) * (double) 100;
						stToReturn += "<li>"
								+ "Number of genomes with presence of at least 1 CDS conserved during evolution: "+featuredGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution 
								+ " (" + df.format(featuredGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution) + "%)"
								+ "</li>" ;
						double featuredGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = ( (double) featuredGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship / (double) hsOrgaIdsFeaturedGenomes.size() ) * (double) 100;
						stToReturn += "<li>"
								+ "Number of genomes with presence of at least 1 CDS in synteny relationship: "+ featuredGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship 
								+ " (" + df.format(featuredGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship) + "%)"
								+ "</li>" ;
						stToReturn += "<li>Average % identity for the alignment(s): "+df.format(featuredGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution) + "%</li>";
//						stToReturn += "<li>Standard deviation % identity for the alignment(s): "+df.format(featuredGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution) + "%</li>";
						stToReturn += "<li>Average % query and subject lenght coverage for the alignment(s): "+df.format(featuredGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
//						stToReturn += "<li>Standard deviation % query and subject lenght coverage for the alignment(s): "+df.format(featuredGenomesList_stdevQueryLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
//						stToReturn += "<li>Average % subject lenght coverage for the alignment(s): "+df.format(featuredGenomesList_avgSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
//						stToReturn += "<li>Standard deviation % subject lenght coverage for the alignment(s): "+df.format(featuredGenomesList_stdevSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
						stToReturn += "<li>Median Evalue for the alignment(s): "+featuredGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
//						stToReturn += "<li>Min Evalue for the alignment(s): "+featuredGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
//						stToReturn += "<li>Max Evalue for the alignment(s): "+featuredGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
						stToReturn += "</ul>";
					} else {
						stToReturn += "<br/><b><i>There is no genome in your featured list. Please add to your featured list the genomes you would like to calculate the summary conservation statistics for.</i></b>";
					}
	
					//if ( ! hsOrgaIdsMainList.isEmpty()) {
						//stToReturn += "<br/><b><i>Comparison with "+hsOrgaIdsMainList.size()+" genomes from the main list:</i></b><ul>";
						//double mainGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution = ( (double) mainGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution / (double) hsOrgaIdsMainList.size() ) * (double) 100;
						//stToReturn += "<li>Number of genomes with presence of at least 1 CDS conserved during evolution: "+mainGenomesList_numberGenomesPresenceAtLeastOneCDSConservedDuringEvolution + " (" + df.format(mainGenomesList_percentGenomesPresenceAtLeastOneCDSConservedDuringEvolution) + "%)</li>" ; // give count and %
						//double mainGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship = ( (double) mainGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship / (double) hsOrgaIdsMainList.size() ) * (double) 100;
						//stToReturn += "<li>Number of genomes with presence of at least 1 CDS in synteny relationship: "+ mainGenomesList_numberGenomesPresenceAtLeastOneCDSinSyntenyRelationship + " (" + df.format(mainGenomesList_percentGenomesPresenceAtLeastOneCDSinSyntenyRelationship) + "%)</li>" ; // give count and %
						//stToReturn += "<li>Average % identity for the alignment(s): "+df.format(mainGenomesList_avgPercentIdentityWithCDSsConservedDuringEvolution) + "%</li>";
	//					stToReturn += "<li>Standard deviation % identity for the alignment(s): "+df.format(mainGenomesList_stdevPercentIdentityWithCDSsConservedDuringEvolution) + "%</li>";
						//stToReturn += "<li>Average % query and subject lenght coverage for the alignment(s): "+df.format(mainGenomesList_avgQueryAndSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
	//					stToReturn += "<li>Standard deviation % query and subject lenght coverage for the alignment(s): "+df.format(mainGenomesList_stdevQueryLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
	//					stToReturn += "<li>Average % subject lenght coverage for the alignment(s): "+df.format(mainGenomesList_avgSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
	//					stToReturn += "<li>Standard deviation % subject lenght coverage for the alignment(s): "+df.format(mainGenomesList_stdevSubjectLenghtCoverageAlignmentsWithCDSsConservedDuringEvolution) + "%</li>";
						//stToReturn += "<li>Median Evalue for the alignment(s): "+mainGenomesList_medianEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
	//					stToReturn += "<li>Min Evalue for the alignment(s): "+mainGenomesList_minEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
	//					stToReturn += "<li>Max Evalue for the alignment(s): "+mainGenomesList_maxEvalueAlignmentsWithCDSsConservedDuringEvolution + "</li>";
						//stToReturn += "</ul>";
					//}
			    
				}
			
			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
			UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}// try
		
		return stToReturn;
		
	}
	
	
}
