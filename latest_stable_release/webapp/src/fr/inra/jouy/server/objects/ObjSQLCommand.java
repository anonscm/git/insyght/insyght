package fr.inra.jouy.server.objects;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import java.sql.ResultSet;
//import java.util.HashMap;
//import java.util.HashSet;
import fr.inra.jouy.server.UtilitiesMethodsServer;
import fr.inra.jouy.shared.interfaceIs.IsMappingKeyValueToFields;

public class ObjSQLCommand {

	/* exemple use :
	// select columns
	//int
	ConcurrentHashMap<String, String> chm_alColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
	//empty
	//chm_alColumnSelectAsInt_aliase2columnName.put("??", "??");
	//String
	ConcurrentHashMap<String, String> chm_alColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
	//empty
	//chm_alColumnSelectAsString_aliase2columnName.put("??", "??");
	//Boolean
	ConcurrentHashMap<String, String> chm_alColumnSelectAsBoolean_aliase2columnName = new ConcurrentHashMap<String, String>();
	//empty
	//chm_alColumnSelectAsBoolean_aliase2columnName.put("??", "??");
	//Long
	ConcurrentHashMap<String, String> chm_alColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
	//empty
	//chm_alColumnSelectAsLong_aliase2columnName.put("??", "??");
	//Double
	ConcurrentHashMap<String, String> chm_alColumnSelectAsDouble_aliase2columnName = new ConcurrentHashMap<String, String>();
	//empty
	//chm_alColumnSelectAsDouble_aliase2columnName.put("??", "??");
	//Float
	ConcurrentHashMap<String, String> chm_alColumnSelectAsFloat_aliase2columnName = new ConcurrentHashMap<String, String>();
	//empty
	//chm_alColumnSelectAsFloat_aliase2columnName.put("??", "??");
	ArrayList<String> tableFrom = new ArrayList<String>();
	tableFrom.add("organisms");
	tableFrom.add("elements");
	String whereClause = "WHERE organisms.organism_id = elements.organism_id"
			+ " ORDER BY organisms.organism_id";
	ObjSQLCommand objSQLCommandIT = new ObjSQLCommand(
			chm_alColumnSelectAsInt_aliase2columnName,
			chm_alColumnSelectAsString_aliase2columnName,
			chm_alColumnSelectAsBoolean_aliase2columnName,
			chm_alColumnSelectAsLong_aliase2columnName,
			chm_alColumnSelectAsDouble_aliase2columnName,
			chm_alColumnSelectAsFloat_aliase2columnName,
			tableFrom, whereClause);
	*/
	
	private ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
	private ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
	private ConcurrentHashMap<String, String> chmColumnSelectAsBoolean_aliase2columnName = new ConcurrentHashMap<String, String>();
	private ConcurrentHashMap<String, String> chmColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
	private ConcurrentHashMap<String, String> chmColumnSelectAsDouble_aliase2columnName = new ConcurrentHashMap<String, String>();
	private ConcurrentHashMap<String, String> chmColumnSelectAsFloat_aliase2columnName = new ConcurrentHashMap<String, String>();
	private ArrayList<String> tableFrom = new ArrayList<String>();
	private String whereClause = "";
	private int limitClause = -1;
	
	//constructor

	//all fields + limit
	public ObjSQLCommand(
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsBoolean_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsLong_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsDouble_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsFloat_aliase2columnName,
			ArrayList<String> tableFrom, String whereClause, int limitClause) {
		this(
				chmColumnSelectAsInt_aliase2columnName,
				chmColumnSelectAsString_aliase2columnName,
				chmColumnSelectAsBoolean_aliase2columnName,
				chmColumnSelectAsLong_aliase2columnName,
				chmColumnSelectAsDouble_aliase2columnName,
				chmColumnSelectAsFloat_aliase2columnName,
				tableFrom,
				whereClause
				);
		this.limitClause = limitClause;
	}

	//all fields
	public ObjSQLCommand(
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsBoolean_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsLong_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsDouble_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsFloat_aliase2columnName,
			ArrayList<String> tableFrom, String whereClause) {
		if (chmColumnSelectAsInt_aliase2columnName == null) {
			this.chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		} else {
			this.chmColumnSelectAsInt_aliase2columnName = chmColumnSelectAsInt_aliase2columnName;
		}
		if (chmColumnSelectAsString_aliase2columnName == null) {
			this.chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		} else {
			this.chmColumnSelectAsString_aliase2columnName = chmColumnSelectAsString_aliase2columnName;
		}
		if (chmColumnSelectAsBoolean_aliase2columnName == null) {
			this.chmColumnSelectAsBoolean_aliase2columnName = new ConcurrentHashMap<String, String>();
		} else {
			this.chmColumnSelectAsBoolean_aliase2columnName = chmColumnSelectAsBoolean_aliase2columnName;
		}
		if (chmColumnSelectAsLong_aliase2columnName == null) {
			this.chmColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
		} else {
			this.chmColumnSelectAsLong_aliase2columnName = chmColumnSelectAsLong_aliase2columnName;
		}
		if (chmColumnSelectAsDouble_aliase2columnName == null) {
			this.chmColumnSelectAsDouble_aliase2columnName = new ConcurrentHashMap<String, String>();
		} else {
			this.chmColumnSelectAsDouble_aliase2columnName = chmColumnSelectAsDouble_aliase2columnName;
		}
		if (chmColumnSelectAsFloat_aliase2columnName == null) {
			this.chmColumnSelectAsFloat_aliase2columnName = new ConcurrentHashMap<String, String>();
		} else {
			this.chmColumnSelectAsFloat_aliase2columnName = chmColumnSelectAsFloat_aliase2columnName;
		}
		this.tableFrom = tableFrom;
		this.whereClause = whereClause;
	}
	
	//only Int and String (most use cases)
	public ObjSQLCommand(
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName,
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName,
			ArrayList<String> tableFrom, String whereClause) {
		if (chmColumnSelectAsInt_aliase2columnName == null) {
			this.chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		} else {
			this.chmColumnSelectAsInt_aliase2columnName = chmColumnSelectAsInt_aliase2columnName;
		}
		if (chmColumnSelectAsString_aliase2columnName == null) {
			this.chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
		} else {
			this.chmColumnSelectAsString_aliase2columnName = chmColumnSelectAsString_aliase2columnName;
		}
		
		this.tableFrom = tableFrom;
		this.whereClause = whereClause;
	}
	

	//setters and getters


	public ConcurrentHashMap<String, String> getChmColumnSelectAsInt_aliase2columnName() {
		return chmColumnSelectAsInt_aliase2columnName;
	}

	public void setChmColumnSelectAsInt_aliase2columnName(
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName) {
		this.chmColumnSelectAsInt_aliase2columnName = chmColumnSelectAsInt_aliase2columnName;
	}

	public ConcurrentHashMap<String, String> getChmColumnSelectAsString_aliase2columnName() {
		return chmColumnSelectAsString_aliase2columnName;
	}

	public void setChmColumnSelectAsString_aliase2columnName(
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName) {
		this.chmColumnSelectAsString_aliase2columnName = chmColumnSelectAsString_aliase2columnName;
	}


	public ConcurrentHashMap<String, String> getChmColumnSelectAsBoolean_aliase2columnName() {
		return chmColumnSelectAsBoolean_aliase2columnName;
	}


	public void setChmColumnSelectAsBoolean_aliase2columnName(
			ConcurrentHashMap<String, String> chmColumnSelectAsBoolean_aliase2columnName) {
		this.chmColumnSelectAsBoolean_aliase2columnName = chmColumnSelectAsBoolean_aliase2columnName;
	}


	public ConcurrentHashMap<String, String> getChmColumnSelectAsLong_aliase2columnName() {
		return chmColumnSelectAsLong_aliase2columnName;
	}


	public void setChmColumnSelectAsLong_aliase2columnName(
			ConcurrentHashMap<String, String> chmColumnSelectAsLong_aliase2columnName) {
		this.chmColumnSelectAsLong_aliase2columnName = chmColumnSelectAsLong_aliase2columnName;
	}


	public ConcurrentHashMap<String, String> getChmColumnSelectAsDouble_aliase2columnName() {
		return chmColumnSelectAsDouble_aliase2columnName;
	}


	public void setChmColumnSelectAsDouble_aliase2columnName(
			ConcurrentHashMap<String, String> chmColumnSelectAsDouble_aliase2columnName) {
		this.chmColumnSelectAsDouble_aliase2columnName = chmColumnSelectAsDouble_aliase2columnName;
	}


	public ConcurrentHashMap<String, String> getChmColumnSelectAsFloat_aliase2columnName() {
		return chmColumnSelectAsFloat_aliase2columnName;
	}


	public void setChmColumnSelectAsFloat_aliase2columnName(
			ConcurrentHashMap<String, String> chmColumnSelectAsFloat_aliase2columnName) {
		this.chmColumnSelectAsFloat_aliase2columnName = chmColumnSelectAsFloat_aliase2columnName;
	}


	public ArrayList<String> getTableFrom() {
		return tableFrom;
	}

	public void setTableFrom(ArrayList<String> tableFrom) {
		this.tableFrom = tableFrom;
	}

	public String getWhereClause() {
		return whereClause;
	}

	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}

	public int getLimitClause() {
		return limitClause;
	}

	public void setLimitClause(int limitClause) {
		this.limitClause = limitClause;
	}


	//special methods
	public String getAllColumnsToSelectInCommand(){
		return getAllColumnsToSelectInCommand("");
	}
	
	private String utilityMethodBuildSelectRecursively(
			String currSelectString,
			ConcurrentHashMap<String, String> chmSent,
			String tablePrefix){
		for (Map.Entry<String, String> entry : chmSent.entrySet()) {
			if( ! currSelectString.isEmpty()){
				currSelectString += ", ";
			}
			currSelectString += tablePrefix+entry.getValue()+" AS "+entry.getKey();
		}
		return currSelectString;
	}
	
	public String getAllColumnsToSelectInCommand(String tablePrefix){
		
		String stringToReturn = "";
		//chmColumnSelectAsInt_aliase2columnName
		stringToReturn = utilityMethodBuildSelectRecursively(
				stringToReturn,
				chmColumnSelectAsInt_aliase2columnName,
				tablePrefix);
		//boolean notFirstColumn = false;
//		for (Map.Entry<String, String> entry : chmColumnSelectAsInt_aliase2columnName.entrySet()) {
//			if(notFirstColumn){
//				stringToReturn += ", ";
//			} else {
//				notFirstColumn = true;
//			}
//			stringToReturn += tablePrefix+entry.getValue()+" AS "+entry.getKey();
//		}
		//chmColumnSelectAsString_aliase2columnName
		stringToReturn = utilityMethodBuildSelectRecursively(
				stringToReturn,
				chmColumnSelectAsString_aliase2columnName,
				tablePrefix);
		//chmColumnSelectAsBoolean_aliase2columnName
		stringToReturn = utilityMethodBuildSelectRecursively(
				stringToReturn,
				chmColumnSelectAsBoolean_aliase2columnName,
				tablePrefix);
		//chmColumnSelectAsLong_aliase2columnName
		stringToReturn = utilityMethodBuildSelectRecursively(
				stringToReturn,
				chmColumnSelectAsLong_aliase2columnName,
				tablePrefix);
		//chmColumnSelectAsDouble_aliase2columnName
		stringToReturn = utilityMethodBuildSelectRecursively(
				stringToReturn,
				chmColumnSelectAsDouble_aliase2columnName,
				tablePrefix);
		//chmColumnSelectAsFloat_aliase2columnName
		stringToReturn = utilityMethodBuildSelectRecursively(
				stringToReturn,
				chmColumnSelectAsFloat_aliase2columnName,
				tablePrefix);
		
		return stringToReturn;
		
	}
	
	public String getSQLCommand() {
		//select
		String command = "SELECT ";
		command += getAllColumnsToSelectInCommand();
		//from
		if(tableFrom != null){
			command += " FROM "
					+ UtilitiesMethodsShared.getItemsAsStringFromCollection(tableFrom, ",", "")
					//+ tableFrom.toString().replaceAll("[\\[\\]]", "")
					;
		}
		//where
		command += " " + whereClause;
		//limit
		if(limitClause > 0){
			command += " LIMIT " + limitClause;
		}
		return command;
	}

	public <T extends IsMappingKeyValueToFields> ArrayList<T> parseRsIntoClassExtendsAbstractFieldsMapper(
			Class<T> classToInstanciate,
			ResultSet rs,
			boolean shouldBeNotEmptyOrZero,
			String methodNameToReport) throws Exception {
		
		ArrayList<T> alToReturn = new ArrayList<T>();
		
		while (rs.next()) {
			//ObjAssociationAsMap qrsIT = new ObjAssociationAsMap();
			T tIT = classToInstanciate.newInstance();
			
			//getChmColumnSelectAsInt_aliase2columnName
			for(String columnIT : getChmColumnSelectAsInt_aliase2columnName().keySet()){
				int intNext = rs.getInt(columnIT);
				if (shouldBeNotEmptyOrZero && intNext == 0) {
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("Negative int detected in result of query : " + this.getSQLCommand())
					);
				} else {
					boolean fieldWasMapped = tIT.mapKeyIntValueToObjectField(columnIT, intNext);
					if( ! fieldWasMapped){
						UtilitiesMethodsServer.reportException(methodNameToReport,
								new Exception("The column "+columnIT+" with int value "+intNext+" was not mapped to any field for the class "+classToInstanciate.getName()
										+" ; Original SQL command : " + this.getSQLCommand())
						);
					}
				}
			}
			//getChmColumnSelectAsString_aliase2columnName
			for(String columnIT : getChmColumnSelectAsString_aliase2columnName().keySet()){
				String stNext = rs.getString(columnIT);
				if (shouldBeNotEmptyOrZero && ( stNext == null || stNext.isEmpty() ) ) {
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("Empty string detected in result of query : " + this.getSQLCommand())
					);
				} else {
					boolean fieldWasMapped = tIT.mapKeyStringValueToObjectField(columnIT, stNext);
					if( ! fieldWasMapped){
						UtilitiesMethodsServer.reportException(methodNameToReport,
								new Exception("The column "+columnIT+" with string value "+stNext+" was not mapped to any field for the class "+classToInstanciate.getName()
										+" ; Original SQL command : " + this.getSQLCommand())
						);
					}
				}
			}
			//chmColumnSelectAsBoolean_aliase2columnName
			for(String columnIT : getChmColumnSelectAsBoolean_aliase2columnName().keySet()){
				boolean boulNext = rs.getBoolean(columnIT);
				boolean fieldWasMapped = tIT.mapKeyBooleanValueToObjectField(columnIT, boulNext);
				if( ! fieldWasMapped){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("The column "+columnIT+" with boolean value "+boulNext+" was not mapped to any field for the class "+classToInstanciate.getName()
									+" ; Original SQL command : " + this.getSQLCommand())
					);
				}
			}
			//chmColumnSelectAsLong_aliase2columnName
			for(String columnIT : getChmColumnSelectAsLong_aliase2columnName().keySet()){
				Long longNext = rs.getLong(columnIT);
				if (shouldBeNotEmptyOrZero && longNext == 0) {
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("Negative Long detected in result of query : " + this.getSQLCommand())
					);
				} else {
					boolean fieldWasMapped = tIT.mapKeyLongValueToObjectField(columnIT, longNext);
					if( ! fieldWasMapped){
						UtilitiesMethodsServer.reportException(methodNameToReport,
								new Exception("The column "+columnIT+" with Long value "+longNext+" was not mapped to any field for the class "+classToInstanciate.getName()
										+" ; Original SQL command : " + this.getSQLCommand())
						);
					}
				}
			}
			//chmColumnSelectAsDouble_aliase2columnName
			for(String columnIT : getChmColumnSelectAsDouble_aliase2columnName().keySet()){
				Double doubleNext = rs.getDouble(columnIT);
				if (shouldBeNotEmptyOrZero && doubleNext == 0) {
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("Negative Double detected in result of query : " + this.getSQLCommand())
					);
				} else {
					boolean fieldWasMapped = tIT.mapKeyDoubleValueToObjectField(columnIT, doubleNext);
					if( ! fieldWasMapped){
						UtilitiesMethodsServer.reportException(methodNameToReport,
								new Exception("The column "+columnIT+" with Double value "+doubleNext+" was not mapped to any field for the class "+classToInstanciate.getName()
										+" ; Original SQL command : " + this.getSQLCommand())
						);
					}
				}
			}
			//chmColumnSelectAsFloat_aliase2columnName
			for(String columnIT : getChmColumnSelectAsFloat_aliase2columnName().keySet()){
				Float floatNext = rs.getFloat(columnIT);
				if (shouldBeNotEmptyOrZero && floatNext == 0) {
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("Negative Float detected in result of query : " + this.getSQLCommand())
					);
				} else {
					boolean fieldWasMapped = tIT.mapKeyFloatValueToObjectField(columnIT, floatNext);
					if( ! fieldWasMapped){
						UtilitiesMethodsServer.reportException(methodNameToReport,
								new Exception("The column "+columnIT+" with Float value "+floatNext+" was not mapped to any field for the class "+classToInstanciate.getName()
										+" ; Original SQL command : " + this.getSQLCommand())
						);
					}
				}
			}
			
			alToReturn.add(tIT);
		}
		
		return alToReturn;
		
	}
	
	// DEL excecuteCommandAndParseMultipleRowRS
	// can cause limit overhead problems
	/*
	public ArrayList<String> parseRsIntoAlString(
			ResultSet rs,
			String columnStringToParse,
			boolean shouldBeNotEmptyOrZero,
			String methodNameToReport) throws Exception {
		ArrayList<String> alToReturn = new ArrayList<String>();
		while (rs.next()) {
			String stNext = rs.getString(columnStringToParse);
			if (shouldBeNotEmptyOrZero && stNext.isEmpty()) {
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("Empty string detected in result of query : " + this.getSQLCommand())
				);
			} else {
				alToReturn.add(stNext);
			}
		}
		return alToReturn;
	}
	
	
	public ArrayList<Long> parseRsIntoAlLong(
			ResultSet rs
			, String columnIntToParse
			, boolean shouldBeNotEmptyOrZero
			, boolean storeAsNegativeValue
			, String methodNameToReport
			) throws Exception {
		ArrayList<Long> alToReturn = new ArrayList<Long>();
		while (rs.next()) {
			Long longNext = rs.getLong(columnIntToParse);
			if (shouldBeNotEmptyOrZero && longNext == 0) {
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("0 int detected in result of query : " + this.getSQLCommand())
				);
			} else {
				if (storeAsNegativeValue) {
					alToReturn.add(-longNext);
				} else {
					alToReturn.add(longNext);	
				}
			}
		}
		return alToReturn;
	}
	
	
	public ArrayList<Integer> parseRsIntoAlInt(
			ResultSet rs,
			String columnIntToParse,
			boolean shouldBeNotEmptyOrZero,
			String methodNameToReport) throws Exception {
		ArrayList<Integer> alToReturn = new ArrayList<Integer>();
		while (rs.next()) {
			int intNext = rs.getInt(columnIntToParse);
			if (shouldBeNotEmptyOrZero && intNext == 0) {
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("0 int detected in result of query : " + this.getSQLCommand())
				);
			} else {
				alToReturn.add(intNext);
			}
		}
		return alToReturn;
	}
	
	

	
	
	@SuppressWarnings("unchecked")
	public <T,U> HashMap<T, U> parseRsIntoHM(
			String keyColumnName
			, Class<T> keyClassToInstanciate
			, String valueColumnName
			, Class<U> valueClassToInstanciate
			, ResultSet rs
			, boolean shouldBeNotEmptyOrZero
			, boolean shouldNotFindDuplicateKeys
			, boolean storeKeysAsNegativeIfNumber
			, boolean storeValuesAsNegativeIfNumber
			, String methodNameToReport
			) throws Exception {

		HashMap<T, U> hmToReturn = new HashMap<>();

		while (rs.next()) {
			
			T key = null;
			U value = null;
			
			//get the key
			//if(keyClassToInstanciate.getCanonicalName().compareTo(Integer.class.getCanonicalName()) == 0){
			if(keyClassToInstanciate.equals(Integer.class)){
				//Integer
				int keyInt = rs.getInt(keyColumnName);
				if(shouldBeNotEmptyOrZero && keyInt == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("key instanceof Integer is <= 0"
									+ " ; keyColumnName = " + keyColumnName
									+ " ; keyInt = " + keyInt
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				if (storeKeysAsNegativeIfNumber) {
					keyInt = -keyInt;
				}
				key = (T) Integer.valueOf(keyInt);

			} else if (keyClassToInstanciate.equals(String.class)) {
				//String
				key = (T) rs.getString(keyColumnName);
				boolean passEmptyNegativeTest = false;
				if(shouldBeNotEmptyOrZero){
					if(key != null){
						if( ! ((String) key).isEmpty()){
							passEmptyNegativeTest = true;
						}
					}
				}
				if( ! passEmptyNegativeTest){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("key instanceof String is null or empty"
									+ " ; keyColumnName = " + keyColumnName
									+ " ; key = " + key
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
					
			} else if (keyClassToInstanciate.equals(Long.class)) {
				//Long
				long keyLong = rs.getLong(keyColumnName);
				if(shouldBeNotEmptyOrZero && keyLong == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("key instanceof Long is <= 0"
									+ " ; keyColumnName = " + keyColumnName
									+ " ; keyLong = " + keyLong
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				if (storeKeysAsNegativeIfNumber) {
					keyLong = -keyLong;
				}
				key = (T) Long.valueOf(keyLong);
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("Type of key not supported"
								+ " ; keyColumnName = " + keyColumnName
								+ " ; keyClassToInstanciate = " + keyClassToInstanciate.getCanonicalName()
								+ " ; Original SQL command : " + this.getSQLCommand())
				);
			}

			//get the value
			if(valueClassToInstanciate.equals(Integer.class)){
				//Integer
				int valueInt = rs.getInt(valueColumnName);
				if(shouldBeNotEmptyOrZero && valueInt == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof Integer is <= 0"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; valueInt = " + valueInt
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				if (storeValuesAsNegativeIfNumber) {
					valueInt = -valueInt;
				}
				value = (U) Integer.valueOf(valueInt);
			} else if (valueClassToInstanciate.equals(String.class)) {
				//String
				value = (U) rs.getString(valueColumnName);
				boolean passEmptyNegativeTest = false;
				if(shouldBeNotEmptyOrZero){
					if(value != null){
						if( ! ((String) value).isEmpty()){
							passEmptyNegativeTest = true;
						}
					}
				}
				if( ! passEmptyNegativeTest){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof String is null or empty"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; value = " + value
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
					
			} else if (valueClassToInstanciate.equals(Long.class)) {
				//Long
				long valueLong = rs.getLong(valueColumnName);
				if(shouldBeNotEmptyOrZero && valueLong == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof Long is <= 0"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; valueLong = " + valueLong
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				if (storeValuesAsNegativeIfNumber) {
					valueLong = -valueLong;
				}
				value = (U) Long.valueOf(valueLong);
			} else if (valueClassToInstanciate.equals(Double.class)) {
				//Double
				double valueDouble = rs.getDouble(valueColumnName);
				if(shouldBeNotEmptyOrZero && valueDouble == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof Double is <= 0"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; valueDouble = " + valueDouble
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				if (storeValuesAsNegativeIfNumber) {
					valueDouble = -valueDouble;
				}
				value = (U) Double.valueOf(valueDouble);
			} else if (valueClassToInstanciate.equals(Float.class)) {
				//Float
				float valueFloat = rs.getFloat(valueColumnName);
				if(shouldBeNotEmptyOrZero && valueFloat == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof Float is <= 0"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; valueFloat = " + valueFloat
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				if (storeValuesAsNegativeIfNumber) {
					valueFloat = -valueFloat;
				}
				value = (U) Float.valueOf(valueFloat);
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("Type of value not supported"
								+ " ; valueColumnName = " + valueColumnName
								+ " ; valueClassToInstanciate = " + valueClassToInstanciate.getCanonicalName()
								+ " ; Original SQL command : " + this.getSQLCommand())
				);
			}

			
			if(key == null && shouldBeNotEmptyOrZero){
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("key is null"
								+ " ; keyColumnName = " + keyColumnName
								+ " ; keyClassToInstanciate = " + keyClassToInstanciate.getCanonicalName()
								+ " ; Original SQL command : " + this.getSQLCommand())
				);
			}
			if(value == null && shouldBeNotEmptyOrZero){
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("value is null"
								+ " ; valueColumnName = " + valueColumnName
								+ " ; class type of hmToReturn = " + valueClassToInstanciate.getCanonicalName()
								+ " ; Original SQL command : " + this.getSQLCommand())
				);
			}
		
			
			//map
			if(hmToReturn.containsKey(key) && shouldNotFindDuplicateKeys){
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("shouldNotFindDuplicateKeys is true but found duplicate keys "
								+ " ; key = " + key
								+ " ; class type of hmToReturn = " + valueClassToInstanciate.getCanonicalName()
								+ " ; Original SQL command : " + this.getSQLCommand())
				);
			} else {
				hmToReturn.put(key, value);
			}
			
		}
		
		return hmToReturn;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public <T,U> HashMap<T, HashSet<U>> parseRsIntoHMofHS(
			String keyColumnName,
			Class<T> keyClassToInstanciate,
			String valueColumnName,
			Class<U> valueClassToInstanciate,
			ResultSet rs,
			boolean shouldBeNotEmptyOrZero,
			String methodNameToReport
			) throws Exception {
		
		HashMap<T, HashSet<U>> hmToReturn = new HashMap<>();
		
		while (rs.next()) {
			
			T key = null;
			U value = null;
			
			//get the key
			//if(keyClassToInstanciate.getCanonicalName().compareTo(Integer.class.getCanonicalName()) == 0){
			if(keyClassToInstanciate.equals(Integer.class)){
				//Integer
				int keyInt = rs.getInt(keyColumnName);
				if(shouldBeNotEmptyOrZero && keyInt == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("key instanceof Integer is <= 0"
									+ " ; keyColumnName = " + keyColumnName
									+ " ; keyInt = " + keyInt
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				key = (T) Integer.valueOf(keyInt);
			} else if (keyClassToInstanciate.equals(String.class)) {
				//String
				key = (T) rs.getString(keyColumnName);
				boolean passEmptyNegativeTest = false;
				if(shouldBeNotEmptyOrZero){
					if(key != null){
						if( ! ((String) key).isEmpty()){
							passEmptyNegativeTest = true;
						}
					}
				}
				if( ! passEmptyNegativeTest){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("key instanceof String is null or empty"
									+ " ; keyColumnName = " + keyColumnName
									+ " ; key = " + key
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
					
			} else if (keyClassToInstanciate.equals(Long.class)) {
				//Long
				long keyLong = rs.getLong(keyColumnName);
				if(shouldBeNotEmptyOrZero && keyLong == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("key instanceof Long is <= 0"
									+ " ; keyColumnName = " + keyColumnName
									+ " ; keyLong = " + keyLong
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				key = (T) Long.valueOf(keyLong);
			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("Type of key not supported"
								+ " ; keyColumnName = " + keyColumnName
								+ " ; keyClassToInstanciate = " + keyClassToInstanciate.getCanonicalName()
								+ " ; Original SQL command : " + this.getSQLCommand())
				);
			}

			//get the value
			if(valueClassToInstanciate.equals(Integer.class)){
				//Integer
				int valueInt = rs.getInt(valueColumnName);
				if(shouldBeNotEmptyOrZero && valueInt == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof Integer is <= 0"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; valueInt = " + valueInt
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				value = (U) Integer.valueOf(valueInt);
			} else if (valueClassToInstanciate.equals(String.class)) {
				//String
				value = (U) rs.getString(valueColumnName);
				boolean passEmptyNegativeTest = false;
				if(shouldBeNotEmptyOrZero){
					if(value != null){
						if( ! ((String) value).isEmpty()){
							passEmptyNegativeTest = true;
						}
					}
				}
				if( ! passEmptyNegativeTest){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof String is null or empty"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; value = " + value
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
					
			} else if (valueClassToInstanciate.equals(Long.class)) {
				//Long
				long valueLong = rs.getLong(valueColumnName);
				if(shouldBeNotEmptyOrZero && valueLong == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof Long is <= 0"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; valueLong = " + valueLong
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				value = (U) Long.valueOf(valueLong);
			} else if (valueClassToInstanciate.equals(Double.class)) {
				//Double
				double valueDouble = rs.getDouble(valueColumnName);
				if(shouldBeNotEmptyOrZero && valueDouble == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof Double is <= 0"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; valueDouble = " + valueDouble
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				value = (U) Double.valueOf(valueDouble);
			} else if (valueClassToInstanciate.equals(Float.class)) {
				//Float
				float valueFloat = rs.getFloat(valueColumnName);
				if(shouldBeNotEmptyOrZero && valueFloat == 0){
					UtilitiesMethodsServer.reportException(methodNameToReport,
							new Exception("value instanceof Float is <= 0"
									+ " ; valueColumnName = " + valueColumnName
									+ " ; valueFloat = " + valueFloat
									+ " ; Original SQL command : " + this.getSQLCommand())
					);
				}
				value = (U) Float.valueOf(valueFloat);

			} else {
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("Type of value not supported"
								+ " ; valueColumnName = " + valueColumnName
								+ " ; valueClassToInstanciate = " + valueClassToInstanciate.getCanonicalName()
								+ " ; Original SQL command : " + this.getSQLCommand())
				);
			}

			
			if(key == null && shouldBeNotEmptyOrZero){
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("key is null"
								+ " ; keyColumnName = " + keyColumnName
								+ " ; keyClassToInstanciate = " + keyClassToInstanciate.getCanonicalName()
								+ " ; Original SQL command : " + this.getSQLCommand())
				);
			}
			if(value == null && shouldBeNotEmptyOrZero){
				UtilitiesMethodsServer.reportException(methodNameToReport,
						new Exception("value is null"
								+ " ; valueColumnName = " + valueColumnName
								+ " ; class type of hmToReturn = " + valueClassToInstanciate.getCanonicalName()
								+ " ; Original SQL command : " + this.getSQLCommand())
				);
			}
			
			//map
			if(hmToReturn.containsKey(key)){
				HashSet<U> hsIT = hmToReturn.get(key);
				hsIT.add(value);
			} else {
				HashSet<U> hsIT = new HashSet<>();
				hsIT.add(value);
				hmToReturn.put(key, hsIT);
			}
			
		}
		
		return hmToReturn;
		
	}
	*/

}
