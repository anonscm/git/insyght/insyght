package fr.inra.jouy.server.objects;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.HashMap;
//import java.util.concurrent.ConcurrentHashMap;
import com.google.gwt.user.client.rpc.IsSerializable;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.interfaceIs.IsStringifyToJSON;
import fr.inra.jouy.shared.interfaceIs.IsMappingKeyValueToFields;

public class ObjAssociationAsMap implements IsSerializable, IsMappingKeyValueToFields, IsStringifyToJSON {
	
	//RETAB ConcurrentHashMap ??
	//String
	private HashMap<String, String> rsColumn2String = new HashMap<String,String>();
	private HashMap<String, ArrayList<String>> rsColumn2AlString = new HashMap<String,ArrayList<String>>();
	//Int
	private HashMap<String, Integer> rsColumn2Int = new HashMap<String,Integer>();
	private HashMap<String, ArrayList<Integer>> rsColumn2AlInt = new HashMap<String,ArrayList<Integer>>();
	//Boolean
	private HashMap<String, Boolean> rsColumn2Boolean = new HashMap<String,Boolean>();
	private HashMap<String, ArrayList<Boolean>> rsColumn2AlBoolean = new HashMap<String,ArrayList<Boolean>>();
	//Long
	private HashMap<String, Long> rsColumn2Long = new HashMap<String,Long>();
	private HashMap<String, ArrayList<Long>> rsColumn2AlLong = new HashMap<String,ArrayList<Long>>();
	//Double
	private HashMap<String, Double> rsColumn2Double = new HashMap<String,Double>();
	private HashMap<String, ArrayList<Double>> rsColumn2AlDouble = new HashMap<String,ArrayList<Double>>();
	//Float
	private HashMap<String, Float> rsColumn2Float = new HashMap<String,Float>();
	private HashMap<String, ArrayList<Float>> rsColumn2AlFloat = new HashMap<String,ArrayList<Float>>();
	
	//constructor
	public ObjAssociationAsMap(){
	}
	public ObjAssociationAsMap(String key, String value){
		rsColumn2String.put(key, value);
	}
	public ObjAssociationAsMap(String key, int value){
		rsColumn2Int.put(key, value);
	}
	public ObjAssociationAsMap(String key, Boolean value){
		rsColumn2Boolean.put(key, value);
	}
	public ObjAssociationAsMap(String key, Long value){
		rsColumn2Long.put(key, value);
	}
	public ObjAssociationAsMap(String key, Double value){
		rsColumn2Double.put(key, value);
	}
	public ObjAssociationAsMap(String key, Float value){
		rsColumn2Float.put(key, value);
	}
	
	//getters and setters
	public HashMap<String, String> getRsColumn2String() {
		return rsColumn2String;
	}
	public void setRsColumn2String(HashMap<String, String> rsColumn2String) {
		this.rsColumn2String = rsColumn2String;
	}
	public HashMap<String, Integer> getRsColumn2Int() {
		return rsColumn2Int;
	}
	public void setRsColumn2Int(HashMap<String, Integer> rsColumn2Int) {
		this.rsColumn2Int = rsColumn2Int;
	}
	public HashMap<String, ArrayList<Integer>> getRsColumn2AlInt() {
		return rsColumn2AlInt;
	}
	public void setRsColumn2AlInt(HashMap<String, ArrayList<Integer>> rsColumn2AlInt) {
		this.rsColumn2AlInt = rsColumn2AlInt;
	}
	public HashMap<String, ArrayList<String>> getRsColumn2AlString() {
		return rsColumn2AlString;
	}
	public void setRsColumn2AlString(HashMap<String, ArrayList<String>> rsColumn2AlString) {
		this.rsColumn2AlString = rsColumn2AlString;
	}

	public HashMap<String, Boolean> getRsColumn2Boolean() {
		return rsColumn2Boolean;
	}
	public void setRsColumn2Boolean(
			HashMap<String, Boolean> rsColumn2Boolean) {
		this.rsColumn2Boolean = rsColumn2Boolean;
	}
	public HashMap<String, ArrayList<Boolean>> getRsColumn2AlBoolean() {
		return rsColumn2AlBoolean;
	}
	public void setRsColumn2AlBoolean(
			HashMap<String, ArrayList<Boolean>> rsColumn2AlBoolean) {
		this.rsColumn2AlBoolean = rsColumn2AlBoolean;
	}
	public HashMap<String, Long> getRsColumn2Long() {
		return rsColumn2Long;
	}
	public void setRsColumn2Long(HashMap<String, Long> rsColumn2Long) {
		this.rsColumn2Long = rsColumn2Long;
	}
	public HashMap<String, ArrayList<Long>> getRsColumn2AlLong() {
		return rsColumn2AlLong;
	}
	public void setRsColumn2AlLong(
			HashMap<String, ArrayList<Long>> rsColumn2AlLong) {
		this.rsColumn2AlLong = rsColumn2AlLong;
	}
	public HashMap<String, Double> getRsColumn2Double() {
		return rsColumn2Double;
	}
	public void setRsColumn2Double(HashMap<String, Double> rsColumn2Double) {
		this.rsColumn2Double = rsColumn2Double;
	}
	public HashMap<String, ArrayList<Double>> getRsColumn2AlDouble() {
		return rsColumn2AlDouble;
	}
	public void setRsColumn2AlDouble(
			HashMap<String, ArrayList<Double>> rsColumn2AlDouble) {
		this.rsColumn2AlDouble = rsColumn2AlDouble;
	}
	public HashMap<String, Float> getRsColumn2Float() {
		return rsColumn2Float;
	}
	public void setRsColumn2Float(HashMap<String, Float> rsColumn2Float) {
		this.rsColumn2Float = rsColumn2Float;
	}
	public HashMap<String, ArrayList<Float>> getRsColumn2AlFloat() {
		return rsColumn2AlFloat;
	}
	public void setRsColumn2AlFloat(
			HashMap<String, ArrayList<Float>> rsColumn2AlFloat) {
		this.rsColumn2AlFloat = rsColumn2AlFloat;
	}
	@Override
	public boolean mapKeyIntValueToObjectField(String key, Integer value) {
		getRsColumn2Int().put(key, value);
		return true;
	}
	@Override
	public boolean mapKeyStringValueToObjectField(String key, String value) {
		getRsColumn2String().put(key, value);
		return true;
	}
	@Override
	public boolean mapKeyBooleanValueToObjectField(String key, Boolean value) {
		getRsColumn2Boolean().put(key, value);
		return true;
	}
	@Override
	public boolean mapKeyLongValueToObjectField(String key, Long value) {
		getRsColumn2Long().put(key, value);
		return true;
	}
	@Override
	public boolean mapKeyDoubleValueToObjectField(String key, Double value) {
		getRsColumn2Double().put(key, value);
		return true;
	}
	@Override
	public boolean mapKeyFloatValueToObjectField(String key, Float value) {
		getRsColumn2Float().put(key, value);
		return true;
	}
	@Override
	public boolean mapKeyUnknowTypeValueToObjectField(String key, String value) {
		return false;
	}
	
	@Override
	public String stringifyToJSON(boolean encloseWithCurlyBraces, boolean propagateToParentClassViaSuper) throws Exception {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String field2String = "";
		
		//String
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringString(
				getRsColumn2String().entrySet(),
				field2String.isEmpty()
				);
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringAlString(
				getRsColumn2AlString().entrySet(),
				field2String.isEmpty()
				);
		
		//Int
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringComparableNumber(
				getRsColumn2Int().entrySet(),
				null,
				field2String.isEmpty()
				);
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringAlComparableNumber(
				getRsColumn2AlInt().entrySet(),
				field2String.isEmpty()
				);

		//Boolean
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringBoolean(
				getRsColumn2Boolean().entrySet(),
				field2String.isEmpty()
				);
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringAlBoolean(
				getRsColumn2AlBoolean().entrySet(),
				field2String.isEmpty()
				);
		

		//Long
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringComparableNumber(
				getRsColumn2Long().entrySet(),
				null,
				field2String.isEmpty()
				);
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringAlComparableNumber(
				getRsColumn2AlLong().entrySet(),
				field2String.isEmpty()
				);
		
		//Double
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringComparableNumber(
				getRsColumn2Double().entrySet(),
				null,
				field2String.isEmpty()
				);
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringAlComparableNumber(
				getRsColumn2AlDouble().entrySet(),
				field2String.isEmpty()
				);
		
		
		//Float
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringComparableNumber(
				getRsColumn2Float().entrySet(),
				null,
				field2String.isEmpty()
				);
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_entrySetStringAlComparableNumber(
				getRsColumn2AlFloat().entrySet(),
				field2String.isEmpty()
				);
		
		if( encloseWithCurlyBraces){
			return "{"+field2String+"}";
		} else {
			return field2String;
		}
		
	}


//	@Override
//	public void fromJSON(String JSON) throws Exception {
//		//"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]
//	    for (MatchResult matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON); matcher != null; matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON)) {
//			String keyIT = matcher.getGroup(1);
//			String valueIT = matcher.getGroup(2);
//			boolean mappingSuccessful = mapKeyUnknowTypeValueToObjectField(keyIT,valueIT);
//			if( ! mappingSuccessful){
//				throw new Exception("The key "+keyIT+" (value = "+valueIT+") has not been mapped to any attribute");
//			}
//	    }
//	}
	
	
}
