package fr.inra.jouy.client;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.RPC.CallForLogin;
import fr.inra.jouy.client.RPC.CallForLoginAsync;

public class UncaughtErrorsHandler extends PopupPanel {

	private static UncaughtErrorsHandlerUiBinder uiBinder = GWT
			.create(UncaughtErrorsHandlerUiBinder.class);
	
	private final CallForLoginAsync loginService = (CallForLoginAsync) GWT
			.create(CallForLogin.class);
	
	interface UncaughtErrorsHandlerUiBinder extends
			UiBinder<Widget, UncaughtErrorsHandler> {
	}
	
	@UiField public Button hideError;
	@UiField public HTML errorHTML;

	public UncaughtErrorsHandler() {
		setWidget(uiBinder.createAndBindUi(this));
		setAnimationEnabled(true);
		setAutoHideEnabled(false);
		setAutoHideOnHistoryEventsEnabled(true);
		setPopupPosition(0, 0);
	}
	
	
	@UiHandler("hideError")
	void onhideErrorClick(ClickEvent e) {
		hide();
	}
	
	public void addAnErrorAndShowForXSeconds(int seconds, Throwable uncaughtError) {
		
		Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  hide();
		      }
		    };

		AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				Window.alert("Error in addAnErrorAndShowForXSeconds : "+caught.getLocalizedMessage());
				System.err.println(caught.getLocalizedMessage());
			}
			public void onSuccess(String stringSent){
				//Window.alert("stringSent : "+stringSent);
			}
		};
		
		
		
		try {
			
			String textFromThrowable = "Uncaught exception: ";
		
			//get the root cause
			StackTraceElement[] ste = uncaughtError.getStackTrace();
			textFromThrowable += "\n" + uncaughtError.toString() + "\n";
			for (int i = 0; i < ste.length; i++) {
				textFromThrowable += "    at " + ste[i] + "\n";
			}
			int loopCount = 0;
			while (uncaughtError.getCause() != null) {
				loopCount++;
				uncaughtError = uncaughtError.getCause();
				textFromThrowable += "\n" + uncaughtError.toString() + "\n";
				StackTraceElement[] steCause = uncaughtError.getStackTrace();
				for (int i = 0; i < steCause.length; i++) {
					textFromThrowable += "    at " + steCause[i] + "\n";
				}
				if (loopCount > 20) {
					break;
				}
			}
			
			/* OLD
			while (uncaughtError != null) {
				if(uncaughtError.getCause() == null){
					break;
					
				}
				uncaughtError = uncaughtError.getCause();
			}
			if(uncaughtError != null){
				textFromThrowable = "Uncaught exception: ";
				StackTraceElement[] stackTraceElements = uncaughtError.getStackTrace();
				textFromThrowable += uncaughtError.toString() + "\n";
				for (int i = 0; i < stackTraceElements.length; i++) {
					textFromThrowable += "    at " + stackTraceElements[i] + "\n";
				}
			}
			*/
		
			String recipient = "Insyght_dev@inra.fr";// thomas.lacroix@inra.fr ; insyght@jouy.inra.fr includes me, JF et Val, Insyght_dev@inra.fr is a mail box for the app
			String subject = "[Insyght] Automated error report";
			String content = "Automated error report from Insyght :\n\n";
			
//			if(errorText != null){
//				content += " - Error type :\n"+errorText+"\n";
//				content += "\n";
//			}
			if(!textFromThrowable.isEmpty()){
				content += " - Error Message :\n"+textFromThrowable+"\n";
				content += "\n";
			}
			
			content += " - Url :\n"+Window.Location.getHref()+"\n";
			content += "\n";
			
			content += " - Last navigation history :\n";
			for(String navHistory : NavigationControler.AL_HISTORY_STACK){
				content += "     - "+navHistory+"\n";
			}
			content += "\n";
			
			content += " - User agent :\n"+Window.Navigator.getUserAgent()+"\n";
			content += "\n";

			content += " - Permutation Strong Name :\n"+GWT.getPermutationStrongName()+"\n";
			content += "\n";
			
			content += " - Additional information :\n";
			content += "    *** CurrSelSyntenyCanvasItemRefId = "+Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId();
			if(Insyght.APP_CONTROLER.getCurrSelctGenomePanel() != null){
				content += "    *** CurrSelGenomePanelOrganismId = "+Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getOrganismId();
			}
			
			content += "\n";

			GWT.log(content);
			GWT.log("Error : ", uncaughtError);
			//System.err.println(uncaughtError.getLocalizedMessage());
			uncaughtError.printStackTrace();
			loginService.sendEmail(recipient,
					subject,
					content,
					callback);
			
			content = content.replaceAll(" ", "&nbsp;");
			content = content.replaceAll("\n", "</br>");
			
			errorHTML.setHTML(content);
			show();
			// Schedule the timer to run once in X seconds.
			t.schedule(seconds*1000);
			
		} catch (Exception e) {
			String mssgError = "ERROR voidifySessionIdIndb : " + e.getMessage();
			Window.alert(mssgError);
		}
		
		
	}

}
