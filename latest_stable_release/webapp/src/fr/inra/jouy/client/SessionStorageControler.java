package fr.inra.jouy.client;

import java.util.ArrayList;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Window;

import fr.inra.jouy.client.view.crossSections.popUp.PopUpListGenesDialog;
import fr.inra.jouy.client.view.search.SearchViewImpl;


/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

public class SessionStorageControler {

	// attributes
	//CountMostRepresPheno
	static public String CountMostRepresPheno_KEY_SORTED_LIST = "sortedGeneIdList.CountMostRepresPheno";
	static public String CountMostRepresPheno_PREFIX_KEY_INDIVIDUALS = "geneId.CountMostRepresPheno.";
	static public String CountMostRepresPheno_PREFIX_VALUE_INDIVIDUALS = "";
	static public int CountMostRepresPheno_REF_ORGA_ID = -1;
	static public ArrayList<Integer> CountMostRepresPheno_alSelectedPhenoPlusOrgaIds = new ArrayList<Integer>();
	
	//PValOverReprPheno
	static public String PValOverReprPheno_KEY_SORTED_LIST = "sortedGeneIdList.PValOverReprPheno";
	static public String PValOverReprPheno_PREFIX_KEY_INDIVIDUALS = "geneId.PValOverReprPheno.";
	static public String PValOverReprPheno_PREFIX_VALUE_INDIVIDUALS = "";
	static public int PValOverReprPheno_REF_ORGA_ID = -1;
	static public ArrayList<Integer> PValOverReprPheno_alSelectedPhenoPlusOrgaIds = new ArrayList<Integer>();
	static public ArrayList<Integer> PValOverReprPheno_alSelectedPhenoMinusOrgaIds = new ArrayList<Integer>();


	
	// methods
	// all purpose methods
	
	public static String getInfoSpecialScoreForGeneIfRelevantAndAvailable(
			int geneId
			, boolean asBulletList
			, boolean asBig) {
		String stToReturn = "";
		if(PopUpListGenesDialog.geneListSortType != null 
				&& PopUpListGenesDialog.geneListSortType.compareTo(PopUpListGenesDialog.GeneListSortType.SCORE_PValOverReprPheno) == 0){
			Storage sessionStorage = Storage.getSessionStorageIfSupported();
			if(sessionStorage != null){
				String score = sessionStorage.getItem(SessionStorageControler.PValOverReprPheno_PREFIX_KEY_INDIVIDUALS
			    				+ geneId);
				if(score != null){
					if(asBig){
						stToReturn += "<big>";
					}
					if(asBulletList){
						stToReturn += "<ul><li>";
					}
					stToReturn += "<span style=\"color:#145A32;\"><i>P-value over representation in group + :</i> "+score+"</span>";//#154360
					if(asBulletList){
						stToReturn += "</li></ul>";
					}
					if(asBig){
						stToReturn += "</big>";
					}
					if( ! asBulletList){
						stToReturn += "<br/>";
					}
				}
			}
		} else if (PopUpListGenesDialog.geneListSortType != null 
				&& PopUpListGenesDialog.geneListSortType.compareTo(PopUpListGenesDialog.GeneListSortType.SCORE_CountMostRepresPheno) == 0){
			Storage sessionStorage = Storage.getSessionStorageIfSupported();
			if(sessionStorage != null){
				String score = sessionStorage.getItem(SessionStorageControler.CountMostRepresPheno_PREFIX_KEY_INDIVIDUALS
			    				+ geneId);
				if(score != null){
					if(asBig){
						stToReturn += "<big>";
					}
					if(asBulletList){
						stToReturn += "<ul><li>";
					}
					if ( SearchViewImpl.countParalogs ) {
						stToReturn += "<span style=\"color:#145A32;\"><i>Number different paralogs :</i> "+score+"</span>";//#154360, #145A32
					} else {
						stToReturn += "<span style=\"color:#145A32;\"><i>Number different orthologs in group + :</i> "+score+"</span>";//#154360, #145A32
					}
					
					if(asBulletList){
						stToReturn += "</li></ul>";
					}
					if(asBig){
						stToReturn += "</big>";
					}
					if( ! asBulletList){
						stToReturn += "<br/>";
					}
				}
			}
		}
		return stToReturn;
	}
	
	
	public static void clearAllScoresPhenotypeCategories() {
		SessionStorageControler.clear_PValOverReprPheno();
		SessionStorageControler.clear_CountMostRepresPheno();
	}

	//CountMostRepresPheno
	private static void clear_CountMostRepresPheno() {
		//clear app attributes
		CountMostRepresPheno_REF_ORGA_ID = -1;
		CountMostRepresPheno_alSelectedPhenoPlusOrgaIds = new ArrayList<Integer>();
		//clear sessionStorage data
		Storage sessionStorage = Storage.getSessionStorageIfSupported();
		if(sessionStorage != null){
			//sessionStorage.clear();
			ArrayList<String> listKeysToRemove = new ArrayList<>();
			for(int i = 0 ; i < sessionStorage.getLength() ; i++){
				String keyIT = sessionStorage.key(i);
				if(keyIT.compareTo(
						SessionStorageControler.CountMostRepresPheno_KEY_SORTED_LIST
						) == 0){
					listKeysToRemove.add(keyIT);
				} else if (keyIT.startsWith(
						SessionStorageControler.CountMostRepresPheno_PREFIX_KEY_INDIVIDUALS
						)){
					listKeysToRemove.add(keyIT);
				}
			}
			for(String KeyToRemoveIT : listKeysToRemove){
				sessionStorage.removeItem(KeyToRemoveIT);
			}
		} else {
			Window.alert("Session Storage is not supported in your browser, this functionality is therfore not available."
					+ " Please update your browser to enable this functionality.");
		}
	}
	
	//PValOverReprPheno
//	static public String getToolTip_PValOverReprPheno(){
//		if(PValOverReprPheno_REF_ORGA_ID >= 0){
//			String stToReturn = "Reference organism : "
//					+ Insyght.APP_CONTROLER.findOrganismItemWithOrigamiOrganismId(PValOverReprPheno_REF_ORGA_ID).getFullName();
//			stToReturn +=  " ; " + PValOverReprPheno_alSelectedPhenoPlusOrgaIds.size() + " organism(s) phenotypes + : ";
//			for (OrganismItem oiIT : Insyght.APP_CONTROLER.findAlPublicOrganismItemWithAlOrganismId(PValOverReprPheno_alSelectedPhenoPlusOrgaIds)) {
//				stToReturn += oiIT.getFullName() + ", ";
//			}
//			stToReturn +=  " ; " + PValOverReprPheno_alSelectedPhenoMinusOrgaIds.size() + " organism(s) phenotypes - : ";
//			for (OrganismItem oiIT : Insyght.APP_CONTROLER.findAlPublicOrganismItemWithAlOrganismId(PValOverReprPheno_alSelectedPhenoMinusOrgaIds)) {
//				stToReturn += oiIT.getFullName() + ", ";
//			}
//			return stToReturn;
//		}
//		return "";
//	}
	static public void clear_PValOverReprPheno(){
		//clear app attributes
		PValOverReprPheno_REF_ORGA_ID = -1;
		PValOverReprPheno_alSelectedPhenoPlusOrgaIds = new ArrayList<Integer>();
		PValOverReprPheno_alSelectedPhenoMinusOrgaIds = new ArrayList<Integer>();
		//clear sessionStorage data
		Storage sessionStorage = Storage.getSessionStorageIfSupported();
		if(sessionStorage != null){
			//sessionStorage.clear();
			ArrayList<String> listKeysToRemove = new ArrayList<>();
			for(int i = 0 ; i < sessionStorage.getLength() ; i++){
				String keyIT = sessionStorage.key(i);
				if(keyIT.compareTo(
						SessionStorageControler.PValOverReprPheno_KEY_SORTED_LIST
						) == 0){
					listKeysToRemove.add(keyIT);
				} else if (keyIT.startsWith(
						SessionStorageControler.PValOverReprPheno_PREFIX_KEY_INDIVIDUALS
						)){
					listKeysToRemove.add(keyIT);
				}
			}
			for(String KeyToRemoveIT : listKeysToRemove){
				sessionStorage.removeItem(KeyToRemoveIT);
			}
		} else {
			Window.alert("Session Storage is not supported in your browser, this functionality is therfore not available."
					+ " Please update your browser to enable this functionality.");
		}
	}

	
}
