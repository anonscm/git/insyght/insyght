package fr.inra.jouy.client.view.search;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.shared.pojos.databaseMapping.TaxoItem;

public class PopupGroupCoreDisp extends PopupPanel {

	private static PopupGroupCoreDispUiBinder uiBinder = GWT.create(PopupGroupCoreDispUiBinder.class);

	interface PopupGroupCoreDispUiBinder extends UiBinder<Widget, PopupGroupCoreDisp> {
	}
	
	@UiField static HTML title;
	@UiField static Button close;
	@UiField static HTMLPanel menu;
	@UiField static Button setSelectedOrganismsAsGroupPlus;
	@UiField static Button setSelectedOrganismsAsGroupMinus;
	@UiField static Button setSelectedOrganismsAsGroupNotTakenIntoAccount;
	@UiField static HTML headerGroup;
	@UiField static VerticalPanel vpListOrga;
	
	private SearchViewImpl.EnumGroupGenomes group;
	
	private static boolean groupsHaveChanged = false;
	private static boolean IS_RESIZING = false;
	
	public PopupGroupCoreDisp() {
		setWidget(uiBinder.createAndBindUi(this));
		setAnimationEnabled(true);
		setAutoHideEnabled(true);
		setAutoHideOnHistoryEventsEnabled(true);
		setGlassEnabled(true);
		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				if (isShowing() && ! IS_RESIZING) {
					IS_RESIZING = true;
					adaptSizeAndShow();
					IS_RESIZING = false;
				}
			}
        });
		
		setSelectedOrganismsAsGroupPlus.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int madeSomeChangesIt = 0;
				for (int i=vpListOrga.getWidgetCount()-1; i >= 0 ; i--) {
					CheckBox cbIT = (CheckBox) vpListOrga.getWidget(i);
					if (cbIT.getValue()) {
						TaxoItem tiIT = getCorrectSourceAlTaxoItem().get(i);
						int innerCountChanges = 0;
						innerCountChanges += SearchViewImpl.manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, SearchViewImpl.EnumGroupGenomes.plus, true);
						if (innerCountChanges > 0) {
							innerCountChanges += SearchViewImpl.manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, group, false);
						}
						madeSomeChangesIt += innerCountChanges;
						//SearchViewImpl.alRefTaxoNodeOrgaItemsInPhenoPlusCategory.add(getCorrectSourceAlTaxoItem().get(i));
						//getCorrectSourceAlTaxoItem().remove(i);
					}
				}
				if (madeSomeChangesIt > 0) {
					setGroupToShow(getCorrectSourceAlTaxoItem(), group);
					groupsHaveChanged = true;
				} else {
					Window.alert("The maximum number of organisms allowed in group + and group - combined have been reached.");
				}
			}
		});
		setSelectedOrganismsAsGroupMinus.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int madeSomeChangesIt = 0;
				for (int i=vpListOrga.getWidgetCount()-1; i >= 0 ; i--) {
					CheckBox cbIT = (CheckBox) vpListOrga.getWidget(i);
					if (cbIT.getValue()) {
						TaxoItem tiIT = getCorrectSourceAlTaxoItem().get(i);
						int innerCountChanges = 0;
						innerCountChanges += SearchViewImpl.manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, SearchViewImpl.EnumGroupGenomes.minus, true);
						if (innerCountChanges > 0) {
							innerCountChanges += SearchViewImpl.manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, group, false);
						}
						madeSomeChangesIt += innerCountChanges;
//						SearchViewImpl.alRefTaxoNodeOrgaItemsInPhenoMinusCategory.add(getCorrectSourceAlTaxoItem().get(i));
//						getCorrectSourceAlTaxoItem().remove(i);
					}
				}
				if (madeSomeChangesIt > 0) {
					setGroupToShow(getCorrectSourceAlTaxoItem(), group);
					groupsHaveChanged = true;
				} else {
					Window.alert("The maximum number of organisms allowed in group + and group - combined have been reached.");
				}
			}
		});
		setSelectedOrganismsAsGroupNotTakenIntoAccount.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				boolean hasChanged = false;
				for (int i=vpListOrga.getWidgetCount()-1; i >= 0 ; i--) {
					CheckBox cbIT = (CheckBox) vpListOrga.getWidget(i);
					if (cbIT.getValue()) {
						hasChanged = true;
						TaxoItem tiIT = getCorrectSourceAlTaxoItem().get(i);
						SearchViewImpl.manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, SearchViewImpl.EnumGroupGenomes.notTakenAccount, true);
						SearchViewImpl.manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, group, false);
//						SearchViewImpl.alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.add(getCorrectSourceAlTaxoItem().get(i));
//						getCorrectSourceAlTaxoItem().remove(i);
					}
				}
				if (hasChanged) {
					setGroupToShow(getCorrectSourceAlTaxoItem(), group);
					groupsHaveChanged = true;
				}
			}
		});
		
	}
	
//	@Override
//	protected void onAttach() {
//		super.onAttach();
//		groupsHaveChanged = false;
//	}
	@Override
	protected void onLoad() {
		groupsHaveChanged = false;
	}
	
//	@Override
//	protected void onDetach() {
//		super.onDetach();
//	}
	@Override
	protected void onUnload() {
		if (groupsHaveChanged) {
			SearchViewImpl.updateRelativeCountGenomesInPhenotypeCategories();
			SearchViewImpl.colorTaxoTreeInCoreDispGenomeContext(SearchViewImpl.selectedRefTaxoNode);
			SearchViewImpl.forceRedrawTaxoBrowserRefNode("NO_SELECTION");
			SearchViewImpl.parametersToCalculateCoreDispSetCDSshaveChanged = true;
			SearchViewImpl.updateCurrentCoreDispSetCDSs();
		}
	}
	
	
	@UiHandler("close")
	void onClickClose(ClickEvent e) {
		hide();
	}
	
	public void adaptSizeAndShow(){
		int clientWidth = Window.getClientWidth();
		int clientHeight = Window.getClientHeight();
		this.setHeight(((3 * clientHeight) / 4) + "px");
		this.setWidth(((3 * clientWidth) / 4) + "px");
		center();
	}
	
	public void setGroupToShow(
			ArrayList<TaxoItem> alTaxoItemsSent
			, SearchViewImpl.EnumGroupGenomes groupSent
			){
		
		group = groupSent;
				
		if (group.compareTo(SearchViewImpl.EnumGroupGenomes.plus) == 0) {
			title.setHTML("Detailed <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span> information");
			setSelectedOrganismsAsGroupPlus.setVisible(false);
			setSelectedOrganismsAsGroupMinus.setVisible(true);
			setSelectedOrganismsAsGroupNotTakenIntoAccount.setVisible(true);
			vpListOrga.clear();
			if (getCorrectSourceAlTaxoItem().isEmpty()) {
				menu.setVisible(false);
				setSelectedOrganismsAsGroupPlus.setEnabled(false);
				setSelectedOrganismsAsGroupMinus.setEnabled(false);
				setSelectedOrganismsAsGroupNotTakenIntoAccount.setEnabled(false);
				String htmlTextIT = "Reference organism : "
				+ Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
				+ "</br>";
				htmlTextIT += "In addition to the reference organism, <span style=\"font-weight: bold;\">no other organism</span> are in the <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span>.</br>";
				headerGroup.setHTML(htmlTextIT);
			} else {
				menu.setVisible(true);
				setSelectedOrganismsAsGroupPlus.setEnabled(true);
				setSelectedOrganismsAsGroupMinus.setEnabled(true);
				setSelectedOrganismsAsGroupNotTakenIntoAccount.setEnabled(true);
				String htmlTextIT = "Reference organism : "
				+ Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
				+ "</br>";
				htmlTextIT += "In addition to the reference organism, the following <span style=\"font-weight: bold;\">"+getCorrectSourceAlTaxoItem().size()+" organism(s)</span> are in the <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span> :</br>";
				headerGroup.setHTML(htmlTextIT);
				for(TaxoItem tiIT : getCorrectSourceAlTaxoItem()){
					final CheckBox cboxIT = new CheckBox(
							"<big>"+ tiIT.getFullName() +"</big>"
							, true
							);
					vpListOrga.add(cboxIT);
				}
			}
				
			
		} else if (group.compareTo(SearchViewImpl.EnumGroupGenomes.minus) == 0) {
			title.setHTML("Detailed <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span> information");
			setSelectedOrganismsAsGroupPlus.setVisible(true);
			setSelectedOrganismsAsGroupMinus.setVisible(false);
			setSelectedOrganismsAsGroupNotTakenIntoAccount.setVisible(true);
			vpListOrga.clear();
			if (getCorrectSourceAlTaxoItem().isEmpty()) {
				menu.setVisible(false);
				setSelectedOrganismsAsGroupPlus.setEnabled(false);
				setSelectedOrganismsAsGroupMinus.setEnabled(false);
				setSelectedOrganismsAsGroupNotTakenIntoAccount.setEnabled(false);
				String htmlTextIT = "<span style=\"font-weight: bold;\">No organism</span> are in the <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span>.</br>";
				headerGroup.setHTML(htmlTextIT);
			} else {
				menu.setVisible(true);
				setSelectedOrganismsAsGroupPlus.setEnabled(true);
				setSelectedOrganismsAsGroupMinus.setEnabled(true);
				setSelectedOrganismsAsGroupNotTakenIntoAccount.setEnabled(true);
				String htmlTextIT = "The following <span style=\"font-weight: bold;\">"+getCorrectSourceAlTaxoItem().size()+" organism(s)</span> are in the <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span> :</br>";
				headerGroup.setHTML(htmlTextIT);
				for(TaxoItem tiIT : getCorrectSourceAlTaxoItem()){
					final CheckBox cboxIT = new CheckBox(
							"<big>"+ tiIT.getFullName() +"</big>"
							, true
							);
					vpListOrga.add(cboxIT);
				}
			}
		} else if (group.compareTo(SearchViewImpl.EnumGroupGenomes.notTakenAccount) == 0) {
			title.setHTML("Detailed <span style=\"background-color:#ffe6eb;color:#8B0A50;\">not taken into account</span> information");
			setSelectedOrganismsAsGroupPlus.setVisible(true);
			setSelectedOrganismsAsGroupMinus.setVisible(true);
			setSelectedOrganismsAsGroupNotTakenIntoAccount.setVisible(false);
			vpListOrga.clear();
			if (getCorrectSourceAlTaxoItem().isEmpty()) {
				menu.setVisible(false);
				setSelectedOrganismsAsGroupPlus.setEnabled(false);
				setSelectedOrganismsAsGroupMinus.setEnabled(false);
				setSelectedOrganismsAsGroupNotTakenIntoAccount.setEnabled(false);
				String htmlTextIT = "<span style=\"font-weight: bold;\">No organism</span> are <span style=\"background-color:#ffe6eb;color:#8B0A50;\">not taken into account</span>.</br>";
				headerGroup.setHTML(htmlTextIT);
			} else {
				menu.setVisible(true);
				setSelectedOrganismsAsGroupPlus.setEnabled(true);
				setSelectedOrganismsAsGroupMinus.setEnabled(true);
				setSelectedOrganismsAsGroupNotTakenIntoAccount.setEnabled(true);
				String htmlTextIT = "The following <span style=\"font-weight: bold;\">"+getCorrectSourceAlTaxoItem().size()+" organism(s)</span> are <span style=\"background-color:#ffe6eb;color:#8B0A50;\">not taken into account</span> :</br>";
				headerGroup.setHTML(htmlTextIT);
				for(TaxoItem tiIT : getCorrectSourceAlTaxoItem()){
					final CheckBox cboxIT = new CheckBox(
							"<big>"+ tiIT.getFullName() +"</big>"
							, true
							);
					vpListOrga.add(cboxIT);
				}
			}
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in PopupGroupCoreDisp setGroupToShow : unrecognized group = "+group));
		}
	}

	private ArrayList<TaxoItem> getCorrectSourceAlTaxoItem() {
		if (group.compareTo(SearchViewImpl.EnumGroupGenomes.plus) == 0) {
			return SearchViewImpl.alRefTaxoNodeOrgaItemsInPhenoPlusCategory;
		} else if (group.compareTo(SearchViewImpl.EnumGroupGenomes.minus) == 0) {
			return SearchViewImpl.alRefTaxoNodeOrgaItemsInPhenoMinusCategory;
		} else if (group.compareTo(SearchViewImpl.EnumGroupGenomes.notTakenAccount) == 0) {
			return SearchViewImpl.alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory;
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in PopupGroupCoreDisp setGroupToShow : unrecognized group = "+group));
		}
		return null;
	}

}
