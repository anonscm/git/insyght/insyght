package fr.inra.jouy.client.view.result;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import java.util.ArrayList;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.RPC.CallForHomoBrowResu;
import fr.inra.jouy.client.RPC.CallForHomoBrowResuAsync;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;

public class ExportTableDiag extends PopupPanel {

	private static ExportTableDiagUiBinder uiBinder = GWT
			.create(ExportTableDiagUiBinder.class);

	private final static CallForHomoBrowResuAsync homologieBrowsingResultService = (CallForHomoBrowResuAsync) GWT
			.create(CallForHomoBrowResu.class);

	interface ExportTableDiagUiBinder extends UiBinder<Widget, ExportTableDiag> {
	}

	@UiField public static DockLayoutPanel rootPanel;
	@UiField HTML close;
	@UiField DeckPanel dpRoot;
	@UiField HTML titleExportBox;
	@UiField ListBox lbDetailSummaryOrtholoTable;
	@UiField ListBox lbFieldDelimiter;
	@UiField ListBox lbDownloadEmailPopup;
	@UiField TextBox exportEmailAddress;
	@UiField Button submit;
	@UiField HTML infoExport;
	@UiField Button closeAfterSubmit;
	@UiField HTML feedBackMssg;
	private static boolean IS_RESIZING = false;
	
	public ExportTableDiag() {
		setWidget(uiBinder.createAndBindUi(this));
		setAnimationEnabled(true);
		setGlassEnabled(true);
		setAutoHideEnabled(true);
		dpRoot.showWidget(0);
		feedBackMssg.setText("");
		feedBackMssg.setStyleDependentName("fadeIn7Seconds", false);//fadeIn7Seconds fadeInRed4Seconds
		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				if (isShowing() && ! IS_RESIZING) {
					IS_RESIZING = true;
					displayPoPup();
					IS_RESIZING = false;
				}
			}
        });
		lbDownloadEmailPopup.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if (lbDownloadEmailPopup.getSelectedValue().compareTo("email") == 0) {
					exportEmailAddress.setVisible(true);
				} else {
					exportEmailAddress.setVisible(false);
				}
			}
		});
		
	}

	public void displayPoPup(){
		
		setUpPopUpLayout();
		
		
		int clientWidth = Window.getClientWidth();
		int clientHeight = Window.getClientHeight();
		rootPanel.setHeight(((3 * clientHeight) / 4) + "px");
		rootPanel.setWidth(((3 * clientWidth) / 4) + "px");
		center();
	}
	
	private void setUpPopUpLayout() {
		// TODO check
		if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getViewTypeInsyght()
				.compareTo(EnumResultViewTypes.homolog_table) == 0) {
			titleExportBox.setHTML("<span style=\"font-size:large;\">Export data from <span style=\"color:#8B3A3A;\"> the homologs table</span></span>");
			lbDetailSummaryOrtholoTable.clear();
			lbDetailSummaryOrtholoTable.addItem("summary", "SUMMARY");
			lbDetailSummaryOrtholoTable.addItem("detailled", "DETAIL");
			lbFieldDelimiter.clear();
			lbFieldDelimiter.addItem("semi-colon", ";");
			lbFieldDelimiter.addItem("tab", "\t");
			//lbFieldDelimiter.addItem("coma", ",");
			lbDownloadEmailPopup.clear();
			lbDownloadEmailPopup.addItem("email :", "email");
			exportEmailAddress.setVisible(true);
			//lbDownloadEmailPopup.addItem("pop-up browser", "pop_up_browser");
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getViewTypeInsyght()
				.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
			titleExportBox.setHTML("<span style=\"font-size:large;\">Export data from <span style=\"color:#218868;\">the annotation comparator</span></span>");
			lbDetailSummaryOrtholoTable.clear();
			lbDetailSummaryOrtholoTable.addItem("detailled", "DETAIL");
			lbFieldDelimiter.clear();
			lbFieldDelimiter.addItem("coma", ",");
			lbDownloadEmailPopup.clear();
			lbDownloadEmailPopup.addItem("email :", "email");
			exportEmailAddress.setVisible(true);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ExportTableDiag -> setUpPopUpLayout : unrecognized ViewType = "+Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getViewTypeInsyght()) );
		}
		
	}

	@UiHandler("close")
	void onCloseClick(ClickEvent e) {
		hide();
	}
	
	@UiHandler("closeAfterSubmit")
	void onCloseAfterSubmitClick(ClickEvent e) {
		hide();
	}
	
	@UiHandler("submit")
	void onSubmitClick(ClickEvent e) {
		parseFormAndExportTable();
	}
	
//	@Override
//	protected void onDetach() {
//		super.onDetach();
//	}
	@Override
	protected void onUnload() {
		exportEmailAddress.setText("");
		feedBackMssg.setText("");
		feedBackMssg.setStyleDependentName("fadeIn7Seconds", false);//fadeIn7Seconds fadeInRed4Seconds
	}
	
	private void parseFormAndExportTable() {
		final String emailAddress = exportEmailAddress.getText().trim();

		//getCompaAnnotRefGenesWithGeneIdAndParams
		if( lbDownloadEmailPopup.getSelectedValue().compareTo("email") == 0 && 
				(emailAddress.isEmpty() || ! emailAddress.contains("@") ) ){
//			dpRoot.showWidget(1);
//			infoExport.setStyleDependentName("greenText", false);
//			infoExport.setStyleDependentName("redText", true);
//			infoExport.setText("Error Invalid parameter : email address is empty");
			feedBackMssg.setHTML(
					"Error Invalid parameter : email address"
						);
			feedBackMssg.setStyleDependentName("fadeIn7Seconds", true);//fadeIn7Seconds fadeInRed4Seconds
			Timer timer = new Timer() {
			     @Override
			     public void run() {
						feedBackMssg.setHTML("");
						feedBackMssg.setStyleDependentName("fadeIn7Seconds", false);//fadeIn7Seconds fadeInRed4Seconds
			     }
			};
			timer.schedule(7000);
			return;
		}
		

		AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				dpRoot.showWidget(1);
				infoExport.setText("Error: "+caught.getMessage());
				caught.printStackTrace();
			}
			public void onSuccess(String returnedMssg) {
				dpRoot.showWidget(1);
				infoExport.setStyleDependentName("redText", false);
				infoExport.setStyleDependentName("greenText", true);
				infoExport.setText("The data is being exported. You will received the results in your email: "+
						emailAddress+". The waiting time may vary from a few minutes to a couple of hours."
						);
				
			}
		};
		
		if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getViewTypeInsyght()
				.compareTo(EnumResultViewTypes.homolog_table) == 0) {
			
			boolean summaryConservationData = false;
			if ( lbDetailSummaryOrtholoTable.getSelectedValue().compareTo("SUMMARY") == 0 ) {
				summaryConservationData = true;
			}
			
			HashSet<Integer> hsFeaturedOrganismIds = new HashSet<>();
			HashSet<Integer> hsMainListOrganismIds = new HashSet<>();
//			for (LightOrganismItem loiFeaturedIT : Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults()) {
//				hsFeaturedOrganismIds.add(loiFeaturedIT.getOrganismId());
//			}
			for (GenomePanelItem gpiFeaturedListIT : Insyght.APP_CONTROLER.getCurrentListFeaturedResultGenomePanelItem()) {
				hsFeaturedOrganismIds.add(gpiFeaturedListIT.getOrganismId());
			}
			for (GenomePanelItem gpiMainListIT : Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem()) {
				hsMainListOrganismIds.add(gpiMainListIT.getOrganismId());
			}
			
			
			try {

				homologieBrowsingResultService.exportToEmailHomoBroTable(
						emailAddress
						, Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable()
						, Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId()
						, hsFeaturedOrganismIds
						, hsMainListOrganismIds
						//, Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getResultListSortScopeType()
						, Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_scope()
						, Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortType()
						, Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortOrder()
						, summaryConservationData
						, lbFieldDelimiter.getSelectedValue()
						, callback
						);
			} catch (Exception ex) {
				//String mssgError = "ERROR in parseFormAndExportTable : "+ ex.getMessage();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, ex);
				//ex.printStackTrace();
			}
			
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getViewTypeInsyght()
				.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {

			
			try {

				Double paramMaxEvalueToSend = AnnotCompaViewImpl.getParamMaxEvalueToSend();
				if(paramMaxEvalueToSend == -1.0){
					dpRoot.showWidget(1);
					infoExport.setStyleDependentName("greenText", false);
					infoExport.setStyleDependentName("redText", true);
					infoExport.setText("Error Invalid parameter : Homologs max Evalue");
					return;
				}
				int paramMinPercentIdentityToSend = AnnotCompaViewImpl.getParamMinPercentIdentityToSend();
				if(paramMinPercentIdentityToSend == -1){
					dpRoot.showWidget(1);
					infoExport.setStyleDependentName("greenText", false);
					infoExport.setStyleDependentName("redText", true);
					infoExport.setText("Error Invalid parameter : Homologs min % identity");
					return;
				}
				int paramMinPercentAlignLenghtToSend = AnnotCompaViewImpl.getParamMinPercentAlignLenghtToSend();
				if(paramMinPercentAlignLenghtToSend == -1){
					dpRoot.showWidget(1);
					infoExport.setStyleDependentName("greenText", false);
					infoExport.setStyleDependentName("redText", true);
					infoExport.setText("Error Invalid parameter : Homologs min % alignment length");
					return;
				}
				boolean paramBdbhOnlyToSend = AnnotCompaViewImpl.getParamBdbhOnlyToSend();
				ArrayList<Integer> alOrgaIdsToSend = new ArrayList<Integer>();
//				if(assumeAllOrgaRoot){
//					//do nothing
//				}else 
				if(AnnotCompaViewImpl.listComparedOrgaToSend == null){
					dpRoot.showWidget(1);
					infoExport.setStyleDependentName("greenText", false);
					infoExport.setStyleDependentName("redText", true);
					infoExport.setText("Error Invalid parameter : compared set of organisms");
					return;
				}else{
					for(int i=0;i<AnnotCompaViewImpl.listComparedOrgaToSend.size();i++){
						OrganismItem oiIT = AnnotCompaViewImpl.listComparedOrgaToSend.get(i);
						alOrgaIdsToSend.add(oiIT.getOrganismId());
					}
				}
				
				homologieBrowsingResultService.exportToEmailCompaAnnotRefGenesWithListGeneIdAndParams(
						emailAddress,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator(),
						paramMaxEvalueToSend,
						paramMinPercentIdentityToSend,
						paramMinPercentAlignLenghtToSend,
						alOrgaIdsToSend,
						paramBdbhOnlyToSend,
						callback);
			} catch (Exception ex) {
				//String mssgError = "ERROR in parseFormAndExportTable : "+ ex.getMessage();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, ex);
				//ex.printStackTrace();
			}
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ExportTable -> parseFormAndExportTable : unrecognized ViewType = "+Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getViewTypeInsyght()) );
		}
		
		
	}
}
