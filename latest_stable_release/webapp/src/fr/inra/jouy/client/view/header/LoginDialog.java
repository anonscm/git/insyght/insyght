package fr.inra.jouy.client.view.header;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.RPC.CallForLogin;
import fr.inra.jouy.client.RPC.CallForLoginAsync;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.admin.AdminTab;
import fr.inra.jouy.client.view.result.CenterSLPAllGenomePanels;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
import fr.inra.jouy.client.view.search.SearchViewImpl;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.users.PersonsObj;

public class LoginDialog extends DialogBox {

	private static LoginDialogUiBinder uiBinder = GWT
			.create(LoginDialogUiBinder.class);

	private final CallForLoginAsync loginService = (CallForLoginAsync) GWT
			.create(CallForLogin.class);

	interface LoginDialogUiBinder extends UiBinder<Widget, LoginDialog> {
	}

	//login
	@UiField
	DisclosurePanel dpLogin;
	@UiField
	Button loginButton;
	@UiField
	Button closeButton;
	@UiField
	TextBox tbLogin;
	@UiField
	PasswordTextBox ptbLogin;

	//Register
	@UiField
	DisclosurePanel dpRegister;
	@UiField
	Button registerButtonR;
	@UiField
	Button closeButtonR;
	@UiField
	TextBox tbLoginR;
	/*@UiField
	PasswordTextBox ptbLoginR;
	@UiField
	PasswordTextBox ptbLoginRC;*/

	//forgott pssd
	@UiField
	DisclosurePanel dpGotgotPssd;
	@UiField
	Button forgotButtonF;
	@UiField
	Button closeButtonF;
	@UiField
	TextBox tbLoginF;

	public LoginDialog(boolean logout) {
		setWidget(uiBinder.createAndBindUi(this));
		if(logout){
			voidifySessionIdIndb();
		}else{
			setText("Login / register :");
			
			setAnimationEnabled(true);
			setGlassEnabled(true);
	
			// disclosure panel desc score
			dpLogin.setAnimationEnabled(true);
			dpLogin.setWidth("97%");
			dpLogin.setOpen(true);
	
			dpRegister.setAnimationEnabled(true);
			dpRegister.setWidth("97%");
	
			dpGotgotPssd.setAnimationEnabled(true);
			dpGotgotPssd.setWidth("97%");
		}
	}

	
	private void voidifySessionIdIndb() {
		
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			/* for Java 1.6 JDK et compiler compliance
			@Override
			 */
			public void onSuccess(String stringReturned) {
				//Window.alert("Your have been loged out successfully : "+stringReturned);
				//hide();
			}
		};


		try {
			loginService.voidifySessionIdIndb(Insyght.APP_CONTROLER.getCurrentUser().getSessionId(), callback);
		} catch (Exception e) {
			//String mssgError = "ERROR voidifySessionIdIndb : " + e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}
	
	}

	
	//Login handling

	@UiHandler("closeButton")
	void onSignOutClicked(ClickEvent event) {
		hide();
	}

	@UiHandler("loginButton")
	void onLoginClicked(ClickEvent event) {
		//Window.alert("login : "+tbLogin.getText()+" pssd : "+ptbLogin.getText());

		AsyncCallback<PersonsObj> callback = new AsyncCallback<PersonsObj>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Window.alert("RPC authentificationByLogin failed : "
						+ caught.getMessage());
			}

			/* for Java 1.6 JDK et compiler compliance
			@Override
			 */

			public void onSuccess(PersonsObj personObjReturned) {

				setLoginParameters(personObjReturned, true);

				hide();
			}
		};

		try {
			
			loginService.authentificationByLogin(tbLogin.getText().trim(),
					ptbLogin.getText().trim()
					, callback);
		} catch (Exception e) {
			String mssgError = "ERROR : " + e.getMessage();
			Window.alert(mssgError);
		}

	}

	//Register handling

	@UiHandler("closeButtonR")
	void onCloseButtonRClicked(ClickEvent event) {
		hide();
	}

	@UiHandler("registerButtonR")
	void onRegisterButtonRClicked(ClickEvent event) {

		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			/* for Java 1.6 JDK et compiler compliance
			@Override
			 */
			public void onSuccess(String stringReturned) {

				Window.alert("An email has been sent to the address your specified." +
								" It contains instructions on how to to activate your account." +
								" If any problem, don't hesitate to contact us at insyght@jouy.inra.fr.");
				hide();
			}
		};


		try {
			loginService.registerNewUser(tbLoginR.getText(), callback);
		} catch (Exception e) {
			//String mssgError = "ERROR : " + e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}
	}

	//Forgott pssd

	@UiHandler("closeButtonF")
	void onCloseButtonFClicked(ClickEvent event) {
		hide();
	}

	@UiHandler("forgotButtonF")
	void onForgotButtonFClicked(ClickEvent event) {
		//Window.alert("forgot too bad...");

		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			/* for Java 1.6 JDK et compiler compliance
			@Override
			 */
			public void onSuccess(String stringReturned) {
				Window
						.alert("Your new password has been sent to your email account at "
								+ tbLoginF.getText());
				hide();
			}
		};

		if (tbLoginF.getText().length() < 1) {
			Window.alert("ERROR : please enter your email address above.");
		} else {
			try {
				loginService.forgotPssd(tbLoginF.getText(), callback);
			} catch (Exception e) {
				//String mssgError = "ERROR : " + e.getMessage();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			}
		}

	}

	public static void setLoginParameters(PersonsObj personObjReturned,
			boolean reloadCorrectUIUndelayed) {


		/*Window.alert("login in..." + personObjReturned.getLogin()
				+ ", reload = " + reloadCorrectUIUndelayed);*/

		//login
		Insyght.APP_CONTROLER.setCurrentUser(personObjReturned);
		GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
		CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED = personObjReturned.getListGroupsSubscribed().size();
		Insyght.APP_CONTROLER.resetCurrentListPrivateResultGenomePanelItem();

		//Window.alert("login succesful : session_id is "+GeneralControler.SESSION_ID);
		final long DURATION = 1000 * 60 * 60 * 24 * 14; //duration remembering login. 2 weeks in this example.
		Date expires = new Date(System.currentTimeMillis() + DURATION);
		Cookies.setCookie("sid", Insyght.APP_CONTROLER.getCurrentUser()
				.getSessionId(), expires, null, "/", false);
		
		if (reloadCorrectUIUndelayed) {
			
			refreshUIOnLoginLogout(true, "refresh", "refresh", "refresh");
			
		} else {

			Scheduler.get().scheduleDeferred(new ScheduledCommand() {
				//@Override
				public void execute() {
					refreshUIOnLoginLogout(true, "refresh", "refresh", "refresh");
				}
			});

		}

	}


	public static void logout(boolean reloadCorrectUI) {

		//Window.alert("loging out... reload = " + reloadCorrectUI);

		//logout
		Insyght.APP_CONTROLER.setCurrentUser(new PersonsObj());
		CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED = 0;
		Insyght.APP_CONTROLER.resetCurrentListPrivateResultGenomePanelItem();
		Cookies.removeCookie("sid", "/");
		if (reloadCorrectUI) {
			refreshUIOnLoginLogout(true, "hide", "hide", "hide");		
		}
		
	}
	

	private static void refreshUIOnLoginLogout(boolean loginDeck, String adminTab, String resultTab, String searchTab) {
		
		if(loginDeck){
			HeaderViewImpl.showCorrectLoginDeck();
		}
		
		if(adminTab.compareTo("hide") == 0){
			AdminTab.hideAdminTab();
		}else if(adminTab.compareTo("refresh") == 0){
			AdminTab.refreshAdminTab();
		}
		
		if(resultTab.compareTo("hide") == 0){
			GenoOrgaAndHomoTablViewImpl.hideUserSpecifics();
			if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem() == null){
				MainTabPanelView.tabPanel.selectTab(1);
				Insyght.APP_CONTROLER.clearAppControlerFromAllResultData();
			}else if(!Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().isPublic()){
				MainTabPanelView.tabPanel.selectTab(1);
				Insyght.APP_CONTROLER.clearAppControlerFromAllResultData();
			}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult() != null
					&& (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0
					|| Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0)){
				CenterSLPAllGenomePanels.START_INDEX = 0;
				GenoOrgaAndHomoTablViewImpl.SP_VP_QUICKNAV.scrollToTop();
				CenterSLPAllGenomePanels.updateResultsDisplay(false, true, false);
			}
		}else if(resultTab.compareTo("refresh") == 0){
			GenoOrgaAndHomoTablViewImpl.refreshUserSpecifics();
			if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult() != null
					&& (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0
					|| Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0)){
				CenterSLPAllGenomePanels.START_INDEX = 0;
				GenoOrgaAndHomoTablViewImpl.SP_VP_QUICKNAV.scrollToTop();
				CenterSLPAllGenomePanels.updateResultsDisplay(false, true, false);
			}
		}
		
		if(searchTab.compareTo("hide") == 0){
			SearchViewImpl.hideUserSpecifics();
		}else if(searchTab.compareTo("refresh") == 0){
			SearchViewImpl.refreshUserSpecifics();
		}
	}
}
