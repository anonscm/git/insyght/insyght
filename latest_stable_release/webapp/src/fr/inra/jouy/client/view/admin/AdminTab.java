package fr.inra.jouy.client.view.admin;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.shared.pojos.users.GroupUsersObj;
import fr.inra.jouy.shared.pojos.users.PersonsObj;

public class AdminTab extends Composite {

	private static AdminTabUiBinder uiBinder = GWT
			.create(AdminTabUiBinder.class);

	interface AdminTabUiBinder extends UiBinder<Widget, AdminTab> {
	}

	@UiField
	static HTMLPanel adminTabHTMLNoLogin;
	@UiField
	static HTMLPanel adminTabHTMLAccountAdmin;
	@UiField
	static HTML adminTabHTMLProjectAdmin_login;
	@UiField
	static HTML adminTabHTMLProjectAdmin_name;
	@UiField
	static HTML adminTabHTMLProjectAdmin_firstName;
	@UiField
	static HTML adminTabHTMLProjectAdmin_affiliation;
	@UiField
	static Button adminTabHTMLProjectAdmin_updateUserAcctBT;
	@UiField
	static VerticalPanel VPListPrivateProjects;

	public AdminTab() {
		initWidget(uiBinder.createAndBindUi(this));
		refreshAdminTab();

	}
	

	public static void hideAdminTab() {
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			//@Override
			public void execute() {
				MainTabPanelView.getTabPanel().getTabWidget(5).setVisible(false);
			}
		}); 
		
		adminTabHTMLNoLogin.setVisible(true);
		adminTabHTMLAccountAdmin.setVisible(false);
		
	}

	public static void showAdminTab() {
		//setHTMLInAdminUserTabCorrectly();
		//setHTMLInAdminFamSoftTabCorrectly();
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			//@Override
			public void execute() {
				MainTabPanelView.getTabPanel().getTabWidget(5).setVisible(true);
			}
		}); 
		adminTabHTMLNoLogin.setVisible(false);
		adminTabHTMLAccountAdmin.setVisible(true);
		//Window.alert("show admin tab");
	}

	@UiHandler("adminTabHTMLProjectAdmin_updateUserAcctBT")
	void onAdminTabHTMLProjectAdmin_updateUserAcctBTClick(ClickEvent e) {
		//Window.alert("show update popup!");
		AdminUserAccountDialog dlg = new AdminUserAccountDialog();
		dlg.show();
		dlg.center();
	}


	public static void refreshAdminTab(){
		

		if (Insyght.APP_CONTROLER.getCurrentUser().getSessionId() == null) {
			hideAdminTab();
		} else {
			setHTMLInAdminUserTabCorrectly();
			showAdminTab();
		}

		
	}
	
	private static void setHTMLInAdminUserTabCorrectly() {

		PersonsObj currentUser = Insyght.APP_CONTROLER.getCurrentUser();
		
		//set HTML in admin tab correctly
		getAdminTabHTMLProjectAdmin_affiliation().setText(
				"Your Lab : "
						+ 
						currentUser.getLabo());
		getAdminTabHTMLProjectAdmin_login().setText(
				"Your login : "
						+ currentUser.getLogin());
		getAdminTabHTMLProjectAdmin_name().setText(
				"Your last name : "
						+ currentUser.getNom());
		getAdminTabHTMLProjectAdmin_firstName().setText(
				"Your first name : "
						+ currentUser.getPrenom());

		VPListPrivateProjects.clear();
		for(int i =0;i<currentUser.getListGroupsSubscribed().size();i++){
			GroupUsersObj guo = currentUser.getListGroupsSubscribed().get(i);
			VPListPrivateProjects.add(new HTML("<ul><li>Group  "+guo.getLibelle()+" : private access to species : "+
			//guo.getSpecies()+" (Strain :<i>"+guo.getStrain()+"</i>)" +
			guo.getFullName() +
			"</li></ul>"));			
		}

		
	}

	public static HTML getAdminTabHTMLProjectAdmin_login() {
		return adminTabHTMLProjectAdmin_login;
	}

	public static void setAdminTabHTMLProjectAdmin_login(
			HTML adminTabHTMLProjectAdmin_login) {
		AdminTab.adminTabHTMLProjectAdmin_login = adminTabHTMLProjectAdmin_login;
	}

	public static HTML getAdminTabHTMLProjectAdmin_name() {
		return adminTabHTMLProjectAdmin_name;
	}

	public static void setAdminTabHTMLProjectAdmin_name(
			HTML adminTabHTMLProjectAdmin_name) {
		AdminTab.adminTabHTMLProjectAdmin_name = adminTabHTMLProjectAdmin_name;
	}

	public static HTML getAdminTabHTMLProjectAdmin_firstName() {
		return adminTabHTMLProjectAdmin_firstName;
	}

	public static void setAdminTabHTMLProjectAdmin_firstName(
			HTML adminTabHTMLProjectAdmin_firstName) {
		AdminTab.adminTabHTMLProjectAdmin_firstName = adminTabHTMLProjectAdmin_firstName;
	}

	public static HTML getAdminTabHTMLProjectAdmin_affiliation() {
		return adminTabHTMLProjectAdmin_affiliation;
	}

	public static void setAdminTabHTMLProjectAdmin_affiliation(
			HTML adminTabHTMLProjectAdmin_affiliation) {
		AdminTab.adminTabHTMLProjectAdmin_affiliation = adminTabHTMLProjectAdmin_affiliation;
	}

	public static Button getAdminTabHTMLProjectAdmin_updateUserAcctBT() {
		return adminTabHTMLProjectAdmin_updateUserAcctBT;
	}

	public static void setAdminTabHTMLProjectAdmin_updateUserAcctBT(
			Button adminTabHTMLProjectAdmin_updateUserAcctBT) {
		AdminTab.adminTabHTMLProjectAdmin_updateUserAcctBT = adminTabHTMLProjectAdmin_updateUserAcctBT;
	}


}
