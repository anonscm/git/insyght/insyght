package fr.inra.jouy.client.view.crossSections.popUp;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
//import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.view.crossSections.components.AbsoPropQElemItemCell;
import fr.inra.jouy.client.view.crossSections.components.GeneCell;
import fr.inra.jouy.client.view.crossSections.components.RangeLabelPager;
import fr.inra.jouy.client.view.crossSections.components.ShowMorePagerPanel;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
import fr.inra.jouy.client.view.result.GenomePanel;
import fr.inra.jouy.client.view.search.GetResultDialog;
import fr.inra.jouy.client.view.search.SearchViewImpl;
import fr.inra.jouy.client.view.search.SearchViewImpl.EnumCoreDispParameterCriterion;
import fr.inra.jouy.shared.comparators.AbsoPropQElemItemComparators;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu;
import fr.inra.jouy.shared.pojos.applicationItems.SharedAppParams;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;

public class PopUpListGenesDialog extends PopupPanel {

	private static PopUpListGenesDialogUiBinder uiBinder = GWT
			.create(PopUpListGenesDialogUiBinder.class);

	interface PopUpListGenesDialogUiBinder extends
			UiBinder<Widget, PopUpListGenesDialog> {
	}
	
	@UiField
	public DockLayoutPanel rootPanelFindGene;
	//@UiField static DeckPanel dpFindOrNavigateButtonBottom;
	//@UiField static Button buttonViewHomologTable;
	//@UiField static Button buttonViewAnnotCompa;
	
	//@UiField static Button findGeneSelectedWindow;
	//@UiField static Button findGeneAllWindows;
	@UiField public static MenuBar menufindSelectedGene;
	@UiField static MenuItem findGene_SelectedWindow;//done
	@UiField static MenuItem findGene_AllWindows;//done

	
	@UiField
	public static HTML htmlTitle;
	@UiField
	Button cancelFindGene;
	//@UiField HTML HTMLAvailableMolecules;
	//@UiField VerticalPanel step3VPRBElement;
	@UiField public static DockLayoutPanel DLPAvailableAndSelectMolecules;
	@UiField public static ShowMorePagerPanel pagerPanel_moleculesFromSearchCDSInRef;
	private static CellList<AbsoPropQElemItem> cellList_moleculesFromSearchCDSInRef;
	private static ListDataProvider<AbsoPropQElemItem> dataProvider_moleculesFromSearchCDSInRef;
	private static final MultiSelectionModel<AbsoPropQElemItem> selectionModel_moleculesFromSearchCDSInRef = new MultiSelectionModel<>();
	private static boolean DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false;
	@UiField public static HTML HTMLMoleculesSelectedToBeSearch_PopUp;
	@UiField public static Anchor AnchorSelectAllMoleculesSelectedToBeSearch_PopUp;
	@UiField public static Anchor AnchorDeselectAllMoleculesSelectedToBeSearch_PopUp;
	
	@UiField static VerticalPanel vpSearchGeneBy;
	@UiField
	public static Button refreshListGenes;
	@UiField
	public static Button clearAllFilters;
	@UiField 
	static ShowMorePagerPanel pagerPanel;
	@UiField
	RangeLabelPager rangeLabelPager;
	//@UiField static Anchor exportGeneList;

	@UiField MenuBar mainMenuSortExport;//done
	@UiField MenuItem sortCDSsListByGenomicPosition;
	@UiField public static MenuItem sortCDSsListByCountOrthologsGroupPlus;
	@UiField public static MenuItem sortCDSsListByPValueOverRepresentationGroupPlus;
	
	@UiField MenuItem transfertGeneSetToCsvFile; //done
	@UiField MenuItem transfertGeneSetToOrthologsTable;//done
	@UiField MenuItem transfertGeneSetToAnnotationsComparator;//done
	
	@UiField public static HTML htmlBasicGeneInfoInSearch;
	@UiField public static HTML htmlDetailledGeneInfoInSearch;
	@UiField public static DeckPanel deckPFetchGeneList;
	@UiField static DockLayoutPanel centerPanelTShowOrHide;
	//@UiField public static VerticalPanel htmlPanelToHideIfNotFindGene;
	@UiField public static DeckPanel deckPSpecificGeneSelected;
	@UiField public static Button AddThisCDSToSelectionReferenceGeneSet;
	@UiField public static HTML notificationCDSAddedToSelectionReferenceGeneSet;
	
	//private static AbsoPropQElemItem selectedLightElementItem = null;
	public static ArrayList<LightGeneItem> alReferenceGenesList = new ArrayList<LightGeneItem>();
	private static LightGeneItem lgiSelected = null;
	
	private static CellList<LightGeneItem> cellListStep3;
	private static ListDataProvider<LightGeneItem> dataProviderStep3;
	
	private static String sourceDataType = "";
	private static boolean IS_RESIZING = false;
	private static boolean showSelectedGeneSetOnClose = false;
	
	public enum GeneListSortType {
		  GENOMIC_POSITION,
		  SCORE_PValOverReprPheno,
		  SCORE_CountMostRepresPheno
		}
	public static GeneListSortType geneListSortType = null;
	public static GeneListSortType savedSortCDSListBy = null;
	
	public PopUpListGenesDialog() {
		setWidget(uiBinder.createAndBindUi(this));
		setAnimationEnabled(true);
		setGlassEnabled(true);
		setAutoHideEnabled(true);
		
		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				if (isShowing() && ! IS_RESIZING) {
					IS_RESIZING = true;
					displayPoPup();
					IS_RESIZING = false;
				}
			}
        });
		

//		@UiHandler("cancelFindGene")
//		void onCancelFindGeneClick(ClickEvent e) {
//			hide();
//		}
		cancelFindGene.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				hide();
			}
		});

//		@UiHandler("refreshListGenes")
//		void onClickRefreshListGenes(ClickEvent e) {
//			loadDataWithFilterCriteria();
//		}
		refreshListGenes.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				loadDataWithFilterCriteria();
			}
		});
//		@UiHandler("clearAllFilters")
//		void onClickClearAllFilters(ClickEvent e) {
//			SearchViewImpl.doClearAllFilters(vpSearchGeneBy);
//			loadDataWithFilterCriteria();
//		}
		clearAllFilters.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				SearchViewImpl.doClearAllFilters(vpSearchGeneBy);
				loadDataWithFilterCriteria();
			}
		});
		
		
		
		transfertGeneSetToCsvFile.setScheduledCommand(new ScheduledCommand() {
			@Override
			public void execute() {
				onExportGeneListClick();
			}
		});
		transfertGeneSetToOrthologsTable.setScheduledCommand(new ScheduledCommand() {
			@Override
			public void execute() {
				onButtonViewHomologTableClick();
			}
		});
		transfertGeneSetToAnnotationsComparator.setScheduledCommand(new ScheduledCommand() {
			@Override
			public void execute() {
				onButtonViewAnnotCompaClick();
			}
		});
		
		sortCDSsListByGenomicPosition.setScheduledCommand(new ScheduledCommand() {
			@Override
			public void execute() {
				//onSortCDSsListByGenomicPositionClick();
				LightGeneItem.sortCDSsListByGenomicPosition(dataProviderStep3.getList());
				savedSortCDSListBy = GeneListSortType.GENOMIC_POSITION;
				refreshGeneList(dataProviderStep3.getList()
						//, false
						);
			}
		});
//		protected void onSortCDSsListByGenomicPositionClick() {
//			Comparator<LightGeneItem> compaByGeneStartComparatorIT = new LightGeneItem.ByStartGeneComparator();
//			Collections.sort(dataProviderStep3.getList(), compaByGeneStartComparatorIT);
//			PopUpListGenesDialog.refreshGeneList(dataProviderStep3.getList(), false);
//		}
		
		sortCDSsListByCountOrthologsGroupPlus.setScheduledCommand(new ScheduledCommand() {
			@Override
			public void execute() {
				ArrayList<LightGeneItem> sortedAlLGI = LightGeneItem.sortCDSsListByCountOrthologsGroupPlus(dataProviderStep3.getList());
				savedSortCDSListBy = GeneListSortType.SCORE_CountMostRepresPheno;
				refreshGeneList(sortedAlLGI
						//, false
						);
			}
		});
		
		sortCDSsListByPValueOverRepresentationGroupPlus.setScheduledCommand(new ScheduledCommand() {
			@Override
			public void execute() {
				ArrayList<LightGeneItem> sortedAlLGI = LightGeneItem.sortCDSsListByPValueOverRepresentationGroupPlus(dataProviderStep3.getList());
				savedSortCDSListBy = GeneListSortType.SCORE_PValOverReprPheno;
				refreshGeneList(sortedAlLGI
						//, false
						);
			}
		});
		
		
//		bExportListTo.addClickHandler(new ClickHandler() {
//			@Override
//			public void onClick(ClickEvent event) {
//				showMenuExportListTo();
//			}
//		});
//		exportGeneList.addStyleDependentName("cliquable");
//		buttonViewHomologTable.addStyleDependentName("colorHomologTable");
//		buttonViewAnnotCompa.addStyleDependentName("colorAnnotCompa");
//		buttonViewHomologTable.addStyleDependentName("bigBold");
//		buttonViewAnnotCompa.addStyleDependentName("bigBold");


		// the AbsoPropQElemItem list box
		cellList_moleculesFromSearchCDSInRef = new CellList<AbsoPropQElemItem>(new AbsoPropQElemItemCell());
		cellList_moleculesFromSearchCDSInRef.setPageSize(1000);
		cellList_moleculesFromSearchCDSInRef.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		selectionModel_moleculesFromSearchCDSInRef.addSelectionChangeHandler(new Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				if ( ! DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF) {
					if (selectionModel_moleculesFromSearchCDSInRef.getSelectedSet() != null && ! selectionModel_moleculesFromSearchCDSInRef.getSelectedSet().isEmpty()) {
						HTMLMoleculesSelectedToBeSearch_PopUp.setHTML(selectionModel_moleculesFromSearchCDSInRef.getSelectedSet().size()+" molecules selected to be searched (see panel below) out of "+dataProvider_moleculesFromSearchCDSInRef.getList().size()+" :");
						centerPanelTShowOrHide.setVisible(true);
						loadDataWithFilterCriteria();
					} else {
						//deckPVGOorBGSCenter.showWidget(0);
						if (sourceDataType == "SelectedElement") {
							centerPanelTShowOrHide.setVisible(false);
						}
						HTMLMoleculesSelectedToBeSearch_PopUp.setHTML("No molecules have been selected to be searched. Click on 1 or multiple molecules in the panel on the right.");
					}
				}
			}
		});
		cellList_moleculesFromSearchCDSInRef.setSelectionModel(selectionModel_moleculesFromSearchCDSInRef);
		// Add the CellList to the data provider in the database.
		dataProvider_moleculesFromSearchCDSInRef = new ListDataProvider<AbsoPropQElemItem>();
		dataProvider_moleculesFromSearchCDSInRef.addDataDisplay(cellList_moleculesFromSearchCDSInRef);
		pagerPanel_moleculesFromSearchCDSInRef.setDisplay(cellList_moleculesFromSearchCDSInRef);
		AnchorSelectAllMoleculesSelectedToBeSearch_PopUp.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selectAllMoleculesFromSearchCDSInRef();
			}
		});
		AnchorDeselectAllMoleculesSelectedToBeSearch_PopUp.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//selectAllMoleculesFromSearchCDSInRef(false);
				selectionModel_moleculesFromSearchCDSInRef.clear();
			}
		});
		
		// the gene list box
		GeneCell geneCell = new GeneCell();
		cellListStep3 = new CellList<LightGeneItem>(geneCell);
		cellListStep3.setPageSize(30);
		cellListStep3.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);

		// Add a selection model so we can select cells.

		// final SingleSelectionModel<LightGeneItem> selectionModel = new
		// SingleSelectionModel<LightGeneItem>();
		//final SingleSelectionModel<LightGeneItem> selectionModel = new SingleSelectionModel<>();
		final MultiSelectionModel<LightGeneItem> selectionModel = new MultiSelectionModel<>();
		cellListStep3.setSelectionModel(selectionModel);
		selectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						if ( ! selectionModel.getSelectedSet().isEmpty()) {
							if (selectionModel.getSelectedSet().size() == 1) {
								for (LightGeneItem lgiIT : selectionModel.getSelectedSet()) {
									selectGene(lgiIT);
									break;
								}
								AddThisCDSToSelectionReferenceGeneSet.setVisible(true);
								AddThisCDSToSelectionReferenceGeneSet.setHTML("<big>Add this CDS to your selection of reference gene set</big>");
								notificationCDSAddedToSelectionReferenceGeneSet.setHTML("");
								notificationCDSAddedToSelectionReferenceGeneSet.setStyleDependentName("fadeIn7Seconds", false);
								menufindSelectedGene.setVisible(true);
							} else {
								htmlBasicGeneInfoInSearch.setHTML("<big>"+selectionModel.getSelectedSet().size()+" CDSs selected</big>");
								htmlDetailledGeneInfoInSearch.setHTML("");
								AddThisCDSToSelectionReferenceGeneSet.setVisible(true);
								AddThisCDSToSelectionReferenceGeneSet.setHTML("<big>Add those CDSs to your selection of reference gene set</big>");
								notificationCDSAddedToSelectionReferenceGeneSet.setHTML("");
								notificationCDSAddedToSelectionReferenceGeneSet.setStyleDependentName("fadeIn7Seconds", false);
								menufindSelectedGene.setVisible(false);
							}
						} else {					
							htmlBasicGeneInfoInSearch.setHTML("");
							htmlDetailledGeneInfoInSearch.setHTML("");
							AddThisCDSToSelectionReferenceGeneSet.setVisible(false);
							notificationCDSAddedToSelectionReferenceGeneSet.setHTML("");
							notificationCDSAddedToSelectionReferenceGeneSet.setStyleDependentName("fadeIn7Seconds", false);
							menufindSelectedGene.setVisible(false);
						}
					}
				});
		
		
		// Set the cellList as the display of the pagers. This example has two
		// pagers. pagerPanel is a scrollable pager that extends the range when
		// the
		// user scrolls to the bottom. rangeLabelPager is a pager that displays
		// the
		// current range, but does not have any controls to change the range.
		pagerPanel.setDisplay(cellListStep3);
		rangeLabelPager.setDisplay(cellListStep3);
		
		//gene details
		findGene_SelectedWindow.setScheduledCommand(new ScheduledCommand() {
			@Override
			public void execute() {
				onFindGeneSelectedWindowClick();
			}
		});
		findGene_AllWindows.setScheduledCommand(new ScheduledCommand() {
			@Override
			public void execute() {
				onFindGeneAllWindowsClick();
			}
		});
		AddThisCDSToSelectionReferenceGeneSet.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				showSelectedGeneSetOnClose = true;
				int countCDSAddedToSelectedGeneSet = 0;
				int countCDSalreadyAddedPrevisouly = 0;
				int maxCapacityReached = 0;
				for (LightGeneItem lgiITSelected : selectionModel.getSelectedSet()) {
					boolean alreadyAdded = false;
					for (int i = 0; i < SearchViewImpl.alLGIFromBuildGeneSet.size(); i++) {
						LightGeneItem lgiAlreadyAdded = SearchViewImpl.alLGIFromBuildGeneSet.get(i);
						if (lgiAlreadyAdded.getGeneId() == lgiITSelected.getGeneId()) {
							// already added
							alreadyAdded = true;
							break;
							//selectionModel.setSelected(lgiAlreadyAdded, true);
						}
					}
					if ( ! alreadyAdded ) {
						if ( SearchViewImpl.alLGIFromBuildGeneSet.size() < SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED ) {
							SearchViewImpl.alLGIFromBuildGeneSet.add(lgiITSelected);
							countCDSAddedToSelectedGeneSet++;
						} else {
							maxCapacityReached++;
						}
					} else {
						countCDSalreadyAddedPrevisouly++;
					}
				}
				
				String textIT = "";
				if ( countCDSAddedToSelectedGeneSet >= 1 ) {
//					SearchViewImpl.sortListReferenceGeneSetByStart();
//					SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, SearchViewImpl.alLGIFromBuildGeneSet, true, -1);	
					if (countCDSAddedToSelectedGeneSet == 1) {
						textIT += "<span style=\"color:green;\">1 CDS has been added to your selection of reference gene set.</span>";
					} else if (countCDSAddedToSelectedGeneSet >= 1) {
						textIT += "<span style=\"color:green;\">" + countCDSAddedToSelectedGeneSet + " CDSs have been added to your selection of reference gene set.</span>";
					}
				}
				if ( countCDSalreadyAddedPrevisouly >= 1 ) {
					if ( ! textIT.isEmpty()) {
						textIT += " ";
					}
					if (countCDSalreadyAddedPrevisouly == 1) {
						textIT += "<span style=\"color:red;\">1 CDS was already added to your selection of reference gene set.</span>";
					} else if (countCDSalreadyAddedPrevisouly >= 1) {
						textIT += "<span style=\"color:red;\">" + countCDSalreadyAddedPrevisouly + " CDSs were already added to your selection of reference gene set.</span>";
					}
				}
				if ( maxCapacityReached >= 1 ) {
					if ( ! textIT.isEmpty()) {
						textIT += " ";
					}
					if ( maxCapacityReached == 1) {
						textIT += "<span style=\"color:red;\">1 CDS couldn't be added to your selection of reference gene set because your selection cart reached its maximum capacity.</span>";
					} else if ( maxCapacityReached >= 1) {
						textIT += "<span style=\"color:red;\">" + maxCapacityReached + " CDSs couldn't be added to your selection of reference gene set because your selection cart reached its maximum capacity.</span>";
					}
				}
				if ( countCDSAddedToSelectedGeneSet >= 1 || countCDSalreadyAddedPrevisouly >= 1 || maxCapacityReached >= 1 ) {
//					SearchViewImpl.sortListReferenceGeneSetByStart();
//					SearchViewImpl.refreshDisplayListSlectedLightGeneItemFromSlectedElementStep3(-1);
//					String textIT = "";
//					if (countCDSAddedToSelectedGeneSet == 1) {
//						textIT += "<span style=\"color:green;\">1 CDS has been added to your selection of reference gene set.</span>";
//					} else if (countCDSAddedToSelectedGeneSet >= 1) {
//						textIT += "<span style=\"color:green;\">" + countCDSAddedToSelectedGeneSet + " CDSs have been added to your selection of reference gene set.</span>";
//					}
//					if ( ! textIT.isEmpty()) {
//						textIT += " ";
//					}
//					if (countCDSalreadyAddedPrevisouly == 1) {
//						textIT += "<span style=\"color:red;\">1 CDS was already added to your selection of reference gene set.</span>";
//					} else if (countCDSalreadyAddedPrevisouly >= 1) {
//						textIT += "<span style=\"color:red;\">" + countCDSAddedToSelectedGeneSet + " CDSs were already added to your selection of reference gene set.</span>";
//					}
					textIT += " Close this window and navigate to the menu Build your reference gene set to see it.";
					
					notificationCDSAddedToSelectionReferenceGeneSet.setHTML(
							textIT
								);
					//notificationCDSAddedToSelectionReferenceGeneSet.setVisible(true);
					notificationCDSAddedToSelectionReferenceGeneSet.setStyleDependentName("fadeIn7Seconds", true);//fadeIn7Seconds fadeInRed4Seconds
					
				}
						
			}
		});
		
	}
	
//	@Override
//	protected void onDetach() {
//		super.onDetach();
//	}
	@Override
	protected void onUnload() {
		if(showSelectedGeneSetOnClose) {
			SearchViewImpl.rbBuildGeneSet.setValue(true, true);
			showSelectedGeneSetOnClose = false;
		}
	}
	
	public void displayPoPup(){
		if (SearchViewImpl.coreDispParameter_criterion.compareTo(EnumCoreDispParameterCriterion.mostOrthologsGroupPlus) == 0) {
			sortCDSsListByCountOrthologsGroupPlus.setEnabled(true);
			sortCDSsListByCountOrthologsGroupPlus.setStyleDependentName("disabledMenuItem", false);
			sortCDSsListByPValueOverRepresentationGroupPlus.setEnabled(false);
			sortCDSsListByPValueOverRepresentationGroupPlus.setStyleDependentName("disabledMenuItem", true);
		} else if (SearchViewImpl.coreDispParameter_criterion.compareTo(EnumCoreDispParameterCriterion.overrepresented) == 0) {
			sortCDSsListByCountOrthologsGroupPlus.setEnabled(false);
			sortCDSsListByCountOrthologsGroupPlus.setStyleDependentName("disabledMenuItem", true);
			sortCDSsListByPValueOverRepresentationGroupPlus.setEnabled(true);
			sortCDSsListByPValueOverRepresentationGroupPlus.setStyleDependentName("disabledMenuItem", false);
		} else if (SearchViewImpl.coreDispParameter_criterion.compareTo(EnumCoreDispParameterCriterion.presenceAbsenceOrthologs) == 0) {
			sortCDSsListByCountOrthologsGroupPlus.setEnabled(false);
			sortCDSsListByCountOrthologsGroupPlus.setStyleDependentName("disabledMenuItem", true);
			sortCDSsListByPValueOverRepresentationGroupPlus.setEnabled(false);
			sortCDSsListByPValueOverRepresentationGroupPlus.setStyleDependentName("disabledMenuItem", true);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
					new Exception("ERROR in displayPoPup : "
							+ "SearchViewImpl.coreDispParameter_criterion not recognized : "
							+ SearchViewImpl.coreDispParameter_criterion.toString()
						));
		}
		int clientWidth = Window.getClientWidth();
		int clientHeight = Window.getClientHeight();
		rootPanelFindGene.setHeight(((3 * clientHeight) / 4) + "px");
		rootPanelFindGene.setWidth(((3 * clientWidth) / 4) + "px");
		center();
	}
	
	protected static void selectGene(LightGeneItem lgiSelectedSent) {
		
		if(lgiSelectedSent == null){
			lgiSelected = null;
			htmlBasicGeneInfoInSearch.setHTML("");
			htmlDetailledGeneInfoInSearch.setHTML("");
			AddThisCDSToSelectionReferenceGeneSet.setVisible(false);
			notificationCDSAddedToSelectionReferenceGeneSet.setHTML("");
			notificationCDSAddedToSelectionReferenceGeneSet.setStyleDependentName("fadeIn7Seconds", false);
			menufindSelectedGene.setVisible(false);
		}else{

			lgiSelected = lgiSelectedSent;
			
			if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult() != null){
				menufindSelectedGene.setVisible(true);
				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght() != null){
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
						findGene_SelectedWindow.setEnabled(false);
						findGene_SelectedWindow.setStyleDependentName("disabledMenuItem", true);
					}else{
						findGene_SelectedWindow.setEnabled(true);
						findGene_SelectedWindow.setStyleDependentName("disabledMenuItem", false);
					}
					//findGene_AllWindows.setEnabled(true);
					//findGene_AllWindows.setStyleDependentName("disabledMenuItem", false);
				}
			}
			
			// show basic gene info
			String textBasicGeneInfo = "<ul><big>";
	        textBasicGeneInfo += "<li><i>Name : </i>"
							+ lgiSelectedSent.getMostSignificantGeneNameAsStrippedHTMLPlusLink()+"</li>";
	        textBasicGeneInfo += "<li><i>Locus tag : </i>"
							+ lgiSelectedSent.getLocusTagAsHTMLPlusLink()+"</li>";
			textBasicGeneInfo += "<li><i>Start : </i>"
					+ NumberFormat.getFormat("###,###,###,###,###,###.##").format(lgiSelectedSent.getStart())+"</li>";
			textBasicGeneInfo += "<li><i>Stop : </i>"
					+ NumberFormat.getFormat("###,###,###,###,###,###.##").format(lgiSelectedSent.getStop())+"</li>";
			if (lgiSelectedSent.getStrand() == 1) {
				textBasicGeneInfo += "<li><i>Strand : </i>+"+"</li>";
			} else if (lgiSelectedSent.getStrand() == -1) {
				textBasicGeneInfo += "<li><i>Strand : </i>-"+"</li>";
			}
			textBasicGeneInfo += "<li><i>Element : </i>"
					+ lgiSelectedSent.getType()+"</li>";
			htmlBasicGeneInfoInSearch
					.setHTML(textBasicGeneInfo);
			
			@SuppressWarnings("unused")
			GetResultDialog lDiag = new GetResultDialog(lgiSelectedSent.getGeneId(), false, true);
		}
		
	}

	
	void onExportGeneListClick() {
		ArrayList<Integer> alToSent = new ArrayList<Integer>();
		for(LightGeneItem lgiIT : dataProviderStep3.getList()){
			alToSent.add(lgiIT.getGeneId());
		}
		GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.displayPoPup(
				null, null, null, -1,
				null, null, null, -1,
				-1, -1,
				-1,-1,-1,
				-1,-1,-1,
				alToSent);
		//GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.center();
		
	}
	
	
	//@UiHandler("buttonViewHomologTable")
	void onButtonViewHomologTableClick(
			//ClickEvent e
			) {
		
		ArrayList<LightGeneItem> tmpAl = null;
		if(Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet() != null){
			tmpAl = new ArrayList<LightGeneItem>();
			if(!Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().isEmpty()){
				tmpAl.addAll(Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet());
			}
			//Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(new ArrayList<LightGeneItem>());
			//Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().addAll((dataProviderStep3.getList()));
			SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, dataProviderStep3.getList(), false, -1);
		}

		
//		if( ! SearchViewImpl.refNodeTaxoBrowserSelectionModel.getSelectedSet().isEmpty()){
//			//core genome, feature orga
//			Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().clear();
//			ArrayList<OrganismItem> alOiUnderSelectedNode = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(SearchViewImpl.selectedRefTaxoNode.getFullPathInHierarchie()); 
//			for(OrganismItem oiIT : alOiUnderSelectedNode){
//				if(oiIT.getOrganismId() != Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getOrganismId()){
//					Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().add(oiIT);
//				}
//			}
//		}
		
		hide();

		SearchViewImpl.submitSearch(RefGenoPanelAndListOrgaResu.EnumResultViewTypes.homolog_table);
		//retab previous search parameters
		//Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(tmpAl);
		SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, tmpAl, false, -1);
		
	}

	//@UiHandler("buttonViewAnnotCompa")
	void onButtonViewAnnotCompaClick(
			//ClickEvent e
			) {
		
		ArrayList<LightGeneItem> tmpAl = null;
		if(Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet() != null){
			tmpAl = new ArrayList<LightGeneItem>();
			if(!Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().isEmpty()){
				tmpAl.addAll(Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet());
			}
			//Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(new ArrayList<LightGeneItem>());
			//Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().addAll((dataProviderStep3.getList()));
			SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, dataProviderStep3.getList(), false, -1);
			
		}
//		if( ! SearchViewImpl.refNodeTaxoBrowserSelectionModel.getSelectedSet().isEmpty()){
//			//core genome, feature orga
//			Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().clear();
//			ArrayList<OrganismItem> alOiUnderSelectedNode = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(SearchViewImpl.selectedRefTaxoNode.getFullPathInHierarchie()); 
//			for(OrganismItem oiIT : alOiUnderSelectedNode){
//				if(oiIT.getOrganismId() != Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getOrganismId()){
//					Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().add(oiIT);
//				}
//			}
//		}
		hide();

		SearchViewImpl.submitSearch(RefGenoPanelAndListOrgaResu.EnumResultViewTypes.annotations_comparator);
		//retab previous search parameters
		//Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(tmpAl);
		SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, tmpAl, false, -1);
		
	}
	
	
	//@UiHandler("findGeneSelectedWindow")
	void onFindGeneSelectedWindowClick(
			//ClickEvent e
			) {
		
		Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1,
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
		//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().setLookUpForSymbolicSliceAtQPosition(lgiSelected.getStart());
		//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().setLookUpForSymbolicSliceAtQElementId(lgiSelected.getElementId());
		Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().setLookUpForGeneWithQStart(lgiSelected.getStart(),
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
		Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().setLookUpForGeneWithQStop(lgiSelected.getStop(),
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
		Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().setLookUpForGeneOnQElementId(lgiSelected.getElementId(),
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
		Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawViewToCanvas(1.0, 0, true, true, false, false);

		hide();
		
	}

	//@UiHandler("findGeneAllWindows")
	void onFindGeneAllWindowsClick(
			//ClickEvent e
			) {
		
		//System.out.println("getStart="+lgiSelected.getStart()+" ; lgiSelected.getElementId()="+lgiSelected.getElementId());
		
		centerAllResultPanelsOnThisGene(lgiSelected.getElementId(), lgiSelected.getStart(), lgiSelected.getStop());
		
		hide();
	}

	
	public static void centerAllResultPanelsOnThisGene(int elementIdSent, int startSent,
			int stopSent) {
		
		if(Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem() != null){
			for(int i=0;i<Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem().size();i++){
				GenomePanelItem gPIIT = Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem().get(i);
				gPIIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				//gPIIT.setLookUpForSymbolicSliceAtQPosition(lgiSelected.getStart());
				//gPIIT.setLookUpForSymbolicSliceAtQElementId(lgiSelected.getElementId());
				gPIIT.setLookUpForGeneWithQStart(startSent,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gPIIT.setLookUpForGeneWithQStop(stopSent,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gPIIT.setLookUpForGeneOnQElementId(elementIdSent,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}
		}
		if(Insyght.APP_CONTROLER.getCurrentListPrivateResultGenomePanelItem() != null){
			for(int i=0;i<Insyght.APP_CONTROLER.getCurrentListPrivateResultGenomePanelItem().size();i++){
				GenomePanelItem gPIIT = Insyght.APP_CONTROLER.getCurrentListPrivateResultGenomePanelItem().get(i);
				gPIIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				//gPIIT.setLookUpForSymbolicSliceAtQPosition(lgiSelected.getStart());
				//gPIIT.setLookUpForSymbolicSliceAtQElementId(lgiSelected.getElementId());
				gPIIT.setLookUpForGeneWithQStart(startSent,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gPIIT.setLookUpForGeneWithQStop(stopSent,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gPIIT.setLookUpForGeneOnQElementId(elementIdSent,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}
		}
			
		if(Insyght.APP_CONTROLER.getCurrentListFeaturedResultGenomePanelItem() != null){
			for(int i=0;i<Insyght.APP_CONTROLER.getCurrentListFeaturedResultGenomePanelItem().size();i++){
				GenomePanelItem gPIIT = Insyght.APP_CONTROLER.getCurrentListFeaturedResultGenomePanelItem().get(i);
				gPIIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				//gPIIT.setLookUpForSymbolicSliceAtQPosition(lgiSelected.getStart());
				//gPIIT.setLookUpForSymbolicSliceAtQElementId(lgiSelected.getElementId());
				gPIIT.setLookUpForGeneWithQStart(startSent,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gPIIT.setLookUpForGeneWithQStop(stopSent,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gPIIT.setLookUpForGeneOnQElementId(elementIdSent,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}
		}
		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1,
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
		//Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setLookUpForSymbolicSliceAtQPosition(lgiSelected.getStart());
		//Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setLookUpForSymbolicSliceAtQElementId(lgiSelected.getElementId());
		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setLookUpForGeneWithQStart(startSent,
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setLookUpForGeneWithQStop(stopSent,
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setLookUpForGeneOnQElementId(elementIdSent,
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
		
		for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
			GenomePanel gpToUpdate = i.next();
			if(gpToUpdate.isVisible()){
				gpToUpdate.syntenyCanvas.drawViewToCanvas(1.0, 0, true, true, false, false);
			}
		}
	}

	
	
	/*public static void selectLightElementItem(AbsoPropQElemItem leiSelected) {
		
		selectGene(null);
		
		if (leiSelected != null) {
			selectedLightElementItem = leiSelected;
			loadDataWithFilterCriteria();
			centerPanelTShowOrHide.setVisible(true);
			
		}else{
			centerPanelTShowOrHide.setVisible(false);
			selectedLightElementItem = null;
		}
	}*/
	
	public void initListGenesWithAlLgi(ArrayList<LightGeneItem> alLgiSent, GeneListSortType typeOfSorting){
		geneListSortType = typeOfSorting;
		alReferenceGenesList = alLgiSent;
		//selectedLightElementItem = null;
		refreshMoleculesFromSearchCDSInRef(null);
		refreshGeneList(alLgiSent
				//, false
				);
		sourceDataType = "GeneListSent";
		setStep3ToDefault(true);
		showCorrectWidgetsOnInit(true, false);
	}
	

	public void showCorrectWidgetsOnRefresh(){

		deckPFetchGeneList.showWidget(1);
		centerPanelTShowOrHide.setVisible(true);
		htmlBasicGeneInfoInSearch.setText("");
		htmlDetailledGeneInfoInSearch.setText("");
		AddThisCDSToSelectionReferenceGeneSet.setVisible(false);
		notificationCDSAddedToSelectionReferenceGeneSet.setHTML("");
		notificationCDSAddedToSelectionReferenceGeneSet.setStyleDependentName("fadeIn7Seconds", false);
		menufindSelectedGene.setVisible(false);
		
	}
	
	
	public void showCorrectWidgetsOnInit(boolean navigateMode, boolean showListElement) {
		
		//clear radiobutton element
		if(sourceDataType.compareTo("GeneListSent")==0){
			//step3VPRBElement.clear();
			refreshMoleculesFromSearchCDSInRef(null);
		}
		//findGeneSelectedWindow.setVisible(!hideFindButtons);
		//findGeneAllWindows.setVisible(!hideFindButtons);
		//textFindGeneWindow.setVisible(!hideFindButtons);
		if(!navigateMode){
			htmlTitle.setText("Find gene");
			//dpFindOrNavigateButtonBottom.showWidget(0);
			deckPSpecificGeneSelected.showWidget(0);
			//htmlPanelToHideIfNotFindGene.setVisible(true);
			mainMenuSortExport.setVisible(false);
		}else{
			htmlTitle.setText("Navigate the list of "+dataProviderStep3.getList().size()+" CDS");
			//dpFindOrNavigateButtonBottom.showWidget(1);
			deckPSpecificGeneSelected.showWidget(1);
			//htmlPanelToHideIfNotFindGene.setVisible(false);
			mainMenuSortExport.setVisible(true);
		}
		DLPAvailableAndSelectMolecules.setVisible(showListElement);
		
		showCorrectWidgetsOnRefresh();
		
	}

	public void initFindGeneInListElementIds(final ArrayList<AbsoPropQElemItem> alQElementItem){
		sourceDataType = "SelectedElement";
		alReferenceGenesList.clear();
		
		setStep3ToDefault(false);
		
		//HTMLAvailableMolecules.setHTML("<big>Select among "+alQElementItem.size()+" available molecules :</big>");
		
		/*step3VPRBElement.clear();
		for (int i = 0; i < alQElementItem.size(); i++) {
			final AbsoPropQElemItem ei = alQElementItem.get(i);
			RadioButton rb = new RadioButton("groupElement", ei.getqElementType()
					 + " "
					 + ei.getqAccnum() + " ("+ei.getQsizeOfElementinPb()+" bp)");
			rb.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					selectLightElementItem(ei);
				}
			});
			step3VPRBElement.add(rb);
			if (i == 0) {
				rb.setValue(true);
				selectLightElementItem(ei);
			}
		}*/
		refreshMoleculesFromSearchCDSInRef(alQElementItem);
		setDefaultselectedMoleculeForSearchCDSInRef();
		showCorrectWidgetsOnInit(false, true);
		
	}
	
	
	public static void refreshMoleculesFromSearchCDSInRef(ArrayList<AbsoPropQElemItem> alAPQESent) {
		pagerPanel_moleculesFromSearchCDSInRef.getDisplay().setVisibleRange(0, 1000);
		if ( alAPQESent == null ) {
			dataProvider_moleculesFromSearchCDSInRef.setList( new ArrayList<AbsoPropQElemItem>() );
		} else {
			ArrayList<AbsoPropQElemItem> alLEIIT = new ArrayList<>(alAPQESent);
			Collections.sort(alLEIIT, AbsoPropQElemItemComparators.byQAccessionComparator); 
			dataProvider_moleculesFromSearchCDSInRef.setList(alLEIIT);
		}
		selectionModel_moleculesFromSearchCDSInRef.clear();
	}
	
	protected static void setDefaultselectedMoleculeForSearchCDSInRef() {
		if ( dataProvider_moleculesFromSearchCDSInRef.getList() == null || dataProvider_moleculesFromSearchCDSInRef.getList().isEmpty() ){
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
					new Exception("ERROR in setDefaultselectedMoleculeForSearchCDSInRef : "
							+ " dataProvider_moleculesFromSearchCDSInRef.getList() is null or empty : " 
							+ ( (dataProvider_moleculesFromSearchCDSInRef.getList() != null ) ? dataProvider_moleculesFromSearchCDSInRef.getList().toString() : "NULL" )
							+ " ; getReferenceOrganism = " + Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism()
							)
					);
		} else {
			if ( selectionModel_moleculesFromSearchCDSInRef.getSelectedSet().isEmpty() ) {
				selectionModel_moleculesFromSearchCDSInRef.setSelected(dataProvider_moleculesFromSearchCDSInRef.getList().get(0), true);
			}
		}
	}
	
	public static void selectAllMoleculesFromSearchCDSInRef (
			//boolean select
			) {
		/*Iterator<AbsoPropQElemItem> it0 = null;
		if (select) {
			it0 = dataProvider_moleculesFromSearchCDSInRef.getList().iterator();
		} else {
			it0 = selectionModel_moleculesFromSearchCDSInRef.getSelectedSet().iterator();
		}
		DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = true;
	    while(it0.hasNext()){
	    	AbsoPropQElemItem leiIT = it0.next();
	    	if ( ! it0.hasNext()) {
	    	    // last iteration
	    		DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false;
	    	}
	    	selectionModel_moleculesFromSearchCDSInRef.setSelected(leiIT, select);
	    }
	    DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false; //safety if iterator empty*/
		Iterator<AbsoPropQElemItem> it0 = dataProvider_moleculesFromSearchCDSInRef.getList().iterator();
		DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = true;
	    while(it0.hasNext()){
	    	AbsoPropQElemItem leiIT = it0.next();
	    	if ( ! it0.hasNext()) {
	    	    // last iteration
	    		DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false;
	    	}
	    	selectionModel_moleculesFromSearchCDSInRef.setSelected(leiIT, true);
	    }
	    DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false; //safety if iterator empty
	}
	
	private static void setStep3ToDefault(boolean defaultZeroAndMaxForGenomicLocFilter) {
		//selectLightElementItem(null);
		
		vpSearchGeneBy.clear();
		SearchViewImpl.addARootFilter(true, vpSearchGeneBy, defaultZeroAndMaxForGenomicLocFilter);
		Button addAnotherRootFilter = new Button("New filter AND/OR...");
		addAnotherRootFilter.setTitle("Clicking this bouton adds a filter."
				+ " You can combine multiple filters to build a biologically relevant query according to multiple criteria."
				+ " For example, you could list the genes that are niche specific / core genome and related to a given molecular function or biological process."
				+ " The operators AND (intersection) and OR (union) are supported."
				+ " You can combine an unlimited number of filters.");
		addAnotherRootFilter.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				SearchViewImpl.addARootFilter(false, vpSearchGeneBy, false);
			}
		});
		vpSearchGeneBy.add(addAnotherRootFilter);
	}

	

	public static void loadDataWithFilterCriteria() {
		
		
		int maxSizeAmongSelectedElement = -1;
		if(sourceDataType.compareTo("SelectedElement")==0){
//			if (selectedLightElementItem != null) {
//				if (selectedLightElementItem.getqOrigamiElementId() > 0) {
//					sizeSelectedElement = selectedLightElementItem.getQsizeOfElementinPb();
//				}
//			}
//			if(sizeSelectedElement <= 0){
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in loadDataStep3 : size of selected element <= 0"));
//				return;
//			}
			
			if (selectionModel_moleculesFromSearchCDSInRef.getSelectedSet() == null || selectionModel_moleculesFromSearchCDSInRef.getSelectedSet().isEmpty() ) {
				return;
			}
			
		    Iterator<AbsoPropQElemItem> it0 = selectionModel_moleculesFromSearchCDSInRef.getSelectedSet().iterator();
		    while(it0.hasNext()){
		    	int sizeIt = it0.next().getQsizeOfElementinPb();
				if (sizeIt > 0) {
					if (sizeIt > maxSizeAmongSelectedElement) {
						maxSizeAmongSelectedElement = sizeIt;
					}
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in loadDataWithFilterCriteria :" +
					" size of element "+it0.next().getqAccnum()+" id = "+it0.next().getqOrigamiElementId()+" <= 0"));
					return;
				}
		    }
		}
		

		ArrayList<FilterGeneItem> listFilters = new ArrayList<FilterGeneItem>();
		int parseStatus = SearchViewImpl.parseVPFiltersN(vpSearchGeneBy, listFilters, maxSizeAmongSelectedElement);
		if( parseStatus == 1 && !listFilters.isEmpty() ){

			refreshListGenes.setEnabled(false);
			deckPFetchGeneList.showWidget(0);
			
			if(sourceDataType.compareTo("SelectedElement")==0){
				//ArrayList<Integer> alToSend = new ArrayList<Integer>();
				//alToSend.add(selectedLightElementItem.getqOrigamiElementId());
				ArrayList<Integer> alEletIdToSent = new ArrayList<Integer>();
			    Iterator<AbsoPropQElemItem> it1 = selectionModel_moleculesFromSearchCDSInRef.getSelectedSet().iterator();
			    while(it1.hasNext()){
			    	alEletIdToSent.add(it1.next().getqOrigamiElementId());
			    }
				SearchViewImpl.sld.loadGeneListForSelectedFilters(
						listFilters
						//, null
						, alEletIdToSent
						, FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_SelectedElement
						, false
						);
				
			}else if(sourceDataType.compareTo("GeneListSent")==0){
				
				//alReferenceGenesList
				//
				HashSet<Integer> hs = new HashSet<Integer>();
				for(LightGeneItem lgiIT : alReferenceGenesList){
					hs.add(lgiIT.getElementId());
				}
//				hs.addAll(alToSend);
//				alToSend.clear();
//				alToSend.addAll(hs);
				ArrayList<Integer> alToSend = new ArrayList<Integer>(hs);
				SearchViewImpl.sld.loadGeneListForSelectedFilters(
						listFilters
						//, null
						, alToSend
						, FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_GeneListSent
						, true
						);
				
			}else{
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in loadDataWithFilterCriteria : unrecognized sourceDataType = "+sourceDataType));
			}
		} else if (parseStatus == 2) {
			SearchViewImpl.doClearAllFilters(vpSearchGeneBy);
			loadDataWithFilterCriteria();
		}

	}

	public static ArrayList<LightGeneItem> intersectWithRefGeneList(List<LightGeneItem> listOfLGISent){

		ArrayList<LightGeneItem> alIntersect = new ArrayList<LightGeneItem>();
		for(LightGeneItem lgiSentIT : listOfLGISent){
			for(LightGeneItem lgiRefIT : alReferenceGenesList){
				if(lgiSentIT.getGeneId() == lgiRefIT.getGeneId()){
					alIntersect.add(lgiSentIT);
					break;
				}
			}
		}
		return alIntersect;
	}
	
	public static void refreshGeneList(
			List<LightGeneItem> listOfLGISent
			//, final boolean intersectWithRefGeneList
			) {
		// Add the CellList to the data provider in the database.
		if(dataProviderStep3 == null){
			dataProviderStep3 = new ListDataProvider<LightGeneItem>(listOfLGISent);
			dataProviderStep3.addDataDisplay(cellListStep3);
		}else{
			dataProviderStep3.setList(listOfLGISent);
		}
		
		pagerPanel.getDisplay().setVisibleRange(0, 100);

	}


	
}
