package fr.inra.jouy.client.view.search;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DialogBox;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.SessionStorageControler;
import fr.inra.jouy.client.RPC.CallForInfoDB;
import fr.inra.jouy.client.RPC.CallForInfoDBAsync;
import fr.inra.jouy.client.RPC.CallForOrigAtMIGinfoDB;
import fr.inra.jouy.client.RPC.CallForOrigAtMIGinfoDBAsync;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.crossSections.popUp.PopUpListGenesDialog;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.comparators.LightOrganismItemComparators;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem;
import fr.inra.jouy.shared.pojos.applicationItems.SharedAppParams;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem.EnumOperatorFilterGeneBy;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem.EnumTypeFilterGeneBy;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.TaxoItem;
import fr.inra.jouy.shared.pojos.transitComposite.AlItemsOfIntPrimaryIdAttributePlusScore;

public class SearchLoadingDialog extends DialogBox {

	//private static SearchLoadingDialogUiBinder uiBinder = GWT.create(SearchLoadingDialogUiBinder.class);

	private final CallForInfoDBAsync infoDBService = (CallForInfoDBAsync) GWT
			.create(CallForInfoDB.class);

	private final CallForOrigAtMIGinfoDBAsync origamiAtMIGinfoDBService = (CallForOrigAtMIGinfoDBAsync) GWT
			.create(CallForOrigAtMIGinfoDB.class);

	// Set appropriate exemple taxon id
	int defaultRefOrganismExempleTaxoId = 224308;
	// For 2688 Bacteria : Bacillus subtilis strain 168, taxo_id = 224308
	// For Archaea : Acidilobus saccharovorans 345-15, taxo_id = 666510 [CP001742]
	// For private VM : -1
	int defaultComparedOrganismExempleTaxoId = 261594;
	// For 2688 Bacteria : Bacillus anthracis str. 'Ames Ancestor' strain Ames Ancestor; A2084, taxo_id = 261594 [AE017334]
	// For Archaea :  Caldisphaera lagunensis DSM 15908, taxo_id = 1056495
	// For private VM : -1
	
	
	//private boolean doneWithInitialLoadAllPublicOganisms = false;
	private boolean doneWithInitialLoadAllPublicMoleculesOfOrganisms = false;
	private boolean doneWithInitialLoadAllPublicLightOganisms = false;
	private boolean doneWithInitialLoadTaxoTree = false;
	private boolean alreadyDoneInitListAllPublicOrganismItem = false;
	ArrayList<LightOrganismItem> tmpInitListLOI = new ArrayList<>();
	ArrayList<LightElementItem> tmpInitListLEI = new ArrayList<>();
	
	
	public SearchLoadingDialog(){
	}
	
	
//	public SearchLoadingDialog(String type) {
//		setWidget(uiBinder.createAndBindUi(this));
//		if (type.compareTo("All_organisms") == 0) {
//			loadAllPublicOganisms();
//		} else {
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in SearchLoadingDialog " +
//					"All_organisms"));
//		}
//	}

	
	
//	public SearchLoadingDialog(ArrayList<FilterGeneItem> listFilters,
//			//int sizeSelectedElement,
//			ArrayList<Integer> alSelectedOrganismIds,
//			ArrayList<Integer> alSelectedElementIds,
//			final String updateType,
//			final boolean intersectWithRefGeneList
//			) {
//		//updateType:
//		//updateBuildGeneSetSearchTab
//		//updateCoreDispSearchTab
//		//updateListGenePopUp
//		
//		setWidget(uiBinder.createAndBindUi(this));
//
//		if (updateType.compareTo("updateBuildGeneSetSearchTab")==0) {
//			SearchViewImpl.refreshListStep3.setEnabled(false);
//			SearchViewImpl.deckPFetchGeneList.showWidget(0);
//			loadGeneListForSelectedFilters(listFilters,
//					alSelectedOrganismIds,
//					alSelectedElementIds,
//					updateType,
//					intersectWithRefGeneList);
//		}else if (updateType.compareTo("updateCoreDispSearchTab")==0) {
//			//SearchViewImpl.alLGIFromCoreDispRequest.clear();
//			SearchViewImpl.htmlStep3ResultingReferenceGenesSetInTaxoNodeContext.setHTML("<span style=\"color:green;\">Loading, please wait...</span>");
//			loadGeneListForSelectedFilters(listFilters, 
//					alSelectedOrganismIds,
//					alSelectedElementIds,
//					updateType,
//					intersectWithRefGeneList);
//		}else if (updateType.compareTo("updateListGenePopUp")==0) {
//			PopUpListGenesDialog.refreshListGenes.setEnabled(false);
//			PopUpListGenesDialog.deckPFetchGeneList.showWidget(0);
//			loadGeneListForSelectedFilters(listFilters, 
//					alSelectedOrganismIds,
//					alSelectedElementIds,
//					updateType,
//					intersectWithRefGeneList);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in SearchLoadingDialog :" +
//					" unrecognized updateType = "+updateType));
//			hide();
//		}
//		
//	}


	public void loadTaxoTree(
			//ArrayList<Integer> listTaxoIds
			) {

		AsyncCallback<TaxoItem> callback = new AsyncCallback<TaxoItem>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				// Window.alert("loadTaxoTree failed : "+ caught.getMessage());
				TaxoItem taxoTreeErrorRoot = new TaxoItem("rootAll", -1, null);
				TaxoItem taxoTreeError = new TaxoItem(
						"Taxonomic tree unavailable.",
						-1, taxoTreeErrorRoot);
				taxoTreeErrorRoot.getTaxoChildrens().add(taxoTreeError);
				Insyght.APP_CONTROLER.setTaxoTree(taxoTreeErrorRoot);
				doneWithInitialLoadTaxoTree = true;
				finishInitialLoadingOrgaAndTaxoTree();
//				SearchViewImpl.buildMainTaxoBrowser();
//				//MainTabPanelView.getTabPanel().getTabWidget(1).setVisible(true);
//				MainTabPanelView.getTabPanel().setTabText(1, "SEARCH");
//				MainTabPanelView.getTabPanel().getTabWidget(2).setVisible(true);
//				MainTabPanelView.getTabPanel().getTabWidget(3).setVisible(true);
//				MainTabPanelView.getTabPanel().getTabWidget(4).setVisible(true);
			}

			// @Override
			public void onSuccess(TaxoItem taxoItemSent) {
				// Window.alert("loadTaxoTree success!");
//				long milli = System.currentTimeMillis();
//				long milliPrint2 = -1;
//				GWT.log("onSuccess SearchLoadingDialog loadTaxoTree");
				
				// TaxoItem taxoItemSent
				// int countSelectable =
				sortTheTaxoTreeAndCalculateFullPathInHierarchie(taxoItemSent);
				Insyght.APP_CONTROLER.setTaxoTree(taxoItemSent);
				
				doneWithInitialLoadTaxoTree = true;
				finishInitialLoadingOrgaAndTaxoTree();

//				milliPrint2 = System.currentTimeMillis() - milli;
//				GWT.log("done onSuccess SearchLoadingDialog loadTaxoTree 1 took\t" + milliPrint2 + "\tmilliseconds");
				

			}
		};

		try {
			origamiAtMIGinfoDBService.getTaxoTreePublicOganisms(
					//listTaxoIds, 
					callback);
		} catch (Exception e) {
			//String mssgError = "ERROR loadTaxoTree : " + e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
	
		}

	}

	protected void finishInitialLoadingOrgaAndTaxoTree() {
		

//		long milli = System.currentTimeMillis();
//		long milliPrint2 = -1;
//		GWT.log("start finishInitialLoadingOrgaAndTaxoTree"
//				+ " ; doneWithInitialLoadAllPublicMoleculesOfOrganisms="+doneWithInitialLoadAllPublicMoleculesOfOrganisms
//				+ " ; doneWithInitialLoadAllPublicLightOganisms="+doneWithInitialLoadAllPublicLightOganisms
//				+ " ; doneWithInitialLoadTaxoTree="+doneWithInitialLoadTaxoTree);
		
		
		
		if (doneWithInitialLoadAllPublicLightOganisms && doneWithInitialLoadAllPublicMoleculesOfOrganisms && ! alreadyDoneInitListAllPublicOrganismItem ) {

			alreadyDoneInitListAllPublicOrganismItem = true;
			
			//construct HashMap hmOrgaId2OrganismItem
			HashMap<Integer, OrganismItem> hmOrgaId2OrganismItem = new HashMap<Integer, OrganismItem>();
			for(LightOrganismItem loiIT : tmpInitListLOI){
				hmOrgaId2OrganismItem.put(loiIT.getOrganismId(), new OrganismItem(loiIT));
			}

			for(LightElementItem leiIT : tmpInitListLEI){
				OrganismItem oiIT = hmOrgaId2OrganismItem.get(leiIT.getOrganismId());
				if(oiIT != null){
					oiIT.getListAllLightElementItem().add(leiIT);
				}
			}
			tmpInitListLOI.clear();
			tmpInitListLEI.clear();
			
			ArrayList<OrganismItem> alOI = new ArrayList<OrganismItem>(hmOrgaId2OrganismItem.values());
			Insyght.APP_CONTROLER.setListAllPublicOrganismItem(alOI);

			// SearchViewImpl.refreshAllGeneListStep2();
			SearchViewImpl.oracle.clear();
			//ArrayList<String> aloiReturnedAsString = new ArrayList<String>();
			for (int i = 0; i < Insyght.APP_CONTROLER.getListAllPublicOrganismItem().size(); i++) {
				OrganismItem oi = Insyght.APP_CONTROLER.getListAllPublicOrganismItem().get(i);
				String sIT = oi.getFullName();
				SearchViewImpl.oracle.add(sIT);
				//aloiReturnedAsString.add(sIT);
			}

			if(!Insyght.APP_CONTROLER.getListAllPublicOrganismItem().isEmpty()){
				Scheduler.get().scheduleDeferred(new ScheduledCommand() {
					// @Override
					public void execute() {
						makeQuickAccessExamples();
					}
				});
				
			}


//			milliPrint2 = System.currentTimeMillis() - milli;
//			GWT.log("doneWithInitialLoadAllPublicLightOganisms && doneWithInitialLoadAllPublicMoleculesOfOrganisms took\t" + milliPrint2 + "\tmilliseconds");

			
		}
		
		if (doneWithInitialLoadAllPublicLightOganisms && doneWithInitialLoadAllPublicMoleculesOfOrganisms && doneWithInitialLoadTaxoTree ) {

			if (Insyght.APP_CONTROLER.getListAllPublicOrganismItem().isEmpty()) {
				
				SearchViewImpl.deckPSearchRoot.showWidget(2);
				MainTabPanelView.tabPanel.setTabText(1, "NO DATA...");
				SearchViewImpl.noOrganismInDatabase = true;
				MainTabPanelView.tabPanel.selectTab(1);
				Scheduler.get().scheduleDeferred(
						new ScheduledCommand() {
							// @Override
							public void execute() {
								MainTabPanelView.tabPanel.remove(5);
								MainTabPanelView.tabPanel.remove(4);
								MainTabPanelView.tabPanel.remove(3);
								MainTabPanelView.tabPanel.remove(2);
							}
						}
						);
			} else {
				
				Insyght.APP_CONTROLER.mapOrganismsToTaxoTree();
				calculateSumOfLeafsInTaxoTreeRecursively(Insyght.APP_CONTROLER.getTaxoTree());
				SearchViewImpl.buildMainTaxoBrowser();
				ArrayList<Integer> fullPathInHierarchieInitIt = SearchViewImpl.getTheFurthestNode(
						//taxoItemSent
						Insyght.APP_CONTROLER.getTaxoTree()
						, false).getFullPathInHierarchie();
				if (fullPathInHierarchieInitIt.isEmpty()) {
					fullPathInHierarchieInitIt.add(0);
				}
				SearchViewImpl.selectATaxoItemInMainTaxoBrowser(fullPathInHierarchieInitIt, true);
				SearchViewImpl.sugBox.setText("");
				SearchViewImpl.sugBox_CoreDisp.setText("");
				MainTabPanelView.getTabPanel().setTabText(1, "SEARCH");
				MainTabPanelView.getTabPanel().getTabWidget(2).setVisible(true);
				MainTabPanelView.getTabPanel().getTabWidget(3).setVisible(true);
				MainTabPanelView.getTabPanel().getTabWidget(4).setVisible(true);
			}

//			milliPrint2 = System.currentTimeMillis() - milli;
//			GWT.log("doneWithInitialLoadAllPublicLightOganisms && doneWithInitialLoadAllPublicMoleculesOfOrganisms && doneWithInitialLoadTaxoTree took\t" + milliPrint2 + "\tmilliseconds");
			
		}
		
		
	}


	protected int calculateSumOfLeafsInTaxoTreeRecursively(TaxoItem taxoItemSent) {
		int countIsSelectable = 0;
		if (!taxoItemSent.getTaxoChildrens().isEmpty()) {
			for (int i = 0; i < taxoItemSent.getTaxoChildrens().size(); i++) {
				TaxoItem childTaxoItem = taxoItemSent.getTaxoChildrens().get(i);
				int countSelectableBubbledUp = calculateSumOfLeafsInTaxoTreeRecursively(childTaxoItem);
				countIsSelectable += countSelectableBubbledUp;
			}
			taxoItemSent.setSumOfLeafsUnderneath(countIsSelectable);
		}
		if (taxoItemSent.getAssociatedOrganismItem() != null) {
			countIsSelectable++;
		}
		return countIsSelectable;
	}

	protected void sortTheTaxoTreeAndCalculateFullPathInHierarchie(
			TaxoItem taxoItemSent) {

		if (!taxoItemSent.getTaxoChildrens().isEmpty()) {
			Comparator<TaxoItem> compaByNameTaxoItemComparatorIT = new ByNameTaxoItemComparator();
			Collections.sort(taxoItemSent.getTaxoChildrens(),
					compaByNameTaxoItemComparatorIT);

			//wrong header, depth not related to taxonomic levels
//			// add headers for
//			if (taxoItemSent.getFullPathInHierarchie().size() == 2) {
//				// depth = 3 => Domain
//				if (!taxoItemSent.getTaxoChildrens().isEmpty()) {
//					TaxoItem taxoHeaderIT = new TaxoItem(taxoItemSent);
//					taxoHeaderIT.setName("Domain");
//					taxoItemSent.getTaxoChildrens().add(0, taxoHeaderIT);
//				}
//			} else if (taxoItemSent.getFullPathInHierarchie().size() == 3) {
//				// depth = 4 => Phylum
//				if (!taxoItemSent.getTaxoChildrens().isEmpty()) {
//					TaxoItem taxoHeaderIT = new TaxoItem(taxoItemSent);
//					taxoHeaderIT.setName("Phylum");
//					taxoItemSent.getTaxoChildrens().add(0, taxoHeaderIT);
//				}
//			} else if (taxoItemSent.getFullPathInHierarchie().size() == 4) {
//				// depth = 5 => Class
//				if (!taxoItemSent.getTaxoChildrens().isEmpty()) {
//					TaxoItem taxoHeaderIT = new TaxoItem(taxoItemSent);
//					taxoHeaderIT.setName("Class");
//					taxoItemSent.getTaxoChildrens().add(0, taxoHeaderIT);
//				}
//			} else if (taxoItemSent.getFullPathInHierarchie().size() == 5) {
//				// depth = 6 => Order
//				if (!taxoItemSent.getTaxoChildrens().isEmpty()) {
//					TaxoItem taxoHeaderIT = new TaxoItem(taxoItemSent);
//					taxoHeaderIT.setName("Order");
//					taxoItemSent.getTaxoChildrens().add(0, taxoHeaderIT);
//				}
//			} else if (taxoItemSent.getFullPathInHierarchie().size() == 6) {
//				// depth = 7 => Family
//				if (!taxoItemSent.getTaxoChildrens().isEmpty()) {
//					TaxoItem taxoHeaderIT = new TaxoItem(taxoItemSent);
//					taxoHeaderIT.setName("Family");
//					taxoItemSent.getTaxoChildrens().add(0, taxoHeaderIT);
//				}
//			} else if (taxoItemSent.getFullPathInHierarchie().size() == 7) {
//				// depth = 8 => Genus
//				if (!taxoItemSent.getTaxoChildrens().isEmpty()) {
//					TaxoItem taxoHeaderIT = new TaxoItem(taxoItemSent);
//					taxoHeaderIT.setName("Genus");
//					taxoItemSent.getTaxoChildrens().add(0, taxoHeaderIT);
//				}
//			} else if (taxoItemSent.getFullPathInHierarchie().size() == 8) {
//				// depth = 9 => Species
//				if (!taxoItemSent.getTaxoChildrens().isEmpty()) {
//					TaxoItem taxoHeaderIT = new TaxoItem(taxoItemSent);
//					taxoHeaderIT.setName("Species");
//					taxoItemSent.getTaxoChildrens().add(0, taxoHeaderIT);
//				}
//			}

			for (int i = 0; i < taxoItemSent.getTaxoChildrens().size(); i++) {
				TaxoItem childTaxoItem = taxoItemSent.getTaxoChildrens().get(i);
				// set its index
				childTaxoItem.getFullPathInHierarchie().addAll(
						taxoItemSent.getFullPathInHierarchie());
				childTaxoItem.getFullPathInHierarchie().add(i);

				sortTheTaxoTreeAndCalculateFullPathInHierarchie(childTaxoItem);
			}
		}

	}

	
	public void loadAllPublicMoleculesOfOrganisms() {
		
		AsyncCallback<ArrayList<LightElementItem>> callback = new AsyncCallback<
				ArrayList<LightElementItem>
				>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			// @Override
			public void onSuccess(
					ArrayList<LightElementItem> listLEI
					) {
//				long milli = System.currentTimeMillis();
//				long milliPrint2 = -1;
//				GWT.log("onSuccess SearchLoadingDialog loadAllPublicMoleculesOfOrganisms ; listLEI.size() = " + listLEI.size());

				tmpInitListLEI = listLEI;
				doneWithInitialLoadAllPublicMoleculesOfOrganisms = true;
				finishInitialLoadingOrgaAndTaxoTree();

//				milliPrint2 = System.currentTimeMillis() - milli;
//				GWT.log("Done onSuccess SearchLoadingDialog loadAllPublicMoleculesOfOrganisms\t" + milliPrint2 + "\tmilliseconds");
			}
		};

		try {
			infoDBService.loadAllPublicMoleculesOfOrganisms(callback);
		} catch (Exception e) {
			//String mssgError = "ERROR loadAllPublicOganisms : "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}
	}
	

	public void loadAllPublicLightOganisms() {
		
		AsyncCallback<ArrayList<LightOrganismItem>> callback = new AsyncCallback<
				ArrayList<LightOrganismItem>
				//AlOrganismItemAndTaxoItem
				>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			// @Override
			public void onSuccess(
					ArrayList<LightOrganismItem> listLOI
					) {
				
//				long milli = System.currentTimeMillis();
//				long milliPrint2 = -1;
//				GWT.log("onSuccess SearchLoadingDialog loadAllPublicLightOganisms ; "+listLOI.size());

				tmpInitListLOI = listLOI;
			
				//deal with SearchViewImpl.comparedGenomes_alOrgaMainList
				SearchViewImpl.comparedGenomes_alOrgaMainList_filtredIn = new ArrayList<LightOrganismItem>(listLOI);
				Collections.sort(SearchViewImpl.comparedGenomes_alOrgaMainList_filtredIn, LightOrganismItemComparators.byFullNameComparator);
				SearchViewImpl.comparedGenomes_refreshListDataProviderMainListIfNedded();
				
				//SearchViewImpl.oracle.addAll(aloiReturnedAsString);
				SearchViewImpl.countAllNumOrgaAvail.setHTML("<span style=\"font-size: large;\">A total of <span style=\"font-weight: bold;\">"
						+ listLOI.size() + " public organisms</span> are available."
										+ " First select your reference genome or taxonomic group by using the browser below."
										+ " Then you can refine your search by using the menu on the left : reference CDSs, compared genomes, and vizualisation."
										+ " Click the submit button on the left when finished to fetch the results."
										+ "</span>");
				
				doneWithInitialLoadAllPublicLightOganisms = true;
				finishInitialLoadingOrgaAndTaxoTree();
				

//				milliPrint2 = System.currentTimeMillis() - milli;
//				GWT.log("done onSuccess SearchLoadingDialog loadAllPublicLightOganisms in \t" + milliPrint2 + "\tmilliseconds");
				
			}
		};

		try {
			
			infoDBService.loadAllPublicLightOrganisms(callback);
			
		} catch (Exception e) {
			//String mssgError = "ERROR loadAllPublicOganisms : "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}

	}
	
	
	
//	public void loadAllPublicOganisms() {
//		
//		AsyncCallback<ArrayList<OrganismItem>> callback = new AsyncCallback<
//				ArrayList<OrganismItem>
//				//AlOrganismItemAndTaxoItem
//				>() {
//
//			public void onFailure(Throwable caught) {
//				// do some UI stuff to show failure
//				
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
//			}
//
//			// @Override
//			public void onSuccess(
//					ArrayList<OrganismItem> listLOI
//					//AlOrganismItemAndTaxoItem aoiatiIT
//					) {
//				
//				
//				if (listLOI.isEmpty()) {
//					
//					//SearchViewImpl.searchPreviousButton.setVisible(false);
//					//SearchViewImpl.titleWizardSearch.setText("NO DATA...");
//					//SearchViewImpl.searchNextButton.setVisible(false);
//					SearchViewImpl.deckPSearchRoot.showWidget(2);
//					MainTabPanelView.tabPanel.setTabText(1, "NO DATA...");
//					SearchViewImpl.noOrganismInDatabase = true;
//					MainTabPanelView.tabPanel.selectTab(1);
//					Scheduler.get().scheduleDeferred(
//							new ScheduledCommand() {
//								// @Override
//								public void execute() {
//									MainTabPanelView.tabPanel.remove(5);
//									MainTabPanelView.tabPanel.remove(4);
//									MainTabPanelView.tabPanel.remove(3);
//									MainTabPanelView.tabPanel.remove(2);
//								}
//							}
//							);
//
//				} else {

//					
//					Insyght.APP_CONTROLER.setListAllPublicOrganismItem(listLOI);
//					
//
//					SearchViewImpl.comparedGenomes_alOrgaMainList = new ArrayList<LightOrganismItem>(listLOI);
//
//					Collections.sort(SearchViewImpl.comparedGenomes_alOrgaMainList, LightOrganismItemComparators.byFullNameComparator);
//					
//					SearchViewImpl.comparedGenomes_refreshListDataProviderMainListIfNedded();
//					
//
//					// SearchViewImpl.refreshAllGeneListStep2();
//					SearchViewImpl.oracle.clear();
//					ArrayList<String> aloiReturnedAsString = new ArrayList<String>();
//					for (int i = 0; i < listLOI.size(); i++) {
//						OrganismItem oi = listLOI.get(i);
//						String sIT = oi.getFullName();
//						aloiReturnedAsString.add(sIT);
//					}
//
//					SearchViewImpl.oracle.addAll(aloiReturnedAsString);
//					SearchViewImpl.countAllNumOrgaAvail.setHTML("<span style=\"font-size: large;\">A total of <span style=\"font-weight: bold;\">"
//							+ Insyght.APP_CONTROLER.getListAllPublicOrganismItem()
//									.size() + " public organisms</span> are available."
//											+ " First select your reference genome or taxonomic group by using the browser below."
//											+ " Then you can refine your search by using the menu on the left : reference CDSs, compared genomes, and vizualisation."
//											+ " Click the submit button on the left when finished to fetch the results."
//											+ "</span>");
//					
//
//					if(!listLOI.isEmpty()){
//						makeQuickAccessExamples();
//					}
//
//
//					doneWithInitialLoadAllPublicOganisms = true;
//					if (doneWithInitialLoadTaxoTree) {
//						finishInitialLoadingOrgaAndTaxoTree();
//					}
//					//try to do it in parrallel
////					// get the taxo tree
////					//loadTaxoTree(listTaxIdsReturned);
////					TaxoItem taxoItemSent = aoiatiIT.getTaxoItem();
////					// int countSelectable =
////					sortTheTaxoTreeAndCalculateFullPathInHierarchie(taxoItemSent);
////					// System.err.println("countSelectable = "+countSelectable);
////					Insyght.APP_CONTROLER.setTaxoTree(taxoItemSent);
////					Insyght.APP_CONTROLER.mapOrganismsToTaxoTree();
////					calculateSumOfLeafsInTaxoTreeRecursively(Insyght.APP_CONTROLER.getTaxoTree());
////					SearchViewImpl.buildMainTaxoBrowser();
////					ArrayList<Integer> fullPathInHierarchieInitIt = SearchViewImpl.getTheFurthestNode(taxoItemSent, false).getFullPathInHierarchie();
////					if (fullPathInHierarchieInitIt.isEmpty()) {
////						fullPathInHierarchieInitIt.add(0);
////					}
////					SearchViewImpl.selectATaxoItemInMainTaxoBrowser(fullPathInHierarchieInitIt, true); //SearchViewImpl.getTheFurthestNode(taxoItemSent, false).getFullPathInHierarchie()
////					//SearchViewImpl.MenuItems_ReferenceGenomeOrTaxonomicGroup.setTitle("Default reference organism : "+ Insyght.APP_CONTROLER.getDefaultOrganismIfNoSelection().getFullName());
////					//SearchViewImpl.titleWizardSearch.setHTML("1. Select a reference organism or taxonomic node");
////					SearchViewImpl.sugBox.setText("");
////					SearchViewImpl.sugBox_CoreDisp.setText("");
////					// SearchViewImpl.selectATaxoItem(Insyght.APP_CONTROLER.getListAllPublicOrganismItem().get(2).getLineageInCellBrowserByIdx());
////					//MainTabPanelView.getTabPanel().getTabWidget(1).setVisible(true);
////					MainTabPanelView.getTabPanel().setTabText(1, "SEARCH");
////					MainTabPanelView.getTabPanel().getTabWidget(2).setVisible(true);
////					MainTabPanelView.getTabPanel().getTabWidget(3).setVisible(true);
////					MainTabPanelView.getTabPanel().getTabWidget(4).setVisible(true);
//					
//					
//				}
//				
//			}
//		};
//
//		try {
//			
//			
//			infoDBService.getAllPublicOrganismItem(callback);
//			//infoDBService.getAllPublicOrganismItemAndTaxoTree(callback);
//			
//		} catch (Exception e) {
//			//String mssgError = "ERROR loadAllPublicOganisms : "+ e.getMessage();
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
//		}
//
//	}


	protected void makeQuickAccessExamples() {

		// setDefaultOrganismIfNoSelection
		OrganismItem defaultRefOrganismExemple = null;
		OrganismItem defaultComparedOrganismExemple = null;
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getListAllPublicOrganismItem().size(); i++) {
			OrganismItem oiIT = Insyght.APP_CONTROLER
					.getListAllPublicOrganismItem().get(i);
			if (oiIT.getTaxonId() == defaultRefOrganismExempleTaxoId) {// Bacillus subtilis strain 168, taxo_id = 224308
				Insyght.APP_CONTROLER.setDefaultOrganismIfNoSelection(oiIT);
				defaultRefOrganismExemple = oiIT;
				if(defaultComparedOrganismExemple != null){break;}
			}
			if (oiIT.getTaxonId() == defaultComparedOrganismExempleTaxoId) {// Bacillus anthracis str. 'Ames Ancestor' strain Ames Ancestor; A2084, taxo_id = 261594 [AE017334]
				defaultComparedOrganismExemple = oiIT;
				if(defaultRefOrganismExemple != null){break;}
			}
		}
		if (Insyght.APP_CONTROLER.getDefaultOrganismIfNoSelection() == null) {
			Insyght.APP_CONTROLER
					.setDefaultOrganismIfNoSelection(Insyght.APP_CONTROLER.getListAllPublicOrganismItem().get(0));
		}

		// example, browse the orthologs table for
		// for subtilis 168
		// all gene involved in the DNA replication process
		// that have homologs with Bacillus anthracis strain Ames ancestor,
		// taxo_id = 261594
		// 30 gene names: dnaA, dnaN, recF, dnaX, holB, yabA, ligA, sbcC, priA,
		// polC, nrdE, nrdF, lexA, yocI, dnaD,
		// dinG, scpB, scpA, dinB1, dnaG, dnaJ, yqeN, yshC, dnaI, dnaB, polA,
		// dnaE, tdk, dnaC, ssb

		if (defaultRefOrganismExemple != null && defaultComparedOrganismExemple != null) {

			ArrayList<FilterGeneItem> listFiltersGeneNameDNARep = new ArrayList<FilterGeneItem>();
			
			// whose product is involved with DNA related functions
			// For Archaea : Acidilobus saccharovorans 345-15, taxo_id = 666510 [CP001742]
			// For Archaea :  Caldisphaera lagunensis DSM 15908, taxo_id = 1056495
			
			FilterGeneItem fgiIT1 = new FilterGeneItem();
			fgiIT1.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Notes_product);
			fgiIT1.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.FIRST);
			fgiIT1.getListSubFilter().add(" ~* \'dna\'");//was " ~* \'^[^{]*16\\.9\\:\\s+Replicate.*$\'"
			listFiltersGeneNameDNARep.add(fgiIT1);

			FilterGeneItem fgiIT2 = new FilterGeneItem();
			fgiIT2.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Presence_absence_homology);
			fgiIT2.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.COMMA_DOTS_AND_DOTS_COMMA);
			fgiIT2.getListSubFilter().add("presence");
			fgiIT2.getListSubFilter().add(""+defaultComparedOrganismExemple.getOrganismId());
			listFiltersGeneNameDNARep.add(fgiIT2);
			
//			ArrayList<String> listGeneNameDNARep = new ArrayList<String>();
//			listGeneNameDNARep.add("dnaA");
//			listGeneNameDNARep.add("dnaN");
//			listGeneNameDNARep.add("recF");
//			listGeneNameDNARep.add("dnaX");
//			listGeneNameDNARep.add("holB");
//			listGeneNameDNARep.add("yabA");
//			listGeneNameDNARep.add("ligA");
//			listGeneNameDNARep.add("sbcC");
//			listGeneNameDNARep.add("priA");
//			listGeneNameDNARep.add("polC");
//			listGeneNameDNARep.add("nrdE");
//			listGeneNameDNARep.add("nrdF");
//			listGeneNameDNARep.add("lexA");
//			listGeneNameDNARep.add("yocI");
//			listGeneNameDNARep.add("dnaD");
//			listGeneNameDNARep.add("dinG");
//			listGeneNameDNARep.add("scpB");
//			listGeneNameDNARep.add("scpA");
//			listGeneNameDNARep.add("dinB1");
//			listGeneNameDNARep.add("dnaG");
//			listGeneNameDNARep.add("dnaJ");
//			listGeneNameDNARep.add("yqeN");
//			listGeneNameDNARep.add("yshC");
//			listGeneNameDNARep.add("dnaI");
//			listGeneNameDNARep.add("dnaB");
//			listGeneNameDNARep.add("polA");
//			listGeneNameDNARep.add("dnaE");
//			listGeneNameDNARep.add("tdk");
//			listGeneNameDNARep.add("dnaC");
//			listGeneNameDNARep.add("ssb");
//
//			ArrayList<FilterGeneItem> listFiltersGeneNameDNARep = new ArrayList<FilterGeneItem>();
//			boolean firstIter = true;
//			for(String geneNameDNARepIT : listGeneNameDNARep){
//				FilterGeneItem fgiIT = new FilterGeneItem();
//				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Gene_name);
//				if(firstIter){
//					fgiIT.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.FIRST);
//					firstIter = false;
//				} else {
//					fgiIT.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.COMMA_DOTS_OR_DOTS_COMMA);
//				}
//				fgiIT.getListSubFilter().add(" ~* \'^.*"+geneNameDNARepIT+".*$\'");
//				listFiltersGeneNameDNARep.add(fgiIT);
//			}
			

			loadGeneListForExampleOrganisms(
					defaultRefOrganismExemple,
					defaultComparedOrganismExemple,
					listFiltersGeneNameDNARep);
			
		}

		// example genomic organisation
		SearchViewImpl.HTMLExamplePreMade.setVisible(true);
		SearchViewImpl.step2VPExamplePreMade.setVisible(true);
		Anchor acBacillusExample = new Anchor();
		acBacillusExample.addStyleDependentName("cliquable");
		acBacillusExample
				.setHTML("<span style=\"color:#7A378B;font-size:large;\">&#8594; Visualize the <b>genomic organisation</b> for "
						+ Insyght.APP_CONTROLER
								.getDefaultOrganismIfNoSelection()
								.getFullName() + "</span>");
		//acBacillusExample.setWidth("600px");
		acBacillusExample.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				SearchViewImpl.setOrganismItemInSingleRefOrgaContext(Insyght.APP_CONTROLER
						.getDefaultOrganismIfNoSelection(), true, true);
				SearchViewImpl.submitSearch(EnumResultViewTypes.genomic_organization);
			}
		});
		SearchViewImpl.step2VPExamplePreMade.add(acBacillusExample);

	}

	

	private void loadGeneListForExampleOrganisms(
			final OrganismItem defaultRefOrganismIfNoSelectionOrga,
			final OrganismItem defaultComparedOrganismIfNoSelectionOrga,
			final ArrayList<FilterGeneItem> listFiltersGeneNameDNARep
			) {
		
		AsyncCallback<ArrayList<LightGeneItem>> callback = new AsyncCallback<ArrayList<LightGeneItem>>() {

			public void onFailure(Throwable caught) {
				// do nothing
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(
					final ArrayList<LightGeneItem> arrayListLightGeneItemReturned) {
				// Window.alert("returned from loadGeneListForExampleSubtilis168DNARep with success!"+arrayListLightGeneItemReturned.toString());

				if (!arrayListLightGeneItemReturned.isEmpty()) {
					// bacillus example
					SearchViewImpl.HTMLExamplePreMade.setVisible(true);
					SearchViewImpl.step2VPExamplePreMade.setVisible(true);
					Anchor acBacillusExample = new Anchor();
					acBacillusExample.addStyleDependentName("cliquable");
					acBacillusExample
							.setHTML("<span style=\"color:#8B3A3A;font-size:large;\">&#8594; Browse the <b>orthologs table</b> for all the CDS from "
									+ defaultRefOrganismIfNoSelectionOrga.getFullName()
									+ " whose product is involved with DNA related functions"
									+ " and that have homologs with "
									+ defaultComparedOrganismIfNoSelectionOrga.getFullName()
									+ "</span>");
					acBacillusExample.setWidth("600px");
					acBacillusExample.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							// select orga
							SearchViewImpl.setOrganismItemInSingleRefOrgaContext(defaultRefOrganismIfNoSelectionOrga, true, true);
							
							SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(
									true
									, arrayListLightGeneItemReturned
									, true
									, -1
									);
							SearchViewImpl.sortListReferenceGeneSetByStart();
							//SearchViewImpl.refreshDisplayListSlectedLightGeneItemFromSlectedElementStep3(0);
							
							// submit search
							SearchViewImpl.submitSearch(EnumResultViewTypes.homolog_table);
						}
					});
					
					Anchor acBacillusExampleAnnotCompa = new Anchor();
					acBacillusExampleAnnotCompa.addStyleDependentName("cliquable");
					acBacillusExampleAnnotCompa
							.setHTML("<span style=\"color:#218868;font-size:large;\">&#8594; View the <b>annotation comparator</b> for all the CDS from "
									+ defaultRefOrganismIfNoSelectionOrga.getFullName()
									+ " whose product is involved with DNA related functions"
									+ " and that have homologs with "
									+ defaultComparedOrganismIfNoSelectionOrga.getFullName()
									+ "</span>");
					//acBacillusExample.setWidth("600px");
					acBacillusExampleAnnotCompa.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							// select orga
							SearchViewImpl.setOrganismItemInSingleRefOrgaContext(defaultRefOrganismIfNoSelectionOrga, true, true);
							
							SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(
									true
									, arrayListLightGeneItemReturned
									, true
									, -1
									);
													
							SearchViewImpl.sortListReferenceGeneSetByStart();
							//SearchViewImpl.refreshDisplayListSlectedLightGeneItemFromSlectedElementStep3(0);
							// submit search
							SearchViewImpl.submitSearch(EnumResultViewTypes.annotations_comparator);
						}
					});
					
					SearchViewImpl.step2VPExamplePreMade.insert(acBacillusExampleAnnotCompa, 0);
					SearchViewImpl.step2VPExamplePreMade.insert(acBacillusExample, 0);
					
				}
			}
		};

		try {
			
//			infoDBService.getAllLightGeneItemWithOrgaIdAndListGeneName(
//					oiBacillusExample.getOrganismId(), listGeneNameDNARep,
//					callback);
			
			ArrayList<Integer> alRefElementIds = new ArrayList<Integer>();
			for(LightElementItem leiIT : defaultRefOrganismIfNoSelectionOrga.getListAllLightElementItem()){
				alRefElementIds.add(leiIT.getElementId());
			}
			
			infoDBService.getAllLightGeneItemWithListFiltersAndAlElementIds(
					listFiltersGeneNameDNARep
					, alRefElementIds
//					, alRefOrganismIds
//					, null,
					, callback
					);

		} catch (Exception e) {
			//String mssgError = "ERROR loadGeneListForSlectedElementWithFilterECNumber : "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}
	}

	

	
	public void loadGeneListForSelectedFilters(
			ArrayList<FilterGeneItem> listFilters,
			//int sizeSelectedElement,
			//int selectedElementId,
			//ArrayList<Integer> alSelectedOrganismIds,
			ArrayList<Integer> alSelectedElementIds,
			final FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters updateType,
			final boolean intersectWithRefGeneList) {

		
		AsyncCallback<ArrayList<LightGeneItem>> callback = new AsyncCallback<ArrayList<LightGeneItem>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.INSYGHT_LOADING_MASK.hide();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateBuildGeneSetSearchTab)==0) {
					SearchViewImpl.refreshListStep3.setEnabled(true);
				}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateCoreDispSearchTab)==0) {
					//SearchViewImpl.htmlStep3ResultingReferenceGenesSetInTaxoNodeContext.setHTML("<span style=\"color:red;\">Error</span>");
					SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<big><span style=\"color:red;\">Error during query</span></big>");
					SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, null, false, -1);
//					if (SearchViewImpl.alLGIFromCoreDispRequest.isEmpty()) {
//						SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs: 0 CDS selected -> default to whole proteome)");
//						SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(false);
//					} else {
//						SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs:"+SearchViewImpl.alLGIFromCoreDispRequest.size()+" CDS(s) selected");
//						SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(true);
//					}
				}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_GeneListSent)==0
						|| updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_SelectedElement)==0) {
					PopUpListGenesDialog.refreshListGenes.setEnabled(true);
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in loadGeneListForSelectedFilters : " +
							"unrecognized updateType = "+updateType));
					hide();
				}
				
				// hide();
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(
					ArrayList<LightGeneItem> arrayListLightGeneItemReturned) {
				
				if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateBuildGeneSetSearchTab)==0) {
					// SearchViewImpl.DATA_STEP3_LOADED = true;
					Insyght.APP_CONTROLER.setAllListLightGeneItemFromSlectedElement(arrayListLightGeneItemReturned);
					SearchViewImpl.deckPFetchGeneList.showWidget(1);
					SearchViewImpl.refreshAllGeneListStep3();
					SearchViewImpl.refreshListStep3.setEnabled(true);
					if(arrayListLightGeneItemReturned.size() > SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED){
						SearchViewImpl.buttonAddAllMaxFirstGeneInCart.setText("Add First "+SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED+" >>");
					}else{
						SearchViewImpl.buttonAddAllMaxFirstGeneInCart.setText("Add All >>");
					}
				}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateCoreDispSearchTab)==0) {
					//SearchViewImpl.alLGIFromCoreDispRequest = arrayListLightGeneItemReturned;
					//SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, arrayListLightGeneItemReturned, false, -1);
					if (arrayListLightGeneItemReturned.isEmpty()) {
						SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, null, false, -1);
						SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<big>&#8658; no CDS are matching this request</big>");
						//SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(false);
					} else {
						LightGeneItem.sortCDSsListByGenomicPosition(arrayListLightGeneItemReturned);
						//SearchViewImpl.alLGIFromCoreDispRequest = arrayListLightGeneItemReturned;
						SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, arrayListLightGeneItemReturned, false, -1);
						PopUpListGenesDialog.savedSortCDSListBy = PopUpListGenesDialog.GeneListSortType.GENOMIC_POSITION;
						SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<big>&#8658; "+arrayListLightGeneItemReturned.size()+" CDSs</big>");
						//SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(true);
					}

					//do not pop up gene list
//					GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.initListGenesWithAlLgi(
//							SearchViewImpl.alLGIFromCoreDispRequest,
//							PopUpListGenesDialog.GeneListSortType.GENOMIC_POSITION);
//					GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.displayPoPup();

				}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_GeneListSent)==0
						|| updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_SelectedElement)==0) {
					if (intersectWithRefGeneList) {
						ArrayList<LightGeneItem> alIntersect = PopUpListGenesDialog.intersectWithRefGeneList(arrayListLightGeneItemReturned);
						if (PopUpListGenesDialog.savedSortCDSListBy.compareTo(PopUpListGenesDialog.GeneListSortType.GENOMIC_POSITION) == 0 ) {
							LightGeneItem.sortCDSsListByGenomicPosition(alIntersect);
						} else if (PopUpListGenesDialog.savedSortCDSListBy.compareTo(PopUpListGenesDialog.GeneListSortType.SCORE_PValOverReprPheno) == 0 ) {
							alIntersect = LightGeneItem.sortCDSsListByPValueOverRepresentationGroupPlus(alIntersect);
						} else if (PopUpListGenesDialog.savedSortCDSListBy.compareTo(PopUpListGenesDialog.GeneListSortType.SCORE_CountMostRepresPheno) == 0 ) {
							alIntersect = LightGeneItem.sortCDSsListByCountOrthologsGroupPlus(alIntersect);
						} else {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in " +
									"loadGeneListForSelectedFilters : unrecognized PopUpListGenesDialog.savedSortCDSListBy = "+PopUpListGenesDialog.savedSortCDSListBy.toString()));
							hide();
						}
						PopUpListGenesDialog.refreshGeneList(alIntersect
								//, intersectWithRefGeneList
								);
					} else {
						PopUpListGenesDialog.refreshGeneList(arrayListLightGeneItemReturned
								//, intersectWithRefGeneList
								);	
					}

					GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.showCorrectWidgetsOnRefresh();
					//PopUpListGenesDialog.deckPFetchGeneList.showWidget(1);
					PopUpListGenesDialog.refreshListGenes.setEnabled(true);
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in " +
							"loadGeneListForSelectedFilters : unrecognized updateType = "+updateType));
					hide();
				}
				Insyght.INSYGHT_LOADING_MASK.hide();
				
			}
		};

		try {

			
			String htmlTextLoading = FilterGeneItem.getEnglishTraductionOflistFilters(listFilters);

			if (htmlTextLoading.startsWith("ERROR")) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in " +
						"loadGeneListForSelectedFilters : "+htmlTextLoading));
				if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateBuildGeneSetSearchTab)==0) {
					SearchViewImpl.refreshListStep3.setEnabled(true);
					SearchViewImpl.HTMLLoadingGeneList.setStyleDependentName(
							"greenText", false);
					SearchViewImpl.HTMLLoadingGeneList.setStyleDependentName(
							"redText", true);
					SearchViewImpl.HTMLLoadingGeneList
							.setHTML("Invalid query :<br/><br/>" + htmlTextLoading);
				}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateCoreDispSearchTab)==0) {
					//SearchViewImpl.htmlStep3ResultingReferenceGenesSetInTaxoNodeContext.setHTML("<span style=\"color:red;\">Error</span>");
					SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<span style=\"color:red;\">Error</span>");
				}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_GeneListSent)==0
						|| updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_SelectedElement)==0) {
					PopUpListGenesDialog.refreshListGenes.setEnabled(true);
				}

			} else if (htmlTextLoading.startsWith("TOO MANY ARGUMENTS")) {
				Window.alert(htmlTextLoading);
				if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateBuildGeneSetSearchTab)==0) {
					SearchViewImpl.refreshListStep3.setEnabled(true);
					SearchViewImpl.HTMLLoadingGeneList.setStyleDependentName(
							"greenText", false);
					SearchViewImpl.HTMLLoadingGeneList.setStyleDependentName(
							"redText", true);
					SearchViewImpl.HTMLLoadingGeneList
							.setHTML(htmlTextLoading);
				}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateCoreDispSearchTab)==0) {
					SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<span style=\"color:red;\">"+htmlTextLoading+"</span>");
				}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_GeneListSent)==0
						|| updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_SelectedElement)==0) {
					PopUpListGenesDialog.refreshListGenes.setEnabled(true);
				}
			} else {
				if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateBuildGeneSetSearchTab)==0) {
					SearchViewImpl.HTMLLoadingGeneList.setStyleDependentName(
							"greenText", true);
					SearchViewImpl.HTMLLoadingGeneList.setStyleDependentName(
							"redText", false);
					SearchViewImpl.HTMLLoadingGeneList.setHTML("Valid query :<br/>"
							+ htmlTextLoading);
				}
				Insyght.INSYGHT_LOADING_MASK.center();
				
				ArrayList<FilterGeneItem> listFiltersConvertClientSideFilter = FilterGeneItem
						.convertClientSideFilterToListPresenceGeneId(
								listFilters
								//, updateType
								);
				if(listFiltersConvertClientSideFilter != null){
					
					infoDBService.getAllLightGeneItemWithListFiltersAndAlElementIds(
							listFiltersConvertClientSideFilter
							//, alSelectedOrganismIds
							, alSelectedElementIds
							, callback);
				} else {
					infoDBService.getAllLightGeneItemWithListFiltersAndAlElementIds(
							listFilters
							//, alSelectedOrganismIds
							, alSelectedElementIds
							, callback);
				}
				
			}

		} catch (Exception e) {
			Insyght.INSYGHT_LOADING_MASK.hide();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateBuildGeneSetSearchTab)==0) {
				SearchViewImpl.refreshListStep3.setEnabled(true);
			}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateCoreDispSearchTab)==0) {
				//SearchViewImpl.htmlStep3ResultingReferenceGenesSetInTaxoNodeContext.setHTML("<span style=\"color:red;\">Error</span>");
				SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<span style=\"color:red;\">Error</span>");
			}else if (updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_GeneListSent)==0
					|| updateType.compareTo(FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_SelectedElement)==0) {
				PopUpListGenesDialog.refreshListGenes.setEnabled(true);
			}else{
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in " +
						"loadGeneListForSelectedFilters : unrecognized updateType = "+updateType));
				hide();
			}
			// hide();
		}
	}

	//Ref genes over-represented in "phenotype +" and under-represented in "phenotype -" (P-value)
	public void getReferenceGenesAndOrthologsOverRepresentatedInPhenotype(
			final Integer ReferenceOrgaId,
			final ArrayList<Integer> alSelectedPhenoPlusOrgaIds,
			final ArrayList<Integer> alSelectedPhenoMinusOrgaIds
			//final ArrayList<Integer> alSelectedNotTakenIntoAccountOrgaIds
			) {
		
		if(alSelectedPhenoPlusOrgaIds.size() + alSelectedPhenoMinusOrgaIds.size() + 1 > UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined){
			Window.alert("The combined count of organisms in group + and - is greater than "+UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined+"."
					+ " Because of server resources limitation,"
					+ " this functionality is disabled for this amount of data."
					+ " Please select "+UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined+" organisms total or less."
					);
			return;
		}

		AsyncCallback<AlItemsOfIntPrimaryIdAttributePlusScore> callback = new AsyncCallback<AlItemsOfIntPrimaryIdAttributePlusScore>() {
			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.INSYGHT_LOADING_MASK.hide();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<big><span style=\"color:red;\">Error during query</span></big>");
				SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, null, false, -1);
//				if (SearchViewImpl.alLGIFromCoreDispRequest.isEmpty()) {
//					SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs: 0 CDS selected -> default to whole proteome)");
//					SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(false);
//				} else {
//					SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs:"+SearchViewImpl.alLGIFromCoreDispRequest.size()+" CDS(s) selected");
//					SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(true);
//				}
				hide();
			}
			public void onSuccess(AlItemsOfIntPrimaryIdAttributePlusScore objIT) {
				
				ArrayList<LightGeneItem> alLightGeneItem = objIT.getAlItemsOfIntPrimaryIdAttribute();
				LinkedHashMap<Integer, String> sortedQGeneId2probability = objIT.getIntPrimaryIdAttributed2stScore();
				HashMap<Integer, Integer> geneIds2orderPosition = new HashMap<>();
				
				Storage sessionStorage = Storage.getSessionStorageIfSupported();
				if(sessionStorage != null){

					//clear related Session Storage
					SessionStorageControler.clearAllScoresPhenotypeCategories();
					int orderPosition = 0;
					if(sortedQGeneId2probability == null || alLightGeneItem == null ){
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
								new Exception("Error in getReferenceGenesAndOrthologsOverRepresentatedInPhenotype : sortedQGeneId2probability or alLightGeneItem null")
								);
					}	
						
					if(sortedQGeneId2probability.isEmpty() || alLightGeneItem.isEmpty()){
						//Window.alert("Your query returned no significant results");
						//SearchViewImpl.alLGIFromCoreDispRequest = new ArrayList<>();
						SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<big>&#8658; no CDS are matching this request</big>");
						SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, null, false, -1);
//						SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs: 0 CDS selected -> default to whole proteome)");
//						SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(false);
					} else {

						Iterator<Entry<Integer, String>> entries = sortedQGeneId2probability.entrySet().iterator();

						while (entries.hasNext()) {
							Entry<Integer, String> entry = entries.next();
							//store key value
						    sessionStorage.setItem(
						    		SessionStorageControler.PValOverReprPheno_PREFIX_KEY_INDIVIDUALS
						    				+ entry.getKey(),
						    		SessionStorageControler.PValOverReprPheno_PREFIX_VALUE_INDIVIDUALS
						    				+ entry.getValue()
						    		);
						    geneIds2orderPosition.put(entry.getKey(),orderPosition);
						    orderPosition++;
						}

						
						//store sorted list
						sessionStorage.setItem(
								SessionStorageControler.PValOverReprPheno_KEY_SORTED_LIST,
								UtilitiesMethodsShared.getItemsAsStringFromCollection(sortedQGeneId2probability.keySet())
								//sbSortedGeneIdList.toString()
								);
						//store ReferenceOrgaId
						SessionStorageControler.PValOverReprPheno_REF_ORGA_ID = ReferenceOrgaId;
						SessionStorageControler.PValOverReprPheno_alSelectedPhenoPlusOrgaIds = alSelectedPhenoPlusOrgaIds;
						SessionStorageControler.PValOverReprPheno_alSelectedPhenoMinusOrgaIds = alSelectedPhenoMinusOrgaIds;
						//SessionStorageControler.PValOverReprPheno_alSelectedEitherPresenceOrAbsenceOrgaIds = alSelectedEitherPresenceOrAbsenceOrgaIds;

						//if(alLightGeneItem != null && !alLightGeneItem.isEmpty()){
						ArrayList<LightGeneItem> sortedAlLGI = UtilitiesMethodsShared.createNewSortedArrayAccordingToPositionProviderKeyInt(
								alLightGeneItem // the array to sort
								, geneIds2orderPosition // the position provider with hasPrimaryIdAttribute as key
								, orderPosition
								, true
								, true
								);
						
						//SearchViewImpl.alLGIFromCoreDispRequest = sortedAlLGI;
						PopUpListGenesDialog.savedSortCDSListBy = PopUpListGenesDialog.GeneListSortType.SCORE_PValOverReprPheno;
						SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<big>&#8658; "+sortedAlLGI.size()+" CDSs</big>");
						SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, sortedAlLGI, false, -1);
//						SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs:"+SearchViewImpl.alLGIFromCoreDispRequest.size()+" CDS(s) selected");
//						SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(true);
//								//was pop up gene list
//								GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.initListGenesWithAlLgi(
//										sortedAlLGI,
//										PopUpListGenesDialog.GeneListSortType.SCORE_PValOverReprPheno);
//								GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.displayPoPup();
//						} else {
//							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
//									new Exception("alLightGeneItem null or empty")
//									);
//						}
						
					}
					
				} else {
					Window.alert("Session Storage is not supported in your browser, this functionality is therefore not available."
							+ " Please update your browser to enable this functionality.");
					hide();
				}
				Insyght.INSYGHT_LOADING_MASK.hide();
				
			}
		};

		try {
			
			Insyght.INSYGHT_LOADING_MASK.center();
			
			infoDBService.getReferenceGenesAndOrthologsOverRepresentatedInPhenotype(
					ReferenceOrgaId,
					alSelectedPhenoPlusOrgaIds,
					alSelectedPhenoMinusOrgaIds,
					//alSelectedNotTakenIntoAccountOrgaIds,
					callback);
			
		} catch (Exception e) {
			Insyght.INSYGHT_LOADING_MASK.hide();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			hide();
		}
		
	}
	
	// Ref genes most represented in "phenotype +" (count)
	public void getReferenceGenesMostRepresentedPhenotypePlus(
			final Integer ReferenceOrgaId,
			final ArrayList<Integer> alSelectedPhenoPlusOrgaIds) {
		
		if(alSelectedPhenoPlusOrgaIds.size() + 1 > UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined){
			Window.alert("The count of organisms in group + is greater than "+UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined+"."
					+ " Because of server resources limitation,"
					+ " this functionality is disabled for this amount of data."
					+ " Please select "+UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined+" organisms or less."
					);
			return;
		}
		
		AsyncCallback<AlItemsOfIntPrimaryIdAttributePlusScore> callback = new AsyncCallback<AlItemsOfIntPrimaryIdAttributePlusScore>() {
			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.INSYGHT_LOADING_MASK.hide();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<big><span style=\"color:red;\">Error during query</span></big>");
				SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, null, false, -1);
//				if (SearchViewImpl.alLGIFromCoreDispRequest.isEmpty()) {
//					SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs: 0 CDS selected -> default to whole proteome)");
//					SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(false);
//				} else {
//					SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs:"+SearchViewImpl.alLGIFromCoreDispRequest.size()+" CDS(s) selected");
//					SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(true);
//				}
				hide();
			}
			public void onSuccess(AlItemsOfIntPrimaryIdAttributePlusScore objIT) {

				ArrayList<LightGeneItem> alLightGeneItem = objIT.getAlItemsOfIntPrimaryIdAttribute();
				LinkedHashMap<Integer, Integer> qGeneId2countHomologs = objIT.getIntPrimaryIdAttributed2intScore();
				//sort client side		
				LinkedHashMap<Integer, Integer> sortedQGeneId2countHomologs = UtilitiesMethodsShared.sortMapByIntegerValues(
						qGeneId2countHomologs,
						true);
				HashMap<Integer, Integer> geneIds2orderPosition = new HashMap<>();
				
				Storage sessionStorage = Storage.getSessionStorageIfSupported();
				if(sessionStorage != null){

					//clear related Session Storage
					SessionStorageControler.clearAllScoresPhenotypeCategories();
					int orderPosition = 0;
					
					if(sortedQGeneId2countHomologs == null || alLightGeneItem == null ){
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
								new Exception("Error in getReferenceGenesMostRepresentedPhenotypePlus : sortedQGeneId2countHomologs or alLightGeneItem null")
								);
					}	
					
					if(sortedQGeneId2countHomologs.isEmpty() || alLightGeneItem.isEmpty()){
						//Window.alert("Your query returned no significant results");
						//SearchViewImpl.alLGIFromCoreDispRequest = new ArrayList<>();
						SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<big>&#8658; no CDS are matching this request</big>");
						SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, null, false, -1);
//						SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs: 0 CDS selected -> default to whole proteome)");
//						SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(false);
					} else {

						
						Iterator<Entry<Integer, Integer>> entries = sortedQGeneId2countHomologs.entrySet().iterator();

						while (entries.hasNext()) {
							Entry<Integer, Integer> entry = entries.next();
							//store key value
						    sessionStorage.setItem(
						    		SessionStorageControler.CountMostRepresPheno_PREFIX_KEY_INDIVIDUALS
						    				+ entry.getKey(),
						    		SessionStorageControler.CountMostRepresPheno_PREFIX_VALUE_INDIVIDUALS
						    				+ entry.getValue()
						    		);
						    geneIds2orderPosition.put(entry.getKey(),orderPosition);
						    orderPosition++;
						}

						
						//store sorted list
						sessionStorage.setItem(
								SessionStorageControler.CountMostRepresPheno_KEY_SORTED_LIST,
								UtilitiesMethodsShared.getItemsAsStringFromCollection(sortedQGeneId2countHomologs.keySet())
								//sbSortedGeneIdList.toString()
								);
						//store ReferenceOrgaId
						SessionStorageControler.CountMostRepresPheno_REF_ORGA_ID = ReferenceOrgaId;
						SessionStorageControler.CountMostRepresPheno_alSelectedPhenoPlusOrgaIds = alSelectedPhenoPlusOrgaIds;
						//SessionStorageControler.PValOverReprPheno_alSelectedEitherPresenceOrAbsenceOrgaIds = alSelectedEitherPresenceOrAbsenceOrgaIds;


						ArrayList<LightGeneItem> sortedAlLGI = UtilitiesMethodsShared.createNewSortedArrayAccordingToPositionProviderKeyInt(
								alLightGeneItem // the array to sort
								, geneIds2orderPosition // the position provider with hasPrimaryIdAttribute as key
								, orderPosition
								, true
								, true
								);
						
						//SearchViewImpl.alLGIFromCoreDispRequest = sortedAlLGI;
						PopUpListGenesDialog.savedSortCDSListBy = PopUpListGenesDialog.GeneListSortType.SCORE_CountMostRepresPheno;
						SearchViewImpl.countCDSsCurrentSelectionCoreDispGeneSet.setHTML("<big>&#8658; "+sortedAlLGI.size()+" CDSs</big>");
						SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, sortedAlLGI, false, -1);
//						SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs:"+SearchViewImpl.alLGIFromCoreDispRequest.size()+" CDS(s) selected");
//						SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(true);
						//was pop up gene list
//							GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.initListGenesWithAlLgi(
//										sortedAlLGI,
//										PopUpListGenesDialog.GeneListSortType.SCORE_CountMostRepresPheno);
//							GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.displayPoPup();

					}
					
					
					
					
				} else {
					Window.alert("Session Storage is not supported in your browser, this functionality is therefore not available."
							+ " Please update your browser to enable this functionality.");
					hide();
				}
				Insyght.INSYGHT_LOADING_MASK.hide();
				
			}
		};

		try {
			
			Insyght.INSYGHT_LOADING_MASK.center();
			
			infoDBService.getReferenceGenesMostRepresentedInAlOrganisms(
					ReferenceOrgaId,
					alSelectedPhenoPlusOrgaIds,
					callback);
			
		} catch (Exception e) {
			Insyght.INSYGHT_LOADING_MASK.hide();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			hide();
		}
		
		
	}

	
	// comparator
	public class ByNameTaxoItemComparator implements Comparator<TaxoItem> {
		// @Override
		public int compare(TaxoItem arg0, TaxoItem arg1) {
			return arg0.getName().compareTo(arg1.getName());
		}
	}



}
