package fr.inra.jouy.client.view.crossSections.popUp;


/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.NavigationControler;
import fr.inra.jouy.client.ressources.MyResources;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.crossSections.components.OrganismCell;
import fr.inra.jouy.client.view.crossSections.components.RangeLabelPager;
import fr.inra.jouy.client.view.crossSections.components.RangeLabelPagerPlusFilteredOutInfo;
import fr.inra.jouy.client.view.crossSections.components.ShowMorePagerPanel;
import fr.inra.jouy.client.view.result.CenterSLPAllGenomePanels;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
import fr.inra.jouy.client.view.search.GetResultDialog;
import fr.inra.jouy.client.view.search.SearchViewImpl;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.comparators.LightOrganismItemComparators;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_scope;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortType;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.TaxoItem;

public class ManageListComparedOrganisms extends PopupPanel{

	private static ManageListComparedOrganismsUiBinder uiBinder = GWT.create(ManageListComparedOrganismsUiBinder.class);

	interface ManageListComparedOrganismsUiBinder extends UiBinder<Widget, ManageListComparedOrganisms> {}

	// var

	//tab1
	//not Needed tab 1 anymore, search tab has its own implementation
	//public static ArrayList<LightOrganismItem> alOrgaMainListForTab1 = new ArrayList<>();
	//public static ArrayList<LightOrganismItem> alOrgaFeaturedListForTab1 = new ArrayList<>();
	//public static ArrayList<LightOrganismItem> alOrgaMainListFilteredOutForTab1 = new ArrayList<>();
	//tab2And4
	//Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult()
	//Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults()

	
	// general UI
	@UiField
	public static DockLayoutPanel dlpManageListComparedOrganisms;
	@UiField
	public static Button closeWindowFromUpperBar;
	@UiField
	public static DeckPanel DP_MAIN_MENU;
			

	// menu
	@UiField
	public static HTML MenuItems_FindFeatureGenomes;
	@UiField
	public static HTML MenuItems_SortResultListBy;
	@UiField
	public static Button closeWindowFromMenu;

	
	// Find / feature genomes
	@UiField public static HTML mainList_title;
	private static CellList<LightOrganismItem> clMainList;
	private static ListDataProvider<LightOrganismItem> ldpMainList;
	final MultiSelectionModel<LightOrganismItem> selectionModelMainList = new MultiSelectionModel<LightOrganismItem>();
	@UiField public static RangeLabelPagerPlusFilteredOutInfo rlpMainList;
	@UiField public static ShowMorePagerPanel ppMainList;
	@UiField static VerticalPanel VPSuggestBoxSearch;
	public static SuggestBox suggestBoxSearchMain = new SuggestBox(SearchViewImpl.oracle);
	public static SuggestBox suggestBoxSearchFeatured = new SuggestBox(SearchViewImpl.oracle);
	@UiField public static Image mainList_searchIcon;
	@UiField public static Button clearSearchMain;
	private static CellList<LightOrganismItem> clFeaturedList;
	private static ListDataProvider<LightOrganismItem> ldpFeaturedList;
	final MultiSelectionModel<LightOrganismItem> selectionModelFeaturedList = new MultiSelectionModel<LightOrganismItem>();
	@UiField public static VerticalPanel featuredOrganisms_VPSuggestBoxSearch;
	@UiField public static Image featuredOrganisms_searchIcon;
	@UiField public static Button featuredOrganisms_clearSearchFeatured;
	@UiField public static HTML featuredList_title;
	@UiField public static RangeLabelPager rlpFeaturedList;
	@UiField public static ShowMorePagerPanel ppFeaturedList;
	@UiField public static HTML btAddFeaturedOrga;
	@UiField public static HTML btRemoveFeaturedOrga;
	@UiField public static HTML btMoveUpOrgaInList;
	@UiField public static HTML btMoveDownOrgaInList;
	@UiField public static HTML btAddAllFeaturedOrga;
	@UiField public static HTML btRemoveAllFeaturedOrga;
	@UiField public static Button btResetFindFeatureGenomes;
	
	public static ArrayList<LightOrganismItem> alOrgaMainList_filtredIn = new ArrayList<>();
	public static ArrayList<LightOrganismItem> alOrgaMainList_filtredOut = new ArrayList<>();
	public static ArrayList<LightOrganismItem> alOrgaFeaturedList_filtredIn = new ArrayList<>();
	public static ArrayList<LightOrganismItem> alOrgaFeaturedList_filtredOut = new ArrayList<>();
	//	public static ArrayList<LightOrganismItem> alOrgaMainList = new ArrayList<>();
//	public static ArrayList<LightOrganismItem> alOrgaFeaturedList = new ArrayList<>();
//	public static ArrayList<LightOrganismItem> alOrgaMainListFilteredOut = new ArrayList<>();
	public static HashMap<Integer, Integer> tmSavedMovesOrgaId2FinalPositionInMainList = new HashMap<>();
	//public static ArrayList<LightOrganismItem> alOrgaMainListFilteredOutForTab2And4 = new ArrayList<>();
	public static boolean genomeResultListHasChanged = false;
	public static boolean sortOptionsHaveChanged = false;
	public static boolean IS_RESIZING = false;
	public static boolean listDataProviderLOIMainListHasChanged = false;
	public static boolean listDataProviderLOIFeaturedListHasChanged = false;
	
	
	// sort result by
	@UiField
	VerticalPanel VP_SORT_SCOPE;
	public static RadioButton RB1_SORT_SCOPE_0;
	public static RadioButton RB1_SORT_SCOPE_1;
	public static RadioButton RB1_SORT_SCOPE_2;
	@UiField
	VerticalPanel VP_SORT_TYPE;
	public static RadioButton RB1_SORT_TYPE_0;
	public static RadioButton RB1_SORT_TYPE_1;
	public static RadioButton RB1_SORT_TYPE_2;
	public static RadioButton RB1_SORT_TYPE_3;
	@UiField
	VerticalPanel VP_SORT_ORDER;
	public static RadioButton RB1_SORT_ORDER_0;
	public static RadioButton RB1_SORT_ORDER_1;
	// @UiField Button BT_SORT;
	
	@Override
	protected void onUnload() {
		
		/*if ( MainTabPanelView.tabPanel.getSelectedIndex() == 1) {
			Insyght.APP_CONTROLER.getUserSearchItem().setListUserSelectedOrgaToBeResults(new ArrayList<LightOrganismItem>(alOrgaFeaturedList));
		} else */
		if ( MainTabPanelView.tabPanel.getSelectedIndex() == 2 || MainTabPanelView.tabPanel.getSelectedIndex() == 4 ) {
			//alOrgaMainListFilteredOut = alOrgaMainListFilteredOutForTab2And4;
			applyUserOptionsToExistingResultAndLaunchUpdateDisplay();
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR ManageListComparedOrganisms onUnload : not recognized MainTabPanelView.tabPanel.getSelectedIndex() = "
					+ MainTabPanelView.tabPanel.getSelectedIndex()
					));
		}
		clearSearchMain(true, true); //must be before super.onDetach i norder to have the all list of compared organism in main list despite filters by user
		clearSearchFeatured(true, true);
	}
//	@Override
//	protected void onDetach() {
//		super.onDetach();
//	}
	
	public ManageListComparedOrganisms() {
		setWidget(uiBinder.createAndBindUi(this));
		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				if (isShowing() && ! IS_RESIZING) {
					IS_RESIZING = true;
					setHeightWidthAndShowCenter();
					IS_RESIZING = false;
				}
			}
        });
		setAnimationEnabled(true);
		setGlassEnabled(true);
		setAutoHideEnabled(true);
		
		// init Find / feature genomes
		// MainList
		selectionModelMainList.addSelectionChangeHandler(new Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				if ( ! selectionModelFeaturedList.getSelectedSet().isEmpty() && ! selectionModelMainList.getSelectedSet().isEmpty() ) {
					selectionModelFeaturedList.clear();
				}
			}
		});
		clMainList = new CellList<LightOrganismItem>(new OrganismCell());
		clMainList.setPageSize(30);
		clMainList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		clMainList.setSelectionModel(selectionModelMainList);
		ldpMainList = new ListDataProvider<LightOrganismItem>();
		ldpMainList.addDataDisplay(clMainList);
		ppMainList.setDisplay(clMainList);
		rlpMainList.setDisplay(clMainList);
		mainList_searchIcon.setResource(MyResources.INSTANCE.searchIcon());
		mainList_searchIcon.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				searchForOrganismsOrTaxons_mainList(suggestBoxSearchMain.getText());
			}
		});
		
		suggestBoxSearchMain.setWidth("100%");
		suggestBoxSearchMain.setAutoSelectEnabled(false);
		suggestBoxSearchMain.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					searchForOrganismsOrTaxons_mainList(suggestBoxSearchMain.getText());
				}
			}
		});
		suggestBoxSearchMain.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				searchForOrganismsOrTaxons_mainList(event
						.getSelectedItem().getReplacementString());
			}
		});
		suggestBoxSearchMain.getValueBox().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				suggestBoxSearchMain.setText("");
			}
		});
		suggestBoxSearchMain.setText("filter for organisms and taxons");
		VPSuggestBoxSearch.add(suggestBoxSearchMain);
		
		//FeaturedGenomes
		featuredOrganisms_searchIcon.setResource(MyResources.INSTANCE.searchIcon());
		featuredOrganisms_searchIcon.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				searchForOrganismsOrTaxons_featuredList(suggestBoxSearchFeatured.getText());
			}
		});
		selectionModelFeaturedList.addSelectionChangeHandler(new Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				if ( ! selectionModelMainList.getSelectedSet().isEmpty() && ! selectionModelFeaturedList.getSelectedSet().isEmpty() ) {
					selectionModelMainList.clear();
				}
			}
		});
		clFeaturedList = new CellList<LightOrganismItem>(new OrganismCell());
		clFeaturedList.setPageSize(30);
		clFeaturedList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		clFeaturedList.setSelectionModel(selectionModelFeaturedList);
		ldpFeaturedList = new ListDataProvider<LightOrganismItem>();
		ldpFeaturedList.addDataDisplay(clFeaturedList);
		ppFeaturedList.setDisplay(clFeaturedList);
		rlpFeaturedList.setDisplay(clFeaturedList);
		suggestBoxSearchFeatured.setWidth("100%");
		suggestBoxSearchFeatured.setAutoSelectEnabled(false);
		suggestBoxSearchFeatured.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					searchForOrganismsOrTaxons_featuredList(suggestBoxSearchFeatured.getText());
				}
			}
		});
		suggestBoxSearchFeatured.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				searchForOrganismsOrTaxons_featuredList(event
						.getSelectedItem().getReplacementString());
			}
		});
		suggestBoxSearchFeatured.getValueBox().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				suggestBoxSearchFeatured.setText("");
			}
		});
		suggestBoxSearchFeatured.setText("filter for organisms and taxons");
		featuredOrganisms_VPSuggestBoxSearch.add(suggestBoxSearchFeatured);
		featuredOrganisms_clearSearchFeatured.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				clearSearchFeatured(true, true);
			}
		});
		
		// init Sort result list by
		RB1_SORT_SCOPE_0 = new RadioButton("group_SORT_SCOPE",
				"Selected reference gene");
		RB1_SORT_SCOPE_0.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				RB1_SORT_TYPE_2.setEnabled(true);
				RB1_SORT_TYPE_3.setEnabled(true);
				sortOptionsHaveChanged = true;
				//sortMainResultListsAccordingToUserOptions();
			}
		});
		RB1_SORT_SCOPE_0
				.setTitle(
						//"Sort by a single reference gene. This is equivalent to sorting by a single column. To use this scope, select a reference gene previous to changing the scope."
						"Sort by a single reference gene."
						+ " This is equivalent to sorting by a single column in the homolog browsing view."
						+ " To use this scope, select the reference gene in the \"Ortholog table\" or \"Genomic organisation\" view previous to changing this scope."
						+ " For this reason, this option is de-activated in the \"Search\" tab but activated in the \"Ortholog table\" and \"Genomic organisation\" through the menu \"Compared organisms -> Manage list / Find\""
						+ " The score for each organism is calculated according to the sort type selected below."
						);
		VP_SORT_SCOPE.add(RB1_SORT_SCOPE_0);
		RB1_SORT_SCOPE_0.setEnabled(false);
		RB1_SORT_SCOPE_1 = new RadioButton("group_SORT_SCOPE",
				"Reference gene set");
		RB1_SORT_SCOPE_1.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				RB1_SORT_TYPE_2.setEnabled(false);
				RB1_SORT_TYPE_3.setEnabled(false);
				if (RB1_SORT_TYPE_2.getValue() || RB1_SORT_TYPE_3.getValue()) {
					RB1_SORT_TYPE_0.setValue(true);
				}
				sortOptionsHaveChanged = true;
				//sortMainResultListsAccordingToUserOptions();
			}
		});
		RB1_SORT_SCOPE_1
				.setTitle(
						//"The scope is the gene set that you selected under the search tab ; If the current view is the homolog browsing view, it is equivalent to sorting by all columns"
						"The scope is the gene set that you build or selected."
						+ " The score for each organism is calculated according to the sort type selected below."
						+ " The total score for a given organism is the sum of the individual scores for each gene in the gene set."
						+ " For the \"Ortholog table\" view, it is equivalent to sorting by all columns when displaying a reference gene set."
						);
		VP_SORT_SCOPE.add(RB1_SORT_SCOPE_1);
		RB1_SORT_SCOPE_2 = new RadioButton("group_SORT_SCOPE", "Whole organism");
		RB1_SORT_SCOPE_2.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				// RB1_SORT_TYPE_2.setEnabled(false);
				RB1_SORT_TYPE_2.setEnabled(true);
				RB1_SORT_TYPE_3.setEnabled(false);
				if (//RB1_SORT_TYPE_2.getValue() || 
						RB1_SORT_TYPE_3.getValue()) {
					RB1_SORT_TYPE_0.setValue(true);
				}
				sortOptionsHaveChanged = true;
				//sortMainResultListsAccordingToUserOptions();
			}
		});
		RB1_SORT_SCOPE_2
				.setTitle(
						//"The scope is the whole reference organism, regardless of the gene set or displayed region"
						"The scope is the whole reference organism, regardless of the gene set or displayed region."
						+ " The score for each organism is calculated according to the sort type selected below."
						+ " The total score for a given organism is the sum of the individual scores for each gene of the organism."
						+ " For the \"Ortholog table\" view, it is equivalent to sorting by all columns when displaying all the CDSs of the organism."
						);
		VP_SORT_SCOPE.add(RB1_SORT_SCOPE_2);

		RB1_SORT_TYPE_0 = new RadioButton("group_SORT_TYPE",
				"Abundance of orthologs");
		/*
		 * RB1_SORT_TYPE_0.addClickHandler(new ClickHandler() { public void
		 * onClick(ClickEvent event) {
		 * setCorrectSORT_SCOPE_TYPE_FUTUR_QUICK_SEARCH(); } });
		 */
		RB1_SORT_TYPE_0
				.setTitle(
						//"The more homologs with the reference, the higher the organism will score."
						"The more number of orthologs with the reference, the higher the score."
						+ " 1 point for 1 ortholog."
						);
		RB1_SORT_TYPE_0.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sortOptionsHaveChanged = true;
				//sortMainResultListsAccordingToUserOptions();
			}
		});
		VP_SORT_TYPE.add(RB1_SORT_TYPE_0);

		RB1_SORT_TYPE_1 = new RadioButton("group_SORT_TYPE", "Synteny score");
		/*
		 * RB1_SORT_TYPE_1.addClickHandler(new ClickHandler() { public void
		 * onClick(ClickEvent event) {
		 * setCorrectSORT_SCOPE_TYPE_FUTUR_QUICK_SEARCH(); } });
		 */
		RB1_SORT_TYPE_1
				.setTitle(
						//"The syntenies that have the longer stroke of co-localized homologies will have the better score."
						"The score for a given synteny is defined as follow according to its constituants (score by default) : 4 points for each ortholog BDBH, 2 points for each ortholog non BDBH, -3 points for each gap or mismatch."
						+ " Therefore the syntenies with the better score will be the one that have the highest number of co-localized orthologs and the fewest number of gaps or mismatchs."
						+ " The total synteny score for a given compared organism is the sum of each synteny score of the orthologs for the selected scope."
						);
		RB1_SORT_TYPE_1.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sortOptionsHaveChanged = true;
				//sortMainResultListsAccordingToUserOptions();
			}
		});
		VP_SORT_TYPE.add(RB1_SORT_TYPE_1);

		RB1_SORT_TYPE_2 = new RadioButton("group_SORT_TYPE", "Alignemnt score");

		RB1_SORT_TYPE_2
				.setTitle(
						"The alignemnt score for a given compared organism sums all the blast alignment score of the orthologs for the selected scope."
						+ " Therefore the more overall pair base similarities between orthologs, the higher the compared organism will score."
						);
		RB1_SORT_TYPE_2.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sortOptionsHaveChanged = true;
				//sortMainResultListsAccordingToUserOptions();
			}
		});
		VP_SORT_TYPE.add(RB1_SORT_TYPE_2);

		RB1_SORT_TYPE_3 = new RadioButton("group_SORT_TYPE",
				"Abundance of orthologs' annotations");
		RB1_SORT_TYPE_3
				.setTitle(
						"For a given ortholog, the score for abundance of orthologs' annotations is the number of its functional annotations (i.e molecular function, product, etc.)"
						+ " The total score for a given compared organism is the sum of each abundance of homologs' annotations score of the orthologs for the selected scope."
						);
		RB1_SORT_TYPE_3.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sortOptionsHaveChanged = true;
				//sortMainResultListsAccordingToUserOptions();
			}
		});
		VP_SORT_TYPE.add(RB1_SORT_TYPE_3);

		RB1_SORT_ORDER_0 = new RadioButton("group_SORT_ORDER", "Descending");
		RB1_SORT_ORDER_0.setTitle("Arranged from largest to smallest. Decreasing.");
		RB1_SORT_ORDER_0.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sortOptionsHaveChanged = true;
				//sortMainResultListsAccordingToUserOptions();
			}
		});
		VP_SORT_ORDER.add(RB1_SORT_ORDER_0);

		RB1_SORT_ORDER_1 = new RadioButton("group_SORT_ORDER", "Ascending");
		RB1_SORT_ORDER_1.setTitle("Arranged from smallest to largest. Increasing.");
		RB1_SORT_ORDER_1.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sortOptionsHaveChanged = true;
				//sortMainResultListsAccordingToUserOptions();
			}
		});
		VP_SORT_ORDER.add(RB1_SORT_ORDER_1);
		
		selectMenu("Find / feature genomes");
		
	}


//	@UiHandler("searchIcon")
//	void onSearchIconClick(ClickEvent e) {
//		searchForOrganismsOrTaxons(suggestBoxSearchMain.getText());
//	}
	
	@UiHandler("clearSearchMain")
	void onClearSearchMainClick(ClickEvent e) {
		clearSearchMain(true, true);
	}
	
	
	@UiHandler("closeWindowFromUpperBar")
	void onCancelFindGeneClick(ClickEvent e) {
		hide();
	}
	
	@UiHandler("closeWindowFromMenu")
	void onCloseWindowFromMenuClick(ClickEvent e) {
		hide();
	}
	

	
	
	@UiHandler("MenuItems_FindFeatureGenomes")
	void onMenuItems_FindFeatureGenomesClick(ClickEvent e) {
		selectMenu("Find / feature genomes");
	}
	
	@UiHandler("MenuItems_SortResultListBy")
	void onMenuItems_SortResultListByClick(ClickEvent e) {
		selectMenu("Sort result list by");
	}
	
	@UiHandler("btAddAllFeaturedOrga")
	void onBtAddAllFeaturedOrgaClick(ClickEvent e) {
		clearSearchFeatured(false, true);
		HashSet<Integer> hsAlreadyAdded = new HashSet<>();
		for (LightOrganismItem loiIT : alOrgaFeaturedList_filtredIn ) {
			hsAlreadyAdded.add(loiIT.getOrganismId());
		}
		Iterator<LightOrganismItem> it = alOrgaMainList_filtredIn.iterator();
		while (it.hasNext()) {
			LightOrganismItem loiIT = it.next();
			if ( ! hsAlreadyAdded.contains( loiIT.getOrganismId() )) {
				alOrgaFeaturedList_filtredIn.add(loiIT);
			}
		    it.remove();
		}	

		listDataProviderLOIMainListHasChanged = true;
		listDataProviderLOIFeaturedListHasChanged = true;
		refreshListDataProviderMainListIfNedded();
		refreshListDataProviderFeaturedListIfNedded();
		genomeResultListHasChanged = true;
	}
	
	@UiHandler("btAddFeaturedOrga")
	void onBtAddFeaturedOrgaClick(ClickEvent e) {
		
		clearSearchFeatured(false, true);
		HashSet<Integer> hsAlreadyAdded = new HashSet<>();
		for (LightOrganismItem loiIT : alOrgaFeaturedList_filtredIn ) {
			hsAlreadyAdded.add(loiIT.getOrganismId());
		}
		HashSet<Integer> hsToAdd = new HashSet<>();
		for (LightOrganismItem loiIT : selectionModelMainList.getSelectedSet() ) {
			hsToAdd.add(loiIT.getOrganismId());
		}
		Iterator<LightOrganismItem> it = alOrgaMainList_filtredIn.iterator();
		while (it.hasNext()) {
			LightOrganismItem loiIT = it.next();
			if (hsToAdd.contains(loiIT.getOrganismId())) {
				//found a selected
				if ( ! hsAlreadyAdded.contains( loiIT.getOrganismId() )) {
					alOrgaFeaturedList_filtredIn.add(loiIT);
				}
			    it.remove();
			}
		}	
		
		listDataProviderLOIMainListHasChanged = true;
		listDataProviderLOIFeaturedListHasChanged = true;
		refreshListDataProviderMainListIfNedded();
		refreshListDataProviderFeaturedListIfNedded();
		genomeResultListHasChanged = true;
		
		
//		Set<LightOrganismItem> setLOIToAdd = selectionModelMainList.getSelectedSet();
//		LIST_SELECTED_ORGA_TO_ADD_TO_FEATURED_GENOMES: for (LightOrganismItem LOISelected : setLOIToAdd) {
//			for (LightOrganismItem LOIMainListIT : alOrgaMainList) {
//				if (LOISelected.getOrganismId() == LOIMainListIT.getOrganismId()) {
//					//found it
//					alOrgaFeaturedList.add(LOISelected);
//					alOrgaMainList.remove(LOIMainListIT);
//					continue LIST_SELECTED_ORGA_TO_ADD_TO_FEATURED_GENOMES;
//				}
//			}
//		}
//		
//		listDataProviderLOIMainListHasChanged = true;
//		listDataProviderLOIFeaturedGenomesHasChanged = true;
//		refreshListDataProviderMainListIfNedded();
//		refreshListDataProviderFeaturedGenomesIfNedded();
//		genomeResultListHasChanged = true;
	}

	@UiHandler("btRemoveFeaturedOrga")
	void onBtRemoveFeaturedOrgaClick(ClickEvent e) {
		clearSearchMain(false, true);
		Set<LightOrganismItem> setLOIToRemove = selectionModelFeaturedList.getSelectedSet();
		alOrgaMainList_filtredIn.addAll(setLOIToRemove);
		Collections.sort(alOrgaMainList_filtredIn, LightOrganismItemComparators.byPositionInResultSetComparator);
		replaySavedMovesToChangePositionInMainList();
		alOrgaFeaturedList_filtredIn.removeAll(setLOIToRemove);
		listDataProviderLOIMainListHasChanged = true;
		listDataProviderLOIFeaturedListHasChanged = true;
		refreshListDataProviderMainListIfNedded();
		refreshListDataProviderFeaturedListIfNedded();
		genomeResultListHasChanged = true;
	}
//	@UiHandler("btRemoveFeaturedOrga")
//	void onBtRemoveFeaturedOrgaClick(ClickEvent e) {
//		Set<LightOrganismItem> setLOIToRemove = selectionModelFeaturedGenomes.getSelectedSet();	
//		alOrgaMainList.addAll(setLOIToRemove);
//		Collections.sort(alOrgaMainList, LightOrganismItemComparators.byPositionInResultSetComparator);
//		replaySavedMovesToChangePositionInMainList();
//		alOrgaFeaturedList.removeAll(setLOIToRemove);
//		listDataProviderLOIMainListHasChanged = true;
//		listDataProviderLOIFeaturedGenomesHasChanged = true;
//		refreshListDataProviderMainListIfNedded();
//		refreshListDataProviderFeaturedGenomesIfNedded();
//		genomeResultListHasChanged = true;
//	}
	
	@UiHandler("btRemoveAllFeaturedOrga")
	void onBtRemoveAllFeaturedOrgaClick(ClickEvent e) {
		clearSearchMain(false, true);
		alOrgaMainList_filtredIn.addAll(alOrgaFeaturedList_filtredIn);
		Collections.sort(alOrgaMainList_filtredIn, LightOrganismItemComparators.byPositionInResultSetComparator);
		replaySavedMovesToChangePositionInMainList();
		alOrgaFeaturedList_filtredIn.clear();
		listDataProviderLOIMainListHasChanged = true;
		listDataProviderLOIFeaturedListHasChanged = true;
		refreshListDataProviderMainListIfNedded();
		refreshListDataProviderFeaturedListIfNedded();
		genomeResultListHasChanged = true;
	}
	
	
	@UiHandler("btMoveUpOrgaInList")
	void onBtMoveUpOrgaInListClick(ClickEvent e) {
		Set<LightOrganismItem> setSelectedSetMainListUnsorted = selectionModelMainList.getSelectedSet();
		ArrayList<LightOrganismItem> setSelectedSetMainList = new ArrayList<>(setSelectedSetMainListUnsorted);
		Collections.sort(setSelectedSetMainList, LightOrganismItemComparators.byPositionInSearchViewImplComparedGenomes_alOrgaMainList);
		for (LightOrganismItem selectedSetMainListIT : setSelectedSetMainList) {
			int idxToMoveUp = alOrgaMainList_filtredIn.indexOf(selectedSetMainListIT);
			if (idxToMoveUp > 0) {
				Collections.swap(alOrgaMainList_filtredIn, idxToMoveUp, idxToMoveUp-1);
				tmSavedMovesOrgaId2FinalPositionInMainList.put(selectedSetMainListIT.getOrganismId(), idxToMoveUp-1);
				listDataProviderLOIMainListHasChanged = true;
			} else {
				break;
			}
		}
		Set<LightOrganismItem> setSelectedSetFeaturedGenomesUnsorted = selectionModelFeaturedList.getSelectedSet();
		ArrayList<LightOrganismItem> setSelectedSetFeaturedGenomes = new ArrayList<>(setSelectedSetFeaturedGenomesUnsorted);
		Collections.sort(setSelectedSetFeaturedGenomes, LightOrganismItemComparators.byPositionInGetUserSearchItemGetListUserSelectedOrgaToBeResults);
		for (LightOrganismItem selectedSetFeaturedGenomesIT : setSelectedSetFeaturedGenomes) {
			int idxToMoveUp = alOrgaFeaturedList_filtredIn.indexOf(selectedSetFeaturedGenomesIT);
			if (idxToMoveUp > 0) {
				Collections.swap(alOrgaFeaturedList_filtredIn, idxToMoveUp, idxToMoveUp-1);
				listDataProviderLOIFeaturedListHasChanged = true;
			} else {
				break;
			}
		}
		refreshListDataProviderMainListIfNedded();
		refreshListDataProviderFeaturedListIfNedded();
		genomeResultListHasChanged = true;
	}
//	@UiHandler("btMoveUpOrgaInList")
//	void onBtMoveUpOrgaInListClick(ClickEvent e) {
//
//		Set<LightOrganismItem> setSelectedSetMainListUnsorted = selectionModelMainList.getSelectedSet();
//		ArrayList<LightOrganismItem> setSelectedSetMainList = new ArrayList<>(setSelectedSetMainListUnsorted);
//		Collections.sort(setSelectedSetMainList, LightOrganismItemComparators.byPositionInManageListComparedOrganismsAlOrgaMainList);
//		for (LightOrganismItem selectedSetMainListIT : setSelectedSetMainList) {
//			int idxToMoveUp = alOrgaMainList.indexOf(selectedSetMainListIT);
//			if (idxToMoveUp > 0) {
//				Collections.swap(alOrgaMainList, idxToMoveUp, idxToMoveUp-1);
//				tmSavedMovesOrgaId2FinalPositionInMainList.put(selectedSetMainListIT.getOrganismId(), idxToMoveUp-1);
//				listDataProviderLOIMainListHasChanged = true;
//			} else {
//				break;
//			}
//		}
//		Set<LightOrganismItem> setSelectedSetFeaturedGenomesUnsorted = selectionModelFeaturedGenomes.getSelectedSet();
//		ArrayList<LightOrganismItem> setSelectedSetFeaturedGenomes = new ArrayList<>(setSelectedSetFeaturedGenomesUnsorted);
//		Collections.sort(setSelectedSetFeaturedGenomes, LightOrganismItemComparators.byPositionInManageListComparedOrganismsAlOrgaFeaturedList);
//		for (LightOrganismItem selectedSetFeaturedGenomesIT : setSelectedSetFeaturedGenomes) {
//			int idxToMoveUp = alOrgaFeaturedList.indexOf(selectedSetFeaturedGenomesIT);
//			if (idxToMoveUp > 0) {
//				Collections.swap(alOrgaFeaturedList, idxToMoveUp, idxToMoveUp-1);
//				listDataProviderLOIFeaturedGenomesHasChanged = true;
//			} else {
//				break;
//			}
//		}
//		refreshListDataProviderMainListIfNedded();
//		refreshListDataProviderFeaturedGenomesIfNedded();
//		genomeResultListHasChanged = true;
//	}
	
	@UiHandler("btMoveDownOrgaInList")
	void onBtMoveDownOrgaInListClick(ClickEvent e) {
		Set<LightOrganismItem> setSelectedSetMainListUnsorted = selectionModelMainList.getSelectedSet();
		ArrayList<LightOrganismItem> setSelectedSetMainList = new ArrayList<>(setSelectedSetMainListUnsorted);
		Collections.sort(setSelectedSetMainList, LightOrganismItemComparators.byPositionInSearchViewImplComparedGenomes_alOrgaMainList.reversed());
		for (LightOrganismItem selectedSetMainListIT : setSelectedSetMainList) {
			int idxToMoveDown = alOrgaMainList_filtredIn.indexOf(selectedSetMainListIT);
			if (idxToMoveDown < alOrgaMainList_filtredIn.size() - 1 ) {
				Collections.swap(alOrgaMainList_filtredIn, idxToMoveDown, idxToMoveDown+1);
				tmSavedMovesOrgaId2FinalPositionInMainList.put(selectedSetMainListIT.getOrganismId(), idxToMoveDown+1 );
				listDataProviderLOIMainListHasChanged = true;
			} else {
				break;
			}
		}
		Set<LightOrganismItem> setSelectedSetFeaturedGenomesUnsorted = selectionModelFeaturedList.getSelectedSet();
		ArrayList<LightOrganismItem> setSelectedSetFeaturedGenomes = new ArrayList<>(setSelectedSetFeaturedGenomesUnsorted);
		Collections.sort(setSelectedSetFeaturedGenomes, LightOrganismItemComparators.byPositionInGetUserSearchItemGetListUserSelectedOrgaToBeResults.reversed());
		for (LightOrganismItem selectedSetFeaturedGenomesIT : setSelectedSetFeaturedGenomes) {
			int idxToMoveDown = alOrgaFeaturedList_filtredIn.indexOf(selectedSetFeaturedGenomesIT);
			if (idxToMoveDown < alOrgaFeaturedList_filtredIn.size() - 1 ) {
				Collections.swap(alOrgaFeaturedList_filtredIn, idxToMoveDown, idxToMoveDown+1);
				listDataProviderLOIFeaturedListHasChanged = true;
			} else {
				break;
			}
		}
		refreshListDataProviderMainListIfNedded();
		refreshListDataProviderFeaturedListIfNedded();
		genomeResultListHasChanged = true;
	}
//	@UiHandler("btMoveDownOrgaInList")
//	void onBtMoveDownOrgaInListClick(ClickEvent e) {
//
//		Set<LightOrganismItem> setSelectedSetMainListUnsorted = selectionModelMainList.getSelectedSet();
//		ArrayList<LightOrganismItem> setSelectedSetMainList = new ArrayList<>(setSelectedSetMainListUnsorted);
//		Collections.sort(setSelectedSetMainList, LightOrganismItemComparators.byPositionInManageListComparedOrganismsAlOrgaMainList.reversed());
//		for (LightOrganismItem selectedSetMainListIT : setSelectedSetMainList) {
//			int idxToMoveDown = alOrgaMainList.indexOf(selectedSetMainListIT);
//			if (idxToMoveDown < alOrgaMainList.size() - 1 ) {
//				Collections.swap(alOrgaMainList, idxToMoveDown, idxToMoveDown+1);
//				tmSavedMovesOrgaId2FinalPositionInMainList.put(selectedSetMainListIT.getOrganismId(), idxToMoveDown+1 );
//				listDataProviderLOIMainListHasChanged = true;
//			} else {
//				break;
//			}
//		}
//		Set<LightOrganismItem> setSelectedSetFeaturedGenomesUnsorted = selectionModelFeaturedGenomes.getSelectedSet();
//		ArrayList<LightOrganismItem> setSelectedSetFeaturedGenomes = new ArrayList<>(setSelectedSetFeaturedGenomesUnsorted);
//		Collections.sort(setSelectedSetFeaturedGenomes, LightOrganismItemComparators.byPositionInManageListComparedOrganismsAlOrgaFeaturedList.reversed());
//		for (LightOrganismItem selectedSetFeaturedGenomesIT : setSelectedSetFeaturedGenomes) {
//			int idxToMoveDown = alOrgaFeaturedList.indexOf(selectedSetFeaturedGenomesIT);
//			if (idxToMoveDown < alOrgaFeaturedList.size() - 1 ) {
//				Collections.swap(alOrgaFeaturedList, idxToMoveDown, idxToMoveDown+1);
//				listDataProviderLOIFeaturedGenomesHasChanged = true;
//			} else {
//				break;
//			}
//		}
//		refreshListDataProviderMainListIfNedded();
//		refreshListDataProviderFeaturedGenomesIfNedded();
//		genomeResultListHasChanged = true;
//	}
	
	@UiHandler("btResetFindFeatureGenomes")
	void onBtResetFindFeatureGenomesClick(ClickEvent e) {
		clearSearchFeatured(false, true);
		clearSearchMain(false, true);
		alOrgaMainList_filtredIn.addAll(alOrgaFeaturedList_filtredIn);
		alOrgaFeaturedList_filtredIn.clear();
		tmSavedMovesOrgaId2FinalPositionInMainList.clear();
		Collections.sort(alOrgaMainList_filtredIn, LightOrganismItemComparators.byPositionInResultSetComparator);
		listDataProviderLOIMainListHasChanged = true;
		listDataProviderLOIFeaturedListHasChanged = true;
		refreshListDataProviderMainListIfNedded();
		refreshListDataProviderFeaturedListIfNedded();
		genomeResultListHasChanged = true;
	}
//	@UiHandler("btResetFindFeatureGenomes")
//	void onBtResetFindFeatureGenomesClick(ClickEvent e) {
//		alOrgaMainList.addAll(alOrgaFeaturedList);
//		alOrgaFeaturedList.clear();
//		tmSavedMovesOrgaId2FinalPositionInMainList.clear();
//		Collections.sort(alOrgaMainList, LightOrganismItemComparators.byPositionInResultSetComparator);
//		listDataProviderLOIMainListHasChanged = true;
//		listDataProviderLOIFeaturedGenomesHasChanged = true;
//		refreshListDataProviderMainListIfNedded();
//		refreshListDataProviderFeaturedGenomesIfNedded();
//		genomeResultListHasChanged = true;
//	}
	

	
	protected static void searchForOrganismsOrTaxons_featuredList(String searchTerm) {
		clearSearchFeatured(false, false);
		ArrayList<LightOrganismItem> alLoiAfterSearch = new ArrayList<>();
		for (int i = 0; i < alOrgaFeaturedList_filtredIn.size(); i++) { //alOrgaFeaturedList
			LightOrganismItem oiIT = alOrgaFeaturedList_filtredIn.get(i);//alOrgaFeaturedList
			if ( searchTerm.startsWith(oiIT.getFullName()) ) {
				alLoiAfterSearch.add(oiIT);
			}
		}
		if ( ! alLoiAfterSearch.isEmpty() ) {
			restrictFeaturedListOrganismsToSubsetOfOrganisms(alLoiAfterSearch);
		} else {
			TaxoItem tiExactPathToBranch = Insyght.APP_CONTROLER.findPathInTaxoTreeWithTextString(searchTerm);
			if (tiExactPathToBranch != null) {
				ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiExactPathToBranch.getFullPathInHierarchie());
				restrictFeaturedListOrganismsToSubsetOfOrganisms(alOiAfterSearch);
			} else {
				TaxoItem tiClosestPathInTree = Insyght.APP_CONTROLER.findClosestPathInTaxoTreeWithTextString(searchTerm);
				if (tiClosestPathInTree != null) {
					ArrayList<TaxoItem> alTaxoItemAndSubNodesWithPathInTree = Insyght.APP_CONTROLER.getListTaxoItemAndSubNodesWithPathInTree(tiClosestPathInTree.getFullPathInHierarchie());
					String approximateFound = "";
					if (alTaxoItemAndSubNodesWithPathInTree != null && ! alTaxoItemAndSubNodesWithPathInTree.isEmpty() ) {
						approximateFound = alTaxoItemAndSubNodesWithPathInTree.get(0).getName();
					}
					suggestBoxSearchFeatured.setText("CLOSEST MATCH FOR: "+searchTerm+ " IS: " +approximateFound);
					ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiClosestPathInTree.getFullPathInHierarchie());
					restrictFeaturedListOrganismsToSubsetOfOrganisms(alOiAfterSearch);
				} else {
					suggestBoxSearchFeatured.setText("NO MATCH FOR: "+searchTerm);
					listDataProviderLOIFeaturedListHasChanged = true;
					refreshListDataProviderFeaturedListIfNedded();
				}
			}
		}
	}
	
	protected static void searchForOrganismsOrTaxons_mainList(String searchTerm) {
		clearSearchMain(false, false);
		ArrayList<LightOrganismItem> alLoiAfterSearch = new ArrayList<>();
		for (int i = 0; i < alOrgaMainList_filtredIn.size(); i++) {
			LightOrganismItem oiIT = alOrgaMainList_filtredIn.get(i);
			if ( searchTerm.startsWith(oiIT.getFullName()) ) {
				alLoiAfterSearch.add(oiIT);
			}
		}
		if ( ! alLoiAfterSearch.isEmpty() ) {
			restrictMainListOrganismsToSubsetOfOrganisms(alLoiAfterSearch);
		} else {
			TaxoItem tiExactPathToBranch = Insyght.APP_CONTROLER.findPathInTaxoTreeWithTextString(searchTerm);
			if (tiExactPathToBranch != null) {
				ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiExactPathToBranch.getFullPathInHierarchie());
				restrictMainListOrganismsToSubsetOfOrganisms(alOiAfterSearch);

			} else {
				TaxoItem tiClosestPathInTree = Insyght.APP_CONTROLER.findClosestPathInTaxoTreeWithTextString(searchTerm);
				if (tiClosestPathInTree != null) {
					ArrayList<TaxoItem> alTaxoItemAndSubNodesWithPathInTree = Insyght.APP_CONTROLER.getListTaxoItemAndSubNodesWithPathInTree(tiClosestPathInTree.getFullPathInHierarchie());
					String approximateFound = "";
					if (alTaxoItemAndSubNodesWithPathInTree != null && ! alTaxoItemAndSubNodesWithPathInTree.isEmpty() ) {
						approximateFound = alTaxoItemAndSubNodesWithPathInTree.get(0).getName();
					}
					suggestBoxSearchMain.setText("CLOSEST MATCH FOR: "+searchTerm+ " IS: " +approximateFound);
					ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiClosestPathInTree.getFullPathInHierarchie());
					restrictMainListOrganismsToSubsetOfOrganisms(alOiAfterSearch);
				} else {
					suggestBoxSearchMain.setText("NO MATCH FOR: "+searchTerm);
					listDataProviderLOIMainListHasChanged = true;
					refreshListDataProviderMainListIfNedded();
				}
			}
		}
	}
	
//	protected void searchForOrganismsOrTaxons(String searchTerm) {
//		if ( ! alOrgaMainListFilteredOut.isEmpty()) {
//			clearSearchMain(false);
//		}
//		ArrayList<LightOrganismItem> alLoiAfterSearch = new ArrayList<>();
//		for (int i = 0; i < alOrgaMainList.size(); i++) {
//			LightOrganismItem oiIT = alOrgaMainList.get(i);
//			if ( searchTerm.startsWith(oiIT.getFullName()) ) {
//				alLoiAfterSearch.add(oiIT);
//			}
//		}
//		if ( ! alLoiAfterSearch.isEmpty() ) {
//			restrictMainListOrganismsToSubsetOfOrganisms(alLoiAfterSearch);
//		} else {
//			TaxoItem tiExactPathToBranch = Insyght.APP_CONTROLER.findPathInTaxoTreeWithTextString(searchTerm);
//			if (tiExactPathToBranch != null) {
//				ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiExactPathToBranch.getFullPathInHierarchie());
//				restrictMainListOrganismsToSubsetOfOrganisms(alOiAfterSearch);
//
//			} else {
//				TaxoItem tiClosestPathInTree = Insyght.APP_CONTROLER.findClosestPathInTaxoTreeWithTextString(searchTerm);
//				if (tiClosestPathInTree != null) {
//					ArrayList<TaxoItem> alTaxoItemAndSubNodesWithPathInTree = Insyght.APP_CONTROLER.getListTaxoItemAndSubNodesWithPathInTree(tiClosestPathInTree.getFullPathInHierarchie());
//					String approximateFound = "";
//					if (alTaxoItemAndSubNodesWithPathInTree != null && ! alTaxoItemAndSubNodesWithPathInTree.isEmpty() ) {
//						approximateFound = alTaxoItemAndSubNodesWithPathInTree.get(0).getName();
//					}
//					
//					suggestBoxSearchMain.setText("CLOSEST MATCH FOR: "+searchTerm+ " IS: " +approximateFound);
//					ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiClosestPathInTree.getFullPathInHierarchie());
//					restrictMainListOrganismsToSubsetOfOrganisms(alOiAfterSearch);
//				} else {
//					suggestBoxSearchMain.setText("NO MATCH FOR: "+searchTerm);
//					listDataProviderLOIMainListHasChanged = true;
//					refreshListDataProviderMainListIfNedded();
//				}
//			}
//		}
//	}
	
	private static void replaySavedMovesToChangePositionInMainList() {
		// TreeMap tmSavedMovesFinalPositionInAlInMainList2OrgaId
		// sort hashmap by value descending
		LinkedHashMap<Integer, Integer> lhmIT = UtilitiesMethodsShared.sortMapByIntegerValues(tmSavedMovesOrgaId2FinalPositionInMainList, false);
	    Iterator<Entry<Integer, Integer>> it = lhmIT.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>)it.next();
	        int orgaIdIT = pair.getKey();
	        int finalPositionInAlIT = pair.getValue();
	        for ( int i = 0 ; i < alOrgaMainList_filtredIn.size() ; i++ ) {
	        	LightOrganismItem loiIT = alOrgaMainList_filtredIn.get(i);
	        	if (loiIT.getOrganismId() == orgaIdIT) {
	        		// found it
	        		if (finalPositionInAlIT < 0) {
	        			finalPositionInAlIT = 0;
	        		}
	        		if (finalPositionInAlIT > alOrgaMainList_filtredIn.size()) {
	        			finalPositionInAlIT = alOrgaMainList_filtredIn.size();
	        		}
	        		alOrgaMainList_filtredIn.remove(loiIT);
	        		alOrgaMainList_filtredIn.add(finalPositionInAlIT, loiIT);
	        		break;
	        	}
	        }
	    }
	}
//	private void replaySavedMovesToChangePositionInMainList() {
//		// TreeMap tmSavedMovesFinalPositionInAlInMainList2OrgaId
//		// sort hashmap by value descending
//		LinkedHashMap<Integer, Integer> lhmIT = UtilitiesMethodsShared.sortMapByIntegerValues(tmSavedMovesOrgaId2FinalPositionInMainList, false);
//	    Iterator<Entry<Integer, Integer>> it = lhmIT.entrySet().iterator();
//	    while (it.hasNext()) {
//	        Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>)it.next();
//	        int orgaIdIT = pair.getKey();
//	        int finalPositionInAlIT = pair.getValue();
//	        for ( int i = 0 ; i < alOrgaMainList.size() ; i++ ) {
//	        	LightOrganismItem loiIT = alOrgaMainList.get(i);
//	        	if (loiIT.getOrganismId() == orgaIdIT) {
//	        		// found it
//	        		if (finalPositionInAlIT < 0) {
//	        			finalPositionInAlIT = 0;
//	        		}
//	        		if (finalPositionInAlIT > alOrgaMainList.size()) {
//	        			finalPositionInAlIT = alOrgaMainList.size();
//	        		}
//	        		alOrgaMainList.remove(loiIT);
//	        		alOrgaMainList.add(finalPositionInAlIT, loiIT);
//	        		break;
//	        	}
//	        }
//	    }
//	}



	private static void restrictMainListOrganismsToSubsetOfOrganisms(ArrayList<? extends LightOrganismItem> alLoiAfterSearch) {
		ArrayList<Integer> alIntOrgaIDIT = new ArrayList<>();
		for (LightOrganismItem loiIT : alLoiAfterSearch ) {
			alIntOrgaIDIT.add(loiIT.getOrganismId());
		}
		ListIterator<LightOrganismItem> iter = alOrgaMainList_filtredIn.listIterator();
		//int i=-1;
		while(iter.hasNext()){
			LightOrganismItem nextLOTIT = iter.next();
			//i++;
			if (alIntOrgaIDIT.contains(nextLOTIT.getOrganismId())) {
		    } else {
		    	alOrgaMainList_filtredOut.add(nextLOTIT);
		    	//alMapPositionToInsertInMainList.put(nextLOTIT.getOrganismId(), i);
		    	iter.remove();
		    }
		}
		listDataProviderLOIMainListHasChanged = true;
		refreshListDataProviderMainListIfNedded();
		genomeResultListHasChanged = true;
	}


	
	private static void restrictFeaturedListOrganismsToSubsetOfOrganisms(ArrayList<? extends LightOrganismItem> alLoiAfterSearch) {
		ArrayList<Integer> alIntOrgaIDIT = new ArrayList<>();
		for (LightOrganismItem loiIT : alLoiAfterSearch ) {
			alIntOrgaIDIT.add(loiIT.getOrganismId());
		}
		ListIterator<LightOrganismItem> iter = 
				//alOrgaFeaturedList
				//Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults()
				alOrgaFeaturedList_filtredIn.listIterator();
		//int i=-1;
		while(iter.hasNext()){
			LightOrganismItem nextLOTIT = iter.next();
			//i++;
			if (alIntOrgaIDIT.contains(nextLOTIT.getOrganismId())) {
		    } else {
		    	//Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome().add(nextLOTIT);
		    	//alMapPositionToInsertInMainList.put(nextLOTIT.getOrganismId(), i);
		    	alOrgaFeaturedList_filtredOut.add(nextLOTIT);
		    	iter.remove();
		    }
		}
		listDataProviderLOIFeaturedListHasChanged = true;
		refreshListDataProviderFeaturedListIfNedded();
		//genomeResultListHasChanged = true;
	}
//	private void restrictMainListOrganismsToSubsetOfOrganisms(ArrayList<? extends LightOrganismItem> alLoiAfterSearch) {
//		ArrayList<Integer> alIntOrgaIDIT = new ArrayList<>();
//		for (LightOrganismItem loiIT : alLoiAfterSearch ) {
//			alIntOrgaIDIT.add(loiIT.getOrganismId());
//		}
//		ListIterator<LightOrganismItem> iter = alOrgaMainList.listIterator();
//		//int i=-1;
//		while(iter.hasNext()){
//			LightOrganismItem nextLOTIT = iter.next();
//			//i++;
//			if (alIntOrgaIDIT.contains(nextLOTIT.getOrganismId())) {
//		    } else {
//		    	alOrgaMainListFilteredOut.add(nextLOTIT);
//		    	//alMapPositionToInsertInMainList.put(nextLOTIT.getOrganismId(), i);
//		    	iter.remove();
//		    }
//		}
//		listDataProviderLOIMainListHasChanged = true;
//		refreshListDataProviderMainListIfNedded();
//		genomeResultListHasChanged = true;
//	}
	
	private static void clearSearchFeatured(boolean refreshListDataProvider
			, boolean eraseTextInSuggestBox) {
		//alOrgaFeaturedList
		alOrgaFeaturedList_filtredIn.addAll(alOrgaFeaturedList_filtredOut);
		alOrgaFeaturedList_filtredOut.clear();
		//Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().addAll(Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome());
		Collections.sort(
				//alOrgaFeaturedList
				alOrgaFeaturedList_filtredIn
				, LightOrganismItemComparators.byPositionInResultSetComparator);
		//replaySavedMovesToChangePositionInFeaturedList();
		//Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome().clear();
		if (refreshListDataProvider) {
			listDataProviderLOIFeaturedListHasChanged = true;
			refreshListDataProviderFeaturedListIfNedded();
			//genomeResultListHasChanged = true;
		}
		if (eraseTextInSuggestBox) {
			suggestBoxSearchFeatured.setText("filter for organisms and taxons");
		}
	}

	private static void clearSearchMain(boolean refreshListDataProvider
			, boolean eraseTextInSuggestBox) {
		
		alOrgaMainList_filtredIn.addAll(alOrgaMainList_filtredOut);
		alOrgaMainList_filtredOut.clear();
		//alOrgaMainList.addAll(Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome());
		Collections.sort(alOrgaMainList_filtredIn, LightOrganismItemComparators.byPositionInResultSetComparator);
		replaySavedMovesToChangePositionInMainList();
		//Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome().clear();
		if (refreshListDataProvider) {
			listDataProviderLOIMainListHasChanged = true;
			refreshListDataProviderMainListIfNedded();
			genomeResultListHasChanged = true;
		}
		if (eraseTextInSuggestBox) {
			suggestBoxSearchMain.setText("filter for organisms and taxons");
		}
		
//		alOrgaMainList.addAll(alOrgaMainListFilteredOut);
//		Collections.sort(alOrgaMainList, LightOrganismItemComparators.byPositionInResultSetComparator);
//		replaySavedMovesToChangePositionInMainList();
//		alOrgaMainListFilteredOut.clear();
//		if (refreshListDataProvider) {
//			listDataProviderLOIMainListHasChanged = true;
//			refreshListDataProviderMainListIfNedded();
//			genomeResultListHasChanged = true;
//			suggestBoxSearchMain.setText("search for organisms and taxons");
//		}
	}
	
	
	public void displayPoPup(){
		genomeResultListHasChanged = false;
		sortOptionsHaveChanged = false;
		setCorrectVariablesDependingOnContext();
		listDataProviderLOIMainListHasChanged = true;
		listDataProviderLOIFeaturedListHasChanged = true;
		refreshListDataProviderMainListIfNedded();
		refreshListDataProviderFeaturedListIfNedded();
		setHeightWidthAndShowCenter();

	}
	
	
	private void setHeightWidthAndShowCenter() {
		int clientWidth = Window.getClientWidth();
		int clientHeight = Window.getClientHeight();
		dlpManageListComparedOrganisms.setHeight(((3 * clientHeight) / 4) + "px");
		dlpManageListComparedOrganisms.setWidth(((3 * clientWidth) / 4) + "px");
		center();
	}

	private void setCorrectVariablesDependingOnContext() {
		/*if ( MainTabPanelView.tabPanel.getSelectedIndex() == 1) {
			alOrgaMainList = alOrgaMainListForTab1;
			alOrgaFeaturedList = alOrgaFeaturedListForTab1;
			alOrgaMainListFilteredOut = alOrgaMainListFilteredOutForTab1;
		} else */
		if ( MainTabPanelView.tabPanel.getSelectedIndex() == 2 || MainTabPanelView.tabPanel.getSelectedIndex() == 4 ) {
			alOrgaMainList_filtredIn = new ArrayList<LightOrganismItem>(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult());
			alOrgaFeaturedList_filtredIn = new ArrayList<LightOrganismItem>(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults());
			//alOrgaMainListFilteredOut = alOrgaMainListFilteredOutForTab2And4;
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR ManageListComparedOrganisms setCorrectVariablesDependingOnContext : not recognized MainTabPanelView.tabPanel.getSelectedIndex() = "
					+ MainTabPanelView.tabPanel.getSelectedIndex()
					));
		}
	}


//	public static void refreshListDataProviderMainListIfNedded() {
//		if (listDataProviderLOIMainListHasChanged) {
//			ppMainList.getDisplay().setVisibleRange(0, 100);
//			ldpMainList.setList(alOrgaMainList);
//			listDataProviderLOIMainListHasChanged = false;
//		}
//	}
	public static void refreshListDataProviderMainListIfNedded() {
		if (listDataProviderLOIMainListHasChanged) {
			ppMainList.getDisplay().setVisibleRange(0, 100);
			ldpMainList.setList(alOrgaMainList_filtredIn);
			listDataProviderLOIMainListHasChanged = false;
			int cumSizeIT = alOrgaMainList_filtredIn.size() + alOrgaMainList_filtredOut.size();
			mainList_title.setHTML("Main list ("+alOrgaMainList_filtredIn.size()+" / "+cumSizeIT+"):");
		}
	}

	
	//FeaturedGenomes
//	public static void refreshListDataProviderFeaturedGenomesIfNedded() {
//		if (listDataProviderLOIFeaturedGenomesHasChanged) {
//			if ( ! alOrgaFeaturedList.isEmpty() ) {
//				ppFeaturedGenomes.getDisplay().setVisibleRange(0, alOrgaFeaturedList.size());
//			} else {
//				ppFeaturedGenomes.getDisplay().setVisibleRange(0, 100);
//			}
//			ldpFeaturedGenomes.setList(alOrgaFeaturedList);
//			listDataProviderLOIFeaturedGenomesHasChanged = false;
//		}
//	}
	public static void refreshListDataProviderFeaturedListIfNedded() {
		if (listDataProviderLOIFeaturedListHasChanged) {
			ppFeaturedList.getDisplay().setVisibleRange(0, 100);
			ldpFeaturedList.setList(
					//alOrgaFeaturedList
					alOrgaFeaturedList_filtredIn 
					);
			listDataProviderLOIFeaturedListHasChanged = false;
			int cumSizeIT = alOrgaFeaturedList_filtredIn.size() + alOrgaFeaturedList_filtredOut.size();
			featuredList_title.setHTML("Featured organisms ("+alOrgaFeaturedList_filtredIn.size() +" / "+cumSizeIT+"):");
		}
	}
	
	public static void setCorrectUISortScopeTypeButtons(boolean fireEvent) {
		// System.err.println("here");

		if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_scope().compareTo(
				Enum_comparedGenomes_SortResultListBy_scope.SelectedRefGene) == 0
			) {
			RB1_SORT_SCOPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(true);
		} else if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_scope().compareTo(
				Enum_comparedGenomes_SortResultListBy_scope.RefGeneSet) == 0
			) {
			RB1_SORT_SCOPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(false);
			RB1_SORT_TYPE_3.setEnabled(false);
		} else if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_scope().compareTo(
				Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism) == 0
			) {
			RB1_SORT_SCOPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(false);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR setCorrectUISortScopeTypeButtons unrecognized getSortResultListBy_scope : "
					+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getSortResultListBy_scope().toString()));
		}
		

		if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortType().compareTo(
				Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs) == 0
			) {
			RB1_SORT_TYPE_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortType().compareTo(
				Enum_comparedGenomes_SortResultListBy_sortType.SyntenyScore) == 0
			) {
			RB1_SORT_TYPE_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortType().compareTo(
				Enum_comparedGenomes_SortResultListBy_sortType.AlignemntScore) == 0
			) {
			RB1_SORT_TYPE_2.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortType().compareTo(
				Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologsAnnotations) == 0
			) {
			RB1_SORT_TYPE_3.setValue(true, fireEvent);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR setCorrectUISortScopeTypeButtons unrecognized getSortResultListBy_sortType : "
					+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getSortResultListBy_sortType().toString()));
		}
		

		if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortOrder().compareTo(
				Enum_comparedGenomes_SortResultListBy_sortOrder.Descending) == 0
			) {
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortOrder().compareTo(
				Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending) == 0
			) {
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR setCorrectUISortScopeTypeButtons unrecognized getSortResultListBy_sortOrder : "
					+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getSortResultListBy_sortOrder().toString()));
		}
		
		/*
		if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getResultListSortScopeType()
				.compareTo(SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC
				) == 0
			) {
			RB1_SORT_SCOPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(true);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0) {
			RB1_SORT_SCOPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(true);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0) {
			RB1_SORT_SCOPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(true);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0) {
			RB1_SORT_SCOPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(true);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0) {
			RB1_SORT_SCOPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(true);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0) {
			RB1_SORT_SCOPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(true);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGSANNOTATIONS_ORDER_ASC) == 0) {
			RB1_SORT_SCOPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_3.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(true);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGSANNOTATIONS_ORDER_DESC) == 0) {
			RB1_SORT_SCOPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_3.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(true);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0) {
			RB1_SORT_SCOPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(false);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0) {
			RB1_SORT_SCOPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(false);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0) {
			RB1_SORT_SCOPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(false);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0) {
			RB1_SORT_SCOPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(false);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0) {
			RB1_SORT_SCOPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(false);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0) {
			RB1_SORT_SCOPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(false);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC) == 0) {
			// System.err.println("not ok");
			RB1_SORT_SCOPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC) == 0) {
			// System.err.println("not ok");
			RB1_SORT_SCOPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_0.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_ASC) == 0) {
			// System.err.println("ok");
			RB1_SORT_SCOPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_DESC) == 0) {
			// System.err.println("ok");
			RB1_SORT_SCOPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_1.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setEnabled(true);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC) == 0) {
			RB1_SORT_SCOPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setValue(true, fireEvent);
			//RB1_SORT_TYPE_2.setEnabled(false);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_1.setValue(true, fireEvent);
		} else if (Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getResultListSortScopeType()
				.compareTo(
						SearchItem.EnumResultListSortScopeType.SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC) == 0) {
			RB1_SORT_SCOPE_2.setValue(true, fireEvent);
			RB1_SORT_TYPE_2.setValue(true, fireEvent);
			//RB1_SORT_TYPE_2.setEnabled(false);
			RB1_SORT_TYPE_3.setEnabled(false);
			RB1_SORT_ORDER_0.setValue(true, fireEvent);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR setCorrectUISortScopeTypeButtons unrecognized getResultListSortScopeType : "
					+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getResultListSortScopeType().toString()));
		}*/
	}


	void applyUserOptionsToExistingResultAndLaunchUpdateDisplay() {
		GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage(); // before setting up new LstOrgaResult and ListUserSelectedOrgaToBeResults
		if (genomeResultListHasChanged && sortOptionsHaveChanged) {
			ArrayList<LightOrganismItem> alOrgaMainList = new ArrayList<LightOrganismItem>();
			alOrgaMainList.addAll(alOrgaMainList_filtredIn);
			alOrgaMainList.addAll(alOrgaMainList_filtredOut);
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().setLstOrgaResult(alOrgaMainList);
			ArrayList<LightOrganismItem> alOrgaFeaturedList = new ArrayList<LightOrganismItem>();
			alOrgaFeaturedList.addAll(alOrgaFeaturedList_filtredIn);
			alOrgaFeaturedList.addAll(alOrgaFeaturedList_filtredOut);
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().setListUserSelectedOrgaToBeResults(alOrgaFeaturedList);
			sortComparedOrgaListsByAccordingToUserOptions();
		} else if (genomeResultListHasChanged) {
			ArrayList<LightOrganismItem> alOrgaMainList = new ArrayList<LightOrganismItem>();
			alOrgaMainList.addAll(alOrgaMainList_filtredIn);
			alOrgaMainList.addAll(alOrgaMainList_filtredOut);
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().setLstOrgaResult(alOrgaMainList);
			ArrayList<LightOrganismItem> alOrgaFeaturedList = new ArrayList<LightOrganismItem>();
			alOrgaFeaturedList.addAll(alOrgaFeaturedList_filtredIn);
			alOrgaFeaturedList.addAll(alOrgaFeaturedList_filtredOut);
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().setListUserSelectedOrgaToBeResults(alOrgaFeaturedList);
			Insyght.APP_CONTROLER.resetAllCurrentListResultGenomePanelItem();
			GenoOrgaAndHomoTablViewImpl.setResultsForRefGenomePanelAndListOrgaResultReturned();
			CenterSLPAllGenomePanels.updateResultsDisplay(true, true, true);
			GenoOrgaAndHomoTablViewImpl.stackPWest.showWidget(1);
			NavigationControler.newItem(NavigationControler.getURLTockenFromCurrentState(), false);
		} else if (sortOptionsHaveChanged) {
			sortComparedOrgaListsByAccordingToUserOptions();
		}
	}
	
	
	void sortComparedOrgaListsByAccordingToUserOptions(
			//boolean updateMainListOnly
			//, boolean doCleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage
			) {

		SearchItem siToSend = new SearchItem();
		siToSend.setListExcludedGenome(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListExcludedGenome());
		ArrayList<LightGeneItem> alLGIIT = new ArrayList<LightGeneItem>();
		for(Integer geneId : Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable() ){
			LightGeneItem lgiIT = new LightGeneItem();
			lgiIT.setGeneId(geneId);
			alLGIIT.add(lgiIT);
		}
		siToSend.setListReferenceGeneSet(alLGIIT);
		siToSend.setListUserSelectedOrgaToBeResults(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults());
		OrganismItem leiIt = new OrganismItem();
		leiIt.setOrganismId(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId());		
		siToSend.setReferenceOrganism(leiIt);
		siToSend.setViewTypeDesired(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
		
		
		if ( RB1_SORT_SCOPE_0.getValue() ) {
			siToSend.setSortResultListBy_scope(Enum_comparedGenomes_SortResultListBy_scope.SelectedRefGene);
		} else if ( RB1_SORT_SCOPE_1.getValue() ) {
			siToSend.setSortResultListBy_scope(Enum_comparedGenomes_SortResultListBy_scope.RefGeneSet);
		} else if ( RB1_SORT_SCOPE_2.getValue() ) {
			siToSend.setSortResultListBy_scope(Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR sortComparedOrgaListsByAccordingToUserOptions unrecognized RB1_SORT_SCOPE"));
		}
		
		if (RB1_SORT_TYPE_0.getValue()) {
			siToSend.setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs);
		} else if (RB1_SORT_TYPE_1.getValue()) {
			siToSend.setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType.SyntenyScore);
		} else if (RB1_SORT_TYPE_2.getValue()) {
			siToSend.setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType.AlignemntScore);
		} else if (RB1_SORT_TYPE_3.getValue()) {
			siToSend.setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologsAnnotations);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR sortComparedOrgaListsByAccordingToUserOptions unrecognized RB1_SORT_TYPE"));
		}
		
		if (RB1_SORT_ORDER_0.getValue()) {
			siToSend.setSortResultListBy_sortOrder(Enum_comparedGenomes_SortResultListBy_sortOrder.Descending);
		} else if (RB1_SORT_ORDER_1.getValue()) {
			siToSend.setSortResultListBy_sortOrder(Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR sortComparedOrgaListsByAccordingToUserOptions unrecognized RB1_SORT_ORDER"));
		}
				
		@SuppressWarnings("unused")
		GetResultDialog resDiag = new GetResultDialog(siToSend, true);
		
	}


	//"Find / feature genomes"
	//"Sort result list by"
	private void selectMenu(String selectedMenu) {
		if ( selectedMenu.compareTo("Find / feature genomes") == 0 ) {
			DP_MAIN_MENU.showWidget(0);
			MenuItems_FindFeatureGenomes.setStyleName("HTMLMenuItems_selected");
			MenuItems_SortResultListBy.setStyleName("HTMLMenuItems_notSelected");
		} else if ( selectedMenu.compareTo("Sort result list by") == 0 ) {
			DP_MAIN_MENU.showWidget(1);
			MenuItems_FindFeatureGenomes.setStyleName("HTMLMenuItems_notSelected");
			MenuItems_SortResultListBy.setStyleName("HTMLMenuItems_selected");
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
					new Exception("Error in ManageListComparedOrganisms selectMenu : selectedMenu not supported : "+selectedMenu)
			);
		}
	}
	
	
}


