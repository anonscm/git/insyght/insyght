package fr.inra.jouy.client.view.search;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.CompositeCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.HasCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.dom.client.Element;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.TreeViewModel;

import fr.inra.jouy.shared.pojos.databaseMapping.TaxoItem;

public class CustomTreeModelCoreDispGenome implements TreeViewModel {

	private final DefaultSelectionEventManager<TaxoItem> selectionManager = DefaultSelectionEventManager.createCheckboxManager();
	private SelectionModel<TaxoItem> selectionModel = null;

//	private Cell<TaxoItem> cellTaxoItem = new AbstractCell<TaxoItem>(
//			BrowserEvents.CLICK, BrowserEvents.DBLCLICK) {
//		@Override
//		public void render(Context context, TaxoItem value,
//				SafeHtmlBuilder sb) {
//			if (value != null) {
//				String postFix = "";
//				if (value.getName().compareTo("Unknown lineage") == 0) {
//					postFix += " (" + value.getTaxoChildrens().size() + ")";
//				} else if (!value.getTaxoChildrens().isEmpty()) {
//					postFix += " (" + value.getSumOfLeafsUnderneath() + ")";
//				}
//				if (context.getIndex() == 0
//						&& (value.getName().compareTo("Domain") == 0
//								|| value.getName().compareTo("Phylum") == 0
//								|| value.getName().compareTo("Class") == 0
//								|| value.getName().compareTo("Order") == 0
//								|| value.getName().compareTo("Family") == 0
//								|| value.getName().compareTo("Genus") == 0 || value
//								.getName().compareTo("Species") == 0)) {
//
//					String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
//							+ value.getFullName()
//							+ postFix
//							+ "</center></span>";
//					sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
//				} else {
//					
//					if(value.equals(SearchViewImpl.selectedRefTaxoNode)){
//						//normal black bold
//						sb.append(SafeHtmlUtils.fromTrustedString("<b><big><center>Reference taxonomic node : " + value.getFullName()
//								+ postFix + "</big></center></b>"));
//					}else if(SearchViewImpl.geneSetBuildMode == 0){
//						if(value.isRefGenomeInRefNodeContext()){
//							//background-color:#FFF380;color:#000000;	
//							sb.append(SafeHtmlUtils.fromTrustedString("<span style=\"background-color:#FFF380;color:#000000;\">" + value.getFullName()
//									+ postFix + "</span>"));
//						}else if(value.isAssertPresenceInRefNodeContext()){
//							//background-color:#ECF1EF;color:#00008B;
//							sb.append(SafeHtmlUtils.fromTrustedString("<span style=\"background-color:#BCD2EE;color:#00008B;\">" + value.getFullName()
//									+ postFix + "</span>"));
//						}else if(value.isAssertAbsenceInRefNodeContext()){
//							//background-color:#FEE0C6;color:#CD0000;
//							sb.append(SafeHtmlUtils.fromTrustedString("<span style=\"background-color:#FFA07A;color:#8B0000;\">" + value.getFullName()
//									+ postFix + "</span>"));
//						}else if(value.isAssertEitherPresenceOrAbsenceInRefNodeContext()){
//							//background-color:#F8F8FF;color:#8B8378;
//							sb.append(SafeHtmlUtils.fromTrustedString(
//									"<span style=\"background-color:#ffe6eb;color:#8B0A50;\">" + 
//									value.getFullName()
//									+ postFix 
//									+ "</span>"
//									));
//						}else{
//							//normal black
//							sb.append(SafeHtmlUtils.fromTrustedString(value.getFullName()
//									+ postFix));
//						}
//					}else{
//						if(value.isRefGenomeInRefNodeContext()){
//							//background-color:#FFF380;color:#000000;	
//							sb.append(SafeHtmlUtils.fromTrustedString("<span style=\"background-color:#FFF380;color:#000000;\">" + value.getFullName()
//									+ postFix + "</span>"));
//						}else{
//							//normal black
//							sb.append(SafeHtmlUtils.fromTrustedString(value.getFullName()
//									+ postFix));
//						}
//					}
//					
//				}
//
//			}
//		}
//		
//		@Override
//		public void onBrowserEvent(Context context, Element parent,
//				TaxoItem value,
//				NativeEvent event,
//				ValueUpdater<TaxoItem> valueUpdater) {
//			
//			super.onBrowserEvent(context, parent, value, event,valueUpdater);
//
//			//System.err.println("here = "+Event.getTypeInt(event.getType()));
//			
//			if (Event.getTypeInt(event.getType()) == Event.ONDBLCLICK) {
//				doubleClickOnSpecialTaxoBrowser(event);
//			}else if (Event.getTypeInt(event.getType()) == Event.ONCLICK) {
//				if(event.getClientY() == SearchViewImpl.savedTaxoBrowserMouseY
//						&& event.getClientX() == SearchViewImpl.savedTaxoBrowserMouseX
//						&&  (Duration.currentTimeMillis() - SearchViewImpl.savedTaxoBrowserTimeClick) < 500){
//					//double click
//					doubleClickOnSpecialTaxoBrowser(event);
//				}else{
//					SearchViewImpl.savedTaxoBrowserMouseY = event.getClientY();
//					SearchViewImpl.savedTaxoBrowserMouseX = event.getClientX();
//					SearchViewImpl.savedTaxoBrowserTimeClick = Duration.currentTimeMillis();
//					
//				}
//			}
//		}		
//	};
	
	private final Cell<TaxoItem> contactCell;
	public CustomTreeModelCoreDispGenome(final SelectionModel<TaxoItem> selectionModel) {
		this.selectionModel = selectionModel;
		// Construct a composite cell for contacts that includes a checkbox.
	    List<HasCell<TaxoItem, ?>> hasCells = new ArrayList<HasCell<TaxoItem, ?>>();
	    hasCells.add(new HasCell<TaxoItem, Boolean>() {
	      private CheckboxCell cell = new CheckboxCell(true, false);
	      public Cell<Boolean> getCell() {
	        return cell;
	      }
	      public FieldUpdater<TaxoItem, Boolean> getFieldUpdater() {
	        return null;
	      }
	      public Boolean getValue(TaxoItem object) {
	        return selectionModel.isSelected(object);
	      }
	    });
	    hasCells.add(new HasCell<TaxoItem, TaxoItem>() {
	      private CellTaxoItem cell = new CellTaxoItem();
	      public Cell<TaxoItem> getCell() {
	        return cell;
	      }
	      public FieldUpdater<TaxoItem, TaxoItem> getFieldUpdater() {
	        return null;
	      }
	      public TaxoItem getValue(TaxoItem object) {
	        return object;
	      }
	    });
	    contactCell = new CompositeCell<TaxoItem>(hasCells) {
	      @Override
	      public void render(Context context, TaxoItem value, SafeHtmlBuilder sb) {
	        sb.appendHtmlConstant("<table><tbody><tr>");
	        super.render(context, value, sb);
	        sb.appendHtmlConstant("</tr></tbody></table>");
	      }
	      @Override
	      protected Element getContainerElement(Element parent) {
	        // Return the first TR element in the table.
	        return parent.getFirstChildElement().getFirstChildElement().getFirstChildElement();
	      }
	      @Override
	      protected <X> void render(Context context, TaxoItem value,
	          SafeHtmlBuilder sb, HasCell<TaxoItem, X> hasCell) {
	        Cell<X> cell = hasCell.getCell();
	        sb.appendHtmlConstant("<td>");
	        cell.render(context, hasCell.getValue(value), sb);
	        sb.appendHtmlConstant("</td>");
	      }
	    };
	}
	
//	protected void doubleClickOnSpecialTaxoBrowser(NativeEvent event) {
//		TaxoItem tiIT = SearchViewImpl.getTheFurthestNode(SearchViewImpl.refNodeTaxoBrowserSelectionModel.getSelectedObject(), false);
//		//enable/disable
//		if (tiIT.getAssociatedOrganismItem() != null
//				&& !tiIT.isRefGenomeInRefNodeContext()) {
//			SearchViewImpl.myPopupMenuTaxoBrowser.setAsReferenceOrganism.setEnabled(true);
//		} else {
//			SearchViewImpl.myPopupMenuTaxoBrowser.setAsReferenceOrganism.setEnabled(false);
//		}
//		if(SearchViewImpl.geneSetBuildMode == 0){
//			//core / disp genome
//			if(tiIT.isRefGenomeInRefNodeContext()){
//				SearchViewImpl.myPopupMenuTaxoBrowser.setNodeAndSubnodesToCategories.setEnabled(false);
//			}else{
//				SearchViewImpl.myPopupMenuTaxoBrowser.setNodeAndSubnodesToCategories.setEnabled(true);
//			}
//		}else{
//			SearchViewImpl.myPopupMenuTaxoBrowser.setNodeAndSubnodesToCategories.setEnabled(false);
//		}
//		
//		final int eventClientX = event.getClientX();
//		final int eventClientY = event.getClientY();
//		SearchViewImpl.myPopupMenuTaxoBrowser.setPopupPositionAndShow(new PositionCallback() {
//			public void setPosition(int offsetWidth,
//					int offsetHeight) {
//				int left = eventClientX
//						- (offsetWidth / 2);
//				int top = eventClientY - offsetHeight - 3;
//				SearchViewImpl.myPopupMenuTaxoBrowser.setPopupPosition(left, top);
//			}
//		});
//	}
	
	@Override
	public <T> NodeInfo<?> getNodeInfo(T value) {
		
		if (value == null) {
			// null = error.
			// Create a data provider that says loading.
			final ArrayList<String> listLoading = new ArrayList<String>();
			listLoading.add("Taxonomic tree unavailable.");
			ListDataProvider<String> dataProvider = new ListDataProvider<String>(
					listLoading);
			return new DefaultNodeInfo<String>(dataProvider, new TextCell()
			// ,orgaBrowserPanelSelectionModelNotLeaf, null
			);

		} else if (value instanceof TaxoItem) {
			// TaxoItem.
			ListDataProvider<TaxoItem> dataProvider = new ListDataProvider<TaxoItem>(
					((TaxoItem) value).getTaxoChildrens());

			// Return a node info that pairs the data provider and the cell.
			return new DefaultNodeInfo<TaxoItem>(dataProvider,
					contactCell, selectionModel, selectionManager, null);

		}
		// Unhandled type.
	    String type = value.getClass().getName();
	    throw new IllegalArgumentException("Unsupported object type: " + type);
		
	}

	@Override
	public boolean isLeaf(Object value) {
		
		if (value instanceof String) {
			return true;
		}
		// The leaf nodes are the TaxoItem.
		if (value instanceof TaxoItem) {
			if (((TaxoItem) value).getTaxoChildrens().isEmpty()) {
				return true;
			}
		}
		return false;
	}

}
