package fr.inra.jouy.client.view.result;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellBrowser;
import com.google.gwt.user.cellview.client.CellBrowser.Builder;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ProvidesResize;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.PopupPanel.PositionCallback;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
//import com.google.gwt.view.client.SelectionChangeEvent;
//import com.google.gwt.view.client.SingleSelectionModel;
import com.google.gwt.view.client.TreeViewModel;
//import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.TreeViewModel.NodeInfo;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.RPC.CallForHomoBrowResu;
import fr.inra.jouy.client.RPC.CallForHomoBrowResuAsync;
import fr.inra.jouy.client.RPC.CallForInfoDB;
import fr.inra.jouy.client.RPC.CallForInfoDBAsync;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotCategoryCompa;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotClasses;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotComparedGene;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotNames;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotOrganisms;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotRefGenes;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotRoot;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;

public class CompareHomologsAnnot extends Composite implements ProvidesResize,
		RequiresResize {

	private static CompareHomologsAnnotUiBinder uiBinder = GWT
			.create(CompareHomologsAnnotUiBinder.class);

	interface CompareHomologsAnnotUiBinder extends
			UiBinder<Widget, CompareHomologsAnnot> {
	}

	private final static CallForInfoDBAsync callForInfoDBService = (CallForInfoDBAsync) GWT
			.create(CallForInfoDB.class);

	private final static CallForHomoBrowResuAsync homologieBrowsingResultService = (CallForHomoBrowResuAsync) GWT
			.create(CallForHomoBrowResu.class);

	@UiField
	static HTML selectedOrganism;
	@UiField
	static VerticalPanel orgaBrowserPanelCompaHomo;
	private final static SingleSelectionModel<CompaAnnotRefGenes> orgaBrowserPanelCompaAnnotRefGenesSelectionModel = new SingleSelectionModel<CompaAnnotRefGenes>();
	private static TreeViewModel model = new CustomTreeModel();
	private static CellBrowser cellBrowserCompaHomo;
	@UiField static DockLayoutPanel dlpRoot;
	static ListDataProvider<CompaAnnotRefGenes> compaAnnotRefGenesDataProvider;

	private static boolean doUpdateDetailsRefGeneSelected = false;

	private static ResultLoadingDialog RLD = new ResultLoadingDialog();
	
	public CompareHomologsAnnot() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void onResize() {
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			// @Override
			public void execute() {
				if (MainTabPanelView.tabPanel.getSelectedIndex() == 3) {
					cellBrowserCompaHomo.setHeight((dlpRoot.getOffsetHeight() - dlpRoot
							.getWidget(0).getOffsetHeight()) + "px");
				} else {
					MainTabPanelView.postponeResizeOfAnnotCompa = true;
				}
				AnnotCompaViewImpl.alreadyResizing = false;
				// System.err.println("here="+(dlpRoot.getOffsetHeight()-dlpRoot.getWidget(0).getOffsetHeight()));
			}
		});

	}

	public static void getComparisonHomologAnnotation() {

		AsyncCallback<CompaAnnotRoot> callback = new AsyncCallback<CompaAnnotRoot>() {
			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				//caught.printStackTrace();
			}

			public void onSuccess(final CompaAnnotRoot compaAnnotRootSent) {
				// //remake refAlGeneId to be safe
				// if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForAnnotationsComparator()
				// == null){
				// Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().setListReferenceGeneSetForAnnotationsComparator(new
				// ArrayList<Integer>());
				// }
				// Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForAnnotationsComparator().clear();
				// for(CompaAnnotRefGenes cargIT :
				// compaAnnotRootSent.getChildrens()){
				// Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForAnnotationsComparator().add(cargIT.getGeneId());
				// }
				
				buildCompaAnnotBrowser(compaAnnotRootSent);
				
			}
		};

		try {

			// set loading
			CompaAnnotRefGenes loadingNodeCompaAnnot = new CompaAnnotRefGenes();
			loadingNodeCompaAnnot.setTextAkaGeneIdentifier("Loading...");
			CompaAnnotRoot preRootNodeCompaAnnot = new CompaAnnotRoot();
			preRootNodeCompaAnnot.getChildrens().add(loadingNodeCompaAnnot);
			buildCompaAnnotBrowser(preRootNodeCompaAnnot);

			homologieBrowsingResultService.getCompaAnnotRootWithAlGeneId(
					Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getRefGeneSetForAnnotationsComparator(),
					// .getListReferenceGeneSetForAnnotationsComparator(),
					// AnnotCompaViewImpl.listElementIdsThenStartPbthenStopPBLooped,
					callback);

		} catch (Exception e) {
			//String mssgError = "ERROR in getComparisonHomologAnnotation : "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//e.printStackTrace();
		}
	}

	protected static void buildCompaAnnotBrowser(
			CompaAnnotRoot compaAnnotPreRootSent) {

		orgaBrowserPanelCompaAnnotRefGenesSelectionModel
				.addSelectionChangeHandler(new Handler() {
					@Override
					public void onSelectionChange(SelectionChangeEvent event) {
						AnnotCompaViewImpl.currClickedRefGeneId = orgaBrowserPanelCompaAnnotRefGenesSelectionModel
								.getSelectedObject().getGeneId();

						try {
							AnnotCompaViewImpl.basicInfoGene
									.setHTML("Reference gene:<br/>"
											+ orgaBrowserPanelCompaAnnotRefGenesSelectionModel
													.getSelectedObject()
													.getTextAkaGeneIdentifier());
							
//							@SuppressWarnings("unused")
//							ResultLoadingDialog rld = new ResultLoadingDialog(
//									DetailledInfoStack.DetailledGeneInfoRef,
//									null,
//									AnnotCompaViewImpl.currClickedRefGeneId,
//									Insyght.APP_CONTROLER
//											.getCurrentRefGenomePanelAndListOrgaResult()
//											.getViewTypeInsyght()
//									);
							RLD.getDetailledGeneInfo(
									null,
									AnnotCompaViewImpl.currClickedRefGeneId, false, Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght(), true
									);
							
//							@SuppressWarnings("unused")
//							ResultLoadingDialog rldEletBas = new ResultLoadingDialog(
//									BasicInfoStack.BasicQElement,
//									null,
//									AnnotCompaViewImpl.currClickedRefGeneId,
//									Insyght.APP_CONTROLER
//											.getCurrentRefGenomePanelAndListOrgaResult()
//											.getViewTypeInsyght()
//									);
							RLD.getBasicElementInfoWithGeneId(null,
									AnnotCompaViewImpl.currClickedRefGeneId, true, Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght());
							
//							@SuppressWarnings("unused")
//							ResultLoadingDialog rldEletDet = new ResultLoadingDialog(
//									DetailledInfoStack.DetailledQElement,
//									null,
//									AnnotCompaViewImpl.currClickedRefGeneId,
//									Insyght.APP_CONTROLER
//											.getCurrentRefGenomePanelAndListOrgaResult()
//											.getViewTypeInsyght()
//									);
							RLD.getDetailledElementInfo(null,
									AnnotCompaViewImpl.currClickedRefGeneId, true, Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght());
							
							
							
						} catch (Exception e) {
							//String mssgError = "ERROR in showdetailledGeneInfo: "+ e.getMessage();
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
							AnnotCompaViewImpl.vpInfoGene.clear();
							AnnotCompaViewImpl.vpInfoGene
									.add(new HTML(
											"<ul><li><b>ERROR detailledGeneInfo</b></li></ul>"));
						}
					}
				});

		/*
		 * Create the browser using the model.
		 */
		Builder<CompaAnnotRoot> builder = new Builder<CompaAnnotRoot>(model,
				compaAnnotPreRootSent);
		builder.loadingIndicator(new HTML(
				"<b><big>Loading, please wait...</b></big>"));
		cellBrowserCompaHomo = builder.build();
		cellBrowserCompaHomo.setAnimationEnabled(true);
		cellBrowserCompaHomo
				.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		cellBrowserCompaHomo.setMinimumColumnWidth(200);
		cellBrowserCompaHomo.setWidth("100%");// "100%"
		// cellBrowserCompaHomo.setHeight("100%");//20em
		cellBrowserCompaHomo.setHeight((orgaBrowserPanelCompaHomo
				.getOffsetHeight()) + "px");
		cellBrowserCompaHomo.setHeight((dlpRoot.getOffsetHeight() - dlpRoot
				.getWidget(0).getOffsetHeight()) + "px");
		orgaBrowserPanelCompaHomo.clear();
		orgaBrowserPanelCompaHomo.add(cellBrowserCompaHomo);

		AnnotCompaViewImpl.clearDetailsRefGeneSelected();

	}

	private static class CompaAnnotForSpecificGeneDataProvider extends
			AsyncDataProvider<CompaAnnotCategoryCompa> {

		Integer selectedRefGenesId = -1;

		public CompaAnnotForSpecificGeneDataProvider(CompaAnnotRefGenes value) {
			selectedRefGenesId = value.getGeneId();
		}

		/**
		 * {@link #onRangeChanged(HasData)} is called when the table requests a
		 * new range of data. You can push data back to the displays using
		 * {@link #updateRowData(int, List)}.
		 */
		@Override
		protected void onRangeChanged(HasData<CompaAnnotCategoryCompa> display) {
			// Get the new range.
			final Range range = display.getVisibleRange();
			/*
			 * Query the data asynchronously. If you are using a database, you
			 * can make an RPC call here.
			 */
			AsyncCallback<ArrayList<CompaAnnotCategoryCompa>> callback = new AsyncCallback<ArrayList<CompaAnnotCategoryCompa>>() {
				public void onFailure(Throwable caught) {
					// do some UI stuff to show failure
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
					//caught.printStackTrace();
				}

				public void onSuccess(ArrayList<CompaAnnotCategoryCompa> newData) {

					if (newData == null) {
						newData = new ArrayList<CompaAnnotCategoryCompa>();
						// //set no results
						CompaAnnotCategoryCompa headerCompaAnnotCategoryCompa = new CompaAnnotCategoryCompa();
						headerCompaAnnotCategoryCompa
								.setTextAkaCategoryOfCompa("No result");
						newData.add(headerCompaAnnotCategoryCompa);
					} else if (newData.isEmpty()) {
						// //set no results
						CompaAnnotCategoryCompa headerCompaAnnotCategoryCompa = new CompaAnnotCategoryCompa();
						headerCompaAnnotCategoryCompa
								.setTextAkaCategoryOfCompa("No result");
						newData.add(headerCompaAnnotCategoryCompa);
					}

					// //set header
					// CompaAnnotCategoryCompa headerCompaAnnotCategoryCompa =
					// new CompaAnnotCategoryCompa();
					// headerCompaAnnotCategoryCompa.setTextAkaCategoryOfCompa("Comparison categories");
					// newData.add(0, headerCompaAnnotCategoryCompa);
					updateRowData(range.getStart(), newData);
				}
			};

			try {

				Double paramMaxEvalueToSend = AnnotCompaViewImpl
						.getParamMaxEvalueToSend();
				if (paramMaxEvalueToSend == -1.0) {
					ArrayList<CompaAnnotCategoryCompa> errorData = new ArrayList<CompaAnnotCategoryCompa>();
					CompaAnnotCategoryCompa cacErrorIT = new CompaAnnotCategoryCompa();
					cacErrorIT
							.setTextAkaCategoryOfCompa("Error Invalid parameter : Homologs max Evalue");
					errorData.add(cacErrorIT);
					updateRowData(range.getStart(), errorData);
					return;
				}

				int paramMinPercentIdentityToSend = AnnotCompaViewImpl
						.getParamMinPercentIdentityToSend();
				if (paramMinPercentIdentityToSend == -1) {
					ArrayList<CompaAnnotCategoryCompa> errorData = new ArrayList<CompaAnnotCategoryCompa>();
					CompaAnnotCategoryCompa cacErrorIT = new CompaAnnotCategoryCompa();
					cacErrorIT
							.setTextAkaCategoryOfCompa("Error Invalid parameter : Homologs min % identity");
					errorData.add(cacErrorIT);
					updateRowData(range.getStart(), errorData);
					return;
				}

				int paramMinPercentAlignLenghtToSend = AnnotCompaViewImpl
						.getParamMinPercentAlignLenghtToSend();
				if (paramMinPercentAlignLenghtToSend == -1) {
					ArrayList<CompaAnnotCategoryCompa> errorData = new ArrayList<CompaAnnotCategoryCompa>();
					CompaAnnotCategoryCompa cacErrorIT = new CompaAnnotCategoryCompa();
					cacErrorIT
							.setTextAkaCategoryOfCompa("Error Invalid parameter : Homologs min % alignment length");
					errorData.add(cacErrorIT);
					updateRowData(range.getStart(), errorData);
					return;
				}

				boolean paramBdbhOnlyToSend = AnnotCompaViewImpl
						.getParamBdbhOnlyToSend();

				ArrayList<Integer> alOrgaIdsToSend = new ArrayList<Integer>();
				// if (AnnotCompaViewImpl.assumeAllOrgaRoot) {
				// // do nothing
				// } else
				if (AnnotCompaViewImpl.listComparedOrgaToSend == null) {
					// errorParamMssg.setText("Error Invalid parameter : compared set of organisms");
					ArrayList<CompaAnnotCategoryCompa> errorData = new ArrayList<CompaAnnotCategoryCompa>();
					CompaAnnotCategoryCompa cacErrorIT = new CompaAnnotCategoryCompa();
					cacErrorIT
							.setTextAkaCategoryOfCompa("Error Invalid parameter : compared set of organisms");
					errorData.add(cacErrorIT);
					updateRowData(range.getStart(), errorData);
					return;
				} else {
					for (int i = 0; i < AnnotCompaViewImpl.listComparedOrgaToSend
							.size(); i++) {
						OrganismItem oiIT = AnnotCompaViewImpl.listComparedOrgaToSend
								.get(i);
						alOrgaIdsToSend.add(oiIT.getOrganismId());
					}
				}

				homologieBrowsingResultService
						.getCompaAnnotRefGenesWithGeneIdAndParams(
								selectedRefGenesId, paramMaxEvalueToSend,
								paramMinPercentIdentityToSend,
								paramMinPercentAlignLenghtToSend,
								alOrgaIdsToSend, paramBdbhOnlyToSend, callback);

			} catch (Exception e) {
				//String mssgError = "ERROR in CompaAnnotForSpecificGeneDataProvider : "+ e.getMessage();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
				//e.printStackTrace();
			}
		}
	}

	private static class GeneBasicInfoDataProvider extends
			AsyncDataProvider<CompaAnnotComparedGene> {
		ArrayList<Integer> listGenesIds = null;

		public GeneBasicInfoDataProvider(CompaAnnotOrganisms value) {
			listGenesIds = value.getChildrensAkaGeneIds();
		}

		/**
		 * {@link #onRangeChanged(HasData)} is called when the table requests a
		 * new range of data. You can push data back to the displays using
		 * {@link #updateRowData(int, List)}.
		 */
		@Override
		protected void onRangeChanged(HasData<CompaAnnotComparedGene> display) {
			// Get the new range.
			final Range range = display.getVisibleRange();

			/*
			 * Query the data asynchronously. If you are using a database, you
			 * can make an RPC call here.
			 */
			AsyncCallback<ArrayList<CompaAnnotComparedGene>> callback = new AsyncCallback<ArrayList<CompaAnnotComparedGene>>() {
				public void onFailure(Throwable caught) {
					// do some UI stuff to show failure
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
					//caught.printStackTrace();
				}

				public void onSuccess(ArrayList<CompaAnnotComparedGene> newData) {
					if (newData == null) {
						newData = new ArrayList<CompaAnnotComparedGene>();
						// set no result
						CompaAnnotComparedGene headerLightGeneItem = new CompaAnnotComparedGene();
						headerLightGeneItem
								.setTextAkaComparedGeneIdentifier("No result");
						newData.add(headerLightGeneItem);
					} else if (newData.isEmpty()) {
						// set no result
						CompaAnnotComparedGene headerLightGeneItem = new CompaAnnotComparedGene();
						headerLightGeneItem
								.setTextAkaComparedGeneIdentifier("No result");
						newData.add(headerLightGeneItem);
					}

					// set header
					CompaAnnotComparedGene headerLightGeneItem = new CompaAnnotComparedGene();
					headerLightGeneItem
							.setTextAkaComparedGeneIdentifier("Compared Genes");
					newData.add(0, headerLightGeneItem);
					updateRowData(range.getStart(), newData);
				}
			};

			try {

				homologieBrowsingResultService
						.getAllCompaAnnotComparedGeneWithRefGeneIdAndListGeneIds(
								AnnotCompaViewImpl.currClickedRefGeneId,
								listGenesIds, callback);

				// callForInfoDBService
				// .getAllLightGeneItemWithListGeneIds(listGenesIds,
				// callback);

			} catch (Exception e) {
				//String mssgError = "ERROR in GeneBasicInfoDataProvider : "+ e.getMessage();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
				//e.printStackTrace();
			}
		}
	}

	private static class GeneDetailDataProvider extends
			AsyncDataProvider<String> {
		Integer geneId = -1;

		public GeneDetailDataProvider(CompaAnnotComparedGene value) {
			geneId = value.getComparedGeneId();
		}

		/**
		 * {@link #onRangeChanged(HasData)} is called when the table requests a
		 * new range of data. You can push data back to the displays using
		 * {@link #updateRowData(int, List)}.
		 */
		@Override
		protected void onRangeChanged(HasData<String> display) {
			// Get the new range.
			display.setVisibleRange(0, 150);
			final Range range = display.getVisibleRange();

			/*
			 * Query the data asynchronously. If you are using a database, you
			 * can make an RPC call here. We'll use a Timer to simulate a delay.
			 */
			AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
				public void onFailure(Throwable caught) {
					// do some UI stuff to show failure
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
					//caught.printStackTrace();
				}

				public void onSuccess(ArrayList<String> newData) {

					if (newData == null) {
						newData = new ArrayList<String>();
						// set no result
						String headerSt = "No result";
						newData.add(headerSt);
					} else if (newData.isEmpty()) {
						// set no result
						String headerSt = "No result";
						newData.add(headerSt);
					}
					
					for(int i = newData.size()-1;i>=0;i--){
						String toTestForDbxref = newData.get(i);
//						if(toTestForDbxref.contains("db_xref :")){
//							newData.remove(i);
//						}else{
							newData.set(i, "<big>" + toTestForDbxref + "</big>");
//						}
					}
					
					//keep to allow coloring of the comp gene
					AnnotCompaViewImpl.updateDetailsRefGeneSelected();


					// set header
					String headerSt = "Detail of the compared gene";
					newData.add(0, headerSt);
					updateRowData(range.getStart(), newData);
//					Scheduler.get().scheduleDeferred(new ScheduledCommand() {
//						// @Override
//						public void execute() {
//							AnnotCompaViewImpl
//									.colorUncoloredRefGeneDetailsInRed();
//						}
//					});

				}
			};

			try {
				callForInfoDBService
						.getDetailledGeneInfoAsListStringForHTMLWithGeneId(
								geneId, callback);

			} catch (Exception e) {
				//String mssgError = "ERROR in getComparisonHomologAnnotation : "+ e.getMessage();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
				//e.printStackTrace();
			}
		}
	}

	/**
	 * The model that defines the nodes in the tree.
	 */
	private static class CustomTreeModel implements TreeViewModel {

		// dblclick, click, "keydown"
		private Cell<CompaAnnotRefGenes> cellCompaAnnotRefGene = new AbstractCell<CompaAnnotRefGenes>(
				"click", "dblclick") {

			@Override
			public void render(Context context, CompaAnnotRefGenes value,
					SafeHtmlBuilder sb) {
				if (value != null) {
					if (context.getIndex() == 0) {
						// String htmlSt =
						// "<center><b><big>"+value.getTextAkaAnnotationType()+"</big></b></center>";
						String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
								+ value.getTextAkaGeneIdentifier()
								+ "</center></span>";
						sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
					} else {
						sb.appendEscaped(value.getTextAkaGeneIdentifier());
					}
				}
				if (doUpdateDetailsRefGeneSelected) {
					AnnotCompaViewImpl.updateDetailsRefGeneSelected();
					doUpdateDetailsRefGeneSelected = false;
				}
			}

			@Override
			public void onBrowserEvent(Context context, Element parent,
					CompaAnnotRefGenes value, NativeEvent event,
					ValueUpdater<CompaAnnotRefGenes> valueUpdater) {

				super.onBrowserEvent(context, parent, value, event,
						valueUpdater);

				if (Event.getTypeInt(event.getType()) == Event.ONDBLCLICK) {
					// onEnterKeyDown(context, parent, value, event,
					// valueUpdater);
					// Window.alert("double click");
					boolean doShowContextualPopUp = buildMenuContextuelInPopUp(value);
					if (doShowContextualPopUp) {
						final int eventClientX = event.getClientX();
						final int eventClientY = event.getClientY();
						MainTabPanelView.CANVAS_MENU_POPUP
								.setPopupPositionAndShow(new PositionCallback() {
									public void setPosition(int offsetWidth,
											int offsetHeight) {
										int left = eventClientX
												- (offsetWidth / 2);
										int top = eventClientY - offsetHeight
												- 3;
										MainTabPanelView.CANVAS_MENU_POPUP
												.setPopupPosition(left, top);
									}
								});
					}
				}

			}

		};

		private Cell<CompaAnnotCategoryCompa> cellCompaAnnotCategoryCompa = new AbstractCell<CompaAnnotCategoryCompa>() {
			@Override
			public void render(Context context, CompaAnnotCategoryCompa value,
					SafeHtmlBuilder sb) {
				if (value != null) {
					if (context.getIndex() == 0) {
						// String htmlSt =
						// "<center><b><big>"+value.getTextAkaAnnotationType()+"</big></b></center>";
						String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
								+ value.getTextAkaCategoryOfCompa()
								+ "</center></span>";
						sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
					} else {
						int count = 0;
						for (int i = 1; i < value.getChildrens().size(); i++) {
							CompaAnnotClasses cacIT = value.getChildrens().get(
									i);
							count += (cacIT.getChildrens().size() - 1);
						}
						sb.appendEscaped(value.getTextAkaCategoryOfCompa()
								+ " (" + count + ")");
					}
				}
				if (doUpdateDetailsRefGeneSelected) {
					AnnotCompaViewImpl.updateDetailsRefGeneSelected();
					doUpdateDetailsRefGeneSelected = false;
				}
			}

		};
		private Cell<CompaAnnotClasses> cellCompaAnnotClasses = new AbstractCell<CompaAnnotClasses>() {
			@Override
			public void render(Context context, CompaAnnotClasses value,
					SafeHtmlBuilder sb) {
				if (value != null) {
					if (context.getIndex() == 0) {
						// String htmlSt =
						// "<center><b><big>"+value.getTextAkaAnnotationType()+"</big></b></center>";
						String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
								+ value.getTextAkaAnnotationClass()
								+ "</center></span>";
						sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
					} else {

						sb.appendEscaped(value.getTextAkaAnnotationClass()
								+ " (" + (value.getChildrens().size() - 1)
								+ ")");
					}
				}
				if (doUpdateDetailsRefGeneSelected) {
					AnnotCompaViewImpl.updateDetailsRefGeneSelected();
					doUpdateDetailsRefGeneSelected = false;
				}
			}
		};
		private Cell<CompaAnnotNames> cellCompaAnnotNames = new AbstractCell<CompaAnnotNames>() {
			@Override
			public void render(Context context, CompaAnnotNames value,
					SafeHtmlBuilder sb) {
				if (value != null) {
					if (context.getIndex() == 0) {
						// String htmlSt =
						// "<center><b><big>"+value.getTextAkaAnnotationType()+"</big></b></center>";
						String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
								+ value.getTextAkaAnnotationName()
								+ "</center></span>";
						sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
					} else {
						int sizeListComparedOrgaToSend = -1;
						if (AnnotCompaViewImpl.listComparedOrgaToSend.isEmpty()) {
							sizeListComparedOrgaToSend = Insyght.APP_CONTROLER
									.getListAllPublicOrganismItem().size();
						} else {
							sizeListComparedOrgaToSend = AnnotCompaViewImpl.listComparedOrgaToSend
									.size();
						}
						Double fracOrga = (double) ((double) (value
								.getChildrens().size() - 1) / (double) (sizeListComparedOrgaToSend));
						String fracOrgaFormatted = NumberFormat.getFormat(
								"##.#%").format(fracOrga);
						sb.appendEscaped(value.getTextAkaAnnotationName()
								+ " (" + (value.getChildrens().size() - 1)
								+ " = " + fracOrgaFormatted + ")");
					}
				}
				if (doUpdateDetailsRefGeneSelected) {
					AnnotCompaViewImpl.updateDetailsRefGeneSelected();
					doUpdateDetailsRefGeneSelected = false;
				}
			}
		};
		private Cell<CompaAnnotOrganisms> cellCompaAnnotOrganisms = new AbstractCell<CompaAnnotOrganisms>() {
			@Override
			public void render(Context context, CompaAnnotOrganisms value,
					SafeHtmlBuilder sb) {
				if (value != null) {
					if (context.getIndex() == 0) {
						// String htmlSt =
						// "<center><b><big>"+value.getTextAkaAnnotationType()+"</big></b></center>";
						String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
								+ value.getReplacementString()
								+ "</center></span>";
						sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
					} else {
						String textOrgaToShow = "";
						ArrayList<OrganismItem> alOrgaToSearch = null;
						if (AnnotCompaViewImpl.listComparedOrgaToSend.isEmpty()) {
							alOrgaToSearch = Insyght.APP_CONTROLER
									.getListAllPublicOrganismItem();
						} else {
							alOrgaToSearch = AnnotCompaViewImpl.listComparedOrgaToSend;
						}
						for (int i = 0; i < alOrgaToSearch.size(); i++) {
							OrganismItem oiIT = alOrgaToSearch.get(i);
							if (oiIT.getOrganismId() == value.getOrgaId()) {
								// right orga
								textOrgaToShow = oiIT.getFullName() + " ("
										+ value.getChildrensAkaGeneIds().size()
										+ ")";
							}
						}
						if (!textOrgaToShow.isEmpty()) {
							sb.appendEscaped(textOrgaToShow);
						} else {
							// pb ???
							sb.appendEscaped("orga id = " + value.getOrgaId()
									+ " ("
									+ value.getChildrensAkaGeneIds().size()
									+ ")");
						}
					}

				}
				if (doUpdateDetailsRefGeneSelected) {
					AnnotCompaViewImpl.updateDetailsRefGeneSelected();
					doUpdateDetailsRefGeneSelected = false;
				}
			}
		};
		private Cell<CompaAnnotComparedGene> cellCompaAnnotComparedGene = new AbstractCell<CompaAnnotComparedGene>() {
			@Override
			public void render(Context context, CompaAnnotComparedGene value,
					final SafeHtmlBuilder sb) {
				if (value != null) {
					if (context.getIndex() == 0) {
						// String htmlSt =
						// "<center><b><big>"+value.getTextAkaAnnotationType()+"</big></b></center>";
						String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
								+ value.getTextAkaComparedGeneIdentifier()
								+ "</center></span>";
						sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
					} else {
						String alignmentdetails = "";
						if (value.getAlAlignmentDataWithRefGene() != null) {
							for (int i = 0; i < value
									.getAlAlignmentDataWithRefGene().size(); i++) {

								String qAlignFracformatted = NumberFormat
										.getFormat("##.#%")
										.format(value
												.getAlAlignmentDataWithRefGene()
												.get(i).getQAlignFrac());

								alignmentdetails += "<ul><li>Alignment: Evalue = "
										+ value.getAlAlignmentDataWithRefGene()
												.get(i).getEValue()
										+ "</li><li>Score = "
										+ value.getAlAlignmentDataWithRefGene()
												.get(i).getScore()
										+ "</li><li>Percentage identity = "
										+ value.getAlAlignmentDataWithRefGene()
												.get(i).getIdentity()
										+ "%"
										+ "</li><li>Percentage query alignment length = "
										+ qAlignFracformatted + "</li></ul>";
							}

						}
						sb.appendEscaped(value
								.getTextAkaComparedGeneIdentifier());
						sb.append(SafeHtmlUtils
								.fromTrustedString(alignmentdetails));
					}
				}
				if (doUpdateDetailsRefGeneSelected) {
					AnnotCompaViewImpl.updateDetailsRefGeneSelected();
					doUpdateDetailsRefGeneSelected = false;
				}
			}
		};
		private Cell<String> cellCompaAnnotString = new AbstractCell<String>() {
			@Override
			public void render(Context context, String value,
					final SafeHtmlBuilder sb) {
				if (value != null) {
					if (context.getIndex() == 0) {
						// String htmlSt =
						// "<center><b><big>"+value.getTextAkaAnnotationType()+"</big></b></center>";
						String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
								+ value + "</center></span>";
						sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
					} else {
						// background-color green: #C1FFC1
						// background-color red: #FEE0C6
						doUpdateDetailsRefGeneSelected = true;
						boolean doNotMatchRenderAsIt = false;
						//boolean doNotRenderAtAll = false;
						int indexInRef = -1;

						if (doNotColorThisTag(value)) {
							doNotMatchRenderAsIt = true;
						} else {
							// boolean matchFound = false;
							for (int i = 0; i < AnnotCompaViewImpl.vpInfoGene
									.getWidgetCount(); i++) {
								if(AnnotCompaViewImpl.vpInfoGene.getWidget(i) instanceof HTML){
									String htmlRefIT = ((HTML) AnnotCompaViewImpl.vpInfoGene
											.getWidget(i)).getHTML();
									
									if (htmlRefIT.compareTo(value) == 0) {
										indexInRef = i;
										// //matchFound = true;
										// System.err.println("htmlRefIT = "+htmlRefIT);
										// System.err.println("value = "+value);
										break;
									}
								}
							}
						}

						String htmlSt = "";
						if (doNotMatchRenderAsIt) {
							htmlSt = value;
						} else if (indexInRef >= 0) {
							// match, in green
							htmlSt = "<span style=\"background-color:#C1FFC1;\">"
									+ value + "</span>";
							((HTML) AnnotCompaViewImpl.vpInfoGene
									.getWidget(indexInRef))
									.setHTML("<span style=\"background-color:#C1FFC1;\">"
											+ ((HTML) AnnotCompaViewImpl.vpInfoGene
													.getWidget(indexInRef))
													.getHTML() + "</span>");
						} else {
							// no match
							htmlSt = 
									//"<span style=\"background-color:#FEE0C6;\">" +
									value
									//+ "</span>"
									;
						}
						if(!htmlSt.isEmpty()){
							sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
						}
					}
				}
			}

		};

		/**
		 * Get the {@link NodeInfo} that provides the children of the specified
		 * value.
		 */
		public <T> NodeInfo<?> getNodeInfo(T value) {

			if (value == null) {
				// null = error.
				// Create a data provider that says loading.
				final ArrayList<String> listLoading = new ArrayList<String>();
				listLoading.add("Comparison of annotations unavailable.");
				ListDataProvider<String> dataProvider = new ListDataProvider<String>(
						listLoading);
				return new DefaultNodeInfo<String>(dataProvider, new TextCell()
				// ,orgaBrowserPanelSelectionModelNotLeaf, null
				);

			} else if (value instanceof CompaAnnotRoot) {
				compaAnnotRefGenesDataProvider = new ListDataProvider<CompaAnnotRefGenes>(
						((CompaAnnotRoot) value).getChildrens());
				// Return a node info that pairs the data provider and the cell.
				return new DefaultNodeInfo<CompaAnnotRefGenes>(
						compaAnnotRefGenesDataProvider,
						cellCompaAnnotRefGene
						// );
						, orgaBrowserPanelCompaAnnotRefGenesSelectionModel,
						null);
			} else if (value instanceof CompaAnnotRefGenes) {
				CompaAnnotForSpecificGeneDataProvider dataProvider = new CompaAnnotForSpecificGeneDataProvider(
						(CompaAnnotRefGenes) value);
				// ListDataProvider<CompaAnnotCategoryCompa> dataProvider = new
				// ListDataProvider<CompaAnnotCategoryCompa>(
				// ((CompaAnnotRefGenes) value).getChildrens());
				// Return a node info that pairs the data provider and the cell.
				return new DefaultNodeInfo<CompaAnnotCategoryCompa>(
						dataProvider, cellCompaAnnotCategoryCompa);
				// , orgaBrowserPanelCompaAnnotRefGenesSelectionModel, null);
			} else if (value instanceof CompaAnnotCategoryCompa) {
				ListDataProvider<CompaAnnotClasses> dataProvider = new ListDataProvider<CompaAnnotClasses>(
						((CompaAnnotCategoryCompa) value).getChildrens());
				// Return a node info that pairs the data provider and the cell.
				return new DefaultNodeInfo<CompaAnnotClasses>(dataProvider,
						cellCompaAnnotClasses);
				// , orgaBrowserPanelCompaHomoSelectionModel, null);

			} else if (value instanceof CompaAnnotClasses) {
				ListDataProvider<CompaAnnotNames> dataProvider = new ListDataProvider<CompaAnnotNames>(
						((CompaAnnotClasses) value).getChildrens());
				// Return a node info that pairs the data provider and the cell.
				return new DefaultNodeInfo<CompaAnnotNames>(dataProvider,
						cellCompaAnnotNames);
				// , orgaBrowserPanelCompaHomoSelectionModel, null);

			} else if (value instanceof CompaAnnotNames) {
				ListDataProvider<CompaAnnotOrganisms> dataProvider = new ListDataProvider<CompaAnnotOrganisms>(
						((CompaAnnotNames) value).getChildrens());
				// Return a node info that pairs the data provider and the cell.
				return new DefaultNodeInfo<CompaAnnotOrganisms>(dataProvider,
						cellCompaAnnotOrganisms);
				// , orgaBrowserPanelCompaHomoThirdLevelSelectionModel, null);
			} else if (value instanceof CompaAnnotOrganisms) {
				// ListDataProvider<Integer> dataProvider = new
				// ListDataProvider<Integer>(
				// ((CompaAnnotThirdLevel) value).getChildrensAkaGeneIds());
				// // Return a node info that pairs the data provider and the
				// cell.
				// return new DefaultNodeInfo<Integer>(dataProvider,
				// cellCompaAnnotFourthLevel);
				// , orgaBrowserPanelCompaHomoSelectionModel, null);
				GeneBasicInfoDataProvider dataProvider = new GeneBasicInfoDataProvider(
						(CompaAnnotOrganisms) value);
				return new DefaultNodeInfo<CompaAnnotComparedGene>(
						dataProvider, cellCompaAnnotComparedGene);

			} else if (value instanceof CompaAnnotComparedGene) {
				GeneDetailDataProvider dataProvider = new GeneDetailDataProvider(
						(CompaAnnotComparedGene) value);
				return new DefaultNodeInfo<String>(dataProvider,
							cellCompaAnnotString);
				
			}

			return null;

		}

		

		protected boolean buildMenuContextuelInPopUp(
				CompaAnnotRefGenes compaAnnotRefGeneIT) {
			MainTabPanelView.CANVAS_MENU_POPUP.disableAllLeafs();
			
			MainTabPanelView.CANVAS_MENU_POPUP
					.addTransfertGeneToHomologBrowsingView(compaAnnotRefGeneIT
							.getGeneId());
			
			MainTabPanelView.CANVAS_MENU_POPUP
					.addTransfertGeneSetCompaAnnotToHomologBrowsingView(Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getRefGeneSetForAnnotationsComparator());
			MainTabPanelView.CANVAS_MENU_POPUP
					.addFindGeneInGenomicOrganizationView(compaAnnotRefGeneIT
							.getGeneId());
			MainTabPanelView.CANVAS_MENU_POPUP.addExportGeneSequence(
					compaAnnotRefGeneIT.getGeneId(), -1);
			MainTabPanelView.CANVAS_MENU_POPUP.addExportTable();
			return true;
		}

		/**
		 * Check if the specified value represents a leaf node. Leaf nodes
		 * cannot be opened.
		 */
		public boolean isLeaf(Object value) {
			if (value instanceof CompaAnnotRoot) {
				if (((CompaAnnotRoot) value).getChildrens() == null) {
					return true;
				} else {
					if (((CompaAnnotRoot) value).getChildrens().isEmpty()) {
						return true;
					}
				}
			}
			if (value instanceof CompaAnnotRefGenes) {
				if (((CompaAnnotRefGenes) value).getGeneId() < 0) {
					return true;
				}
			}
			if (value instanceof CompaAnnotCategoryCompa) {
				if (((CompaAnnotCategoryCompa) value).getChildrens() == null) {
					return true;
				} else {
					if (((CompaAnnotCategoryCompa) value).getChildrens().size() <= 1) {
						return true;
					}
				}
			}
			if (value instanceof CompaAnnotClasses) {
				if (((CompaAnnotClasses) value).getChildrens() == null) {
					return true;
				} else {
					if (((CompaAnnotClasses) value).getChildrens().size() <= 1) {
						return true;
					}
				}
			}
			if (value instanceof CompaAnnotNames) {
				if (((CompaAnnotNames) value).getChildrens() == null) {
					return true;
				} else {
					if (((CompaAnnotNames) value).getChildrens().size() <= 1) {
						return true;
					}
				}
			}
			if (value instanceof CompaAnnotOrganisms) {
				if (((CompaAnnotOrganisms) value).getChildrensAkaGeneIds() == null) {
					return true;
				} else {
					if (((CompaAnnotOrganisms) value).getChildrensAkaGeneIds()
							.isEmpty()) {
						return true;
					}
				}
			}
			if (value instanceof CompaAnnotComparedGene) {
				if (((CompaAnnotComparedGene) value).getComparedGeneId() < 0) {
					return true;
				}
			}
			if (value instanceof Integer || value instanceof String) {
				return true;
			}

			return false;
		}
	}
	
	public static boolean doNotColorThisTag(String tag) {
		if(tag.endsWith(": </i></big>")){
			//empty tag
			return true;
		} else if(tag.contains("<a href=\"http://migale.jouy.inra.fr/IGO/")
				|| tag.contains("Locus tag :")// Locus tag :
				|| tag.contains("Location :")// Location : 
				|| tag.startsWith("<span style=") //tag deja color�
				){
			return true;
		} else {
			return false;
		}
	}
	
}
