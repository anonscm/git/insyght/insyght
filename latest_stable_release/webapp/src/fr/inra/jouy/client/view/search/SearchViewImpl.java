package fr.inra.jouy.client.view.search;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.TextCell;
//import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellBrowser;
import com.google.gwt.user.cellview.client.CellBrowser.Builder;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TreeNode;
//import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.OrderedMultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.gwt.view.client.TreeViewModel;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.NavigationControler;
import fr.inra.jouy.client.SessionStorageControler;
import fr.inra.jouy.client.ressources.MyResources;
import fr.inra.jouy.client.view.crossSections.components.GeneCell;
import fr.inra.jouy.client.view.crossSections.components.LightElementItemCell;
import fr.inra.jouy.client.view.crossSections.components.MultiValueSuggestBox;
import fr.inra.jouy.client.view.crossSections.components.OrganismCell;
import fr.inra.jouy.client.view.crossSections.components.RangeLabelPager;
import fr.inra.jouy.client.view.crossSections.components.RangeLabelPagerPlusFilteredOutInfo;
import fr.inra.jouy.client.view.crossSections.components.ShowMorePagerPanel;
import fr.inra.jouy.client.view.result.CenterSLPAllGenomePanels;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_scope;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortType;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder;
import fr.inra.jouy.shared.pojos.applicationItems.SharedAppParams;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem.EnumOperatorFilterGeneBy;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem.EnumTypeFilterGeneBy;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.TaxoItem;
import fr.inra.jouy.shared.pojos.users.GroupUsersObj;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.comparators.LightElementItemComparators;
import fr.inra.jouy.shared.comparators.LightGeneItemComparators;
import fr.inra.jouy.shared.comparators.LightOrganismItemComparators;
import fr.inra.jouy.client.view.crossSections.popUp.PopUpListGenesDialog;

public class SearchViewImpl extends Composite {

	private static SearchViewImplUiBinder uiBinder = GWT.create(SearchViewImplUiBinder.class);

	interface SearchViewImplUiBinder extends UiBinder<Widget, SearchViewImpl> {
	}

	// DeckPanel
	@UiField
	public static DeckPanel deckPSearchRoot;
	//@UiField public static DeckPanel deckPVGOorBGSNorth;
	@UiField
	public static DeckPanel deckPVGOorBGSCenter;
	//@UiField public static DeckPanel deckPNextButtons;
	//@UiField public static DeckPanel mainDpStep3;

	// menu
	@UiField
	public static HTML MenuItems_ReferenceGenomeOrTaxonomicGroup;
	@UiField
	public static HTML MenuItems_ReferenceCDSs;
	@UiField
	public static HTML MenuItems_ComparedGenomes;
	@UiField
	public static HTML MenuItems_Visualization;
	@UiField
	public static Button MenuItems_SubmitYourSearch;
	/*// headerWizard
	@UiField
	static Button searchPreviousButton;
	@UiField
	static HTML titleWizardSearch;
	@UiField
	static Button searchNextButton;*/

	// step2
	@UiField static HTML countAllNumOrgaAvail;
	static boolean noOrganismInDatabase = false;
	@UiField
	static VerticalPanel step2VPSuggestBoxOrga;
	public static MultiWordSuggestOracle oracle = new MultiWordSuggestOracle();
	public static SuggestBox sugBox = new SuggestBox(oracle);
	@UiField
	static VerticalPanel step2VPPrivateOrga;
	@UiField
	static HTML HTMLPrivateProject;
	static boolean PRIVATE_GENOME_SELECTED = false;
	@UiField
	static VerticalPanel orgaBrowserPanel;
	private final static SingleSelectionModel<TaxoItem> orgaBrowserPanelSelectionModel = new SingleSelectionModel<TaxoItem>();
	private static CellBrowser cellBrowserTaxoAllOrga;
	public static @UiField VerticalPanel errorOrWarningTextStepSelectOrga;
	public static @UiField HTML HTMLExamplePreMade;
	public static @UiField
	VerticalPanel step2VPExamplePreMade;

	// step select ReferenceCDSs
	public static @UiField
	VerticalPanel VP_SelectRefCDS;
	static RadioButton rbWholeProteome;
	static RadioButton rbCoreDispensableGeneSet;
	public static RadioButton rbBuildGeneSet;
	@UiField public static HorizontalPanel HP_moleculesFromBuildRefGeneSet;
	@UiField public static ShowMorePagerPanel pagerPanel_moleculesFromBuildRefGeneSet;
	private static CellList<LightElementItem> cellList_moleculesFromBuildRefGeneSet;
	private static ListDataProvider<LightElementItem> dataProvider_moleculesFromBuildRefGeneSet;
	private static final MultiSelectionModel<LightElementItem> selectionModel_moleculesFromBuildRefGeneSet = new MultiSelectionModel<LightElementItem>();
	private static boolean DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false;
	@UiField public static HTML HTMLMoleculesSelectedToBeSearched;
	@UiField public static Anchor AnchorSelectAllMoleculesSelectedToBeSearched;
	@UiField public static Anchor AnchorDeselectAllMoleculesSelectedToBeSearched;
			
	//@UiField public static VerticalPanel step3VPRBElement;
	//public static int geneSetBuildMode;
	public static boolean doColorCellTaxoItemAccordingToGroupPlusMinus = false;
	// deckpanel 0 ref taxo node
	@UiField public static VerticalPanel vpRefNodeTaxoBrowserPanelPlusSearch;
	@UiField public static VerticalPanel vpRefNodeTaxoBrowser;
	public final static OrderedMultiSelectionModel<TaxoItem> refNodeTaxoBrowserSelectionModel = new OrderedMultiSelectionModel<TaxoItem>();
	private static boolean doNotTriggerEventRefNodeTaxoBrowserSelectionModel = false;
	private static ArrayList<TaxoItem> alRecursiveChildrenOfTaxoItemIT = new ArrayList<>();
	//public static boolean DO_NOT_TRIGGER_SELECTION_RECUR = false; //
	
	private static Set<TaxoItem> selectedSetBeforeSelectionEvent = new HashSet<>();

	private static CellBrowser cellBrowserRefNodeTaxoBrowser;
	//@UiField public static ScrollPanel spStep3CoreDisp;
	@UiField
	public static Button buttonPhenoPlus;
	//@UiField public static Button buttonRefOrganism;
	@UiField
	public static Button buttonPhenoMinus;
	@UiField
	public static Button buttonNotTakenIntoAccount;
	@UiField
	public static Anchor htmlCountOrgaNotTakenIntoAccount;
	@UiField
	public static Anchor htmlCountOrgaPhenoMinus;
	@UiField
	public static Anchor htmlCountOrgaPhenoPlus;
	public static boolean countParalogs = false;
	static PopupGroupCoreDisp myPopupGroupCoreDisp = new PopupGroupCoreDisp();
	@UiField public static VerticalPanel step2VPSuggestBoxOrga_CoreDisp;
	@UiField public static VerticalPanel errorOrWarningTextStepSelectOrga_CoreDisp;
	public static SuggestBox sugBox_CoreDisp = new SuggestBox(oracle);
	
	@UiField
	public static HTMLPanel toHideIfNotCoreDispGenome_Plus;
	@UiField
	public static HTMLPanel toHideIfNotCoreDispGenome_Minus;
	@UiField
	public static HTMLPanel toHideIfNotCoreDispGenome_NotTakenIntoAccount;
	//@UiField public static Anchor htmlStep3ResultingReferenceGenesSetInTaxoNodeContext;
	//@UiField public static Anchor htmlStep3ReferenceGenesOverRepresentated;
	
	@UiField public static DeckPanel deckPHeaderWholeProtOrCoreDisp;
	@UiField public static HTML titleCurrCoreDispGeneSet;
	@UiField public static Anchor countCDSsCurrentSelectionCoreDispGeneSet;
	@UiField public static Button buttonSetRefOrganismFromRefNodeTaxoBrowser_WholeProtContext;
	@UiField public static Button buttonSetRefOrganismFromRefNodeTaxoBrowser_CoreDisp;
	@UiField public static RadioButton rbReferenceCDSs_mostOrthologsGroupPlus;
	@UiField public static RadioButton rbReferenceCDSs_overrepresented;
	@UiField public static RadioButton rbReferenceCDSs_presenceAbsenceOrthologs;
	//@UiField public static Button bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails;
	//@UiField public static Button bCDSsCurrentSelectionCoreDispGeneSet_AddToSelectionBuild;
	@UiField public static HTMLPanel HTMLPanelCoreDispSpecific;
	static boolean parametersToCalculateCoreDispSetCDSshaveChanged = true;
	public static enum EnumCoreDispParameterCriterion {
		mostOrthologsGroupPlus, overrepresented, presenceAbsenceOrthologs
	}
	public static EnumCoreDispParameterCriterion coreDispParameter_criterion = null;
					
	//@UiField public static HTML htmlText2AboveSpecialTaxoBrowser;
	//@UiField public static HTML acRefGenomeAboveSpecialTaxoBrowser;
	//@UiField public static HTMLPanel htmlPanelAboveSpecialTaxoBrowser;
	public static TaxoItem selectedRefTaxoNode = null;
	public static TaxoItem refTaxoNodeDataUploadedStepReferenceCDSs = null;
	public static OrganismItem roDataUploadedStepReferenceCDSs = null;
	static ArrayList<TaxoItem> alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory = new ArrayList<>();
	static ArrayList<TaxoItem> alRefTaxoNodeOrgaItemsInPhenoPlusCategory = new ArrayList<>();
	static ArrayList<TaxoItem> alRefTaxoNodeOrgaItemsInPhenoMinusCategory = new ArrayList<>();
	@UiField public static HTML messagePhenoPlusMinusCategoryReachedMaxLimit;
	public static enum EnumGroupGenomes { 
		plus, minus, notTakenAccount, reference
	}
	
	private static TaxoItem tiAsIntermediateRoot = null;
//	public static ArrayList<LightGeneItem> alLGIFromCoreDispRequest = new ArrayList<LightGeneItem>();
	public static ArrayList<LightGeneItem> alLGIFromBuildGeneSet = new ArrayList<LightGeneItem>();
	// deckpanel 2 ref orga
	@UiField
	static VerticalPanel vpFilter;
	@UiField static Button addButtonListSelectGene;
	@UiField static Button buttonAddAllMaxFirstGeneInCart;
	@UiField static HTML HTMLLoadingGeneList;
	@UiField static DeckPanel deckPFetchGeneList;
	@UiField static Button refreshListStep3;
	@UiField static ShowMorePagerPanel pagerPanel;
	@UiField static RangeLabelPager rangeLabelPager;
	@UiField static Anchor exportFilteredGeneList;
	private static CellList<LightGeneItem> cellListStep3;
	private static ListDataProvider<LightGeneItem> dataProviderStep3;
	@UiField static ShowMorePagerPanel pagerPanelSelectedGenes;
	@UiField static RangeLabelPager rangeLabelPagerSelectedGenes;
	@UiField static Anchor exportSelectedGeneList;
	private static CellList<LightGeneItem> cellListStep3SelectedGenes;
	private static ListDataProvider<LightGeneItem> dataProviderStep3SelectedGenes;
	@UiField static HTML HTMLMaxGeneInCart;
	@UiField Anchor clearGeneSelection;
	@UiField Anchor clearGeneSelectionAll;
	static SearchPopupPanel mySearchPopupInfo = new SearchPopupPanel();
	@UiField static Button checkQueryValidity;
	@UiField static Button clearAllFilters;
	final static Button addAnotherRootFilter = new Button("Add a new filter");

	//step 3 compared genomes
	@UiField static HTML comparedGenomes_mainList_title;
	@UiField static VerticalPanel comparedGenomes_mainList_VPSuggestBoxSearch;
	@UiField public static Image comparedGenomes_mainList_searchIcon;
	@UiField public static Button comparedGenomes_mainList_clearSearchMain;
	@UiField public static RangeLabelPagerPlusFilteredOutInfo comparedGenomes_rlpMainList;
	@UiField public static ShowMorePagerPanel comparedGenomes_ppMainList;
	@UiField public static HTML comparedGenomes_btAddFeaturedOrga;
	@UiField public static HTML comparedGenomes_btRemoveFeaturedOrga;
	@UiField public static HTML comparedGenomes_btMoveUpOrgaInList;
	@UiField public static HTML comparedGenomes_btMoveDownOrgaInList;
	@UiField public static HTML comparedGenomes_btAddAllFeaturedOrga;
	@UiField public static HTML comparedGenomes_btRemoveAllFeaturedOrga;
	@UiField public static Button comparedGenomes_btResetFindFeatureGenomes;
	@UiField static VerticalPanel comparedGenomes_featuredOrganisms_VPSuggestBoxSearch;
	@UiField public static Image comparedGenomes_featuredOrganisms_searchIcon;
	@UiField public static Button comparedGenomes_featuredOrganisms_clearSearchFeatured;
	@UiField public static HTML comparedGenomes_featuredList_title;
	@UiField public static RangeLabelPager comparedGenomes_rlpFeaturedList;
	@UiField public static ShowMorePagerPanel comparedGenomes_ppFeaturedList;
	private static CellList<LightOrganismItem> comparedGenomes_clMainList;
	private static ListDataProvider<LightOrganismItem> comparedGenomes_ldpMainList;
	final static MultiSelectionModel<LightOrganismItem> comparedGenomes_selectionModelMainList = new MultiSelectionModel<LightOrganismItem>();
	public static SuggestBox comparedGenomes_suggestBoxSearchMain = new SuggestBox(SearchViewImpl.oracle);
	public static SuggestBox comparedGenomes_suggestBoxSearchFeatured = new SuggestBox(SearchViewImpl.oracle);
	private static CellList<LightOrganismItem> comparedGenomes_clFeaturedList;
	private static ListDataProvider<LightOrganismItem> comparedGenomes_ldpFeaturedList;
	final static MultiSelectionModel<LightOrganismItem> comparedGenomes_selectionModelFeaturedList = new MultiSelectionModel<LightOrganismItem>();
	// var
	public static ArrayList<LightOrganismItem> comparedGenomes_alOrgaMainList_filtredIn = new ArrayList<>();
	public static ArrayList<LightOrganismItem> comparedGenomes_alOrgaMainList_filtredOut = new ArrayList<>();
	public static ArrayList<LightOrganismItem> comparedGenomes_alOrgaFeaturedList_filtredIn = new ArrayList<>();
	public static ArrayList<LightOrganismItem> comparedGenomes_alOrgaFeaturedList_filtredOut = new ArrayList<>();
	
	//public static ArrayList<LightOrganismItem> comparedGenomes_alOrgaFeaturedList = new ArrayList<>();
	//public static ArrayList<LightOrganismItem> comparedGenomes_alOrgaMainListFilteredOut = new ArrayList<>();
	public static HashMap<Integer, Integer> comparedGenomes_tmSavedMovesOrgaId2FinalPositionInMainList = new HashMap<>();
	public static boolean comparedGenomes_listDataProviderLOIMainListHasChanged = true;
	public static boolean comparedGenomes_genomeResultListHasChanged = false;
	public static boolean comparedGenomes_listDataProviderLOIFeaturedListHasChanged = false;
	//@UiField public static HorizontalPanel comparedGenomes_HPSortResultListBy_scope;
	//@UiField public static HorizontalPanel comparedGenomes_HPSortResultListBy_sortType;
	//@UiField public static HorizontalPanel comparedGenomes_HPSortResultListBy_sortOrder;
	@UiField public static RadioButton rbComparedGenomesHPSortResultListBy_scope_SelectedRefGene;
	@UiField public static RadioButton rbComparedGenomesHPSortResultListBy_scope_RefGeneSet;
	@UiField public static RadioButton rbComparedGenomesHPSortResultListBy_scope_WholeOrganism;
	@UiField public static RadioButton rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologs;
	@UiField public static RadioButton rbcomparedGenomes_HPSortResultListBy_sortType_SyntenyScore;
	@UiField public static RadioButton rbcomparedGenomes_HPSortResultListBy_sortType_AlignemntScore;
	@UiField public static RadioButton rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologsAnnotations;
	@UiField public static RadioButton rbcomparedGenomes_HPSortResultListBy_sortOrder_Descending;
	@UiField public static RadioButton rbcomparedGenomes_HPSortResultListBy_sortOrder_Ascending;
	
			
	// Visualisation
	private static RefGenoPanelAndListOrgaResu.EnumResultViewTypes choosenVisualisation;
	@UiField static VerticalPanel VPStepVisualization;
	static RadioButton rbViewHomologTable;
	static RadioButton rbViewAnnotCompa;
	static RadioButton rbViewGenomicOrga;
	
	
	// other var
	public static int savedTaxoBrowserMouseY = -1;
	public static int savedTaxoBrowserMouseX = -1;
	public static double savedTaxoBrowserTimeClick = -1;
	private static int intRowPrivate = -1;
	public static SearchLoadingDialog sld = new SearchLoadingDialog();
	//private static LightElementItem selectedLightElementItem = null;
	

	
	
	public SearchViewImpl() {

		initWidget(uiBinder.createAndBindUi(this));
		// rootDeckP.showWidget(0);
		deckPSearchRoot.setAnimationEnabled(true);
		deckPVGOorBGSCenter.setAnimationEnabled(true);
		//deckPNextButtons.setAnimationEnabled(true);
		
		/* Main Menu */
//		@UiHandler("MenuItems_ReferenceGenomeOrTaxonomicGroup")
//		void onMenuItems_ReferenceGenomeOrTaxonomicGroupClick(ClickEvent e) {
//			showCorrectStepSearchView("ReferenceGenomeOrTaxonomicGroup", true);
//		}
		MenuItems_ReferenceGenomeOrTaxonomicGroup.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if ( ! noOrganismInDatabase) {
					showCorrectStepSearchView("ReferenceGenomeOrTaxonomicGroup", true);
				}
			}
		});
//		@UiHandler("MenuItems_ReferenceCDSs")
//		void onMenuItems_ReferenceCDSsClick(ClickEvent e) {
//			showCorrectStepSearchView("ReferenceCDSs", true);
//		}
		MenuItems_ReferenceCDSs.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if ( ! noOrganismInDatabase) {
					showCorrectStepSearchView("ReferenceCDSs", true);
				}
			}
		});
//		@UiHandler("MenuItems_ComparedGenomes")
//		void onMenuItems_ComparedGenomesClick(ClickEvent e) {
//			showCorrectStepSearchView("ComparedGenomes", true);
//		}
		MenuItems_ComparedGenomes.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if ( ! noOrganismInDatabase) {
					showCorrectStepSearchView("ComparedGenomes", true);
				}
			}
		});
//		@UiHandler("MenuItems_Visualization")
//		void onMenuItems_VisualizationClick(ClickEvent e) {
//			showCorrectStepSearchView("Visualization", true);
//		}
		MenuItems_Visualization.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if ( ! noOrganismInDatabase) {
					showCorrectStepSearchView("Visualization", true);
				}
			}
		});
//		@UiHandler("MenuItems_SubmitYourSearch")
//		void onMenuItems_SubmitYourSearchClick(ClickEvent e) {
//			submitSearch(choosenVisualisation);
//		}
		MenuItems_SubmitYourSearch.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if ( ! noOrganismInDatabase) {
					submitSearch(choosenVisualisation);
				}
			}
		});
		
		// build step ReferenceGenomeOrTaxonomicGroup
		buildUIStepReferenceGenomeOrTaxonomicGroup();

		// build step select ReferenceCDSs
		buildUIStepReferenceCDSs();
		
		buildUIStepComparedGenomes();
		
		buildUIStepChooseVisualization();
		
		htmlCountOrgaNotTakenIntoAccount.addStyleDependentName("big");
		htmlCountOrgaPhenoMinus.addStyleDependentName("big");
		htmlCountOrgaPhenoPlus.addStyleDependentName("big");
		
		htmlCountOrgaNotTakenIntoAccount.addStyleDependentName("cliquable");
		htmlCountOrgaPhenoMinus.addStyleDependentName("cliquable");
		htmlCountOrgaPhenoPlus.addStyleDependentName("cliquable");
		
		//acRefGenomeAboveSpecialTaxoBrowser.addStyleDependentName("floatLeft");
		
		//htmlStep3ResultingReferenceGenesSetInTaxoNodeContext.addStyleDependentName("big");
		//htmlStep3ResultingReferenceGenesSetInTaxoNodeContext.addStyleDependentName("cliquable");
		//htmlStep3ReferenceGenesOverRepresentated.addStyleDependentName("cliquable");
		
		exportSelectedGeneList.addStyleDependentName("cliquable");
		exportFilteredGeneList.addStyleDependentName("cliquable");
		
		// build step4
		//buildUIStep4();

		showCorrectStepSearchView("ReferenceGenomeOrTaxonomicGroup", false);

	}
	


	
	
	/* build gene set */
	@UiHandler("exportFilteredGeneList")
	void onClickExportFilteredGeneList(ClickEvent event) {
		
		ArrayList<Integer> alToSent = new ArrayList<Integer>();
		for(LightGeneItem lgiIT : dataProviderStep3.getList()){
			alToSent.add(lgiIT.getGeneId());
		}
		GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.displayPoPup(
				null, null, null, -1,
				null, null, null, -1,
				-1, -1,
				-1,-1,-1,
				-1,-1,-1,
				alToSent);
				//GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.center();
		
	}

	@UiHandler("exportSelectedGeneList")
	void onClickExportSelectedGeneList(ClickEvent event) {
		
		ArrayList<Integer> alToSent = new ArrayList<Integer>();
		for(LightGeneItem lgiIT : dataProviderStep3SelectedGenes.getList()){
			alToSent.add(lgiIT.getGeneId());
		}
		GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.displayPoPup(
				null, null, null, -1,
				null, null, null, -1,
				-1, -1,
				-1,-1,-1,
				-1,-1,-1,
				alToSent);
		//GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.center();
		
	}
	
//	@UiHandler("htmlStep3ResultingReferenceGenesSetInTaxoNodeContext")
//	void onClickHtmlStep3ResultingReferenceGenesSetInTaxoNodeContext(ClickEvent event) {
//		getGeneSetWithPresenceAbsenceInRefNodeContext();
//	}
	
	
	@UiHandler("buttonNotTakenIntoAccount")
	public static void onClickButtonNotTakenIntoAccount(ClickEvent event) {
		onClickBtCoreDispSetNotTakenIntoAccount();
	}
	
	public static void onClickBtCoreDispSetNotTakenIntoAccount() {
		Set<TaxoItem> selectedSet = refNodeTaxoBrowserSelectionModel.getSelectedSet();
		if(selectedSet.isEmpty()){
			return;
		}
		int madeSomeChangesIt = 0;
		for (TaxoItem tiIT : selectedSet) {
			if(tiIT.isRefGenomeInRefNodeContext()
					){
				Window.alert("The reference genome can not be in another group than the group +");
			}else if(tiIT.getTaxoChildrens().isEmpty()){
				//leaf organism
				if(tiIT.getAssociatedOrganismItem() == null){
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
							new Exception("ERROR in onClickBtCoreDispSetNotTakenIntoAccount : "
									+ "tiIT.getTaxoChildrens().isEmpty() but tiIT.getAssociatedOrganismItem()"
									+ " ; tiIT = "
									+ tiIT.getFullName()));
				} else {
					if(tiIT.isAssertPresenceInRefNodeContext()
							){
						//alRefTaxoNodeOrgaItemsInPhenoPlusCategory.remove(tiIT.getAssociatedOrganismItem());
						//alRefTaxoNodeOrgaItemsInPhenoMinusCategory
						//alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.add(tiIT.getAssociatedOrganismItem());
						int innerCountChanges = 0;
						innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.notTakenAccount, true);
						if (innerCountChanges > 0) {
							innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.plus, false);
						}
						madeSomeChangesIt += innerCountChanges;
						
					} else if (tiIT.isAssertAbsenceInRefNodeContext()) {
						//alRefTaxoNodeOrgaItemsInPhenoPlusCategory
						//alRefTaxoNodeOrgaItemsInPhenoMinusCategory.remove(tiIT.getAssociatedOrganismItem());
						//alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.add(tiIT.getAssociatedOrganismItem());
						int innerCountChanges = 0;
						innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.notTakenAccount, true);
						if (innerCountChanges > 0) {
							innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.minus, false);
						}
						madeSomeChangesIt += innerCountChanges;
					} else if (tiIT.isAssertEitherPresenceOrAbsenceInRefNodeContext()) {
						//do nothing
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
								new Exception("ERROR in onClickBtCoreDispSetNotTakenIntoAccount : "
										+ "tiIT in unknown state with regard to group +/-/not"
										+ " ; tiIT = "
										+ tiIT.getFullName()));
					}
				}
//				tiIT.setAssertPresenceInRefNodeContext(false);
//				tiIT.setAssertAbsenceInRefNodeContext(false);
//				tiIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(true);
//				tiIT.setRefGenomeInRefNodeContext(false);
			} else {
				//not a leaf, do not anything for now, wait until all leafs are set
			}
		}
		if (madeSomeChangesIt > 0) {
			updateRelativeCountGenomesInPhenotypeCategories();
			colorTaxoTreeInCoreDispGenomeContext(selectedRefTaxoNode);
			forceRedrawTaxoBrowserRefNode("NO_SELECTION");
			parametersToCalculateCoreDispSetCDSshaveChanged = true;
			updateCurrentCoreDispSetCDSs();
		}

	}
	
	@UiHandler("buttonPhenoPlus")
	public static void onClickButtonPhenoPlus(ClickEvent event) {
		onClickBtCoreDispSetPhenotypePlus(true);
	}
	
	public static void onClickBtCoreDispSetPhenotypePlus(boolean updateCurrentCoreDispSetCDSs) {
		
		Set<TaxoItem> selectedSet = refNodeTaxoBrowserSelectionModel.getSelectedSet();
		if(selectedSet.isEmpty()){
			return;
		}
		int madeSomeChangesIt = 0;
		for (TaxoItem tiIT : selectedSet) {

			
			if(tiIT.isRefGenomeInRefNodeContext()){
				//Window.alert("The reference genome can not be in another group than the group +");
			}else if(tiIT.getTaxoChildrens().isEmpty()){

				
				//leaf organism
				if(tiIT.getAssociatedOrganismItem() == null){
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
							new Exception("ERROR in onClickBtCoreDispSetPhenotypePlus : "
									+ "tiIT.getTaxoChildrens().isEmpty() but tiIT.getAssociatedOrganismItem()"
									+ " ; tiIT = "
									+ tiIT.getFullName()));
				} else {
					if(tiIT.isAssertPresenceInRefNodeContext()){
						//do nothing
					} else if (tiIT.isAssertAbsenceInRefNodeContext()) {
//						alRefTaxoNodeOrgaItemsInPhenoPlusCategory.add(tiIT.getAssociatedOrganismItem());
//						alRefTaxoNodeOrgaItemsInPhenoMinusCategory.remove(tiIT.getAssociatedOrganismItem());
						//alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory
						int innerCountChanges = 0;
						innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.plus, true);
						if (innerCountChanges > 0) {
							innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.minus, false);
						}
						madeSomeChangesIt += innerCountChanges;
					} else if (tiIT.isAssertEitherPresenceOrAbsenceInRefNodeContext()) {					
//						alRefTaxoNodeOrgaItemsInPhenoPlusCategory.add(tiIT.getAssociatedOrganismItem());
//						//alRefTaxoNodeOrgaItemsInPhenoMinusCategory
//						alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.remove(tiIT.getAssociatedOrganismItem());
						int innerCountChanges = 0;
						innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.plus, true);
						if (innerCountChanges > 0) {
							innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.notTakenAccount, false);
						}
						madeSomeChangesIt += innerCountChanges;
					} else {
						//assume set to group + intially on choose default ref genome
						//alRefTaxoNodeOrgaItemsInPhenoPlusCategory.add(tiIT);
						madeSomeChangesIt += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.plus, true);
//						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
//								new Exception("ERROR in onClickBtCoreDispSetPhenotypePlus : "
//										+ "tiIT in unknown state with regard to group +/-/not"
//										+ " ; tiIT = "
//										+ tiIT.getFullName()));
					}
				}
//				tiIT.setAssertPresenceInRefNodeContext(true);
//				tiIT.setAssertAbsenceInRefNodeContext(false);
//				tiIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
//				tiIT.setRefGenomeInRefNodeContext(false);
			} else {
				//not a leaf, do not anything for now, wait until all leafs are set
			}
		}
		
		if (madeSomeChangesIt > 0) {
			updateRelativeCountGenomesInPhenotypeCategories();
			colorTaxoTreeInCoreDispGenomeContext(selectedRefTaxoNode);
			forceRedrawTaxoBrowserRefNode("NO_SELECTION");
			parametersToCalculateCoreDispSetCDSshaveChanged = true;
			if (updateCurrentCoreDispSetCDSs) {
				updateCurrentCoreDispSetCDSs();
			}
		}


	}
	
	@UiHandler("buttonPhenoMinus")
	public static void onClickButtonPhenoMinus(ClickEvent event) {
		onClickBtCoreDispSetPhenotypeMinus();
	}
	
	//@UiHandler("btCoreDispSetAbsence")
	public static void onClickBtCoreDispSetPhenotypeMinus() {
		
		Set<TaxoItem> selectedSet = refNodeTaxoBrowserSelectionModel.getSelectedSet();
		if(selectedSet.isEmpty()){
			return;
		}
		int madeSomeChangesIt = 0;
		for (TaxoItem tiIT : selectedSet) {
			if(tiIT.isRefGenomeInRefNodeContext()){
				Window.alert("The reference genome can not be in another group than the group +");
			}else if(tiIT.getTaxoChildrens().isEmpty()){
				//leaf organism
				if(tiIT.getAssociatedOrganismItem() == null){
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
							new Exception("ERROR in onClickBtCoreDispSetPhenotypeMinus : "
									+ "tiIT.getTaxoChildrens().isEmpty() but tiIT.getAssociatedOrganismItem()"
									+ " ; tiIT = "
									+ tiIT.getFullName()));
				} else {
					if(tiIT.isAssertPresenceInRefNodeContext()){
						//alRefTaxoNodeOrgaItemsInPhenoPlusCategory.remove(tiIT.getAssociatedOrganismItem());
						//alRefTaxoNodeOrgaItemsInPhenoMinusCategory.add(tiIT.getAssociatedOrganismItem());
						//alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory
						int innerCountChanges = 0;
						innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.minus, true);
						if (innerCountChanges > 0) {
							innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.plus, false);
						}
						madeSomeChangesIt += innerCountChanges;
						
					} else if (tiIT.isAssertAbsenceInRefNodeContext()) {
						//do nothing
					} else if (tiIT.isAssertEitherPresenceOrAbsenceInRefNodeContext()) {
						//alRefTaxoNodeOrgaItemsInPhenoPlusCategory
//						alRefTaxoNodeOrgaItemsInPhenoMinusCategory.add(tiIT.getAssociatedOrganismItem());
//						alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.remove(tiIT.getAssociatedOrganismItem());
						int innerCountChanges = 0;
						innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.minus, true);
						if (innerCountChanges > 0) {
							innerCountChanges += manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiIT, EnumGroupGenomes.notTakenAccount, false);
						}
						madeSomeChangesIt += innerCountChanges;
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
								new Exception("ERROR in onClickBtCoreDispSetPhenotypeMinus : "
										+ "tiIT in unknown state with regard to group +/-/not"
										+ " ; tiIT = "
										+ tiIT.getFullName()));
					}
				}
//				tiIT.setAssertPresenceInRefNodeContext(false);
//				tiIT.setAssertAbsenceInRefNodeContext(true);
//				tiIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
//				tiIT.setRefGenomeInRefNodeContext(false);
			} else {
				//not a leaf, do not anything for now, wait until all leafs are set
			}
		}
		if (madeSomeChangesIt > 0) {
			updateRelativeCountGenomesInPhenotypeCategories();
			colorTaxoTreeInCoreDispGenomeContext(selectedRefTaxoNode);
			forceRedrawTaxoBrowserRefNode("NO_SELECTION");
			parametersToCalculateCoreDispSetCDSshaveChanged = true;
			updateCurrentCoreDispSetCDSs();
		}

	}

	public static void onClickBtCoreDispSetAsRefGenome() {
		
		Set<TaxoItem> selectedSet = refNodeTaxoBrowserSelectionModel.getSelectedSet();
		if(selectedSet.isEmpty()){
			return;
		}
		for(TaxoItem firstTaxoItem : selectedSet){
			if( ! firstTaxoItem.getTaxoChildrens().isEmpty()){
				//not leaf
				continue;
			}
			//set old ref genome as presence
			if(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism() != null){
				//find old taxo node
				TaxoItem oldtiIT = Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getAssociatedTaxoItem();
				if(oldtiIT != null){
					manageTaxoItemsAndOrganismWithinGroupPlusMinus(oldtiIT, EnumGroupGenomes.plus, true);
				}
			}
			
			setRefOiFromSelectedTaxoNodeInRefTaxoNodeContext(
					firstTaxoItem
					//,false
					//isBrowseAllGenomeTrue
					);

//			//redraw browser
//			selectATaxoItemInCoreDispBrowser(
//					getRelativePathForCoreDispFromFullPath(getTheFurthestNode(refNodeTaxoBrowserSelectionModel.getSelectedObject(), false),
//					selectedRefTaxoNode), true);
			updateRelativeCountGenomesInPhenotypeCategories();
			colorTaxoTreeInCoreDispGenomeContext(selectedRefTaxoNode);
			forceRedrawTaxoBrowserRefNode("NO_SELECTION");
			parametersToCalculateCoreDispSetCDSshaveChanged = true;
			updateCurrentCoreDispSetCDSs();
			break;//only first selection
		}
		
	}
	
	private static void clearAllTaxoNodeOrgaItemsInPhenoPlusCategory(){
		for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInPhenoPlusCategory ){
			tiIT.setAssertPresenceInRefNodeContext(false);
		}
		alRefTaxoNodeOrgaItemsInPhenoPlusCategory.clear();
	}
	
	private static void clearAllTaxoNodeOrgaItemsInPhenoMinusCategory(){
		for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInPhenoMinusCategory ){
			tiIT.setAssertAbsenceInRefNodeContext(false);
		}
		alRefTaxoNodeOrgaItemsInPhenoMinusCategory.clear();
	}
	
	private static void clearAllTaxoNodeOrgaItemsNotTakenIntoAccountCategory(){
		for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory ){
			tiIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
		}
		alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.clear();
	}
	
	//return 1 if made a change, 0 otherwise
	static int manageTaxoItemsAndOrganismWithinGroupPlusMinus(
			TaxoItem tiSent
			, EnumGroupGenomes groupGenomes
			, boolean add
			) {

		int intToReturn = 1;
		
		if (groupGenomes.compareTo(EnumGroupGenomes.plus) == 0) {
			if (add) {
				//add
				if ( alRefTaxoNodeOrgaItemsInPhenoPlusCategory.size() + alRefTaxoNodeOrgaItemsInPhenoMinusCategory.size() + 1 >= UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined
						&& ! alRefTaxoNodeOrgaItemsInPhenoMinusCategory.contains(tiSent)) {
					// reached max limit, can not add more except transfer from alRefTaxoNodeOrgaItemsInPhenoMinusCategory
					if (alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.contains(tiSent)) {
						intToReturn = 0;
					} else {
						manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiSent, EnumGroupGenomes.notTakenAccount, add);
						return 0;
					}
					
				} else {
					//deal with taxo node
					tiSent.setAssertPresenceInRefNodeContext(true);
					tiSent.setAssertAbsenceInRefNodeContext(false);
					tiSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
					tiSent.setRefGenomeInRefNodeContext(false);
					//deal with array
					alRefTaxoNodeOrgaItemsInPhenoPlusCategory.add(tiSent);
					alRefTaxoNodeOrgaItemsInPhenoMinusCategory.remove(tiSent);
					alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.remove(tiSent);
				}
			} else {
				//remove
				tiSent.setAssertPresenceInRefNodeContext(false);
				alRefTaxoNodeOrgaItemsInPhenoPlusCategory.remove(tiSent);
			}
		} else if (groupGenomes.compareTo(EnumGroupGenomes.minus) == 0) {
			if (add) {
				//add
				if ( alRefTaxoNodeOrgaItemsInPhenoPlusCategory.size() + alRefTaxoNodeOrgaItemsInPhenoMinusCategory.size() + 1 >= UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined
						&& ! alRefTaxoNodeOrgaItemsInPhenoPlusCategory.contains(tiSent)) {
					// reached max limit, can not add more except transfer from alRefTaxoNodeOrgaItemsInPhenoMinusCategory
					if (alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.contains(tiSent)) {
						intToReturn = 0;
					} else {
						manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiSent, EnumGroupGenomes.notTakenAccount, add);
						return 0;
					}
				} else {
					//deal with taxo node
					tiSent.setAssertPresenceInRefNodeContext(false);
					tiSent.setAssertAbsenceInRefNodeContext(true);
					tiSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
					tiSent.setRefGenomeInRefNodeContext(false);
					//deal with array
					alRefTaxoNodeOrgaItemsInPhenoPlusCategory.remove(tiSent);
					alRefTaxoNodeOrgaItemsInPhenoMinusCategory.add(tiSent);
					alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.remove(tiSent);
				}
			} else {
				//remove
				tiSent.setAssertAbsenceInRefNodeContext(false);
				alRefTaxoNodeOrgaItemsInPhenoMinusCategory.remove(tiSent);
			}
		} else if (groupGenomes.compareTo(EnumGroupGenomes.notTakenAccount) == 0) {
			if (add) {
				//add
				//deal with taxo node
				tiSent.setAssertPresenceInRefNodeContext(false);
				tiSent.setAssertAbsenceInRefNodeContext(false);
				tiSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(true);
				tiSent.setRefGenomeInRefNodeContext(false);
				//deal with array
				alRefTaxoNodeOrgaItemsInPhenoPlusCategory.remove(tiSent);
				alRefTaxoNodeOrgaItemsInPhenoMinusCategory.remove(tiSent);
				alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.add(tiSent);
			} else {
				//remove
				tiSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
				alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.remove(tiSent);
			}
		} else if (groupGenomes.compareTo(EnumGroupGenomes.reference) == 0) {
			if (add) {
				//add
				//deal with taxo node
				tiSent.setAssertPresenceInRefNodeContext(false);
				tiSent.setAssertAbsenceInRefNodeContext(false);
				tiSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
				tiSent.setRefGenomeInRefNodeContext(true);
				//deal with array
				alRefTaxoNodeOrgaItemsInPhenoPlusCategory.remove(tiSent);
				alRefTaxoNodeOrgaItemsInPhenoMinusCategory.remove(tiSent);
				alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.remove(tiSent);
			} else {
				//remove
				tiSent.setRefGenomeInRefNodeContext(false);
			}
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
					new Exception("ERROR in manageTaxoItemsAndOrganismWithinGroupPlusMinus : not recognized groupGenomes"
							+ groupGenomes.toString()
							)
					);
		}
		
		if ( alRefTaxoNodeOrgaItemsInPhenoPlusCategory.size() + alRefTaxoNodeOrgaItemsInPhenoMinusCategory.size() + 1 >= UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined
				){
			messagePhenoPlusMinusCategoryReachedMaxLimit.setVisible(true);
		} else {
			messagePhenoPlusMinusCategoryReachedMaxLimit.setVisible(false);
		}
		return intToReturn;
	}


	static void updateRelativeCountGenomesInPhenotypeCategories() {
		htmlCountOrgaPhenoPlus.setHTML("<big><span style=\"background-color:#BCD2EE;color:#00008B;\">"+ ( alRefTaxoNodeOrgaItemsInPhenoPlusCategory.size() + 1 ) + " organism(s)</span></big>");
		htmlCountOrgaPhenoMinus.setHTML("<big><span style=\"background-color:#FFA07A;color:#8B0000;\">"+ alRefTaxoNodeOrgaItemsInPhenoMinusCategory.size() + " organism(s)</span></big>");
		htmlCountOrgaNotTakenIntoAccount.setHTML("<big><span style=\"background-color:#ffe6eb;color:#8B0A50;\">"+alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.size() + " organism(s)</span></big>");

		boolean disableRbReferenceCDSs_overrepresented = false;
		String causeDisableRbReferenceCDSs_overrepresented = "";
		//boolean disableRbReferenceCDSs_presenceAbsenceOrthologs = false;
		String textRbReferenceCDSs_presenceAbsenceOrthologs = "";
		String titleRbReferenceCDSs_presenceAbsenceOrthologs = "";

		if(alRefTaxoNodeOrgaItemsInPhenoPlusCategory.isEmpty()){
			countParalogs = true;
			disableRbReferenceCDSs_overrepresented = true;
			causeDisableRbReferenceCDSs_overrepresented = "the <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span> only contains the reference organism";
			rbReferenceCDSs_mostOrthologsGroupPlus.setHTML("Most paralogs in <span style=\"background-color:#FFF380;color:#000000;\">reference organism</span>");
			rbReferenceCDSs_mostOrthologsGroupPlus.setTitle("For each CDS of the reference organism, sum the number of paralogs in the reference organism."
					+ " Return the list of reference CDSs sorted by this value from bigger to smaller.");
			// no paralog
			//textRbReferenceCDSs_presenceAbsenceOrthologs += "Presence of paralogs in <span style=\"background-color:#FFF380;color:#000000;\">reference organism</span>";
			//titleRbReferenceCDSs_presenceAbsenceOrthologs += " at least 1 paralog in the reference organism";
		} else {
			countParalogs = false;
			rbReferenceCDSs_mostOrthologsGroupPlus.setHTML("Most orthologs in <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span>");
			rbReferenceCDSs_mostOrthologsGroupPlus.setTitle("For each CDS of the reference organism, sum the number of orthologs in each organisms in group +."
					+ " Return the list of reference CDSs sorted by this value from bigger to smaller.");
			textRbReferenceCDSs_presenceAbsenceOrthologs += "Presence of orthologs in <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span>";
			titleRbReferenceCDSs_presenceAbsenceOrthologs += " at least 1 ortholog with each organisms in group +";
		}
		
		if(alRefTaxoNodeOrgaItemsInPhenoMinusCategory.isEmpty()){
			//htmlStep3ReferenceGenesOverRepresentated.setHTML("<big>&#8658; Ref genes most represented in <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span> (count)</big>");
			disableRbReferenceCDSs_overrepresented = true;
			causeDisableRbReferenceCDSs_overrepresented = "the <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span> is empty of organism";
			//disableRbReferenceCDSs_presenceAbsenceOrthologs = true;
		} else {
			if (textRbReferenceCDSs_presenceAbsenceOrthologs.isEmpty()) {
				textRbReferenceCDSs_presenceAbsenceOrthologs += "Absence of orthologs in <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span>";
				titleRbReferenceCDSs_presenceAbsenceOrthologs += " no ortholog with each organisms in group -";
			} else {
				textRbReferenceCDSs_presenceAbsenceOrthologs += " and absence of orthologs in <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span>";
				titleRbReferenceCDSs_presenceAbsenceOrthologs += " and no ortholog with each organisms in group -";
			}
		}
		
		if (textRbReferenceCDSs_presenceAbsenceOrthologs.isEmpty()) {
			// disable
			rbReferenceCDSs_presenceAbsenceOrthologs.setEnabled(false);
			rbReferenceCDSs_presenceAbsenceOrthologs.setHTML(
					"<span style=\"color: #696969;\">Presence orthologs in <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span> and absence in <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span>.</span>"
					+ " <span style=\"font-style: italic;\">This functionality is disabled, it needs at least 1 organism in <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span>"
					+ " that is not the reference or 1 organism in <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span> (see above to dispatch organisms into groups).</span>"
					);
		} else {
			rbReferenceCDSs_presenceAbsenceOrthologs.setEnabled(true);
			rbReferenceCDSs_presenceAbsenceOrthologs.setHTML(textRbReferenceCDSs_presenceAbsenceOrthologs);
			rbReferenceCDSs_presenceAbsenceOrthologs.setTitle("For each CDS of the reference organism, check that it has"
					+ titleRbReferenceCDSs_presenceAbsenceOrthologs
					+ "."
					+ " Return the list of reference CDSs that meet this criterion (sorted by genomic position)."
					+ " This functionality is disabled if the group - is empty of organism (see above to dispatch organisms into groups)."
					);
		}
		
		
		if (disableRbReferenceCDSs_overrepresented) {
			rbReferenceCDSs_overrepresented.setEnabled(false);
			rbReferenceCDSs_overrepresented.setHTML(
					"<span style=\"color: #696969;\">Overrepresented in <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span> compared to <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span>.</span>"
					+ " <span style=\"font-style: italic;\">This functionality is disabled because "+causeDisableRbReferenceCDSs_overrepresented+" (see above to dispatch organisms into groups).</span>"
					);
			
		} else {
			rbReferenceCDSs_overrepresented.setEnabled(true);
			rbReferenceCDSs_overrepresented.setHTML(
					"Overrepresented in <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span> compared to <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span>"
					);
		}
//		if (disableRbReferenceCDSs_presenceAbsenceOrthologs) {
//			rbReferenceCDSs_presenceAbsenceOrthologs.setEnabled(false);
//			rbReferenceCDSs_presenceAbsenceOrthologs.setHTML(
//					"<span style=\"color: #696969;\">"+textRbReferenceCDSs_presenceAbsenceOrthologs+".</span>"
//					+ " <span style=\"font-style: italic;\">This functionality is disabled because the group - is empty of organism (see above to dispatch organisms into groups).</span>"
//					);
//		} else {
//			rbReferenceCDSs_presenceAbsenceOrthologs.setEnabled(true);
//			rbReferenceCDSs_presenceAbsenceOrthologs.setHTML(
//					textRbReferenceCDSs_presenceAbsenceOrthologs
//					);
//		}
		
		
		
	}

	
	@UiHandler("htmlCountOrgaPhenoPlus")
	void onHtmlCountOrgaPhenoPlusClick(ClickEvent event) {
		showDetailsOrgaPhenoPlus();
	}


	static int colorTaxoTreeInCoreDispGenomeContext(TaxoItem taxoItemSent) {
		// 0 : all group +
		// 1 : all group -
		// 2 : all group not taken into account
		// 3 : mixed
		// 4 : N/A (header) or not colored
		// 5 : error do not recursive call
		
		if(taxoItemSent.getTaxoChildrens().isEmpty()){
			//leaf, check if already colored
			if( ! CellTaxoItem.isCellBrowserHeader(taxoItemSent) ){
				if(taxoItemSent.isAssertPresenceInRefNodeContext()
						|| taxoItemSent.isRefGenomeInRefNodeContext()){
					return 0;
				} else if (taxoItemSent.isAssertAbsenceInRefNodeContext()) {
					return 1;
				} else if (taxoItemSent.isAssertEitherPresenceOrAbsenceInRefNodeContext()) {
					return 2;
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
							new Exception("Error in colorTaxoTree :"
									+ " leaf is not categorized : "+taxoItemSent.getFullName()));
					return 5;
				}
			} else {
				return 4;
			}
		} else {
			//branch
			HashSet<Integer> coloredLeafes = new HashSet<Integer>();
			for(TaxoItem childtaxoItem : taxoItemSent.getTaxoChildrens()){
				coloredLeafes.add(colorTaxoTreeInCoreDispGenomeContext(childtaxoItem));
			}
			coloredLeafes.remove(4);
			if(coloredLeafes.size() == 1){
				//return single color
				 //get the Iterator
				int singleColor = -1;
			    Iterator<Integer> itr = coloredLeafes.iterator();
			    while(itr.hasNext()) {
			    	singleColor = itr.next();
			    }
			    if(singleColor == 0){
			    	//all group +
			    	taxoItemSent.setAssertPresenceInRefNodeContext(true);
			    	taxoItemSent.setAssertAbsenceInRefNodeContext(false);
			    	taxoItemSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
			    	taxoItemSent.setRefGenomeInRefNodeContext(false);
			    } else if (singleColor == 1) {
			    	//all group -
			    	taxoItemSent.setAssertPresenceInRefNodeContext(false);
			    	taxoItemSent.setAssertAbsenceInRefNodeContext(true);
			    	taxoItemSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
			    	taxoItemSent.setRefGenomeInRefNodeContext(false);
			    } else if (singleColor == 2) {
			    	//all group not taken into account
			    	taxoItemSent.setAssertPresenceInRefNodeContext(false);
			    	taxoItemSent.setAssertAbsenceInRefNodeContext(false);
			    	taxoItemSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(true);
			    	taxoItemSent.setRefGenomeInRefNodeContext(false);
			    } else {
			    	taxoItemSent.setAssertPresenceInRefNodeContext(false);
			    	taxoItemSent.setAssertAbsenceInRefNodeContext(false);
			    	taxoItemSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
			    	taxoItemSent.setRefGenomeInRefNodeContext(false);
			    }
			    return singleColor;
			} else {
				//mixed
		    	taxoItemSent.setAssertPresenceInRefNodeContext(false);
		    	taxoItemSent.setAssertAbsenceInRefNodeContext(false);
		    	taxoItemSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
		    	taxoItemSent.setRefGenomeInRefNodeContext(false);
				return 3;
			}
		}
		
	}
	
	private static void showDetailsOrgaPhenoPlus() {
//		String htmlTextIT = "<big>Reference organism :</br><ul><li>"
//				+ Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
//				+ "</li></ul></big>";
//		
//		htmlTextIT += "<big>In addition, the following "+alRefTaxoNodeOrgaItemsInPhenoPlusCategory.size()+" organism(s) are in the <span style=\"background-color:#BCD2EE;color:#00008B;\">group +</span> :</br>";
//		htmlTextIT += "<ul>";
//		for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInPhenoPlusCategory){
//			htmlTextIT += "<li>" + tiIT.getFullName() + "</li>";
//		}
//		htmlTextIT += "</ul></big>";
//		//clickToDisplayGeneInfo.setHTML(htmlTextIT);
//		mySearchPopupInfo.setDirectHtml(htmlTextIT);
//		mySearchPopupInfo.setCallBackHtml("");
//		mySearchPopupInfo.adaptSizeAndShow();
		
		myPopupGroupCoreDisp.setGroupToShow(alRefTaxoNodeOrgaItemsInPhenoPlusCategory, EnumGroupGenomes.plus);
		myPopupGroupCoreDisp.adaptSizeAndShow();
		
	}

	@UiHandler("htmlCountOrgaPhenoMinus")
	void onhtmlCountOrgaPhenoMinusClick(ClickEvent event) {
		showDetailsOrgaPhenoMinus();
	}

	private static void showDetailsOrgaPhenoMinus() {
//		String htmlTextIT = "<big>The following "+alRefTaxoNodeOrgaItemsInPhenoMinusCategory.size()+" organism(s) are in the <span style=\"background-color:#FFA07A;color:#8B0000\">group -</span> :</br>";
//		htmlTextIT += "<ul>";
//		for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInPhenoMinusCategory){
//			htmlTextIT += "<li>" + tiIT.getFullName() + "</li>";
//		}
//		htmlTextIT += "</ul></big>";
//		//clickToDisplayGeneInfo.setHTML(htmlTextIT);
//		mySearchPopupInfo.setDirectHtml(htmlTextIT);
//		mySearchPopupInfo.setCallBackHtml("");
//		mySearchPopupInfo.adaptSizeAndShow();
		
		myPopupGroupCoreDisp.setGroupToShow(alRefTaxoNodeOrgaItemsInPhenoMinusCategory, EnumGroupGenomes.minus);
		myPopupGroupCoreDisp.adaptSizeAndShow();
		
	}

	@UiHandler("htmlCountOrgaNotTakenIntoAccount")
	void onhtmlCountOrgaNotTakenIntoAccountClick(ClickEvent event) {
		showDetailsOrgaNotTakenIntoAccount();
	}
	
	private static void showDetailsOrgaNotTakenIntoAccount() {
//		String htmlTextIT = "<big>The following "+alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.size()+" organism(s) are not taken into account :</br>";
//		htmlTextIT += "<ul>";
//		for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory){
//			htmlTextIT += "<li>" + tiIT.getFullName() + "</li>";
//		}
//		htmlTextIT += "</ul></big>";
		//clickToDisplayGeneInfo.setHTML(htmlTextIT);
		myPopupGroupCoreDisp.setGroupToShow(alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory, EnumGroupGenomes.notTakenAccount);
		myPopupGroupCoreDisp.adaptSizeAndShow();
	}
	
	@UiHandler("clearAllFilters")
	void onClearAllFiltersClick(ClickEvent event) {
		doClearAllFilters(vpFilter
				//true
				);
		loadDataRefOrgaStep3();
	}
	

	

	//was setNextStepInTaxoNodeMultiOrgaMode
	private static void setAllowTaxoNodeMultiOrgaMode(boolean taxoNodeMultiOrgaMode) {
		// deal with VP_SelectRefCDS
		// 0 : rbWholeProteome
		// 1 : rbCoreDispensableGeneSet
		// 2 : rbBuildGeneSet
		// 3 : step3VPRBElement
		//RadioButton rbCoreDispGenome = (RadioButton) VP_SelectRefCDS.getWidget(1);
		//GWT.log("setAllowTaxoNodeMultiOrgaMode = "+taxoNodeMultiOrgaMode);
		
		if(taxoNodeMultiOrgaMode){
			//idx 1 : special browser taxo
			deckPVGOorBGSCenter.showWidget(1);
			vpRefNodeTaxoBrowserPanelPlusSearch.setVisible(true);
		}else{
			//idx 0 : nothing, hide
			deckPVGOorBGSCenter.showWidget(0);
			vpRefNodeTaxoBrowserPanelPlusSearch.setVisible(false);
		}

	}


	public static void submitSearch(
			RefGenoPanelAndListOrgaResu.EnumResultViewTypes defaultViewType
			) {

		errorOrWarningTextStepSelectOrga_CoreDisp.clear();
		errorOrWarningTextStepSelectOrga.clear();
		
		if (CenterSLPAllGenomePanels.CURR_COUNT_VISIBLE_RESULT_DISPLAYED > 0
				&& CenterSLPAllGenomePanels.START_INDEX >= 0) {
			GenoOrgaAndHomoTablViewImpl
					.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
		}
		
		Insyght.APP_CONTROLER.getUserSearchItem().setViewTypeDesired(defaultViewType);
		Insyght.APP_CONTROLER.clearAllUserSelection();
		
		Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().clear();
		Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().addAll(comparedGenomes_alOrgaFeaturedList_filtredIn);
		Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().addAll(comparedGenomes_alOrgaFeaturedList_filtredOut);
		//clear search main to have a complete list of compared orga in main list
		//comparedGenomes_clearSearchMain(true);

		boolean preventLaunchSearchIt = false;
		if(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism() == null){
			// was setOrganismItemInSingleRefOrgaContext(Insyght.APP_CONTROLER.getDefaultOrganismIfNoSelection());
			if (selectedRefTaxoNode != null) {
				setDefaultRefGenomeWithSelectedRefTaxoNode();
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
						new Exception("Error in SearchViewImpl.submitSearch :"
								+ " no reference organism is selected "));
				preventLaunchSearchIt = true;
			}
		}

		if ( ! preventLaunchSearchIt) {
			@SuppressWarnings("unused")
			GetResultDialog lDiag = new GetResultDialog(
					Insyght.APP_CONTROLER.getUserSearchItem()
					//,-1, -1,
					//null, null
					//false
					, false
					);
		}


	}

	public static void showCorrectStepSearchView(String stepToShow, boolean makeHistoryTocken) {
		//stepToShow :
		// deckPSearchRoot idx 0 : ReferenceGenomeOrTaxonomicGroup (WAS selectRefOrgaOrTaxoNode)
		//deckPSearchRoot idx 1 : ReferenceCDSs (WAS VisuGenomicOrgaOrBuildGeneSet and chooseRefGeneSet)
		//deckPSearchRoot idx 2 : no orga available
		//deckPSearchRoot idx 3 : ComparedGenomes
		//deckPSearchRoot idx 4 : Visualization
		/* OLD
		//idx 0 : selectRefOrgaOrTaxoNode
		//idx 1 : VisuGenomicOrga
		//idx 2 : chooseRefGeneSet-CoreDispensableGenome
		//idx 2 : chooseRefGeneSet-BrowseAllGenes
		//idx 2 : chooseRefGeneSet-BuildOwnGeneSetWithAvailableMolecules
		 * 
		 */

		// deckPSearchRoot idx 0 : ReferenceGenomeOrTaxonomicGroup (WAS selectRefOrgaOrTaxoNode)
		//deckPSearchRoot idx 1 : ReferenceCDSs (WAS VisuGenomicOrga OR chooseRefGeneSet-CoreDispensableGenome OR chooseRefGeneSet-BrowseAllGenes OR chooseRefGeneSet-BuildOwnGeneSetWithAvailableMolecules)
		//deckPSearchRoot idx 2 : no orga available
		//deckPSearchRoot idx 3 : ComparedGenomes
		//deckPSearchRoot idx 4 : Visualization
		errorOrWarningTextStepSelectOrga_CoreDisp.clear();
		errorOrWarningTextStepSelectOrga.clear();
		
		if(stepToShow.compareTo("ReferenceGenomeOrTaxonomicGroup")==0){
			//idx 0 : ReferenceGenomeOrTaxonomicGroup
			deckPSearchRoot.showWidget(0);	
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setStyleName("HTMLMenuItems_notSelected", false);
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setStyleName("HTMLMenuItems_selected", true);
			MenuItems_ReferenceCDSs.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_ReferenceCDSs.setStyleName("HTMLMenuItems_notSelected", true);
			MenuItems_ComparedGenomes.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_ComparedGenomes.setStyleName("HTMLMenuItems_notSelected", true);
			MenuItems_Visualization.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_Visualization.setStyleName("HTMLMenuItems_notSelected", true);
			
			
			if(makeHistoryTocken){
				NavigationControler.newItem(NavigationControler.getURLTockenFromCurrentState(), false);
			}
			
		} else if ( stepToShow.compareTo("ReferenceCDSs") == 0 ){
			
			/*titleWizardSearch.setHTML("2. View <span style=\"color:#7A378B;\">genomic organisation</span>" +
					" or browse gene set" +
					" with <span style=\"color:#8B3A3A;\">Orthologs table</span>" +
					" or <span style=\"color:#218868;\">Annotations comparator</span>");*/
			
			
			// handle if ref selection got changed
			if ( selectedRefTaxoNode != null ) {
				if ( selectedRefTaxoNode != refTaxoNodeDataUploadedStepReferenceCDSs) {
					setAllowTaxoNodeMultiOrgaMode(true);
					// load data browser
					buildTaxoBrowserCoreDispGenome();
					//set default ref genome
					setDefaultRefGenomeWithSelectedRefTaxoNode();
					updateContentRadioButton_BrowseAllGene();
					updateContentRadioButton_CoreDispGenome();
					updateContentRadioButton_BuildGeneSet(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism());
					refTaxoNodeDataUploadedStepReferenceCDSs = selectedRefTaxoNode;
					if (rbBuildGeneSet.getValue()) {
						setDefaultselectedMoleculeForGeneBuilding();
					}

					
				}
			} else if(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism() != null){
				if (Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism() != roDataUploadedStepReferenceCDSs) {
					setAllowTaxoNodeMultiOrgaMode(false);
					updateContentRadioButton_BrowseAllGene();
					updateContentRadioButton_CoreDispGenome();
					updateContentRadioButton_BuildGeneSet(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism());
					roDataUploadedStepReferenceCDSs = Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism();
					if (rbBuildGeneSet.getValue()) {
						setDefaultselectedMoleculeForGeneBuilding();
					}
				}
			}

			//set default if needed
			if ( ! rbWholeProteome.getValue() && ! rbCoreDispensableGeneSet.getValue() && ! rbBuildGeneSet.getValue()) {
				rbWholeProteome.setValue(true, true);
			}
			
			//handle cases when change is not appropriate
			if ( ! rbCoreDispensableGeneSet.isEnabled() && rbCoreDispensableGeneSet.getValue()) {
				rbWholeProteome.setValue(true, true);
			}
			
			deckPSearchRoot.showWidget(1);	
			/*MenuItems_ReferenceGenomeOrTaxonomicGroup.setStyleName("HTMLMenuItems_notSelected");
			MenuItems_ReferenceCDSs.setStyleName("HTMLMenuItems_selected");
			MenuItems_ComparedGenomes.setStyleName("HTMLMenuItems_notSelected");
			MenuItems_Visualization.setStyleName("HTMLMenuItems_notSelected");*/
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setStyleName("HTMLMenuItems_notSelected", true);
			MenuItems_ReferenceCDSs.setStyleName("HTMLMenuItems_notSelected", false);
			MenuItems_ReferenceCDSs.setStyleName("HTMLMenuItems_selected", true);
			MenuItems_ComparedGenomes.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_ComparedGenomes.setStyleName("HTMLMenuItems_notSelected", true);
			MenuItems_Visualization.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_Visualization.setStyleName("HTMLMenuItems_notSelected", true);
			//deckPNextButtons.showWidget(1);
			
			if(makeHistoryTocken){
				NavigationControler.newItem(NavigationControler.getURLTockenFromCurrentState(), false);
			}
			
			
		} else if ( stepToShow.compareTo("ComparedGenomes") == 0 ){
			
			comparedGenomes_genomeResultListHasChanged = false;
			//comparedGenomes_alOrgaMainList Done in SearchLoadingDialog.loadAllPublicOganisms
			//comparedGenomes_alOrgaFeaturedList = Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults();//new ArrayList<LightOrganismItem>(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults());
			//comparedGenomes_alOrgaMainListFilteredOut = Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome();
			//comparedGenomes_sortOptionsHaveChanged = false;
			comparedGenomes_listDataProviderLOIMainListHasChanged = true;
			comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
			comparedGenomes_refreshListDataProviderMainListIfNedded();
			comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
			
			deckPSearchRoot.showWidget(3);
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setStyleName("HTMLMenuItems_notSelected", true);
			MenuItems_ReferenceCDSs.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_ReferenceCDSs.setStyleName("HTMLMenuItems_notSelected", true);
			MenuItems_ComparedGenomes.setStyleName("HTMLMenuItems_notSelected", false);
			MenuItems_ComparedGenomes.setStyleName("HTMLMenuItems_selected", true);
			MenuItems_Visualization.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_Visualization.setStyleName("HTMLMenuItems_notSelected", true);

			
		} else if ( stepToShow.compareTo("Visualization") == 0 ){
			
			deckPSearchRoot.showWidget(4);
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setStyleName("HTMLMenuItems_notSelected", true);
			MenuItems_ReferenceCDSs.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_ReferenceCDSs.setStyleName("HTMLMenuItems_notSelected", true);
			MenuItems_ComparedGenomes.setStyleName("HTMLMenuItems_selected", false);
			MenuItems_ComparedGenomes.setStyleName("HTMLMenuItems_notSelected", true);
			MenuItems_Visualization.setStyleName("HTMLMenuItems_notSelected", false);
			MenuItems_Visualization.setStyleName("HTMLMenuItems_selected", true);

	
		}else{
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in showCorrectStepSearchView : unrecognized stepToShow = "+stepToShow));
		}
	
		
	}


	private static void showWidgetRelatedToCoreGenome(boolean isCoreDispGenomeDisplay) {
		//hide if not core genome
		//htmlCountOrgaNotTakenIntoAccount.setVisible(isCoreDispGenomeDisplay);
		//buttonRefOrganism.setVisible(isCoreDispGenomeDisplay);
		if(isCoreDispGenomeDisplay){
			//buttonRefOrganism.setHTML("<big><span style=\"background-color:#FFF380;\">Ref organism</span></big>");
			deckPHeaderWholeProtOrCoreDisp.showWidget(1);
		} else {
			//buttonRefOrganism.setHTML("<big><span style=\"background-color:#FFF380;\">Set select organism below as reference</span></big>");
			deckPHeaderWholeProtOrCoreDisp.showWidget(0);
		}
		toHideIfNotCoreDispGenome_Plus.setVisible(isCoreDispGenomeDisplay);
		toHideIfNotCoreDispGenome_Minus.setVisible(isCoreDispGenomeDisplay);
		toHideIfNotCoreDispGenome_NotTakenIntoAccount.setVisible(isCoreDispGenomeDisplay);
//		buttonPhenoPlus.setVisible(isCoreDispGenomeDisplay);
//		buttonPhenoMinus.setVisible(isCoreDispGenomeDisplay);
//		buttonNotTakenIntoAccount.setVisible(isCoreDispGenomeDisplay);
//		//htmlCountOrgaPhenoPlus.setVisible(isCoreDispGenomeDisplay);
//		htmlCountOrgaPhenoMinus.setVisible(isCoreDispGenomeDisplay);
//		htmlCountOrgaNotTakenIntoAccount.setVisible(isCoreDispGenomeDisplay);
//		//acRefGenomeAboveSpecialTaxoBrowser.setVisible(isCoreDispGenomeDisplay);
		//htmlStep3ResultingReferenceGenesSetInTaxoNodeContext.setVisible(isCoreDispGenomeDisplay);//hide if not core genome
		//htmlStep3ReferenceGenesOverRepresentated.setVisible(isCoreDispGenomeDisplay);//hide if not core genome
		HTMLPanelCoreDispSpecific.setVisible(isCoreDispGenomeDisplay);
	}

	/*
	private static void setRadioButtonOptionAsSelectedInUI(int modeSent, LightElementItem eiSent,
			int idxOfWidgetInVerticalPanelSent
			//boolean showTextTipClickButtonLeft,
			//boolean showTextTipClickButtonBelowRefGenomeSent,
			//boolean showTextTipClickButtonBelowPresenceAbsenceSent
			) {
		
		geneSetBuildMode = modeSent;
		
		if(modeSent == 0){
			//CoreDispGenome
			
			//set value in and bold style appropriately
			HTMLViewGenomicOrga.setStyleDependentName("bold", false);
			HTMLCoreDispensableGenome.setStyleDependentName("bold", true);
			HTMLBrowseAllGenesReferenceOrganism.setStyleDependentName("bold", false);
			HTMLAvailableMolecules.setStyleDependentName("bold", false);
			
			((RadioButton) step3VPRBChoiceVisuGenomicOrgaOrBuildGeneSet.getWidget(0)).setStyleDependentName("bold", false);
			((RadioButton) step3VPRBCoreDispenTaxoNode.getWidget(0)).setStyleDependentName("bold", true);
			((RadioButton) step3VPRBAllGeneOrga.getWidget(0)).setStyleDependentName("bold", false);
			//((RadioButton) step3VPRBElement.getWidget(0)).setStyleDependentName("bold", false);
			removeBoldStyleFromAllRadiobuttonInStep3VPRBElement();

		}else if(modeSent == 1){
			//browse all gene of organism
			
			//set value in and bold style appropriately
			HTMLViewGenomicOrga.setStyleDependentName("bold", false);
			HTMLCoreDispensableGenome.setStyleDependentName("bold", false);
			HTMLBrowseAllGenesReferenceOrganism.setStyleDependentName("bold", true);
			HTMLAvailableMolecules.setStyleDependentName("bold", false);
			
			((RadioButton) step3VPRBChoiceVisuGenomicOrgaOrBuildGeneSet.getWidget(0)).setStyleDependentName("bold", false);
			if(step3VPRBCoreDispenTaxoNode.getWidgetCount()>0){
				((RadioButton) step3VPRBCoreDispenTaxoNode.getWidget(0)).setStyleDependentName("bold", false);
			}
			((RadioButton) step3VPRBAllGeneOrga.getWidget(0)).setStyleDependentName("bold", true);
			//((RadioButton) step3VPRBElement.getWidget(0)).setStyleDependentName("bold", false);
			removeBoldStyleFromAllRadiobuttonInStep3VPRBElement();

		}else if(modeSent == 2){
			//build gene set
			
			//set value in and bold style appropriately
			HTMLViewGenomicOrga.setStyleDependentName("bold", false);
			HTMLCoreDispensableGenome.setStyleDependentName("bold", false);
			HTMLBrowseAllGenesReferenceOrganism.setStyleDependentName("bold", false);
			HTMLAvailableMolecules.setStyleDependentName("bold", true);
			
			((RadioButton) step3VPRBChoiceVisuGenomicOrgaOrBuildGeneSet.getWidget(0)).setStyleDependentName("bold", false);
			if(step3VPRBCoreDispenTaxoNode.getWidgetCount()>0){
				((RadioButton) step3VPRBCoreDispenTaxoNode.getWidget(0)).setStyleDependentName("bold", false);
			}
			((RadioButton) step3VPRBAllGeneOrga.getWidget(0)).setStyleDependentName("bold", false);
			removeBoldStyleFromAllRadiobuttonInStep3VPRBElement();
			((RadioButton) step3VPRBElement.getWidget(idxOfWidgetInVerticalPanelSent)).setStyleDependentName("bold", true);
			
		}else if(modeSent == 3){
			//view genomic orga
			
			//set value in and bold style appropriately
			HTMLViewGenomicOrga.setStyleDependentName("bold", true);
			HTMLCoreDispensableGenome.setStyleDependentName("bold", false);
			HTMLBrowseAllGenesReferenceOrganism.setStyleDependentName("bold", false);
			HTMLAvailableMolecules.setStyleDependentName("bold", false);
			
			((RadioButton) step3VPRBChoiceVisuGenomicOrgaOrBuildGeneSet.getWidget(0)).setStyleDependentName("bold", true);
			if(step3VPRBCoreDispenTaxoNode.getWidgetCount()>0){
				((RadioButton) step3VPRBCoreDispenTaxoNode.getWidget(0)).setStyleDependentName("bold", false);
			}
			((RadioButton) step3VPRBAllGeneOrga.getWidget(0)).setStyleDependentName("bold", false);
			removeBoldStyleFromAllRadiobuttonInStep3VPRBElement();

		
		}else{
			//error
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in selectGeneSetBuildMode : unrecognized modeSent = "+modeSent));
		}

	}

	
	private static void removeBoldStyleFromAllRadiobuttonInStep3VPRBElement() {
		for(int i=0;i<step3VPRBElement.getWidgetCount();i++){
			((RadioButton) step3VPRBElement.getWidget(i)).setStyleDependentName("bold", false);
		}
	}*/
	
/*
	private static void updateContentRadioButton_VisuGenomicOrga_setRadioButtonOptionAsSelectedInUI(RadioButton rbStep2VPRBChoiceVisuGenomicOrgaOrBuildGeneSet){
		rbStep2VPRBChoiceVisuGenomicOrgaOrBuildGeneSet.setValue(true, false);
		setRadioButtonOptionAsSelectedInUI(3, null, -1
				//, false
				//, true
				//, false
				);
	}

	private static void updateContentRadioButton_VisuGenomicOrga_updateUICenterAfterSelection(){
		showCorrectStepSearchView("VisuGenomicOrga", false);
	}
	
	private static void updateContentRadioButton_VisuGenomicOrga_updateAssociatedVariables(){
		//clear selection
		clearSelectionStep3(false, false);
		Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(new ArrayList<LightGeneItem>());
	}
	
	private static void updateContentRadioButton_VisuGenomicOrga(
			boolean setRadioButtonOptionAsSelectedInUI,
			boolean updateUICenterAfterSelection,
			boolean updateAssociatedVariables
			){
		
		final RadioButton rbStep2VPRBChoiceVisuGenomicOrgaOrBuildGeneSet = new RadioButton("groupElement",Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName());
		rbStep2VPRBChoiceVisuGenomicOrgaOrBuildGeneSet.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//showCorrectStepSearchView("VisuGenomicOrga", false);
				updateContentRadioButton_VisuGenomicOrga_updateUICenterAfterSelection();
				updateContentRadioButton_VisuGenomicOrga_setRadioButtonOptionAsSelectedInUI(rbStep2VPRBChoiceVisuGenomicOrgaOrBuildGeneSet);
				updateContentRadioButton_VisuGenomicOrga_updateAssociatedVariables();
				
				//update the special taxo browser to hide color
				if(selectedRefTaxoNode != null
						&& refNodeTaxoBrowserSelectionModel != null){
					forceRedrawTaxoBrowserRefNode("KEEP_CURRENT_SELECTION");
				}


			}
		});
		step3VPRBChoiceVisuGenomicOrgaOrBuildGeneSet.clear();
		step3VPRBChoiceVisuGenomicOrgaOrBuildGeneSet.add(rbStep2VPRBChoiceVisuGenomicOrgaOrBuildGeneSet);
		
		if(setRadioButtonOptionAsSelectedInUI){
			updateContentRadioButton_VisuGenomicOrga_setRadioButtonOptionAsSelectedInUI(rbStep2VPRBChoiceVisuGenomicOrgaOrBuildGeneSet);
		}
		if(updateUICenterAfterSelection){
			updateContentRadioButton_VisuGenomicOrga_updateUICenterAfterSelection();
		}
		if(updateAssociatedVariables){
			updateContentRadioButton_VisuGenomicOrga_updateAssociatedVariables();
		}
		
		
//		if(select){
//
//			//showCorrectStepSearchView("VisuGenomicOrga", false);
//			updateContentRadioButton_VisuGenomicOrga_updateUICenterAfterSelection();
////			rbStep2VPRBChoiceVisuGenomicOrgaOrBuildGeneSet.setValue(true);
////			setRadioButtonOptionAsSelectedInUI(3, null, -1
////					, false
////					//, true
////					, false
////					);
//			updateContentRadioButton_VisuGenomicOrga_setRadioButtonOptionAsSelectedInUI(rbStep2VPRBChoiceVisuGenomicOrgaOrBuildGeneSet);
//			updateContentRadioButton_VisuGenomicOrga_updateAssociatedVariables();
//		}
		
	}
*/
	
	/*
	private static void updateContentRadioButton_CoreDispGenome_setRadioButtonOptionAsSelectedInUI(RadioButton rbCoreDispGenome){
		rbCoreDispGenome.setValue(true, false);
		setRadioButtonOptionAsSelectedInUI(0, null, -1
				//, false
				//, true
				//, true
				);
	}
	*/

	private static void updateContentRadioButton_CoreDispGenome_updateUICenterAfterSelection(){
		//showCorrectStepSearchView("chooseRefGeneSet-CoreDispensableGenome", false);
		

		showWidgetRelatedToCoreGenome(true);
//		if(selectedRefTaxoNode != null) {
//			//text introductif change ref + assert prese / absence
//			//htmlPanelAboveSpecialTaxoBrowser.setVisible(true);
//			//htmlText2AboveSpecialTaxoBrowser.setVisible(true);
////			htmlText2AboveSpecialTaxoBrowser.setHTML(
////					"<big>"
////					+ "&#8595; "
//////					+ "Double-click on an organism or node in the taxonomic browser below to change the"
//////					+ " <span style=\"background-color:#FFF380;\">reference genome</span>"
////					+ "To change the"
////					+ " <span style=\"background-color:#FFF380;\">ref genome</span>"
////					+ " or which genomes to categorize in "
////					+ " <span style=\"background-color:#BCD2EE;color:#00008B;\">phenotype +</span>"
////					+ " or"
////					+ " <span style=\"background-color:#FFA07A;color:#8B0000\">phenotype -</span>"
////					+ " groups,"
////					+ " check the boxes next to organisms or taxonomic nodes below"
////					+ " and click the appropriate button above"
////					+ ".</big>"
////					);
//		}
		HP_moleculesFromBuildRefGeneSet.setVisible(false);
		//idx 1 : special browser taxo  ;
		deckPVGOorBGSCenter.showWidget(1);
		
	}
	
//	private static void updateContentRadioButton_CoreDispGenome_updateAssociatedVariables(){
//		//clear selection
//		clearSelectionStep3(false, false);
//		//Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(alLGIFromCoreDispRequest);
//		manageUserSearchItem_ListReferenceGeneSet(true, alLGIFromCoreDispRequest, false, -1);
//		doColorCellTaxoItemAccordingToGroupPlusMinus = true;
//	}
	
	private static void updateContentRadioButton_CoreDispGenome(
			//boolean setRadioButtonOptionAsSelectedInUI,
			//boolean updateUICenterAfterSelection,
			//boolean updateAssociatedVariables
			){
		
		// deal with VP_SelectRefCDS
		// 0 : rbWholeProteome
		// 1 : rbCoreDispensableGeneSet
		// 2 : rbBuildGeneSet
		// 3 : step3VPRBElement
		
		RadioButton rbCoreDispGenome = (RadioButton) VP_SelectRefCDS.getWidget(1);
		if (selectedRefTaxoNode != null) {
			rbCoreDispGenome.setEnabled(true);
			rbCoreDispGenome.setHTML(
					"<big><b>Core / dispensable set of CDSs</b> for reference node "
					+ selectedRefTaxoNode.getFullName()
					+ " (" + selectedRefTaxoNode.getSumOfLeafsUnderneath()
					+ " organisms)"
					+ "</big>"
					);
		} else {
			rbCoreDispGenome.setEnabled(false);
			rbCoreDispGenome.setHTML(
					"<span style=\"color: #696969;\"><big>"
					+ "Core / dispensable set of CDSs (No reference taxonomic group was choosen, select a taxonomic group with the menu on the left)"
					+ "</big></span>"
					);
		}

		
	}
	

	//KEEP_CURRENT_SELECTION
	//NO_SELECTION
	//ALL_SELECTION
	protected static void forceRedrawTaxoBrowserRefNode(final String selectionStateAfterward) {
		
		if (tiAsIntermediateRoot == null) {
			buildTaxoBrowserCoreDispGenome();
		}
		
		if(selectionStateAfterward.compareTo("KEEP_CURRENT_SELECTION")==0){
			final ArrayList<TaxoItem> selectedTaxoItemBeforeRedraw = new ArrayList<>(selectedSetBeforeSelectionEvent);
			//selectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot.getTaxoChildrens(), false, null);//deselect all once
			//no Need to deselect first (?)
			//selectUnselectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot, false);//deselect all once
			//selectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot.getTaxoChildrens(), true, null);//select all once
			selectUnselectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot, true);//select all once
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {
				// @Override
				public void execute() {
					//selectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot.getTaxoChildrens(), null, selectedTaxoItemBeforeRedraw);//reselect previously selected, do schedule deffered to trick browser
					selectUnselectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot, false);//deselect all once
					setSelectedTaxoItemsInCoreDispBrowser(selectedTaxoItemBeforeRedraw, true);
				}
			});
		} else if (selectionStateAfterward.compareTo("NO_SELECTION")==0){
			//selectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot.getTaxoChildrens(), false, null);//deselect all once
			//no Need to deselect first (?)
			//selectUnselectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot, false);//deselect all once
			//selectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot.getTaxoChildrens(), true, null);//select all once
			selectUnselectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot, true);//select all once
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {
				// @Override
				public void execute() {
					//selectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot.getTaxoChildrens(), false, null);//deselect all once
					selectUnselectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot, false);//deselect all once
				}
			});
		} else if (selectionStateAfterward.compareTo("ALL_SELECTION")==0){
			//selectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot.getTaxoChildrens(), false, null);//deselect all once
			selectUnselectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot, false);//deselect all once
			//selectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot.getTaxoChildrens(), true, null);//select all once
			selectUnselectChildrenOfTaxoItemRecursively(tiAsIntermediateRoot, true);//select all once
		}

		
	}


	private static void updateContentRadioButton_BrowseAllGene_updateUICenterAfterSelection(){
		showWidgetRelatedToCoreGenome(false);
		HP_moleculesFromBuildRefGeneSet.setVisible(false);
		
		if(selectedRefTaxoNode != null) {
			//idx 1 : special browser taxo  ; text introductif change ref only
			deckPVGOorBGSCenter.showWidget(1);
		} else if (Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism() != null) {
			//idx 0 : nothing, hide
			deckPVGOorBGSCenter.showWidget(0);
		}
	}
	
	private static void updateContentRadioButton_BrowseAllGene_updateAssociatedVariables(){
		//clear selection
		clearSelectionStep3(false, false);
		SearchViewImpl.manageUserSearchItem_ListReferenceGeneSet(true, null, false, -1);
//		Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(new ArrayList<LightGeneItem>());
//		MenuItems_ReferenceCDSs.setTitle("Current selection : whole proteome");
		doColorCellTaxoItemAccordingToGroupPlusMinus = false;
	}
	
	private static void updateContentRadioButton_BrowseAllGene(
			){
		
		// deal with VP_SelectRefCDS
		// 0 : rbWholeProteome
		// 1 : rbCoreDispensableGeneSet
		// 2 : rbBuildGeneSet
		// 3 : step3VPRBElement
		
		RadioButton rbWholeProteome = (RadioButton) VP_SelectRefCDS.getWidget(0);
		rbWholeProteome.setHTML(
				"<big><b>Whole proteome</b> of "
				+ Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
				+ "</big>"
				);		
	}

	private static void updateContentRadioButton_BuildGeneSet_updateUICenterAfterSelection(){
		//showCorrectStepSearchView("chooseRefGeneSet-BuildOwnGeneSetWithAvailableMolecules", false);
		
		showWidgetRelatedToCoreGenome(false);
		HP_moleculesFromBuildRefGeneSet.setVisible(true);
		//idx 2 : special build gene set from element
		deckPVGOorBGSCenter.showWidget(2);
	}
	
//	private static void updateContentRadioButton_BuildGeneSet_updateAssociatedVariables(LightElementItem eiSent){
//		selectLightElementItem(eiSent);
//		manageUserSearchItem_ListReferenceGeneSet(true, alLGIFromBuildGeneSet, true, -1);
//		//Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(alLGIFromBuildGeneSet);
//		doColorCellTaxoItemAccordingToGroupPlusMinus = false;
//	}
	
	/* methods Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet() */ 
	
	
	public static void sortListReferenceGeneSetByStart() {
		//Comparator<LightGeneItem> compaByGeneStartComparatorIT = new LightGeneItem.ByStartGeneComparator();
		Collections.sort(Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet(), LightGeneItemComparators.byStartGeneComparator);
	}
	
	
	public static void manageUserSearchItem_ListReferenceGeneSet(
			boolean eraseCurrentUserSearchItemListReferenceGeneSetBeforeHand
			, List<LightGeneItem> allGIToAdd
			, boolean truncateListToMAX_NUMBER_IN_GENE_SET_ALLOWED
			//, boolean fromBuildYourReferenceGeneSetMenu
			, int idxOfFirstSelectedInBuildGeneSet
			) {
		
		if (eraseCurrentUserSearchItemListReferenceGeneSetBeforeHand) {
			Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(null);
		}
		
		if (allGIToAdd != null && ! allGIToAdd.isEmpty()) {
			Insyght.APP_CONTROLER.getUserSearchItem().addListToListReferenceGeneSet(allGIToAdd);
		}
		
		if (truncateListToMAX_NUMBER_IN_GENE_SET_ALLOWED) {
			if (Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().size() > SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED) {
				Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().subList(
						SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED
						, Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().size()
						).clear();
			}
		}

		//set title MenuItems_ReferenceCDSs
		if (rbWholeProteome.getValue()) {
			MenuItems_ReferenceCDSs.setTitle("Whole proteome");
			if ( ! rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.getValue()) {
				rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.setValue(true, true);
			}
		} else if (rbCoreDispensableGeneSet.getValue()) {
			if ( Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().isEmpty() ) {
				SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs : 0 CDS selected -> default to whole proteome)");
				if ( ! rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.getValue()) {
					rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.setValue(true, true);
				}
			} else {
				SearchViewImpl.MenuItems_ReferenceCDSs.setTitle("Core / dispensable set of CDSs : "+Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().size()+" CDS(s) selected");
				if ( ! rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.getValue()) {
					rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.setValue(true, true);
				}
			}
		} else if (rbBuildGeneSet.getValue()) {
			if ( Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().isEmpty() ) {
				MenuItems_ReferenceCDSs.setTitle("Build your reference gene set : 0 CDS selected -> default to whole proteome)");
				if ( ! rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.getValue()) {
					rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.setValue(true, true);
				}
			} else {
				MenuItems_ReferenceCDSs.setTitle("Build your reference gene set : "+Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().size()+" CDS(s) selected");
				if ( ! rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.getValue()) {
					rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.setValue(true, true);
				}
			}
			
			pagerPanelSelectedGenes.getDisplay().setVisibleRange(
					0,
					Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().size());
			dataProviderStep3SelectedGenes.setList(Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet());
			if (idxOfFirstSelectedInBuildGeneSet >= 0
					&& idxOfFirstSelectedInBuildGeneSet < Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().size() - 1) {
				cellListStep3SelectedGenes.getRowElement(idxOfFirstSelectedInBuildGeneSet)
						.scrollIntoView();
			}
			// cellListStep3SelectedGenes.getSelectionModel().setSelected(object,
			// selected);

			if (Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().isEmpty()) {
				HTMLMaxGeneInCart.setHTML("<b>Your selection : (Max = "
						+ SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED + ")</b>");
			} else {
				HTMLMaxGeneInCart.setHTML("<b>Your selection : "
						+ Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().size() + " out of "
						+ SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED + "</b>");
			}
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(
					6
					, new Exception("Error in manageUserSearchItem_ListReferenceGeneSet : not rbWholeProteome.getValue() or rbCoreDispensableGeneSet.getValue() or rbBuildGeneSet.getValue() ")
					);
		}
		
		if ( Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().isEmpty() ) {
			rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.setEnabled(false);
			rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.setHTML("<span style=\"color: #696969;\">Reference gene set</span>");
			//SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(false);
		} else {
			rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.setEnabled(true);
			rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.setHTML("Reference gene set");
			//SearchViewImpl.bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails.setEnabled(true);
		}
		
	}


	private static void updateContentRadioButton_BuildGeneSet(
			OrganismItem refOrganismSent
			//, boolean selectByDefault
			//, boolean setRadioButtonOptionAsSelectedInUI
			//, boolean updateUICenterAfterSelection
			//, boolean updateAssociatedVariables
			) {
		

		// deal with VP_SelectRefCDS
		// 0 : rbWholeProteome
		// 1 : rbCoreDispensableGeneSet
		// 2 : rbBuildGeneSet
		// 3 : step3VPRBElement
		
		RadioButton rbBuildGeneSet = (RadioButton) VP_SelectRefCDS.getWidget(2);
		rbBuildGeneSet.setHTML(
				"<big><b>Build your reference gene set</b> among "
						+ refOrganismSent.getListAllLightElementItem().size()
						+ " available molecules of "
						+ refOrganismSent.getFullName()
						+ " :</big>"
				);
		
		
		/*HTMLAvailableMolecules.setHTML("<big>Or build your reference gene set among "
				+ refOrganismSent.getListAllLightElementItem().size()
				+ " available molecules :</big>");*/
		refreshMoleculesFromBuildRefGeneSet(refOrganismSent.getListAllLightElementItem());
		/*step3VPRBElement.clear();
		for (int i = 0; i < refOrganismSent.getListAllLightElementItem()
				.size(); i++) {
			final LightElementItem ei = refOrganismSent.getListAllLightElementItem().get(i);
			final RadioButton rb = new RadioButton("groupElement",
					"<big>" + ei.getType() + " " + ei.getAccession() + " ("
							+ NumberFormat.getFormat("###,###,###,###,###,###.##").format(ei.getSize()) + " bp)</big>"
							, true);
			//final int iFinal = i;
			rb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					updateContentRadioButton_BuildGeneSet_updateUICenterAfterSelection();
					//updateContentRadioButton_BuildGeneSet_setRadioButtonOptionAsSelectedInUI(rb, ei, iFinal);
					
					// was updateContentRadioButton_BuildGeneSet_updateAssociatedVariables(ei);
					selectLightElementItem(ei);
					manageUserSearchItem_ListReferenceGeneSet(true, alLGIFromBuildGeneSet, true, -1);
					//Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(alLGIFromBuildGeneSet);
					doColorCellTaxoItemAccordingToGroupPlusMinus = false;
				}
			});
			step3VPRBElement.add(rb);
		}*/

	}


	@UiHandler("refreshListStep3")
	void onClickRefreshListStep3(ClickEvent e) {
		loadDataRefOrgaStep3();
	}

//	private static void getListGenesWithFilters() {
//		//Insyght.APP_CONTROLER.getAllListLightGeneItemFromSlectedElement().clear();
//		loadDataRefOrgaStep3();
//	}

	@UiHandler("checkQueryValidity")
	void onClickCheckQueryValidity(ClickEvent e) {
		int maxSizeAmongSelectedElement = -1;
	    Iterator<LightElementItem> it0 = selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet().iterator();
	    while(it0.hasNext()){
	    	int sizeIt = it0.next().getSize();
			if (sizeIt > 0) {
				if (sizeIt > maxSizeAmongSelectedElement) {
					maxSizeAmongSelectedElement = sizeIt;
				}
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in onClickCheckQueryValidity :" +
				" size of element "+it0.next().getAccession()+" id = "+it0.next().getElementId()+" <= 0"));
				return;
			}
	    }
		/*int sizeSelectedElement = -1;
		if (selectedLightElementItem != null) {
			if (selectedLightElementItem.getElementId() > 0) {
				sizeSelectedElement = selectedLightElementItem.getSize();
			}
		}
		if (sizeSelectedElement <= 0) {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in onClickCheckQueryValidity :" +
					" size of selected element <= 0"));
			return;
		}*/
		ArrayList<FilterGeneItem> listFilters = new ArrayList<FilterGeneItem>();
		parseVPFiltersN(vpFilter, listFilters
				, maxSizeAmongSelectedElement
				);
		String htmlTextLoading = FilterGeneItem.getEnglishTraductionOflistFilters(listFilters);
		if (htmlTextLoading.startsWith("ERROR")) {
			HTMLLoadingGeneList.setStyleDependentName("greenText", false);
			HTMLLoadingGeneList.setStyleDependentName("redText", true);
			HTMLLoadingGeneList.setHTML("Invalid query :<br/><br/>"
					+ htmlTextLoading);
		} else {
			HTMLLoadingGeneList.setStyleDependentName("greenText", true);
			HTMLLoadingGeneList.setStyleDependentName("redText", false);
			HTMLLoadingGeneList.setHTML("Valid query :<br/>" + htmlTextLoading);
		}
		deckPFetchGeneList.showWidget(0);
	}

	
	@UiHandler("clearGeneSelectionAll")
	void onClickClearGeneSelectionAll(ClickEvent e) {
		alLGIFromBuildGeneSet.clear();
		manageUserSearchItem_ListReferenceGeneSet(true, null, true, -1);
		//refreshDisplayListSlectedLightGeneItemFromSlectedElementStep3(-1);
	}
	
	
	@UiHandler("clearGeneSelection")
	void onClickClearGeneSelection(ClickEvent e) {
		@SuppressWarnings("unchecked")
		Set<LightGeneItem> setLgiToAdd = ((MultiSelectionModel<LightGeneItem>) cellListStep3SelectedGenes
				.getSelectionModel()).getSelectedSet();
		boolean removeAtLeastOne = false;
		TreeSet<LightGeneItem> TreeSetLgiToAdd = new TreeSet<LightGeneItem>(
				setLgiToAdd);
		Iterator<LightGeneItem> iter = TreeSetLgiToAdd.iterator();
		while (iter.hasNext()) {
			// System.out.println(iter.next());
			LightGeneItem lgiToAdd = iter.next();
			if (alLGIFromBuildGeneSet.remove(lgiToAdd)) {
				removeAtLeastOne = true;
			}
		}
		if (removeAtLeastOne) {
			manageUserSearchItem_ListReferenceGeneSet(true, alLGIFromBuildGeneSet, true, -1);
			//refreshDisplayListSlectedLightGeneItemFromSlectedElementStep3(-1);
		}
	}


	
//	public static void selectLightElementItem(
//			//LightElementItem leiSelected
//			//Set<LightElementItem> setLeiSelected
//			) {
//		/*if (leiSelected != null) {
//			selectedLightElementItem = leiSelected;
//			Insyght.APP_CONTROLER.getAllListLightGeneItemFromSlectedElement()
//					.clear();
//			loadDataRefOrgaStep3();
//		} else {
//			selectedLightElementItem = null;
//		}*/
//		//if (setLeiSelected != null && ! setLeiSelected.isEmpty()) {
//			Insyght.APP_CONTROLER.getAllListLightGeneItemFromSlectedElement().clear();
//			loadDataRefOrgaStep3(
//					//setLeiSelected
//					);
//		//}
//	}


	public static void setRefTaxoNode(TaxoItem taxoItemSent) {
		
		
		
		//forbidden selections
		if(taxoItemSent != null){
			if(taxoItemSent.getName().compareTo("Domain")==0){
				taxoItemSent = null;
			}else if(taxoItemSent.getName().compareTo("Phylum")==0){
				taxoItemSent = null;
			}else if(taxoItemSent.getName().compareTo("Class")==0){
				taxoItemSent = null;
			}else if(taxoItemSent.getName().compareTo("Order")==0){
				taxoItemSent = null;
			}else if(taxoItemSent.getName().compareTo("Family")==0){
				taxoItemSent = null;
			}else if(taxoItemSent.getName().compareTo("Genus")==0){
				taxoItemSent = null;
			}else if(taxoItemSent.getName().compareTo("Species")==0){
				taxoItemSent = null;
			}
		}
		
		if(taxoItemSent == null){
//			if(selectedRefTaxoNode == null){
//				return;
//			}
			selectedRefTaxoNode = null;
			refTaxoNodeDataUploadedStepReferenceCDSs = null;
			//titleWizardSearch.setHTML("1. Select a reference organism or taxonomic node");	
			
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setTitle("Selected reference taxonomic node set to Null");
			
		}else{
			
			setOrganismItemInSingleRefOrgaContext(null, false, false);
			
			selectedRefTaxoNode = taxoItemSent;
			sugBox.setText(taxoItemSent.getFullName());
			//titleWizardSearch.setHTML("Selected reference taxonomic node : "
			TaxoItem defaultReferenceOrganism = getTheFurthestNode(selectedRefTaxoNode, true);
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setTitle("Reference taxonomic node : "
					+ selectedRefTaxoNode.getFullName() + " (" + selectedRefTaxoNode.getSumOfLeafsUnderneath() + " organisms)"
					+ "\n"
					+ "Reference organism in node : " + defaultReferenceOrganism.getFullName()
					);			
			
		}
		
		//setEnableButtonFurtherStepOrAnalysisFromSelectRefOrgaOrTaxoNode();
	}

	private static void setRefOiFromSelectedTaxoNodeInRefTaxoNodeContext(
			TaxoItem tiSent
			//, boolean byDefault
			) {
		
		Insyght.APP_CONTROLER.getUserSearchItem().setReferenceOrganism(tiSent.getAssociatedOrganismItem());
		manageTaxoItemsAndOrganismWithinGroupPlusMinus(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getAssociatedTaxoItem(), EnumGroupGenomes.reference, true);

		//clear arrayGeneset
		//alLGIFromCoreDispRequest.clear();
		alLGIFromBuildGeneSet.clear();
		manageUserSearchItem_ListReferenceGeneSet(true, null, true, -1);
		//refreshDisplayListSlectedLightGeneItemFromSlectedElementStep3(-1);
		
		buttonSetRefOrganismFromRefNodeTaxoBrowser_WholeProtContext.setTitle(
				"Current reference genome: "
				+ Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
				+ ".\nTo change the reference genome, check the box next to an organism in the taxonomic browser below and click this button."
				);
		buttonSetRefOrganismFromRefNodeTaxoBrowser_CoreDisp.setTitle(
				"Current reference genome: "
				+ Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
				+ ".\nTo change the reference genome, check the box next to an organism in the taxonomic browser below and click this button."
				);
		titleCurrCoreDispGeneSet.setHTML(
				"<ul><li>Resulting core / dispensable set of CDSs for "
				+ Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
				+ " :</li></ul>"
				);
		
		MenuItems_ReferenceGenomeOrTaxonomicGroup.setTitle("Reference taxonomic node : "
				+ selectedRefTaxoNode.getFullName() + " (" + selectedRefTaxoNode.getSumOfLeafsUnderneath() + " organisms)."
						+ "\n"
						+ "Reference organism in node : " + Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
						);
		
		
		updateContentRadioButton_BrowseAllGene();
		updateContentRadioButton_CoreDispGenome();
		updateContentRadioButton_BuildGeneSet(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism()
				//, false, false, false, false
				);
		
		//special browser
		manageTaxoItemsAndOrganismWithinGroupPlusMinus(tiSent, EnumGroupGenomes.reference, true);
		//deal with array
//		alRefTaxoNodeOrgaItemsInPhenoPlusCategory.remove(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism());
//		alRefTaxoNodeOrgaItemsInPhenoMinusCategory.remove(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism());
//		alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.remove(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism());
//		tiSent.setAssertAbsenceInRefNodeContext(false);
//		tiSent.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
//		tiSent.setAssertPresenceInRefNodeContext(false);
//		tiSent.setRefGenomeInRefNodeContext(true);
		
	}
	
	
	private static void setDefaultRefGenomeWithSelectedRefTaxoNode() {
		
//		long milli = System.currentTimeMillis();
//		long milliPrint2 = -1;
//		milliPrint2 = System.currentTimeMillis() - milli;
//		System.out.println("Start stepToShow.compareTo(ReferenceCDSs");
	
		//clear all arrays
		clearAllTaxoNodeOrgaItemsInPhenoPlusCategory();
		clearAllTaxoNodeOrgaItemsInPhenoMinusCategory();
		clearAllTaxoNodeOrgaItemsNotTakenIntoAccountCategory();


		
		TaxoItem defaultNode = getTheFurthestNode(selectedRefTaxoNode, true);
		setRefOiFromSelectedTaxoNodeInRefTaxoNodeContext(defaultNode
				//,true
				//false
				);
		//deal with PresenceAbsenceBothOrUndef in whole taxonTree
		//Insyght.APP_CONTROLER.setPresenceAbsenceEitherOrUndefInTaxoTreeWithTaxoNodeChange(defaultNode, "RefTaxoNode");

		
		// select all
//		OLD selectChildrenOfTaxoItemRecursively(
//				selectedRefTaxoNode.getTaxoChildrens()
//				  , true, null);
		selectUnselectChildrenOfTaxoItemRecursively(
				selectedRefTaxoNode
				  , true);
		
		//set all in group +
		onClickBtCoreDispSetPhenotypePlus(rbCoreDispensableGeneSet.getValue());
		
		//deselect all
//		OLD selectChildrenOfTaxoItemRecursively(
//				selectedRefTaxoNode.getTaxoChildrens()
//				  , false, null);
		selectUnselectChildrenOfTaxoItemRecursively(
				selectedRefTaxoNode
				  , false);

//		alRefTaxoNodeOrgaItemsInPhenoPlusCategory.clear();
//		alRefTaxoNodeOrgaItemsInPhenoMinusCategory.clear();
//		alRefTaxoNodeOrgaItemsInNotTakenIntoAccountCategory.clear();


//		milliPrint2 = System.currentTimeMillis() - milli;
//		GWT.log(
//				"Done 5 in\t" + milliPrint2 + "\tmilliseconds"
//			);
		
		
		colorTaxoTreeInCoreDispGenomeContext(selectedRefTaxoNode);
		//Insyght.APP_CONTROLER.setPresenceAbsenceEitherOrUndefInTaxoTreeWithTaxoNodeChange(selectedRefTaxoNode, "Presence");
		//getGeneSetWithPresenceAbsenceInRefNodeContext
		
		selectATaxoItemInCoreDispBrowser(
				getRelativePathForCoreDispFromFullPath(defaultNode, selectedRefTaxoNode)
				, true);
		
		updateRelativeCountGenomesInPhenotypeCategories();
		
		//getGeneSetWithPresenceAbsenceInRefNodeContext();
		
		//selectATaxoItemInCoreDispBrowser(getRelativePathForCoreDispFromFullPath(defaultNode, selectedRefTaxoNode), false);
		//spStep3CoreDisp.scrollToBottom();
		
		//titleWizardSearch.setHTML("Selected reference taxonomic node : "


		MenuItems_ReferenceGenomeOrTaxonomicGroup.setTitle("Reference taxonomic node : "
				+ selectedRefTaxoNode.getFullName() + " (" + selectedRefTaxoNode.getSumOfLeafsUnderneath() + " organisms)."
						+ "\n"
						+ "Reference organism in node : " + Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
						);


		
			
	}
	
	public static void setOrganismItemInSingleRefOrgaContext(
			OrganismItem selected
			, boolean selectATaxoItemInMainTaxoBrowser
			, boolean setSelectInSelectionModel
			) {
		
		if (selected != null) {
			
			
			if (Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism() != selected) {

				setRefTaxoNode(null);
				
				if (Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism() != null) {
					manageTaxoItemsAndOrganismWithinGroupPlusMinus(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getAssociatedTaxoItem(), EnumGroupGenomes.reference, false);
				}
				Insyght.APP_CONTROLER.getUserSearchItem().setReferenceOrganism(selected);
				manageTaxoItemsAndOrganismWithinGroupPlusMinus(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getAssociatedTaxoItem(), EnumGroupGenomes.reference, true);
				
				//clear arrayGeneset
				//alLGIFromCoreDispRequest.clear();
				alLGIFromBuildGeneSet.clear();
				manageUserSearchItem_ListReferenceGeneSet(true, null, true, -1);
				
				// clean step3
				//step3VPRBCoreDispenTaxoNode.clear();
				//step3VPRBAllGeneOrga.clear();
				//step3VPRBElement.clear();
				refreshMoleculesFromBuildRefGeneSet(null);

				// clean error mssg if any
				errorOrWarningTextStepSelectOrga.clear();
//				errorOrWarningTextStepSelectOrga.setText("");
//				errorOrWarningTextStepSelectOrga.setStyleDependentName("fadeIn7Seconds", false);
				
//				// clean info text
//				clickToDisplayGeneInfo.setText("[click on a gene to display its info]");
//				htmlBasicGeneInfoInSearch.setText("");
//				htmlDetailledGeneInfoInSearch.setText("");

				// ALLOW_STEP3 = true;
				clearSelectionStep3(true, true);
				MenuItems_ReferenceGenomeOrTaxonomicGroup.setTitle("Selected reference organism : "+ selected.getFullName());
				//titleWizardSearch.setHTML("1. Selected reference organism : "+ selected.getFullName());
				
				sugBox.setText(selected.getFullName());

				if (selectATaxoItemInMainTaxoBrowser) {
					selectATaxoItemInMainTaxoBrowser(selected.getAssociatedTaxoItem().getFullPathInHierarchie(), setSelectInSelectionModel);//was false
				}
				
			}

		} else {
			
			if (Insyght.APP_CONTROLER.getUserSearchItem()
					.getReferenceOrganism() != null) {
				manageTaxoItemsAndOrganismWithinGroupPlusMinus(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getAssociatedTaxoItem(), EnumGroupGenomes.reference, false);
				Insyght.APP_CONTROLER.getUserSearchItem().setReferenceOrganism(null);
				roDataUploadedStepReferenceCDSs = null;
			}
			//titleWizardSearch.setHTML("1. Select a reference organism or taxonomic node");
			// clean step3
			//step3VPRBElement.clear();
			refreshMoleculesFromBuildRefGeneSet(null);
			clearSelectionStep3(true, true);
			MenuItems_ReferenceGenomeOrTaxonomicGroup.setTitle("Selected reference organism set to Null");
			alLGIFromBuildGeneSet.clear();
			manageUserSearchItem_ListReferenceGeneSet(true, null, true, -1);
		}

		//setEnableButtonFurtherStepOrAnalysisFromSelectRefOrgaOrTaxoNode();
		
	}


	private static void buildUIStepReferenceGenomeOrTaxonomicGroup() {

		// sugBox = new SuggestBox(oracle);
		sugBox.setWidth("80%");// 600px
		sugBox.setAutoSelectEnabled(false);
		sugBox.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					// Window.alert("addKeyDownHandler!");
					makeTheBestSelectionPossibleWithSearchTerm(sugBox.getText());
				}
			}
		});
		sugBox.addSelectionHandler(new SelectionHandler<Suggestion>() {
			// @Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				// selectLightElementItem(null);
				//selectAllMoleculesFromBuildRefGeneSet(false);
				selectionModel_moleculesFromBuildRefGeneSet.clear();
				// Window.alert("here : "+event.getSelectedItem().getReplacementString());
				// Window.alert("addSelectionHandler!");
				makeTheBestSelectionPossibleWithSearchTerm(event
						.getSelectedItem().getReplacementString());
			}
		});
		sugBox.getValueBox().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				sugBox.setText("");
				//errorOrWarningTextStepSelectOrga_CoreDisp.clear();
			}
		});
		step2VPSuggestBoxOrga.add(sugBox);
		HTMLPrivateProject.setVisible(false);
		step2VPPrivateOrga.setVisible(false);

//		@SuppressWarnings("unused")
//		SearchLoadingDialog lDiag = new SearchLoadingDialog("All_organisms");
		sld.loadAllPublicLightOganisms();
		sld.loadAllPublicMoleculesOfOrganisms();
		sld.loadTaxoTree();
		
		// default title set by
		// NOT NEEDED MenuItems_ReferenceGenomeOrTaxonomicGroup.setTitle("Default : ");, set in taxotree loading method and then on node selection
		

	}

	
	
	protected static void makeTheBestSelectionPossibleWithSearchTerm(
			String searchTerm) {
		
		OrganismItem oi = null;
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getListAllPublicOrganismItem().size(); i++) {
			OrganismItem oiIT = Insyght.APP_CONTROLER
					.getListAllPublicOrganismItem().get(i);
			if ( searchTerm.startsWith(oiIT.getFullName()) ) {
				oi = oiIT;
				break;
			}
		}
		if (oi != null) {
			errorOrWarningTextStepSelectOrga.clear();
			setOrganismItemInSingleRefOrgaContext(oi, true, true);
		} else {
			TaxoItem tiExactPathToBranch = Insyght.APP_CONTROLER
					.findPathInTaxoTreeWithTextString(searchTerm);
			if (tiExactPathToBranch != null) {
				errorOrWarningTextStepSelectOrga.clear();
				TaxoItem tiIT = getTheFurthestNode(tiExactPathToBranch, false);
				selectATaxoItemInMainTaxoBrowser(tiIT.getFullPathInHierarchie(), true);
				//setOrganismItemInSingleRefOrgaContext(null, false, false);// Repeat ?
			} else {
				TaxoItem tiClosestPathInTree = Insyght.APP_CONTROLER
						.findClosestPathInTaxoTreeWithTextString(searchTerm);
				if (tiClosestPathInTree != null) {				
					HTML htmlErrorOrWarningTextStepSelectOrga = new HTML("<span style=\"color:red;\">Exact match not found for **"
							+ searchTerm
							+ "**, the closest match is "+tiClosestPathInTree.getFullName()+"</span>");
					errorOrWarningTextStepSelectOrga.clear();
					errorOrWarningTextStepSelectOrga.add(htmlErrorOrWarningTextStepSelectOrga);
					htmlErrorOrWarningTextStepSelectOrga.setStyleDependentName("fadeIn7Seconds", true);
					
					TaxoItem tiIT = getTheFurthestNode(tiClosestPathInTree, false);
					selectATaxoItemInMainTaxoBrowser(tiIT.getFullPathInHierarchie(), true);
					//setOrganismItemInSingleRefOrgaContext(null, false, false);// Repeat ?
				} else {
					HTML htmlErrorOrWarningTextStepSelectOrga = new HTML("<span style=\"color:red;\">No match found for your search term</span>");
					errorOrWarningTextStepSelectOrga.clear();
					errorOrWarningTextStepSelectOrga.add(htmlErrorOrWarningTextStepSelectOrga);
					htmlErrorOrWarningTextStepSelectOrga.setStyleDependentName("fadeIn7Seconds", true);
				}
			}
		}
	}
	

	protected static void makeTheBestSelectionPossibleWithSearchTerm_CoreDisp(
			String searchTerm) {

		OrganismItem oi = null;
		for (int i = 0; i < Insyght.APP_CONTROLER.getListAllPublicOrganismItem().size(); i++) {
			OrganismItem oiIT = Insyght.APP_CONTROLER
					.getListAllPublicOrganismItem().get(i);
			if ( searchTerm.startsWith(oiIT.getFullName()) ) {
				oi = oiIT;
				break;
			}
		}
		if (oi != null) {
			ArrayList<Integer> alIT = getRelativePathForCoreDispFromFullPath(oi.getAssociatedTaxoItem(), selectedRefTaxoNode);
			if (alIT == null) {
				HTML htmlErrorOrWarningTextStepSelectOrga_CoreDisp = new HTML("<span style=\"color:red;\">This organism is not found under the selected reference node</span>");
				errorOrWarningTextStepSelectOrga_CoreDisp.clear();
				errorOrWarningTextStepSelectOrga_CoreDisp.add(htmlErrorOrWarningTextStepSelectOrga_CoreDisp);
				htmlErrorOrWarningTextStepSelectOrga_CoreDisp.setStyleDependentName("fadeIn7Seconds", true);
			} else {
				errorOrWarningTextStepSelectOrga_CoreDisp.clear();
				selectATaxoItemInCoreDispBrowser(alIT, true); //unselect afterward else valur checked if organism
			}
			
		} else {
			TaxoItem tiExactPathToBranch = Insyght.APP_CONTROLER.findPathInTaxoTreeWithTextString(searchTerm);
			if (tiExactPathToBranch != null) {
				TaxoItem tiIT = getTheFurthestNode(tiExactPathToBranch, false);
				ArrayList<Integer> alIT = getRelativePathForCoreDispFromFullPath(tiIT, selectedRefTaxoNode);
				if (alIT == null) {
					HTML htmlErrorOrWarningTextStepSelectOrga_CoreDisp = new HTML("<span style=\"color:red;\">This taxonomic node is not found under the selected reference node</span>");
					errorOrWarningTextStepSelectOrga_CoreDisp.clear();
					errorOrWarningTextStepSelectOrga_CoreDisp.add(htmlErrorOrWarningTextStepSelectOrga_CoreDisp);
					htmlErrorOrWarningTextStepSelectOrga_CoreDisp.setStyleDependentName("fadeIn7Seconds", true);
				} else {
					errorOrWarningTextStepSelectOrga_CoreDisp.clear();
					selectATaxoItemInCoreDispBrowser(alIT, false);
				}
			} else {
				TaxoItem tiClosestPathInTree = Insyght.APP_CONTROLER
						.findClosestPathInTaxoTreeWithTextString(searchTerm);
				if (tiClosestPathInTree != null) {
					TaxoItem tiIT = getTheFurthestNode(tiClosestPathInTree, false);
					ArrayList<Integer> alIT = getRelativePathForCoreDispFromFullPath(tiIT, selectedRefTaxoNode);
					if (alIT == null) {
						HTML htmlErrorOrWarningTextStepSelectOrga_CoreDisp = new HTML("<span style=\"color:red;\">Exact match not found for **"
								+ searchTerm
								+ "**, the closest match is "+tiClosestPathInTree.getFullName()
								+ " but it is not found under the selected reference node</span>");
						errorOrWarningTextStepSelectOrga_CoreDisp.clear();
						errorOrWarningTextStepSelectOrga_CoreDisp.add(htmlErrorOrWarningTextStepSelectOrga_CoreDisp);
						htmlErrorOrWarningTextStepSelectOrga_CoreDisp.setStyleDependentName("fadeIn7Seconds", true);
					} else {
						selectATaxoItemInCoreDispBrowser(alIT, false);
						HTML htmlErrorOrWarningTextStepSelectOrga_CoreDisp = new HTML("Exact match not found for **"
								+ searchTerm
								+ "**, the closest match is "+tiClosestPathInTree.getFullName());
						errorOrWarningTextStepSelectOrga_CoreDisp.clear();
						errorOrWarningTextStepSelectOrga_CoreDisp.add(htmlErrorOrWarningTextStepSelectOrga_CoreDisp);
						htmlErrorOrWarningTextStepSelectOrga_CoreDisp.setStyleDependentName("fadeIn7Seconds", true);
					}
				} else {
					HTML htmlErrorOrWarningTextStepSelectOrga_CoreDisp = new HTML("<span style=\"color:red;\">No match found for your search term</span>");
					errorOrWarningTextStepSelectOrga_CoreDisp.clear();
					errorOrWarningTextStepSelectOrga_CoreDisp.add(htmlErrorOrWarningTextStepSelectOrga_CoreDisp);
					htmlErrorOrWarningTextStepSelectOrga_CoreDisp.setStyleDependentName("fadeIn7Seconds", true);
				}
			}
		}
	}

	public static void selectATaxoItemInCoreDispBrowser(
			ArrayList<Integer> pathToSelect,
			boolean uncheckBoxAfterward //sometimes needed to show colors
			) {
		if( pathToSelect != null ){
			TreeNode tnIT = cellBrowserRefNodeTaxoBrowser.getRootTreeNode();
			for (int i = 0; i < pathToSelect.size(); i++) {
				int idxIT = pathToSelect.get(i);
				if (i == pathToSelect.size() - 1) {
					// last entry, should be leaf -> select
					final TaxoItem taxoItemToSelect = (TaxoItem) tnIT.getChildValue(idxIT);
					if(taxoItemToSelect.getAssociatedOrganismItem() != null){
						refNodeTaxoBrowserSelectionModel.setSelected(taxoItemToSelect,true);// select node
						if(uncheckBoxAfterward){
							Scheduler.get().scheduleDeferred(new ScheduledCommand() {
								// @Override
								public void execute() {
									refNodeTaxoBrowserSelectionModel.setSelected(taxoItemToSelect,false);// select node
								}
							});
						}
					}else{
						// open under it
						tnIT.setChildOpen(idxIT, false, false);// close before opening
						tnIT = tnIT.setChildOpen(idxIT, true, false);// open not leaf
					}
				} else {
					// not leaf -> open
					tnIT = tnIT.setChildOpen(idxIT, true, false);// open not leaf
					if (tnIT == null) {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in selectATaxoItem : tnIT == null"));
					}
				}
			}
		}
	}
	
	
	public static void selectATaxoItemInMainTaxoBrowser(
			ArrayList<Integer> pathToSelect
			, boolean setSelectInSelectionModel
			) {
		
		if(pathToSelect != null){
			
//			if(orgaBrowserPanelSelectionModel.getSelectedObject() != null){
//				if (orgaBrowserPanelSelectionModel.getSelectedObject().getFullPathInHierarchie().toString().compareTo(pathToSelect.toString())==0){
//					//already selected
//					if(orgaBrowserPanelSelectionModel.getSelectedObject().getAssociatedOrganismItem() == null){
//						setRefTaxoNode(orgaBrowserPanelSelectionModel.getSelectedObject());
//					}
//					return;
//				}
//			}
			
			TreeNode tnIT = cellBrowserTaxoAllOrga.getRootTreeNode();
			// close it at first
//			if (closeLeafsBeforeOpening) {
//				for (int i = 0; i < tnIT.getChildCount(); i++) {
//					tnIT.setChildOpen(i, false, false);
//				}
//			}

			for (int i = 0; i < pathToSelect.size(); i++) {
				int idxIT = pathToSelect.get(i);
				if (i == pathToSelect.size() - 1) {
					// last entry, should be leaf -> select
					
					
					if (setSelectInSelectionModel) {
						TaxoItem taxoItemToSelect = (TaxoItem) tnIT.getChildValue(idxIT);
						orgaBrowserPanelSelectionModel.setSelected(taxoItemToSelect,true);// select node
					}
					//open under it
					tnIT.setChildOpen(idxIT, false, false);// close before opening
					tnIT = tnIT.setChildOpen(idxIT, true, true);// open leaf
					
//					if(taxoItemToSelect.getAssociatedOrganismItem() == null){
//						setRefTaxoNode(taxoItemToSelect);
//					}
				} else {
					// not leaf -> open
					tnIT = tnIT.setChildOpen(idxIT, true, false);// open not leaf
					if (tnIT == null) {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in selectATaxoItem : tnIT == null"));
						//setRefTaxoNode(null);
					}
				}
			}
		}
	}
	
	
	public static void buildTaxoBrowserCoreDispGenome() {
		//TreeViewModel model = new CustomTreeModelCoreDispGenome();
		
		//final MultiSelectionModel<TaxoItem> selectionModel = new MultiSelectionModel<TaxoItem>();
		
		refNodeTaxoBrowserSelectionModel.addSelectionChangeHandler(
				
	        new SelectionChangeEvent.Handler() {
	          public void onSelectionChange(SelectionChangeEvent event) {
	        	  //find the last element clicked, selected or deselected
	        	  //triger when click box, not when click taxonomy to display tree
	        	  
	        	  //if( ! refNodeTaxoBrowserSelectionModel.getSelectedSet().isEmpty()){
	        	  if( ! doNotTriggerEventRefNodeTaxoBrowserSelectionModel ) {
	        	  
	        		  TaxoItem lastTIClicked = null;
		        	  Boolean addedToSElection = null;
		        	  if(selectedSetBeforeSelectionEvent.size() < refNodeTaxoBrowserSelectionModel.getSelectedSet().size()){
		        		  //added an element to selection
		        		  lastTIClicked = refNodeTaxoBrowserSelectionModel.getSelectedList().get(refNodeTaxoBrowserSelectionModel.getSelectedList().size()-1);
		        		  addedToSElection = true;
		        		  selectedSetBeforeSelectionEvent.add(lastTIClicked);
		        	  } else {
		        		  //removed an element to selection
		        		  ArrayList<TaxoItem> tmpTiSet = new ArrayList<TaxoItem>(selectedSetBeforeSelectionEvent);
		        		  tmpTiSet.removeAll(refNodeTaxoBrowserSelectionModel.getSelectedSet());
		        		  if(tmpTiSet.size() == 1){
			        		  lastTIClicked = tmpTiSet.get(0);
			        		  addedToSElection = false;
			        		  selectedSetBeforeSelectionEvent.remove(lastTIClicked);
		        		  }
		        	  }  

		        	  if(lastTIClicked != null
		        			  && addedToSElection != null
//		        			  &&  ! DO_NOT_TRIGGER_SELECTION_RECUR
		        			  ){
		        		  if( ! lastTIClicked.getTaxoChildrens().isEmpty()){
		        			  selectUnselectChildrenOfTaxoItemRecursively(lastTIClicked, addedToSElection);
//		        			  DO_NOT_TRIGGER_SELECTION_RECUR = true;
//		        			  selectChildrenOfTaxoItemRecursively(
//			        				  lastTIClicked.getTaxoChildrens()
//			        				  , addedToSElection, null);
//		        			  DO_NOT_TRIGGER_SELECTION_RECUR = false;
		        		  } else if (CellTaxoItem.isCellBrowserHeader(lastTIClicked)){
//		        			  selectChildrenOfTaxoItemRecursively(
//		        							  lastTIClicked.getParentTaxoItem().getTaxoChildrens()
//		    		        				  , true, null);
		        			  selectUnselectChildrenOfTaxoItemRecursively(lastTIClicked.getParentTaxoItem(), addedToSElection);
		        		  }
		        	  }
//		        	  else {
//		        		  Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
//		        				  new Exception("Error in buildTaxoBrowserCoreDispGenome : not lastTIClicked != null && addedToSElection != null"));	
//		        	  }
		        	  
	        	  }
	        	  
	          }
	        });
		TreeViewModel model = new CustomTreeModelCoreDispGenome(refNodeTaxoBrowserSelectionModel);
		
		/*
		 * Create the browser using the model.
		 */
		tiAsIntermediateRoot = new TaxoItem("Test", null, null);
		tiAsIntermediateRoot.getTaxoChildrens().add(selectedRefTaxoNode);
		Builder<TaxoItem> builder = new Builder<TaxoItem>(model,
				//selectedRefTaxoNode
				tiAsIntermediateRoot
				);
		builder.pageSize(5000);
		builder.pagerFactory(null);
		builder.loadingIndicator(new HTML("Loading..."));
		cellBrowserRefNodeTaxoBrowser = builder.build();
		cellBrowserRefNodeTaxoBrowser.setAnimationEnabled(true);
		cellBrowserRefNodeTaxoBrowser.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		cellBrowserRefNodeTaxoBrowser.setMinimumColumnWidth(200);
		cellBrowserRefNodeTaxoBrowser.setWidth("100%");
		cellBrowserRefNodeTaxoBrowser.setHeight("20em");
		
		cellBrowserRefNodeTaxoBrowser.addOpenHandler(new OpenHandler<TreeNode>() {
			@Override
			public void onOpen(OpenEvent<TreeNode> event) {
				openAutomaticFurthestNotBranchedNodeRefNodeTaxoBrowser(cellBrowserRefNodeTaxoBrowser.getRootTreeNode());
			}
		});
		vpRefNodeTaxoBrowser.clear();
		vpRefNodeTaxoBrowser.add(cellBrowserRefNodeTaxoBrowser);
		
	}
	
	
	protected static void openAutomaticFurthestNotBranchedNodeRefNodeTaxoBrowser(TreeNode treeNodeIT) {
		
		int indexOpenNode = -1;
		boolean hasAChildTaxoNameHeader = false;
		for (int i=0 ; i < treeNodeIT.getChildCount() ; i++ ) {
			if (i==0 && ! Insyght.APP_CONTROLER.ensureTaxoNameNotAHeader((TaxoItem) treeNodeIT.getChildValue(i)) ) {
				hasAChildTaxoNameHeader = true;
			}
			if (treeNodeIT.isChildOpen(i)) {
				indexOpenNode = i;
				break;
			}
		}
		
		//GWT.log("Done checking TreeNode = " + ( (TaxoItem) treeNodeIT.getValue()).getFullName() + " ; treeNodeIT.getChildCount = "+ treeNodeIT.getChildCount() + " ; hasAChildTaxoNameHeader = "+hasAChildTaxoNameHeader) ;
		
		TreeNode childTreeNodeToOpen = null;
		if (indexOpenNode > -1) {
			//at least one child open already
			if ( ! treeNodeIT.isChildLeaf(indexOpenNode)) {
				childTreeNodeToOpen = treeNodeIT.setChildOpen(indexOpenNode, true, false);
				//GWT.log("child already Open index = "+indexOpenNode+ " ; " + ((TaxoItem) childTreeNodeToOpen.getValue()).getFullName());
			}
		} else if (treeNodeIT.getChildCount() == 1 && ! hasAChildTaxoNameHeader ) {
			// no child open but only 1 children without header so automatic open possible
			if ( ! treeNodeIT.isChildLeaf(0)) {
				childTreeNodeToOpen = treeNodeIT.setChildOpen(0, true, false);
				//GWT.log("no child open but only 1 children without header so automatic open possible ; " + ((TaxoItem) childTreeNodeToOpen.getValue()).getFullName());
			}
		} else if (treeNodeIT.getChildCount() == 2 && hasAChildTaxoNameHeader ) {
			// no child open but only 2 children including header so automatic open possible
			if ( ! treeNodeIT.isChildLeaf(1)) {
				childTreeNodeToOpen = treeNodeIT.setChildOpen(1, true, false);
				//GWT.log("no child open but only 2 children including header so automatic open possible ; " + ((TaxoItem) childTreeNodeToOpen.getValue()).getFullName());
			}
		}
		if ( childTreeNodeToOpen != null ) {
			openAutomaticFurthestNotBranchedNodeRefNodeTaxoBrowser(childTreeNodeToOpen);
		}
	}


	//fill up ArrayList<TaxoItem> alRecursiveChildrenOfTaxoItemIT
	protected static void getRecursiveChildrenOfTaxoItem(
			TaxoItem taxoItemIT) {
		//ArrayList<TaxoItem> alToReturn = new ArrayList<>();
		if (taxoItemIT != null) {
			for( TaxoItem childToSelectAsWellIT : taxoItemIT.getTaxoChildrens() ){
//				if(CellTaxoItem.isCellBrowserHeader(childToSelectAsWellIT)){
//		  			  //do not select
//		  		} else {
				alRecursiveChildrenOfTaxoItemIT.add(childToSelectAsWellIT);
		  		//alToReturn.add(childToSelectAsWellIT);
				if ( ! childToSelectAsWellIT.getTaxoChildrens().isEmpty()) {
					getRecursiveChildrenOfTaxoItem(childToSelectAsWellIT);
				}
//		  		}
			}
		}
		//return alToReturn;
	}
	

	private static void setSelectedTaxoItemsInCoreDispBrowser(
			ArrayList<TaxoItem> alTaxoItemToSelectUnselect
			, Boolean select
			) {
		doNotTriggerEventRefNodeTaxoBrowserSelectionModel = true;
		for( TaxoItem tiToSelectUnselectIT : alTaxoItemToSelectUnselect ){
			if(tiToSelectUnselectIT != null){
		  		  if(CellTaxoItem.isCellBrowserHeader(tiToSelectUnselectIT)){
		  			  //do not select in any case
		  			  refNodeTaxoBrowserSelectionModel.setSelected(tiToSelectUnselectIT, false);
		  		  } else {
		  			  refNodeTaxoBrowserSelectionModel.setSelected(tiToSelectUnselectIT, select);
		  		  }
//		  		  if(select && ! refNodeTaxoBrowserSelectionModel.isSelected(tiToSelectUnselectIT)){
//		  			  refNodeTaxoBrowserSelectionModel.setSelected(tiToSelectUnselectIT, select);
//		  		  } else if ( ! select && refNodeTaxoBrowserSelectionModel.isSelected(tiToSelectUnselectIT)){
//		  			  refNodeTaxoBrowserSelectionModel.setSelected(tiToSelectUnselectIT, select);
//		  		  }
			}
		}
		doNotTriggerEventRefNodeTaxoBrowserSelectionModel = false;
		selectedSetBeforeSelectionEvent.clear();
		selectedSetBeforeSelectionEvent.addAll(refNodeTaxoBrowserSelectionModel.getSelectedSet());
	}


	protected static void selectUnselectChildrenOfTaxoItemRecursively(
			TaxoItem taxoItemIT
			, Boolean select) {
		if (taxoItemIT != null) {
			ArrayList<TaxoItem> alTaxoItemToSelectUnselect = new ArrayList<>();
			alTaxoItemToSelectUnselect.add(taxoItemIT);
			
			alRecursiveChildrenOfTaxoItemIT.clear();
			getRecursiveChildrenOfTaxoItem(taxoItemIT); // fill up alRecursiveChildrenOfTaxoItemIT
			alTaxoItemToSelectUnselect.addAll(alRecursiveChildrenOfTaxoItemIT);
			alRecursiveChildrenOfTaxoItemIT.clear();
			
			setSelectedTaxoItemsInCoreDispBrowser(alTaxoItemToSelectUnselect, select);
			
		}
	}
	
	// OLD, CHANGED TO selectUnselectChildrenOfTaxoItemRecursively
//	protected static void OLD_selectChildrenOfTaxoItemRecursively(
//			ArrayList<TaxoItem> childrenToSelectAsWell,
//			Boolean select,
//			ArrayList<TaxoItem> listTaxoItemToSelect
//			) {
//		
//		if(childrenToSelectAsWell == null){
//			return;
//		}
//		if(select != null){
//		  	  for( TaxoItem childToSelectAsWellIT : childrenToSelectAsWell ){
//		  		  if(CellTaxoItem.isCellBrowserHeader(childToSelectAsWellIT)){
//		  			  //do not select
//		  			refNodeTaxoBrowserSelectionModel.setSelected(childToSelectAsWellIT, false);
//		  		  } else if(select && ! refNodeTaxoBrowserSelectionModel.isSelected(childToSelectAsWellIT)){
//		  			  refNodeTaxoBrowserSelectionModel.setSelected(childToSelectAsWellIT, select);
//		  		  } else if ( ! select && refNodeTaxoBrowserSelectionModel.isSelected(childToSelectAsWellIT)){
//		  			  refNodeTaxoBrowserSelectionModel.setSelected(childToSelectAsWellIT, select);
//		  		  }
//				  if(childToSelectAsWellIT.getTaxoChildrens() != null && ! childToSelectAsWellIT.getTaxoChildrens().isEmpty()){
//					  selectChildrenOfTaxoItemRecursively(childToSelectAsWellIT.getTaxoChildrens(), select, null);
//				  }
//			  }
//		} else if (listTaxoItemToSelect != null){
//			for( TaxoItem childToSelectAsWellIT : childrenToSelectAsWell ){
//				if(CellTaxoItem.isCellBrowserHeader(childToSelectAsWellIT)){
//		  			  //do not select
//		  			refNodeTaxoBrowserSelectionModel.setSelected(childToSelectAsWellIT, false);
//		  		} else if(refNodeTaxoBrowserSelectionModel.isSelected(childToSelectAsWellIT)
//						&& listTaxoItemToSelect.contains(childToSelectAsWellIT)){
//					//ok correctly selected, do nothing
//				} else if (refNodeTaxoBrowserSelectionModel.isSelected(childToSelectAsWellIT)
//						&& ! listTaxoItemToSelect.contains(childToSelectAsWellIT)){
//					refNodeTaxoBrowserSelectionModel.setSelected(childToSelectAsWellIT, false);
//				} else if ( ! refNodeTaxoBrowserSelectionModel.isSelected(childToSelectAsWellIT)
//						&& listTaxoItemToSelect.contains(childToSelectAsWellIT)){
//					refNodeTaxoBrowserSelectionModel.setSelected(childToSelectAsWellIT, true);
//				} else if ( ! refNodeTaxoBrowserSelectionModel.isSelected(childToSelectAsWellIT)
//						&& ! listTaxoItemToSelect.contains(childToSelectAsWellIT)){
//					//ok correctly not selected
//				}
//				if(childToSelectAsWellIT.getTaxoChildrens() != null && ! childToSelectAsWellIT.getTaxoChildrens().isEmpty()){
//					  selectChildrenOfTaxoItemRecursively(childToSelectAsWellIT.getTaxoChildrens(), null, listTaxoItemToSelect);
//				}
//			}
//		}
//	}

	protected static ArrayList<Integer> getRelativePathForCoreDispFromFullPath(
			TaxoItem ti, TaxoItem selectedTaxoNodeAsSubstituteRoot) {
		ArrayList<Integer> alToReturn = new ArrayList<Integer>();

		String tiAsString = UtilitiesMethodsShared.getItemsAsStringFromCollection(ti.getFullPathInHierarchie());
		String selectedAsString = UtilitiesMethodsShared.getItemsAsStringFromCollection(selectedTaxoNodeAsSubstituteRoot.getFullPathInHierarchie());
		if (tiAsString.startsWith(selectedAsString)) {
			alToReturn.add(0);
			alToReturn.addAll(ti.getFullPathInHierarchie().subList(selectedTaxoNodeAsSubstituteRoot.getFullPathInHierarchie().size(),
					ti.getFullPathInHierarchie().size()));
			return alToReturn;
		} else {
			return null;
		}
	}

	public static void buildMainTaxoBrowser() {
		// Create a model for the browser.
		TreeViewModel model = new CustomTreeModel();
		orgaBrowserPanelSelectionModel.addSelectionChangeHandler(new Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {

				
				if (orgaBrowserPanelSelectionModel.getSelectedObject().getAssociatedOrganismItem() != null) {
					if(orgaBrowserPanelSelectionModel.getSelectedObject().getFullPathInHierarchie().toString().startsWith("[0, "+intRowPrivate+",")){
						//private
						for (int i = 0; i < Insyght.APP_CONTROLER.getCurrentUser()
								.getListGroupsSubscribed().size(); i++) {
							GroupUsersObj guo = Insyght.APP_CONTROLER.getCurrentUser().getListGroupsSubscribed().get(i);
							if(orgaBrowserPanelSelectionModel.getSelectedObject().getNcbiTaxoId()
									== guo.getTaxonId()){
								//found it
								setOrganismItemInSingleRefOrgaContext(guo, false, false);
								break;
							}
						}
					}else{
						
						//non private
						if (orgaBrowserPanelSelectionModel.getSelectedObject()
								.getNcbiTaxoId() > 0) {
							//System.err.println("here1");
							OrganismItem selectedOi = orgaBrowserPanelSelectionModel.getSelectedObject().getAssociatedOrganismItem();
							setOrganismItemInSingleRefOrgaContext(selectedOi, false, false);
						}else{
							makeTheBestSelectionPossibleWithSearchTerm(orgaBrowserPanelSelectionModel.getSelectedObject().getName());
						}
					}
				} else {			
					TaxoItem ti = getTheFurthestNode(orgaBrowserPanelSelectionModel.getSelectedObject(), false);
					boolean furthestNodeDifferentThanSelected = false;
					if ( orgaBrowserPanelSelectionModel.getSelectedObject().getFullPathInHierarchie().toString().compareTo(ti.getFullPathInHierarchie().toString()) != 0 ) {
						furthestNodeDifferentThanSelected = true;
					}
					
					if(ti.getAssociatedOrganismItem() == null){
						//sugBox.setText(ti.getFullName());
						//setOrganismItemInSingleRefOrgaContext(null);
						selectATaxoItemInMainTaxoBrowser(ti.getFullPathInHierarchie(), false);
						
						setRefTaxoNode(ti);
					} else {
						setOrganismItemInSingleRefOrgaContext(ti.getAssociatedOrganismItem(), furthestNodeDifferentThanSelected, furthestNodeDifferentThanSelected);
					}
				}
			}
		});

		/*
		 * Create the browser using the model.
		 */
		Builder<TaxoItem> builder = new Builder<TaxoItem>(model,
				Insyght.APP_CONTROLER.getTaxoTree());
		builder.pageSize(5000);
		builder.pagerFactory(null);
		builder.loadingIndicator(new HTML("Loading..."));
		cellBrowserTaxoAllOrga = builder.build();
		cellBrowserTaxoAllOrga.setAnimationEnabled(true);
		cellBrowserTaxoAllOrga.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		// cellBrowser.setDefaultColumnWidth(200);
		cellBrowserTaxoAllOrga.setMinimumColumnWidth(200);
		cellBrowserTaxoAllOrga.setWidth("100%");
		cellBrowserTaxoAllOrga.setHeight("20em");
		orgaBrowserPanel.clear();
		orgaBrowserPanel.add(cellBrowserTaxoAllOrga);

	}


	protected static TaxoItem getTheFurthestNode(TaxoItem selectedObject,
			boolean autoSelectFirstNodeWhenMultipleChoice) {
		//boolean doAutoSelectChild = false;
		int toAddIfColumnTitle = 0;
		TaxoItem tiToReturn = null;
		
		//if(!orgaBrowserPanelSelectionModel.getSelectedObject().isSelectableAsOrganism()){
		//if(!selectedObject.isSelectableAsOrganism()){
		int countTmp = 0;
		for(int i=0;i<selectedObject.getTaxoChildrens().size();i++){
			TaxoItem tiIT = selectedObject.getTaxoChildrens().get(i);
			if((tiIT.getName().compareTo("Domain") == 0
						|| tiIT.getName().compareTo("Phylum") == 0
						|| tiIT.getName().compareTo("Class") == 0
						|| tiIT.getName().compareTo("Order") == 0
						|| tiIT.getName().compareTo("Family") == 0
						|| tiIT.getName().compareTo("Genus") == 0
						|| tiIT.getName().compareTo("Species") == 0)) {
				toAddIfColumnTitle = 1;
				continue;
				
			}
			countTmp++;
			if(countTmp > 2){
				break;
			}
		}
		if(countTmp == 1){
			if(selectedObject.getTaxoChildrens().get(0+toAddIfColumnTitle).getAssociatedOrganismItem() != null){
				tiToReturn = selectedObject.getTaxoChildrens().get(0+toAddIfColumnTitle);
			}else{
				tiToReturn = getTheFurthestNode(selectedObject.getTaxoChildrens().get(0+toAddIfColumnTitle), autoSelectFirstNodeWhenMultipleChoice);
			}
		}else{
			if(!autoSelectFirstNodeWhenMultipleChoice){
				tiToReturn = selectedObject;
			}else{
				//System.err.println("autoSelectFirstNodeWhenMultipleChoice");
				if(selectedObject.getAssociatedOrganismItem() != null){
					//System.err.println("isSelectableAsOrganism : "+selectedObject.getFullName());
					tiToReturn = selectedObject;
				}else if(selectedObject.getSumOfLeafsUnderneath()==1){
					//System.err.println("getSumOfLeafsUnderneath()==1 : "+selectedObject.getFullName());
					tiToReturn = getTheFurthestNode(selectedObject.getTaxoChildrens().get(0+toAddIfColumnTitle), false);
				}else{
					//System.err.println("keep going with "+selectedObject.getTaxoChildrens().get(0+toAddIfColumnTitle).getFullName());
					tiToReturn = getTheFurthestNode(selectedObject.getTaxoChildrens().get(0+toAddIfColumnTitle), true);
				}
			}
		}
		//}
		return tiToReturn;
	}
	
	

	
	
	/**
	 * The model that defines the nodes in the tree.
	 */
	private static class CustomTreeModel implements TreeViewModel {

		private Cell<TaxoItem> cellTaxoItem = new AbstractCell<TaxoItem>() {
			@Override
			public void render(Context context, TaxoItem value,
					SafeHtmlBuilder sb) {
				if (value != null) {
					String postFix = "";
//					if (value.isSelectableAsOrganism()) {
//						if (value.getNcbiTaxoId() > 0) {
//							postFix += " [tax_id = " + value.getNcbiTaxoId()
//									+ "]";
//						}
//					}
					if (value.getName().compareTo("Unknown lineage") == 0) {
						postFix += " (" + value.getTaxoChildrens().size() + ")";
					} else if (!value.getTaxoChildrens().isEmpty()) {
						postFix += " (" + value.getSumOfLeafsUnderneath() + ")";
					}
					
					if (context.getIndex() == 0
							&& (value.getName().compareTo("Domain") == 0
									|| value.getName().compareTo("Phylum") == 0
									|| value.getName().compareTo("Class") == 0
									|| value.getName().compareTo("Order") == 0
									|| value.getName().compareTo("Family") == 0
									|| value.getName().compareTo("Genus") == 0 || value
									.getName().compareTo("Species") == 0)) {

						String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
								+ value.getFullName()
								+ postFix
								+ "</center></span>";
						sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
					} else {
						sb.appendEscaped(value.getFullName()
								+ postFix);
					}

				}
			}
		};

		/**
		 * Get the {@link NodeInfo} that provides the children of the specified
		 * value.
		 */
		public <T> NodeInfo<?> getNodeInfo(T value) {

			if (value == null) {
				// null = error.

				// Create a data provider that says loading.
				final ArrayList<String> listLoading = new ArrayList<String>();
				listLoading.add("Taxonomic tree unavailable.");
				ListDataProvider<String> dataProvider = new ListDataProvider<String>(
						listLoading);
				return new DefaultNodeInfo<String>(dataProvider, new TextCell()
				// ,orgaBrowserPanelSelectionModelNotLeaf, null
				);

			} else if (value instanceof TaxoItem) {
				// TaxoItem.
				ListDataProvider<TaxoItem> dataProvider = new ListDataProvider<TaxoItem>(
						((TaxoItem) value).getTaxoChildrens());

				// Return a node info that pairs the data provider and the cell.
				return new DefaultNodeInfo<TaxoItem>(dataProvider,
						cellTaxoItem, orgaBrowserPanelSelectionModel, null);

			}

			return null;

		}

		/**
		 * Check if the specified value represents a leaf node. Leaf nodes
		 * cannot be opened.
		 */
		public boolean isLeaf(Object value) {
			if (value instanceof String) {
				return true;
			}
			// The leaf nodes are the TaxoItem.
			if (value instanceof TaxoItem) {
				if (((TaxoItem) value).getTaxoChildrens().isEmpty()) {
					return true;
				}
			}
			return false;
		}
	}
	

	/** add a filter relative to special score, par ex PValOverReprPheno or CountMostRepresPheno **/
	protected static void addSpecScoreCoreFilter(
			final VerticalPanel ssFp) {
		
		final ListBox giLbChoiceSurrMatch = new ListBox();
		giLbChoiceSurrMatch.addItem("greater than", "greater_than");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(0))
				.setTitle(
						"Find genes for which the filter's field is greater than your search term.");
		giLbChoiceSurrMatch.addItem("lower than", "lower_than");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(1))
				.setTitle(
						"Find genes for which the filter's field is lower than your search term.");
//		giLbChoiceSurrMatch.addItem("is null", "is_null");
//		Element.as(giLbChoiceSurrMatch.getElement().getChild(2))
//				.setTitle(
//						"Find genes for which the filter's field is null.");
		giLbChoiceSurrMatch.addItem("is not null", "is_not_null");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(2))
				.setTitle(
						"Find genes for which the filter's field is not null.");

		final TextBox gidTb = new TextBox();
		gidTb.setWidth("11em"); // 11em
		
		giLbChoiceSurrMatch.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if (
//						giLbChoiceSurrMatch.getSelectedValue().compareTo("is_null")==0
//						|| 
						giLbChoiceSurrMatch.getSelectedValue().compareTo("is_not_null")==0) {
					gidTb.setVisible(false);
					ssFp.getWidget((ssFp.getWidgetCount() - 1)).setVisible(
							false);
				} else {
					gidTb.setVisible(true);
					ssFp.getWidget((ssFp.getWidgetCount() - 1))
							.setVisible(true);
				}

				if (giLbChoiceSurrMatch.getSelectedValue().compareTo("greater_than") == 0) {
					// greater than
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field is greater than your search term.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("lower_than") == 0) {
					// lower than
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field is lower than your search term.");
//				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("is_null") == 0) {
//					// is null
//					giLbChoiceSurrMatch
//							.setTitle("Find genes for which the filter's field is null.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("is_not_null") == 0) {
					// is not null
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field is not null.");
				}
			}
		});

		ssFp.add(giLbChoiceSurrMatch);
		ssFp.add(gidTb);
		giLbChoiceSurrMatch.setItemSelected(0, true);
		giLbChoiceSurrMatch.setTitle("Find genes for which the filter's field is greater than your search term.");

	}
	
	/** add a filter relative to lenght aa **/
	protected static void addLpCoreFilter(
			//boolean firstFilter,
			final VerticalPanel gbFp) {

		HTML gbHTMLStart = new HTML("greater than (aa lenght) :");
		gbHTMLStart.setStyleDependentName("cursorQuestionMark", true);
		gbHTMLStart.setTitle("List genes whose protein lenght are within a given boundaries. \"greater than\" and \"lower than\" must be entered in lenght of amino acids.");
		TextBox gbTbStart = new TextBox();
		gbTbStart.setWidth("11em");
		gbTbStart.setText("0");

		HTML gbHTMLStop = new HTML("lower than (aa lenght) :");
		gbHTMLStop.setStyleDependentName("cursorQuestionMark", true);
		gbHTMLStop.setTitle("List genes whose protein lenght are within a given boundaries. \"greater than\" and \"lower than\" must be entered in lenght of amino acids.");
		TextBox gbTbStop = new TextBox();
		gbTbStop.setWidth("11em");
		gbTbStop.setText("MAX_LENGHT");
		
		gbFp.add(gbHTMLStart);
		gbFp.add(gbTbStart);
		gbFp.add(gbHTMLStop);
		gbFp.add(gbTbStop);
	}

	/** add a filter relative to genomic location **/
	protected static void addGbCoreFilter(
			//boolean firstFilter,
			final VerticalPanel gbFp, boolean defaultZeroAndMax) {

		
		HTML gbHTMLStart = new HTML("start (bp relative to ORI) :");
		gbHTMLStart.setStyleDependentName("cursorQuestionMark", true);
		gbHTMLStart.setTitle("List genes whose genomic locations are within a given boundaries. Start and stop must be entered in base pairs relative to the origin of replication ORI.");

		TextBox gbTbStart = new TextBox();
		gbTbStart.setWidth("11em"); // 11em
		if(defaultZeroAndMax){
			gbTbStart.setText("0");
		}
		

		HTML gbHTMLStop = new HTML("stop (bp relative to ORI) :");
		gbHTMLStop.setStyleDependentName("cursorQuestionMark", true);
		gbHTMLStop.setTitle("List genes whose genomic locations are within a given boundaries. Start and stop must be entered in base pairs relative to the origin of replication ORI.");
		
		TextBox gbTbStop = new TextBox();
		gbTbStop.setWidth("11em");
		if(defaultZeroAndMax){
			gbTbStop.setText("MAX_STOP");
		}
		
		gbFp.add(gbHTMLStart);
		gbFp.add(gbTbStart);
		gbFp.add(gbHTMLStop);
		gbFp.add(gbTbStop);
		
	}

	/** ajouter un filtre qualifiers **/
	protected static void addGiCoreFilter(
			//boolean firstFilter,
			final VerticalPanel giFp, final String value) {

		final ListBox giLbChoiceSurrMatch = new ListBox();
		giLbChoiceSurrMatch.addItem("containing", "containing");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(0))
				.setTitle(
						"Find genes for which the filter's field contain your search term at any position.");
		giLbChoiceSurrMatch.addItem("not containing", "not_containing");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(1))
				.setTitle(
						"Find genes for which the filter's field does not contain your search term anywhere.");
		giLbChoiceSurrMatch.addItem("starting with", "starting_with");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(2))
				.setTitle(
						"Find genes for which the filter's field starts with your search term.");
		giLbChoiceSurrMatch.addItem("not starting with", "not_starting_with");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(3))
				.setTitle(
						"Find genes for which the filter's field does not start with your search term.");
		giLbChoiceSurrMatch.addItem("ending with", "ending_with");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(4))
				.setTitle(
						"Find genes for which the filter's field ends with your search term.");
		giLbChoiceSurrMatch.addItem("not ending with", "not_ending_with");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(5))
				.setTitle(
						"Find genes for which the filter's field does not end with your search term.");
		giLbChoiceSurrMatch.addItem("strictly", "strictly");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(6))
				.setTitle(
						"Find genes for which the filter's field matches strictly your search term.");
		giLbChoiceSurrMatch.addItem("strictly not", "strictly_not");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(7))
				.setTitle(
						"Find genes for which the filter's field matches everything but strictly your search term.");
		giLbChoiceSurrMatch.addItem("is null", "is_null");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(8))
				.setTitle(
						"Find genes for which the current filter has no value or a null value.");
		giLbChoiceSurrMatch.addItem("is not null", "is_not_null");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(9))
				.setTitle(
						"Find genes for which the current filter has a value of any kind.");
		giLbChoiceSurrMatch.addItem("POSIX Regex", "POSIX_Regex");
		Element.as(giLbChoiceSurrMatch.getElement().getChild(10))
				.setTitle(
						"Enable the Posix regular expression mode: you can build search pattern by using a special set of caracters. For example . (DOT) matches any single caractere, * (MULTIPLY BY) matches the preceding element zero or more times, etc... See the Posix regular expression specification for more information.");

		final TextBox gidTb = new TextBox();
		gidTb.setWidth("11em"); // 11em

		giLbChoiceSurrMatch.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if (giLbChoiceSurrMatch.getSelectedValue().compareTo("is_null")==0
						|| giLbChoiceSurrMatch.getSelectedValue().compareTo("is_not_null")==0) {
					// IS_NOT_NULL
					gidTb.setVisible(false);
					giFp.getWidget((giFp.getWidgetCount() - 1)).setVisible(
							false);
				} else {
					gidTb.setVisible(true);
					giFp.getWidget((giFp.getWidgetCount() - 1))
							.setVisible(true);
				}

				if (giLbChoiceSurrMatch.getSelectedValue().compareTo("containing")==0) {
					// containing
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field contain your search term at any position.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("not_containing")==0) {
					// not containing
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field does not contain your search term anywhere.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("starting_with")==0) {
					// starting with
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field starts with your search term.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("not_starting_with")==0) {
					// not starting with
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field does not start with your search term.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("ending_with")==0) {
					// ending with
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field ends with your search term.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("not_ending_with")==0) {
					// not ending with
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field does not end with your search term.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("strictly")==0) {
					// strictly
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field matches strictly your search term.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("strictly_not")==0) {
					// strictly not
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the filter's field matches everything but strictly your search term.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("is_null")==0) {
					// is null
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the current filter has no value or a null value.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("is_not_null")==0) {
					// is not null
					giLbChoiceSurrMatch
							.setTitle("Find genes for which the current filter has a value of any kind.");
				} else if (giLbChoiceSurrMatch.getSelectedValue().compareTo("POSIX_Regex")==0) {
					// POSIX Regex
					giLbChoiceSurrMatch
							.setTitle("Enable the Posix regular expression mode: you can build search pattern by using a special set of caracters. For example . (DOT) matches any single caractere, * (MULTIPLY BY) matches the preceding element zero or more times, etc... See the Posix regular expression specification for more information.");
				}

			}
		});

		giFp.add(giLbChoiceSurrMatch);
		giFp.add(gidTb);
		giLbChoiceSurrMatch.setItemSelected(0, true);
		giLbChoiceSurrMatch.setTitle("Find genes for which the filter's field contain your search term at any position.");

	}


	/** add a filter relative to absence / presence homology **/
	protected static void addHoCoreFilter(
			//boolean firstFilter,
			final VerticalPanel hoFp) {

		final ListBox hoLbChoiceInNotIn = new ListBox();
		hoLbChoiceInNotIn.addItem("presence", "presence");
		Element.as(hoLbChoiceInNotIn.getElement().getChild(0))
				.setTitle(
						"Find genes for which there is at least one homolog in each given compared genomes.");

		hoLbChoiceInNotIn.addItem("absence", "absence");
		Element.as(hoLbChoiceInNotIn.getElement().getChild(1))
				.setTitle(
						"Find genes for which there is no homolog in any of the given compared genomes.");

		hoLbChoiceInNotIn.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if (hoLbChoiceInNotIn.getSelectedValue().compareTo("presence") == 0) {
					// presence
					hoLbChoiceInNotIn
							.setTitle("Find genes for which there is at least one homolog in each given compared genomes.");
				} else if (hoLbChoiceInNotIn.getSelectedValue().compareTo("absence") == 0) {
					// absence
					hoLbChoiceInNotIn
							.setTitle("Find genes for which there is no homolog in any of the given compared genomes.");
				}
			}
		});

		MultiValueSuggestBox mvsb = new MultiValueSuggestBox(oracle, "blue",
				"13em", 50);

		hoFp.add(hoLbChoiceInNotIn);
		final HTML htmlHomo = new HTML("of homologs in organism(s) :");
		htmlHomo.setStyleDependentName("cursorQuestionMark", true);
		htmlHomo.setTitle("BLAST is used to compare all pairs of genes. The best bi-directional hits (BBDH) with an e-value lower than 0.01 are categorized as orthologs. Multiple \"compared\" organisms can be entered simultaneously to obtain the core genome (modifiers presence) or niche specific genes (modifiers absence).");
		hoFp.add(htmlHomo);
		hoFp.add(mvsb);

		// default
		hoLbChoiceInNotIn.setItemSelected(0, true);
		hoLbChoiceInNotIn.setTitle("Find genes for which there is at least one homolog in each given compared genomes.");

	}

	/**
	 * main method to add a filter to the filter box, can be called multiple
	 * times
	 **/
	public static void addARootFilter(boolean firstFilter,
			final VerticalPanel vpFilterToAddTo, boolean defaultZeroAndMaxForGenomicLocFilter) {

		// Make a new list box, adding a few items to it.

		final ListBox lbRootFilter = new ListBox();
		lbRootFilter.setWidth("18em");
		final DeckPanel deckPRootFilter = new DeckPanel();
		deckPRootFilter.setStylePrimaryName("gwt-DeckPanel-paddingLeft");

		lbRootFilter.addItem("Genomic locations", "Genomic_locations");
		Element.as(lbRootFilter.getElement().getChild(0)).setTitle("List genes whose genomic locations are within a given boundaries. Start and stop must be entered in base pairs relative to the origin of replication ORI.");
		final VerticalPanel gbFp = new VerticalPanel();
		gbFp.setSpacing(2);
		gbFp.setWidth("100%");
		addGbCoreFilter(
				//true, 
				gbFp, defaultZeroAndMaxForGenomicLocFilter);
		deckPRootFilter.add(gbFp);

		lbRootFilter.addItem("Presence / absence homology","Presence_absence_homology");
		Element.as(lbRootFilter.getElement().getChild(1)).setTitle("BLAST is used to compare all pairs of genes. The best bi-directional hits (BBDH) with an e-value lower than 0.01 are categorized as orthologs. Multiple \"compared\" organisms can be entered simultaneously to obtain the core genome (modifiers presence) or niche specific genes (modifiers absence).");
		final VerticalPanel hoFp = new VerticalPanel();
		hoFp.setSpacing(2);
		hoFp.setWidth("100%");
		addHoCoreFilter(
				//true, 
				hoFp);
		deckPRootFilter.add(hoFp);


		lbRootFilter.addItem("Lenght of protein", "Lenght_protein");
		Element.as(lbRootFilter.getElement().getChild(0)).setTitle("List genes whose protein lenght are within a given boundaries. \"greater than\" and \"lower than\" must be entered in lenght of amino acids.");
		final VerticalPanel lpFp = new VerticalPanel();
		lpFp.setSpacing(2);
		lpFp.setWidth("100%");
		addLpCoreFilter(
				//true, 
				lpFp);
		deckPRootFilter.add(lpFp);
		
		addNewQualifiersFilter(
				"Identifier",
				"Gene_name",
				lbRootFilter,
				deckPRootFilter,
				"Valid identifiers are gene name / synonymous, locus tag, EMBL or Uniprot Ids. Example: DnaA. BSU00680, P37472.");
		addNewQualifiersFilter(
				"Molecular function",
				"Molecular_function",
				lbRootFilter,
				deckPRootFilter,
				"Filter by Gene Ontology keywords or identifiers relative to the gene's molecular function. Example: atp binding, GO:0004422. See the Gene Ontology specification for more information.");
		addNewQualifiersFilter(
				"Biological process",
				"Biological_process",
				lbRootFilter,
				deckPRootFilter,
				"Filter by Gene Ontology keywords or identifiers relative to the gene's biological process. Example: DNA replication, GO:0006400. See the Gene Ontology specification for more information.");
		addNewQualifiersFilter(
				"EC number",
				"EC_number",
				lbRootFilter,
				deckPRootFilter,
				"Filter by the Enzyme Commission number (EC number). Example: start with 1.2. See the Enzyme Commission number website for more information.");
		addNewQualifiersFilter(
				"Product",
				"Notes_product",
				lbRootFilter,
				deckPRootFilter,
				"Filter by the qualifier \"Product\". This field may contain various information such as the protein name, putative molecular function, enzymatic reaction, etc...");
		addNewQualifiersFilter(
				"Cellular component",
				"Cellular_component",
				lbRootFilter,
				deckPRootFilter,
				"Filter by Gene Ontology keywords or identifiers relative to the gene's cellular location. Example: cytoplasm, GO:0016021. See the Gene Ontology specification for more information.");
		addNewQualifiersFilter(
				"db_xref",
				"db_xref",
				lbRootFilter,
				deckPRootFilter,
				"List of supported external identifiers are: InterPro, HOGENOM, HSSP, GO, SubtiList, PDB, Rfam, UniParc, and TIGR.");
		addNewQualifiersFilter(
				"Type of evidence",
				"Evidence",
				lbRootFilter,
				deckPRootFilter,
				"List of supported types of evidence are : \"Evidence at protein level\", \"Evidence at transcript level\", \"Inferred from homology\", \"Predicted\". Sometimes this field is simply not empty with a reference to a protein identifier for example.");
		
		if(SessionStorageControler.CountMostRepresPheno_REF_ORGA_ID > 0){
			lbRootFilter.addItem("Count most represented group +", "CountMostRepresPheno");
			Element.as(lbRootFilter.getElement().getChild(0)).setTitle("List genes according to ortholog's count whithin the selected organisms group +. \"greater than\" and \"lower than\" must be entered relative to the count score.");
			final VerticalPanel cmrpFp = new VerticalPanel();
			cmrpFp.setSpacing(2);
			cmrpFp.setWidth("100%");
			addSpecScoreCoreFilter(
					cmrpFp);
			deckPRootFilter.add(cmrpFp);
		}
		if(SessionStorageControler.PValOverReprPheno_REF_ORGA_ID > 0){
			lbRootFilter.addItem("P-Value over-represented group +", "PValOverReprPheno");
			Element.as(lbRootFilter.getElement().getChild(0)).setTitle("List genes according to the P-Value of orthologs' over-representation in group + and under-representation in group -. \"greater than\" and \"lower than\" must be entered relative to the P-Value score.");
			final VerticalPanel pvorFp = new VerticalPanel();
			pvorFp.setSpacing(2);
			pvorFp.setWidth("100%");
			addSpecScoreCoreFilter(
					pvorFp);
			deckPRootFilter.add(pvorFp);
		}
		
		lbRootFilter.setVisibleItemCount(1);

		lbRootFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				deckPRootFilter.showWidget(lbRootFilter.getSelectedIndex());
				// if no filter, disable new filter button

				if (lbRootFilter.getSelectedValue().compareTo("Genomic_locations")==0) {
					// Genomic locations
					lbRootFilter
							.setTitle("Query genes whose genomic locations are within a given boundaries. Start and stop must be entered in base pairs relative to the origin of replication ORI.");
				} else if (lbRootFilter.getSelectedValue().compareTo("Presence_absence_homology")==0) {
					// Presence / absence of homology
					lbRootFilter
							.setTitle("Query genes with presence or absence of homologs in a compared species or species under given taxonomic nodes. The core genome is defined as genes having homologs in all the compared organisms under consideration. The disposable gene set is defined as any combination that imply the absence of homologs in at least one compared species.");
				} else if (lbRootFilter.getSelectedValue().compareTo("Gene_name")==0) {
					// Identifier
					lbRootFilter
							.setTitle("Query genes according to name, synonymous, locus tag, EMBL or Uniprot identifiers. Example: DnaA. BSU00680, P37472. Wherever you are allowed to freely enter a search term, a few modifiers are made accessible to contextualize your search: containing, not containing, starting with, not starting with, ending with, not ending with, strictly, strictly not, is null, is not null, and Posix regex. Posix regular expressions allows to perform more complex query by creating a pattern that can match multiple terms. Please check a resource on the internet for the list of supported posix regular expressions (i.e. https://en.wikipedia.org/wiki/Regular_expression).");
				} else if (lbRootFilter.getSelectedValue().compareTo("Molecular_function")==0) {
					// molecular Function
					lbRootFilter
							.setTitle("Filter by Gene Ontology keywords or identifiers relative to the gene's molecular function. Example: atp binding, GO:0004422. See the Gene Ontology specification for more information. A few modifiers are made accessible to contextualize your search (containing, not containing, etc.).");
				} else if (lbRootFilter.getSelectedValue().compareTo("Biological_process")==0) {
					// Biological process
					lbRootFilter
							.setTitle("Query genes by Gene Ontology keywords or identifiers relative to the gene's biological process. Example: DNA replication, GO:0006400. See the Gene Ontology specification for more information. A few modifiers are made accessible to contextualize your search (containing, not containing, etc.).");
				} else if (lbRootFilter.getSelectedValue().compareTo("EC_number")==0) {
					// EC number
					lbRootFilter
							.setTitle("Query genes by the Enzyme Commission number (EC number). Example: start with 1.2. See the Enzyme Commission number website for more information. A few modifiers are made accessible to contextualize your search (containing, not containing, etc.).");
				} else if (lbRootFilter.getSelectedValue().compareTo("Notes_product")==0) {
					// Product
					lbRootFilter
							.setTitle("Query genes by the qualifier \"Product\". This field may contain various information such as the protein name, putative molecular function, enzymatic reaction, etc. A few modifiers are made accessible to contextualize your search (containing, not containing, etc.).");
				} else if (lbRootFilter.getSelectedValue().compareTo("Cellular_component")==0) {
					// Cellular component
					lbRootFilter
							.setTitle("Query genes by Gene Ontology keywords or identifiers relative to the gene's cellular location. Example: cytoplasm, GO:0016021. See the Gene Ontology specification for more information. A few modifiers are made accessible to contextualize your search (containing, not containing, etc.).");
				} else if (lbRootFilter.getSelectedValue().compareTo("db_xref")==0) {
					// db_xref
					lbRootFilter
							.setTitle("List of supported external identifiers are: InterPro, HOGENOM, HSSP, GO, SubtiList, PDB, Rfam, UniParc, and TIGR.");
				} else if (lbRootFilter.getSelectedValue().compareTo("Evidence")==0) {
					// Type of evidence
					lbRootFilter
							.setTitle("List of supported types of evidence are : \"Evidence at protein level\", \"Evidence at transcript level\", \"Inferred from homology\", \"Predicted\". Sometimes this field is simply not empty with a reference to a protein identifier for example.");
				} else if (lbRootFilter.getSelectedValue().compareTo("Lenght_protein")==0) {
					// Type of evidence
					lbRootFilter
							.setTitle("Filter by the lenght of the protein.");
				} else if (lbRootFilter.getSelectedValue().compareTo("CountMostRepresPheno")==0){
					//CountMostRepresPheno
					lbRootFilter
					.setTitle("Filter by the ortholog's count whithin the selected organisms group +.");
				} else if (lbRootFilter.getSelectedValue().compareTo("PValOverReprPheno")==0){
					//CountMostRepresPheno
					lbRootFilter
					.setTitle("Filter by the P-Value of orthologs' over-representation in group + and under-representation in group -.");
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
							new Exception("Error in lbRootFilter.addChangeHandler: type of filter not found: "+lbRootFilter.getSelectedValue()));
				}

			}
		});

		if (firstFilter) {
			vpFilterToAddTo.add(lbRootFilter);
			// default selection
			lbRootFilter.setItemSelected(0, true);
			deckPRootFilter.showWidget(0);
			lbRootFilter.setTitle("List genes whose genomic locations are within a given boundaries. Start and stop must be entered in base pairs relative to the origin of replication ORI.");

			vpFilterToAddTo.add(deckPRootFilter);
		} else {

			final HorizontalPanel hp = new HorizontalPanel();
			hp.setSpacing(5);

			final ListBox lbRootOperator = new ListBox();
			lbRootOperator.setTitle(
					"Choose the type of operator for linking two or more filters."
							+ " The dots represent the arguments to be linked by the union (AND) or the intersection (OR) operators."
							+ " When the parenthesis encloses the dots like in (...AND...) and (...OR...)"
							+ ", the 2 filters above and below the operator will be linked directly such that they are within the same parenthesis group."
							+ " When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..."
							+ ", the filters above the operator are part of a group of parenthesis and the filters below the operator are part of another group of parenthesis."
							+ " This operator then links the results of the 2 groups of parenthesis and has therfore a lower precedence than the operators within the parenthesis."
							+ " For example the query \"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
							+ " It is not allowed to mix union (AND) and intersection (OR) operators within a parenthesis."
							+ " However 2 different groups of parenthesis can have different operators."
							+ " All operators outside of the parenthesis groups must be of similar type."
					);
			lbRootOperator.addItem("(... AND ...)", "(..._AND_...)");
//			Element.as(lbRootFilter.getElement().getChild(1))
//					.setTitle(
//							"Operator for intersecting the two filters within the parenthesis."
//									+ "The dots represent the arguments to be linked, "
//									+ "when the parenthesis encloses the dots like in (...AND...) and (...OR...), it means the linkage will be interpreted as an inner level."
//									+ "When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..., we are dealing with an operator of an outer level."
//									+ "Therefore you can specify which operators have a higher precedence by enclosing them within a parenthesis, for example the query�\"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
//									+ "It is forbidden to mix union and intersection operators within a parenthesis but different types of operators are authorized if they are separated by an operator outside a parenthesis."
//									+ "Similarly all operators outside of the parenthesis must be of similar type.");
			lbRootOperator.addItem("...) AND (...", "...)_AND_(...");
//			Element.as(lbRootFilter.getElement().getChild(1))
//					.setTitle(
//							"Operator for intersecting the two filters outside of the parenthesis."
//									+ "The dots represent the arguments to be linked, "
//									+ "when the parenthesis encloses the dots like in (...AND...) and (...OR...), it means the linkage will be interpreted as an inner level."
//									+ "When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..., we are dealing with an operator of an outer level."
//									+ "Therefore you can specify which operators have a higher precedence by enclosing them within a parenthesis, for example the query�\"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
//									+ "It is forbidden to mix union and intersection operators within a parenthesis but different types of operators are authorized if they are separated by an operator outside a parenthesis."
//									+ "Similarly all operators outside of the parenthesis must be of similar type.");
			lbRootOperator.addItem("(... OR ...)", "(..._OR_...)");
//			Element.as(lbRootFilter.getElement().getChild(1))
//					.setTitle(
//							"Operator for the union of the two filters within the parenthesis."
//									+ "The dots represent the arguments to be linked, "
//									+ "when the parenthesis encloses the dots like in (...AND...) and (...OR...), it means the linkage will be interpreted as an inner level."
//									+ "When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..., we are dealing with an operator of an outer level."
//									+ "Therefore you can specify which operators have a higher precedence by enclosing them within a parenthesis, for example the query�\"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
//									+ "It is forbidden to mix union and intersection operators within a parenthesis but different types of operators are authorized if they are separated by an operator outside a parenthesis."
//									+ "Similarly all operators outside of the parenthesis must be of similar type.");
			lbRootOperator.addItem("...) OR (...", "...)_OR_(...");
//			Element.as(lbRootFilter.getElement().getChild(1))
//					.setTitle(
//							"Operator for the union of the two filters outside of the parenthesis."
//									+ "The dots represent the arguments to be linked, "
//									+ "when the parenthesis encloses the dots like in (...AND...) and (...OR...), it means the linkage will be interpreted as an inner level."
//									+ "When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..., we are dealing with an operator of an outer level."
//									+ "Therefore you can specify which operators have a higher precedence by enclosing them within a parenthesis, for example the query�\"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
//									+ "It is forbidden to mix union and intersection operators within a parenthesis but different types of operators are authorized if they are separated by an operator outside a parenthesis."
//									+ "Similarly all operators outside of the parenthesis must be of similar type.");
			// lbRootOperator.addItem("DELETE FILTER");
			final int idxToAdd = vpFilterToAddTo.getWidgetCount() - 1;

			hp.add(lbRootOperator);
			hp.setCellVerticalAlignment(lbRootOperator,
					HasVerticalAlignment.ALIGN_MIDDLE);

//			lbRootOperator.addChangeHandler(new ChangeHandler() {
//				@Override
//				public void onChange(ChangeEvent event) {
//					if (lbRootOperator.getSelectedValue().compareTo("(..._AND_...)") == 0) {
//						lbRootFilter
//								.setTitle("Operator for intersecting the two filters within the parenthesis."
//										+ "The dots represent the arguments to be linked, "
//										+ "when the parenthesis encloses the dots like in (...AND...) and (...OR...), it means the linkage will be interpreted as an inner level."
//										+ "When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..., we are dealing with an operator of an outer level."
//										+ "Therefore you can specify which operators have a higher precedence by enclosing them within a parenthesis, for example the query�\"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
//										+ "It is forbidden to mix union and intersection operators within a parenthesis but different types of operators are authorized if they are separated by an operator outside a parenthesis."
//										+ "Similarly all operators outside of the parenthesis must be of similar type.");
//					} else if (lbRootOperator.getSelectedValue().compareTo("...)_AND_(...") == 0) {
//						lbRootFilter
//								.setTitle("Operator for intersecting the two filters outside of the parenthesis."
//										+ "The dots represent the arguments to be linked, "
//										+ "when the parenthesis encloses the dots like in (...AND...) and (...OR...), it means the linkage will be interpreted as an inner level."
//										+ "When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..., we are dealing with an operator of an outer level."
//										+ "Therefore you can specify which operators have a higher precedence by enclosing them within a parenthesis, for example the query�\"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
//										+ "It is forbidden to mix union and intersection operators within a parenthesis but different types of operators are authorized if they are separated by an operator outside a parenthesis."
//										+ "Similarly all operators outside of the parenthesis must be of similar type.");
//					} else if (lbRootOperator.getSelectedValue().compareTo("(..._OR_...)") == 0) {
//						lbRootFilter
//								.setTitle("Operator for the union of the two filters within the parenthesis."
//										+ "The dots represent the arguments to be linked, "
//										+ "when the parenthesis encloses the dots like in (...AND...) and (...OR...), it means the linkage will be interpreted as an inner level."
//										+ "When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..., we are dealing with an operator of an outer level."
//										+ "Therefore you can specify which operators have a higher precedence by enclosing them within a parenthesis, for example the query�\"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
//										+ "It is forbidden to mix union and intersection operators within a parenthesis but different types of operators are authorized if they are separated by an operator outside a parenthesis."
//										+ "Similarly all operators outside of the parenthesis must be of similar type.");
//					} else if (lbRootOperator.getSelectedValue().compareTo("...)_OR_(...") == 0) {
//						lbRootFilter
//								.setTitle("Operator for the union of the two filters outside of the parenthesis."
//										+ "The dots represent the arguments to be linked, "
//										+ "when the parenthesis encloses the dots like in (...AND...) and (...OR...), it means the linkage will be interpreted as an inner level."
//										+ "When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..., we are dealing with an operator of an outer level."
//										+ "Therefore you can specify which operators have a higher precedence by enclosing them within a parenthesis, for example the query�\"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
//										+ "It is forbidden to mix union and intersection operators within a parenthesis but different types of operators are authorized if they are separated by an operator outside a parenthesis."
//										+ "Similarly all operators outside of the parenthesis must be of similar type.");
//					} else {
//						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
//								new Exception("Error in addARootFilter :" +
//								" lbRootOperator.getSelectedValue not recognized : "
//								+ lbRootOperator.getSelectedValue()));
//					}
//				}
//			});
			// which operator by default
			lbRootOperator.setItemSelected(0, true);
			lbRootFilter
					.setTitle(
					"set the operator for intersecting two or more filters within the parenthesis."
							+ "The dots represent the arguments to be linked by the union (AND) or the intersection (OR) operators."
							+ " When the parenthesis encloses the dots like in (...AND...) and (...OR...)"
							+ ", the 2 filters above and below the operator will be linked directly such that they are within the parenthesis."
							+ " When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..."
							+ ", the filters above this operator are part of a group of parenthesis and the filters below this operator are part of another group of parenthesis."
							+ " This operator then links the results of the 2 groups of parenthesis and has therfore a lower precedence than the operators within the parenthesis."
							+ " For example the query \"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
							+ " It is not allowed to mix union (AND) and intersection (OR) operators within a parenthesis."
							+ " However 2 different groups of parenthesis can have different operators."
							+ " All operators outside of the parenthesis groups must be of similar type."
					);
//					.setTitle("Operator for intersecting the two filters within the parenthesis."
//							+ "The dots represent the arguments to be linked, "
//							+ "when the parenthesis encloses the dots like in (...AND...) and (...OR...), it means the linkage will be interpreted as an inner level."
//							+ "When the dots are outside the parenthesis like in ...)AND(... and ...)OR(..., we are dealing with an operator of an outer level."
//							+ "Therefore you can specify which operators have a higher precedence by enclosing them within a parenthesis, for example the query�\"(1 AND 2) OR 3\" will yield a different result than the query \"1 AND (2 OR 3)\"."
//							+ "It is forbidden to mix union and intersection operators within a parenthesis but different types of operators are authorized if they are separated by an operator outside a parenthesis."
//							+ "Similarly all operators outside of the parenthesis must be of similar type.");

			HTML htmlDel = new HTML(
					"<span style=\"color:red; cursor:pointer;\"><big><b>(x)</b></big></span>");
			htmlDel.setTitle("Delete this filter");
			hp.add(htmlDel);
			hp.setCellVerticalAlignment(htmlDel,
					HasVerticalAlignment.ALIGN_MIDDLE);
			htmlDel.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					int idxToStartDel = vpFilterToAddTo.getWidgetIndex(hp);
					vpFilterToAddTo.remove(idxToStartDel);
					vpFilterToAddTo.remove(idxToStartDel);
					vpFilterToAddTo.remove(idxToStartDel);
				}
			});

			// which filter by default when not first filter added
			lbRootFilter.setItemSelected(0, true);
			deckPRootFilter.showWidget(0);
			lbRootFilter.setTitle("List genes whose genomic locations are within a given boundaries. Start and stop must be entered in base pairs relative to the origin of replication ORI.");

			vpFilterToAddTo.insert(deckPRootFilter, idxToAdd);
			vpFilterToAddTo.insert(lbRootFilter, idxToAdd);
			vpFilterToAddTo.insert(hp, idxToAdd);

		}

	}

	/** ajouter un filtre qualifiers **/
	private static void addNewQualifiersFilter(String title, String value,
			ListBox lbRootFilter, DeckPanel deckPRootFilter, String toolTip) {

		lbRootFilter.addItem(title, value);
		Element.as(
				lbRootFilter.getElement().getChild(
						lbRootFilter.getItemCount() - 1)).setTitle(toolTip);
		final VerticalPanel cecoFp = new VerticalPanel();
		cecoFp.setWidth("100%");
		cecoFp.setSpacing(2);
		addGiCoreFilter(
				//true, 
				cecoFp, value);

		deckPRootFilter.add(cecoFp);
	}


	private static void buildUIStepComparedGenomes() {
		
		comparedGenomes_selectionModelMainList.addSelectionChangeHandler(new Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				if ( ! comparedGenomes_selectionModelFeaturedList.getSelectedSet().isEmpty() && ! comparedGenomes_selectionModelMainList.getSelectedSet().isEmpty() ) {
					comparedGenomes_selectionModelFeaturedList.clear();
				}
			}
		});
		comparedGenomes_clMainList = new CellList<LightOrganismItem>(new OrganismCell());
		comparedGenomes_clMainList.setPageSize(30);
		comparedGenomes_clMainList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		comparedGenomes_clMainList.setSelectionModel(comparedGenomes_selectionModelMainList);
		comparedGenomes_ldpMainList = new ListDataProvider<LightOrganismItem>();
		comparedGenomes_ldpMainList.addDataDisplay(comparedGenomes_clMainList);
		comparedGenomes_ppMainList.setDisplay(comparedGenomes_clMainList);
		comparedGenomes_rlpMainList.setDisplay(comparedGenomes_clMainList);
		comparedGenomes_mainList_searchIcon.setResource(MyResources.INSTANCE.searchIcon());
		comparedGenomes_mainList_searchIcon.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				comparedGenomes_searchForOrganismsOrTaxons_mainList(comparedGenomes_suggestBoxSearchMain.getText());
			}
		});
//		@UiHandler("comparedGenomes_searchIcon")
//		void onComparedGenomes_searchIconClick(ClickEvent e) {
//			comparedGenomes_searchForOrganismsOrTaxons(comparedGenomes_suggestBoxSearchMain.getText());
//		}

		
		comparedGenomes_suggestBoxSearchMain.setWidth("100%");
		comparedGenomes_suggestBoxSearchMain.setAutoSelectEnabled(false);
		comparedGenomes_suggestBoxSearchMain.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					comparedGenomes_searchForOrganismsOrTaxons_mainList(comparedGenomes_suggestBoxSearchMain.getText());
				}
			}
		});
		comparedGenomes_suggestBoxSearchMain.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				comparedGenomes_searchForOrganismsOrTaxons_mainList(event
						.getSelectedItem().getReplacementString());
			}
		});
		comparedGenomes_suggestBoxSearchMain.getValueBox().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				comparedGenomes_suggestBoxSearchMain.setText("");
			}
		});
		comparedGenomes_suggestBoxSearchMain.setText("filter for organisms and taxons");
		comparedGenomes_mainList_VPSuggestBoxSearch.add(comparedGenomes_suggestBoxSearchMain);
		comparedGenomes_mainList_clearSearchMain.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				comparedGenomes_clearSearchMain(true, true);
			}
		});
		
		//FeaturedGenomes
		comparedGenomes_featuredOrganisms_searchIcon.setResource(MyResources.INSTANCE.searchIcon());
		comparedGenomes_featuredOrganisms_searchIcon.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				comparedGenomes_searchForOrganismsOrTaxons_featuredList(comparedGenomes_suggestBoxSearchFeatured.getText());
			}
		});
		comparedGenomes_selectionModelFeaturedList.addSelectionChangeHandler(new Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				if ( ! comparedGenomes_selectionModelMainList.getSelectedSet().isEmpty() && ! comparedGenomes_selectionModelFeaturedList.getSelectedSet().isEmpty() ) {
					comparedGenomes_selectionModelMainList.clear();
				}
			}
		});
		comparedGenomes_clFeaturedList = new CellList<LightOrganismItem>(new OrganismCell());
		comparedGenomes_clFeaturedList.setPageSize(30);
		comparedGenomes_clFeaturedList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		comparedGenomes_clFeaturedList.setSelectionModel(comparedGenomes_selectionModelFeaturedList);
		comparedGenomes_ldpFeaturedList = new ListDataProvider<LightOrganismItem>();
		comparedGenomes_ldpFeaturedList.addDataDisplay(comparedGenomes_clFeaturedList);
		comparedGenomes_ppFeaturedList.setDisplay(comparedGenomes_clFeaturedList);
		comparedGenomes_rlpFeaturedList.setDisplay(comparedGenomes_clFeaturedList);
		comparedGenomes_suggestBoxSearchFeatured.setWidth("100%");
		comparedGenomes_suggestBoxSearchFeatured.setAutoSelectEnabled(false);
		comparedGenomes_suggestBoxSearchFeatured.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					comparedGenomes_searchForOrganismsOrTaxons_featuredList(comparedGenomes_suggestBoxSearchFeatured.getText());
				}
			}
		});
		comparedGenomes_suggestBoxSearchFeatured.addSelectionHandler(new SelectionHandler<Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				comparedGenomes_searchForOrganismsOrTaxons_featuredList(event
						.getSelectedItem().getReplacementString());
			}
		});
		comparedGenomes_suggestBoxSearchFeatured.getValueBox().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				comparedGenomes_suggestBoxSearchFeatured.setText("");
			}
		});
		comparedGenomes_suggestBoxSearchFeatured.setText("filter for organisms and taxons");
		comparedGenomes_featuredOrganisms_VPSuggestBoxSearch.add(comparedGenomes_suggestBoxSearchFeatured);
		comparedGenomes_featuredOrganisms_clearSearchFeatured.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				comparedGenomes_clearSearchFeatured(true, true);
			}
		});
		
		//build Sort result list by
		
		//comparedGenomes_HPSortResultListBy_scope
		/*RadioButton rbComparedGenomesHPSortResultListBy_scope_SelectedRefGene = new RadioButton("comparedGenomes_HPSortResultListBy_scope",
				"<big>Selected reference gene</big>"
				, true);*/
		rbComparedGenomesHPSortResultListBy_scope_SelectedRefGene.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//comparedGenomes_SortResultListBy_scope = Enum_comparedGenomes_SortResultListBy_scope.SelectedRefGene;
				Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_scope(Enum_comparedGenomes_SortResultListBy_scope.SelectedRefGene);
				updateTitleMenuItems_ComparedGenomes();
			}
		});
		rbComparedGenomesHPSortResultListBy_scope_SelectedRefGene.setTitle(
				"Sort by a single reference gene."
				+ " This is equivalent to sorting by a single column in the homolog browsing view."
				+ " To use this scope, select the reference gene in the \"Ortholog table\" or \"Genomic organisation\" view previous to changing this scope."
				+ " For this reason, this option is disabled in the \"Search\" tab but activated in the \"Ortholog table\" and \"Genomic organisation\" through the menu \"Compared organisms -> Manage list / Find\"."
				+ " The score for each organism is calculated according to the sort type selected below."
				);
		rbComparedGenomesHPSortResultListBy_scope_SelectedRefGene.setEnabled(false);
		rbComparedGenomesHPSortResultListBy_scope_SelectedRefGene.setHTML("<span style=\"color: #696969;\">Selected reference gene</span>");
		//comparedGenomes_HPSortResultListBy_scope.add(rbComparedGenomesHPSortResultListBy_scope_SelectedRefGene);
		
		/*RadioButton rbComparedGenomesHPSortResultListBy_scope_RefGeneSet = new RadioButton("comparedGenomes_HPSortResultListBy_scope",
				"<big>Reference gene set</big>"
				, true);*/
		rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//comparedGenomes_SortResultListBy_scope = Enum_comparedGenomes_SortResultListBy_scope.RefGeneSet;
				Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_scope(Enum_comparedGenomes_SortResultListBy_scope.RefGeneSet);
				rbcomparedGenomes_HPSortResultListBy_sortType_AlignemntScore.setHTML("<span style=\"color: #696969;\">Alignemnt score</span>");
				rbcomparedGenomes_HPSortResultListBy_sortType_AlignemntScore.setEnabled(false);
				rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologsAnnotations.setHTML("<span style=\"color: #696969;\">Abundance of orthologs' annotations</span>");
				rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologsAnnotations.setEnabled(false);
				updateTitleMenuItems_ComparedGenomes();
			}
		});
		rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.setTitle(
				"The scope is the gene set that you build or selected."
				+ " The score for each organism is calculated according to the sort type selected below."
				+ " The total score for a given organism is the sum of the individual scores for each gene in the gene set."
				+ " This option is disabled if you didn't select any genes in the \"Reference CDSs\" menu."
				+ " For the \"Ortholog table\" view, it is equivalent to sorting by all columns when displaying a reference gene set."
				);
		rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.setEnabled(false);
		rbComparedGenomesHPSortResultListBy_scope_RefGeneSet.setHTML("<span style=\"color: #696969;\">Reference gene set</span>");
		//comparedGenomes_HPSortResultListBy_scope.add(rbComparedGenomesHPSortResultListBy_scope_RefGeneSet);
		
		/*RadioButton rbComparedGenomesHPSortResultListBy_scope_WholeOrganism = new RadioButton("comparedGenomes_HPSortResultListBy_scope",
				"<big>Whole organism</big>"
				, true);*/
		rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//comparedGenomes_SortResultListBy_scope = Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism;
				Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_scope(Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism);
				rbcomparedGenomes_HPSortResultListBy_sortType_AlignemntScore.setHTML("Alignemnt score");
				rbcomparedGenomes_HPSortResultListBy_sortType_AlignemntScore.setEnabled(true);
				rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologsAnnotations.setHTML("<span style=\"color: #696969;\">Abundance of orthologs' annotations</span>");
				rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologsAnnotations.setEnabled(false);
				updateTitleMenuItems_ComparedGenomes();
			}
		});
		
		rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.setTitle(
				"The scope is the whole reference organism, regardless of the gene set or displayed region."
				+ " The score for each organism is calculated according to the sort type selected below."
				+ " The total score for a given organism is the sum of the individual scores for each gene of the organism."
				+ " For the \"Ortholog table\" view, it is equivalent to sorting by all columns when displaying all the CDSs of the organism."
				);
		//comparedGenomes_HPSortResultListBy_scope.add(rbComparedGenomesHPSortResultListBy_scope_WholeOrganism);

		//comparedGenomes_HPSortResultListBy_sortType
		/*RadioButton rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologs = new RadioButton("comparedGenomes_HPSortResultListBy_sortType",
				"<big>Abundance of orthologs</big>"
				, true);*/
		rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologs.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//comparedGenomes_SortResultListBy_sortType = Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs;
				Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs);
				updateTitleMenuItems_ComparedGenomes();
			}
		});
		rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologs.setTitle(
				"The more number of orthologs with the reference, the higher the score."
				+ " 1 point for 1 ortholog."
				);
		//comparedGenomes_HPSortResultListBy_sortType.add(rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologs);
		
		/*RadioButton rbcomparedGenomes_HPSortResultListBy_sortType_SyntenyScore = new RadioButton("comparedGenomes_HPSortResultListBy_sortType",
				"<big>Synteny score</big>"
				, true);*/
		rbcomparedGenomes_HPSortResultListBy_sortType_SyntenyScore.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//comparedGenomes_SortResultListBy_sortType = Enum_comparedGenomes_SortResultListBy_sortType.SyntenyScore;
				Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType.SyntenyScore);
				updateTitleMenuItems_ComparedGenomes();
			}
		});
		rbcomparedGenomes_HPSortResultListBy_sortType_SyntenyScore.setTitle(
				"The score for a given synteny is defined as follow according to its constituants (score by default): 4 points for each ortholog BDBH, 2 points for each ortholog non BDBH, -3 points for each gap or mismatch."
				+ " Therefore the syntenies with the better score will be the one that have the highest number of co-localized orthologs and the fewest number of gaps or mismatchs."
				+ " The total score for a given compared organism is the sum of each synteny score of the orthologs for the selected scope."
				);
		//comparedGenomes_HPSortResultListBy_sortType.add(rbcomparedGenomes_HPSortResultListBy_sortType_SyntenyScore);
		
		/*RadioButton rbcomparedGenomes_HPSortResultListBy_sortType_AlignemntScore = new RadioButton("comparedGenomes_HPSortResultListBy_sortType",
				"<big>Alignemnt score</big>"
				, true);*/
		rbcomparedGenomes_HPSortResultListBy_sortType_AlignemntScore.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//comparedGenomes_SortResultListBy_sortType = Enum_comparedGenomes_SortResultListBy_sortType.AlignemntScore;
				Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType.AlignemntScore);
				updateTitleMenuItems_ComparedGenomes();
			}
		});
		rbcomparedGenomes_HPSortResultListBy_sortType_AlignemntScore.setTitle(
				"The alignemnt score for a given compared organism sums all the blast alignment score of the orthologs for the selected scope."
				+ " Therefore the more overall pair base similarities between orthologs, the higher the compared organism will score."
						);
		//comparedGenomes_HPSortResultListBy_sortType.add(rbcomparedGenomes_HPSortResultListBy_sortType_AlignemntScore);
		

		/*RadioButton rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologsAnnotations = new RadioButton("comparedGenomes_HPSortResultListBy_sortType",
				"<big>Abundance of orthologs' annotations</big>"
				, true);*/
		rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologsAnnotations.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//comparedGenomes_SortResultListBy_sortType = Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologsAnnotations;
				Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologsAnnotations);
				updateTitleMenuItems_ComparedGenomes();
			}
		});
		rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologsAnnotations.setTitle(
				"For a given ortholog, the score for abundance of orthologs' annotations is the number of its functional annotations (i.e molecular function, product, etc.)"
				+ " The total score for a given compared organism is the sum of each abundance of homologs' annotations score of the orthologs for the selected scope."
				);
		//comparedGenomes_HPSortResultListBy_sortType.add(rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologsAnnotations);
		
			
		//comparedGenomes_HPSortResultListBy_sortOrder
		/*RadioButton rbcomparedGenomes_HPSortResultListBy_sortOrder_Descending = new RadioButton("comparedGenomes_HPSortResultListBy_sortOrder",
				"<big>Descending</big>"
				, true);*/
		rbcomparedGenomes_HPSortResultListBy_sortOrder_Descending.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//comparedGenomes_SortResultListBy_sortOrder = Enum_comparedGenomes_SortResultListBy_sortOrder.Descending;
				Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_sortOrder(Enum_comparedGenomes_SortResultListBy_sortOrder.Descending);
				updateTitleMenuItems_ComparedGenomes();
			}
		});
		rbcomparedGenomes_HPSortResultListBy_sortOrder_Descending.setTitle(
				"Arranged from largest to smallest. Decreasing."
				);
		//comparedGenomes_HPSortResultListBy_sortOrder.add(rbcomparedGenomes_HPSortResultListBy_sortOrder_Descending);
		
		/*RadioButton rbcomparedGenomes_HPSortResultListBy_sortOrder_Ascending = new RadioButton("comparedGenomes_HPSortResultListBy_sortOrder",
				"<big>Ascending</big>"
				, true);*/
		rbcomparedGenomes_HPSortResultListBy_sortOrder_Ascending.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//comparedGenomes_SortResultListBy_sortOrder = Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending;
				Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_sortOrder(Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending);
				updateTitleMenuItems_ComparedGenomes();
			}
		});
		rbcomparedGenomes_HPSortResultListBy_sortOrder_Ascending.setTitle(
				"Arranged from largest to smallest. Decreasing."
				);
		//comparedGenomes_HPSortResultListBy_sortOrder.add(rbcomparedGenomes_HPSortResultListBy_sortOrder_Ascending);

		//default

		Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_scope(Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism);
		Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs);
		Insyght.APP_CONTROLER.getUserSearchItem().setSortResultListBy_sortOrder(Enum_comparedGenomes_SortResultListBy_sortOrder.Descending);
		rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.setValue(false, false); // false before true to trigger event
		rbComparedGenomesHPSortResultListBy_scope_WholeOrganism.setValue(true, true);
		rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologs.setValue(false, false);// false before true to trigger event
		rbcomparedGenomes_HPSortResultListBy_sortType_AbundanceHomologs.setValue(true, true);
		rbcomparedGenomes_HPSortResultListBy_sortOrder_Descending.setValue(false, false);// false before true to trigger event
		rbcomparedGenomes_HPSortResultListBy_sortOrder_Descending.setValue(true, true);
	}

	protected static void updateTitleMenuItems_ComparedGenomes() {
		String stTitle = "";
		int cumSizeFeaturedList = comparedGenomes_alOrgaFeaturedList_filtredIn.size() + comparedGenomes_alOrgaFeaturedList_filtredOut.size();
		int cumSizeMainList = comparedGenomes_alOrgaMainList_filtredIn.size() + comparedGenomes_alOrgaMainList_filtredOut.size();
		stTitle += cumSizeFeaturedList + " featured compared genomes";
		stTitle += ", " + cumSizeMainList + " compared genomes in main list.";
//		if ( ! isComparedGenomes_alOrgaMainListInitialized) {
//			stTitle += ", all compared genomes in main list.";
//		} else {
//			stTitle += ", " + comparedGenomes_alOrgaMainList.size() + " compared genomes in main list.";
//		}
		
		
		stTitle += "\nSort main result list: ";
		if (Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_scope().compareTo(Enum_comparedGenomes_SortResultListBy_scope.SelectedRefGene) ==0 ) {
			stTitle += "scope \"Selected reference gene\"";
		} else if (Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_scope().compareTo(Enum_comparedGenomes_SortResultListBy_scope.RefGeneSet) ==0 ) {
			stTitle += "scope \"Reference gene set\"";
		} else if (Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_scope().compareTo(Enum_comparedGenomes_SortResultListBy_scope.WholeOrganism) ==0 ) {
			stTitle += "scope \"Whole organism\"";
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
					new Exception("ERROR in updateTitleMenuItems_ComparedGenomes : "
							+ "getSortResultListBy_scope not recognized : "
							+ Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_scope().toString()
							)
					);
		}
		stTitle += ", ";
		if (Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_sortType().compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologs) ==0 ) {
			stTitle += "type \"Abundance of orthologs\"";
		} else if (Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_sortType().compareTo(Enum_comparedGenomes_SortResultListBy_sortType.SyntenyScore) ==0 ) {
			stTitle += "type \"Synteny score\"";
		} else if (Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_sortType().compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AlignemntScore) ==0 ) {
			stTitle += "type \"Alignemnt score\"";
		} else if (Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_sortType().compareTo(Enum_comparedGenomes_SortResultListBy_sortType.AbundanceHomologsAnnotations) ==0 ) {
			stTitle += "type \"Abundance of orthologs' annotations\"";
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
					new Exception("ERROR in updateTitleMenuItems_ComparedGenomes : "
							+ "getSortResultListBy_sortType not recognized : "
							+ Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_sortType().toString()
							)
					);
		}
		stTitle += ", ";
		if (Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_sortOrder().compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Descending) == 0) {
			stTitle += "order \"Descending\"";
		} else if (Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_sortOrder().compareTo(Enum_comparedGenomes_SortResultListBy_sortOrder.Ascending) == 0) {
			stTitle += "order \"Ascending\"";
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
					new Exception("ERROR in updateTitleMenuItems_ComparedGenomes : "
							+ "getSortResultListBy_sortOrder not recognized : "
							+ Insyght.APP_CONTROLER.getUserSearchItem().getSortResultListBy_sortOrder().toString()
							)
					);
		}
		stTitle += ".";

		MenuItems_ComparedGenomes.setTitle(stTitle);
	}


	private static void comparedGenomes_clearSearchFeatured(
			boolean comparedGenomes_refreshListDataProvider
			, boolean eraseTextInSuggestBox
			) {
		//comparedGenomes_alOrgaFeaturedList
		comparedGenomes_alOrgaFeaturedList_filtredIn.addAll(comparedGenomes_alOrgaFeaturedList_filtredOut);
		comparedGenomes_alOrgaFeaturedList_filtredOut.clear();
		//Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().addAll(Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome());
		Collections.sort(
				//comparedGenomes_alOrgaFeaturedList
				comparedGenomes_alOrgaFeaturedList_filtredIn
				, LightOrganismItemComparators.byPositionInResultSetComparator);
		//comparedGenomes_replaySavedMovesToChangePositionInFeaturedList();
		//Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome().clear();
		if (comparedGenomes_refreshListDataProvider) {
			comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
			comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
			//comparedGenomes_genomeResultListHasChanged = true;
		}
		if (eraseTextInSuggestBox) {
			comparedGenomes_suggestBoxSearchFeatured.setText("filter for organisms and taxons");
		}
	}
	
	
	private static void comparedGenomes_clearSearchMain(
			boolean comparedGenomes_refreshListDataProvider
			, boolean eraseTextInSuggestBox) {
		comparedGenomes_alOrgaMainList_filtredIn.addAll(comparedGenomes_alOrgaMainList_filtredOut);
		comparedGenomes_alOrgaMainList_filtredOut.clear();
		//comparedGenomes_alOrgaMainList.addAll(Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome());
		Collections.sort(comparedGenomes_alOrgaMainList_filtredIn, LightOrganismItemComparators.byPositionInResultSetComparator);
		comparedGenomes_replaySavedMovesToChangePositionInMainList();
		//Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome().clear();
		if (comparedGenomes_refreshListDataProvider) {
			comparedGenomes_listDataProviderLOIMainListHasChanged = true;
			comparedGenomes_refreshListDataProviderMainListIfNedded();
			comparedGenomes_genomeResultListHasChanged = true;
		}
		if (eraseTextInSuggestBox) {
			comparedGenomes_suggestBoxSearchMain.setText("filter for organisms and taxons");
		}
	}
	
	
	@UiHandler("comparedGenomes_btAddFeaturedOrga")
	void onComparedGenomes_BtAddFeaturedOrgaClick(ClickEvent e) {
		
		comparedGenomes_clearSearchFeatured(false, true);
		HashSet<Integer> hsAlreadyAdded = new HashSet<>();
		for (LightOrganismItem loiIT : comparedGenomes_alOrgaFeaturedList_filtredIn ) {
			hsAlreadyAdded.add(loiIT.getOrganismId());
		}
		HashSet<Integer> hsToAdd = new HashSet<>();
		for (LightOrganismItem loiIT : comparedGenomes_selectionModelMainList.getSelectedSet() ) {
			hsToAdd.add(loiIT.getOrganismId());
		}
		Iterator<LightOrganismItem> it = comparedGenomes_alOrgaMainList_filtredIn.iterator();
		while (it.hasNext()) {
			LightOrganismItem loiIT = it.next();
			if (hsToAdd.contains(loiIT.getOrganismId())) {
				//found a selected
				if ( ! hsAlreadyAdded.contains( loiIT.getOrganismId() )) {
					comparedGenomes_alOrgaFeaturedList_filtredIn.add(loiIT);
				}
			    it.remove();
			}
		}	
		
		comparedGenomes_listDataProviderLOIMainListHasChanged = true;
		comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
		comparedGenomes_refreshListDataProviderMainListIfNedded();
		comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
		comparedGenomes_genomeResultListHasChanged = true;
		
	}
	
	@UiHandler("comparedGenomes_btAddAllFeaturedOrga")
	void onComparedGenomes_BtAddAllFeaturedOrgaClick(ClickEvent e) {
		comparedGenomes_clearSearchFeatured(false, true);
		HashSet<Integer> hsAlreadyAdded = new HashSet<>();
		for (LightOrganismItem loiIT : comparedGenomes_alOrgaFeaturedList_filtredIn ) {
			hsAlreadyAdded.add(loiIT.getOrganismId());
		}
		Iterator<LightOrganismItem> it = comparedGenomes_alOrgaMainList_filtredIn.iterator();
		while (it.hasNext()) {
			LightOrganismItem loiIT = it.next();
			if ( ! hsAlreadyAdded.contains( loiIT.getOrganismId() )) {
				comparedGenomes_alOrgaFeaturedList_filtredIn.add(loiIT);
			}
		    it.remove();
		}	

		comparedGenomes_listDataProviderLOIMainListHasChanged = true;
		comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
		comparedGenomes_refreshListDataProviderMainListIfNedded();
		comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
		comparedGenomes_genomeResultListHasChanged = true;
	}
	

//	public static void comparedGenomes_refreshListDataProviderFeaturedGenomesIfNedded() {
//		if (comparedGenomes_listDataProviderLOIFeaturedListHasChanged) {
//			if ( ! Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().isEmpty() ) {
//				comparedGenomes_ppFeaturedList.getDisplay().setVisibleRange(0, Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().size());
//			} else {
//				comparedGenomes_ppFeaturedList.getDisplay().setVisibleRange(0, 100);
//			}
//			comparedGenomes_ldpFeaturedList.setList(Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults());
//			comparedGenomes_listDataProviderLOIFeaturedListHasChanged = false;
//			updateTitleMenuItems_ComparedGenomes();
//		}
//	}
	
	public static void comparedGenomes_refreshListDataProviderFeaturedListIfNedded() {
		if (comparedGenomes_listDataProviderLOIFeaturedListHasChanged) {
			comparedGenomes_ppFeaturedList.getDisplay().setVisibleRange(0, 100);
			comparedGenomes_ldpFeaturedList.setList(
					//comparedGenomes_alOrgaFeaturedList
					comparedGenomes_alOrgaFeaturedList_filtredIn 
					);
			comparedGenomes_listDataProviderLOIFeaturedListHasChanged = false;
			updateTitleMenuItems_ComparedGenomes();
			int cumSizeIT = comparedGenomes_alOrgaFeaturedList_filtredIn.size() + comparedGenomes_alOrgaFeaturedList_filtredOut.size();
			comparedGenomes_featuredList_title.setHTML("Featured organisms ("+comparedGenomes_alOrgaFeaturedList_filtredIn.size() +" / "+cumSizeIT+"):");
		}
	}
	

	public static void comparedGenomes_refreshListDataProviderMainListIfNedded() {
		if (comparedGenomes_listDataProviderLOIMainListHasChanged) {
			comparedGenomes_ppMainList.getDisplay().setVisibleRange(0, 100);
			comparedGenomes_ldpMainList.setList(comparedGenomes_alOrgaMainList_filtredIn);
			comparedGenomes_listDataProviderLOIMainListHasChanged = false;
			updateTitleMenuItems_ComparedGenomes();
			int cumSizeIT = comparedGenomes_alOrgaMainList_filtredIn.size() + comparedGenomes_alOrgaMainList_filtredOut.size();
			comparedGenomes_mainList_title.setHTML("Main list ("+comparedGenomes_alOrgaMainList_filtredIn.size()+" / "+cumSizeIT+"):");
		}
	}
	

	@UiHandler("comparedGenomes_btRemoveFeaturedOrga")
	void onComparedGenomes_BtRemoveFeaturedOrgaClick(ClickEvent e) {
		comparedGenomes_clearSearchMain(false, true);
		Set<LightOrganismItem> setLOIToRemove = comparedGenomes_selectionModelFeaturedList.getSelectedSet();
		comparedGenomes_alOrgaMainList_filtredIn.addAll(setLOIToRemove);
		Collections.sort(comparedGenomes_alOrgaMainList_filtredIn, LightOrganismItemComparators.byPositionInResultSetComparator);
		comparedGenomes_replaySavedMovesToChangePositionInMainList();
		comparedGenomes_alOrgaFeaturedList_filtredIn.removeAll(setLOIToRemove);
		comparedGenomes_listDataProviderLOIMainListHasChanged = true;
		comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
		comparedGenomes_refreshListDataProviderMainListIfNedded();
		comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
		comparedGenomes_genomeResultListHasChanged = true;
	}
	
	@UiHandler("comparedGenomes_btRemoveAllFeaturedOrga")
	void onComparedGenomes_BtRemoveAllFeaturedOrgaClick(ClickEvent e) {
		comparedGenomes_clearSearchMain(false, true);
		comparedGenomes_alOrgaMainList_filtredIn.addAll(comparedGenomes_alOrgaFeaturedList_filtredIn);
		Collections.sort(comparedGenomes_alOrgaMainList_filtredIn, LightOrganismItemComparators.byPositionInResultSetComparator);
		comparedGenomes_replaySavedMovesToChangePositionInMainList();
		comparedGenomes_alOrgaFeaturedList_filtredIn.clear();
		comparedGenomes_listDataProviderLOIMainListHasChanged = true;
		comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
		comparedGenomes_refreshListDataProviderMainListIfNedded();
		comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
		comparedGenomes_genomeResultListHasChanged = true;
	}
	

	private static void comparedGenomes_replaySavedMovesToChangePositionInMainList() {
		// TreeMap tmSavedMovesFinalPositionInAlInMainList2OrgaId
		// sort hashmap by value descending
		LinkedHashMap<Integer, Integer> lhmIT = UtilitiesMethodsShared.sortMapByIntegerValues(comparedGenomes_tmSavedMovesOrgaId2FinalPositionInMainList, false);
	    Iterator<Entry<Integer, Integer>> it = lhmIT.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>)it.next();
	        int orgaIdIT = pair.getKey();
	        int finalPositionInAlIT = pair.getValue();
	        for ( int i = 0 ; i < comparedGenomes_alOrgaMainList_filtredIn.size() ; i++ ) {
	        	LightOrganismItem loiIT = comparedGenomes_alOrgaMainList_filtredIn.get(i);
	        	if (loiIT.getOrganismId() == orgaIdIT) {
	        		// found it
	        		if (finalPositionInAlIT < 0) {
	        			finalPositionInAlIT = 0;
	        		}
	        		if (finalPositionInAlIT > comparedGenomes_alOrgaMainList_filtredIn.size()) {
	        			finalPositionInAlIT = comparedGenomes_alOrgaMainList_filtredIn.size();
	        		}
	        		comparedGenomes_alOrgaMainList_filtredIn.remove(loiIT);
	        		comparedGenomes_alOrgaMainList_filtredIn.add(finalPositionInAlIT, loiIT);
	        		break;
	        	}
	        }
	    }
	}
	
	
	@UiHandler("comparedGenomes_btMoveUpOrgaInList")
	void onComparedGenomes_BtMoveUpOrgaInListClick(ClickEvent e) {
		Set<LightOrganismItem> setSelectedSetMainListUnsorted = comparedGenomes_selectionModelMainList.getSelectedSet();
		ArrayList<LightOrganismItem> setSelectedSetMainList = new ArrayList<>(setSelectedSetMainListUnsorted);
		Collections.sort(setSelectedSetMainList, LightOrganismItemComparators.byPositionInSearchViewImplComparedGenomes_alOrgaMainList);
		for (LightOrganismItem selectedSetMainListIT : setSelectedSetMainList) {
			int idxToMoveUp = comparedGenomes_alOrgaMainList_filtredIn.indexOf(selectedSetMainListIT);
			if (idxToMoveUp > 0) {
				Collections.swap(comparedGenomes_alOrgaMainList_filtredIn, idxToMoveUp, idxToMoveUp-1);
				comparedGenomes_tmSavedMovesOrgaId2FinalPositionInMainList.put(selectedSetMainListIT.getOrganismId(), idxToMoveUp-1);
				comparedGenomes_listDataProviderLOIMainListHasChanged = true;
			} else {
				break;
			}
		}
		Set<LightOrganismItem> setSelectedSetFeaturedGenomesUnsorted = comparedGenomes_selectionModelFeaturedList.getSelectedSet();
		ArrayList<LightOrganismItem> setSelectedSetFeaturedGenomes = new ArrayList<>(setSelectedSetFeaturedGenomesUnsorted);
		Collections.sort(setSelectedSetFeaturedGenomes, LightOrganismItemComparators.byPositionInGetUserSearchItemGetListUserSelectedOrgaToBeResults);
		for (LightOrganismItem selectedSetFeaturedGenomesIT : setSelectedSetFeaturedGenomes) {
			int idxToMoveUp = comparedGenomes_alOrgaFeaturedList_filtredIn.indexOf(selectedSetFeaturedGenomesIT);
			if (idxToMoveUp > 0) {
				Collections.swap(comparedGenomes_alOrgaFeaturedList_filtredIn, idxToMoveUp, idxToMoveUp-1);
				comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
			} else {
				break;
			}
		}
		comparedGenomes_refreshListDataProviderMainListIfNedded();
		comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
		comparedGenomes_genomeResultListHasChanged = true;
	}
	

	@UiHandler("comparedGenomes_btMoveDownOrgaInList")
	void onComparedGenomes_BtMoveDownOrgaInListClick(ClickEvent e) {
		Set<LightOrganismItem> setSelectedSetMainListUnsorted = comparedGenomes_selectionModelMainList.getSelectedSet();
		ArrayList<LightOrganismItem> setSelectedSetMainList = new ArrayList<>(setSelectedSetMainListUnsorted);
		Collections.sort(setSelectedSetMainList, LightOrganismItemComparators.byPositionInSearchViewImplComparedGenomes_alOrgaMainList.reversed());
		for (LightOrganismItem selectedSetMainListIT : setSelectedSetMainList) {
			int idxToMoveDown = comparedGenomes_alOrgaMainList_filtredIn.indexOf(selectedSetMainListIT);
			if (idxToMoveDown < comparedGenomes_alOrgaMainList_filtredIn.size() - 1 ) {
				Collections.swap(comparedGenomes_alOrgaMainList_filtredIn, idxToMoveDown, idxToMoveDown+1);
				comparedGenomes_tmSavedMovesOrgaId2FinalPositionInMainList.put(selectedSetMainListIT.getOrganismId(), idxToMoveDown+1 );
				comparedGenomes_listDataProviderLOIMainListHasChanged = true;
			} else {
				break;
			}
		}
		Set<LightOrganismItem> setSelectedSetFeaturedGenomesUnsorted = comparedGenomes_selectionModelFeaturedList.getSelectedSet();
		ArrayList<LightOrganismItem> setSelectedSetFeaturedGenomes = new ArrayList<>(setSelectedSetFeaturedGenomesUnsorted);
		Collections.sort(setSelectedSetFeaturedGenomes, LightOrganismItemComparators.byPositionInGetUserSearchItemGetListUserSelectedOrgaToBeResults.reversed());
		for (LightOrganismItem selectedSetFeaturedGenomesIT : setSelectedSetFeaturedGenomes) {
			int idxToMoveDown = comparedGenomes_alOrgaFeaturedList_filtredIn.indexOf(selectedSetFeaturedGenomesIT);
			if (idxToMoveDown < comparedGenomes_alOrgaFeaturedList_filtredIn.size() - 1 ) {
				Collections.swap(comparedGenomes_alOrgaFeaturedList_filtredIn, idxToMoveDown, idxToMoveDown+1);
				comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
			} else {
				break;
			}
		}
		comparedGenomes_refreshListDataProviderMainListIfNedded();
		comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
		comparedGenomes_genomeResultListHasChanged = true;
	}
	

	@UiHandler("comparedGenomes_btResetFindFeatureGenomes")
	void onComparedGenomes_BtResetFindFeatureGenomesClick(ClickEvent e) {
		comparedGenomes_clearSearchFeatured(false, true);
		comparedGenomes_clearSearchMain(false, true);
		comparedGenomes_alOrgaMainList_filtredIn.addAll(comparedGenomes_alOrgaFeaturedList_filtredIn);
		comparedGenomes_alOrgaFeaturedList_filtredIn.clear();
		comparedGenomes_tmSavedMovesOrgaId2FinalPositionInMainList.clear();
		Collections.sort(comparedGenomes_alOrgaMainList_filtredIn, LightOrganismItemComparators.byPositionInResultSetComparator);
		comparedGenomes_listDataProviderLOIMainListHasChanged = true;
		comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
		comparedGenomes_refreshListDataProviderMainListIfNedded();
		comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
		comparedGenomes_genomeResultListHasChanged = true;
	}
	
	
	private static void comparedGenomes_restrictFeaturedListOrganismsToSubsetOfOrganisms(ArrayList<? extends LightOrganismItem> alLoiAfterSearch) {
		ArrayList<Integer> alIntOrgaIDIT = new ArrayList<>();
		for (LightOrganismItem loiIT : alLoiAfterSearch ) {
			alIntOrgaIDIT.add(loiIT.getOrganismId());
		}
		ListIterator<LightOrganismItem> iter = 
				//comparedGenomes_alOrgaFeaturedList
				//Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults()
				comparedGenomes_alOrgaFeaturedList_filtredIn.listIterator();
		//int i=-1;
		while(iter.hasNext()){
			LightOrganismItem nextLOTIT = iter.next();
			//i++;
			if (alIntOrgaIDIT.contains(nextLOTIT.getOrganismId())) {
		    } else {
		    	//Insyght.APP_CONTROLER.getUserSearchItem().getListExcludedGenome().add(nextLOTIT);
		    	//alMapPositionToInsertInMainList.put(nextLOTIT.getOrganismId(), i);
		    	comparedGenomes_alOrgaFeaturedList_filtredOut.add(nextLOTIT);
		    	iter.remove();
		    }
		}
		comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
		comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
		//comparedGenomes_genomeResultListHasChanged = true;
	}
	
	
	private static void comparedGenomes_restrictMainListOrganismsToSubsetOfOrganisms(ArrayList<? extends LightOrganismItem> alLoiAfterSearch) {
		ArrayList<Integer> alIntOrgaIDIT = new ArrayList<>();
		for (LightOrganismItem loiIT : alLoiAfterSearch ) {
			alIntOrgaIDIT.add(loiIT.getOrganismId());
		}
		ListIterator<LightOrganismItem> iter = comparedGenomes_alOrgaMainList_filtredIn.listIterator();
		//int i=-1;
		while(iter.hasNext()){
			LightOrganismItem nextLOTIT = iter.next();
			//i++;
			if (alIntOrgaIDIT.contains(nextLOTIT.getOrganismId())) {
		    } else {
		    	comparedGenomes_alOrgaMainList_filtredOut.add(nextLOTIT);
		    	//alMapPositionToInsertInMainList.put(nextLOTIT.getOrganismId(), i);
		    	iter.remove();
		    }
		}
		comparedGenomes_listDataProviderLOIMainListHasChanged = true;
		comparedGenomes_refreshListDataProviderMainListIfNedded();
		comparedGenomes_genomeResultListHasChanged = true;
	}

	protected static void comparedGenomes_searchForOrganismsOrTaxons_featuredList(String searchTerm) {
		comparedGenomes_clearSearchFeatured(false, false);
		ArrayList<LightOrganismItem> alLoiAfterSearch = new ArrayList<>();
		for (int i = 0; i < comparedGenomes_alOrgaFeaturedList_filtredIn.size(); i++) { //comparedGenomes_alOrgaFeaturedList
			LightOrganismItem oiIT = comparedGenomes_alOrgaFeaturedList_filtredIn.get(i);//comparedGenomes_alOrgaFeaturedList
			if ( searchTerm.startsWith(oiIT.getFullName()) ) {
				alLoiAfterSearch.add(oiIT);
			}
		}
		if ( ! alLoiAfterSearch.isEmpty() ) {
			comparedGenomes_restrictFeaturedListOrganismsToSubsetOfOrganisms(alLoiAfterSearch);
		} else {
			TaxoItem tiExactPathToBranch = Insyght.APP_CONTROLER.findPathInTaxoTreeWithTextString(searchTerm);
			if (tiExactPathToBranch != null) {
				ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiExactPathToBranch.getFullPathInHierarchie());
				comparedGenomes_restrictFeaturedListOrganismsToSubsetOfOrganisms(alOiAfterSearch);
			} else {
				TaxoItem tiClosestPathInTree = Insyght.APP_CONTROLER.findClosestPathInTaxoTreeWithTextString(searchTerm);
				if (tiClosestPathInTree != null) {
					ArrayList<TaxoItem> alTaxoItemAndSubNodesWithPathInTree = Insyght.APP_CONTROLER.getListTaxoItemAndSubNodesWithPathInTree(tiClosestPathInTree.getFullPathInHierarchie());
					String approximateFound = "";
					if (alTaxoItemAndSubNodesWithPathInTree != null && ! alTaxoItemAndSubNodesWithPathInTree.isEmpty() ) {
						approximateFound = alTaxoItemAndSubNodesWithPathInTree.get(0).getName();
					}
					comparedGenomes_suggestBoxSearchFeatured.setText("CLOSEST MATCH FOR: "+searchTerm+ " IS: " +approximateFound);
					ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiClosestPathInTree.getFullPathInHierarchie());
					comparedGenomes_restrictFeaturedListOrganismsToSubsetOfOrganisms(alOiAfterSearch);
				} else {
					comparedGenomes_suggestBoxSearchFeatured.setText("NO MATCH FOR: "+searchTerm);
					comparedGenomes_listDataProviderLOIFeaturedListHasChanged = true;
					comparedGenomes_refreshListDataProviderFeaturedListIfNedded();
				}
			}
		}
	}
	
	protected static void comparedGenomes_searchForOrganismsOrTaxons_mainList(String searchTerm) {
		comparedGenomes_clearSearchMain(false, false);
		ArrayList<LightOrganismItem> alLoiAfterSearch = new ArrayList<>();
		for (int i = 0; i < comparedGenomes_alOrgaMainList_filtredIn.size(); i++) {
			LightOrganismItem oiIT = comparedGenomes_alOrgaMainList_filtredIn.get(i);
			if ( searchTerm.startsWith(oiIT.getFullName()) ) {
				alLoiAfterSearch.add(oiIT);
			}
		}
		if ( ! alLoiAfterSearch.isEmpty() ) {
			comparedGenomes_restrictMainListOrganismsToSubsetOfOrganisms(alLoiAfterSearch);
		} else {
			TaxoItem tiExactPathToBranch = Insyght.APP_CONTROLER.findPathInTaxoTreeWithTextString(searchTerm);
			if (tiExactPathToBranch != null) {
				ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiExactPathToBranch.getFullPathInHierarchie());
				comparedGenomes_restrictMainListOrganismsToSubsetOfOrganisms(alOiAfterSearch);

			} else {
				TaxoItem tiClosestPathInTree = Insyght.APP_CONTROLER.findClosestPathInTaxoTreeWithTextString(searchTerm);
				if (tiClosestPathInTree != null) {
					ArrayList<TaxoItem> alTaxoItemAndSubNodesWithPathInTree = Insyght.APP_CONTROLER.getListTaxoItemAndSubNodesWithPathInTree(tiClosestPathInTree.getFullPathInHierarchie());
					String approximateFound = "";
					if (alTaxoItemAndSubNodesWithPathInTree != null && ! alTaxoItemAndSubNodesWithPathInTree.isEmpty() ) {
						approximateFound = alTaxoItemAndSubNodesWithPathInTree.get(0).getName();
					}
					comparedGenomes_suggestBoxSearchMain.setText("CLOSEST MATCH FOR: "+searchTerm+ " IS: " +approximateFound);
					ArrayList<OrganismItem> alOiAfterSearch = Insyght.APP_CONTROLER.getListOrganismItemWithFullPathInHierarchie(tiClosestPathInTree.getFullPathInHierarchie());
					comparedGenomes_restrictMainListOrganismsToSubsetOfOrganisms(alOiAfterSearch);
				} else {
					comparedGenomes_suggestBoxSearchMain.setText("NO MATCH FOR: "+searchTerm);
					comparedGenomes_listDataProviderLOIMainListHasChanged = true;
					comparedGenomes_refreshListDataProviderMainListIfNedded();
				}
			}
		}
	}
	
	
	private static void buildUIStepChooseVisualization() {
		
		// set 
		// deal with VPStepVisualization
		// 0 : rbViewHomologTable
		// 1 : rbViewAnnotCompa
		// 2 : rbViewGenomicOrga
		
		VPStepVisualization.setSpacing(5);
		
		//HTML htmlTypeOfView = new HTML("<ul><li><big><b>Type of view : </b></big></li></ul>");
		HTML htmlTypeOfView = new HTML("<span style=\"font-size: large;\"><ul><li><b>Type of view : </b></li></ul></span>");
		VPStepVisualization.add(htmlTypeOfView);
		
		
		rbViewHomologTable = new RadioButton("VPStepVisualization",
				"<span style=\"color:#8B3A3A;\"><big>Orthologs table</big></span>"
				, true
				);
		rbViewHomologTable.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				choosenVisualisation = RefGenoPanelAndListOrgaResu.EnumResultViewTypes.homolog_table;
				Insyght.APP_CONTROLER.getUserSearchItem().setViewTypeDesired(RefGenoPanelAndListOrgaResu.EnumResultViewTypes.homolog_table);
				MenuItems_Visualization.setTitle("Type of view : Orthologs table");
			}
		});
		rbViewHomologTable.setTitle("A spreadsheet view dedicated to browsing orthologs."
				+ " It features a familiar layout (genes as columns, organisms as rows)"
				+ ", information (i.e. annotations, alignments, genomic location, etc.) at your fingertip"
				+ ", genes in adjacent columns with similar background color are part of the same synteny"
				+ ", multiple homologs can be stacked in 1 cell."
				+ " This view is interconnected with the 2 other views."
				);
		rbViewHomologTable.addStyleDependentName("leftPadding");
		VPStepVisualization.add(rbViewHomologTable);
		
		rbViewAnnotCompa = new RadioButton("VPStepVisualization",
				"<span style=\"color:#218868;\"><big>Annotations comparator</big></span>"
				, true);
		rbViewAnnotCompa.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				choosenVisualisation = RefGenoPanelAndListOrgaResu.EnumResultViewTypes.annotations_comparator;
				Insyght.APP_CONTROLER.getUserSearchItem().setViewTypeDesired(RefGenoPanelAndListOrgaResu.EnumResultViewTypes.annotations_comparator);
				MenuItems_Visualization.setTitle("Type of view : Annotations comparator");
			}
		});
		rbViewAnnotCompa.setTitle("The orthologs’ functional annotations are compared and classified into 3 categories."
				+ " [Shared] present in the reference gene and at least in one ortholog"
				+ ", [Missing] present in at least one ortholog but missing in the reference gene"
				+ ", [Unique] present in the reference gene but missing in orthologs."
				+ " You then have access to subcategories (i.e. functional annotation, homologous genes, sequence alignment, etc.)."
				+ " Furthermore you can restrict the set of organisms considered or filter homologs using criteria."
				+ " This view is interconnected with the 2 other views."
				);
		rbViewAnnotCompa.addStyleDependentName("leftPadding");
		VPStepVisualization.add(rbViewAnnotCompa);
		
		rbViewGenomicOrga = new RadioButton("VPStepVisualization",
				"<span style=\"color:#7A378B;\"><big>Genomic organization</big></span>"
				, true);
		rbViewGenomicOrga.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				choosenVisualisation = RefGenoPanelAndListOrgaResu.EnumResultViewTypes.genomic_organization;
				Insyght.APP_CONTROLER.getUserSearchItem().setViewTypeDesired(RefGenoPanelAndListOrgaResu.EnumResultViewTypes.genomic_organization);
				MenuItems_Visualization.setTitle("Type of view : View genomic organization");
			}
		});
		rbViewGenomicOrga.setTitle("A new way to explore the landscape of conserved and idiosyncratic genomic regions across multiple pair wise comparisons."
				+ " The symbols provide legibility whilst the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales."
				+ " You can synchronize the navigation among multiple compared genomes."
				+ " This view is interconnected with the 2 other views."
				);
		rbViewGenomicOrga.addStyleDependentName("leftPadding");
		VPStepVisualization.add(rbViewGenomicOrga);
		
		// default
		choosenVisualisation = RefGenoPanelAndListOrgaResu.EnumResultViewTypes.homolog_table;
		//Insyght.APP_CONTROLER.getUserSearchItem().setViewTypeDesired(RefGenoPanelAndListOrgaResu.EnumResultViewTypes.homolog_table);
		rbViewHomologTable.setValue(true, true);
		//MenuItems_Visualization.setTitle("Default: ortholog table");
		
		
	}
	
	
	private static void buildUIStepReferenceCDSs() {

		
		// deal with VP_SelectRefCDS
		// 0 : rbWholeProteome
		// 1 : rbCoreDispensableGeneSet
		// 2 : rbBuildGeneSet
		// 3 : step3VPRBElement
		VP_SelectRefCDS.setSpacing(5);
		
		rbWholeProteome = new RadioButton("VP_SelectRefCDS",
				"whole proteome"
				//+ " of "+Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
				);
		rbWholeProteome.addStyleDependentName("hiddenTextOverflow");
		rbWholeProteome.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				updateContentRadioButton_BrowseAllGene_updateUICenterAfterSelection();
				//updateContentRadioButton_BrowseAllGene_setRadioButtonOptionAsSelectedInUI(rbAllGOrga);
				updateContentRadioButton_BrowseAllGene_updateAssociatedVariables();
				//update the special taxo browser to show color
				if(selectedRefTaxoNode != null
						&& refNodeTaxoBrowserSelectionModel != null){
					forceRedrawTaxoBrowserRefNode("KEEP_CURRENT_SELECTION");
				}
			}
		});
		/*
		rbWholeProteome.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				updateContentRadioButton_BrowseAllGene_updateUICenterAfterSelection();
				//updateContentRadioButton_BrowseAllGene_setRadioButtonOptionAsSelectedInUI(rbAllGOrga);
				updateContentRadioButton_BrowseAllGene_updateAssociatedVariables();
				//update the special taxo browser to show color
				if(selectedRefTaxoNode != null
						&& refNodeTaxoBrowserSelectionModel != null){
					forceRedrawTaxoBrowserRefNode("KEEP_CURRENT_SELECTION");
				}
			}
		});*/
		rbWholeProteome.setTitle("Your search will include all the CDS of the reference and compared organisms");
		//rbWholeProteome.addStyleDependentName("cursorQuestionMark");
		VP_SelectRefCDS.add(rbWholeProteome);
		

		
		rbCoreDispensableGeneSet = new RadioButton("VP_SelectRefCDS",
				"Core / dispensable set of CDSs for reference node "
						//+ selectedRefTaxoNode.getFullName() + " (" + selectedRefTaxoNode.getSumOfLeafsUnderneath() + " organisms"
				);
		rbCoreDispensableGeneSet.addStyleDependentName("hiddenTextOverflow");
		rbCoreDispensableGeneSet.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				
				updateContentRadioButton_CoreDispGenome_updateUICenterAfterSelection();
				//updateContentRadioButton_CoreDispGenome_setRadioButtonOptionAsSelectedInUI(rbCoreDispGenome);
				//was updateContentRadioButton_CoreDispGenome_updateAssociatedVariables();
				//clear selection
				clearSelectionStep3(false, false);
				//Insyght.APP_CONTROLER.getUserSearchItem().setListReferenceGeneSet(alLGIFromCoreDispRequest);
				//manageUserSearchItem_ListReferenceGeneSet(true, alLGIFromCoreDispRequest, false, -1);
				doColorCellTaxoItemAccordingToGroupPlusMinus = true;
				
				//update the special taxo browser to show color
				if(selectedRefTaxoNode != null
						&& refNodeTaxoBrowserSelectionModel != null){
					forceRedrawTaxoBrowserRefNode("KEEP_CURRENT_SELECTION");
				}
				updateCurrentCoreDispSetCDSs();
			}
		});
		rbCoreDispensableGeneSet.setTitle("Select CDSs from the reference genome based on whether it is core or dispensable in relation to the other organisms in the taxonomic node you selected."
				+ " This gene set can then be visualised with the Orthologs table or the Annotations comparator."
				+ " This option is disabled if you selected a reference organism as opposed to a reference taxonomic group."
				);
		//rbCoreDispensableGeneSet.addStyleDependentName("cursorQuestionMark");
		VP_SelectRefCDS.add(rbCoreDispensableGeneSet);
		
		
		rbBuildGeneSet = new RadioButton("VP_SelectRefCDS",
				"Build your reference gene set from the available reference molecules"
				//+ " of "+Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getFullName()
				);
		rbBuildGeneSet.addStyleDependentName("hiddenTextOverflow");
		rbBuildGeneSet.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				//step3VPRBElement.setVisible(true);
				updateContentRadioButton_BuildGeneSet_updateUICenterAfterSelection();
				//updateContentRadioButton_CoreDispGenome_setRadioButtonOptionAsSelectedInUI(rbCoreDispGenome);
				//set default Build your reference gene set if needed
				setDefaultselectedMoleculeForGeneBuilding();
				//updateContentRadioButton_BuildGeneSet_updateAssociatedVariables();
				//update the special taxo browser to show color
				if(selectedRefTaxoNode != null
						&& refNodeTaxoBrowserSelectionModel != null){
					forceRedrawTaxoBrowserRefNode("KEEP_CURRENT_SELECTION");
				}
				
			}
		});
		rbBuildGeneSet.setTitle("Build your reference gene set from the available molecules (chromosomes or plasmids) from the reference organism."
				+ " This gene set can then be visualised with the Orthologs table or the Annotations comparator."
				);
		VP_SelectRefCDS.add(rbBuildGeneSet);
		//VP_SelectRefCDS.add(step3VPRBElement);

		rbWholeProteome.setValue(true, true);
		//MenuItems_ReferenceCDSs.setTitle("Default : whole proteome");
		

		// Whole Proteome
		buttonSetRefOrganismFromRefNodeTaxoBrowser_WholeProtContext.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				onClickBtCoreDispSetAsRefGenome();
			}
		});
		
		
		// core disp
		buttonSetRefOrganismFromRefNodeTaxoBrowser_CoreDisp.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				onClickBtCoreDispSetAsRefGenome();
			}
		});
		//deal with core disp genome
		rbReferenceCDSs_mostOrthologsGroupPlus.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				setCoreDispParameter_criterion(EnumCoreDispParameterCriterion.mostOrthologsGroupPlus);
				parametersToCalculateCoreDispSetCDSshaveChanged = true;
				updateCurrentCoreDispSetCDSs();
			}
		});
//				new ClickHandler() {
//			@Override
//			public void onClick(ClickEvent event) {
//				coreDispParameter_criterion = EnumCoreDispParameterCriterion.MostOrthologsGroupPlus;
//				parametersToCalculateCoreDispSetCDSshaveChanged = true;
//			}
//		});

		rbReferenceCDSs_overrepresented.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				setCoreDispParameter_criterion(EnumCoreDispParameterCriterion.overrepresented);
				parametersToCalculateCoreDispSetCDSshaveChanged = true;
				updateCurrentCoreDispSetCDSs();
			}
		});
		rbReferenceCDSs_presenceAbsenceOrthologs.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				setCoreDispParameter_criterion(EnumCoreDispParameterCriterion.presenceAbsenceOrthologs);
				parametersToCalculateCoreDispSetCDSshaveChanged = true;
				updateCurrentCoreDispSetCDSs();
			}
		});
		//bCDSsCurrentSelectionCoreDispGeneSet_ViewDetails
		countCDSsCurrentSelectionCoreDispGeneSet.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if ( ! Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet().isEmpty() ) {
					//pop up
					if (coreDispParameter_criterion.compareTo(EnumCoreDispParameterCriterion.mostOrthologsGroupPlus) == 0) {
						GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.initListGenesWithAlLgi(
								//alLGIFromCoreDispRequest
								Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet()
								, PopUpListGenesDialog.GeneListSortType.SCORE_CountMostRepresPheno
								);
						GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.displayPoPup();
					} else if (coreDispParameter_criterion.compareTo(EnumCoreDispParameterCriterion.overrepresented) == 0) {
//						//was pop up gene list
						GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.initListGenesWithAlLgi(
								//alLGIFromCoreDispRequest,
								Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet()
								, PopUpListGenesDialog.GeneListSortType.SCORE_PValOverReprPheno
								);
						GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.displayPoPup();
					} else if (coreDispParameter_criterion.compareTo(EnumCoreDispParameterCriterion.presenceAbsenceOrthologs) == 0) {
						GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.initListGenesWithAlLgi(
								//alLGIFromCoreDispRequest
								Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet()
								, PopUpListGenesDialog.GeneListSortType.GENOMIC_POSITION
								);
						GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.displayPoPup();
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
								new Exception("ERROR in buildUIStepReferenceCDSs : "
										+ "coreDispParameter_criterion not recognized : "
										+ coreDispParameter_criterion.toString()
									));
					}
				}
			}
		});

		//default
		setCoreDispParameter_criterionToMostOrthologsGroupPlus();

		sugBox_CoreDisp.setWidth("90%");// 600px
		sugBox_CoreDisp.setAutoSelectEnabled(false);
		sugBox_CoreDisp.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					makeTheBestSelectionPossibleWithSearchTerm_CoreDisp(sugBox_CoreDisp.getText());
				}
			}
		});
		sugBox_CoreDisp.addSelectionHandler(new SelectionHandler<Suggestion>() {
			// @Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				makeTheBestSelectionPossibleWithSearchTerm_CoreDisp(event
						.getSelectedItem().getReplacementString());
			}
		});
		sugBox_CoreDisp.getValueBox().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				sugBox_CoreDisp.setText("");
			}
		});
		step2VPSuggestBoxOrga_CoreDisp.add(sugBox_CoreDisp);
		
		
		//deal with build your own gene set

		// the pagerPanel_moleculesFromBuildRefGeneSet
		
		// the LightElementItem list box
		cellList_moleculesFromBuildRefGeneSet = new CellList<LightElementItem>(new LightElementItemCell());
		cellList_moleculesFromBuildRefGeneSet.setPageSize(1000);
		cellList_moleculesFromBuildRefGeneSet.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		selectionModel_moleculesFromBuildRefGeneSet.addSelectionChangeHandler(new Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				if ( ! DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF) {
					if (selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet() != null && ! selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet().isEmpty()) {
						HTMLMoleculesSelectedToBeSearched.setHTML(selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet().size()+" molecules selected to be searched (see panel below)");
						updateContentRadioButton_BuildGeneSet_updateUICenterAfterSelection(); //deckPVGOorBGSCenter.showWidget(2);
						//selectLightElementItem(ei);
						//selectLightElementItem();
						loadDataRefOrgaStep3();
						manageUserSearchItem_ListReferenceGeneSet(true, alLGIFromBuildGeneSet, true, -1);
						doColorCellTaxoItemAccordingToGroupPlusMinus = false;
					} else {
						if (rbBuildGeneSet.getValue()) {
							deckPVGOorBGSCenter.showWidget(0);
						}
						HTMLMoleculesSelectedToBeSearched.setHTML("No molecules have been selected to be searched. Click on 1 or multiple molecules in the panel on the left.");
					}
				}
			}
		});
		cellList_moleculesFromBuildRefGeneSet.setSelectionModel(selectionModel_moleculesFromBuildRefGeneSet);
		// Add the CellList to the data provider in the database.
		dataProvider_moleculesFromBuildRefGeneSet = new ListDataProvider<LightElementItem>();
		dataProvider_moleculesFromBuildRefGeneSet.addDataDisplay(cellList_moleculesFromBuildRefGeneSet);
		pagerPanel_moleculesFromBuildRefGeneSet.setDisplay(cellList_moleculesFromBuildRefGeneSet);
		AnchorSelectAllMoleculesSelectedToBeSearched.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selectAllMoleculesFromBuildRefGeneSet(
						//true
						);
			}
		});
		AnchorDeselectAllMoleculesSelectedToBeSearched.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//selectAllMoleculesFromBuildRefGeneSet(false);
				selectionModel_moleculesFromBuildRefGeneSet.clear();
			}
		});
		
		// the filter box
		HTMLMaxGeneInCart.setHTML("<b>Your selection : (Max = "
				+ SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED + ")</b>");
		
		//selectLightElementItem(null);
		createBoxFilter();

		final MultiSelectionModel<LightGeneItem> selectionModel = new MultiSelectionModel<LightGeneItem>();

		buttonAddAllMaxFirstGeneInCart.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				int idxOfFirstSelected = 0;
				for(int j=0;j<Insyght.APP_CONTROLER.getAllListLightGeneItemFromSlectedElement().size();j++){
					LightGeneItem lgiToAdd = Insyght.APP_CONTROLER.getAllListLightGeneItemFromSlectedElement().get(j);
					boolean alreadyAdded = false;
					
					for (int i = 0; i < alLGIFromBuildGeneSet.size(); i++) {
						LightGeneItem lgiAlreadyAdded = alLGIFromBuildGeneSet.get(i);
						if (lgiAlreadyAdded.getGeneId() == lgiToAdd.getGeneId()) {
							// already added
							alreadyAdded = true;
						}
					}
					if (!alreadyAdded
							&& alLGIFromBuildGeneSet.size() < SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED) {
						idxOfFirstSelected = alLGIFromBuildGeneSet.size();
						alLGIFromBuildGeneSet.add(lgiToAdd);
					}
					if(alLGIFromBuildGeneSet.size() >= SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED){
						break;
					}
				}
				sortListReferenceGeneSetByStart();
				manageUserSearchItem_ListReferenceGeneSet(true, alLGIFromBuildGeneSet, true, idxOfFirstSelected);
				//refreshDisplayListSlectedLightGeneItemFromSlectedElementStep3(idxOfFirstSelected);
			}

		});	
				
		
		addButtonListSelectGene.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				// LightGeneItem lgiToAdd =
				// selectionModel.getSelectedObject();
				Set<LightGeneItem> setLgiToAdd = selectionModel.getSelectedSet();
				if(!setLgiToAdd.isEmpty()){
					TreeSet<LightGeneItem> TreeSetLgiToAdd = new TreeSet<LightGeneItem>(
							setLgiToAdd);
					Iterator<LightGeneItem> iter = TreeSetLgiToAdd.iterator();
					
					int idxOfFirstSelected = 0;
					while (iter.hasNext()) {
						// System.out.println(iter.next());
						LightGeneItem lgiToAdd = iter.next();
						boolean alreadyAdded = false;
						for (int i = 0; i < alLGIFromBuildGeneSet.size(); i++) {
							LightGeneItem lgiAlreadyAdded = alLGIFromBuildGeneSet.get(i);
							if (lgiAlreadyAdded.getGeneId() == lgiToAdd.getGeneId()) {
								// already added
								alreadyAdded = true;

								selectionModel.setSelected(lgiAlreadyAdded, true);
							}
						}
						if (!alreadyAdded
								&& alLGIFromBuildGeneSet.size() < SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED) {
							idxOfFirstSelected = alLGIFromBuildGeneSet.size();
							alLGIFromBuildGeneSet.add(lgiToAdd);
						}
					}
					sortListReferenceGeneSetByStart();
					manageUserSearchItem_ListReferenceGeneSet(true, alLGIFromBuildGeneSet, true, idxOfFirstSelected);
					//refreshDisplayListSlectedLightGeneItemFromSlectedElementStep3(idxOfFirstSelected);
				}
			}
		});

		// the gene list box
		cellListStep3 = new CellList<LightGeneItem>(new GeneCell());
		cellListStep3.setPageSize(30);
		cellListStep3
				.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellListStep3.setSelectionModel(selectionModel);

		// Add a selection model so we can select cells.
		// Add the CellList to the data provider in the database.
		dataProviderStep3 = new ListDataProvider<LightGeneItem>(
				Insyght.APP_CONTROLER
						.getAllListLightGeneItemFromSlectedElement());
		dataProviderStep3.addDataDisplay(cellListStep3);

		// Set the cellList as the display of the pagers. This example has two
		// pagers. pagerPanel is a scrollable pager that extends the range when
		// the
		// user scrolls to the bottom. rangeLabelPager is a pager that displays
		// the
		// current range, but does not have any controls to change the range.
		pagerPanel.setDisplay(cellListStep3);
		rangeLabelPager.setDisplay(cellListStep3);
		// deal with selected genes

		cellListStep3SelectedGenes = new CellList<LightGeneItem>(new GeneCell());
		cellListStep3SelectedGenes.setPageSize(30);
		cellListStep3SelectedGenes
				.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellListStep3SelectedGenes.setSelectionModel(selectionModel);
		dataProviderStep3SelectedGenes = new ListDataProvider<LightGeneItem>(alLGIFromBuildGeneSet);
		dataProviderStep3SelectedGenes.addDataDisplay(cellListStep3SelectedGenes);
		pagerPanelSelectedGenes.setDisplay(cellListStep3SelectedGenes);
		rangeLabelPagerSelectedGenes.setDisplay(cellListStep3SelectedGenes);
		
	}
	
	public static void refreshMoleculesFromBuildRefGeneSet(ArrayList<LightElementItem> alLEISent) {
		pagerPanel_moleculesFromBuildRefGeneSet.getDisplay().setVisibleRange(0, 1000);
		if (alLEISent == null) {
			dataProvider_moleculesFromBuildRefGeneSet.setList(new ArrayList<LightElementItem>());
		} else {
			ArrayList<LightElementItem> alLEIIT = new ArrayList<>(alLEISent);
			Collections.sort(alLEIIT, LightElementItemComparators.byAccessionComparator); 
			dataProvider_moleculesFromBuildRefGeneSet.setList(alLEIIT);
		}
		selectionModel_moleculesFromBuildRefGeneSet.clear();
	}
	
	public static void selectAllMoleculesFromBuildRefGeneSet (
			//boolean select
			) {
		/*Iterator<LightElementItem> it0 = null;
		//Iterator<LightElementItem> it0 = selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet().iterator();
		if (select) {
			it0 = dataProvider_moleculesFromBuildRefGeneSet.getList().iterator();
		} else {
			it0 = selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet().iterator();
		}
		DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = true;
	    while(it0.hasNext()){
	    	LightElementItem leiIT = it0.next();
	    	if ( ! it0.hasNext()) {
	    	    // last iteration
	    		DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false;
	    	}
	    	selectionModel_moleculesFromBuildRefGeneSet.setSelected(leiIT, select);
	    }
	    DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false; //safety if iterator empty*/
		Iterator<LightElementItem> it0 = dataProvider_moleculesFromBuildRefGeneSet.getList().iterator();
		DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = true;
	    while(it0.hasNext()){
	    	LightElementItem leiIT = it0.next();
	    	if ( ! it0.hasNext()) {
	    	    // last iteration
	    		DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false;
	    	}
	    	selectionModel_moleculesFromBuildRefGeneSet.setSelected(leiIT, true);
	    }
	    DO_NOT_LAUNCH_SEARCH_AMONG_SELECTED_MOLECULES_REF = false; //safety if iterator empty
	}
	
	private static void setCoreDispParameter_criterionToMostOrthologsGroupPlus() {
		rbReferenceCDSs_mostOrthologsGroupPlus.setValue(true, false);
		setCoreDispParameter_criterion(EnumCoreDispParameterCriterion.mostOrthologsGroupPlus);
		parametersToCalculateCoreDispSetCDSshaveChanged = true;
	}


	protected static void setCoreDispParameter_criterion(EnumCoreDispParameterCriterion enumCoreDispParameterCriterionSent) {
		coreDispParameter_criterion = enumCoreDispParameterCriterionSent;
	}


	protected static void updateCurrentCoreDispSetCDSs() {
		
		if (parametersToCalculateCoreDispSetCDSshaveChanged) {
			// coreDispParameter_criterion : numCoreDispParameterCriterion.mostOrthologsGroupPlus, overrepresented, presenceAbsenceOrthologs
			
			if (coreDispParameter_criterion.compareTo(EnumCoreDispParameterCriterion.mostOrthologsGroupPlus) == 0) {
				getCoreDispGeneSetMostOrthologsGroupPlus();
			} else if (coreDispParameter_criterion.compareTo(EnumCoreDispParameterCriterion.overrepresented) == 0) {
				if ( ! rbReferenceCDSs_overrepresented.isEnabled()) {
					setCoreDispParameter_criterionToMostOrthologsGroupPlus();
					getCoreDispGeneSetMostOrthologsGroupPlus();
				} else {
					getCoreDispGeneSetWithOverrepresentedGroupPlus();
				}
			} else if (coreDispParameter_criterion.compareTo(EnumCoreDispParameterCriterion.presenceAbsenceOrthologs) == 0) {
				if ( ! rbReferenceCDSs_presenceAbsenceOrthologs.isEnabled()) {
					setCoreDispParameter_criterionToMostOrthologsGroupPlus();
					getCoreDispGeneSetMostOrthologsGroupPlus();
				} else {
					getCoreDispGeneSetWithPresenceAbsenceInRefNodeContext();
				}
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
						new Exception("ERROR in updateCurrentCoreDispSetCDSs : "
								+ "coreDispParameter_criterion not recognized : "
								+ coreDispParameter_criterion.toString()
							));
			}
		}
	}


	// if (criterionCurrentSelectionCDSsCoreDispGeneSet.compareTo(EnumCriterionDefineCDSsCoreDispGeneSet.presenceGroupPlusAbsenceGroupMinus) == 0) {
	private static void getCoreDispGeneSetWithPresenceAbsenceInRefNodeContext() {
		
		FilterGeneItem fgi = new FilterGeneItem();
		fgi.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.FIRST);
		fgi.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Presence_absence_homology);
		boolean doGetGeneListWithFilter = false;
		
		if(!alRefTaxoNodeOrgaItemsInPhenoPlusCategory.isEmpty()){
			fgi.getListSubFilter().add("presence");
			ArrayList<Integer> alSelectedSpeciesIds = new ArrayList<Integer>();
			for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInPhenoPlusCategory){
				alSelectedSpeciesIds.add(tiIT.getAssociatedOrganismItem().getOrganismId());
			}
			fgi.getListSubFilter().add(
					//alSelectedSpeciesIds.toString().replaceAll("[\\[\\]]","")
					UtilitiesMethodsShared.getItemsAsStringFromCollection(alSelectedSpeciesIds)
					);
			doGetGeneListWithFilter = true;
		}
		if(!alRefTaxoNodeOrgaItemsInPhenoMinusCategory.isEmpty()){
			if(doGetGeneListWithFilter){
				fgi.getListSubFilter().add("AND");
			}
			fgi.getListSubFilter().add("absence");
			ArrayList<Integer> alSelectedSpeciesIds = new ArrayList<Integer>();
			for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInPhenoMinusCategory){
				alSelectedSpeciesIds.add(tiIT.getAssociatedOrganismItem().getOrganismId());
			}
			fgi.getListSubFilter().add(
					//alSelectedSpeciesIds.toString().replaceAll("[\\[\\]]","")
					UtilitiesMethodsShared.getItemsAsStringFromCollection(alSelectedSpeciesIds)
					);
			doGetGeneListWithFilter = true;
		}
		
		
		if(doGetGeneListWithFilter){
			ArrayList<FilterGeneItem> listFilters = new ArrayList<FilterGeneItem>();
			listFilters.add(fgi);
			if(Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism() != null){
				ArrayList<Integer> alEletId = new ArrayList<Integer>();
				for(LightElementItem leiIT : Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getListAllLightElementItem()) {
					alEletId.add(leiIT.getElementId());
				}
				
				//htmlStep3ResultingReferenceGenesSetInTaxoNodeContext.setHTML("<span style=\"color:green;\">Loading, please wait...</span>");
				sld.loadGeneListForSelectedFilters(
						listFilters
						, alEletId
//						, alOrgaId
//						, null
						, FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateCoreDispSearchTab
						, false
						);
				
			}else{
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getGeneSetWithPresenceAbsenceInRefNodeContext :" +
						" selectedRefOiFromRefTaxoNode is null"));
			}
		}
	}
	
	private static void getCoreDispGeneSetMostOrthologsGroupPlus () {
		ArrayList<Integer> alSelectedPhenoPlusOrgaIds = new ArrayList<Integer>();
		for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInPhenoPlusCategory){
			alSelectedPhenoPlusOrgaIds.add(tiIT.getAssociatedOrganismItem().getOrganismId());
		}
//		ArrayList<Integer> alSelectedPhenoMinusOrgaIds = new ArrayList<Integer>();
//		for(OrganismItem oiIT : alRefTaxoNodeOrgaItemsInPhenoMinusCategory){
//			alSelectedPhenoMinusOrgaIds.add(oiIT.getOrganismId());
//		}
		sld.getReferenceGenesMostRepresentedPhenotypePlus(
					Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getOrganismId(),
					alSelectedPhenoPlusOrgaIds);
	}
	
	private static void getCoreDispGeneSetWithOverrepresentedGroupPlus() {
		ArrayList<Integer> alSelectedPhenoPlusOrgaIds = new ArrayList<Integer>();
		for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInPhenoPlusCategory){
			alSelectedPhenoPlusOrgaIds.add(tiIT.getAssociatedOrganismItem().getOrganismId());
		}
		ArrayList<Integer> alSelectedPhenoMinusOrgaIds = new ArrayList<Integer>();
		for(TaxoItem tiIT : alRefTaxoNodeOrgaItemsInPhenoMinusCategory){
			alSelectedPhenoMinusOrgaIds.add(tiIT.getAssociatedOrganismItem().getOrganismId());
		}
		
		if(alSelectedPhenoMinusOrgaIds.isEmpty()){
			// Ref genes most represented in "phenotype +" (count)
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getCoreDispGeneSetWithOverrepresentedGroupPlus :" +
					" alSelectedPhenoMinusOrgaIds.isEmpty()"));
		} else {
			//Ref genes over-represented in "phenotype +" and under-represented in "phenotype -" (P-value)
			sld.getReferenceGenesAndOrthologsOverRepresentatedInPhenotype(
					Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism().getOrganismId(),
					alSelectedPhenoPlusOrgaIds,
					alSelectedPhenoMinusOrgaIds
					//alSelectedNotTakenIntoAccountOrgaIds
					);
		}

	}
	
	
	protected static void setDefaultselectedMoleculeForGeneBuilding() {
		if ( dataProvider_moleculesFromBuildRefGeneSet.getList() == null || dataProvider_moleculesFromBuildRefGeneSet.getList().isEmpty() ){
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
					new Exception("ERROR in setDefaultselectedMoleculeForGeneBuilding : "
							+ " dataProvider_moleculesFromBuildRefGeneSet.getList() is null or empty : " 
							+ ( (dataProvider_moleculesFromBuildRefGeneSet.getList() != null ) ? dataProvider_moleculesFromBuildRefGeneSet.getList().toString() : "NULL" )
							+ " ; getReferenceOrganism = " + Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism()
							)
					);
		} else {
			if ( selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet().isEmpty() ) {
				selectionModel_moleculesFromBuildRefGeneSet.setSelected(dataProvider_moleculesFromBuildRefGeneSet.getList().get(0), true);
			}
		}
		
	}

	protected static void showBasicGeneInfoSearchTab(
			LightGeneItem lgiToGetDetailledInfo) {

		String textBasicGeneInfo = "<big><b>Gene Info: </b>";
		textBasicGeneInfo += "<ul>";
		textBasicGeneInfo += "<li><i>Name : </i>"
				+ lgiToGetDetailledInfo
						.getMostSignificantGeneNameAsStrippedHTMLPlusLink()+"</li>";

		textBasicGeneInfo += "<li><i>Locus tag : </i>"
				+ lgiToGetDetailledInfo.getLocusTagAsHTMLPlusLink()+"</li>";

		String location_pre = "";
		String location_post = "";
		if (lgiToGetDetailledInfo.getStrand() == -1) {
			location_pre = "complement(";
			location_post = ")";
		}
		textBasicGeneInfo += "<li><i>Location : </i>"
						+ location_pre
						+ NumberFormat.getFormat("###,###,###,###,###,###.##").format(lgiToGetDetailledInfo.getStart())
						+ ".."
						+ NumberFormat.getFormat("###,###,###,###,###,###.##").format(lgiToGetDetailledInfo.getStop())
						+ location_post;
				
//		textBasicGeneInfo += "<li><i>Start : </i>"
//				+ NumberFormat.getFormat("###,###,###,###,###,###.##").format(lgiToGetDetailledInfo.getStart())+"</li>";
//		textBasicGeneInfo += "<li><i>Stop : </i>"
//				+ NumberFormat.getFormat("###,###,###,###,###,###.##").format(lgiToGetDetailledInfo.getStop())+"</li>";
		//textBasicGeneInfo += "<li><i>Element : </i>" + lgiToGetDetailledInfo.getType()+"</li>";
		
		textBasicGeneInfo += "</ul></big>";
		
		textBasicGeneInfo += "<br/><br/>Loading ...";
		
		mySearchPopupInfo.setDirectHtml(textBasicGeneInfo);
		mySearchPopupInfo.setCallBackHtml("");
		mySearchPopupInfo.adaptSizeAndShow();
	}

	
	/** Creation de la boite de filtres **/
	public static void createBoxFilter() {

		vpFilter.clear();
		addARootFilter(true, vpFilter, false);
		addAnotherRootFilter.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				addARootFilter(false, vpFilter, false);
			}
		});
		addAnotherRootFilter
				.setTitle("Clicking this bouton adds a filter."
						+ " You can combine multiple filters to build a biologically relevant query according to multiple criteria."
						+ " For example, you could list the CDSs that are niche specific / core genome and related to a given molecular function or biological process."
						+ " The operators AND (intersection) and OR (union) are supported."
						+ " You can combine an unlimited number of filters.");
		vpFilter.add(addAnotherRootFilter);

	}


	
	public static void loadDataRefOrgaStep3(
			//Set<LightElementItem> setLeiSelected
			) {
		
		Insyght.APP_CONTROLER.getAllListLightGeneItemFromSlectedElement().clear();
		
		// Window.alert("build search obj");
		if (selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet() == null || selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet().isEmpty() ) {
			return;
		}
		
		int maxSizeAmongSelectedElement = -1;
	    Iterator<LightElementItem> it0 = selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet().iterator();
	    while(it0.hasNext()){
	    	int sizeIt = it0.next().getSize();
			if (sizeIt > 0) {
				if (sizeIt > maxSizeAmongSelectedElement) {
					maxSizeAmongSelectedElement = sizeIt;
				}
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in loadDataRefOrgaStep3 :" +
				" size of element "+it0.next().getAccession()+" id = "+it0.next().getElementId()+" <= 0"));
				return;
			}
	    }

		ArrayList<FilterGeneItem> listFilters = new ArrayList<FilterGeneItem>();
		int parseStatus = parseVPFiltersN(vpFilter, listFilters
				,maxSizeAmongSelectedElement
				);

		if (parseStatus == 1 && !listFilters.isEmpty()) {
			ArrayList<Integer> alEletIdToSent = new ArrayList<Integer>();
		    Iterator<LightElementItem> it1 = selectionModel_moleculesFromBuildRefGeneSet.getSelectedSet().iterator();
		    while(it1.hasNext()){
		    	alEletIdToSent.add(it1.next().getElementId());
		    }
			refreshListStep3.setEnabled(false);
			deckPFetchGeneList.showWidget(0);
			sld.loadGeneListForSelectedFilters(
					listFilters
					//, null
					, alEletIdToSent
					, FilterGeneItem.EnumUpdateTypeLoadGeneListForSelectedFilters.updateBuildGeneSetSearchTab
					, false
					);
		} else if (parseStatus == 2) {
			doClearAllFilters(vpFilter);
			loadDataRefOrgaStep3();
		}

	}

	public static void doubleClickOnGeneList(NativeEvent event, LightGeneItem value) {
		showBasicGeneInfoSearchTab(value);
		@SuppressWarnings("unused")
		GetResultDialog lDiag = new GetResultDialog(
				value.getGeneId(), true,
					false);
	}

	/** clear all filters **/
	public static void doClearAllFilters(
			//boolean doGetListGenesWithFilters
			VerticalPanel vpSent
			) {
		// del additional filters if present
		delAdditionalFiltersIfPresent(vpSent);

		// clearTheFieldOfAllFilters
		clearTheFieldOfAllFilters(vpSent);

		// set first filter to genomic location
		//for this class is vpFilter
		if (vpSent.getWidget(0) instanceof ListBox) {
			((ListBox) vpSent.getWidget(0)).setItemSelected(0, true);
			((ListBox) vpSent.getWidget(0))
					.setTitle("List genes whose genomic locations are within a given boundaries. Start and stop must be entered in base pairs relative to the origin of replication ORI.");
			if (vpSent.getWidget(1) instanceof DeckPanel) {
				((DeckPanel) vpSent.getWidget(1)).showWidget(0);
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR doClearAllFilters:" +
						" detail.getWidget(1) not instanceof DeckPanel"));
			}
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR doClearAllFilters:" +
					" detail.getWidget(0) not instanceof ListBox"));
		}

	}

	private static void clearTheFieldOfAllFilters(VerticalPanel vpSent) {
		
		if (vpSent.getWidget(0) instanceof ListBox) {
			if (vpSent.getWidget(1) instanceof DeckPanel) {

				// reset genomic location
				if (((DeckPanel) vpSent.getWidget(1)).getWidget(0) instanceof VerticalPanel) {
					if (((VerticalPanel) ((DeckPanel) vpSent.getWidget(1))
							.getWidget(0)).getWidget(1) instanceof TextBox) {
						((TextBox) ((VerticalPanel) ((DeckPanel) vpSent
								.getWidget(1)).getWidget(0)).getWidget(1))
								.setText("");
						if (((VerticalPanel) ((DeckPanel) vpSent.getWidget(1))
								.getWidget(0)).getWidget(3) instanceof TextBox) {
							((TextBox) ((VerticalPanel) ((DeckPanel) vpSent
									.getWidget(1)).getWidget(0)).getWidget(3))
									.setText("");
						} else {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
									" ((VerticalPanel)((DeckPanel)detail.getWidget(1)).getWidget(0)).getWidget(3) not instanceof TextBox"));
						}
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
								" ((VerticalPanel)((DeckPanel)detail.getWidget(1)).getWidget(0)).getWidget(1) not instanceof TextBox"));
					}
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
							" ((DeckPanel)detail.getWidget(1)).getWidget(0) not instanceof VerticalPanel"));
				}

				// reset presence / absence of homology
				if (((DeckPanel) vpSent.getWidget(1)).getWidget(1) instanceof VerticalPanel) {
					if (((VerticalPanel) ((DeckPanel) vpSent.getWidget(1))
							.getWidget(1)).getWidget(0) instanceof ListBox) {
						((ListBox) ((VerticalPanel) ((DeckPanel) vpSent
								.getWidget(1)).getWidget(1)).getWidget(0))
								.setSelectedIndex(0);
						((ListBox) ((VerticalPanel) ((DeckPanel) vpSent
								.getWidget(1)).getWidget(1)).getWidget(0))
								.setTitle("Find genes for which there is at least one homolog in each given compared genomes.");
						if (((VerticalPanel) ((DeckPanel) vpSent.getWidget(1))
								.getWidget(1)).getWidget(2) instanceof MultiValueSuggestBox) {
							((VerticalPanel) ((DeckPanel) vpSent.getWidget(1))
									.getWidget(1)).remove(2);
							((VerticalPanel) ((DeckPanel) vpSent.getWidget(1))
									.getWidget(1))
									.add(new MultiValueSuggestBox(oracle,
											"blue", "13em", 50));
						} else {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
									" ((VerticalPanel)((DeckPanel)detail.getWidget(1)).getWidget(1)).getWidget(3) instanceof" +
									" MultiValueSuggestBox"));
						}
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
								" ((VerticalPanel)((DeckPanel)detail.getWidget(1)).getWidget(1)).getWidget(0) instanceof ListBox"));
					}
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
							" ((DeckPanel)detail.getWidget(1)).getWidget(0) not instanceof VerticalPanel"));
				}

				// reset genomic location
				if (((DeckPanel) vpSent.getWidget(1)).getWidget(2) instanceof VerticalPanel) {
					if (((VerticalPanel) ((DeckPanel) vpSent.getWidget(1))
							.getWidget(2)).getWidget(1) instanceof TextBox) {
						((TextBox) ((VerticalPanel) ((DeckPanel) vpSent
								.getWidget(1)).getWidget(2)).getWidget(1))
								.setText("0");
						if (((VerticalPanel) ((DeckPanel) vpSent.getWidget(1))
								.getWidget(2)).getWidget(3) instanceof TextBox) {
							((TextBox) ((VerticalPanel) ((DeckPanel) vpSent
									.getWidget(1)).getWidget(2)).getWidget(3))
									.setText("MAX_LENGHT");
						} else {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
									" ((VerticalPanel)((DeckPanel)detail.getWidget(1)).getWidget(2)).getWidget(3) not instanceof TextBox"));
						}
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
								" ((VerticalPanel)((DeckPanel)detail.getWidget(1)).getWidget(2)).getWidget(1) not instanceof TextBox"));
					}
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
							" ((DeckPanel)detail.getWidget(1)).getWidget(2) not instanceof VerticalPanel"));
				}
				
				// reset identifiers and all other qualifiers fields
				for (int i = 3; i < ((DeckPanel) vpSent.getWidget(1)).getWidgetCount(); i++) {
					if (((DeckPanel) vpSent.getWidget(1)).getWidget(i) instanceof VerticalPanel) {
						if (((VerticalPanel) ((DeckPanel) vpSent.getWidget(1))
								.getWidget(i)).getWidget(0) instanceof ListBox) {
							((ListBox) ((VerticalPanel) ((DeckPanel) vpSent
									.getWidget(1)).getWidget(i)).getWidget(0))
									.setSelectedIndex(0);
							((ListBox) ((VerticalPanel) ((DeckPanel) vpSent
									.getWidget(1)).getWidget(i)).getWidget(0))
									.setTitle("Find genes for which the filter's field contain your search term at any position.");
							if (((VerticalPanel) ((DeckPanel) vpSent
									.getWidget(1)).getWidget(i)).getWidget(1) instanceof TextBox) {
								((TextBox) ((VerticalPanel) ((DeckPanel) vpSent
										.getWidget(1)).getWidget(i))
										.getWidget(1)).setText("");
							} else {
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
										" ((VerticalPanel)((DeckPanel)detail.getWidget(1)).getWidget(i)).getWidget(1) instanceof" +
										" TextBox with i="
										+ i));
							}
						} else {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
									" ((VerticalPanel)((DeckPanel)detail.getWidget(1)).getWidget(i)).getWidget(0) instanceof ListBox"));
						}
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
								" ((DeckPanel)detail.getWidget(1)).getWidget(0) not instanceof VerticalPanel"));
					}
				}

				// giLbChoiceSurrMatch.setTitle("Find genes for which the filter's field contain your search term at any position.");

			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters:" +
						" detail.getWidget(1) not instanceof DeckPanel"));
			}
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR clearTheFieldOfAllFilters: detail.getWidget(0)" +
					" not instanceof ListBox"));
		}

	}

	/** ne laisse que le premier filtre **/
	private static void delAdditionalFiltersIfPresent(VerticalPanel vpSent) {
		for (int i = 2; vpSent.getWidgetCount() > 3; i++) {
			vpSent.remove(i);
			i--;
		}
	}

	/**
	 * construit la liste de filtre a partir de input utilisateur. code 1: all
	 * good ; code 2: no filter ; code -1: error
	 **/
	public static int parseVPFiltersN(VerticalPanel vpFilterToParse,
			ArrayList<FilterGeneItem> listFiltersToFill
			, int maxSizeAmongSelectedElement
			) {

		boolean noFilterMode = false;

		for (int i = 0; i < vpFilterToParse.getWidgetCount(); i++) {
			Widget widgListBoxFirstIT = vpFilterToParse.getWidget(i);
			if (widgListBoxFirstIT instanceof ListBox) {
				// first type of match
				if ((i + 1) < vpFilterToParse.getWidgetCount()) {
					Widget widgNext = vpFilterToParse.getWidget(i + 1);
					if (widgNext instanceof DeckPanel) {
						// first type of match
						FilterGeneItem fgiIT = new FilterGeneItem();
						fgiIT.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.FIRST);
						VerticalPanel vpToParse = setTypeFilterAndDPWidget(
								fgiIT, widgListBoxFirstIT, widgNext,
								"loadDataStep3 : first type");
						if (vpToParse != null) {
							if (noFilterMode) {
								return 2;
							}
							int statusFilter = loopOverVerticalPanelAndFillFilter(
									vpToParse, fgiIT
									, maxSizeAmongSelectedElement
									);
							if (statusFilter < 0) {
								return -1;
							}
						} else {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in parseVPFiltersN :" +
									" first type of match couldn't determine vpNextIT Bis"));
							return -1;
						}
						listFiltersToFill.add(fgiIT);

					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in parseVPFiltersN :" +
								" not listBox-DeckPanel neither listBox-listBox 1"));
					}
				}
			} else if (widgListBoxFirstIT instanceof HorizontalPanel) {
				// operator
				if ((i + 1) < vpFilterToParse.getWidgetCount()) {
					Widget widgNext = vpFilterToParse.getWidget(i + 1);

					if (widgNext instanceof ListBox) {
						if ((i + 2) < vpFilterToParse.getWidgetCount()) {
							Widget widgNextNext = vpFilterToParse
									.getWidget(i + 2);
							if (widgNextNext instanceof DeckPanel) {
								// second type of match
								i++;
								FilterGeneItem fgiIT = new FilterGeneItem();

								Widget listBoxOpera = ((HorizontalPanel) widgListBoxFirstIT)
										.getWidget(0);
								if (listBoxOpera instanceof ListBox) {
									// ok
								} else {
									Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in parseVPFiltersN :" +
											" widgNextListBox not instanceof ListBox"));
									return -1;
								}
								setOperatorFilter(fgiIT, listBoxOpera,
										"loadDataStep3 : second type");

								VerticalPanel vpToParse = setTypeFilterAndDPWidget(
										fgiIT, widgNext, widgNextNext,
										"loadDataStep3 : second type");
								if (vpToParse != null) {
									if (noFilterMode) {
										return 2;
									}
									int statusFilter = loopOverVerticalPanelAndFillFilter(
											vpToParse, fgiIT
											, maxSizeAmongSelectedElement
											);
									if (statusFilter < 0) {
										return -1;
									}
								} else {
									Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in parseVPFiltersN :" +
											" second type of match couldn't determine vpNextIT Bis"));
									return -1;
								}
								listFiltersToFill.add(fgiIT);

							} else {
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in parseVPFiltersN :" +
										" not listBox-listBox-DeckPanel 2"));
							}

						}
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in parseVPFiltersN :" +
								" not listBox-DeckPanel neither listBox-listBox 1"));
					}

				}
			}
		}
		return 1;
	}

	/** parse la recherche utilisateur et construit le filtre **/
	private static Integer loopOverVerticalPanelAndFillFilter(
			VerticalPanel vpNextIT,
			FilterGeneItem fgiIT,
			int maxSizeAmongSelectedElement
			) {

		for (int j = 0; j < ((VerticalPanel) vpNextIT).getWidgetCount(); j++) {
			Widget widgOfvpNextITIT = ((VerticalPanel) vpNextIT).getWidget(j);

			if (widgOfvpNextITIT instanceof TextBox
					&& (fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Cellular_component) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.db_xref) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.EC_number) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.Evidence) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.Function) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.Biological_process) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.Gene_name) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.Genomic_locations) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.Notes_product) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.Lenght_protein) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.CountMostRepresPheno) == 0
							|| fgiIT.getTypeFilterGeneBy().compareTo(
									EnumTypeFilterGeneBy.PValOverReprPheno) == 0
							// Gene_id, Element_id, Organism_id are hidden so no need to parse them
							)) {

				String rawText = ((TextBox) widgOfvpNextITIT).getText().trim();
				if (fgiIT.getTypeFilterGeneBy().compareTo( EnumTypeFilterGeneBy.Genomic_locations) == 0) {
					rawText = rawText.replace(",", "");
				}
				boolean rawTextIsEmpty = false;

				if (rawText == null) {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in loopOverVerticalPanelAndFillFilter :" +
							" couldn't determine rawText"));
					return -1;
				} else if (
						( fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Genomic_locations) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Lenght_protein) == 0)
						&& (j - 1) % 5 == 0) {
					// each start
					if (rawText.isEmpty()) {
						((TextBox) widgOfvpNextITIT).setText("0");
						rawText = "0";
					} else if (!rawText.trim().matches("^\\d*$")) {
						((TextBox) widgOfvpNextITIT).setText("0");
						rawText = "0";
					} else if (Integer.parseInt(rawText) < 0) {
						((TextBox) widgOfvpNextITIT).setText("0");
						rawText = "0";
					} else {
						// fine
					}
				} else if (
						( fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Genomic_locations) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Lenght_protein) == 0)
						&& (j - 3) % 5 == 0) {
					// each stop
					if (rawText.isEmpty()) {
						if(fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Genomic_locations) == 0
								&& maxSizeAmongSelectedElement > 0){
							((TextBox) widgOfvpNextITIT).setText("MAX_STOP ("
									+ maxSizeAmongSelectedElement + ")");
							rawText = "" + maxSizeAmongSelectedElement;
						}else{
							((TextBox) widgOfvpNextITIT).setText("MAX_STOP");
							rawText = "" + Integer.MAX_VALUE;
						}
					} else if (!rawText.trim().matches("^\\d*$")) {
						if(fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Genomic_locations) == 0
								&& maxSizeAmongSelectedElement > 0){
							((TextBox) widgOfvpNextITIT).setText("MAX_STOP ("
									+ maxSizeAmongSelectedElement + ")");
							rawText = "" + maxSizeAmongSelectedElement;
						}else{
							((TextBox) widgOfvpNextITIT).setText("MAX_STOP");
							rawText = "" + Integer.MAX_VALUE;
						}
					} else if (fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Genomic_locations) == 0
							&& Integer.parseInt(rawText) > maxSizeAmongSelectedElement) {
						if(maxSizeAmongSelectedElement > 0){
							((TextBox) widgOfvpNextITIT).setText("MAX_STOP ("
									+ maxSizeAmongSelectedElement + ")");
							rawText = "" + maxSizeAmongSelectedElement;
						}
					} else {
						// fine
					}
				} else if (rawText.isEmpty()) {
					rawTextIsEmpty = true;
				}
				String finalText = "";

				// inner AND/OR modifier 2 before ?
				if (j - 2 >= 0) {
					if (((VerticalPanel) vpNextIT).getWidget(j - 2) instanceof ListBox) {
						// fgiIT.getListSubFilter().add("AND/OR");
						Widget lbInnerAndOrModif = ((VerticalPanel) vpNextIT)
								.getWidget(j - 2);
						if (((ListBox) lbInnerAndOrModif).getItemCount() == 3) {
							// inner AND/OR modifier 2 before
							if (((ListBox) lbInnerAndOrModif)
									.getSelectedIndex() == 0) {
								// inner AND
								fgiIT.getListSubFilter().add("AND");
							} else if (((ListBox) lbInnerAndOrModif)
									.getSelectedIndex() == 1) {
								// inner OR
								fgiIT.getListSubFilter().add("OR");
							} else {
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in loopOverVerticalPanelAndFillFilter :" +
										" couldn't determine inner AND/OR modifier 2 before"));
								return -1;
							}
						}
					}
				}

				// need to match no bracket before stringToMatch ?:
				// ^[^{]*9.*$');
				String AllCharOrAllCharButBracket = ".";
				if (fgiIT.getTypeFilterGeneBy().compareTo(
						EnumTypeFilterGeneBy.EC_number) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Evidence) == 0) {
					AllCharOrAllCharButBracket = "[^{]";
				}
				if (fgiIT.getTypeFilterGeneBy().compareTo(
						EnumTypeFilterGeneBy.Function) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Biological_process) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Cellular_component) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(
								EnumTypeFilterGeneBy.Notes_product) == 0) {
					if (rawText.startsWith("GO:")
							|| (rawText.matches("^\\d+$") && rawText.length() > 3)) {
						// do nothing
					} else {
						AllCharOrAllCharButBracket = "[^{]";
					}
				}

				// reg modifiers just before ?
				if (j - 1 >= 0) {
					if (((VerticalPanel) vpNextIT).getWidget(j - 1) instanceof ListBox) {
						// text modified by contaning etc...
						Widget lbMofifierRegexText = ((VerticalPanel) vpNextIT)
								.getWidget(j - 1);
						String escapedRawText = escapRawText(rawText);
						if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("containing")==0) {
							// containing
							finalText = " ~* '^" + AllCharOrAllCharButBracket
									+ "*" + escapedRawText + ".*$'";
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("not_containing")==0) {
							// not containing
							finalText = " !~* '^" + AllCharOrAllCharButBracket
									+ "*" + escapedRawText + ".*$'";
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("starting_with")==0) {
							// starting with
							finalText = " ~* '" + "^" + escapedRawText + ".*$'";
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("not_starting_with")==0) {
							// not starting with
							finalText = " !~* '" + "^" + escapedRawText
									+ ".*$'";
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("ending_with")==0) {
							// ending with
							finalText = " ~* '" + "^"
									+ AllCharOrAllCharButBracket + "*"
									+ escapedRawText + "(\\s\\{.*)*$'";
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("not_ending_with")==0) {
							// not ending with
							finalText = " !~* '" + "^"
									+ AllCharOrAllCharButBracket + "*"
									+ escapedRawText + "(\\s\\{.*)*$'";
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("strictly")==0) {
							// strictly
							finalText = " ~* '" + "^" + escapedRawText
									+ "(\\s\\{.*)*$'";
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("strictly_not")==0) {
							// strictly not
							finalText = " !~* '" + "^" + escapedRawText
									+ "(\\s\\{.*)*$'";
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("is_null")==0) {
							// IS_NULL
							finalText = "IS_NULL";
							rawTextIsEmpty = false;
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("is_not_null")==0) {
							// IS_NOT_NULL
							finalText = "IS_NOT_NULL";
							rawTextIsEmpty = false;
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("POSIX_Regex")==0) {
							// POSIX Regex
							if (rawText.startsWith(".+")) {
								rawText = rawText.replaceFirst("\\.\\+", "");
								rawText = AllCharOrAllCharButBracket + "+"
										+ rawText;
							}
							if (rawText.startsWith(".*")) {
								rawText = rawText.replaceFirst("\\.\\*", "");
								rawText = AllCharOrAllCharButBracket + "*"
										+ rawText;
							}
							finalText = " ~* '" + "^" + rawText + "$'";
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("greater_than")==0) {
							// greater_than
							finalText = "> " + rawText;
							rawTextIsEmpty = false;
						} else if (((ListBox) lbMofifierRegexText).getSelectedValue().compareTo("lower_than")==0) {
							// lower_than
							finalText = "< " + rawText;
							rawTextIsEmpty = false;
						} else {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in loopOverVerticalPanelAndFillFilter :" +
									" couldn't determine modifier regex selection"));
							return -1;
						}
					}
				}

				if (rawTextIsEmpty) {
					Window.alert("Please fill in all the fields for the filter "
							+ fgiIT.getTypeFilterGeneBy().toString());
					return -1;
				}

				if (finalText.isEmpty()) {
					finalText = rawText;
				}
				
				fgiIT.getListSubFilter().add(finalText);

			} else if (widgOfvpNextITIT instanceof MultiValueSuggestBox
					&& fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Presence_absence_homology) == 0) {
				// Presence_absence_homology

				// inner AND/OR modifier 3 before ?
				if (j - 3 >= 0) {
					if (((VerticalPanel) vpNextIT).getWidget(j - 3) instanceof ListBox) {
						// fgiIT.getListSubFilter().add("AND/OR");
						// System.out.println("here");
						Widget lbInnerAndOrModif = ((VerticalPanel) vpNextIT)
								.getWidget(j - 3);
						if (((ListBox) lbInnerAndOrModif).getItemCount() == 3) {
							// inner AND/OR modifier 2 before
							// System.out.println("here2");
							if (((ListBox) lbInnerAndOrModif)
									.getSelectedIndex() == 0) {
								// inner AND
								fgiIT.getListSubFilter().add("AND");
							} else if (((ListBox) lbInnerAndOrModif)
									.getSelectedIndex() == 1) {
								// inner OR
								fgiIT.getListSubFilter().add("OR");
							} else {
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in loopOverVerticalPanelAndFillFilter :" +
										" couldn't determine inner AND/OR modifier 3 before"));
								return -1;
							}
						}
					}
				}

				// Presence / absence modifier 2 before ?
				// presence
				// absence
				if (j - 2 >= 0) {

					if (((VerticalPanel) vpNextIT).getWidget(j - 2) instanceof ListBox) {
						// fgiIT.getListSubFilter().add("AND/OR");

						Widget lbInnerAndOrModif = ((VerticalPanel) vpNextIT)
								.getWidget(j - 2);
						if (((ListBox) lbInnerAndOrModif).getItemCount() == 2) {
							// inner AND/OR modifier 2 before
							if (((ListBox) lbInnerAndOrModif)
									.getSelectedIndex() == 0) {
								// absence
								fgiIT.getListSubFilter().add("presence");
							} else if (((ListBox) lbInnerAndOrModif)
									.getSelectedIndex() == 1) {
								// absence
								fgiIT.getListSubFilter().add("absence");
							} else {
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in loopOverVerticalPanelAndFillFilter :" +
										" couldn't determine inner presence/absence modifier 2 before"));
								return -1;
							}
						}
					}
				}

				ArrayList<String> alSelectedSpecies = ((MultiValueSuggestBox) widgOfvpNextITIT)
						.getListItemsSelected();
				if (alSelectedSpecies.isEmpty()) {
					Window.alert("Please fill in all the fields for the filter Presence / absence of homology");
					return -1;
				}

				ArrayList<Integer> alSelectedSpeciesIds = new ArrayList<Integer>();
//				String stToAddToShowInfoAboutOrgaUnderNode = "";
//				boolean doShowListOrganismsUnderNode = false;
				for (int i = 0; i < alSelectedSpecies.size(); i++) {
					String selectedSpIT = alSelectedSpecies.get(i);
					ArrayList<OrganismItem> alOiIT = Insyght.APP_CONTROLER
							.getListOrganismItemWithStringFullName(selectedSpIT);
					if (!alOiIT.isEmpty()) {
//						stToAddToShowInfoAboutOrgaUnderNode += "<br/>"
//								+ selectedSpIT
//								+ " has been matched to the following "
//								+ alOiIT.size() + " organisms :<ul>";
						for (int l = 0; l < alOiIT.size(); l++) {
							alSelectedSpeciesIds.add(alOiIT.get(l)
									.getOrganismId());
//							stToAddToShowInfoAboutOrgaUnderNode += "<li>"
//									+ alOiIT.get(l).getFullName() + "</li>";
						}
//						stToAddToShowInfoAboutOrgaUnderNode += "</ul>";
						// show list orga under node if needed
//						if (alOiIT.size() > 1) {
//							doShowListOrganismsUnderNode = true;
//						}
					} else {
						return -1;
					}
				}
//				if (doShowListOrganismsUnderNode) {
//					showListOrganismsUnderNode(stToAddToShowInfoAboutOrgaUnderNode);
//				}

				fgiIT.getListSubFilter().add(
						//alSelectedSpeciesIds.toString().replaceAll("[\\[\\]]","")
						UtilitiesMethodsShared.getItemsAsStringFromCollection(alSelectedSpeciesIds)
						);

			}
		}

		return 1;
	}

	private static String escapRawText(String rawText) {
		rawText = rawText.replace("\\", "\\" + "\\" + "\\");
		rawText = rawText.replace(".", "\\.");
		rawText = rawText.replace("(", "\\(");
		rawText = rawText.replace(")", "\\)");
		rawText = rawText.replace("?", "\\?");
		rawText = rawText.replace("{", "\\{");
		rawText = rawText.replace("}", "\\}");
		rawText = rawText.replace("*", "\\*");
		rawText = rawText.replace("+", "\\+");
		rawText = rawText.replace(",", "\\,");
		rawText = rawText.replace("^", "\\^");
		rawText = rawText.replace("$", "\\$");
		rawText = rawText.replace("=", "\\=");

		return rawText;
	}

	private static void setOperatorFilter(FilterGeneItem fgiIT,
			Widget widgListBoxOperator, String error) {

		// 0 (... AND ...)");
		// 1 "...) AND (...");
		// 2 "(... OR ...)");
		// 3 "...) OR (..."

		if (widgListBoxOperator instanceof ListBox) {
			if (((ListBox) widgListBoxOperator).getSelectedIndex() == 0) {
				fgiIT.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.COMMA_DOTS_AND_DOTS_COMMA);
			} else if (((ListBox) widgListBoxOperator).getSelectedIndex() == 1) {
				fgiIT.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.DOTS_COMMA_AND_COMMA_DOTS);
			} else if (((ListBox) widgListBoxOperator).getSelectedIndex() == 2) {
				fgiIT.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.COMMA_DOTS_OR_DOTS_COMMA);
			} else if (((ListBox) widgListBoxOperator).getSelectedIndex() == 3) {
				fgiIT.setOperatorFilterGeneBy(EnumOperatorFilterGeneBy.DOTS_COMMA_OR_COMMA_DOTS);
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in " + error
						+ " couldn't determine setOperatorFilter = "+((ListBox) widgListBoxOperator).getSelectedIndex()));
			}
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in "
					+ error
					+ " couldn't determine setOperatorFilter : not instance of ListBox"));
		}

	}

	/** get the type of the filter being parsed **/
	private static VerticalPanel setTypeFilterAndDPWidget(FilterGeneItem fgiIT,
			Widget widgListBoxTypeIT, Widget deckPanelToParseAfterward,
			String error) {

		if (widgListBoxTypeIT instanceof ListBox) {
			if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("Genomic_locations") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Genomic_locations);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("Presence_absence_homology") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Presence_absence_homology);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("Lenght_protein") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Lenght_protein);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("Gene_name") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Gene_name);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("Molecular_function") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Function);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("Biological_process") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Biological_process);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("EC_number") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.EC_number);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("Cellular_component") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Cellular_component);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("Notes_product") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Notes_product);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("db_xref") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.db_xref);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("Evidence") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Evidence);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("CountMostRepresPheno") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.CountMostRepresPheno);
			} else if (((ListBox) widgListBoxTypeIT).getSelectedValue().compareTo("PValOverReprPheno") == 0) {
				fgiIT.setTypeFilterGeneBy(EnumTypeFilterGeneBy.PValOverReprPheno);
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in " + error
						+ " couldn't determine setTypeFilterAndDPWidget"));
				return null;
			}

			Widget vpToParse = ((DeckPanel) deckPanelToParseAfterward)
					.getWidget(((ListBox) widgListBoxTypeIT)
							.getSelectedIndex());
			if (vpToParse instanceof VerticalPanel) {
				return (VerticalPanel) vpToParse;
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in " + error
						+ " couldn't determine vpToParse "+((ListBox) widgListBoxTypeIT).getValue(((ListBox) widgListBoxTypeIT).getSelectedIndex())));
				return null;
			}
			
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in "
					+ error
					+ " couldn't determine setTypeFilterAndDPWidget : not instance of ListBox"));
			return null;
		}
		
		
	}

	public static void refreshAllGeneListStep3() {

		pagerPanel.getDisplay().setVisibleRange(0, 100);
		dataProviderStep3.setList(Insyght.APP_CONTROLER
				.getAllListLightGeneItemFromSlectedElement());

	}

	public static void clearSelectionStep3(boolean doSetStep3ToDefault, boolean doClearGenesInCart) {
		//selectLightElementItem(null);
		//selectAllMoleculesFromBuildRefGeneSet(false);
		selectionModel_moleculesFromBuildRefGeneSet.clear();
		Insyght.APP_CONTROLER.getAllListLightGeneItemFromSlectedElement().clear();
		
		if (doSetStep3ToDefault) {
			doClearAllFilters(vpFilter);
		}
		if(doClearGenesInCart){
			alLGIFromBuildGeneSet.clear();
			manageUserSearchItem_ListReferenceGeneSet(true, null, true, -1);
			//refreshDisplayListSlectedLightGeneItemFromSlectedElementStep3(-1);
		}

	}

//	private static void buildUIStep4() {
//
//		HTML htmlFeat = new HTML(
//				"<ul><li><b>Select organisms that you would like to feature (they will appear on top of the result list) : (Max = "
//						+ SharedAppParams.MAX_NUMBER_FEATURED_ORGA_ALLOWED
//						+ ")</b></li></ul>");
//		vpFeatExclStep4.add(htmlFeat);
//
//		MultiValueSuggestBox mvsbFeat = new MultiValueSuggestBox(oracle,
//				"yellow", "80%",
//				SharedAppParams.MAX_NUMBER_FEATURED_ORGA_ALLOWED);
//		vpFeatExclStep4.add(mvsbFeat);
//		vpFeatExclStep4.setCellHorizontalAlignment(mvsbFeat,
//				HasHorizontalAlignment.ALIGN_CENTER);
//
//		HTML htmlExc = new HTML(
//				"<ul><li><b>Select organisms that you would like to exclude from the result list : (Max = "
//						+ SharedAppParams.MAX_NUMBER_EXCLUDED_ORGA_ALLOWED
//						+ ")</b></li></ul>");
//		vpFeatExclStep4.add(htmlExc);
//
//		MultiValueSuggestBox mvsbExcl = new MultiValueSuggestBox(oracle, "red",
//				"80%", SharedAppParams.MAX_NUMBER_EXCLUDED_ORGA_ALLOWED);
//		vpFeatExclStep4.add(mvsbExcl);
//		vpFeatExclStep4.setCellHorizontalAlignment(mvsbExcl,
//				HasHorizontalAlignment.ALIGN_CENTER);
//
//		vpDecoListPrivateProject.setVisible(false);
//
//	}
//
	public static void hideUserSpecifics() {
		if(Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getTaxoChildrens().get(Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getTaxoChildrens().size()-1).getName().compareTo("Private genome(s)")==0){
			//ok private genome
		
			Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getTaxoChildrens().remove(Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getTaxoChildrens().size()-1);
			
			try{
				//redraw main browser
				if(orgaBrowserPanelSelectionModel.getSelectedObject() != null){
					ArrayList<Integer>  alToSelect = orgaBrowserPanelSelectionModel.getSelectedObject().getFullPathInHierarchie();
					//System.err.println("ghfghf = "+alToSelect.toString());
					if(alToSelect.toString().startsWith("[0, "+intRowPrivate+",")){
						alToSelect = new ArrayList<Integer>();
						alToSelect.add(0);
						alToSelect.add(0);
					}
					selectATaxoItemInMainTaxoBrowser(alToSelect, false);
				}
				if(selectedRefTaxoNode != null
						&& refNodeTaxoBrowserSelectionModel != null
						&& deckPSearchRoot.getVisibleWidget() == 1){
					
					//update the special taxo browser to show color
					forceRedrawTaxoBrowserRefNode("KEEP_CURRENT_SELECTION");
					
				}
			}catch (Exception e){
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			}
			
		}
//		else {
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in hideUserSpecifics : Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().size() = "+Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().size()));
//		}


	}

	public static void refreshUserSpecifics() {
		if (Insyght.APP_CONTROLER.getCurrentUser().getSessionId() == null) {
			hideUserSpecifics();
		} else {
			TaxoItem privateTaxoItems = new TaxoItem("Private genome(s)", -1, Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0));
			ArrayList<Integer> fullPathInHierarchieRootPrivate = new ArrayList<Integer>();
			fullPathInHierarchieRootPrivate.add(0);
			intRowPrivate = Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getTaxoChildrens().size();
			//fullPathInHierarchieRootPrivate.add(1);
			fullPathInHierarchieRootPrivate.add(intRowPrivate);
			privateTaxoItems.setFullPathInHierarchie(fullPathInHierarchieRootPrivate);
			for (int i = 0; i < Insyght.APP_CONTROLER.getCurrentUser()
					.getListGroupsSubscribed().size(); i++) {
				GroupUsersObj guo = Insyght.APP_CONTROLER.getCurrentUser().getListGroupsSubscribed().get(i);
				
				//create the new leaf to the tree
				TaxoItem privateTaxoItemsIT = new TaxoItem(guo.getFullName(), guo.getTaxonId(), privateTaxoItems);
				ArrayList<Integer> fullPathInHierarchieIT = new ArrayList<Integer>();
				fullPathInHierarchieIT.add(0);
				//fullPathInHierarchieIT.add(1);
				fullPathInHierarchieIT.add(intRowPrivate);
				fullPathInHierarchieIT.add(i);
				privateTaxoItemsIT.setFullPathInHierarchie(fullPathInHierarchieIT);
				
				//add the new leaf to the tree
				privateTaxoItems.getTaxoChildrens().add(privateTaxoItemsIT);
				
				//make bidirectional association
				guo.setAssociatedTaxoItem(privateTaxoItemsIT);
				privateTaxoItemsIT.setAssociatedOrganismItem(guo);
				
			}
			if(!Insyght.APP_CONTROLER.getCurrentUser().getListGroupsSubscribed().isEmpty()){
				privateTaxoItems.setSumOfLeafsUnderneath(Insyght.APP_CONTROLER.getCurrentUser().getListGroupsSubscribed().size());
				if(Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getTaxoChildrens().size() == intRowPrivate){
					Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getTaxoChildrens().add(privateTaxoItems);
				}else if(Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getTaxoChildrens().size() == (intRowPrivate+1)){
					Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getTaxoChildrens().set((intRowPrivate+1), privateTaxoItems);
				}else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in hideUserSpecifics : Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().size() = "+Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().size()+" while intRowPrivate = "+intRowPrivate));
				}
				
				try{
					//redraw main browser
					if(orgaBrowserPanelSelectionModel.getSelectedObject() != null){
						ArrayList<Integer>  alToSelect = orgaBrowserPanelSelectionModel.getSelectedObject().getFullPathInHierarchie();
						if(alToSelect.toString().startsWith("[0, "+intRowPrivate+",")){
							alToSelect = new ArrayList<Integer>();
							alToSelect.add(0);
							alToSelect.add(0);
						}
						selectATaxoItemInMainTaxoBrowser(alToSelect, false);
					}
					if(selectedRefTaxoNode != null
							&& refNodeTaxoBrowserSelectionModel != null){
						
						//update the special taxo browser to show color
						forceRedrawTaxoBrowserRefNode("KEEP_CURRENT_SELECTION");
						
					}
				}catch (Exception e){
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
				}
				
			}
		}
	}
	



	

}
