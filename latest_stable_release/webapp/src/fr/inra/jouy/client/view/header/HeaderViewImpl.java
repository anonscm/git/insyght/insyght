package fr.inra.jouy.client.view.header;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.ressources.MyResources;

public class HeaderViewImpl extends Composite {

	private static HeaderViewImplUiBinder uiBinder = GWT
			.create(HeaderViewImplUiBinder.class);

	interface HeaderViewImplUiBinder extends UiBinder<Widget, HeaderViewImpl> {
	}

	@UiField
	Image imgLogoMIG;
	@UiField
	Image imgLogoINRA;
	@UiField
	static DeckPanel deckP;
	@UiField
	static Anchor logoutLink;
	@UiField
	Anchor loginLink;
	
	public HeaderViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		imgLogoMIG.setResource(MyResources.INSTANCE.logoMig());
		imgLogoMIG.setTitle("http://maiage.jouy.inra.fr/?q=en");
		imgLogoINRA.setResource(MyResources.INSTANCE.logoINRAE());
		imgLogoINRA.setTitle("www.inrae.fr");
		showCorrectLoginDeck();
	}

	@UiHandler("loginLink")
	void onLoginLinkClicked(ClickEvent event) {
		
		LoginDialog dlg = new LoginDialog(false);
		dlg.show();
		dlg.center();
	}

	@UiHandler("logoutLink")
	void onLogoutLinkClicked(ClickEvent event) {
		@SuppressWarnings("unused")
		LoginDialog dlg = new LoginDialog(true);
		LoginDialog.logout(true);
	}

	@UiHandler("imgLogoMIG")
	void onImgLogoMIGClick(ClickEvent e) {
		Window.open("http://maiage.jouy.inra.fr/?q=en", "MaIAGE", null);
	}
	
	@UiHandler("imgLogoINRA")
	void onImgLogoINRAClick(ClickEvent e) {
		Window.open("https://www.inrae.fr/", "INRAE", null);
	}
	
	public static void showCorrectLoginDeck() {

		if (Insyght.APP_CONTROLER.getCurrentUser().getSessionId() == null) {
			deckP.showWidget(0);
		} else {
			logoutLink.setText("[log out "
					+ Insyght.APP_CONTROLER.getCurrentUser().getLogin() + "]");
			deckP.showWidget(1);
		}
		
	}
}
