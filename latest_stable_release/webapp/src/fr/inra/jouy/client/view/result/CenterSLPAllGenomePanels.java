package fr.inra.jouy.client.view.result;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.Iterator;
//import java.util.List;


import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
//import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.AppController;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.NavigationControler;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem.GpiSizeState;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;

public class CenterSLPAllGenomePanels extends ResizeComposite {

	private static CenterSLPAllGenomePanelsUiBinder uiBinder = GWT
			.create(CenterSLPAllGenomePanelsUiBinder.class);

	interface CenterSLPAllGenomePanelsUiBinder extends
			UiBinder<Widget, CenterSLPAllGenomePanels> {
	}

	@UiField
	public static LayoutPanel LP_CENTER_ALL_GENOME_PANEL;

	// ref genome
	@UiField
	public static GenomePanel REF_GENOME_PANEL;

	// Nav result bar
	@UiField
	static HorizontalPanel HP_NAV_RES;
	@UiField
	static Button resultNavResultPreviousButton;
	@UiField
	static HTML resultNavResultTitle;
	@UiField
	static Button resultNavResultNextButton;

	// result panel
	/*
	 * @UiField static ScrollPanel SP_VP_RESULT_GENOME;
	 */
	/*
	 * @UiField public static VerticalPanel VP_RESULT_GENOME;
	 */
	@UiField
	static GenomePanel GENOME_PANEL_RES1;
	@UiField
	static GenomePanel GENOME_PANEL_RES2;
	@UiField
	static GenomePanel GENOME_PANEL_RES3;
	@UiField
	static GenomePanel GENOME_PANEL_RES4;
	@UiField
	static GenomePanel GENOME_PANEL_RES5;
	@UiField
	static GenomePanel GENOME_PANEL_RES6;
	@UiField
	static GenomePanel GENOME_PANEL_RES7;
	@UiField
	static GenomePanel GENOME_PANEL_RES8;
	@UiField
	static GenomePanel GENOME_PANEL_RES9;
	@UiField
	static GenomePanel GENOME_PANEL_RES10;
	@UiField
	static GenomePanel GENOME_PANEL_RES11;
	@UiField
	static GenomePanel GENOME_PANEL_RES12;
	//public static int MAX_VISIBLE_RESULT_COUNT = 6;
	
	
	// other static fields

	public static int START_INDEX = -1;
	public static int CURR_COUNT_VISIBLE_RESULT_DISPLAYED = -1;
	// user specific size of list of private
	public static int SIZE_LIST_GROUP_SUBSCRIBED = 0;
	public static int CURR_HEIGHT_GENOME_PANEL_LP = -1;
	public static int CURR_WIDTH_GENOME_PANEL_LP = -1;
	public static int HEIGHT_PX_NAVBARRES = 40;//
	public static int HEIGHT_PX_SPACING = 6;
	private static ArrayList<Integer> listStartPxPos = new ArrayList<Integer>();
	private static ArrayList<Integer> listHeightPxPos = new ArrayList<Integer>();
	private static ArrayList<Boolean> listBooleanRedrawPos = new ArrayList<Boolean>();
	public static boolean RESIZE_RESULTP_WHEN_SLCTED = false;
	public static int BUILDED_SIZE;
	private static boolean BREAK_ON_SHOW_GENOME_RETURN;

	// lock
	private static boolean LOCK_SYNCHRONIZED_RESIZE_WIDTHGP = false;

	public CenterSLPAllGenomePanels() {
		initWidget(uiBinder.createAndBindUi(this));

		REF_GENOME_PANEL.setReferenceGenome(true);

		// init arrays
		//for (int i = 0; i < MAX_VISIBLE_RESULT_COUNT + 2; i++) {
		for (int i = 0; i < (Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().size()-1) + 2; i++) {
			listStartPxPos.add(-1);
			listHeightPxPos.add(-1);
			listBooleanRedrawPos.add(true);
		}

	}


	@Override
	public void onResize() {
		//GWT2.8 : seems to be called more often (when shown from tab etc...) and for minor px diff

//		GWT.log("onResize() called LOCK="+LOCK_SYNCHRONIZED_RESIZE_WIDTHGP
//				+"\n ; CURR_WIDTH_GENOME_PANEL_LP="+CURR_WIDTH_GENOME_PANEL_LP
//				+" ; calculateDisplayedWidthGenomePanel()="+calculateDisplayedWidthGenomePanel()
//				+"\n ; CURR_HEIGHT_GENOME_PANEL_LP="+CURR_HEIGHT_GENOME_PANEL_LP
//				+" ; LP_CENTER_ALL_GENOME_PANEL.getOffsetHeight()="+LP_CENTER_ALL_GENOME_PANEL.getOffsetHeight()
//				);
		

			super.onResize();
			
			
			if (!LOCK_SYNCHRONIZED_RESIZE_WIDTHGP) {
				LOCK_SYNCHRONIZED_RESIZE_WIDTHGP = true;
				Scheduler.get().scheduleDeferred(new ScheduledCommand() {
					// @Override
					public void execute() {
		
							//GWT.log("execute() MainTabPanelView.tabPanel.getSelectedIndex()="+MainTabPanelView.tabPanel.getSelectedIndex()); 
						
							if (MainTabPanelView.tabPanel.getSelectedIndex() == 2
									|| MainTabPanelView.tabPanel.getSelectedIndex() == 4) {
								getCurrPxSizeAndUpdateDisplayAccordingly();
							} else {
								if (
										significantDiffResize(CURR_WIDTH_GENOME_PANEL_LP, calculateDisplayedWidthGenomePanel())
										|| significantDiffResize(CURR_HEIGHT_GENOME_PANEL_LP, LP_CENTER_ALL_GENOME_PANEL.getOffsetHeight())
										) {
									RESIZE_RESULTP_WHEN_SLCTED = true;
								}
							}
							LOCK_SYNCHRONIZED_RESIZE_WIDTHGP = false;
					}
				});
			}
			
		
	}

	
	public static int calculateDisplayedWidthGenomePanel() {
		return (HP_NAV_RES.getOffsetWidth() - 3);
	}

	public static void getCurrPxSizeAndUpdateDisplayAccordingly() {
		
		// clear user selection
		Insyght.APP_CONTROLER.clearAllUserSelection();

		boolean doUpdateResultsDisplay = false;
		// boolean widthHasChanged = false;
		// boolean heightHasChanged = false;

		// deal with width change
		
		if (
				significantDiffResize(CURR_WIDTH_GENOME_PANEL_LP, calculateDisplayedWidthGenomePanel())
				//CURR_WIDTH_GENOME_PANEL_LP != calculateDisplayedWidthGenomePanel()
				) {
			//GWT.log("resize width CURR_WIDTH_GENOME_PANEL_LP : "+CURR_WIDTH_GENOME_PANEL_LP+" ; calculateDisplayedWidthGenomePanel : "+calculateDisplayedWidthGenomePanel());
			CURR_WIDTH_GENOME_PANEL_LP = calculateDisplayedWidthGenomePanel();
			SyntenyCanvas.CALCULATE_CANVAS_PARAMETERS = true;
			for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
					.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
				GenomePanel gpToRTesize = i.next();
				// gpToRTesize.getSyntenyCanvas().clearWholeCanvas();
				gpToRTesize.getSyntenyCanvas().setCoordinateSpace = true;
				// gpToRTesize.getSyntenyCanvas().setWidth(CURR_WIDTH_GENOME_PANEL_LP+"px");
				doUpdateResultsDisplay = true;
				// widthHasChanged = true;
			}
		}

		// deal with height change
		
		if (
				significantDiffResize(CURR_HEIGHT_GENOME_PANEL_LP, LP_CENTER_ALL_GENOME_PANEL.getOffsetHeight())
				) {
			
//				GWT.log("resize height CURR_HEIGHT_GENOME_PANEL_LP = "+
//				  CURR_HEIGHT_GENOME_PANEL_LP
//				  +" ; LP_CENTER_ALL_GENOME_PANEL : "+LP_CENTER_ALL_GENOME_PANEL
//				  .getOffsetHeight());
			 
			CURR_HEIGHT_GENOME_PANEL_LP = LP_CENTER_ALL_GENOME_PANEL
					.getOffsetHeight();
			if (Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getLstOrgaResult() != null) {
				if (Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getLstOrgaResult().size() > 0) {
					GenoOrgaAndHomoTablViewImpl
							.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
					doUpdateResultsDisplay = true;
				}
			}
		}

		if (doUpdateResultsDisplay) {
			updateResultsDisplay(false, true, true);
		}

		RESIZE_RESULTP_WHEN_SLCTED = false;
	}

	private static boolean significantDiffResize(int size1px,
			int size2px) {
		int diffPxSize = size1px - size2px;
		if(diffPxSize > 7 || diffPxSize < -7 ){
			//GWT.log("significantDiffResize true = "+diffPxSize);
			return true;
		} else {
			//GWT.log("significantDiffResize false = "+diffPxSize);
			return false;
		}
	}


	@UiHandler("resultNavResultPreviousButton")
	void onResultNavResultPreviousButtonClick(ClickEvent e) {

		if (AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE > 0) {
			//Window.alert("waiting for data to come back from server, please wait");
		} else {

			// Window.alert("Hello resultNavResultPreviousButton!");
			// clean up old CSS styling for displayed page.
			GenoOrgaAndHomoTablViewImpl.stackPWest.showWidget(1);
			GenoOrgaAndHomoTablViewImpl
					.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();

			// Move back a page.

			int intToGoBack = 0;
			int buildedSize =
			// HEIGHT_PX_NAVBAR +
			REF_GENOME_PANEL.getScLowerSize() + GenomePanel.HEIGHT_PX_TITLEBAR
					+ HEIGHT_PX_SPACING + HEIGHT_PX_NAVBARRES;
			for (int j = START_INDEX - 1; j >= 0; j--) {

				if (j < CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED) {
					if (Insyght.APP_CONTROLER
							.getCurrentListPublicResultGenomePanelItem().get(j) == null) {
						buildedSize += GenomePanel.MEDIUM_PX_scLowerSize
								+ GenomePanel.HEIGHT_PX_TITLEBAR
								+ HEIGHT_PX_SPACING;
					} else {
						buildedSize += convertSizeStateToPx(Insyght.APP_CONTROLER
								.getCurrentListPublicResultGenomePanelItem()
								.get(j).getSizeState())
								+ HEIGHT_PX_SPACING;
						;
					}
				} else if (j < CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
						+ Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getListUserSelectedOrgaToBeResults().size()) {
					if (Insyght.APP_CONTROLER
							.getCurrentListPublicResultGenomePanelItem()
							.get(j
									- CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED) == null) {
						buildedSize += GenomePanel.MEDIUM_PX_scLowerSize
								+ GenomePanel.HEIGHT_PX_TITLEBAR
								+ HEIGHT_PX_SPACING;
					} else {
						buildedSize += convertSizeStateToPx(Insyght.APP_CONTROLER
								.getCurrentListPublicResultGenomePanelItem()
								.get(j
										- CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED)
								.getSizeState())
								+ HEIGHT_PX_SPACING;
						;
					}
				} else if (j < CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
						+ Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getListUserSelectedOrgaToBeResults().size()
						+ Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getLstOrgaResult().size()) {
					if (Insyght.APP_CONTROLER
							.getCurrentListPublicResultGenomePanelItem()
							.get(j
									- CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
									- Insyght.APP_CONTROLER
											.getCurrentRefGenomePanelAndListOrgaResult()
											.getListUserSelectedOrgaToBeResults()
											.size()) == null) {
						buildedSize += GenomePanel.MEDIUM_PX_scLowerSize
								+ GenomePanel.HEIGHT_PX_TITLEBAR
								+ HEIGHT_PX_SPACING;
					} else {
						buildedSize += convertSizeStateToPx(Insyght.APP_CONTROLER
								.getCurrentListPublicResultGenomePanelItem()
								.get(j
										- CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
										- Insyght.APP_CONTROLER
												.getCurrentRefGenomePanelAndListOrgaResult()
												.getListUserSelectedOrgaToBeResults()
												.size()).getSizeState())
								+ HEIGHT_PX_SPACING;
						;
					}
				} else {
					// Don't read past the end.
				}

				// System.out.println("j : "+j+" ; buildedSize : "+buildedSize+" ; CURR_HEIGHT_GENOME_PANEL_LP : "+CURR_HEIGHT_GENOME_PANEL_LP);
				if (buildedSize < CURR_HEIGHT_GENOME_PANEL_LP) {
					intToGoBack++;
				} else {
					break;
				}
			}

			// System.out.println("START_INDEX : "+START_INDEX+" ; intToGoBack : "+intToGoBack);

			START_INDEX -= intToGoBack;
			if (START_INDEX < 0) {
				START_INDEX = 0;
				updateResultsDisplay(true, true, false);
			} else {
				// styleRow(selectedRow, false);
				// selectedRow = -1;
				updateResultsDisplay(true, true, false);
			}
		}

	}

	@UiHandler("resultNavResultNextButton")
	void onResultNavResultNextButtonClick(ClickEvent e) {
		if (AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE > 0) {
			//Window.alert("waiting for data to come back from server, please wait");
		} else {
			// Window.alert("Hello resultNavResultNextButton!");
			// clean up old CSS styling for displayed page.
			GenoOrgaAndHomoTablViewImpl.stackPWest.showWidget(1);
			GenoOrgaAndHomoTablViewImpl
					.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();

			// Move forward a page.
			START_INDEX += CURR_COUNT_VISIBLE_RESULT_DISPLAYED;
			// if (START_INDEX >=
			// Insyght.APP_CONTROLER.getCurrentListResultGenomePanelItem().size())
			// {
			if (START_INDEX >= CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
					+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getListUserSelectedOrgaToBeResults().size()
					+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getLstOrgaResult().size()) {
				START_INDEX -= CURR_COUNT_VISIBLE_RESULT_DISPLAYED;
			} else {
				// styleRow(selectedRow, false);
				// selectedRow = -1;

				updateResultsDisplay(true, true, false);

			}
		}

	}

	/*
	 * private void addAResultGenomePanel() {
	 * 
	 * GenomePanel nonReferenceGenomePanel = new GenomePanel();
	 * nonReferenceGenomePanel.setReferenceGenome(false);
	 * VP_RESULT_GENOME.add(nonReferenceGenomePanel);
	 * 
	 * // VP_RES.setCellHorizontalAlignment(nonReferenceGenomePanel, //
	 * HorizontalPanel.ALIGN_LEFT);
	 * 
	 * }
	 * 
	 * private void addReferenceGenomePanel() {
	 * 
	 * REF_GENOME_PANEL.setReferenceGenome(true); //
	 * AppController.REF_GENOME_PANEL = new GenomePanel(true); //
	 * VP_REF_GENOME.add(AppController.REF_GENOME_PANEL);
	 * 
	 * }
	 */

	private static void showPrivateGenomes(GenomePanel genomePanelToDealWith,
			int sendIndexGPIArray, int sendIndexUIEltIteration,
			boolean forceRedraw) {

		if (Insyght.APP_CONTROLER.getCurrentListPrivateResultGenomePanelItem()
				.get(sendIndexGPIArray)
				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0) {
			forceRedraw = true;
		}

		if (Insyght.APP_CONTROLER.getCurrentListPrivateResultGenomePanelItem()
				.get(sendIndexGPIArray) == null) {
			// gpi not loaded yet

			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in showPrivateGenomes : Insyght.APP_CONTROLER.getCurrentListPrivateResultGenomePanelItem()get(sendIndexGPIArray) is null"));

			// if (listStartPxPos.get(sendIndexUIEltIteration + 2) ==
			// BUILDED_SIZE
			// && listHeightPxPos.get(sendIndexUIEltIteration + 2) ==
			// GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR) {
			// // do not redraw
			// listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, "F");
			// } else {
			// listStartPxPos.set(sendIndexUIEltIteration + 2, BUILDED_SIZE);
			// listHeightPxPos.set(sendIndexUIEltIteration + 2,
			// GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR);
			// listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, "T");
			// }
			//
			// BUILDED_SIZE += GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR
			// + HEIGHT_PX_SPACING;
			//
			// //
			// System.out.println("buildedSize : "+buildedSize+" ; CURR_SIZE_GENOME_PANEL_LP : "+CURR_SIZE_GENOME_PANEL_LP);//test
			//
			// if (BUILDED_SIZE < CURR_HEIGHT_GENOME_PANEL_LP) {
			// // System.out.println("loading new gpi");
			// CURR_COUNT_VISIBLE_RESULT_DISPLAYED = sendIndexUIEltIteration +
			// 1;
			// if (!genomePanelToDealWith.isVisible()) {
			// genomePanelToDealWith.setVisible(true);
			// }
			// //
			// // genomePanelToDealWith.setLoadingState(true);
			// //
			// // @SuppressWarnings("unused")
			// // GetResultDialog lDiag = new
			// GetResultDialog(GetResultDialog.EnumGetResultListCategory.GET_PRIVATE_GENOME_PANEL_ITEM,
			// null,
			// // sendIndexUIEltIteration, sendIndexGPIArray, true);
			//
			// } else {
			// // hide this gp but still in good position so that it
			// // does not bother others
			//
			// genomePanelToDealWith.setVisible(false);
			// sendIndexUIEltIteration++;
			//
			// for (; sendIndexUIEltIteration < MAX_VISIBLE_RESULT_COUNT;
			// ++sendIndexUIEltIteration) {
			// GenomePanel genomePanelToHide = (GenomePanel)
			// LP_CENTER_ALL_GENOME_PANEL
			// .getWidget(sendIndexUIEltIteration + 3);
			//
			// if (listStartPxPos.get(sendIndexUIEltIteration + 2) ==
			// BUILDED_SIZE
			// && listHeightPxPos.get(sendIndexUIEltIteration + 2) ==
			// GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR) {
			// // do not redraw
			// listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, "F");
			// } else {
			// // System.out.println("redraw "+i+" bc "+listStartPxPos.get(i
			// // + 2)+" different from "+buildedSize+" and ");
			// listStartPxPos.set(sendIndexUIEltIteration + 2, BUILDED_SIZE);
			// listHeightPxPos
			// .set(
			// sendIndexUIEltIteration + 2,
			// GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR);
			// listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, "T");
			// }
			//
			// BUILDED_SIZE += GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR
			// + HEIGHT_PX_SPACING;
			// genomePanelToHide.setVisible(false);
			// }
			// BREAK_ON_SHOW_GENOME_RETURN = true;
			// }

		} else {
			// gpi already loaded
			if (listStartPxPos.get(sendIndexUIEltIteration + 2) == BUILDED_SIZE
					&& listHeightPxPos.get(sendIndexUIEltIteration + 2) == convertSizeStateToPx(Insyght.APP_CONTROLER
							.getCurrentListPrivateResultGenomePanelItem()
							.get(sendIndexGPIArray).getSizeState())) {
				// do not redraw
				listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, false);
			} else {
				listStartPxPos.set(sendIndexUIEltIteration + 2, BUILDED_SIZE);
				listHeightPxPos.set(sendIndexUIEltIteration + 2,
						convertSizeStateToPx(Insyght.APP_CONTROLER
								.getCurrentListPrivateResultGenomePanelItem()
								.get(sendIndexGPIArray).getSizeState()));
				listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, true);
			}

			BUILDED_SIZE += convertSizeStateToPx(Insyght.APP_CONTROLER
					.getCurrentListPrivateResultGenomePanelItem()
					.get(sendIndexGPIArray).getSizeState())
					+ HEIGHT_PX_SPACING;

			// System.out.println("buildedSize : "+buildedSize+" ; CURR_SIZE_GENOME_PANEL_LP : "+CURR_SIZE_GENOME_PANEL_LP);//test

			if (BUILDED_SIZE < CURR_HEIGHT_GENOME_PANEL_LP) {
				CURR_COUNT_VISIBLE_RESULT_DISPLAYED = sendIndexUIEltIteration + 1;
				if (!genomePanelToDealWith.isVisible()) {
					genomePanelToDealWith.setVisible(true);
				}

				if (genomePanelToDealWith.getGenomePanelItem() == null) {
					// System.out.println("loading gpi from array");
					genomePanelToDealWith
							.setAssociatedGenomePanelItem(Insyght.APP_CONTROLER
									.getCurrentListPrivateResultGenomePanelItem()
									.get(sendIndexGPIArray));
					// genomePanelToDealWith.getScGrid().scrollToTop();
					// //.setScrollPosition(0);
				} else {
					if (genomePanelToDealWith.getGenomePanelItem() != Insyght.APP_CONTROLER
							.getCurrentListPrivateResultGenomePanelItem().get(
									sendIndexGPIArray)
							|| forceRedraw) {
						// load new gpi
						// System.out.println("loading gpi from array");
						genomePanelToDealWith
								.setAssociatedGenomePanelItem(Insyght.APP_CONTROLER
										.getCurrentListPrivateResultGenomePanelItem()
										.get(sendIndexGPIArray));
						// genomePanelToDealWith.getScGrid().scrollToTop();
						// //.setScrollPosition(0);
					}// else {System.out.println("same gpi, do not load"); }
				}

			} else {

				// hide this gp but still in good position so that it
				// does not bother others

				genomePanelToDealWith.setVisible(false);
				sendIndexUIEltIteration++;

				for (; sendIndexUIEltIteration < (Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().size()-1); ++sendIndexUIEltIteration) {
					GenomePanel genomePanelToHide = (GenomePanel) LP_CENTER_ALL_GENOME_PANEL
							.getWidget(sendIndexUIEltIteration + 3);

					if (listStartPxPos.get(sendIndexUIEltIteration + 2) == BUILDED_SIZE
							&& listHeightPxPos.get(sendIndexUIEltIteration + 2) == GenomePanel.MEDIUM_PX_scLowerSize
									+ GenomePanel.HEIGHT_PX_TITLEBAR) {
						// do not redraw
						listBooleanRedrawPos.set(sendIndexUIEltIteration + 2,
								false);
					} else {
						listStartPxPos.set(sendIndexUIEltIteration + 2,
								BUILDED_SIZE);
						listHeightPxPos.set(sendIndexUIEltIteration + 2,
								GenomePanel.MEDIUM_PX_scLowerSize
										+ GenomePanel.HEIGHT_PX_TITLEBAR);
						listBooleanRedrawPos.set(sendIndexUIEltIteration + 2,
								true);
					}

					BUILDED_SIZE += GenomePanel.MEDIUM_PX_scLowerSize
							+ GenomePanel.HEIGHT_PX_TITLEBAR
							+ HEIGHT_PX_SPACING;
					genomePanelToHide.setVisible(false);
				}
				BREAK_ON_SHOW_GENOME_RETURN = true;
			}

		}

	}

	private static void showFeaturedGenomes(GenomePanel genomePanelToDealWith,
			int sendIndexGPIArray, int sendIndexUIEltIteration,
			boolean forceRedraw) {

		if (Insyght.APP_CONTROLER.getCurrentListFeaturedResultGenomePanelItem()
				.get(sendIndexGPIArray)
				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0) {
			forceRedraw = true;
		}

		if (Insyght.APP_CONTROLER.getCurrentListFeaturedResultGenomePanelItem()
				.get(sendIndexGPIArray) == null) {
			// gpi not loaded yet

			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in showFeaturedGenomes : Insyght.APP_CONTROLER.getCurrentListFeaturedResultGenomePanelItem()get(sendIndexGPIArray) is null"));

			// if (listStartPxPos.get(sendIndexUIEltIteration + 2) ==
			// BUILDED_SIZE
			// && listHeightPxPos.get(sendIndexUIEltIteration + 2) ==
			// GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR) {
			// // do not redraw
			// listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, "F");
			// } else {
			// listStartPxPos.set(sendIndexUIEltIteration + 2, BUILDED_SIZE);
			// listHeightPxPos.set(sendIndexUIEltIteration + 2,
			// GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR);
			// listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, "T");
			// }
			//
			// BUILDED_SIZE += GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR
			// + HEIGHT_PX_SPACING;
			//
			// //
			// System.out.println("buildedSize : "+buildedSize+" ; CURR_SIZE_GENOME_PANEL_LP : "+CURR_SIZE_GENOME_PANEL_LP);//test
			//
			// if (BUILDED_SIZE < CURR_HEIGHT_GENOME_PANEL_LP) {
			// // System.out.println("loading new gpi");
			// CURR_COUNT_VISIBLE_RESULT_DISPLAYED = sendIndexUIEltIteration +
			// 1;
			// if (!genomePanelToDealWith.isVisible()) {
			// genomePanelToDealWith.setVisible(true);
			// }
			// // genomePanelToDealWith.setLoadingState(true);
			// // @SuppressWarnings("unused")
			// // GetResultDialog lDiag = new
			// GetResultDialog(GetResultDialog.EnumGetResultListCategory.GET_FEATURED_GENOME_PANEL_ITEM,
			// null,
			// // sendIndexUIEltIteration, sendIndexGPIArray, true);
			//
			// } else {
			// // hide this gp but still in good position so that it
			// // does not bother others
			//
			// genomePanelToDealWith.setVisible(false);
			// sendIndexUIEltIteration++;
			//
			// for (; sendIndexUIEltIteration < MAX_VISIBLE_RESULT_COUNT;
			// ++sendIndexUIEltIteration) {
			// GenomePanel genomePanelToHide = (GenomePanel)
			// LP_CENTER_ALL_GENOME_PANEL
			// .getWidget(sendIndexUIEltIteration + 3);
			//
			// if (listStartPxPos.get(sendIndexUIEltIteration + 2) ==
			// BUILDED_SIZE
			// && listHeightPxPos.get(sendIndexUIEltIteration + 2) ==
			// GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR) {
			// // do not redraw
			// listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, "F");
			// } else {
			// // System.out.println("redraw "+i+" bc "+listStartPxPos.get(i
			// // + 2)+" different from "+buildedSize+" and ");
			// listStartPxPos.set(sendIndexUIEltIteration + 2, BUILDED_SIZE);
			// listHeightPxPos
			// .set(
			// sendIndexUIEltIteration + 2,
			// GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR);
			// listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, "T");
			// }
			//
			// BUILDED_SIZE += GenomePanel.MEDIUM_PX_scLowerSize
			// + GenomePanel.HEIGHT_PX_TITLEBAR
			// + HEIGHT_PX_SPACING;
			// genomePanelToHide.setVisible(false);
			// }
			// BREAK_ON_SHOW_GENOME_RETURN = true;
			// }

		} else {
			// gpi already loaded
			if (listStartPxPos.get(sendIndexUIEltIteration + 2) == BUILDED_SIZE
					&& listHeightPxPos.get(sendIndexUIEltIteration + 2) == convertSizeStateToPx(Insyght.APP_CONTROLER
							.getCurrentListFeaturedResultGenomePanelItem()
							.get(sendIndexGPIArray).getSizeState())) {
				// do not redraw
				listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, false);
			} else {
				listStartPxPos.set(sendIndexUIEltIteration + 2, BUILDED_SIZE);
				listHeightPxPos.set(sendIndexUIEltIteration + 2,
						convertSizeStateToPx(Insyght.APP_CONTROLER
								.getCurrentListFeaturedResultGenomePanelItem()
								.get(sendIndexGPIArray).getSizeState()));
				listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, true);
			}

			BUILDED_SIZE += convertSizeStateToPx(Insyght.APP_CONTROLER
					.getCurrentListFeaturedResultGenomePanelItem()
					.get(sendIndexGPIArray).getSizeState())
					+ HEIGHT_PX_SPACING;

			// System.out.println("buildedSize : "+buildedSize+" ; CURR_SIZE_GENOME_PANEL_LP : "+CURR_SIZE_GENOME_PANEL_LP);//test

			if (BUILDED_SIZE < CURR_HEIGHT_GENOME_PANEL_LP) {
				CURR_COUNT_VISIBLE_RESULT_DISPLAYED = sendIndexUIEltIteration + 1;
				if (!genomePanelToDealWith.isVisible()) {
					genomePanelToDealWith.setVisible(true);
				}

				if (genomePanelToDealWith.getGenomePanelItem() == null) {
					// System.out.println("loading gpi from array");
					genomePanelToDealWith
							.setAssociatedGenomePanelItem(Insyght.APP_CONTROLER
									.getCurrentListFeaturedResultGenomePanelItem()
									.get(sendIndexGPIArray));

					// genomePanelToDealWith.getScGrid().scrollToTop();
					// //.setScrollPosition(0);
				} else {
					if (genomePanelToDealWith.getGenomePanelItem() != Insyght.APP_CONTROLER
							.getCurrentListFeaturedResultGenomePanelItem().get(
									sendIndexGPIArray)
							|| forceRedraw) {
						// load new gpi
						// System.out.println("loading gpi from array");
						genomePanelToDealWith
								.setAssociatedGenomePanelItem(Insyght.APP_CONTROLER
										.getCurrentListFeaturedResultGenomePanelItem()
										.get(sendIndexGPIArray));
						// genomePanelToDealWith.getScGrid().scrollToTop();
						// //.setScrollPosition(0);

					}// else {System.out.println("same gpi, do not load"); }
				}

			} else {

				// hide this gp but still in good position so that it
				// does not bother others

				genomePanelToDealWith.setVisible(false);
				sendIndexUIEltIteration++;

				for (; sendIndexUIEltIteration < (Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().size()-1); ++sendIndexUIEltIteration) {
					GenomePanel genomePanelToHide = (GenomePanel) LP_CENTER_ALL_GENOME_PANEL
							.getWidget(sendIndexUIEltIteration + 3);

					if (listStartPxPos.get(sendIndexUIEltIteration + 2) == BUILDED_SIZE
							&& listHeightPxPos.get(sendIndexUIEltIteration + 2) == GenomePanel.MEDIUM_PX_scLowerSize
									+ GenomePanel.HEIGHT_PX_TITLEBAR) {
						// do not redraw
						listBooleanRedrawPos.set(sendIndexUIEltIteration + 2,
								false);
					} else {
						listStartPxPos.set(sendIndexUIEltIteration + 2,
								BUILDED_SIZE);
						listHeightPxPos.set(sendIndexUIEltIteration + 2,
								GenomePanel.MEDIUM_PX_scLowerSize
										+ GenomePanel.HEIGHT_PX_TITLEBAR);
						listBooleanRedrawPos.set(sendIndexUIEltIteration + 2,
								true);
					}

					BUILDED_SIZE += GenomePanel.MEDIUM_PX_scLowerSize
							+ GenomePanel.HEIGHT_PX_TITLEBAR
							+ HEIGHT_PX_SPACING;
					genomePanelToHide.setVisible(false);
				}
				BREAK_ON_SHOW_GENOME_RETURN = true;
			}

		}
	}

	private static void showPublicGenomes(GenomePanel genomePanelToDealWith,
			int sendIndexGPIArray, int sendIndexUIEltIteration,
			boolean forceRedraw) {

		//System.out.println("here : "+Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem().get(sendIndexGPIArray).getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed());

		if (Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem()
				.get(sendIndexGPIArray)
				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0) {
			forceRedraw = true;
		}

		if (Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem()
				.get(sendIndexGPIArray) == null) {
			// gpi not loaded yet
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in showPublicGenomes : Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem()get(sendIndexGPIArray) is null"));

		} else {
			// gpi already loaded
			if (listStartPxPos.get(sendIndexUIEltIteration + 2) == BUILDED_SIZE
					&& listHeightPxPos.get(sendIndexUIEltIteration + 2) == convertSizeStateToPx(Insyght.APP_CONTROLER
							.getCurrentListPublicResultGenomePanelItem()
							.get(sendIndexGPIArray).getSizeState())) {
				// do not redraw
				listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, false);
			} else {
				listStartPxPos.set(sendIndexUIEltIteration + 2, BUILDED_SIZE);
				listHeightPxPos.set(sendIndexUIEltIteration + 2,
						convertSizeStateToPx(Insyght.APP_CONTROLER
								.getCurrentListPublicResultGenomePanelItem()
								.get(sendIndexGPIArray).getSizeState()));
				listBooleanRedrawPos.set(sendIndexUIEltIteration + 2, true);
			}

			BUILDED_SIZE += convertSizeStateToPx(Insyght.APP_CONTROLER
					.getCurrentListPublicResultGenomePanelItem()
					.get(sendIndexGPIArray).getSizeState())
					+ HEIGHT_PX_SPACING;

			// System.out.println("buildedSize : "+buildedSize+" ; CURR_SIZE_GENOME_PANEL_LP : "+CURR_SIZE_GENOME_PANEL_LP);//test

			if (BUILDED_SIZE < CURR_HEIGHT_GENOME_PANEL_LP) {
				CURR_COUNT_VISIBLE_RESULT_DISPLAYED = sendIndexUIEltIteration + 1;
				if (!genomePanelToDealWith.isVisible()) {
					genomePanelToDealWith.setVisible(true);
				}

				if (genomePanelToDealWith.getGenomePanelItem() == null) {
					// System.out.println("loading gpi from array");
					genomePanelToDealWith
							.setAssociatedGenomePanelItem(Insyght.APP_CONTROLER
									.getCurrentListPublicResultGenomePanelItem()
									.get(sendIndexGPIArray));
					// genomePanelToDealWith.getScGrid().scrollToTop();
					// //.setScrollPosition(0);

				} else {
					if (genomePanelToDealWith.getGenomePanelItem() != Insyght.APP_CONTROLER
							.getCurrentListPublicResultGenomePanelItem().get(
									sendIndexGPIArray)
							|| forceRedraw) {
						// load new gpi

						// System.out.println("setAssociatedGenomePanelItem for "+sendIndexGPIArray);
						genomePanelToDealWith
								.setAssociatedGenomePanelItem(Insyght.APP_CONTROLER
										.getCurrentListPublicResultGenomePanelItem()
										.get(sendIndexGPIArray));
						// genomePanelToDealWith.getScGrid().scrollToTop();
						// //.setScrollPosition(0);

					}// else {System.out.println("same gpi, do not load"); }
				}

			} else {

				// hide this gp but still in good position so that it
				// does not bother others

				genomePanelToDealWith.setVisible(false);
				sendIndexUIEltIteration++;

				for (; sendIndexUIEltIteration < (Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().size()-1); ++sendIndexUIEltIteration) {
					GenomePanel genomePanelToHide = (GenomePanel) LP_CENTER_ALL_GENOME_PANEL
							.getWidget(sendIndexUIEltIteration + 3);

					if (listStartPxPos.get(sendIndexUIEltIteration + 2) == BUILDED_SIZE
							&& listHeightPxPos.get(sendIndexUIEltIteration + 2) == GenomePanel.MEDIUM_PX_scLowerSize
									+ GenomePanel.HEIGHT_PX_TITLEBAR) {
						// do not redraw
						listBooleanRedrawPos.set(sendIndexUIEltIteration + 2,
								false);
					} else {
						listStartPxPos.set(sendIndexUIEltIteration + 2,
								BUILDED_SIZE);
						listHeightPxPos.set(sendIndexUIEltIteration + 2,
								GenomePanel.MEDIUM_PX_scLowerSize
										+ GenomePanel.HEIGHT_PX_TITLEBAR);
						listBooleanRedrawPos.set(sendIndexUIEltIteration + 2,
								true);
					}

					BUILDED_SIZE += GenomePanel.MEDIUM_PX_scLowerSize
							+ GenomePanel.HEIGHT_PX_TITLEBAR
							+ HEIGHT_PX_SPACING;
					genomePanelToHide.setVisible(false);
				}
				BREAK_ON_SHOW_GENOME_RETURN = true;
			}

		}

	}

	public static void updateResultsDisplay(
			boolean scrollQuickNavToEnsureVisible,
			boolean clearCurrentUserSelection, boolean forceRedraw) {

		//GWT2.8 change : onResize called much more often
		
//		GWT.log("updateResultsDisplay"
//				+ " scrollQuickNavToEnsureVisible="+scrollQuickNavToEnsureVisible
//				+ " clearCurrentUserSelection="+clearCurrentUserSelection
//				+ " forceRedraw="+forceRedraw
//				);
		
		
		if (START_INDEX < 0) {
			return;
		}


		//System.out.println("START_INDEX updateResultsDisplay : "+START_INDEX);

		// clear the current gene selection
		if (clearCurrentUserSelection) {
			// GeneWidget.clearCurrentGeneWidgetSelection();
			Insyght.APP_CONTROLER.clearAllUserSelection();
		}

		// int buildedSize = listHeightPxPos.get(0);
		BUILDED_SIZE = listHeightPxPos.get(0);

		if (BUILDED_SIZE ==
		// HEIGHT_PX_NAVBAR +
		REF_GENOME_PANEL.getScLowerSize() + GenomePanel.HEIGHT_PX_TITLEBAR
				+ HEIGHT_PX_SPACING + HEIGHT_PX_NAVBARRES) {
			listBooleanRedrawPos.set(0, false);
		} else {
			listHeightPxPos.set(0,
			// HEIGHT_PX_NAVBAR +
					REF_GENOME_PANEL.getScLowerSize()
							+ GenomePanel.HEIGHT_PX_TITLEBAR
							+ HEIGHT_PX_SPACING + HEIGHT_PX_NAVBARRES);
			listBooleanRedrawPos.set(0, true);
			BUILDED_SIZE =
			// HEIGHT_PX_NAVBAR +
			REF_GENOME_PANEL.getScLowerSize() + GenomePanel.HEIGHT_PX_TITLEBAR
					+ HEIGHT_PX_SPACING + HEIGHT_PX_NAVBARRES;
		}
		// System.out.println("REF_GENOME_PANEL.getScLowerSize() : "+REF_GENOME_PANEL.getScLowerSize());
		BREAK_ON_SHOW_GENOME_RETURN = false;
		int i = 0;
		for (; i < (Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().size()-1); ++i) {

			GenomePanel genomePanelToDealWith = (GenomePanel) LP_CENTER_ALL_GENOME_PANEL
					.getWidget(i + 
							3
							);

			if (CenterSLPAllGenomePanels.START_INDEX + i < CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED) {
				// System.out.println("showPrivateGenomes");
				int idxIT = START_INDEX + i;
				
				showPrivateGenomes(genomePanelToDealWith, idxIT, i,
						forceRedraw);
				if (BREAK_ON_SHOW_GENOME_RETURN) {
					break;
				}
				// highlight correct page in QuickNav
				GenoOrgaAndHomoTablViewImpl.VP_QUICKNAV_PRIVATE.getWidget(
						idxIT).addStyleDependentName(
						"LabelInQuickNav-OnDisplayedPage");
				
				if (scrollQuickNavToEnsureVisible) {
					GenoOrgaAndHomoTablViewImpl.SP_VP_QUICKNAV
							.ensureVisible(GenoOrgaAndHomoTablViewImpl.VP_QUICKNAV_PRIVATE
									.getWidget(idxIT));
					scrollQuickNavToEnsureVisible  = false;
				}
				
			} else if (CenterSLPAllGenomePanels.START_INDEX + i < CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
					+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getListUserSelectedOrgaToBeResults().size()) {
				// System.out.println("showFeaturedGenomes");
				int idxIT = START_INDEX
						+ i
						- CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
						;
				
				showFeaturedGenomes(
						genomePanelToDealWith
						, idxIT
						, i
						, forceRedraw);
				if (BREAK_ON_SHOW_GENOME_RETURN) {
					break;
				}
				// highlight correct page in QuickNav
				GenoOrgaAndHomoTablViewImpl.VP_QUICKNAV_FEATURED
						.getWidget(
								idxIT)
						.addStyleDependentName(
								"LabelInQuickNav-OnDisplayedPage");
				

				if (scrollQuickNavToEnsureVisible) {
					GenoOrgaAndHomoTablViewImpl.SP_VP_QUICKNAV
							.ensureVisible(GenoOrgaAndHomoTablViewImpl.VP_QUICKNAV_FEATURED
									.getWidget(idxIT));
					scrollQuickNavToEnsureVisible  = false;
				}
				
			} else if (CenterSLPAllGenomePanels.START_INDEX + i < CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
					+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getListUserSelectedOrgaToBeResults().size()
					+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getLstOrgaResult().size()) {
				// System.out.println("showPublicGenomes");
				
				int idxIT = START_INDEX
						+ i
						- CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
						- Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getListUserSelectedOrgaToBeResults().size()
						;
				
				showPublicGenomes(genomePanelToDealWith
						, idxIT
						, i
						, forceRedraw);
				if (BREAK_ON_SHOW_GENOME_RETURN) {
					break;
				}
				// highlight correct page in QuickNav
				GenoOrgaAndHomoTablViewImpl.VP_QUICKNAV_PUBLIC
						.getWidget(idxIT)
						.addStyleDependentName("LabelInQuickNav-OnDisplayedPage");
				

				if (scrollQuickNavToEnsureVisible) {
					GenoOrgaAndHomoTablViewImpl.SP_VP_QUICKNAV
							.ensureVisible(GenoOrgaAndHomoTablViewImpl.VP_QUICKNAV_PUBLIC
									.getWidget(idxIT));
					scrollQuickNavToEnsureVisible  = false;
				}
				
			} else {
				// Don't read past the end.
				genomePanelToDealWith.setVisible(false);
			}

		} // for

		// set result for refGenome
		CenterSLPAllGenomePanels.REF_GENOME_PANEL
				.setAssociatedGenomePanelItem(Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getReferenceGenomePanelItem());
		// CenterSLPAllGenomePanels.REF_GENOME_PANEL.getScGrid().scrollToTop();
		// //.setScrollPosition(0);

		// Update the previous/next buttons & label.
		int count = CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
				+ Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getListUserSelectedOrgaToBeResults().size()
				+ Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getLstOrgaResult().size();
		int max = START_INDEX + CURR_COUNT_VISIBLE_RESULT_DISPLAYED;
		if (max > count) {
			max = count;
		}

		resultNavResultPreviousButton.setVisible(START_INDEX != 0);
		resultNavResultNextButton.setVisible(START_INDEX
				+ CURR_COUNT_VISIBLE_RESULT_DISPLAYED < count);
		if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
				.getViewTypeInsyght()
				.compareTo(EnumResultViewTypes.genomic_organization) == 0) {
			resultNavResultTitle
					.setHTML("<span style=\"color:#7A378B;\">Genomic organisation view : displaying results "
							+ (START_INDEX + 1)
							+ " - "
							+ max
							+ " of "
							+ count
							+ "</span>");
		} else {
			resultNavResultTitle
					.setHTML("<span style=\"color:#8B3A3A;\">Orthologs browsing table view : displaying results "
							+ (START_INDEX + 1)
							+ " - "
							+ max
							+ " of "
							+ count
							+ "</span>");
		}


		// error fault bc sometimes featured results too, do not take only VP_QUICKNAV_PUBLIC into account
//		if (scrollQuickNavToEnsureVisible) {
//			// SP_VP_RESULT_GENOME.setScrollPosition(0);
//			GenoOrgaAndHomoTablViewImpl.SP_VP_QUICKNAV
//					.ensureVisible(GenoOrgaAndHomoTablViewImpl.VP_QUICKNAV_PUBLIC
//							.getWidget(START_INDEX));
//		}
		

		//update firstRowShownId
		if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(1).getGenomePanelItem() != null){
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().setFirstRowShownId(
						Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(1).getGenomePanelItem().getOrganismId());
		}
		
		
		drawDisplay();

	}// private static void updateResultsDisplay(String pagingFrom) {

	private static int convertSizeStateToPx(GpiSizeState sizeState) {

		// can be HIDDEN, INTERMEDIATE, MAX or MIN
		/*
		 * if (sizeState.compareTo("HIDDEN") == 0) { return 0; } else
		 */
		if (sizeState.compareTo(GenomePanelItem.GpiSizeState.INTERMEDIATE) == 0) {
			return GenomePanel.MEDIUM_PX_scLowerSize
					+ GenomePanel.HEIGHT_PX_TITLEBAR;
		} else if (sizeState.compareTo(GenomePanelItem.GpiSizeState.MAX) == 0) {
			return GenomePanel.MAX_PX_scLowerSize
					+ GenomePanel.HEIGHT_PX_TITLEBAR;
		} else if (sizeState.compareTo(GenomePanelItem.GpiSizeState.MIN) == 0) {
			return GenomePanel.HEIGHT_PX_TITLEBAR;
		} else {
			return -1;
		}

	}

	private static void drawDisplay() {

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			// @Override
			public void execute() {

				if (listBooleanRedrawPos.get(0)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							REF_GENOME_PANEL,
							// HEIGHT_PX_NAVBARRES,
							3, Unit.PX, REF_GENOME_PANEL.getScLowerSize()
									+ GenomePanel.HEIGHT_PX_TITLEBAR, Unit.PX);

					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(HP_NAV_RES,
							// HEIGHT_PX_NAVBARRES
							3 + REF_GENOME_PANEL.getScLowerSize()
									+ GenomePanel.HEIGHT_PX_TITLEBAR
									+ HEIGHT_PX_SPACING, Unit.PX,
							HEIGHT_PX_NAVBARRES, Unit.PX);
				}

				if (listBooleanRedrawPos.get(2)) {
					
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES1, listStartPxPos.get(2), Unit.PX,
							listHeightPxPos.get(2), Unit.PX);
				}

				if (listBooleanRedrawPos.get(3)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES2, listStartPxPos.get(3), Unit.PX,
							listHeightPxPos.get(3), Unit.PX);
				}

				if (listBooleanRedrawPos.get(4)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES3, listStartPxPos.get(4), Unit.PX,
							listHeightPxPos.get(4), Unit.PX);
				}

				if (listBooleanRedrawPos.get(5)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES4, listStartPxPos.get(5), Unit.PX,
							listHeightPxPos.get(5), Unit.PX);
				}

				if (listBooleanRedrawPos.get(6)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES5, listStartPxPos.get(6), Unit.PX,
							listHeightPxPos.get(6), Unit.PX);
				}

				if (listBooleanRedrawPos.get(7)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES6, listStartPxPos.get(7), Unit.PX,
							listHeightPxPos.get(7), Unit.PX);
				}
				
				
				if (listBooleanRedrawPos.get(8)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES7, listStartPxPos.get(8), Unit.PX,
							listHeightPxPos.get(8), Unit.PX);
				}

				if (listBooleanRedrawPos.get(9)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES8, listStartPxPos.get(9), Unit.PX,
							listHeightPxPos.get(9), Unit.PX);
				}

				if (listBooleanRedrawPos.get(10)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES9, listStartPxPos.get(10), Unit.PX,
							listHeightPxPos.get(10), Unit.PX);
				}

				if (listBooleanRedrawPos.get(11)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES10, listStartPxPos.get(11), Unit.PX,
							listHeightPxPos.get(11), Unit.PX);
				}

				if (listBooleanRedrawPos.get(12)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES11, listStartPxPos.get(12), Unit.PX,
							listHeightPxPos.get(12), Unit.PX);
				}

				if (listBooleanRedrawPos.get(13)) {
					LP_CENTER_ALL_GENOME_PANEL.setWidgetTopHeight(
							GENOME_PANEL_RES12, listStartPxPos.get(13), Unit.PX,
							listHeightPxPos.get(13), Unit.PX);
				}
				LP_CENTER_ALL_GENOME_PANEL.animate(600);

				//TODO CustomHistory to override newItem
				NavigationControler.newItem(NavigationControler.getURLTockenFromCurrentState(), false);
//				String tockenToTest = NavigationControler.getURLTockenFromCurrentState();
//				if(tockenToTest.compareTo(History.getToken())!=0){
//					//replace if url param not canonical
//					if(NavigationControler.checkTockenIsCononical(History.getToken())){
//						History.newItem(tockenToTest, false);
//					} else {
//						History.replaceItem(tockenToTest, false);
//					}
//				}
				
			}
		});

	}

}
