package fr.inra.jouy.client.view.result;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.SessionStorageControler;
import fr.inra.jouy.client.RPC.CallForHomoBrowResu;
import fr.inra.jouy.client.RPC.CallForHomoBrowResuAsync;
import fr.inra.jouy.client.RPC.CallForInfoDB;
import fr.inra.jouy.client.RPC.CallForInfoDBAsync;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.crossSections.popUp.ManageListComparedOrganisms;
import fr.inra.jouy.client.view.crossSections.popUp.PopUpListGenesDialog;
import fr.inra.jouy.shared.RefGeneSetForAnnotCompa;
import fr.inra.jouy.shared.pojos.applicationItems.SharedAppParams;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSSyntItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem.QSEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem.QEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem.SEnumBlockType;
import fr.inra.jouy.shared.TransAPMissGeneHomoInfo;

public class ResultLoadingDialog //extends DialogBox 
	{

	//private static ResultLoadingDialogUiBinder uiBinder = GWT.create(ResultLoadingDialogUiBinder.class);

	private final CallForInfoDBAsync callForInfoDBService = (CallForInfoDBAsync) GWT
			.create(CallForInfoDB.class);

	private final CallForHomoBrowResuAsync homologieBrowsingResultService = (CallForHomoBrowResuAsync) GWT
			.create(CallForHomoBrowResu.class);

//	interface ResultLoadingDialogUiBinder extends
//			UiBinder<Widget, ResultLoadingDialog> {
//	}

//	public enum TypeOfTransfert {
//		GENE_ID_IN_GENOMIC_ORGANISATION,
//		LIST_ElementIdsThenStartPbthenStopPBLooped_TO_HOMO_BRO_VIEW,
//		GENE_SET_COMPA_ANNOT_TO_HOMOLOG_BROWSING,
//		SYNTENY_REGION_TO_HOMOLOG_BROWSING
//	}
//
//	public enum BasicInfoStack {
//		BasicQElement
//	}
//	
//	public enum DetailledInfoStack {
//		DetailledSyntenyInfo,
//		DetailledGeneInfoRef,
//		DetailledGeneInfoComp,
//		DetailledQElement, DetailledSElement,
//		GeneCountForQGenomicRegionInsertion,
//		GeneCountForQElement,
//		GeneCountForSGenomicRegionInsertion,
//		GeneCountForSElement,
//		missingInfoForGeneHomology,
//		missingInfoForQGeneInsertion,
//		missingInfoForSGeneInsertion
//	}
//	
//	public enum TypeOfExport {
//		GENE_SEQUENCES
//	}


	public ResultLoadingDialog() {
	}


//	public ResultLoadingDialog(BasicInfoStack type, final String currSelectedSyntenyCanvasItemRefId, final int origamiGeneId,
//			final EnumResultViewTypes viewTypeSent){
//		setWidget(uiBinder.createAndBindUi(this));
//		
//		if(type.compareTo(BasicInfoStack.BasicQElement)==0){
//			getBasicElementInfoWithGeneId(currSelectedSyntenyCanvasItemRefId, origamiGeneId, true, viewTypeSent);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog 1 : unrecognized type"));
//		}
//	}
	
//	public ResultLoadingDialog(DetailledInfoStack type, final String currSelectedSyntenyCanvasItemRefId, final int origamiId,
//			final EnumResultViewTypes viewTypeSent){
//		setWidget(uiBinder.createAndBindUi(this));
//		
//		if(type.compareTo(DetailledInfoStack.DetailledQElement)==0){
//			getDetailledElementInfo(currSelectedSyntenyCanvasItemRefId, origamiId, true, viewTypeSent);
//		}else if(type.compareTo(DetailledInfoStack.DetailledSElement)==0){
//			getDetailledElementInfo(currSelectedSyntenyCanvasItemRefId, origamiId, false, viewTypeSent);
//		}else if(type.compareTo(DetailledInfoStack.DetailledGeneInfoRef)==0){
//			getDetailledGeneInfo(currSelectedSyntenyCanvasItemRefId, origamiId, false, viewTypeSent, true);
//		}else if(type.compareTo(DetailledInfoStack.DetailledGeneInfoComp)==0){
//			getDetailledGeneInfo(currSelectedSyntenyCanvasItemRefId, origamiId, false, viewTypeSent, false);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog 1 : unrecognized type"));
//		}
//	}
	
//	public ResultLoadingDialog(DetailledInfoStack type, final String currSelectedSyntenyCanvasItemRefId, final long alignmentId,
//			final EnumResultViewTypes viewTypeSent){
//		setWidget(uiBinder.createAndBindUi(this));
//		
//		if(type.compareTo(DetailledInfoStack.DetailledSyntenyInfo)==0){
//			getDetailledSyntenyInfo(currSelectedSyntenyCanvasItemRefId, alignmentId, false, viewTypeSent);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog 1 : unrecognized type"));
//		}
//	}

//	public ResultLoadingDialog(
//			DetailledInfoStack type,
//			final int indexOfHAPIInFullArray,
//			final int indexOfHAPIInDisplayedArray,
//			final int indexSliceInDisplay,
//			final int indexOfGenomePanelInLIST_GENOME_PANEL,
//			final int indexStartInSyntenyCanvasNavigationDisplay,
//			final int indexStartInResultNavigationDisplay,
//			final double percentSyntenyShowStartQGenome,
//			final double percentSyntenyShowStopQGenome,
//			final double percentSyntenyShowStartSGenome,
//			final double percentSyntenyShowStopSGenome,
//			final int origamiElementIdSent,
//			final int pbStartInElement,
//			final int pbStopInElement,
//			final SEnumBlockType sEnumBlockTypeSent,
//			final boolean redrawOnTopSent,
//			final boolean inRedSent,
//			final EnumResultViewTypes viewTypeSent,
//			final boolean previousSOfNextSlice,
//			final boolean nextSOfPreviousSlice
//			) {
//		
//		if(type.compareTo(DetailledInfoStack.GeneCountForSGenomicRegionInsertion)==0){
//			getGeneCountForSGenomicRegionInsertion(indexOfHAPIInFullArray,
//					indexOfHAPIInDisplayedArray,
//					indexSliceInDisplay,
//					indexOfGenomePanelInLIST_GENOME_PANEL,
//					indexStartInSyntenyCanvasNavigationDisplay,
//					indexStartInResultNavigationDisplay,
//					percentSyntenyShowStartQGenome,
//					percentSyntenyShowStopQGenome,
//					percentSyntenyShowStartSGenome,
//					percentSyntenyShowStopSGenome,
//					origamiElementIdSent,
//					pbStartInElement,
//					pbStopInElement,
//					sEnumBlockTypeSent,
//					redrawOnTopSent,
//					inRedSent,
//					viewTypeSent,
//					previousSOfNextSlice,
//					nextSOfPreviousSlice
//					);
//		}else if(type.compareTo(DetailledInfoStack.GeneCountForSElement)==0){
//			getGeneCountForSElement(indexOfHAPIInFullArray,
//					indexOfHAPIInDisplayedArray,
//					indexSliceInDisplay,
//					indexOfGenomePanelInLIST_GENOME_PANEL,
//					indexStartInSyntenyCanvasNavigationDisplay,
//					indexStartInResultNavigationDisplay,
//					percentSyntenyShowStartQGenome,
//					percentSyntenyShowStopQGenome,
//					percentSyntenyShowStartSGenome,
//					percentSyntenyShowStopSGenome,
//					origamiElementIdSent,
//					sEnumBlockTypeSent,
//					redrawOnTopSent,
//					inRedSent,
//					viewTypeSent);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog 2 : unrecognized type"));
//		}
//		
//	}
//	
//	public ResultLoadingDialog(
//			DetailledInfoStack type,
//			final int indexOfHAPIInFullArray,
//			final int indexOfHAPIInDisplayedArray,
//			final int indexSliceInDisplay,
//			final int indexOfGenomePanelInLIST_GENOME_PANEL,
//			final int indexStartInSyntenyCanvasNavigationDisplay,
//			final int indexStartInResultNavigationDisplay,
//			final double percentSyntenyShowStartQGenome,
//			final double percentSyntenyShowStopQGenome,
//			final double percentSyntenyShowStartSGenome,
//			final double percentSyntenyShowStopSGenome,
//			final int origamiElementIdSent,
//			final int pbStartInElement,
//			final int pbStopInElement,
//			final QEnumBlockType qEnumBlockTypeSent,
//			final boolean redrawOnTopSent,
//			final boolean inRedSent,
//			final EnumResultViewTypes viewTypeSent) {
//		
//		if(type.compareTo(DetailledInfoStack.GeneCountForQGenomicRegionInsertion)==0){
//			getGeneCountForQGenomicRegionInsertion(indexOfHAPIInFullArray,
//					indexOfHAPIInDisplayedArray,
//					indexSliceInDisplay,
//					indexOfGenomePanelInLIST_GENOME_PANEL,
//					indexStartInSyntenyCanvasNavigationDisplay,
//					indexStartInResultNavigationDisplay,
//					percentSyntenyShowStartQGenome,
//					percentSyntenyShowStopQGenome,
//					percentSyntenyShowStartSGenome,
//					percentSyntenyShowStopSGenome,
//					origamiElementIdSent,
//					pbStartInElement,
//					pbStopInElement,
//					qEnumBlockTypeSent,
//					redrawOnTopSent,
//					inRedSent,
//					viewTypeSent);
//		}else if(type.compareTo(DetailledInfoStack.GeneCountForQElement)==0){
//			getGeneCountForQElement(indexOfHAPIInFullArray,
//					indexOfHAPIInDisplayedArray,
//					indexSliceInDisplay,
//					indexOfGenomePanelInLIST_GENOME_PANEL,
//					indexStartInSyntenyCanvasNavigationDisplay,
//					indexStartInResultNavigationDisplay,
//					percentSyntenyShowStartQGenome,
//					percentSyntenyShowStopQGenome,
//					percentSyntenyShowStartSGenome,
//					percentSyntenyShowStopSGenome,
//					origamiElementIdSent,
//					qEnumBlockTypeSent,
//					redrawOnTopSent,
//					inRedSent,
//					viewTypeSent);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog 3 : unrecognized type"));
//		}
//		
//	}
//
//	
//
//	public ResultLoadingDialog(
//			DetailledInfoStack type,
//			final int indexOfHAPIInFullArray,
//			final int indexOfHAPIInDisplayedArray,
//			final int indexSliceInDisplay,
//			final int indexOfGenomePanelInLIST_GENOME_PANEL,
//			final int indexStartInSyntenyCanvasNavigationDisplay,
//			final int indexStartInResultNavigationDisplay,
//			final double percentSyntenyShowStartQGenome,
//			final double percentSyntenyShowStopQGenome,
//			final double percentSyntenyShowStartSGenome,
//			final double percentSyntenyShowStopSGenome,
//			AbsoPropQSGeneHomoItem absoluteProportionQComparableQSGeneHomologyItemSent,
//			final int numberOtherMatch,
//			final boolean redrawOnTopSent,
//			final boolean inRedSent,
//			final EnumResultViewTypes viewTypeSent
//			//,final boolean geneHomologyGeneNameUpClicked, final boolean geneHomologyGeneNameDownClicked, final boolean geneHomologyMatchZoneClicked
//			) {
//		
//		if(type.compareTo(DetailledInfoStack.missingInfoForGeneHomology)==0){
//			getMissingInfoForGeneHomology(indexOfHAPIInFullArray,
//					indexOfHAPIInDisplayedArray,
//					indexSliceInDisplay,
//					indexOfGenomePanelInLIST_GENOME_PANEL,
//					indexStartInSyntenyCanvasNavigationDisplay,
//					indexStartInResultNavigationDisplay,
//					percentSyntenyShowStartQGenome,
//					percentSyntenyShowStopQGenome,
//					percentSyntenyShowStartSGenome,
//					percentSyntenyShowStopSGenome,
//					absoluteProportionQComparableQSGeneHomologyItemSent,
//					numberOtherMatch,
//					redrawOnTopSent,
//					inRedSent,
//					viewTypeSent
//					//,geneHomologyGeneNameUpClicked, geneHomologyGeneNameDownClicked, geneHomologyMatchZoneClicked
//					);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog 4 : unrecognized type"));
//		}
//		
//	}
//
//	public ResultLoadingDialog(
//			DetailledInfoStack type,
//			final int indexOfHAPIInFullArray,
//			final int indexOfHAPIInDisplayedArray,
//			final int indexSliceInDisplay,
//			final int indexOfGenomePanelInLIST_GENOME_PANEL,
//			final int indexStartInSyntenyCanvasNavigationDisplay,
//			final int indexStartInResultNavigationDisplay,
//			final double percentSyntenyShowStartQGenome,
//			final double percentSyntenyShowStopQGenome,
//			final double percentSyntenyShowStartSGenome,
//			final double percentSyntenyShowStopSGenome,
//			AbsoPropQGeneInserItem absoluteProportionQComparableQGeneInsertionItemSent,
//			final int numberOtherMatch,
//			final boolean redrawOnTopSent,
//			final boolean inRedSent,
//			final EnumResultViewTypes viewTypeSent) {
//		
//		if(type.compareTo(DetailledInfoStack.missingInfoForQGeneInsertion)==0){
//			getMissingInfoForQGeneInsertion(indexOfHAPIInFullArray,
//					indexOfHAPIInDisplayedArray,
//					indexSliceInDisplay,
//					indexOfGenomePanelInLIST_GENOME_PANEL,
//					indexStartInSyntenyCanvasNavigationDisplay,
//					indexStartInResultNavigationDisplay,
//					percentSyntenyShowStartQGenome,
//					percentSyntenyShowStopQGenome,
//					percentSyntenyShowStartSGenome,
//					percentSyntenyShowStopSGenome,
//					absoluteProportionQComparableQGeneInsertionItemSent,
//					numberOtherMatch,
//					redrawOnTopSent,
//					inRedSent,
//					viewTypeSent);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog 5 : unrecognized type"));
//		}
//		
//	}
//
//	public ResultLoadingDialog(
//			DetailledInfoStack type,
//			final int indexOfHAPIInFullArray,
//			final int indexOfHAPIInDisplayedArray,
//			final int indexSliceInDisplay,
//			final int indexOfGenomePanelInLIST_GENOME_PANEL,
//			final int indexStartInSyntenyCanvasNavigationDisplay,
//			final int indexStartInResultNavigationDisplay,
//			final double percentSyntenyShowStartQGenome,
//			final double percentSyntenyShowStopQGenome,
//			final double percentSyntenyShowStartSGenome,
//			final double percentSyntenyShowStopSGenome,
//			AbsoPropSGeneInserItem absoluteProportionSComparableSGeneInsertionItemSent,
//			final int numberOtherMatch,
//			final boolean redrawOnTopSent,
//			final boolean inRedSent,
//			final EnumResultViewTypes viewTypeSent) {
//		
//		if(type.compareTo(DetailledInfoStack.missingInfoForSGeneInsertion)==0){
//			getMissingInfoForSGeneInsertion(indexOfHAPIInFullArray,
//					indexOfHAPIInDisplayedArray,
//					indexSliceInDisplay,
//					indexOfGenomePanelInLIST_GENOME_PANEL,
//					indexStartInSyntenyCanvasNavigationDisplay,
//					indexStartInResultNavigationDisplay,
//					percentSyntenyShowStartQGenome,
//					percentSyntenyShowStopQGenome,
//					percentSyntenyShowStartSGenome,
//					percentSyntenyShowStopSGenome,
//					absoluteProportionSComparableSGeneInsertionItemSent,
//					numberOtherMatch,
//					redrawOnTopSent,
//					inRedSent,
//					viewTypeSent);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog 6 : unrecognized type"));
//		}
//		
//	}
//
//
//	public ResultLoadingDialog(TypeOfTransfert typeOfTransfertSent,
//			final int geneId,
//			final EnumResultViewTypes viewTypeSent) {
//		
//		if(typeOfTransfertSent.compareTo(TypeOfTransfert.GENE_ID_IN_GENOMIC_ORGANISATION)==0){
//			transfertGeneIdInGenomicOrgaView(geneId, viewTypeSent);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog 7 : unrecognized type"));
//		}
//	}
//
//	public ResultLoadingDialog(
//			TypeOfTransfert typeOfTransfertSent,
//			final EnumResultViewTypes viewTypeSent,
//			final ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent) {
//		setWidget(uiBinder.createAndBindUi(this));
//			if (typeOfTransfertSent.compareTo(TypeOfTransfert.LIST_ElementIdsThenStartPbthenStopPBLooped_TO_HOMO_BRO_VIEW) == 0) {
//				transfertGenesFromListElementIdsThenStartPbthenStopPBLoopedToHomologBrowsing(listElementIdsThenStartPbthenStopPBLoopedSent, 
//						viewTypeSent
//						);
//			}else{
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error ResultLoadingDialog 8 DISPLAYED_REGION_TO_HOMOLOG_BROWSING : unrecognized type of ResultLoadingMoreAbsoluteProportionItemDialog"));
//			}
//	}
//
//	
//	public ResultLoadingDialog(
//			TypeOfTransfert typeOfTransfertSent,
//			final EnumResultViewTypes viewTypeSent,
//			RefGeneSetForAnnotCompa rgsfacIT) {
//		setWidget(uiBinder.createAndBindUi(this));
//		if (typeOfTransfertSent.compareTo(TypeOfTransfert.GENE_SET_COMPA_ANNOT_TO_HOMOLOG_BROWSING) == 0) {
//			transfertGenesSetToHomologBrowsing(
//					viewTypeSent,
//					rgsfacIT
//					);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error ResultLoadingDialog 9 SYNTENY_REGION_TO_HOMOLOG_BROWSING : unrecognized type of ResultLoadingMoreAbsoluteProportionItemDialog"));
//		}
//	}
//
//	public ResultLoadingDialog(
//			TypeOfTransfert typeOfTransfertSent,
//			final EnumResultViewTypes viewTypeSent,
//			final long mainOrigamiSyntenyId) {
//		setWidget(uiBinder.createAndBindUi(this));
//			if (typeOfTransfertSent.compareTo(TypeOfTransfert.SYNTENY_REGION_TO_HOMOLOG_BROWSING) == 0) {
//				transfertGenesOfSyntenyRegionToHomologBrowsing(
//						mainOrigamiSyntenyId, 
//						viewTypeSent
//						);
//			}else{
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error ResultLoadingDialog 11 SYNTENY_REGION_TO_HOMOLOG_BROWSING : unrecognized type of ResultLoadingMoreAbsoluteProportionItemDialog"));
//			}
//	}
//
//	public ResultLoadingDialog(TypeOfExport typeOfExportSent,
//			EnumResultViewTypes viewTypeInsyght,
//			int refGeneId,
//			int compaGeneId) {
//		if(typeOfExportSent.compareTo(TypeOfExport.GENE_SEQUENCES)==0){
//			exportGeneSequences(viewTypeInsyght,
//					refGeneId,
//					compaGeneId);
//		}else{
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error ResultLoadingDialog 12 SYNTENY_REGION_TO_HOMOLOG_BROWSING : unrecognized type of ResultLoadingMoreAbsoluteProportionItemDialog"));
//		}
//	}

	
	public void showGeneListInPopUp(
			final long mainOrigamiSyntenyId,
			final ArrayList<Integer> alElementIdTenStartThenStopLooped
			){

		AsyncCallback<ArrayList<LightGeneItem>> callback = new AsyncCallback<ArrayList<LightGeneItem>>(){

			@Override
			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			@Override
			public void onSuccess(ArrayList<LightGeneItem> alLgiSent) {
				GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.initListGenesWithAlLgi(alLgiSent,
						PopUpListGenesDialog.GeneListSortType.GENOMIC_POSITION);
				GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.displayPoPup();
			}
			
		};
		

		try {
			if(mainOrigamiSyntenyId != 0
					&& alElementIdTenStartThenStopLooped == null){
				callForInfoDBService.getListQLightGeneItemWithOrigamiSyntenyAlignmentId(
						mainOrigamiSyntenyId,
						callback);

			}else if (mainOrigamiSyntenyId == 0
					&& alElementIdTenStartThenStopLooped != null){
				callForInfoDBService.getListLightGeneItemWithWithAlElementIdAndStartPbAndStopPbLooped(
						alElementIdTenStartThenStopLooped
						, true
						, callback
						);
			}else{
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in showGeneListInPopUp : unrecognized case : mainOrigamiSyntenyId = "+mainOrigamiSyntenyId+" ; alElementIdTenStartThenStopLooped = "
			+ ( (alElementIdTenStartThenStopLooped != null ) ? alElementIdTenStartThenStopLooped.toString() : "NULL" )
			));
			}
			
			
		} catch (Exception e) {
			//String mssgError = "ERROR try in showGeneListInPopUp: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			
		}
		
	}
	

	public void exportGeneSequences(EnumResultViewTypes viewTypeInsyght,
			final int refGeneId,
			final int compaGeneId) {
		
		AsyncCallback<ArrayList<LightGeneItem>> callback = new AsyncCallback<ArrayList<LightGeneItem>>() {

			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			
			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<LightGeneItem> alLgiSent){
				
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				
				if(alLgiSent != null){
					
					String refMostSignificantQGeneNameAsStrippedStringToSend = null;
					String refMostSignificantQGeneLocusTagToSend = null;
					String refQGeneAccnumToSend = null;
					int refQGeneIdToSend = -1;
					String compMostSignificantSGeneNameAsStrippedStringToSend = null;
					String compMostSignificantSGeneLocusTagToSend = null;
					String compSGeneAccnumToSend = null;
					int compSGeneIdToSend = -1;
					int SyntenyOrigamiAlignmentIdToSend = -1;
					int SyntenyNumberGenesToSend = -1;
					int qOrigamiElementIdToSend = -1;
					int qPbStartQGenomicRegionInsertionInElementToSend = -1;
					int qPbStopQGenomicRegionInsertionInElementToSend = -1;
					int sOrigamiElementIdToSend = -1;
					int sPbStartSGenomicRegionInsertionInElementToSend = -1;
					int sPbStopSGenomicRegionInsertionInElementToSend = -1;
					
					if(refGeneId > 0 && alLgiSent.size() > 0){
						LightGeneItem refLgi = alLgiSent.get(0);
						refMostSignificantQGeneNameAsStrippedStringToSend = refLgi.getMostSignificantGeneNameAsStrippedText();
						refMostSignificantQGeneLocusTagToSend = refLgi.getLocusTagAsStrippedString();
						refQGeneAccnumToSend = refLgi.getAccession();
						refQGeneIdToSend = refLgi.getGeneId();
					}
					if(compaGeneId > 0 && alLgiSent.size() > 1){
						LightGeneItem compLgi = alLgiSent.get(1);
						compMostSignificantSGeneNameAsStrippedStringToSend = compLgi.getMostSignificantGeneNameAsStrippedText();
						compMostSignificantSGeneLocusTagToSend = compLgi.getLocusTagAsStrippedString();
						compSGeneAccnumToSend = compLgi.getAccession();
						compSGeneIdToSend = compLgi.getGeneId();
					}
					GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.displayPoPup(
							refMostSignificantQGeneNameAsStrippedStringToSend,
							refMostSignificantQGeneLocusTagToSend, refQGeneAccnumToSend,
							refQGeneIdToSend,
							compMostSignificantSGeneNameAsStrippedStringToSend,
							compMostSignificantSGeneLocusTagToSend, compSGeneAccnumToSend,
							compSGeneIdToSend, SyntenyOrigamiAlignmentIdToSend,
							SyntenyNumberGenesToSend, qOrigamiElementIdToSend,
							qPbStartQGenomicRegionInsertionInElementToSend,
							qPbStopQGenomicRegionInsertionInElementToSend,
							sOrigamiElementIdToSend,
							sPbStartSGenomicRegionInsertionInElementToSend,
							sPbStopSGenomicRegionInsertionInElementToSend,
							null);
					//GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.center();
				}
				
			}
		};

		try {
			ArrayList<Integer> listGenesIds = new ArrayList<Integer>();
			if(refGeneId > 0){
				listGenesIds.add(refGeneId);
			}
			if(compaGeneId > 0){
				listGenesIds.add(compaGeneId);
			}
			
			callForInfoDBService.getAllLightGeneItemWithListGeneIds_sortedByGeneId(listGenesIds, callback);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
		} catch (Exception e) {
			//String mssgError = "ERROR try in transfertGenesFromListElementIdsThenStartPbthenStopPBLoopedToHomologBrowsing: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
		}
		
	}


	public void transfertGenesFromListElementIdsThenStartPbthenStopPBLoopedToHomologBrowsing(
			final ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent,
			final EnumResultViewTypes viewTypeSent
			//,final int indexGenomePanelInLIST_GENOME_PANEL
			) {

		AsyncCallback<ArrayList<Integer>> callback = new AsyncCallback<ArrayList<Integer>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			public void onSuccess(ArrayList<Integer> alGeneIds) {
				if(alGeneIds.isEmpty()){
					//show no result
					//Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("No results to display", false);
					Window.alert("No results to display");
				}else{
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){
						initChangeToViewHomologTable(alGeneIds);
					}
				}
				
			}
		};

		try {
			callForInfoDBService
					.getGeneSetIdsWithListElementIdsThenStartPbthenStopPBLooped(
							listElementIdsThenStartPbthenStopPBLoopedSent
							, true
							, callback);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
		} catch (Exception e) {
			//String mssgError = "ERROR try in transfertGenesFromListElementIdsThenStartPbthenStopPBLoopedToHomologBrowsing: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);

		}
		
	}
	
	public void transfertGenesSetToHomologBrowsing(
			final EnumResultViewTypes viewTypeSent,
			RefGeneSetForAnnotCompa rgsfacIT) {
		
		AsyncCallback<ArrayList<Integer>> callback = new AsyncCallback<ArrayList<Integer>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<Integer> alGeneIds) {

				if(alGeneIds.isEmpty()){
					//show no result
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error transfertGenesOfSyntenyRegionToHomologBrowsing : No result to show"));
					//Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("No results to display", false);
				}else{
					
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){
						initChangeToViewHomologTable(alGeneIds);
					}
				}
				
				
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				
			}
		};

		try {
			boolean listOfGenesAlreadyHere = false;
			if(rgsfacIT.getListGeneIds() != null){
				if(!rgsfacIT.getListGeneIds().isEmpty()){
					listOfGenesAlreadyHere = true;
				}
			}
			if(listOfGenesAlreadyHere){
				initChangeToViewHomologTable(rgsfacIT.getListGeneIds());
			}else{
				callForInfoDBService
				.getGeneSetIdsWithListElementIdsThenStartPbthenStopPBLooped(
						rgsfacIT.getListElementIdsThenStartPbthenStopPBLooped()
						, true
						, callback);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
			}
			
		} catch (Exception e) {
			//String mssgError = "ERROR try in TransfertGenesOfSyntenyRegionToHomologBrowsing: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;

		}
	}
	
	protected void initChangeToViewHomologTable(ArrayList<Integer> alGeneIds) {
		
		//clear user selection
		Insyght.APP_CONTROLER.clearAllUserSelection();
		
		//set new ref gene set and clear old homo brow stuff
		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().setListReferenceGeneSetForHomologsTable(alGeneIds);
		Insyght.APP_CONTROLER.setLookUpForGeneForAllResults(-1, -1,
				-1, EnumResultViewTypes.homolog_table);
		
		//set enable/disable sort by type
		if(Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable().isEmpty()
				|| Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable().size()>SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED){
			ManageListComparedOrganisms.RB1_SORT_SCOPE_1.setEnabled(false);
		}else{
			ManageListComparedOrganisms.RB1_SORT_SCOPE_1.setEnabled(true);
		}

		//GenomicOrgaAndHomoTableViewImpl.changeToViewHomologTable(true);
		GenoOrgaAndHomoTablViewImpl.initScaleHomologTable = true;
		MainTabPanelView.getTabPanel().selectTab(2);
	}
	

	public void transfertGenesOfSyntenyRegionToHomologBrowsing(
			final long mainOrigamiSyntenyId,
			final EnumResultViewTypes viewTypeSent
			//,final int indexGenomePanelInLIST_GENOME_PANEL
			) {
		
		AsyncCallback<ArrayList<Integer>> callback = new AsyncCallback<ArrayList<Integer>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<Integer> alGeneIds) {

				if(alGeneIds.isEmpty()){
					//show no result
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error transfertGenesOfSyntenyRegionToHomologBrowsing : No result to show"));
					//Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("No results to display", false);
				}else{
					
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){

						initChangeToViewHomologTable(alGeneIds);

					}
				}
				
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;				
			}
		};

		try {
			
			// RQ type for genes
			// type 1 : strong homolog BDBH with avg score 220, average evalue
			// 5.76e-5
			// type 2 : weaker homolog with avg score 137, average evalue
			// 2.2e-4
			// type 3 mismatch
			// type 4 s insertion
			// type 5 q insertion
			HashSet<Integer> hsTypeToRetrieve = new HashSet<>();
			hsTypeToRetrieve.add(1);
			hsTypeToRetrieve.add(2);
			hsTypeToRetrieve.add(3);
			//hsTypeToRetrieve.add(4);
			hsTypeToRetrieve.add(5);
			
			callForInfoDBService.getGeneSetIdsWithOrigamiSyntenyId(
							mainOrigamiSyntenyId
							, hsTypeToRetrieve
							, callback
							);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
		} catch (Exception e) {
			//String mssgError = "ERROR try in TransfertGenesOfSyntenyRegionToHomologBrowsing: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
		}
		
	}

	public void transfertGeneIdInGenomicOrgaView(final int geneId,
			final EnumResultViewTypes viewTypeSent) {
		
		AsyncCallback<LightGeneItem> callback = new AsyncCallback<LightGeneItem>() {

			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(LightGeneItem lgiSent) {
				if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){
					
					Insyght.APP_CONTROLER.setLookUpForGeneForAllResults(lgiSent.getElementId(), lgiSent.getStart(),
							lgiSent.getStop(), EnumResultViewTypes.genomic_organization);
					//GenomicOrgaAndHomoTableViewImpl.changeToViewGenomicOrganisation();
					MainTabPanelView.getTabPanel().selectTab(4);
				}
			}
			
		};
		
		try {
			
			callForInfoDBService.getLightGeneItemWithGeneId(geneId, callback);
		} catch (Exception e) {
			//String mssgError = "ERROR try in transfertGeneIdInGenomicOrgaView: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			// do some UI stuff to show failure
		}
		
	}


	public void getMissingInfoForQGeneInsertion(final int indexOfHAPIInFullArray,
			final int indexOfHAPIInDisplayedArray,
			final int indexSliceInDisplay,
			final int indexOfGenomePanelInLIST_GENOME_PANEL,
			final int indexStartInSyntenyCanvasNavigationDisplay,
			final int indexStartInResultNavigationDisplay,
			final double percentSyntenyShowStartQGenome,
			final double percentSyntenyShowStopQGenome,
			final double percentSyntenyShowStartSGenome,
			final double percentSyntenyShowStopSGenome,
			AbsoPropQGeneInserItem absoluteProportionQComparableQGeneInsertionItemSent,
			final int numberOtherMatch,
			final boolean redrawOnTopSent,
			final boolean inRedSent,
			final EnumResultViewTypes viewTypeSent) {
		
		AsyncCallback<TransAPMissGeneHomoInfo> callback = new AsyncCallback<TransAPMissGeneHomoInfo>() {

			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(TransAPMissGeneHomoInfo tapmghiReturned) {
				
				if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){
					
					//if user navigation too fast, hide error
					try {
						//update objects
						//full array
						AbsoPropQCompaQSpanItem apqcqsiFullIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
						.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
						.getAbsoluteProportionQComparableQSpanItem();
						if(apqcqsiFullIT != null){
							if(apqcqsiFullIT instanceof AbsoPropQGeneInserItem){
								//ok update
								((AbsoPropQGeneInserItem)apqcqsiFullIT).setqOrigamiAlignmentId(tapmghiReturned.getAlignmentId());
								((AbsoPropQGeneInserItem)apqcqsiFullIT).setqOrigamiAlignmentPairsType(tapmghiReturned.getType());
								((AbsoPropQGeneInserItem)apqcqsiFullIT).setQGeneId(tapmghiReturned.getqGeneId());
								((AbsoPropQGeneInserItem)apqcqsiFullIT).setQLocusTag(tapmghiReturned.getqLocusTag());
								((AbsoPropQGeneInserItem)apqcqsiFullIT).setQName(tapmghiReturned.getqName());
								((AbsoPropQGeneInserItem)apqcqsiFullIT).setQStrand(tapmghiReturned.getqStrand());
							}else{
								//silent error if navigation too fast
								//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForQGeneInsertion full : apqcqsiFullIT not instanceof AbsoluteProportionQGeneInsertionItem"));
							}
						}
						
						//partial array ?
						boolean partialArray = false;
						AbsoPropQCompaQSpanItem apqcqsiPartialIT = null;
						if(doUpdatePartialArrayToo(percentSyntenyShowStartQGenome, 
								percentSyntenyShowStopQGenome, 
								percentSyntenyShowStartSGenome, 
								percentSyntenyShowStopSGenome,
								indexOfGenomePanelInLIST_GENOME_PANEL)){
							//ok partial has not changed for this stuff
									partialArray = true;
									apqcqsiPartialIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getListHAPI()
									.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getIdxToDisplayInListHAPI())
									.getAbsoluteProportionQComparableQSpanItem();
									
									if(apqcqsiPartialIT instanceof AbsoPropQGeneInserItem){
										//ok update
										((AbsoPropQGeneInserItem)apqcqsiPartialIT).setqOrigamiAlignmentId(tapmghiReturned.getAlignmentId());
										((AbsoPropQGeneInserItem)apqcqsiPartialIT).setqOrigamiAlignmentPairsType(tapmghiReturned.getType());
										((AbsoPropQGeneInserItem)apqcqsiPartialIT).setQGeneId(tapmghiReturned.getqGeneId());
										((AbsoPropQGeneInserItem)apqcqsiPartialIT).setQLocusTag(tapmghiReturned.getqLocusTag());
										((AbsoPropQGeneInserItem)apqcqsiPartialIT).setQName(tapmghiReturned.getqName());
										((AbsoPropQGeneInserItem)apqcqsiPartialIT).setQStrand(tapmghiReturned.getqStrand());
									
										if(tapmghiReturned.getsGeneId() >= 0){
											((AbsoPropQGeneInserItem)apqcqsiPartialIT).setIfMissingTargetSGeneId(tapmghiReturned.getsGeneId());
											((AbsoPropQGeneInserItem)apqcqsiPartialIT).setIfMissingTargetSLocusTag(tapmghiReturned.getsLocusTag());
											((AbsoPropQGeneInserItem)apqcqsiPartialIT).setIfMissingTargetSName(tapmghiReturned.getsName());
											((AbsoPropQGeneInserItem)apqcqsiPartialIT).setIfMissingTargetSStrand(tapmghiReturned.getsStrand());
										}
									
									}else{
										//silent error if navigation too fast
										//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForQGeneInsertion partial : apqcqsiPartialIT not instanceof AbsoluteProportionQGeneInsertionItem"));
									}
						}
						
						
						
						//update display slice ?
						if(doUpdateDisplay(percentSyntenyShowStartQGenome, 
								percentSyntenyShowStopQGenome, 
								percentSyntenyShowStartSGenome, 
								percentSyntenyShowStopSGenome,
								indexStartInResultNavigationDisplay,
								indexStartInSyntenyCanvasNavigationDisplay,
								indexOfGenomePanelInLIST_GENOME_PANEL)){
							//ok update display
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.contextBgk.translate(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.addToLeftToCenterDrawing+
									(SyntenyCanvas.SLICE_CANVAS_WIDTH*indexSliceInDisplay)
									,0);
							if(partialArray){
								if(apqcqsiPartialIT instanceof AbsoPropQGeneInserItem){
									Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockQGeneInsertionOrMissingTarget((AbsoPropQGeneInserItem)apqcqsiPartialIT,
											redrawOnTopSent, inRedSent,
											numberOtherMatch,
											false, -1, -1
											);
								}else{
									//silent error if navigation too fast
									//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForQGeneInsertion partial : apscqsiPartialIT not instanceof AbsoluteProportionQGeneInsertionItem"));
								}
							}else{
								if(apqcqsiFullIT != null){
									if(apqcqsiFullIT instanceof AbsoPropQGeneInserItem){
										Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockQGeneInsertionOrMissingTarget((AbsoPropQGeneInserItem)apqcqsiFullIT,
												redrawOnTopSent, inRedSent,
												numberOtherMatch,
												false, -1, -1
												);
									}else{
										//silent error if navigation too fast
										//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForQGeneInsertion full : apscqsiFullIT not instanceof AbsoluteProportionQGeneInsertionItem"));
									}
								}
								
							}
							
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();

							
						}
					} catch (IndexOutOfBoundsException ex) {
						//silent error if navigation too fast
						//
					}
					
				}
			}
			
		};
		
		try {
			TransAPMissGeneHomoInfo tToSent = new TransAPMissGeneHomoInfo(absoluteProportionQComparableQGeneInsertionItemSent);
			
			//getFullQGeneInfoWithPartialQGeneInsertionItem
			callForInfoDBService
					.getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem(
							tToSent, false, 
							callback);
		} catch (Exception e) {
			//String mssgError = "ERROR try in getMissingInfoForQGeneInsertion: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			// do some UI stuff to show failure
		}
		
		
	}

	public void getMissingInfoForSGeneInsertion(final int indexOfHAPIInFullArray,
			final int indexOfHAPIInDisplayedArray,
			final int indexSliceInDisplay,
			final int indexOfGenomePanelInLIST_GENOME_PANEL,
			final int indexStartInSyntenyCanvasNavigationDisplay,
			final int indexStartInResultNavigationDisplay,
			final double percentSyntenyShowStartQGenome,
			final double percentSyntenyShowStopQGenome,
			final double percentSyntenyShowStartSGenome,
			final double percentSyntenyShowStopSGenome,
			AbsoPropSGeneInserItem absoluteProportionSComparableSGeneInsertionItemSent,
			final int numberOtherMatch,
			final boolean redrawOnTopSent,
			final boolean inRedSent,
			final EnumResultViewTypes viewTypeSent) {
		
		AsyncCallback<TransAPMissGeneHomoInfo> callback = new AsyncCallback<TransAPMissGeneHomoInfo>() {

			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(TransAPMissGeneHomoInfo tapmghReturned) {
				

				if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){
					
					//if user navigation too fast, hide error
					try {
						//update objects
						//full array
						AbsoPropSCompaSSpanItem apscssiFullIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
						.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
						.getAbsoluteProportionSComparableSSpanItem();
						if(apscssiFullIT != null){
							if(apscssiFullIT instanceof AbsoPropSGeneInserItem){
								//ok update
								((AbsoPropSGeneInserItem)apscssiFullIT).setsOrigamiAlignmentId(tapmghReturned.getAlignmentId());
								((AbsoPropSGeneInserItem)apscssiFullIT).setsOrigamiAlignmentPairsType(tapmghReturned.getType());
								((AbsoPropSGeneInserItem)apscssiFullIT).setsGeneId(tapmghReturned.getsGeneId());
								((AbsoPropSGeneInserItem)apscssiFullIT).setsLocusTag(tapmghReturned.getsLocusTag());
								((AbsoPropSGeneInserItem)apscssiFullIT).setsName(tapmghReturned.getsName());
								((AbsoPropSGeneInserItem)apscssiFullIT).setsStrand(tapmghReturned.getsStrand());
								
							}else{
								//silent error if navigation too fast
								//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForSGeneInsertion full : apscssiFullIT not instanceof AbsoluteProportionSGeneInsertionItem"));
							}
						}
						
						
						
						//partial array ?
						boolean partialArray = false;
						AbsoPropSCompaSSpanItem apscssiPartialIT = null;
						if(doUpdatePartialArrayToo(percentSyntenyShowStartQGenome, 
								percentSyntenyShowStopQGenome, 
								percentSyntenyShowStartSGenome, 
								percentSyntenyShowStopSGenome,
								indexOfGenomePanelInLIST_GENOME_PANEL)){
							//ok partial has not changed for this stuff
							partialArray = true;
							apscssiPartialIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getListHAPI()
									.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getIdxToDisplayInListHAPI())
									.getAbsoluteProportionSComparableSSpanItem();
							if(apscssiPartialIT instanceof AbsoPropSGeneInserItem){
								//ok update
								((AbsoPropSGeneInserItem)apscssiPartialIT).setsOrigamiAlignmentId(tapmghReturned.getAlignmentId());
								((AbsoPropSGeneInserItem)apscssiPartialIT).setsOrigamiAlignmentPairsType(tapmghReturned.getType());
								((AbsoPropSGeneInserItem)apscssiPartialIT).setsGeneId(tapmghReturned.getsGeneId());
								((AbsoPropSGeneInserItem)apscssiPartialIT).setsLocusTag(tapmghReturned.getsLocusTag());
								((AbsoPropSGeneInserItem)apscssiPartialIT).setsName(tapmghReturned.getsName());
								((AbsoPropSGeneInserItem)apscssiPartialIT).setsStrand(tapmghReturned.getsStrand());
								
								if(tapmghReturned.getqGeneId() >= 0){
									((AbsoPropSGeneInserItem)apscssiPartialIT).setIfMissingTargetQGeneId(tapmghReturned.getqGeneId());
									((AbsoPropSGeneInserItem)apscssiPartialIT).setIfMissingTargetQLocusTag(tapmghReturned.getqLocusTag());
									((AbsoPropSGeneInserItem)apscssiPartialIT).setIfMissingTargetQName(tapmghReturned.getqName());
									((AbsoPropSGeneInserItem)apscssiPartialIT).setIfMissingTargetQStrand(tapmghReturned.getqStrand());
								}
								
							}else{
								//silent error if navigation too fast
								//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForSGeneInsertion partial : apscssiPartialIT not instanceof AbsoluteProportionSGeneInsertionItem"));
							}
							
						}
						
						//update display slice ?
						if(doUpdateDisplay(percentSyntenyShowStartQGenome, 
								percentSyntenyShowStopQGenome, 
								percentSyntenyShowStartSGenome, 
								percentSyntenyShowStopSGenome,
								indexStartInResultNavigationDisplay,
								indexStartInSyntenyCanvasNavigationDisplay,
								indexOfGenomePanelInLIST_GENOME_PANEL)){
							//ok update display
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.contextBgk.translate(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.addToLeftToCenterDrawing+
									(SyntenyCanvas.SLICE_CANVAS_WIDTH*indexSliceInDisplay)
									,0);
							
//							public void drawBlockSGeneInsertionOrMissingTarget(
//									AbsoluteProportionSGeneInsertionItem absoluteProportionSComparableSGeneInsertionItemSent,
//									boolean redrawOnTop, boolean inRed, int numberOtherMatch,
//									boolean allowFetchMissingData, int idxHAPInFullArray, int idxSliceDisplayed) {
							if(partialArray){
								if(apscssiPartialIT instanceof AbsoPropSGeneInserItem){
									Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockSGeneInsertionOrMissingTarget((AbsoPropSGeneInserItem)apscssiPartialIT,
											redrawOnTopSent, inRedSent,
											numberOtherMatch,
											false, -1, -1
											);
								}else{
									//silent error if navigation too fast
									//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForSGeneInsertion partial : apscssiPartialIT not instanceof AbsoluteProportionSGeneInsertionItem"));
								}
							}else{
								if(apscssiFullIT != null){
									if(apscssiFullIT instanceof AbsoPropSGeneInserItem){
										Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockSGeneInsertionOrMissingTarget((AbsoPropSGeneInserItem)apscssiFullIT,
												redrawOnTopSent, inRedSent,
												numberOtherMatch,
												false, -1, -1
												);
									}else{
										//silent error if navigation too fast
										//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForSGeneInsertion full : apscssiFullIT not instanceof AbsoluteProportionSGeneInsertionItem"));
									}
								}
								
							}
							
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
							
						}
					} catch (IndexOutOfBoundsException ex) {
						//silent error if navigation too fast
						//
					}
					
				}
			}
			
		};
		
		try {
			
			//System.out.println("before getqPercentStart : "+absoluteProportionQComparableQSGeneHomologyItemSent.getqPercentStart());
			TransAPMissGeneHomoInfo tToSent = new TransAPMissGeneHomoInfo(absoluteProportionSComparableSGeneInsertionItemSent);
			
			//.getFullSGeneInfoWithPartialSGeneInsertionItem
			callForInfoDBService.getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem(
							tToSent, false, 
							callback);
		} catch (Exception e) {
			//String mssgError = "ERROR try in getMissingInfoForSGeneInsertion: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			// do some UI stuff to show failure
		}
		
	}

	public void getMissingInfoForGeneHomology(final int indexOfHAPIInFullArray,
			final int indexOfHAPIInDisplayedArray,
			final int indexSliceInDisplay,
			final int indexOfGenomePanelInLIST_GENOME_PANEL,
			final int indexStartInSyntenyCanvasNavigationDisplay,
			final int indexStartInResultNavigationDisplay,
			final double percentSyntenyShowStartQGenome,
			final double percentSyntenyShowStopQGenome,
			final double percentSyntenyShowStartSGenome,
			final double percentSyntenyShowStopSGenome,
			AbsoPropQSGeneHomoItem absoluteProportionQComparableQSGeneHomologyItemSent,
			final int numberOtherMatch,
			final boolean redrawOnTopSent,
			final boolean inRedSent,
			final EnumResultViewTypes viewTypeSent
			//,final boolean geneHomologyGeneNameUpClicked, final boolean geneHomologyGeneNameDownClicked, final boolean geneHomologyMatchZoneClicked
			) {
		
		AsyncCallback<TransAPMissGeneHomoInfo> callback = new AsyncCallback<TransAPMissGeneHomoInfo>() {

			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(TransAPMissGeneHomoInfo tReturned) {
				
				if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){

					//if user navigation too fast, hide error
					try {
						//update objects
						//full array
						AbsoPropQCompaQSSpanItem apqcqsiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
						.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
								.getAbsoluteProportionQComparableQSSpanItem();
						if(apqcqsiIT instanceof AbsoPropQSGeneHomoItem){
							//OK update
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setListLightGeneMatchItem(tReturned.getListLightGeneMatchItem());
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setSyntenyOrigamiAlignmentId(tReturned.getAlignmentId());
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsOrigamiAlignmentPairsType(tReturned.getType());
							if(tReturned.getType() == 1){
								((AbsoPropQSGeneHomoItem)apqcqsiIT).getQsStyleItem().setStyleAsOrtholog(true);
							}else{
								((AbsoPropQSGeneHomoItem)apqcqsiIT).getQsStyleItem().setStyleAsOrtholog(false);
							}
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsQGeneId(tReturned.getqGeneId());
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsQLocusTag(tReturned.getqLocusTag());
							//if (tReturned.getqGeneId() == 12) {
							//	GWT.log("qsQName = "+tReturned.getqName());
							//	GWT.log("qsQLocusTag = "+tReturned.getqLocusTag());
							//}
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsQName(tReturned.getqName());
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsQStrand(tReturned.getqStrand());
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsSGeneId(tReturned.getsGeneId());
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsSLocusTag(tReturned.getsLocusTag());
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsSName(tReturned.getsName());
							((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsSStrand(tReturned.getsStrand());
							//set setQsEnumBlockType
							if(tReturned.getqStrand()+tReturned.getsStrand()==0){
								((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsEnumBlockType(QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK);
							}else{
								((AbsoPropQSGeneHomoItem)apqcqsiIT).setQsEnumBlockType(QSEnumBlockType.QS_HOMOLOGS_BLOCK);
							}
							
						}else{
							//silent error if navigation too fast
							//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForGeneHomology full : apqcqsiIT not instanceof AbsoluteProportionQSGeneHomologyItem"));
						}
						

						//partial array ?
						boolean partialArray = false;
						AbsoPropQCompaQSSpanItem apqcqsiPartialIT = null;
						if(doUpdatePartialArrayToo(percentSyntenyShowStartQGenome, 
								percentSyntenyShowStopQGenome, 
								percentSyntenyShowStartSGenome, 
								percentSyntenyShowStopSGenome,
								indexOfGenomePanelInLIST_GENOME_PANEL)){
									//ok partial has not changed for this stuff
									partialArray = true;
									
									apqcqsiPartialIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getListHAPI()
											.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getIdxToDisplayInListHAPI())
											.getAbsoluteProportionQComparableQSSpanItem();
									if(apqcqsiPartialIT instanceof AbsoPropQSGeneHomoItem){
										//OK update
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setListLightGeneMatchItem(tReturned.getListLightGeneMatchItem());
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setSyntenyOrigamiAlignmentId(tReturned.getAlignmentId());
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setQsOrigamiAlignmentPairsType(tReturned.getType());
										if(tReturned.getType() == 1){
											((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).getQsStyleItem().setStyleAsOrtholog(true);
										}else{
											((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).getQsStyleItem().setStyleAsOrtholog(false);
										}
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setQsQGeneId(tReturned.getqGeneId());
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setQsQLocusTag(tReturned.getqLocusTag());
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setQsQName(tReturned.getqName());
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setQsQStrand(tReturned.getqStrand());
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setQsSGeneId(tReturned.getsGeneId());
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setQsSLocusTag(tReturned.getsLocusTag());
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setQsSName(tReturned.getsName());
										((AbsoPropQSGeneHomoItem)apqcqsiPartialIT).setQsSStrand(tReturned.getsStrand());
										
									
									}else{
										//silent error if navigation too fast
										//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForGeneHomology partial : apqcqsiPartialIT not instanceof AbsoluteProportionQSGeneHomologyItem"));
									}
						}
						
						//update display slice ?
						if(doUpdateDisplay(percentSyntenyShowStartQGenome, 
								percentSyntenyShowStopQGenome, 
								percentSyntenyShowStartSGenome, 
								percentSyntenyShowStopSGenome,
								indexStartInResultNavigationDisplay,
								indexStartInSyntenyCanvasNavigationDisplay,
								indexOfGenomePanelInLIST_GENOME_PANEL)){
							//ok update display
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.contextBgk.translate(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.addToLeftToCenterDrawing+
									(SyntenyCanvas.SLICE_CANVAS_WIDTH*indexSliceInDisplay)
									,0);
							if(partialArray){
								if(apqcqsiPartialIT instanceof AbsoPropQSGeneHomoItem){
									Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockQSGeneHomology((AbsoPropQSGeneHomoItem)apqcqsiPartialIT,
											redrawOnTopSent, inRedSent,
											true, true, true, true, true, true,
											numberOtherMatch,
											//geneHomologyGeneNameUpClicked, geneHomologyGeneNameDownClicked, geneHomologyMatchZoneClicked,
											false, -1, -1,
											null, null
											//,true, false, false
											);
									
								}else{
									//silent error if navigation too fast
									//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForGeneHomology partial : apqcqsiPartialIT not instanceof AbsoluteProportionQSGeneHomologyItem"));
								}
							}else{
								if(apqcqsiIT instanceof AbsoPropQSGeneHomoItem){
									Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockQSGeneHomology((AbsoPropQSGeneHomoItem)apqcqsiIT,
											redrawOnTopSent, inRedSent,
											true, true, true, true, true, true,
											numberOtherMatch,
											//geneHomologyGeneNameUpClicked, geneHomologyGeneNameDownClicked, geneHomologyMatchZoneClicked,
											false, -1, -1,
											null, null
											//,true, false, false
											);
								}else{
									//silent error if navigation too fast
									//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getMissingInfoForGeneHomology full : apqcqsiIT not instanceof AbsoluteProportionQSGeneHomologyItem"));
								}
							}
							
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
						
							
						}
					} catch (IndexOutOfBoundsException ex) {
						//silent error if navigation too fast
						//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, ex);
					}
					
				}
			}
		};
		
		try {
			
			//System.out.println("before getqPercentStart : "+absoluteProportionQComparableQSGeneHomologyItemSent.getqPercentStart());
			TransAPMissGeneHomoInfo tToSent = new TransAPMissGeneHomoInfo(absoluteProportionQComparableQSGeneHomologyItemSent);
			
			callForInfoDBService.getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem(
							tToSent, true, 
							callback);
		} catch (Exception e) {
			//String mssgError = "ERROR try in getMissingInfoForGeneHomology: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			// do some UI stuff to show failure
		}
		
			
	}



	public void getGeneCountForSElement(
			final int indexOfHAPIInFullArray,
			final int indexOfHAPIInDisplayedArray,
			final int indexSliceInDisplay,
			final int indexOfGenomePanelInLIST_GENOME_PANEL,
			final int indexStartInSyntenyCanvasNavigationDisplay,
			final int indexStartInResultNavigationDisplay,
			final double percentSyntenyShowStartQGenome,
			final double percentSyntenyShowStopQGenome,
			final double percentSyntenyShowStartSGenome,
			final double percentSyntenyShowStopSGenome,
			final int origamiElementIdSent,
			final SEnumBlockType sEnumBlockTypeSent,
			final boolean redrawOnTopSent,
			final boolean inRedSent,
			final EnumResultViewTypes viewTypeSent) {
		
		AsyncCallback<Integer> callback = new AsyncCallback<Integer>() {

			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(Integer intReturned) {
				
				if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){

					//update objects
					//full array
					((AbsoPropSElemItem)
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
					.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
					.getAbsoluteProportionSComparableSSpanItem())
					.setsElementNumberGene(intReturned);
					
					//partial array ?
					if(doUpdatePartialArrayToo(percentSyntenyShowStartQGenome, 
							percentSyntenyShowStopQGenome, 
							percentSyntenyShowStartSGenome, 
							percentSyntenyShowStopSGenome,
							indexOfGenomePanelInLIST_GENOME_PANEL)){
						//ok partial has not changed for this stuff
						((AbsoPropSElemItem)
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getListHAPI()
								.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getIdxToDisplayInListHAPI())
								.getAbsoluteProportionSComparableSSpanItem())
								.setsElementNumberGene(intReturned);
					}
					
					//update display slice ?
					if(doUpdateDisplay(percentSyntenyShowStartQGenome, 
							percentSyntenyShowStopQGenome, 
							percentSyntenyShowStartSGenome, 
							percentSyntenyShowStopSGenome,
							indexStartInResultNavigationDisplay,
							indexStartInSyntenyCanvasNavigationDisplay,
							indexOfGenomePanelInLIST_GENOME_PANEL)){
						if(intReturned > 0){
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.clearSymbolicSliceSOnly(indexSliceInDisplay, true, true);
							String newText = intReturned+" CDS";
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.contextBgk.translate(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.addToLeftToCenterDrawing+
									(SyntenyCanvas.SLICE_CANVAS_WIDTH*indexSliceInDisplay)
									,0);
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockSGenomicRegionInsertionOrMissingTarget(
									sEnumBlockTypeSent, newText, redrawOnTopSent, inRedSent,
									false, false, false,
									false,
									false, false,
									intReturned);
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
						}
					}
					
				}
				
				
			}
		};
		

		try {
			homologieBrowsingResultService
					.getNumberGenesWithOrigamiElementId(origamiElementIdSent,
							callback);
		} catch (Exception e) {
			//String mssgError = "ERROR try in getGeneCountForSElement: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			// do some UI stuff to show failure
		}
		
		
	}


	public void getGeneCountForSGenomicRegionInsertion(
			final int indexOfHAPIInFullArray,
			final int indexOfHAPIInDisplayedArray,
			final int indexSliceInDisplay,
			final int indexOfGenomePanelInLIST_GENOME_PANEL,
			final int indexStartInSyntenyCanvasNavigationDisplay,
			final int indexStartInResultNavigationDisplay,
			final double percentSyntenyShowStartQGenome,
			final double percentSyntenyShowStopQGenome,
			final double percentSyntenyShowStartSGenome,
			final double percentSyntenyShowStopSGenome,
			final int origamiElementIdSent,
			final int pbStartInElement,
			final int pbStopInElement,
			final SEnumBlockType sEnumBlockTypeSent,
			final boolean redrawOnTopSent,
			final boolean inRedSent,
			final EnumResultViewTypes viewTypeSent,
			final boolean previousSOfNextSlice,
			final boolean nextSOfPreviousSlice) {
		
		AsyncCallback<Integer> callback = new AsyncCallback<Integer>() {

			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(Integer intReturned) {
				
				//System.err.println("here:"+intReturned+" ; pbStartInElement="+pbStartInElement+" ; pbStopInElement="+pbStopInElement);
				
				if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){
					//update objects
					//full array
					AbsoPropSGenoRegiInserItem apsgriiInFullArray = null;
					if(previousSOfNextSlice){
						int newindexOfHAPIInFullArray = indexOfHAPIInFullArray+1;
						if(newindexOfHAPIInFullArray > 0
								&& newindexOfHAPIInFullArray < Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getFullAssociatedlistAbsoluteProportionItemToDisplay().size()){
							AbsoPropQCompaQSSpanItem apqcqssiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
									.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(newindexOfHAPIInFullArray).getListHAPI()
									.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
											.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(newindexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
									.getAbsoluteProportionQComparableQSSpanItem();
							if(apqcqssiIT != null){
								if(apqcqssiIT instanceof AbsoPropQSSyntItem){
									apsgriiInFullArray = ((AbsoPropQSSyntItem)apqcqssiIT).getPreviousGenomicRegionSInsertion();
								}
							}
						}
					}else if(nextSOfPreviousSlice){
						int newindexOfHAPIInFullArray = indexOfHAPIInFullArray-1;
						if(newindexOfHAPIInFullArray > 0
								&& newindexOfHAPIInFullArray < Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getFullAssociatedlistAbsoluteProportionItemToDisplay().size()){
							AbsoPropQCompaQSSpanItem apqcqssiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
									.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(newindexOfHAPIInFullArray).getListHAPI()
									.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
											.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(newindexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
									.getAbsoluteProportionQComparableQSSpanItem();
							if(apqcqssiIT != null){
								if(apqcqssiIT instanceof AbsoPropQSSyntItem){
									apsgriiInFullArray = ((AbsoPropQSSyntItem)apqcqssiIT).getNextGenomicRegionSInsertion();
								}
							}
						}
					}else{
						apsgriiInFullArray = ((AbsoPropSGenoRegiInserItem)
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
								.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
										.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
								.getAbsoluteProportionSComparableSSpanItem());
					}
					
					if(apsgriiInFullArray != null){
						apsgriiInFullArray.setSGenomicRegionInsertionNumberGene(intReturned);
					}
					
					//partial array ?
					if(doUpdatePartialArrayToo(percentSyntenyShowStartQGenome, 
							percentSyntenyShowStopQGenome, 
							percentSyntenyShowStartSGenome, 
							percentSyntenyShowStopSGenome,
							indexOfGenomePanelInLIST_GENOME_PANEL)){
						//ok partial has not changed for this stuff
						
						
						AbsoPropSGenoRegiInserItem apsgriiInPartialArray = null;
						if(previousSOfNextSlice){
							int newindexOfHAPIInFullArray = indexOfHAPIInFullArray+1;
							if(newindexOfHAPIInFullArray > 0
									&& newindexOfHAPIInFullArray < Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()){
								AbsoPropQCompaQSSpanItem apqcqssiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getListHAPI()
										.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getIdxToDisplayInListHAPI())
										.getAbsoluteProportionQComparableQSSpanItem();
								if(apqcqssiIT != null){
									if(apqcqssiIT instanceof AbsoPropQSSyntItem){
										apsgriiInPartialArray = ((AbsoPropQSSyntItem)apqcqssiIT).getPreviousGenomicRegionSInsertion();
									}
								}
							}
						}else if(nextSOfPreviousSlice){
							int newindexOfHAPIInFullArray = indexOfHAPIInFullArray-1;
							if(newindexOfHAPIInFullArray > 0
									&& newindexOfHAPIInFullArray < Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()){
								AbsoPropQCompaQSSpanItem apqcqssiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getListHAPI()
										.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getIdxToDisplayInListHAPI())
										.getAbsoluteProportionQComparableQSSpanItem();
								if(apqcqssiIT != null){
									if(apqcqssiIT instanceof AbsoPropQSSyntItem){
										apsgriiInPartialArray = ((AbsoPropQSSyntItem)apqcqssiIT).getNextGenomicRegionSInsertion();
									}
								}
							}
						}else{
							apsgriiInPartialArray = ((AbsoPropSGenoRegiInserItem)
									Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getListHAPI()
									.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getIdxToDisplayInListHAPI())
									.getAbsoluteProportionSComparableSSpanItem());
						}
						
						if(apsgriiInPartialArray != null){
							apsgriiInPartialArray.setSGenomicRegionInsertionNumberGene(intReturned);
						}
					}
					
					//update display slice
					if(doUpdateDisplay(percentSyntenyShowStartQGenome, 
							percentSyntenyShowStopQGenome, 
							percentSyntenyShowStartSGenome, 
							percentSyntenyShowStopSGenome,
							indexStartInResultNavigationDisplay,
							indexStartInSyntenyCanvasNavigationDisplay,
							indexOfGenomePanelInLIST_GENOME_PANEL)){
						if(intReturned > 0){
							if(apsgriiInFullArray != null){
								if(previousSOfNextSlice){
									Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.clearSymbolicSliceSOnly(indexSliceInDisplay, false, true);
								}else if(nextSOfPreviousSlice){
									Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.clearSymbolicSliceSOnly(indexSliceInDisplay, true, false);
								}else{
									Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.clearSymbolicSliceSOnly(indexSliceInDisplay, true, true);
								}
								
								String newText = intReturned+" CDS";
								//System.err.println("indexSliceInDisplay : "+indexSliceInDisplay);
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.contextBgk.translate(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.addToLeftToCenterDrawing+
										(SyntenyCanvas.SLICE_CANVAS_WIDTH*indexSliceInDisplay)
										,0);
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockSGenomicRegionInsertionOrMissingTarget(
										sEnumBlockTypeSent, newText, redrawOnTopSent, inRedSent,
										false,
										apsgriiInFullArray.isAnchoredToPrevious(),
										apsgriiInFullArray.isAnchoredToNext(),
										apsgriiInFullArray.isAnnotationCollision(),
										apsgriiInFullArray.isAnchoredToStartElement(),
										apsgriiInFullArray.isAnchoredToStopElement(),
										intReturned
								);
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
							}
						}
					}
				}
			}
		};
		

		try {
			homologieBrowsingResultService
					.getNumberGenesBetweenGenomicRegionWithOrigamiElementIDAndStartPbAndStopPb(origamiElementIdSent,
							pbStartInElement,
							pbStopInElement,
							callback);
		} catch (Exception e) {
			//String mssgError = "ERROR try in getGeneCountForSGenomicRegionInsertion: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			// do some UI stuff to show failure
		}
		
		
	}


	
	public void getGeneCountForQElement(
			final int indexOfHAPIInFullArray,
			final int indexOfHAPIInDisplayedArray,
			final int indexSliceInDisplay,
			final int indexOfGenomePanelInLIST_GENOME_PANEL,
			final int indexStartInSyntenyCanvasNavigationDisplay,
			final int indexStartInResultNavigationDisplay,
			final double percentSyntenyShowStartQGenome,
			final double percentSyntenyShowStopQGenome,
			final double percentSyntenyShowStartSGenome,
			final double percentSyntenyShowStopSGenome,
			final int origamiElementIdSent,
			final QEnumBlockType qEnumBlockTypeSent,
			final boolean redrawOnTopSent,
			final boolean inRedSent,
			final EnumResultViewTypes viewTypeSent) {
		
		AsyncCallback<Integer> callback = new AsyncCallback<Integer>() {

			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(Integer intReturned) {
				
				if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){
				
					//update objects
					//full array
					((AbsoPropQElemItem)
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
					.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
					.getAbsoluteProportionQComparableQSpanItem())
					.setqElementNumberGene(intReturned);
					
					//partial array ?
					if(doUpdatePartialArrayToo(percentSyntenyShowStartQGenome, 
							percentSyntenyShowStopQGenome, 
							percentSyntenyShowStartSGenome, 
							percentSyntenyShowStopSGenome,
							indexOfGenomePanelInLIST_GENOME_PANEL)){
						//ok partial has not changed for this stuff
						((AbsoPropQElemItem)
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getListHAPI()
								.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getIdxToDisplayInListHAPI())
								.getAbsoluteProportionQComparableQSpanItem())
								.setqElementNumberGene(intReturned);
					}
					
					
					//update display slice
					//update display slice
					if(doUpdateDisplay(percentSyntenyShowStartQGenome, 
							percentSyntenyShowStopQGenome, 
							percentSyntenyShowStartSGenome, 
							percentSyntenyShowStopSGenome,
							indexStartInResultNavigationDisplay,
							indexStartInSyntenyCanvasNavigationDisplay,
							indexOfGenomePanelInLIST_GENOME_PANEL)){

						if(intReturned > 0){
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.clearSymbolicSliceQOnly(indexSliceInDisplay);
							String newText = intReturned+" CDS";
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.contextBgk.translate(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.addToLeftToCenterDrawing+
									(SyntenyCanvas.SLICE_CANVAS_WIDTH*indexSliceInDisplay)
									,0);
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(
									qEnumBlockTypeSent, newText, redrawOnTopSent, inRedSent,
									false, false,
									false, false,
									intReturned);
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
						}
					}
					
				
				}
				
				
				
			}
		};
		

		try {
			homologieBrowsingResultService
					.getNumberGenesWithOrigamiElementId(origamiElementIdSent,
							callback);
		} catch (Exception e) {
			//String mssgError = "ERROR try in getGeneCountForQElement: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			// do some UI stuff to show failure
		}
		
			
	}
	

	public void getGeneCountForQGenomicRegionInsertion(
			final int indexOfHAPIInFullArray,
			final int indexOfHAPIInDisplayedArray,
			final int indexSliceInDisplay,
			final int indexOfGenomePanelInLIST_GENOME_PANEL,
			final int indexStartInSyntenyCanvasNavigationDisplay,
			final int indexStartInResultNavigationDisplay,
			final double percentSyntenyShowStartQGenome,
			final double percentSyntenyShowStopQGenome,
			final double percentSyntenyShowStartSGenome,
			final double percentSyntenyShowStopSGenome,
			final int origamiElementIdSent,
			final int pbStartInElement,
			final int pbStopInElement,
			final QEnumBlockType qEnumBlockTypeSent,
			final boolean redrawOnTopSent,
			final boolean inRedSent,
			final EnumResultViewTypes viewTypeSent) {
		
		AsyncCallback<Integer> callback = new AsyncCallback<Integer>() {

			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(Integer intReturned) {
				
				if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){

					//update objects
					//full array
					((AbsoPropQGenoRegiInserItem)
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
					.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
					.getAbsoluteProportionQComparableQSpanItem())
					.setQGenomicRegionInsertionNumberGene(intReturned);
					
					//partial array ?
					if(doUpdatePartialArrayToo(percentSyntenyShowStartQGenome, 
							percentSyntenyShowStopQGenome, 
							percentSyntenyShowStartSGenome, 
							percentSyntenyShowStopSGenome,
							indexOfGenomePanelInLIST_GENOME_PANEL)){
						//ok partial has not changed for this stuff
						((AbsoPropQGenoRegiInserItem)
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getListHAPI()
								.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexOfHAPIInDisplayedArray).getIdxToDisplayInListHAPI())
								.getAbsoluteProportionQComparableQSpanItem())
								.setQGenomicRegionInsertionNumberGene(intReturned);
					}
					
					
					
					//update display slice
					if(doUpdateDisplay(percentSyntenyShowStartQGenome, 
							percentSyntenyShowStopQGenome, 
							percentSyntenyShowStartSGenome, 
							percentSyntenyShowStopSGenome,
							indexStartInResultNavigationDisplay,
							indexStartInSyntenyCanvasNavigationDisplay,
							indexOfGenomePanelInLIST_GENOME_PANEL)){

						if(intReturned > 0){
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.clearSymbolicSliceQOnly(indexSliceInDisplay);
							String newText = intReturned+" CDS";
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.contextBgk.translate(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.addToLeftToCenterDrawing+
									(SyntenyCanvas.SLICE_CANVAS_WIDTH*indexSliceInDisplay)
									,0);
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(
									qEnumBlockTypeSent, newText, redrawOnTopSent, inRedSent,
									false,
									((AbsoPropQGenoRegiInserItem)
											Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
											.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
											.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
													.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
											.getAbsoluteProportionQComparableQSpanItem()).isAnnotationCollision(),
									((AbsoPropQGenoRegiInserItem)
											Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
											.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
											.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
													.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
											.getAbsoluteProportionQComparableQSpanItem()).isAnchoredToStartElement(),
									((AbsoPropQGenoRegiInserItem)
											Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
											.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getListHAPI()
											.get(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
													.getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexOfHAPIInFullArray).getIdxToDisplayInListHAPI())
											.getAbsoluteProportionQComparableQSpanItem()).isAnchoredToStopElement(),
											intReturned
									);
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.restoreToOriginCoordinateSpace();
						}
					}
					
				
				}
				
			}
		};
		

		try {
			homologieBrowsingResultService
					.getNumberGenesBetweenGenomicRegionWithOrigamiElementIDAndStartPbAndStopPb(origamiElementIdSent,
							pbStartInElement,
							pbStopInElement,
							callback);
		} catch (Exception e) {
			//String mssgError = "ERROR try in getGeneCountForQGenomicRegionInsertion: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			// do some UI stuff to show failure
		}
		
			
	}

	public void getBasicElementInfoWithGeneId(
			final String currSelectedSyntenyCanvasItemRefId, final int origamiGeneId, final boolean q,
			final EnumResultViewTypes viewTypeSent) {
		
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				//check if selection has changed
				
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);

				if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
					AnnotCompaViewImpl.basicElementInfo.setHTML("<ul><li><b>ERROR getDetailledElementInfo</b></li></ul>");
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog getBasicElementInfoWithGeneId : unrecognized viewTypeSent = "+viewTypeSent.toString()));
				}
				

			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(String stringReturned) {
				
				if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
					AnnotCompaViewImpl.basicElementInfo.setHTML(stringReturned);
				}else{
					Window.alert("Error in ResultLoadingDialog getBasicElementInfoWithGeneId : unrecognized viewTypeSent");
				}
				
				
				
			}
		};

		try {
			
			if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
				callForInfoDBService.getBasicElementInfoAsStringWithGeneId(origamiGeneId,
						callback);
			}else{
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog getBasicElementInfoWithGeneId : unrecognized viewTypeSent = "+viewTypeSent));
			}
			
			
		} catch (Exception e) {
			//String mssgError = "ERROR try in getBasicElementInfoWithGeneId: "+ e.getMessage();
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			
			if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
				AnnotCompaViewImpl.basicElementInfo.setHTML("<ul><li><b>Error</b></li></ul>");
			}
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			
		}
		
	}

	
	public void getDetailledElementInfo(
			final String currSelectedSyntenyCanvasItemRefId, final int origamiElementId, final boolean q,
			final EnumResultViewTypes viewTypeSent) {
		
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				//check if selection has changed
				
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				if(viewTypeSent.compareTo(EnumResultViewTypes.genomic_organization) == 0
						|| viewTypeSent.compareTo(EnumResultViewTypes.homolog_table) == 0){
					if(q){
						GenoOrgaAndHomoTablViewImpl.detailledQElementInfo
						.setHTML("<ul><li><b>Error</b></li></ul>");
					}else{
						GenoOrgaAndHomoTablViewImpl.detailledSElementInfo
						.setHTML("<ul><li><b>Error</b></li></ul>");
					}
				}else if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
					AnnotCompaViewImpl.detailledElementInfo.setHTML("<ul><li><b>ERROR getDetailledElementInfo</b></li></ul>");
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog getDetailledElementInfo : unrecognized viewTypeSent = "+viewTypeSent));
				}
				

			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(String stringReturned) {
				
				if(viewTypeSent.compareTo(EnumResultViewTypes.genomic_organization) == 0
						|| viewTypeSent.compareTo(EnumResultViewTypes.homolog_table) == 0){
					if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){

						//check if selection has changed
						if(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId() != null
								&& currSelectedSyntenyCanvasItemRefId  != null){
							if(currSelectedSyntenyCanvasItemRefId.compareTo(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId())==0){
								//ok selection has not changed
								if(q){
									GenoOrgaAndHomoTablViewImpl.detailledQElementInfo
									.setHTML(stringReturned);
								}else{
									GenoOrgaAndHomoTablViewImpl.detailledSElementInfo
									.setHTML(stringReturned);
								}
							}
						}
					}
				}else if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
					AnnotCompaViewImpl.detailledElementInfo.setHTML(stringReturned);
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog getDetailledElementInfo : unrecognized viewTypeSent = "+viewTypeSent));
				}
				
				
				
			}
		};

		try {
			
			if(viewTypeSent.compareTo(EnumResultViewTypes.genomic_organization) == 0
					|| viewTypeSent.compareTo(EnumResultViewTypes.homolog_table) == 0){
				callForInfoDBService
						.getDetailledElementInfoAsStringWithOrigamiElementId(origamiElementId,
								callback);
			}else if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
				callForInfoDBService.getDetailledElementInfoAsStringWithGeneId(origamiElementId,
						callback);
			}else{
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog getDetailledElementInfo : unrecognized viewTypeSent = "+viewTypeSent));
			}
			
			
		} catch (Exception e) {
			
			// do some UI stuff to show failure
			if(viewTypeSent.compareTo(EnumResultViewTypes.genomic_organization) == 0
					|| viewTypeSent.compareTo(EnumResultViewTypes.homolog_table) == 0){
				if(q){
					GenoOrgaAndHomoTablViewImpl.detailledQElementInfo
					.setHTML("<ul><li><b>Error</b></li></ul>");
				}else{
					GenoOrgaAndHomoTablViewImpl.detailledSElementInfo
					.setHTML("<ul><li><b>Error</b></li></ul>");
				}
			}else if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
				AnnotCompaViewImpl.detailledElementInfo.setHTML("<ul><li><b>Error</b></li></ul>");
			}else{
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingDialog getDetailledElementInfo : unrecognized viewTypeSent = "+viewTypeSent));
			}
			//String mssgError = "ERROR try in getDetailledElementInfo: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
		}
		
	}

	public void getDetailledSyntenyInfo(final String currSelectedSyntenyCanvasItemRefId,
			final long origamiAlignmentId, final boolean showStack,
			final EnumResultViewTypes viewTypeSent) {

		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				//check if selection has changed
				if(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId() != null
						&& currSelectedSyntenyCanvasItemRefId  != null){
					if(currSelectedSyntenyCanvasItemRefId.compareTo(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId())==0){
						//ok selection has not changed
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
						GenoOrgaAndHomoTablViewImpl.detailledSyntenyInfo
								.setHTML("<ul><li><b>ERROR detailledSyntenyInfo</b></li></ul>");
					}
				}
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */

			public void onSuccess(String stringReturned) {

				if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){
				
					//check if selection has changed
					if(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId() != null
							&& currSelectedSyntenyCanvasItemRefId  != null){
						if(currSelectedSyntenyCanvasItemRefId.compareTo(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId())==0){
							//ok selection has not changed
							boolean singleton = false;
							if(stringReturned.contains("<big><i>Orthologs : </i></big>1<br/>")
									&& stringReturned.contains("<big><i>Homologs : </i></big>0<br/>")
									){
								singleton = true;
							}
							if(stringReturned.contains("<big><i>Orthologs : </i></big>0<br/>")
									&& stringReturned.contains("<big><i>Homologs : </i></big>1<br/>")
									){
								singleton = true;
							}
							if(singleton){
								//GenomicOrgaAndHomoTableViewImpl.discPSyntenyInfo.setHeader(new HTML("kjhgkhgkhg"));
								GenoOrgaAndHomoTablViewImpl.discPSyntenyInfo.setVisible(false);
								String currSymbolInfo = GenoOrgaAndHomoTablViewImpl.symbolInfo.getHTML();
								//System.err.println(currSymbolInfo);
								String newSymbolInfo = currSymbolInfo.replace("<big><i>Background colored according to : </i></big>synteny", "<big><i>Background colored according to : </i></big>Gene homology not in synteny");
								GenoOrgaAndHomoTablViewImpl.symbolInfo.setHTML(newSymbolInfo);
							}else{
								GenoOrgaAndHomoTablViewImpl.detailledSyntenyInfo.setHTML(stringReturned);
							}
							
						}
					}
					
				}
				
			}
		};

		try {
			callForInfoDBService.getDetailledSyntenyInfoAsStringForHTMLWithAlignmentId(
					origamiAlignmentId, callback);
		} catch (Exception e) {
			//String mssgError = "ERROR in getDetailledSyntenyInfo: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			GenoOrgaAndHomoTablViewImpl.detailledSyntenyInfo
					.setHTML("<ul><li><b>ERROR detailledSyntenyInfo</b></li></ul>");
		}

		
	}

	public void getDetailledGeneInfoWithSingletonAlignmentId(final String currSelectedSyntenyCanvasItemRefId,
			final Long alignmentId, final boolean showStack,
			final EnumResultViewTypes viewTypeSent, final boolean referenceGene) {
		
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				if(viewTypeSent.compareTo(EnumResultViewTypes.genomic_organization) == 0
						|| viewTypeSent.compareTo(EnumResultViewTypes.homolog_table) == 0){
					//check if selection has changed
					if(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId() != null
							&& currSelectedSyntenyCanvasItemRefId  != null){
						if(currSelectedSyntenyCanvasItemRefId.compareTo(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId())==0){
							//ok selection has not changed
							if(referenceGene){
								GenoOrgaAndHomoTablViewImpl.detailledGeneInfoRef.setHTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
							}else{
								GenoOrgaAndHomoTablViewImpl.detailledGeneInfoComp.setHTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
							}
						}
					}
				}else if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
					AnnotCompaViewImpl.vpInfoGene.clear();
					AnnotCompaViewImpl.vpInfoGene.add(new HTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>"));
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in ResultLoadingDialog getDetailledGeneInfoWithSingletonAlignmentId : unrecognized viewTypeSent = "+viewTypeSent));
				}
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */

			public void onSuccess(ArrayList<String> alStringReturned) {
				if(viewTypeSent.compareTo(EnumResultViewTypes.genomic_organization) == 0
						|| viewTypeSent.compareTo(EnumResultViewTypes.homolog_table) == 0){
					if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){
						//check if selection has changed
						if(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId() != null
								&& currSelectedSyntenyCanvasItemRefId  != null){
							if(currSelectedSyntenyCanvasItemRefId.compareTo(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId())==0){
								//ok selection has not changed
								if(referenceGene){
									GenoOrgaAndHomoTablViewImpl.detailledGeneInfoRef.setHTML("");
								}else{
									GenoOrgaAndHomoTablViewImpl.detailledGeneInfoComp.setHTML("");
								}
								String infoGene = "<big>";
								boolean infoGeneNoEmpty = false;
								for (String stringReturnedIT : alStringReturned) {
										infoGeneNoEmpty = true;
										infoGene += stringReturnedIT + "<br/>";
								}
								infoGene += "</big>";
								if(infoGeneNoEmpty){
									if(referenceGene){
										GenoOrgaAndHomoTablViewImpl.basicGeneInfoRef.setHTML("");
										GenoOrgaAndHomoTablViewImpl.detailledGeneInfoRef.setHTML(infoGene);
									}else{
										GenoOrgaAndHomoTablViewImpl.basicGeneInfoComp.setHTML("");
										GenoOrgaAndHomoTablViewImpl.detailledGeneInfoComp.setHTML(infoGene);
									}
								}
							}
						}
					}
				} else if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
					AnnotCompaViewImpl.alStringDetailsRefGeneSelected = alStringReturned;
					AnnotCompaViewImpl.updateDetailsRefGeneSelected();
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in ResultLoadingDialog getDetailledGeneInfoWithSingletonAlignmentId : unrecognized viewTypeSent = "+viewTypeSent));
				}
			}
		};

		try {
			callForInfoDBService.getDetailledGeneInfoAsListStringForHTMLWithSingletonAlignmentId(
					alignmentId, referenceGene, callback);
		} catch (Exception e) {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			if(referenceGene){
				GenoOrgaAndHomoTablViewImpl.detailledGeneInfoRef.setHTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
			}else{
				GenoOrgaAndHomoTablViewImpl.detailledGeneInfoComp.setHTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
			}
		}
	}
	
	
	public void getDetailledGeneInfo(final String currSelectedSyntenyCanvasItemRefId,
			final int origamiGeneId, final boolean showStack,
			final EnumResultViewTypes viewTypeSent, final boolean referenceGeneOrNot) {

		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				if(viewTypeSent.compareTo(EnumResultViewTypes.genomic_organization) == 0
						|| viewTypeSent.compareTo(EnumResultViewTypes.homolog_table) == 0){
					//check if selection has changed
					if(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId() != null
							&& currSelectedSyntenyCanvasItemRefId  != null){
						if(currSelectedSyntenyCanvasItemRefId.compareTo(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId())==0){
							//ok selection has not changed
							if(referenceGeneOrNot){
								GenoOrgaAndHomoTablViewImpl.detailledGeneInfoRef.setHTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
							}else{
								GenoOrgaAndHomoTablViewImpl.detailledGeneInfoComp.setHTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
							}
						}
					}
				}else if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
					AnnotCompaViewImpl.vpInfoGene.clear();
					AnnotCompaViewImpl.vpInfoGene.add(new HTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>"));
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in ResultLoadingDialog getDetailledGeneInfo : unrecognized viewTypeSent = "+viewTypeSent));
				}
				

			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */

			public void onSuccess(ArrayList<String> alStringReturned) {
				if(viewTypeSent.compareTo(EnumResultViewTypes.genomic_organization) == 0
						|| viewTypeSent.compareTo(EnumResultViewTypes.homolog_table) == 0){
					if(viewTypeSent.compareTo(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght())==0){
						//check if selection has changed
						if(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId() != null
								&& currSelectedSyntenyCanvasItemRefId  != null){
							if(currSelectedSyntenyCanvasItemRefId.compareTo(Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId())==0){
								//ok selection has not changed
								if(referenceGeneOrNot){
									GenoOrgaAndHomoTablViewImpl.detailledGeneInfoRef.setHTML("");
								}else{
									GenoOrgaAndHomoTablViewImpl.detailledGeneInfoComp.setHTML("");
								}
								String infoGene = "<big>";
								boolean infoGeneNoEmpty = false;
								for (String stringReturnedIT : alStringReturned) {
										infoGeneNoEmpty = true;
										infoGene += stringReturnedIT + "<br/>";
								}
								infoGene += "</big>";
								infoGene += SessionStorageControler
										.getInfoSpecialScoreForGeneIfRelevantAndAvailable(
												origamiGeneId,
												false,
												true);
								if(infoGeneNoEmpty){
									if(referenceGeneOrNot){
										GenoOrgaAndHomoTablViewImpl.basicGeneInfoRef.setHTML("");
										GenoOrgaAndHomoTablViewImpl.detailledGeneInfoRef.setHTML(infoGene);
									}else{
										GenoOrgaAndHomoTablViewImpl.basicGeneInfoComp.setHTML("");
										GenoOrgaAndHomoTablViewImpl.detailledGeneInfoComp.setHTML(infoGene);
									}
								}
							}
						}
					}
				} else if (viewTypeSent.compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
					AnnotCompaViewImpl.alStringDetailsRefGeneSelected = alStringReturned;
					String infoSpecialScore = SessionStorageControler
													.getInfoSpecialScoreForGeneIfRelevantAndAvailable(
															origamiGeneId,
															false,
															false);
					if(infoSpecialScore != null && ! infoSpecialScore.isEmpty()){
						AnnotCompaViewImpl.alStringDetailsRefGeneSelected.add(infoSpecialScore);
					}
					AnnotCompaViewImpl.updateDetailsRefGeneSelected();
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in ResultLoadingDialog getDetailledGeneInfo : unrecognized viewTypeSent = "+viewTypeSent));
				}
				
			}
		};

		try {
			callForInfoDBService.getDetailledGeneInfoAsListStringForHTMLWithGeneId(
					origamiGeneId, callback);
		} catch (Exception e) {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			if(referenceGeneOrNot){
				GenoOrgaAndHomoTablViewImpl.detailledGeneInfoRef.setHTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
			}else{
				GenoOrgaAndHomoTablViewImpl.detailledGeneInfoComp.setHTML("<ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
			}
		}
	}


	//other Methods

	protected boolean doUpdatePartialArrayToo(
			double percentSyntenyShowStartQGenome,
			double percentSyntenyShowStopQGenome,
			double percentSyntenyShowStartSGenome,
			double percentSyntenyShowStopSGenome,
			int indexOfGenomePanelInLIST_GENOME_PANEL) {
		
		if(percentSyntenyShowStartQGenome != 0
				|| percentSyntenyShowStopQGenome != 1
				|| percentSyntenyShowStartSGenome != 0
				|| percentSyntenyShowStopSGenome != 1){
			if(percentSyntenyShowStartQGenome == Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getPercentSyntenyShowStartQGenome()
					&& percentSyntenyShowStopQGenome == Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getPercentSyntenyShowStopQGenome()
					&& percentSyntenyShowStartSGenome == Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getPercentSyntenyShowStartSGenome()
					&& percentSyntenyShowStopSGenome == Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getPercentSyntenyShowStopSGenome()){
				//ok partial has not changed for this stuff
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
		
	}


	protected boolean doUpdateDisplay(double percentSyntenyShowStartQGenome,
			double percentSyntenyShowStopQGenome,
			double percentSyntenyShowStartSGenome,
			double percentSyntenyShowStopSGenome,
			int indexStartInResultNavigationDisplay,
			int indexStartInSyntenyCanvasNavigationDisplay,
			int indexOfGenomePanelInLIST_GENOME_PANEL) {
		
		//check if navigation has changed
		//System.err.println("start check : "+indexStartInResultNavigationDisplay+" ; "+CenterSLPAllGenomePanels.START_INDEX);
		if(indexStartInResultNavigationDisplay == CenterSLPAllGenomePanels.START_INDEX && Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).isVisible()){
			if(indexStartInSyntenyCanvasNavigationDisplay == Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()){
				if(percentSyntenyShowStartQGenome == Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getPercentSyntenyShowStartQGenome()
						&& percentSyntenyShowStopQGenome == Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getPercentSyntenyShowStopQGenome()
						&& percentSyntenyShowStartSGenome == Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getPercentSyntenyShowStartSGenome()
						&& percentSyntenyShowStopSGenome == Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexOfGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getPercentSyntenyShowStopSGenome()){
					//ok display has not changed for this stuff
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public void linkToVisuAlignment(int qGeneIdForVisuAlignment, int sGeneIdForVisuAlignment){
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			public void onSuccess(ArrayList<String> alStringReturned) {
				if(alStringReturned != null){
					if(alStringReturned.size() < 2){
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in linkToVisuAlignment : alStringReturned.size() < 2 : "+alStringReturned.size()));
					} else {
						String qIdentifiersForVisuAlignment = alStringReturned.get(0);
						String sIdentifiersForVisuAlignment = alStringReturned.get(1);
						String buildUrl = "http://blast.ncbi.nlm.nih.gov/BlastAlign.cgi?ALIGNMENT_VIEW=Pairwise&DATABASE=n/a&FORMAT_OBJECT=Alignment&FORMAT_TYPE=HTML&MATRIX_NAME=BLOSUM62&NEW_VIEW=true&PROGRAM=blastp&SHOW_LINKOUT=true&SHOW_OVERVIEW=true&CMD=Put&NCBI_GI=true"
								+ "&QUERY=" + qIdentifiersForVisuAlignment
								+ "&SUBJECTS=" + sIdentifiersForVisuAlignment
								;
						Window.open(buildUrl, "_blank", "");
//						http://blast.ncbi.nlm.nih.gov/BlastAlign.cgi?ALIGNMENT_VIEW=Pairwise&DATABASE=n/a&FORMAT_OBJECT=Alignment&FORMAT_TYPE=HTML&MATRIX_NAME=BLOSUM62&NEW_VIEW=true&PROGRAM=blastp&SHOW_LINKOUT=true&SHOW_OVERVIEW=true&CMD=Put&NCBI_GI=true&QUERY=49240383&SUBJECTS=13621326
//							with &QUERY= and &SUBJECTS=
//							can be
//							 - gi number from qualifier /db_xref="GI:XXXXXX" ; without the GI:, par exemple 13621326 pour /db_xref="GI:13621326", 13621327 pour /db_xref="GI:13621327"
//							 - protein accnum via the protein_id qualifiers, par exemple /protein_id="AAK33146.1" et /protein_id="AAK33147.1"
//							 - protein sequence, par exemple :
//							MTENEQIFWNRVLELAQSQLKQATYEFFVHDARLLKVKNKIK..................
//							et
//							MIQFSINRTLFIHALNTTKRAISTKQHQVVLTSGKSEITLK..............
					}
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in linkToVisuAlignment : alStringReturned == null : "));
				}
			}
		};
		try {
			callForInfoDBService.getIdentifiersForVisuAlignmentWithQAndSGeneId(
					qGeneIdForVisuAlignment, sGeneIdForVisuAlignment, callback);
		} catch (Exception e) {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}
	}


	public void showStatSummaryRefCDS(
			HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneName
			, HashSet<Integer> hsOrgaIdsFeaturedGenomes
			//, HashSet<Integer> hsOrgaIdsMainList
			) {
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.INSYGHT_LOADING_MASK.hide();
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			public void onSuccess(String stringReturned) {
				Insyght.INSYGHT_LOADING_MASK.hide();
				if(stringReturned != null){
					Insyght.POP_UP_DISPLAY_GENERAL_INFORMATION.setDisplayedTitle("Summary conservation statistics for the selected reference CDS");
					Insyght.POP_UP_DISPLAY_GENERAL_INFORMATION.setDisplayedMainText(stringReturned);
					Insyght.POP_UP_DISPLAY_GENERAL_INFORMATION.displayPoPup();
					//Window.alert(stringReturned);
					
				} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in linkToVisuAlignment : stringReturned == null : "));
				}
			}
		};
		try {
			Insyght.INSYGHT_LOADING_MASK.show();
			Insyght.INSYGHT_LOADING_MASK.center();
			callForInfoDBService.getStatSummaryRefCDS(
					qOrganismId2qGeneId2qGeneName
					, hsOrgaIdsFeaturedGenomes
					//, hsOrgaIdsMainList
					, callback);
		} catch (Exception e) {
			Insyght.INSYGHT_LOADING_MASK.hide();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}
	}
	

}
