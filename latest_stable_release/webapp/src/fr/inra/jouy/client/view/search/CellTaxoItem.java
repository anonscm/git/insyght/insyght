package fr.inra.jouy.client.view.search;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;

import fr.inra.jouy.shared.pojos.databaseMapping.TaxoItem;

public class CellTaxoItem extends AbstractCell<TaxoItem> {

	public CellTaxoItem() {
	}

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context, TaxoItem value, SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (value == null) {
			return;
		} else {
			//sb.appendHtmlConstant("<table>");
			//sb.appendHtmlConstant("<tr><td style='font-size:95%;'>");
			//sb.appendEscaped(value.getFullName());
			//sb.appendHtmlConstant("</td></tr>");
			//sb.appendHtmlConstant("</table>");

			if (value != null) {
				String postFix = "";
				if (value.getName().compareTo("Unknown lineage") == 0) {
					postFix += " (" + value.getTaxoChildrens().size() + ")";
				} else if (!value.getTaxoChildrens().isEmpty()) {
					postFix += " (" + value.getSumOfLeafsUnderneath() + ")";
				}
				if (context.getIndex() == 0
						&& isCellBrowserHeader(value)
						) {

					String htmlSt = "<span class=\"cellBrowserHeader\"><center>"
							+ value.getFullName()
							+ postFix
							+ "</center></span>";
					sb.append(SafeHtmlUtils.fromTrustedString(htmlSt));
				} else {

					if(value.equals(SearchViewImpl.selectedRefTaxoNode)){
						//normal black bold
						sb.append(SafeHtmlUtils.fromTrustedString("<b><big><center>Reference taxonomic node : " + value.getFullName()
						+ postFix + "</big></center></b>"));
					}else if(SearchViewImpl.doColorCellTaxoItemAccordingToGroupPlusMinus){ //geneSetBuildMode == 0
						if(value.isRefGenomeInRefNodeContext()){
							//background-color:#FFF380;color:#000000;	
							sb.append(SafeHtmlUtils.fromTrustedString("<span style=\"background-color:#FFF380;color:#000000;\">" + value.getFullName()
							+ postFix + "</span>"));
						}else if(value.isAssertPresenceInRefNodeContext()){
							//background-color:#ECF1EF;color:#00008B;
							sb.append(SafeHtmlUtils.fromTrustedString("<span style=\"background-color:#BCD2EE;color:#00008B;\">" + value.getFullName()
							+ postFix + "</span>"));
						}else if(value.isAssertAbsenceInRefNodeContext()){
							//background-color:#FEE0C6;color:#CD0000;
							sb.append(SafeHtmlUtils.fromTrustedString("<span style=\"background-color:#FFA07A;color:#8B0000;\">" + value.getFullName()
							+ postFix + "</span>"));
						}else if(value.isAssertEitherPresenceOrAbsenceInRefNodeContext()){
							//background-color:#F8F8FF;color:#8B8378;
							sb.append(SafeHtmlUtils.fromTrustedString(
									"<span style=\"background-color:#ffe6eb;color:#8B0A50;\">" + 
											value.getFullName()
									+ postFix 
									+ "</span>"
									));
						}else{
							//normal black
							sb.append(SafeHtmlUtils.fromTrustedString(value.getFullName()
									+ postFix));
						}
					}else{
						if(value.isRefGenomeInRefNodeContext()){
							//background-color:#FFF380;color:#000000;	
							sb.append(SafeHtmlUtils.fromTrustedString("<span style=\"background-color:#FFF380;color:#000000;\">" + value.getFullName()
							+ postFix + "</span>"));
						}else{
							//normal black
							sb.append(SafeHtmlUtils.fromTrustedString(value.getFullName()
									+ postFix));
						}
					}
				}
			}
		}
	}

	public static boolean isCellBrowserHeader(TaxoItem tiSent) {
		if (tiSent.getName().compareTo("Domain") == 0
				|| tiSent.getName().compareTo("Phylum") == 0
				|| tiSent.getName().compareTo("Class") == 0
				|| tiSent.getName().compareTo("Order") == 0
				|| tiSent.getName().compareTo("Family") == 0
				|| tiSent.getName().compareTo("Genus") == 0
				|| tiSent.getName().compareTo("Species") == 0) {
			return true;
		}
		return false;
	}

}
