package fr.inra.jouy.client.view.result;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.inra.jouy.client.AppController;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.RPC.CallForHomoBrowResu;
import fr.inra.jouy.client.RPC.CallForHomoBrowResuAsync;
import fr.inra.jouy.shared.pojos.applicationItems.GeneWidgetStyleItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.TransAlignmPairs;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.SuperHoldAbsoPropItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem.QSEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem.QEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem.SEnumBlockType;
import fr.inra.jouy.shared.TransStartStopGeneInfo;

public class ResuLoadMoreAbsoPropItemDial {

//	private static ResultLoadingMoreAbsoluteProportionItemDialogUiBinder uiBinder = GWT
//			.create(ResultLoadingMoreAbsoluteProportionItemDialogUiBinder.class);

//	interface ResultLoadingMoreAbsoluteProportionItemDialogUiBinder extends
//			UiBinder<Widget, ResuLoadMoreAbsoPropItemDial> {
//	}

	private final CallForHomoBrowResuAsync homologieBrowsingResultService = (CallForHomoBrowResuAsync) GWT
			.create(CallForHomoBrowResu.class);
	
	
	private boolean waitingForFillingTmpListAPQSGeneHomoSortedByQ = false;
	private boolean waitingForFillingTmpListAPQSGeneHomoSortedByS = false;
	private boolean waitingForFillingTmpListAPQGeneInserItem = false;
	private boolean waitingForFillingTmpListAPSGeneInserItem = false;
	
//	public enum TypeOfRegion {
//		SYNTENY_REGION, Q_REGION, S_REGION
//	}

	public ResuLoadMoreAbsoPropItemDial(){
	}
			
	
//	public ResuLoadMoreAbsoPropItemDial(
//			TypeOfRegion typeOfRegion,
//			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
//			final long mainOrigamiSyntenyId, 
//			final ArrayList<Long> listOrigamiAlignmentIdsContainedOtherSyntenies,
//			final ArrayList<Long> listOrigamiAlignmentIdsMotherSyntenies,
//			final ArrayList<Long> listOrigamiAlignmentIdsSprungOffSyntenies,
//			//final String qAccnum, final int qPbStartOfElementInOrga, final int qSizeOfOragnismInPb,
//			//final String sAccnum, final int sPbStartOfElementInOrga, final int sSizeOfOragnismInPb,
//			final EnumResultViewTypes viewTypeSent,
//			final boolean isContainedWithinAnotherMotherSynteny, final boolean isMotherOfContainedOtherSyntenies,
//			final boolean isMotherOfSprungOffSyntenies, final boolean isSprungOffAnotherMotherSynteny,
//			final int idxToDisplayInListHAPI) {
//		setWidget(uiBinder.createAndBindUi(this));		
//		if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
//				.isGpiLoadingAdditionalData()){
//			Window.alert("Already Loading data for this comparison, please wait");
//		}else{
//			if (typeOfRegion.compareTo(TypeOfRegion.SYNTENY_REGION) == 0) {
//				getArrayListAbsoPropGenesSyntenyRegion(indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL,
//						mainOrigamiSyntenyId, 
//						listOrigamiAlignmentIdsContainedOtherSyntenies,
//						listOrigamiAlignmentIdsMotherSyntenies,
//						listOrigamiAlignmentIdsSprungOffSyntenies,
//						//qAccnum,
//						//qPbStartOfElementInOrga, qSizeOfOragnismInPb,
//						//sAccnum,
//						//sPbStartOfElementInOrga, sSizeOfOragnismInPb,
//						viewTypeSent,
//						isContainedWithinAnotherMotherSynteny, isMotherOfContainedOtherSyntenies,
//						isMotherOfSprungOffSyntenies, isSprungOffAnotherMotherSynteny,
//						idxToDisplayInListHAPI);
//			}else{
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error ResultLoadingMoreAbsoluteProportionItemDialog SYNTENY_REGION : unrecognized type of ResultLoadingMoreAbsoluteProportionItemDialog = "+typeOfRegion.toString()));
//			}
//		}
//		
//	}
//
//	public ResuLoadMoreAbsoPropItemDial(
//			TypeOfRegion typeOfRegion,
//			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
//			final int origamiElementId, final int startPb, final int stopPb, final String qAccnum, final int qPbStartOfElementInOrga, final int qSizeOfOragnismInPb,
//			final String sAccnum, final int sPbStartOfElementInOrga, final int sSizeOfOragnismInPb,
//			final EnumResultViewTypes viewTypeSent,
//			final boolean previousSOfNextSlice,
//			final boolean nextSOfPreviousSlice) {
//		setWidget(uiBinder.createAndBindUi(this));
//		//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
//		if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
//				.isGpiLoadingAdditionalData()){
//			Window.alert("Already Loading data for this comparison, please wait");
//		}else{
//			if(typeOfRegion.compareTo(TypeOfRegion.Q_REGION) == 0){
//				getArrayListAbsoPropGenesQRegion(indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL
//						,origamiElementId, startPb, stopPb,
//						qAccnum, qPbStartOfElementInOrga, qSizeOfOragnismInPb,
//						viewTypeSent);
//			}else if(typeOfRegion.compareTo(TypeOfRegion.S_REGION) == 0){
//				getArrayListAbsoPropGenesSRegion(indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL
//						,origamiElementId, startPb, stopPb,
//						sAccnum, sPbStartOfElementInOrga, sSizeOfOragnismInPb,
//						viewTypeSent,
//						previousSOfNextSlice, nextSOfPreviousSlice);
//			}else{
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error ResultLoadingMoreAbsoluteProportionItemDialog Q_REGION S_REGION : unrecognized type of ResultLoadingMoreAbsoluteProportionItemDialog Bis = "+typeOfRegion.toString()));
//			}
//		}
//	}

	
	public void getArrayListAbsoPropGenesSRegion(
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
			final int origamiElementId, final int startPb, final int stopPb, final String sAccnum,
			final int sPbStartOfElementInOrga, final int sSizeOfOragnismInPb,
			final EnumResultViewTypes viewTypeSent,
			final boolean previousSOfNextSlice,
			final boolean nextSOfPreviousSlice) {

		AsyncCallback<ArrayList<TransStartStopGeneInfo>> callback = new AsyncCallback<ArrayList<TransStartStopGeneInfo>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				AppController.backFromCallBackCheckingOfLoadingMask();
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<TransStartStopGeneInfo> hirs) {
				
				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){

					ArrayList<AbsoPropSGeneInserItem> listAbsoluteProportionSGeneInsertionItem = new ArrayList<AbsoPropSGeneInserItem>();
					
					//GeneWidgetStyleItem gwst = new GeneWidgetStyleItem();
					//gwst.setBkgColor(CommonMethodsViewResults.getNextAvailableBkgColor(hirs.size()));
					
					for(int i=0;i<hirs.size();i++){
						TransStartStopGeneInfo tssgiIT = hirs.get(i);
					
						AbsoPropSGeneInserItem newAPSGII = new AbsoPropSGeneInserItem();
						
						newAPSGII.setsGeneId(tssgiIT.getOrigamiGeneId());
						//newAPSGII.setsStyleItem(gwst);
						
						//basic info, tmp
						newAPSGII.setSEnumBlockType(SEnumBlockType.S_INSERTION);
						
						//fill new data
						newAPSGII.setsPbStartGeneInElement(tssgiIT.getStart());
						newAPSGII.setsPbStopGeneInElement(tssgiIT.getStop());

						//then compute qPercentstart et qPercentstop et sPercentstart et sPercentstop for each gene homology
						newAPSGII.setsPercentStart(((double)sPbStartOfElementInOrga+(double)newAPSGII.getsPbStartGeneInElement())/(double)sSizeOfOragnismInPb);
						newAPSGII.setsPercentStop(((double)sPbStartOfElementInOrga+(double)newAPSGII.getsPbStopGeneInElement())/(double)sSizeOfOragnismInPb);
		
						//set other vital info
						newAPSGII.setsOrigamiElementId(tssgiIT.getOrigamiElementId());
						newAPSGII.setsAccnum(sAccnum);
						newAPSGII.setsPbStartOfElementInOrga(sPbStartOfElementInOrga);
						newAPSGII.setsSizeOfOragnismInPb(sSizeOfOragnismInPb);
						
						listAbsoluteProportionSGeneInsertionItem.add(newAPSGII);

					}

					CommonMethodsViewResults cmvrIT = new CommonMethodsViewResults();
					
					ArrayList<SuperHoldAbsoPropItem> newAlAlHAPI = new ArrayList<SuperHoldAbsoPropItem>();
					Collections.sort(listAbsoluteProportionSGeneInsertionItem);
					
					//cmvrIT.insertCorrectlyAbsoluteProportionQAndSItemsIntoListDisplayedItem(null, listAbsoluteProportionSGeneInsertionItem, newAlAlHAPI);
					cmvrIT.buildCorrectlyListSuperHolderFromAbsoluteProportionSGeneInsertionItemList(listAbsoluteProportionSGeneInsertionItem, newAlAlHAPI);
					
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex(newAlAlHAPI, indexInFullAssociatedListAbsoluteProportionItemToDisplay,
							previousSOfNextSlice,
							nextSOfPreviousSlice);
					//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.setIndexStartBirdSynteny(0);
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawViewToCanvas(1.0, 0, true, true, false, false);

				}
				
				
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
//				Insyght.CANVAS_MENU_POPUP.hide();
				
			}
		};
		
		try {
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(true);
			
			homologieBrowsingResultService
					.getListTransientStartStopGeneInfoWithOrigamiElementIdAndStartPbAndStopPb(
							origamiElementId, startPb, stopPb,
							callback);
			
			AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
			
		} catch (Exception e) {
			//String mssgError = "ERROR try in getArrayListAbsoPropGenesSRegion: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
		}
		
	}
	
	public void getArrayListAbsoPropGenesQRegion(
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
			final int origamiElementId, final int startPb, final int stopPb,
			final String qAccnum, final int qPbStartOfElementInOrga, final int qSizeOfOragnismInPb,
			final EnumResultViewTypes viewTypeSent) {

		AsyncCallback<ArrayList<TransStartStopGeneInfo>> callback = new AsyncCallback<ArrayList<TransStartStopGeneInfo>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				AppController.backFromCallBackCheckingOfLoadingMask();
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<TransStartStopGeneInfo> hirs) {
				

				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){

					ArrayList<AbsoPropQGeneInserItem> listAbsoluteProportionQGeneInsertionItem = new ArrayList<AbsoPropQGeneInserItem>();
					
					//GeneWidgetStyleItem gwst = new GeneWidgetStyleItem();
					//gwst.setBkgColor(CommonMethodsViewResults.getNextAvailableBkgColor(hirs.size()));
					
					for(int i=0;i<hirs.size();i++){
						TransStartStopGeneInfo tssgiIT = hirs.get(i);
						
						AbsoPropQGeneInserItem newAPQGII = new AbsoPropQGeneInserItem();
						
						newAPQGII.setQGeneId(tssgiIT.getOrigamiGeneId());
						//newAPQGII.setqStyleItem(gwst);
						
						//basic info, tmp
						newAPQGII.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
						
						//fill new data
						newAPQGII.setqPbStartGeneInElement(tssgiIT.getStart());
						newAPQGII.setqPbStopGeneInElement(tssgiIT.getStop());

						//then compute qPercentstart et qPercentstop et sPercentstart et sPercentstop for each gene homology
						newAPQGII.setqPercentStart(((double)qPbStartOfElementInOrga+(double)newAPQGII.getqPbStartGeneInElement())/(double)qSizeOfOragnismInPb);
						newAPQGII.setqPercentStop(((double)qPbStartOfElementInOrga+(double)newAPQGII.getqPbStopGeneInElement())/(double)qSizeOfOragnismInPb);
		
						//set other vital info
						newAPQGII.setqOrigamiElementId(tssgiIT.getOrigamiElementId());
						newAPQGII.setqAccnum(qAccnum);
						newAPQGII.setqPbStartOfElementInOrga(qPbStartOfElementInOrga);
						newAPQGII.setQsizeOfOragnismInPb(qSizeOfOragnismInPb);
						
						listAbsoluteProportionQGeneInsertionItem.add(newAPQGII);

					}

					CommonMethodsViewResults cmvrIT = new CommonMethodsViewResults();
					
					ArrayList<SuperHoldAbsoPropItem> newAlAlHAPI = new ArrayList<SuperHoldAbsoPropItem>();
					Collections.sort(listAbsoluteProportionQGeneInsertionItem);
					
					//cmvrIT.insertCorrectlyAbsoluteProportionQAndSItemsIntoListDisplayedItem(listAbsoluteProportionQGeneInsertionItem, null, newAlAlHAPI);
					cmvrIT.buildCorrectlyListSuperHolderFromAbsoluteProportionQGeneInsertionItemList(listAbsoluteProportionQGeneInsertionItem, newAlAlHAPI);
					
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().insertGenesAtSpecifiedQGenomicRegionInsertionItemIndex(newAlAlHAPI, indexInFullAssociatedListAbsoluteProportionItemToDisplay);
					//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.setIndexStartBirdSynteny(0);
					
					//System.out.println("inserted q genes with succes");
					
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawViewToCanvas(1.0, 0, true, true, false, false);

				}
				
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
//				Insyght.CANVAS_MENU_POPUP.hide();
				
			}
		};
		
		try {
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(true);
			
			//System.out.println("launching getListTransientStartStopGeneInfoWithOrigamiElementIdAndStartPbAndStopPb");
			
			homologieBrowsingResultService
					.getListTransientStartStopGeneInfoWithOrigamiElementIdAndStartPbAndStopPb(
							origamiElementId, startPb, stopPb,
							callback);
			
			AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
			
		} catch (Exception e) {
			//String mssgError = "ERROR try in getArrayListAbsoPropGenesQRegion: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
		}
		
	
	}

	
	
	public void getArrayListAbsoPropGenesSyntenyRegion(
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
			final long mainOrigamiSyntenyId, 
			final ArrayList<Long> listOrigamiAlignmentIdsContainedOtherSyntenies,
			final ArrayList<Long> listOrigamiAlignmentIdsMotherSyntenies,
			final ArrayList<Long> listOrigamiAlignmentIdsSprungOffSyntenies,
			//final String qAccnum, final int qPbStartOfElementInOrga, final int qSizeOfOragnismInPb,
			//final String sAccnum, final int sPbStartOfElementInOrga, final int sSizeOfOragnismInPb,
			final EnumResultViewTypes viewTypeSent,
			final boolean isContainedWithinAnotherMotherSynteny, final boolean isMotherOfContainedOtherSyntenies,
			final boolean isMotherOfSprungOffSyntenies, final boolean isSprungOffAnotherMotherSynteny,
			final int idxToDisplayInListHAPI) {

		AsyncCallback<ArrayList<TransAlignmPairs>> callback = new AsyncCallback<ArrayList<TransAlignmPairs>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<TransAlignmPairs> hirs) {

				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){

					//construct hash alignmentId2countTAP
					HashMap<Long,Integer> alignmentId2countTAP = new HashMap<Long, Integer>();
					for(int i=0;i<hirs.size();i++){
						TransAlignmPairs tapIT = hirs.get(i);
						if(alignmentId2countTAP.containsKey(tapIT.getAlignmentId())){
							int currContIT = alignmentId2countTAP.get(tapIT.getAlignmentId());
							alignmentId2countTAP.put(tapIT.getAlignmentId(), currContIT+1);
						} else {
							alignmentId2countTAP.put(tapIT.getAlignmentId(), 1);
						}
					}
					
					HashMap<Long,Integer> origamiSyntenyId2Color = new HashMap<Long, Integer>();
					//create color for listOrigamiSyntenyId
					origamiSyntenyId2Color.put(mainOrigamiSyntenyId, CommonMethodsViewResults.getNextAvailableBkgColor(alignmentId2countTAP.get(mainOrigamiSyntenyId)));
					for(int k=0;k<listOrigamiAlignmentIdsContainedOtherSyntenies.size();k++){
						long origamiSyntenyIdIT = listOrigamiAlignmentIdsContainedOtherSyntenies.get(k);
						if (alignmentId2countTAP.containsKey(origamiSyntenyIdIT)) {
							int colorForThisOriAliId = CommonMethodsViewResults.getNextAvailableBkgColor(alignmentId2countTAP.get(origamiSyntenyIdIT));
							origamiSyntenyId2Color.put(origamiSyntenyIdIT, colorForThisOriAliId);
						}
					}
					for(int k=0;k<listOrigamiAlignmentIdsMotherSyntenies.size();k++){
						long origamiSyntenyIdIT = listOrigamiAlignmentIdsMotherSyntenies.get(k);
						if (alignmentId2countTAP.containsKey(origamiSyntenyIdIT)) {
							int colorForThisOriAliId = CommonMethodsViewResults.getNextAvailableBkgColor(alignmentId2countTAP.get(origamiSyntenyIdIT));
							origamiSyntenyId2Color.put(origamiSyntenyIdIT, colorForThisOriAliId);
						}
					}
					for(int k=0;k<listOrigamiAlignmentIdsSprungOffSyntenies.size();k++){
						long origamiSyntenyIdIT = listOrigamiAlignmentIdsSprungOffSyntenies.get(k);
						if (alignmentId2countTAP.containsKey(origamiSyntenyIdIT)) {
							int colorForThisOriAliId = CommonMethodsViewResults.getNextAvailableBkgColor(alignmentId2countTAP.get(origamiSyntenyIdIT));
							origamiSyntenyId2Color.put(origamiSyntenyIdIT, colorForThisOriAliId);
						}
					}
					for(int i=0;i<hirs.size();i++){
						TransAlignmPairs tapIT = hirs.get(i);
						processThisTAP(indexGenomePanelInLIST_GENOME_PANEL, tapIT, origamiSyntenyId2Color);
					}
					

					if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getTmpListAPQSGeneHomo().size() > 0){
						getArrayListTransientFillingStartStopGeneForTmpListAPQSGeneHomoSortedByQ(
								indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL,
								//qAccnum, qPbStartOfElementInOrga, qSizeOfOragnismInPb,
								//sAccnum, sPbStartOfElementInOrga, sSizeOfOragnismInPb,
								viewTypeSent,
								idxToDisplayInListHAPI);
						getArrayListTransientFillingStartStopGeneForTtmpListAPQSGeneHomoSortedByS(
								indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL,
								//qAccnum, qPbStartOfElementInOrga, qSizeOfOragnismInPb,
								//sAccnum, sPbStartOfElementInOrga, sSizeOfOragnismInPb,
								viewTypeSent,
								idxToDisplayInListHAPI);
					}else{
						waitingForFillingTmpListAPQSGeneHomoSortedByQ = false;
						waitingForFillingTmpListAPQSGeneHomoSortedByS = false;
						
					}
					if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getTmpListAPQGeneInserItem().size() > 0){

						getArrayListTransientFillingStartStopGeneForTtmpListAPQGeneInserItem(
								indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL,
								//qAccnum, qPbStartOfElementInOrga, qSizeOfOragnismInPb,
								//sAccnum, sPbStartOfElementInOrga, sSizeOfOragnismInPb,
								viewTypeSent,
								idxToDisplayInListHAPI);
					}else{
						waitingForFillingTmpListAPQGeneInserItem = false;
					}
					if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getTmpListAPSGeneInserItem().size() > 0){

						getArrayListTransientFillingStartStopGeneForTtmpListAPSGeneInserItem(
								indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL,
								//qAccnum, qPbStartOfElementInOrga, qSizeOfOragnismInPb,
								//sAccnum, sPbStartOfElementInOrga, sSizeOfOragnismInPb,
								viewTypeSent,
								idxToDisplayInListHAPI);
					}else{
						waitingForFillingTmpListAPSGeneInserItem = false;
					}
					
				}
				
				
				//System.out.println("size : "+hirs.size());
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				//Insyght.CANVAS_MENU_POPUP.hide();
				
			}

			private void processThisTAP(
					int indexGenomePanelInLIST_GENOME_PANEL,
					TransAlignmPairs tapIT,
					HashMap<Long,Integer> origamiSyntenyId2Color) {
				if(tapIT.getType() == 1 || tapIT.getType() == 2){
					
					AbsoPropQSGeneHomoItem newAPQSGHI = new AbsoPropQSGeneHomoItem();
					newAPQSGHI.setSyntenyOrigamiAlignmentId(tapIT.getAlignmentId());
					newAPQSGHI.setQsQGeneId(tapIT.getqGeneId());
					newAPQSGHI.setQsSGeneId(tapIT.getsGeneId());
					newAPQSGHI.setQsOrigamiAlignmentPairsType(tapIT.getType());
					GeneWidgetStyleItem gwst = new GeneWidgetStyleItem();
					gwst.setBkgColor(origamiSyntenyId2Color.get(tapIT.getAlignmentId()));
					if(tapIT.getType() == 1){
						gwst.setStyleAsOrtholog(true);
					}
					newAPQSGHI.setQsStyleItem(gwst);
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQSGeneHomo().add(newAPQSGHI);
				}else if(tapIT.getType() == 3){
					
					AbsoPropQGeneInserItem newAPQGII = new AbsoPropQGeneInserItem();
					newAPQGII.setqOrigamiAlignmentId(tapIT.getAlignmentId());
					newAPQGII.setQGeneId(tapIT.getqGeneId());
					newAPQGII.setqOrigamiAlignmentPairsType(tapIT.getType());
					GeneWidgetStyleItem gwst = new GeneWidgetStyleItem();
					gwst.setBkgColor(-1);
					newAPQGII.setqStyleItem(gwst);
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQGeneInserItem().add(newAPQGII);
					
					AbsoPropSGeneInserItem newAPSGII = new AbsoPropSGeneInserItem();
					newAPSGII.setsOrigamiAlignmentId(tapIT.getAlignmentId());
					newAPSGII.setsGeneId(tapIT.getsGeneId());
					newAPSGII.setsOrigamiAlignmentPairsType(tapIT.getType());
					GeneWidgetStyleItem gwstDos = new GeneWidgetStyleItem();
					gwstDos.setBkgColor(-1);
					newAPSGII.setsStyleItem(gwstDos);
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPSGeneInserItem().add(newAPSGII);
				}else if(tapIT.getType() == 4){
					
					AbsoPropSGeneInserItem newAPSGII = new AbsoPropSGeneInserItem();
					newAPSGII.setsOrigamiAlignmentId(tapIT.getAlignmentId());
					newAPSGII.setsGeneId(tapIT.getsGeneId());
					newAPSGII.setsOrigamiAlignmentPairsType(tapIT.getType());
					GeneWidgetStyleItem gwst = new GeneWidgetStyleItem();
					gwst.setBkgColor(-1);
					newAPSGII.setsStyleItem(gwst);
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPSGeneInserItem().add(newAPSGII);
				}else if(tapIT.getType() == 5){
					
					AbsoPropQGeneInserItem newAPQGII = new AbsoPropQGeneInserItem();
					newAPQGII.setqOrigamiAlignmentId(tapIT.getAlignmentId());
					newAPQGII.setQGeneId(tapIT.getqGeneId());
					newAPQGII.setqOrigamiAlignmentPairsType(tapIT.getType());
					GeneWidgetStyleItem gwst = new GeneWidgetStyleItem();
					gwst.setBkgColor(-1);
					newAPQGII.setqStyleItem(gwst);
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQGeneInserItem().add(newAPQGII);
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getArrayListAbsoPropGenesSyntenyRegion on success : type not recognized : "+tapIT.getType()));
				}
				
			}
		};

		try {
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(true);
			homologieBrowsingResultService
					.getListTransientAlignmentPairsWithMainSyntenyAndRelated(
							mainOrigamiSyntenyId, 
							listOrigamiAlignmentIdsContainedOtherSyntenies,
							listOrigamiAlignmentIdsMotherSyntenies,
							listOrigamiAlignmentIdsSprungOffSyntenies,
							isContainedWithinAnotherMotherSynteny, isMotherOfContainedOtherSyntenies,
							isMotherOfSprungOffSyntenies, isSprungOffAnotherMotherSynteny,
							callback);
			AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
		} catch (Exception e) {
			//String mssgError = "ERROR try in getArrayListAbsoPropGenesSyntenyRegion: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
		}
		
		
	}
	
	protected void getArrayListTransientFillingStartStopGeneForTtmpListAPSGeneInserItem(
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
			//final String qAccnum, final int qPbStartOfElementInOrga, final int qSizeOfOragnismInPb,
			//final String sAccnum, final int sPbStartOfElementInOrga, final int sSizeOfOragnismInPb,
			final EnumResultViewTypes viewTypeSent,
			final int idxToDisplayInListHAPI) {
		
		String methodNameToReport = "ResuLoadMoreAbsoPropItemDial getArrayListTransientFillingStartStopGeneForTtmpListAPSGeneInserItem"
				+ " ; indexInFullAssociatedListAbsoluteProportionItemToDisplay="+indexInFullAssociatedListAbsoluteProportionItemToDisplay
				+ " ; indexGenomePanelInLIST_GENOME_PANEL="+ indexGenomePanelInLIST_GENOME_PANEL
				+ " ; viewTypeSent="+ viewTypeSent.toString()
				+ " ; idxToDisplayInListHAPI="+ idxToDisplayInListHAPI
				;
		AsyncCallback<ArrayList<TransStartStopGeneInfo>> callback = new AsyncCallback<ArrayList<TransStartStopGeneInfo>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
				//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
				waitingForFillingTmpListAPSGeneInserItem = false;
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<TransStartStopGeneInfo> alTSSGISent) {

				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){
					
					HashMap<Integer,AbsoPropSElemItem> origamiSElementId2Apse = new HashMap<Integer, AbsoPropSElemItem>();
					for(int k=0;
							k<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().size();
							k++){
						AbsoPropSElemItem apseIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().get(k);
						origamiSElementId2Apse.put(apseIT.getsOrigamiElementId(), apseIT);
					}
					
					//sort alTSSGISent
					Comparator<TransStartStopGeneInfo> compaByGeneIdTransientStartStopGeneInfoComparatorIT = new ByGeneIdTransientStartStopGeneInfoComparator();
					Collections.sort(alTSSGISent, compaByGeneIdTransientStartStopGeneInfoComparatorIT);
					
					//sort tmpListAPSGeneInserItem
					Comparator<AbsoPropSGeneInserItem> compaBySGeneIdAbsoluteProportionSGeneInsertionItemComparatorIT = new BySGeneIdAbsoluteProportionSGeneInsertionItemComparator();
					Collections.sort(
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPSGeneInserItem(),
					compaBySGeneIdAbsoluteProportionSGeneInsertionItemComparatorIT
					);
					
					int j=-1;
					//fill tmpListAPSGeneInserItem with new data and compute other info
					for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getTmpListAPSGeneInserItem().size();i++){
						j++;
						if(j>alTSSGISent.size()-1){
							j=alTSSGISent.size()-1;
						}
						
						AbsoPropSGeneInserItem apsgiiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getTmpListAPSGeneInserItem().get(i);
						
						TransStartStopGeneInfo tssgiIT = alTSSGISent.get(j);
						
						//check right one
						if(apsgiiIT.getsGeneId() == tssgiIT.getOrigamiGeneId()){
							//ok

							//basic info, tmp
							apsgiiIT.setSEnumBlockType(SEnumBlockType.S_INSERTION);
							
							//fill new data
							apsgiiIT.setsPbStartGeneInElement(tssgiIT.getStart());
							apsgiiIT.setsPbStopGeneInElement(tssgiIT.getStop());

							//then compute qPercentstart et qPercentstop for each synteny
							apsgiiIT.setsPercentStart(((double)origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsPbStartOfElementInOrga()+(double)apsgiiIT.getsPbStartGeneInElement())/(double)origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsSizeOfOragnismInPb());
							apsgiiIT.setsPercentStop(((double)origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsPbStartOfElementInOrga()+(double)apsgiiIT.getsPbStopGeneInElement())/(double)origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsSizeOfOragnismInPb());
							
							//set other vital info
							apsgiiIT.setsOrigamiElementId(tssgiIT.getOrigamiElementId());
							apsgiiIT.setsAccnum(origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsAccnum());
							apsgiiIT.setsPbStartOfElementInOrga(origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsPbStartOfElementInOrga());
							apsgiiIT.setsSizeOfOragnismInPb(origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsSizeOfOragnismInPb());

						}else{
							//patine j
							i = i - 1;
							j = j - 2;
							if(j < -1){
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in "+methodNameToReport+"\n onSuccess :\n different apsgiiIT and tssgiIT to the end : "+apsgiiIT.getsGeneId()+" ; "+tssgiIT.getOrigamiGeneId()));
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
								break;
							}else{
								continue;
							}
						}
					}
				}
				
				waitingForFillingTmpListAPSGeneInserItem = false;
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				
				checkWetherAllTransientFillingStartStopGeneAreBack(indexInFullAssociatedListAbsoluteProportionItemToDisplay,
						indexGenomePanelInLIST_GENOME_PANEL,
						viewTypeSent,
						idxToDisplayInListHAPI);
					
				
				
			}
		};

		try {
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(true);
			ArrayList<Integer> listSGeneIds = new ArrayList<Integer>();
			for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPSGeneInserItem().size();i++){
				AbsoPropSGeneInserItem apsciIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.getTmpListAPSGeneInserItem().get(i);
				//TransientStartStopGeneInfo newTSSGI = new TransientStartStopGeneInfo();
				//newTSSGI.setOrigamiAlignmentId(apsciIT.getsOrigamiAlignmentId());
				//newTSSGI.setOrigamiSGeneId(apsciIT.getsGeneId());
				listSGeneIds.add(apsciIT.getsGeneId());
				
				
			}

			waitingForFillingTmpListAPSGeneInserItem = true;
			homologieBrowsingResultService
					.getListTransientStartStopGeneInfoWithListGeneIds(
							listSGeneIds,
							callback);
			AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
		} catch (Exception e) {
			//String mssgError = "ERROR try in getArrayListTransientFillingStartStopGeneForTtmpListAPSGeneInserItem: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			waitingForFillingTmpListAPSGeneInserItem = false;
		}
		
		

	}

	protected void getArrayListTransientFillingStartStopGeneForTtmpListAPQGeneInserItem(
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
			//final String qAccnum, final int qPbStartOfElementInOrga, final int qSizeOfOragnismInPb,
			//final String sAccnum, final int sPbStartOfElementInOrga, final int sSizeOfOragnismInPb,
			final EnumResultViewTypes viewTypeSent,
			final int idxToDisplayInListHAPI) {

		AsyncCallback<ArrayList<TransStartStopGeneInfo>> callback = new AsyncCallback<ArrayList<TransStartStopGeneInfo>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
				//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
				waitingForFillingTmpListAPQGeneInserItem = false;
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<TransStartStopGeneInfo> alTSSGISent) {

				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){
					
					HashMap<Integer,AbsoPropQElemItem> origamiQElementId2Apqe = new HashMap<Integer, AbsoPropQElemItem>();
					for(int k=0;
							k<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemQ().size();
							k++){
						AbsoPropQElemItem apqeIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemQ().get(k);
						origamiQElementId2Apqe.put(apqeIT.getqOrigamiElementId(), apqeIT);
					}
					
					//sort alTSSGISent
					Comparator<TransStartStopGeneInfo> compaByGeneIdTransientStartStopGeneInfoComparatorIT = new ByGeneIdTransientStartStopGeneInfoComparator();
					Collections.sort(alTSSGISent, compaByGeneIdTransientStartStopGeneInfoComparatorIT);
					
					//sort tmpListAPSGeneInserItem
					Comparator<AbsoPropQGeneInserItem> compaByQGeneIdAbsoluteProportionQGeneInsertionItemComparatorIT = new ByQGeneIdAbsoluteProportionQGeneInsertionItemComparator();
					Collections.sort(
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQGeneInserItem(),
					compaByQGeneIdAbsoluteProportionQGeneInsertionItemComparatorIT
					);
					
					//fill tmpListAPSGeneInserItem with new data and compute other info
					int j=-1;
					for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getTmpListAPQGeneInserItem().size();i++){
						j++;
						if(j>alTSSGISent.size()-1){
							j=alTSSGISent.size()-1;
						}
						
						AbsoPropQGeneInserItem apqgiiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getTmpListAPQGeneInserItem().get(i);
						
						TransStartStopGeneInfo tssgiIT = alTSSGISent.get(j);
						
						//check right one
						if(apqgiiIT.getQGeneId() == tssgiIT.getOrigamiGeneId()){
							//ok

							//basic info, tmp
							apqgiiIT.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
							
							//fill new data
							apqgiiIT.setqPbStartGeneInElement(tssgiIT.getStart());
							apqgiiIT.setqPbStopGeneInElement(tssgiIT.getStop());

							//then compute qPercentstart et qPercentstop for each synteny
							apqgiiIT.setqPercentStart(((double)origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getqPbStartOfElementInOrga()+(double)apqgiiIT.getqPbStartGeneInElement())/(double)origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getQsizeOfOragnismInPb());
							apqgiiIT.setqPercentStop(((double)origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getqPbStartOfElementInOrga()+(double)apqgiiIT.getqPbStopGeneInElement())/(double)origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getQsizeOfOragnismInPb());
							
							//set other vital info
							apqgiiIT.setqOrigamiElementId(tssgiIT.getOrigamiElementId());
							apqgiiIT.setqAccnum(origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getqAccnum());
							apqgiiIT.setqPbStartOfElementInOrga(origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getqPbStartOfElementInOrga());
							apqgiiIT.setQsizeOfOragnismInPb(origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getQsizeOfOragnismInPb());

							
						}else{
							//patine j
							i = i - 1;
							j = j - 2;
							if(j < -1){
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getArrayListTransientFillingStartStopGeneForTtmpListAPQGeneInserItem onSuccess : different apsgiiIT and tssgiIT : "+apqgiiIT.getQGeneId()+" ; "+tssgiIT.getOrigamiGeneId()));
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
								break;
							}else{
								continue;
							}
						}
					
					}
					
				}
				
				
				waitingForFillingTmpListAPQGeneInserItem = false;
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				
				checkWetherAllTransientFillingStartStopGeneAreBack(indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL,
						viewTypeSent,
						idxToDisplayInListHAPI);
				
			}
		};

		try {
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(true);
			ArrayList<Integer> listQGeneInserIds = new ArrayList<Integer>();
			for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQGeneInserItem().size();i++){
				AbsoPropQGeneInserItem apqciIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.getTmpListAPQGeneInserItem().get(i);
				//TransientStartStopGeneInfo newTSSGI = new TransientStartStopGeneInfo();
				//newTSSGI.setOrigamiAlignmentId(apqciIT.getqOrigamiAlignmentId());
				//newTSSGI.setOrigamiQGeneId(apqciIT.getQGeneId());
				listQGeneInserIds.add(apqciIT.getQGeneId());
			}
			

			waitingForFillingTmpListAPQGeneInserItem = true;
			homologieBrowsingResultService
					.getListTransientStartStopGeneInfoWithListGeneIds(
							listQGeneInserIds,
							callback);
			AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
		} catch (Exception e) {
			//String mssgError = "ERROR try in getArrayListTransientFillingStartStopGeneForTtmpListAPQGeneInserItem: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			waitingForFillingTmpListAPQGeneInserItem = false;
			
		}
		
		
		
	}

	protected void getArrayListTransientFillingStartStopGeneForTtmpListAPQSGeneHomoSortedByS(
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
			//final String qAccnum, final int qPbStartOfElementInOrga, final int qSizeOfOragnismInPb,
			//final String sAccnum, final int sPbStartOfElementInOrga, final int sSizeOfOragnismInPb,
			final EnumResultViewTypes viewTypeSent,
			final int idxToDisplayInListHAPI) {

		AsyncCallback<ArrayList<TransStartStopGeneInfo>> callback = new AsyncCallback<ArrayList<TransStartStopGeneInfo>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
				//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
				waitingForFillingTmpListAPQSGeneHomoSortedByS = false;
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<TransStartStopGeneInfo> alTSSGISent) {

				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){

					HashMap<Integer,AbsoPropSElemItem> origamiSElementId2Apse = new HashMap<Integer, AbsoPropSElemItem>();
					for(int k=0;
							k<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().size();
							k++){
						AbsoPropSElemItem apseIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().get(k);
						origamiSElementId2Apse.put(apseIT.getsOrigamiElementId(), apseIT);
					}
					
					//sort alTSSGISent
					Comparator<TransStartStopGeneInfo> compaByGeneIdTransientStartStopGeneInfoComparatorIT = new ByGeneIdTransientStartStopGeneInfoComparator();
					Collections.sort(alTSSGISent, compaByGeneIdTransientStartStopGeneInfoComparatorIT);
					
					//sort getTmpListAPQSGeneHomo
					Comparator<AbsoPropQSGeneHomoItem> compaBySGeneIdAbsoluteProportionQSGeneHomologyItemComparatorIT = new BySGeneIdAbsoluteProportionQSGeneHomologyItemComparator();
					Collections.sort(
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQSGeneHomo(),
					compaBySGeneIdAbsoluteProportionQSGeneHomologyItemComparatorIT
					);
					
					//fill getTmpListAPQSGeneHomo with new data and compute other info
					int j = -1;
					for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getTmpListAPQSGeneHomo().size();i++){
						j++;
						if(j>alTSSGISent.size()-1){
							j=alTSSGISent.size()-1;
						}
						AbsoPropQSGeneHomoItem apqsgiiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getTmpListAPQSGeneHomo().get(i);
						
						TransStartStopGeneInfo tssgiIT = alTSSGISent.get(j);
						
						//check right one
						if(apqsgiiIT.getQsSGeneId() == tssgiIT.getOrigamiGeneId()){
							//ok

							//basic info, tmp
							apqsgiiIT.setQsEnumBlockType(QSEnumBlockType.QS_HOMOLOGS_BLOCK);
							
							//fill new data
							apqsgiiIT.setQsSPbStartGeneInElement(tssgiIT.getStart());
							apqsgiiIT.setQsSPbStopGeneInElement(tssgiIT.getStop());

							//then compute qPercentstart et qPercentstop et sPercentstart et sPercentstop for each gene homology
							apqsgiiIT.setsPercentStart(((double)origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsPbStartOfElementInOrga()+(double)apqsgiiIT.getQsSPbStartGeneInElement())/(double)origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsSizeOfOragnismInPb());
							apqsgiiIT.setsPercentStop(((double)origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsPbStartOfElementInOrga()+(double)apqsgiiIT.getQsSPbStopGeneInElement())/(double)origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsSizeOfOragnismInPb());
			
							//set other vital info
							apqsgiiIT.setQsSOrigamiElementId(tssgiIT.getOrigamiElementId());
							apqsgiiIT.setQsSAccnum(origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsAccnum());
							apqsgiiIT.setQsSPbStartOfElementInOrga(origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsPbStartOfElementInOrga());
							apqsgiiIT.setQsSSizeOfOrganismInPb(origamiSElementId2Apse.get(tssgiIT.getOrigamiElementId()).getsSizeOfOragnismInPb());

							
						}else{
							//patine j
							i = i - 1;
							j = j - 2;
							if(j < -1){
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getArrayListTransientFillingStartStopGeneForTtmpListAPQSGeneHomoSortedByS onSuccess : different apsgiiIT and tssgiIT : "+apqsgiiIT.getQsSGeneId()+" ; "+tssgiIT.getOrigamiGeneId()));
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
								break;
							}else{
								continue;
							}
						}
						
					}
					
				}
				
				
				waitingForFillingTmpListAPQSGeneHomoSortedByS = false;
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				
				checkWetherAllTransientFillingStartStopGeneAreBack(indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL,
						viewTypeSent,
						idxToDisplayInListHAPI);
				
			}
		};

		try {
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(true);
			ArrayList<Integer> listSGeneIds = new ArrayList<Integer>();
			for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQSGeneHomo().size();i++){
				AbsoPropQSGeneHomoItem apqsciIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.getTmpListAPQSGeneHomo().get(i);
				//TransientStartStopGeneInfo newTSSGI = new TransientStartStopGeneInfo();
				//newTSSGI.setOrigamiAlignmentId(apqsciIT.getSyntenyOrigamiAlignmentId());
				//newTSSGI.setOrigamiQGeneId(apqsciIT.getQsQGeneId());
				//newTSSGI.setOrigamiSGeneId(apqsciIT.getQsSGeneId());
				listSGeneIds.add(apqsciIT.getQsSGeneId());
			}
			
			waitingForFillingTmpListAPQSGeneHomoSortedByS = true;
			homologieBrowsingResultService
					.getListTransientStartStopGeneInfoWithListGeneIds(
							listSGeneIds,
							callback);
			AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
		} catch (Exception e) {
			//String mssgError = "ERROR try in getArrayListTransientFillingStartStopGeneForTtmpListAPQSGeneHomoSortedByS: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			waitingForFillingTmpListAPQSGeneHomoSortedByS = false;
		}
		
		
	}

	protected void getArrayListTransientFillingStartStopGeneForTmpListAPQSGeneHomoSortedByQ(
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
			//final String qAccnum, final int qPbStartOfElementInOrga, final int qSizeOfOragnismInPb,
			//final String sAccnum, final int sPbStartOfElementInOrga, final int sSizeOfOragnismInPb,
			final EnumResultViewTypes viewTypeSent,
			final int idxToDisplayInListHAPI) {
		
		AsyncCallback<ArrayList<TransStartStopGeneInfo>> callback = new AsyncCallback<ArrayList<TransStartStopGeneInfo>>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
				//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
				waitingForFillingTmpListAPQSGeneHomoSortedByQ = false;
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<TransStartStopGeneInfo> alTSSGISent) {

				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){

					HashMap<Integer,AbsoPropQElemItem> origamiQElementId2Apqe = new HashMap<Integer, AbsoPropQElemItem>();
					for(int k=0;
							k<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemQ().size();
							k++){
						AbsoPropQElemItem apqeIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemQ().get(k);
						origamiQElementId2Apqe.put(apqeIT.getqOrigamiElementId(), apqeIT);
					}
					
					//sort alTSSGISent
					Comparator<TransStartStopGeneInfo> compaByGeneIdTransientStartStopGeneInfoComparatorIT = new ByGeneIdTransientStartStopGeneInfoComparator();
					Collections.sort(alTSSGISent, compaByGeneIdTransientStartStopGeneInfoComparatorIT);
					
					//sort getTmpListAPQSGeneHomo
					Comparator<AbsoPropQSGeneHomoItem> compaByQGeneIdAbsoluteProportionQSGeneHomologyItemComparatorIT = new ByQGeneIdAbsoluteProportionQSGeneHomologyItemComparator();
					Collections.sort(
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQSGeneHomo(),
					compaByQGeneIdAbsoluteProportionQSGeneHomologyItemComparatorIT
					);
					
					//fill getTmpListAPQSGeneHomo with new data and compute other info
					int j=-1;
					for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getTmpListAPQSGeneHomo().size();i++){
						j++;
						if(j>alTSSGISent.size()-1){
							j=alTSSGISent.size()-1;
						}
						AbsoPropQSGeneHomoItem apqsgiiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getTmpListAPQSGeneHomo().get(i);
						
						TransStartStopGeneInfo tssgiIT = alTSSGISent.get(j);
						
						//check right one
						if(apqsgiiIT.getQsQGeneId() == tssgiIT.getOrigamiGeneId()){
							//ok

							//basic info, tmp
							apqsgiiIT.setQsEnumBlockType(QSEnumBlockType.QS_HOMOLOGS_BLOCK);
							
							//fill new data
							apqsgiiIT.setQsQPbStartGeneInElement(tssgiIT.getStart());
							apqsgiiIT.setQsQPbStopGeneInElement(tssgiIT.getStop());
							//then compute qPercentstart et qPercentstop et sPercentstart et sPercentstop for each gene homology
							apqsgiiIT.setqPercentStart(((double)origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getqPbStartOfElementInOrga()+(double)apqsgiiIT.getQsQPbStartGeneInElement())/(double)origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getQsizeOfOragnismInPb());
							apqsgiiIT.setqPercentStop(((double)origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getqPbStartOfElementInOrga()+(double)apqsgiiIT.getQsQPbStopGeneInElement())/(double)origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getQsizeOfOragnismInPb());

							//set other vital info
							apqsgiiIT.setQsQOrigamiElementId(tssgiIT.getOrigamiElementId());
							apqsgiiIT.setQsQAccnum(origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getqAccnum());
							apqsgiiIT.setQsQPbStartOfElementInOrga(origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getqPbStartOfElementInOrga());
							apqsgiiIT.setQsQSizeOfOrganismInPb(origamiQElementId2Apqe.get(tssgiIT.getOrigamiElementId()).getQsizeOfOragnismInPb());
							
//							//then compute qPercentstart et qPercentstop et sPercentstart et sPercentstop for each gene homology
//							apqsgiiIT.setqPercentStart(((double)qPbStartOfElementInOrga+(double)apqsgiiIT.getQsQPbStartGeneInElement())/(double)qSizeOfOragnismInPb);
//							apqsgiiIT.setqPercentStop(((double)qPbStartOfElementInOrga+(double)apqsgiiIT.getQsQPbStopGeneInElement())/(double)qSizeOfOragnismInPb);
//
//							//set other vital info
//							apqsgiiIT.setQsQAccnum(qAccnum);
//							apqsgiiIT.setQsQPbStartOfElementInOrga(qPbStartOfElementInOrga);
//							apqsgiiIT.setQsQSizeOfOrganismInPb(qSizeOfOragnismInPb);

						}else{
							//patine j
							i = i - 1;
							j = j - 2;
							if(j < -1){
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getArrayListTransientFillingStartStopGeneForTmpListAPQSGeneHomoSortedByQ onSuccess : different apsgiiIT and tssgiIT : "+apqsgiiIT.getQsQGeneId()+" ; "+tssgiIT.getOrigamiGeneId()));
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
								break;
							}else{
								continue;
							}
						}
						
					}
					
				}
				
				
				waitingForFillingTmpListAPQSGeneHomoSortedByQ = false;
				//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
				AppController.backFromCallBackCheckingOfLoadingMask();
				
				checkWetherAllTransientFillingStartStopGeneAreBack(indexInFullAssociatedListAbsoluteProportionItemToDisplay, indexGenomePanelInLIST_GENOME_PANEL,
						viewTypeSent,
						idxToDisplayInListHAPI);
				
			}
		};

		try {
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(true);
			ArrayList<Integer> listGeneIds = new ArrayList<Integer>();
			for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQSGeneHomo().size();i++){
				AbsoPropQSGeneHomoItem apqsciIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.getTmpListAPQSGeneHomo().get(i);
				//TransientStartStopGeneInfo newTSSGI = new TransientStartStopGeneInfo();
				//newTSSGI.setOrigamiAlignmentId(apqsciIT.getSyntenyOrigamiAlignmentId());
				//newTSSGI.setOrigamiQGeneId(apqsciIT.getQsQGeneId());
				//newTSSGI.setOrigamiSGeneId(apqsciIT.getQsSGeneId());
				listGeneIds.add(apqsciIT.getQsQGeneId());
			}
			
			waitingForFillingTmpListAPQSGeneHomoSortedByQ = true;
			homologieBrowsingResultService
					.getListTransientStartStopGeneInfoWithListGeneIds(
							listGeneIds,
							callback);
			AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
		} catch (Exception e) {
			//String mssgError = "ERROR try in getArrayListTransientFillingStartStopGeneForTmpListAPQSGeneHomoSortedByQ: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(true);
			//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
			waitingForFillingTmpListAPQSGeneHomoSortedByQ = false;
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
		}
		
		
	}

	protected void checkWetherAllTransientFillingStartStopGeneAreBack(
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay, final int indexGenomePanelInLIST_GENOME_PANEL,
			final EnumResultViewTypes viewTypeSent,
			final int idxToDisplayInListHAPI) {
		
		if(waitingForFillingTmpListAPQSGeneHomoSortedByQ
				|| waitingForFillingTmpListAPQSGeneHomoSortedByS
				|| waitingForFillingTmpListAPQGeneInserItem
				|| waitingForFillingTmpListAPSGeneInserItem){
			//still waiting for info to get back, do nothing
		}else{
			
			//check for error 
			if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().isGpiErrorWhileLoadingAdditionalData()){
				
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("Error", true);
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
//				Insyght.CANVAS_MENU_POPUP.hide();
				
			}else{
			
				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(viewTypeSent)==0){

					CommonMethodsViewResults cmvrIT = new CommonMethodsViewResults();
					
					ArrayList<Object> alQSAndS = new ArrayList<Object>();
					alQSAndS.addAll(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getTmpListAPQSGeneHomo());
					alQSAndS.addAll(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
							.getTmpListAPSGeneInserItem());
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPQSGeneHomo().clear();
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
					.getTmpListAPSGeneInserItem().clear();
					
					Comparator<Object> compaByPbSPercentStartQSGeneHomoAndSGeneInsertComparatorIT = new ByPbSPercentStartQSGeneHomoAndSGeneInsertComparator();
					Collections.sort(alQSAndS, compaByPbSPercentStartQSGeneHomoAndSGeneInsertComparatorIT);
					
					boolean atLeastOneQS = cmvrIT.crunchSGeneInsertionIntoQSTmpList(alQSAndS);
					ArrayList<SuperHoldAbsoPropItem> alHAPIIT = new ArrayList<SuperHoldAbsoPropItem>();
					
					if(atLeastOneQS){
						alQSAndS.addAll(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getTmpListAPQGeneInserItem());
						Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.getTmpListAPQGeneInserItem().clear();
						Comparator<Object> compaByPbQPercentStartQSGeneHomoAndQGeneInsertComparatorIT = new ByPbQPercentStartQSGeneHomoAndQGeneInsertComparator();
						Collections.sort(alQSAndS, compaByPbQPercentStartQSGeneHomoAndQGeneInsertComparatorIT);
						cmvrIT.crunchQSGeneHomologyListOfTmpOtherAndQGeneInsertionIntoQSTmpList(alQSAndS);
						cmvrIT.buildCorrectlyListSuperHolderFromGeneList(alQSAndS, alHAPIIT);
					}else{
						//all insertion
						Comparator<Object> compaByPbQPercentStartQSGeneHomoAndQGeneInsertComparatorIT = new ByPbQPercentStartQSGeneHomoAndQGeneInsertComparator();
						Collections.sort(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getTmpListAPQGeneInserItem(), compaByPbQPercentStartQSGeneHomoAndQGeneInsertComparatorIT);
						alQSAndS.addAll(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.getTmpListAPQGeneInserItem());
						Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.getTmpListAPQGeneInserItem().clear();
						cmvrIT.buildCorrectlyListSuperHolderFromGeneListAllInsertion(alQSAndS, alHAPIIT);
					}
					
					
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().insertGenesAtSpecifiedSyntenyItemIndex(alHAPIIT, indexInFullAssociatedListAbsoluteProportionItemToDisplay,
							idxToDisplayInListHAPI);
					//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.setIndexStartBirdSynteny(0);
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawViewToCanvas(1.0, 0, true, true, false, false);

				}
				
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiErrorWhileLoadingAdditionalData(false);
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setGpiLoadingAdditionalData(false);
				//Insyght.CANVAS_MENU_POPUP.hide();
			
			}
		}

	}

	//comparator
	
	public class ByGeneIdTransientStartStopGeneInfoComparator implements Comparator<TransStartStopGeneInfo> {
		//@Override
		public int compare(TransStartStopGeneInfo arg0, TransStartStopGeneInfo arg1) {
			int byGeneIdCompa = arg0.getOrigamiGeneId() - arg1.getOrigamiGeneId();
			return byGeneIdCompa;
		}
	}
	
	public class BySGeneIdAbsoluteProportionSGeneInsertionItemComparator implements Comparator<AbsoPropSGeneInserItem> {
		//@Override
		public int compare(AbsoPropSGeneInserItem arg0, AbsoPropSGeneInserItem arg1) {
			int byGeneIdCompa = arg0.getsGeneId() - arg1.getsGeneId();
			return byGeneIdCompa;
		}
	}
	
	public class ByQGeneIdAbsoluteProportionQGeneInsertionItemComparator implements Comparator<AbsoPropQGeneInserItem> {
		//@Override
		public int compare(AbsoPropQGeneInserItem arg0, AbsoPropQGeneInserItem arg1) {
			int byGeneIdCompa = arg0.getQGeneId() - arg1.getQGeneId();
			return byGeneIdCompa;
		}
	}

	public class BySGeneIdAbsoluteProportionQSGeneHomologyItemComparator implements Comparator<AbsoPropQSGeneHomoItem> {
		//@Override
		public int compare(AbsoPropQSGeneHomoItem arg0, AbsoPropQSGeneHomoItem arg1) {
			int byGeneIdCompa = arg0.getQsSGeneId() - arg1.getQsSGeneId();
			return byGeneIdCompa;
		}
	}

	public class ByQGeneIdAbsoluteProportionQSGeneHomologyItemComparator implements Comparator<AbsoPropQSGeneHomoItem> {
		//@Override
		public int compare(AbsoPropQSGeneHomoItem arg0, AbsoPropQSGeneHomoItem arg1) {
			int byGeneIdCompa = arg0.getQsQGeneId() - arg1.getQsQGeneId();
			return byGeneIdCompa;
		}
	}
	

	public class ByPbQStartGeneHomoInOrgaAndQLenghtComparator implements Comparator<AbsoPropQSGeneHomoItem> {
		//@Override
		public int compare(AbsoPropQSGeneHomoItem arg0, AbsoPropQSGeneHomoItem arg1) {
			int startQCompa = (arg0.getQsQPbStartOfElementInOrga()+arg0.getQsQPbStartGeneInElement()) - (arg1.getQsQPbStartOfElementInOrga()+arg1.getQsQPbStartGeneInElement());
			if (startQCompa == 0){
				int lenghtQCompa = (arg0.getQsQPbStopGeneInElement()-arg0.getQsQPbStartGeneInElement()) - (arg1.getQsQPbStopGeneInElement()-arg1.getQsQPbStartGeneInElement());
				return -lenghtQCompa;
			}else{
				return startQCompa;
			}
		}
	}

	public class ByPbSPercentStartQSGeneHomoAndSGeneInsertComparator implements Comparator<Object> {
		//@Override
		public int compare(Object arg0, Object arg1) {
			double sPercentStart1 = -1;
			double sPercentStart2 = -2;
			if(arg0 instanceof AbsoPropQSGeneHomoItem){
				sPercentStart1 = ((AbsoPropQSGeneHomoItem)arg0).getsPercentStart();
			}
			if(arg0 instanceof AbsoPropSGeneInserItem){
				sPercentStart1 = ((AbsoPropSGeneInserItem)arg0).getsPercentStart();
			}
			if(arg1 instanceof AbsoPropQSGeneHomoItem){
				sPercentStart2 = ((AbsoPropQSGeneHomoItem)arg1).getsPercentStart();
			}
			if(arg1 instanceof AbsoPropSGeneInserItem){
				sPercentStart2 = ((AbsoPropSGeneInserItem)arg1).getsPercentStart();
			}
			if((sPercentStart1 - sPercentStart2)<0){
				return -1;
			}else if((sPercentStart1 - sPercentStart2)==0){
				return 0;
			}else{
				return 1;
			}
		}
	}
	
	//same in resultLoadinginitial
	public class ByPbQPercentStartQSGeneHomoAndQGeneInsertComparator implements Comparator<Object> {
		//@Override
		public int compare(Object arg0, Object arg1) {
			double qPercentStart1 = -1;
			double qPercentStart2 = -2;
			if(arg0 instanceof AbsoPropQSGeneHomoItem){
				qPercentStart1 = ((AbsoPropQSGeneHomoItem)arg0).getqPercentStart();
			}
			if(arg0 instanceof AbsoPropQGeneInserItem){
				qPercentStart1 = ((AbsoPropQGeneInserItem)arg0).getqPercentStart();
			}
			if(arg1 instanceof AbsoPropQSGeneHomoItem){
				qPercentStart2 = ((AbsoPropQSGeneHomoItem)arg1).getqPercentStart();
			}
			if(arg1 instanceof AbsoPropQGeneInserItem){
				qPercentStart2 = ((AbsoPropQGeneInserItem)arg1).getqPercentStart();
			}
			if((qPercentStart1 - qPercentStart2)<0){
				return -1;
			}else if((qPercentStart1 - qPercentStart2)==0){
				return 0;
			}else{
				return 1;
			}
		}
	}
	
//	
//	public class ByQGeneIdThenSGeneIdThenAlignmentIdTransientStartStopGeneInfoComparator implements Comparator<TransientStartStopGeneInfo> {
//		@Override
//		public int compare(TransientStartStopGeneInfo arg0, TransientStartStopGeneInfo arg1) {
//			int byQGeneIdCompa = arg0.getOrigamiQGeneId() - arg1.getOrigamiQGeneId();
//			if (byQGeneIdCompa == 0){
//				int bySGeneIdCompa = arg0.getOrigamiSGeneId() - arg1.getOrigamiSGeneId();
//				if (bySGeneIdCompa == 0){
//					int byAlignmentIdCompa = arg0.getOrigamiAlignmentId() - arg1.getOrigamiAlignmentId();
//					if (byAlignmentIdCompa == 0){
//						Window.alert("ERROR in ByQGeneIdThenSGeneIdThenAlignmentIdTransientStartStopGeneInfoComparator, could not sort QGeneId = "+arg0.getOrigamiQGeneId()+" ; SGeneId = "+arg0.getOrigamiSGeneId()+" ; getOrigamiAlignmentId = "+arg0.getOrigamiAlignmentId());
//						return 0;
//					}else{
//						return byAlignmentIdCompa;
//					}
//				}else{
//					return bySGeneIdCompa;
//				}
//			}else{
//				return byQGeneIdCompa;
//			}
//		}
//	}
//	
//	public class ByQGeneIdThenAlignmentIdTransientStartStopGeneInfoComparator implements Comparator<TransientStartStopGeneInfo> {
//		@Override
//		public int compare(TransientStartStopGeneInfo arg0, TransientStartStopGeneInfo arg1) {
//			int byQGeneIdCompa = arg0.getOrigamiQGeneId() - arg1.getOrigamiQGeneId();
//			if (byQGeneIdCompa == 0){
//					int byAlignmentIdCompa = arg0.getOrigamiAlignmentId() - arg1.getOrigamiAlignmentId();
//					if (byAlignmentIdCompa == 0){
//						Window.alert("ERROR in ByQGeneIdThenAlignmentIdTransientStartStopGeneInfoComparator, could not sort QGeneId = "+arg0.getOrigamiQGeneId()+" ; getOrigamiAlignmentId = "+arg0.getOrigamiAlignmentId());
//						return 0;
//					}else{
//						return byAlignmentIdCompa;
//					}
//			}else{
//				return byQGeneIdCompa;
//			}
//		}
//	}
//	
//	public class BySGeneIdThenAlignmentIdTransientStartStopGeneInfoComparator implements Comparator<TransientStartStopGeneInfo> {
//		@Override
//		public int compare(TransientStartStopGeneInfo arg0, TransientStartStopGeneInfo arg1) {
//			int bySGeneIdCompa = arg0.getOrigamiSGeneId() - arg1.getOrigamiSGeneId();
//			if (bySGeneIdCompa == 0){
//				int byAlignmentIdCompa = arg0.getOrigamiAlignmentId() - arg1.getOrigamiAlignmentId();
//				if (byAlignmentIdCompa == 0){
//					Window.alert("ERROR in BySGeneIdThenAlignmentIdTransientStartStopGeneInfoComparator, could not sort SGeneId = "+arg0.getOrigamiSGeneId()+" ; getOrigamiAlignmentId = "+arg0.getOrigamiAlignmentId());
//					return 0;
//				}else{
//					return byAlignmentIdCompa;
//				}
//			}else{
//				return bySGeneIdCompa;
//			}
//			
//		}
//	}
//
//	public class ByQGeneIdThenSGeneIdThenAlignmentIdAbsoluteProportionQSGeneHomologyItemComparator implements Comparator<AbsoluteProportionQSGeneHomologyItem> {
//		@Override
//		public int compare(AbsoluteProportionQSGeneHomologyItem arg0, AbsoluteProportionQSGeneHomologyItem arg1) {
//			int byQGeneIdCompa = arg0.getQsQGeneId() - arg1.getQsQGeneId();
//			if (byQGeneIdCompa == 0){
//				int bySGeneIdCompa = arg0.getQsSGeneId() - arg1.getQsSGeneId();
//				if (bySGeneIdCompa == 0){
//					int byAlignmentIdCompa = arg0.getSyntenyOrigamiAlignmentId() - arg1.getSyntenyOrigamiAlignmentId();
//					if (byAlignmentIdCompa == 0){
//						Window.alert("ERROR in ByQGeneIdThenSGeneIdThenAlignmentIdAbsoluteProportionQSGeneHomologyItemComparator, could not sort QGeneId = "+arg0.getQsQGeneId()+" ; SGeneId = "+arg0.getQsSGeneId()+" ; getOrigamiAlignmentId = "+arg0.getSyntenyOrigamiAlignmentId());
//						return 0;
//					}else{
//						return byAlignmentIdCompa;
//					}
//				}else{
//					return bySGeneIdCompa;
//				}
//			}else{
//				return byQGeneIdCompa;
//			}
//		}
//	}
//	
//	public class ByQGeneIdThenAlignmentIdAbsoluteProportionQGeneInsertionItemComparator implements Comparator<AbsoluteProportionQGeneInsertionItem> {
//		@Override
//		public int compare(AbsoluteProportionQGeneInsertionItem arg0, AbsoluteProportionQGeneInsertionItem arg1) {
//			int byQGeneIdCompa = arg0.getQGeneId() - arg1.getQGeneId();
//			if (byQGeneIdCompa == 0){
//					int byAlignmentIdCompa = arg0.getqOrigamiAlignmentId() - arg1.getqOrigamiAlignmentId();
//					if (byAlignmentIdCompa == 0){
//						Window.alert("ERROR in ByQGeneIdThenAlignmentIdAbsoluteProportionQGeneInsertionItemComparator, could not sort QGeneId = "+arg0.getQGeneId()+" ; getOrigamiAlignmentId = "+arg0.getqOrigamiAlignmentId());
//						return 0;
//					}else{
//						return byAlignmentIdCompa;
//					}
//			}else{
//				return byQGeneIdCompa;
//			}
//		}
//	}
//	
//	
//	public class BySGeneIdThenAlignmentIdAbsoluteProportionSGeneInsertionItemComparator implements Comparator<AbsoluteProportionSGeneInsertionItem> {
//		@Override
//		public int compare(AbsoluteProportionSGeneInsertionItem arg0, AbsoluteProportionSGeneInsertionItem arg1) {
//			int bySGeneIdCompa = arg0.getsGeneId() - arg1.getsGeneId();
//			if (bySGeneIdCompa == 0){
//				int byAlignmentIdCompa = arg0.getsOrigamiAlignmentId() - arg1.getsOrigamiAlignmentId();
//				if (byAlignmentIdCompa == 0){
//					Window.alert("ERROR in BySGeneIdThenAlignmentIdAbsoluteProportionSGeneInsertionItemComparator, could not sort SGeneId = "+arg0.getsGeneId()+" ; getOrigamiAlignmentId = "+arg0.getsOrigamiAlignmentId());
//					return 0;
//				}else{
//					return byAlignmentIdCompa;
//				}
//			}else{
//				return bySGeneIdCompa;
//			}
//		}
//	}
//	
	
}
