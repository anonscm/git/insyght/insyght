package fr.inra.jouy.client.view.result;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
//import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.NavigationControler;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.crossSections.popUp.PopUpListGenesDialog;
import fr.inra.jouy.client.view.search.GetResultDialog;
import fr.inra.jouy.shared.RefGeneSetForAnnotCompa;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSSyntItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.HolderAbsoluteProportionItem;
import fr.inra.jouy.shared.pojos.symbols.SuperHoldAbsoPropItem;

public class CanvasMenuPopUp extends PopupPanel {

	private static CanvasMenuPopUpUiBinder uiBinder = GWT.create(CanvasMenuPopUpUiBinder.class);

	interface CanvasMenuPopUpUiBinder extends UiBinder<Widget, CanvasMenuPopUp> {
	}

	private static int MAX_COUNT = 8;
	private static String MAX_HEIGHT = "15em";

	//@UiField MenuBar mainMenu;

	// @UiField MenuItem transfertGene;
	@UiField
	MenuItem transfertGeneToHomologBrowsingTable;
	@UiField
	MenuItem transfertGeneToAnnotationsComparator;
	@UiField
	MenuItem transfertGeneToGenomicOrganizationView;
	// @UiField MenuItem transfertGeneSet;
	@UiField
	MenuItem transfertGeneSetToHomologBrowsingTable;
	@UiField
	MenuItem transfertGeneSetToAnnotationsComparator;
	// @UiField MenuItem transfertDisplayedRegion;
	@UiField
	MenuItem transfertDisplayedRegionToHomologBrowsingTable;
	@UiField
	MenuItem transfertDisplayedRegionToAnnotationsComparator;
	
	@UiField
	MenuItem selectedSymbolListGenes;
	@UiField
	MenuItem selectedSymbolExpandCollapseGenes;
	@UiField
	public MenuItem selectedSymbolListOffShoots;
	@UiField
	MenuItem statSummaryRefCDS;
	

	@UiField
	MenuItem quickNavigationFindGene;
	@UiField
	MenuItem quickNavigationSynchronizeAllDisplaysOnReferenceGene;
	@UiField
	MenuItem quickNavigationCenterDisplayAtClickPosition;
	@UiField
	MenuItem navSymbolsNext;
	@UiField
	MenuItem navSymbolsNextGeneHomology;
	@UiField
	MenuItem navSymbolsNextSyntenyRegion;
	@UiField
	MenuItem navSymbolsNextSyntenyRegion5Genes;
	@UiField
	MenuItem navSymbolsNextSyntenyRegion20Genes;
	@UiField
	MenuItem navSymbolsNextReferenceGeneGenomicRegionInsertion;
	@UiField
	MenuItem navSymbolsPrevious;
	@UiField
	MenuItem navSymbolsPreviousGeneHomology;
	@UiField
	MenuItem navSymbolsPreviousSyntenyRegion;
	@UiField
	MenuItem navSymbolsPreviousSyntenyRegion5Genes;
	@UiField
	MenuItem navSymbolsPreviousSyntenyRegion20Genes;
	@UiField
	MenuItem navSymbolsPreviousReferenceGeneGenomicRegionInsertion;
	@UiField
	public MenuItem referenceGenomeZoomIn;
	@UiField
	MenuItem referenceGenomeZoomOut2X;
	@UiField
	MenuItem referenceGenomeZoomOut5X;
	@UiField
	MenuItem referenceGenomeZoomOutMax;
	@UiField
	public MenuItem comparedGenomeZoomIn;
	@UiField
	MenuItem comparedGenomeZoomOut2X;
	@UiField
	MenuItem comparedGenomeZoomOut5X;
	@UiField
	MenuItem comparedGenomeZoomOutMax;
	@UiField
	MenuItem quickNavigationSetComparedOrganismAsReference;

	@UiField MenuItem exportGeneSequence;
	@UiField MenuItem exportTable;
	@UiField MenuItem exportGraphics;
	

	private ResultLoadingDialog RLD = new ResultLoadingDialog();
	private ResuLoadMoreAbsoPropItemDial RLDAPID = new ResuLoadMoreAbsoPropItemDial();
	
	public CanvasMenuPopUp() {
		// initWidget(uiBinder.createAndBindUi(this));
		setWidget(uiBinder.createAndBindUi(this));
		setAnimationEnabled(true);
		setAutoHideEnabled(true);
		setGlassEnabled(true);
		setAutoHideOnHistoryEventsEnabled(true);
	}

	
	public void addTransfertGeneToHomologBrowsingView(final int geneId) {
		enableTransfertGeneToHomologBrowsingTable(true);
		transfertGeneToHomologBrowsingTable
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();
						ResultLoadingDialog rld = new ResultLoadingDialog();
						ArrayList<Integer> alGeneIds = new ArrayList<Integer>();
						alGeneIds.add(geneId);
						rld.initChangeToViewHomologTable(alGeneIds);
					}
				});
	}

	
	public void addTransfertGeneWithParalogsFromRefOrganismToHomologBrowsingView(
			final ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent
			//,final int indexGenomePanelInLIST_GENOME_PANEL,
			//final boolean isDisplayedRegion
			) {
		enableTransfertGeneToHomologBrowsingTable(true);
		// selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization
		transfertGeneToHomologBrowsingTable
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawLoading();
//						@SuppressWarnings("unused")
//						ResultLoadingDialog rld = new ResultLoadingDialog(
//								ResultLoadingDialog.TypeOfTransfert.LIST_ElementIdsThenStartPbthenStopPBLooped_TO_HOMO_BRO_VIEW,
//								// indexGenomePanelInLIST_GENOME_PANEL,
//								Insyght.APP_CONTROLER
//										.getCurrentRefGenomePanelAndListOrgaResult()
//										.getViewTypeInsyght(),
//								listElementIdsThenStartPbthenStopPBLoopedSent);
						RLD.transfertGenesFromListElementIdsThenStartPbthenStopPBLoopedToHomologBrowsing(listElementIdsThenStartPbthenStopPBLoopedSent, 
								Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght()
								);
					}
				});
	}
	

	public void addTransfertGeneToAnnotationsComparator(
			final int geneId,
			final ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent
			) {
		enableTransfertGeneToAnnotationsComparator(true);
		transfertGeneToAnnotationsComparator
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();
						ArrayList<Integer> alRefGeneIds = new ArrayList<Integer>();
						alRefGeneIds.add(geneId);
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getRefGeneSetForAnnotationsComparator()
								.setListGeneIds(alRefGeneIds);
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getRefGeneSetForAnnotationsComparator()
								.setListElementIdsThenStartPbthenStopPBLooped(
										listElementIdsThenStartPbthenStopPBLoopedSent);
						AnnotCompaViewImpl.listDisplayedGeneIdsAnnotCompaHasChanged = true;
						MainTabPanelView.tabPanel.selectTab(3);
					}
				});
	}
	

	public void addFindGeneInGenomicOrganizationView(
			final int geneId) {
		enableTransfertGeneToGenomicOrganizationView(true);
		transfertGeneToGenomicOrganizationView
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						
						// clear user selection
						Insyght.APP_CONTROLER.clearAllUserSelection();
					
						hide();
//						@SuppressWarnings("unused")
//						ResultLoadingDialog rld = new ResultLoadingDialog(
//								ResultLoadingDialog.TypeOfTransfert.GENE_ID_IN_GENOMIC_ORGANISATION,
//								geneId,
//								Insyght.APP_CONTROLER
//										.getCurrentRefGenomePanelAndListOrgaResult()
//										.getViewTypeInsyght());
						RLD.transfertGeneIdInGenomicOrgaView(geneId,
								Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght());
					}
				});
	}

	public void addFindGeneInGenomicOrganizationView(
			final int qOrigamiElementId, final int qPbStartQGeneInElement,
			final int qPbStopQGeneInElement
			// ,final int indexGenomePanelInLIST_GENOME_PANEL
	) {

		enableTransfertGeneToGenomicOrganizationView(true);
		transfertGeneToGenomicOrganizationView
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						
						// clear user selection
						Insyght.APP_CONTROLER.clearAllUserSelection();
					
						hide();
						Insyght.APP_CONTROLER.setLookUpForGeneForAllResults(
								qOrigamiElementId, qPbStartQGeneInElement,
								qPbStopQGeneInElement, EnumResultViewTypes.genomic_organization);
						//GenomicOrgaAndHomoTableViewImpl.changeToViewGenomicOrganisation();
						MainTabPanelView.getTabPanel().selectTab(4);
					}
				});
	}

	
	public void addTransfertGeneSetToHomologBrowsingView(
			final ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent
			//,final int indexGenomePanelInLIST_GENOME_PANEL,
			//final boolean isDisplayedRegion
			) {
		
		enableTransfertGeneSetToHomologBrowsingTable(true);
		// selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization
		transfertGeneSetToHomologBrowsingTable
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
								.drawLoading();
//						@SuppressWarnings("unused")
//						ResultLoadingDialog rld = new ResultLoadingDialog(
//								ResultLoadingDialog.TypeOfTransfert.LIST_ElementIdsThenStartPbthenStopPBLooped_TO_HOMO_BRO_VIEW,
//								// indexGenomePanelInLIST_GENOME_PANEL,
//								Insyght.APP_CONTROLER
//										.getCurrentRefGenomePanelAndListOrgaResult()
//										.getViewTypeInsyght(),
//								listElementIdsThenStartPbthenStopPBLoopedSent);
						RLD.transfertGenesFromListElementIdsThenStartPbthenStopPBLoopedToHomologBrowsing(
								listElementIdsThenStartPbthenStopPBLoopedSent, 
								Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght()
								);
					}
				});
	}


	public void addTransfertGenesSetFromSyntenyToHomologBrowsingView(
			final long mainOrigamiSyntenyId
	// ,final int indexGenomePanelInLIST_GENOME_PANEL
	) {
		// selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization.setEnabled(true);
		enableTransfertGeneSetToHomologBrowsingTable(true);

		// selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization
		transfertGeneSetToHomologBrowsingTable
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						// lbToAdd.addClickHandler(new ClickHandler() {
						// public void onClick(ClickEvent event) {
						hide();
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
								.drawLoading();
//						@SuppressWarnings("unused")
//						ResultLoadingDialog rld = new ResultLoadingDialog(
//								ResultLoadingDialog.TypeOfTransfert.SYNTENY_REGION_TO_HOMOLOG_BROWSING,
//								// indexGenomePanelInLIST_GENOME_PANEL,
//								Insyght.APP_CONTROLER
//										.getCurrentRefGenomePanelAndListOrgaResult()
//										.getViewTypeInsyght(),
//								mainOrigamiSyntenyId);
						RLD.transfertGenesOfSyntenyRegionToHomologBrowsing(
								mainOrigamiSyntenyId, 
								Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght()
								);
					}
				});
	}



	public void addTransfertGeneSetToAnnotationsComparator(
			final ArrayList<Integer> alRefGeneIds,
			final ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent
	// final int pbQStartInElement,
	// final int pbQStopInElement,
	// final int origamiElementId
	) {
		enableTransfertGeneSetToAnnotationsComparator(true);
		// selectedSymbolCompareHomologsAnnotation
		transfertGeneSetToAnnotationsComparator
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();

						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getRefGeneSetForAnnotationsComparator()
								.setListGeneIds(alRefGeneIds);
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getRefGeneSetForAnnotationsComparator()
								.setListElementIdsThenStartPbthenStopPBLooped(
										listElementIdsThenStartPbthenStopPBLoopedSent);
						AnnotCompaViewImpl.listDisplayedGeneIdsAnnotCompaHasChanged = true;
						MainTabPanelView.tabPanel.selectTab(3);

					}
				});
	}

	public void addTransfertGeneSetCompaAnnotToHomologBrowsingView(
			final RefGeneSetForAnnotCompa rgsfacIT) {
		enableTransfertGeneSetToHomologBrowsingTable(true);
		transfertGeneSetToHomologBrowsingTable
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();
						// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawLoading();
//						@SuppressWarnings("unused")
//						ResultLoadingDialog rld = new ResultLoadingDialog(
//								ResultLoadingDialog.TypeOfTransfert.GENE_SET_COMPA_ANNOT_TO_HOMOLOG_BROWSING,
//								Insyght.APP_CONTROLER
//										.getCurrentRefGenomePanelAndListOrgaResult()
//										.getViewTypeInsyght(),
//								// indexGenomePanelInLIST_GENOME_PANEL,
//								// Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght(),
//								rgsfacIT);
						RLD.transfertGenesSetToHomologBrowsing(
								Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght(),
								rgsfacIT
								);
					}
				});
	}
	
	public void addTransfertDisplayedRegionToHomologyBrowsingView(
			final ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent
			//,final int indexGenomePanelInLIST_GENOME_PANEL,
			//final boolean isDisplayedRegion
			) {

		enableTransfertDisplayedRegionToHomologBrowsingTable(true);
		// referenceGenomeTransferDisplayedRegionHomologBrowsingView
		transfertDisplayedRegionToHomologBrowsingTable
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
								.drawLoading();
//						@SuppressWarnings("unused")
//						ResultLoadingDialog rld = new ResultLoadingDialog(
//								ResultLoadingDialog.TypeOfTransfert.LIST_ElementIdsThenStartPbthenStopPBLooped_TO_HOMO_BRO_VIEW,
//								// indexGenomePanelInLIST_GENOME_PANEL,
//								Insyght.APP_CONTROLER
//										.getCurrentRefGenomePanelAndListOrgaResult()
//										.getViewTypeInsyght(),
//								listElementIdsThenStartPbthenStopPBLoopedSent);
						RLD.transfertGenesFromListElementIdsThenStartPbthenStopPBLoopedToHomologBrowsing(
								listElementIdsThenStartPbthenStopPBLoopedSent, 
								Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght()
								);
					}
				});

	}


	public void addTransfertDisplayedRegionToCompareHomologsAnnotation(
			final ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent) {
		enableTransfertDisplayedRegionToAnnotationsComparator(true);
		// referenceGenomeDisplayedRegionCompareHomologsAnnotation
		transfertDisplayedRegionToAnnotationsComparator
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getRefGeneSetForAnnotationsComparator()
								.setListGeneIds(null);
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getRefGeneSetForAnnotationsComparator()
								.setListElementIdsThenStartPbthenStopPBLooped(
										listElementIdsThenStartPbthenStopPBLoopedSent);
						AnnotCompaViewImpl.listDisplayedGeneIdsAnnotCompaHasChanged = true;
						MainTabPanelView.tabPanel.selectTab(3);

					}
				});
	}

	
	public void addListGenesInSelectedSymbol(
			//final ArrayList<LightGeneItem> alLgiSent,
			final long mainOrigamiSyntenyId,
			final ArrayList<Integer> alElementIdTenStartThenStopLooped
			){
		enableSelectedSymbolListGenes(true);
		selectedSymbolListGenes.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				hide();
				ResultLoadingDialog rld = new ResultLoadingDialog();
				rld.showGeneListInPopUp(mainOrigamiSyntenyId, alElementIdTenStartThenStopLooped);
			}
		});
	}
	
	public void addStatSummaryRefCDS(
			HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneName
			){
		enableStatSummaryRefCDS(true);
		statSummaryRefCDS.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				hide();
				ResultLoadingDialog rld = new ResultLoadingDialog();
				HashSet<Integer> hsOrgaIdsFeaturedGenomes = new HashSet<>();
				for (LightOrganismItem loiIT : Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults()) {
					hsOrgaIdsFeaturedGenomes.add(loiIT.getOrganismId());
				}
//				HashSet<Integer> hsOrgaIdsMainList = new HashSet<>();
//				for (LightOrganismItem loiIT : Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult()) {
//					hsOrgaIdsMainList.add(loiIT.getOrganismId());
//				}
				rld.showStatSummaryRefCDS(
						qOrganismId2qGeneId2qGeneName
						, hsOrgaIdsFeaturedGenomes
						//, hsOrgaIdsMainList
						);
			}
		});
	}

	public void addShowGenesInSyntenyRegion(
			final long mainOrigamiSyntenyId,
			final ArrayList<Long> listOrigamiAlignmentIdsContainedOtherSyntenies,
			final ArrayList<Long> listOrigamiAlignmentIdsMotherSyntenies,
			final ArrayList<Long> listOrigamiAlignmentIdsSprungOffSyntenies,
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay,
			final int indexGenomePanelInLIST_GENOME_PANEL,
			final boolean isContainedWithinAnotherMotherSynteny,
			final boolean isMotherOfContainedOtherSyntenies,
			final boolean isMotherOfSprungOffSyntenies,
			final boolean isSprungOffAnotherMotherSynteny,
			final int idxToDisplayInListHAPI) {

		enableSelectedSymbolExpandCollapseGenes(true);
		selectedSymbolExpandCollapseGenes
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();
						// clear user selection before deleting
						GenomePanel savedGP = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel();
						Insyght.APP_CONTROLER.clearAllUserSelection();
						// select genomePanel
						savedGP.selectThisGenomePanel();
						
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
								.drawLoading();
						
//						@SuppressWarnings("unused")
//						ResuLoadMoreAbsoPropItemDial rld = new ResuLoadMoreAbsoPropItemDial(
//								ResuLoadMoreAbsoPropItemDial.TypeOfRegion.SYNTENY_REGION,
//								indexInFullAssociatedListAbsoluteProportionItemToDisplay,
//								indexGenomePanelInLIST_GENOME_PANEL,
//								mainOrigamiSyntenyId,
//								listOrigamiAlignmentIdsContainedOtherSyntenies,
//								listOrigamiAlignmentIdsMotherSyntenies,
//								listOrigamiAlignmentIdsSprungOffSyntenies,
//								Insyght.APP_CONTROLER
//										.getCurrentRefGenomePanelAndListOrgaResult()
//										.getViewTypeInsyght(),
//								isContainedWithinAnotherMotherSynteny,
//								isMotherOfContainedOtherSyntenies,
//								isMotherOfSprungOffSyntenies,
//								isSprungOffAnotherMotherSynteny,
//								idxToDisplayInListHAPI);
						
						RLDAPID.getArrayListAbsoPropGenesSyntenyRegion(
								indexInFullAssociatedListAbsoluteProportionItemToDisplay,
								indexGenomePanelInLIST_GENOME_PANEL,
								mainOrigamiSyntenyId,
								listOrigamiAlignmentIdsContainedOtherSyntenies,
								listOrigamiAlignmentIdsMotherSyntenies,
								listOrigamiAlignmentIdsSprungOffSyntenies,
								Insyght.APP_CONTROLER
										.getCurrentRefGenomePanelAndListOrgaResult()
										.getViewTypeInsyght(),
								isContainedWithinAnotherMotherSynteny,
								isMotherOfContainedOtherSyntenies,
								isMotherOfSprungOffSyntenies,
								isSprungOffAnotherMotherSynteny,
								idxToDisplayInListHAPI);
						
						
					}
				});
	}

	public void addShowGenesInQGenomicRegionInsertion(final int startPb,
			final int stopPb, final int origamiElementId,
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay,
			final int indexGenomePanelInLIST_GENOME_PANEL,
			final String qAccnum, final int qPbStartOfElementInOrga,
			final int qSizeOfOragnismInPb) {
		enableSelectedSymbolExpandCollapseGenes(true);
		selectedSymbolExpandCollapseGenes
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						// lbToAdd.addClickHandler(new ClickHandler() {
						// public void onClick(ClickEvent event) {
						hide();
						
						// clear user selection before deleting
						GenomePanel savedGP = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel();
						Insyght.APP_CONTROLER.clearAllUserSelection();
						// select genomePanel
						savedGP.selectThisGenomePanel();
						
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
								.drawLoading();
//						@SuppressWarnings("unused")
//						ResuLoadMoreAbsoPropItemDial rld = new ResuLoadMoreAbsoPropItemDial(
//								ResuLoadMoreAbsoPropItemDial.TypeOfRegion.Q_REGION,
//								indexInFullAssociatedListAbsoluteProportionItemToDisplay,
//								indexGenomePanelInLIST_GENOME_PANEL,
//								origamiElementId,
//								startPb,
//								stopPb,
//								qAccnum,
//								qPbStartOfElementInOrga,
//								qSizeOfOragnismInPb,
//								null,
//								-1,
//								-1,
//								Insyght.APP_CONTROLER
//										.getCurrentRefGenomePanelAndListOrgaResult()
//										.getViewTypeInsyght(), false, false);
						
						RLDAPID.getArrayListAbsoPropGenesQRegion(
								indexInFullAssociatedListAbsoluteProportionItemToDisplay,
								indexGenomePanelInLIST_GENOME_PANEL,
								origamiElementId,
								startPb,
								stopPb,
								qAccnum,
								qPbStartOfElementInOrga,
								qSizeOfOragnismInPb,
								Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght());
						

					}
				});
	}

	public void addShowGenesInSGenomicRegionInsertion(final int startPb,
			final int stopPb, final int origamiElementId,
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay,
			final int indexGenomePanelInLIST_GENOME_PANEL,
			final String sAccnum, final int sPbStartOfElementInOrga,
			final int sSizeOfOragnismInPb, final boolean previousSOfNextSlice,
			final boolean nextSOfPreviousSlice) {
		enableSelectedSymbolExpandCollapseGenes(true);
		selectedSymbolExpandCollapseGenes
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						// lbToAdd.addClickHandler(new ClickHandler() {
						// public void onClick(ClickEvent event) {
						hide();
						
						// clear user selection before deleting
						GenomePanel savedGP = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel();
						Insyght.APP_CONTROLER.clearAllUserSelection();
						// select genomePanel
						savedGP.selectThisGenomePanel();
						
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
								.drawLoading();
//						@SuppressWarnings("unused")
//						ResuLoadMoreAbsoPropItemDial rld = new ResuLoadMoreAbsoPropItemDial(
//								ResuLoadMoreAbsoPropItemDial.TypeOfRegion.S_REGION,
//								indexInFullAssociatedListAbsoluteProportionItemToDisplay,
//								indexGenomePanelInLIST_GENOME_PANEL,
//								origamiElementId,
//								startPb,
//								stopPb,
//								null,
//								-1,
//								-1,
//								sAccnum,
//								sPbStartOfElementInOrga,
//								sSizeOfOragnismInPb,
//								Insyght.APP_CONTROLER
//										.getCurrentRefGenomePanelAndListOrgaResult()
//										.getViewTypeInsyght(),
//								previousSOfNextSlice, nextSOfPreviousSlice);
						
						RLDAPID.getArrayListAbsoPropGenesSRegion(
								indexInFullAssociatedListAbsoluteProportionItemToDisplay,
								indexGenomePanelInLIST_GENOME_PANEL,
								origamiElementId,
								startPb,
								stopPb,
								sAccnum,
								sPbStartOfElementInOrga,
								sSizeOfOragnismInPb,
								Insyght.APP_CONTROLER
										.getCurrentRefGenomePanelAndListOrgaResult()
										.getViewTypeInsyght(),
								previousSOfNextSlice,
								nextSOfPreviousSlice);
						
						
								
					}
				});
	}

	public void addHideGenesInSyntenyRegion(final int startIndexInFullArrayIT,
			final int stopIndexInFullArrayIT, final int displayIndexStartSent,
			final boolean doDeleteCutStartAsWell) {

		enableSelectedSymbolExpandCollapseGenes(true);
		selectedSymbolExpandCollapseGenes
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						// clear user selection before deleting
						GenomePanel savedGP = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel();
						Insyght.APP_CONTROLER.clearAllUserSelection();
						// select genomePanel
						savedGP.selectThisGenomePanel();

						// System.err.println("del");
						Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.deleteGeneHomologyItemsAtSpecifiedIndexInFullArray(
										startIndexInFullArrayIT,
										stopIndexInFullArrayIT,
										doDeleteCutStartAsWell);
						Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
										displayIndexStartSent,
										EnumResultViewTypes.genomic_organization
										);
						// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.restoreToOriginCoordinateSpace();
						// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.clearLowerPartCanvas(true);
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
								.drawViewToCanvas(1.0, 0, true, true, false,
										false);
						MainTabPanelView.CANVAS_MENU_POPUP.hide();

					}
				});
	}


	public void addShowOffShoots(
			// final int indexInAssociatedListAbsoluteProportionItemToDisplay,
			final int selectedSliceX,
			SuperHoldAbsoPropItem alhapiIT,
			// final int mainOrigamiSyntenyId,
			// final ArrayList<Integer>
			// listOrigamiAlignmentIdsContainedOtherSyntenies,
			// final ArrayList<Integer> listOrigamiAlignmentIdsMotherSyntenies,
			// final ArrayList<Integer>
			// listOrigamiAlignmentIdsSprungOffSyntenies,
			final int indexInFullAssociatedListAbsoluteProportionItemToDisplay,
			final int indexGenomePanelInLIST_GENOME_PANEL
			// final String qAccnum,
			// final int qPbStartOfElementInOrga, final int qSizeOfOragnismInPb,
			// final String sAccnum,
			// final int sPbStartOfElementInOrga, final int sSizeOfOragnismInPb,
			// final boolean isContainedWithinAnotherMotherSynteny, final
			// boolean isMotherOfContainedOtherSyntenies,
			// final boolean isMotherOfSprungOffSyntenies, final boolean
			// isSprungOffAnotherMotherSynteny
			, VerticalPanel vpRoot, ScrollPanel scRoot, final PopupPanel pop) {

		// System.err.println("alhapiIT.getListHAPI().size()="+alhapiIT.getListHAPI().size());

		for (int i = 0; i < alhapiIT.getListHAPI().size(); i++) {

			HolderAbsoluteProportionItem hapiIT = alhapiIT.getListHAPI().get(i);

			if (hapiIT.getAbsoluteProportionQComparableQSSpanItem() != null) {

				if (hapiIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {

					String geneName = "";
					final double qAbsolutePercentageStart = ((AbsoPropQSGeneHomoItem) hapiIT
							.getAbsoluteProportionQComparableQSSpanItem())
							.getqPercentStart();
					final double qAbsolutePercentageStop = ((AbsoPropQSGeneHomoItem) hapiIT
							.getAbsoluteProportionQComparableQSSpanItem())
							.getqPercentStop();
					final double sAbsolutePercentageStart = ((AbsoPropQSGeneHomoItem) hapiIT
							.getAbsoluteProportionQComparableQSSpanItem())
							.getsPercentStart();
					final double sAbsolutePercentageStop = ((AbsoPropQSGeneHomoItem) hapiIT
							.getAbsoluteProportionQComparableQSSpanItem())
							.getsPercentStop();
					geneName = ((AbsoPropQSGeneHomoItem) hapiIT
							.getAbsoluteProportionQComparableQSSpanItem())
							.getQsSMostSignificantGeneNameAsStrippedString();
					if (geneName.compareTo("NO_NAME") == 0) {
						geneName = "s_start="
								+ ((AbsoPropQSGeneHomoItem) hapiIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getQsSPbStartGeneInElement()
								+ " - s_stop="
								+ ((AbsoPropQSGeneHomoItem) hapiIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getQsSPbStopGeneInElement();
					}

					if (!geneName.isEmpty()) {
						final Label lbToAdd = new Label("Show other homolog: "
								+ geneName);
						lbToAdd.setStyleDependentName("MenuContextuel", true);
						lbToAdd.setWidth("100%");
						lbToAdd.addMouseOverHandler(new MouseOverHandler() {
							public void onMouseOver(MouseOverEvent event) {
								// System.out.println("over it...");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", true);
								if (qAbsolutePercentageStart >= Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getPercentSyntenyShowStartQGenome()
										&& qAbsolutePercentageStop <= Insyght.APP_CONTROLER
												.getCurrSelctGenomePanel()
												.getGenomePanelItem()
												.getPercentSyntenyShowStopQGenome()
										&& sAbsolutePercentageStart >= Insyght.APP_CONTROLER
												.getCurrSelctGenomePanel()
												.getGenomePanelItem()
												.getPercentSyntenyShowStartSGenome()
										&& sAbsolutePercentageStop <= Insyght.APP_CONTROLER
												.getCurrSelctGenomePanel()
												.getGenomePanelItem()
												.getPercentSyntenyShowStopSGenome()) {

								} else {
									lbToAdd.setStyleDependentName(
											"MenuContextuelOverItOutOfZoomRegion",
											true);
								}
							}
						});
						lbToAdd.addMouseOutHandler(new MouseOutHandler() {
							public void onMouseOut(MouseOutEvent event) {
								// System.out.println("addMouseOutHandler");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", false);
								if (qAbsolutePercentageStart >= Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getPercentSyntenyShowStartQGenome()
										&& qAbsolutePercentageStop <= Insyght.APP_CONTROLER
												.getCurrSelctGenomePanel()
												.getGenomePanelItem()
												.getPercentSyntenyShowStopQGenome()
										&& sAbsolutePercentageStart >= Insyght.APP_CONTROLER
												.getCurrSelctGenomePanel()
												.getGenomePanelItem()
												.getPercentSyntenyShowStartSGenome()
										&& sAbsolutePercentageStop <= Insyght.APP_CONTROLER
												.getCurrSelctGenomePanel()
												.getGenomePanelItem()
												.getPercentSyntenyShowStopSGenome()) {

								} else {
									lbToAdd.setStyleDependentName(
											"MenuContextuelOverItOutOfZoomRegion",
											false);
								}
							}
						});
						final int finalIdxI = i;
						lbToAdd.addClickHandler(new ClickHandler() {
							public void onClick(ClickEvent event) {
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectBlockSynteny(selectedSliceX,
								// false, false);
								//System.err.println("nvx idx :"+finalIdxI);
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
										.selectSymbol(selectedSliceX,
												false, true, finalIdxI, true,
												// false, false, false,
												false, false, false,
												false,
												false);
								// hide();
								pop.hide();
							}
						});
						vpRoot.add(lbToAdd);
						if (vpRoot.getWidgetCount() > MAX_COUNT) {
							scRoot.setHeight(MAX_HEIGHT);
						} else {
							scRoot.setHeight("100%");
						}
						continue;
					}
				} else if (hapiIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {

					AbsoPropQSSyntItem apqsiIT = ((AbsoPropQSSyntItem) hapiIT
							.getAbsoluteProportionQComparableQSSpanItem());
					final int qStart = apqsiIT.getPbQStartSyntenyInElement();
					final int qStop = apqsiIT.getPbQStopSyntenyInElement();
					final int sStart = apqsiIT.getPbSStartSyntenyInElement();
					final int sStop = apqsiIT.getPbSStopSyntenyInElement();
					String startPbStopPb = "q_start=" + qStart + ";q_stop="
							+ qStop + ";s_start=" + sStart + ";s_stop=" + sStop;
					final double qAbsolutePercentageStart = apqsiIT
							.getqPercentStart();
					final double qAbsolutePercentageStop = apqsiIT
							.getqPercentStop();
					final double sAbsolutePercentageStart = apqsiIT
							.getsPercentStart();
					final double sAbsolutePercentageStop = apqsiIT
							.getsPercentStop();

					final Label lbToAdd = new Label("Off shoot: "
							+ startPbStopPb);
					lbToAdd.setStyleDependentName("MenuContextuel", true);
					lbToAdd.setWidth("100%");
					final int finali = i;
					lbToAdd.addMouseOverHandler(new MouseOverHandler() {
						public void onMouseOver(MouseOverEvent event) {
							// System.out.println("over it...");
							lbToAdd.setStyleDependentName(
									"MenuContextuelOverIt", true);
							if (qAbsolutePercentageStart >= Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getPercentSyntenyShowStartQGenome()
									&& qAbsolutePercentageStop <= Insyght.APP_CONTROLER
											.getCurrSelctGenomePanel()
											.getGenomePanelItem()
											.getPercentSyntenyShowStopQGenome()
									&& sAbsolutePercentageStart >= Insyght.APP_CONTROLER
											.getCurrSelctGenomePanel()
											.getGenomePanelItem()
											.getPercentSyntenyShowStartSGenome()
									&& sAbsolutePercentageStop <= Insyght.APP_CONTROLER
											.getCurrSelctGenomePanel()
											.getGenomePanelItem()
											.getPercentSyntenyShowStopSGenome()) {

							} else {
								// System.err.println(qAbsolutePercentageStart);
								// System.err.println(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome());
								// System.err.println(qAbsolutePercentageStop);
								// System.err.println(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome());
								// System.err.println(sAbsolutePercentageStart);
								// System.err.println(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome());
								// System.err.println(sAbsolutePercentageStop);
								// System.err.println(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome());
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										true);
							}

							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.selectSymbol(selectedSliceX, false,
											true, finali, true, false, false,
											false, false, false);

						}
					});
					lbToAdd.addMouseOutHandler(new MouseOutHandler() {
						public void onMouseOut(MouseOutEvent event) {
							// System.out.println("addMouseOutHandler");
							lbToAdd.setStyleDependentName(
									"MenuContextuelOverIt", false);
							if (qAbsolutePercentageStart >= Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getPercentSyntenyShowStartQGenome()
									&& qAbsolutePercentageStop <= Insyght.APP_CONTROLER
											.getCurrSelctGenomePanel()
											.getGenomePanelItem()
											.getPercentSyntenyShowStopQGenome()
									&& sAbsolutePercentageStart >= Insyght.APP_CONTROLER
											.getCurrSelctGenomePanel()
											.getGenomePanelItem()
											.getPercentSyntenyShowStartSGenome()
									&& sAbsolutePercentageStop <= Insyght.APP_CONTROLER
											.getCurrSelctGenomePanel()
											.getGenomePanelItem()
											.getPercentSyntenyShowStopSGenome()) {

							} else {
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										false);
							}
							// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectBlockSynteny(selectedSliceX,
							// false, true, 0, true,
							// false, false, false);
							// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.clearMarkerBottom(true,
							// true, true);
							// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawSyntenyMarkerScaffoldAllDisplayedSlices(true,
							// false, true);
						}
					});
					lbToAdd.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							// event.stopPropagation();
							// hide();
							// if(qAbsolutePercentageStart >=
							// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()
							// && qAbsolutePercentageStop >=
							// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome()
							// && sAbsolutePercentageStart >=
							// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()
							// && sAbsolutePercentageStop >=
							// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome()){
							// //Do nothing
							// }else{
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.clearMarkerBottom(true, true, true);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawSyntenyMarkerScaffoldAllDisplayedSlices(
											true, false, true);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.selectSymbol(selectedSliceX, false,
											true, finali, true, false, false,
											false, true, false);

							// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawLoading();
							// @SuppressWarnings("unused")
							// ResultLoadingMoreAbsoluteProportionItemDialog rld
							// = new
							// ResultLoadingMoreAbsoluteProportionItemDialog(
							// ResultLoadingMoreAbsoluteProportionItemDialog.TypeOfRegion.SYNTENY_REGION,
							// indexInFullAssociatedListAbsoluteProportionItemToDisplay,
							// indexGenomePanelInLIST_GENOME_PANEL,
							// mainOrigamiSyntenyId,
							// listOrigamiAlignmentIdsContainedOtherSyntenies,
							// listOrigamiAlignmentIdsMotherSyntenies,
							// listOrigamiAlignmentIdsSprungOffSyntenies,
							// //qAccnum, qPbStartOfElementInOrga,
							// qSizeOfOragnismInPb,
							// //sAccnum, sPbStartOfElementInOrga,
							// sSizeOfOragnismInPb,
							// Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewType(),
							// isContainedWithinAnotherMotherSynteny,
							// isMotherOfContainedOtherSyntenies,
							// isMotherOfSprungOffSyntenies,
							// isSprungOffAnotherMotherSynteny
							// );

							// }
							pop.hide();

						}
					});
					vpRoot.add(lbToAdd);
					if (vpRoot.getWidgetCount() > MAX_COUNT) {
						scRoot.setHeight(MAX_HEIGHT);
					} else {
						scRoot.setHeight("100%");
					}
					continue;
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in Menu pop up addShowOtherMatchsOfSelectedSyntenyWholeQAndS : getAbsoluteProportionQComparableQSSpanItem not AbsoluteProportionQSSyntenyItem"));
				}

			}

			if (hapiIT.getAbsoluteProportionQComparableQSpanItem() != null) {
				if (hapiIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem
						&& hapiIT.getAbsoluteProportionQComparableQSpanItem()
								.getqEnumBlockType().toString()
								.startsWith("Q_MISSING_TARGET_S_")) {
					String geneName = "";
					// final double qAbsolutePercentageStart =
					// ((AbsoluteProportionQGeneInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getqPercentStart();
					// final double qAbsolutePercentageStop =
					// ((AbsoluteProportionQGeneInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getqPercentStop();
					geneName = ((AbsoPropQGeneInserItem) hapiIT
							.getAbsoluteProportionQComparableQSpanItem())
							.getIfMissingTargetSMostSignificantGeneNameAsStrippedString();
					if (geneName.compareTo("NO_NAME") == 0) {
						geneName = "q_start="
								+ ((AbsoPropQGeneInserItem) hapiIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqPbStartGeneInElement()
								+ " - q_stop="
								+ ((AbsoPropQGeneInserItem) hapiIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqPbStopGeneInElement();
					}

					if (!geneName.isEmpty()) {
						final Label lbToAdd = new Label("Show other homolog: "
								+ geneName);
						lbToAdd.setStyleDependentName("MenuContextuel", true);
						lbToAdd.setWidth("100%");
						lbToAdd.addMouseOverHandler(new MouseOverHandler() {
							public void onMouseOver(MouseOverEvent event) {
								// System.out.println("over it...");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", true);
								// if(qAbsolutePercentageStart >=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()
								// && qAbsolutePercentageStop <=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome()){
								//
								// }else{
								// lbToAdd.setStyleDependentName("MenuContextuelOverItOutOfZoomRegion",
								// true);
								// }
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										true);
							}
						});
						lbToAdd.addMouseOutHandler(new MouseOutHandler() {
							public void onMouseOut(MouseOutEvent event) {
								// System.out.println("addMouseOutHandler");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", false);
								// if(qAbsolutePercentageStart >=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()
								// && qAbsolutePercentageStop <=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome()){
								//
								// }else{
								// lbToAdd.setStyleDependentName("MenuContextuelOverItOutOfZoomRegion",
								// false);
								// }
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										false);
							}
						});
						final int finalIdxI = i;
						lbToAdd.addClickHandler(new ClickHandler() {
							public void onClick(ClickEvent event) {
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectBlockSynteny(selectedSliceX,
								// false, false);
								// System.err.println("nvx idx :"+finalIdxI);
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
										.selectSymbol(selectedSliceX,
												false, true, finalIdxI, true,
												// false, false, false,
												false, false, false, false,
												true);
								// hide();
								pop.hide();
							}
						});
						vpRoot.add(lbToAdd);
						if (vpRoot.getWidgetCount() > MAX_COUNT) {
							scRoot.setHeight(MAX_HEIGHT);
						} else {
							scRoot.setHeight("100%");
						}
						continue;
					}
				} else if (hapiIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem
						&& hapiIT.getAbsoluteProportionQComparableQSpanItem()
								.getqEnumBlockType().toString()
								.startsWith("Q_MISSING_TARGET_S_")) {

					// final double qAbsolutePercentageStart =
					// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getqPercentStart();
					// final double qAbsolutePercentageStop =
					// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getqPercentStop();

					String regionName = "q_start="
							+ ((AbsoPropQGenoRegiInserItem) hapiIT
									.getAbsoluteProportionQComparableQSpanItem())
									.getqPbStartQGenomicRegionInsertionInElement()
							+ " - q_stop="
							+ ((AbsoPropQGenoRegiInserItem) hapiIT
									.getAbsoluteProportionQComparableQSpanItem())
									.getqPbStopQGenomicRegionInsertionInElement();

					if (!regionName.isEmpty()) {
						final Label lbToAdd = new Label("Off shoot: "
								+ regionName);
						lbToAdd.setStyleDependentName("MenuContextuel", true);
						lbToAdd.setWidth("100%");
						lbToAdd.addMouseOverHandler(new MouseOverHandler() {
							public void onMouseOver(MouseOverEvent event) {
								// System.out.println("over it...");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", true);
								// if(qAbsolutePercentageStart >=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()
								// && qAbsolutePercentageStop <=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome()){
								//
								// }else{
								// lbToAdd.setStyleDependentName("MenuContextuelOverItOutOfZoomRegion",
								// true);
								// }
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										true);
							}
						});
						lbToAdd.addMouseOutHandler(new MouseOutHandler() {
							public void onMouseOut(MouseOutEvent event) {
								// System.out.println("addMouseOutHandler");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", false);
								// if(qAbsolutePercentageStart >=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()
								// && qAbsolutePercentageStop <=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome()){
								//
								// }else{
								// lbToAdd.setStyleDependentName("MenuContextuelOverItOutOfZoomRegion",
								// false);
								// }
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										false);
							}
						});
						final int finalIdxI = i;
						lbToAdd.addClickHandler(new ClickHandler() {
							public void onClick(ClickEvent event) {
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectBlockSynteny(selectedSliceX,
								// false, false);
								// System.err.println("nvx idx :"+finalIdxI);
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
										.selectSymbol(selectedSliceX,
												false, true, finalIdxI, true,
												// false, false, false,
												false, false, false, false,
												true);
								// hide();
								pop.hide();
							}
						});
						vpRoot.add(lbToAdd);
						if (vpRoot.getWidgetCount() > MAX_COUNT) {
							scRoot.setHeight(MAX_HEIGHT);
						} else {
							scRoot.setHeight("100%");
						}
						continue;
					}
				}

			}
			if (hapiIT.getAbsoluteProportionSComparableSSpanItem() != null) {
				if (hapiIT.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem
						&& hapiIT.getAbsoluteProportionSComparableSSpanItem()
								.getSEnumBlockType().toString()
								.startsWith("S_MISSING_TARGET_Q_")) {
					String geneName = "";
					// final double sAbsolutePercentageStart =
					// ((AbsoluteProportionSGeneInsertionItem)hapiIT.getAbsoluteProportionSComparableSSpanItem()).getsPercentStart();
					// final double sAbsolutePercentageStop =
					// ((AbsoluteProportionSGeneInsertionItem)hapiIT.getAbsoluteProportionSComparableSSpanItem()).getsPercentStop();
					geneName = ((AbsoPropSGeneInserItem) hapiIT
							.getAbsoluteProportionSComparableSSpanItem())
							.getsMostSignificantGeneNameAsStrippedString();
					if (geneName.compareTo("NO_NAME") == 0) {
						geneName = "s_start="
								+ ((AbsoPropSGeneInserItem) hapiIT
										.getAbsoluteProportionSComparableSSpanItem())
										.getsPbStartGeneInElement()
								+ " - s_stop="
								+ ((AbsoPropSGeneInserItem) hapiIT
										.getAbsoluteProportionSComparableSSpanItem())
										.getsPbStopGeneInElement();
					}

					if (!geneName.isEmpty()) {
						final Label lbToAdd = new Label("Show other homolog: "
								+ geneName);
						lbToAdd.setStyleDependentName("MenuContextuel", true);
						lbToAdd.setWidth("100%");
						lbToAdd.addMouseOverHandler(new MouseOverHandler() {
							public void onMouseOver(MouseOverEvent event) {
								// System.out.println("over it...");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", true);
								// if(sAbsolutePercentageStart >=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()
								// && sAbsolutePercentageStop <=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome()){
								//
								// }else{
								// lbToAdd.setStyleDependentName("MenuContextuelOverItOutOfZoomRegion",
								// true);
								// }
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										true);
							}
						});
						lbToAdd.addMouseOutHandler(new MouseOutHandler() {
							public void onMouseOut(MouseOutEvent event) {
								// System.out.println("addMouseOutHandler");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", false);
								// if(sAbsolutePercentageStart >=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()
								// && sAbsolutePercentageStop <=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome()){
								//
								// }else{
								// lbToAdd.setStyleDependentName("MenuContextuelOverItOutOfZoomRegion",
								// false);
								// }
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										false);
							}
						});
						final int finalIdxI = i;
						lbToAdd.addClickHandler(new ClickHandler() {
							public void onClick(ClickEvent event) {
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectBlockSynteny(selectedSliceX,
								// false, false);
								// System.err.println("nvx idx :"+finalIdxI);
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
										.selectSymbol(selectedSliceX,
												false, true, finalIdxI, true,
												// false, false, false,
												false, false, false, false,
												true);
								// hide();
								pop.hide();
							}
						});
						vpRoot.add(lbToAdd);
						if (vpRoot.getWidgetCount() > MAX_COUNT) {
							scRoot.setHeight(MAX_HEIGHT);
						} else {
							scRoot.setHeight("100%");
						}
						continue;
					}
				} else if (hapiIT.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem
						&& hapiIT.getAbsoluteProportionSComparableSSpanItem()
								.getSEnumBlockType().toString()
								.startsWith("S_MISSING_TARGET_Q_")) {

					// System.err.println("here");

					// final double sAbsolutePercentageStart =
					// ((AbsoluteProportionSGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionSComparableSSpanItem()).getsPercentStart();
					// final double sAbsolutePercentageStop =
					// ((AbsoluteProportionSGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionSComparableSSpanItem()).getsPercentStop();
					String regionName = "s_start="
							+ ((AbsoPropSGenoRegiInserItem) hapiIT
									.getAbsoluteProportionSComparableSSpanItem())
									.getsPbStartSGenomicRegionInsertionInElement()
							+ " - s_stop="
							+ ((AbsoPropSGenoRegiInserItem) hapiIT
									.getAbsoluteProportionSComparableSSpanItem())
									.getsPbStopSGenomicRegionInsertionInElement();

					if (!regionName.isEmpty()) {
						final Label lbToAdd = new Label("Off shoot: "
								+ regionName);
						lbToAdd.setStyleDependentName("MenuContextuel", true);
						lbToAdd.setWidth("100%");
						lbToAdd.addMouseOverHandler(new MouseOverHandler() {
							public void onMouseOver(MouseOverEvent event) {
								// System.out.println("over it...");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", true);
								// if(sAbsolutePercentageStart >=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()
								// && sAbsolutePercentageStop <=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome()){
								//
								// }else{
								// lbToAdd.setStyleDependentName("MenuContextuelOverItOutOfZoomRegion",
								// true);
								// }
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										true);
							}
						});
						lbToAdd.addMouseOutHandler(new MouseOutHandler() {
							public void onMouseOut(MouseOutEvent event) {
								// System.out.println("addMouseOutHandler");
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverIt", false);
								// if(sAbsolutePercentageStart >=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()
								// && sAbsolutePercentageStop <=
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome()){
								//
								// }else{
								// lbToAdd.setStyleDependentName("MenuContextuelOverItOutOfZoomRegion",
								// false);
								// }
								lbToAdd.setStyleDependentName(
										"MenuContextuelOverItOutOfZoomRegion",
										false);
							}
						});
						final int finalIdxI = i;
						lbToAdd.addClickHandler(new ClickHandler() {
							public void onClick(ClickEvent event) {
								// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectBlockSynteny(selectedSliceX,
								// false, false);
								// System.err.println("nvx idx :"+finalIdxI);
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
										.selectSymbol(selectedSliceX,
												false, true, finalIdxI, true,
												// false, false, false,
												true, false, false, false, true);
								// hide();
								pop.hide();
							}
						});
						vpRoot.add(lbToAdd);
						if (vpRoot.getWidgetCount() > MAX_COUNT) {
							scRoot.setHeight(MAX_HEIGHT);
						} else {
							scRoot.setHeight("100%");
						}
						continue;
					}
				}

			} else {
				//System.err.println("ERROR in CanvasMenuPopUp addShowOffShoots : not a qs, q or s item");
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in CanvasMenuPopUp addShowOffShoots : not a qs, q or s item"));
			}
		}
	}

	public void addFindGeneInQElement() {

		enableQuickNavigationFindGene(true);
		
		final ArrayList<AbsoPropQElemItem> alQElementItem = Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getReferenceGenomePanelItem()
				.getListAbsoluteProportionElementItemQ();

		quickNavigationFindGene.setScheduledCommand(new ScheduledCommand() {
			public void execute() {

				// clear user selection
				GenomePanel savedGP = Insyght.APP_CONTROLER
						.getCurrSelctGenomePanel();
				Insyght.APP_CONTROLER.clearAllUserSelection();
				// select genomePanel
				savedGP.selectThisGenomePanel();
				
				// System.err.println("new idx:"+finalNewIdx);
				hide();
				// Insyght.FIND_GENE_IN_ELEMENT_DIAG.initFindGeneInstance(organismSpeciesShort,
				// organismStrain, alQElementItem,
				// indexGenomePanelInLIST_GENOME_PANEL);
				GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.initFindGeneInListElementIds(alQElementItem);
				int clientWidth = Window.getClientWidth();
				int clientHeight = Window.getClientHeight();
				GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.rootPanelFindGene
						.setHeight(((3 * clientHeight) / 4) + "px");
				GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG.rootPanelFindGene
						.setWidth(((3 * clientWidth) / 4) + "px");
				GenoOrgaAndHomoTablViewImpl.LIST_GENES_DIAG
						.center();

			}
		});

	}

	public void addSynchronizeAllDisplaysOnReferenceGene(final String geneName,
			final int qOrigamiElementId, final int qPbStartQGeneInElement,
			final int qPbStopQGeneInElement
			//,final int indexGenomePanelInLIST_GENOME_PANEL
			) {

		enableQuickNavigationSynchronizeAllDisplaysOnReferenceGene(true);
		quickNavigationSynchronizeAllDisplaysOnReferenceGene
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						hide();
						PopUpListGenesDialog
								.centerAllResultPanelsOnThisGene(
										qOrigamiElementId,
										qPbStartQGeneInElement,
										qPbStopQGeneInElement);
					}
				});

	}

	public void addCenterDisplayAtClickPosition(
			final double absolutePrecentageCliqued) {
		// boolean q=true;
		// final Label lbToAdd = new Label("Center display at click position");
		// lbToAdd.setStyleDependentName("MenuContextuel", true);
		// lbToAdd.setWidth("100%");
		int newIdx = -1;
		// double smallestDiff = 1;
		// find startIdxIfclick
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getCurrSelctGenomePanel()
				.getGenomePanelItem()
				.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
				.size(); i++) {
			HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
					.getCurrSelctGenomePanel()
					.getGenomePanelItem()
					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
					.get(i)
					.getListHAPI()
					.get(Insyght.APP_CONTROLER
							.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(i).getIdxToDisplayInListHAPI());
			double absolutePrecentageStartIT = -1;
			double absolutePrecentageStopIT = -1;
			if (hapiIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
				absolutePrecentageStartIT = hapiIT
						.getAbsoluteProportionQComparableQSSpanItem()
						.getqPercentStart();
				absolutePrecentageStopIT = hapiIT
						.getAbsoluteProportionQComparableQSSpanItem()
						.getqPercentStop();
			} else if (hapiIT.getAbsoluteProportionQComparableQSpanItem() != null) {
				absolutePrecentageStartIT = hapiIT
						.getAbsoluteProportionQComparableQSpanItem()
						.getqPercentStart();
				absolutePrecentageStopIT = hapiIT
						.getAbsoluteProportionQComparableQSpanItem()
						.getqPercentStop();
			} else if (hapiIT.getAbsoluteProportionSComparableSSpanItem() != null) {
				continue;
			}
			// System.err.println("absolutePrecentageCliqued "+absolutePrecentageCliqued);
			// System.err.println("absolutePrecentageStartIT "+absolutePrecentageStartIT);
			// System.err.println("absolutePrecentageStopIT "+absolutePrecentageStopIT);
			if (absolutePrecentageCliqued < absolutePrecentageStartIT) {
				// before
				// System.err.println("before "+i);
				break;
			} else if (absolutePrecentageCliqued < absolutePrecentageStopIT) {
				// in the middle
				// System.err.println("middle"+i);
				newIdx = i;
				break;
			} else {
				// click is after
				// System.err.println("after "+i);
				newIdx = i;
				continue;
			}
		}

		if (newIdx < 0) {
			// Window.alert("Error in addCenterDisplayHere : newIdx < 0");
			// return;
			newIdx = 0;
		} else if (newIdx > Insyght.APP_CONTROLER
				.getCurrSelctGenomePanel()
				.getGenomePanelItem()
				.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
				.size()
				- SyntenyCanvas.NUMBER_SLICE_WIDTH) {
			// Window.alert("Error in addCenterDisplayHere : newIdx > getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()-1");
			// return;
			newIdx = Insyght.APP_CONTROLER
					.getCurrSelctGenomePanel()
					.getGenomePanelItem()
					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
					.size()
					- SyntenyCanvas.NUMBER_SLICE_WIDTH;
			if (newIdx < 0) {
				newIdx = 0;
			}
		}

		final int finalNewIdx = newIdx;

		enableQuickNavigationCenterDisplayAtClickPosition(true);
		quickNavigationCenterDisplayAtClickPosition
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {

						// System.err.println("new idx:"+finalNewIdx);

						if (Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght()
								.compareTo(EnumResultViewTypes.homolog_table) == 0) {
							// do all featured

							for (int i = 0; i < Insyght.APP_CONTROLER
									.getCurrentListFeaturedResultGenomePanelItem()
									.size(); i++) {
								GenomePanelItem gpiIT = Insyght.APP_CONTROLER
										.getCurrentListFeaturedResultGenomePanelItem()
										.get(i);
								gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(finalNewIdx,
										Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
							}
							// do all public
							for (int i = 0; i < Insyght.APP_CONTROLER
									.getCurrentListPublicResultGenomePanelItem()
									.size(); i++) {
								GenomePanelItem gpiIT = Insyght.APP_CONTROLER
										.getCurrentListPublicResultGenomePanelItem()
										.get(i);
								gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(finalNewIdx,
										Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
							}
							// do all private
							for (int i = 0; i < Insyght.APP_CONTROLER
									.getCurrentListPrivateResultGenomePanelItem()
									.size(); i++) {
								GenomePanelItem gpiIT = Insyght.APP_CONTROLER
										.getCurrentListPrivateResultGenomePanelItem()
										.get(i);
								gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(finalNewIdx,
										Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
							}
							// do ref genome
							Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getReferenceGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											finalNewIdx,
											Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());

							for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
									.getLIST_GENOME_PANEL().iterator(); i
									.hasNext();) {
								GenomePanel gpIT = i.next();
								if (gpIT.isVisible()) {
									gpIT.syntenyCanvas.drawViewToCanvas(1.0, 0,
											true, true, false, false);
								}
							}
							hide();

						} else {

							Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											finalNewIdx,
											Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawViewToCanvas(1.0, 0, true, true,
											false, false);
							hide();

						}

					}
				});
		// vpRoot.add(lbToAdd);
		// if(vpRoot.getWidgetCount() > MAX_COUNT){
		// scRoot.setHeight(MAX_HEIGHT);
		// }else{
		// scRoot.setHeight("100%");
		// }

	}
	

	public void addNavSymbolsNextGeneHomology() {

		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsNextGeneHomology.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				int newIdxToSet = -1;
				LOOP_addNavSymbolsNextGeneHomology: for (int j = Insyght.APP_CONTROLER
						.getCurrSelctGenomePanel()
						.getGenomePanelItem()
						.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + 1; j < Insyght.APP_CONTROLER
						.getCurrSelctGenomePanel()
						.getGenomePanelItem()
						.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
						.size(); j++) {
					for (int k = 0; k < Insyght.APP_CONTROLER
							.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(j).getListHAPI().size(); k++) {
						HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.get(j).getListHAPI().get(k);
						if (hapiIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
							if (hapiIT
									.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
								newIdxToSet = j;
								break LOOP_addNavSymbolsNextGeneHomology;
							}
						}
					}
				}
				if (newIdxToSet >= 0) {
					Insyght.APP_CONTROLER
							.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
									newIdxToSet,
									EnumResultViewTypes.genomic_organization);
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
							.drawViewToCanvas(1.0, 0, true, true, false, false);
					hide();
				} else {
					hide();
					Window.alert("Next gene homology not found");
				}
			}
		});

	}

	public void addNavSymbolsNextSyntenyRegion() {

		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsNextSyntenyRegion.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				int newIdxToSet = -1;
				LOOP_addNavSymbolsNextSyntenyRegion: for (int j = Insyght.APP_CONTROLER
						.getCurrSelctGenomePanel()
						.getGenomePanelItem()
						.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + 1; j < Insyght.APP_CONTROLER
						.getCurrSelctGenomePanel()
						.getGenomePanelItem()
						.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
						.size(); j++) {
					for (int k = 0; k < Insyght.APP_CONTROLER
							.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(j).getListHAPI().size(); k++) {
						HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.get(j).getListHAPI().get(k);
						if (hapiIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
							if (hapiIT
									.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
								newIdxToSet = j;
								break LOOP_addNavSymbolsNextSyntenyRegion;
							}
						}
					}
				}
				if (newIdxToSet >= 0) {
					Insyght.APP_CONTROLER
							.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
									newIdxToSet,
									EnumResultViewTypes.genomic_organization);
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
							.drawViewToCanvas(1.0, 0, true, true, false, false);
					hide();
				} else {
					hide();
					Window.alert("Next synteny not found");
				}
			}
		});

	}

	public void addNavSymbolsNextSyntenyRegion5Genes() {

		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsNextSyntenyRegion5Genes
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						int newIdxToSet = -1;
						LOOP_addNavSymbolsNextSyntenyRegion5Genes: for (int j = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + 1; j < Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.size(); j++) {
							for (int k = 0; k < Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(j).getListHAPI().size(); k++) {
								HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(j).getListHAPI().get(k);
								if (hapiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (hapiIT
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										if (((AbsoPropQSSyntItem) hapiIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyNumberGene() >= 3) {
											newIdxToSet = j;
											break LOOP_addNavSymbolsNextSyntenyRegion5Genes;
										}
									}
								}
							}
						}
						if (newIdxToSet >= 0) {
							Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											newIdxToSet,
											EnumResultViewTypes.genomic_organization);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawViewToCanvas(1.0, 0, true, true,
											false, false);
							hide();
						} else {
							hide();
							Window.alert("Next synteny > 3 not found");
						}
					}
				});

	}

	public void addNavSymbolsNextSyntenyRegion20Genes() {

		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsNextSyntenyRegion20Genes
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						int newIdxToSet = -1;
						LOOP_addNavSymbolsNextSyntenyRegion20Genes: for (int j = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + 1; j < Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.size(); j++) {
							for (int k = 0; k < Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(j).getListHAPI().size(); k++) {
								HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(j).getListHAPI().get(k);
								if (hapiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (hapiIT
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										if (((AbsoPropQSSyntItem) hapiIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyNumberGene() >= 20) {
											newIdxToSet = j;
											break LOOP_addNavSymbolsNextSyntenyRegion20Genes;
										}
									}
								}
							}
						}
						if (newIdxToSet >= 0) {
							Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											newIdxToSet,
											EnumResultViewTypes.genomic_organization);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawViewToCanvas(1.0, 0, true, true,
											false, false);
							hide();
						} else {
							hide();
							Window.alert("Next synteny > 20 not found");
						}
					}
				});

	}

	public void addNavSymbolsNextReferenceGeneGenomicRegionInsertion() {

		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsNextReferenceGeneGenomicRegionInsertion
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						int newIdxToSet = -1;
						LOOP_addNavSymbolsNextNextReferenceGeneGenomicRegionInsertion: for (int j = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + 1; j < Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.size(); j++) {
							for (int k = 0; k < Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(j).getListHAPI().size(); k++) {
								HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(j).getListHAPI().get(k);
								if (hapiIT
										.getAbsoluteProportionQComparableQSpanItem() != null) {
									if (hapiIT
											.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem
											|| hapiIT
													.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
										newIdxToSet = j;
										break LOOP_addNavSymbolsNextNextReferenceGeneGenomicRegionInsertion;
									}
								}
							}
						}
						if (newIdxToSet >= 0) {
							Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											newIdxToSet,
											EnumResultViewTypes.genomic_organization);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawViewToCanvas(1.0, 0, true, true,
											false, false);
							hide();
						} else {
							hide();
							Window.alert("Next Reference gene / genomic region insertion not found");
						}
					}
				});

	}

	public void addNavSymbolsPreviousGeneHomology() {

		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsPreviousGeneHomology
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						int newIdxToSet = -1;
						LOOP_addNavSymbolsPreviousGeneHomology: for (int j = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1; j >= 0; j--) {
							for (int k = 0; k < Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(j).getListHAPI().size(); k++) {
								HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(j).getListHAPI().get(k);
								if (hapiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (hapiIT
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
										newIdxToSet = j;
										break LOOP_addNavSymbolsPreviousGeneHomology;
									}
								}
							}
						}
						if (newIdxToSet >= 0) {
							Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											newIdxToSet,
											EnumResultViewTypes.genomic_organization);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawViewToCanvas(1.0, 0, true, true,
											false, false);
							hide();
						} else {
							hide();
							Window.alert("Previous gene homology not found");
						}
					}
				});
	}

	public void addNavSymbolsPreviousSyntenyRegion() {

		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsPreviousSyntenyRegion
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						int newIdxToSet = -1;
						LOOP_addNavSymbolsPreviousSyntenyRegion: for (int j = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1; j >= 0; j--) {
							for (int k = 0; k < Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(j).getListHAPI().size(); k++) {
								HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(j).getListHAPI().get(k);
								if (hapiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (hapiIT
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										newIdxToSet = j;
										break LOOP_addNavSymbolsPreviousSyntenyRegion;
									}
								}
							}
						}
						if (newIdxToSet >= 0) {
							Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											newIdxToSet,
											EnumResultViewTypes.genomic_organization);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawViewToCanvas(1.0, 0, true, true,
											false, false);
							hide();
						} else {
							hide();
							Window.alert("Previous synteny not found");
						}
					}
				});

	}

	public void addNavSymbolsPreviousSyntenyRegion5Genes() {

		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsPreviousSyntenyRegion5Genes
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						int newIdxToSet = -1;
						LOOP_addNavSymbolsPreviousSyntenyRegion5Genes: for (int j = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1; j >= 0; j--) {
							for (int k = 0; k < Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(j).getListHAPI().size(); k++) {
								HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(j).getListHAPI().get(k);
								if (hapiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (hapiIT
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										if (((AbsoPropQSSyntItem) hapiIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyNumberGene() >= 3) {
											newIdxToSet = j;
											break LOOP_addNavSymbolsPreviousSyntenyRegion5Genes;
										}
									}
								}
							}
						}
						if (newIdxToSet >= 0) {
							Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											newIdxToSet,
											EnumResultViewTypes.genomic_organization);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawViewToCanvas(1.0, 0, true, true,
											false, false);
							hide();
						} else {
							hide();
							Window.alert("Previous synteny > 3 not found");
						}
					}
				});

	}

	public void addNavSymbolsPreviousSyntenyRegion20Genes() {

		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsPreviousSyntenyRegion20Genes
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						int newIdxToSet = -1;
						LOOP_addNavSymbolsPreviousSyntenyRegion20Genes: for (int j = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1; j >= 0; j--) {
							for (int k = 0; k < Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(j).getListHAPI().size(); k++) {
								HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(j).getListHAPI().get(k);
								if (hapiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (hapiIT
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										if (((AbsoPropQSSyntItem) hapiIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyNumberGene() >= 20) {
											newIdxToSet = j;
											break LOOP_addNavSymbolsPreviousSyntenyRegion20Genes;
										}
									}
								}
							}
						}
						if (newIdxToSet >= 0) {
							Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											newIdxToSet,
											EnumResultViewTypes.genomic_organization);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawViewToCanvas(1.0, 0, true, true,
											false, false);
							hide();
						} else {
							hide();
							Window.alert("Previous synteny > 20 not found");
						}
					}
				});

	}

	public void addNavSymbolsPreviousReferenceGeneGenomicRegionInsertion() {
		// enableNavSymbolsNextGeneHomology(true);
		navSymbolsPreviousReferenceGeneGenomicRegionInsertion
				.setScheduledCommand(new ScheduledCommand() {
					public void execute() {
						int newIdxToSet = -1;
						LOOP_addNavSymbolsPreviousReferenceGeneGenomicRegionInsertion: for (int j = Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1; j >= 0; j--) {
							for (int k = 0; k < Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(j).getListHAPI().size(); k++) {
								HolderAbsoluteProportionItem hapiIT = Insyght.APP_CONTROLER
										.getCurrSelctGenomePanel()
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(j).getListHAPI().get(k);
								if (hapiIT
										.getAbsoluteProportionQComparableQSpanItem() != null) {
									if (hapiIT
											.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem
											|| hapiIT
													.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
										newIdxToSet = j;
										break LOOP_addNavSymbolsPreviousReferenceGeneGenomicRegionInsertion;
									}
								}
							}
						}
						if (newIdxToSet >= 0) {
							Insyght.APP_CONTROLER
									.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											newIdxToSet,
											EnumResultViewTypes.genomic_organization);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
									.drawViewToCanvas(1.0, 0, true, true,
											false, false);
							hide();
						} else {
							hide();
							Window.alert("Previous Reference gene / genomic region insertion not found");
						}
					}
				});
	}


	public void addToZoomIn(final String accnum,
			final double absolutePercentageStart,
			final double absolutePercentageStop, final boolean Q,
			VerticalPanel vpRoot, ScrollPanel scRoot, final PopupPanel pop) {
		final Label lbToAdd = new Label("Zoom in " + accnum);
		lbToAdd.setStyleDependentName("MenuContextuel", true);
		lbToAdd.setWidth("100%");
		double relativePercentageStop;
		double relativePercentageStart;
		if (Q) {
			relativePercentageStop = SyntenyCanvas.claculateRelativePercentage(
					Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getReferenceGenomePanelItem()
							.getListAbsoluteProportionElementItemQ().get(0)
							.getQsizeOfOragnismInPb(), absolutePercentageStop,
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.getPercentSyntenyShowStopQGenome(),
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.getPercentSyntenyShowStartQGenome());
			relativePercentageStart = SyntenyCanvas
					.claculateRelativePercentage(Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getReferenceGenomePanelItem()
							.getListAbsoluteProportionElementItemQ().get(0)
							.getQsizeOfOragnismInPb(), absolutePercentageStart,
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getPercentSyntenyShowStopQGenome(),
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getPercentSyntenyShowStartQGenome());
		} else {
			relativePercentageStop = SyntenyCanvas.claculateRelativePercentage(
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.getListAbsoluteProportionElementItemS().get(0)
							.getsSizeOfOragnismInPb(), absolutePercentageStop,
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.getPercentSyntenyShowStopSGenome(),
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel()
							.getGenomePanelItem()
							.getPercentSyntenyShowStartSGenome());
			relativePercentageStart = SyntenyCanvas
					.claculateRelativePercentage(Insyght.APP_CONTROLER
							.getCurrSelctGenomePanel().getGenomePanelItem()
							.getListAbsoluteProportionElementItemS().get(0)
							.getsSizeOfOragnismInPb(), absolutePercentageStart,
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getPercentSyntenyShowStopSGenome(),
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel()
									.getGenomePanelItem()
									.getPercentSyntenyShowStartSGenome());
		}
		final int mouseXPixelStart = (int) (SyntenyCanvas.SCAFFOLD_X_START_POINT + (SyntenyCanvas.TOTAL_LENGHT_SCAFFOLD * relativePercentageStart));
		final int mouseXPixelStop = (int) (SyntenyCanvas.SCAFFOLD_X_START_POINT + (SyntenyCanvas.TOTAL_LENGHT_SCAFFOLD * relativePercentageStop));
		lbToAdd.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				// System.out.println("over it...");
				lbToAdd.setStyleDependentName("MenuContextuelOverIt", true);
				if ((Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getViewTypeInsyght()
						.compareTo(EnumResultViewTypes.homolog_table) == 0 || SyntenyCanvas.SYNCHRONIZE_Q_ZOOMING)
						&& Q) {
					int mouseXPixelStartToPassAlong = mouseXPixelStart;
					int mouseXPixelStopToPassAlong = mouseXPixelStop;
					String accnumToPassAlong = accnum;
					double absolutePercentageStartToPassAlong = absolutePercentageStart;
					double absolutePercentageStopToPassAlong = absolutePercentageStop;
					for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
							.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
						GenomePanel gpIT = i.next();
						if (gpIT.isVisible()) {
							gpIT.getSyntenyCanvas().drawZoomPosition(false,
									1.0, false, false, true,
									mouseXPixelStartToPassAlong,
									mouseXPixelStopToPassAlong,
									absolutePercentageStartToPassAlong,
									absolutePercentageStopToPassAlong, true,
									"Zoom in " + accnumToPassAlong);
						}
					}
				} else {
					// just do this gp
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
							.drawZoomPosition(false, 1.0, false, false, Q,
									mouseXPixelStart, mouseXPixelStop,
									absolutePercentageStart,
									absolutePercentageStop, true, "Zoom in "
											+ accnum);
				}
			}
		});
		lbToAdd.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				// System.out.println("addMouseOutHandler");
				lbToAdd.setStyleDependentName("MenuContextuelOverIt", false);
				if ((Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getViewTypeInsyght()
						.compareTo(EnumResultViewTypes.homolog_table) == 0 || SyntenyCanvas.SYNCHRONIZE_Q_ZOOMING)
						&& Q) {
					// System.err.println("multi");
					for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
							.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
						GenomePanel gpIT = i.next();
						if (gpIT.isVisible()) {
							gpIT.getSyntenyCanvas()
									.redrawOnTopToClearScaffoldAndMarkerArea(
											true, true, true);
						}
					}
				} else {
					// System.err.println("uni");
					// just do this gp
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
							.redrawOnTopToClearScaffoldAndMarkerArea(Q, true,
									true);
				}
			}
		});
		lbToAdd.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				// MenuItem lbToAdd = new MenuItem("Zoom in "+accnum, new
				// Command() {
				// @Override
				// public void execute() {

				// Window.alert("zoom in "+accnum);
				if (mouseXPixelStart < SyntenyCanvas.SCAFFOLD_X_START_POINT
						|| mouseXPixelStop < SyntenyCanvas.SCAFFOLD_X_START_POINT
						|| mouseXPixelStart > SyntenyCanvas.SCAFFOLD_X_STOP_POINT
						|| mouseXPixelStop > SyntenyCanvas.SCAFFOLD_X_STOP_POINT) {
					// dezoom first
					// final int mouseXPixelStartAbsolute = (int)
					// (SyntenyCanvas.SCAFFOLD_X_START_POINT+(SyntenyCanvas.TOTAL_LENGHT_SCAFFOLD*absolutePercentageStart));
					// final int mouseXPixelStopAbsolute = (int)
					// (SyntenyCanvas.SCAFFOLD_X_START_POINT+(SyntenyCanvas.TOTAL_LENGHT_SCAFFOLD*absolutePercentageStop));
					// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.triggerZoomAction(true,
					// false, Q, mouseXPixelStartAbsolute,
					// mouseXPixelStopAbsolute, true);
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
							.launchZoomActionByPercent(true, false, Q,
									absolutePercentageStart,
									absolutePercentageStop, true);
				} else {
					// Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.triggerZoomAction(false,
					// false, Q, mouseXPixelStart, mouseXPixelStop, true);
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
							.launchZoomActionByPercent(false, false, Q,
									absolutePercentageStart,
									absolutePercentageStop, true);
				}
				pop.hide();
			}
		});

		vpRoot.add(lbToAdd);
		if (vpRoot.getWidgetCount() > MAX_COUNT) {
			scRoot.setHeight(MAX_HEIGHT);
		} else {
			scRoot.setHeight("100%");
		}

	}

	public void addReferenceGenomeZoomIn(final int left, final int top) {
		enableReferenceGenomeZoomIn(true);
		referenceGenomeZoomIn.setScheduledCommand(new ScheduledCommand() {
			@Override
			public void execute() {
				final PopupPanel pop = new PopupPanel(true);
				VerticalPanel vpRoot = new VerticalPanel();
				ScrollPanel scRoot = new ScrollPanel(vpRoot);
				for (int i = 0; i < Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getReferenceGenomePanelItem()
						.getListAbsoluteProportionElementItemQ().size(); i++) {
					AbsoPropQElemItem qapei = Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getReferenceGenomePanelItem()
							.getListAbsoluteProportionElementItemQ()
							.get(i);
					MainTabPanelView.CANVAS_MENU_POPUP.addToZoomIn(
							qapei.getqAccnum(),
							qapei.getqPercentStart(),
							qapei.getqPercentStop(), true, vpRoot,
							scRoot, pop);
				}
				pop.setWidget(scRoot);
				// pop.showRelativeTo(Insyght.CANVAS_MENU_POPUP);
				pop.setPopupPositionAndShow(new PositionCallback() {
					@Override
					public void setPosition(int offsetWidth,
							int offsetHeight) {
						pop.setPopupPosition(left, top);
					}
				});
				MainTabPanelView.CANVAS_MENU_POPUP.hide();
			}
		});

	}
	
	public void addReferenceGenomeZoomOut2X(final double currPercentStart,
			final double currPercentStop) {
		final boolean q = true;
		final int zoomOutScale = 2;
		double newPercentStart = currPercentStart / (double) zoomOutScale;
		if (newPercentStart < 0) {
			newPercentStart = 0;
		}
		double newPercentStop = currPercentStop
				+ ((((double) zoomOutScale - 1) * (1 - currPercentStop)) / (double) zoomOutScale);
		if (newPercentStop > 1) {
			newPercentStop = 1;
		}
		final double finalNewPercentStart = newPercentStart;
		final double finalNewPercentStop = newPercentStop;

		// lbToAdd.addClickHandler(new ClickHandler() {
		// public void onClick(ClickEvent event) {
		// referenceGenomeZoomOut2X.setEnabled(true);
		enableReferenceGenomeZoomOut2X(true);
		referenceGenomeZoomOut2X.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
						.launchZoomActionByPercent(false, true, q,
								finalNewPercentStart, finalNewPercentStop, true);
			}
		});

	}

	public void addReferenceGenomeZoomOut5X(final double currPercentStart,
			final double currPercentStop) {
		final boolean q = true;
		final int zoomOutScale = 5;
		double newPercentStart = currPercentStart / (double) zoomOutScale;
		if (newPercentStart < 0) {
			newPercentStart = 0;
		}
		double newPercentStop = currPercentStop
				+ ((((double) zoomOutScale - 1) * (1 - currPercentStop)) / (double) zoomOutScale);
		if (newPercentStop > 1) {
			newPercentStop = 1;
		}
		final double finalNewPercentStart = newPercentStart;
		final double finalNewPercentStop = newPercentStop;
		// referenceGenomeZoomOut5X.setEnabled(true);
		enableReferenceGenomeZoomOut5X(true);
		referenceGenomeZoomOut5X.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
						.launchZoomActionByPercent(false, true, q,
								finalNewPercentStart, finalNewPercentStop, true);
			}
		});
	}


	public void addReferenceGenomeZoomOutMax() {
		final boolean q = true;
		// final int zoomOutScale = 5;
		enableReferenceGenomeZoomOutMax(true);
		referenceGenomeZoomOutMax.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
						.launchZoomActionByPercent(false, true, q, 0.0, 1.0,
								true);
			}
		});

	}


	public void addComparedGenomeZoomOut2X(final double currPercentStart,
			final double currPercentStop) {
		final boolean q = false;
		final int zoomOutScale = 2;
		double newPercentStart = currPercentStart / (double) zoomOutScale;
		if (newPercentStart < 0) {
			newPercentStart = 0;
		}
		double newPercentStop = currPercentStop
				+ ((((double) zoomOutScale - 1) * (1 - currPercentStop)) / (double) zoomOutScale);
		if (newPercentStop > 1) {
			newPercentStop = 1;
		}
		final double finalNewPercentStart = newPercentStart;
		final double finalNewPercentStop = newPercentStop;
		// referenceGenomeZoomOut2X.setEnabled(true);
		enableComparedGenomeZoomOut2X(true);
		comparedGenomeZoomOut2X.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
						.launchZoomActionByPercent(false, true, q,
								finalNewPercentStart, finalNewPercentStop, true);
			}
		});
	}

	public void addComparedGenomeZoomOut5X(final double currPercentStart,
			final double currPercentStop) {
		final boolean q = false;
		final int zoomOutScale = 5;
		double newPercentStart = currPercentStart / (double) zoomOutScale;
		if (newPercentStart < 0) {
			newPercentStart = 0;
		}
		double newPercentStop = currPercentStop
				+ ((((double) zoomOutScale - 1) * (1 - currPercentStop)) / (double) zoomOutScale);
		if (newPercentStop > 1) {
			newPercentStop = 1;
		}
		final double finalNewPercentStart = newPercentStart;
		final double finalNewPercentStop = newPercentStop;
		// referenceGenomeZoomOut5X.setEnabled(true);
		enableComparedGenomeZoomOut5X(true);
		comparedGenomeZoomOut5X.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
						.launchZoomActionByPercent(false, true, q,
								finalNewPercentStart, finalNewPercentStop, true);
			}
		});
	}

	

	public void addComparedGenomeZoomOutMax() {
		final boolean q = false;
		// final int zoomOutScale = 5;
		enableComparedGenomeZoomOutMax(true);
		comparedGenomeZoomOutMax.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas
						.launchZoomActionByPercent(false, true, q, 0.0, 1.0,
								true);
			}
		});

	}

	public void addQuickNavigationSetComparedOrganismAsReference() {
		enableQuickNavigationSetComparedOrganismAsReference(true);
		quickNavigationSetComparedOrganismAsReference.setScheduledCommand(new ScheduledCommand() {
			public void execute() {

				String urlIT = NavigationControler.setNewRefOrgaAsURLParameters(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem());
				
				SearchItem siToSend = NavigationControler.getSearchItemFromUrlParams(urlIT);
				
				hide();
				@SuppressWarnings("unused")
				GetResultDialog resDiag = new GetResultDialog(siToSend, true);
			}
		});
	}
	

	public void addExportGeneSequence(final int refGeneId,
			final int compaGeneId) {
		enableExportGeneSequence(true);
		exportGeneSequence.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				hide();
				RLD.exportGeneSequences(
						Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getViewTypeInsyght(),
						refGeneId,
						compaGeneId);
			}
		});
	}
	
	
	public void addExportGeneSequence(
			final String refMostSignificantQGeneNameAsStrippedString,
			final String refMostSignificantQGeneLocusTag,
			final String refQGeneAccnum, final int refQGeneId,
			final String compMostSignificantSGeneNameAsStrippedString,
			final String compMostSignificantSGeneLocusTag,
			final String compSGeneAccnum, final int compSGeneId,
			final long syntenyOrigamiAlignmentId, final int syntenyNumberGenes,
			final int qOrigamiElementId,
			final int qPbStartQGenomicRegionInsertionInElement,
			final int qPbStopQGenomicRegionInsertionInElement,
			final int sOrigamiElementId,
			final int sPbStartSGenomicRegionInsertionInElement,
			final int sPbStopSGenomicRegionInsertionInElement,
			final ArrayList<Integer> refGeneSetIds) {
		enableExportGeneSequence(true);
		exportGeneSequence.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				hide();
				GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.displayPoPup(
						refMostSignificantQGeneNameAsStrippedString,
						refMostSignificantQGeneLocusTag, refQGeneAccnum,
						refQGeneId,
						compMostSignificantSGeneNameAsStrippedString,
						compMostSignificantSGeneLocusTag, compSGeneAccnum,
						compSGeneId, syntenyOrigamiAlignmentId,
						syntenyNumberGenes, qOrigamiElementId,
						qPbStartQGenomicRegionInsertionInElement,
						qPbStopQGenomicRegionInsertionInElement,
						sOrigamiElementId,
						sPbStartSGenomicRegionInsertionInElement,
						sPbStopSGenomicRegionInsertionInElement,
						refGeneSetIds
						);
				//GenoOrgaAndHomoTablViewImpl.EXPORT_CDS_SEQUENCE_DIAG.center();
			}
		});
	}

	
	
	public void addExportTable(){
		enableExportTable(true);
		exportTable.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				hide();
				final ExportTableDiag dlg = new ExportTableDiag();
				//dlg.center();
				dlg.displayPoPup();
			}
		});
	}
	
	
	public void addExportGraphics(){
		enableExportGraphics(true);
		exportGraphics.setScheduledCommand(new ScheduledCommand() {
			public void execute() {
				hide();
				Window.alert("To export the graphical representation as .png file, right-click on the graphics and choose \"Save as...\"");
			}
		});
	}
	
	
	/* public void enable ... */

	public void enableTransfertGeneToHomologBrowsingTable(boolean enable) {
		transfertGeneToHomologBrowsingTable.setStyleDependentName(
				"disabledMenuItem", !enable);
		transfertGeneToHomologBrowsingTable.setEnabled(enable);
	}

	public void enableTransfertGeneToAnnotationsComparator(boolean enable) {
		transfertGeneToAnnotationsComparator.setStyleDependentName(
				"disabledMenuItem", !enable);
		transfertGeneToAnnotationsComparator.setEnabled(enable);
	}

	public void enableTransfertGeneToGenomicOrganizationView(boolean enable) {
		transfertGeneToGenomicOrganizationView.setStyleDependentName(
				"disabledMenuItem", !enable);
		transfertGeneToGenomicOrganizationView.setEnabled(enable);
	}

	public void enableTransfertGeneSetToHomologBrowsingTable(boolean enable) {
		transfertGeneSetToHomologBrowsingTable.setStyleDependentName(
				"disabledMenuItem", !enable);
		transfertGeneSetToHomologBrowsingTable.setEnabled(enable);
	}

	public void enableTransfertGeneSetToAnnotationsComparator(boolean enable) {
		transfertGeneSetToAnnotationsComparator.setStyleDependentName(
				"disabledMenuItem", !enable);
		transfertGeneSetToAnnotationsComparator.setEnabled(enable);
	}

	public void enableTransfertDisplayedRegionToHomologBrowsingTable(
			boolean enable) {
		transfertDisplayedRegionToHomologBrowsingTable.setStyleDependentName(
				"disabledMenuItem", !enable);
		transfertDisplayedRegionToHomologBrowsingTable.setEnabled(enable);
	}

	public void enableTransfertDisplayedRegionToAnnotationsComparator(
			boolean enable) {
		transfertDisplayedRegionToAnnotationsComparator.setStyleDependentName(
				"disabledMenuItem", !enable);
		transfertDisplayedRegionToAnnotationsComparator.setEnabled(enable);
	}

	public void enableSelectedSymbolListGenes(boolean enable) {
		selectedSymbolListGenes.setStyleDependentName(
				"disabledMenuItem", !enable);
		selectedSymbolListGenes.setEnabled(enable);
	}
	
	public void enableStatSummaryRefCDS(boolean enable) {
		statSummaryRefCDS.setStyleDependentName(
				"disabledMenuItem", !enable);
		statSummaryRefCDS.setEnabled(enable);
	}
	
	
	public void enableSelectedSymbolExpandCollapseGenes(boolean enable) {
		selectedSymbolExpandCollapseGenes.setStyleDependentName(
				"disabledMenuItem", !enable);
		selectedSymbolExpandCollapseGenes.setEnabled(enable);
	}

	public void enableSelectedSymbolListOffShoots(boolean enable) {
		selectedSymbolListOffShoots.setStyleDependentName("disabledMenuItem",
				!enable);
		selectedSymbolListOffShoots.setEnabled(enable);
	}

	public void enableQuickNavigationFindGene(boolean enable) {
		quickNavigationFindGene.setStyleDependentName("disabledMenuItem",
				!enable);
		quickNavigationFindGene.setEnabled(enable);
	}

	
	
	public void enableQuickNavigationSynchronizeAllDisplaysOnReferenceGene(
			boolean enable) {
		quickNavigationSynchronizeAllDisplaysOnReferenceGene.setStyleDependentName(
				"disabledMenuItem", !enable);
		quickNavigationSynchronizeAllDisplaysOnReferenceGene.setEnabled(enable);
	}


	public void enableQuickNavigationCenterDisplayAtClickPosition(boolean enable) {
		quickNavigationCenterDisplayAtClickPosition.setStyleDependentName("disabledMenuItem",
				!enable);
		quickNavigationCenterDisplayAtClickPosition.setEnabled(enable);
	}

	public void enableNavSymbolsNext(boolean enable) {
		navSymbolsNext.setStyleDependentName("disabledMenuItem", !enable);
		navSymbolsNext.setEnabled(enable);
	}

	public void enableNavSymbolsPrevious(boolean enable) {
		navSymbolsPrevious.setStyleDependentName("disabledMenuItem", !enable);
		navSymbolsPrevious.setEnabled(enable);
	}
	
	public void enableReferenceGenomeZoomIn(boolean enable) {
		referenceGenomeZoomIn.setStyleDependentName("disabledMenuItem",
				!enable);
		referenceGenomeZoomIn.setEnabled(enable);
	}
	
	public void enableReferenceGenomeZoomOut2X(boolean enable) {
		referenceGenomeZoomOut2X.setStyleDependentName("disabledMenuItem",
				!enable);
		referenceGenomeZoomOut2X.setEnabled(enable);
	}
	
	public void enableReferenceGenomeZoomOut5X(boolean enable) {
		referenceGenomeZoomOut5X.setStyleDependentName("disabledMenuItem",
				!enable);
		referenceGenomeZoomOut5X.setEnabled(enable);
	}


	public void enableReferenceGenomeZoomOutMax(boolean enable) {
		referenceGenomeZoomOutMax.setStyleDependentName("disabledMenuItem",
				!enable);
		referenceGenomeZoomOutMax.setEnabled(enable);
	}

	public void enableComparedGenomeZoomIn(boolean enable) {
		comparedGenomeZoomIn.setStyleDependentName("disabledMenuItem",
				!enable);
		comparedGenomeZoomIn.setEnabled(enable);
	}
	
	public void enableComparedGenomeZoomOut2X(boolean enable) {
		comparedGenomeZoomOut2X.setStyleDependentName("disabledMenuItem",
				!enable);
		comparedGenomeZoomOut2X.setEnabled(enable);
	}
	
	public void enableComparedGenomeZoomOut5X(boolean enable) {
		comparedGenomeZoomOut5X.setStyleDependentName("disabledMenuItem",
				!enable);
		comparedGenomeZoomOut5X.setEnabled(enable);
	}


	public void enableComparedGenomeZoomOutMax(boolean enable) {
		comparedGenomeZoomOutMax.setStyleDependentName("disabledMenuItem",
				!enable);
		comparedGenomeZoomOutMax.setEnabled(enable);
	}

	public void enableQuickNavigationSetComparedOrganismAsReference(boolean enable) {
		quickNavigationSetComparedOrganismAsReference.setStyleDependentName("disabledMenuItem",
				!enable);
		quickNavigationSetComparedOrganismAsReference.setEnabled(enable);
	}
	
	public void enableExportGeneSequence(boolean enable) {
		exportGeneSequence.setStyleDependentName("disabledMenuItem", !enable);
		exportGeneSequence.setEnabled(enable);
	}

	
	public void enableExportTable(boolean enable) {
		exportTable.setStyleDependentName("disabledMenuItem", !enable);
		exportTable.setEnabled(enable);
	}
	
	
	public void enableExportGraphics(boolean enable) {
		exportGraphics.setStyleDependentName("disabledMenuItem", !enable);
		exportGraphics.setEnabled(enable);
	}
	
	public void disableAllLeafs() {
		
		enableTransfertGeneToHomologBrowsingTable(false);
		enableTransfertGeneToAnnotationsComparator(false);
		enableTransfertGeneToGenomicOrganizationView(false);

		enableTransfertGeneSetToHomologBrowsingTable(false);
		enableTransfertGeneSetToAnnotationsComparator(false);

		enableTransfertDisplayedRegionToHomologBrowsingTable(false);
		enableTransfertDisplayedRegionToAnnotationsComparator(false);
		
		enableSelectedSymbolListGenes(false);
		enableSelectedSymbolExpandCollapseGenes(false);
		enableSelectedSymbolListOffShoots(false);
		enableStatSummaryRefCDS(false);

		enableQuickNavigationFindGene(false);
		enableQuickNavigationSynchronizeAllDisplaysOnReferenceGene(false);
		enableQuickNavigationCenterDisplayAtClickPosition(false);
		enableNavSymbolsNext(false);
		enableNavSymbolsPrevious(false);
		
		enableReferenceGenomeZoomIn(false);
		enableReferenceGenomeZoomOut2X(false);
		enableReferenceGenomeZoomOut5X(false);
		enableReferenceGenomeZoomOutMax(false);
		
		enableComparedGenomeZoomIn(false);
		enableComparedGenomeZoomOut2X(false);
		enableComparedGenomeZoomOut5X(false);
		enableComparedGenomeZoomOutMax(false);
		
		enableQuickNavigationSetComparedOrganismAsReference(false);
		
		enableExportGeneSequence(false);
		enableExportTable(false);
		enableExportGraphics(false);
		
		
	}

}
