package fr.inra.jouy.client.view;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
//import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.NavigationControler;
import fr.inra.jouy.client.view.admin.AdminTab;
import fr.inra.jouy.client.view.crossSections.popUp.ManageListComparedOrganisms;
import fr.inra.jouy.client.view.header.HeaderView;
import fr.inra.jouy.client.view.home.HomeViewImpl;
import fr.inra.jouy.client.view.result.AnnotCompaViewImpl;
import fr.inra.jouy.client.view.result.CanvasMenuPopUp;
import fr.inra.jouy.client.view.result.CenterSLPAllGenomePanels;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
import fr.inra.jouy.client.view.result.SyntenyCanvas;
import fr.inra.jouy.client.view.result.FakeViewImpl;
import fr.inra.jouy.client.view.search.SearchViewImpl;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;

public class MainTabPanelView extends ResizeComposite {

	private static MainTabPanelViewUiBinder uiBinder = GWT
			.create(MainTabPanelViewUiBinder.class);

	interface MainTabPanelViewUiBinder extends
			UiBinder<Widget, MainTabPanelView> {
	}

	@UiField
	public static TabLayoutPanel tabPanel;
	public static boolean postponeResizeOfAnnotCompa = false;
	
	public static TabLayoutPanel getTabPanel() {
		return tabPanel;
	}

	public static void setTabPanel(TabLayoutPanel tabPanel) {
		MainTabPanelView.tabPanel = tabPanel;
	}



	//cross section instance
	public static ManageListComparedOrganisms MLCO = new ManageListComparedOrganisms();
	public static CanvasMenuPopUp CANVAS_MENU_POPUP = new CanvasMenuPopUp();
	
			
	public MainTabPanelView() {
		initWidget(uiBinder.createAndBindUi(this));	
		tabPanel.add(new HomeViewImpl(), "HOME");
		Insyght.HOME_VIEW_LOADED = true;
		Scheduler.get().scheduleDeferred(
				new ScheduledCommand() {
					// @Override
					public void execute() {
						GWT.runAsync(new RunAsyncCallback() {
					          public void onFailure(Throwable caught) {
					        	  Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
					          }

					          public void onSuccess() {
					        	  
					        	HeaderView.loadHeaderView();
					        	
					        	tabPanel.add(new SearchViewImpl(), "LOADING, PLEASE WAIT...");//SEARCH
					        	Insyght.SEARCH_VIEW_LOADED = true;
					        	
					      		tabPanel.add(new GenoOrgaAndHomoTablViewImpl(), "<span style=\"color:#8B3A3A;\">ORTHOLOGS TABLE</span>", true);//#8B008B
					      		Insyght.GENOMIC_ORGA_AND_HOMO_TABLE_VIEW_LOADED = true;
					      		
					      		tabPanel.add(new AnnotCompaViewImpl(), "<span style=\"color:#218868;\">ANNOTATIONS COMPARATOR</span>", true);//#006400
					      		Insyght.ANNOT_COMPA_VIEW_LOADED = true;

					      		tabPanel.add(new FakeViewImpl(), "<span style=\"color:#7A378B;\">GENOMIC ORGANIZATION</span>", true);//#8B2323
					      		//Insyght.FAKE_VIEW_LOADED = true;
					        	  
					    		tabPanel.add(new AdminTab(), "ADMIN");
					    		Insyght.ADMIN_VIEW_LOADED = true;
					    		
					    		//hide tab at first
					    		//tabPanel.getTabWidget(1).setVisible(false);
					    		tabPanel.getTabWidget(2).setVisible(false);
					    		tabPanel.getTabWidget(3).setVisible(false);
					    		tabPanel.getTabWidget(4).setVisible(false);
					    		tabPanel.getTabWidget(5).setVisible(false);
					    		//AdminView.loadAdminView();
					    		

					    		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
					    			public void onSelection(SelectionEvent<Integer> event) {
					    				if (tabPanel.getSelectedIndex() == 0) {
					    					//NavigationControler.newItem(NavigationControler.getTockenFromChoosenOptions(), false);
					    				}else if (tabPanel.getSelectedIndex() == 1) {
					    					//SEARCH
					    					NavigationControler.newItem(NavigationControler.getURLTockenFromCurrentState(), false);
					    					Scheduler.get().scheduleDeferred(
					    							new ScheduledCommand() {
					    								// @Override
					    								public void execute() {
					    									SearchViewImpl.sugBox.setFocus(true);
					    								}
					    							});
					    				}else if (tabPanel.getSelectedIndex() == 2) {
					    					//ORTHOLOGS TABLE
					    					
					    					if (Insyght.APP_CONTROLER
					    							.getCurrentRefGenomePanelAndListOrgaResult() == null) {
					    						//SearchViewImpl.errorOrWarningTextStepSelectOrga.setText("No result to display. Please submit your search first.");
					    						SearchViewImpl.showCorrectStepSearchView("ReferenceGenomeOrTaxonomicGroup", false);
					    						tabPanel.selectTab(1);
					    						SearchViewImpl.submitSearch(RefGenoPanelAndListOrgaResu.EnumResultViewTypes.homolog_table);
					    					} else {
					    						if (Insyght.APP_CONTROLER
					    								.getCurrentRefGenomePanelAndListOrgaResult()
					    								.getReferenceGenomePanelItem() == null) {
					    							//Window.alert("No result to display. Please perform a search first.");
					    							//SearchViewImpl.errorOrWarningTextStepSelectOrga.setText("No result to display. Please submit your search first.");
						    						SearchViewImpl.showCorrectStepSearchView("ReferenceGenomeOrTaxonomicGroup", false);
					    							tabPanel.selectTab(1);
					    							//SearchViewImpl.selectOrganismItem(Insyght.APP_CONTROLER.getDefaultOrganismIfNoSelection());
						    						SearchViewImpl.submitSearch(RefGenoPanelAndListOrgaResu.EnumResultViewTypes.homolog_table);
					    						}else{
					    							
					    							if(tabPanel.getWidget(2) instanceof FakeViewImpl){
					    								//show widget GenomicOrgaAndHomoTableViewImpl as ORTHOLOGS TABLE
					    								
					    								if(tabPanel.getWidget(4) instanceof GenoOrgaAndHomoTablViewImpl){
					    									tabPanel.insert(tabPanel.getWidget(4), "<span style=\"color:#8B3A3A;\">ORTHOLOGS TABLE</span>", true, 2);
					    									tabPanel.insert(tabPanel.getWidget(3), "<span style=\"color:#7A378B;\">GENOMIC ORGANIZATION</span>", true, 5);
				
							    							tabPanel.selectTab(2);
					    								}else{
					    									Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in MainTabPanelView: tabPanel.getWidget(4) not instanceof GenomicOrgaAndHomoTableViewImpl"));
					    								}
						    							
					    							}else{
					    								//clear user selection
					    								Insyght.APP_CONTROLER.clearAllUserSelection();
					    								GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
					    								//Needed for GWT2.7 and less
//					    								Scheduler.get().scheduleDeferred(new ScheduledCommand() {
//					    											// @Override
//					    											public void execute() {
					    												if (CenterSLPAllGenomePanels.RESIZE_RESULTP_WHEN_SLCTED) {
					    													//GWT.log("tab 2 RESIZE_RESULTP_WHEN_SLCTED");
					    													GenoOrgaAndHomoTablViewImpl.changeToViewHomologTable(false);
					    													CenterSLPAllGenomePanels.getCurrPxSizeAndUpdateDisplayAccordingly();
					    												}else{
					    													
//					    													GWT.log("tab 2 NOT !!! RESIZE_RESULTP_WHEN_SLCTED"
//					    															+ " ; FIRST_RESULT_LOADED="+GenoOrgaAndHomoTablViewImpl.FIRST_RESULT_LOADED
//					    															+ " ; TOTAL_CANVAS_WIDTH="+SyntenyCanvas.TOTAL_CANVAS_WIDTH
//					    															);
					    													// do not update display twice on first loading
					    													if (SyntenyCanvas.TOTAL_CANVAS_WIDTH <= 0) {
					    														//first time loading
					    														GenoOrgaAndHomoTablViewImpl.changeToViewHomologTable(false);
					    													} else {
					    														GenoOrgaAndHomoTablViewImpl.changeToViewHomologTable(true);
					    													}
					    													
					    												}
//					    											}
//					    								});
					    							}
					    						}
					    					}
					    				}else if (tabPanel.getSelectedIndex() == 3) {
					    					//ANNOTATIONS COMPARATOR
					    					
					    					if (Insyght.APP_CONTROLER
					    							.getCurrentRefGenomePanelAndListOrgaResult() == null) {
					    						//SearchViewImpl.errorOrWarningTextStepSelectOrga.setText("No result to display. Please submit your search first.");
					    						SearchViewImpl.showCorrectStepSearchView("ReferenceGenomeOrTaxonomicGroup", false);
					    						tabPanel.selectTab(1);
					    						//SearchViewImpl.selectOrganismItem(Insyght.APP_CONTROLER.getDefaultOrganismIfNoSelection());
					    						SearchViewImpl.submitSearch(EnumResultViewTypes.annotations_comparator);
					    					} else {
					    						if (Insyght.APP_CONTROLER
					    								.getCurrentRefGenomePanelAndListOrgaResult()
					    								.getReferenceGenomePanelItem() == null) {
					    							//Window.alert("No result to display. Please perform a search first.");
					    							//SearchViewImpl.errorOrWarningTextStepSelectOrga.setText("No result to display. Please submit your search first.");
						    						SearchViewImpl.showCorrectStepSearchView("ReferenceGenomeOrTaxonomicGroup", false);
					    							tabPanel.selectTab(1);
					    							//SearchViewImpl.selectOrganismItem(Insyght.APP_CONTROLER.getDefaultOrganismIfNoSelection());
						    						SearchViewImpl.submitSearch(EnumResultViewTypes.annotations_comparator);
					    						}else{
					    							//clear user selection
				    								Insyght.APP_CONTROLER.clearAllUserSelection();
				    								Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().setViewTypeInsyght(EnumResultViewTypes.annotations_comparator);
				    								AnnotCompaViewImpl.loadResults();
				    								if(postponeResizeOfAnnotCompa){
				    									((AnnotCompaViewImpl)tabPanel.getWidget(3)).onResize();
				    									postponeResizeOfAnnotCompa = false;
				    								}
				    								
				    								NavigationControler.newItem(NavigationControler.getURLTockenFromCurrentState(), false);
					    						}
					    					}
					    				}else if (tabPanel.getSelectedIndex() == 4) {
					    					//GENOMIC ORGANIZATION
					    					if (Insyght.APP_CONTROLER
					    							.getCurrentRefGenomePanelAndListOrgaResult() == null) {
					    						//SearchViewImpl.errorOrWarningTextStepSelectOrga.setText("No result to display. Please submit your search first.");
					    						SearchViewImpl.showCorrectStepSearchView("ReferenceGenomeOrTaxonomicGroup", false);
					    						tabPanel.selectTab(1);
					    						//SearchViewImpl.selectOrganismItem(Insyght.APP_CONTROLER.getDefaultOrganismIfNoSelection());
					    						SearchViewImpl.submitSearch(EnumResultViewTypes.genomic_organization);
					    					} else {
					    						if (Insyght.APP_CONTROLER
					    								.getCurrentRefGenomePanelAndListOrgaResult()
					    								.getReferenceGenomePanelItem() == null) {
					    							//Window.alert("No result to display. Please perform a search first.");
					    							//SearchViewImpl.errorOrWarningTextStepSelectOrga.setText("No result to display. Please submit your search first.");
						    						SearchViewImpl.showCorrectStepSearchView("ReferenceGenomeOrTaxonomicGroup", false);
					    							tabPanel.selectTab(1);
					    							//SearchViewImpl.selectOrganismItem(Insyght.APP_CONTROLER.getDefaultOrganismIfNoSelection());
						    						SearchViewImpl.submitSearch(EnumResultViewTypes.genomic_organization);
					    						} else {
					    							if(tabPanel.getWidget(4) instanceof FakeViewImpl){
					    								//show widget GenomicOrgaAndHomoTableViewImpl as GENOMIC ORGANIZATION
					    								
					    								if(tabPanel.getWidget(2) instanceof GenoOrgaAndHomoTablViewImpl){
					    									
					    									tabPanel.insert(tabPanel.getWidget(2), "<span style=\"color:#7A378B;\">GENOMIC ORGANIZATION</span>", true, 5);
					    									tabPanel.insert(tabPanel.getWidget(3), "<span style=\"color:#8B3A3A;\">ORTHOLOGS TABLE</span>", true, 2);
							    							tabPanel.selectTab(4);
					    								}else{
					    									Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in MainTabPanelView: tabPanel.getWidget(4) not instanceof GenomicOrgaAndHomoTableViewImpl"));
					    								}
						    							
					    							}else{
					    								Insyght.APP_CONTROLER.clearAllUserSelection();
					    								GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
					    								//Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().setViewTypeInsyght(EnumResultViewTypes.genomic_organization);
					    								//Needed for GWT2.7 and less
//					    								Scheduler.get().scheduleDeferred(new ScheduledCommand() {
//					    											// @Override
//					    											public void execute() {
					    												if (CenterSLPAllGenomePanels.RESIZE_RESULTP_WHEN_SLCTED) {
					    													//GWT.log("tab 4 RESIZE_RESULTP_WHEN_SLCTED");
					    													GenoOrgaAndHomoTablViewImpl.changeToViewGenomicOrganisation(false);
					    													CenterSLPAllGenomePanels.getCurrPxSizeAndUpdateDisplayAccordingly();
					    												}else{
					    													//GWT.log("tab 4 NOT !!!! RESIZE_RESULTP_WHEN_SLCTED");
					    													// do not update display twice on first loading
					    													if (SyntenyCanvas.TOTAL_CANVAS_WIDTH <= 0) {
					    														//first time loading
					    														GenoOrgaAndHomoTablViewImpl.changeToViewGenomicOrganisation(false);
					    													} else {
					    														GenoOrgaAndHomoTablViewImpl.changeToViewGenomicOrganisation(true);
					    													}
					    												}
					    											
//					    											}
//					    								});
					    								
					    								
					    							}
					    						}
					    					}
					    				}
					    			}
					    		});
					          }
					    });
					}
				});

		
		// dpRootResult.showWidget(0);
	}

	
	

}
