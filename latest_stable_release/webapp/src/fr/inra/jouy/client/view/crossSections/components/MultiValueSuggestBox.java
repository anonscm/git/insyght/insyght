package fr.inra.jouy.client.view.crossSections.components;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.view.search.BulletList;
import fr.inra.jouy.client.view.search.ListItem;
import fr.inra.jouy.client.view.search.Paragraph;
import fr.inra.jouy.client.view.search.Span;

public class MultiValueSuggestBox extends Composite {
	ArrayList<String> itemsSelected = new ArrayList<String>();
	ArrayList<ListItem> itemsHighlighted = new ArrayList<ListItem>();
	private final TextBox itemBox = new TextBox();
	private final BulletList list = new BulletList();
	private String color = "blue";
	private int maxCountInList = -1;
	
    public MultiValueSuggestBox(MultiWordSuggestOracle suggestions, final String colorSent, final String width, int maxCountInListSent) {
    	color = colorSent;
    	maxCountInList = maxCountInListSent;
    			
    	if(maxCountInList < 4){
    		maxCountInList = 4;
    	}
    	
    	String uniqueId = DOM.createUniqueId();
    	//System.out.println("uniqueId = "+uniqueId);
    	
        FlowPanel panel = new FlowPanel();
        initWidget(panel);
        // 2. Show the following element structure and set the last <div> to
        // display: block
        /*
         * <ul class="multiValueSuggestBox-list"> <li
         * class="multiValueSuggestBox-input-token"> <input type="text" style=
         * "outline-color: -moz-use-text-color; outline-style: none; outline-width: medium;"
         * /> </li> </ul> <div class="multiValueSuggestBox-dropdown"
         * style="display: none;"/>
         */
        
        list.setStyleName("multiValueSuggestBox-list");
        list.setWidth(width);
        final ListItem item = new ListItem();
        item.setStyleName("multiValueSuggestBox-input-token");
        
        itemBox.getElement().setAttribute("style",
                "outline-color: -moz-use-text-color; outline-style: none; outline-width: medium;");
        final SuggestBox box = new SuggestBox(suggestions, itemBox);
        box.getElement().setId("suggestion_box"+uniqueId);
        item.add(box);
        list.add(item);
 
        // this needs to be on the itemBox rather than box, or backspace will
        // get executed twice
        itemBox.addKeyDownHandler(new KeyDownHandler() {
            // handle key events on the suggest box
            public void onKeyDown(KeyDownEvent event) {
                switch (event.getNativeKeyCode()) {
//                    case KeyCodes.KEY_ENTER:
//                        // only allow manual entries with @ signs (assumed email
//                        // addresses)
//                        if (itemBox.getValue().contains("@")) {
//                            deselectItem(itemBox, list);
//                        }
//                        break;
 
                    // handle backspace
                    case KeyCodes.KEY_BACKSPACE:
                        if (itemBox.getValue().trim().isEmpty()) {
                            if (itemsHighlighted.isEmpty()) {
                                if (itemsSelected.size() > 0) {
                                    ListItem li = (ListItem) list.getWidget(list.getWidgetCount() - 2);
                                    Paragraph p = (Paragraph) li.getWidget(0);
                                    if (itemsSelected.contains(p.getText())) {
                                        // remove selected item
                                        itemsSelected.remove(p.getText());
                                    }
                                    list.remove(li);
                                }
                            }
                        }
                        // continue to delete
                     
                    // handle delete
                    case KeyCodes.KEY_DELETE:
                        if (itemBox.getValue().trim().isEmpty()) {
                            for (ListItem li : itemsHighlighted) {
                                list.remove(li);
                                Paragraph p = (Paragraph) li.getWidget(0);
                                itemsSelected.remove(p.getText());
                            }
                            itemsHighlighted.clear();
                        }
                        itemBox.setFocus(true);
                        break;
                }
            }
        });
 
        final int finalMaxCountInList = maxCountInList;
        box.addSelectionHandler(new SelectionHandler<SuggestOracle.Suggestion>() {
            // called when an item is selected from list of suggestions
            public void onSelection(SelectionEvent<SuggestOracle.Suggestion> selectionEvent) {
                deselectItem(
                		//itemBox, list, color, 
                		finalMaxCountInList);
            }
        });
 
        panel.add(list);
 
        panel.getElement().setAttribute("onclick", "document.getElementById('suggestion_box"+uniqueId+"').focus()");
        box.setFocus(true);
        /*
         * Div structure after a few elements have been added: <ul
         * class="multiValueSuggestBox-list"> <li
         * class="multiValueSuggestBox-token"> <p>What's New Scooby-Doo?</p>
         * <span class="multiValueSuggestBox-delete-token">x</span> </li> <li
         * class="multiValueSuggestBox-token"> <p>Fear Factor</p> <span
         * class="multiValueSuggestBox-delete-token">x</span> </li> <li
         * class="multiValueSuggestBox-input-token"> <input type="text" style=
         * "outline-color: -moz-use-text-color; outline-style: none; outline-width: medium;"
         * /> </li> </ul>
         */
    }
 
    private void deselectItem(
    		//final TextBox itemBox, final BulletList list, String color, 
    		final int maxCountInList) {
        if (itemBox.getValue() != null && !"".equals(itemBox.getValue().trim())) {
            /**
             * Change to the following structure: <li class="multiValueSuggestBox-token">
             * <p>
             * What's New Scooby-Doo?
             * </p>
             * <span class="multiValueSuggestBox-delete-token">x</span></li>
             */
 
        	//check not already added in list
        	int intAlreadyAddedInList = -1;
        	for(int i=0;i<itemsSelected.size();i++){
        		if(itemsSelected.get(i).compareTo(itemBox.getValue())==0){
        			intAlreadyAddedInList = i;
        			break;
        		}
        	}
        	
        	if(intAlreadyAddedInList < 0){
        		//not already added
        		
            		final ListItem displayItem = new ListItem();
                    displayItem.setStyleName("multiValueSuggestBox-token-"+color);
                   
                    Paragraph p = new Paragraph(itemBox.getValue());
         
                    displayItem.addClickHandler(new ClickHandler() {
                        // called when a list item is clicked on
                        public void onClick(ClickEvent clickEvent) {
                            if (itemsHighlighted.contains(displayItem)) { 
                                displayItem.removeStyleDependentName("selected");
                                itemsHighlighted.remove(displayItem);
                            }
                            else {
                                displayItem.addStyleDependentName("selected");
                                itemsHighlighted.add(displayItem);
                            }
                        }
                    });
         
                    Span span = new Span("x");
                    span.addClickHandler(new ClickHandler() {
                        public void onClick(ClickEvent clickEvent) {
                            removeListItem(displayItem
                            		//, list
                            		);
                        }
                    });
         
                    displayItem.add(p);
                    displayItem.add(span);
                    // hold the original value of the item selected
         
                    // add selected item
                    itemsSelected.add(itemBox.getValue());
         
                    list.insert(displayItem, list.getWidgetCount() - 1);
                    itemBox.setValue("");
                    itemBox.setFocus(true);
                    
                    if(itemsSelected.size() > maxCountInList){
                    	//reach limit, remove previous one
                    	removeListItem((ListItem)list.getWidget(list.getWidgetCount()-3)
                    			//, list
                    			);
                    }
                
        		
                
        	}else{
        		//already added, select to highlight
        		list.getWidget(intAlreadyAddedInList).addStyleDependentName("selected");
        		itemsHighlighted.add((ListItem)list.getWidget(intAlreadyAddedInList));
        		itemBox.setValue("");
                itemBox.setFocus(true);
        	}
            
        }
    }
 
    private void removeListItem(ListItem displayItem
    		//, BulletList list
    		) {
        itemsSelected.remove(displayItem.getWidget(0).getElement().getInnerHTML());
        list.remove(displayItem);
    }
    
    public ArrayList<String> getListItemsSelected(){
    	return itemsSelected;
    }
    
    public void clear(){
    	for(int i=(list.getWidgetCount()-2);i>=0;i--){
    		removeListItem((ListItem)list.getWidget(i));
    	}
    }
    
    public void addRootSelectionIfAvailable(){
    	
    	if(Insyght.APP_CONTROLER.getTaxoTree() != null){
    		if(Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0) != null){
    			if(Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getName().compareTo("root")==0){
    		    	itemBox.setText("root");
    		    	deselectItem(maxCountInList);
    			}else{
    				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in addRootSelectionIfAvailable : Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getName().compareTo(root)!=0 : "+Insyght.APP_CONTROLER.getTaxoTree().getTaxoChildrens().get(0).getName()));
    			}
    		}
    	}
    }
    
    
}
