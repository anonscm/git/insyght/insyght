package fr.inra.jouy.client.view.result;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import java.util.Collections;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.shared.pojos.applicationItems.GeneWidgetStyleItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSSyntItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.HolderAbsoluteProportionItem;
import fr.inra.jouy.shared.pojos.symbols.SuperHoldAbsoPropItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem.QEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem.SEnumBlockType;

public class CommonMethodsViewResults {

	private static int NEXT_AVAILABLE_BKG_COLOR = 0;
	private static int DELTA_OVERLAP_GENE = 25;
	
	public CommonMethodsViewResults() {

	}

	// private ArrayList<AbsoluteProportionQGenomicRegionInsertionItem>
	// getQGenomicRegionInsertionWithArrayListSynteny(
	// ArrayList<AbsoluteProportionQSSyntenyItem> alITQSSynteny, int
	// sizeOfElementInPb, int qPbStartOfElementInOrga, int qSizeOfOragnismInPb,
	// int qOrigamiElementId, String qAccnum) {
	public ArrayList<Integer> crunchQSSyntenyIntoOneSlotAndListOfTmpOtherAndGetQGenomicRegionInsertionWithArrayListSynteny(
			ArrayList<AbsoPropQSSyntItem> alITQSSynteny,
			ArrayList<AbsoPropQElemItem> alAPQEIIT) {

		// Comparator<AbsoluteProportionQSSyntenyItem>
		// compaAPQSSynteyByPbQStartSyntenyInOrgaAndQLenghtIT = new
		// ByPbQStartSyntenyInOrgaAndQLenghtComparator();
		// Collections.sort(alITQSSynteny,
		// compaAPQSSynteyByPbQStartSyntenyInOrgaAndQLenghtIT);

		ArrayList<Integer> missingElements = new ArrayList<Integer>();
		int currIdxInalAPQEIIT = 0;
		
		
		// System.out.println("Q alITQSSynteny.size() before ="+alITQSSynteny.size());

		// ArrayList<AbsoluteProportionQElementItem> alAPQGRIIToReturn = new
		// ArrayList<AbsoluteProportionQElementItem>();

		// HashMap<Integer,Integer> elementDone = new HashMap<Integer,
		// Integer>();

		ArrayList<Integer> indexToDelInOriginalListAfterward = new ArrayList<Integer>();

		LOOP_getQGenomicRegionInsertionWithArrayListSynteny_IT: for (int i = 0; i < alITQSSynteny
				.size(); i++) {
			AbsoPropQSSyntItem apqciIT = alITQSSynteny.get(i);

			
			//int earliestPbQStartOfGroup = apqciIT.getPbQStartSyntenyInElement();
			int furthestPbQStopOfGroup = apqciIT.getPbQStopSyntenyInElement();
			int currPbSStartOfMainReferentSynteny = apqciIT.getPbSStartSyntenyInElement();
			int currPbSStopOfMainReferentSynteny = apqciIT.getPbSStopSyntenyInElement();
			
			AbsoPropQSSyntItem mainSyntenyIT = apqciIT;
			
			// get and set
			int qSizeOfElementInPbIT = apqciIT.getQsQSizeOfElementinPb();
			int qPbStartOfElementInOrgaIT = apqciIT
					.getQsQPbStartOfElementInOrga();
			int qSizeOfOragnismInPbIT = apqciIT.getQsQSizeOfOrganismInPb();
			String qAccnumIT = apqciIT.getQsQAccnum();

			if (i == 0) {
				
				//check if missed an element ?
				
				if(currIdxInalAPQEIIT < alAPQEIIT.size()){
					if(alAPQEIIT.get(currIdxInalAPQEIIT).getqOrigamiElementId() != apqciIT.getQsQOrigamiElementId()){
						//apqciIT not on element at idx 0...
						for(int k=currIdxInalAPQEIIT;k<alAPQEIIT.size();k++){
							missingElements.add(k);
							
							if(alAPQEIIT.get(k).getqOrigamiElementId() == apqciIT.getQsQOrigamiElementId()){
								currIdxInalAPQEIIT = k;
								break;
							}else if(k == (alAPQEIIT.size()-1)){
								currIdxInalAPQEIIT = k;
								break;
							}
						}
					}
				}
				
				
				
				// deal with first insertion
				//if (apqciIT.getPbSStartSyntenyInElement() > 1) {
					AbsoPropQGenoRegiInserItem insertionRegion = new AbsoPropQGenoRegiInserItem();
					insertionRegion.setqOrigamiElementId(apqciIT
							.getQsQOrigamiElementId());
					insertionRegion
							.setqPbStartQGenomicRegionInsertionInElement(0);
					insertionRegion
							.setqPbStopQGenomicRegionInsertionInElement((apqciIT
									.getPbQStartSyntenyInElement() - 1));
					insertionRegion
							.setqPercentStart((((double) qPbStartOfElementInOrgaIT) + (0.0))
									/ (double) qSizeOfOragnismInPbIT);
					insertionRegion
							.setqPercentStop(((double) qPbStartOfElementInOrgaIT + (double) (apqciIT
									.getPbQStartSyntenyInElement() - (1.0)))
									/ (double) qSizeOfOragnismInPbIT);
					insertionRegion.setqAccnum(qAccnumIT);
					insertionRegion
							.setqPbStartOfElementInOrga(qPbStartOfElementInOrgaIT);
					insertionRegion
							.setQsizeOfOragnismInPb(qSizeOfOragnismInPbIT);
					insertionRegion
							.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
					insertionRegion.setAnchoredToStartElement(true);
					// alAPQGRIIToReturn.add(insertionRegion);
					apqciIT.setTmpPreviousGenomicRegionQInsertion(insertionRegion);
				//}
			}

			for (int j = (i + 1); j < alITQSSynteny.size(); j++) {
				AbsoPropQSSyntItem apqciNext = alITQSSynteny
						.get(j);

				if (apqciIT.getQsQOrigamiElementId() == apqciNext
						.getQsQOrigamiElementId()) {
					// nextElementIs same ThanIT

					if (apqciNext.getPbQStartSyntenyInElement() > furthestPbQStopOfGroup - DELTA_OVERLAP_GENE
							&& apqciIT.getPbQStopSyntenyInElement() > apqciIT.getPbQStartSyntenyInElement()) {
						
						if (apqciNext.getPbQStartSyntenyInElement() > furthestPbQStopOfGroup){
							// apqciNext start after stop of apqciIT : new region in
							// between
							AbsoPropQGenoRegiInserItem insertionRegion = new AbsoPropQGenoRegiInserItem();
							insertionRegion.setqOrigamiElementId(apqciIT
									.getQsQOrigamiElementId());
							
							insertionRegion
									.setqPbStartQGenomicRegionInsertionInElement(furthestPbQStopOfGroup + 1);
							insertionRegion
									.setqPbStopQGenomicRegionInsertionInElement(apqciNext
											.getPbQStartSyntenyInElement() - 1);
							insertionRegion
									.setqPercentStart((((double) qPbStartOfElementInOrgaIT)
											+ (double) furthestPbQStopOfGroup + (1.0))
											/ (double) qSizeOfOragnismInPbIT);
							insertionRegion
									.setqPercentStop(((double) qPbStartOfElementInOrgaIT
											+ (double) apqciNext
													.getPbQStartSyntenyInElement() - (1.0))
											/ (double) qSizeOfOragnismInPbIT);
							insertionRegion.setqAccnum(qAccnumIT);
							insertionRegion
									.setqPbStartOfElementInOrga(qPbStartOfElementInOrgaIT);
							insertionRegion
									.setQsizeOfOragnismInPb(qSizeOfOragnismInPbIT);
							insertionRegion
									.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
							// alAPQGRIIToReturn.add(insertionRegion);
							apqciNext.setTmpPreviousGenomicRegionQInsertion(insertionRegion);
							continue LOOP_getQGenomicRegionInsertionWithArrayListSynteny_IT;
							
						}else{
							
							//collide < DELTA_OVERLAP_GENE
							AbsoPropQGenoRegiInserItem collisionRegion = new AbsoPropQGenoRegiInserItem();
							collisionRegion.setAnnotationCollision(true);
							collisionRegion.setqOrigamiElementId(apqciIT
									.getQsQOrigamiElementId());
							collisionRegion
									.setqPbStartQGenomicRegionInsertionInElement(
											apqciNext
											.getPbQStartSyntenyInElement());
							collisionRegion
									.setqPbStopQGenomicRegionInsertionInElement(apqciIT
											.getPbQStopSyntenyInElement());
							collisionRegion
									.setqPercentStart(((double) qPbStartOfElementInOrgaIT
											+ (double) apqciNext
											.getPbQStartSyntenyInElement())
									/ (double) qSizeOfOragnismInPbIT);
							collisionRegion
									.setqPercentStop(
											(((double) qPbStartOfElementInOrgaIT)
													+ (double) apqciIT
															.getPbQStopSyntenyInElement())
													/ (double) qSizeOfOragnismInPbIT
											);
							collisionRegion.setqAccnum(qAccnumIT);
							collisionRegion
									.setqPbStartOfElementInOrga(qPbStartOfElementInOrgaIT);
							collisionRegion
									.setQsizeOfOragnismInPb(qSizeOfOragnismInPbIT);
							collisionRegion
									.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
							// alAPQGRIIToReturn.add(insertionRegion);
							apqciNext
									.setTmpPreviousGenomicRegionQInsertion(collisionRegion);
							
							continue LOOP_getQGenomicRegionInsertionWithArrayListSynteny_IT;

						}
						
					} else if (apqciNext.getPbQStopSyntenyInElement() > furthestPbQStopOfGroup) {
						// apqciNext extends stop of apqciIT
						furthestPbQStopOfGroup = apqciNext.getPbQStopSyntenyInElement();
						
						if (apqciIT.getListOrigamiAlignmentIdsContainedOtherSyntenies().isEmpty()
								&& apqciIT.getTmpListOfOtherQSSyntenySprungOff().isEmpty()
								&& apqciIT.getSyntenyNumberGene() == 1
								&& apqciNext.getListOrigamiAlignmentIdsContainedOtherSyntenies().isEmpty()
								&& apqciNext.getTmpListOfOtherQSSyntenySprungOff().isEmpty()
								&& apqciNext.getSyntenyNumberGene() == 1) {
							// singleton, gene directly
							//both annotation collides...
							AbsoPropQGenoRegiInserItem collisionRegion = new AbsoPropQGenoRegiInserItem();
							collisionRegion.setAnnotationCollision(true);
							collisionRegion.setqOrigamiElementId(apqciIT
									.getQsQOrigamiElementId());
							collisionRegion
									.setqPbStartQGenomicRegionInsertionInElement(
											apqciNext
											.getPbQStartSyntenyInElement());
							collisionRegion
									.setqPbStopQGenomicRegionInsertionInElement(apqciIT
											.getPbQStopSyntenyInElement());
							collisionRegion
									.setqPercentStart(((double) qPbStartOfElementInOrgaIT
											+ (double) apqciNext
											.getPbQStartSyntenyInElement())
									/ (double) qSizeOfOragnismInPbIT);
							collisionRegion
									.setqPercentStop(
											(((double) qPbStartOfElementInOrgaIT)
													+ (double) apqciIT
															.getPbQStopSyntenyInElement())
													/ (double) qSizeOfOragnismInPbIT
											);
							collisionRegion.setqAccnum(qAccnumIT);
							collisionRegion
									.setqPbStartOfElementInOrga(qPbStartOfElementInOrgaIT);
							collisionRegion
									.setQsizeOfOragnismInPb(qSizeOfOragnismInPbIT);
							collisionRegion
									.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
							// alAPQGRIIToReturn.add(insertionRegion);
							apqciNext
									.setTmpPreviousGenomicRegionQInsertion(collisionRegion);
							
							continue LOOP_getQGenomicRegionInsertionWithArrayListSynteny_IT;

						}else{
							if(apqciNext.getSyntenyNumberGene() > apqciIT.getSyntenyNumberGene()){
								//next become new referent, all other sprung off
								//next do
								apqciNext.setMotherOfSprungOffSyntenies(true);
								
								apqciNext.getListOrigamiAlignmentIdsSprungOffSyntenies().add(apqciIT.getSyntenyOrigamiAlignmentId());
								apqciNext.getListOrigamiAlignmentIdsSprungOffSyntenies().addAll(apqciIT.getListOrigamiAlignmentIdsSprungOffSyntenies());
								
								apqciNext.getTmpListOfOtherQSSyntenySprungOff().add(apqciIT);
								apqciNext.getTmpListOfOtherQSSyntenySprungOff().addAll(apqciIT.getTmpListOfOtherQSSyntenySprungOff());
								
								apqciNext.getListOrigamiAlignmentIdsContainedOtherSyntenies().addAll(apqciIT.getListOrigamiAlignmentIdsContainedOtherSyntenies());
								apqciNext.setTmpNextGenomicRegionQInsertion(apqciIT.getTmpNextGenomicRegionQInsertion());
								apqciNext.setTmpPreviousGenomicRegionQInsertion(apqciIT.getTmpPreviousGenomicRegionQInsertion());
								
								mainSyntenyIT = apqciNext;
								
								//apqciIT undo
								apqciIT.setMotherOfSprungOffSyntenies(false);
								apqciIT.getListOrigamiAlignmentIdsSprungOffSyntenies().clear();
								apqciIT.getTmpListOfOtherQSSyntenySprungOff().clear();
								//apqciIT do
								apqciIT.setSprungOffAnotherMotherSynteny(true);
								apqciIT.getListOrigamiAlignmentIdsMotherSyntenies()
										.add(apqciNext.getSyntenyOrigamiAlignmentId());
								
								currPbSStartOfMainReferentSynteny = apqciNext.getPbSStartSyntenyInElement();
								currPbSStopOfMainReferentSynteny = apqciNext.getPbSStopSyntenyInElement();
								
								
								indexToDelInOriginalListAfterward.add(alITQSSynteny.indexOf(apqciIT));
								i++;
							}else{
								// next is sprung off
								apqciNext.setSprungOffAnotherMotherSynteny(true);
								apqciNext
										.getListOrigamiAlignmentIdsMotherSyntenies()
										.add(mainSyntenyIT.getSyntenyOrigamiAlignmentId());
								mainSyntenyIT.setMotherOfSprungOffSyntenies(true);
								mainSyntenyIT.getListOrigamiAlignmentIdsSprungOffSyntenies()
										.add(apqciNext
												.getSyntenyOrigamiAlignmentId());
								mainSyntenyIT.getTmpListOfOtherQSSyntenySprungOff().add(
										apqciNext);
								indexToDelInOriginalListAfterward.add(j);
								i++;
							}
							
							
						}
						
					
					} else {
						if (currPbSStartOfMainReferentSynteny <= apqciNext
								.getPbSStartSyntenyInElement()
								&& currPbSStopOfMainReferentSynteny >= apqciNext
										.getPbSStopSyntenyInElement()) {
							// apqciNext all included in apqciIT
							apqciNext
									.setContainedWithinAnotherMotherSynteny(true);
							apqciNext
									.getListOrigamiAlignmentIdsMotherSyntenies()
									.add(mainSyntenyIT.getSyntenyOrigamiAlignmentId());
							mainSyntenyIT.setMotherOfContainedOtherSyntenies(true);
							mainSyntenyIT.getListOrigamiAlignmentIdsContainedOtherSyntenies()
									.add(apqciNext
											.getSyntenyOrigamiAlignmentId());

//							apqciIT.getListOfOtherQSSyntenyContainedWithin()
//									.add(apqciNext);
							indexToDelInOriginalListAfterward.add(j);
							i++;
						} else {
							// System.err.println("off shoot");
							// sprung off
							apqciNext.setSprungOffAnotherMotherSynteny(true);
							apqciNext
									.getListOrigamiAlignmentIdsMotherSyntenies()
									.add(mainSyntenyIT.getSyntenyOrigamiAlignmentId());
							mainSyntenyIT.setMotherOfSprungOffSyntenies(true);
							mainSyntenyIT.getListOrigamiAlignmentIdsSprungOffSyntenies()
									.add(apqciNext
											.getSyntenyOrigamiAlignmentId());

							mainSyntenyIT.getTmpListOfOtherQSSyntenySprungOff().add(
									apqciNext);
							indexToDelInOriginalListAfterward.add(j);
							i++;
						}

					}

					if(mainSyntenyIT.getTmpListOfOtherQSSyntenySprungOff().isEmpty()){
						// do not do if multiple layers
						AbsoPropSGenoRegiInserItem apseiITNext = mainSyntenyIT
								.getNextGenomicRegionSInsertion();
						if(apseiITNext != null){
							if (apseiITNext.getsPbStopSGenomicRegionInsertionInElement() == (apqciNext.getPbSStartSyntenyInElement() - 1)) {
								//foundAnchoringStraight = true;
								// check wether S region insertion are
								AbsoPropSGenoRegiInserItem apseiNextPrevious = apqciNext
										.getPreviousGenomicRegionSInsertion();
								if(apseiNextPrevious != null){
									if (apseiNextPrevious.getsPbStartSGenomicRegionInsertionInElement() == (mainSyntenyIT.getPbSStopSyntenyInElement() + 1)) {
										apseiNextPrevious.setAnchoredToPrevious(true);
										apseiITNext.setAnchoredToNext(true);
									}else{
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in crunchQSSyntenyIntoOneSlotAndListOfTmpOtherAndGetQGenomicRegionInsertionWithArrayListSynteny :" +
												"foundAnchoringStraight once but not twice : " +
												"\n apseiITNext.getsPbStopSGenomicRegionInsertionInElement() = "+apseiITNext.getsPbStopSGenomicRegionInsertionInElement() +
												"\n apqciNext.getPbQStartSyntenyInElement() = "+apqciNext.getPbQStartSyntenyInElement() +
												"\n apseiNextPrevious.getsPbStartSGenomicRegionInsertionInElement() = "+apseiNextPrevious.getsPbStartSGenomicRegionInsertionInElement() +
												"\n apqciIT.getPbQStopSyntenyInElement() = "+mainSyntenyIT.getPbQStopSyntenyInElement()
												));
									}
								}
							}
						}
					}
					
					

				} else {
					// nextElementIsDifferentThanIT

					
					// deal with last between
					// apqciIT.getPbQStopSyntenyInElement() and
					// qSizeOfElementInPbIT
					
						
						AbsoPropQGenoRegiInserItem insertionRegion = new AbsoPropQGenoRegiInserItem();
						insertionRegion.setqOrigamiElementId(apqciIT
								.getQsQOrigamiElementId());
						
						if (apqciIT.getPbQStopSyntenyInElement() > apqciIT.getPbQStartSyntenyInElement()) {
							insertionRegion
							.setqPbStartQGenomicRegionInsertionInElement((apqciIT
									.getPbQStopSyntenyInElement() + 1));
							insertionRegion
							.setqPercentStart((((double) qPbStartOfElementInOrgaIT)
									+ ((double) apqciIT
											.getPbQStopSyntenyInElement()) + (1.0))
									/ (double) qSizeOfOragnismInPbIT);
						}else{
							//else gene on ori
							insertionRegion
							.setqPbStartQGenomicRegionInsertionInElement(qSizeOfElementInPbIT);
							insertionRegion
							.setqPercentStart(((double) qPbStartOfElementInOrgaIT + (double) qSizeOfElementInPbIT)
									/ (double) qSizeOfOragnismInPbIT);
						}
						
						insertionRegion
								.setqPbStopQGenomicRegionInsertionInElement(qSizeOfElementInPbIT);
						
						insertionRegion
								.setqPercentStop(((double) qPbStartOfElementInOrgaIT + (double) qSizeOfElementInPbIT)
										/ (double) qSizeOfOragnismInPbIT);
						insertionRegion.setqAccnum(qAccnumIT);
						insertionRegion
								.setqPbStartOfElementInOrga(qPbStartOfElementInOrgaIT);
						insertionRegion
								.setQsizeOfOragnismInPb(qSizeOfOragnismInPbIT);
						insertionRegion
								.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
						insertionRegion.setAnchoredToStopElement(true);
						// alAPQGRIIToReturn.add(insertionRegion);
						apqciIT.setTmpNextGenomicRegionQInsertion(insertionRegion);
					//}


					//check if missed an element ?
					
					currIdxInalAPQEIIT++;
					if(currIdxInalAPQEIIT < alAPQEIIT.size()){
						if(alAPQEIIT.get(currIdxInalAPQEIIT).getqOrigamiElementId() != apqciNext.getQsQOrigamiElementId()){
							for(int k=currIdxInalAPQEIIT;k<alAPQEIIT.size();k++){
								missingElements.add(k);
								if(alAPQEIIT.get(k).getqOrigamiElementId() == apqciNext.getQsQOrigamiElementId()){
									currIdxInalAPQEIIT = k;
									break;
								}else if(k == (alAPQEIIT.size()-1)){
									currIdxInalAPQEIIT = k;
									break;
								}
							}
						}
					}
					
						
					
					// deal with first between and
					//if (apqciNext.getPbQStartSyntenyInElement() > 1) {
						AbsoPropQGenoRegiInserItem insertionRegionFirst = new AbsoPropQGenoRegiInserItem();
						insertionRegionFirst.setqOrigamiElementId(apqciNext.getQsQOrigamiElementId());
						insertionRegionFirst.setqPbStartQGenomicRegionInsertionInElement(0);
						insertionRegionFirst.setqPbStopQGenomicRegionInsertionInElement(apqciNext
										.getPbQStartSyntenyInElement() - 1);
						insertionRegionFirst.setqPercentStart(((double) apqciNext
										.getQsQPbStartOfElementInOrga())
										/ (double) apqciNext
												.getQsQSizeOfOrganismInPb());
						insertionRegionFirst.setqPercentStop(((double) apqciNext
										.getQsQPbStartOfElementInOrga()
										+ (double) apqciNext
												.getPbQStartSyntenyInElement() - (1.0))
										/ (double) apqciNext
												.getQsQSizeOfOrganismInPb());
						insertionRegionFirst.setqAccnum(apqciNext.getQsQAccnum());
						insertionRegionFirst.setqPbStartOfElementInOrga(apqciNext
								.getQsQPbStartOfElementInOrga());
						insertionRegionFirst.setQsizeOfOragnismInPb(apqciNext
								.getQsQSizeOfOrganismInPb());
						insertionRegionFirst.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
						insertionRegionFirst.setAnchoredToStartElement(true);
						// alAPQGRIIToReturn.add(insertionRegion);
						apqciNext.setTmpPreviousGenomicRegionQInsertion(insertionRegionFirst);
					//}
					continue LOOP_getQGenomicRegionInsertionWithArrayListSynteny_IT;

				}
			}

			if (i == (alITQSSynteny.size() - 1)) {
				// do last insertion
				//if (apqciIT.getPbQStopSyntenyInElement() < qSizeOfElementInPbIT) {
					AbsoPropQGenoRegiInserItem insertionRegion = new AbsoPropQGenoRegiInserItem();
					insertionRegion.setqOrigamiElementId(apqciIT
							.getQsQOrigamiElementId());
					insertionRegion
							.setqPbStartQGenomicRegionInsertionInElement(apqciIT
									.getPbQStopSyntenyInElement() + 1);
					insertionRegion
							.setqPbStopQGenomicRegionInsertionInElement(qSizeOfElementInPbIT);
					insertionRegion
							.setqPercentStart((((double) qPbStartOfElementInOrgaIT)
									+ (double) apqciIT
											.getPbQStopSyntenyInElement() + (1.0))
									/ (double) qSizeOfOragnismInPbIT);
					insertionRegion
							.setqPercentStop((((double) qPbStartOfElementInOrgaIT + (double) qSizeOfElementInPbIT))
									/ (double) qSizeOfOragnismInPbIT);
					insertionRegion.setqAccnum(qAccnumIT);
					insertionRegion
							.setqPbStartOfElementInOrga(qPbStartOfElementInOrgaIT);
					insertionRegion
							.setQsizeOfOragnismInPb(qSizeOfOragnismInPbIT);
					insertionRegion
							.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
					insertionRegion.setAnchoredToStopElement(true);
					// alAPQGRIIToReturn.add(insertionRegion);
					apqciIT.setTmpNextGenomicRegionQInsertion(insertionRegion);
				//}
					
					//check if missed an element ?
					currIdxInalAPQEIIT++;
					if(currIdxInalAPQEIIT < alAPQEIIT.size()){
						if(alAPQEIIT.get(currIdxInalAPQEIIT).getqOrigamiElementId() != apqciIT.getQsQOrigamiElementId()){
							for(int k=currIdxInalAPQEIIT;k<alAPQEIIT.size();k++){
								missingElements.add(k);
								if(alAPQEIIT.get(k).getqOrigamiElementId() == apqciIT.getQsQOrigamiElementId()){
									currIdxInalAPQEIIT = k;
									break;
								}else if(k == (alAPQEIIT.size()-1)){
									currIdxInalAPQEIIT = k;
									break;
								}
							}
						}
					}
					
					
					
			}

		}


		// //del index in indexToDelInOriginalListAfterward
		Collections.sort(indexToDelInOriginalListAfterward);
		if (!indexToDelInOriginalListAfterward.isEmpty()) {
			// for(int k=0;k<indexToDelAfterward.size();k++){
			for (int k = (indexToDelInOriginalListAfterward.size() - 1); k >= 0; k--) {
				// System.out.println("deleting "+(int)
				// indexToDelAfterward.get(k));
				alITQSSynteny.remove((int) indexToDelInOriginalListAfterward
						.get(k));
			}
		}

		// System.out.println("Q alITQSSynteny.size() after ="+alITQSSynteny.size());

		return missingElements;

	}

	/*
	 * private ArrayList<AbsoluteProportionSGenomicRegionInsertionItem>
	 * getSGenomicRegionInsertionWithArrayListSynteny(
	 * ArrayList<AbsoluteProportionQSSyntenyItem> alITQSSynteny, int
	 * sizeOfElementInPb, int sPbStartOfElementInOrga, int sSizeOfOragnismInPb,
	 * int sOrigamiElementId, String sAccnum) {
	 */
	public void getSGenomicRegionInsertionWithArrayListSynteny(
			ArrayList<AbsoPropQSSyntItem> alITQSSynteny,
			ArrayList<AbsoPropSElemItem> alAPSEIIT) {

		// System.out.println("alITQSSynteny.size()="+alITQSSynteny.size());

		// HashMap<Integer,Integer> elementDone = new HashMap<Integer,
		// Integer>();

		// ArrayList<AbsoluteProportionSElementItem> alAPSGRIIToReturn = new
		// ArrayList<AbsoluteProportionSElementItem>();

		LOOP_getSGenomicRegionInsertionWithArrayListSynteny_IT: for (int i = 0; i < alITQSSynteny
				.size(); i++) {
			AbsoPropQSSyntItem apqsciIT = alITQSSynteny.get(i);

			// System.out.println("i="+i);

			// elementDone.put(apqsciIT.getQsSOrigamiElementId(), 0);

			int sSizeOfElementInPbIT = apqsciIT.getQsSSizeOfElementinPb();
			int sPbStartOfElementInOrgaIT = apqsciIT
					.getQsSPbStartOfElementInOrga();
			int sSizeOfOragnismInPbIT = apqsciIT.getQsSSizeOfOrganismInPb();
			String sAccnumIT = apqsciIT.getQsSAccnum();

			if (i == 0) {
				// deal with first insertion

					AbsoPropSGenoRegiInserItem insertionRegion = new AbsoPropSGenoRegiInserItem();
					insertionRegion.setsOrigamiElementId(apqsciIT
							.getQsSOrigamiElementId());
					insertionRegion
							.setsPbStartSGenomicRegionInsertionInElement(0);
					insertionRegion
							.setsPbStopSGenomicRegionInsertionInElement((apqsciIT
									.getPbSStartSyntenyInElement() - 1));
					insertionRegion
							.setsPercentStart((((double) sPbStartOfElementInOrgaIT) + (0.0))
									/ (double) sSizeOfOragnismInPbIT);
					insertionRegion
							.setsPercentStop(((double) sPbStartOfElementInOrgaIT + (double) (apqsciIT
									.getPbSStartSyntenyInElement() - (1.0)))
									/ (double) sSizeOfOragnismInPbIT);
					insertionRegion.setsAccnum(sAccnumIT);
					insertionRegion
							.setsPbStartOfElementInOrga(sPbStartOfElementInOrgaIT);
					insertionRegion
							.setsSizeOfOragnismInPb(sSizeOfOragnismInPbIT);
					insertionRegion
							.setSEnumBlockType(SEnumBlockType.S_INSERTION);
					insertionRegion.setAnchoredToNext(true);
					insertionRegion.setAnchoredToStartElement(true);
					insertionRegion.setPreviousSOfNextSlice(true);
					// alAPSGRIIToReturn.add(insertionRegion);
					apqsciIT.setPreviousGenomicRegionSInsertion(insertionRegion);
				
			}

			for (int j = (i + 1); j < alITQSSynteny.size(); j++) {
				AbsoPropQSSyntItem apqsciNext = alITQSSynteny
						.get(j);

				if (apqsciIT.getQsSOrigamiElementId() == apqsciNext
						.getQsSOrigamiElementId()) {
					// nextElementIs same ThanIT

					if (apqsciNext.getPbSStartSyntenyInElement() > apqsciIT
							.getPbSStopSyntenyInElement()) {
						// apqciNext start after stop of apqciIT : new region in
						// between
						AbsoPropSGenoRegiInserItem insertionRegion = new AbsoPropSGenoRegiInserItem();
						insertionRegion.setsOrigamiElementId(apqsciIT
								.getQsSOrigamiElementId());
						insertionRegion
								.setsPbStartSGenomicRegionInsertionInElement(apqsciIT
										.getPbSStopSyntenyInElement() + 1);
						insertionRegion
								.setsPbStopSGenomicRegionInsertionInElement(apqsciNext
										.getPbSStartSyntenyInElement() - 1);
						insertionRegion
								.setsPercentStart((((double) sPbStartOfElementInOrgaIT)
										+ (double) apqsciIT
												.getPbSStopSyntenyInElement() + (1.0))
										/ (double) sSizeOfOragnismInPbIT);
						insertionRegion
								.setsPercentStop(((double) sPbStartOfElementInOrgaIT
										+ (double) apqsciNext
												.getPbSStartSyntenyInElement() - (1.0))
										/ (double) sSizeOfOragnismInPbIT);
						insertionRegion.setsAccnum(sAccnumIT);
						insertionRegion
								.setsPbStartOfElementInOrga(sPbStartOfElementInOrgaIT);
						insertionRegion
								.setsSizeOfOragnismInPb(sSizeOfOragnismInPbIT);
						insertionRegion
								.setSEnumBlockType(SEnumBlockType.S_INSERTION);
						// alAPSGRIIToReturn.add(insertionRegion);
						insertionRegion.setAnchoredToNext(true);
						insertionRegion.setPreviousSOfNextSlice(true);
						apqsciNext.setPreviousGenomicRegionSInsertion(insertionRegion);
						
						AbsoPropSGenoRegiInserItem insertionRegionMirror = new AbsoPropSGenoRegiInserItem(insertionRegion);
						insertionRegionMirror.setAnchoredToNext(false);
						insertionRegionMirror.setAnchoredToPrevious(true);
						insertionRegionMirror.setPreviousSOfNextSlice(false);
						insertionRegionMirror.setNextSOfPreviousSlice(true);
						apqsciIT.setNextGenomicRegionSInsertion(insertionRegionMirror);
						
						continue LOOP_getSGenomicRegionInsertionWithArrayListSynteny_IT;
						
					} else if (apqsciNext.getPbSStopSyntenyInElement() > apqsciIT
							.getPbSStopSyntenyInElement()) {
						// apqciNext extends stop of apqciIT
						
						AbsoPropSGenoRegiInserItem insertionRegion = new AbsoPropSGenoRegiInserItem();
						insertionRegion.setsOrigamiElementId(apqsciIT
								.getQsSOrigamiElementId());
						insertionRegion
								.setsPbStartSGenomicRegionInsertionInElement(apqsciNext
										.getPbSStartSyntenyInElement());
						insertionRegion
								.setsPbStopSGenomicRegionInsertionInElement(apqsciIT
										.getPbSStopSyntenyInElement());
						insertionRegion
								.setsPercentStart(((double) sPbStartOfElementInOrgaIT
										+ (double) apqsciNext
										.getPbSStartSyntenyInElement())
								/ (double) sSizeOfOragnismInPbIT);
						insertionRegion
								.setsPercentStop((((double) sPbStartOfElementInOrgaIT)
										+ (double) apqsciIT
												.getPbSStopSyntenyInElement())
										/ (double) sSizeOfOragnismInPbIT);
						insertionRegion.setsAccnum(sAccnumIT);
						insertionRegion
								.setsPbStartOfElementInOrga(sPbStartOfElementInOrgaIT);
						insertionRegion
								.setsSizeOfOragnismInPb(sSizeOfOragnismInPbIT);
						insertionRegion
								.setSEnumBlockType(SEnumBlockType.S_INSERTION);
						// alAPSGRIIToReturn.add(insertionRegion);
						insertionRegion.setAnchoredToNext(true);
						insertionRegion.setAnnotationCollision(true);
						insertionRegion.setPreviousSOfNextSlice(true);
						apqsciNext.setPreviousGenomicRegionSInsertion(insertionRegion);
						
						AbsoPropSGenoRegiInserItem insertionRegionMirror = new AbsoPropSGenoRegiInserItem(insertionRegion);
						insertionRegionMirror.setAnchoredToNext(false);
						insertionRegionMirror.setAnchoredToPrevious(true);
						insertionRegionMirror.setPreviousSOfNextSlice(false);
						insertionRegionMirror.setNextSOfPreviousSlice(true);
						apqsciIT.setNextGenomicRegionSInsertion(insertionRegionMirror);
						
						
						continue LOOP_getSGenomicRegionInsertionWithArrayListSynteny_IT;
					} else {
						// apqciNext all included in apqciIT
						i++;
					}
				} else {
					// nextElementIs different ThanIT

					// System.err.println("found next element : "+apqsciIT.getQsSOrigamiElementId()+" and "+apqsciNext.getQsSOrigamiElementId());

					// deal with last between
					// apqciIT.getPbQStopSyntenyInElement() and
					// qSizeOfElementInPbIT
					
						AbsoPropSGenoRegiInserItem insertionRegionLast = new AbsoPropSGenoRegiInserItem();
						insertionRegionLast.setsOrigamiElementId(apqsciIT
								.getQsSOrigamiElementId());
						insertionRegionLast
								.setsPbStartSGenomicRegionInsertionInElement(apqsciIT
										.getPbSStopSyntenyInElement() + 1);
						insertionRegionLast
								.setsPbStopSGenomicRegionInsertionInElement(sSizeOfElementInPbIT);
						insertionRegionLast
								.setsPercentStart(((double) sPbStartOfElementInOrgaIT
										+ (double) apqsciIT
												.getPbSStopSyntenyInElement() + (1.0))
										/ (double) sSizeOfOragnismInPbIT);
						insertionRegionLast
								.setsPercentStop((((double) sPbStartOfElementInOrgaIT + (double) sSizeOfElementInPbIT))
										/ (double) sSizeOfOragnismInPbIT);
						insertionRegionLast.setsAccnum(sAccnumIT);
						insertionRegionLast
								.setsPbStartOfElementInOrga(sPbStartOfElementInOrgaIT);
						insertionRegionLast
								.setsSizeOfOragnismInPb(sSizeOfOragnismInPbIT);
						insertionRegionLast
								.setSEnumBlockType(SEnumBlockType.S_INSERTION);
						insertionRegionLast.setAnchoredToStopElement(true);
						insertionRegionLast.setAnchoredToPrevious(true);
						// alAPSGRIIToReturn.add(insertionRegion);
						insertionRegionLast.setNextSOfPreviousSlice(true);
						apqsciIT.setNextGenomicRegionSInsertion(insertionRegionLast);


					// deal with first between and
					
						AbsoPropSGenoRegiInserItem insertionRegionFirst = new AbsoPropSGenoRegiInserItem();
						insertionRegionFirst.setsOrigamiElementId(apqsciNext
								.getQsSOrigamiElementId());
						insertionRegionFirst
								.setsPbStartSGenomicRegionInsertionInElement(0);
						insertionRegionFirst
								.setsPbStopSGenomicRegionInsertionInElement(apqsciNext
										.getPbSStartSyntenyInElement() - 1);
						insertionRegionFirst.setsPercentStart(((double) apqsciNext
								.getQsSPbStartOfElementInOrga())
								/ (double) apqsciNext
										.getQsSSizeOfOrganismInPb());
						insertionRegionFirst.setsPercentStop(((double) apqsciNext
								.getQsSPbStartOfElementInOrga()
								+ (double) apqsciNext
										.getPbSStartSyntenyInElement() - (1.0))
								/ (double) apqsciNext
										.getQsSSizeOfOrganismInPb());
						insertionRegionFirst.setsAccnum(apqsciNext.getQsSAccnum());
						insertionRegionFirst.setsPbStartOfElementInOrga(apqsciNext
								.getQsSPbStartOfElementInOrga());
						insertionRegionFirst.setsSizeOfOragnismInPb(apqsciNext
								.getQsSSizeOfOrganismInPb());
						insertionRegionFirst
								.setSEnumBlockType(SEnumBlockType.S_INSERTION);
						insertionRegionFirst.setAnchoredToNext(true);
						insertionRegionFirst.setAnchoredToStartElement(true);
						// alAPSGRIIToReturn.add(insertionRegion);
						insertionRegionFirst.setPreviousSOfNextSlice(true);
						apqsciNext
								.setPreviousGenomicRegionSInsertion(insertionRegionFirst);

					
					continue LOOP_getSGenomicRegionInsertionWithArrayListSynteny_IT;
				}
			}

			if (i == (alITQSSynteny.size() - 1)) {
				
					AbsoPropSGenoRegiInserItem insertionRegion = new AbsoPropSGenoRegiInserItem();
					insertionRegion.setsOrigamiElementId(apqsciIT
							.getQsSOrigamiElementId());
					insertionRegion
							.setsPbStartSGenomicRegionInsertionInElement(apqsciIT
									.getPbSStopSyntenyInElement() + 1);
					insertionRegion
							.setsPbStopSGenomicRegionInsertionInElement(sSizeOfElementInPbIT);
					insertionRegion
							.setsPercentStart((((double) sPbStartOfElementInOrgaIT)
									+ (double) apqsciIT
											.getPbSStopSyntenyInElement() + (1.0))
									/ (double) sSizeOfOragnismInPbIT);
					insertionRegion
							.setsPercentStop(((double) sPbStartOfElementInOrgaIT + (double) sSizeOfElementInPbIT)
									/ (double) sSizeOfOragnismInPbIT);
					insertionRegion.setsAccnum(sAccnumIT);
					insertionRegion
							.setsPbStartOfElementInOrga(sPbStartOfElementInOrgaIT);
					insertionRegion
							.setsSizeOfOragnismInPb(sSizeOfOragnismInPbIT);
					insertionRegion
							.setSEnumBlockType(SEnumBlockType.S_INSERTION);
					insertionRegion.setAnchoredToStopElement(true);
					insertionRegion.setAnchoredToPrevious(true);
					// alAPSGRIIToReturn.add(insertionRegion);
					insertionRegion.setNextSOfPreviousSlice(true);
					apqsciIT.setNextGenomicRegionSInsertion(insertionRegion);
				
			}
		}

		// //deal with not done element
		// //elementDone
		// for(int i=0;i<alAPSEIIT.size();i++){
		// AbsoluteProportionSElementItem apseiIT = alAPSEIIT.get(i);
		// if(!elementDone.containsKey(apseiIT.getsOrigamiElementId())){
		// //System.out.println("add element ="+apseiIT.getsOrigamiElementId());
		// //alAPSGRIIToReturn.add(apseiIT);
		// alITQSSynteny.get(alITQSSynteny.size()-1).getTmpListOfNextGenomicRegionSInsertion().add(apseiIT);
		// }
		// }

		// return alAPSGRIIToReturn;

	}

	public void buildCorrectlyListSuperHolderFromGeneListAllInsertion(
			ArrayList<Object> alQS,
			ArrayList<SuperHoldAbsoPropItem> alAlHAPISent) {

		alAlHAPISent.clear();
		// insert q
		for (int i = 0; i < alQS.size(); i++) {
			Object oIT = alQS.get(i);
			if (oIT instanceof AbsoPropQGeneInserItem) {
				HolderAbsoluteProportionItem newAPS2 = new HolderAbsoluteProportionItem();
				newAPS2.setAbsoluteProportionQComparableQSpanItem((AbsoPropQGeneInserItem) oIT);
				SuperHoldAbsoPropItem alHAPI2 = new SuperHoldAbsoPropItem();
				alHAPI2.getListHAPI().add(newAPS2);
				alAlHAPISent.add(alHAPI2);
			} else if (oIT instanceof AbsoPropSGeneInserItem) {
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in buildCorrectlyListSuperHolderFromGeneListAllInsertion : oIT not instanceof "
						+ "AbsoluteProportionQGeneInsertionItem or AbsoluteProportionSGeneInsertionItem"));
			}
		}
		// insert s
		for (int i = 0; i < alQS.size(); i++) {
			Object oIT = alQS.get(i);
			if (oIT instanceof AbsoPropSGeneInserItem) {
				for (int j = 0; j < alAlHAPISent.size(); j++) {
					if (alAlHAPISent.get(j).getListHAPI().get(0)
							.getAbsoluteProportionSComparableSSpanItem() == null
							&& alAlHAPISent
									.get(j)
									.getListHAPI()
									.get(0)
									.getAbsoluteProportionQComparableQSSpanItem() == null) {
						alAlHAPISent
								.get(j)
								.getListHAPI()
								.get(0)
								.setAbsoluteProportionSComparableSSpanItem(
										(AbsoPropSGeneInserItem) oIT);
					} else {
						if (j >= alAlHAPISent.size()) {
							HolderAbsoluteProportionItem newAPS2 = new HolderAbsoluteProportionItem();
							newAPS2.setAbsoluteProportionSComparableSSpanItem((AbsoPropSGeneInserItem) oIT);
							SuperHoldAbsoPropItem alHAPI2 = new SuperHoldAbsoPropItem();
							alHAPI2.getListHAPI().add(newAPS2);
							alAlHAPISent.add(alHAPI2);
							break;
						} else {
							// next is free ?
							continue;
						}
					}
				}
			} else if (oIT instanceof AbsoPropQGeneInserItem) {
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in buildCorrectlyListSuperHolderFromGeneListAllInsertion : oIT not instanceof"
						+ " AbsoluteProportionSGeneInsertionItem or AbsoluteProportionQGeneInsertionItem"));
			}
		}

		recalculateHAPIIndexInFullArray(alAlHAPISent, false);

	}

	public void buildCorrectlyListSuperHolderFromGeneList(
			ArrayList<Object> alQS,
			ArrayList<SuperHoldAbsoPropItem> alAlHAPISent) {

		alAlHAPISent.clear();
		for (int i = 0; i < alQS.size(); i++) {
			Object oIT = alQS.get(i);
			if (oIT instanceof AbsoPropQSGeneHomoItem) {

				// add q gene insertion before
				for (int k = 0; k < ((AbsoPropQSGeneHomoItem) oIT)
						.getTmpListOfPreviousAPQGeneInsertion().size(); k++) {
					AbsoPropQGeneInserItem apqeiIT = ((AbsoPropQSGeneHomoItem) oIT)
							.getTmpListOfPreviousAPQGeneInsertion().get(k);
					HolderAbsoluteProportionItem newAPS2 = new HolderAbsoluteProportionItem();
					newAPS2.setAbsoluteProportionQComparableQSpanItem(apqeiIT);
					SuperHoldAbsoPropItem alHAPI2 = new SuperHoldAbsoPropItem();
					alHAPI2.getListHAPI().add(newAPS2);
					alAlHAPISent.add(alHAPI2);
				}
				// add gene s insertion after
				for (int k = 0; k < ((AbsoPropQSGeneHomoItem) oIT)
						.getTmpListOfPreviousAPSGeneInsertion().size(); k++) {
					AbsoPropSGeneInserItem apseiIT = ((AbsoPropQSGeneHomoItem) oIT)
							.getTmpListOfPreviousAPSGeneInsertion().get(k);
					int idxToInsertSInsertionUnderQ = alAlHAPISent.size()
							- ((AbsoPropQSGeneHomoItem) oIT)
									.getTmpListOfPreviousAPQGeneInsertion()
									.size() + k;
					if (idxToInsertSInsertionUnderQ >= alAlHAPISent.size()) {
						// not good, insert end of array
						HolderAbsoluteProportionItem newAPS2 = new HolderAbsoluteProportionItem();
						newAPS2.setAbsoluteProportionSComparableSSpanItem(apseiIT);
						SuperHoldAbsoPropItem alHAPI2 = new SuperHoldAbsoPropItem();
						alHAPI2.getListHAPI().add(newAPS2);
						alAlHAPISent.add(alHAPI2);
					} else {
						// free s under q!
						if (alAlHAPISent.get(idxToInsertSInsertionUnderQ)
								.getListHAPI().get(0)
								.getAbsoluteProportionSComparableSSpanItem() == null) {
							alAlHAPISent
									.get(idxToInsertSInsertionUnderQ)
									.getListHAPI()
									.get(0)
									.setAbsoluteProportionSComparableSSpanItem(
											apseiIT);
						} else {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in buildCorrectlyListSuperHolderFromGeneList 1 : free s under q is not free!!"));
						}
					}
				}
				((AbsoPropQSGeneHomoItem) oIT)
						.getTmpListOfPreviousAPQGeneInsertion().clear();
				((AbsoPropQSGeneHomoItem) oIT)
						.getTmpListOfPreviousAPSGeneInsertion().clear();

				// add GH itself
				HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
				newAPS.setAbsoluteProportionQComparableQSSpanItem((AbsoPropQSGeneHomoItem) oIT);
				SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
				alHAPI.getListHAPI().add(newAPS);
				alAlHAPISent.add(alHAPI);

				// add q gene insertion after
				for (int k = 0; k < ((AbsoPropQSGeneHomoItem) oIT)
						.getTmpListOfNextAPQGeneInsertion().size(); k++) {
					AbsoPropQGeneInserItem apqeiIT = ((AbsoPropQSGeneHomoItem) oIT)
							.getTmpListOfNextAPQGeneInsertion().get(k);
					HolderAbsoluteProportionItem newAPS2 = new HolderAbsoluteProportionItem();
					newAPS2.setAbsoluteProportionQComparableQSpanItem(apqeiIT);
					SuperHoldAbsoPropItem alHAPI2 = new SuperHoldAbsoPropItem();
					alHAPI2.getListHAPI().add(newAPS2);
					alAlHAPISent.add(alHAPI2);
				}

				// add gene s insertion after
				for (int k = 0; k < ((AbsoPropQSGeneHomoItem) oIT)
						.getTmpListOfNextAPSGeneInsertion().size(); k++) {
					AbsoPropSGeneInserItem apseiIT = ((AbsoPropQSGeneHomoItem) oIT)
							.getTmpListOfNextAPSGeneInsertion().get(k);
					int idxToInsertSInsertionUnderQ = alAlHAPISent.size()
							- ((AbsoPropQSGeneHomoItem) oIT)
									.getTmpListOfNextAPQGeneInsertion().size()
							+ k;
					if (idxToInsertSInsertionUnderQ >= alAlHAPISent.size()) {
						// not good, insert end of array
						HolderAbsoluteProportionItem newAPS2 = new HolderAbsoluteProportionItem();
						newAPS2.setAbsoluteProportionSComparableSSpanItem(apseiIT);
						SuperHoldAbsoPropItem alHAPI2 = new SuperHoldAbsoPropItem();
						alHAPI2.getListHAPI().add(newAPS2);
						alAlHAPISent.add(alHAPI2);
					} else {
						// free s under q!
						if (alAlHAPISent.get(idxToInsertSInsertionUnderQ)
								.getListHAPI().get(0)
								.getAbsoluteProportionSComparableSSpanItem() == null) {
							alAlHAPISent
									.get(idxToInsertSInsertionUnderQ)
									.getListHAPI()
									.get(0)
									.setAbsoluteProportionSComparableSSpanItem(
											apseiIT);
						} else {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in buildCorrectlyListSuperHolderFromGeneList 1 : free s under q is not free!!"));
						}
					}
				}
				((AbsoPropQSGeneHomoItem) oIT)
						.getTmpListOfNextAPQGeneInsertion().clear();
				((AbsoPropQSGeneHomoItem) oIT)
						.getTmpListOfNextAPSGeneInsertion().clear();

				// deal with multi dimmension in array
				if (!((AbsoPropQSGeneHomoItem) oIT)
						.getTmpListOfOtherQSHomologiesForTheQGene().isEmpty()) {
					for (int j = 0; j < ((AbsoPropQSGeneHomoItem) oIT)
							.getTmpListOfOtherQSHomologiesForTheQGene().size(); j++) {
						AbsoPropQSGeneHomoItem otherAPQSGHI = ((AbsoPropQSGeneHomoItem) oIT)
								.getTmpListOfOtherQSHomologiesForTheQGene()
								.get(j);
						HolderAbsoluteProportionItem newOtherAPS = new HolderAbsoluteProportionItem();
						newOtherAPS
								.setAbsoluteProportionQComparableQSSpanItem(otherAPQSGHI);
						alHAPI.getListHAPI().add(newOtherAPS);
					}
					((AbsoPropQSGeneHomoItem) oIT)
							.getTmpListOfOtherQSHomologiesForTheQGene().clear();
				}

			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in buildCorrectlyListSuperHolderFromGeneList : oIT not instanceof AbsoluteProportionQSGeneHomologyItem"));
			}
		}

		recalculateHAPIIndexInFullArray(alAlHAPISent, false);

	}

	public void buildCorrectlyListSuperHolderFromSyntenyList(
			ArrayList<AbsoPropQSSyntItem> alAPSyntenySent,
			ArrayList<SuperHoldAbsoPropItem> alAlHAPISent,
			ArrayList<Integer> missingElements) {

		alAlHAPISent.clear();

		//deal with synteny
		for (int i = 0; i < alAPSyntenySent.size(); i++) {
			AbsoPropQSSyntItem apqssIT = alAPSyntenySent.get(i);
			// System.err.println("i"+i);

			// add genomic q insertion previous
			// for(int
			// j=0;j<apqssIT.getTmpListOfPreviousGenomicRegionQInsertion().size();j++){
			if (apqssIT.getTmpPreviousGenomicRegionQInsertion() != null) {
				AbsoPropQElemItem apqeiIT = apqssIT
						.getTmpPreviousGenomicRegionQInsertion();
				HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
				newAPS.setAbsoluteProportionQComparableQSpanItem(apqeiIT);
				SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
				alHAPI.getListHAPI().add(newAPS);
				alAlHAPISent.add(alHAPI);
			}
			
			apqssIT.setTmpPreviousGenomicRegionQInsertion(null);

			
			SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
			// add synteny itself
			if (apqssIT.getListOrigamiAlignmentIdsContainedOtherSyntenies().isEmpty()
					&& apqssIT.getTmpListOfOtherQSSyntenySprungOff().isEmpty()
					&& apqssIT.getSyntenyNumberGene() == 1) {
				// singleton, gene directly
				AbsoPropQSGeneHomoItem newAPQSGHI = turnQSSyntenyItemIntoQSGeneHomoItem(apqssIT);
				HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
				newAPS.setAbsoluteProportionQComparableQSSpanItem(newAPQSGHI);
				alHAPI.getListHAPI().add(newAPS);
				alAlHAPISent.add(alHAPI);
			} else {
				HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
				newAPS.setAbsoluteProportionQComparableQSSpanItem(apqssIT);
				alHAPI.getListHAPI().add(newAPS);
				alAlHAPISent.add(alHAPI);
			}

			// add genomic q insertion after
			if (apqssIT.getTmpNextGenomicRegionQInsertion() != null) {
				AbsoPropQElemItem apqeiIT = apqssIT.getTmpNextGenomicRegionQInsertion();
				HolderAbsoluteProportionItem newAPS2 = new HolderAbsoluteProportionItem();
				newAPS2.setAbsoluteProportionQComparableQSpanItem(apqeiIT);
				SuperHoldAbsoPropItem alHAPI2 = new SuperHoldAbsoPropItem();
				alHAPI2.getListHAPI().add(newAPS2);
				alAlHAPISent.add(alHAPI2);
			}
			apqssIT.setTmpNextGenomicRegionQInsertion(null);

			
			// deal with multi dimmension in array
			if (!apqssIT.getTmpListOfOtherQSSyntenySprungOff().isEmpty()) {
				for (int j = 0; j < apqssIT.getTmpListOfOtherQSSyntenySprungOff().size(); j++) {
					AbsoPropQSSyntItem otherAPQSGHI = apqssIT.getTmpListOfOtherQSSyntenySprungOff().get(j);
					if (otherAPQSGHI.getSyntenyNumberGene() == 1) {
						// singleton, gene directly
						AbsoPropQSGeneHomoItem newAPQSGHI = turnQSSyntenyItemIntoQSGeneHomoItem(otherAPQSGHI);
						HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
						newAPS.setAbsoluteProportionQComparableQSSpanItem(newAPQSGHI);
						alHAPI.getListHAPI().add(newAPS);
					} else {
						// add synteny itself
						HolderAbsoluteProportionItem newOtherAPS = new HolderAbsoluteProportionItem();
						newOtherAPS.setAbsoluteProportionQComparableQSSpanItem(otherAPQSGHI);
						alHAPI.getListHAPI().add(newOtherAPS);
					}

				}
				apqssIT.getTmpListOfOtherQSSyntenySprungOff().clear();
			}
		}

		//deal with missing elements

		//insert missingElements
		if(!missingElements.isEmpty()){
			for(int i=0;i<missingElements.size();i++){
				AbsoPropQElemItem apqeIT = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(missingElements.get(i));
				if(missingElements.get(i) == 0){
					//first element whole insertion
					HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
					AbsoPropQGenoRegiInserItem missingElementRegion = new AbsoPropQGenoRegiInserItem();
					missingElementRegion.setqOrigamiElementId(apqeIT.getqOrigamiElementId());
					missingElementRegion.setqPbStartQGenomicRegionInsertionInElement(0);
					missingElementRegion.setqPbStopQGenomicRegionInsertionInElement(apqeIT.getQsizeOfElementinPb());
					missingElementRegion
							.setqPercentStart(((double) apqeIT.getqPbStartOfElementInOrga())
									/ (double) apqeIT.getQsizeOfOragnismInPb());
					missingElementRegion
							.setqPercentStop(((double) (apqeIT.getqPbStartOfElementInOrga()+
									apqeIT.getQsizeOfElementinPb()))
									/ (double) apqeIT.getQsizeOfOragnismInPb());
					missingElementRegion.setqAccnum(apqeIT.getqAccnum());
					missingElementRegion.setqPbStartOfElementInOrga(apqeIT.getqPbStartOfElementInOrga());
					missingElementRegion.setQsizeOfOragnismInPb(apqeIT.getQsizeOfOragnismInPb());
					missingElementRegion.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
					missingElementRegion.setAnchoredToStartElement(true);
					missingElementRegion.setAnchoredToStopElement(true);
					newAPS.setAbsoluteProportionQComparableQSpanItem(missingElementRegion);
					SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
					alHAPI.getListHAPI().add(newAPS);
					alAlHAPISent.add(alHAPI);
				}else{
					//find last spot of previousElementId on array
					boolean insertedInArray = false;
					for(int j=0;j<alAlHAPISent.size();j++){
						HolderAbsoluteProportionItem hapIT = alAlHAPISent.get(j).getListHAPI().get(0);
						if(hapIT.getAbsoluteProportionQComparableQSSpanItem() != null){
							if(hapIT.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart() > apqeIT.getqPercentStart()){
								//past it...
									HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
									AbsoPropQGenoRegiInserItem missingElementRegion = new AbsoPropQGenoRegiInserItem();
									missingElementRegion.setqOrigamiElementId(apqeIT.getqOrigamiElementId());
									missingElementRegion.setqPbStartQGenomicRegionInsertionInElement(0);
									missingElementRegion.setqPbStopQGenomicRegionInsertionInElement(apqeIT.getQsizeOfElementinPb());
									missingElementRegion
											.setqPercentStart(((double) apqeIT.getqPbStartOfElementInOrga())
													/ (double) apqeIT.getQsizeOfOragnismInPb());
									missingElementRegion
											.setqPercentStop(((double) (apqeIT.getqPbStartOfElementInOrga()+
													apqeIT.getQsizeOfElementinPb()))
													/ (double) apqeIT.getQsizeOfOragnismInPb());
									missingElementRegion.setqAccnum(apqeIT.getqAccnum());
									missingElementRegion.setqPbStartOfElementInOrga(apqeIT.getqPbStartOfElementInOrga());
									missingElementRegion.setQsizeOfOragnismInPb(apqeIT.getQsizeOfOragnismInPb());
									missingElementRegion.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
									missingElementRegion.setAnchoredToStartElement(true);
									missingElementRegion.setAnchoredToStopElement(true);
									newAPS.setAbsoluteProportionQComparableQSpanItem(missingElementRegion);
									SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
									alHAPI.getListHAPI().add(newAPS);
									alAlHAPISent.add(j ,alHAPI);
									insertedInArray = true;
									break;
							}
						}else if(hapIT.getAbsoluteProportionQComparableQSpanItem() != null){
							if(hapIT.getAbsoluteProportionQComparableQSpanItem().getqPercentStart() > apqeIT.getqPercentStart()){
								//past it...
									HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
									AbsoPropQGenoRegiInserItem missingElementRegion = new AbsoPropQGenoRegiInserItem();
									missingElementRegion.setqOrigamiElementId(apqeIT.getqOrigamiElementId());
									missingElementRegion
											.setqPbStartQGenomicRegionInsertionInElement(0);
									missingElementRegion
											.setqPbStopQGenomicRegionInsertionInElement(apqeIT.getQsizeOfElementinPb());
									missingElementRegion
											.setqPercentStart(((double) apqeIT.getqPbStartOfElementInOrga())
													/ (double) apqeIT.getQsizeOfOragnismInPb());
									missingElementRegion
											.setqPercentStop(((double) (apqeIT.getqPbStartOfElementInOrga()+
													apqeIT.getQsizeOfElementinPb()))
													/ (double) apqeIT.getQsizeOfOragnismInPb());
									missingElementRegion.setqAccnum(apqeIT.getqAccnum());
									missingElementRegion
											.setqPbStartOfElementInOrga(apqeIT.getqPbStartOfElementInOrga());
									missingElementRegion
											.setQsizeOfOragnismInPb(apqeIT.getQsizeOfOragnismInPb());
									missingElementRegion
											.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
									missingElementRegion.setAnchoredToStartElement(true);
									missingElementRegion.setAnchoredToStopElement(true);
									newAPS.setAbsoluteProportionQComparableQSpanItem(missingElementRegion);
									SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
									alHAPI.getListHAPI().add(newAPS);
									alAlHAPISent.add(j ,alHAPI);
									insertedInArray = true;
									break;
							}
						}
					}
					if(!insertedInArray){
						//insert at the end
						HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
						AbsoPropQGenoRegiInserItem missingElementRegion = new AbsoPropQGenoRegiInserItem();
						missingElementRegion.setqOrigamiElementId(apqeIT.getqOrigamiElementId());
						missingElementRegion
								.setqPbStartQGenomicRegionInsertionInElement(0);
						missingElementRegion
								.setqPbStopQGenomicRegionInsertionInElement(apqeIT.getQsizeOfElementinPb());
						missingElementRegion
								.setqPercentStart(((double) apqeIT.getqPbStartOfElementInOrga())
										/ (double) apqeIT.getQsizeOfOragnismInPb());
						missingElementRegion
								.setqPercentStop(((double) (apqeIT.getqPbStartOfElementInOrga()+
										apqeIT.getQsizeOfElementinPb()))
										/ (double) apqeIT.getQsizeOfOragnismInPb());
						missingElementRegion.setqAccnum(apqeIT.getqAccnum());
						missingElementRegion
								.setqPbStartOfElementInOrga(apqeIT.getqPbStartOfElementInOrga());
						missingElementRegion
								.setQsizeOfOragnismInPb(apqeIT.getQsizeOfOragnismInPb());
						missingElementRegion
								.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
						missingElementRegion.setAnchoredToStartElement(true);
						missingElementRegion.setAnchoredToStopElement(true);
						newAPS.setAbsoluteProportionQComparableQSpanItem(missingElementRegion);
						SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
						alHAPI.getListHAPI().add(newAPS);
						alAlHAPISent.add(alHAPI);
					}
				}
			}
		}
		
		// recalculate idx
		recalculateHAPIIndexInFullArray(alAlHAPISent, false);
		
	}


	private AbsoPropQSGeneHomoItem turnQSSyntenyItemIntoQSGeneHomoItem(
			AbsoPropQSSyntItem apqssIT) {
		
		AbsoPropQSGeneHomoItem newAPQSGHI = new AbsoPropQSGeneHomoItem();
		newAPQSGHI.setSyntenyOrigamiAlignmentId(apqssIT.getSyntenyOrigamiAlignmentId());
		newAPQSGHI.setQsQGeneId(-1);
		newAPQSGHI.setQsSGeneId(-1);
		// newAPQSGHI.setQsOrigamiAlignmentPairsType();
		GeneWidgetStyleItem gwst = new GeneWidgetStyleItem();
		gwst.setBkgColor(getNextAvailableBkgColor(newAPQSGHI.getSyntenyNumberGene()));
		newAPQSGHI.setQsStyleItem(gwst);
		// basic info, tmp
		newAPQSGHI.setQsEnumBlockType(apqssIT.getQsEnumBlockType());
		// fill new data
		
		newAPQSGHI.setQsQPbStartGeneInElement(apqssIT.getPbQStartSyntenyInElement());
		newAPQSGHI.setQsQPbStopGeneInElement(apqssIT.getPbQStopSyntenyInElement());
		newAPQSGHI.setQsSPbStartGeneInElement(apqssIT.getPbSStartSyntenyInElement());
		newAPQSGHI.setQsSPbStopGeneInElement(apqssIT.getPbSStopSyntenyInElement());
		// then compute qPercentstart et qPercentstop et sPercentstart
		// et sPercentstop for each gene homology
		newAPQSGHI.setqPercentStart(apqssIT.getqPercentStart());
		newAPQSGHI.setqPercentStop(apqssIT.getqPercentStop());
		newAPQSGHI.setsPercentStart(apqssIT.getsPercentStart());
		newAPQSGHI.setsPercentStop(apqssIT.getsPercentStop());
		// set other vital info
		newAPQSGHI.setQsQOrigamiElementId(apqssIT.getQsQOrigamiElementId());
		newAPQSGHI.setQsQAccnum(apqssIT.getQsQAccnum());
		newAPQSGHI.setQsQPbStartOfElementInOrga(apqssIT.getQsQPbStartOfElementInOrga());
		newAPQSGHI.setQsQSizeOfOrganismInPb(apqssIT.getQsQSizeOfOrganismInPb());
		
		newAPQSGHI.setQsSOrigamiElementId(apqssIT.getQsSOrigamiElementId());
		newAPQSGHI.setQsSAccnum(apqssIT.getQsSAccnum());
		newAPQSGHI.setQsSPbStartOfElementInOrga(apqssIT.getQsSPbStartOfElementInOrga());
		newAPQSGHI.setQsSSizeOfOrganismInPb(apqssIT.getQsSSizeOfOrganismInPb());
		newAPQSGHI.setNextGenomicRegionSInsertion(apqssIT.getNextGenomicRegionSInsertion());
		newAPQSGHI.setPreviousGenomicRegionSInsertion(apqssIT.getPreviousGenomicRegionSInsertion());
		
		return newAPQSGHI;
		
	}

	public void buildCorrectlyListSuperHolderFromAbsoluteProportionSGeneInsertionItemList(
			ArrayList<AbsoPropSGeneInserItem> listapsgiiIT,
			ArrayList<SuperHoldAbsoPropItem> alAlHAPISent) {

		alAlHAPISent.clear();
		for (int i = 0; i < listapsgiiIT.size(); i++) {
			AbsoPropSGeneInserItem apsgiiIt = listapsgiiIT.get(i);
			// add s insert itself
			HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
			newAPS.setAbsoluteProportionSComparableSSpanItem(apsgiiIt);
			SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
			alHAPI.getListHAPI().add(newAPS);
			alAlHAPISent.add(alHAPI);

		}

	}

	public void buildCorrectlyListSuperHolderFromAbsoluteProportionQGeneInsertionItemList(
			ArrayList<AbsoPropQGeneInserItem> listapqgiiIT,
			ArrayList<SuperHoldAbsoPropItem> alAlHAPISent) {

		alAlHAPISent.clear();
		for (int i = 0; i < listapqgiiIT.size(); i++) {
			AbsoPropQGeneInserItem apsgiiIt = listapqgiiIT.get(i);
			// add q insert itself
			HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem();
			newAPS.setAbsoluteProportionQComparableQSpanItem(apsgiiIt);
			SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
			alHAPI.getListHAPI().add(newAPS);
			alAlHAPISent.add(alHAPI);
		}
	}

	//
	// public void
	// insertCorrectlyAbsoluteProportionQAndSItemsIntoListDisplayedItem(
	// ArrayList<AbsoluteProportionQComparableItem>
	// tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS,
	// ArrayList<AbsoluteProportionSComparableSSpanItem>
	// tmpfullAssociatedlistAbsoluteProportionSyntenyItemS,
	// ArrayList<SuperHolderAbsoluteProportionItem> alAlHAPISent) {
	//
	// if(tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS != null){
	// if(tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS.size() > 0){
	// //sort and insert correctly QS and Q item
	// //Collections.sort(tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS);
	// //crunchQSSyntenyOrGeneHomologyIntoOneSlotAndListOfTmpOther(tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS);
	// //turnQContainingFeaturesIntoCutQStartAndStop(tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS);
	// insertQItemsIntoListDisplayedItems(tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS,
	// alAlHAPISent);
	// }
	// }
	//
	// if(tmpfullAssociatedlistAbsoluteProportionSyntenyItemS != null){
	// if(tmpfullAssociatedlistAbsoluteProportionSyntenyItemS.size() > 0){
	// //Collections.sort(tmpfullAssociatedlistAbsoluteProportionSyntenyItemS);
	// //turnSContainingFeaturesIntoCutSStartAndStop(tmpfullAssociatedlistAbsoluteProportionSyntenyItemS);
	// insertSItemsIntoListAbsoluteProportionQItems(alAlHAPISent,
	// tmpfullAssociatedlistAbsoluteProportionSyntenyItemS);
	// }
	// }
	//
	// recalculateHAPIIndexInFullArray(alAlHAPISent, false);
	//
	// }
	//
	//

	public void recalculateHAPIIndexInFullArray(
			ArrayList<SuperHoldAbsoPropItem> alAlHAPISent,
			boolean recalculatePartialArrayIfNeeded) {

		for (int i = 0; i < alAlHAPISent.size(); i++) {
			for (int j = 0; j < alAlHAPISent.get(i).getListHAPI().size(); j++) {
				HolderAbsoluteProportionItem hapiIT = alAlHAPISent.get(i)
						.getListHAPI().get(j);
				hapiIT.setIndexInFullArray(i);
			}
		}

	}

	public boolean crunchSGeneInsertionIntoQSTmpList(ArrayList<Object> alQSAndS) {
		ArrayList<Integer> indexToDelInOriginalListAfterward = new ArrayList<Integer>();

		boolean atLeastOneQS = false;
		LOOP1crunchSGeneInsertionIntoQSTmpList: for (int i = 0; i < alQSAndS
				.size(); i++) {
			Object oIT = alQSAndS.get(i);
			// System.err.println("here avant");
			if (oIT instanceof AbsoPropQSGeneHomoItem) {
				// System.err.println("here apres");
				atLeastOneQS = true;
				int startJ = (i + 1);
				for (int j = startJ; j < alQSAndS.size(); j++) {
					Object oNext = alQSAndS.get(j);
					if (oNext instanceof AbsoPropQSGeneHomoItem) {
						atLeastOneQS = true;
						continue LOOP1crunchSGeneInsertionIntoQSTmpList;
					} else if (oNext instanceof AbsoPropSGeneInserItem) {
						// put AbsoluteProportionSGeneInsertionItem into list
						// tmp
						((AbsoPropQSGeneHomoItem) oIT)
								.getTmpListOfNextAPSGeneInsertion()
								.add((AbsoPropSGeneInserItem) oNext);
						indexToDelInOriginalListAfterward.add(j);
						i++;
						continue;
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in crunchSGeneInsertionIntoQSTmpList : oNext not instanceof AbsoluteProportionQSGeneHomologyItem or AbsoluteProportionSGeneInsertionItem"));
					}

				}
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in crunchSGeneInsertionIntoQSTmpList : oIT not instanceof AbsoluteProportionQSGeneHomologyItem"));
			}
		}
		// del index in indexToDelInOriginalListAfterward
		Collections.sort(indexToDelInOriginalListAfterward);
		if (!indexToDelInOriginalListAfterward.isEmpty()) {
			// for(int k=0;k<indexToDelAfterward.size();k++){
			for (int k = (indexToDelInOriginalListAfterward.size() - 1); k >= 0; k--) {
				// System.out.println("deleting "+(int)
				// indexToDelAfterward.get(k));
				alQSAndS.remove((int) indexToDelInOriginalListAfterward.get(k));
			}
		}

		return atLeastOneQS;
	}

	public boolean crunchQSGeneHomologyListOfTmpOtherAndQGeneInsertionIntoQSTmpList(
			ArrayList<Object> alQSAndQ) {

		boolean atLeastOneQS = false;

		ArrayList<Integer> indexToDelInOriginalListAfterward = new ArrayList<Integer>();

		LOOP1crunchQSGeneHomologyListOfTmpOtherAndQGeneInsertionIntoQSTmpList: for (int i = 0; i < alQSAndQ
				.size(); i++) {
			Object oIT = alQSAndQ.get(i);

			if (oIT instanceof AbsoPropQGeneInserItem) {
				int startJ = (i + 1);
				for (int j = startJ; j < alQSAndQ.size(); j++) {
					Object oNext = alQSAndQ.get(j);
					if (oNext instanceof AbsoPropQSGeneHomoItem) {
						atLeastOneQS = true;
						// put in tmpbefore
						((AbsoPropQSGeneHomoItem) oNext)
								.getTmpListOfPreviousAPQGeneInsertion()
								.add((AbsoPropQGeneInserItem) oIT);
						indexToDelInOriginalListAfterward.add(i);
						continue LOOP1crunchQSGeneHomologyListOfTmpOtherAndQGeneInsertionIntoQSTmpList;
					} else if (oNext instanceof AbsoPropQGeneInserItem) {
						continue;
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in crunchQSGeneHomologyListOfTmpOtherAndQGeneInsertionIntoQSTmpList : oNext not instanceof AbsoluteProportionQSGeneHomologyItem or AbsoluteProportionQGeneInsertionItem"));
					}
				}
			} else if (oIT instanceof AbsoPropQSGeneHomoItem) {
				atLeastOneQS = true;
				int startJ = (i + 1);
				for (int j = startJ; j < alQSAndQ.size(); j++) {
					Object oNext = alQSAndQ.get(j);
					if (oNext instanceof AbsoPropQSGeneHomoItem) {
						// double iTQStop = apqciIT.getqPercentStop();
						// double nextQStart = apqciNext.getqPercentStart();
						int iTQGeneId = ((AbsoPropQSGeneHomoItem) oIT)
								.getQsQGeneId();
						int nextQGeneId = ((AbsoPropQSGeneHomoItem) oNext)
								.getQsQGeneId();

						// if(nextQStart <= iTQStop){
						if (iTQGeneId == nextQGeneId) {
							((AbsoPropQSGeneHomoItem) oIT)
									.getTmpListOfOtherQSHomologiesForTheQGene()
									.add((AbsoPropQSGeneHomoItem) oNext);
							indexToDelInOriginalListAfterward.add(j);
							i++;
							continue;
						} else {
							// different GH
							continue LOOP1crunchQSGeneHomologyListOfTmpOtherAndQGeneInsertionIntoQSTmpList;
						}
					} else if (oNext instanceof AbsoPropQGeneInserItem) {
						// put AbsoluteProportionSGeneInsertionItem into list
						// tmp
						((AbsoPropQSGeneHomoItem) oIT)
								.getTmpListOfNextAPQGeneInsertion()
								.add((AbsoPropQGeneInserItem) oNext);
						indexToDelInOriginalListAfterward.add(j);
						i++;
						continue;
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in crunchQSGeneHomologyListOfTmpOtherAndQGeneInsertionIntoQSTmpList : oNext not instanceof AbsoluteProportionQSGeneHomologyItem or AbsoluteProportionQGeneInsertionItem"));
					}
				}
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in crunchQSGeneHomologyListOfTmpOtherAndQGeneInsertionIntoQSTmpList : oIT not instanceof AbsoluteProportionQSGeneHomologyItem or AbsoluteProportionQGeneInsertionItem"));
			}
		}

		// del index in indexToDelInOriginalListAfterward
		Collections.sort(indexToDelInOriginalListAfterward);
		if (!indexToDelInOriginalListAfterward.isEmpty()) {
			// for(int k=0;k<indexToDelAfterward.size();k++){
			for (int k = (indexToDelInOriginalListAfterward.size() - 1); k >= 0; k--) {
				// System.out.println("deleting "+(int)
				// indexToDelAfterward.get(k));
				alQSAndQ.remove((int) indexToDelInOriginalListAfterward.get(k));
			}
		}

		return atLeastOneQS;

	}


	public static int getNextAvailableBkgColor(
			int numberGeneInSynteny
			) {
		if (numberGeneInSynteny > 1) {
			NEXT_AVAILABLE_BKG_COLOR++;
			if (NEXT_AVAILABLE_BKG_COLOR > 4) {
				NEXT_AVAILABLE_BKG_COLOR = NEXT_AVAILABLE_BKG_COLOR % 5;
			}
			return NEXT_AVAILABLE_BKG_COLOR;
		} else {
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error CommonMethodsViewResults -> getNextAvailableBkgColor,"
//					+ " numberGeneInSynteny not recognized:"+numberGeneInSynteny));
			return -1;
		}
	}

}
