package fr.inra.jouy.client.view.result;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import fr.inra.jouy.client.AppController;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.RPC.CallForHomoBrowResu;
import fr.inra.jouy.client.RPC.CallForHomoBrowResuAsync;
import fr.inra.jouy.shared.pojos.applicationItems.GeneWidgetStyleItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSSyntItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.pojos.symbols.SuperHoldAbsoPropItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem.QSEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem.QEnumBlockType;
import fr.inra.jouy.shared.TransAbsoPropQSSyntItem;
import fr.inra.jouy.shared.TransAbsoPropResuGeneSet;
import fr.inra.jouy.shared.TransAbsoPropResuGeneSetHold;
import fr.inra.jouy.shared.TransAbsoPropResuHold;
import fr.inra.jouy.shared.comparators.AbsoPropQComparators;
import fr.inra.jouy.shared.comparators.AbsoPropQSSyntItemComparators;

public class ResuLoadInitAbsoPropItemDial {

//	private static ResultLoadingInitialAbsoluteProportionItemDialogUiBinder uiBinder = GWT
//			.create(ResultLoadingInitialAbsoluteProportionItemDialogUiBinder.class);
//
//	interface ResultLoadingInitialAbsoluteProportionItemDialogUiBinder extends
//			UiBinder<Widget, ResuLoadInitAbsoPropItemDial> {
//	}

	private final CallForHomoBrowResuAsync homologieBrowsingResultService = (CallForHomoBrowResuAsync) GWT
			.create(CallForHomoBrowResu.class);
//	
//	public enum TypeOfLoadingToDo {
//		LOAD_ItemsAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertion
//	}
//
	
	public ResuLoadInitAbsoPropItemDial(){
		
	}
			
			
//	public ResuLoadInitAbsoPropItemDial(
//			TypeOfLoadingToDo typeOfLoadingToDo,
//			final int sOrigamiOrgaIdSent,
//			final int indexGenomePanelInLIST_GENOME_PANEL,
//			final EnumResultViewTypes viewTypeSent,
//			ArrayList<Integer> listReferenceGeneSet) {
//		setWidget(uiBinder.createAndBindUi(this));
//		
//		if (typeOfLoadingToDo
//				.compareTo(TypeOfLoadingToDo.LOAD_ItemsAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertion) == 0) {
//			if(viewTypeSent.compareTo(EnumResultViewTypes.homolog_table)==0){
//				loadItemsAbsoluteProportionGeneSet(sOrigamiOrgaIdSent, indexGenomePanelInLIST_GENOME_PANEL, listReferenceGeneSet);
//			}else if(viewTypeSent.compareTo(EnumResultViewTypes.genomic_organization)==0){
//				loadItemsAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertion(sOrigamiOrgaIdSent, indexGenomePanelInLIST_GENOME_PANEL);
//			}else{
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in ResultLoadingInitialAbsoluteProportionItemDialog : unrecognized view type = "+viewTypeSent));
//			}
//			
//			
//		} else {
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in ResultLoadingInitialAbsoluteProportionItemDialog : unrecognized TypeOfLoadingToDo = "+typeOfLoadingToDo));
//			//Window.alert("ERROR in ResultLoadingInitialAbsoluteProportionItemDialog : unrecognized TypeOfLoadingToDo");
//		}
//
//	}

	public void loadItemsAbsoluteProportionGeneSet(final int sOrigamiOrgaIdSent,
			final int indexGenomePanelInLIST_GENOME_PANEL,
			ArrayList<Integer> listReferenceGeneSet) {
		
		//System.err.println("loadItemsAbsoluteProportionGeneSet");

		AsyncCallback<TransAbsoPropResuGeneSetHold> callback = new AsyncCallback<TransAbsoPropResuGeneSetHold>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("Error", true);

				AppController.backFromCallBackCheckingOfLoadingMask();
//				AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
//				Insyght.INSYGHT_LOADING_MASK.hide();
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(TransAbsoPropResuGeneSetHold hirs) {

				if(
						//hirs.getAlAPSEI().isEmpty() || //can be empty now if ordering of element known already
						hirs.getAlAPGeneSet().isEmpty()){
					//show no result
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("No results to display", false);
				
				}else{
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){

						//order of molecules is undefined at first, default sort by size
						if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().isEmpty()){
							if(hirs.getAlAPSEI().isEmpty()) {
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
										new Throwable(
												"Error in loadItemsAbsoluteProportionGeneSet onSuccess : hirs.getAlAPSEI().isEmpty() for "
												+ "reference organism = "
												+ Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getFullName()
												+ "compared organism = "
												+ Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getFullName()
												)
										);
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("Error", true);
								return;
							} else {
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
								.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.setListAbsoluteProportionElementItemS(hirs.getAlAPSEI());
							}

						}
						
						ArrayList<Object> alAPGeneSet = new ArrayList<Object>();
						//create color for listOrigamiSyntenyId

						HashMap<Long,Integer> origamiSyntenyId2Color = new HashMap<Long, Integer>();
						for(int k=0;k<hirs.getAlAPGeneSet().size();k++){
							TransAbsoPropResuGeneSet apqsTransientGSIT = hirs.getAlAPGeneSet().get(k);
							if(apqsTransientGSIT.getAlignmentId() != 0){
								if(!origamiSyntenyId2Color.containsKey(apqsTransientGSIT.getAlignmentId())){
									int colorForThisOriAliId = CommonMethodsViewResults.getNextAvailableBkgColor(apqsTransientGSIT.getNumberGeneInSynteny());
									origamiSyntenyId2Color.put(apqsTransientGSIT.getAlignmentId(), colorForThisOriAliId);
								}
							}
						}
						
						HashMap<Integer,AbsoPropQElemItem> qOrigamiElementId2AbsoluteProportionQElementItem = new HashMap<Integer, AbsoPropQElemItem>();
						for(int i=0;i<Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().size();i++){
							AbsoPropQElemItem apqeiIT = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(i);
							int qOrigamiElementId = apqeiIT.getqOrigamiElementId();
							qOrigamiElementId2AbsoluteProportionQElementItem.put(qOrigamiElementId, apqeiIT);
						}
						
						HashMap<Integer,AbsoPropSElemItem> sOrigamiElementId2AbsoluteProportionSElementItem = new HashMap<Integer, AbsoPropSElemItem>();
						for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().size();i++){
							AbsoPropSElemItem apseiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().get(i);
							int sOrigamiElementId = apseiIT.getsOrigamiElementId();
							sOrigamiElementId2AbsoluteProportionSElementItem.put(sOrigamiElementId, apseiIT);
						}
						/*for(int i=0;i<hirs.getAlAPSEI().size();i++){
							AbsoPropSElemItem apseiIT = hirs.getAlAPSEI().get(i);
							int sOrigamiElementId = apseiIT.getsOrigamiElementId();
							sOrigamiElementId2AbsoluteProportionSElementItem.put(sOrigamiElementId, apseiIT);
						}*/
						
						//for each TransientAbsoluteProportionQSSyntenyItem,
						for(int k=0;k<hirs.getAlAPGeneSet().size();k++){
							TransAbsoPropResuGeneSet apqsTransientGSIT = hirs.getAlAPGeneSet().get(k);
							
							if(apqsTransientGSIT.getsGeneId() < 0
									|| apqsTransientGSIT.getType() > 2){
								//q gene insertion
								AbsoPropQGeneInserItem apqsGIIT = new AbsoPropQGeneInserItem(apqsTransientGSIT);

								apqsGIIT.setQsizeOfElementinPb(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsGIIT.getqOrigamiElementId()).getQsizeOfElementinPb());
								apqsGIIT.setqPbStartOfElementInOrga(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsGIIT.getqOrigamiElementId()).getqPbStartOfElementInOrga());
								apqsGIIT.setQsizeOfOragnismInPb(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsGIIT.getqOrigamiElementId()).getQsizeOfOragnismInPb());
								apqsGIIT.setqAccnum(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsGIIT.getqOrigamiElementId()).getqAccnum());

								//apqsGIIT.setQsSSizeOfElementinPb(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsGIIT.getQsSOrigamiElementId()).getsSizeOfElementinPb());
								//apqsGIIT.setQsSPbStartOfElementInOrga(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsGIIT.getQsSOrigamiElementId()).getsPbStartOfElementInOrga());
								//apqsGIIT.setQsSSizeOfOrganismInPb(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsGIIT.getQsSOrigamiElementId()).getsSizeOfOragnismInPb());
								//apqsGIIT.setQsSAccnum(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsGIIT.getQsSOrigamiElementId()).getsAccnum());
								
								//then compute qPercentstart et qPercentstop for each synteny
								apqsGIIT.setqPercentStart(((double)apqsGIIT.getqPbStartOfElementInOrga()+(double)apqsGIIT.getqPbStartGeneInElement())/(double)apqsGIIT.getQsizeOfOragnismInPb());
								apqsGIIT.setqPercentStop(((double)apqsGIIT.getqPbStartOfElementInOrga()+(double)apqsGIIT.getqPbStopGeneInElement())/(double)apqsGIIT.getQsizeOfOragnismInPb());
								//apqsGIIT.setsPercentStart(((double)apqsGIIT.getQsSPbStartOfElementInOrga()+(double)apqsSyntenyIT.getPbSStartSyntenyInElement())/(double)apqsSyntenyIT.getQsSSizeOfOrganismInPb());
								//apqsGIIT.setsPercentStop(((double)apqsGIIT.getQsSPbStartOfElementInOrga()+(double)apqsSyntenyIT.getPbSStopSyntenyInElement())/(double)apqsSyntenyIT.getQsSSizeOfOrganismInPb());
								
								apqsGIIT.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
								
								alAPGeneSet.add(apqsGIIT);
								
							}else{
								//gene homolog
								
								AbsoPropQSGeneHomoItem apqsGHIT = new AbsoPropQSGeneHomoItem(apqsTransientGSIT);

								apqsGHIT.setQsQSizeOfElementinPb(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsGHIT.getQsQOrigamiElementId()).getQsizeOfElementinPb());
								apqsGHIT.setQsQPbStartOfElementInOrga(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsGHIT.getQsQOrigamiElementId()).getqPbStartOfElementInOrga());
								apqsGHIT.setQsQSizeOfOrganismInPb(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsGHIT.getQsQOrigamiElementId()).getQsizeOfOragnismInPb());
								apqsGHIT.setQsQAccnum(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsGHIT.getQsQOrigamiElementId()).getqAccnum());

								apqsGHIT.setQsSSizeOfElementinPb(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsGHIT.getQsSOrigamiElementId()).getsSizeOfElementinPb());
								apqsGHIT.setQsSPbStartOfElementInOrga(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsGHIT.getQsSOrigamiElementId()).getsPbStartOfElementInOrga());
								apqsGHIT.setQsSSizeOfOrganismInPb(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsGHIT.getQsSOrigamiElementId()).getsSizeOfOragnismInPb());
								apqsGHIT.setQsSAccnum(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsGHIT.getQsSOrigamiElementId()).getsAccnum());
								
								//then compute qPercentstart et qPercentstop for each synteny
								apqsGHIT.setqPercentStart(((double)apqsGHIT.getQsQPbStartOfElementInOrga()+(double)apqsGHIT.getQsQPbStartGeneInElement())/(double)apqsGHIT.getQsQSizeOfOrganismInPb());
								apqsGHIT.setqPercentStop(((double)apqsGHIT.getQsQPbStartOfElementInOrga()+(double)apqsGHIT.getQsQPbStopGeneInElement())/(double)apqsGHIT.getQsQSizeOfOrganismInPb());
								apqsGHIT.setsPercentStart(((double)apqsGHIT.getQsSPbStartOfElementInOrga()+(double)apqsGHIT.getQsSPbStartGeneInElement())/(double)apqsGHIT.getQsSSizeOfOrganismInPb());
								apqsGHIT.setsPercentStop(((double)apqsGHIT.getQsSPbStartOfElementInOrga()+(double)apqsGHIT.getQsSPbStopGeneInElement())/(double)apqsGHIT.getQsSSizeOfOrganismInPb());

								apqsGHIT.setQsEnumBlockType(QSEnumBlockType.QS_HOMOLOGS_BLOCK);
								if(apqsGHIT.getSyntenyOrigamiAlignmentId()!=0){
									GeneWidgetStyleItem gwst = new GeneWidgetStyleItem();
									gwst.setBkgColor(origamiSyntenyId2Color.get(apqsGHIT.getSyntenyOrigamiAlignmentId()));
									apqsGHIT.setQsStyleItem(gwst);
								}
								alAPGeneSet.add(apqsGHIT);
								
							}
						}

						CommonMethodsViewResults cmvrIT = new CommonMethodsViewResults();
						//Comparator<Object> compaByPbQPercentStartQSGeneHomoAndQGeneInsertComparatorBisIT = new ByPbQPercentStartQSGeneHomoAndQGeneInsertComparatorBis();
						Collections.sort(alAPGeneSet, AbsoPropQComparators.byPbQPercentStartQSGeneHomoAndQGeneInsertComparatorBis);// compaByPbQPercentStartQSGeneHomoAndQGeneInsertComparatorBisIT);
						boolean atLeastOneQS = cmvrIT.crunchQSGeneHomologyListOfTmpOtherAndQGeneInsertionIntoQSTmpList(alAPGeneSet);
						ArrayList<SuperHoldAbsoPropItem> alHAPIIT = new ArrayList<SuperHoldAbsoPropItem>();
						if(atLeastOneQS){
							cmvrIT.buildCorrectlyListSuperHolderFromGeneList(alAPGeneSet, alHAPIIT);
						}else{
							cmvrIT.buildCorrectlyListSuperHolderFromGeneListAllInsertion(alAPGeneSet, alHAPIIT);
						}
						
						
						//set list appropriately
						Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
								.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.setFullAssociatedlistAbsoluteProportionItemToDisplay(alHAPIIT);

						
						//AppController.LIST_GENOME_PANEL
						//.get(indexGenomePanelInLIST_GENOME_PANEL).setWaitingForSyntenyViewToFinishLoading(false);
						//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).checkEverythingIsLoadedBeforeDrawingClassicSyntenyView();
						//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0);
						if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0
								&& Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getLookUpForGeneWithQStart() < 0){
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
						}
						Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.clearWholeCanvas();
						Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawViewToCanvas(1.0, 0, true, true, false, false);
					}
				}
				
				AppController.backFromCallBackCheckingOfLoadingMask();
//				AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
//				if (AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
//					Scheduler.get().scheduleDeferred(new ScheduledCommand() {
//						// @Override
//						public void execute() {
//							Insyght.INSYGHT_LOADING_MASK.hide();
//						}
//					});
//				}
				
			}
			
			
		};

		try {
			
//			ArrayList<Integer> alQGeneIds = new ArrayList<Integer>();
//			for(int i=0;i<listReferenceGeneSet.size();i++){
//				alQGeneIds.add(listReferenceGeneSet.get(i).getGeneId());
//			}

			

//			if (sOrigamiOrgaIdSent == 2) {
//				GWT.log("HERE sOrigamiOrgaIdSent="+sOrigamiOrgaIdSent); //+" listReferenceGeneSet="+listReferenceGeneSet.toString()
//			}
			
			homologieBrowsingResultService
					.getTransientAbsoluteProportionType1Or2ResultGeneSetHolderWithOrgaIdAndSOrgaIdAndALQGeneIds(
							//Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ()
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId()
							, sOrigamiOrgaIdSent
							//, Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getAlignmentParametersForSynteny()
							, listReferenceGeneSet
							, Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().isEmpty()
							, callback);
			Insyght.INSYGHT_LOADING_MASK.show();
			Insyght.INSYGHT_LOADING_MASK.center();
			AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;
		} catch (Exception e) {
			//String mssgError = "ERROR try in loadItemsAbsoluteProportionGeneSet: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("Error", true);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			Insyght.INSYGHT_LOADING_MASK.hide();
		}
		
	}

	public void loadItemsAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertion(
			final int sOrigamiOrgaIdSent,
			final int indexGenomePanelInLIST_GENOME_PANEL) {
		
		AsyncCallback<TransAbsoPropResuHold> callback = new AsyncCallback<TransAbsoPropResuHold>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("Error", true);
				AppController.backFromCallBackCheckingOfLoadingMask();
//				AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
//				Insyght.INSYGHT_LOADING_MASK.hide();
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(TransAbsoPropResuHold hirs) {
				
				//long milli = System.currentTimeMillis(); // TEST remove test time

				if(//hirs.getAlAPSEI().isEmpty() || //can be empty now if ordering of element known already
						hirs.getAlAPSyntenyItem().isEmpty()){
					//show no result
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("No results to display", false);
				
				}else{

					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
						
						//order of molecules is undefined at first, default sort by size
						if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().isEmpty()){
							if(hirs.getAlAPSEI().isEmpty()) {
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
										new Throwable(
												"Error in loadItemsAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertion onSuccess : hirs.getAlAPSEI().isEmpty() for "
												+ "reference organism = "
												+ Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getFullName()
												+ "compared organism = "
												+ Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getFullName()
												)
										);
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("Error", true);
								return;
							} else {
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
								.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.setListAbsoluteProportionElementItemS(hirs.getAlAPSEI());
							}

						}
						
						ArrayList<AbsoPropQSSyntItem> alAPSyntenyOnly = new ArrayList<AbsoPropQSSyntItem>();
						
						HashMap<Integer,AbsoPropQElemItem> qOrigamiElementId2AbsoluteProportionQElementItem = new HashMap<Integer, AbsoPropQElemItem>();
						for(int i=0;i<Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().size();i++){
							AbsoPropQElemItem apqeiIT = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(i);
							int qOrigamiElementId = apqeiIT.getqOrigamiElementId();
							
							//System.out.println(qOrigamiElementId);
							qOrigamiElementId2AbsoluteProportionQElementItem.put(qOrigamiElementId, apqeiIT);
						}
						HashMap<Integer,AbsoPropSElemItem> sOrigamiElementId2AbsoluteProportionSElementItem = new HashMap<Integer, AbsoPropSElemItem>();
						for(int i=0;i<Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().size();i++){
							AbsoPropSElemItem apseiIT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().get(i);
							int sOrigamiElementId = apseiIT.getsOrigamiElementId();
							sOrigamiElementId2AbsoluteProportionSElementItem.put(sOrigamiElementId, apseiIT);
						}
//						HashMap<Integer,AbsoPropSElemItem> sOrigamiElementId2AbsoluteProportionSElementItem = new HashMap<Integer, AbsoPropSElemItem>();
//						for(int i=0;i<hirs.getAlAPSEI().size();i++){
//							AbsoPropSElemItem apseiIT = hirs.getAlAPSEI().get(i);
//							int sOrigamiElementId = apseiIT.getsOrigamiElementId();
//							sOrigamiElementId2AbsoluteProportionSElementItem.put(sOrigamiElementId, apseiIT);
//						}
						
						//for each TransientAbsoluteProportionQSSyntenyItem,
						for(int k=0;k<hirs.getAlAPSyntenyItem().size();k++){
							TransAbsoPropQSSyntItem apqsTransientSyntenyIT = hirs.getAlAPSyntenyItem().get(k);
							
							//build the real AbsoluteProportionQSSyntenyItem
							AbsoPropQSSyntItem apqsSyntenyIT = new AbsoPropQSSyntItem(apqsTransientSyntenyIT);


							apqsSyntenyIT.setQsQSizeOfElementinPb(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsSyntenyIT.getQsQOrigamiElementId()).getQsizeOfElementinPb());
							apqsSyntenyIT.setQsQPbStartOfElementInOrga(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsSyntenyIT.getQsQOrigamiElementId()).getqPbStartOfElementInOrga());
							apqsSyntenyIT.setQsQSizeOfOrganismInPb(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsSyntenyIT.getQsQOrigamiElementId()).getQsizeOfOragnismInPb());
							apqsSyntenyIT.setQsQAccnum(qOrigamiElementId2AbsoluteProportionQElementItem.get(apqsSyntenyIT.getQsQOrigamiElementId()).getqAccnum());

							apqsSyntenyIT.setQsSSizeOfElementinPb(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsSyntenyIT.getQsSOrigamiElementId()).getsSizeOfElementinPb());
							apqsSyntenyIT.setQsSPbStartOfElementInOrga(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsSyntenyIT.getQsSOrigamiElementId()).getsPbStartOfElementInOrga());
							apqsSyntenyIT.setQsSSizeOfOrganismInPb(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsSyntenyIT.getQsSOrigamiElementId()).getsSizeOfOragnismInPb());
							apqsSyntenyIT.setQsSAccnum(sOrigamiElementId2AbsoluteProportionSElementItem.get(apqsSyntenyIT.getQsSOrigamiElementId()).getsAccnum());
							
							//then compute qPercentstart et qPercentstop for each synteny
							apqsSyntenyIT.setqPercentStart(((double)apqsSyntenyIT.getQsQPbStartOfElementInOrga()+(double)apqsSyntenyIT.getPbQStartSyntenyInElement())/(double)apqsSyntenyIT.getQsQSizeOfOrganismInPb());
							apqsSyntenyIT.setqPercentStop(((double)apqsSyntenyIT.getQsQPbStartOfElementInOrga()+(double)apqsSyntenyIT.getPbQStopSyntenyInElement())/(double)apqsSyntenyIT.getQsQSizeOfOrganismInPb());
							apqsSyntenyIT.setsPercentStart(((double)apqsSyntenyIT.getQsSPbStartOfElementInOrga()+(double)apqsSyntenyIT.getPbSStartSyntenyInElement())/(double)apqsSyntenyIT.getQsSSizeOfOrganismInPb());
							apqsSyntenyIT.setsPercentStop(((double)apqsSyntenyIT.getQsSPbStartOfElementInOrga()+(double)apqsSyntenyIT.getPbSStopSyntenyInElement())/(double)apqsSyntenyIT.getQsSSizeOfOrganismInPb());
							
							if(apqsTransientSyntenyIT.isOrientationConserved()){
								apqsSyntenyIT.setQsEnumBlockType(QSEnumBlockType.QS_HOMOLOGS_BLOCK);
							}else{
								apqsSyntenyIT.setQsEnumBlockType(QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK);
							}
							
							alAPSyntenyOnly.add(apqsSyntenyIT);
						}
						
						CommonMethodsViewResults cmvrIT = new CommonMethodsViewResults();
						//Comparator<AbsoPropQSSyntItem> compaByPbSStartSyntenyInOrgaAndSLenghtComparatorIT = new ByPbSStartSyntenyInOrgaAndSLenghtComparator();
						Collections.sort(alAPSyntenyOnly, AbsoPropQSSyntItemComparators.byPbSStartSyntenyInOrgaAndSLenghtComparator);// compaByPbSStartSyntenyInOrgaAndSLenghtComparatorIT);
						cmvrIT.getSGenomicRegionInsertionWithArrayListSynteny(alAPSyntenyOnly, Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS());
						//cmvrIT.getSGenomicRegionInsertionWithArrayListSynteny(alAPSyntenyOnly, hirs.getAlAPSEI());
						//Comparator<AbsoPropQSSyntItem> compaAPQSSynteyByPbQStartSyntenyInOrgaAndQLenghtIT = new ByPbQStartSyntenyInOrgaAndQLenghtComparator();
						Collections.sort(alAPSyntenyOnly, AbsoPropQSSyntItemComparators.byPbQStartSyntenyInOrgaAndQLenghtComparator);//; compaAPQSSynteyByPbQStartSyntenyInOrgaAndQLenghtIT);
						
						ArrayList<Integer> missingElements = cmvrIT.crunchQSSyntenyIntoOneSlotAndListOfTmpOtherAndGetQGenomicRegionInsertionWithArrayListSynteny(alAPSyntenyOnly, Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ());
						//Window.alert("done getSGenomicRegionInsertionWithArrayListSynteny");
						ArrayList<SuperHoldAbsoPropItem> alHAPIIT = new ArrayList<SuperHoldAbsoPropItem>();
						cmvrIT.buildCorrectlyListSuperHolderFromSyntenyList(alAPSyntenyOnly, alHAPIIT, missingElements);
						
						
						//set list appropriately
						Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
								.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
								.setFullAssociatedlistAbsoluteProportionItemToDisplay(alHAPIIT);
						/*Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
						.get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem()
						.setListAbsoluteProportionElementItemS(hirs.getAlAPSEI());*/
						
						//AppController.LIST_GENOME_PANEL
						//.get(indexGenomePanelInLIST_GENOME_PANEL).setWaitingForSyntenyViewToFinishLoading(false);
						//AppController.LIST_GENOME_PANEL.get(indexGenomePanelInLIST_GENOME_PANEL).checkEverythingIsLoadedBeforeDrawingClassicSyntenyView();
						if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0
								&& Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getLookUpForGeneWithQStart() < 0){
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
						}
						Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.clearWholeCanvas();
						Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawViewToCanvas(1.0, 0, true, true, false, false);
					}
					
				}
				
				AppController.backFromCallBackCheckingOfLoadingMask();
//				AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
//				if (AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
//					Scheduler.get().scheduleDeferred(new ScheduledCommand() {
//						// @Override
//						public void execute() {
//							Insyght.INSYGHT_LOADING_MASK.hide();
//						}
//					});
//				}

				
			}
		};

		try {

			homologieBrowsingResultService
					.getItemAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertionWithQArrayListOrigamiElementIdsAndSOrigamiOrgaId(
							//Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ(),
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId()
							, sOrigamiOrgaIdSent
							//Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getAlignmentParametersForSynteny(),
							, Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).getGenomePanelItem().getListAbsoluteProportionElementItemS().isEmpty()
							, callback);
			Insyght.INSYGHT_LOADING_MASK.show();
			Insyght.INSYGHT_LOADING_MASK.center();
			AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE++;

		} catch (Exception e) {
			//String mssgError = "ERROR try in loadItemsAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertion: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//System.err.println(e.getLocalizedMessage());
			//e.printStackTrace();
			Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(indexGenomePanelInLIST_GENOME_PANEL).syntenyCanvas.drawMssgOnCanvas("Error", true);
			//AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
			Insyght.INSYGHT_LOADING_MASK.hide();
		}
	}

	

}
