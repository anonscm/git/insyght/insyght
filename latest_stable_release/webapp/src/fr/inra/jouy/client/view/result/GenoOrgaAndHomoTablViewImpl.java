package fr.inra.jouy.client.view.result;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.crossSections.popUp.ExportCDSSeqDiag;
import fr.inra.jouy.client.view.crossSections.popUp.ManageListComparedOrganisms;
import fr.inra.jouy.client.view.crossSections.popUp.PopUpListGenesDialog;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.SharedAppParams;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem.GpiSizeState;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneMatchItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSSyntItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem.QSEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem.QEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem.SEnumBlockType;
import fr.inra.jouy.shared.pojos.users.GroupUsersObj;


//public class ResultViewImpl extends Composite {
public class GenoOrgaAndHomoTablViewImpl extends ResizeComposite {

	private static ResultViewImplUiBinder uiBinder = GWT
			.create(ResultViewImplUiBinder.class);

	interface ResultViewImplUiBinder extends
			UiBinder<Widget, GenoOrgaAndHomoTablViewImpl> {
	}

	@UiField
	public static StackLayoutPanel stackPWest;

	// detailled info
	@UiField
	static ScrollPanel scrollPDetailedInfo;
	@UiField
	static HTML clickForInfo;
	@UiField
	static DisclosurePanel discPSymbolInfo;
	@UiField
	static HTML symbolInfo;
	@UiField
	static Anchor acSeeSymbolList;
	@UiField
	static DisclosurePanel discPGenomeInfo;
	@UiField
	static HTML genomeInfo;
	@UiField
	static DisclosurePanel discPElementInfo;
	@UiField
	static HTML basicQElementInfo;
	@UiField
	static HTML detailledQElementInfo;
	@UiField
	static HTML basicSElementInfo;
	@UiField
	static HTML detailledSElementInfo;
	@UiField
	static DisclosurePanel discPSyntenyInfo;
	@UiField
	static HTML basicGenomicRegionInfo;
	@UiField
	static HTML detailledSyntenyInfo;
	@UiField
	static DisclosurePanel discPGeneInfo;
	@UiField
	static DisclosurePanel discPGeneInfoRef;
	@UiField
	static DisclosurePanel discPGeneInfoComp;
	@UiField
	static HTML basicGeneInfoRef;
	@UiField
	static HTML detailledGeneInfoRef;
	@UiField
	static HTML basicGeneInfoComp;
	@UiField
	static HTML detailledGeneInfoComp;
	@UiField
	static DisclosurePanel discPGeneMatchInfo;
	@UiField
	static HTML geneMatchInfo;
	@UiField
	static Anchor acVisuAlignment;
	// Compared organisms
	@UiField
	static HTMLPanel PANEL_QUICKNAV_PRIVATE;
	@UiField
	static VerticalPanel VP_QUICKNAV_PRIVATE;
	@UiField
	static HTMLPanel PANEL_QUICKNAV_FEATURED;
	@UiField
	static VerticalPanel VP_QUICKNAV_FEATURED;
	@UiField
	static VerticalPanel VP_QUICKNAV_PUBLIC;
	@UiField
	public static ScrollPanel SP_VP_QUICKNAV;
	@UiField
	public static Button BtManageListComparedOrganisms;
	

	// View types
	@UiField
	static Button BT_DISPLAY_Less_columns;
	@UiField
	static Button BT_DISPLAY_More_columns;
	@UiField
	static Button BT_DISPLAY_Less_rows;
	@UiField
	static Button BT_DISPLAY_More_rows;
	@UiField
	static CheckBox SYNCHRO_NAVIGATION;
	@UiField
	static CheckBox SYNCHRO_ZOOMING;
	
	// var for VisuAlignment
	private static int qGeneIdForVisuAlignment = -1;
	private static int sGeneIdForVisuAlignment = -1;

	public static boolean FIRST_RESULT_LOADED = false;
	public static boolean initScaleHomologTable = false;


	public static CompareHomologsAnnot COMPARE_HOMOLOGS_ANNOTATION_DIAG = new CompareHomologsAnnot();
	public static PopUpListGenesDialog LIST_GENES_DIAG = new PopUpListGenesDialog();
	public static ExportCDSSeqDiag EXPORT_CDS_SEQUENCE_DIAG = new ExportCDSSeqDiag();
	public static ResultLoadingDialog RLD = new ResultLoadingDialog();

	

	public static int levelDetailShowInfo = 0;// 0 : no info // 1 : genomeInfo // 2 : syntenyInfo // 3 : GeneInfo gene homology
	// 4 : geneMatchInfo // 5 : elementInfo // 6 : geneInfo s without homologs // 7 : geneInfo q without homologs
	public static boolean forceDiscPSyntenyInfoVisible = false;
			
	public GenoOrgaAndHomoTablViewImpl() {

		initWidget(uiBinder.createAndBindUi(this));

		// detailled info
		discPSymbolInfo.setAnimationEnabled(true);
		discPSymbolInfo.setOpen(true);
		discPGenomeInfo.setAnimationEnabled(true);
		discPElementInfo.setAnimationEnabled(true);
		discPSyntenyInfo.setAnimationEnabled(true);
		discPSyntenyInfo.addOpenHandler(new OpenHandler<DisclosurePanel>() {
			@Override
			public void onOpen(OpenEvent<DisclosurePanel> event) {
				if(levelDetailShowInfo != 2){
					forceDiscPSyntenyInfoVisible = true;
				};
			}
		});
		discPSyntenyInfo.addCloseHandler(new CloseHandler<DisclosurePanel>() {
			@Override
			public void onClose(CloseEvent<DisclosurePanel> event) {
				if(levelDetailShowInfo != 2){
					forceDiscPSyntenyInfoVisible = false;
				};
			}
		});
		discPGeneInfo.setAnimationEnabled(true);
		discPGeneMatchInfo.setAnimationEnabled(true);
		showUserSelectionDetailledInfo();
		

		acSeeSymbolList.addStyleDependentName("cliquable");
		acVisuAlignment.addStyleDependentName("cliquable");
		
		PANEL_QUICKNAV_PRIVATE.setVisible(false);
		PANEL_QUICKNAV_FEATURED.setVisible(false);


	}
	
	
	@UiHandler("BtManageListComparedOrganisms")
	void onManageListComparedOrganismsClick(ClickEvent e) {
		MainTabPanelView.MLCO.displayPoPup();
	}

	@UiHandler("acSeeSymbolList")
	void onAcSeeSymbolListClick(ClickEvent e) {
		Window.open("http://genome.jouy.inra.fr/Insyght_doc_online/", "insyght-online-documentation", null);
	}
	
	@UiHandler("acVisuAlignment")
	void onAcVisuAlignmentClick(ClickEvent e) {
		ResultLoadingDialog rld = new ResultLoadingDialog();
		rld.linkToVisuAlignment(qGeneIdForVisuAlignment, sGeneIdForVisuAlignment);
	}
	
	@UiHandler("BT_DISPLAY_More_columns")
	void onBT_DISPLAY_More_columnsClick(ClickEvent e) {
		SyntenyCanvas.DISPLAY_SIZE_MODIFIER_COLUMN--;
		SyntenyCanvas.CALCULATE_CANVAS_PARAMETERS = true;
		CenterSLPAllGenomePanels.updateResultsDisplay(false, true, true);
	}

	@UiHandler("BT_DISPLAY_Less_columns")
	void onBT_DISPLAY_Less_columnsClick(ClickEvent e) {
		SyntenyCanvas.DISPLAY_SIZE_MODIFIER_COLUMN++;
		SyntenyCanvas.CALCULATE_CANVAS_PARAMETERS = true;
		CenterSLPAllGenomePanels.updateResultsDisplay(false, true, true);
	}

	@UiHandler("BT_DISPLAY_Less_rows")
	void onBT_DISPLAY_Less_rowsClick(ClickEvent e) {
		GenomePanel.MAX_PX_scLowerSize = GenomePanel.MAX_PX_scLowerSize + 10;
		if (GenomePanel.MAX_PX_scLowerSize > 400) {
			GenomePanel.MAX_PX_scLowerSize = 400;
		}
		GenomePanel.MEDIUM_PX_scLowerSize = GenomePanel.MAX_PX_scLowerSize / 2;
		for (GenomePanel gpiIT : Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()) {
			gpiIT.getSyntenyCanvas().canvasBgk
					.setHeight(GenomePanel.MAX_PX_scLowerSize + "px");
			gpiIT.getSyntenyCanvas().setCoordinateSpace = true;
		}
		SyntenyCanvas.CALCULATE_CANVAS_PARAMETERS = true;
		CenterSLPAllGenomePanels.updateResultsDisplay(false, true, true);
	}

	@UiHandler("BT_DISPLAY_More_rows")
	void onBT_DISPLAY_More_rowsClick(ClickEvent e) {
		GenomePanel.MAX_PX_scLowerSize = GenomePanel.MAX_PX_scLowerSize - 10;
		if (GenomePanel.MAX_PX_scLowerSize < 40) {
			GenomePanel.MAX_PX_scLowerSize = 40;
		}
		GenomePanel.MEDIUM_PX_scLowerSize = GenomePanel.MAX_PX_scLowerSize / 2;
		for (GenomePanel gpiIT : Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()) {
			gpiIT.getSyntenyCanvas().canvasBgk
					.setHeight(GenomePanel.MAX_PX_scLowerSize + "px");
			gpiIT.getSyntenyCanvas().setCoordinateSpace = true;
		}
		SyntenyCanvas.CALCULATE_CANVAS_PARAMETERS = true;
		CenterSLPAllGenomePanels.updateResultsDisplay(false, true, true);
	}

	@UiHandler("SYNCHRO_NAVIGATION")
	void onSYNCHRO_NAVIGATIONClick(ClickEvent e) {
		if (SYNCHRO_NAVIGATION.getValue()) {
			SyntenyCanvas.SYNCHRONIZE_NAVIGATION = true;
		} else {
			SyntenyCanvas.SYNCHRONIZE_NAVIGATION = false;
		}
	}

	@UiHandler("SYNCHRO_ZOOMING")
	void onSYNCHRO_ZOOMINGClick(ClickEvent e) {
		if (SYNCHRO_ZOOMING.getValue()) {
			SyntenyCanvas.SYNCHRONIZE_Q_ZOOMING = true;
		} else {
			SyntenyCanvas.SYNCHRONIZE_Q_ZOOMING = false;
		}
	}

	public static void changeToViewHomologTable(boolean updateDisplay) {

		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
				.setViewTypeInsyght(EnumResultViewTypes.homolog_table);

		SYNCHRO_NAVIGATION.setEnabled(false);
		SYNCHRO_ZOOMING.setEnabled(false);

		// set correct default size state and q start/stop show for all gpi
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getCurrentListFeaturedResultGenomePanelItem().size(); i++) {
			GenomePanelItem gpiIT = Insyght.APP_CONTROLER
					.getCurrentListFeaturedResultGenomePanelItem().get(i);
			gpiIT.setSizeState(GpiSizeState.INTERMEDIATE);
			if (initScaleHomologTable) {
				gpiIT.setPercentSyntenyShowStartQGenome(0.0, false, true);
				gpiIT.setPercentSyntenyShowStopQGenome(1.0, false, true);
				gpiIT.setPercentSyntenyShowStartSGenome(0.0, false, true);
				gpiIT.setPercentSyntenyShowStopSGenome(1.0, false, true);
				gpiIT.getFullAssociatedlistAbsoluteProportionItemToDisplay()
						.clear();
				if (gpiIT.getPartialListAbsoluteProportionItemToDisplay() != null) {
					gpiIT.getPartialListAbsoluteProportionItemToDisplay()
							.clear();
				}
				gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,EnumResultViewTypes.homolog_table);
			} else {
				// so that to force calcule of partial from scratch
				gpiIT.setPartialListAbsoluteProportionItemToDisplay(null);
			}
		}
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getCurrentListPrivateResultGenomePanelItem().size(); i++) {
			GenomePanelItem gpiIT = Insyght.APP_CONTROLER
					.getCurrentListPrivateResultGenomePanelItem().get(i);
			gpiIT.setSizeState(GpiSizeState.INTERMEDIATE);
			if (initScaleHomologTable) {
				gpiIT.setPercentSyntenyShowStartQGenome(0.0, false, true);
				gpiIT.setPercentSyntenyShowStopQGenome(1.0, false, true);
				gpiIT.setPercentSyntenyShowStartSGenome(0.0, false, true);
				gpiIT.setPercentSyntenyShowStopSGenome(1.0, false, true);
				gpiIT.getFullAssociatedlistAbsoluteProportionItemToDisplay()
						.clear();
				if (gpiIT.getPartialListAbsoluteProportionItemToDisplay() != null) {
					gpiIT.getPartialListAbsoluteProportionItemToDisplay()
							.clear();
				}
				gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,EnumResultViewTypes.homolog_table);
			} else {
				// so that to force calcule of partial from scratch
				gpiIT.setPartialListAbsoluteProportionItemToDisplay(null);
			}
		}
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getCurrentListPublicResultGenomePanelItem().size(); i++) {
			GenomePanelItem gpiIT = Insyght.APP_CONTROLER
					.getCurrentListPublicResultGenomePanelItem().get(i);
			gpiIT.setSizeState(GpiSizeState.INTERMEDIATE);
			if (initScaleHomologTable) {
				gpiIT.setPercentSyntenyShowStartQGenome(0.0, false, true);
				gpiIT.setPercentSyntenyShowStopQGenome(1.0, false, true);
				gpiIT.setPercentSyntenyShowStartSGenome(0.0, false, true);
				gpiIT.setPercentSyntenyShowStopSGenome(1.0, false, true);
				gpiIT.getFullAssociatedlistAbsoluteProportionItemToDisplay()
						.clear();
				if (gpiIT.getPartialListAbsoluteProportionItemToDisplay() != null) {
					gpiIT.getPartialListAbsoluteProportionItemToDisplay()
							.clear();
				}
				gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,EnumResultViewTypes.homolog_table);
			} else {
				// so that to force calcule of partial from scratch
				gpiIT.setPartialListAbsoluteProportionItemToDisplay(null);
			}
		}
		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
				.getReferenceGenomePanelItem().setSizeState(GpiSizeState.MIN);
		if (initScaleHomologTable) {
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem()
					.setPercentSyntenyShowStartQGenome(0.0, false, true);
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem()
					.setPercentSyntenyShowStopQGenome(1.0, false, true);
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem()
					.setPercentSyntenyShowStartSGenome(0.0, false, true);
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem()
					.setPercentSyntenyShowStopSGenome(1.0, false, true);
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem()
					.getFullAssociatedlistAbsoluteProportionItemToDisplay()
					.clear();
			if (Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem()
					.getPartialListAbsoluteProportionItemToDisplay() != null) {
				Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getReferenceGenomePanelItem()
						.getPartialListAbsoluteProportionItemToDisplay()
						.clear();
			}
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem()
					.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,EnumResultViewTypes.homolog_table);
		} else {
			// so that to force calcule of partial from scratch
			Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem()
					.setPartialListAbsoluteProportionItemToDisplay(null);
		}

		for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
				.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
			GenomePanel gpIT = i.next();
			if (gpIT.getGenomePanelItem() != null) {
				// reset variable gpi
				// gpIT.getGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0);
				if (gpIT.isReferenceGenome) {
					gpIT.setSizeToMin(false);
				} else {
					gpIT.setSizeToIntermediate(false);
				}
			}
		}
		initScaleHomologTable = false;
		if(updateDisplay){
			CenterSLPAllGenomePanels.updateResultsDisplay(true, false, true);
		}
		
	}

	public static void changeToViewGenomicOrganisation(boolean updateDisplay) {

		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
				.setViewTypeInsyght(EnumResultViewTypes.genomic_organization);

		SYNCHRO_NAVIGATION.setEnabled(true);
		SYNCHRO_ZOOMING.setEnabled(true);

		// TEST
		// set correct default size state for all gpi
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getCurrentListFeaturedResultGenomePanelItem().size(); i++) {
			GenomePanelItem gpiIT = Insyght.APP_CONTROLER
					.getCurrentListFeaturedResultGenomePanelItem().get(i);
			gpiIT.setSizeState(GpiSizeState.MAX);
			// so that to force calcule of partial from scratch
			gpiIT.setPartialListAbsoluteProportionItemToDisplay(null);
		}
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getCurrentListPrivateResultGenomePanelItem().size(); i++) {
			GenomePanelItem gpiIT = Insyght.APP_CONTROLER
					.getCurrentListPrivateResultGenomePanelItem().get(i);
			gpiIT.setSizeState(GpiSizeState.MAX);
			// so that to force calcule of partial from scratch
			gpiIT.setPartialListAbsoluteProportionItemToDisplay(null);
		}
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getCurrentListPublicResultGenomePanelItem().size(); i++) {
			GenomePanelItem gpiIT = Insyght.APP_CONTROLER
					.getCurrentListPublicResultGenomePanelItem().get(i);
			gpiIT.setSizeState(GpiSizeState.MAX);
			// so that to force calcule of partial from scratch
			gpiIT.setPartialListAbsoluteProportionItemToDisplay(null);
		}
		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
				.getReferenceGenomePanelItem().setSizeState(GpiSizeState.MIN);
		// so that to force calcule of partial from scratch
		Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
				.getReferenceGenomePanelItem()
				.setPartialListAbsoluteProportionItemToDisplay(null);

		for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
				.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
			GenomePanel gpIT = i.next();
			// reset variable gpi
			if (gpIT.getGenomePanelItem() != null) {
				// System.err.println(gpIT.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed());
				// gpIT.getGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0);
				if (gpIT.isReferenceGenome) {
					gpIT.setSizeToMin(false);
				} else {
					gpIT.setSizeToMax(false);
				}

			}
		}
		
		if(updateDisplay){
			CenterSLPAllGenomePanels.updateResultsDisplay(true, false, true);
		}
		

	}

	public static void clearAllInfoInStack() {
		clickForInfo.setHTML("[Click on a symbol to display detailed information.]<br/><br/>[Double click on a symbol to access the contextual menu.]");
		clearSymbolInfoInStack();
		clearGenomeInfoInStack();
		clearElementInfoInStack();
		clearGeneInfoInStack();
		clearGeneMatchInfoInStack();
		clearSyntenyInfoInStack();
	}

	private static void clearSymbolInfoInStack() {
		symbolInfo.setHTML("");
		discPSymbolInfo.setVisible(false);
	}
	
	private static void clearGenomeInfoInStack() {
		genomeInfo.setHTML("");
		discPGenomeInfo.setVisible(false);
	}

	private static void clearElementInfoInStack() {
		basicQElementInfo.setHTML("");
		detailledQElementInfo.setHTML("");
		basicSElementInfo.setHTML("");
		detailledSElementInfo.setHTML("");
		discPElementInfo.setVisible(false);
	}

	private static void clearSyntenyInfoInStack() {
		basicGenomicRegionInfo.setHTML("");
		detailledSyntenyInfo.setHTML("");
		discPSyntenyInfo.setVisible(false);
		// SYNTENY_INFO_AVAILABLE_IN_STACK = false;
	}

	private static void clearGeneInfoInStack() {
		basicGeneInfoRef.setHTML("");
		detailledGeneInfoRef.setHTML("");
		basicGeneInfoComp.setHTML("");
		detailledGeneInfoComp.setHTML("");
		discPGeneInfo.setVisible(false);
	}

	private static void clearGeneMatchInfoInStack() {
		geneMatchInfo.setHTML("");
		acVisuAlignment.setVisible(false);
		discPGeneMatchInfo.setVisible(false);
	}

	
	public static void fillBasicGenomeInfoInStack(
			LightOrganismItem qOrganismItemSent, int referenceCumulativeSize,
			LightOrganismItem sOrganismItemSent, int resultCumulativeSize,
			boolean showStack) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		
		String textGenomeInfo = "";

		if (qOrganismItemSent != null) {
			textGenomeInfo += "<big>---- Query</big><br/><big><i>Species : </i></big>"
					+ qOrganismItemSent.getSpecies()
					// + " [id = "
					// + qOrganismItemSent.getOrganismId()
					// + "]"
					+ "<br/><big><i>Strain : </i></big>"
					+ qOrganismItemSent.getStrain()
					+ "<br/><big><i>Cumulative size : </i></big>"
					// + referenceCumulativeSize
					+ NumberFormat.getFormat("###,###,###,###,###,###").format(
							referenceCumulativeSize) + " bp" + "<br/>";

		}

		if (sOrganismItemSent != null) {
			textGenomeInfo += "<big>---- Subject</big><br/><big><i>Species : </i></big>"
					+ sOrganismItemSent.getSpecies()
					// + " [id = "
					// + sOrganismItemSent.getOrganismId()
					// + "]"
					+ "<br/><big><i>Strain : </i></big>"
					+ sOrganismItemSent.getStrain()
					+ "<br/><big><i>Cumulative size : </i></big>"
					+ NumberFormat.getFormat("###,###,###,###,###,###").format(
							resultCumulativeSize) + " bp" + "<br/>";
		}

		genomeInfo.setHTML(textGenomeInfo);
		// GENOME_INFO_AVAILABLE_IN_STACK = true;
		discPGenomeInfo.setVisible(true);

		if (showStack) {
			levelDetailShowInfo = 1;
			showUserSelectionDetailledInfo();
		}
	}

	public static void fillElementInfoInStack(
			AbsoPropSElemItem apseiIT, boolean showStack,
			boolean getDetailledInfo) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		// basic
		String textBasicSElementInfo = "<big>---- Subject</big><br/><big><i>Accnum : </i></big>"
				+ apseiIT.getsAccnum()
				+ "<br/><big><i>Type : </i></big>"
				+ apseiIT.getsElementType()
				+ "<br/><big><i>Size in pb : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apseiIT.getsSizeOfElementinPb()) + "<br/>";

		basicSElementInfo.setHTML(textBasicSElementInfo);

		// detailled
		// qElementsItem.getQsQElementNumberGene();
		if (getDetailledInfo) {
//			@SuppressWarnings("unused")
//			ResultLoadingDialog rld = new ResultLoadingDialog(
//					DetailledInfoStack.DetailledSElement,
//					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
//					apseiIT.getsOrigamiElementId(),
//					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()
//					);
			RLD.getDetailledElementInfo(
					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
					apseiIT.getsOrigamiElementId(),
					false,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()
					);

		} else {
			detailledSElementInfo.setHTML("");
		}

		discPElementInfo.setVisible(true);
		if (showStack) {
			levelDetailShowInfo = 5;
			showUserSelectionDetailledInfo();
		}

	}

	public static void fillElementInfoInStack(
			AbsoPropQElemItem apqeiIT, boolean showStack,
			boolean getDetailledInfo) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		
		// basic
		// qElementsItem.getQsQAccnum();
		// qElementsItem.getQsQElementType();
		// qElementsItem.getQsQSizeOfElementinPb();
		String textBasicQElementInfo = "<big>---- Query</big><br/><big><i>Accnum : </i></big>"
				+ apqeiIT.getqAccnum()
				+ "<br/><big><i>Type : </i></big>"
				+ apqeiIT.getqElementType()
				+ "<br/><big><i>Size in pb : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apqeiIT.getQsizeOfElementinPb()) + "<br/>";

		basicQElementInfo.setHTML(textBasicQElementInfo);

		// detailled
		// qElementsItem.getQsQElementNumberGene();
		if (getDetailledInfo) {
//			@SuppressWarnings("unused")
//			ResultLoadingDialog rld = new ResultLoadingDialog(
//					DetailledInfoStack.DetailledQElement,
//					Insyght.APP_CONTROLER
//							.getCurrSelectedSyntenyCanvasItemRefId(),
//					apqeiIT.getqOrigamiElementId(), Insyght.APP_CONTROLER
//							.getCurrentRefGenomePanelAndListOrgaResult()
//							.getViewTypeInsyght());
			RLD.getDetailledElementInfo(
					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
					apqeiIT.getqOrigamiElementId(),
					true,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()
					);

		} else {
			detailledQElementInfo.setHTML("");
		}

		discPElementInfo.setVisible(true);
		if (showStack) {
			levelDetailShowInfo = 5;
			showUserSelectionDetailledInfo();
		}

	}

	public static void fillElementInfoInStack(
			AbsoPropQElemItem qElementsItem,
			AbsoPropSElemItem sElementsItem, boolean showStack,
			boolean getDetailledInfo) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		if (qElementsItem != null) {
			// basic
			String textBasicQElementInfo = "<big>---- Query</big><br/><big><i>Accnum : </i></big>"
					+ qElementsItem.getqAccnum()
					+ "<br/><big><i>Type : </i></big>"
					+ qElementsItem.getqElementType()
					+ "<br/><big><i>Size in pb : </i></big>"
					+ NumberFormat.getFormat("###,###,###,###,###,###").format(
							qElementsItem.getQsizeOfElementinPb()) + "<br/>";

			basicQElementInfo.setHTML(textBasicQElementInfo);

			// detailled
			// qElementsItem.getQsQElementNumberGene();
			if (getDetailledInfo) {
//				@SuppressWarnings("unused")
//				ResultLoadingDialog rld = new ResultLoadingDialog(
//						DetailledInfoStack.DetailledQElement,
//						Insyght.APP_CONTROLER
//								.getCurrSelectedSyntenyCanvasItemRefId(),
//						qElementsItem.getqOrigamiElementId(),
//						Insyght.APP_CONTROLER
//								.getCurrentRefGenomePanelAndListOrgaResult()
//								.getViewTypeInsyght());
				RLD.getDetailledElementInfo(
						Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
						qElementsItem.getqOrigamiElementId(),
						true,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()
						);
			} else {
				detailledQElementInfo.setHTML("");
			}

		} else {
			basicQElementInfo.setHTML("");
			detailledQElementInfo.setHTML("");
		}

		if (sElementsItem != null) {
			String textBasicSElementInfo = "<big>---- Subject</big><br/><big><i>Accnum : </i></big>"
					+ sElementsItem.getsAccnum()
					+ "<br/><big><i>Type : </i></big>"
					+ sElementsItem.getsElementType()
					+ "<br/><big><i>Size in pb : </i></big>"
					+ NumberFormat.getFormat("###,###,###,###,###,###").format(
							sElementsItem.getsSizeOfElementinPb()) + "<br/>";

			basicSElementInfo.setHTML(textBasicSElementInfo);

			// detailled
			// qElementsItem.getQsQElementNumberGene();
			if (getDetailledInfo) {
//				@SuppressWarnings("unused")
//				ResultLoadingDialog rld = new ResultLoadingDialog(
//						DetailledInfoStack.DetailledSElement,
//						Insyght.APP_CONTROLER
//								.getCurrSelectedSyntenyCanvasItemRefId(),
//						sElementsItem.getsOrigamiElementId(),
//						Insyght.APP_CONTROLER
//								.getCurrentRefGenomePanelAndListOrgaResult()
//								.getViewTypeInsyght());
				RLD.getDetailledElementInfo(
						Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
						sElementsItem.getsOrigamiElementId(),
						false,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()
						);
			} else {
				detailledSElementInfo.setHTML("");
			}

		} else {
			basicSElementInfo.setHTML("");
			detailledSElementInfo.setHTML("");
		}

		discPElementInfo.setVisible(true);
		if (showStack) {
			levelDetailShowInfo = 5;
			showUserSelectionDetailledInfo();
		}
	}

	public static void fillSGenomicRegionInfoInStack(
			AbsoPropSGenoRegiInserItem apsgriiIT,
			boolean showStack) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		// basic
		// String textBasicSyntenyInfo =
		// "<br/><big>Subject genomic region Insertion</big>";
		
		String textSymbolInfo = "";
		String textGenomicRegionInfo = "";
		
		boolean isSyntenyOriginally = false;
		
		if (apsgriiIT.getSEnumBlockType().compareTo(SEnumBlockType.S_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_CUT_START_S_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Opened (start)";
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_CUT_STOP_S_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Opened (stop)";
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (start)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the left, cut start) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (start)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the right, cut start) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (stop)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the left, cut stop) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (stop)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the right, cut stop) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the left) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType()
				.compareTo(
						SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (start)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the left, cut start) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType()
				.compareTo(
						SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (start)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the right, cut start) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT
				.getSEnumBlockType()
				.compareTo(
						SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (stop)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the left, cut stop) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT
				.getSEnumBlockType()
				.compareTo(
						SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (stop)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the right, cut stop) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny not reversed (partial left) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_LEFT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			//textBasicSyntenyInfo += "Subject genomic region insertion (partial left)";
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny reversed (partial left) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (partial right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			//textBasicSyntenyInfo += "Subject genomic region insertion (partial right)";
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (partial right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (partial left and right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and Downstream";
//			textBasicSyntenyInfo += "Subject genomic region insertion (partial left and right)";
		} else if (apsgriiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (partial left and right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		}
		if (apsgriiIT.isAnnotationCollision()) {
			textSymbolInfo = "<big><i>Symbol type : </i></big>Overlap region between two homologous loci";
			//textBasicSyntenyInfo += " (region of overlap between two different syntenies)";
		}
		//generalSymbolInfo
		textSymbolInfo += "<br/><big><i>Location : </i></big>Compared genome";
//		if(isSyntenyOriginally){
//			textSymbolInfo += "<br/><big><i>Background colored according to : </i></big>synteny";
//		}
		
		// genomic region info
		textGenomicRegionInfo += "<br/><big><i>Number of CDS : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apsgriiIT.getSGenomicRegionInsertionNumberGene());
		textGenomicRegionInfo += "<br/><big><i>Subject start (pb) : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###")
						.format(apsgriiIT
								.getsPbStartSGenomicRegionInsertionInElement());
		textGenomicRegionInfo += "<br/><big><i>Subject stop (pb) : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apsgriiIT.getsPbStopSGenomicRegionInsertionInElement());
		
		if(isSyntenyOriginally){
			discPSyntenyInfo.getHeaderTextAccessor().setText("Synteny info");
		}else{
			discPSyntenyInfo.getHeaderTextAccessor().setText("Genomic region Info");
		}
		
		symbolInfo.setHTML(textSymbolInfo);
		discPSymbolInfo.setVisible(true);
		
		basicGenomicRegionInfo.setHTML(textGenomicRegionInfo);
		detailledSyntenyInfo.setHTML("");
		discPSyntenyInfo.setVisible(true);

		if (showStack) {
			levelDetailShowInfo = 2;
			showUserSelectionDetailledInfo();
		}

	}

	public static void fillQGenomicRegionInfoInStack(
			AbsoPropQGenoRegiInserItem apqgriiIT,
			boolean showStack) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		

		String textSymbolInfo = "";
		String textGenomicRegionInfo = "";
		
		boolean isSyntenyOriginally = false;
		
		if (apqgriiIT.getqEnumBlockType().compareTo(QEnumBlockType.Q_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_CUT_START_Q_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Opened (start)";
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_CUT_STOP_Q_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Opened (stop)";
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (start)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the left, cut start) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (start)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the right, cut start) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (stop)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the left, cut stop) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (stop)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (missing target query genomic region to the right, cut stop) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the left) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType()
				.compareTo(
						QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (start)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the left, cut start) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType()
				.compareTo(
						QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (start)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the right, cut start) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType()
				.compareTo(
						QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_LEFT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (stop)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the left, cut stop) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType()
				.compareTo(
						QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_RIGHT) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing ; Opened (stop)";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (missing target query genomic region to the right, cut stop) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny not reversed (partial left) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_LEFT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			//textBasicSyntenyInfo += "Subject genomic region insertion (partial left)";
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
//			textBasicSyntenyInfo += "Synteny reversed (partial left) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (partial right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			//textBasicSyntenyInfo += "Subject genomic region insertion (partial right)";
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (partial right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and Downstream";
//			textBasicSyntenyInfo += "Synteny not reversed (partial left and right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Genomic region without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and Downstream";
//			textBasicSyntenyInfo += "Subject genomic region insertion (partial left and right)";
		} else if (apqgriiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			isSyntenyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textGenomicRegionInfo += "<big><i>Conserved synteny id : </i></big>"+NumberFormat.getFormat("###,###,###,###,###,###").format(apqgriiIT.getqIfMissingTargetOrigamiAlignementId());
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and Downstream";
//			textBasicSyntenyInfo += "Synteny reversed (partial left and right) : id="
//					+ NumberFormat.getFormat("###,###,###,###,###,###").format(apsgriiIT.getsIfMissingTargetOrigamiAlignmentId());
		}
		if (apqgriiIT.isAnnotationCollision()) {
			textSymbolInfo = "<big><i>Symbol type : </i></big>Overlap region between two homologous loci";
			//textBasicSyntenyInfo += " (region of overlap between two different syntenies)";
		}
		//generalSymbolInfo
		textSymbolInfo += "<br/><big><i>Location : </i></big>Reference genome";
//		if(isSyntenyOriginally){
//			textSymbolInfo += "<br/><big><i>Background colored according to : </i></big>synteny";
//		}
		
		textGenomicRegionInfo += "<br/><big><i>Number of CDS : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apqgriiIT.getQGenomicRegionInsertionNumberGene());
		textGenomicRegionInfo += "<br/><big><i>Query start (pb) : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###")
						.format(apqgriiIT
								.getqPbStartQGenomicRegionInsertionInElement());
		textGenomicRegionInfo += "<br/><big><i>Query stop (pb) : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apqgriiIT.getqPbStopQGenomicRegionInsertionInElement());

		if(isSyntenyOriginally){
			discPSyntenyInfo.getHeaderTextAccessor().setText("Synteny info");
		}else{
			discPSyntenyInfo.getHeaderTextAccessor().setText("Genomic region Info");
		}
		
		symbolInfo.setHTML(textSymbolInfo);
		discPSymbolInfo.setVisible(true);
		
		basicGenomicRegionInfo.setHTML(textGenomicRegionInfo);
		detailledSyntenyInfo.setHTML("");
		discPSyntenyInfo.setVisible(true);

		if (showStack) {
			levelDetailShowInfo = 2;
			showUserSelectionDetailledInfo();
		}

	}

	public static void fillSyntenyInfoInStack(
			AbsoPropQSSyntItem apqssiIT, boolean showStack,
			boolean getDetail) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		// basic
		String textBasicSyntenyInfo = "";
		String textSymbolInfo = "";
		
		if (apqssiIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			//textSymbolInfo += "<big><i>Symbol modification : </i></big>Opened (start)";
			//textBasicSyntenyInfo += "<big><i>Symbol modification : </i></big>Opened (start)";
			////textBasicSyntenyInfo += "<big><i>Synteny type : </i></big>not reversed";
		} else if (apqssiIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			//textBasicSyntenyInfo += "<big><i>Synteny type : </i></big>reversed";
		} else if (apqssiIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Opened (start)";
			textBasicSyntenyInfo += "<br/><big><i>Symbol modification : </i></big>Opened (start)";
			//textBasicSyntenyInfo += "<big><i>Synteny type : </i></big>not reversed (cut start)";
		} else if (apqssiIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Opened (start)";
			textBasicSyntenyInfo += "<br/><big><i>Symbol modification : </i></big>Opened (start)";
			
			//textBasicSyntenyInfo += "<big><i>Synteny type : </i></big>reversed (cut start)";
		} else if (apqssiIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Opened (stop)";
			textBasicSyntenyInfo += "<br/><big><i>Symbol modification : </i></big>Opened (stop)";
			
			//textBasicSyntenyInfo += "<big><i>Synteny type : </i></big>not reversed (cut stop)";
		} else if (apqssiIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Opened (stop)";
			textBasicSyntenyInfo += "<br/><big><i>Symbol modification : </i></big>Opened (stop)";
			
			//textBasicSyntenyInfo += "<big><i>Synteny type : </i></big>reversed (cut stop)";
		}
		textSymbolInfo += "<br/><big><i>Location : </i></big>Reference and compared genomes";
		//textSymbolInfo += "<br/><big><i>Background colored according to : </i></big>synteny";
		textSymbolInfo += "<br/><big><i>Synteny id : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apqssiIT.getSyntenyOrigamiAlignmentId());
		textBasicSyntenyInfo += "<br/><big><i>Synteny id : </i></big>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apqssiIT.getSyntenyOrigamiAlignmentId());
		
		discPSyntenyInfo.getHeaderTextAccessor().setText("Synteny info");
		
		symbolInfo.setHTML(textSymbolInfo);
		discPSymbolInfo.setVisible(true);
		
		basicGenomicRegionInfo.setHTML(textBasicSyntenyInfo);

		// detailled
		// qElementsItem.getQsQElementNumberGene();
		if (getDetail) {
//			@SuppressWarnings("unused")
//			ResultLoadingDialog rld = new ResultLoadingDialog(
//					DetailledInfoStack.DetailledSyntenyInfo,
//					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
//					apqssiIT.getSyntenyOrigamiAlignmentId(),
//					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()
//					);
			RLD.getDetailledSyntenyInfo(
					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
					apqssiIT.getSyntenyOrigamiAlignmentId(),
					showStack,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()
					);
		} else {
			detailledSyntenyInfo.setHTML("");
		}

		discPSyntenyInfo.setVisible(true);

		if (showStack) {
			levelDetailShowInfo = 2;
			showUserSelectionDetailledInfo();
		}
	}

	public static void fillGeneInfoInStack(
			AbsoPropSGeneInserItem apsgiiIT, boolean showStack,
			boolean getDetail) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		// basic
		String textBasicSGeneInfo = "<big>---- Subject<br/>";
		String textSymbolInfo = "";

		textBasicSGeneInfo += "<i>Name : </i>";
		textBasicSGeneInfo += apsgiiIT.getsNameAsHTMLPlusLink()+"<br/>";

		textBasicSGeneInfo += "<li><i>Locus tag : </i>";
		textBasicSGeneInfo += apsgiiIT.getsLocusTagAsHTMLPlusLink()+"<br/>";

		textBasicSGeneInfo += "<li><i>Start (pb) : </i>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apsgiiIT.getsPbStartGeneInElement())+"<br/>"
				+ "<li><i>Stop (pb) : </i>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apsgiiIT.getsPbStopGeneInElement())+"<br/>"
				+ "<li><i>Strand : </i>";
		if (apsgiiIT.getsStrand() > 0) {
			textBasicSGeneInfo += "+"+"<br/>";
		} else {
			textBasicSGeneInfo += "-"+"<br/>";
		}

		String locusAndLinkIfMissingTargetQMostSignificantGeneName = apsgiiIT
				.getIfMissingTargetQMostSignificantGeneNameAsHTMLPlusLink();

		//boolean isGeneHomologyOriginally = false;
		// textBasicQGeneInfo += "<br/><big><i>Type : </i></big>";
		if (apsgiiIT.getSEnumBlockType().compareTo(SEnumBlockType.S_INSERTION) == 0) {
			textSymbolInfo += "<li><i>Symbol type : </i>Gene without homolog";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_CUT_START_S_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (cut start)";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_CUT_STOP_S_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (cut stop)";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (missing target query gene "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//					+ " to the left, cut stop)";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (missing target query gene "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//					+ " to the right, cut stop)";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//					+ " to the left)";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//					+ " to the right)";
		} else if (apsgiiIT.getSEnumBlockType()
				.compareTo(
						SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//					+ " to the left, cut start)";
		} else if (apsgiiIT.getSEnumBlockType()
				.compareTo(
						SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//					+ " to the right, cut start)";
		} else if (apsgiiIT.getSEnumBlockType()
				.compareTo(
						SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//					+ " to the left, cut stop)";
		} else if (apsgiiIT.getSEnumBlockType()
				.compareTo(
						SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//					+ " to the right, cut stop)";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (partial left, target query gene is "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_LEFT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene without homolog altered by zoom, missing downstream gene display"+"<br/>";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (partial left)";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (partial left, target query gene is "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (partial right, target query gene is "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene without homolog altered by zoom, missing upstream gene display"+"<br/>";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (partial right)";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (partial right, target query gene is "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream and upstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream and upstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (partial left and right, target query gene is "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene without homolog altered by zoom, missing upstream and downstream gene display"+"<br/>";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (partial left and right)";
		} else if (apsgiiIT.getSEnumBlockType().compareTo(
				SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and downstream";
			textBasicSGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream and downstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetQMostSignificantGeneName+"<br/>";
//			textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (partial left and right, target query gene is "
//					+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		}
		//textBasicSGeneInfo += "</ul>";
		textSymbolInfo += "<br/><big><i>Location : </i></big>Compared genome";
//		if(isGeneHomologyOriginally){
//			textSymbolInfo += "<br/><big><i>Background colored according to : </i></big>synteny";
//		}
		
		symbolInfo.setHTML(textSymbolInfo);
		discPSymbolInfo.setVisible(true);
		
		basicGeneInfoComp.setHTML(textBasicSGeneInfo);

		// detailled
		// qElementsItem.getQsQElementNumberGene();
		if (getDetail) {
//			@SuppressWarnings("unused")
//			ResultLoadingDialog rld = new ResultLoadingDialog(
//					DetailledInfoStack.DetailledGeneInfoComp,
//					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
//					apsgiiIT.getsGeneId(), 
//					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			RLD.getDetailledGeneInfo(
					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
					apsgiiIT.getsGeneId(),
					showStack,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght(),
					false);
		} else {
			detailledGeneInfoComp.setHTML("");
		}

		discPGeneInfo.setVisible(true);

		if (showStack) {
			levelDetailShowInfo = 6;
			showUserSelectionDetailledInfo();
		}

	}

	public static void fillGeneInfoInStack(
			AbsoPropQGeneInserItem apgqiiIT, boolean showStack,
			boolean getDetail) {
		
		//TODO color gene annot if homology in red and green as annot compa
		
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		// basic
		String textBasicQGeneInfo = "<big>---- Query<br/><i>Name : </i>";
		textBasicQGeneInfo += apgqiiIT.getQNameAsHTMLPlusLink()+"<br/>";
		textBasicQGeneInfo += "<i>Locus tag : </i>";
		textBasicQGeneInfo += apgqiiIT.getQLocusTagAsHTMLPlusLink()+"<br/>";
		textBasicQGeneInfo += "<i>Start (pb) : </i>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apgqiiIT.getqPbStartGeneInElement())+"<br/>"
				+ "<i>Stop (pb) : </i>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apgqiiIT.getqPbStopGeneInElement())+"<br/>"
				+ "<i>Strand : </i>";
		if (apgqiiIT.getQStrand() > 0) {
			textBasicQGeneInfo += "+"+"<br/>";
		} else {
			textBasicQGeneInfo += "-"+"<br/>";
		}
		
		String textSymbolInfo = "";
		
		String locusAndLinkIfMissingTargetSMostSignificantGeneName = apgqiiIT
				.getIfMissingTargetSMostSignificantGeneNameAsHTMLPlusLink();
		//boolean isGeneHomologyOriginally = false;
		
		if (apgqiiIT.getqEnumBlockType().compareTo(QEnumBlockType.Q_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_CUT_START_Q_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (cut start)";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_CUT_STOP_Q_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (cut stop)";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (missing target query gene "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//							+ " to the left, cut stop)";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (missing target query gene "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//							+ " to the right, cut stop)";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//							+ " to the left)";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Compared genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//							+ " to the right)";
		} else if (apgqiiIT.getqEnumBlockType()
				.compareTo(
						QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//							+ " to the left, cut start)";
		} else if (apgqiiIT.getqEnumBlockType()
				.compareTo(
						QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//							+ " to the right, cut start)";
		} else if (apgqiiIT.getqEnumBlockType()
				.compareTo(
						QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_LEFT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located upstream of the current display."+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//							+ " to the left, cut stop)";
		} else if (apgqiiIT.getqEnumBlockType()
				.compareTo(
						QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_RIGHT) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName
					+ " is located downstream of the current display."+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (missing target query gene "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName
//							+ " to the right, cut stop)";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (partial left, target query gene is "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_LEFT_INSERTION) == 0) {
			
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene without homolog altered by zoom, missing downstream gene display"+"<br/>";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (partial left)";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (partial left, target query gene is "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (partial right, target query gene is "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene without homolog altered by zoom, missing upstream gene display"+"<br/>";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (partial right)";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (partial right, target query gene is "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream and upstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing downstream and upstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>not reversed (partial left and right, target query gene is "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene without homolog";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene without homolog altered by zoom, missing upstream and downstream gene display"+"<br/>";
			//textBasicSGeneInfo += "<br/><big><i>Gene insertion type : </i></big>subject (partial left and right)";
		} else if (apgqiiIT.getqEnumBlockType().compareTo(
				QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			//isGeneHomologyOriginally = true;
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream and downstream";
			textBasicQGeneInfo += "<i>Symbol type : </i>Gene homology altered by zoom, missing upstream and downstream gene display ; the missing reference homolog is "
					+ locusAndLinkIfMissingTargetSMostSignificantGeneName+"<br/>";
//					textBasicSGeneInfo += "<br/><big><i>Gene homology type : </i></big>reversed (partial left and right, target query gene is "
//							+ locusAndLinkIfMissingTargetQMostSignificantGeneName + ")";
		}
		//textBasicQGeneInfo += "</ul>";
		textSymbolInfo += "<br/><big><i>Location : </i></big>Reference genome";
//		if(isGeneHomologyOriginally){
//			textSymbolInfo += "<br/><big><i>Background colored according to : </i></big>synteny";
//		}
		
		symbolInfo.setHTML(textSymbolInfo);
		discPSymbolInfo.setVisible(true);
		
		basicGeneInfoRef.setHTML(textBasicQGeneInfo);

		// detailled
		// qElementsItem.getQsQElementNumberGene();
		if (getDetail) {
//			@SuppressWarnings("unused")
//			ResultLoadingDialog rld = new ResultLoadingDialog(
//					DetailledInfoStack.DetailledGeneInfoRef,
//					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
//					apgqiiIT.getQGeneId(), 
//					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()
//					);
			RLD.getDetailledGeneInfo(
					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(), 
					apgqiiIT.getQGeneId(), 
					showStack, 
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght(), 
					true
					);
		} else {
			detailledGeneInfoRef.setHTML("");
		}

		discPGeneInfo.setVisible(true);

		if (showStack) {
			levelDetailShowInfo = 7;
			showUserSelectionDetailledInfo();
		}

	}

	public static void fillGeneInfoInStack(
			AbsoPropQSGeneHomoItem apqsghIIT, boolean showStack,
			boolean getDetail) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		
		String textBasicQGeneInfo = "<big>---- Query<br/><i>Name : </i>";
		textBasicQGeneInfo += apqsghIIT.getQsQNameAsHTMLPlusLink()+"<br/>";
		textBasicQGeneInfo += "<i>Locus tag : </i>";
		textBasicQGeneInfo += apqsghIIT.getQsQLocusTagAsHTMLPlusLink()+"<br/>";
		textBasicQGeneInfo += "<i>Start (pb) : </i>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apqsghIIT.getQsQPbStartGeneInElement())+"<br/>"
				+ "<i>Stop (pb) : </i>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apqsghIIT.getQsQPbStopGeneInElement())+"<br/>"
				+ "<i>Strand : </i>";
		if (apqsghIIT.getQsQStrand() > 0) {
			textBasicQGeneInfo += "+"+"<br/>";
		} else {
			textBasicQGeneInfo += "-"+"<br/>";
		}
		

		String textSymbolInfo = "";
		if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			//textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			//textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			//textBasicQGeneInfo += "not reversed";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			//textBasicQGeneInfo += "reversed";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			//textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			//textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
//			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			//textBasicQGeneInfo += "not reversed (cut start)";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			//textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			//textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
//			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Downstream";
			//textBasicQGeneInfo += "reversed (cut start)";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			//textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>No";
			//textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
//			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			//textBasicQGeneInfo += "not reversed (cut stop)";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK) == 0) {
			textSymbolInfo += "<big><i>Symbol type : </i></big>Gene homology";
			//textBasicSyntenyInfo += "<big><i>Symbol type : </i></big>Conserved synteny";
			textSymbolInfo += "<br/><big><i>Reversed alignment ? : </i></big>Yes";
			//textBasicSyntenyInfo += "<br/><big><i>Reversed synteny ? : </i></big>No";
			textSymbolInfo += "<br/><big><i>Symbol modification : </i></big>Zoom, part of the display is missing";
//			textSymbolInfo += "<br/><big><i>Missing display on : </i></big>Reference genome";
			textSymbolInfo += "<br/><big><i>Missing display direction : </i></big>Upstream";
			//textBasicQGeneInfo += "reversed (cut stop)";
		}
		//textBasicQGeneInfo += "</ul>";
		textSymbolInfo += "<br/><big><i>Location : </i></big>Reference and compared genomes";
		textSymbolInfo += "<br/><big><i>Background colored according to : </i></big>synteny";
		
		symbolInfo.setHTML(textSymbolInfo);
		discPSymbolInfo.setVisible(true);
		// detailled
		basicGeneInfoRef.setHTML(textBasicQGeneInfo);
		// detailled
		// qElementsItem.getQsQElementNumberGene();
		if (getDetail) {
			
			if(apqsghIIT.getQsQGeneId() < 0) {
				RLD.getDetailledGeneInfoWithSingletonAlignmentId(
						Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
						apqsghIIT.getSyntenyOrigamiAlignmentId(), 
						showStack, 
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght(), 
						true
						);
			} else {
				RLD.getDetailledGeneInfo(
						Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
						apqsghIIT.getQsQGeneId(), 
						showStack, 
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght(), 
						true
						);
			}
			
			
		} else {
			detailledGeneInfoRef.setHTML("");
		}

		String textBasicSGeneInfo = "<br/><big>---- Subject<br/><i>Name : </i>";
		textBasicSGeneInfo += apqsghIIT.getQsSNameAsHTMLPlusLink()+"<br/>";
		textBasicSGeneInfo += "<i>Locus tag : </i>";
		textBasicSGeneInfo += apqsghIIT.getQsSLocusTagAsHTMLPlusLink()+"<br/>";
		textBasicSGeneInfo += "<i>Start (pb) : </i>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apqsghIIT.getQsSPbStartGeneInElement())+"<br/>"
				+ "<i>Stop (pb) : </i>"
				+ NumberFormat.getFormat("###,###,###,###,###,###").format(
						apqsghIIT.getQsSPbStopGeneInElement())+"<br/>"
				+ "<i>Strand : </i>";
		if (apqsghIIT.getQsSStrand() > 0) {
			textBasicSGeneInfo += "+"+"<br/>";
		} else {
			textBasicSGeneInfo += "-"+"<br/>";
		}
		textBasicSGeneInfo += "<i>Homologie type : </i>";
		if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_HOMOLOGS_BLOCK) == 0) {
			textBasicSGeneInfo += "not reversed";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK) == 0) {
			textBasicSGeneInfo += "reversed";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK) == 0) {
			textBasicSGeneInfo += "not reversed (cut start)";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK) == 0) {
			textBasicSGeneInfo += "reversed (cut start)";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK) == 0) {
			textBasicSGeneInfo += "not reversed (cut stop)";
		} else if (apqsghIIT.getQsEnumBlockType().compareTo(
				QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK) == 0) {
			textBasicSGeneInfo += "reversed (cut stop)";
		}
		//textBasicSGeneInfo += "</li></ul>";
		basicGeneInfoComp.setHTML(textBasicSGeneInfo);

		// detailled
		// qElementsItem.getQsQElementNumberGene();
		if (getDetail) {
//			@SuppressWarnings("unused")
//			ResultLoadingDialog rld = new ResultLoadingDialog(
//					DetailledInfoStack.DetailledGeneInfoComp,
//					Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
//					apqsghIIT.getQsSGeneId(), 
//					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()
//					);
			if(apqsghIIT.getQsSGeneId() < 0){
				RLD.getDetailledGeneInfoWithSingletonAlignmentId(
						Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
						apqsghIIT.getSyntenyOrigamiAlignmentId(),
						showStack,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght(),
						false);
			} else {
				RLD.getDetailledGeneInfo(
						Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId(),
						apqsghIIT.getQsSGeneId(),
						showStack,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght(),
						false
						);
			}
			
		} else {
			detailledGeneInfoComp.setHTML("");
		}

		discPGeneInfo.setVisible(true);

		if (showStack) {
			levelDetailShowInfo = 3;
			showUserSelectionDetailledInfo();
		}

	}

	public static void fillGeneMatchInfoInStack(
			AbsoPropQSGeneHomoItem apqsghiIT
			//, boolean showStack
			) {
		clickForInfo.setHTML("[Double click on a symbol to access the contextual menu.]");
		
		// basic
		String textBasicGeneMatchInfo = "<big><i>Query gene : </i></big>"
				+ apqsghiIT.getQsQMostSignificantGeneNameAsHTMLPlusLink()
				+ "<big><i> versus subject gene : </i></big>"
				+ apqsghiIT.getQsSMostSignificantGeneNameAsHTMLPlusLink();
		textBasicGeneMatchInfo += "<br/><big><i>Ortholog ? : </i></big>";
		if (apqsghiIT.getQsOrigamiAlignmentPairsType() == 1) {
			textBasicGeneMatchInfo += "true";
		} else {
			textBasicGeneMatchInfo += "false";
		}

		for (int i = 0; i < apqsghiIT.getListLightGeneMatchItem().size(); i++) {
			LightGeneMatchItem lgmiIT = apqsghiIT.getListLightGeneMatchItem()
					.get(i);

			// basic
			textBasicGeneMatchInfo += "<br/><big><i>EValue : </i></big>"
					+ lgmiIT.getEValue()
					+ "<br/><big><i>Identity : </i></big>"
					+ lgmiIT.getIdentity()
					+ "%<br/><big><i>Score : </i></big>"
					+ lgmiIT.getScore()
					+ "<br/><big><i>Alignment start on query : </i></big>"
					+ NumberFormat.getFormat("#.##%").format(
							lgmiIT.getQFirstFrac())
					+ "<br/><big><i>Alignment stop on query : </i></big>"
					+ NumberFormat.getFormat("#.##%").format(
							lgmiIT.getQFirstFrac()+lgmiIT.getQAlignFrac())
					+ "<br/><big><i>Alignment start on subject : </i></big>"
					+ NumberFormat.getFormat("#.##%").format(
							lgmiIT.getSFirstFrac())
					+ "<br/><big><i>Alignment stop on subject : </i></big>"
					+ NumberFormat.getFormat("#.##%").format(
							lgmiIT.getSFirstFrac()+lgmiIT.getSAlignFrac());
		}

		geneMatchInfo.setHTML(textBasicGeneMatchInfo);
		qGeneIdForVisuAlignment = apqsghiIT.getQsQGeneId();
		sGeneIdForVisuAlignment = apqsghiIT.getQsSGeneId();
		acVisuAlignment.setVisible(true);
		discPGeneMatchInfo.setVisible(true);

//		if (showStack) {
//			levelDetailShowInfo = 4;
//			GenoOrgaAndHomoTablViewImpl.showUserSelectionDetailledInfo();
//		}

	}

	static void showUserSelectionDetailledInfo() {
		
		if (levelDetailShowInfo == 0) {
			// 0 : no info
			discPSymbolInfo.setVisible(false);
			discPGenomeInfo.setVisible(false);
			discPElementInfo.setVisible(false);
			discPSyntenyInfo.setVisible(false);
			discPGeneInfo.setVisible(false);
			discPGeneMatchInfo.setVisible(false);
		} else if (levelDetailShowInfo == 1) {
			// 1 : genomeInfo
			// discPGenomeInfo.setVisible(true);
			stackPWest.showWidget(0);
			discPGenomeInfo.setOpen(true);
			//scrollPDetailedInfo.ensureVisible(discPGenomeInfo);
		} else if (levelDetailShowInfo == 2) {
			// 2 : syntenyInfo
			// discPSyntenyInfo.setVisible(true);
			stackPWest.showWidget(0);
			discPSyntenyInfo.setOpen(true);
			//scrollPDetailedInfo.ensureVisible(discPSyntenyInfo);
		} else if (levelDetailShowInfo == 3) {
			// 3 : GeneInfo gene homology
			// discPGeneInfo.setVisible(true);
			stackPWest.showWidget(0);
			discPSyntenyInfo.setOpen(forceDiscPSyntenyInfoVisible); //0 defaut, -1 force close, 1 force open
			discPGeneInfo.setOpen(true);
			discPGeneMatchInfo.setOpen(true);
			//scrollPDetailedInfo.ensureVisible(discPGeneInfo);
//		} else if (levelDetailShowInfo == 4) {
//			// 4 : geneMatchInfo
//			// discPGeneMatchInfo.setVisible(true);
//			stackPWest.showWidget(0);
//			discPGeneMatchInfo.setOpen(true);
//			//scrollPDetailedInfo.ensureVisible(discPGeneMatchInfo);
		} else if (levelDetailShowInfo == 5) {
			// 5 : elementInfo
			// discPGeneMatchInfo.setVisible(true);
			stackPWest.showWidget(0);
			discPElementInfo.setOpen(true);
			//scrollPDetailedInfo.ensureVisible(discPElementInfo);
		} else if (levelDetailShowInfo == 6) {
			// 6 : geneInfo s without homologs
			// discPGeneMatchInfo.setVisible(true);
			stackPWest.showWidget(0);
			discPSyntenyInfo.setOpen(forceDiscPSyntenyInfoVisible); //0 defaut, -1 force close, 1 force open
			discPGeneInfo.setOpen(true);
			discPGeneInfoComp.setOpen(true);
			discPGeneInfoRef.setOpen(false);
		} else if (levelDetailShowInfo == 7) {
			// 7 : geneInfo q without homologs
			// discPGeneMatchInfo.setVisible(true);
			stackPWest.showWidget(0);
			discPSyntenyInfo.setOpen(forceDiscPSyntenyInfoVisible); //0 defaut, -1 force close, 1 force open
			discPGeneInfo.setOpen(true);
			discPGeneInfoComp.setOpen(false);
			discPGeneInfoRef.setOpen(true);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
					new Exception("Error in showUserSelectionDetailledInfo : levelDetailShowInfo not supported : "+levelDetailShowInfo)
			);
		}

	}


	public static void setResultsForRefGenomePanelAndListOrgaResultReturned() {

		// if first time set correct size of scgrid
		if (!FIRST_RESULT_LOADED) {
			CenterSLPAllGenomePanels.CURR_HEIGHT_GENOME_PANEL_LP = MainTabPanelView
					.getTabPanel().getOffsetHeight() - 30;
			FIRST_RESULT_LOADED = true;
		}

		// deal with quick nav faetured
		VP_QUICKNAV_FEATURED.clear();
		if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
				.getListUserSelectedOrgaToBeResults().size() > 0) {
			// System.out.println("VP_QUICKNAV_FEATURED");
			for (int i = 0; i < Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getListUserSelectedOrgaToBeResults().size(); i++) {
				// System.out.println("i : "+i);
				LightOrganismItem loi = Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getListUserSelectedOrgaToBeResults().get(i);
				final Anchor ac = new Anchor();
				ac.setText(getTextToDisplayForComparedOrganismsInResultList(loi));
				ac.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						
						if (VP_QUICKNAV_FEATURED.getWidgetIndex(ac) < (CenterSLPAllGenomePanels.START_INDEX + CenterSLPAllGenomePanels.CURR_COUNT_VISIBLE_RESULT_DISPLAYED)
								&& VP_QUICKNAV_FEATURED.getWidgetIndex(ac) >= CenterSLPAllGenomePanels.START_INDEX) {
							// System.out.println("already showing");
						} else {
							GenoOrgaAndHomoTablViewImpl
									.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
							CenterSLPAllGenomePanels.START_INDEX = VP_QUICKNAV_FEATURED
									.getWidgetIndex(ac)
									+ CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED;
							CenterSLPAllGenomePanels.updateResultsDisplay(
									false, true, false);
						}
					}
				});
				ac.addStyleDependentName("LabelInQuickNav");
				VP_QUICKNAV_FEATURED.add(ac);
			}
			PANEL_QUICKNAV_FEATURED.setVisible(true);
		} else {
			PANEL_QUICKNAV_FEATURED.setVisible(false);
		}

		// deal with quick nav public
		buildQuickNavPublic();

		// set enable/disable sort by type
		if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
				.getListReferenceGeneSetForHomologsTable().isEmpty()
				|| Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getListReferenceGeneSetForHomologsTable().size() > SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED) {
			ManageListComparedOrganisms.RB1_SORT_SCOPE_1.setEnabled(false);
		} else {
			ManageListComparedOrganisms.RB1_SORT_SCOPE_1.setEnabled(true);
		}
		// deal with resultListSortScopeType
		ManageListComparedOrganisms.setCorrectUISortScopeTypeButtons(false);

		stackPWest.showWidget(1);
		// cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
		CenterSLPAllGenomePanels.START_INDEX = 0;
		// cleanUpCSSForAllLabelsInQuickNavOnDisplayedPage();

	}

	public static void buildQuickNavPublic() {

		// for (int i = 0; i <
		// SharedAppParams.MAX_NUMBER_GENOME_PANEL_ITEM_TO_BE_RETURNED_ALLOWED;
		// i++) {
		// Anchor ac = (Anchor) VP_QUICKNAV_PUBLIC.getWidget(i);
		// if (i < Insyght.APP_CONTROLER
		// .getCurrentRefGenomePanelAndListOrgaResult()
		// .getLstOrgaResult().size()) {
		// ac.setText(" #"
		// + (i + 1)
		// + " (s="
		// + Insyght.APP_CONTROLER
		// .getCurrentRefGenomePanelAndListOrgaResult()
		// .getLstOrgaResult().get(i).getScore()
		// + ") "
		// + Insyght.APP_CONTROLER
		// .getCurrentRefGenomePanelAndListOrgaResult()
		// .getLstOrgaResult().get(i).getFullName());
		// ac.setVisible(true);
		// } else {
		// ac.setVisible(false);
		// }
		// }

		VP_QUICKNAV_PUBLIC.clear();
		for (int i = 0; i < Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult()
				.size(); i++) {
			if (i < VP_QUICKNAV_PUBLIC.getWidgetCount()) {
				Anchor ac = (Anchor) VP_QUICKNAV_PUBLIC.getWidget(i);
				LightOrganismItem loiIT = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult().get(i);
				String textIT = getTextToDisplayForComparedOrganismsInResultList(loiIT);
				ac.setText(textIT);
				ac.setVisible(true);
			} else {
				// Create new anchor
				// System.out.println("create");
				final Anchor ac = new Anchor();
				LightOrganismItem loiIT = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult().get(i);
				/*String displayPositionInResultListIfGraterThanZero = "";
				if(Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getLstOrgaResult().get(i).getPositionInResultList() >= 0){
					displayPositionInResultListIfGraterThanZero = " #"
							+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getLstOrgaResult().get(i).getPositionInResultList()
							;
				}
				String displayScoreIfGraterThanZero = " ";
				if(Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getLstOrgaResult().get(i).getScore() >= 0){
					displayScoreIfGraterThanZero = " (s="
							+ Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getLstOrgaResult().get(i).getScore()
							+ ") ";
				}*/
				ac.setText(getTextToDisplayForComparedOrganismsInResultList(loiIT));
				/*ac.setText(displayPositionInResultListIfGraterThanZero
						+ displayScoreIfGraterThanZero
						+ Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getLstOrgaResult().get(i).getFullName());*/
				ac.addStyleDependentName("LabelInQuickNav");
				ac.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						if (VP_QUICKNAV_PUBLIC.getWidgetIndex(ac)
								+ CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
								+ Insyght.APP_CONTROLER
										.getCurrentRefGenomePanelAndListOrgaResult()
										.getListUserSelectedOrgaToBeResults()
										.size() < (CenterSLPAllGenomePanels.START_INDEX + CenterSLPAllGenomePanels.CURR_COUNT_VISIBLE_RESULT_DISPLAYED)
								&& VP_QUICKNAV_PUBLIC.getWidgetIndex(ac)
										+ CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
										+ Insyght.APP_CONTROLER
												.getCurrentRefGenomePanelAndListOrgaResult()
												.getListUserSelectedOrgaToBeResults()
												.size() >= CenterSLPAllGenomePanels.START_INDEX) {
							// System.out.println("already showing");
						} else {
							GenoOrgaAndHomoTablViewImpl
									.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
							CenterSLPAllGenomePanels.START_INDEX = VP_QUICKNAV_PUBLIC
									.getWidgetIndex(ac)
									+ CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
									+ Insyght.APP_CONTROLER
											.getCurrentRefGenomePanelAndListOrgaResult()
											.getListUserSelectedOrgaToBeResults()
											.size();
							CenterSLPAllGenomePanels.updateResultsDisplay(
									false, true, false);
						}
					}
				});
				VP_QUICKNAV_PUBLIC.add(ac);
			}
		}

		// hide all other
		// int startI = Insyght.APP_CONTROLER
		// .getCurrentRefGenomePanelAndListOrgaResult()
		// .getLstOrgaResult().size();
		// int endI = VP_QUICKNAV_PUBLIC.getWidgetCount();
		// if(startI < endI){
		// //System.out.println(startI+" - "+endI);
		// for(int i=startI;i<endI;i++){
		// Anchor ac = (Anchor) VP_QUICKNAV_PUBLIC.getWidget(i);
		// ac.setVisible(false);
		// }
		// }

	}

	private static String getTextToDisplayForComparedOrganismsInResultList(LightOrganismItem loiIT) {
		String textIT = "";
		if (loiIT.getPositionInResultList() >= 0) {
			textIT += " #"
					+ loiIT.getPositionInResultList();
		}
		if (loiIT.getScore() >= 0) {
			textIT += " (s="
					+ loiIT.getScore() + ")";
		}
		textIT += " " + loiIT.getFullName();
		return textIT;
	}


	public static void cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage() {

		if (CenterSLPAllGenomePanels.START_INDEX > -1
				&& Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getLstOrgaResult() != null) {

			// System.out.println("getListUserSelectedOrgaToBeResults().size() : "+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults().size());
			// System.out.println("getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult().size() : "+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getLstOrgaResult().size());

			int i = 0;
			for (; i < CenterSLPAllGenomePanels.CURR_COUNT_VISIBLE_RESULT_DISPLAYED; ++i) {

				if (CenterSLPAllGenomePanels.START_INDEX + i < CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED) {
					// System.out.println("in 1");
					VP_QUICKNAV_PRIVATE.getWidget(
							CenterSLPAllGenomePanels.START_INDEX + i)
							.removeStyleDependentName(
									"LabelInQuickNav-OnDisplayedPage");
				} else if (CenterSLPAllGenomePanels.START_INDEX + i < CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
						+ Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getListUserSelectedOrgaToBeResults().size()) {
					// System.out.println("in 2 : "+(CenterSLPAllGenomePanels.START_INDEX
					// + i -
					// CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED));
					VP_QUICKNAV_FEATURED
							.getWidget(
									(CenterSLPAllGenomePanels.START_INDEX + i - CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED))
							.removeStyleDependentName(
									"LabelInQuickNav-OnDisplayedPage");
				} else if (CenterSLPAllGenomePanels.START_INDEX + i < CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED
						+ Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getListUserSelectedOrgaToBeResults().size()
						+ Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getLstOrgaResult().size()) {
					// System.out.println("in 3");
					VP_QUICKNAV_PUBLIC
							.getWidget(
									(CenterSLPAllGenomePanels.START_INDEX
											+ i
											- CenterSLPAllGenomePanels.SIZE_LIST_GROUP_SUBSCRIBED - Insyght.APP_CONTROLER
											.getCurrentRefGenomePanelAndListOrgaResult()
											.getListUserSelectedOrgaToBeResults()
											.size())).removeStyleDependentName(
									"LabelInQuickNav-OnDisplayedPage");
				} else {
					// System.out.println("in 4");
					break;
				}

			}
		}

	}

	public static void hideUserSpecifics() {

		PANEL_QUICKNAV_PRIVATE.setVisible(false);
		VP_QUICKNAV_PRIVATE.clear();

	}

	public static void refreshUserSpecifics() {

		if (Insyght.APP_CONTROLER.getCurrentUser().getSessionId() == null) {
			VP_QUICKNAV_PRIVATE.clear();
			hideUserSpecifics();
		} else {
			VP_QUICKNAV_PRIVATE.clear();
			for (int i = 0; i < Insyght.APP_CONTROLER.getCurrentUser()
					.getListGroupsSubscribed().size(); i++) {

				GroupUsersObj guo = Insyght.APP_CONTROLER.getCurrentUser()
						.getListGroupsSubscribed().get(i);
				final Anchor ac = new Anchor(
				// guo.getSpecies()+ " (strain = "+guo.getStrain()+")"
						guo.getFullName());
				ac.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						/*
						 * Window .alert("clicked " +
						 * Insyght.APP_CONTROLER.getCurrentUser
						 * ().getListGroupsSubscribed
						 * ().get(VP_QUICKNAV_PRIVATE.getWidgetIndex(ac))
						 * .getSpecies() + " (strain = " +
						 * Insyght.APP_CONTROLER.
						 * getCurrentUser().getListGroupsSubscribed
						 * ().get(VP_QUICKNAV_PRIVATE.getWidgetIndex(ac))
						 * .getStrain() + ")");
						 */

						if (VP_QUICKNAV_PRIVATE.getWidgetIndex(ac) < (CenterSLPAllGenomePanels.START_INDEX + CenterSLPAllGenomePanels.CURR_COUNT_VISIBLE_RESULT_DISPLAYED)
								&& VP_QUICKNAV_PRIVATE.getWidgetIndex(ac) >= CenterSLPAllGenomePanels.START_INDEX) {
							// System.out.println("already showing");
						} else {
							GenoOrgaAndHomoTablViewImpl
									.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
							CenterSLPAllGenomePanels.START_INDEX = VP_QUICKNAV_PRIVATE
									.getWidgetIndex(ac);
							CenterSLPAllGenomePanels.updateResultsDisplay(
									false, true, false);
						}

					}
				});
				ac.addStyleDependentName("LabelInQuickNav");
				VP_QUICKNAV_PRIVATE.add(ac);

			}
			if (Insyght.APP_CONTROLER.getCurrentUser()
					.getListGroupsSubscribed().size() > 0) {
				PANEL_QUICKNAV_PRIVATE.setVisible(true);
			} else {
				PANEL_QUICKNAV_PRIVATE.setVisible(false);
			}
		}

	}


}
