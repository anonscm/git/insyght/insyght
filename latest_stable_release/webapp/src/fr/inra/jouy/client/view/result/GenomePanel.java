package fr.inra.jouy.client.view.result;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;

public class GenomePanel extends Composite {

	private static GenomePanelUiBinder uiBinder = GWT
			.create(GenomePanelUiBinder.class);

	interface GenomePanelUiBinder extends UiBinder<Widget, GenomePanel> {
	}
	
	@UiField
	DeckPanel dpRootGenomePanel;
	@UiField
	Label genomeName;
	@UiField
	ToggleButton buttonIncreaseSize;
	@UiField
	ToggleButton buttonNormalSize;
	@UiField
	ToggleButton buttonDecreaseSize;
	private int scLowerSize;
	
	@UiField
	public SyntenyCanvas syntenyCanvas;
		
	
	public static int MAX_PX_scLowerSize = 130;//150
	//public static int MEDIUM_PX_scLowerSize = 75;
	public static int MEDIUM_PX_scLowerSize = MAX_PX_scLowerSize / 2;
	public static int HEIGHT_PX_TITLEBAR = 31;
	
	
	/**
	 * True if the GenomePanel holds the reference genome.
	 */
	public boolean isReferenceGenome;

	/**
	 * GenomePanelItem that load its data into the GenomePanel
	 */
	private GenomePanelItem associatedGenomePanelItem = null;

	private ResuLoadInitAbsoPropItemDial RIAPID = new ResuLoadInitAbsoPropItemDial();
	
	public Label getGenomeNameLabel() {
		return genomeName;
	}
	

	public SyntenyCanvas getSyntenyCanvas(){
		return syntenyCanvas;
	}
	
	/**
	 * Returns the associatedGenomePanelItem
	 */	
	public GenomePanelItem getGenomePanelItem(){

		return associatedGenomePanelItem;
	}


	public boolean isReferenceGenome() {
		return isReferenceGenome;
	}


	public void setScLowerSize(int scLowerSize) {
		this.scLowerSize = scLowerSize;
	}


	public int getScLowerSize() {
		return scLowerSize;
	}

	public void setReferenceGenome(boolean isReferenceGenome) {
		this.isReferenceGenome = isReferenceGenome;
		if (isReferenceGenome) {
			genomeName.setStyleDependentName("ResultGenome", false);
			genomeName.setStyleDependentName("ReferenceGenome", true);
		} else {
			genomeName.setStyleDependentName("ReferenceGenome", false);
			genomeName.setStyleDependentName("ResultGenome", true);
		}
		
	}
	
	public void setLoadingState(boolean isLoading){
		if(isLoading){
			dpRootGenomePanel.showWidget(0);
		}else{
			dpRootGenomePanel.showWidget(1);
		}
	}
	

	public GenomePanel() {
		initWidget(uiBinder.createAndBindUi(this));
		
		dpRootGenomePanel.setAnimationEnabled(false);
		setLoadingState(false);
		//register GenomePanel
		Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().add(this);
		
		//register to child syntenyCanvas
		syntenyCanvas.setAssociatedGenomePanel(this);

		//not ref by default
		setReferenceGenome(false);
		
	}

	/*
	@UiHandler("buttonX")
	void onButtonXClick(ClickEvent e) {
		setSizeStateToHidden(true);
	}
	*/
	
	@UiHandler("buttonIncreaseSize")
	void onButtonIncreaseSizeClick(ClickEvent e) {
		if(associatedGenomePanelItem.getSizeState().compareTo(GenomePanelItem.GpiSizeState.INTERMEDIATE)==0){
			//System.out.println("was INTERMEDIATE");
			syntenyCanvas.drawViewToCanvas(1, 0, false, true, true, false);
		}else{
			//System.out.println("was NOT INTERMEDIATE");
			syntenyCanvas.drawViewToCanvas(1, 0, true, true, true, false);
		}
		
		setSizeToMax(true);
	}

	@UiHandler("buttonNormalSize")
	void onButtonNormalSizeClick(ClickEvent e) {
		setSizeToIntermediate(true);
	}

	@UiHandler("buttonDecreaseSize")
	void onButtonDecreaseSizeClick(ClickEvent e) {
		setSizeToMin(true);
	}

	
	public void setSizeToMax(boolean reload){
		buttonNormalSize.setDown(false);
		buttonIncreaseSize.setDown(true);
		buttonDecreaseSize.setDown(false);
		if(!syntenyCanvas.isVisible()){syntenyCanvas.setVisible(true);}
		setScLowerSize(MAX_PX_scLowerSize);
		syntenyCanvas.setHeight(MAX_PX_scLowerSize+"px");
		syntenyCanvas.navigateLeftSpeed1.setHeight((int) Math.floor(MAX_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateLeftSpeed2.setHeight((int) Math.floor(MAX_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateLeftSpeed3.setHeight((int) Math.floor(MAX_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateRightSpeed1.setHeight((int) Math.floor(MAX_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateRightSpeed2.setHeight((int) Math.floor(MAX_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateRightSpeed3.setHeight((int) Math.floor(MAX_PX_scLowerSize/3)+"px");
		associatedGenomePanelItem.setSizeState(GenomePanelItem.GpiSizeState.MAX);//"MAX"
		if(reload){
			GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
			CenterSLPAllGenomePanels.updateResultsDisplay(false, true, false);
		}
	}

	public void setSizeToMin(boolean reload){
		buttonNormalSize.setDown(false);
		buttonIncreaseSize.setDown(false);
		buttonDecreaseSize.setDown(true);
		//scLower.setVisible(false);
		syntenyCanvas.setVisible(false);
		setScLowerSize(0);
		associatedGenomePanelItem.setSizeState(GenomePanelItem.GpiSizeState.MIN);

		
		if(reload){
			GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
			CenterSLPAllGenomePanels.updateResultsDisplay(false, true, false);
		}

	}	

	/**
	 * Sets the genomePanel size to intermediate
	 */
	public void setSizeToIntermediate(boolean reload){
		buttonNormalSize.setDown(true);
		buttonIncreaseSize.setDown(false);
		buttonDecreaseSize.setDown(false);
		if(!this.isVisible()){this.setVisible(true);}
		//if(!scLower.isVisible()){scLower.setVisible(true);}
		if(!syntenyCanvas.isVisible()){syntenyCanvas.setVisible(true);}
		setScLowerSize(MEDIUM_PX_scLowerSize);
		//scLower.setHeight(MEDIUM_PX_scLowerSize+"px");
		syntenyCanvas.setHeight(MEDIUM_PX_scLowerSize+"px");
		syntenyCanvas.navigateLeftSpeed1.setHeight((int) Math.floor(MEDIUM_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateLeftSpeed2.setHeight((int) Math.floor(MEDIUM_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateLeftSpeed3.setHeight((int) Math.floor(MEDIUM_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateRightSpeed1.setHeight((int) Math.floor(MEDIUM_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateRightSpeed2.setHeight((int) Math.floor(MEDIUM_PX_scLowerSize/3)+"px");
		syntenyCanvas.navigateRightSpeed3.setHeight((int) Math.floor(MEDIUM_PX_scLowerSize/3)+"px");
		associatedGenomePanelItem.setSizeState(GenomePanelItem.GpiSizeState.INTERMEDIATE);
		if(reload){
			GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
			CenterSLPAllGenomePanels.updateResultsDisplay(false, true, false);
		}
		
	}
	
	
	public void selectThisGenomePanel(){
		
		//select genomePanel
		if (!this.equals(Insyght.APP_CONTROLER
				.getCurrSelctGenomePanel())) {
			
			//clear current selection
			if(Insyght.APP_CONTROLER
					.getCurrSelctGenomePanel() != null){
				Insyght.APP_CONTROLER.clearGenomePanelSelection();
			}
			     
			//set variable
			Insyght.APP_CONTROLER.setCurrSelctGenomePanel(this);
			//set style 
			this.getGenomeNameLabel().addStyleDependentName("GenomePanel-SelectedGenome");
			
			int referenceCumulativeSize = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().
						getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(0).getQsizeOfOragnismInPb();
			int resultCumulativeSize = -1;
			try {
				resultCumulativeSize = this.getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb();

				// fill with info
				GenoOrgaAndHomoTablViewImpl.fillBasicGenomeInfoInStack(
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem(),
						referenceCumulativeSize,
						this.getGenomePanelItem(),
						resultCumulativeSize, 
						false);
				
			} catch (Exception e) {
				//else previous statement give rise to an of bound error when no alignments are found to be shown = associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemS() empty
				resultCumulativeSize = -1;
			}
			
			
		}
	}
	

	public void setAssociatedGenomePanelItem(GenomePanelItem associatedGenomePanelItemSent){

		associatedGenomePanelItem = associatedGenomePanelItemSent;

		if(associatedGenomePanelItem.isReferenceGenome()){
			String stringToDW = " Reference organism : "+
			//associatedGenomePanelItem.getSpecies() + " (strain : "+associatedGenomePanelItem.getStrain()+")";//+ " [orga id="+associatedGenomePanelItem.getOrganismId()+"] ";
			associatedGenomePanelItem.getFullName();
			genomeName.setText(stringToDW);
		}else{
			String stringToDW = "";
			if(associatedGenomePanelItem.getPositionInResultList()>0){
				stringToDW += " #"+associatedGenomePanelItem.getPositionInResultList()+" ";
			}
			if(associatedGenomePanelItem.getScore()>0){
				stringToDW += " (score="+associatedGenomePanelItem.getScore()+")";
			}
			stringToDW += " "+
			//associatedGenomePanelItem.getSpecies() + " (strain : "+associatedGenomePanelItem.getStrain()+")";//+" [orga id="+associatedGenomePanelItem.getOrganismId()+"] ";
			associatedGenomePanelItem.getFullName();
			genomeName.setText(stringToDW);
		}
		
		// load the correct size state
		/*if(associatedGenomePanelItem.getSizeState().compareTo("HIDDEN") == 0){
			setSizeStateToHidden(false);
		}else */
		if(associatedGenomePanelItem.getSizeState().compareTo(GenomePanelItem.GpiSizeState.INTERMEDIATE) == 0){
			setSizeToIntermediate(false);
		}else if(associatedGenomePanelItem.getSizeState().compareTo(GenomePanelItem.GpiSizeState.MAX) == 0){
			setSizeToMax(false);
		}else if(associatedGenomePanelItem.getSizeState().compareTo(GenomePanelItem.GpiSizeState.MIN) == 0){
			setSizeToMin(false);
		}
				
		// make the grid with the newly sent GenomePanelItem
		//makeGrid();
		
		showCorrectViewType(false);
		
		//setLoadingState(false);
		//CenterSLPAllGenomePanels.placeWidgetCorrectlyOnCenterSLPAllGenomePanels((Widget)this, scLowerSize);
		
	}//public void setAssociatedGenomePanelItem(GenomePanelItem associatedGenomePanelItemSent){


	public void showErrorMssg() {
		dpRootGenomePanel.showWidget(2);
	}

	public void showCorrectViewType(boolean scrollToLeft) {
		
				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()==null){
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in showCorrectViewType : getCurrentRefGenomePanelAndListOrgaResult is null"));
				}else{
					if(getGenomePanelItem().getListAbsoluteProportionElementItemQ().isEmpty()){
						getGenomePanelItem().setListAbsoluteProportionElementItemQ(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ());
					}
					
					if(getGenomePanelItem().getFullAssociatedlistAbsoluteProportionItemToDisplay().isEmpty()){
						//&& !waitingForSyntenyViewToFinishLoading){
					//System.err.println("loading new");
					syntenyCanvas.drawLoading();
//					@SuppressWarnings("unused")
//					ResuLoadInitAbsoPropItemDial dl = new ResuLoadInitAbsoPropItemDial(
//							ResuLoadInitAbsoPropItemDial.TypeOfLoadingToDo.LOAD_ItemsAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertion,
//							getGenomePanelItem().getOrganismId(),
//							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(this),
//							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght(),
//							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable());
					
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
						RIAPID.loadItemsAbsoluteProportionGeneSet(
								getGenomePanelItem().getOrganismId(),
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(this),
								Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable()
								);
					}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
						RIAPID.loadItemsAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertion(
								getGenomePanelItem().getOrganismId(),
								Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(this)
								);
					}else{
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in showCorrectViewType : unrecognized view type = "+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght()));
					}
					
					
					}else{
						syntenyCanvas.clearWholeCanvas();
						syntenyCanvas.drawViewToCanvas(1.0, 0, true, true, false, false);
					}
					
				}
				
		
	}




}
