package fr.inra.jouy.client.view.result;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.ProvidesResize;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.view.crossSections.components.MultiValueSuggestBox;
import fr.inra.jouy.client.view.search.SearchViewImpl;
import fr.inra.jouy.shared.pojos.applicationItems.SharedAppParams;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;

public class AnnotCompaViewImpl extends Composite implements ProvidesResize, RequiresResize{

	private static AnnotCompaViewImplUiBinder uiBinder = GWT
			.create(AnnotCompaViewImplUiBinder.class);

	interface AnnotCompaViewImplUiBinder extends
			UiBinder<Widget, AnnotCompaViewImpl> {
	}
	
	@UiField
	static StackLayoutPanel stackPWest;
	
	@UiField
	static HTML basicElementInfo;
	@UiField
	static HTML detailledElementInfo;
			
	@UiField
	static HTML basicInfoGene;
//	@UiField
//	static HTML infoGene;
	@UiField
	static VerticalPanel vpInfoGene;

	
	@UiField
	static VerticalPanel vpComparedSetOrganismsMultiSuggBox;
	@UiField
	static Button refreshComparedSetOrganisms;
	@UiField
	static Button resetComparedSetOrganisms;
	@UiField
	static HTML infoComparedSetOrganisms;
			
	
	@UiField
	static CheckBox paramBDBHOnly;
	@UiField
	static TextBox paramMaxEvalue;
	@UiField
	static TextBox paramMinPercentIdentity;
	@UiField
	static TextBox paramMinPercentAlignLenght;
	@UiField
	Button resetParam;
	@UiField
	Button refreshParam;
	@UiField
	static HTML infoParam;
	
	
//	@UiField Button export;
//	@UiField TextBox exportEmailAddress;
//	@UiField static HTML infoExport;
	
	//static ArrayList<Integer> refAlGeneId = null;
	static int currClickedRefGeneId = -1;
	//static ArrayList<Integer> listElementIdsThenStartPbthenStopPBLooped = null;
	static ArrayList<OrganismItem> listComparedOrgaToSend = new ArrayList<OrganismItem>();
	//static boolean assumeAllOrgaRoot = true;
	//static String allOrgaRootText = "root, taxo_id = 1";
	public static boolean listDisplayedGeneIdsAnnotCompaHasChanged = true;
	public static boolean alreadyResizing = false;
	@UiField CompareHomologsAnnot compareHomologsAnnotIT;
	public static ArrayList<String> alStringDetailsRefGeneSelected = new ArrayList<String>();
	
	
	public AnnotCompaViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void onResize() {
		//System.err.println("here1");
		if(!alreadyResizing){
			alreadyResizing = true;
			compareHomologsAnnotIT.onResize();
		}
	}

	@UiHandler("refreshParam")
	void onRefreshParamClick(ClickEvent e) {
		CompareHomologsAnnot.getComparisonHomologAnnotation();
	}
	
	@UiHandler("resetParam")
	void onResetParamClick(ClickEvent e) {
		resetParamHomolog();
		CompareHomologsAnnot.getComparisonHomologAnnotation();
	}
	
	@UiHandler("resetComparedSetOrganisms")
	void onResetComparedSetOrganismsClick(ClickEvent e) {
		listComparedOrgaToSend.clear();
		resetParamComparedSetOrganisms();
	}
	
	@UiHandler("refreshComparedSetOrganisms")
	void onRefreshComparedSetOrganismsClick(ClickEvent e) {
		listComparedOrgaToSend.clear();
		if(vpComparedSetOrganismsMultiSuggBox.getWidgetCount() == 1){
			if(vpComparedSetOrganismsMultiSuggBox.getWidget(0) instanceof MultiValueSuggestBox){
				ArrayList<String> alSelectedSpecies = ((MultiValueSuggestBox) vpComparedSetOrganismsMultiSuggBox.getWidget(0))
						.getListItemsSelected();
				
				if (alSelectedSpecies.isEmpty()) {
					infoComparedSetOrganisms.setStyleDependentName("greenText", false);
					infoComparedSetOrganisms.setStyleDependentName("redText", true);
					infoComparedSetOrganisms.setHTML("No Organism detected. Please select at least one organism or taxonomic node.");
					return;
				}
				
				String stToAddToShowInfoAboutOrgaUnderNode = "";
				for (int i = 0; i < alSelectedSpecies.size(); i++) {
					String selectedSpIT = alSelectedSpecies.get(i);
					ArrayList<OrganismItem> alOiIT = Insyght.APP_CONTROLER
							.getListOrganismItemWithStringFullName(selectedSpIT);
					if (!alOiIT.isEmpty()) {
						if(alOiIT.size() > 1){
							stToAddToShowInfoAboutOrgaUnderNode += "<br/>"
									+ selectedSpIT
									+ " has been matched to the following "
									+ alOiIT.size() + " organisms :<ul>";
							for (int l = 0; l < alOiIT.size(); l++) {
								listComparedOrgaToSend.add(alOiIT.get(l));
								stToAddToShowInfoAboutOrgaUnderNode += "<li>"
										+ alOiIT.get(l).getFullName() + "</li>";
							}
							stToAddToShowInfoAboutOrgaUnderNode += "</ul>";
						}else{
							listComparedOrgaToSend.add(alOiIT.get(0));
						}
						
					} else {
						infoComparedSetOrganisms.setStyleDependentName("greenText", false);
						infoComparedSetOrganisms.setStyleDependentName("redText", true);
						infoComparedSetOrganisms.setHTML("Error: No Organism detected for search term "+selectedSpIT);
						return;
					}
				}
				
				infoComparedSetOrganisms.setStyleDependentName("redText", false);
				infoComparedSetOrganisms.setStyleDependentName("greenText", true);
				infoComparedSetOrganisms.setHTML(stToAddToShowInfoAboutOrgaUnderNode);
				
				
			}else{
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in resetParam: orgaBrowserPanelCompaHomo.getWidget(0) not instanceof MultiValueSuggestBox :"));
			}
		}else{
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in resetParam: orgaBrowserPanelCompaHomo.getWidgetCount() != 1 :"+vpComparedSetOrganismsMultiSuggBox.getWidgetCount()));
		}
		CompareHomologsAnnot.getComparisonHomologAnnotation();
		
	}
	

	static void resetParamComparedSetOrganisms(){
		if(vpComparedSetOrganismsMultiSuggBox.getWidgetCount() == 1){
			if(vpComparedSetOrganismsMultiSuggBox.getWidget(0) instanceof MultiValueSuggestBox){
				//((MultiValueSuggestBox)vpComparedSetOrganismsMultiSuggBox.getWidget(0)).setText(allOrgaRootText);
				((MultiValueSuggestBox)vpComparedSetOrganismsMultiSuggBox.getWidget(0)).clear();
				((MultiValueSuggestBox)vpComparedSetOrganismsMultiSuggBox.getWidget(0)).addRootSelectionIfAvailable();
			}else{
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in resetParam: orgaBrowserPanelCompaHomo.getWidget(0) not instanceof MultiValueSuggestBox :"));
			}
		}else{
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in resetParam: orgaBrowserPanelCompaHomo.getWidgetCount() != 1 :"+vpComparedSetOrganismsMultiSuggBox.getWidgetCount()));
		}
	}
	
	static void resetParamHomolog() {
		//default
		paramBDBHOnly.setValue(true);
		paramMaxEvalue.setText("0.01");
		paramMinPercentIdentity.setText("0");
		paramMinPercentAlignLenght.setText("0");
		infoParam.setText("");
	}
	
	

	public static Double getParamMaxEvalueToSend() {
		if(paramMaxEvalue.getText().matches("^[\\d\\.]*$")){
			Double paramMaxEvalueIT = Double.parseDouble(paramMaxEvalue
					.getText());
			if (paramMaxEvalueIT < 0) {
				paramMaxEvalueIT = 0.0;
				paramMaxEvalue.setText("0.0");
			}
			return paramMaxEvalueIT;
		}else{
			//error
			infoParam.setText("Invalid parameter : Homologs max Evalue. Only digits and period are allowed.");
			return -1.0;
		}
	}
	


	public static boolean getParamBdbhOnlyToSend() {
		return paramBDBHOnly.getValue();
	}

	public static int getParamMinPercentAlignLenghtToSend() {
		if(paramMinPercentAlignLenght.getText().matches("^\\d*$")){
			int paramMinPercentAlignLenghtIT = Integer.parseInt(paramMinPercentAlignLenght.getText());
			if (paramMinPercentAlignLenghtIT > 100) {
				paramMinPercentAlignLenghtIT = 100;
				paramMinPercentAlignLenght.setText("100");
			}
			if (paramMinPercentAlignLenghtIT < 0) {
				paramMinPercentAlignLenghtIT = 0;
				paramMinPercentAlignLenght.setText("0");
			}
			return paramMinPercentAlignLenghtIT;
		}else{
			//error
			infoParam.setText("Invalid parameter : Homologs min % alignment length. Only digits are allowed.");
			return -1;
		}
	}

	public static int getParamMinPercentIdentityToSend() {
		if(paramMinPercentIdentity.getText().matches("^\\d*$")){
			int paramMinPercentIdentityIT = Integer.parseInt(paramMinPercentIdentity.getText());
			if (paramMinPercentIdentityIT > 100) {
				paramMinPercentIdentityIT = 100;
				paramMinPercentIdentity.setText("100");
			}
			if (paramMinPercentIdentityIT < 0) {
				paramMinPercentIdentityIT = 0;
				paramMinPercentIdentity.setText("0");
			}
			return paramMinPercentIdentityIT;
		}else{
			//error
			infoParam.setText("Invalid parameter : Homologs min % identity. Only digits are allowed.");
			return -1;
		}
	}


	private static void setvpComparedSetOrganismsSuggBox() {
		vpComparedSetOrganismsMultiSuggBox.clear();
		final MultiValueSuggestBox mvsbCompaHomo = new MultiValueSuggestBox(SearchViewImpl.oracle,
				"blue", "80%",
				SharedAppParams.MAX_NUMBER_FEATURED_ORGA_ALLOWED);
		//final SuggestBox sugBoxCompa = new SuggestBox(SearchViewImpl.oracle);
		vpComparedSetOrganismsMultiSuggBox.add(mvsbCompaHomo);
		vpComparedSetOrganismsMultiSuggBox.setCellHorizontalAlignment(mvsbCompaHomo,
				HasHorizontalAlignment.ALIGN_CENTER);
		mvsbCompaHomo.addRootSelectionIfAvailable();
	}


	public static void loadResults(
			) {
		if(listDisplayedGeneIdsAnnotCompaHasChanged){
			// set variable and subtitle text
			CompareHomologsAnnot.selectedOrganism.setHTML("Reference organism : "+Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getReferenceGenomePanelItem().getFullName()
					);
			setvpComparedSetOrganismsSuggBox();
			resetParamHomolog();
			CompareHomologsAnnot.getComparisonHomologAnnotation();
			listDisplayedGeneIdsAnnotCompaHasChanged = false;
		}
	}

	
	public static void updateDetailsRefGeneSelected(){
		vpInfoGene.clear();
		//String infoGene = "<ul><big>";
		//String db_xref = "";
		
		//boolean infoGeneNoEmpty = false;
		//boolean db_xrefNoEmpty = false;
		
		for(String stIT : alStringDetailsRefGeneSelected){
			//System.out.println(stIT);
//			if(stIT.contains("db_xref :")){
//				db_xrefNoEmpty = true;
//				//System.out.println("ok");
//				db_xref += stIT + "<br/>";
//			}else{
//				infoGeneNoEmpty = true;
//				infoGene += "<li>" + stIT + "</li>";
				vpInfoGene.add(new HTML("<big>"+stIT+"</big>"));
//			}
		}
		//infoGene += "</big></ul>";
		//db_xref += "</big>";
//		if(infoGeneNoEmpty){
//			vpInfoGene.add(new HTML(infoGene));
//		}
//		if(db_xrefNoEmpty){
//			DisclosurePanel dpIT = new DisclosurePanel();
//			dpIT.setOpen(false);
//			dpIT.setAnimationEnabled(true);
//			dpIT.setHeader(new HTML("[ > Show / Hide list db_xref ]"));
//			dpIT.setContent(new HTML(db_xref));
//			vpInfoGene.add(dpIT);
//		}
		stackPWest.showWidget(0);
	}

	public static void clearDetailsRefGeneSelected(){
		alStringDetailsRefGeneSelected = new ArrayList<String>();
		basicElementInfo.setHTML("");
		detailledElementInfo.setHTML("");
		vpInfoGene.clear();
		vpInfoGene.add(new HTML("[Click on a reference gene to display detailed information.]" +
				"<br/><br/>[Double click on a reference gene to access the contextual menu.]"));
	}
	
//	public static void colorUncoloredRefGeneDetailsInRed(){
//		for(int i=0;i<AnnotCompaViewImpl.vpInfoGene.getWidgetCount();i++){
//			if(AnnotCompaViewImpl.vpInfoGene.getWidget(i) instanceof HTML){
//				String htmlRefIT = ((HTML)AnnotCompaViewImpl.vpInfoGene.getWidget(i)).getHTML();
//				boolean doColorInRed = true;
//				if(CompareHomologsAnnot.doNotColorThisTag(htmlRefIT)){
//					doColorInRed = false;
//				}
//				if(doColorInRed){
//					((HTML)AnnotCompaViewImpl.vpInfoGene.getWidget(i)).setHTML(
//							"<span style=\"background-color:#FEE0C6;\">"
//							+ ((HTML)AnnotCompaViewImpl.vpInfoGene.getWidget(i)).getHTML() + "</span>");
//				}
//			}
//		}
//	}
	
}
