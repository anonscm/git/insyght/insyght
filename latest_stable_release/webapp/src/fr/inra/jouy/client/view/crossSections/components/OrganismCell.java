package fr.inra.jouy.client.view.crossSections.components;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
//import com.google.gwt.core.client.GWT;
//import com.google.gwt.safecss.shared.SafeStyles;
//import com.google.gwt.safehtml.client.SafeHtmlTemplates;

import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;


public class OrganismCell extends AbstractCell<LightOrganismItem> {

	
    /*interface Templates extends SafeHtmlTemplates {
    	//@SafeHtmlTemplates.Template("<div style=\"border-bottom: 1px solid {0}\">{1}</div>")
		@SafeHtmlTemplates.Template("<div style=\"{0}\">{1}</div>")
    	SafeHtml div(String gradient, SafeHtml value);
    }
    private static Templates templates = GWT.create(Templates.class);
    */
	
	public OrganismCell() {
		super("click"); // , "dblclick"
	}

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context,
			LightOrganismItem value, SafeHtmlBuilder sb) {

		// Value can be null, so do a null check..
		if (value == null) {
			return;
		}
		
		String safeGradient = "border-bottom: 1px solid rgba(209, 209, 224,1); border-image: linear-gradient(to right, rgba(209, 209, 224,0), rgba(209, 209, 224,1), rgba(209, 209, 224,0)); border-image-slice: 1;";
		//sb.append(templates.div(safeGradient, safeValue));
		sb.appendHtmlConstant("<div style=\""+safeGradient+"\">");
		SafeHtml safeValue = SafeHtmlUtils.fromTrustedString(
				"<big><big><big>&nbsp;&nbsp;</big></big></big><big>"
				);
		sb.append(safeValue);
		
		if (value.getPositionInResultList() >= 0) {
			safeValue = SafeHtmlUtils.fromTrustedString(
					" #"
					+ value.getPositionInResultList()
					);
			sb.append(safeValue);
		}
		
		if (value.getScore() >= 0) {
			safeValue = SafeHtmlUtils.fromTrustedString(
					" (s="
					+ value.getScore()
					+ ")");
			sb.append(safeValue);
		}

		safeValue = SafeHtmlUtils.fromTrustedString(" "+value.getFullName()
				+ "</big>");
		sb.append(safeValue);

		sb.appendHtmlConstant("</div>");
		/*sb.appendHtmlConstant("<div class=styleRowsCellList>");
		sb.appendHtmlConstant("<big><big><big>&nbsp;&nbsp;-</big></big></big>");
		sb.appendHtmlConstant("<big>");
		
		sb.appendHtmlConstant(" (s=");
		sb.appendEscaped(""+value.getScore());
		sb.appendHtmlConstant(")");
		
		sb.appendEscaped(" "+value.getFullName());
		
		sb.appendHtmlConstant("</big>");
		sb.appendHtmlConstant("</div>");*/
		//sb.appendEscaped(" (" + NumberFormat.getFormat("###,###,###,###,###,###.##").format(value.getStart()));
		//sb.appendHtmlConstant("-");
		//sb.appendEscaped("" + NumberFormat.getFormat("###,###,###,###,###,###.##").format(value.getStop()));
		//sb.appendHtmlConstant(")");*/
	}
	

}
