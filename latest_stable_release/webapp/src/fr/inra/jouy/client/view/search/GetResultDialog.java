package fr.inra.jouy.client.view.search;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
//import com.google.gwt.regexp.shared.MatchResult;
//import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
//import com.google.gwt.user.client.History;
//import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.NavigationControler;
import fr.inra.jouy.client.SessionStorageControler;
import fr.inra.jouy.client.RPC.CallForHomoBrowResu;
import fr.inra.jouy.client.RPC.CallForHomoBrowResuAsync;
import fr.inra.jouy.client.RPC.CallForInfoDB;
import fr.inra.jouy.client.RPC.CallForInfoDBAsync;
import fr.inra.jouy.client.view.crossSections.popUp.ManageListComparedOrganisms;
import fr.inra.jouy.client.view.crossSections.popUp.PopUpListGenesDialog;
//import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.result.AnnotCompaViewImpl;
import fr.inra.jouy.client.view.result.CenterSLPAllGenomePanels;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem;
//import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;

public class GetResultDialog extends DialogBox {

	private static getResultDialogUiBinder uiBinder = GWT
			.create(getResultDialogUiBinder.class);

	interface getResultDialogUiBinder extends UiBinder<Widget, GetResultDialog> {
	}

	private final CallForHomoBrowResuAsync homologieBrowsingResultService = (CallForHomoBrowResuAsync) GWT
			.create(CallForHomoBrowResu.class);

	private final CallForInfoDBAsync callForInfoDBService = (CallForInfoDBAsync) GWT.create(CallForInfoDB.class);

	@UiField Label lb;

	public GetResultDialog(
			final int geneId
			, final boolean updateSearchTab
			, final boolean updateFindGenePopUp
			) {
		//get detailled gene info
		
		setWidget(uiBinder.createAndBindUi(this));
		AsyncCallback<ArrayList<String>> callback = new AsyncCallback<ArrayList<String>>() {
			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				SearchViewImpl.mySearchPopupInfo.setCallBackHtml("<br/><ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(ArrayList<String> alStringReturned) {
				String modifiedStringToReturn = "";
				for (int i = 0; i < alStringReturned.size(); i++) {
					if (i == 0) {
						modifiedStringToReturn += "<ul><big>";
					}
					modifiedStringToReturn += "<li>"+alStringReturned.get(i)+"</li>";
				}
				if(!alStringReturned.isEmpty()){
					modifiedStringToReturn += "</big></ul>";
				}
				if(updateSearchTab){
					SearchViewImpl.mySearchPopupInfo.setDirectHtml("");
					SearchViewImpl.mySearchPopupInfo.setCallBackHtml(modifiedStringToReturn);
				}
				if(updateFindGenePopUp){
					modifiedStringToReturn += SessionStorageControler
							.getInfoSpecialScoreForGeneIfRelevantAndAvailable(
									geneId,
									true, true) ;//+ modifiedStringToReturn
					PopUpListGenesDialog.htmlBasicGeneInfoInSearch.setHTML("");
					PopUpListGenesDialog.htmlDetailledGeneInfoInSearch.setHTML(modifiedStringToReturn);
					PopUpListGenesDialog.menufindSelectedGene.setVisible(true);
					PopUpListGenesDialog.AddThisCDSToSelectionReferenceGeneSet.setVisible(true);
					PopUpListGenesDialog.notificationCDSAddedToSelectionReferenceGeneSet.setHTML("");
					PopUpListGenesDialog.notificationCDSAddedToSelectionReferenceGeneSet.setStyleDependentName("fadeIn7Seconds", false);
				}
			}
		};

		try {
			callForInfoDBService.getDetailledGeneInfoAsListStringForHTMLWithGeneId(
					geneId, callback);
		} catch (Exception e) {
			//String mssgError = "ERROR in showdetailledGeneInfo: "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			SearchViewImpl.mySearchPopupInfo.setCallBackHtml("<br/><ul><li><b>ERROR detailledGeneInfo</b></li></ul>");
		}
	}

	public GetResultDialog(SearchItem siSent
			//,
			//final int displayIndex,
			//final int resultIndex,
			//final String group1PNavTab,
			//final String group1PFirstRowShownId
			//boolean adaptUIAccordingToNavUrlParam
			, boolean updateResultsDisplay
			) {
		setWidget(uiBinder.createAndBindUi(this));
		
		AsyncCallback<RefGenoPanelAndListOrgaResu> callback = new AsyncCallback<RefGenoPanelAndListOrgaResu>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
				//SearchViewImpl.rootDeckP.showWidget(0);
				Insyght.INSYGHT_LOADING_MASK.hide();
			}

			/*
			 * for Java 1.6 JDK et compiler compliance
			 * 
			 * @Override
			 */
			public void onSuccess(
					final RefGenoPanelAndListOrgaResu refGenomePanelAndListOrgaResultReturned) {
				
				// set the current HomologieBrowsingResultItem
				Insyght.APP_CONTROLER
						.setCurrentRefGenomePanelAndListOrgaResult(refGenomePanelAndListOrgaResultReturned);
				Insyght.APP_CONTROLER.resetAllCurrentListResultGenomePanelItem();
				
				AnnotCompaViewImpl.listDisplayedGeneIdsAnnotCompaHasChanged = true;
				ManageListComparedOrganisms.listDataProviderLOIMainListHasChanged = true;
				
				
				Scheduler.get().scheduleDeferred(new ScheduledCommand() {
					public void execute() {
						GenoOrgaAndHomoTablViewImpl.setResultsForRefGenomePanelAndListOrgaResultReturned();				
						NavigationControler.showCorrectNavigationState(
								Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().toString(),
								Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getFirstRowShownId()
								);
						if (updateResultsDisplay) {
							CenterSLPAllGenomePanels.updateResultsDisplay(true, true, true);
						}
						Insyght.INSYGHT_LOADING_MASK.hide();
					}
				});
			}
		};

		try {
			
			//MainTabPanelView.tabPanel.selectTab(1);


			String sessionIdToSend = "";
			if(Insyght.APP_CONTROLER.getCurrentUser() == null){
				sessionIdToSend = "";
			}else if(Insyght.APP_CONTROLER.getCurrentUser().getIdPerson() < 0){
				sessionIdToSend = "";
			}else{
				sessionIdToSend = Insyght.APP_CONTROLER.getCurrentUser().getSessionId();
			}

			homologieBrowsingResultService
					.getRefGenomePanelAndListOrgaResult(siSent, sessionIdToSend, callback);
			Insyght.INSYGHT_LOADING_MASK.show();
			Insyght.INSYGHT_LOADING_MASK.center();

		} catch (Exception e) {
			//String mssgError = "ERROR GetResultDialog GET_REF_GENOME_PANEL_AND_LIST_ORGA_RESULT : "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//e.printStackTrace();
			Insyght.INSYGHT_LOADING_MASK.hide();
			// hide();
		}


		
	}
	
	
	

}
