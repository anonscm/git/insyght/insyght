package fr.inra.jouy.client.view.result;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
//import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.PopupPanel.PositionCallback;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.CanvasGradient;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.canvas.dom.client.Context2d.LineCap;
import com.google.gwt.canvas.dom.client.Context2d.TextAlign;
import com.google.gwt.canvas.dom.client.Context2d.TextBaseline;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.i18n.client.NumberFormat;

import fr.inra.jouy.client.AppController;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneMatchItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSSyntItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.HolderAbsoluteProportionItem;
import fr.inra.jouy.shared.pojos.symbols.SuperHoldAbsoPropItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem.QSEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem.QEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem.SEnumBlockType;

import com.google.gwt.animation.client.*;

public class SyntenyCanvas extends Composite {

	private static SyntenyCanvasUiBinder uiBinder = GWT
			.create(SyntenyCanvasUiBinder.class);

	interface SyntenyCanvasUiBinder extends UiBinder<Widget, SyntenyCanvas> {
	}

	@UiField
	DeckPanel dpRootCanvas;
	@UiField
	DockLayoutPanel panelSupportCanvas;
	@UiField
	Button navigateLeftSpeed1;
	@UiField
	Button navigateLeftSpeed2;
	@UiField
	Button navigateLeftSpeed3;
	@UiField
	Button navigateRightSpeed1;
	@UiField
	Button navigateRightSpeed2;
	@UiField
	Button navigateRightSpeed3;

	Canvas canvasBgk;
	public Context2d contextBgk;
	final CssColor redrawColorWhite = CssColor.make("#fff");
	final CssColor redrawColorRed = CssColor.make("#FF0000");
	final CssColor redrawColorGreen = CssColor.make("green");
	final CssColor redrawColorDarkGreen = CssColor.make("#2F4F4F");
	final CssColor redrawColorBlack = CssColor.make("black"); //85% and up
	//final CssColor redrawColorLightGrey = CssColor.make("#8F8F8F");
	final CssColor redrawColorGrey4 = CssColor.make("#484848"); // 50 - 85
	//final CssColor redrawColorGrey50to37 = CssColor.make("#707070");
	final CssColor redrawColorGrey6 = CssColor.make("#696969"); // 35 - 50 // dark grey
	final CssColor redrawColorGrey9 = CssColor.make("#989898"); // 23-35%
	final CssColor redrawColorGreyC = CssColor.make("#C0C0C0"); //23% or less


	
	final CssColor redrawColorAzure3 = CssColor.make("#C1CDCD");
	
	// color synteny block
	// color for homolog block : #b3ffe5  (was FrostedPeach #FCF8DC)
	final CssColor redrawColorFrostedPeachForSyntenyHomologBlock = CssColor.make("#b3ffe5");
	final String colorFrostedPeachForSyntenyHomologBlockAsString = "#b3ffe5";
	// color q insertion : pale grey #EEEEEE (was OstrichFeather #FEF1E9)
	final CssColor redrawColorOstrichFeatherForSyntenyQInsertion = CssColor.make("#EEEEEE");
	final String colorOstrichFeatherForSyntenyQInsertionAsString = "#EEEEEE";
	// color s insertion : pale grey #EEEEEE (was FarHorizon #ECF1EF)
	final CssColor redrawColorFarHorizonForSyntenySInsertion = CssColor.make("#EEEEEE");
	final String colorFarHorizonForSyntenySInsertionAsString = "#EEEEEE";
	// color reverse block : #ffccb3  (was Green Veil #EEF3E2)
	final CssColor redrawColorGreenVeilForSyntenyReverseBlock = CssColor.make("#ffccb3");
	final String colorGreenVeilForSyntenyReverseBlockAsString = "#ffccb3";

	// color for gene bkg
	final String colorGene0JauneAsString = "#FFFF4A"; // was FFFF99
	final String colorGene1VertAsString = "#35FF35"; // was CCFFCC
	final String colorGene2OrangeAsString = "#FF933A"; // was FFCC66
	final String colorGene3BleuAsString = "#34FFFF"; // was CCFFFF
	final String colorGene4RoseAsString = "#D09CFF"; // was FFCCCC
	final String colorGeneDefaulGrisPaleAsString = "#EEEEEE"; //
	final CssColor colorGene0JauneAsCssColor = CssColor.make("#FFFF4A");
	final CssColor colorGene1VertAsCssColor = CssColor.make("#35FF35");
	final CssColor colorGene2OrangeAsCssColor = CssColor.make("#FF933A");
	final CssColor colorGene3BleuAsCssColor = CssColor.make("#34FFFF");
	final CssColor colorGene4RoseAsCssColor = CssColor.make("#D09CFF");
	final CssColor colorGeneDefaulGrisPaleAsCssColor = CssColor.make("#EEEEEE");

	// can be modify to increase or decrase canvas drawings sizes
	public static int DISPLAY_SIZE_MODIFIER_COLUMN = -7;

	// mouse positions relative to canvas
	int mouseX, mouseY;
	private static boolean MOUSE_DOWN = false;
	// private static boolean DRAGGING = false;
	private static boolean DRAGGING_RIGHT_NOW = false;
	int mouseXStartDragging, mouseYStartDragging;
	int mouseXStopDragging;
	private static final int SENSITIVITY_DRAGGING = 8;
	public boolean zoomQ = false;
	private boolean dezoom = false;

	// private static final double PERCENT_HEIGHT_DRAWING = 0.8;
	private static final double PERCENT_HEIGHT_TEXT = 0.7;
	// private static final double CORRECT_FOR_TEXT_CENTER = 0.35;
	private static final double PERCENT_HEIGHT_HOMOLOG_BLOCK = 0.4;
	private static final double PERCENT_WIDTH_HOMOLOG_BLOCK = 0.95;
	// private static final double PERCENT_HEIGHT_Q_INSERT_BLOCK = 0.9;
	// private static final double PERCENT_WIDTH_Q_INSERT_BLOCK = 1.1;
	// private static final double PERCENT_HEIGHT_S_INSERT_BLOCK = 0.9;
	private static final double PERCENT_WIDTH_S_INSERT_BLOCK = 1.1;
	// private static final int WAVE_EFFECT = 4;
	private static final int PADDING = 2;
	private static final double PERCENT_HEIGHT_SCAFFOLD_GENOME_TO_TOP = 0.28;
	private static final double PERCENT_WIDTH_TEXT_SCAFFOLD_GENOME_TO_LEFT = 0.23;
	// private static final double PERCENT_WIDTH_START_END_POINT = 0;

	private static final double RATIO_BEZIZE_CURVE_TO = 0.5522847498307933984022516322796; // 0.5522847498307933984022516322796
																							// if
																							// the
																							// Bezier
																							// is
																							// approximating
																							// an
																							// elliptic
																							// arc
																							// with
																							// best
																							// fitting
																							// //
																							// original
																							// :
																							// 2.0
																							// /
																							// 3.0

	public static boolean CALCULATE_CANVAS_PARAMETERS = true;
	public boolean setCoordinateSpace = true;

	public static int TOTAL_CANVAS_WIDTH = -1;
	public static int TOTAL_CANVAS_HEIGHT = -1;
	public static int TOTAL_CANVAS_HEIGHT_UPPER_PART = -1;
	public static int UNIT_WIDTH = -1;
	public static int SLICE_CANVAS_WIDTH = -1;
	// public static double numberSliceWidthAsDouble = -1;
	public static int NUMBER_SLICE_WIDTH = -1;
	public static int EXTRA_SPACE_AFTER_LAST_SLICE = 0;
	public int addToLeftToCenterDrawing = 0;
	public static int Y_TEXT = -1;
	public static int FONT_SIZE = -1;

	// homolog
	public static int X_RECT_HOMOLOG_BLOCK = -1;
	public static int Y_RECT_HOMOLOG_BLOCK = -1;
	public static int W_RECT_HOMOLOG_BLOCK = -1;
	public static int H_RECT_HOMOLOG_BLOCK = -1;

	// q_insertion
	public static int Q_INSERTION_X_POINT = -1;
	public static int Q_INSERTION_Y_POINT = -1;
	public static int Q_INSERTION_X_LBEZIER_LTC = -1;
	public static int Q_INSERTION_Y_LBEZIER_LTC = -1;
	public static int Q_INSERTION_LBEZIER_X_RTC = -1;
	public static int Q_INSERTION_LBEZIER_Y_RTC = -1;

	// s_insertion
	public static int S_INSERTION_X_POINT = -1;
	public static int S_INSERTION_Y_POINT = -1;
	public static int S_INSERTION_X_ARROW_LC = -1;
	public static int S_INSERTION_Y_ARROW_LC = -1;
	public static int S_INSERTION_X_ARROW_LMC = -1;
	public static int S_INSERTION_Y_ARROW_LMC = -1;
	public static int S_INSERTION_X_LBEZIER_cp1x = -1;
	public static int S_INSERTION_Y_LBEZIER_cp1y = -1;
	public static int S_INSERTION_X_LBEZIER_cp2x = -1;
	public static int S_INSERTION_Y_LBEZIER_cp2y = -1;
	public static int S_INSERTION_X_LBEZIER_LTC = -1;
	public static int S_INSERTION_Y_LBEZIER_LTC = -1;
	public static int S_INSERTION_LBEZIER_X_RTC = -1;
	public static int S_INSERTION_LBEZIER_Y_RTC = -1;
	public static int S_INSERTION_X_ARROW_RC = -1;
	// public static int S_INSERTION_Y_ARROW_RC = -1;

	// REVERSE BLOCK
	public static int REVERSE_X_TLC = -1;
	public static int REVERSE_Y_TLC = -1;
	public static int REVERSE_X_1_cp1x = -1;
	public static int REVERSE_Y_1_cp1y = -1;
	public static int REVERSE_X_1_cp2x = -1;
	public static int REVERSE_Y_1_cp2y = -1;
	public static int REVERSE_X_BRC = -1;
	public static int REVERSE_Y_BRC = -1;
	public static int REVERSE_X_TRC = -1;
	public static int REVERSE_Y_TRC = -1;
	public static int REVERSE_X_2_cp1x = -1;
	public static int REVERSE_Y_2_cp1y = -1;
	public static int REVERSE_X_2_cp2x = -1;
	public static int REVERSE_Y_2_cp2y = -1;
	public static int REVERSE_X_BLC = -1;
	public static int REVERSE_Y_BLC = -1;

	// bottom Scaffold
	public static int Y_AXIS_TO_DRAW_AT_BOTTOM = -1;
	public static int Y_AXIS_TO_DRAW_AT_TOP = -1;
	public static int SCAFFOLD_X_START_POINT = -1;
	public static int SCAFFOLD_X_STOP_POINT = -1;
	public static int TOTAL_LENGHT_SCAFFOLD = -1;
	public static int WIDTH_SIZE_PB_TEXT = -1;
	public static int SCAFFOLD_X_START_GENOME_TEXT = -1;

	public static boolean LOCK_DOING_ANIMATION = false;
	public static int ANI_TIME = 500; // 500ms time for animation

	public static boolean SYNCHRONIZE_Q_ZOOMING = false;
	public static boolean SYNCHRONIZE_NAVIGATION = false;

	public static int LIMIT_PROPORTIONATE_DRAWING_LOWER_QUALITY = 10000;
	public static int LIMIT_WIDTH_PROPORTIONATE_BKG_PX = 5;
	// public static int LIMIT_PROPORTIONATE_DRAWING_TOO_MUCH = 2000;

	// user selected
	// public int indexStartBirdSynteny = -1;
	public int selectedSliceX = -1;
	public boolean selectedQ = false;
	// public boolean selectedMainQS = false;
	public boolean selectedPreviousSOfNextSlice = false;
	public boolean selectedNextSOfPreviousSlice = false;

	// associated genomePanel
	private GenomePanel associatedGenomePanel = null;

	private ResultLoadingDialog RLD = new ResultLoadingDialog();
	private ResuLoadMoreAbsoPropItemDial RLMAPID = new ResuLoadMoreAbsoPropItemDial();
	
	// getters and setters

	public GenomePanel getAssociatedGenomePanel() {
		return associatedGenomePanel;
	}

	public void setAssociatedGenomePanel(GenomePanel associatedGenomePanel) {
		this.associatedGenomePanel = associatedGenomePanel;
	}

	// ui handlers
	@UiHandler("navigateLeftSpeed1")
	void onNavigateLeftSpeed1Click(ClickEvent e) {

		if (!LOCK_DOING_ANIMATION
				&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
			LOCK_DOING_ANIMATION = true;
			final boolean synchNavIT = SYNCHRONIZE_NAVIGATION;
			
			//try{
			Insyght.APP_CONTROLER.clearAllUserSelection();
			// select genomePanel
			associatedGenomePanel.selectThisGenomePanel();
//			}catch(Exception exc){
//				//return if bug
//				return;
//			}
			Animation anim = new Animation() {
				@Override
				protected void onUpdate(double progress) {
					if (Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght()
							.compareTo(EnumResultViewTypes.homolog_table) == 0
							|| synchNavIT) {
						double progressToPassAlong = progress;
						for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
								.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
							GenomePanel gpIT = i.next();
							if (gpIT.isVisible()) {
								gpIT.getSyntenyCanvas().drawViewToCanvas(
										progressToPassAlong,
										SLICE_CANVAS_WIDTH, true, false, false,
										true);
							}
						}
					} else {
						// just do this gp
						drawViewToCanvas(progress, SLICE_CANVAS_WIDTH, true,
								false, false, true);
					}
				}

				@Override
				protected void onComplete() {
					completeNavigation(-1, synchNavIT);
				}
			};
			// run animation for 500 ms
			anim.run(ANI_TIME);
		}
	}

	protected void completeNavigation(int slicesToMove, boolean synchNavIT) {
		
		boolean isHomologTableView = false;
		int newIdxForAll = -1;
		if(Insyght.APP_CONTROLER
				.getCurrentRefGenomePanelAndListOrgaResult()
				.getViewTypeInsyght()
				.compareTo(EnumResultViewTypes.homolog_table) == 0){
			isHomologTableView = true;
		}
		
		if (isHomologTableView || synchNavIT) {

			for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
					.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
				GenomePanel gpIT = i.next();
				if (gpIT.isVisible()) {
					gpIT.getSyntenyCanvas()
							.highlightPropRepreForAllDisplayedSlices(
									false, true);
					if(isHomologTableView && newIdxForAll < 0 && !gpIT.isReferenceGenome()){
						newIdxForAll = gpIT.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + slicesToMove;
					}
				}
			}

			// do all featured
			for (int i = 0; i < Insyght.APP_CONTROLER
					.getCurrentListFeaturedResultGenomePanelItem()
					.size(); i++) {
				GenomePanelItem gpiIT = Insyght.APP_CONTROLER
						.getCurrentListFeaturedResultGenomePanelItem()
						.get(i);
				if (gpiIT
						.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0) {
					gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}
				//int newIdx = ;
				// if(newIdx < 0){
				// newIdx = 0;
				// }
				if(isHomologTableView){
					gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(newIdxForAll,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}else{
					gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(gpiIT.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + slicesToMove,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}
				gpiIT.setLookUpForGeneWithQStart(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gpiIT.setLookUpForGeneWithQStop(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gpiIT.setLookUpForGeneOnQElementId(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}
			// do all public
			for (int i = 0; i < Insyght.APP_CONTROLER
					.getCurrentListPublicResultGenomePanelItem()
					.size(); i++) {
				GenomePanelItem gpiIT = Insyght.APP_CONTROLER
						.getCurrentListPublicResultGenomePanelItem()
						.get(i);
				if (gpiIT
						.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0) {
					gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}
				//int newIdx = gpiIT.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1;
				// if(newIdx < 0){
				// newIdx = 0;
				// }
				if(isHomologTableView){
					gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(newIdxForAll,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}else{
					gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(gpiIT.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + slicesToMove,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}
				gpiIT.setLookUpForGeneWithQStart(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gpiIT.setLookUpForGeneWithQStop(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gpiIT.setLookUpForGeneOnQElementId(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}
			// do all private
			for (int i = 0; i < Insyght.APP_CONTROLER
					.getCurrentListPrivateResultGenomePanelItem()
					.size(); i++) {
				GenomePanelItem gpiIT = Insyght.APP_CONTROLER
						.getCurrentListPrivateResultGenomePanelItem()
						.get(i);
				if (gpiIT
						.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0) {
					gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}
				//int newIdx = gpiIT.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1;
				// if(newIdx < 0){
				// newIdx = 0;
				// }
				if(isHomologTableView){
					gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(newIdxForAll,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}else{
					gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(gpiIT.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + slicesToMove,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}
				gpiIT.setLookUpForGeneWithQStart(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gpiIT.setLookUpForGeneWithQStop(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				gpiIT.setLookUpForGeneOnQElementId(-1,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}
			// do ref genome
			if (Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem()
					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0) {
				Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getReferenceGenomePanelItem()
						.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(0,
								Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}
			//int newIdxRef = Insyght.APP_CONTROLER
			//		.getCurrentRefGenomePanelAndListOrgaResult()
			//		.getReferenceGenomePanelItem()
			//		.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1;
			// if(newIdxRef < 0){
			// newIdxRef = 0;
			// }
			if(isHomologTableView){
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(newIdxForAll,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}else{
				Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + slicesToMove,
						Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}
			//Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem()
			//		.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(newIdxRef);
			Insyght.APP_CONTROLER
			.getCurrentRefGenomePanelAndListOrgaResult()
			.getReferenceGenomePanelItem().setLookUpForGeneWithQStart(-1,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			Insyght.APP_CONTROLER
			.getCurrentRefGenomePanelAndListOrgaResult()
			.getReferenceGenomePanelItem().setLookUpForGeneWithQStop(-1,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			Insyght.APP_CONTROLER
			.getCurrentRefGenomePanelAndListOrgaResult()
			.getReferenceGenomePanelItem().setLookUpForGeneOnQElementId(-1,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());

			for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
					.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
				GenomePanel gpIT = i.next();
				if (gpIT.isVisible()) {
					gpIT.getSyntenyCanvas().drawViewToCanvas(1.0,
							0, true, true, false, true);
				}
			}
		} else {
			// just do this gp
			highlightPropRepreForAllDisplayedSlices(false, true);
			int newIdx = associatedGenomePanel
					.getGenomePanelItem()
					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + slicesToMove;
			// if(newIdx < 0){
			// newIdx = 0;
			// }
			associatedGenomePanel.getGenomePanelItem()
					.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
							newIdx,
							Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			associatedGenomePanel.getGenomePanelItem().setLookUpForGeneWithQStart(-1,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			associatedGenomePanel.getGenomePanelItem().setLookUpForGeneWithQStop(-1,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			associatedGenomePanel.getGenomePanelItem().setLookUpForGeneOnQElementId(-1,
					Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			
			// sometimes progress don't go up to 1...
			drawViewToCanvas(1.0, 0, true, true, false, true);
		}

		LOCK_DOING_ANIMATION = false;
	}

	@UiHandler("navigateLeftSpeed2")
	void onNavigateLeftSpeed2Click(ClickEvent e) {

		if (!LOCK_DOING_ANIMATION
				&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
			LOCK_DOING_ANIMATION = true;
			final boolean synchNavIT = SYNCHRONIZE_NAVIGATION;
			
			//try{
			Insyght.APP_CONTROLER.clearAllUserSelection();
			// select genomePanel
			associatedGenomePanel.selectThisGenomePanel();
//			}catch(Exception exc){
//				//return if bug
//				return;
//			}
			
			Animation anim = new Animation() {

				@Override
				protected void onUpdate(double progress) {
					if (Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght()
							.compareTo(EnumResultViewTypes.homolog_table) == 0
							|| synchNavIT) {
						double progressToPassAlong = progress;
						for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
								.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
							GenomePanel gpIT = i.next();
							if (gpIT.isVisible()) {
								gpIT.getSyntenyCanvas()
										.drawViewToCanvas(
												progressToPassAlong,
												(TOTAL_CANVAS_WIDTH / 2)
														+ (EXTRA_SPACE_AFTER_LAST_SLICE / 3),
												true, false, false, true);
							}
						}
					} else {
						// just do this gp
						drawViewToCanvas(progress, (TOTAL_CANVAS_WIDTH / 2)
								+ (EXTRA_SPACE_AFTER_LAST_SLICE / 3), true,
								false, false, true);
					}
				}

				@Override
				protected void onComplete() {
					//TODO TEST
					int slicesToMove = (int) (Math.floor((double) NUMBER_SLICE_WIDTH / 2)) + 1;//
					completeNavigation(-slicesToMove, synchNavIT);
					
				}
			};
			// run animation for 500 ms
			anim.run(ANI_TIME);
		}
	}

	@UiHandler("navigateLeftSpeed3")
	void onNavigateLeftSpeed3Click(ClickEvent e) {

		if (!LOCK_DOING_ANIMATION
				&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
			LOCK_DOING_ANIMATION = true;
			final boolean synchNavIT = SYNCHRONIZE_NAVIGATION;
			
			//try{
			Insyght.APP_CONTROLER.clearAllUserSelection();
			// select genomePanel
			associatedGenomePanel.selectThisGenomePanel();
//			}catch(Exception exc){
//				//return if bug
//				return;
//			}
			
			Animation anim = new Animation() {

				@Override
				protected void onUpdate(double progress) {
					if (Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght()
							.compareTo(EnumResultViewTypes.homolog_table) == 0
							|| synchNavIT) {
						double progressToPassAlong = progress;
						for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
								.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
							GenomePanel gpIT = i.next();
							if (gpIT.isVisible()) {
								gpIT.getSyntenyCanvas()
										.drawViewToCanvas(
												progressToPassAlong,
												TOTAL_CANVAS_WIDTH
														- (addToLeftToCenterDrawing * 2)
														- (EXTRA_SPACE_AFTER_LAST_SLICE),
												true, false, false, true);
							}
						}
					} else {
						// just do this gp
						drawViewToCanvas(progress, TOTAL_CANVAS_WIDTH
								- (addToLeftToCenterDrawing * 2)
								- (EXTRA_SPACE_AFTER_LAST_SLICE), true, false,
								false, true);
					}
				}

				@Override
				protected void onComplete() {

					int slicesToMove = NUMBER_SLICE_WIDTH;
					completeNavigation(-slicesToMove, synchNavIT);
					
				}
			};
			// run animation for 500 ms
			anim.run(ANI_TIME);
		}
	}

	@UiHandler("navigateRightSpeed1")
	void onNavigateRightSpeed1Click(ClickEvent e) {

		if (!LOCK_DOING_ANIMATION
				&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
			LOCK_DOING_ANIMATION = true;
			final boolean synchNavIT = SYNCHRONIZE_NAVIGATION;

			//try{
			Insyght.APP_CONTROLER.clearAllUserSelection();
			// select genomePanel
			associatedGenomePanel.selectThisGenomePanel();
//			}catch(Exception exc){
////				//return if bug
////				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, exc);
////				return;
//			}
			
			Animation anim = new Animation() {
				@Override
				protected void onUpdate(double progress) {
					if (Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght()
							.compareTo(EnumResultViewTypes.homolog_table) == 0
							|| synchNavIT) {
						double progressToPassAlong = progress;
						for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
								.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
							GenomePanel gpIT = i.next();
							if (gpIT.isVisible()) {
								gpIT.getSyntenyCanvas().drawViewToCanvas(
										-progressToPassAlong,
										SLICE_CANVAS_WIDTH, true, false, false,
										true);
							}
						}
					} else {
						// just do this gp
						drawViewToCanvas(-progress, SLICE_CANVAS_WIDTH, true,
								false, false, true);
					}
				}

				@Override
				protected void onComplete() {
					
					int slicesToMove = 1;
					completeNavigation(slicesToMove, synchNavIT);
					
				}
			};
			// run animation for 500 ms
			anim.run(ANI_TIME);
		}
	}

	@UiHandler("navigateRightSpeed2")
	void onNavigateRightSpeed2Click(ClickEvent e) {

		if (!LOCK_DOING_ANIMATION
				&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
			LOCK_DOING_ANIMATION = true;
			final boolean synchNavIT = SYNCHRONIZE_NAVIGATION;

			//try{
			Insyght.APP_CONTROLER.clearAllUserSelection();
			// select genomePanel
			associatedGenomePanel.selectThisGenomePanel();
//			}catch(Exception exc){
//				//return if bug
//				return;
//			}
			
			Animation anim = new Animation() {
				@Override
				protected void onUpdate(double progress) {
					if (Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght()
							.compareTo(EnumResultViewTypes.homolog_table) == 0
							|| synchNavIT) {
						double progressToPassAlong = progress;
						for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
								.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
							GenomePanel gpIT = i.next();
							if (gpIT.isVisible()) {
								gpIT.getSyntenyCanvas()
										.drawViewToCanvas(
												-progressToPassAlong,
												(TOTAL_CANVAS_WIDTH / 2)
														+ (EXTRA_SPACE_AFTER_LAST_SLICE / 3),
												true, false, false, true);
							}
						}
					} else {
						// just do this gp
						drawViewToCanvas(-progress, (TOTAL_CANVAS_WIDTH / 2)
								+ (EXTRA_SPACE_AFTER_LAST_SLICE / 3), true,
								false, false, true);
					}
				}

				@Override
				protected void onComplete() {
					//TODO TEST
					int slicesToMove = (int) (Math.floor((double) NUMBER_SLICE_WIDTH / 2)) + 1;//;
					completeNavigation(slicesToMove, synchNavIT);
					
				}
			};
			// run animation for 500 ms
			anim.run(ANI_TIME);
		}
	}

	@UiHandler("navigateRightSpeed3")
	void onNavigateRightSpeed3Click(ClickEvent e) {

		if (!LOCK_DOING_ANIMATION
				&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
			LOCK_DOING_ANIMATION = true;
			final boolean synchNavIT = SYNCHRONIZE_NAVIGATION;

			//try{
			Insyght.APP_CONTROLER.clearAllUserSelection();
			// select genomePanel
			associatedGenomePanel.selectThisGenomePanel();
//			}catch(Exception exc){
//				//return if bug
//				return;
//			}
			
			Animation anim = new Animation() {
				@Override
				protected void onUpdate(double progress) {
					if (Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght()
							.compareTo(EnumResultViewTypes.homolog_table) == 0
							|| synchNavIT) {
						double progressToPassAlong = progress;
						for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
								.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
							GenomePanel gpIT = i.next();
							if (gpIT.isVisible()) {
								gpIT.getSyntenyCanvas()
										.drawViewToCanvas(
												-progressToPassAlong,
												TOTAL_CANVAS_WIDTH
														- (addToLeftToCenterDrawing * 2)
														- (EXTRA_SPACE_AFTER_LAST_SLICE),
												true, false, false, true);
							}//
						}
					} else {
						// just do this gp
						drawViewToCanvas(-progress, TOTAL_CANVAS_WIDTH
								- (addToLeftToCenterDrawing * 2)
								- (EXTRA_SPACE_AFTER_LAST_SLICE), true, false,
								false, true);
					}
				}

				@Override
				protected void onComplete() {

					int slicesToMove = NUMBER_SLICE_WIDTH;
					completeNavigation(slicesToMove, synchNavIT);
					
				}
			};
			// run animation for 500 ms
			anim.run(ANI_TIME);
		}
	}

	public SyntenyCanvas() {// GenomePanel associatedGenomePanelSent
		initWidget(uiBinder.createAndBindUi(this));

		dpRootCanvas.showWidget(0);

		canvasBgk = Canvas.createIfSupported();
		if (canvasBgk == null) {
			dpRootCanvas.showWidget(2);
			return;
		} else {
			navigateLeftSpeed1.setText("<");
			navigateLeftSpeed2.setText("<<");
			navigateLeftSpeed3.setText("<<<");
			navigateRightSpeed1.setText(">");
			navigateRightSpeed2.setText(">>");
			navigateRightSpeed3.setText(">>>");


			canvasBgk.setStylePrimaryName("gwt-Canvas");
			canvasBgk.setWidth("99%");
			// canvas.setHeight("96%");
			canvasBgk.setHeight(GenomePanel.MAX_PX_scLowerSize + "px");
			contextBgk = canvasBgk.getContext2d();
			// set default font
			panelSupportCanvas.add(canvasBgk);
			// System.out.println("dpRootCanvas.showWidget(3)");
			dpRootCanvas.showWidget(3);
		}

		// init handlers
		initHandlers();


	}

	void initHandlers() {

		canvasBgk.addMouseMoveHandler(new MouseMoveHandler() {
			public void onMouseMove(MouseMoveEvent event) {
				mouseX = event.getRelativeX(canvasBgk.getElement());
				mouseY = event.getRelativeY(canvasBgk.getElement());

				if (!LOCK_DOING_ANIMATION
						&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
					if (!DRAGGING_RIGHT_NOW
							&& MOUSE_DOWN
							&& mouseY > TOTAL_CANVAS_HEIGHT_UPPER_PART
							&& Math.abs(mouseX - mouseXStartDragging) > SENSITIVITY_DRAGGING) {
						// System.out.println("dragging : x = "+mouseX+" ; y = "+mouseY);
						// Start dragging
						DRAGGING_RIGHT_NOW = true;
						// Insyght.APP_CONTROLER.clearUserSelection();
						clearMarkerBottom(true, true, true);
					}

					if (DRAGGING_RIGHT_NOW) {
						if (mouseY < TOTAL_CANVAS_HEIGHT_UPPER_PART) {
							// break dragging
							breakDragging(false);
						} else {
							// drawZoomPosition(1);
							// protected void drawZoomPosition(boolean
							// duringAnimation, double progress, boolean
							// dragging, boolean dezoomSent, boolean Q, int
							// mouseXPixelStart, int mouseXPixelStop, boolean
							// addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
							if ((Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght()
									.compareTo(
											EnumResultViewTypes.homolog_table) == 0 || SYNCHRONIZE_Q_ZOOMING)
									&& zoomQ) {
								int currMouseZoomPos = -1;
								if (mouseX < SCAFFOLD_X_START_POINT) {
									currMouseZoomPos = SCAFFOLD_X_START_POINT;
								} else if (mouseX > SCAFFOLD_X_STOP_POINT) {
									currMouseZoomPos = SCAFFOLD_X_STOP_POINT;
								} else {
									currMouseZoomPos = mouseX;
								}
								int startZoomX = -1;
								int stopZoomX = -1;
								if (currMouseZoomPos > mouseXStartDragging) {
									startZoomX = mouseXStartDragging;
									stopZoomX = currMouseZoomPos;
								} else {
									startZoomX = currMouseZoomPos;
									stopZoomX = mouseXStartDragging;
								}
								for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
										.getLIST_GENOME_PANEL().iterator(); i
										.hasNext();) {
									GenomePanel gpIT = i.next();
									if (gpIT.isVisible()) {
										gpIT.getSyntenyCanvas()
												.drawZoomPosition(false, 1,
														true, false, true,
														startZoomX, stopZoomX,
														-1, -1, true, "");
									}
								}
							} else {
								int currMouseZoomPos = -1;
								if (mouseX < SCAFFOLD_X_START_POINT) {
									currMouseZoomPos = SCAFFOLD_X_START_POINT;
								} else if (mouseX > SCAFFOLD_X_STOP_POINT) {
									currMouseZoomPos = SCAFFOLD_X_STOP_POINT;
								} else {
									currMouseZoomPos = mouseX;
								}
								int startZoomX = -1;
								int stopZoomX = -1;
								if (currMouseZoomPos > mouseXStartDragging) {
									startZoomX = mouseXStartDragging;
									stopZoomX = currMouseZoomPos;
								} else {
									startZoomX = currMouseZoomPos;
									stopZoomX = mouseXStartDragging;
								}
								// just do this gp
								// drawZoomPosition(false, 1, true, false,
								// zoomQ, -1, -1, true, "");
								drawZoomPosition(false, 1, true, false, zoomQ,
										startZoomX, stopZoomX, -1, -1, true, "");
							}

						}
					}
				}

			}
		});

		canvasBgk.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				mouseX = -200;
				mouseY = -200;
				if (DRAGGING_RIGHT_NOW) {
					breakDragging(false);
				}

			}
		});

		canvasBgk.addClickHandler(new ClickHandler() {

			// int mouseYLastClick = -1;
			// int mouseXLastClick = -1;

			public void onClick(ClickEvent event) {

				if (!LOCK_DOING_ANIMATION
						&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {

					// if(mouseY == mouseYLastClick && mouseX ==
					// mouseXLastClick){
					// //System.err.println("double click");
					// //double click
					// mouseYLastClick = -1;
					// mouseXLastClick = -1;
					// }else{
					// //System.err.println("click");
					// //simple click
					// mouseYLastClick = mouseY;
					// mouseXLastClick = mouseX;

					// no need done on mouse down
					// Insyght.APP_CONTROLER.clearAllUserSelection();
					// select genomePanel
					// associatedGenomePanel.selectThisGenomePanel();

					// System.out.println("check for click!!!");

					if (mouseY < TOTAL_CANVAS_HEIGHT_UPPER_PART) {
						// clickedUpperPartCanvas
						if (mouseX > addToLeftToCenterDrawing
								&& mouseX < (TOTAL_CANVAS_WIDTH - addToLeftToCenterDrawing)) {
							int sliceXClicked = (int) Math
									.floor((mouseX - addToLeftToCenterDrawing)
											/ SLICE_CANVAS_WIDTH);

							boolean clickedRightSideOfSlice = false;
							if (mouseX > ((SLICE_CANVAS_WIDTH * sliceXClicked) + (SLICE_CANVAS_WIDTH / 2))) {
								clickedRightSideOfSlice = true;
							}
							// System.err.println("clickedRightSide ? : "+clickedRightSideOfSlice);
							boolean clickedSSymbolInUpperPart = false;
							if (mouseY > (TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)) {
								clickedSSymbolInUpperPart = true;
							}
							// System.err.println("clickedSSymbolInUpperPart ? : "+clickedSSymbolInUpperPart);

							if ((associatedGenomePanel
									.getGenomePanelItem()
									.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + sliceXClicked) < associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.size()) {

								int idxIT = associatedGenomePanel
										.getGenomePanelItem()
										.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
												associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
														+ sliceXClicked);

								HolderAbsoluteProportionItem hapiITHere = associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(associatedGenomePanel
												.getGenomePanelItem()
												.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
												+ sliceXClicked).getListHAPI()
										.get(idxIT
										// associatedGenomePanel.getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+sliceXClicked)
										);
								if (hapiITHere
										.getAbsoluteProportionQComparableQSSpanItem() != null) {

									if (hapiITHere
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
										// selected GeneHomologyItem
										// check if gene name up clicked ?
										if (mouseY < Y_RECT_HOMOLOG_BLOCK) {
											// gene up clicked
											selectSymbol(sliceXClicked,
													true, true, -1, false,
													// true, false, false,
													false, false, false, false,
													false);
											// selectBlockSynteny(int
											// sliceXClicked, boolean
											// redrawOnTopSent, boolean
											// inRedSent, int
											// newIdxInArrayOfThisSlice, boolean
											// clearSentSliceIdxEntirely,
											// boolean
											// geneHomologyGeneNameUpClicked,
											// boolean
											// geneHomologyGeneNameDownClicked,
											// boolean
											// geneHomologyMatchZoneClicked) {
										} else if (mouseY < (Y_RECT_HOMOLOG_BLOCK + (S_INSERTION_Y_POINT - Y_RECT_HOMOLOG_BLOCK))) {
											// gene match clicked
											selectSymbol(sliceXClicked,
													true, true, -1, false,
													// false, false, true,
													false, false, false, false,
													false);
											// selectBlockSynteny(int
											// sliceXClicked, boolean
											// redrawOnTopSent, boolean
											// inRedSent, int
											// newIdxInArrayOfThisSlice, boolean
											// clearSentSliceIdxEntirely,
											// boolean
											// geneHomologyGeneNameUpClicked,
											// boolean
											// geneHomologyGeneNameDownClicked,
											// boolean
											// geneHomologyMatchZoneClicked) {
										} else if (mouseY < S_INSERTION_Y_ARROW_LMC) {
											// gene down clicked
											selectSymbol(sliceXClicked,
													true, true, -1, false,
													// false, true, false,
													false, false, false, false,
													false);

										} else {
											// clicked other match ?
											if (associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.get(associatedGenomePanel
															.getGenomePanelItem()
															.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
															+ sliceXClicked)
													.getListHAPI().size() > 1) {

												idxIT++;
												if (idxIT > associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.get(associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																+ sliceXClicked)
														.getListHAPI().size() - 1) {
													idxIT = 0;
												}
												selectSymbol(
														sliceXClicked, false,
														true, idxIT, true,
														// false, true, false,
														false, false, false,
														false, false);
												// Window.alert("clicked other match");
											}

										}
									} else if (hapiITHere
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										if (mouseY < (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK)) {
											selectSymbol(sliceXClicked,
													true, true, -1, false,
													false, false, false, false,
													false);
										} else {
											// clicked other match ?

											if (associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.get(associatedGenomePanel
															.getGenomePanelItem()
															.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
															+ sliceXClicked)
													.getListHAPI().size() > 1
													&& !((AbsoPropQSSyntItem) hapiITHere
															.getAbsoluteProportionQComparableQSSpanItem())
															.getQsEnumBlockType()
															.toString()
															.startsWith(
																	"QS_CUT_")) {

												idxIT++;
												if (idxIT > associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.get(associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																+ sliceXClicked)
														.getListHAPI().size() - 1) {
													idxIT = 0;
												}
												selectSymbol(
														sliceXClicked, false,
														true, idxIT, true,
														false, false, false,
														true, false);
												// Window.alert("clicked other match");
											}
										}
									} else {
										selectSymbol(sliceXClicked, true,
												true, -1, false, false, false,
												false, false, false);
									}

								} else {
									if (clickedSSymbolInUpperPart
											&& clickedRightSideOfSlice) {
										selectSymbol(sliceXClicked, true,
												true, -1, false,
												clickedSSymbolInUpperPart,
												true, false, false, false);

									} else if (clickedSSymbolInUpperPart
											&& !clickedRightSideOfSlice) {
										selectSymbol(sliceXClicked, true,
												true, -1, false,
												clickedSSymbolInUpperPart,
												false, true, false, false);
									} else {
										// TODO missing target flip trough
										// SMissingTargetQ
										// yStartPoint =
										// S_INSERTION_LBEZIER_Y_RTC;
										// yStopPoint =
										// S_INSERTION_Y_POINT+((S_INSERTION_LBEZIER_Y_RTC-S_INSERTION_Y_POINT)/2);
										// QMissingTargetS
										// yStartPoint =
										// Q_INSERTION_LBEZIER_Y_RTC;
										// yStopPoint =
										// Q_INSERTION_Y_POINT-((Q_INSERTION_Y_POINT-Q_INSERTION_LBEZIER_Y_RTC)/2

										selectSymbol(sliceXClicked, true,
												true, -1, false,
												clickedSSymbolInUpperPart,
												false, false, false, false);
									}
								}
							}
						}
					} else {
						// clicked lower part canvas
						// check if clicked on a marker
						restoreToOriginCoordinateSpace();
						contextBgk.translate(0, TOTAL_CANVAS_HEIGHT_UPPER_PART);
						// boolean foundSelectedSynteny = false;
						ADDCLICKHANDLERCHECKMARKERMAINLOOP: for (int i = 0; i < NUMBER_SLICE_WIDTH; i++) {
							if (associatedGenomePanel
									.getGenomePanelItem()
									.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
									+ i >= associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.size()) {
								break;
							}

							// for(int
							// j=0;j<associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+i).getListHAPI().size();j++){
							// HolderAbsoluteProportionItem siIT =
							// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexStartBirdSynteny+i).get(associatedGenomePanel.getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(indexStartBirdSynteny+i));
							HolderAbsoluteProportionItem siIT = associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
											+ i)
									.getListHAPI()
									.get(associatedGenomePanel
											.getGenomePanelItem()
											.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
													associatedGenomePanel
															.getGenomePanelItem()
															.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
															+ i));

							if (siIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
								// if(siIT.getAbsoluteProportionQComparableQSSpanItem()
								// instanceof
								// AbsoluteProportionQSGeneHomologyItem){
								for (int j = 0; j < associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(associatedGenomePanel
												.getGenomePanelItem()
												.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
												+ i).getListHAPI().size(); j++) {
									HolderAbsoluteProportionItem siITBis = associatedGenomePanel
											.getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
											.get(associatedGenomePanel
													.getGenomePanelItem()
													.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
													+ i).getListHAPI().get(j);
									if (siITBis != null) {
										if (siITBis
												.getAbsoluteProportionQComparableQSSpanItem() != null) {
											double qPercentStartIT = claculateRelativePercentage(
													Insyght.APP_CONTROLER
															.getCurrentRefGenomePanelAndListOrgaResult()
															.getReferenceGenomePanelItem()
															.getListAbsoluteProportionElementItemQ()
															.get(0)
															.getQsizeOfOragnismInPb(),
													siITBis.getAbsoluteProportionQComparableQSSpanItem()
															.getqPercentStart(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopQGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartQGenome());
											double qPercentStopIT = claculateRelativePercentage(
													Insyght.APP_CONTROLER
															.getCurrentRefGenomePanelAndListOrgaResult()
															.getReferenceGenomePanelItem()
															.getListAbsoluteProportionElementItemQ()
															.get(0)
															.getQsizeOfOragnismInPb(),
													siITBis.getAbsoluteProportionQComparableQSSpanItem()
															.getqPercentStop(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopQGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartQGenome());
											double sPercentStartIT = claculateRelativePercentage(
													associatedGenomePanel
															.getGenomePanelItem()
															.getListAbsoluteProportionElementItemS()
															.get(0)
															.getsSizeOfOragnismInPb(),
													siITBis.getAbsoluteProportionQComparableQSSpanItem()
															.getsPercentStart(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopSGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartSGenome());
											double sPercentStopIT = claculateRelativePercentage(
													associatedGenomePanel
															.getGenomePanelItem()
															.getListAbsoluteProportionElementItemS()
															.get(0)
															.getsSizeOfOragnismInPb(),
													siITBis.getAbsoluteProportionQComparableQSSpanItem()
															.getsPercentStop(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopSGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartSGenome());

											if (qPercentStartIT >= 0
													&& qPercentStartIT <= 1
													&& qPercentStopIT >= 0
													&& qPercentStopIT <= 1
													&& sPercentStartIT >= 0
													&& sPercentStartIT <= 1
													&& sPercentStopIT >= 0
													&& sPercentStopIT <= 1) {
												int fromCheckingIsInPath = checkIfSyntenyMarkerScaffoldIsInPath(
														true, true,
														qPercentStartIT,
														qPercentStopIT,
														sPercentStartIT,
														sPercentStopIT);

												if (fromCheckingIsInPath == 1) {
													// true for q
													if (j != associatedGenomePanel
															.getGenomePanelItem()
															.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																	associatedGenomePanel
																			.getGenomePanelItem()
																			.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																			+ i)) {
														selectSymbol(i,
																false, true,
																j,
																true,
																// true,false,
																// false,
																false, false,
																false, false,
																false);
													} else {
														selectSymbol(i,
																true, true,
																-1,
																false,
																// true,false,
																// false,
																false, false,
																false, false,
																false);
													}

													break ADDCLICKHANDLERCHECKMARKERMAINLOOP;
												} else if (fromCheckingIsInPath == -1) {
													// true for s
													if (j != associatedGenomePanel
															.getGenomePanelItem()
															.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																	associatedGenomePanel
																			.getGenomePanelItem()
																			.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																			+ i)) {
														// new
														// newIdxInArrayOfThisSlice
														// System.err.println("new newIdxInArrayOfThisSlice");
														selectSymbol(i,
																false, true,
																j,
																true,
																// false,true,false,
																false, false,
																false, false,
																false);
													} else {
														// same
														// IdxInArrayOfThisSlice
														selectSymbol(i,
																true, true,
																-1,
																false,
																// false,true,
																// false,
																false, false,
																false, false,
																false);
														// selectBlockSynteny(int
														// sliceXClicked,
														// boolean
														// redrawOnTopSent,
														// boolean inRedSent,
														// int
														// newIdxInArrayOfThisSlice,
														// boolean
														// clearSentSliceIdxEntirely,
														// boolean
														// geneHomologyGeneNameUpClicked,
														// boolean
														// geneHomologyGeneNameDownClicked,
														// boolean
														// geneHomologyMatchZoneClicked)
														// {
													}
													break ADDCLICKHANDLERCHECKMARKERMAINLOOP;
												}
											}
										}
									}
								}

							} else {
								if (siIT.getAbsoluteProportionQComparableQSpanItem() != null) {

									double qPercentStartIT = claculateRelativePercentage(
											Insyght.APP_CONTROLER
													.getCurrentRefGenomePanelAndListOrgaResult()
													.getReferenceGenomePanelItem()
													.getListAbsoluteProportionElementItemQ()
													.get(0)
													.getQsizeOfOragnismInPb(),
											siIT.getAbsoluteProportionQComparableQSpanItem()
													.getqPercentStart(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStopQGenome(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStartQGenome());
									double qPercentStopIT = claculateRelativePercentage(
											Insyght.APP_CONTROLER
													.getCurrentRefGenomePanelAndListOrgaResult()
													.getReferenceGenomePanelItem()
													.getListAbsoluteProportionElementItemQ()
													.get(0)
													.getQsizeOfOragnismInPb(),
											siIT.getAbsoluteProportionQComparableQSpanItem()
													.getqPercentStop(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStopQGenome(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStartQGenome());

									int fromCheckingIsInPathIT = checkIfSyntenyMarkerScaffoldIsInPath(
											true, false, qPercentStartIT,
											qPercentStopIT, -1, -1);

									if (fromCheckingIsInPathIT == 1) {
										// clicked q part
										selectSymbol(i, true, true, -1,
												false, false, false, false,
												false, false);

										break ADDCLICKHANDLERCHECKMARKERMAINLOOP;
									}
								}
								if (siIT.getAbsoluteProportionSComparableSSpanItem() != null) {

									double sPercentStartIT = claculateRelativePercentage(
											associatedGenomePanel
													.getGenomePanelItem()
													.getListAbsoluteProportionElementItemS()
													.get(0)
													.getsSizeOfOragnismInPb(),
											siIT.getAbsoluteProportionSComparableSSpanItem()
													.getsPercentStart(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStopSGenome(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStartSGenome());
									double sPercentStopIT = claculateRelativePercentage(
											associatedGenomePanel
													.getGenomePanelItem()
													.getListAbsoluteProportionElementItemS()
													.get(0)
													.getsSizeOfOragnismInPb(),
											siIT.getAbsoluteProportionSComparableSSpanItem()
													.getsPercentStop(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStopSGenome(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStartSGenome());
									int fromCheckingIsInPathIT = checkIfSyntenyMarkerScaffoldIsInPath(
											false, true, -1, -1,
											sPercentStartIT, sPercentStopIT);
									if (fromCheckingIsInPathIT == -1) {
										// clicked s part
										selectSymbol(i, true, true, -1,
												false, true, false, false,
												false, false);
										break ADDCLICKHANDLERCHECKMARKERMAINLOOP;
									}
								} else {

									int idxPrevious = associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
											+ i - 1;
									if (idxPrevious >= 0
											&& idxPrevious < associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.size()) {
										HolderAbsoluteProportionItem siITPrevious = associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.get(idxPrevious)
												.getListHAPI()
												.get(associatedGenomePanel
														.getGenomePanelItem()
														.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																idxPrevious));
										if (siITPrevious != null) {
											if (siITPrevious
													.getAbsoluteProportionQComparableQSSpanItem() != null) {
												if (siITPrevious
														.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem
														|| siITPrevious
																.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
													// check for s previous et
													// next ?

													AbsoPropSGenoRegiInserItem apsgriITNext = ((AbsoPropQSSyntItem) siITPrevious
															.getAbsoluteProportionQComparableQSSpanItem())
															.getNextGenomicRegionSInsertion();
													if (apsgriITNext != null) {
														double sPercentStartIT = claculateRelativePercentage(
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getListAbsoluteProportionElementItemS()
																		.get(0)
																		.getsSizeOfOragnismInPb(),
																apsgriITNext
																		.getsPercentStart(),
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getPercentSyntenyShowStopSGenome(),
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getPercentSyntenyShowStartSGenome());
														double sPercentStopIT = claculateRelativePercentage(
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getListAbsoluteProportionElementItemS()
																		.get(0)
																		.getsSizeOfOragnismInPb(),
																apsgriITNext
																		.getsPercentStop(),
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getPercentSyntenyShowStopSGenome(),
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getPercentSyntenyShowStartSGenome());
														int fromCheckingIsInPathIT = checkIfSyntenyMarkerScaffoldIsInPath(
																false,
																true,
																-1,
																-1,
																sPercentStartIT,
																sPercentStopIT);
														if (fromCheckingIsInPathIT == -1) {
															// clicked s part
															selectSymbol(
																	i, true,
																	true, -1,
																	false,
																	true,
																	false,
																	true,
																	false,
																	false);
															break ADDCLICKHANDLERCHECKMARKERMAINLOOP;
														}
													}
												}
											}
										}
									}

									int idxNext = associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
											+ i + 1;
									if (idxNext >= 0
											&& idxNext < associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.size()) {
										HolderAbsoluteProportionItem siITNext = associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.get(idxNext)
												.getListHAPI()
												.get(associatedGenomePanel
														.getGenomePanelItem()
														.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																idxNext));
										if (siITNext != null) {
											if (siITNext
													.getAbsoluteProportionQComparableQSSpanItem() != null) {
												if (siITNext
														.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem
														|| siITNext
																.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
													// check for s previous et
													// next ?
													AbsoPropSGenoRegiInserItem apsgriITPrevious = ((AbsoPropQSSyntItem) siITNext
															.getAbsoluteProportionQComparableQSSpanItem())
															.getPreviousGenomicRegionSInsertion();
													if (apsgriITPrevious != null) {
														double sPercentStartIT = claculateRelativePercentage(
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getListAbsoluteProportionElementItemS()
																		.get(0)
																		.getsSizeOfOragnismInPb(),
																apsgriITPrevious
																		.getsPercentStart(),
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getPercentSyntenyShowStopSGenome(),
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getPercentSyntenyShowStartSGenome());
														double sPercentStopIT = claculateRelativePercentage(
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getListAbsoluteProportionElementItemS()
																		.get(0)
																		.getsSizeOfOragnismInPb(),
																apsgriITPrevious
																		.getsPercentStop(),
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getPercentSyntenyShowStopSGenome(),
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getPercentSyntenyShowStartSGenome());
														int fromCheckingIsInPathIT = checkIfSyntenyMarkerScaffoldIsInPath(
																false,
																true,
																-1,
																-1,
																sPercentStartIT,
																sPercentStopIT);
														if (fromCheckingIsInPathIT == -1) {
															// clicked s part
															selectSymbol(
																	i, true,
																	true, -1,
																	false,
																	true, true,
																	false,
																	false,
																	false);
															break ADDCLICKHANDLERCHECKMARKERMAINLOOP;
														}
													}

												}
											}
										}
									}

								}
							}
							// }

						}

						restoreToOriginCoordinateSpace();
					}
					// }

				}// LOCK_DOING_ANIMATION
			}
		});

		canvasBgk.addDoubleClickHandler(new DoubleClickHandler() {
			public void onDoubleClick(DoubleClickEvent event) {
				if (!LOCK_DOING_ANIMATION
						&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {

					// non need to reselect as done in first click
					// Insyght.APP_CONTROLER.clearAllUserSelection();
					// select genomePanel
					// associatedGenomePanel.selectThisGenomePanel();

					boolean somethingInMenuToDisplay = false;
					somethingInMenuToDisplay = buildMenuContextuelInPopUp();
					if (somethingInMenuToDisplay) {
						MainTabPanelView.CANVAS_MENU_POPUP
								.setPopupPositionAndShow(new PositionCallback() {
									public void setPosition(int offsetWidth,
											int offsetHeight) {
										int left = canvasBgk.getAbsoluteLeft()
												+ mouseX - (offsetWidth / 2);
										int top = canvasBgk.getAbsoluteTop()
												+ mouseY - offsetHeight - 3;
										MainTabPanelView.CANVAS_MENU_POPUP
												.setPopupPosition(left, top);
									}
								});
					}

				}

			}
		});

		canvasBgk.addKeyDownHandler(new KeyDownHandler() {
			// @Override
			public void onKeyDown(KeyDownEvent event) {
				// System.err.println("down");
				// synch nav et q zoom
				if (event.isControlKeyDown()) {
					// System.err.println("synch");
					SYNCHRONIZE_Q_ZOOMING = true;
					SYNCHRONIZE_NAVIGATION = true;
				}
			}
		});

		canvasBgk.addKeyUpHandler(new KeyUpHandler() {
			// @Override
			public void onKeyUp(KeyUpEvent event) {
				// System.err.println("up");
				// unsynch nav et q zoom
				// System.err.println("unsynch");
				if (SYNCHRONIZE_Q_ZOOMING || SYNCHRONIZE_NAVIGATION) {
					SYNCHRONIZE_Q_ZOOMING = false;
					SYNCHRONIZE_NAVIGATION = false;
					cleanOtherGEnomePanelFromSynchQZooming();
				}
			}
		});

		canvasBgk.addMouseDownHandler(new MouseDownHandler() {

			int mouseYLastClick = -1;
			int mouseXLastClick = -1;

			public void onMouseDown(MouseDownEvent event) {

				if (!LOCK_DOING_ANIMATION
						&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {

					if (mouseY == mouseYLastClick && mouseX == mouseXLastClick) {

						// System.err.println("double click");
						// double click
						mouseYLastClick = -1;
						mouseXLastClick = -1;

					} else {
						// System.err.println("click");
						// simple click
						mouseYLastClick = mouseY;
						mouseXLastClick = mouseX;

						Insyght.APP_CONTROLER.clearAllUserSelection();
						// select genomePanel
						associatedGenomePanel.selectThisGenomePanel();

						MOUSE_DOWN = true;
						if (mouseY > TOTAL_CANVAS_HEIGHT_UPPER_PART) {
							if (mouseX < SCAFFOLD_X_START_POINT) {
								mouseXStartDragging = SCAFFOLD_X_START_POINT;
							} else if (mouseX > SCAFFOLD_X_STOP_POINT) {
								mouseXStartDragging = SCAFFOLD_X_STOP_POINT;
							} else {
								mouseXStartDragging = mouseX;
							}
							mouseYStartDragging = mouseY;
							if (mouseYStartDragging < TOTAL_CANVAS_HEIGHT_UPPER_PART
									+ Y_AXIS_TO_DRAW_AT_TOP
									+ ((Y_AXIS_TO_DRAW_AT_BOTTOM - Y_AXIS_TO_DRAW_AT_TOP) / 2)) {
								zoomQ = true;
							} else {
								zoomQ = false;
							}
						}

					}
				}

			}
		});

		canvasBgk.addMouseUpHandler(new MouseUpHandler() {

			public void onMouseUp(MouseUpEvent event) {
				MOUSE_DOWN = false;
				if (DRAGGING_RIGHT_NOW) {
					breakDragging(true);
				}

			}
		});

	}

	protected void cleanOtherGEnomePanelFromSynchQZooming() {
		if (zoomQ) {
			// System.err.println("cleaning");
			for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
					.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
				GenomePanel gpIT = i.next();
				if (gpIT.isVisible()
						&& gpIT != Insyght.APP_CONTROLER
								.getCurrSelctGenomePanel()) {
					gpIT.getSyntenyCanvas().restoreToOriginCoordinateSpace();
					// gpIT.getSyntenyCanvas().contextBgk.translate(0,
					// TOTAL_CANVAS_HEIGHT_UPPER_PART);
					// gpIT.getSyntenyCanvas().contextBgk.setFillStyle(redrawColorBlack);
					gpIT.getSyntenyCanvas().clearMarkerBottom(true, true, true);
					gpIT.getSyntenyCanvas().drawScaffoldGenome(true, 0, 1,
							true, false, true);
					gpIT.getSyntenyCanvas().drawElementMarkerScaffold(true,
							true);
					gpIT.getSyntenyCanvas()
							.drawSyntenyMarkerScaffoldAllDisplayedSlices(false,
									false, true);

					// gpIT.getSyntenyCanvas().restoreToOriginCoordinateSpace();
				}
			}
		}
	}

	protected void breakDragging(boolean triggerZoomAction) {
		MOUSE_DOWN = false;
		DRAGGING_RIGHT_NOW = false;

		if (zoomQ) {
			if (Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getViewTypeInsyght()
					.compareTo(EnumResultViewTypes.homolog_table) == 0
					|| SYNCHRONIZE_Q_ZOOMING) {

				for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
						.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
					GenomePanel gpIT = i.next();
					if (gpIT.isVisible()) {
						gpIT.getSyntenyCanvas().clearMarkerBottom(true, true,
								false);
						gpIT.getSyntenyCanvas().drawScaffoldGenome(true, 0, 1,
								true, false, true);
					}
				}
			} else {
				// just do this gp
				clearMarkerBottom(true, true, false);
				drawScaffoldGenome(true, 0, 1, true, false, true);
			}

		} else {
			clearMarkerBottom(true, false, true);
			drawScaffoldGenome(false, 0, 1, true, false, true);
		}
		if (triggerZoomAction) {
			int currMouseZoomPos = -1;
			if (mouseX < SCAFFOLD_X_START_POINT) {
				currMouseZoomPos = SCAFFOLD_X_START_POINT;
			} else if (mouseX > SCAFFOLD_X_STOP_POINT) {
				currMouseZoomPos = SCAFFOLD_X_STOP_POINT;
			} else {
				currMouseZoomPos = mouseX;
			}
			mouseXStopDragging = currMouseZoomPos;

			// swap if reverse
			if (mouseXStopDragging < mouseXStartDragging) {
				int tmpMouseXStopDragging = mouseXStartDragging;
				int tmpMouseXStartDragging = mouseXStopDragging;
				mouseXStartDragging = tmpMouseXStartDragging;
				mouseXStopDragging = tmpMouseXStopDragging;
			}

			// mouseYStopDragging = mouseY;
			if (mouseXStartDragging == SCAFFOLD_X_START_POINT
					&& mouseXStopDragging == SCAFFOLD_X_STOP_POINT) {
				dezoom = true;
			} else {
				dezoom = false;
			}

			if (Math.abs(mouseXStartDragging - mouseXStopDragging) < SENSITIVITY_DRAGGING
					|| (mouseXStartDragging >= SCAFFOLD_X_STOP_POINT && mouseXStopDragging >= SCAFFOLD_X_STOP_POINT)
					|| (mouseXStartDragging <= SCAFFOLD_X_START_POINT && mouseXStopDragging <= SCAFFOLD_X_START_POINT)) {
				redrawOnTopToClearScaffoldAndMarkerArea(zoomQ, true, true);
			} else {
				// triggerZoomAction(false, dezoom, zoomQ, mouseXStartDragging,
				// mouseXStopDragging, false);
				if (dezoom) {
					launchZoomActionByPercent(false, dezoom, zoomQ, 0.0, 1.0,
							false);
				} else {
					launchZoomActionByPixelPos(false, dezoom, zoomQ,
							mouseXStartDragging, mouseXStopDragging, false);
				}
			}

		} else {
			mouseXStopDragging = -1;
			// mouseYStopDragging = -1;
			redrawOnTopToClearScaffoldAndMarkerArea(zoomQ, true, true);
		}
	}

	public void redrawOnTopToClearScaffoldAndMarkerArea(boolean q,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART,
			boolean doDrawSyntenyMarkerScaffoldForAllDisplayedSlices) {
		// System.out.println("cancelZoomAction");
		clearMarkerBottom(addTOTAL_CANVAS_HEIGHT_UPPER_PART, q, !q);
		drawScaffoldGenome(q, 0, 1, true, false,
				addTOTAL_CANVAS_HEIGHT_UPPER_PART);
		drawElementMarkerScaffold(q, addTOTAL_CANVAS_HEIGHT_UPPER_PART);
		if (doDrawSyntenyMarkerScaffoldForAllDisplayedSlices) {
			drawSyntenyMarkerScaffoldAllDisplayedSlices(true, false,
					addTOTAL_CANVAS_HEIGHT_UPPER_PART);
		}

	}

	public void launchZoomActionByPixelPos(final boolean animateADezzomFirst,
			final boolean dezoomSent, final boolean Q,
			final int mouseXPixelStart, final int mouseXPixelStop,
			final boolean doHideCanvasMenuPopUp) {

		double newPercentStartGenome = -1;
		double newPercentStopGenome = -1;

		if (Q) {
			if (!dezoomSent) {
				newPercentStartGenome = claculateAbsolutePercentage(
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getReferenceGenomePanelItem()
								.getListAbsoluteProportionElementItemQ().get(0)
								.getQsizeOfOragnismInPb(),
						(((double) mouseXPixelStart - (double) SCAFFOLD_X_START_POINT) / (double) TOTAL_LENGHT_SCAFFOLD),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStopQGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartQGenome());
				newPercentStopGenome = claculateAbsolutePercentage(
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getReferenceGenomePanelItem()
								.getListAbsoluteProportionElementItemQ().get(0)
								.getQsizeOfOragnismInPb(),
						(((double) mouseXPixelStop - (double) SCAFFOLD_X_START_POINT) / (double) TOTAL_LENGHT_SCAFFOLD),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStopQGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartQGenome());

			}
		} else {
			if (!dezoomSent) {
				newPercentStartGenome = claculateAbsolutePercentage(
						associatedGenomePanel.getGenomePanelItem()
								.getListAbsoluteProportionElementItemS().get(0)
								.getsSizeOfOragnismInPb(),
						(((double) mouseXPixelStart - (double) SCAFFOLD_X_START_POINT) / (double) TOTAL_LENGHT_SCAFFOLD),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStopSGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartSGenome());
				newPercentStopGenome = claculateAbsolutePercentage(
						associatedGenomePanel.getGenomePanelItem()
								.getListAbsoluteProportionElementItemS().get(0)
								.getsSizeOfOragnismInPb(),
						(((double) mouseXPixelStop - (double) SCAFFOLD_X_START_POINT) / (double) TOTAL_LENGHT_SCAFFOLD),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStopSGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartSGenome());
			}
		}

		doZoomAction(animateADezzomFirst, dezoomSent, Q, newPercentStartGenome,
				newPercentStopGenome, mouseXPixelStart, mouseXPixelStop,
				doHideCanvasMenuPopUp);

	}

	public void launchZoomActionByPercent(final boolean animateADezzomFirst,
			final boolean dezoomSent, final boolean Q,
			final double newPercentStartGenome,
			final double newPercentStopGenome,
			final boolean doHideCanvasMenuPopUp) {

		int mouseXPixelStart = -1;
		int mouseXPixelStop = -1;
		// if(!dezoomSent){
		mouseXPixelStart = (int) (SCAFFOLD_X_START_POINT + (TOTAL_LENGHT_SCAFFOLD * newPercentStartGenome));
		mouseXPixelStop = (int) (SCAFFOLD_X_START_POINT + (TOTAL_LENGHT_SCAFFOLD * newPercentStopGenome));
		// }

		doZoomAction(animateADezzomFirst, dezoomSent, Q, newPercentStartGenome,
				newPercentStopGenome, mouseXPixelStart, mouseXPixelStop,
				doHideCanvasMenuPopUp);

	}

	private void doZoomAction(final boolean animateADezzomFirst,
			final boolean dezoomSent, final boolean Q,
			final double newPercentStartGenome,
			final double newPercentStopGenome, final int mouseXPixelStart,
			final int mouseXPixelStop, final boolean doHideCanvasMenuPopUp) {

		if (dezoomSent) {
			if (Q) {
				if (associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStartQGenome() == 0
						&& associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStopQGenome() == 1) {
					if (doHideCanvasMenuPopUp) {
						MainTabPanelView.CANVAS_MENU_POPUP.hide();
					}
					return;
				}
			} else {
				if (associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStartSGenome() == 0
						&& associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStopSGenome() == 1) {
					if (doHideCanvasMenuPopUp) {
						MainTabPanelView.CANVAS_MENU_POPUP.hide();
					}
					return;
				}
			}
		}

		if (!LOCK_DOING_ANIMATION
				&& AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE <= 0) {
			LOCK_DOING_ANIMATION = true;
			final boolean synchQZoomIT = SYNCHRONIZE_Q_ZOOMING;
			// System.out.println("start animation");

			// clear all canvas
			if ((Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getViewTypeInsyght()
					.compareTo(EnumResultViewTypes.homolog_table) == 0 || synchQZoomIT)
					&& Q) {
				for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
						.getLIST_GENOME_PANEL().iterator(); i.hasNext();) {
					GenomePanel gpIT = i.next();
					if (gpIT.isVisible()) {
						// gpIT.getSyntenyCanvas().contextBgk.clearRect(0, 0,
						// TOTAL_CANVAS_WIDTH, TOTAL_CANVAS_HEIGHT);
						gpIT.getSyntenyCanvas().clearWholeCanvas();
						gpIT.getSyntenyCanvas().contextBgk
								.setFillStyle(redrawColorGreen);
						gpIT.getSyntenyCanvas().contextBgk.fillText(
								"Loading...", (TOTAL_CANVAS_WIDTH / 2)
										- (UNIT_WIDTH * 2),
								TOTAL_CANVAS_HEIGHT_UPPER_PART / 2);
						gpIT.getSyntenyCanvas().contextBgk
								.setFillStyle(redrawColorBlack);
						gpIT.getSyntenyCanvas().drawScaffoldGenomeText(true,
								true);
						gpIT.getSyntenyCanvas().drawScaffoldGenome(false, 0, 1,
								false, false, true);
					}
				}
			} else {
				// just do this gp
				// contextBgk.clearRect(0, 0, TOTAL_CANVAS_WIDTH,
				// TOTAL_CANVAS_HEIGHT);
				clearWholeCanvas();
				contextBgk.setFillStyle(redrawColorGreen);
				contextBgk.fillText("Loading...", (TOTAL_CANVAS_WIDTH / 2)
						- (UNIT_WIDTH * 2), TOTAL_CANVAS_HEIGHT_UPPER_PART / 2);
				contextBgk.setFillStyle(redrawColorBlack);
				drawScaffoldGenomeText(Q, true);
				drawScaffoldGenome(!Q, 0, 1, false, false, true);
			}

			Animation anim = new Animation() {
				@Override
				protected void onUpdate(double progress) {
					// clear canvas appropriately

					if (progress < 1) {
						// drawZoomPosition(true, progress, false, dezoom,
						// zoomQ, mouseXStartDragging, mouseXStopDragging,
						// true);
						if (animateADezzomFirst) {
							if ((Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght()
									.compareTo(
											EnumResultViewTypes.homolog_table) == 0 || synchQZoomIT)
									&& Q) {
								double progressToPassAlong = progress;
								for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
										.getLIST_GENOME_PANEL().iterator(); i
										.hasNext();) {
									GenomePanel gpIT = i.next();
									if (gpIT.isVisible()) {
										gpIT.getSyntenyCanvas()
												.drawZoomPosition(true,
														progressToPassAlong,
														false, true, true,
														SCAFFOLD_X_START_POINT,
														SCAFFOLD_X_STOP_POINT,
														0.0, 1.0, true, "");
									}
								}
							} else {
								// just do this gp
								drawZoomPosition(true, progress, false, true,
										Q, SCAFFOLD_X_START_POINT,
										SCAFFOLD_X_STOP_POINT, 0.0, 1.0, true,
										"");
							}
						} else {
							if ((Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght()
									.compareTo(
											EnumResultViewTypes.homolog_table) == 0 || synchQZoomIT)
									&& Q) {
								double progressToPassAlong = progress;
								boolean dezoomSentToPassAlong = dezoomSent;
								int mouseXPixelStartToPassAlong = mouseXPixelStart;
								int mouseXPixelStopToPassAlong = mouseXPixelStop;
								double newPercentStartGenomeToPassAlong = newPercentStartGenome;
								double newPercentStopGenomeToPassAlong = newPercentStopGenome;
								for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
										.getLIST_GENOME_PANEL().iterator(); i
										.hasNext();) {
									GenomePanel gpIT = i.next();
									if (gpIT.isVisible()) {
										gpIT.getSyntenyCanvas()
												.drawZoomPosition(
														true,
														progressToPassAlong,
														false,
														dezoomSentToPassAlong,
														true,
														mouseXPixelStartToPassAlong,
														mouseXPixelStopToPassAlong,
														newPercentStartGenomeToPassAlong,
														newPercentStopGenomeToPassAlong,
														true, "");
									}
								}
							} else {
								// just do this gp
								drawZoomPosition(true, progress, false,
										dezoomSent, Q, mouseXPixelStart,
										mouseXPixelStop, newPercentStartGenome,
										newPercentStopGenome, true, "");
							}
						}

					}
				}

				@Override
				protected void onComplete() {

					// System.out.println("done animation");
					if (!animateADezzomFirst) {

						if (Q) {
							if ((Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght()
									.compareTo(
											EnumResultViewTypes.homolog_table) == 0 || synchQZoomIT)
									&& Q) {
								// boolean dezoomSentOver = dezoomSent;
								// do all featured
								for (int i = 0; i < Insyght.APP_CONTROLER
										.getCurrentListFeaturedResultGenomePanelItem()
										.size(); i++) {
									GenomePanelItem gpiIT = Insyght.APP_CONTROLER
											.getCurrentListFeaturedResultGenomePanelItem()
											.get(i);
									// if(!dezoomSentOver){
									gpiIT.setPercentSyntenyShowStartQGenome(
											newPercentStartGenome, false,
											dezoomSent);
									gpiIT.setPercentSyntenyShowStopQGenome(
											newPercentStopGenome, true,
											dezoomSent);
									// }else{
									// gpiIT.setPercentSyntenyShowStartQGenome(0,
									// false);
									// gpiIT.setPercentSyntenyShowStopQGenome(1,
									// true);
									// }
									gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1,
											Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
								}
								// do all public
								for (int i = 0; i < Insyght.APP_CONTROLER
										.getCurrentListPublicResultGenomePanelItem()
										.size(); i++) {
									GenomePanelItem gpiIT = Insyght.APP_CONTROLER
											.getCurrentListPublicResultGenomePanelItem()
											.get(i);
									// if(!dezoomSentOver){
									gpiIT.setPercentSyntenyShowStartQGenome(
											newPercentStartGenome, false,
											dezoomSent);
									gpiIT.setPercentSyntenyShowStopQGenome(
											newPercentStopGenome, true,
											dezoomSent);
									// }else{
									// gpiIT.setPercentSyntenyShowStartQGenome(0,
									// false);
									// gpiIT.setPercentSyntenyShowStopQGenome(1,
									// true);
									// }
									gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1,
											Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
								}
								// do all private
								for (int i = 0; i < Insyght.APP_CONTROLER
										.getCurrentListPrivateResultGenomePanelItem()
										.size(); i++) {
									GenomePanelItem gpiIT = Insyght.APP_CONTROLER
											.getCurrentListPrivateResultGenomePanelItem()
											.get(i);
									// if(!dezoomSentOver){
									gpiIT.setPercentSyntenyShowStartQGenome(
											newPercentStartGenome, false,
											dezoomSent);
									gpiIT.setPercentSyntenyShowStopQGenome(
											newPercentStopGenome, true,
											dezoomSent);
									// }else{
									// gpiIT.setPercentSyntenyShowStartQGenome(0,
									// false);
									// gpiIT.setPercentSyntenyShowStopQGenome(1,
									// true);
									// }
									gpiIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1,
											Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
								}
								// do ref genome
								// if(!dezoomSentOver){
								Insyght.APP_CONTROLER
										.getCurrentRefGenomePanelAndListOrgaResult()
										.getReferenceGenomePanelItem()
										.setPercentSyntenyShowStartQGenome(
												newPercentStartGenome, false,
												dezoomSent);
								Insyght.APP_CONTROLER
										.getCurrentRefGenomePanelAndListOrgaResult()
										.getReferenceGenomePanelItem()
										.setPercentSyntenyShowStopQGenome(
												newPercentStopGenome, true,
												dezoomSent);
								// }else{
								// Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setPercentSyntenyShowStartQGenome(0,
								// false);
								// Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().setPercentSyntenyShowStopQGenome(1,
								// true);
								// }
								Insyght.APP_CONTROLER
										.getCurrentRefGenomePanelAndListOrgaResult()
										.getReferenceGenomePanelItem()
										.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
												-1,
												Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());

							} else {
								// just do this gp
								// if(!dezoomSent){
								associatedGenomePanel.getGenomePanelItem()
										.setPercentSyntenyShowStartQGenome(
												newPercentStartGenome, false,
												dezoomSent);
								associatedGenomePanel.getGenomePanelItem()
										.setPercentSyntenyShowStopQGenome(
												newPercentStopGenome, true,
												dezoomSent);
								// }else{
								// associatedGenomePanel.getGenomePanelItem().setPercentSyntenyShowStartQGenome(0,
								// false);
								// associatedGenomePanel.getGenomePanelItem().setPercentSyntenyShowStopQGenome(1,
								// true);
								// }
							}

						} else {
							// if(!dezoomSent){
							associatedGenomePanel.getGenomePanelItem()
									.setPercentSyntenyShowStartSGenome(
											newPercentStartGenome, false,
											dezoomSent);
							associatedGenomePanel.getGenomePanelItem()
									.setPercentSyntenyShowStopSGenome(
											newPercentStopGenome, true,
											dezoomSent);
							// }else{
							// associatedGenomePanel.getGenomePanelItem().setPercentSyntenyShowStartSGenome(0,
							// false);
							// associatedGenomePanel.getGenomePanelItem().setPercentSyntenyShowStopSGenome(1,
							// true);
							// }
						}

						if ((Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght()
								.compareTo(EnumResultViewTypes.homolog_table) == 0 || synchQZoomIT)
								&& Q) {
							boolean doHideCanvasMenuPopUpOver = doHideCanvasMenuPopUp;
							for (Iterator<GenomePanel> i = Insyght.APP_CONTROLER
									.getLIST_GENOME_PANEL().iterator(); i
									.hasNext();) {
								GenomePanel gpIT = i.next();
								if (gpIT.isVisible()) {
									// //find first start qs or q
									// int newIxStart = 0;
									// if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewType().compareTo(EnumResultViewTypes.homolog_table)!=0){
									// for(int
									// j=0;j<gpIT.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size();j++){
									// HolderAbsoluteProportionItem hapiIT =
									// gpIT.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(j).getListHAPI().get(
									// gpIT.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(j).getIdxToDisplayInListHAPI());
									// if(hapiIT.getAbsoluteProportionQComparableQSpanItem()
									// != null
									// ||
									// hapiIT.getAbsoluteProportionQComparableQSSpanItem()
									// != null){
									// //found start
									// newIxStart = j;
									// break;
									// }
									// }
									// if(newIxStart >
									// (gpIT.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()
									// - NUMBER_SLICE_WIDTH)){
									// newIxStart =
									// (gpIT.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()
									// - NUMBER_SLICE_WIDTH);
									// }
									// }
									// gpIT.getGenomePanelItem().setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(newIxStart);
									if (!doHideCanvasMenuPopUpOver) {
										gpIT.getSyntenyCanvas()
												.clearScaffoldGenome(Q);
									}
									gpIT.getSyntenyCanvas().drawViewToCanvas(1,
											-1, true, true, true, false);
								}
							}
						} else {
							// just do this gp
							// int newIxStart = 0;
							// for(int
							// j=0;j<associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size();j++){
							// HolderAbsoluteProportionItem hapiIT =
							// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(j).getListHAPI().get(
							// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(j).getIdxToDisplayInListHAPI());
							// if((hapiIT.getAbsoluteProportionQComparableQSpanItem()
							// != null
							// &&
							// hapiIT.getAbsoluteProportionSComparableSSpanItem()
							// != null)
							// ||
							// hapiIT.getAbsoluteProportionQComparableQSSpanItem()
							// != null){
							// //found start
							// newIxStart = j;
							// break;
							// }
							// }
							// if(newIxStart >
							// (associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()
							// - NUMBER_SLICE_WIDTH)){
							// newIxStart =
							// (associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()
							// - NUMBER_SLICE_WIDTH);
							// }
							// if(newIxStart < 0){
							// newIxStart = 0;
							// }
							associatedGenomePanel
									.getGenomePanelItem()
									.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
											-1,
											Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
							if (!doHideCanvasMenuPopUp) {
								clearScaffoldGenome(Q);
							}
							drawViewToCanvas(1, -1, true, true, true, false);
						}

						if (doHideCanvasMenuPopUp) {
							MainTabPanelView.CANVAS_MENU_POPUP.hide();
						}
						LOCK_DOING_ANIMATION = false;

					} else {
						if (Q) {
							associatedGenomePanel.getGenomePanelItem()
									.setPercentSyntenyShowStartQGenome(0,
											false, true);
							associatedGenomePanel.getGenomePanelItem()
									.setPercentSyntenyShowStopQGenome(1, true,
											true);
						} else {
							associatedGenomePanel.getGenomePanelItem()
									.setPercentSyntenyShowStartSGenome(0,
											false, true);
							associatedGenomePanel.getGenomePanelItem()
									.setPercentSyntenyShowStopSGenome(1, true,
											true);
						}
						LOCK_DOING_ANIMATION = false;
						// triggerZoomAction(false, dezoomSent, Q,
						// mouseXPixelStart, mouseXPixelStop,
						// doHideCanvasMenuPopUp);
						// triggerZoomAction(false, dezoomSent, Q,
						// newPercentStartGenome, newPercentStopGenome,
						// doHideCanvasMenuPopUp);
						doZoomAction(false, dezoomSent, Q,
								newPercentStartGenome, newPercentStopGenome,
								mouseXPixelStart, mouseXPixelStop,
								doHideCanvasMenuPopUp);
					}

				}
			};
			// run animation for ANI_TIME ms
			anim.run(ANI_TIME);

		}

	}

	public void drawZoomPosition(boolean duringAnimation, double progress,
			boolean dragging, boolean dezoomSent, boolean Q,
			int mouseXPixelStart, int mouseXPixelStop,
			double percentageStartToBe, double percentageStopToBe,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART, String textToAdd) {

		double startZoomX = -1;
		double stopZoomX = -1;
		String textZoom = "";
		boolean startZoomToTheLeft = false;
		boolean stopZoomToTheLeft = false;
		boolean startZoomToTheRight = false;
		boolean stopZoomToTheRight = false;

		if (duringAnimation) {
			// during animation
			// if(!dezoom){
			if (!dezoomSent) {

				// zooming in
				// clear canvas
				// clearMarkerBottom(true, zoomQ, !zoomQ);
				clearMarkerBottom(addTOTAL_CANVAS_HEIGHT_UPPER_PART, Q, !Q);

				// if(mouseXStopDragging > mouseXStartDragging){
				if (mouseXPixelStop > mouseXPixelStart) {
					// startZoomX =
					// mouseXStartDragging-((mouseXStartDragging-SCAFFOLD_X_START_POINT)*progress);
					startZoomX = mouseXPixelStart
							- ((mouseXPixelStart - SCAFFOLD_X_START_POINT) * progress);
					// stopZoomX =
					// mouseXStopDragging+((SCAFFOLD_X_STOP_POINT-mouseXStopDragging)*progress);
					stopZoomX = mouseXPixelStop
							+ ((SCAFFOLD_X_STOP_POINT - mouseXPixelStop) * progress);
				} else {
					// startZoomX =
					// mouseXStopDragging-((mouseXStopDragging-SCAFFOLD_X_START_POINT)*progress);
					startZoomX = mouseXPixelStop
							- ((mouseXPixelStop - SCAFFOLD_X_START_POINT) * progress);
					// stopZoomX =
					// mouseXStartDragging+((SCAFFOLD_X_STOP_POINT-mouseXStartDragging)*progress);
					stopZoomX = mouseXPixelStart
							+ ((SCAFFOLD_X_STOP_POINT - mouseXPixelStart) * progress);
				}

			} else {
				// zooming out
				// clear canvas
				// clearMarkerBottom(true, zoomQ, !zoomQ);
				clearMarkerBottom(addTOTAL_CANVAS_HEIGHT_UPPER_PART, Q, !Q);

				// drawScaffoldGenome(zoomQ, 0, 1, true, false, true);
				drawScaffoldGenome(Q, 0, 1, true, false,
						addTOTAL_CANVAS_HEIGHT_UPPER_PART);

				if (progress == 0) {
					// init phase
					startZoomX = SCAFFOLD_X_START_POINT;
					stopZoomX = SCAFFOLD_X_STOP_POINT;
				} else {
					// if(zoomQ){
					if (Q) {
						// startZoomX =
						// SCAFFOLD_X_START_POINT+TOTAL_LENGHT_SCAFFOLD*((associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStartQGenome()-0.0)*progress);
						// stopZoomX =
						// SCAFFOLD_X_STOP_POINT-TOTAL_LENGHT_SCAFFOLD*((1.0-associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStopQGenome())*progress);
						startZoomX = SCAFFOLD_X_START_POINT
								+ TOTAL_LENGHT_SCAFFOLD
								* ((associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStartQGenome() - percentageStartToBe) * progress);
						stopZoomX = SCAFFOLD_X_STOP_POINT
								- TOTAL_LENGHT_SCAFFOLD
								* ((percentageStopToBe - associatedGenomePanel
										.getGenomePanelItem()
										.getPercentSyntenyShowStopQGenome()) * progress);
					} else {
						// startZoomX =
						// SCAFFOLD_X_START_POINT+TOTAL_LENGHT_SCAFFOLD*((associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStartSGenome()-0.0)*progress);
						// stopZoomX =
						// SCAFFOLD_X_STOP_POINT-TOTAL_LENGHT_SCAFFOLD*((1.0-associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStopSGenome())*progress);
						startZoomX = SCAFFOLD_X_START_POINT
								+ TOTAL_LENGHT_SCAFFOLD
								* ((associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStartSGenome() - percentageStartToBe) * progress);
						stopZoomX = SCAFFOLD_X_STOP_POINT
								- TOTAL_LENGHT_SCAFFOLD
								* ((percentageStopToBe - associatedGenomePanel
										.getGenomePanelItem()
										.getPercentSyntenyShowStopSGenome()) * progress);
					}
				}
			}
			//
			// }else if (dragging){
			// //not in animation, dragging
			//
			// //clear canvas
			// clearMarkerBottom(addTOTAL_CANVAS_HEIGHT_UPPER_PART, Q, !Q);
			// drawScaffoldGenome(Q, 0, 1, true, false,
			// addTOTAL_CANVAS_HEIGHT_UPPER_PART);
			//
			// double currMouseZoomPos = -1;
			// if(mouseX < SCAFFOLD_X_START_POINT){
			// currMouseZoomPos = SCAFFOLD_X_START_POINT;
			// }else if(mouseX > SCAFFOLD_X_STOP_POINT){
			// currMouseZoomPos = SCAFFOLD_X_STOP_POINT;
			// }else{
			// currMouseZoomPos = mouseX;
			// }
			//
			// if(currMouseZoomPos > mouseXStartDragging){
			// startZoomX = mouseXStartDragging;
			// stopZoomX = currMouseZoomPos;
			// }else{
			// startZoomX = currMouseZoomPos;
			// stopZoomX = mouseXStartDragging;
			// }

		} else {

			clearMarkerBottom(addTOTAL_CANVAS_HEIGHT_UPPER_PART, Q, !Q);
			drawScaffoldGenome(Q, 0, 1, true, false,
					addTOTAL_CANVAS_HEIGHT_UPPER_PART);

			if (mouseXPixelStop > mouseXPixelStart) {
				startZoomX = mouseXPixelStart;
				stopZoomX = mouseXPixelStop;
			} else {
				startZoomX = mouseXPixelStop;
				stopZoomX = mouseXPixelStart;
			}

			if (mouseXPixelStart < SCAFFOLD_X_START_POINT) {
				// start of zoom is to the left
				startZoomToTheLeft = true;
				startZoomX = SCAFFOLD_X_START_POINT;
			}
			if (mouseXPixelStop < SCAFFOLD_X_START_POINT) {
				// stop of zoom is to the left
				stopZoomToTheLeft = true;
				stopZoomX = SCAFFOLD_X_START_POINT;
			}
			if (mouseXPixelStart > SCAFFOLD_X_STOP_POINT) {
				// start of zoom is to the right
				startZoomToTheRight = true;
				startZoomX = SCAFFOLD_X_STOP_POINT;
			}
			if (mouseXPixelStop > SCAFFOLD_X_STOP_POINT) {
				// stop of zoom is to the right
				stopZoomToTheRight = true;
				stopZoomX = SCAFFOLD_X_STOP_POINT;
			}

		}

		// int correctionText = -1;
		if (!duringAnimation) {
			if (textToAdd.length() > 0) {
				textZoom = textToAdd;
			} else if (startZoomX == SCAFFOLD_X_START_POINT
					&& stopZoomX == SCAFFOLD_X_STOP_POINT) {
				// dezoom = true;

				if (Q) {
					if (associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStartQGenome() == 0
							&& associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStopQGenome() == 1) {
						textZoom = "Max zoom out reached";
						// correctionText = UNIT_WIDTH*3;
					} else {
						textZoom = "Zoom out max";
						// correctionText = UNIT_WIDTH;
					}
				} else {
					if (associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStartSGenome() == 0
							&& associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStopSGenome() == 1) {
						textZoom = "Max zoom out reached";
						// correctionText = UNIT_WIDTH*3;
					} else {
						textZoom = "Zoom out max";
						// correctionText = UNIT_WIDTH;
					}
				}

			} else {
				// dezoom = false;
				textZoom = "Zoom in";
				// correctionText = (UNIT_WIDTH*textZoom.length())/7;

			}
		}

		// measure text lenght
		// TextMetrics measureTextIT = contextBgk.measureText(textZoom);
		// double widthmeasureTextIT = measureTextIT.getWidth();

		// draw zoom

		contextBgk.setFillStyle(redrawColorRed);
		contextBgk.setStrokeStyle(redrawColorRed);

		double xPointStartZoomRelative = Math
				.abs((startZoomX - (double) SCAFFOLD_X_START_POINT)
						/ (double) TOTAL_LENGHT_SCAFFOLD);
		double xPointStopZoomRelative = Math
				.abs((stopZoomX - (double) SCAFFOLD_X_START_POINT)
						/ (double) TOTAL_LENGHT_SCAFFOLD);
		// double xPointCenterZoomRelative =
		// Math.abs(startZoomX+((stopZoomX-startZoomX)/2));
		double xPointCenterZoomPixel = Math.abs(startZoomX
				+ ((stopZoomX - startZoomX) / 2));

		if (Q) {
			if (!duringAnimation) {
				// if(Math.abs(stopZoomX - startZoomX) > (correctionText*3)+4 &&
				// xPoint3 > SCAFFOLD_X_START_POINT+3 && xPoint3 <
				// SCAFFOLD_X_STOP_POINT - (correctionText*1.5)){
				if (stopZoomToTheRight && startZoomToTheLeft) {
					drawScaffoldGenome(true, xPointStartZoomRelative,
							xPointStopZoomRelative, true, true, true);
					contextBgk.setTextAlign(TextAlign.RIGHT);
					contextBgk.fillText("-->  ", SCAFFOLD_X_STOP_POINT - 2,
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									+ Y_AXIS_TO_DRAW_AT_TOP - 4 + 0.5);
					contextBgk.setTextAlign(TextAlign.LEFT);
					contextBgk.fillText("  <--", SCAFFOLD_X_START_POINT + 2,
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									+ Y_AXIS_TO_DRAW_AT_TOP - 4 + 0.5);
					if (stopZoomX - startZoomX > 6) {
						contextBgk.setTextAlign(TextAlign.CENTER);
						contextBgk.fillText(textZoom, xPointCenterZoomPixel,
								TOTAL_CANVAS_HEIGHT_UPPER_PART
										+ Y_AXIS_TO_DRAW_AT_TOP - 4 + 0.5,
								stopZoomX - startZoomX - 5);
					}
				} else if (stopZoomToTheRight) {
					// int startXText =
					// SCAFFOLD_X_STOP_POINT-(correctionText*2)-2;
					// contextBgk.fillText(textZoom+" -->  ", startXText,
					// TOTAL_CANVAS_HEIGHT_UPPER_PART+Y_AXIS_TO_DRAW_AT_TOP-4+0.5,
					// SCAFFOLD_X_STOP_POINT-startXText);
					if (!startZoomToTheRight) {
						drawPathSyntenyMarkerScaffoldQ(xPointStartZoomRelative,
								true, false);
						contextBgk.fill();
						contextBgk.stroke();
						drawScaffoldGenome(true, xPointStartZoomRelative,
								xPointStopZoomRelative, true, true, true);
					}
					contextBgk.setTextAlign(TextAlign.RIGHT);
					contextBgk.fillText(textZoom + " -->  ",
							SCAFFOLD_X_STOP_POINT - 2,
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									+ Y_AXIS_TO_DRAW_AT_TOP - 4 + 0.5,
							SCAFFOLD_X_STOP_POINT - SCAFFOLD_X_START_POINT);
				} else if (startZoomToTheLeft) {
					if (!stopZoomToTheLeft) {
						drawPathSyntenyMarkerScaffoldQ(xPointStopZoomRelative,
								true, false);
						contextBgk.fill();
						contextBgk.stroke();
						drawScaffoldGenome(true, xPointStartZoomRelative,
								xPointStopZoomRelative, true, true, true);
					}
					contextBgk.setTextAlign(TextAlign.LEFT);
					contextBgk.fillText("  <-- " + textZoom,
							SCAFFOLD_X_START_POINT + 2,
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									+ Y_AXIS_TO_DRAW_AT_TOP - 4 + 0.5,
							SCAFFOLD_X_STOP_POINT - SCAFFOLD_X_START_POINT);
				} else {
					// System.out.println("1:"+contextBgk.getFillStyle());
					drawPathSyntenyMarkerScaffoldQ(xPointStartZoomRelative,
							true, false);
					contextBgk.fill();
					contextBgk.stroke();
					// System.out.println("2:"+contextBgk.getFillStyle());
					drawPathSyntenyMarkerScaffoldQ(xPointStopZoomRelative,
							true, false);
					contextBgk.fill();
					contextBgk.stroke();
					// System.out.println("3:"+contextBgk.getFillStyle());
					drawScaffoldGenome(true, xPointStartZoomRelative,
							xPointStopZoomRelative, true, true, true);
					if (stopZoomX - startZoomX > 6) {
						contextBgk.setTextAlign(TextAlign.CENTER);
						contextBgk.fillText(textZoom, xPointCenterZoomPixel,
								TOTAL_CANVAS_HEIGHT_UPPER_PART
										+ Y_AXIS_TO_DRAW_AT_TOP - 4 + 0.5,
								stopZoomX - startZoomX - 5);
					}

				}
				// }
			} else {
				drawPathSyntenyMarkerScaffoldQ(xPointStartZoomRelative, true,
						false);
				contextBgk.fill();
				contextBgk.stroke();
				drawPathSyntenyMarkerScaffoldQ(xPointStopZoomRelative, true,
						false);
				contextBgk.fill();
				contextBgk.stroke();
				drawScaffoldGenome(true, xPointStartZoomRelative,
						xPointStopZoomRelative, true, true, true);
			}

		} else {

			if (!duringAnimation) {
				// if(Math.abs(stopZoomX - startZoomX) > (correctionText*3)+4 &&
				// xPoint3 > SCAFFOLD_X_START_POINT+3 && xPoint3 <
				// SCAFFOLD_X_STOP_POINT - (correctionText*1.5)){

				contextBgk.setTextBaseline(TextBaseline.TOP);
				if (stopZoomToTheRight && startZoomToTheLeft) {
					drawScaffoldGenome(false, xPointStartZoomRelative,
							xPointStopZoomRelative, true, true, true);
					contextBgk.setTextAlign(TextAlign.RIGHT);
					contextBgk.fillText("-->  ", SCAFFOLD_X_STOP_POINT - 2,
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									+ Y_AXIS_TO_DRAW_AT_BOTTOM + 2 + 0.5);
					contextBgk.setTextAlign(TextAlign.LEFT);
					contextBgk.fillText("  <--", SCAFFOLD_X_START_POINT + 2,
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									+ Y_AXIS_TO_DRAW_AT_BOTTOM + 2 + 0.5);
					if (stopZoomX - startZoomX > 6) {
						contextBgk.setTextAlign(TextAlign.CENTER);
						contextBgk.fillText(textZoom, xPointCenterZoomPixel,
								TOTAL_CANVAS_HEIGHT_UPPER_PART
										+ Y_AXIS_TO_DRAW_AT_BOTTOM + 2 + 0.5,
								stopZoomX - startZoomX - 5);
					}

				} else if (stopZoomToTheRight) {
					if (!startZoomToTheRight) {
						drawPathSyntenyMarkerScaffoldS(xPointStartZoomRelative,
								true, false);
						contextBgk.fill();
						contextBgk.stroke();
						drawScaffoldGenome(false, xPointStartZoomRelative,
								xPointStopZoomRelative, true, true, true);
					}
					contextBgk.setTextAlign(TextAlign.RIGHT);
					contextBgk.fillText(textZoom + " -->  ",
							SCAFFOLD_X_STOP_POINT - 2,
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									+ Y_AXIS_TO_DRAW_AT_BOTTOM + 2 + 0.5,
							SCAFFOLD_X_STOP_POINT - SCAFFOLD_X_START_POINT);

				} else if (startZoomToTheLeft) {
					if (!stopZoomToTheLeft) {
						drawPathSyntenyMarkerScaffoldS(xPointStopZoomRelative,
								true, false);
						contextBgk.fill();
						contextBgk.stroke();
						drawScaffoldGenome(false, xPointStartZoomRelative,
								xPointStopZoomRelative, true, true, true);
					}
					contextBgk.setTextAlign(TextAlign.LEFT);
					contextBgk.fillText("  <-- " + textZoom,
							SCAFFOLD_X_START_POINT + 2,
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									+ Y_AXIS_TO_DRAW_AT_BOTTOM + 2 + 0.5,
							SCAFFOLD_X_STOP_POINT - SCAFFOLD_X_START_POINT);

				} else {
					drawPathSyntenyMarkerScaffoldS(xPointStartZoomRelative,
							true, false);
					contextBgk.fill();
					contextBgk.stroke();
					drawPathSyntenyMarkerScaffoldS(xPointStopZoomRelative,
							true, false);
					contextBgk.fill();
					contextBgk.stroke();
					drawScaffoldGenome(false, xPointStartZoomRelative,
							xPointStopZoomRelative, true, true, true);
					if (stopZoomX - startZoomX > 6) {
						contextBgk.setTextAlign(TextAlign.CENTER);
						contextBgk.fillText(textZoom, xPointCenterZoomPixel,
								TOTAL_CANVAS_HEIGHT_UPPER_PART
										+ Y_AXIS_TO_DRAW_AT_BOTTOM + 2 + 0.5,
								stopZoomX - startZoomX - 5);
					}
				}
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
				// }
			} else {
				drawPathSyntenyMarkerScaffoldS(xPointStartZoomRelative, true,
						false);
				contextBgk.fill();
				contextBgk.stroke();
				drawPathSyntenyMarkerScaffoldS(xPointStopZoomRelative, true,
						false);
				contextBgk.fill();
				contextBgk.stroke();
				drawScaffoldGenome(false, xPointStartZoomRelative,
						xPointStopZoomRelative, true, true, true);
			}
		}
		contextBgk.setTextAlign(TextAlign.LEFT);
	}

	// public void selectBlockSynteny(int sliceXClicked, boolean
	// redrawOnTopSent,
	// boolean inRedSent, int newIdxInArrayOfThisSlice,
	// boolean clearSentSliceIdxEntirely,
	// boolean clickedSSymbolInUpperPart,
	// boolean selectPreviousSOfNextSlice,
	// boolean selectNextSOfPreviousSlice, boolean allowToFetchMoreData,
	// boolean redrawSymbolicReprOfOppositeQorSInBlackUnselected) {
	//
	// // method for not gene homology clicked
	// selectBlockSynteny(sliceXClicked, redrawOnTopSent, inRedSent,
	// newIdxInArrayOfThisSlice, clearSentSliceIdxEntirely, false,
	// false, false, clickedSSymbolInUpperPart,
	// selectPreviousSOfNextSlice, selectNextSOfPreviousSlice,
	// allowToFetchMoreData,
	// redrawSymbolicReprOfOppositeQorSInBlackUnselected);
	//
	// }

	public void selectSymbol(
			int sliceXClicked,
			boolean redrawOnTopSent,
			boolean inRedSent,
			int newIdxInArrayOfThisSlice,
			boolean clearSentSliceIdxEntirely,
			// boolean geneHomologyGeneNameUpClicked,
			// boolean geneHomologyGeneNameDownClicked,
			// boolean geneHomologyMatchZoneClicked,
			boolean clickedSSymbolInUpperPart,
			boolean selectedPreviousSOfNextSliceSent,
			boolean selectedNextSOfPreviousSliceSent,
			boolean allowToFetchMoreData,
			boolean redrawSymbolicReprOfOppositeQorSInBlackUnselected) {
		// general method

		Insyght.APP_CONTROLER.clearAllUserSelection();
		// select genomePanel
		associatedGenomePanel.selectThisGenomePanel();

		// set variable block synteny
		selectedSliceX = sliceXClicked;
		selectedQ = !clickedSSymbolInUpperPart;
		// selectedMainQS = false;
		// if(!selectedPreviousSOfNextSliceSent &&
		// !selectedNextSOfPreviousSliceSent){
		// selectedMainQS = true;
		// }
		selectedPreviousSOfNextSlice = selectedPreviousSOfNextSliceSent;
		selectedNextSOfPreviousSlice = selectedNextSOfPreviousSliceSent;

		if (clearSentSliceIdxEntirely) {
			// System.err.println("clearing slice entirely : "+sliceXClicked);
			clearSymbolicSlice(sliceXClicked);
			// Window.alert("done!");
			int idxNextSlide = associatedGenomePanel.getGenomePanelItem()
					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
					+ selectedSliceX + 1;
			if (idxNextSlide >= 0
					&& idxNextSlide < associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.size()) {
				HolderAbsoluteProportionItem siNextIT = associatedGenomePanel
						.getGenomePanelItem()
						.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
						.get(idxNextSlide)
						.getListHAPI()
						.get(associatedGenomePanel
								.getGenomePanelItem()
								.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
										idxNextSlide));
				if (siNextIT.getAbsoluteProportionQComparableQSSpanItem() != null
						|| siNextIT.getAbsoluteProportionSComparableSSpanItem() != null) {
					// do nothing
					// System.err.println("do nothing next");
				} else {
					clearSymbolicSliceSOnly((sliceXClicked + 1), true, false);
					// System.err.println("clear next");
				}
			}

			int idxPrevSlide = associatedGenomePanel.getGenomePanelItem()
					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
					+ selectedSliceX - 1;
			if (idxPrevSlide >= 0
					&& idxPrevSlide < associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.size()) {
				HolderAbsoluteProportionItem siPrevIT = associatedGenomePanel
						.getGenomePanelItem()
						.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
						.get(idxPrevSlide)
						.getListHAPI()
						.get(associatedGenomePanel
								.getGenomePanelItem()
								.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
										idxPrevSlide));
				if (siPrevIT.getAbsoluteProportionQComparableQSSpanItem() != null
						|| siPrevIT.getAbsoluteProportionSComparableSSpanItem() != null) {
					// do nothing
					// System.err.println("do nothing prev : index="+idxPrevSlide+" ; "+siPrevIT.getAbsoluteProportionQComparableQSSpanItem()+
					// " ; "+siPrevIT.getAbsoluteProportionSComparableSSpanItem());
				} else {
					clearSymbolicSliceSOnly((sliceXClicked - 1), false, true);
					// System.err.println("clear prev");
				}
			}

		}

		if (newIdxInArrayOfThisSlice >= 0) {
			// System.err.println("register nvx idx : "+newIdxInArrayOfThisSlice+" at pos "+(associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+sliceXClicked));
			associatedGenomePanel
					.getGenomePanelItem()
					.setCurrDisplayIndexAtSpecifiedAPIToDisplayIndex(
							(associatedGenomePanel
									.getGenomePanelItem()
									.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + sliceXClicked),
							newIdxInArrayOfThisSlice);
		}

		HolderAbsoluteProportionItem siIT = associatedGenomePanel
				.getGenomePanelItem()
				.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
				.get(associatedGenomePanel
						.getGenomePanelItem()
						.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
						+ selectedSliceX)
				.getListHAPI()
				.get(associatedGenomePanel
						.getGenomePanelItem()
						.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
								associatedGenomePanel
										.getGenomePanelItem()
										.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
										+ selectedSliceX));
		int numberOtherMatch = associatedGenomePanel
				.getGenomePanelItem()
				.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
				.get(associatedGenomePanel
						.getGenomePanelItem()
						.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
						+ selectedSliceX).getListHAPI().size() - 1;

		if (siIT.getAbsoluteProportionQComparableQSSpanItem() != null) {

			// set style upper
			// save to be able to go back to initial canvas state
			restoreToOriginCoordinateSpace();
			contextBgk.translate(addToLeftToCenterDrawing
					+ (SLICE_CANVAS_WIDTH * selectedSliceX), 0);
			if (siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
				// System.out.println("here");
				drawBlockQSGeneHomology(
						(AbsoPropQSGeneHomoItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem(),
						redrawOnTopSent,
						inRedSent,
						true,
						false,
						false,// was geneHomologyMatchZoneClicked,
						false,// was geneHomologyGeneNameUpClicked,
						false,// was geneHomologyGeneNameDownClicked,
						false,
						numberOtherMatch,
						// geneHomologyGeneNameUpClicked,
						// geneHomologyGeneNameDownClicked,
						// geneHomologyMatchZoneClicked,
						true, siIT.getIndexInFullArray(), selectedSliceX,
						((AbsoPropQSGeneHomoItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getPreviousGenomicRegionSInsertion(),
						((AbsoPropQSGeneHomoItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getNextGenomicRegionSInsertion());

			} else if (siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
				int numberPairsToAdd = ((AbsoPropQSSyntItem) siIT
						.getAbsoluteProportionQComparableQSSpanItem())
						.getSyntenyNumberGene();
				// numberPairsToAdd +=
				// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsContainedOtherSyntenies().size();
				// numberPairsToAdd +=
				// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsMotherSyntenies().size();
				// numberPairsToAdd +=
				// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsSprungOffSyntenies().size();

				drawBlockQSSynteny(siIT
						.getAbsoluteProportionQComparableQSSpanItem()
						.getQsEnumBlockType(), numberPairsToAdd,
						redrawOnTopSent, inRedSent, numberOtherMatch,
						((AbsoPropQSSyntItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.isContainedWithinAnotherMotherSynteny(),
						((AbsoPropQSSyntItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.isSprungOffAnotherMotherSynteny(),
						((AbsoPropQSSyntItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.isMotherOfSprungOffSyntenies(),
						((AbsoPropQSSyntItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.isMotherOfContainedOtherSyntenies(),
						((AbsoPropQSSyntItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getPreviousGenomicRegionSInsertion(),
						((AbsoPropQSSyntItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getNextGenomicRegionSInsertion(), true,
						siIT.getIndexInFullArray(), selectedSliceX
				// ,selectedMainQS, selectedPreviousSOfNextSlice,
				// selectedNextSOfPreviousSlice
				);
				// boolean allowFetchMissingData, int idxHAPInFullArray, int
				// idxSliceDisplayed

			} else {
				//System.err.println("ERROR in qs selectBlockSynteny unrecognized class for object sent");
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs selectBlockSynteny unrecognized class for object sent = "+siIT.getAbsoluteProportionQComparableQSSpanItem().getClass().toString()));
			}
			// deal with previous and next s
			if (!selectedPreviousSOfNextSliceSent
					&& !selectedNextSOfPreviousSliceSent && !redrawOnTopSent
					&& newIdxInArrayOfThisSlice > -1) {

				// draw previous and next s
				ArrayList<AbsoPropSCompaSSpanItem> alApscssiIT = new ArrayList<AbsoPropSCompaSSpanItem>();
				ArrayList<Integer> alIntIdxSliceInDisplay = new ArrayList<Integer>();
				boolean dontDrawSRegionTwice = false;
				if (((AbsoPropQSSyntItem) siIT
						.getAbsoluteProportionQComparableQSSpanItem())
						.getPreviousGenomicRegionSInsertion() != null) {
					alApscssiIT.add(((AbsoPropQSSyntItem) siIT
							.getAbsoluteProportionQComparableQSSpanItem())
							.getPreviousGenomicRegionSInsertion());
					alIntIdxSliceInDisplay.add(-1);
					if (((AbsoPropQSSyntItem) siIT
							.getAbsoluteProportionQComparableQSSpanItem())
							.getPreviousGenomicRegionSInsertion()
							.isAnchoredToPrevious()) {
						dontDrawSRegionTwice = true;
					}
				}
				if (!dontDrawSRegionTwice) {
					if (((AbsoPropQSSyntItem) siIT
							.getAbsoluteProportionQComparableQSSpanItem())
							.getNextGenomicRegionSInsertion() != null) {
						alApscssiIT.add(((AbsoPropQSSyntItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getNextGenomicRegionSInsertion());
						alIntIdxSliceInDisplay.add(1);
					}
				}
				for (int p = 0; p < alApscssiIT.size(); p++) {
					AbsoPropSCompaSSpanItem apscssiIT = alApscssiIT
							.get(p);
					int idxSliceInDisplayIT = -1;
					if (alIntIdxSliceInDisplay.get(p) > 0) {
						idxSliceInDisplayIT = sliceXClicked + 1;
						contextBgk.translate(SLICE_CANVAS_WIDTH, 0);
					} else {
						idxSliceInDisplayIT = sliceXClicked - 1;
						contextBgk.translate(-SLICE_CANVAS_WIDTH, 0);
					}
					if (apscssiIT instanceof AbsoPropSGenoRegiInserItem) {
						// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
						// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).getSGenomicRegionInsertionNumberGene(),
						// false, false);
						int numberGenes = ((AbsoPropSGenoRegiInserItem) apscssiIT)
								.getSGenomicRegionInsertionNumberGene();
						if (numberGenes < 0) {
							int sizePb = 1
									+ ((AbsoPropSGenoRegiInserItem) apscssiIT)
											.getsPbStopSGenomicRegionInsertionInElement()
									- ((AbsoPropSGenoRegiInserItem) apscssiIT)
											.getsPbStartSGenomicRegionInsertionInElement();

							// System.err.println("here1");

							drawBlockSGenomicRegionInsertionOrMissingTarget(
									apscssiIT.getSEnumBlockType(),
									convertSizeBpToTextWithAppropriateUnit(sizePb),
									false,
									false,
									true,
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnchoredToPrevious(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnchoredToNext(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnnotationCollision(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnchoredToStartElement(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnchoredToStopElement(),
									numberOtherMatch);

							if (allowToFetchMoreData) {
								
//								@SuppressWarnings("unused")
//								ResultLoadingDialog rld = new ResultLoadingDialog(
//										DetailledInfoStack.GeneCountForSGenomicRegionInsertion,
//										siIT.getIndexInFullArray()
//												+ alIntIdxSliceInDisplay.get(p),
//										(associatedGenomePanel
//												.getGenomePanelItem()
//												.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + idxSliceInDisplayIT),
//										idxSliceInDisplayIT,
//										Insyght.APP_CONTROLER
//												.getLIST_GENOME_PANEL()
//												.indexOf(associatedGenomePanel),
//										associatedGenomePanel
//												.getGenomePanelItem()
//												.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//										CenterSLPAllGenomePanels.START_INDEX,
//										associatedGenomePanel
//												.getGenomePanelItem()
//												.getPercentSyntenyShowStartQGenome(),
//										associatedGenomePanel
//												.getGenomePanelItem()
//												.getPercentSyntenyShowStopQGenome(),
//										associatedGenomePanel
//												.getGenomePanelItem()
//												.getPercentSyntenyShowStartSGenome(),
//										associatedGenomePanel
//												.getGenomePanelItem()
//												.getPercentSyntenyShowStopSGenome(),
//										((AbsoPropSGenoRegiInserItem) apscssiIT)
//												.getsOrigamiElementId(),
//										((AbsoPropSGenoRegiInserItem) apscssiIT)
//												.getsPbStartSGenomicRegionInsertionInElement(),
//										((AbsoPropSGenoRegiInserItem) apscssiIT)
//												.getsPbStopSGenomicRegionInsertionInElement(),
//										apscssiIT.getSEnumBlockType(),
//										false,
//										false,
//										Insyght.APP_CONTROLER
//												.getCurrentRefGenomePanelAndListOrgaResult()
//												.getViewTypeInsyght(),
//										((AbsoPropSGenoRegiInserItem) apscssiIT)
//												.isPreviousSOfNextSlice(),
//										((AbsoPropSGenoRegiInserItem) apscssiIT)
//												.isNextSOfPreviousSlice());
								RLD.getGeneCountForSGenomicRegionInsertion(siIT.getIndexInFullArray()
												+ alIntIdxSliceInDisplay.get(p),
										(associatedGenomePanel
												.getGenomePanelItem()
												.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + idxSliceInDisplayIT),
										idxSliceInDisplayIT,
										Insyght.APP_CONTROLER
												.getLIST_GENOME_PANEL()
												.indexOf(associatedGenomePanel),
										associatedGenomePanel
												.getGenomePanelItem()
												.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
										CenterSLPAllGenomePanels.START_INDEX,
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStartQGenome(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStopQGenome(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStartSGenome(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStopSGenome(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getsOrigamiElementId(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getsPbStartSGenomicRegionInsertionInElement(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getsPbStopSGenomicRegionInsertionInElement(),
										apscssiIT.getSEnumBlockType(),
										false,
										false,
										Insyght.APP_CONTROLER
												.getCurrentRefGenomePanelAndListOrgaResult()
												.getViewTypeInsyght(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isPreviousSOfNextSlice(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isNextSOfPreviousSlice()
										);
								


								
							}

						} else {
							if (numberGenes == 0) {
								int sizePb = 1
										+ ((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getsPbStopSGenomicRegionInsertionInElement()
										- ((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getsPbStartSGenomicRegionInsertionInElement();
								// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
								// sizePb+" bp", false, false);
								// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
								// false, false,
								// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
								// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
								drawBlockSGenomicRegionInsertionOrMissingTarget(
										apscssiIT.getSEnumBlockType(),
										convertSizeBpToTextWithAppropriateUnit(sizePb),
										false,
										false,
										true,
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToPrevious(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToNext(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnnotationCollision(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToStartElement(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToStopElement(),
										numberOtherMatch);
							} else {
								drawBlockSGenomicRegionInsertionOrMissingTarget(
										apscssiIT.getSEnumBlockType(),
										numberGenes + " CDS",
										false,
										false,
										false,
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToPrevious(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToNext(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnnotationCollision(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToStartElement(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToStopElement(),
										numberOtherMatch);
							}
						}
					}
					if (alIntIdxSliceInDisplay.get(p) > 0) {
						contextBgk.translate(-SLICE_CANVAS_WIDTH, 0);
					} else {
						contextBgk.translate(SLICE_CANVAS_WIDTH, 0);
					}
				}

			}

			restoreToOriginCoordinateSpace();

			// RQ : if change here change in AppControler ->
			// clearSyntenyCanvasSelection too
			if (associatedGenomePanel.getGenomePanelItem().getSizeState()
					.compareTo(GenomePanelItem.GpiSizeState.MAX) == 0) {
				// set style bottom
				contextBgk.translate(0, TOTAL_CANVAS_HEIGHT_UPPER_PART);

				// selectedMainQSSynteny, selectedPreviousSOfNextSlice,
				// selectedNextSOfPreviousSlice
				if (siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem
						|| siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {

					double relPercQPercentStart = claculateRelativePercentage(
							Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getReferenceGenomePanelItem()
									.getListAbsoluteProportionElementItemQ()
									.get(0).getQsizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSSpanItem()
									.getqPercentStart(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopQGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartQGenome());
					double relPercQPercentStop = claculateRelativePercentage(
							Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getReferenceGenomePanelItem()
									.getListAbsoluteProportionElementItemQ()
									.get(0).getQsizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSSpanItem()
									.getqPercentStop(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopQGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartQGenome());
					double relPercSPercentStart = claculateRelativePercentage(
							associatedGenomePanel.getGenomePanelItem()
									.getListAbsoluteProportionElementItemS()
									.get(0).getsSizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSSpanItem()
									.getsPercentStart(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopSGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartSGenome());
					double relPercSPercentStop = claculateRelativePercentage(
							associatedGenomePanel.getGenomePanelItem()
									.getListAbsoluteProportionElementItemS()
									.get(0).getsSizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSSpanItem()
									.getsPercentStop(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopSGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartSGenome());

					// System.out.println("relPercQPercentStart="+relPercQPercentStart+";relPercQPercentStop="+relPercQPercentStop
					// +";relPercSPercentStart="+relPercSPercentStart+";relPercSPercentStop="+relPercSPercentStop);

					drawSyntenyRepersentationProportionateQS(
							siIT.getAbsoluteProportionQComparableQSSpanItem(),
							relPercQPercentStart, relPercQPercentStop,
							relPercSPercentStart, relPercSPercentStop,
							redrawOnTopSent, inRedSent, true);

					// drawScaffoldGenome(true, siIT.getqPercentStart(),
					// siIT.getqPercentStop(), true, true, false);
					drawScaffoldGenome(true, relPercQPercentStart,
							relPercQPercentStop, true, inRedSent, false);

					// drawScaffoldGenome(false, siIT.getsPercentStart(),
					// siIT.getsPercentStop(), true, true, false);
					drawScaffoldGenome(false, relPercSPercentStart,
							relPercSPercentStop, true, inRedSent, false);

					// drawMarkerScaffold(blockTypeIT, siIT.getqPercentStart(),
					// siIT.getqPercentStop(), siIT.getsPercentStart(),
					// siIT.getsPercentStop(), true, true, false);
					String colorBgkIT = "";
					if (siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
						colorBgkIT = getBkgColorAssociatedWithColorIDAsString(((AbsoPropQSGeneHomoItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getQsStyleItem().getBkgColor());
					} else {
						colorBgkIT = getDefaultBkgColorAccordingToQSBlockType(siIT
								.getAbsoluteProportionQComparableQSSpanItem()
								.getQsEnumBlockType());
					}

					drawSyntenyMarkerScaffold(true, true, relPercQPercentStart,
							relPercQPercentStop, relPercSPercentStart,
							relPercSPercentStop, redrawOnTopSent, inRedSent,
							false, colorBgkIT);

				} else {
					//System.err.println("ERROR in drawSyntenyRepersentationProportionateQS qs selectBlockSynteny unrecognized class for object sent");
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in drawSyntenyRepersentationProportionateQS qs selectBlockSynteny unrecognized class for object sent = "+siIT.getClass().toString()));
				}

				restoreToOriginCoordinateSpace();
			}

			// select in app controler and show info
			if (siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {

				Insyght.APP_CONTROLER
						.setCurrSelectedSyntenyCanvasItemRefId("GENE_MATCH_"
								+ ((AbsoPropQSGeneHomoItem) siIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getQsQGeneId()
								+ "_VS_"
								+ ((AbsoPropQSGeneHomoItem) siIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getQsSGeneId());
				AbsoPropQElemItem tmpApqeiIT = null;
				AbsoPropSElemItem tmpApseiIT = null;
				for (int m = 0; m < associatedGenomePanel.getGenomePanelItem()
						.getListAbsoluteProportionElementItemQ().size(); m++) {
					AbsoPropQElemItem apqeiIT = associatedGenomePanel
							.getGenomePanelItem()
							.getListAbsoluteProportionElementItemQ().get(m);
					if (apqeiIT
							.getqAccnum()
							.compareTo(
									((AbsoPropQSElemItem) siIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getQsQAccnum()) == 0) {
						tmpApqeiIT = apqeiIT;
						break;
					}
				}
				for (int m = 0; m < associatedGenomePanel.getGenomePanelItem()
						.getListAbsoluteProportionElementItemS().size(); m++) {
					AbsoPropSElemItem apseiIT = associatedGenomePanel
							.getGenomePanelItem()
							.getListAbsoluteProportionElementItemS().get(m);
					if (apseiIT.getsAccnum().compareTo(
									((AbsoPropQSElemItem) siIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getQsSAccnum()) == 0) {
						tmpApseiIT = apseiIT;
						break;
					}
				}
				// ResultViewImpl.fillElementInfoInStack((AbsoluteProportionQSElementsItem)siIT.getAbsoluteProportionQComparableQSSpanItem(),
				// (AbsoluteProportionQSElementsItem)siIT.getAbsoluteProportionQComparableQSSpanItem(),
				// false, false);
				GenoOrgaAndHomoTablViewImpl.fillElementInfoInStack(
						tmpApqeiIT, tmpApseiIT, false, true);
				GenoOrgaAndHomoTablViewImpl.fillSyntenyInfoInStack(
						(AbsoPropQSSyntItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem(),
						false, true);

				GenoOrgaAndHomoTablViewImpl.fillGeneInfoInStack(
						(AbsoPropQSGeneHomoItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem(),
						true, true);
				GenoOrgaAndHomoTablViewImpl.fillGeneMatchInfoInStack(
						(AbsoPropQSGeneHomoItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem()
								//, true
								);
				// }

			} else if (siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
				// selectedMainQSSynteny, selectedPreviousSOfNextSlice,
				// selectedNextSOfPreviousSlice
				// if(selectedMainQS){
				Insyght.APP_CONTROLER
						.setCurrSelectedSyntenyCanvasItemRefId("SYNTENY_QSTART_"
								+ ((AbsoPropQSSyntItem) siIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getPbQStartSyntenyInElement()
								+ "_QSTOP_"
								+ ((AbsoPropQSSyntItem) siIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getPbQStopSyntenyInElement()
								+ "_SSTART_"
								+ ((AbsoPropQSSyntItem) siIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getPbSStartSyntenyInElement()
								+ "_SSTOP_"
								+ ((AbsoPropQSSyntItem) siIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getPbSStopSyntenyInElement());
				AbsoPropQElemItem tmpApqeiIT = null;
				AbsoPropSElemItem tmpApseiIT = null;
				for (int m = 0; m < associatedGenomePanel.getGenomePanelItem()
						.getListAbsoluteProportionElementItemQ().size(); m++) {
					AbsoPropQElemItem apqeiIT = associatedGenomePanel
							.getGenomePanelItem()
							.getListAbsoluteProportionElementItemQ().get(m);
					if (apqeiIT
							.getqAccnum()
							.compareTo(
									((AbsoPropQSElemItem) siIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getQsQAccnum()) == 0) {
						tmpApqeiIT = apqeiIT;
						break;
					}
				}
				for (int m = 0; m < associatedGenomePanel.getGenomePanelItem()
						.getListAbsoluteProportionElementItemS().size(); m++) {
					AbsoPropSElemItem apseiIT = associatedGenomePanel
							.getGenomePanelItem()
							.getListAbsoluteProportionElementItemS().get(m);
					if (apseiIT
							.getsAccnum()
							.compareTo(
									((AbsoPropQSElemItem) siIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getQsSAccnum()) == 0) {
						tmpApseiIT = apseiIT;
						break;
					}
				}
				// ResultViewImpl.fillElementInfoInStack((AbsoluteProportionQSElementsItem)siIT.getAbsoluteProportionQComparableQSSpanItem(),
				// (AbsoluteProportionQSElementsItem)siIT.getAbsoluteProportionQComparableQSSpanItem(),
				// false, false);
				GenoOrgaAndHomoTablViewImpl.fillElementInfoInStack(
						tmpApqeiIT, tmpApseiIT, false, true);
				GenoOrgaAndHomoTablViewImpl.fillSyntenyInfoInStack(
						(AbsoPropQSSyntItem) siIT
								.getAbsoluteProportionQComparableQSSpanItem(),
						true, true);

			} else {
				//System.err.println("ERROR in show info qs selectBlockSynteny unrecognized class for object sent");
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in show info qs selectBlockSynteny unrecognized class for object sent = "+siIT.getAbsoluteProportionQComparableQSSpanItem().getClass().toString()));
			}

		} else {
			if (siIT.getAbsoluteProportionQComparableQSpanItem() != null
					&& selectedQ) {
				// set style upper
				// save to be able to go back to initial canvas state
				restoreToOriginCoordinateSpace();
				contextBgk.translate(addToLeftToCenterDrawing
						+ (SLICE_CANVAS_WIDTH * selectedSliceX), 0);

				if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
					drawBlockQGeneInsertionOrMissingTarget(
							(AbsoPropQGeneInserItem) siIT
									.getAbsoluteProportionQComparableQSpanItem(),
							redrawOnTopSent, inRedSent, numberOtherMatch,
							false, -1, -1);
				} else if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
					int numberGenes = ((AbsoPropQGenoRegiInserItem) siIT
							.getAbsoluteProportionQComparableQSpanItem())
							.getQGenomicRegionInsertionNumberGene();
					if (numberGenes <= 0) {
						int sizePb = 1
								+ ((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqPbStopQGenomicRegionInsertionInElement()
								- ((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqPbStartQGenomicRegionInsertionInElement();
						// System.err.println("here");
						// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
						// redrawOnTopSent, inRedSent);
						drawBlockQGenomicRegionInsertionOrMissingTarget(
								siIT.getAbsoluteProportionQComparableQSpanItem()
										.getqEnumBlockType(),
										convertSizeBpToTextWithAppropriateUnit(sizePb),
								redrawOnTopSent,
								inRedSent,
								true,
								((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.isAnnotationCollision(),
								((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.isAnchoredToStartElement(),
								((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.isAnchoredToStopElement(),
								numberOtherMatch);
						
					} else {
						drawBlockQGenomicRegionInsertionOrMissingTarget(
								siIT.getAbsoluteProportionQComparableQSpanItem()
										.getqEnumBlockType(),
								numberGenes + " CDS",
								redrawOnTopSent,
								inRedSent,
								false,
								((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.isAnnotationCollision(),
								((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.isAnchoredToStartElement(),
								((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.isAnchoredToStopElement(),
								numberOtherMatch);
					}
				} else if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem) {
					int numberGenes = ((AbsoPropQElemItem) siIT
							.getAbsoluteProportionQComparableQSpanItem())
							.getqElementNumberGene();
					if (numberGenes <= 0) {
						int sizePb = 1 + ((AbsoPropQElemItem) siIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getQsizeOfElementinPb();
						// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
						// redrawOnTopSent, inRedSent);
						drawBlockQGenomicRegionInsertionOrMissingTarget(siIT
								.getAbsoluteProportionQComparableQSpanItem()
								.getqEnumBlockType(), convertSizeBpToTextWithAppropriateUnit(sizePb),
								redrawOnTopSent, inRedSent, true, false, false,
								false, numberOtherMatch);

						
					} else {
						drawBlockQGenomicRegionInsertionOrMissingTarget(siIT
								.getAbsoluteProportionQComparableQSpanItem()
								.getqEnumBlockType(), numberGenes + " CDS",
								redrawOnTopSent, inRedSent, false, false,
								false, false, numberOtherMatch);
					}
				} else {
					//System.err.println("ERROR in q selectBlockSynteny unrecognized class for object sent");
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in q selectBlockSynteny unrecognized class for object sent = "+siIT.getAbsoluteProportionQComparableQSpanItem().getClass().toString()));
				}
				restoreToOriginCoordinateSpace();

				if (redrawSymbolicReprOfOppositeQorSInBlackUnselected) {
					if (siIT.getAbsoluteProportionSComparableSSpanItem() != null) {
						restoreToOriginCoordinateSpace();
						contextBgk.translate(addToLeftToCenterDrawing
								+ (SLICE_CANVAS_WIDTH * selectedSliceX), 0);
						if (siIT.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
							drawBlockSGeneInsertionOrMissingTarget(
									(AbsoPropSGeneInserItem) siIT
											.getAbsoluteProportionSComparableSSpanItem(),
									redrawOnTopSent, false, numberOtherMatch,
									false, -1, -1);
						} else if (siIT
								.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
							// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
							// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).getSGenomicRegionInsertionNumberGene(),
							// redrawOnTopSent, inRedSent);
							int numberGenes = ((AbsoPropSGenoRegiInserItem) siIT
									.getAbsoluteProportionSComparableSSpanItem())
									.getSGenomicRegionInsertionNumberGene();

							if (numberGenes < 0) {
								int sizePb = 1
										+ ((AbsoPropSGenoRegiInserItem) siIT
												.getAbsoluteProportionSComparableSSpanItem())
												.getsPbStopSGenomicRegionInsertionInElement()
										- ((AbsoPropSGenoRegiInserItem) siIT
												.getAbsoluteProportionSComparableSSpanItem())
												.getsPbStartSGenomicRegionInsertionInElement();
								// System.err.println("i full="+i);
								drawBlockSGenomicRegionInsertionOrMissingTarget(
										siIT.getAbsoluteProportionSComparableSSpanItem()
												.getSEnumBlockType(),
												convertSizeBpToTextWithAppropriateUnit(sizePb),
										redrawOnTopSent,
										false,
										true,
										((AbsoPropSGenoRegiInserItem) siIT
												.getAbsoluteProportionSComparableSSpanItem())
												.isAnchoredToPrevious(),
										((AbsoPropSGenoRegiInserItem) siIT
												.getAbsoluteProportionSComparableSSpanItem())
												.isAnchoredToNext(),
										((AbsoPropSGenoRegiInserItem) siIT
												.getAbsoluteProportionSComparableSSpanItem())
												.isAnnotationCollision(),
										((AbsoPropSGenoRegiInserItem) siIT
												.getAbsoluteProportionSComparableSSpanItem())
												.isAnchoredToStartElement(),
										((AbsoPropSGenoRegiInserItem) siIT
												.getAbsoluteProportionSComparableSSpanItem())
												.isAnchoredToStopElement(),
										numberOtherMatch);

							} else {
								if (numberGenes == 0) {
									int sizePb = 1
											+ ((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.getsPbStopSGenomicRegionInsertionInElement()
											- ((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.getsPbStartSGenomicRegionInsertionInElement();
									// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
									// sizePb+" bp", false, false);
									// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
									// false, false,
									// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
									// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
									drawBlockSGenomicRegionInsertionOrMissingTarget(
											siIT.getAbsoluteProportionSComparableSSpanItem()
													.getSEnumBlockType(),
											convertSizeBpToTextWithAppropriateUnit(sizePb),
											redrawOnTopSent,
											false,
											true,
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnchoredToPrevious(),
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnchoredToNext(),
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnnotationCollision(),
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnchoredToStartElement(),
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnchoredToStopElement(),
											numberOtherMatch);

								} else {
									drawBlockSGenomicRegionInsertionOrMissingTarget(
											siIT.getAbsoluteProportionSComparableSSpanItem()
													.getSEnumBlockType(),
											numberGenes + " CDS",
											redrawOnTopSent,
											false,
											false,
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnchoredToPrevious(),
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnchoredToNext(),
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnnotationCollision(),
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnchoredToStartElement(),
											((AbsoPropSGenoRegiInserItem) siIT
													.getAbsoluteProportionSComparableSSpanItem())
													.isAnchoredToStopElement(),
											numberOtherMatch);
								}
							}

						} else if (siIT.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSElemItem) {
							// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
							// ((AbsoluteProportionSElementItem)apscssiIT).getsElementNumberGene(),
							// redrawOnTopSent, inRedSent);
							int numberGenes = ((AbsoPropSElemItem) siIT
									.getAbsoluteProportionSComparableSSpanItem())
									.getsElementNumberGene();
							if (numberGenes <= 0) {
								int sizePb = 1 + ((AbsoPropSElemItem) siIT
										.getAbsoluteProportionSComparableSSpanItem())
										.getsSizeOfElementinPb();
								drawBlockSGenomicRegionInsertionOrMissingTarget(
										siIT.getAbsoluteProportionSComparableSSpanItem()
												.getSEnumBlockType(),
												convertSizeBpToTextWithAppropriateUnit(sizePb),
												redrawOnTopSent,
										false, true, true, true, false, false,
										false, numberOtherMatch);
								// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
								// redrawOnTopSent, inRedSent);
							} else {
								drawBlockSGenomicRegionInsertionOrMissingTarget(
										siIT.getAbsoluteProportionSComparableSSpanItem()
												.getSEnumBlockType(),
										numberGenes + " CDS", redrawOnTopSent,
										false, false, false, false, false,
										false, false, numberOtherMatch);
							}
						} else {
							//System.err.println("ERROR in s selectBlockSynteny unrecognized class for object sent");
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in s selectBlockSynteny unrecognized class for object sent = "+siIT.getAbsoluteProportionSComparableSSpanItem()));
						}
						restoreToOriginCoordinateSpace();
					}
				}

				// RQ : if change here change in AppControler ->
				// clearSyntenyCanvasSelection too
				if (associatedGenomePanel.getGenomePanelItem().getSizeState()
						.compareTo(GenomePanelItem.GpiSizeState.MAX) == 0) {
					// set style bottom
					contextBgk.translate(0, TOTAL_CANVAS_HEIGHT_UPPER_PART);
					if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem
							|| siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem
							|| siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
						drawGenomicRegionRepersentationProportionateQ(
								siIT.getAbsoluteProportionQComparableQSpanItem(),
								claculateRelativePercentage(
										Insyght.APP_CONTROLER
												.getCurrentRefGenomePanelAndListOrgaResult()
												.getReferenceGenomePanelItem()
												.getListAbsoluteProportionElementItemQ()
												.get(0)
												.getQsizeOfOragnismInPb(),
										siIT.getAbsoluteProportionQComparableQSpanItem()
												.getqPercentStart(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStopQGenome(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStartQGenome()),
								claculateRelativePercentage(
										Insyght.APP_CONTROLER
												.getCurrentRefGenomePanelAndListOrgaResult()
												.getReferenceGenomePanelItem()
												.getListAbsoluteProportionElementItemQ()
												.get(0)
												.getQsizeOfOragnismInPb(),
										siIT.getAbsoluteProportionQComparableQSpanItem()
												.getqPercentStop(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStopQGenome(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStartQGenome()),
								redrawOnTopSent, inRedSent, true);
					} else {
						//System.err.println("ERROR in drawSyntenyRepersentationProportionateQ q selectBlockSynteny unrecognized class for object sent");
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in drawSyntenyRepersentationProportionateQ q" +
								" selectBlockSynteny unrecognized class for object sent = "+siIT.getAbsoluteProportionQComparableQSpanItem().getClass().toString()));
					}

					// drawScaffoldGenome(true, siIT.getqPercentStart(),
					// siIT.getqPercentStop(), true, true, false);
					drawScaffoldGenome(
							true,
							claculateRelativePercentage(
									Insyght.APP_CONTROLER
											.getCurrentRefGenomePanelAndListOrgaResult()
											.getReferenceGenomePanelItem()
											.getListAbsoluteProportionElementItemQ()
											.get(0).getQsizeOfOragnismInPb(),
									siIT.getAbsoluteProportionQComparableQSpanItem()
											.getqPercentStart(),
									associatedGenomePanel.getGenomePanelItem()
											.getPercentSyntenyShowStopQGenome(),
									associatedGenomePanel
											.getGenomePanelItem()
											.getPercentSyntenyShowStartQGenome()),
							claculateRelativePercentage(
									Insyght.APP_CONTROLER
											.getCurrentRefGenomePanelAndListOrgaResult()
											.getReferenceGenomePanelItem()
											.getListAbsoluteProportionElementItemQ()
											.get(0).getQsizeOfOragnismInPb(),
									siIT.getAbsoluteProportionQComparableQSpanItem()
											.getqPercentStop(),
									associatedGenomePanel.getGenomePanelItem()
											.getPercentSyntenyShowStopQGenome(),
									associatedGenomePanel
											.getGenomePanelItem()
											.getPercentSyntenyShowStartQGenome()),
							true, inRedSent, false);

					String colorBgkIT = "";
					if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
						colorBgkIT = getBkgColorAssociatedWithColorIDAsString(((AbsoPropQGeneInserItem) siIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqStyleItem().getBkgColor());
					} else {
						colorBgkIT = getDefaultBkgColorAccordingToQBlockType(siIT
								.getAbsoluteProportionQComparableQSpanItem()
								.getqEnumBlockType());
					}

					// drawMarkerScaffold(blockTypeIT, siIT.getqPercentStart(),
					// siIT.getqPercentStop(), siIT.getsPercentStart(),
					// siIT.getsPercentStop(), true, true, false);
					drawSyntenyMarkerScaffold(
							true,
							false,
							claculateRelativePercentage(
									Insyght.APP_CONTROLER
											.getCurrentRefGenomePanelAndListOrgaResult()
											.getReferenceGenomePanelItem()
											.getListAbsoluteProportionElementItemQ()
											.get(0).getQsizeOfOragnismInPb(),
									siIT.getAbsoluteProportionQComparableQSpanItem()
											.getqPercentStart(),
									associatedGenomePanel.getGenomePanelItem()
											.getPercentSyntenyShowStopQGenome(),
									associatedGenomePanel
											.getGenomePanelItem()
											.getPercentSyntenyShowStartQGenome()),
							claculateRelativePercentage(
									Insyght.APP_CONTROLER
											.getCurrentRefGenomePanelAndListOrgaResult()
											.getReferenceGenomePanelItem()
											.getListAbsoluteProportionElementItemQ()
											.get(0).getQsizeOfOragnismInPb(),
									siIT.getAbsoluteProportionQComparableQSpanItem()
											.getqPercentStop(),
									associatedGenomePanel.getGenomePanelItem()
											.getPercentSyntenyShowStopQGenome(),
									associatedGenomePanel
											.getGenomePanelItem()
											.getPercentSyntenyShowStartQGenome()),
							-1, -1, redrawOnTopSent, inRedSent, false,
							colorBgkIT);

					restoreToOriginCoordinateSpace();
				}

				// select in app controler and show info
				// ResultViewImpl.fillBasicSyntenyInfoInStack(associatedGenomePanel.getGenomePanelItem().getPartialListAbsoluteProportionSyntenyItem().get(indexStartBirdSynteny+selectedSliceX).getAbsoluteProportionQSyntenyItem().getSyntenyItemId(),
				// true);
				if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {

					Insyght.APP_CONTROLER
							.setCurrSelectedSyntenyCanvasItemRefId("Q_GENE_INSERTION_"
									+ ((AbsoPropQGeneInserItem) siIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getQGeneId());
					// find right AbsoluteProportionQElementItem
					// for(int
					// l=0;l<associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemQ().size();l++){
					for (int m = 0; m < associatedGenomePanel
							.getGenomePanelItem()
							.getListAbsoluteProportionElementItemQ().size(); m++) {
						AbsoPropQElemItem apqeiIT = associatedGenomePanel
								.getGenomePanelItem()
								.getListAbsoluteProportionElementItemQ().get(m);
						if (apqeiIT
								.getqAccnum()
								.compareTo(
										((AbsoPropQGeneInserItem) siIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqAccnum()) == 0) {
							GenoOrgaAndHomoTablViewImpl
									.fillElementInfoInStack(apqeiIT, false,
											true);
							break;
						}
					}
					GenoOrgaAndHomoTablViewImpl
							.fillGeneInfoInStack(
									(AbsoPropQGeneInserItem) siIT
											.getAbsoluteProportionQComparableQSpanItem(),
									true, true);
				} else if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {

					Insyght.APP_CONTROLER
							.setCurrSelectedSyntenyCanvasItemRefId("Q_GENOMIC_REGION_INSERTION_Q_START_"
									+ ((AbsoPropQGenoRegiInserItem) siIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getqPbStartQGenomicRegionInsertionInElement()
									+ "_Q_STOP_"
									+ ((AbsoPropQGenoRegiInserItem) siIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getqPbStopQGenomicRegionInsertionInElement());
					// ResultViewImpl.fillElementInfoInStack((AbsoluteProportionQElementItem)siIT.getAbsoluteProportionQComparableQSpanItem(),
					// false, false);
					for (int m = 0; m < associatedGenomePanel
							.getGenomePanelItem()
							.getListAbsoluteProportionElementItemQ().size(); m++) {
						AbsoPropQElemItem apqeiIT = associatedGenomePanel
								.getGenomePanelItem()
								.getListAbsoluteProportionElementItemQ().get(m);
						if (apqeiIT
								.getqAccnum()
								.compareTo(
										((AbsoPropQGenoRegiInserItem) siIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqAccnum()) == 0) {
							GenoOrgaAndHomoTablViewImpl
									.fillElementInfoInStack(apqeiIT, false,
											true);
							break;
						}
					}
					GenoOrgaAndHomoTablViewImpl
							.fillQGenomicRegionInfoInStack(
									(AbsoPropQGenoRegiInserItem) siIT
											.getAbsoluteProportionQComparableQSpanItem(),
									true);
				} else if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem) {

					Insyght.APP_CONTROLER
							.setCurrSelectedSyntenyCanvasItemRefId("Q_ELEMENT_"
									+ ((AbsoPropQElemItem) siIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getqPbStartOfElementInOrga()
									+ "_SIZE_"
									+ ((AbsoPropQElemItem) siIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getQsizeOfElementinPb());
					GenoOrgaAndHomoTablViewImpl
							.fillElementInfoInStack(
									(AbsoPropQElemItem) siIT
											.getAbsoluteProportionQComparableQSpanItem(),
									true, true);
				} else {
					//System.err.println("ERROR in show info q selectBlockSynteny unrecognized class for object sent");
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in show info q selectBlockSynteny unrecognized class for object sent = "+siIT.getAbsoluteProportionQComparableQSpanItem().getClass().toString()));
				}

			} else if (!selectedQ) {

				// check if siIT.getAbsoluteProportionSComparableSSpanItem() !=
				// null ou previous/next genomic insertion
				AbsoPropSCompaSSpanItem apscssiIT = null;
				if (siIT.getAbsoluteProportionSComparableSSpanItem() != null) {
					apscssiIT = siIT
							.getAbsoluteProportionSComparableSSpanItem();

					if (redrawSymbolicReprOfOppositeQorSInBlackUnselected) {
						restoreToOriginCoordinateSpace();
						contextBgk.translate(addToLeftToCenterDrawing
								+ (SLICE_CANVAS_WIDTH * selectedSliceX), 0);

						if (siIT.getAbsoluteProportionQComparableQSpanItem() != null) {
							// set style upper
							// save to be able to go back to initial canvas
							// state

							if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
								drawBlockQGeneInsertionOrMissingTarget(
										(AbsoPropQGeneInserItem) siIT
												.getAbsoluteProportionQComparableQSpanItem(),
										redrawOnTopSent, false,
										numberOtherMatch, false, -1, -1);
							} else if (siIT
									.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
								int numberGenes = ((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getQGenomicRegionInsertionNumberGene();
								if (numberGenes <= 0) {
									int sizePb = 1
											+ ((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqPbStopQGenomicRegionInsertionInElement()
											- ((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqPbStartQGenomicRegionInsertionInElement();
									// System.err.println("here");
									drawBlockQGenomicRegionInsertionOrMissingTarget(
											siIT.getAbsoluteProportionQComparableQSpanItem()
													.getqEnumBlockType(),
											convertSizeBpToTextWithAppropriateUnit(sizePb),
											redrawOnTopSent,
											false,
											true,
											((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.isAnnotationCollision(),
											((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.isAnchoredToStartElement(),
											((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.isAnchoredToStopElement(),
											numberOtherMatch);
								} else {
									drawBlockQGenomicRegionInsertionOrMissingTarget(
											siIT.getAbsoluteProportionQComparableQSpanItem()
													.getqEnumBlockType(),
											numberGenes + " CDS",
											redrawOnTopSent,
											false,
											false,
											((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.isAnnotationCollision(),
											((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.isAnchoredToStartElement(),
											((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.isAnchoredToStopElement(),
											numberOtherMatch);
								}

							} else if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem) {
								int numberGenes = ((AbsoPropQElemItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqElementNumberGene();
								if (numberGenes <= 0) {
									int sizePb = 1 + ((AbsoPropQElemItem) siIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getQsizeOfElementinPb();
									// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
									// redrawOnTopSent, inRedSent);
									drawBlockQGenomicRegionInsertionOrMissingTarget(
											siIT.getAbsoluteProportionQComparableQSpanItem()
													.getqEnumBlockType(),
											convertSizeBpToTextWithAppropriateUnit(sizePb),
											redrawOnTopSent,
											false, true, false, false, false,
											numberOtherMatch);
								} else {
									drawBlockQGenomicRegionInsertionOrMissingTarget(
											siIT.getAbsoluteProportionQComparableQSpanItem()
													.getqEnumBlockType(),
											numberGenes + " CDS",
											redrawOnTopSent, false, false,
											false, false, false,
											numberOtherMatch);
								}
							} else {
								//System.err.println("ERROR in s redrawSymbolicReprOfOppositeQorSInBlackUnselected selectBlockSynteny unrecognized class for object sent");
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in s redrawSymbolicReprOfOppositeQorSInBlackUnselected " +
										"selectBlockSynteny unrecognized class for object sent = "+siIT.getAbsoluteProportionQComparableQSpanItem().getClass().toString()));
							}

						}

						// deal with previous and next s insertion
						if (apscssiIT instanceof AbsoPropSGenoRegiInserItem) {
							AbsoPropSGenoRegiInserItem previousGenomicRegionSInsertion = ((AbsoPropSGenoRegiInserItem) apscssiIT)
									.getsIfMissingTargetPreviousGenomicRegionSInsertion();
							if (previousGenomicRegionSInsertion != null) {

								contextBgk.translate(-SLICE_CANVAS_WIDTH, 0);
								int numberGenes = previousGenomicRegionSInsertion
										.getSGenomicRegionInsertionNumberGene();
								if (numberGenes < 0) {
									int sizePb = 1
											+ previousGenomicRegionSInsertion
													.getsPbStopSGenomicRegionInsertionInElement()
											- previousGenomicRegionSInsertion
													.getsPbStartSGenomicRegionInsertionInElement();
									drawBlockSGenomicRegionInsertionOrMissingTarget(
											previousGenomicRegionSInsertion
													.getSEnumBlockType(),
											convertSizeBpToTextWithAppropriateUnit(sizePb),
											false,
											false,
											true,
											previousGenomicRegionSInsertion
													.isAnchoredToPrevious(),
											previousGenomicRegionSInsertion
													.isAnchoredToNext(),
											previousGenomicRegionSInsertion
													.isAnnotationCollision(),
											previousGenomicRegionSInsertion
													.isAnchoredToStartElement(),
											previousGenomicRegionSInsertion
													.isAnchoredToStopElement(),
											-1);
								} else {
									if (numberGenes == 0) {
										int sizePb = 1
												+ previousGenomicRegionSInsertion
														.getsPbStopSGenomicRegionInsertionInElement()
												- previousGenomicRegionSInsertion
														.getsPbStartSGenomicRegionInsertionInElement();
										drawBlockSGenomicRegionInsertionOrMissingTarget(
												previousGenomicRegionSInsertion
														.getSEnumBlockType(),
												convertSizeBpToTextWithAppropriateUnit(sizePb),
												false,
												false,
												true,
												previousGenomicRegionSInsertion
														.isAnchoredToPrevious(),
												previousGenomicRegionSInsertion
														.isAnchoredToNext(),
												previousGenomicRegionSInsertion
														.isAnnotationCollision(),
												previousGenomicRegionSInsertion
														.isAnchoredToStartElement(),
												previousGenomicRegionSInsertion
														.isAnchoredToStopElement(),
												-1);
									} else {

										drawBlockSGenomicRegionInsertionOrMissingTarget(
												previousGenomicRegionSInsertion
														.getSEnumBlockType(),
												numberGenes + " CDS",
												false,
												false,
												false,
												previousGenomicRegionSInsertion
														.isAnchoredToPrevious(),
												previousGenomicRegionSInsertion
														.isAnchoredToNext(),
												previousGenomicRegionSInsertion
														.isAnnotationCollision(),
												previousGenomicRegionSInsertion
														.isAnchoredToStartElement(),
												previousGenomicRegionSInsertion
														.isAnchoredToStopElement(),
												-1);
										// Window.alert("here previous");
									}
								}
								contextBgk.translate(SLICE_CANVAS_WIDTH, 0);
							}

							AbsoPropSGenoRegiInserItem nextGenomicRegionSInsertion = ((AbsoPropSGenoRegiInserItem) apscssiIT)
									.getsIfMissingTargetNextGenomicRegionSInsertion();
							if (nextGenomicRegionSInsertion != null) {

								contextBgk.translate(SLICE_CANVAS_WIDTH, 0);
								int numberGenes = nextGenomicRegionSInsertion
										.getSGenomicRegionInsertionNumberGene();
								if (numberGenes < 0) {
									int sizePb = 1
											+ nextGenomicRegionSInsertion
													.getsPbStopSGenomicRegionInsertionInElement()
											- nextGenomicRegionSInsertion
													.getsPbStartSGenomicRegionInsertionInElement();
									drawBlockSGenomicRegionInsertionOrMissingTarget(
											nextGenomicRegionSInsertion
													.getSEnumBlockType(),
											convertSizeBpToTextWithAppropriateUnit(sizePb),
											false,
											false,
											true,
											nextGenomicRegionSInsertion
													.isAnchoredToPrevious(),
											nextGenomicRegionSInsertion
													.isAnchoredToNext(),
											nextGenomicRegionSInsertion
													.isAnnotationCollision(),
											nextGenomicRegionSInsertion
													.isAnchoredToStartElement(),
											nextGenomicRegionSInsertion
													.isAnchoredToStopElement(),
											-1);
								} else {
									if (numberGenes == 0) {
										int sizePb = 1
												+ nextGenomicRegionSInsertion
														.getsPbStopSGenomicRegionInsertionInElement()
												- nextGenomicRegionSInsertion
														.getsPbStartSGenomicRegionInsertionInElement();
										drawBlockSGenomicRegionInsertionOrMissingTarget(
												nextGenomicRegionSInsertion
														.getSEnumBlockType(),
												convertSizeBpToTextWithAppropriateUnit(sizePb),
												false,
												false,
												true,
												nextGenomicRegionSInsertion
														.isAnchoredToPrevious(),
												nextGenomicRegionSInsertion
														.isAnchoredToNext(),
												nextGenomicRegionSInsertion
														.isAnnotationCollision(),
												nextGenomicRegionSInsertion
														.isAnchoredToStartElement(),
												nextGenomicRegionSInsertion
														.isAnchoredToStopElement(),
												-1);
									} else {

										drawBlockSGenomicRegionInsertionOrMissingTarget(
												nextGenomicRegionSInsertion
														.getSEnumBlockType(),
												numberGenes + " CDS",
												false,
												false,
												false,
												nextGenomicRegionSInsertion
														.isAnchoredToPrevious(),
												nextGenomicRegionSInsertion
														.isAnchoredToNext(),
												nextGenomicRegionSInsertion
														.isAnnotationCollision(),
												nextGenomicRegionSInsertion
														.isAnchoredToStartElement(),
												nextGenomicRegionSInsertion
														.isAnchoredToStopElement(),
												-1);
										// Window.alert("here next");
									}
								}
								contextBgk.translate(-SLICE_CANVAS_WIDTH, 0);
							}
						}
						restoreToOriginCoordinateSpace();
					}

				} else {
					if (selectedPreviousSOfNextSlice) {
						// System.err.println("here");
						int idxNextSlice = associatedGenomePanel
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
								+ selectedSliceX + 1;
						if (idxNextSlice >= 0
								&& idxNextSlice < associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.size()) {
							HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(idxNextSlice)
									.getListHAPI()
									.get(associatedGenomePanel
											.getGenomePanelItem()
											.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
													idxNextSlice));
							if (siNextSlice
									.getAbsoluteProportionQComparableQSSpanItem() != null) {
								if (siNextSlice
										.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
									apscssiIT = ((AbsoPropQSSyntItem) siNextSlice
											.getAbsoluteProportionQComparableQSSpanItem())
											.getPreviousGenomicRegionSInsertion();
								}
							} else if (siNextSlice
									.getAbsoluteProportionSComparableSSpanItem() != null) {
								if (siNextSlice
										.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
									if (((AbsoPropSGenoRegiInserItem) siNextSlice
											.getAbsoluteProportionSComparableSSpanItem())
											.getSEnumBlockType().toString()
											.startsWith("S_MISSING_TARGET_Q_")) {
										apscssiIT = ((AbsoPropSGenoRegiInserItem) siNextSlice
												.getAbsoluteProportionSComparableSSpanItem())
												.getsIfMissingTargetPreviousGenomicRegionSInsertion();
									}
								} else if (siNextSlice
										.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
									if (((AbsoPropSGeneInserItem) siNextSlice
											.getAbsoluteProportionSComparableSSpanItem())
											.getSEnumBlockType().toString()
											.startsWith("S_MISSING_TARGET_Q_")) {
										apscssiIT = ((AbsoPropSGeneInserItem) siNextSlice
												.getAbsoluteProportionSComparableSSpanItem())
												.getIfMissingTargetPreviousGenomicRegionSInsertion();
									}
								}
							}
						}
					} else if (selectedNextSOfPreviousSlice) {
						int idxPreviousSlice = associatedGenomePanel
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
								+ selectedSliceX - 1;
						if (idxPreviousSlice >= 0
								&& idxPreviousSlice < associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.size()) {
							HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(idxPreviousSlice)
									.getListHAPI()
									.get(associatedGenomePanel
											.getGenomePanelItem()
											.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
													idxPreviousSlice));
							if (siNextSlice
									.getAbsoluteProportionQComparableQSSpanItem() != null) {
								if (siNextSlice
										.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
									apscssiIT = ((AbsoPropQSSyntItem) siNextSlice
											.getAbsoluteProportionQComparableQSSpanItem())
											.getNextGenomicRegionSInsertion();
								}
							} else if (siNextSlice
									.getAbsoluteProportionSComparableSSpanItem() != null) {
								if (siNextSlice
										.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
									if (((AbsoPropSGenoRegiInserItem) siNextSlice
											.getAbsoluteProportionSComparableSSpanItem())
											.getSEnumBlockType().toString()
											.startsWith("S_MISSING_TARGET_Q_")) {
										apscssiIT = ((AbsoPropSGenoRegiInserItem) siNextSlice
												.getAbsoluteProportionSComparableSSpanItem())
												.getsIfMissingTargetNextGenomicRegionSInsertion();
									}
								} else if (siNextSlice
										.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
									if (((AbsoPropSGeneInserItem) siNextSlice
											.getAbsoluteProportionSComparableSSpanItem())
											.getSEnumBlockType().toString()
											.startsWith("S_MISSING_TARGET_Q_")) {
										apscssiIT = ((AbsoPropSGeneInserItem) siNextSlice
												.getAbsoluteProportionSComparableSSpanItem())
												.getIfMissingTargetNextGenomicRegionSInsertion();
										// System.err.println("here : "+apscssiIT);
									}
								}
							}
						}
					}
				}

				if (apscssiIT != null) {

					// set style upper
					// save to be able to go back to initial canvas state
					restoreToOriginCoordinateSpace();
					contextBgk.translate(addToLeftToCenterDrawing
							+ (SLICE_CANVAS_WIDTH * selectedSliceX), 0);
					if (apscssiIT instanceof AbsoPropSGeneInserItem) {
						drawBlockSGeneInsertionOrMissingTarget(
								(AbsoPropSGeneInserItem) apscssiIT,
								redrawOnTopSent, inRedSent, numberOtherMatch,
								false, -1, -1);
					} else if (apscssiIT instanceof AbsoPropSGenoRegiInserItem) {
						// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
						// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).getSGenomicRegionInsertionNumberGene(),
						// redrawOnTopSent, inRedSent);
						int numberGenes = ((AbsoPropSGenoRegiInserItem) apscssiIT)
								.getSGenomicRegionInsertionNumberGene();

						if (numberGenes < 0) {
							int sizePb = 1
									+ ((AbsoPropSGenoRegiInserItem) apscssiIT)
											.getsPbStopSGenomicRegionInsertionInElement()
									- ((AbsoPropSGenoRegiInserItem) apscssiIT)
											.getsPbStartSGenomicRegionInsertionInElement();
							// System.err.println("i full="+i);
							drawBlockSGenomicRegionInsertionOrMissingTarget(
									apscssiIT.getSEnumBlockType(),
									convertSizeBpToTextWithAppropriateUnit(sizePb),
									redrawOnTopSent,
									inRedSent,
									true,
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnchoredToPrevious(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnchoredToNext(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnnotationCollision(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnchoredToStartElement(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isAnchoredToStopElement(),
									numberOtherMatch);

//							@SuppressWarnings("unused")
//							ResultLoadingDialog rld = new ResultLoadingDialog(
//									DetailledInfoStack.GeneCountForSGenomicRegionInsertion,
//									siIT.getIndexInFullArray(),
//									(associatedGenomePanel
//											.getGenomePanelItem()
//											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + sliceXClicked),
//									sliceXClicked,
//									Insyght.APP_CONTROLER
//											.getLIST_GENOME_PANEL().indexOf(
//													associatedGenomePanel),
//									associatedGenomePanel
//											.getGenomePanelItem()
//											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//									CenterSLPAllGenomePanels.START_INDEX,
//									associatedGenomePanel
//											.getGenomePanelItem()
//											.getPercentSyntenyShowStartQGenome(),
//									associatedGenomePanel.getGenomePanelItem()
//											.getPercentSyntenyShowStopQGenome(),
//									associatedGenomePanel
//											.getGenomePanelItem()
//											.getPercentSyntenyShowStartSGenome(),
//									associatedGenomePanel.getGenomePanelItem()
//											.getPercentSyntenyShowStopSGenome(),
//									((AbsoPropSGenoRegiInserItem) apscssiIT)
//											.getsOrigamiElementId(),
//									((AbsoPropSGenoRegiInserItem) apscssiIT)
//											.getsPbStartSGenomicRegionInsertionInElement(),
//									((AbsoPropSGenoRegiInserItem) apscssiIT)
//											.getsPbStopSGenomicRegionInsertionInElement(),
//									apscssiIT.getSEnumBlockType(),
//									false,
//									false,
//									Insyght.APP_CONTROLER
//											.getCurrentRefGenomePanelAndListOrgaResult()
//											.getViewTypeInsyght(),
//									((AbsoPropSGenoRegiInserItem) apscssiIT)
//											.isPreviousSOfNextSlice(),
//									((AbsoPropSGenoRegiInserItem) apscssiIT)
//											.isNextSOfPreviousSlice());
							// }
							RLD.getGeneCountForSGenomicRegionInsertion(
									siIT.getIndexInFullArray(),
									(associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + sliceXClicked),
									sliceXClicked,
									Insyght.APP_CONTROLER
											.getLIST_GENOME_PANEL().indexOf(
													associatedGenomePanel),
									associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
									CenterSLPAllGenomePanels.START_INDEX,
									associatedGenomePanel
											.getGenomePanelItem()
											.getPercentSyntenyShowStartQGenome(),
									associatedGenomePanel.getGenomePanelItem()
											.getPercentSyntenyShowStopQGenome(),
									associatedGenomePanel
											.getGenomePanelItem()
											.getPercentSyntenyShowStartSGenome(),
									associatedGenomePanel.getGenomePanelItem()
											.getPercentSyntenyShowStopSGenome(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.getsOrigamiElementId(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.getsPbStartSGenomicRegionInsertionInElement(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.getsPbStopSGenomicRegionInsertionInElement(),
									apscssiIT.getSEnumBlockType(),
									false,
									false,
									Insyght.APP_CONTROLER
											.getCurrentRefGenomePanelAndListOrgaResult()
											.getViewTypeInsyght(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isPreviousSOfNextSlice(),
									((AbsoPropSGenoRegiInserItem) apscssiIT)
											.isNextSOfPreviousSlice()
									);

						} else {
							if (numberGenes == 0) {
								int sizePb = 1
										+ ((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getsPbStopSGenomicRegionInsertionInElement()
										- ((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getsPbStartSGenomicRegionInsertionInElement();
								// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
								// sizePb+" bp", false, false);
								// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
								// false, false,
								// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
								// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
								drawBlockSGenomicRegionInsertionOrMissingTarget(
										apscssiIT.getSEnumBlockType(),
										convertSizeBpToTextWithAppropriateUnit(sizePb),
										redrawOnTopSent,
										inRedSent,
										true,
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToPrevious(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToNext(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnnotationCollision(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToStartElement(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToStopElement(),
										numberOtherMatch);

							} else {
								drawBlockSGenomicRegionInsertionOrMissingTarget(
										apscssiIT.getSEnumBlockType(),
										numberGenes + " CDS",
										redrawOnTopSent,
										inRedSent,
										false,
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToPrevious(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToNext(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnnotationCollision(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToStartElement(),
										((AbsoPropSGenoRegiInserItem) apscssiIT)
												.isAnchoredToStopElement(),
										numberOtherMatch);
							}
						}

					} else if (apscssiIT instanceof AbsoPropSElemItem) {
						// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
						// ((AbsoluteProportionSElementItem)apscssiIT).getsElementNumberGene(),
						// redrawOnTopSent, inRedSent);
						int numberGenes = ((AbsoPropSElemItem) apscssiIT)
								.getsElementNumberGene();
						if (numberGenes <= 0) {
							int sizePb = 1 + ((AbsoPropSElemItem) apscssiIT)
									.getsSizeOfElementinPb();
							drawBlockSGenomicRegionInsertionOrMissingTarget(
									apscssiIT.getSEnumBlockType(),
									convertSizeBpToTextWithAppropriateUnit(sizePb),
									redrawOnTopSent,
									inRedSent, true, true, true, false, false,
									false, numberOtherMatch);
							// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
							// redrawOnTopSent, inRedSent);
						} else {
							drawBlockSGenomicRegionInsertionOrMissingTarget(
									apscssiIT.getSEnumBlockType(), numberGenes
											+ " CDS", redrawOnTopSent, inRedSent,
									false, false, false, false, false, false,
									numberOtherMatch);
						}
					} else {
						//System.err.println("ERROR in s selectBlockSynteny unrecognized class for object sent");
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in s selectBlockSynteny unrecognized class for object sent = "+apscssiIT.getClass().toString()));
					}
					restoreToOriginCoordinateSpace();

					// RQ : if change here change in AppControler ->
					// clearSyntenyCanvasSelection too
					if (associatedGenomePanel.getGenomePanelItem()
							.getSizeState()
							.compareTo(GenomePanelItem.GpiSizeState.MAX) == 0) {
						// set style bottom
						contextBgk.translate(0, TOTAL_CANVAS_HEIGHT_UPPER_PART);

						if (apscssiIT instanceof AbsoPropSGenoRegiInserItem
								|| apscssiIT instanceof AbsoPropSElemItem
								|| apscssiIT instanceof AbsoPropSGeneInserItem) {
							drawGenomicRegionRepersentationProportionateS(
									apscssiIT,
									claculateRelativePercentage(
											associatedGenomePanel
													.getGenomePanelItem()
													.getListAbsoluteProportionElementItemS()
													.get(0)
													.getsSizeOfOragnismInPb(),
											apscssiIT.getsPercentStart(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStopSGenome(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStartSGenome()),
									claculateRelativePercentage(
											associatedGenomePanel
													.getGenomePanelItem()
													.getListAbsoluteProportionElementItemS()
													.get(0)
													.getsSizeOfOragnismInPb(),
											apscssiIT.getsPercentStop(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStopSGenome(),
											associatedGenomePanel
													.getGenomePanelItem()
													.getPercentSyntenyShowStartSGenome()),
									redrawOnTopSent, inRedSent, true);
						} else {
							//System.err.println("ERROR in drawSyntenyRepersentationProportionateS s selectBlockSynteny unrecognized class for object sent");
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in drawSyntenyRepersentationProportionateS " +
									"s selectBlockSynteny unrecognized class for object sent = "+apscssiIT.getClass().toString()));
						}

						// drawScaffoldGenome(false, siIT.getsPercentStart(),
						// siIT.getsPercentStop(), true, true, false);
						drawScaffoldGenome(
								false,
								claculateRelativePercentage(
										associatedGenomePanel
												.getGenomePanelItem()
												.getListAbsoluteProportionElementItemS()
												.get(0)
												.getsSizeOfOragnismInPb(),
										apscssiIT.getsPercentStart(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStopSGenome(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStartSGenome()),
								claculateRelativePercentage(
										associatedGenomePanel
												.getGenomePanelItem()
												.getListAbsoluteProportionElementItemS()
												.get(0)
												.getsSizeOfOragnismInPb(),
										apscssiIT.getsPercentStop(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStopSGenome(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStartSGenome()),
								true, inRedSent, false);

						String colorBgkIT = "";
						if (apscssiIT instanceof AbsoPropSGeneInserItem) {
							colorBgkIT = getBkgColorAssociatedWithColorIDAsString(((AbsoPropSGeneInserItem) apscssiIT)
									.getsStyleItem().getBkgColor());
						} else {
							colorBgkIT = getDefaultBkgColorAccordingToSBlockType(apscssiIT
									.getSEnumBlockType());
						}

						// drawMarkerScaffold(blockTypeIT,
						// siIT.getqPercentStart(), siIT.getqPercentStop(),
						// siIT.getsPercentStart(), siIT.getsPercentStop(),
						// true, true, false);
						drawSyntenyMarkerScaffold(
								false,
								true,
								-1,
								-1,
								claculateRelativePercentage(
										associatedGenomePanel
												.getGenomePanelItem()
												.getListAbsoluteProportionElementItemS()
												.get(0)
												.getsSizeOfOragnismInPb(),
										apscssiIT.getsPercentStart(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStopSGenome(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStartSGenome()),
								claculateRelativePercentage(
										associatedGenomePanel
												.getGenomePanelItem()
												.getListAbsoluteProportionElementItemS()
												.get(0)
												.getsSizeOfOragnismInPb(),
										apscssiIT.getsPercentStop(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStopSGenome(),
										associatedGenomePanel
												.getGenomePanelItem()
												.getPercentSyntenyShowStartSGenome()),
								redrawOnTopSent, inRedSent, false, colorBgkIT);

						restoreToOriginCoordinateSpace();
					}

					// select in app controler and show info
					// ResultViewImpl.fillBasicSyntenyInfoInStack(associatedGenomePanel.getGenomePanelItem().getPartialListAbsoluteProportionSyntenyItem().get(indexStartBirdSynteny+selectedSliceX).getAbsoluteProportionSSyntenyItem().getSyntenyItemId(),
					// true);
					if (apscssiIT instanceof AbsoPropSGeneInserItem) {

						Insyght.APP_CONTROLER
								.setCurrSelectedSyntenyCanvasItemRefId("S_GENE_INSERTION_"
										+ ((AbsoPropSGeneInserItem) apscssiIT)
												.getsGeneId());
						for (int m = 0; m < associatedGenomePanel
								.getGenomePanelItem()
								.getListAbsoluteProportionElementItemS().size(); m++) {
							AbsoPropSElemItem apseiIT = associatedGenomePanel
									.getGenomePanelItem()
									.getListAbsoluteProportionElementItemS()
									.get(m);
							if (apseiIT
									.getsAccnum()
									.compareTo(
											((AbsoPropSElemItem) apscssiIT)
													.getsAccnum()) == 0) {
								GenoOrgaAndHomoTablViewImpl
										.fillElementInfoInStack(apseiIT, false,
												true);
								break;
							}
						}
						// ResultViewImpl.fillElementInfoInStack((AbsoluteProportionSElementItem)apscssiIT,
						// false, false);
						GenoOrgaAndHomoTablViewImpl
								.fillGeneInfoInStack(
										(AbsoPropSGeneInserItem) apscssiIT,
										true, true);
					} else if (apscssiIT instanceof AbsoPropSGenoRegiInserItem) {

						Insyght.APP_CONTROLER
								.setCurrSelectedSyntenyCanvasItemRefId("S_GENOMIC_REGION_INSERTION_S_START_"
										+ ((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getsPbStartSGenomicRegionInsertionInElement()
										+ "_S_STOP_"
										+ ((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getsPbStopSGenomicRegionInsertionInElement());
						for (int m = 0; m < associatedGenomePanel
								.getGenomePanelItem()
								.getListAbsoluteProportionElementItemS().size(); m++) {
							AbsoPropSElemItem apseiIT = associatedGenomePanel
									.getGenomePanelItem()
									.getListAbsoluteProportionElementItemS()
									.get(m);
							if (apseiIT
									.getsAccnum()
									.compareTo(
											((AbsoPropSElemItem) apscssiIT)
													.getsAccnum()) == 0) {
								GenoOrgaAndHomoTablViewImpl
										.fillElementInfoInStack(apseiIT, false,
												true);
								break;
							}
						}
						// ResultViewImpl.fillElementInfoInStack((AbsoluteProportionSElementItem)apscssiIT,
						// false, false);
						GenoOrgaAndHomoTablViewImpl
								.fillSGenomicRegionInfoInStack(
										(AbsoPropSGenoRegiInserItem) apscssiIT,
										true);
					} else if (apscssiIT instanceof AbsoPropSElemItem) {
						Insyght.APP_CONTROLER
								.setCurrSelectedSyntenyCanvasItemRefId("S_ELEMENT_"
										+ ((AbsoPropSElemItem) apscssiIT)
												.getsPbStartOfElementInOrga()
										+ "_SIZE_"
										+ ((AbsoPropSElemItem) apscssiIT)
												.getsSizeOfElementinPb());
						GenoOrgaAndHomoTablViewImpl.fillElementInfoInStack(
								(AbsoPropSElemItem) apscssiIT,
								true, true);
					} else {
						//System.err.println("ERROR in show info s selectBlockSynteny unrecognized class for object sent");
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in show info s " +
								"selectBlockSynteny unrecognized class for object sent = "+apscssiIT.getClass().toString()));
					}

				}

			}
		}

	}

	public static String convertSizeBpToTextWithAppropriateUnit(int sizePb) {
		if (sizePb > 1000000000) {
			return NumberFormat.getFormat("###.#").format( sizePb / (Double) 1000000000.0) + " Gb";
		} else if (sizePb > 1000000) {
			return NumberFormat.getFormat("###.#").format(sizePb / (Double) 1000000.0) + " Mb";
		} else if (sizePb > 1000) {
			return NumberFormat.getFormat("###.#").format(sizePb / (Double) 1000.0) + " Kb";	
		}else{
			return sizePb + " bp";
		}
	}

	private void initCoordinateSpace() {
		// System.out.println("initCoordinateSpace");
		canvasBgk.setCoordinateSpaceWidth(TOTAL_CANVAS_WIDTH);
		canvasBgk.setCoordinateSpaceHeight(TOTAL_CANVAS_HEIGHT);
		setCoordinateSpace = false;

	}

	private void saveOriginCoordinateSpace() {
		// save origin
		contextBgk.translate(0, 0);
		contextBgk.setFont(FONT_SIZE + "px Courier New");
		// contextBgk.setFont(FONT_SIZE+"px sans-serif");
		// contextBgk.setFont("bold "+FONT_SIZE+"px arial");
		contextBgk.save();
	}

	public void restoreToOriginCoordinateSpace() {
		contextBgk.restore();
		saveOriginCoordinateSpace();

	}

	public void drawViewToCanvas(double shift_progress, int shiftWidth,
			boolean drawUpperPart, boolean drawBottomPart,
			boolean forceBottomDrawing, boolean bottomRedrawOnTop) {

		// long milli = System.currentTimeMillis(); // TEST remove test time

		// inti params if needed
		if (CALCULATE_CANVAS_PARAMETERS) {
			// System.err.println("here");
			calculateCanvasParameters();
		}

		if (setCoordinateSpace) {
			initCoordinateSpace();
		}

		// System.out.println("totalCanvasWidth :"+totalCanvasWidth);
		if (TOTAL_CANVAS_WIDTH == 0) {
			//System.out.println("TOTAL_CANVAS_WIDTH is null");
			//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("TOTAL_CANVAS_WIDTH is null"));
			return;
		}

		// long milli = System.currentTimeMillis(); // TEST remove test time

		// System.err.println("drawViewToCanvas : "+associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
		// +" ; "+associatedGenomePanel.getGenomePanelItem().getLookUpForSymbolicSliceAtQPosition());

		if (shift_progress == 1) {

			if (associatedGenomePanel.getGenomePanelItem()
					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() > (associatedGenomePanel
					.getGenomePanelItem()
					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
					.size() - NUMBER_SLICE_WIDTH)) {
				associatedGenomePanel
						.getGenomePanelItem()
						.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
								(associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.size() - NUMBER_SLICE_WIDTH),
										Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			}

			if (associatedGenomePanel.getGenomePanelItem()
					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0
					&& associatedGenomePanel.getGenomePanelItem()
							.getLookUpForGeneWithQStart() >= 0
					&& associatedGenomePanel.getGenomePanelItem()
							.getLookUpForGeneWithQStop() > 0
					&& associatedGenomePanel.getGenomePanelItem()
							.getLookUpForGeneOnQElementId() >= 0
//					&& Insyght.APP_CONTROLER
//							.getCurrentRefGenomePanelAndListOrgaResult()
//							.getViewTypeInsyght()
//							.compareTo(EnumResultViewTypes.genomic_organization) == 0
							) {
				// look up for this start position to display

				//System.err.println("look up for this start position to display : "+associatedGenomePanel.getGenomePanelItem().getLookUpForGeneWithQStart());

				int newIxStart = 0;
				// if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewType().compareTo(EnumResultViewTypes.homolog_table)!=0){

				boolean foundGene = false;
				boolean openGenesInQSSyntenyRegion = false;
				boolean openGenesInQGeneInsertionRegion = false;
				// boolean openGenesInSGeneInsertionRegion = false;

				for (int j = 0; j < associatedGenomePanel
						.getGenomePanelItem()
						.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
						.size(); j++) {
					HolderAbsoluteProportionItem hapiIT = associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(j)
							.getListHAPI()
							.get(associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(j).getIdxToDisplayInListHAPI());

					// System.out.println("finding genes...");

					if (hapiIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
						if (hapiIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
							// System.out.println("checking QSGeneHomologyItem at "+j);
							if (((AbsoPropQSGeneHomoItem) hapiIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getQsQOrigamiElementId() == associatedGenomePanel
									.getGenomePanelItem()
									.getLookUpForGeneOnQElementId()
									&& !((AbsoPropQSGeneHomoItem) hapiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getQsEnumBlockType().toString()
											.startsWith("QS_CUT_")) {
								// System.out.println("ok same elements : hapiIT "+((AbsoluteProportionQSGeneHomologyItem)hapiIT.getAbsoluteProportionQComparableQSSpanItem()).getQsQOrigamiElementId()+
								// " ; getLookUpForSymbolicSliceAtQElementId "+associatedGenomePanel.getGenomePanelItem().getLookUpForGeneOnQElementId());

								if (((AbsoPropQSGeneHomoItem) hapiIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getQsQPbStartGeneInElement() == associatedGenomePanel
										.getGenomePanelItem()
										.getLookUpForGeneWithQStart()
										&& ((AbsoPropQSGeneHomoItem) hapiIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsQPbStopGeneInElement() == associatedGenomePanel
												.getGenomePanelItem()
												.getLookUpForGeneWithQStop()) {
									// System.out.println("break QSGeneHomologyItem at "+j);
									foundGene = true;
									openGenesInQSSyntenyRegion = false;
									openGenesInQGeneInsertionRegion = false;
									newIxStart = j;
									break;
								}// else{
									// System.out.println("last QSGeneHomologyItem ok : "+j);
									// openGenesInQSSyntenyRegion = false;
									// openGenesInQGeneInsertionRegion = false;
									// openGenesInSGeneInsertionRegion = false;
									// newIxStart = j;
								// }
							} else {
								// System.out.println("cut or not same elements : hapiIT "+((AbsoluteProportionQSGeneHomologyItem)hapiIT.getAbsoluteProportionQComparableQSSpanItem()).getQsQOrigamiElementId()+
								// " ; getLookUpForSymbolicSliceAtQElementId "+associatedGenomePanel.getGenomePanelItem().getLookUpForGeneOnQElementId());
								continue;
							}
						} else if (hapiIT
								.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
							// System.out.println("checking QSSyntenyItem at "+j);
							if (((AbsoPropQSSyntItem) hapiIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getQsQOrigamiElementId() == associatedGenomePanel
									.getGenomePanelItem()
									.getLookUpForGeneOnQElementId()
									&& !((AbsoPropQSSyntItem) hapiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getQsEnumBlockType().toString()
											.startsWith("QS_CUT_")) {
								// System.out.println("ok same elements : hapiIT "+((AbsoPropQSSyntItem)hapiIT.getAbsoluteProportionQComparableQSSpanItem()).getQsQOrigamiElementId()+
								// " ; getLookUpForSymbolicSliceAtQElementId "+associatedGenomePanel.getGenomePanelItem().getLookUpForGeneOnQElementId());

								boolean startGeneWithinSynteny = false;
								boolean stopGeneWithinSynteny = false;
								if (associatedGenomePanel.getGenomePanelItem()
										.getLookUpForGeneWithQStart() >= ((AbsoPropQSSyntItem) hapiIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getPbQStartSyntenyInElement()
										&& associatedGenomePanel
												.getGenomePanelItem()
												.getLookUpForGeneWithQStart() <= ((AbsoPropQSSyntItem) hapiIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getPbQStopSyntenyInElement()) {
									startGeneWithinSynteny = true;
								}
								if (associatedGenomePanel.getGenomePanelItem()
										.getLookUpForGeneWithQStop() >= ((AbsoPropQSSyntItem) hapiIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getPbQStartSyntenyInElement()
										&& associatedGenomePanel
												.getGenomePanelItem()
												.getLookUpForGeneWithQStop() <= ((AbsoPropQSSyntItem) hapiIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getPbQStopSyntenyInElement()) {
									stopGeneWithinSynteny = true;
								}

								if (startGeneWithinSynteny
										&& stopGeneWithinSynteny) {
									// System.out.println("startGeneWithinSynteny && stopGeneWithinSynteny, break at "+j);
									// start gene and stop gene are within
									// synteny
									foundGene = false;
									openGenesInQSSyntenyRegion = true;
									openGenesInQGeneInsertionRegion = false;
									newIxStart = j;
									break;

								} else if (startGeneWithinSynteny
										&& !stopGeneWithinSynteny) {
									// start gene within synteny but not stop
									// gene
									double percentHere = (((double) ((AbsoPropQSSyntItem) hapiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getPbQStopSyntenyInElement() - (double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStart()) / ((double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStop() - (double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStart()));

									if (percentHere > 0.5) {
										// System.out.println("startGeneWithinSynteny && !stopGeneWithinSynteny, >50 break at "+j);
										// we get more than 50 percent of gene
										// here
										foundGene = false;
										openGenesInQSSyntenyRegion = true;
										openGenesInQGeneInsertionRegion = false;
										newIxStart = j;
										break;
									} else {
										// System.out.println("startGeneWithinSynteny && !stopGeneWithinSynteny, <50");
										continue;
									}

								} else if (!startGeneWithinSynteny
										&& stopGeneWithinSynteny) {
									// stop gene within synteny but not start
									// gene
									double percentHere = (((double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStop() - (double) ((AbsoPropQSSyntItem) hapiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getPbQStartSyntenyInElement()) / ((double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStop() - (double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStart()));

									if (percentHere > 0.5) {
										// System.out.println("!startGeneWithinSynteny && stopGeneWithinSynteny, >50 break at "+j);
										// we get more than 50 percent of gene
										// here
										foundGene = false;
										openGenesInQSSyntenyRegion = true;
										openGenesInQGeneInsertionRegion = false;
										newIxStart = j;
										break;
									} else {
										// System.out.println("startGeneWithinSynteny && !stopGeneWithinSynteny, <50");
										continue;
									}

								} else {
									// neither start gene and stop gene are
									// within synteny
									// System.out.println("!startGeneWithinSynteny && !stopGeneWithinSynteny");
									continue;
								}

								// if(((AbsoPropQSSyntItem)hapiIT.getAbsoluteProportionQComparableQSSpanItem()).getPbQStartSyntenyInElement()
								// >
								// associatedGenomePanel.getGenomePanelItem().getLookUpForSymbolicSliceAtQPosition()){
								// //System.out.println("break QSSyntenyItem at "+j);
								// foundGene = false;
								// break;
								// }else{
								// //System.out.println("last QSSyntenyItem ok : "+j);
								// openGenesInQSSyntenyRegion = true;
								// openGenesInQGeneInsertionRegion = false;
								// //openGenesInSGeneInsertionRegion = false;
								// newIxStart = j;
								// }

							} else {
								// System.out.println("not same elements : hapiIT "+((AbsoPropQSSyntItem)hapiIT.getAbsoluteProportionQComparableQSSpanItem()).getQsQOrigamiElementId()+
								// " ; getLookUpForSymbolicSliceAtQElementId "+associatedGenomePanel.getGenomePanelItem().getLookUpForGeneOnQElementId());
								continue;
							}
						}
					} else if (hapiIT
							.getAbsoluteProportionQComparableQSpanItem() != null) {
						if (hapiIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
							// System.out.println("checking QGeneInsertionItem at "+j);
							if (((AbsoPropQGeneInserItem) hapiIT
									.getAbsoluteProportionQComparableQSpanItem())
									.getqOrigamiElementId() == associatedGenomePanel
									.getGenomePanelItem()
									.getLookUpForGeneOnQElementId()
									&& !((AbsoPropQGeneInserItem) hapiIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getqEnumBlockType().toString()
											.startsWith("Q_CUT_")) {
								// System.out.println("ok same elements : hapiIT "+((AbsoluteProportionQGeneInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getqOrigamiElementId()+
								// " ; getLookUpForSymbolicSliceAtQElementId "+associatedGenomePanel.getGenomePanelItem().getLookUpForGeneOnQElementId());
								if (((AbsoPropQGeneInserItem) hapiIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqPbStartGeneInElement() == associatedGenomePanel
										.getGenomePanelItem()
										.getLookUpForGeneWithQStart()
										&& ((AbsoPropQGeneInserItem) hapiIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqPbStopGeneInElement() == associatedGenomePanel
												.getGenomePanelItem()
												.getLookUpForGeneWithQStop()) {
									// System.out.println("break QGeneInsertionItem at "+j);
									foundGene = true;
									openGenesInQSSyntenyRegion = false;
									openGenesInQGeneInsertionRegion = false;
									newIxStart = j;
									break;
								}
								// else{
								// System.out.println("last QGeneInsertionItem ok : "+j);
								// openGenesInQSSyntenyRegion = false;
								// openGenesInQGeneInsertionRegion = false;
								// openGenesInSGeneInsertionRegion = false;
								// newIxStart = j;
								// }
							} else {
								// System.out.println("cut or not same elements : hapiIT "+((AbsoluteProportionQGeneInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getqOrigamiElementId()+
								// " ; getLookUpForSymbolicSliceAtQElementId "+associatedGenomePanel.getGenomePanelItem().getLookUpForGeneOnQElementId());

								continue;
							}
						} else if (hapiIT
								.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
							// System.out.println("checking QGenomicRegionInsertionItem at "+j);
							if (((AbsoPropQGenoRegiInserItem) hapiIT
									.getAbsoluteProportionQComparableQSpanItem())
									.getqOrigamiElementId() == associatedGenomePanel
									.getGenomePanelItem()
									.getLookUpForGeneOnQElementId()
									&& !((AbsoPropQGenoRegiInserItem) hapiIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getqEnumBlockType().toString()
											.startsWith("Q_CUT_")) {
								// System.out.println("ok same elements : hapiIT "+((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getqOrigamiElementId()+
								// " ; getLookUpForSymbolicSliceAtQElementId "+associatedGenomePanel.getGenomePanelItem().getLookUpForGeneOnQElementId());

								boolean startGeneWithinQGenomicRegionInsertion = false;
								boolean stopGeneWithinQGenomicRegionInsertion = false;
								if (associatedGenomePanel.getGenomePanelItem()
										.getLookUpForGeneWithQStart() >= ((AbsoPropQGenoRegiInserItem) hapiIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqPbStartQGenomicRegionInsertionInElement()
										&& associatedGenomePanel
												.getGenomePanelItem()
												.getLookUpForGeneWithQStart() <= ((AbsoPropQGenoRegiInserItem) hapiIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqPbStopQGenomicRegionInsertionInElement()) {
									startGeneWithinQGenomicRegionInsertion = true;
								}
								if (associatedGenomePanel.getGenomePanelItem()
										.getLookUpForGeneWithQStop() >= ((AbsoPropQGenoRegiInserItem) hapiIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqPbStartQGenomicRegionInsertionInElement()
										&& associatedGenomePanel
												.getGenomePanelItem()
												.getLookUpForGeneWithQStop() <= ((AbsoPropQGenoRegiInserItem) hapiIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqPbStopQGenomicRegionInsertionInElement()) {
									stopGeneWithinQGenomicRegionInsertion = true;
								}

								if (startGeneWithinQGenomicRegionInsertion
										&& stopGeneWithinQGenomicRegionInsertion) {
									// System.out.println("startGeneWithinQGenomicRegionInsertion && stopGeneWithinQGenomicRegionInsertion, break at "+j);
									// start gene and stop gene are within
									// synteny
									foundGene = false;
									openGenesInQSSyntenyRegion = false;
									openGenesInQGeneInsertionRegion = true;
									newIxStart = j;
									break;

								} else if (startGeneWithinQGenomicRegionInsertion
										&& !stopGeneWithinQGenomicRegionInsertion) {
									// start gene within synteny but not stop
									// gene
									double percentHere = (((double) ((AbsoPropQGenoRegiInserItem) hapiIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getqPbStopQGenomicRegionInsertionInElement() - (double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStart()) / ((double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStop() - (double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStart()));

									if (percentHere > 0.5) {
										// System.out.println("startGeneWithinQGenomicRegionInsertion && !stopGeneWithinQGenomicRegionInsertion, >50 break at "+j);
										// we get more than 50 percent of gene
										// here
										foundGene = false;
										openGenesInQSSyntenyRegion = false;
										openGenesInQGeneInsertionRegion = true;
										newIxStart = j;
										break;
									} else {
										// System.out.println("startGeneWithinQGenomicRegionInsertion && !stopGeneWithinQGenomicRegionInsertion, <50");
										continue;
									}

								} else if (!startGeneWithinQGenomicRegionInsertion
										&& stopGeneWithinQGenomicRegionInsertion) {
									// stop gene within synteny but not start
									// gene
									double percentHere = (((double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStop() - (double) ((AbsoPropQGenoRegiInserItem) hapiIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getqPbStartQGenomicRegionInsertionInElement()) / ((double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStop() - (double) associatedGenomePanel
											.getGenomePanelItem()
											.getLookUpForGeneWithQStart()));

									if (percentHere > 0.5) {
										// System.out.println("!startGeneWithinQGenomicRegionInsertion && stopGeneWithinQGenomicRegionInsertion, >50 break at "+j);
										// we get more than 50 percent of gene
										// here
										foundGene = false;
										openGenesInQSSyntenyRegion = false;
										openGenesInQGeneInsertionRegion = true;
										newIxStart = j;
										break;
									} else {
										// System.out.println("!startGeneWithinQGenomicRegionInsertion && stopGeneWithinQGenomicRegionInsertion, >50");
										continue;
									}

								} else {
									// neither start gene and stop gene are
									// within synteny
									// System.out.println("!startGeneWithinQGenomicRegionInsertion && !stopGeneWithinQGenomicRegionInsertion");
									continue;
								}

							} else {
								// System.out.println("not same elements : hapiIT "+((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getqOrigamiElementId()+
								// " ; getLookUpForSymbolicSliceAtQElementId "+associatedGenomePanel.getGenomePanelItem().getLookUpForGeneOnQElementId());

								continue;
							}
						}
					}
				}

				if (foundGene) {
					// System.out.println("foundGene!");
					associatedGenomePanel.getGenomePanelItem()
							.setLookUpForGeneWithQStart(-1,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
					associatedGenomePanel.getGenomePanelItem()
							.setLookUpForGeneWithQStop(-1,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
					associatedGenomePanel.getGenomePanelItem()
							.setLookUpForGeneOnQElementId(-1,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
					associatedGenomePanel
							.getGenomePanelItem()
							.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
									newIxStart,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());

				} else if (openGenesInQSSyntenyRegion) {

					// System.out.println("openGenesInQSSyntenyRegion");

					// get synteny block
					int idxToDisplayInListHAPI = associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(newIxStart).getIdxToDisplayInListHAPI();
					HolderAbsoluteProportionItem apsiSyntenyBlockToOpenIT = associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(newIxStart).getListHAPI()
							.get(idxToDisplayInListHAPI);

					drawLoading();

//					@SuppressWarnings("unused")
//					ResuLoadMoreAbsoPropItemDial rld = new ResuLoadMoreAbsoPropItemDial(
//							ResuLoadMoreAbsoPropItemDial.TypeOfRegion.SYNTENY_REGION,
//							apsiSyntenyBlockToOpenIT.getIndexInFullArray(),
//							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
//									.indexOf(associatedGenomePanel),
//							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
//									.getAbsoluteProportionQComparableQSSpanItem())
//									.getSyntenyOrigamiAlignmentId(),
//							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
//									.getAbsoluteProportionQComparableQSSpanItem())
//									.getListOrigamiAlignmentIdsContainedOtherSyntenies(),
//							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
//									.getAbsoluteProportionQComparableQSSpanItem())
//									.getListOrigamiAlignmentIdsMotherSyntenies(),
//							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
//									.getAbsoluteProportionQComparableQSSpanItem())
//									.getListOrigamiAlignmentIdsSprungOffSyntenies(),
//							Insyght.APP_CONTROLER
//									.getCurrentRefGenomePanelAndListOrgaResult()
//									.getViewTypeInsyght(),
//							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
//									.getAbsoluteProportionQComparableQSSpanItem())
//									.isContainedWithinAnotherMotherSynteny(),
//							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
//									.getAbsoluteProportionQComparableQSSpanItem())
//									.isMotherOfContainedOtherSyntenies(),
//							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
//									.getAbsoluteProportionQComparableQSSpanItem())
//									.isMotherOfSprungOffSyntenies(),
//							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
//									.getAbsoluteProportionQComparableQSSpanItem())
//									.isSprungOffAnotherMotherSynteny(),
//							idxToDisplayInListHAPI);
					
					RLMAPID.getArrayListAbsoPropGenesSyntenyRegion(
							apsiSyntenyBlockToOpenIT.getIndexInFullArray(),
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
									.indexOf(associatedGenomePanel),
							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getSyntenyOrigamiAlignmentId(),
							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getListOrigamiAlignmentIdsContainedOtherSyntenies(),
							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getListOrigamiAlignmentIdsMotherSyntenies(),
							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getListOrigamiAlignmentIdsSprungOffSyntenies(),
							Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght(),
							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.isContainedWithinAnotherMotherSynteny(),
							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.isMotherOfContainedOtherSyntenies(),
							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.isMotherOfSprungOffSyntenies(),
							((AbsoPropQSSyntItem) apsiSyntenyBlockToOpenIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.isSprungOffAnotherMotherSynteny(),
							idxToDisplayInListHAPI);
					

					return;

				} else if (openGenesInQGeneInsertionRegion) {

					// System.out.println("openGenesInQGeneInsertionRegion");

					// get synteny block
					int idxToDisplayInListHAPI = associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(newIxStart).getIdxToDisplayInListHAPI();
					HolderAbsoluteProportionItem apsiQGenomicRegionBlockToOpenIT = associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(newIxStart).getListHAPI()
							.get(idxToDisplayInListHAPI);

					drawLoading();

					// System.out.println("index : "+AppController.LIST_GENOME_PANEL.indexOf(associatedGenomePanel)+" = "+AppController.LIST_GENOME_PANEL.get(AppController.LIST_GENOME_PANEL.indexOf(associatedGenomePanel)).getGenomePanelItem().isGpiLoadingAdditionalData());

//					@SuppressWarnings("unused")
//					ResuLoadMoreAbsoPropItemDial rld = new ResuLoadMoreAbsoPropItemDial(
//							ResuLoadMoreAbsoPropItemDial.TypeOfRegion.Q_REGION,
//							apsiQGenomicRegionBlockToOpenIT
//									.getIndexInFullArray(),
//							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
//									.indexOf(associatedGenomePanel),
//							((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
//									.getAbsoluteProportionQComparableQSpanItem())
//									.getqOrigamiElementId(),
//							((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
//									.getAbsoluteProportionQComparableQSpanItem())
//									.getqPbStartQGenomicRegionInsertionInElement(),
//							((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
//									.getAbsoluteProportionQComparableQSpanItem())
//									.getqPbStopQGenomicRegionInsertionInElement(),
//							((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
//									.getAbsoluteProportionQComparableQSpanItem())
//									.getqAccnum(),
//							((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
//									.getAbsoluteProportionQComparableQSpanItem())
//									.getqPbStartOfElementInOrga(),
//							((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
//									.getAbsoluteProportionQComparableQSpanItem())
//									.getQsizeOfOragnismInPb(),
//							null,
//							-1,
//							-1,
//							Insyght.APP_CONTROLER
//									.getCurrentRefGenomePanelAndListOrgaResult()
//									.getViewTypeInsyght(), false, false);
					
					RLMAPID.getArrayListAbsoPropGenesQRegion(
							apsiQGenomicRegionBlockToOpenIT
							.getIndexInFullArray(),
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
							.indexOf(associatedGenomePanel),
					((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
							.getAbsoluteProportionQComparableQSpanItem())
							.getqOrigamiElementId(),
					((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
							.getAbsoluteProportionQComparableQSpanItem())
							.getqPbStartQGenomicRegionInsertionInElement(),
					((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
							.getAbsoluteProportionQComparableQSpanItem())
							.getqPbStopQGenomicRegionInsertionInElement(),
					((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
							.getAbsoluteProportionQComparableQSpanItem())
							.getqAccnum(),
					((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
							.getAbsoluteProportionQComparableQSpanItem())
							.getqPbStartOfElementInOrga(),
					((AbsoPropQGenoRegiInserItem) apsiQGenomicRegionBlockToOpenIT
							.getAbsoluteProportionQComparableQSpanItem())
							.getQsizeOfOragnismInPb(),
					Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght());
					
					
					return;
				} else {
					//gene not found
//					Window.alert("The gene was not found in the displayed view of "
//							+ associatedGenomePanel.getGenomePanelItem()
//									.getFullName()
//							+ ". Please zoom out and try to find the gene again.");
					associatedGenomePanel.getGenomePanelItem()
							.setLookUpForGeneWithQStart(-1,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
					associatedGenomePanel.getGenomePanelItem()
							.setLookUpForGeneWithQStop(-1,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
					associatedGenomePanel.getGenomePanelItem()
							.setLookUpForGeneOnQElementId(-1,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
					associatedGenomePanel
							.getGenomePanelItem()
							.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
									0,
									Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
				}


			} else if (associatedGenomePanel.getGenomePanelItem()
					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < 0) {

				associatedGenomePanel
						.getGenomePanelItem()
						.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
								0,
								Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());

				// check if need to update
				// getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed, only
				// for genomic orga view
				if (Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getViewTypeInsyght()
						.compareTo(EnumResultViewTypes.genomic_organization) == 0) {

					boolean searchForBetterStartIxd = true;
					for (int i = 0; i < NUMBER_SLICE_WIDTH; i++) {
						int blockToGetFromArray = i
								+ associatedGenomePanel
										.getGenomePanelItem()
										.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed();
						if (blockToGetFromArray >= associatedGenomePanel
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.size()) {
							break;
						}
						HolderAbsoluteProportionItem apsiIT = associatedGenomePanel
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.get(blockToGetFromArray)
								.getListHAPI()
								.get(associatedGenomePanel
										.getGenomePanelItem()
										.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
												blockToGetFromArray));
						boolean nextHasSPrevious = false;
						int blockToGetFromArrayNext = blockToGetFromArray + 1;
						if (blockToGetFromArrayNext < associatedGenomePanel
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.size()) {
							HolderAbsoluteProportionItem apsiNextIT = associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(blockToGetFromArrayNext)
									.getListHAPI()
									.get(associatedGenomePanel
											.getGenomePanelItem()
											.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
													blockToGetFromArrayNext));
							if (apsiNextIT
									.getAbsoluteProportionQComparableQSSpanItem() != null) {
								if (apsiNextIT
										.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
									if (((AbsoPropQSSyntItem) apsiNextIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getPreviousGenomicRegionSInsertion() != null) {
										nextHasSPrevious = true;
									}
								}
							} else if (apsiNextIT
									.getAbsoluteProportionSComparableSSpanItem() != null) {
								if (apsiNextIT
										.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
									if (((AbsoPropSGenoRegiInserItem) apsiNextIT
											.getAbsoluteProportionSComparableSSpanItem())
											.getsIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
										nextHasSPrevious = true;
									}
								} else if (apsiNextIT
										.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
									if (((AbsoPropSGeneInserItem) apsiNextIT
											.getAbsoluteProportionSComparableSSpanItem())
											.getIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
										nextHasSPrevious = true;
									}
								}
							}
						}
						if ((apsiIT
								.getAbsoluteProportionQComparableQSSpanItem() != null && apsiIT
								.getAbsoluteProportionSComparableSSpanItem() != null)
								|| (nextHasSPrevious && apsiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null)
								|| apsiIT
										.getAbsoluteProportionQComparableQSpanItem() != null) {
							searchForBetterStartIxd = false;
							// System.err.println("do not find better spot!!");
							break;
						}
					}
					if (searchForBetterStartIxd) {
						// System.err.println("find better spot!!");
						// find first start qs or q
						int newIxStart = 0;
						boolean foundBetterStart = false;
						if (Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getViewTypeInsyght()
								.compareTo(EnumResultViewTypes.homolog_table) != 0) {
							for (int j = 0; j < associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.size(); j++) {
								HolderAbsoluteProportionItem hapiIT = associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(j)
										.getListHAPI()
										.get(associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.get(j)
												.getIdxToDisplayInListHAPI());
								if (hapiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									// found start
									newIxStart = j;
									foundBetterStart = true;
									break;
								}
							}
							if (newIxStart > (associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.size() - NUMBER_SLICE_WIDTH)) {
								newIxStart = (associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.size() - NUMBER_SLICE_WIDTH);
							}
							if (newIxStart < 0) {
								newIxStart = 0;
							}
							if (!foundBetterStart) {
								for (int j = 0; j < associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.size(); j++) {
									HolderAbsoluteProportionItem hapiIT = associatedGenomePanel
											.getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
											.get(j)
											.getListHAPI()
											.get(associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.get(j)
													.getIdxToDisplayInListHAPI());
									if (hapiIT
											.getAbsoluteProportionQComparableQSpanItem() != null) {
										// found start
										newIxStart = j;
										// foundBetterStart = true;
										break;
									}
								}
								if (newIxStart > (associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.size() - NUMBER_SLICE_WIDTH)) {
									newIxStart = (associatedGenomePanel
											.getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
											.size() - NUMBER_SLICE_WIDTH);
								}
								if (newIxStart < 0) {
									newIxStart = 0;
								}
							}
						}
						associatedGenomePanel
								.getGenomePanelItem()
								.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
										newIxStart,
										Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
					}

				}

			}

			checkWetherToAllowForLeftRightSymbolNavigationOrNot();

		}

		// System.out.println("idx start drawing : "+associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed());

		// deal with upperpart
		if (drawUpperPart) {

			// System.out.println("drawing upper part");

			// clear canvas
			restoreToOriginCoordinateSpace();
			// contextBgk.clearRect(0, 0, TOTAL_CANVAS_WIDTH,
			// TOTAL_CANVAS_HEIGHT_UPPER_PART);
			contextBgk.setFillStyle(redrawColorWhite);
			contextBgk.fillRect(0, 0, TOTAL_CANVAS_WIDTH,
					TOTAL_CANVAS_HEIGHT_UPPER_PART);

			int sumTranslate = 0;

			// System.out.println("NUMBER_SLICE_WIDTH : "+NUMBER_SLICE_WIDTH);

			for (int i = 0; i < NUMBER_SLICE_WIDTH; i++) {

				// System.out.println("countIteration = "+i);

				int translateX;
				if (i == 0) {
					translateX = addToLeftToCenterDrawing
							+ (int) Math.floor(shift_progress * shiftWidth);
					sumTranslate += translateX;
					// System.out.println("shift : "+shift_progress+" ; SLICE_CANVAS_WIDTH = "+SLICE_CANVAS_WIDTH+" (int)Math.floor = "+(int)Math.floor(shift_progress*SLICE_CANVAS_WIDTH));
				} else {
					translateX = SLICE_CANVAS_WIDTH;
					sumTranslate += translateX;
					// System.out.println("shift : "+shift_progress+" ; SLICE_CANVAS_WIDTH = "+SLICE_CANVAS_WIDTH+" (int)Math.floor = "+(int)Math.floor(shift_progress*SLICE_CANVAS_WIDTH));
				}

				contextBgk.translate(translateX, 0);

				int blockToGetFromArray = i
						+ associatedGenomePanel
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed();
				if (blockToGetFromArray >= associatedGenomePanel
						.getGenomePanelItem()
						.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
						.size()) {
					break;
				}

				// is it worth drawing the block ?
				if (sumTranslate < (addToLeftToCenterDrawing - SLICE_CANVAS_WIDTH)) {
					// System.out.println("not worth drawing block, too much to the left : "+blockToGetFromArray+" ; sumTranslate = "+sumTranslate);
				} else if (sumTranslate > TOTAL_CANVAS_WIDTH
						+ SLICE_CANVAS_WIDTH) {
					// System.out.println("not worth drawing block, too much to the right : "+blockToGetFromArray+" ; sumTranslate = "+sumTranslate
					// + " ; TOTAL_CANVAS_WIDTH = " + TOTAL_CANVAS_WIDTH);
				} else {
					// get block to draw
					HolderAbsoluteProportionItem apsiIT = associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(blockToGetFromArray)
							.getListHAPI()
							.get(associatedGenomePanel
									.getGenomePanelItem()
									.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
											blockToGetFromArray));
					int numberOtherMatchIt = associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(blockToGetFromArray).getListHAPI().size() - 1;
					if (apsiIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
						if (apsiIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
							boolean allowToGetMoreData = false;
							if (shift_progress == 1) {
								allowToGetMoreData = true;
							}
							drawBlockQSGeneHomology(
									(AbsoPropQSGeneHomoItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem(),
									false,
									false,
									true,
									true,
									true,
									true,
									true,
									true,
									numberOtherMatchIt,
									// false,false,false,
									allowToGetMoreData,
									apsiIT.getIndexInFullArray(),
									i,
									((AbsoPropQSGeneHomoItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getPreviousGenomicRegionSInsertion(),
									((AbsoPropQSGeneHomoItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getNextGenomicRegionSInsertion()
							// ,true, true, true
							);

						} else if (apsiIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
							boolean allowToGetMoreData = false;
							if (shift_progress == 1) {
								allowToGetMoreData = true;
							}
							int numberPairsToAdd = ((AbsoPropQSSyntItem) apsiIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getSyntenyNumberGene();
							// numberPairsToAdd +=
							// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsContainedOtherSyntenies().size();
							// numberPairsToAdd +=
							// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsMotherSyntenies().size();
							// numberPairsToAdd +=
							// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsSprungOffSyntenies().size();

							drawBlockQSSynteny(
									apsiIT.getAbsoluteProportionQComparableQSSpanItem()
											.getQsEnumBlockType(),
									numberPairsToAdd,
									false,
									false,
									numberOtherMatchIt,
									((AbsoPropQSSyntItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.isContainedWithinAnotherMotherSynteny(),
									((AbsoPropQSSyntItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.isSprungOffAnotherMotherSynteny(),
									((AbsoPropQSSyntItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.isMotherOfSprungOffSyntenies(),
									((AbsoPropQSSyntItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.isMotherOfContainedOtherSyntenies(),
									((AbsoPropQSSyntItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getPreviousGenomicRegionSInsertion(),
									((AbsoPropQSSyntItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getNextGenomicRegionSInsertion(),
									allowToGetMoreData, apsiIT
											.getIndexInFullArray(), i
							// ,true, true, true
							);

						} else {
							//System.err.println("ERROR in qs drawSyntenyView unrecognized class for object sent");
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs drawSyntenyView unrecognized class for object sent = "+apsiIT.getAbsoluteProportionQComparableQSSpanItem().getClass().toString()));
						}
					} else {
						if (apsiIT.getAbsoluteProportionQComparableQSpanItem() != null) {
							if (apsiIT
									.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
								boolean allowToGetMoreData = false;
								if (shift_progress == 1) {
									allowToGetMoreData = true;
								}
								drawBlockQGeneInsertionOrMissingTarget(
										(AbsoPropQGeneInserItem) apsiIT
												.getAbsoluteProportionQComparableQSpanItem(),
										false, false, numberOtherMatchIt,
										allowToGetMoreData, apsiIT
												.getIndexInFullArray(), i);
							} else if (apsiIT
									.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
								// drawBlockQGenomicRegionInsertionOrMissingTarget(apsiIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(),
								// ((AbsoluteProportionQGenomicRegionInsertionItem)apsiIT.getAbsoluteProportionQComparableQSpanItem()).getQGenomicRegionInsertionNumberGene(),
								// false, false);
								int numberGenes = ((AbsoPropQGenoRegiInserItem) apsiIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getQGenomicRegionInsertionNumberGene();
								if (numberGenes < 0) {
									int sizePb = 
											//1 + 
											((AbsoPropQGenoRegiInserItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqPbStopQGenomicRegionInsertionInElement()
											- ((AbsoPropQGenoRegiInserItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqPbStartQGenomicRegionInsertionInElement();
									// System.err.println("i full="+i);
									drawBlockQGenomicRegionInsertionOrMissingTarget(
											apsiIT.getAbsoluteProportionQComparableQSpanItem()
													.getqEnumBlockType(),
											convertSizeBpToTextWithAppropriateUnit(sizePb),
											false,
											false,
											true,
											((AbsoPropQGenoRegiInserItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.isAnnotationCollision(),
											((AbsoPropQGenoRegiInserItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.isAnchoredToStartElement(),
											((AbsoPropQGenoRegiInserItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.isAnchoredToStopElement(),
											numberOtherMatchIt);
									// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
									// false, false);
									if (shift_progress == 1) {
										// System.err.println("i="+i);
//										@SuppressWarnings("unused")
//										ResultLoadingDialog rld = new ResultLoadingDialog(
//												DetailledInfoStack.GeneCountForQGenomicRegionInsertion,
//												apsiIT.getIndexInFullArray(),
//												(associatedGenomePanel
//														.getGenomePanelItem()
//														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + i),
//												i,
//												Insyght.APP_CONTROLER
//														.getLIST_GENOME_PANEL()
//														.indexOf(
//																associatedGenomePanel),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//												CenterSLPAllGenomePanels.START_INDEX,
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStartQGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStopQGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStartSGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStopSGenome(),
//												((AbsoPropQGenoRegiInserItem) apsiIT
//														.getAbsoluteProportionQComparableQSpanItem())
//														.getqOrigamiElementId(),
//												((AbsoPropQGenoRegiInserItem) apsiIT
//														.getAbsoluteProportionQComparableQSpanItem())
//														.getqPbStartQGenomicRegionInsertionInElement(),
//												((AbsoPropQGenoRegiInserItem) apsiIT
//														.getAbsoluteProportionQComparableQSpanItem())
//														.getqPbStopQGenomicRegionInsertionInElement(),
//												apsiIT.getAbsoluteProportionQComparableQSpanItem()
//														.getqEnumBlockType(),
//												false,
//												false,
//												Insyght.APP_CONTROLER
//														.getCurrentRefGenomePanelAndListOrgaResult()
//														.getViewTypeInsyght());
										RLD.getGeneCountForQGenomicRegionInsertion(
												apsiIT.getIndexInFullArray(),
												(associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + i),
												i,
												Insyght.APP_CONTROLER
														.getLIST_GENOME_PANEL()
														.indexOf(
																associatedGenomePanel),
												associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
												CenterSLPAllGenomePanels.START_INDEX,
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getqOrigamiElementId(),
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getqPbStartQGenomicRegionInsertionInElement(),
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getqPbStopQGenomicRegionInsertionInElement(),
												apsiIT.getAbsoluteProportionQComparableQSpanItem()
														.getqEnumBlockType(),
												false,
												false,
												Insyght.APP_CONTROLER
														.getCurrentRefGenomePanelAndListOrgaResult()
														.getViewTypeInsyght());

									}
								} else {
									if (numberGenes == 0) {
										int sizePb = 
												//1 + 
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getqPbStopQGenomicRegionInsertionInElement()
												- ((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getqPbStartQGenomicRegionInsertionInElement();
										// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
										// false, false);
										drawBlockQGenomicRegionInsertionOrMissingTarget(
												apsiIT.getAbsoluteProportionQComparableQSpanItem()
														.getqEnumBlockType(),
												convertSizeBpToTextWithAppropriateUnit(sizePb),
												false,
												false,
												true,
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.isAnnotationCollision(),
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.isAnchoredToStartElement(),
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.isAnchoredToStopElement(),
												numberOtherMatchIt);
									} else {
										drawBlockQGenomicRegionInsertionOrMissingTarget(
												apsiIT.getAbsoluteProportionQComparableQSpanItem()
														.getqEnumBlockType(),
												numberGenes + " CDS",
												false,
												false,
												false,
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.isAnnotationCollision(),
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.isAnchoredToStartElement(),
												((AbsoPropQGenoRegiInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.isAnchoredToStopElement(),
												numberOtherMatchIt);
									}
								}
							} else if (apsiIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem) {
								// drawBlockQGenomicRegionInsertionOrMissingTarget(apsiIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(),
								// ((AbsoluteProportionQElementItem)apsiIT.getAbsoluteProportionQComparableQSpanItem()).getqElementNumberGene(),
								// false, false);
								int numberGenes = ((AbsoPropQElemItem) apsiIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqElementNumberGene();
								if (numberGenes < 0) {
									int sizePb = 
											//1 + 
											((AbsoPropQElemItem) apsiIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getQsizeOfElementinPb();
									drawBlockQGenomicRegionInsertionOrMissingTarget(
											apsiIT.getAbsoluteProportionQComparableQSpanItem()
													.getqEnumBlockType(),
											convertSizeBpToTextWithAppropriateUnit(sizePb), false, false, true,
											false, false, false,
											numberOtherMatchIt);
									// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
									// false, false);
									if (shift_progress == 1) {
//										@SuppressWarnings("unused")
//										ResultLoadingDialog rld = new ResultLoadingDialog(
//												DetailledInfoStack.GeneCountForQElement,
//												apsiIT.getIndexInFullArray(),
//												(associatedGenomePanel
//														.getGenomePanelItem()
//														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + i),
//												i,
//												Insyght.APP_CONTROLER
//														.getLIST_GENOME_PANEL()
//														.indexOf(
//																associatedGenomePanel),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//												CenterSLPAllGenomePanels.START_INDEX,
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStartQGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStopQGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStartSGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStopSGenome(),
//												((AbsoPropQElemItem) apsiIT
//														.getAbsoluteProportionQComparableQSpanItem())
//														.getqOrigamiElementId(),
//												-1,
//												-1,
//												apsiIT.getAbsoluteProportionQComparableQSpanItem()
//														.getqEnumBlockType(),
//												false,
//												false,
//												Insyght.APP_CONTROLER
//														.getCurrentRefGenomePanelAndListOrgaResult()
//														.getViewTypeInsyght());
										RLD.getGeneCountForQElement(
												apsiIT.getIndexInFullArray(),
												(associatedGenomePanel
												.getGenomePanelItem()
												.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + i),
												i,
												Insyght.APP_CONTROLER
														.getLIST_GENOME_PANEL()
														.indexOf(
																associatedGenomePanel),
												associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
												CenterSLPAllGenomePanels.START_INDEX,
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												((AbsoPropQElemItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getqOrigamiElementId(),
												apsiIT.getAbsoluteProportionQComparableQSpanItem()
														.getqEnumBlockType(),
												false,
												false,
												Insyght.APP_CONTROLER
														.getCurrentRefGenomePanelAndListOrgaResult()
														.getViewTypeInsyght());

									}
								} else {
									if (numberGenes == 0) {
										int sizePb = 
												//1 + 
												((AbsoPropQElemItem) apsiIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getQsizeOfElementinPb();
										// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
										// false, false);
										drawBlockQGenomicRegionInsertionOrMissingTarget(
												apsiIT.getAbsoluteProportionQComparableQSpanItem()
														.getqEnumBlockType(),
												convertSizeBpToTextWithAppropriateUnit(sizePb), false, false,
												true, false, false, false,
												numberOtherMatchIt);

									} else {
										drawBlockQGenomicRegionInsertionOrMissingTarget(
												apsiIT.getAbsoluteProportionQComparableQSpanItem()
														.getqEnumBlockType(),
												numberGenes + " CDS", false,
												false, false, false, false,
												false, numberOtherMatchIt);
									}
								}
							} else {
								//System.err.println("ERROR in q drawSyntenyView unrecognized class for object sent");
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in q drawSyntenyView unrecognized class for object sent = "+apsiIT.getAbsoluteProportionQComparableQSpanItem().getClass().toString()));
							}
						}

						ArrayList<AbsoPropSCompaSSpanItem> alApscssiIT = new ArrayList<AbsoPropSCompaSSpanItem>();
						if (apsiIT.getAbsoluteProportionSComparableSSpanItem() != null) {
							alApscssiIT
									.add(apsiIT
											.getAbsoluteProportionSComparableSSpanItem());
						} else {
							boolean dontDrawSRegionTwice = false;
							int idxNextSlice = blockToGetFromArray + 1;
							if (idxNextSlice >= 0
									&& idxNextSlice < associatedGenomePanel
											.getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
											.size()) {
								HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(idxNextSlice)
										.getListHAPI()
										.get(associatedGenomePanel
												.getGenomePanelItem()
												.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
														idxNextSlice));
								if (siNextSlice
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (siNextSlice
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										if (((AbsoPropQSSyntItem) siNextSlice
												.getAbsoluteProportionQComparableQSSpanItem())
												.getPreviousGenomicRegionSInsertion() != null) {
											alApscssiIT
													.add(((AbsoPropQSSyntItem) siNextSlice
															.getAbsoluteProportionQComparableQSSpanItem())
															.getPreviousGenomicRegionSInsertion());
											if (((AbsoPropQSSyntItem) siNextSlice
													.getAbsoluteProportionQComparableQSSpanItem())
													.getPreviousGenomicRegionSInsertion()
													.isAnchoredToPrevious()) {
												dontDrawSRegionTwice = true;
											}
										}
									}
								} else if (siNextSlice
										.getAbsoluteProportionSComparableSSpanItem() != null) {
									if (siNextSlice
											.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
										if (((AbsoPropSGenoRegiInserItem) siNextSlice
												.getAbsoluteProportionSComparableSSpanItem())
												.getSEnumBlockType()
												.toString()
												.startsWith(
														"S_MISSING_TARGET_Q_")) {
											if (((AbsoPropSGenoRegiInserItem) siNextSlice
													.getAbsoluteProportionSComparableSSpanItem())
													.getsIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
												alApscssiIT
														.add(((AbsoPropSGenoRegiInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getsIfMissingTargetPreviousGenomicRegionSInsertion());
												if (((AbsoPropSGenoRegiInserItem) siNextSlice
														.getAbsoluteProportionSComparableSSpanItem())
														.getsIfMissingTargetPreviousGenomicRegionSInsertion()
														.isAnchoredToPrevious()) {
													dontDrawSRegionTwice = true;
												}
											}
										}
									} else if (siNextSlice
											.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
										if (((AbsoPropSGeneInserItem) siNextSlice
												.getAbsoluteProportionSComparableSSpanItem())
												.getSEnumBlockType()
												.toString()
												.startsWith(
														"S_MISSING_TARGET_Q_")) {
											if (((AbsoPropSGeneInserItem) siNextSlice
													.getAbsoluteProportionSComparableSSpanItem())
													.getIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
												alApscssiIT
														.add(((AbsoPropSGeneInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getIfMissingTargetPreviousGenomicRegionSInsertion());
												if (((AbsoPropSGeneInserItem) siNextSlice
														.getAbsoluteProportionSComparableSSpanItem())
														.getIfMissingTargetPreviousGenomicRegionSInsertion()
														.isAnchoredToPrevious()) {
													dontDrawSRegionTwice = true;
												}
											}
										}
									}
								}

							}
							if (!dontDrawSRegionTwice) {
								int idxPreviousSlice = blockToGetFromArray - 1;
								if (idxPreviousSlice >= 0
										&& idxPreviousSlice < associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.size()) {
									HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
											.getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
											.get(idxPreviousSlice)
											.getListHAPI()
											.get(associatedGenomePanel
													.getGenomePanelItem()
													.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
															idxPreviousSlice));
									if (siNextSlice
											.getAbsoluteProportionQComparableQSSpanItem() != null) {
										if (siNextSlice
												.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
											if (((AbsoPropQSSyntItem) siNextSlice
													.getAbsoluteProportionQComparableQSSpanItem())
													.getNextGenomicRegionSInsertion() != null) {
												alApscssiIT
														.add(((AbsoPropQSSyntItem) siNextSlice
																.getAbsoluteProportionQComparableQSSpanItem())
																.getNextGenomicRegionSInsertion());
											}

										}
									} else if (siNextSlice
											.getAbsoluteProportionSComparableSSpanItem() != null) {
										if (siNextSlice
												.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
											if (((AbsoPropSGenoRegiInserItem) siNextSlice
													.getAbsoluteProportionSComparableSSpanItem())
													.getSEnumBlockType()
													.toString()
													.startsWith(
															"S_MISSING_TARGET_Q_")) {
												if (((AbsoPropSGenoRegiInserItem) siNextSlice
														.getAbsoluteProportionSComparableSSpanItem())
														.getsIfMissingTargetNextGenomicRegionSInsertion() != null) {
													alApscssiIT
															.add(((AbsoPropSGenoRegiInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getsIfMissingTargetNextGenomicRegionSInsertion());
												}
											}
										} else if (siNextSlice
												.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
											if (((AbsoPropSGeneInserItem) siNextSlice
													.getAbsoluteProportionSComparableSSpanItem())
													.getSEnumBlockType()
													.toString()
													.startsWith(
															"S_MISSING_TARGET_Q_")) {
												if (((AbsoPropSGeneInserItem) siNextSlice
														.getAbsoluteProportionSComparableSSpanItem())
														.getIfMissingTargetNextGenomicRegionSInsertion() != null) {
													alApscssiIT
															.add(((AbsoPropSGeneInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getIfMissingTargetNextGenomicRegionSInsertion());
												}
											}
										}
									}
								}
							}

						}

						for (int p = 0; p < alApscssiIT.size(); p++) {
							AbsoPropSCompaSSpanItem apscssiIT = alApscssiIT
									.get(p);

							if (apscssiIT instanceof AbsoPropSGeneInserItem) {
								boolean allowToGetMoreData = false;
								if (shift_progress == 1) {
									allowToGetMoreData = true;
								}
								drawBlockSGeneInsertionOrMissingTarget(
										(AbsoPropSGeneInserItem) apscssiIT,
										false, false, numberOtherMatchIt,
										allowToGetMoreData,
										apsiIT.getIndexInFullArray(), i);
							} else if (apscssiIT instanceof AbsoPropSGenoRegiInserItem) {
								// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
								// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).getSGenomicRegionInsertionNumberGene(),
								// false, false);
								int numberGenes = ((AbsoPropSGenoRegiInserItem) apscssiIT)
										.getSGenomicRegionInsertionNumberGene();
								if (numberGenes < 0) {
									int sizePb = 
											//1 + 
											((AbsoPropSGenoRegiInserItem) apscssiIT)
													.getsPbStopSGenomicRegionInsertionInElement()
											- ((AbsoPropSGenoRegiInserItem) apscssiIT)
													.getsPbStartSGenomicRegionInsertionInElement();
									// System.err.println("i full="+i);
									drawBlockSGenomicRegionInsertionOrMissingTarget(
											apscssiIT.getSEnumBlockType(),
											convertSizeBpToTextWithAppropriateUnit(sizePb),
											false,
											false,
											true,
											((AbsoPropSGenoRegiInserItem) apscssiIT)
													.isAnchoredToPrevious(),
											((AbsoPropSGenoRegiInserItem) apscssiIT)
													.isAnchoredToNext(),
											((AbsoPropSGenoRegiInserItem) apscssiIT)
													.isAnnotationCollision(),
											((AbsoPropSGenoRegiInserItem) apscssiIT)
													.isAnchoredToStartElement(),
											((AbsoPropSGenoRegiInserItem) apscssiIT)
													.isAnchoredToStopElement(),
											numberOtherMatchIt);
									// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
									// false, false,
									// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
									// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext()
									// );
									if (shift_progress == 1) {
										// System.err.println("i="+i);
//										@SuppressWarnings("unused")
//										ResultLoadingDialog rld = new ResultLoadingDialog(
//												DetailledInfoStack.GeneCountForSGenomicRegionInsertion,
//												apsiIT.getIndexInFullArray(),
//												(associatedGenomePanel
//														.getGenomePanelItem()
//														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + i),
//												i,
//												Insyght.APP_CONTROLER
//														.getLIST_GENOME_PANEL()
//														.indexOf(
//																associatedGenomePanel),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//												CenterSLPAllGenomePanels.START_INDEX,
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStartQGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStopQGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStartSGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStopSGenome(),
//												((AbsoPropSGenoRegiInserItem) apscssiIT)
//														.getsOrigamiElementId(),
//												((AbsoPropSGenoRegiInserItem) apscssiIT)
//														.getsPbStartSGenomicRegionInsertionInElement(),
//												((AbsoPropSGenoRegiInserItem) apscssiIT)
//														.getsPbStopSGenomicRegionInsertionInElement(),
//												apscssiIT.getSEnumBlockType(),
//												false,
//												false,
//												Insyght.APP_CONTROLER
//														.getCurrentRefGenomePanelAndListOrgaResult()
//														.getViewTypeInsyght(),
//												((AbsoPropSGenoRegiInserItem) apscssiIT)
//														.isPreviousSOfNextSlice(),
//												((AbsoPropSGenoRegiInserItem) apscssiIT)
//														.isNextSOfPreviousSlice());
										RLD.getGeneCountForSGenomicRegionInsertion(
												apsiIT.getIndexInFullArray(),
												(associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + i),
												i,
												Insyght.APP_CONTROLER
														.getLIST_GENOME_PANEL()
														.indexOf(
																associatedGenomePanel),
												associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
												CenterSLPAllGenomePanels.START_INDEX,
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.getsOrigamiElementId(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.getsPbStartSGenomicRegionInsertionInElement(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.getsPbStopSGenomicRegionInsertionInElement(),
												apscssiIT.getSEnumBlockType(),
												false,
												false,
												Insyght.APP_CONTROLER
														.getCurrentRefGenomePanelAndListOrgaResult()
														.getViewTypeInsyght(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isPreviousSOfNextSlice(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isNextSOfPreviousSlice()
												);

									}

								} else {
									if (numberGenes == 0) {
										int sizePb = 
												//1 + 
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.getsPbStopSGenomicRegionInsertionInElement()
												- ((AbsoPropSGenoRegiInserItem) apscssiIT)
														.getsPbStartSGenomicRegionInsertionInElement();
										// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
										// sizePb+" bp", false, false);
										// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
										// false, false,
										// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
										// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
										drawBlockSGenomicRegionInsertionOrMissingTarget(
												apscssiIT.getSEnumBlockType(),
												convertSizeBpToTextWithAppropriateUnit(sizePb),
												false,
												false,
												true,
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnchoredToPrevious(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnchoredToNext(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnnotationCollision(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnchoredToStartElement(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnchoredToStopElement(),
												numberOtherMatchIt);
									} else {
										drawBlockSGenomicRegionInsertionOrMissingTarget(
												apscssiIT.getSEnumBlockType(),
												numberGenes + " CDS",
												false,
												false,
												false,
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnchoredToPrevious(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnchoredToNext(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnnotationCollision(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnchoredToStartElement(),
												((AbsoPropSGenoRegiInserItem) apscssiIT)
														.isAnchoredToStopElement(),
												numberOtherMatchIt);
									}
								}
							} else if (apscssiIT instanceof AbsoPropSElemItem) {
								// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
								// ((AbsoluteProportionSElementItem)apscssiIT).getsElementNumberGene(),
								// false, false);
								int numberGenes = ((AbsoPropSElemItem) apscssiIT)
										.getsElementNumberGene();
								if (numberGenes < 0) {
									int sizePb = 
											//1 + 
											((AbsoPropSElemItem) apscssiIT)
											.getsSizeOfElementinPb();
									drawBlockSGenomicRegionInsertionOrMissingTarget(
											apscssiIT.getSEnumBlockType(),
											convertSizeBpToTextWithAppropriateUnit(sizePb), false, false, true,
											true, true, false, false, false,
											numberOtherMatchIt);
									// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
									// false, false);
									if (shift_progress == 1) {
//										@SuppressWarnings("unused")
//										ResultLoadingDialog rld = new ResultLoadingDialog(
//												DetailledInfoStack.GeneCountForSElement,
//												apsiIT.getIndexInFullArray(),
//												(associatedGenomePanel
//														.getGenomePanelItem()
//														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + i),
//												i,
//												Insyght.APP_CONTROLER
//														.getLIST_GENOME_PANEL()
//														.indexOf(
//																associatedGenomePanel),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//												CenterSLPAllGenomePanels.START_INDEX,
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStartQGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStopQGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStartSGenome(),
//												associatedGenomePanel
//														.getGenomePanelItem()
//														.getPercentSyntenyShowStopSGenome(),
//												((AbsoPropSElemItem) apscssiIT)
//														.getsOrigamiElementId(),
//												-1,
//												-1,
//												apscssiIT.getSEnumBlockType(),
//												false,
//												false,
//												Insyght.APP_CONTROLER
//														.getCurrentRefGenomePanelAndListOrgaResult()
//														.getViewTypeInsyght(),
//												false, false);
										RLD.getGeneCountForSElement(
												apsiIT.getIndexInFullArray(),
												(associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + i),
												i,
												Insyght.APP_CONTROLER
														.getLIST_GENOME_PANEL()
														.indexOf(
																associatedGenomePanel),
												associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
												CenterSLPAllGenomePanels.START_INDEX,
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												((AbsoPropSElemItem) apscssiIT)
														.getsOrigamiElementId(),
												apscssiIT.getSEnumBlockType(),
												false,
												false,
												Insyght.APP_CONTROLER
														.getCurrentRefGenomePanelAndListOrgaResult()
														.getViewTypeInsyght());

									}
								} else {
									if (numberGenes == 0) {
										int sizePb = 
												//1 + 
												((AbsoPropSElemItem) apscssiIT)
												.getsSizeOfElementinPb();
										drawBlockSGenomicRegionInsertionOrMissingTarget(
												apscssiIT.getSEnumBlockType(),
												convertSizeBpToTextWithAppropriateUnit(sizePb), false, false,
												true, true, true, false, false,
												false, numberOtherMatchIt);
										// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
										// false, false);
									} else {
										drawBlockSGenomicRegionInsertionOrMissingTarget(
												apscssiIT.getSEnumBlockType(),
												numberGenes + " CDS", false,
												false, false, false, false,
												false, false, false,
												numberOtherMatchIt);
									}
								}
							} else {
								//System.err.println("ERROR in s drawSyntenyView unrecognized class for object sent");
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in s drawSyntenyView unrecognized class for object sent = "+apscssiIT.getClass().toString()));
							}
						}

					}
					// drawBlock(apsiIT.getSyntenyBlockType(),
					// apsiIT.getSyntenyNumberGene(), apsiIT.getQNumberGene(),
					// apsiIT.getSNumberGene(), false, false);
					// System.out.println("get and draw block "+blockToGetFromArray+" ; sumTranslate = "+sumTranslate);
				}

				// if first, permission to draw left
				if (i == 0) {
					int permissionToDrawLeft = (int) Math.floor(sumTranslate
							/ SLICE_CANVAS_WIDTH) + 1;
					// System.out.println("permission to draw left : "+permissionToDrawLeft+" ; sumTranslate = "+sumTranslate+" ; SLICE_CANVAS_WIDTH = "+SLICE_CANVAS_WIDTH);

					for (int j = 0; j < permissionToDrawLeft + 1; j++) {
						if (j == 0) {
							// first, translate way back
							contextBgk
									.translate(
											-(SLICE_CANVAS_WIDTH * permissionToDrawLeft),
											0);
							// System.out.println("backtranslation : "+(SLICE_CANVAS_WIDTH*permissionToDrawLeft));
							if ((blockToGetFromArray - permissionToDrawLeft) >= 0) {
								HolderAbsoluteProportionItem apsiIT = associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(blockToGetFromArray
												- permissionToDrawLeft)
										.getListHAPI()
										.get(associatedGenomePanel
												.getGenomePanelItem()
												.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
														blockToGetFromArray
																- permissionToDrawLeft));
								int numberOtherMatchIt = associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(blockToGetFromArray
												- permissionToDrawLeft)
										.getListHAPI().size() - 1;
								if (apsiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (apsiIT
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
										boolean allowToGetMoreData = false;
										if (shift_progress == 1) {
											allowToGetMoreData = true;
										}
										drawBlockQSGeneHomology(
												(AbsoPropQSGeneHomoItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem(),
												false,
												false,
												true,
												true,
												true,
												true,
												true,
												true,
												numberOtherMatchIt,
												// false,false,false,
												allowToGetMoreData,
												apsiIT.getIndexInFullArray(),
												-1,
												((AbsoPropQSGeneHomoItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getPreviousGenomicRegionSInsertion(),
												((AbsoPropQSGeneHomoItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getNextGenomicRegionSInsertion()
										// ,true, true, true
										);

									} else if (apsiIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										boolean allowToGetMoreData = false;
										if (shift_progress == 1) {
											allowToGetMoreData = true;
										}
										int numberPairsToAdd = ((AbsoPropQSSyntItem) apsiIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyNumberGene();
										// numberPairsToAdd +=
										// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsContainedOtherSyntenies().size();
										// numberPairsToAdd +=
										// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsMotherSyntenies().size();
										// numberPairsToAdd +=
										// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsSprungOffSyntenies().size();

										drawBlockQSSynteny(
												apsiIT.getAbsoluteProportionQComparableQSSpanItem()
														.getQsEnumBlockType(),
												numberPairsToAdd,
												false,
												false,
												numberOtherMatchIt,
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isContainedWithinAnotherMotherSynteny(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isSprungOffAnotherMotherSynteny(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isMotherOfSprungOffSyntenies(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isMotherOfContainedOtherSyntenies(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getPreviousGenomicRegionSInsertion(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getNextGenomicRegionSInsertion(),
												allowToGetMoreData, apsiIT
														.getIndexInFullArray(),
												-1
										// ,true, true, true
										);

									} else {
										//System.err.println("ERROR in permission to draw left qs drawSyntenyView unrecognized class for object sent");
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in permission to draw left " +
												"qs drawSyntenyView unrecognized class for object sent = "+apsiIT.getAbsoluteProportionQComparableQSSpanItem().getClass().toString()));
									}
								} else {
									if (apsiIT
											.getAbsoluteProportionQComparableQSpanItem() != null) {
										if (apsiIT
												.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
											boolean allowToGetMoreData = false;
											if (shift_progress == 1) {
												allowToGetMoreData = true;
											}
											drawBlockQGeneInsertionOrMissingTarget(
													(AbsoPropQGeneInserItem) apsiIT
															.getAbsoluteProportionQComparableQSpanItem(),
													false,
													false,
													numberOtherMatchIt,
													allowToGetMoreData,
													apsiIT.getIndexInFullArray(),
													-1);
										} else if (apsiIT
												.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
											// drawBlockQGenomicRegionInsertionOrMissingTarget(apsiIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(),
											// ((AbsoluteProportionQGenomicRegionInsertionItem)apsiIT.getAbsoluteProportionQComparableQSpanItem()).getQGenomicRegionInsertionNumberGene(),
											// false, false);
											int numberGenes = ((AbsoPropQGenoRegiInserItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQGenomicRegionInsertionNumberGene();
											if (numberGenes < 0) {
												int sizePb = 
														//1 +
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStopQGenomicRegionInsertionInElement()
														- ((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStartQGenomicRegionInsertionInElement();
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb),
														false,
														false,
														true,
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnnotationCollision(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStartElement(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStopElement(),
														numberOtherMatchIt);
												// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
												// false, false);
												if (shift_progress == 1) {
//													@SuppressWarnings("unused")
//													ResultLoadingDialog rld = new ResultLoadingDialog(
//															DetailledInfoStack.GeneCountForQGenomicRegionInsertion,
//															apsiIT.getIndexInFullArray(),
//															(associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1),
//															-1,
//															Insyght.APP_CONTROLER
//																	.getLIST_GENOME_PANEL()
//																	.indexOf(
//																			associatedGenomePanel),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//															CenterSLPAllGenomePanels.START_INDEX,
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStartQGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStopQGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStartSGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStopSGenome(),
//															((AbsoPropQGenoRegiInserItem) apsiIT
//																	.getAbsoluteProportionQComparableQSpanItem())
//																	.getqOrigamiElementId(),
//															((AbsoPropQGenoRegiInserItem) apsiIT
//																	.getAbsoluteProportionQComparableQSpanItem())
//																	.getqPbStartQGenomicRegionInsertionInElement(),
//															((AbsoPropQGenoRegiInserItem) apsiIT
//																	.getAbsoluteProportionQComparableQSpanItem())
//																	.getqPbStopQGenomicRegionInsertionInElement(),
//															apsiIT.getAbsoluteProportionQComparableQSpanItem()
//																	.getqEnumBlockType(),
//															false,
//															false,
//															Insyght.APP_CONTROLER
//																	.getCurrentRefGenomePanelAndListOrgaResult()
//																	.getViewTypeInsyght());
													RLD.getGeneCountForQGenomicRegionInsertion(
															apsiIT.getIndexInFullArray(),
															(associatedGenomePanel
																	.getGenomePanelItem()
																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1),
															-1,
															Insyght.APP_CONTROLER
																	.getLIST_GENOME_PANEL()
																	.indexOf(
																			associatedGenomePanel),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
															CenterSLPAllGenomePanels.START_INDEX,
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStartQGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStopQGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStartSGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStopSGenome(),
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.getqOrigamiElementId(),
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.getqPbStartQGenomicRegionInsertionInElement(),
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.getqPbStopQGenomicRegionInsertionInElement(),
															apsiIT.getAbsoluteProportionQComparableQSpanItem()
																	.getqEnumBlockType(),
															false,
															false,
															Insyght.APP_CONTROLER
																	.getCurrentRefGenomePanelAndListOrgaResult()
																	.getViewTypeInsyght());
												}

											} else {
												if (numberGenes == 0) {
													int sizePb = 
															//1 +
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.getqPbStopQGenomicRegionInsertionInElement()
															- ((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.getqPbStartQGenomicRegionInsertionInElement();
													// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
													// false, false);
													drawBlockQGenomicRegionInsertionOrMissingTarget(
															apsiIT.getAbsoluteProportionQComparableQSpanItem()
																	.getqEnumBlockType(),
															convertSizeBpToTextWithAppropriateUnit(sizePb),
															false,
															false,
															true,
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.isAnnotationCollision(),
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.isAnchoredToStartElement(),
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.isAnchoredToStopElement(),
															numberOtherMatchIt);

												} else {
													drawBlockQGenomicRegionInsertionOrMissingTarget(
															apsiIT.getAbsoluteProportionQComparableQSpanItem()
																	.getqEnumBlockType(),
															numberGenes
																	+ " CDS",
															false,
															false,
															false,
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.isAnnotationCollision(),
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.isAnchoredToStartElement(),
															((AbsoPropQGenoRegiInserItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.isAnchoredToStopElement(),
															numberOtherMatchIt);
												}
											}
										} else if (apsiIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem) {
											// drawBlockQGenomicRegionInsertionOrMissingTarget(apsiIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(),
											// ((AbsoluteProportionQElementItem)apsiIT.getAbsoluteProportionQComparableQSpanItem()).getqElementNumberGene(),
											// false, false);
											int numberGenes = ((AbsoPropQElemItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqElementNumberGene();
											if (numberGenes < 0) {
												int sizePb = ((AbsoPropQElemItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getQsizeOfElementinPb();
												// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
												// false, false);
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb), false,
														false, true, false,
														false, false,
														numberOtherMatchIt);
												if (shift_progress == 1) {
//													@SuppressWarnings("unused")
//													ResultLoadingDialog rld = new ResultLoadingDialog(
//															DetailledInfoStack.GeneCountForQElement,
//															apsiIT.getIndexInFullArray(),
//															(associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1),
//															-1,
//															Insyght.APP_CONTROLER
//																	.getLIST_GENOME_PANEL()
//																	.indexOf(
//																			associatedGenomePanel),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//															CenterSLPAllGenomePanels.START_INDEX,
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStartQGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStopQGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStartSGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStopSGenome(),
//															((AbsoPropQElemItem) apsiIT
//																	.getAbsoluteProportionQComparableQSpanItem())
//																	.getqOrigamiElementId(),
//															-1,
//															-1,
//															apsiIT.getAbsoluteProportionQComparableQSpanItem()
//																	.getqEnumBlockType(),
//															false,
//															false,
//															Insyght.APP_CONTROLER
//																	.getCurrentRefGenomePanelAndListOrgaResult()
//																	.getViewTypeInsyght());
													
													RLD.getGeneCountForQElement(
															apsiIT.getIndexInFullArray(),
															(associatedGenomePanel
																	.getGenomePanelItem()
																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1),
															-1,
															Insyght.APP_CONTROLER
																	.getLIST_GENOME_PANEL()
																	.indexOf(
																			associatedGenomePanel),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
															CenterSLPAllGenomePanels.START_INDEX,
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStartQGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStopQGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStartSGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStopSGenome(),
															((AbsoPropQElemItem) apsiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.getqOrigamiElementId(),
																	apsiIT.getAbsoluteProportionQComparableQSpanItem()
																	.getqEnumBlockType(),
															false,
															false,
															Insyght.APP_CONTROLER
																	.getCurrentRefGenomePanelAndListOrgaResult()
																	.getViewTypeInsyght());

												}
											} else {
												if (numberGenes == 0) {
													int sizePb = 
															//1 + 
															((AbsoPropQElemItem) apsiIT
															.getAbsoluteProportionQComparableQSpanItem())
															.getQsizeOfElementinPb();
													// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
													// false, false);
													drawBlockQGenomicRegionInsertionOrMissingTarget(
															apsiIT.getAbsoluteProportionQComparableQSpanItem()
																	.getqEnumBlockType(),
															convertSizeBpToTextWithAppropriateUnit(sizePb),
															false, false, true,
															false, false,
															false,
															numberOtherMatchIt);
												} else {
													drawBlockQGenomicRegionInsertionOrMissingTarget(
															apsiIT.getAbsoluteProportionQComparableQSpanItem()
																	.getqEnumBlockType(),
															numberGenes
																	+ " CDS",
															false, false,
															false, false,
															false, false,
															numberOtherMatchIt);
												}
											}
										} else {
											//System.err.println("ERROR in permission to draw left q drawSyntenyView unrecognized class for object sent");
											Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in permission to draw left q " +
													"drawSyntenyView unrecognized class for object sent = "+apsiIT.getAbsoluteProportionQComparableQSpanItem().getClass().toString()));
										}
									}

									ArrayList<AbsoPropSCompaSSpanItem> alApscssiIT = new ArrayList<AbsoPropSCompaSSpanItem>();
									if (apsiIT
											.getAbsoluteProportionSComparableSSpanItem() != null) {
										alApscssiIT
												.add(apsiIT
														.getAbsoluteProportionSComparableSSpanItem());
									} else {

										boolean dontDrawSRegionTwice = false;
										int idxNextSlice = blockToGetFromArray
												- permissionToDrawLeft + 1;
										if (idxNextSlice >= 0
												&& idxNextSlice < associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.size()) {
											HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.get(idxNextSlice)
													.getListHAPI()
													.get(associatedGenomePanel
															.getGenomePanelItem()
															.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																	idxNextSlice));
											if (siNextSlice
													.getAbsoluteProportionQComparableQSSpanItem() != null) {
												if (siNextSlice
														.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
													if (((AbsoPropQSSyntItem) siNextSlice
															.getAbsoluteProportionQComparableQSSpanItem())
															.getPreviousGenomicRegionSInsertion() != null) {
														alApscssiIT
																.add(((AbsoPropQSSyntItem) siNextSlice
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getPreviousGenomicRegionSInsertion());
														if (((AbsoPropQSSyntItem) siNextSlice
																.getAbsoluteProportionQComparableQSSpanItem())
																.getPreviousGenomicRegionSInsertion()
																.isAnchoredToPrevious()) {
															dontDrawSRegionTwice = true;
														}
													}
												}
											} else if (siNextSlice
													.getAbsoluteProportionSComparableSSpanItem() != null) {
												if (siNextSlice
														.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
													if (((AbsoPropSGenoRegiInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getSEnumBlockType()
															.toString()
															.startsWith(
																	"S_MISSING_TARGET_Q_")) {
														if (((AbsoPropSGenoRegiInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getsIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropSGenoRegiInserItem) siNextSlice
																			.getAbsoluteProportionSComparableSSpanItem())
																			.getsIfMissingTargetPreviousGenomicRegionSInsertion());
															if (((AbsoPropSGenoRegiInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getsIfMissingTargetPreviousGenomicRegionSInsertion()
																	.isAnchoredToPrevious()) {
																dontDrawSRegionTwice = true;
															}
														}
													}
												} else if (siNextSlice
														.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
													if (((AbsoPropSGeneInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getSEnumBlockType()
															.toString()
															.startsWith(
																	"S_MISSING_TARGET_Q_")) {
														if (((AbsoPropSGeneInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropSGeneInserItem) siNextSlice
																			.getAbsoluteProportionSComparableSSpanItem())
																			.getIfMissingTargetPreviousGenomicRegionSInsertion());
															if (((AbsoPropSGeneInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getIfMissingTargetPreviousGenomicRegionSInsertion()
																	.isAnchoredToPrevious()) {
																dontDrawSRegionTwice = true;
															}
														}
													}
												}
											}

										}
										if (!dontDrawSRegionTwice) {
											int idxPreviousSlice = blockToGetFromArray
													- permissionToDrawLeft - 1;
											if (idxPreviousSlice >= 0
													&& idxPreviousSlice < associatedGenomePanel
															.getGenomePanelItem()
															.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
															.size()) {
												HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.get(idxPreviousSlice)
														.getListHAPI()
														.get(associatedGenomePanel
																.getGenomePanelItem()
																.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																		idxPreviousSlice));
												if (siNextSlice
														.getAbsoluteProportionQComparableQSSpanItem() != null) {
													if (siNextSlice
															.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
														if (((AbsoPropQSSyntItem) siNextSlice
																.getAbsoluteProportionQComparableQSSpanItem())
																.getNextGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropQSSyntItem) siNextSlice
																			.getAbsoluteProportionQComparableQSSpanItem())
																			.getNextGenomicRegionSInsertion());
														}

													}
												} else if (siNextSlice
														.getAbsoluteProportionSComparableSSpanItem() != null) {
													if (siNextSlice
															.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
														if (((AbsoPropSGenoRegiInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getSEnumBlockType()
																.toString()
																.startsWith(
																		"S_MISSING_TARGET_Q_")) {
															if (((AbsoPropSGenoRegiInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getsIfMissingTargetNextGenomicRegionSInsertion() != null) {
																alApscssiIT
																		.add(((AbsoPropSGenoRegiInserItem) siNextSlice
																				.getAbsoluteProportionSComparableSSpanItem())
																				.getsIfMissingTargetNextGenomicRegionSInsertion());
															}
														}
													} else if (siNextSlice
															.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
														if (((AbsoPropSGeneInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getSEnumBlockType()
																.toString()
																.startsWith(
																		"S_MISSING_TARGET_Q_")) {
															if (((AbsoPropSGeneInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getIfMissingTargetNextGenomicRegionSInsertion() != null) {
																alApscssiIT
																		.add(((AbsoPropSGeneInserItem) siNextSlice
																				.getAbsoluteProportionSComparableSSpanItem())
																				.getIfMissingTargetNextGenomicRegionSInsertion());
															}
														}
													}

												}

											}
										}
									}

									for (int p = 0; p < alApscssiIT.size(); p++) {
										AbsoPropSCompaSSpanItem apscssiIT = alApscssiIT
												.get(p);
										if (apscssiIT instanceof AbsoPropSGeneInserItem) {
											boolean allowToGetMoreData = false;
											if (shift_progress == 1) {
												allowToGetMoreData = true;
											}
											drawBlockSGeneInsertionOrMissingTarget(
													(AbsoPropSGeneInserItem) apscssiIT,
													false,
													false,
													numberOtherMatchIt,
													allowToGetMoreData,
													apsiIT.getIndexInFullArray(),
													-1);
										} else if (apscssiIT instanceof AbsoPropSGenoRegiInserItem) {
											// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
											// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).getSGenomicRegionInsertionNumberGene(),
											// false, false);
											int numberGenes = ((AbsoPropSGenoRegiInserItem) apscssiIT)
													.getSGenomicRegionInsertionNumberGene();
											if (numberGenes < 0) {
												int sizePb = 
														//1 +
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStopSGenomicRegionInsertionInElement()
														- ((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStartSGenomicRegionInsertionInElement();
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb),
														false,
														false,
														true,
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToPrevious(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToNext(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnnotationCollision(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStartElement(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStopElement(),
														numberOtherMatchIt);
												// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
												// false, false,
												// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
												// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
												if (shift_progress == 1) {
//													@SuppressWarnings("unused")
//													ResultLoadingDialog rld = new ResultLoadingDialog(
//															DetailledInfoStack.GeneCountForSGenomicRegionInsertion,
//															apsiIT.getIndexInFullArray(),
//															(associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1),
//															-1,
//															Insyght.APP_CONTROLER
//																	.getLIST_GENOME_PANEL()
//																	.indexOf(
//																			associatedGenomePanel),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//															CenterSLPAllGenomePanels.START_INDEX,
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStartQGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStopQGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStartSGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStopSGenome(),
//															((AbsoPropSGenoRegiInserItem) apscssiIT)
//																	.getsOrigamiElementId(),
//															((AbsoPropSGenoRegiInserItem) apscssiIT)
//																	.getsPbStartSGenomicRegionInsertionInElement(),
//															((AbsoPropSGenoRegiInserItem) apscssiIT)
//																	.getsPbStopSGenomicRegionInsertionInElement(),
//															apscssiIT
//																	.getSEnumBlockType(),
//															false,
//															false,
//															Insyght.APP_CONTROLER
//																	.getCurrentRefGenomePanelAndListOrgaResult()
//																	.getViewTypeInsyght(),
//															((AbsoPropSGenoRegiInserItem) apscssiIT)
//																	.isPreviousSOfNextSlice(),
//															((AbsoPropSGenoRegiInserItem) apscssiIT)
//																	.isNextSOfPreviousSlice());
													RLD.getGeneCountForSGenomicRegionInsertion(
															apsiIT.getIndexInFullArray(),
															(associatedGenomePanel
																	.getGenomePanelItem()
																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1),
															-1,
															Insyght.APP_CONTROLER
																	.getLIST_GENOME_PANEL()
																	.indexOf(
																			associatedGenomePanel),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
															CenterSLPAllGenomePanels.START_INDEX,
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStartQGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStopQGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStartSGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStopSGenome(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsOrigamiElementId(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsPbStartSGenomicRegionInsertionInElement(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsPbStopSGenomicRegionInsertionInElement(),
															apscssiIT
																	.getSEnumBlockType(),
															false,
															false,
															Insyght.APP_CONTROLER
																	.getCurrentRefGenomePanelAndListOrgaResult()
																	.getViewTypeInsyght(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isPreviousSOfNextSlice(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isNextSOfPreviousSlice()
															);

												}

											} else {
												if (numberGenes == 0) {
													int sizePb = 
															//1 +
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsPbStopSGenomicRegionInsertionInElement()
															- ((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsPbStartSGenomicRegionInsertionInElement();
													drawBlockSGenomicRegionInsertionOrMissingTarget(
															apscssiIT
																	.getSEnumBlockType(),
															convertSizeBpToTextWithAppropriateUnit(sizePb),
															false,
															false,
															true,
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnchoredToPrevious(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnchoredToNext(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnnotationCollision(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnchoredToStartElement(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnchoredToStopElement(),
															numberOtherMatchIt);
													// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
													// false, false,
													// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
													// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
												} else {
													drawBlockSGenomicRegionInsertionOrMissingTarget(
															apscssiIT
																	.getSEnumBlockType(),
															numberGenes + " CDS",
															false,
															false,
															false,
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnchoredToPrevious(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnchoredToNext(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnnotationCollision(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnchoredToStartElement(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.isAnchoredToStopElement(),
															numberOtherMatchIt);
												}
											}
										} else if (apscssiIT instanceof AbsoPropSElemItem) {
											// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
											// ((AbsoluteProportionSElementItem)apscssiIT).getsElementNumberGene(),
											// false, false);
											int numberGenes = ((AbsoPropSElemItem) apscssiIT)
													.getsElementNumberGene();
											if (numberGenes < 0) {
												int sizePb = 
														//1 + 
														((AbsoPropSElemItem) apscssiIT)
														.getsSizeOfElementinPb();
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb), false,
														false, true, true,
														true, false, false,
														false,
														numberOtherMatchIt);
												// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
												// false, false);
												if (shift_progress == 1) {
//													@SuppressWarnings("unused")
//													ResultLoadingDialog rld = new ResultLoadingDialog(
//															DetailledInfoStack.GeneCountForSElement,
//															apsiIT.getIndexInFullArray(),
//															(associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1),
//															-1,
//															Insyght.APP_CONTROLER
//																	.getLIST_GENOME_PANEL()
//																	.indexOf(
//																			associatedGenomePanel),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//															CenterSLPAllGenomePanels.START_INDEX,
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStartQGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStopQGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStartSGenome(),
//															associatedGenomePanel
//																	.getGenomePanelItem()
//																	.getPercentSyntenyShowStopSGenome(),
//															((AbsoPropSElemItem) apscssiIT)
//																	.getsOrigamiElementId(),
//															-1,
//															-1,
//															apscssiIT
//																	.getSEnumBlockType(),
//															false,
//															false,
//															Insyght.APP_CONTROLER
//																	.getCurrentRefGenomePanelAndListOrgaResult()
//																	.getViewTypeInsyght(),
//															false, false);
													RLD.getGeneCountForSElement(
															apsiIT.getIndexInFullArray(),
															(associatedGenomePanel
																	.getGenomePanelItem()
																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() - 1),
															-1,
															Insyght.APP_CONTROLER
																	.getLIST_GENOME_PANEL()
																	.indexOf(
																			associatedGenomePanel),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
															CenterSLPAllGenomePanels.START_INDEX,
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStartQGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStopQGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStartSGenome(),
															associatedGenomePanel
																	.getGenomePanelItem()
																	.getPercentSyntenyShowStopSGenome(),
															((AbsoPropSElemItem) apscssiIT)
																	.getsOrigamiElementId(),
																	apscssiIT
																	.getSEnumBlockType(),
															false,
															false,
															Insyght.APP_CONTROLER
																	.getCurrentRefGenomePanelAndListOrgaResult()
																	.getViewTypeInsyght());

												}

											} else {
												if (numberGenes == 0) {
													int sizePb = 
															//1 + 
															((AbsoPropSElemItem) apscssiIT)
															.getsSizeOfElementinPb();
													drawBlockSGenomicRegionInsertionOrMissingTarget(
															apscssiIT
																	.getSEnumBlockType(),
															convertSizeBpToTextWithAppropriateUnit(sizePb),
															false, false, true,
															true, true, false,
															false, false,
															numberOtherMatchIt);
													// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
													// false, false);
												} else {
													drawBlockSGenomicRegionInsertionOrMissingTarget(
															apscssiIT
																	.getSEnumBlockType(),
															numberGenes + " CDS",
															false, false,
															false, false,
															false, false,
															false, false,
															numberOtherMatchIt);
												}
											}
										} else {
											//System.err.println("ERROR in permission to draw left s drawSyntenyView unrecognized class for object sent");
											Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in permission to draw left" +
													" s drawSyntenyView unrecognized class for object sent = "+apscssiIT.getClass().toString()));
										}
									}

								}

								// System.out.println("get and draw block "+(blockToGetFromArray-permissionToDrawLeft));
							}// else{
								// System.out.println("wanted to get and draw block "+(blockToGetFromArray-permissionToDrawLeft)+" but out of range in array");
							// }

						} else if (j == permissionToDrawLeft) {
							// end of stuff, translate once to go back to normal
							contextBgk.translate(SLICE_CANVAS_WIDTH, 0);
							// System.out.println("translation upward final : "+(SLICE_CANVAS_WIDTH*permissionToDrawLeft));

						} else {
							// translate and draw normal
							contextBgk.translate(SLICE_CANVAS_WIDTH, 0);
							// System.out.println("translation upward : "+SLICE_CANVAS_WIDTH);
							if ((blockToGetFromArray - permissionToDrawLeft + j) > 0) {
								HolderAbsoluteProportionItem apsiIT = associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(blockToGetFromArray
												- permissionToDrawLeft + j)
										.getListHAPI()
										.get(associatedGenomePanel
												.getGenomePanelItem()
												.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
														blockToGetFromArray
																- permissionToDrawLeft
																+ j));
								int numberOtherMatchIt = associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(blockToGetFromArray
												- permissionToDrawLeft + j)
										.getListHAPI().size() - 1;
								if (apsiIT
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (apsiIT
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
										drawBlockQSGeneHomology(
												(AbsoPropQSGeneHomoItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem(),
												false,
												false,
												true,
												true,
												true,
												true,
												true,
												true,
												numberOtherMatchIt,
												// false,false,false,
												false,
												-1,
												-1,
												((AbsoPropQSGeneHomoItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getPreviousGenomicRegionSInsertion(),
												((AbsoPropQSGeneHomoItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getNextGenomicRegionSInsertion()
										// ,true, true, true
										);

									} else if (apsiIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										int numberPairsToAdd = ((AbsoPropQSSyntItem) apsiIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyNumberGene();
										// numberPairsToAdd +=
										// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsContainedOtherSyntenies().size();
										// numberPairsToAdd +=
										// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsMotherSyntenies().size();
										// numberPairsToAdd +=
										// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsSprungOffSyntenies().size();

										drawBlockQSSynteny(
												apsiIT.getAbsoluteProportionQComparableQSSpanItem()
														.getQsEnumBlockType(),
												numberPairsToAdd,
												false,
												false,
												numberOtherMatchIt,
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isContainedWithinAnotherMotherSynteny(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isSprungOffAnotherMotherSynteny(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isMotherOfSprungOffSyntenies(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isMotherOfContainedOtherSyntenies(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getPreviousGenomicRegionSInsertion(),
												((AbsoPropQSSyntItem) apsiIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getNextGenomicRegionSInsertion(),
												false, -1, -1
										// ,true, true, true
										);

									} else {
										//System.err.println("ERROR in permission to draw left back to normal qs drawSyntenyView unrecognized class for object sent");
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in permission to draw left back to normal " +
												"qs drawSyntenyView unrecognized class for object sent = "+apsiIT.getAbsoluteProportionQComparableQSSpanItem().getClass().toString()));
									}
								} else {
									if (apsiIT
											.getAbsoluteProportionQComparableQSpanItem() != null) {
										if (apsiIT
												.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
											drawBlockQGeneInsertionOrMissingTarget(
													(AbsoPropQGeneInserItem) apsiIT
															.getAbsoluteProportionQComparableQSpanItem(),
													false, false,
													numberOtherMatchIt, false,
													-1, -1);
										} else if (apsiIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
											// drawBlockQGenomicRegionInsertionOrMissingTarget(apsiIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(),
											// ((AbsoluteProportionQGenomicRegionInsertionItem)apsiIT.getAbsoluteProportionQComparableQSpanItem()).getQGenomicRegionInsertionNumberGene(),
											// false, false);
											int numberGenes = ((AbsoPropQGenoRegiInserItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQGenomicRegionInsertionNumberGene();
											if (numberGenes <= 0) {
												int sizePb = 
														//1 +
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStopQGenomicRegionInsertionInElement()
														- ((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStartQGenomicRegionInsertionInElement();
												// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
												// false, false);
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb),
														false,
														false,
														true,
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnnotationCollision(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStartElement(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStopElement(),
														numberOtherMatchIt);
											} else {
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														numberGenes + " CDS",
														false,
														false,
														false,
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnnotationCollision(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStartElement(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStopElement(),
														numberOtherMatchIt);
											}
										} else if (apsiIT
												.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem) {
											// drawBlockQGenomicRegionInsertionOrMissingTarget(apsiIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(),
											// ((AbsoluteProportionQElementItem)apsiIT.getAbsoluteProportionQComparableQSpanItem()).getqElementNumberGene(),
											// false, false);
											int numberGenes = ((AbsoPropQElemItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqElementNumberGene();
											if (numberGenes <= 0) {
												int sizePb = 
														//1 + 
														((AbsoPropQElemItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getQsizeOfElementinPb();
												// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
												// false, false);
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb), false,
														false, true, false,
														false, false,
														numberOtherMatchIt);
											} else {
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														numberGenes + " CDS",
														false, false, false,
														false, false, false,
														numberOtherMatchIt);
											}
										} else {
											//System.err.println("ERROR in permission to draw left back to normal q drawSyntenyView unrecognized class for object sent");
											Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in permission to draw left back to normal " +
													"q drawSyntenyView unrecognized class for object sent = "+apsiIT.getAbsoluteProportionQComparableQSpanItem().getClass().toString()));
										}

									}

									ArrayList<AbsoPropSCompaSSpanItem> alApscssiIT = new ArrayList<AbsoPropSCompaSSpanItem>();
									if (apsiIT
											.getAbsoluteProportionSComparableSSpanItem() != null) {
										alApscssiIT
												.add(apsiIT
														.getAbsoluteProportionSComparableSSpanItem());
									} else {
										boolean dontDrawSRegionTwice = true;
										int idxNextSlice = blockToGetFromArray
												- permissionToDrawLeft + j + 1;
										if (idxNextSlice >= 0
												&& idxNextSlice < associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.size()) {
											HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.get(idxNextSlice)
													.getListHAPI()
													.get(associatedGenomePanel
															.getGenomePanelItem()
															.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																	idxNextSlice));
											if (siNextSlice
													.getAbsoluteProportionQComparableQSSpanItem() != null) {
												if (siNextSlice
														.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
													if (((AbsoPropQSSyntItem) siNextSlice
															.getAbsoluteProportionQComparableQSSpanItem())
															.getPreviousGenomicRegionSInsertion() != null) {
														alApscssiIT
																.add(((AbsoPropQSSyntItem) siNextSlice
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getPreviousGenomicRegionSInsertion());
														if (((AbsoPropQSSyntItem) siNextSlice
																.getAbsoluteProportionQComparableQSSpanItem())
																.getPreviousGenomicRegionSInsertion()
																.isAnchoredToPrevious()) {
															dontDrawSRegionTwice = true;
														}
													}
												}
											} else if (siNextSlice
													.getAbsoluteProportionSComparableSSpanItem() != null) {
												if (siNextSlice
														.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
													if (((AbsoPropSGenoRegiInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getSEnumBlockType()
															.toString()
															.startsWith(
																	"S_MISSING_TARGET_Q_")) {
														if (((AbsoPropSGenoRegiInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getsIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropSGenoRegiInserItem) siNextSlice
																			.getAbsoluteProportionSComparableSSpanItem())
																			.getsIfMissingTargetPreviousGenomicRegionSInsertion());
															if (((AbsoPropSGenoRegiInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getsIfMissingTargetPreviousGenomicRegionSInsertion()
																	.isAnchoredToPrevious()) {
																dontDrawSRegionTwice = true;
															}
														}
													}
												} else if (siNextSlice
														.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
													if (((AbsoPropSGeneInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getSEnumBlockType()
															.toString()
															.startsWith(
																	"S_MISSING_TARGET_Q_")) {
														if (((AbsoPropSGeneInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropSGeneInserItem) siNextSlice
																			.getAbsoluteProportionSComparableSSpanItem())
																			.getIfMissingTargetPreviousGenomicRegionSInsertion());
															if (((AbsoPropSGeneInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getIfMissingTargetPreviousGenomicRegionSInsertion()
																	.isAnchoredToPrevious()) {
																dontDrawSRegionTwice = true;
															}
														}
													}
												}
											}

										}
										if (!dontDrawSRegionTwice) {
											int idxPreviousSlice = blockToGetFromArray
													- permissionToDrawLeft
													+ j
													- 1;
											if (idxPreviousSlice >= 0
													&& idxPreviousSlice < associatedGenomePanel
															.getGenomePanelItem()
															.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
															.size()) {
												HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.get(idxPreviousSlice)
														.getListHAPI()
														.get(associatedGenomePanel
																.getGenomePanelItem()
																.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																		idxPreviousSlice));
												if (siNextSlice
														.getAbsoluteProportionQComparableQSSpanItem() != null) {
													if (siNextSlice
															.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
														if (((AbsoPropQSSyntItem) siNextSlice
																.getAbsoluteProportionQComparableQSSpanItem())
																.getNextGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropQSSyntItem) siNextSlice
																			.getAbsoluteProportionQComparableQSSpanItem())
																			.getNextGenomicRegionSInsertion());
														}
													}
												} else if (siNextSlice
														.getAbsoluteProportionSComparableSSpanItem() != null) {
													if (siNextSlice
															.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
														if (((AbsoPropSGenoRegiInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getSEnumBlockType()
																.toString()
																.startsWith(
																		"S_MISSING_TARGET_Q_")) {
															if (((AbsoPropSGenoRegiInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getsIfMissingTargetNextGenomicRegionSInsertion() != null) {
																alApscssiIT
																		.add(((AbsoPropSGenoRegiInserItem) siNextSlice
																				.getAbsoluteProportionSComparableSSpanItem())
																				.getsIfMissingTargetNextGenomicRegionSInsertion());
															}
														}
													} else if (siNextSlice
															.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
														if (((AbsoPropSGeneInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getSEnumBlockType()
																.toString()
																.startsWith(
																		"S_MISSING_TARGET_Q_")) {
															if (((AbsoPropSGeneInserItem) siNextSlice
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getIfMissingTargetNextGenomicRegionSInsertion() != null) {
																alApscssiIT
																		.add(((AbsoPropSGeneInserItem) siNextSlice
																				.getAbsoluteProportionSComparableSSpanItem())
																				.getIfMissingTargetNextGenomicRegionSInsertion());
															}
														}
													}
												}

											}
										}

									}

									for (int p = 0; p < alApscssiIT.size(); p++) {
										AbsoPropSCompaSSpanItem apscssiIT = alApscssiIT
												.get(p);

										if (apscssiIT instanceof AbsoPropSGeneInserItem) {
											drawBlockSGeneInsertionOrMissingTarget(
													(AbsoPropSGeneInserItem) apscssiIT,
													false, false,
													numberOtherMatchIt, false,
													-1, -1);
										} else if (apscssiIT instanceof AbsoPropSGenoRegiInserItem) {
											// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
											// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).getSGenomicRegionInsertionNumberGene(),
											// false, false);
											int numberGenes = ((AbsoPropSGenoRegiInserItem) apscssiIT)
													.getSGenomicRegionInsertionNumberGene();
											if (numberGenes <= 0) {
												int sizePb = 
														//1 +
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStopSGenomicRegionInsertionInElement()
														- ((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStartSGenomicRegionInsertionInElement();
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb),
														false,
														false,
														true,
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToPrevious(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToNext(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnnotationCollision(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStartElement(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStopElement(),
														numberOtherMatchIt);
												// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
												// false, false,
												// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
												// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
											} else {
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														numberGenes + " CDS",
														false,
														false,
														false,
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToPrevious(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToNext(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnnotationCollision(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStartElement(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStopElement(),
														numberOtherMatchIt);
											}
										} else if (apscssiIT instanceof AbsoPropSElemItem) {
											// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
											// ((AbsoluteProportionSElementItem)apscssiIT).getsElementNumberGene(),
											// false, false);
											int numberGenes = ((AbsoPropSElemItem) apscssiIT)
													.getsElementNumberGene();
											if (numberGenes <= 0) {
												int sizePb = 
														//1 + 
														((AbsoPropSElemItem) apscssiIT)
														.getsSizeOfElementinPb();
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb), false,
														false, true, true,
														true, false, false,
														false,
														numberOtherMatchIt);
												// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
												// false, false);
											} else {
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														numberGenes + " CDS",
														false, false, false,
														false, false, false,
														false, false,
														numberOtherMatchIt);
											}
										} else {
											//System.err.println("ERROR in permission to draw left back to normal s drawSyntenyView unrecognized class for object sent");
											Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in permission to draw left back to normal s " +
													"drawSyntenyView unrecognized class for object sent = "+apscssiIT.getClass().toString()));
										}
									}
								}
								// drawBlock(apsiIT.getSyntenyBlockType(),
								// apsiIT.getSyntenyNumberGene(),
								// apsiIT.getQNumberGene(),
								// apsiIT.getSNumberGene(), false, false);
								// System.out.println("get and draw block "+(blockToGetFromArray-permissionToDrawLeft+j));
							}// else{
								// System.out.println("wanted to get and draw block "+(blockToGetFromArray-permissionToDrawLeft+j)+" but out of range in array");
							// }
						}

					}

				}

				// if last, permission to draw right?
				if (i == (NUMBER_SLICE_WIDTH - 1)) {
					int permissionToDrawRight = (int) Math
							.floor((TOTAL_CANVAS_WIDTH - sumTranslate - SLICE_CANVAS_WIDTH)
									/ SLICE_CANVAS_WIDTH) + 1;
					// System.out.println("permission to draw right : "+permissionToDrawRight+" ; sumTranslate = "+sumTranslate+" ; SLICE_CANVAS_WIDTH = "+SLICE_CANVAS_WIDTH);

					for (int j = 0; j < permissionToDrawRight; j++) {
						contextBgk.translate(SLICE_CANVAS_WIDTH, 0);
						// System.out.println("translation up : "+SLICE_CANVAS_WIDTH);
						if ((blockToGetFromArray + j + 1) < associatedGenomePanel
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.size()) {
							HolderAbsoluteProportionItem apsiIT = associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(blockToGetFromArray + j + 1)
									.getListHAPI()
									.get(associatedGenomePanel
											.getGenomePanelItem()
											.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
													blockToGetFromArray + j + 1));
							int numberOtherMatchIt = associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(blockToGetFromArray + j + 1)
									.getListHAPI().size() - 1;
							if (apsiIT
									.getAbsoluteProportionQComparableQSSpanItem() != null) {
								if (apsiIT
										.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
									boolean allowToGetMoreData = false;
									if (shift_progress == 1) {
										allowToGetMoreData = true;
									}
									drawBlockQSGeneHomology(
											(AbsoPropQSGeneHomoItem) apsiIT
													.getAbsoluteProportionQComparableQSSpanItem(),
											false,
											false,
											true,
											true,
											true,
											true,
											true,
											true,
											numberOtherMatchIt,
											// false,false,false,
											allowToGetMoreData,
											apsiIT.getIndexInFullArray(),
											(i + j + 1),
											((AbsoPropQSGeneHomoItem) apsiIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getPreviousGenomicRegionSInsertion(),
											((AbsoPropQSGeneHomoItem) apsiIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getNextGenomicRegionSInsertion()
									// ,true, true, true
									);
									// false, -1, -1);
								} else if (apsiIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
									boolean allowToGetMoreData = false;
									if (shift_progress == 1) {
										allowToGetMoreData = true;
									}
									int numberPairsToAdd = ((AbsoPropQSSyntItem) apsiIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getSyntenyNumberGene();
									// numberPairsToAdd +=
									// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsContainedOtherSyntenies().size();
									// numberPairsToAdd +=
									// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsMotherSyntenies().size();
									// numberPairsToAdd +=
									// ((AbsoPropQSSyntItem)apsiIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsSprungOffSyntenies().size();

									drawBlockQSSynteny(
											apsiIT.getAbsoluteProportionQComparableQSSpanItem()
													.getQsEnumBlockType(),
											numberPairsToAdd,
											false,
											false,
											numberOtherMatchIt,
											((AbsoPropQSSyntItem) apsiIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.isContainedWithinAnotherMotherSynteny(),
											((AbsoPropQSSyntItem) apsiIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.isSprungOffAnotherMotherSynteny(),
											((AbsoPropQSSyntItem) apsiIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.isMotherOfSprungOffSyntenies(),
											((AbsoPropQSSyntItem) apsiIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.isMotherOfContainedOtherSyntenies(),
											((AbsoPropQSSyntItem) apsiIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getPreviousGenomicRegionSInsertion(),
											((AbsoPropQSSyntItem) apsiIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getNextGenomicRegionSInsertion(),
											allowToGetMoreData, apsiIT
													.getIndexInFullArray(), (i
													+ j + 1)
									// ,true, true, true
									);

								} else {
									//System.err.println("ERROR in permission to draw right qs drawSyntenyView unrecognized class for object sent");
									Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in permission to draw right qs drawSyntenyView" +
											" unrecognized class for object sent = "+apsiIT.getAbsoluteProportionQComparableQSSpanItem().getClass().toString()));
								}
							} else {
								if (apsiIT
										.getAbsoluteProportionQComparableQSpanItem() != null) {
									if (apsiIT
											.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
										boolean allowToGetMoreData = false;
										if (shift_progress == 1) {
											allowToGetMoreData = true;
										}
										drawBlockQGeneInsertionOrMissingTarget(
												(AbsoPropQGeneInserItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem(),
												false, false,
												numberOtherMatchIt,
												allowToGetMoreData, apsiIT
														.getIndexInFullArray(),
												(i + j + 1));
									} else if (apsiIT
											.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
										// drawBlockQGenomicRegionInsertionOrMissingTarget(apsiIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(),
										// ((AbsoluteProportionQGenomicRegionInsertionItem)apsiIT.getAbsoluteProportionQComparableQSpanItem()).getQGenomicRegionInsertionNumberGene(),
										// false, false);
										int numberGenes = ((AbsoPropQGenoRegiInserItem) apsiIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getQGenomicRegionInsertionNumberGene();
										if (numberGenes < 0) {
											int sizePb = 
													//1 +
													((AbsoPropQGenoRegiInserItem) apsiIT
															.getAbsoluteProportionQComparableQSpanItem())
															.getqPbStopQGenomicRegionInsertionInElement()
													- ((AbsoPropQGenoRegiInserItem) apsiIT
															.getAbsoluteProportionQComparableQSpanItem())
															.getqPbStartQGenomicRegionInsertionInElement();
											drawBlockQGenomicRegionInsertionOrMissingTarget(
													apsiIT.getAbsoluteProportionQComparableQSpanItem()
															.getqEnumBlockType(),
													convertSizeBpToTextWithAppropriateUnit(sizePb),
													false,
													false,
													true,
													((AbsoPropQGenoRegiInserItem) apsiIT
															.getAbsoluteProportionQComparableQSpanItem())
															.isAnnotationCollision(),
													((AbsoPropQGenoRegiInserItem) apsiIT
															.getAbsoluteProportionQComparableQSpanItem())
															.isAnchoredToStartElement(),
													((AbsoPropQGenoRegiInserItem) apsiIT
															.getAbsoluteProportionQComparableQSpanItem())
															.isAnchoredToStopElement(),
													numberOtherMatchIt);
											// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
											// false, false);
											if (shift_progress == 1) {
//												@SuppressWarnings("unused")
//												ResultLoadingDialog rld = new ResultLoadingDialog(
//														DetailledInfoStack.GeneCountForQGenomicRegionInsertion,
//														apsiIT.getIndexInFullArray(),
//														(associatedGenomePanel
//																.getGenomePanelItem()
//																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + (i
//																+ j + 1)),
//														(i + j + 1),
//														Insyght.APP_CONTROLER
//																.getLIST_GENOME_PANEL()
//																.indexOf(
//																		associatedGenomePanel),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//														CenterSLPAllGenomePanels.START_INDEX,
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStartQGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStopQGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStartSGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStopSGenome(),
//														((AbsoPropQGenoRegiInserItem) apsiIT
//																.getAbsoluteProportionQComparableQSpanItem())
//																.getqOrigamiElementId(),
//														((AbsoPropQGenoRegiInserItem) apsiIT
//																.getAbsoluteProportionQComparableQSpanItem())
//																.getqPbStartQGenomicRegionInsertionInElement(),
//														((AbsoPropQGenoRegiInserItem) apsiIT
//																.getAbsoluteProportionQComparableQSpanItem())
//																.getqPbStopQGenomicRegionInsertionInElement(),
//														apsiIT.getAbsoluteProportionQComparableQSpanItem()
//																.getqEnumBlockType(),
//														false,
//														false,
//														Insyght.APP_CONTROLER
//																.getCurrentRefGenomePanelAndListOrgaResult()
//																.getViewTypeInsyght());
												RLD.getGeneCountForQGenomicRegionInsertion(
														apsiIT.getIndexInFullArray(),
														(associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + (i
																+ j + 1)),
														(i + j + 1),
														Insyght.APP_CONTROLER
																.getLIST_GENOME_PANEL()
																.indexOf(
																		associatedGenomePanel),
														associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
														CenterSLPAllGenomePanels.START_INDEX,
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStartQGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStopQGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStartSGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStopSGenome(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqOrigamiElementId(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStartQGenomicRegionInsertionInElement(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStopQGenomicRegionInsertionInElement(),
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														false,
														false,
														Insyght.APP_CONTROLER
																.getCurrentRefGenomePanelAndListOrgaResult()
																.getViewTypeInsyght());

											}

										} else {
											if (numberGenes == 0) {
												int sizePb = 
														//1 +
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStopQGenomicRegionInsertionInElement()
														- ((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStartQGenomicRegionInsertionInElement();
												// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
												// false, false);
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb),
														false,
														false,
														true,
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnnotationCollision(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStartElement(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStopElement(),
														numberOtherMatchIt);

											} else {
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														numberGenes + " CDS",
														false,
														false,
														false,
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnnotationCollision(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStartElement(),
														((AbsoPropQGenoRegiInserItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.isAnchoredToStopElement(),
														numberOtherMatchIt);
											}
										}
									} else if (apsiIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem) {
										// drawBlockQGenomicRegionInsertionOrMissingTarget(apsiIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(),
										// ((AbsoluteProportionQElementItem)apsiIT.getAbsoluteProportionQComparableQSpanItem()).getqElementNumberGene(),
										// false, false);
										int numberGenes = ((AbsoPropQElemItem) apsiIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqElementNumberGene();
										if (numberGenes < 0) {
											int sizePb = 
													//1 + 
													((AbsoPropQElemItem) apsiIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQsizeOfElementinPb();
											drawBlockQGenomicRegionInsertionOrMissingTarget(
													apsiIT.getAbsoluteProportionQComparableQSpanItem()
															.getqEnumBlockType(),
													convertSizeBpToTextWithAppropriateUnit(sizePb), false,
													false, true, false, false,
													false, numberOtherMatchIt);
											// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
											// false, false);
											if (shift_progress == 1) {
//												@SuppressWarnings("unused")
//												ResultLoadingDialog rld = new ResultLoadingDialog(
//														DetailledInfoStack.GeneCountForQElement,
//														apsiIT.getIndexInFullArray(),
//														(associatedGenomePanel
//																.getGenomePanelItem()
//																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + (i
//																+ j + 1)),
//														(i + j + 1),
//														Insyght.APP_CONTROLER
//																.getLIST_GENOME_PANEL()
//																.indexOf(
//																		associatedGenomePanel),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//														CenterSLPAllGenomePanels.START_INDEX,
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStartQGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStopQGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStartSGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStopSGenome(),
//														((AbsoPropQElemItem) apsiIT
//																.getAbsoluteProportionQComparableQSpanItem())
//																.getqOrigamiElementId(),
//														-1,
//														-1,
//														apsiIT.getAbsoluteProportionQComparableQSpanItem()
//																.getqEnumBlockType(),
//														false,
//														false,
//														Insyght.APP_CONTROLER
//																.getCurrentRefGenomePanelAndListOrgaResult()
//																.getViewTypeInsyght());
												RLD.getGeneCountForQElement(
														apsiIT.getIndexInFullArray(),
														(associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + (i
																+ j + 1)),
														(i + j + 1),
														Insyght.APP_CONTROLER
																.getLIST_GENOME_PANEL()
																.indexOf(
																		associatedGenomePanel),
														associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
														CenterSLPAllGenomePanels.START_INDEX,
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStartQGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStopQGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStartSGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStopSGenome(),
														((AbsoPropQElemItem) apsiIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqOrigamiElementId(),
																apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														false,
														false,
														Insyght.APP_CONTROLER
																.getCurrentRefGenomePanelAndListOrgaResult()
																.getViewTypeInsyght());

											}
										} else {
											if (numberGenes == 0) {
												int sizePb = 
														//1 +
														((AbsoPropQElemItem) apsiIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getQsizeOfElementinPb();
												// drawBlockQNoGeneRegionInsertion(sizePb+" bp",
												// false, false);
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb), false,
														false, true, false,
														false, false,
														numberOtherMatchIt);
											} else {
												drawBlockQGenomicRegionInsertionOrMissingTarget(
														apsiIT.getAbsoluteProportionQComparableQSpanItem()
																.getqEnumBlockType(),
														numberGenes + " CDS",
														false, false, false,
														false, false, false,
														numberOtherMatchIt);
											}
										}
									} else {
										//System.err.println("ERROR in permission to draw right q drawSyntenyView unrecognized class for object sent");
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in permission to draw right q" +
												" drawSyntenyView unrecognized class for object sent = "+apsiIT.getAbsoluteProportionQComparableQSpanItem().getClass().toString()));
									}
								}

								ArrayList<AbsoPropSCompaSSpanItem> alApscssiIT = new ArrayList<AbsoPropSCompaSSpanItem>();
								if (apsiIT
										.getAbsoluteProportionSComparableSSpanItem() != null) {
									alApscssiIT
											.add(apsiIT
													.getAbsoluteProportionSComparableSSpanItem());
								} else {
									boolean dontDrawSRegionTwice = false;
									int idxNextSlice = blockToGetFromArray + j
											+ 1 + 1;
									if (idxNextSlice >= 0
											&& idxNextSlice < associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.size()) {
										HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.get(idxNextSlice)
												.getListHAPI()
												.get(associatedGenomePanel
														.getGenomePanelItem()
														.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																idxNextSlice));
										if (siNextSlice
												.getAbsoluteProportionQComparableQSSpanItem() != null) {
											if (siNextSlice
													.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
												if (((AbsoPropQSSyntItem) siNextSlice
														.getAbsoluteProportionQComparableQSSpanItem())
														.getPreviousGenomicRegionSInsertion() != null) {
													alApscssiIT
															.add(((AbsoPropQSSyntItem) siNextSlice
																	.getAbsoluteProportionQComparableQSSpanItem())
																	.getPreviousGenomicRegionSInsertion());
													if (((AbsoPropQSSyntItem) siNextSlice
															.getAbsoluteProportionQComparableQSSpanItem())
															.getPreviousGenomicRegionSInsertion()
															.isAnchoredToPrevious()) {
														dontDrawSRegionTwice = true;
													}
												}
											}
										} else if (siNextSlice
												.getAbsoluteProportionSComparableSSpanItem() != null) {
											if (siNextSlice
													.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
												if (((AbsoPropSGenoRegiInserItem) siNextSlice
														.getAbsoluteProportionSComparableSSpanItem())
														.getSEnumBlockType()
														.toString()
														.startsWith(
																"S_MISSING_TARGET_Q_")) {
													if (((AbsoPropSGenoRegiInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getsIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
														alApscssiIT
																.add(((AbsoPropSGenoRegiInserItem) siNextSlice
																		.getAbsoluteProportionSComparableSSpanItem())
																		.getsIfMissingTargetPreviousGenomicRegionSInsertion());
														if (((AbsoPropSGenoRegiInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getsIfMissingTargetPreviousGenomicRegionSInsertion()
																.isAnchoredToPrevious()) {
															dontDrawSRegionTwice = true;
														}
													}
												}
											} else if (siNextSlice
													.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
												if (((AbsoPropSGeneInserItem) siNextSlice
														.getAbsoluteProportionSComparableSSpanItem())
														.getSEnumBlockType()
														.toString()
														.startsWith(
																"S_MISSING_TARGET_Q_")) {
													if (((AbsoPropSGeneInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
														alApscssiIT
																.add(((AbsoPropSGeneInserItem) siNextSlice
																		.getAbsoluteProportionSComparableSSpanItem())
																		.getIfMissingTargetPreviousGenomicRegionSInsertion());
														if (((AbsoPropSGeneInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getIfMissingTargetPreviousGenomicRegionSInsertion()
																.isAnchoredToPrevious()) {
															dontDrawSRegionTwice = true;
														}
													}
												}
											}
										}

									}
									if (!dontDrawSRegionTwice) {
										int idxPreviousSlice = blockToGetFromArray
												+ j + 1 - 1;
										if (idxPreviousSlice >= 0
												&& idxPreviousSlice < associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.size()) {
											HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.get(idxPreviousSlice)
													.getListHAPI()
													.get(associatedGenomePanel
															.getGenomePanelItem()
															.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																	idxPreviousSlice));
											if (siNextSlice
													.getAbsoluteProportionQComparableQSSpanItem() != null) {
												if (siNextSlice
														.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
													if (((AbsoPropQSSyntItem) siNextSlice
															.getAbsoluteProportionQComparableQSSpanItem())
															.getNextGenomicRegionSInsertion() != null) {
														alApscssiIT
																.add(((AbsoPropQSSyntItem) siNextSlice
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getNextGenomicRegionSInsertion());
													}
												}
											} else if (siNextSlice
													.getAbsoluteProportionSComparableSSpanItem() != null) {
												if (siNextSlice
														.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
													if (((AbsoPropSGenoRegiInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getSEnumBlockType()
															.toString()
															.startsWith(
																	"S_MISSING_TARGET_Q_")) {
														if (((AbsoPropSGenoRegiInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getsIfMissingTargetNextGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropSGenoRegiInserItem) siNextSlice
																			.getAbsoluteProportionSComparableSSpanItem())
																			.getsIfMissingTargetNextGenomicRegionSInsertion());
														}
													}
												} else if (siNextSlice
														.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
													if (((AbsoPropSGeneInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getSEnumBlockType()
															.toString()
															.startsWith(
																	"S_MISSING_TARGET_Q_")) {
														if (((AbsoPropSGeneInserItem) siNextSlice
																.getAbsoluteProportionSComparableSSpanItem())
																.getIfMissingTargetNextGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropSGeneInserItem) siNextSlice
																			.getAbsoluteProportionSComparableSSpanItem())
																			.getIfMissingTargetNextGenomicRegionSInsertion());
														}
													}
												}
											}

										}
									}

								}

								for (int p = 0; p < alApscssiIT.size(); p++) {
									AbsoPropSCompaSSpanItem apscssiIT = alApscssiIT
											.get(p);
									if (apscssiIT instanceof AbsoPropSGeneInserItem) {
										boolean allowToGetMoreData = false;
										if (shift_progress == 1) {
											allowToGetMoreData = true;
										}
										drawBlockSGeneInsertionOrMissingTarget(
												(AbsoPropSGeneInserItem) apscssiIT,
												false, false,
												numberOtherMatchIt,
												allowToGetMoreData,
												apsiIT.getIndexInFullArray(),
												(i + j + 1));
									} else if (apscssiIT instanceof AbsoPropSGenoRegiInserItem) {
										// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
										// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).getSGenomicRegionInsertionNumberGene(),
										// false, false);
										int numberGenes = ((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getSGenomicRegionInsertionNumberGene();
										if (numberGenes < 0) {
											int sizePb = 
													//1 + 
													((AbsoPropSGenoRegiInserItem) apscssiIT)
															.getsPbStopSGenomicRegionInsertionInElement()
													- ((AbsoPropSGenoRegiInserItem) apscssiIT)
															.getsPbStartSGenomicRegionInsertionInElement();
											drawBlockSGenomicRegionInsertionOrMissingTarget(
													apscssiIT
															.getSEnumBlockType(),
													convertSizeBpToTextWithAppropriateUnit(sizePb),
													false,
													false,
													true,
													((AbsoPropSGenoRegiInserItem) apscssiIT)
															.isAnchoredToPrevious(),
													((AbsoPropSGenoRegiInserItem) apscssiIT)
															.isAnchoredToNext(),
													((AbsoPropSGenoRegiInserItem) apscssiIT)
															.isAnnotationCollision(),
													((AbsoPropSGenoRegiInserItem) apscssiIT)
															.isAnchoredToStartElement(),
													((AbsoPropSGenoRegiInserItem) apscssiIT)
															.isAnchoredToStopElement(),
													numberOtherMatchIt);
											// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
											// false, false,
											// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
											// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
											if (shift_progress == 1) {
//												@SuppressWarnings("unused")
//												ResultLoadingDialog rld = new ResultLoadingDialog(
//														DetailledInfoStack.GeneCountForSGenomicRegionInsertion,
//														apsiIT.getIndexInFullArray(),
//														(associatedGenomePanel
//																.getGenomePanelItem()
//																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + (i
//																+ j + 1)),
//														(i + j + 1),
//														Insyght.APP_CONTROLER
//																.getLIST_GENOME_PANEL()
//																.indexOf(
//																		associatedGenomePanel),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//														CenterSLPAllGenomePanels.START_INDEX,
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStartQGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStopQGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStartSGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStopSGenome(),
//														((AbsoPropSGenoRegiInserItem) apscssiIT)
//																.getsOrigamiElementId(),
//														((AbsoPropSGenoRegiInserItem) apscssiIT)
//																.getsPbStartSGenomicRegionInsertionInElement(),
//														((AbsoPropSGenoRegiInserItem) apscssiIT)
//																.getsPbStopSGenomicRegionInsertionInElement(),
//														apscssiIT
//																.getSEnumBlockType(),
//														false,
//														false,
//														Insyght.APP_CONTROLER
//																.getCurrentRefGenomePanelAndListOrgaResult()
//																.getViewTypeInsyght(),
//														((AbsoPropSGenoRegiInserItem) apscssiIT)
//																.isPreviousSOfNextSlice(),
//														((AbsoPropSGenoRegiInserItem) apscssiIT)
//																.isNextSOfPreviousSlice());
												RLD.getGeneCountForSGenomicRegionInsertion(
														apsiIT.getIndexInFullArray(),
														(associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + (i
																+ j + 1)),
														(i + j + 1),
														Insyght.APP_CONTROLER
																.getLIST_GENOME_PANEL()
																.indexOf(
																		associatedGenomePanel),
														associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
														CenterSLPAllGenomePanels.START_INDEX,
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStartQGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStopQGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStartSGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStopSGenome(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsOrigamiElementId(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStartSGenomicRegionInsertionInElement(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStopSGenomicRegionInsertionInElement(),
														apscssiIT
																.getSEnumBlockType(),
														false,
														false,
														Insyght.APP_CONTROLER
																.getCurrentRefGenomePanelAndListOrgaResult()
																.getViewTypeInsyght(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isPreviousSOfNextSlice(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isNextSOfPreviousSlice()
														);

											}
										} else {
											if (numberGenes == 0) {
												int sizePb = 
														//1 +
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStopSGenomicRegionInsertionInElement()
														- ((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStartSGenomicRegionInsertionInElement();
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb),
														false,
														false,
														true,
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToPrevious(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToNext(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnnotationCollision(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStartElement(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStopElement(),
														numberOtherMatchIt);
												// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
												// false, false,
												// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
												// ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
											} else {
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														numberGenes + " CDS",
														false,
														false,
														false,
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToPrevious(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToNext(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnnotationCollision(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStartElement(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.isAnchoredToStopElement(),
														numberOtherMatchIt);
											}
										}
									} else if (apscssiIT instanceof AbsoPropSElemItem) {
										// drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(),
										// ((AbsoluteProportionSElementItem)apscssiIT).getsElementNumberGene(),
										// false, false);
										int numberGenes = ((AbsoPropSElemItem) apscssiIT)
												.getsElementNumberGene();
										if (numberGenes < 0) {
											int sizePb = 
													//1 + 
													((AbsoPropSElemItem) apscssiIT)
													.getsSizeOfElementinPb();
											drawBlockSGenomicRegionInsertionOrMissingTarget(
													apscssiIT
															.getSEnumBlockType(),
													convertSizeBpToTextWithAppropriateUnit(sizePb), false,
													false, true, true, true,
													false, false, false,
													numberOtherMatchIt);
											// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
											// false, false);
											if (shift_progress == 1) {
//												@SuppressWarnings("unused")
//												ResultLoadingDialog rld = new ResultLoadingDialog(
//														DetailledInfoStack.GeneCountForSElement,
//														apsiIT.getIndexInFullArray(),
//														(associatedGenomePanel
//																.getGenomePanelItem()
//																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + (i
//																+ j + 1)),
//														(i + j + 1),
//														Insyght.APP_CONTROLER
//																.getLIST_GENOME_PANEL()
//																.indexOf(
//																		associatedGenomePanel),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//														CenterSLPAllGenomePanels.START_INDEX,
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStartQGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStopQGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStartSGenome(),
//														associatedGenomePanel
//																.getGenomePanelItem()
//																.getPercentSyntenyShowStopSGenome(),
//														((AbsoPropSElemItem) apscssiIT)
//																.getsOrigamiElementId(),
//														-1,
//														-1,
//														apscssiIT
//																.getSEnumBlockType(),
//														false,
//														false,
//														Insyght.APP_CONTROLER
//																.getCurrentRefGenomePanelAndListOrgaResult()
//																.getViewTypeInsyght(),
//														false, false);
												RLD.getGeneCountForSElement(
														apsiIT.getIndexInFullArray(),
														(associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + (i
																+ j + 1)),
														(i + j + 1),
														Insyght.APP_CONTROLER
																.getLIST_GENOME_PANEL()
																.indexOf(
																		associatedGenomePanel),
														associatedGenomePanel
																.getGenomePanelItem()
																.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
														CenterSLPAllGenomePanels.START_INDEX,
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStartQGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStopQGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStartSGenome(),
														associatedGenomePanel
																.getGenomePanelItem()
																.getPercentSyntenyShowStopSGenome(),
														((AbsoPropSElemItem) apscssiIT)
																.getsOrigamiElementId(),
																apscssiIT
																.getSEnumBlockType(),
														false,
														false,
														Insyght.APP_CONTROLER
																.getCurrentRefGenomePanelAndListOrgaResult()
																.getViewTypeInsyght());
												
												
											}
										} else {
											if (numberGenes == 0) {
												int sizePb = 
														//1 + 
														((AbsoPropSElemItem) apscssiIT)
														.getsSizeOfElementinPb();
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														convertSizeBpToTextWithAppropriateUnit(sizePb), false,
														false, true, true,
														true, false, false,
														false,
														numberOtherMatchIt);
												// drawBlockSNoGeneRegionInsertion(sizePb+" bp",
												// false, false);
											} else {
												drawBlockSGenomicRegionInsertionOrMissingTarget(
														apscssiIT
																.getSEnumBlockType(),
														numberGenes + " CDS",
														false, false, false,
														false, false, false,
														false, false,
														numberOtherMatchIt);
											}
										}
									} else {
										//System.err.println("ERROR in permission to draw right s drawSyntenyView unrecognized class for object sent");
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in permission to draw right s " +
												"drawSyntenyView unrecognized class for object sent = "+apscssiIT.getClass().toString()));
									}

								}
							}
							// drawBlock(apsiIT.getSyntenyBlockType(),
							// apsiIT.getSyntenyNumberGene(),
							// apsiIT.getQNumberGene(), apsiIT.getSNumberGene(),
							// false, false);
							// System.out.println("get and draw block "+(blockToGetFromArray+j+1));
						}// else{
							// System.out.println("wanted to get and draw block "+(blockToGetFromArray+j+1)+" but out of range in array");
						// }
					}
				}

			}
			restoreToOriginCoordinateSpace();
		}

		// long spentTime = System.currentTimeMillis() - milli;
		// System.out.println("Finished draw upper part in "
		// + spentTime + " milliseconds"); //test

		// draw bottom part ?
		if (drawBottomPart) {
			if (associatedGenomePanel.getGenomePanelItem().getSizeState()
					.compareTo(GenomePanelItem.GpiSizeState.MAX) == 0
					|| forceBottomDrawing) {
				// System.out.println("drawing bottom part");

				restoreToOriginCoordinateSpace();
				// System.err.println("translate");
				contextBgk.translate(0, TOTAL_CANVAS_HEIGHT_UPPER_PART);

				if (bottomRedrawOnTop) {

					contextBgk.setFillStyle(redrawColorBlack);
					clearMarkerBottom(false, true, true);
					drawElementMarkerScaffold(true, false);
					drawElementMarkerScaffold(false, false);
					drawSyntenyMarkerScaffoldAllDisplayedSlices(
							bottomRedrawOnTop, false, false);
					highlightPropRepreForAllDisplayedSlices(true, false);

				} else {
					// System.out.println("clear all bottom");
					// clear whole canvas
					// contextBgk.clearRect(0, 0, TOTAL_CANVAS_WIDTH,
					// TOTAL_CANVAS_HEIGHT);
					// System.err.println("clearLowerPartCanvas true");
					clearLowerPartCanvas(false);
					// if(associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()
					// < LIMIT_PROPORTIONATE_DRAWING_TOO_MUCH){
					for (int i = 0; i < associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.size(); i++) {
						for (int l = 0; l < associatedGenomePanel
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.get(i).getListHAPI().size(); l++) {
							// HolderAbsoluteProportionItem siIT =
							// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(i).get(associatedGenomePanel.getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(i));
							HolderAbsoluteProportionItem siIT = associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(i).getListHAPI().get(l);
							if (siIT != null) {
								if (siIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem
											|| siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {

										double qPercentStartIT = claculateRelativePercentage(
												Insyght.APP_CONTROLER
														.getCurrentRefGenomePanelAndListOrgaResult()
														.getReferenceGenomePanelItem()
														.getListAbsoluteProportionElementItemQ()
														.get(0)
														.getQsizeOfOragnismInPb(),
												siIT.getAbsoluteProportionQComparableQSSpanItem()
														.getqPercentStart(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartQGenome());
										double qPercentStopIT = claculateRelativePercentage(
												Insyght.APP_CONTROLER
														.getCurrentRefGenomePanelAndListOrgaResult()
														.getReferenceGenomePanelItem()
														.getListAbsoluteProportionElementItemQ()
														.get(0)
														.getQsizeOfOragnismInPb(),
												siIT.getAbsoluteProportionQComparableQSSpanItem()
														.getqPercentStop(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopQGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartQGenome());
										double sPercentStartIT = claculateRelativePercentage(
												associatedGenomePanel
														.getGenomePanelItem()
														.getListAbsoluteProportionElementItemS()
														.get(0)
														.getsSizeOfOragnismInPb(),
												siIT.getAbsoluteProportionQComparableQSSpanItem()
														.getsPercentStart(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome());
										double sPercentStopIT = claculateRelativePercentage(
												associatedGenomePanel
														.getGenomePanelItem()
														.getListAbsoluteProportionElementItemS()
														.get(0)
														.getsSizeOfOragnismInPb(),
												siIT.getAbsoluteProportionQComparableQSSpanItem()
														.getsPercentStop(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome());

										if (qPercentStartIT >= 0
												&& qPercentStartIT <= 1
												&& qPercentStopIT >= 0
												&& qPercentStopIT <= 1
												&& sPercentStartIT >= 0
												&& sPercentStartIT <= 1
												&& sPercentStopIT >= 0
												&& sPercentStopIT <= 1) {
											drawSyntenyRepersentationProportionateQS(
													siIT.getAbsoluteProportionQComparableQSSpanItem(),
													qPercentStartIT,
													qPercentStopIT,
													sPercentStartIT,
													sPercentStopIT,
													bottomRedrawOnTop, false,
													false);
										}

									} else {
										//System.err.println("ERROR in drawSyntenyRepersentationProportionateQS qs drawViewToCanvas unrecognized class for object sent");
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in drawSyntenyRepersentationProportionateQS " +
												"qs drawViewToCanvas unrecognized class for object sent = "+siIT.getAbsoluteProportionQComparableQSSpanItem().getClass().toString()));
									}

								} else {
									if (siIT.getAbsoluteProportionQComparableQSpanItem() != null) {

										if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem
												|| siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem
												|| siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {

											double qPercentStartIT = claculateRelativePercentage(
													Insyght.APP_CONTROLER
															.getCurrentRefGenomePanelAndListOrgaResult()
															.getReferenceGenomePanelItem()
															.getListAbsoluteProportionElementItemQ()
															.get(0)
															.getQsizeOfOragnismInPb(),
													siIT.getAbsoluteProportionQComparableQSpanItem()
															.getqPercentStart(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopQGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartQGenome());
											double qPercentStopIT = claculateRelativePercentage(
													Insyght.APP_CONTROLER
															.getCurrentRefGenomePanelAndListOrgaResult()
															.getReferenceGenomePanelItem()
															.getListAbsoluteProportionElementItemQ()
															.get(0)
															.getQsizeOfOragnismInPb(),
													siIT.getAbsoluteProportionQComparableQSpanItem()
															.getqPercentStop(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopQGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartQGenome());

											if (qPercentStartIT >= 0
													&& qPercentStartIT <= 1
													&& qPercentStopIT >= 0
													&& qPercentStopIT <= 1) {
												drawGenomicRegionRepersentationProportionateQ(
														siIT.getAbsoluteProportionQComparableQSpanItem(),
														qPercentStartIT,
														qPercentStopIT,
														bottomRedrawOnTop,
														false, false);
											}

										} else {
											//System.err.println("ERROR in drawSyntenyRepersentationProportionateQ q drawViewToCanvas unrecognized class for object sent");
											Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in drawSyntenyRepersentationProportionateQ q " +
													"drawViewToCanvas unrecognized class for object sent = "+siIT.getAbsoluteProportionQComparableQSpanItem().getClass().toString()));
										}
									}

									ArrayList<AbsoPropSCompaSSpanItem> alApscssiIT = new ArrayList<AbsoPropSCompaSSpanItem>();
									if (siIT.getAbsoluteProportionSComparableSSpanItem() != null) {
										alApscssiIT
												.add(siIT
														.getAbsoluteProportionSComparableSSpanItem());
									} else {

										int idxNextSlice = i + 1;
										if (idxNextSlice >= 0
												&& idxNextSlice < associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.size()) {
											SuperHoldAbsoPropItem shIT = associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.get(idxNextSlice);
											for (int q = 0; q < shIT
													.getListHAPI().size(); q++) {
												HolderAbsoluteProportionItem siNextSliceIT = shIT
														.getListHAPI().get(q);
												if (siNextSliceIT
														.getAbsoluteProportionQComparableQSSpanItem() != null) {
													if (siNextSliceIT
															.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
														if (((AbsoPropQSSyntItem) siNextSliceIT
																.getAbsoluteProportionQComparableQSSpanItem())
																.getPreviousGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropQSSyntItem) siNextSliceIT
																			.getAbsoluteProportionQComparableQSSpanItem())
																			.getPreviousGenomicRegionSInsertion());
														}
													}
												} else if (siNextSliceIT
														.getAbsoluteProportionSComparableSSpanItem() != null) {
													if (siNextSliceIT
															.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
														if (((AbsoPropSGenoRegiInserItem) siNextSliceIT
																.getAbsoluteProportionSComparableSSpanItem())
																.getSEnumBlockType()
																.toString()
																.startsWith(
																		"S_MISSING_TARGET_Q_")) {
															if (((AbsoPropSGenoRegiInserItem) siNextSliceIT
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getsIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
																alApscssiIT
																		.add(((AbsoPropSGenoRegiInserItem) siNextSliceIT
																				.getAbsoluteProportionSComparableSSpanItem())
																				.getsIfMissingTargetPreviousGenomicRegionSInsertion());
															}
														}
													} else if (siNextSliceIT
															.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
														if (((AbsoPropSGeneInserItem) siNextSliceIT
																.getAbsoluteProportionSComparableSSpanItem())
																.getSEnumBlockType()
																.toString()
																.startsWith(
																		"S_MISSING_TARGET_Q_")) {
															if (((AbsoPropSGeneInserItem) siNextSliceIT
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getIfMissingTargetPreviousGenomicRegionSInsertion() != null) {
																alApscssiIT
																		.add(((AbsoPropSGeneInserItem) siNextSliceIT
																				.getAbsoluteProportionSComparableSSpanItem())
																				.getIfMissingTargetPreviousGenomicRegionSInsertion());
															}
														}
													}
												}

											}
										}

										int idxPreviousSlice = i - 1;
										if (idxPreviousSlice >= 0
												&& idxPreviousSlice < associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.size()) {
											SuperHoldAbsoPropItem shIT = associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.get(idxPreviousSlice);
											for (int q = 0; q < shIT
													.getListHAPI().size(); q++) {
												HolderAbsoluteProportionItem siPreviousSliceIT = associatedGenomePanel
														.getGenomePanelItem()
														.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
														.get(idxPreviousSlice)
														.getListHAPI()
														.get(associatedGenomePanel
																.getGenomePanelItem()
																.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																		idxPreviousSlice));
												if (siPreviousSliceIT
														.getAbsoluteProportionQComparableQSSpanItem() != null) {
													if (siPreviousSliceIT
															.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
														if (((AbsoPropQSSyntItem) siPreviousSliceIT
																.getAbsoluteProportionQComparableQSSpanItem())
																.getNextGenomicRegionSInsertion() != null) {
															alApscssiIT
																	.add(((AbsoPropQSSyntItem) siPreviousSliceIT
																			.getAbsoluteProportionQComparableQSSpanItem())
																			.getNextGenomicRegionSInsertion());
														}
													}
												} else if (siPreviousSliceIT
														.getAbsoluteProportionSComparableSSpanItem() != null) {
													if (siPreviousSliceIT
															.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
														if (((AbsoPropSGenoRegiInserItem) siPreviousSliceIT
																.getAbsoluteProportionSComparableSSpanItem())
																.getSEnumBlockType()
																.toString()
																.startsWith(
																		"S_MISSING_TARGET_Q_")) {
															if (((AbsoPropSGenoRegiInserItem) siPreviousSliceIT
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getsIfMissingTargetNextGenomicRegionSInsertion() != null) {
																alApscssiIT
																		.add(((AbsoPropSGenoRegiInserItem) siPreviousSliceIT
																				.getAbsoluteProportionSComparableSSpanItem())
																				.getsIfMissingTargetNextGenomicRegionSInsertion());
															}
														}
													} else if (siPreviousSliceIT
															.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
														if (((AbsoPropSGeneInserItem) siPreviousSliceIT
																.getAbsoluteProportionSComparableSSpanItem())
																.getSEnumBlockType()
																.toString()
																.startsWith(
																		"S_MISSING_TARGET_Q_")) {
															if (((AbsoPropSGeneInserItem) siPreviousSliceIT
																	.getAbsoluteProportionSComparableSSpanItem())
																	.getIfMissingTargetNextGenomicRegionSInsertion() != null) {
																alApscssiIT
																		.add(((AbsoPropSGeneInserItem) siPreviousSliceIT
																				.getAbsoluteProportionSComparableSSpanItem())
																				.getIfMissingTargetNextGenomicRegionSInsertion());
															}
														}
													}
												}

											}
										}

									}

									for (int p = 0; p < alApscssiIT.size(); p++) {
										AbsoPropSCompaSSpanItem apscssiIT = alApscssiIT
												.get(p);
										if (apscssiIT instanceof AbsoPropSGenoRegiInserItem
												|| apscssiIT instanceof AbsoPropSElemItem
												|| apscssiIT instanceof AbsoPropSGeneInserItem) {

											double sPercentStart = claculateRelativePercentage(
													associatedGenomePanel
															.getGenomePanelItem()
															.getListAbsoluteProportionElementItemS()
															.get(0)
															.getsSizeOfOragnismInPb(),
													apscssiIT
															.getsPercentStart(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopSGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartSGenome());
											double sPercentStop = claculateRelativePercentage(
													associatedGenomePanel
															.getGenomePanelItem()
															.getListAbsoluteProportionElementItemS()
															.get(0)
															.getsSizeOfOragnismInPb(),
													apscssiIT.getsPercentStop(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopSGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartSGenome());
											if (sPercentStart >= 0
													&& sPercentStart <= 1
													&& sPercentStop >= 0
													&& sPercentStop <= 1) {
												drawGenomicRegionRepersentationProportionateS(
														apscssiIT,
														sPercentStart,
														sPercentStop,
														bottomRedrawOnTop,
														false, false);
											}

										} else {
											System.err.println("ERROR in drawSyntenyRepersentationProportionateS s drawViewToCanvas unrecognized class for object sent");
											Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in drawSyntenyRepersentationProportionateS s" +
													" drawViewToCanvas unrecognized class for object sent = "+apscssiIT.getClass().toString()));
										}
									}

								}
							}

						}

					}
					// }else{
					// drawTooMuchOnRepersentationProportionate();
					// }

					drawElementMarkerScaffold(true, false);
					drawElementMarkerScaffold(false, false);
					drawSyntenyMarkerScaffoldAllDisplayedSlices(
							bottomRedrawOnTop, false, false);
					highlightPropRepreForAllDisplayedSlices(true, false);
					drawScaffoldGenome(true, 0, 1, bottomRedrawOnTop, false,
							false);
					drawScaffoldGenome(false, 0, 1, bottomRedrawOnTop, false,
							false);

				}

				restoreToOriginCoordinateSpace();
			}
		}

	}

	protected boolean buildMenuContextuelInPopUp() {

		//else previous statement give rise to an of bound error when no alignments are found to be shown = associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemS() empty
		if (associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemS().isEmpty()){
			return false;
		}
		
		MainTabPanelView.CANVAS_MENU_POPUP.disableAllLeafs();

		// menu always available, context independant
		MainTabPanelView.CANVAS_MENU_POPUP.addFindGeneInQElement();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsNextGeneHomology();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsNextSyntenyRegion();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsNextSyntenyRegion5Genes();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsNextSyntenyRegion20Genes();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsNextReferenceGeneGenomicRegionInsertion();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsPreviousGeneHomology();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsPreviousSyntenyRegion();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsPreviousSyntenyRegion5Genes();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsPreviousSyntenyRegion20Genes();
		MainTabPanelView.CANVAS_MENU_POPUP.addNavSymbolsPreviousReferenceGeneGenomicRegionInsertion();
		MainTabPanelView.CANVAS_MENU_POPUP.addExportGraphics();
		MainTabPanelView.CANVAS_MENU_POPUP.addQuickNavigationSetComparedOrganismAsReference();

		// zoom in q
		MainTabPanelView.CANVAS_MENU_POPUP.addReferenceGenomeZoomIn(
				(canvasBgk.getAbsoluteLeft()+ SCAFFOLD_X_START_GENOME_TEXT),
				(canvasBgk.getAbsoluteTop()+ TOTAL_CANVAS_HEIGHT_UPPER_PART)
				);
		
		
		// menu always available but context dependant
		MainTabPanelView.CANVAS_MENU_POPUP.enableComparedGenomeZoomIn(true);
		MainTabPanelView.CANVAS_MENU_POPUP.comparedGenomeZoomIn
				.setScheduledCommand(new ScheduledCommand() {
					@Override
					public void execute() {
						final PopupPanel pop = new PopupPanel(true);
						VerticalPanel vpRoot = new VerticalPanel();
						ScrollPanel scRoot = new ScrollPanel(vpRoot);
						for (int i = 0; i < associatedGenomePanel
								.getGenomePanelItem()
								.getListAbsoluteProportionElementItemS().size(); i++) {
							AbsoPropSElemItem sapei = associatedGenomePanel
									.getGenomePanelItem()
									.getListAbsoluteProportionElementItemS()
									.get(i);
							MainTabPanelView.CANVAS_MENU_POPUP.addToZoomIn(
									sapei.getsAccnum(),
									sapei.getsPercentStart(),
									sapei.getsPercentStop(), false, vpRoot,
									scRoot, pop);
						}
						pop.setWidget(scRoot);
						// pop.showRelativeTo(Insyght.CANVAS_MENU_POPUP);
						pop.setPopupPositionAndShow(new PositionCallback() {
							@Override
							public void setPosition(int offsetWidth,
									int offsetHeight) {
								int left = canvasBgk.getAbsoluteLeft()
										+ SCAFFOLD_X_START_GENOME_TEXT;
								int top = canvasBgk.getAbsoluteTop()
										+ TOTAL_CANVAS_HEIGHT_UPPER_PART;
								pop.setPopupPosition(left, top);
							}
						});
						MainTabPanelView.CANVAS_MENU_POPUP.hide();
					}
				});

		
		
		if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
				.getViewTypeInsyght()
				.compareTo(EnumResultViewTypes.genomic_organization) == 0) {
			// Synteny view ->
			// addTransfertAllGenesFromRefOrganismToHomologyBrowsingView
			
			MainTabPanelView.CANVAS_MENU_POPUP.enableNavSymbolsNext(true);
			MainTabPanelView.CANVAS_MENU_POPUP.enableNavSymbolsPrevious(true);
			
			// fing first q or qs slice
			int firstElementId = -1;
			int firstStartInFirstElementId = -1;
			for (int i = 0; i < associatedGenomePanel
					.getGenomePanelItem()
					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
					.size(); i++) {
				HolderAbsoluteProportionItem siNextIT = associatedGenomePanel
						.getGenomePanelItem()
						.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
						.get(i).getListHAPI().get(0);
				if (siNextIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
					if (siNextIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
						firstElementId = ((AbsoPropQSGeneHomoItem) siNextIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getQsQOrigamiElementId();
						firstStartInFirstElementId = ((AbsoPropQSGeneHomoItem) siNextIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getQsQPbStartGeneInElement();
						break;
					} else if (siNextIT
							.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
						firstElementId = ((AbsoPropQSSyntItem) siNextIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getQsQOrigamiElementId();
						firstStartInFirstElementId = ((AbsoPropQSSyntItem) siNextIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getPbQStartSyntenyInElement();
						break;
					}
				} else if (siNextIT.getAbsoluteProportionQComparableQSpanItem() != null) {
					// first q or qs
					if (siNextIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
						firstElementId = ((AbsoPropQGeneInserItem) siNextIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqOrigamiElementId();
						firstStartInFirstElementId = ((AbsoPropQGeneInserItem) siNextIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqPbStartGeneInElement();
						break;
					} else if (siNextIT
							.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
						firstElementId = ((AbsoPropQGenoRegiInserItem) siNextIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqOrigamiElementId();
						firstStartInFirstElementId = ((AbsoPropQGenoRegiInserItem) siNextIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqPbStartQGenomicRegionInsertionInElement();
						break;
					}
				}
			}
			// fing last q or qs slice
			int lastElementId = -1;
			int lastStopInLastElementId = -1;
			for (int i = associatedGenomePanel
					.getGenomePanelItem()
					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
					.size() - 1; i >= 0; i--) {
				HolderAbsoluteProportionItem siNextIT = associatedGenomePanel
						.getGenomePanelItem()
						.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
						.get(i).getListHAPI().get(0);
				if (siNextIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
					if (siNextIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
						lastElementId = ((AbsoPropQSGeneHomoItem) siNextIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getQsQOrigamiElementId();
						lastStopInLastElementId = ((AbsoPropQSGeneHomoItem) siNextIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getQsQPbStopGeneInElement();
						break;
					} else if (siNextIT
							.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
						lastElementId = ((AbsoPropQSSyntItem) siNextIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getQsQOrigamiElementId();
						lastStopInLastElementId = ((AbsoPropQSSyntItem) siNextIT
								.getAbsoluteProportionQComparableQSSpanItem())
								.getPbQStopSyntenyInElement();
						break;
					}
				} else if (siNextIT.getAbsoluteProportionQComparableQSpanItem() != null) {
					// first q or qs
					if (siNextIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
						lastElementId = ((AbsoPropQGeneInserItem) siNextIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqOrigamiElementId();
						lastStopInLastElementId = ((AbsoPropQGeneInserItem) siNextIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqPbStopGeneInElement();
						break;
					} else if (siNextIT
							.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
						lastElementId = ((AbsoPropQGenoRegiInserItem) siNextIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqOrigamiElementId();
						lastStopInLastElementId = ((AbsoPropQGenoRegiInserItem) siNextIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqPbStopQGenomicRegionInsertionInElement();
						break;
					}
				}
			}

			if (firstElementId >= 0 && firstStartInFirstElementId >= 0
					&& lastElementId >= 0 && lastStopInLastElementId >= 0) {
				ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent = new ArrayList<Integer>();
				boolean firstElementFound = false;
				boolean lastElementFound = false;
				for (int i = 0; i < associatedGenomePanel.getGenomePanelItem()
						.getListAbsoluteProportionElementItemQ().size(); i++) {
					AbsoPropQElemItem qapei = associatedGenomePanel
							.getGenomePanelItem()
							.getListAbsoluteProportionElementItemQ().get(i);
					if (qapei.getqOrigamiElementId() == firstElementId) {
						firstElementFound = true;
					}
					if (qapei.getqOrigamiElementId() == lastElementId) {
						lastElementFound = true;
					}
					if (firstElementFound
							&& !lastElementFound
							&& listElementIdsThenStartPbthenStopPBLoopedSent
									.isEmpty()) {
						// add whole firstElementId but not finished
						listElementIdsThenStartPbthenStopPBLoopedSent
								.add(firstElementId);
						listElementIdsThenStartPbthenStopPBLoopedSent
								.add(firstStartInFirstElementId);
						listElementIdsThenStartPbthenStopPBLoopedSent.add(qapei
								.getQsizeOfElementinPb());
					} else if (firstElementFound
							&& !lastElementFound
							&& !listElementIdsThenStartPbthenStopPBLoopedSent
									.isEmpty()) {
						// add whole curr element in between but not finished
						listElementIdsThenStartPbthenStopPBLoopedSent.add(qapei
								.getqOrigamiElementId());
						listElementIdsThenStartPbthenStopPBLoopedSent.add(0);
						listElementIdsThenStartPbthenStopPBLoopedSent.add(qapei
								.getQsizeOfElementinPb());
					} else if (firstElementFound
							&& lastElementFound
							&& listElementIdsThenStartPbthenStopPBLoopedSent
									.isEmpty()) {
						// add whole firstElementId and finished
						listElementIdsThenStartPbthenStopPBLoopedSent
								.add(firstElementId);
						listElementIdsThenStartPbthenStopPBLoopedSent
								.add(firstStartInFirstElementId);
						listElementIdsThenStartPbthenStopPBLoopedSent
								.add(lastStopInLastElementId);
						break;
					} else if (firstElementFound
							&& lastElementFound
							&& !listElementIdsThenStartPbthenStopPBLoopedSent
									.isEmpty()) {
						// add whole curr element in between and finished
						listElementIdsThenStartPbthenStopPBLoopedSent
								.add(lastElementId);
						listElementIdsThenStartPbthenStopPBLoopedSent.add(0);
						listElementIdsThenStartPbthenStopPBLoopedSent
								.add(lastStopInLastElementId);
						break;
					}
				}
				MainTabPanelView.CANVAS_MENU_POPUP
						.addTransfertDisplayedRegionToHomologyBrowsingView(
								listElementIdsThenStartPbthenStopPBLoopedSent
								//,Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(associatedGenomePanel)
								//,true
								);
				MainTabPanelView.CANVAS_MENU_POPUP
						.addTransfertDisplayedRegionToCompareHomologsAnnotation(listElementIdsThenStartPbthenStopPBLoopedSent);
			} else {
				// System.out.println("not all of those : firstElementId="+firstElementId+";firstStartInFirstElementId="+
				// firstStartInFirstElementId+";lastElementId="+lastElementId+";lastStopInLastElementId="+lastStopInLastElementId);
			}

		} else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult()
			.getViewTypeInsyght()
			.compareTo(EnumResultViewTypes.homolog_table) == 0){
			// orthologs table view
			MainTabPanelView.CANVAS_MENU_POPUP.enableNavSymbolsNext(false);
			MainTabPanelView.CANVAS_MENU_POPUP.enableNavSymbolsPrevious(false);
			MainTabPanelView.CANVAS_MENU_POPUP
					.enableTransfertDisplayedRegionToHomologBrowsingTable(false);
			MainTabPanelView.CANVAS_MENU_POPUP
					.addTransfertGeneSetToAnnotationsComparator(Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getListReferenceGeneSetForHomologsTable(), null);
		}

		// zoom out q
		// Insyght.CANVAS_MENU_POPUP.addZoomOutxX(true,
		// associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStartQGenome(),
		// associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStopQGenome(),
		// 2
		// );
		MainTabPanelView.CANVAS_MENU_POPUP.addReferenceGenomeZoomOut2X(
				// true,
				associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStartQGenome(),
				associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStopQGenome()
		// ,2
				);

		MainTabPanelView.CANVAS_MENU_POPUP.addReferenceGenomeZoomOut5X(
				// true,
				associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStartQGenome(),
				associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStopQGenome()
		// ,5
				);

		MainTabPanelView.CANVAS_MENU_POPUP.addReferenceGenomeZoomOutMax();

		// Insyght.CANVAS_MENU_POPUP.setZoomOutAvailable(true);
		// Insyght.CANVAS_MENU_POPUP.setCenterDisplayAvailable(false);
		// zoom out s
		MainTabPanelView.CANVAS_MENU_POPUP.addComparedGenomeZoomOut2X(
				// false,
				associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStartSGenome(),
				associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStopSGenome()
		// ,2
				);

		MainTabPanelView.CANVAS_MENU_POPUP.addComparedGenomeZoomOut5X(
				// false,
				associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStartSGenome(),
				associatedGenomePanel.getGenomePanelItem()
						.getPercentSyntenyShowStopSGenome()
		// ,5
				);

		MainTabPanelView.CANVAS_MENU_POPUP.addComparedGenomeZoomOutMax();

		// System.out.println("checkIfClickWantDisplayElement");
		if (mouseY >= TOTAL_CANVAS_HEIGHT_UPPER_PART) {
			// clicked bootom part

			// disable Selected symbol
			// MainTabPanelView.CANVAS_MENU_POPUP.enableSelectedSymbol(false);

			if (mouseY < TOTAL_CANVAS_HEIGHT_UPPER_PART
					+ Y_AXIS_TO_DRAW_AT_BOTTOM - (UNIT_WIDTH * 0.8)) {
				// clicked Q
				// System.out.println("checkIfClickWantDisplayElement Q");

				// disable Compared genome Center display at click position
				// Insyght.CANVAS_MENU_POPUP.enableComparedGenomeCenterDisplay(false);

				if (mouseX >= SCAFFOLD_X_START_POINT
						&& mouseX <= SCAFFOLD_X_STOP_POINT) {
					// clicked scaffold
					// Insyght.CANVAS_MENU_POPUP.clearVpRoot();
					MainTabPanelView.CANVAS_MENU_POPUP
							.addCenterDisplayAtClickPosition(
							// true,
							claculateAbsolutePercentage(
									Insyght.APP_CONTROLER
											.getCurrentRefGenomePanelAndListOrgaResult()
											.getReferenceGenomePanelItem()
											.getListAbsoluteProportionElementItemQ()
											.get(0).getQsizeOfOragnismInPb(),
									(((double) mouseX - (double) SCAFFOLD_X_START_POINT) / (double) TOTAL_LENGHT_SCAFFOLD),
									associatedGenomePanel.getGenomePanelItem()
											.getPercentSyntenyShowStopQGenome(),
									associatedGenomePanel
											.getGenomePanelItem()
											.getPercentSyntenyShowStartQGenome()));

					
				} else {
					// clicked genome name
					// System.out.println("checkIfClickWantDisplayElement Q genome name");
					// return true;
				}

			} else {
				// clicked S
				// System.out.println("checkIfClickWantDisplayElement S");

				// disable Reference genome Center display at click position
				//MainTabPanelView.CANVAS_MENU_POPUP.enableQuickNavigationCenterDisplayAtClickPosition(false);

				if (mouseX >= SCAFFOLD_X_START_POINT
						&& mouseX <= SCAFFOLD_X_STOP_POINT) {
					// clicked scaffold
					// Insyght.CANVAS_MENU_POPUP.clearVpRoot();
					// Insyght.CANVAS_MENU_POPUP.addComparedGenomeCenterDisplayHere(
					// //false,
					// claculateAbsolutePercentage(associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(),
					// (((double)mouseX-(double)SCAFFOLD_X_START_POINT)/(double)TOTAL_LENGHT_SCAFFOLD),
					// associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStopSGenome(),
					// associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStartSGenome())
					// );
					// //zoom in specific element
					// for(int i = 0 ; i <
					// associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemS().size()
					// ; i++){
					// AbsoluteProportionSElementItem sapei =
					// associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemS().get(i);
					// double xPointStopPercent =
					// claculateRelativePercentage(associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(),
					// sapei.getsPercentStop(),
					// associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStopSGenome(),
					// associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStartSGenome());
					// double xPointStop =
					// SCAFFOLD_X_START_POINT+(TOTAL_LENGHT_SCAFFOLD*xPointStopPercent)+0.5;
					// //double xPointStartPercent =
					// //
					// claculateRelativePercentage(associatedGenomePanel.getGenomePanelItem().getSize(),
					// sapei.getPercentStart(),
					// associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStopSGenome(),
					// associatedGenomePanel.getGenomePanelItem().getPercentSyntenyShowStartSGenome());
					// if(mouseX < xPointStop){
					// //System.out.println(sapei.getAccnum()+" ; "+xPointStartPercent+" ; "+xPointStopPercent);
					// Insyght.CANVAS_MENU_POPUP.addToZoomIn(sapei.getsAccnum(),
					// sapei.getsPercentStart(), sapei.getsPercentStop(),
					// false);
					// return true;
					// }
					// }

				} else {
					// clicked genome name
					// Insyght.CANVAS_MENU_POPUP.clearVpRoot();

					// return true;
				}
			}

		} else {
			// clicked upper part

			// disable Compared genome Center display at click position
			// Insyght.CANVAS_MENU_POPUP.enableComparedGenomeCenterDisplay(false);
			// disable Reference genome Center display at click position
			//MainTabPanelView.CANVAS_MENU_POPUP.enableQuickNavigationCenterDisplayAtClickPosition(false);
			// enable Selected symbol
			// MainTabPanelView.CANVAS_MENU_POPUP.enableSelectedSymbol(false);

			// System.err.println("clicked upper part");
			if (mouseX > addToLeftToCenterDrawing
					&& mouseX < (TOTAL_CANVAS_WIDTH - addToLeftToCenterDrawing)) {

				// select slice first
				final int sliceXClicked = (int) Math
						.floor((mouseX - addToLeftToCenterDrawing)
								/ SLICE_CANVAS_WIDTH);

				boolean clickedRightSideOfSlice = false;
				if (mouseX > ((SLICE_CANVAS_WIDTH * sliceXClicked) + (SLICE_CANVAS_WIDTH / 2))) {
					clickedRightSideOfSlice = true;
				}
				// System.err.println("clickedRightSide : "+clickedRightSide);
				boolean clickedSSymbolInUpperPart = false;
				if (mouseY > (TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)) {
					clickedSSymbolInUpperPart = true;
				}

				//final 
				HolderAbsoluteProportionItem preSiIT = null;
				try{
					preSiIT = associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.get(associatedGenomePanel
									.getGenomePanelItem()
									.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
									+ sliceXClicked)
							.getListHAPI()
							.get(associatedGenomePanel
									.getGenomePanelItem()
									.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
											associatedGenomePanel
													.getGenomePanelItem()
													.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
													+ sliceXClicked));

				}catch(Exception e){
					preSiIT = null;
				}
				
				final HolderAbsoluteProportionItem siIT = preSiIT;
				
				if(siIT != null){

					if (siIT.getAbsoluteProportionQComparableQSSpanItem() == null
							&& siIT.getAbsoluteProportionSComparableSSpanItem() == null
							&& clickedSSymbolInUpperPart && clickedRightSideOfSlice) {
						// System.err.println("here good");
						selectSymbol(sliceXClicked, true, true, -1, false,
								clickedSSymbolInUpperPart, true, false, false,
								false);
						
					} else if (siIT.getAbsoluteProportionQComparableQSSpanItem() == null
							&& siIT.getAbsoluteProportionSComparableSSpanItem() == null
							&& clickedSSymbolInUpperPart
							&& !clickedRightSideOfSlice) {
						selectSymbol(sliceXClicked, true, true, -1, false,
								clickedSSymbolInUpperPart, false, true, false,
								false);
						
					} else {
						selectSymbol(sliceXClicked, true, true, -1, false,
								clickedSSymbolInUpperPart, false, false, false,
								false);
					}

					if (siIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
						if (siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
							if (
							associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
											+ selectedSliceX).getListHAPI().size() > 1) {

								// Insyght.CANVAS_MENU_POPUP.selectedSymbolListOffShoots.setEnabled(true);
								MainTabPanelView.CANVAS_MENU_POPUP
										.enableSelectedSymbolListOffShoots(true);
								MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolListOffShoots
										.setScheduledCommand(new ScheduledCommand() {
											@Override
											public void execute() {
												final PopupPanel pop = new PopupPanel(
														true);
												VerticalPanel vpRoot = new VerticalPanel();
												
												ScrollPanel scRoot = new ScrollPanel(
														vpRoot);
												
												MainTabPanelView.CANVAS_MENU_POPUP
														.addShowOffShoots(
																selectedSliceX,
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
																		.get(associatedGenomePanel
																				.getGenomePanelItem()
																				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																				+ selectedSliceX),
																(associatedGenomePanel
																		.getGenomePanelItem()
																		.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + selectedSliceX),
																Insyght.APP_CONTROLER
																		.getLIST_GENOME_PANEL()
																		.indexOf(
																				associatedGenomePanel),
																vpRoot, scRoot, pop);
												pop.setWidget(scRoot);
												// pop.showRelativeTo(Insyght.CANVAS_MENU_POPUP);
												scRoot.setHeight("5.5EM");
												pop.setPopupPositionAndShow(new PositionCallback() {
													@Override
													public void setPosition(
															int offsetWidth,
															int offsetHeight) {
														int left = canvasBgk
																.getAbsoluteLeft()
																+ (sliceXClicked * SLICE_CANVAS_WIDTH);
														int top = canvasBgk
																.getAbsoluteTop()
																- offsetHeight + 5;
														pop.setPopupPosition(left,
																top);
													}
												});
												MainTabPanelView.CANVAS_MENU_POPUP
														.hide();
											}
										});
								// return true;
							}

							MainTabPanelView.CANVAS_MENU_POPUP
									.addTransfertGeneToAnnotationsComparator(
											((AbsoPropQSGeneHomoItem) siIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getQsQGeneId(), null);
							
							int qOrganismId = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId();
//									((AbsoPropQSGeneHomoItem) siIT
//									.getAbsoluteProportionQComparableQSSpanItem())
//									.getQsQOrigamiOrganismId();
							int qGeneId = ((AbsoPropQSGeneHomoItem) siIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getQsQGeneId();
							String qGeneName = ((AbsoPropQSGeneHomoItem) siIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getQsQMostSignificantGeneNameAsStrippedString();
							HashMap<Integer, String> qGeneId2qGeneName = new HashMap<>();
							qGeneId2qGeneName.put(qGeneId, qGeneName);
							HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneName = new HashMap<>();
							qOrganismId2qGeneId2qGeneName.put(qOrganismId, qGeneId2qGeneName);
														
							MainTabPanelView.CANVAS_MENU_POPUP
								.addStatSummaryRefCDS(
										qOrganismId2qGeneId2qGeneName
										);
							
							if (Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght()
									.compareTo(EnumResultViewTypes.homolog_table) == 0) {
								// HOMOLOG_BROWSING -> show genomic context of q
								// gene in syntenic organisation view
								// MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization
								// .setText("See gene in genomic organization view");

								MainTabPanelView.CANVAS_MENU_POPUP
										.addFindGeneInGenomicOrganizationView(
												((AbsoPropQSGeneHomoItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getQsQOrigamiElementId(),
												((AbsoPropQSGeneHomoItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getQsQPbStartGeneInElement(),
												((AbsoPropQSGeneHomoItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getQsQPbStopGeneInElement()
										// ,Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(associatedGenomePanel)
										);
								MainTabPanelView.CANVAS_MENU_POPUP.addExportGeneSequence(
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsQMostSignificantGeneNameAsStrippedString(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsQLocusTagAsStrippedString(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsQAccnum(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsQGeneId(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsSMostSignificantGeneNameAsStrippedString(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsSLocusTagAsStrippedString(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsSAccnum(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsSGeneId(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyOrigamiAlignmentId(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyNumberGene(), -1,
										-1, -1, -1, -1, -1,
										Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable());
								MainTabPanelView.CANVAS_MENU_POPUP.addExportTable();
							} else if (Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getViewTypeInsyght()
									.compareTo(EnumResultViewTypes.genomic_organization) == 0) {
								
								if (Insyght.APP_CONTROLER.getCurrSelctGenomePanel().isReferenceGenome) {
									// syntenic organisation -> transfert this q
									// gene and all its paralogs to view in homolog
									// browsing view
									ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent = new ArrayList<Integer>();
									listElementIdsThenStartPbthenStopPBLoopedSent
											.add(((AbsoPropQSGeneHomoItem) siIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getQsQOrigamiElementId());
									listElementIdsThenStartPbthenStopPBLoopedSent
											.add(((AbsoPropQSGeneHomoItem) siIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getQsQPbStartGeneInElement());
									listElementIdsThenStartPbthenStopPBLoopedSent
											.add(((AbsoPropQSGeneHomoItem) siIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getQsQPbStopGeneInElement());
									for (int i = 0; i < associatedGenomePanel
											.getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
											.get(associatedGenomePanel
													.getGenomePanelItem()
													.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
													+ sliceXClicked).getListHAPI()
											.size(); i++) {
										HolderAbsoluteProportionItem hapiToAddToArrayAsWell = associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.get(associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
														+ sliceXClicked)
												.getListHAPI().get(i);
										if (hapiToAddToArrayAsWell
												.getAbsoluteProportionQComparableQSSpanItem() != null) {
											if (hapiToAddToArrayAsWell
													.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
												listElementIdsThenStartPbthenStopPBLoopedSent
														.add(((AbsoPropQSGeneHomoItem) hapiToAddToArrayAsWell
																.getAbsoluteProportionQComparableQSSpanItem())
																.getQsSOrigamiElementId());
												listElementIdsThenStartPbthenStopPBLoopedSent
														.add(((AbsoPropQSGeneHomoItem) hapiToAddToArrayAsWell
																.getAbsoluteProportionQComparableQSSpanItem())
																.getQsSPbStartGeneInElement());
												listElementIdsThenStartPbthenStopPBLoopedSent
														.add(((AbsoPropQSGeneHomoItem) hapiToAddToArrayAsWell
																.getAbsoluteProportionQComparableQSSpanItem())
																.getQsSPbStopGeneInElement());
											}
										} else if (hapiToAddToArrayAsWell
												.getAbsoluteProportionSComparableSSpanItem() != null) {
											if (hapiToAddToArrayAsWell
													.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
												listElementIdsThenStartPbthenStopPBLoopedSent
														.add(((AbsoPropSGeneInserItem) hapiToAddToArrayAsWell
																.getAbsoluteProportionSComparableSSpanItem())
																.getsOrigamiElementId());
												listElementIdsThenStartPbthenStopPBLoopedSent
														.add(((AbsoPropSGeneInserItem) hapiToAddToArrayAsWell
																.getAbsoluteProportionSComparableSSpanItem())
																.getsPbStartGeneInElement());
												listElementIdsThenStartPbthenStopPBLoopedSent
														.add(((AbsoPropSGeneInserItem) hapiToAddToArrayAsWell
																.getAbsoluteProportionSComparableSSpanItem())
																.getsPbStopGeneInElement());
											}
										}
									}

									// MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization
									// .setText("Transfer all paralogs in homolog browsing view");
									MainTabPanelView.CANVAS_MENU_POPUP
											.addTransfertGeneWithParalogsFromRefOrganismToHomologBrowsingView(
													listElementIdsThenStartPbthenStopPBLoopedSent
													//,Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(associatedGenomePanel)
													//,false
													);

								} else {
									// syntenic organisation -> transfert this q
									// gene to view in homolog browsing view
									// MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization
									// .setText("Transfer gene in homolog browsing view");
									MainTabPanelView.CANVAS_MENU_POPUP
											.addTransfertGeneToHomologBrowsingView(
													((AbsoPropQSGeneHomoItem) siIT
															.getAbsoluteProportionQComparableQSSpanItem()).getQsQGeneId()
													);
								}

								MainTabPanelView.CANVAS_MENU_POPUP
										.addSynchronizeAllDisplaysOnReferenceGene(
												((AbsoPropQSGeneHomoItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getQsQMostSignificantGeneNameAsStrippedString(),
												((AbsoPropQSGeneHomoItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getQsQOrigamiElementId(),
												((AbsoPropQSGeneHomoItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getQsQPbStartGeneInElement(),
												((AbsoPropQSGeneHomoItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getQsQPbStopGeneInElement()
												//,Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(associatedGenomePanel)
												);
								
								MainTabPanelView.CANVAS_MENU_POPUP.addExportGeneSequence(
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsQMostSignificantGeneNameAsStrippedString(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsQLocusTagAsStrippedString(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsQAccnum(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsQGeneId(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsSMostSignificantGeneNameAsStrippedString(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsSLocusTagAsStrippedString(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsSAccnum(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getQsSGeneId(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyOrigamiAlignmentId(),
										((AbsoPropQSGeneHomoItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyNumberGene(), -1,
										-1, -1, -1, -1, -1,
										null);

							}

							
							// return true;

						} else if (siIT
								.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
							
							// System.err.println("mouseY : "+mouseY);
							// System.err.println("Y_RECT_HOMOLOG_BLOCK+H_RECT_HOMOLOG_BLOCK : "+(Y_RECT_HOMOLOG_BLOCK+H_RECT_HOMOLOG_BLOCK));
							// System.err.println("size = "+associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX).getListHAPI().size());
							// System.err.println("selectedSliceX = "+selectedSliceX);

							// Insyght.CANVAS_MENU_POPUP.clearVpRoot();

							// if(mouseY >
							// (Y_RECT_HOMOLOG_BLOCK+H_RECT_HOMOLOG_BLOCK) &&
							// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX).getListHAPI().size()>1){
							// clicked on other match
							// System.err.println("clicked on other match");
							if (((AbsoPropQSSyntItem) siIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getQsEnumBlockType().toString()
									.startsWith("QS_CUT_")) {
								// show nothing because cut
								// return false;
							} else {

								if (associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.get(associatedGenomePanel
												.getGenomePanelItem()
												.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
												+ selectedSliceX).getListHAPI()
										.size() > 1) {
									// Insyght.CANVAS_MENU_POPUP.selectedSymbolListOffShoots.setEnabled(true);
									MainTabPanelView.CANVAS_MENU_POPUP
											.enableSelectedSymbolListOffShoots(true);
									MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolListOffShoots
											.setScheduledCommand(new ScheduledCommand() {
												@Override
												public void execute() {
													final PopupPanel pop = new PopupPanel(
															true);
													VerticalPanel vpRoot = new VerticalPanel();
													ScrollPanel scRoot = new ScrollPanel(
															vpRoot);
													MainTabPanelView.CANVAS_MENU_POPUP
															.addShowOffShoots(
																	selectedSliceX,
																	associatedGenomePanel
																			.getGenomePanelItem()
																			.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
																			.get(associatedGenomePanel
																					.getGenomePanelItem()
																					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																					+ selectedSliceX),
																	// (associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX),
																	siIT.getIndexInFullArray(),
																	Insyght.APP_CONTROLER
																			.getLIST_GENOME_PANEL()
																			.indexOf(
																					associatedGenomePanel),
																	vpRoot, scRoot,
																	pop);

													pop.setWidget(scRoot);
													// pop.showRelativeTo(Insyght.CANVAS_MENU_POPUP);
													pop.setPopupPositionAndShow(new PositionCallback() {
														@Override
														public void setPosition(
																int offsetWidth,
																int offsetHeight) {
															int left = canvasBgk
																	.getAbsoluteLeft()
																	+ (sliceXClicked * SLICE_CANVAS_WIDTH);
															int top = canvasBgk
																	.getAbsoluteTop()
																	- offsetHeight
																	+ 5;
															pop.setPopupPosition(
																	left, top);
														}
													});
													MainTabPanelView.CANVAS_MENU_POPUP
															.hide();
												}
											});
								}

								// return true;
							}

							// }else{
							// clicked on box
							// MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization
							// .setText("Transfer gene(s) in homolog browsing view");
							MainTabPanelView.CANVAS_MENU_POPUP
									.addTransfertGenesSetFromSyntenyToHomologBrowsingView(((AbsoPropQSSyntItem) siIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getSyntenyOrigamiAlignmentId()
									// ,Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(associatedGenomePanel)
									);

							ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedIT = new ArrayList<Integer>();
							listElementIdsThenStartPbthenStopPBLoopedIT
									.add(((AbsoPropQSSyntItem) siIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getQsQOrigamiElementId());
							listElementIdsThenStartPbthenStopPBLoopedIT
									.add(((AbsoPropQSSyntItem) siIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getPbQStartSyntenyInElement());
							listElementIdsThenStartPbthenStopPBLoopedIT
									.add(((AbsoPropQSSyntItem) siIT
											.getAbsoluteProportionQComparableQSSpanItem())
											.getPbQStopSyntenyInElement());
							MainTabPanelView.CANVAS_MENU_POPUP
									.addTransfertGeneSetToAnnotationsComparator(
											null,
											listElementIdsThenStartPbthenStopPBLoopedIT);

							MainTabPanelView.CANVAS_MENU_POPUP
									.addExportGeneSequence(
											null,
											null,
											null,
											-1,
											null,
											null,
											null,
											-1,
											((AbsoPropQSSyntItem) siIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getSyntenyOrigamiAlignmentId(),
											((AbsoPropQSSyntItem) siIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getSyntenyNumberGene(), -1,
											-1, -1, -1, -1, -1,
											null);

							if (((AbsoPropQSSyntItem) siIT
									.getAbsoluteProportionQComparableQSSpanItem())
									.getQsEnumBlockType().toString()
									.startsWith("QS_CUT_")) {
								// display hide instead of show
								int startIndexInFullArrayIT = -1;
								int stopIndexInFullArrayIT = -1;
								int displayIndexStart = -1;
								if (((AbsoPropQSSyntItem) siIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getQsEnumBlockType().toString()
										.startsWith("QS_CUT_START_")) {
									startIndexInFullArrayIT = siIT
											.getIndexInFullArray(); //
									displayIndexStart = associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed();
									// find stop now
									// stopIndexInFullArrayIT =
									// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getIdxRefOtherPartForQSCutStartOrStopTypes();
									BUILDMENUCONTEXTUELFINDCUTSTOP: for (int i = startIndexInFullArrayIT; i < associatedGenomePanel
											.getGenomePanelItem()
											.getFullAssociatedlistAbsoluteProportionItemToDisplay()
											.size(); i++) {
										for (int j = 0; j < associatedGenomePanel
												.getGenomePanelItem()
												.getFullAssociatedlistAbsoluteProportionItemToDisplay()
												.get(i).getListHAPI().size(); j++) {
											HolderAbsoluteProportionItem hapiIT = associatedGenomePanel
													.getGenomePanelItem()
													.getFullAssociatedlistAbsoluteProportionItemToDisplay()
													.get(i).getListHAPI().get(j);
											if (hapiIT
													.getAbsoluteProportionQComparableQSSpanItem() != null) {
												if (hapiIT
														.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
													if (((AbsoPropQSSyntItem) hapiIT
															.getAbsoluteProportionQComparableQSSpanItem())
															.getQsEnumBlockType()
															.toString()
															.startsWith(
																	"QS_CUT_STOP_")) {
														if (((AbsoPropQSSyntItem) siIT
																.getAbsoluteProportionQComparableQSSpanItem())
																.getqPercentStart() == ((AbsoPropQSSyntItem) hapiIT
																.getAbsoluteProportionQComparableQSSpanItem())
																.getqPercentStart()
																&& ((AbsoPropQSSyntItem) siIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getqPercentStop() == ((AbsoPropQSSyntItem) hapiIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getqPercentStop()
																&& ((AbsoPropQSSyntItem) siIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getsPercentStart() == ((AbsoPropQSSyntItem) hapiIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getsPercentStart()
																&& ((AbsoPropQSSyntItem) siIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getsPercentStop() == ((AbsoPropQSSyntItem) hapiIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getsPercentStop()) {
															// found cut stop
															stopIndexInFullArrayIT = i;
															break BUILDMENUCONTEXTUELFINDCUTSTOP;
														}
													}
												}
											}
										}
									}
									if (startIndexInFullArrayIT < 0
											|| stopIndexInFullArrayIT < 0) {
										//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper part cut start startIndexInArrayIT < 0 || stopIndexInArrayIT < 0");
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp clicked upper" +
												" part cut start startIndexInArrayIT < 0 || stopIndexInArrayIT < 0"));
										return false;
									} else {
										MainTabPanelView.CANVAS_MENU_POPUP
												.addHideGenesInSyntenyRegion(
														startIndexInFullArrayIT,
														stopIndexInFullArrayIT,
														displayIndexStart, false);
										// return true;
									}

								} else if (((AbsoPropQSSyntItem) siIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.getQsEnumBlockType().toString()
										.startsWith("QS_CUT_STOP_")) {
									stopIndexInFullArrayIT = siIT
											.getIndexInFullArray(); // indexStartBirdSynteny+selectedSliceX;
									// find start now
									// startIndexInFullArrayIT =
									// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getIdxRefOtherPartForQSCutStartOrStopTypes();
									BUILDMENUCONTEXTUELFINDCUTSTART: for (int i = stopIndexInFullArrayIT; i >= 0; i--) {
										for (int j = 0; j < associatedGenomePanel
												.getGenomePanelItem()
												.getFullAssociatedlistAbsoluteProportionItemToDisplay()
												.get(i).getListHAPI().size(); j++) {
											HolderAbsoluteProportionItem hapiIT = associatedGenomePanel
													.getGenomePanelItem()
													.getFullAssociatedlistAbsoluteProportionItemToDisplay()
													.get(i).getListHAPI().get(j);
											if (hapiIT
													.getAbsoluteProportionQComparableQSSpanItem() != null) {
												if (hapiIT
														.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
													if (((AbsoPropQSSyntItem) hapiIT
															.getAbsoluteProportionQComparableQSSpanItem())
															.getQsEnumBlockType()
															.toString()
															.startsWith(
																	"QS_CUT_START_")) {
														if (((AbsoPropQSSyntItem) siIT
																.getAbsoluteProportionQComparableQSSpanItem())
																.getqPercentStart() == ((AbsoPropQSSyntItem) hapiIT
																.getAbsoluteProportionQComparableQSSpanItem())
																.getqPercentStart()
																&& ((AbsoPropQSSyntItem) siIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getqPercentStop() == ((AbsoPropQSSyntItem) hapiIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getqPercentStop()
																&& ((AbsoPropQSSyntItem) siIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getsPercentStart() == ((AbsoPropQSSyntItem) hapiIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getsPercentStart()
																&& ((AbsoPropQSSyntItem) siIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getsPercentStop() == ((AbsoPropQSSyntItem) hapiIT
																		.getAbsoluteProportionQComparableQSSpanItem())
																		.getsPercentStop()) {
															// found cut start
															startIndexInFullArrayIT = i;
															break BUILDMENUCONTEXTUELFINDCUTSTART;
														}
													}
												}
											}
										}
									}
									if (startIndexInFullArrayIT < 0
											|| stopIndexInFullArrayIT < 0) {
										//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper part cut start startIndexInArrayIT < 0 || stopIndexInArrayIT < 0");
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp" +
												" clicked upper part cut start startIndexInArrayIT < 0 || stopIndexInArrayIT < 0"));
										return false;
									} else {
										displayIndexStart = (associatedGenomePanel
												.getGenomePanelItem()
												.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed())
												- (stopIndexInFullArrayIT - startIndexInFullArrayIT);
										if (displayIndexStart < 0) {
											displayIndexStart = 0;
										}
										MainTabPanelView.CANVAS_MENU_POPUP
												.addHideGenesInSyntenyRegion(
														startIndexInFullArrayIT,
														stopIndexInFullArrayIT,
														displayIndexStart, false);
										// return true;
									}

								} else {
									//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper part getQsEnumBlockType not start or stop");
									Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp clicked" +
											" upper part getQsEnumBlockType not start or stop"));
									return false;
								}
								// return false;
							} else {

								ArrayList<Long> extendedListOrigamiAlignmentIdsMotherSyntenies = new ArrayList<Long>(
										((AbsoPropQSSyntItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getListOrigamiAlignmentIdsMotherSyntenies());

								if (((AbsoPropQSSyntItem) siIT
										.getAbsoluteProportionQComparableQSSpanItem())
										.isContainedWithinAnotherMotherSynteny()
										|| ((AbsoPropQSSyntItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.isSprungOffAnotherMotherSynteny()) {
									// get the mother synteny
									// System.out.println("here1");
									HolderAbsoluteProportionItem siMotherIT = associatedGenomePanel
											.getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
											.get(associatedGenomePanel
													.getGenomePanelItem()
													.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
													+ sliceXClicked).getListHAPI()
											.get(0);
									if (siMotherIT != siIT
											&& siMotherIT
													.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										// System.out.println("here2");
										if (((AbsoPropQSSyntItem) siMotherIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.isMotherOfContainedOtherSyntenies()
												|| ((AbsoPropQSSyntItem) siMotherIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isMotherOfSprungOffSyntenies()) {
											// System.out.println("here3");
											for (int k = 0; k < ((AbsoPropQSSyntItem) siMotherIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getListOrigamiAlignmentIdsContainedOtherSyntenies()
													.size(); k++) {
												long origAlignmentIdToMaybeAddIT = ((AbsoPropQSSyntItem) siMotherIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getListOrigamiAlignmentIdsContainedOtherSyntenies()
														.get(k);
												if (origAlignmentIdToMaybeAddIT != ((AbsoPropQSSyntItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getSyntenyOrigamiAlignmentId()) {
													// System.out.println("here4");
													extendedListOrigamiAlignmentIdsMotherSyntenies
															.add(origAlignmentIdToMaybeAddIT);
												}
											}
											for (int k = 0; k < ((AbsoPropQSSyntItem) siMotherIT
													.getAbsoluteProportionQComparableQSSpanItem())
													.getListOrigamiAlignmentIdsSprungOffSyntenies()
													.size(); k++) {
												long origAlignmentIdToMaybeAddIT = ((AbsoPropQSSyntItem) siMotherIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getListOrigamiAlignmentIdsSprungOffSyntenies()
														.get(k);
												if (origAlignmentIdToMaybeAddIT != ((AbsoPropQSSyntItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getSyntenyOrigamiAlignmentId()) {
													// System.out.println("here5");
													extendedListOrigamiAlignmentIdsMotherSyntenies
															.add(origAlignmentIdToMaybeAddIT);
												}
											}
										}
									}
								}

								
								MainTabPanelView.CANVAS_MENU_POPUP.addListGenesInSelectedSymbol(
										((AbsoPropQSSyntItem) siIT
												.getAbsoluteProportionQComparableQSSpanItem())
												.getSyntenyOrigamiAlignmentId(),
												null);
								
								MainTabPanelView.CANVAS_MENU_POPUP
										.addShowGenesInSyntenyRegion(
												((AbsoPropQSSyntItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getSyntenyOrigamiAlignmentId(),
												((AbsoPropQSSyntItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getListOrigamiAlignmentIdsContainedOtherSyntenies(),
												// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsMotherSyntenies(),
												extendedListOrigamiAlignmentIdsMotherSyntenies,
												((AbsoPropQSSyntItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.getListOrigamiAlignmentIdsSprungOffSyntenies(),
												// (associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX),
												siIT.getIndexInFullArray(),
												Insyght.APP_CONTROLER
														.getLIST_GENOME_PANEL()
														.indexOf(
																associatedGenomePanel),
												// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getQsQAccnum(),
												// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getQsQPbStartOfElementInOrga(),
												// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getQsQSizeOfOrganismInPb(),
												// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getQsSAccnum(),
												// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getQsSPbStartOfElementInOrga(),
												// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getQsSSizeOfOrganismInPb(),
												((AbsoPropQSSyntItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isContainedWithinAnotherMotherSynteny(),
												((AbsoPropQSSyntItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isMotherOfContainedOtherSyntenies(),
												((AbsoPropQSSyntItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isMotherOfSprungOffSyntenies(),
												((AbsoPropQSSyntItem) siIT
														.getAbsoluteProportionQComparableQSSpanItem())
														.isSprungOffAnotherMotherSynteny(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																associatedGenomePanel
																		.getGenomePanelItem()
																		.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																		+ sliceXClicked));

							}

							// }
							// }
						} else {
							//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper partunrecognized class for object sent");
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp clicked upper" +
									" partunrecognized class for object sent"));
							return false;
						}

					} else {

						// System.err.println("here1 : "+selectedQ);
						if (siIT.getAbsoluteProportionQComparableQSpanItem() != null
								&& selectedQ) {
							if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {

								// MainTabPanelView.CANVAS_MENU_POPUP.enableSelectedSymbolReferenceGene(true);

								if (((AbsoPropQGeneInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqEnumBlockType().toString()
										.startsWith("Q_CUT_")) {
									// show nothing because cut
									// return false;
								} else {

									if (associatedGenomePanel
											.getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
											.get(associatedGenomePanel
													.getGenomePanelItem()
													.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
													+ selectedSliceX).getListHAPI()
											.size() > 1) {
										// Insyght.CANVAS_MENU_POPUP.selectedSymbolListOffShoots.setEnabled(true);
										MainTabPanelView.CANVAS_MENU_POPUP
												.enableSelectedSymbolListOffShoots(true);
										MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolListOffShoots
												.setScheduledCommand(new ScheduledCommand() {
													@Override
													public void execute() {
														final PopupPanel pop = new PopupPanel(
																true);
														VerticalPanel vpRoot = new VerticalPanel();
														ScrollPanel scRoot = new ScrollPanel(
																vpRoot);
														MainTabPanelView.CANVAS_MENU_POPUP
																.addShowOffShoots(
																		selectedSliceX,
																		associatedGenomePanel
																				.getGenomePanelItem()
																				.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
																				.get(associatedGenomePanel
																						.getGenomePanelItem()
																						.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																						+ selectedSliceX),
																		// (associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX),
																		siIT.getIndexInFullArray(),
																		Insyght.APP_CONTROLER
																				.getLIST_GENOME_PANEL()
																				.indexOf(
																						associatedGenomePanel),
																		vpRoot,
																		scRoot, pop);
														pop.setWidget(scRoot);
														// pop.showRelativeTo(Insyght.CANVAS_MENU_POPUP);
														pop.setPopupPositionAndShow(new PositionCallback() {
															@Override
															public void setPosition(
																	int offsetWidth,
																	int offsetHeight) {
																int left = canvasBgk
																		.getAbsoluteLeft()
																		+ (sliceXClicked * SLICE_CANVAS_WIDTH);
																int top = canvasBgk
																		.getAbsoluteTop()
																		- offsetHeight
																		+ 5;
																pop.setPopupPosition(
																		left, top);
															}
														});
														MainTabPanelView.CANVAS_MENU_POPUP
																.hide();
													}
												});

									}

								}

								// clicked on box

								if (Insyght.APP_CONTROLER
										.getCurrentRefGenomePanelAndListOrgaResult()
										.getViewTypeInsyght()
										.compareTo(
												EnumResultViewTypes.homolog_table) == 0) {
									// HOMOLOG_BROWSING -> show genomic context of q
									// gene in syntenic organisation view
									// MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization
									// .setText("Transfer gene in genomic organization view");
									MainTabPanelView.CANVAS_MENU_POPUP
											.addFindGeneInGenomicOrganizationView(
													((AbsoPropQGeneInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem())
															.getqOrigamiElementId(),
													((AbsoPropQGeneInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem())
															.getqPbStartGeneInElement(),
													((AbsoPropQGeneInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem())
															.getqPbStopGeneInElement()
											// ,Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(associatedGenomePanel)
											);
									
									MainTabPanelView.CANVAS_MENU_POPUP
									.addExportGeneSequence(
											((AbsoPropQGeneInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQMostSignificantGeneNameAsStrippedString(),
											((AbsoPropQGeneInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQLocusTagAsStrippedString(),
											((AbsoPropQGeneInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqAccnum(),
											((AbsoPropQGeneInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQGeneId(), null, null,
											null, -1, -1, -1, -1, -1, -1, -1,
											-1, -1,
											Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable());
									MainTabPanelView.CANVAS_MENU_POPUP.addExportTable();
								} else {
									// syntenic organisation -> transfert this q
									// gene to view in homolog browsing view
									// MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolTransferGeneToViewHomologBrowsingGenomicOrganization
									// .setText("Transfer gene in homolog browsing view");
									MainTabPanelView.CANVAS_MENU_POPUP
											.addTransfertGeneToHomologBrowsingView(
													((AbsoPropQGeneInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem()).getQGeneId()
//													((AbsoluteProportionQGeneInsertionItem) siIT
//															.getAbsoluteProportionQComparableQSpanItem())
//															.getqOrigamiElementId(),
//													((AbsoluteProportionQGeneInsertionItem) siIT
//															.getAbsoluteProportionQComparableQSpanItem())
//															.getqPbStartGeneInElement(),
//													((AbsoluteProportionQGeneInsertionItem) siIT
//															.getAbsoluteProportionQComparableQSpanItem())
//															.getqPbStopGeneInElement(),
//													Insyght.APP_CONTROLER
//															.getLIST_GENOME_PANEL()
//															.indexOf(
//																	associatedGenomePanel)
													);
									MainTabPanelView.CANVAS_MENU_POPUP
									.addSynchronizeAllDisplaysOnReferenceGene(
											((AbsoPropQGeneInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQMostSignificantGeneNameAsStrippedString(),
													((AbsoPropQGeneInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem())
													.getqOrigamiElementId(),
													((AbsoPropQGeneInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem())
													.getqPbStartGeneInElement(),
													((AbsoPropQGeneInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem())
													.getqPbStopGeneInElement()
											//,Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(associatedGenomePanel)
											);
									
									MainTabPanelView.CANVAS_MENU_POPUP
									.addExportGeneSequence(
											((AbsoPropQGeneInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQMostSignificantGeneNameAsStrippedString(),
											((AbsoPropQGeneInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQLocusTagAsStrippedString(),
											((AbsoPropQGeneInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqAccnum(),
											((AbsoPropQGeneInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getQGeneId(), null, null,
											null, -1, -1, -1, -1, -1, -1, -1,
											-1, -1,
											null);
									
								}

								
								MainTabPanelView.CANVAS_MENU_POPUP
										.addTransfertGeneToAnnotationsComparator(
												((AbsoPropQGeneInserItem) siIT
														.getAbsoluteProportionQComparableQSpanItem())
														.getQGeneId(), null);
								int qOrganismId = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId();
//										((AbsoPropQGeneInserItem) siIT
//										.getAbsoluteProportionQComparableQSpanItem())
//										.getqOrigamiOrganismId();
								int qGeneId = ((AbsoPropQGeneInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getQGeneId();
								String qGeneName = ((AbsoPropQGeneInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getQMostSignificantGeneNameAsStrippedString();
								HashMap<Integer, String> qGeneId2qGeneName = new HashMap<>();
								qGeneId2qGeneName.put(qGeneId, qGeneName);
								HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneName = new HashMap<>();
								qOrganismId2qGeneId2qGeneName.put(qOrganismId, qGeneId2qGeneName);
								MainTabPanelView.CANVAS_MENU_POPUP
										.addStatSummaryRefCDS(
												qOrganismId2qGeneId2qGeneName
												);

							} else if (siIT
									.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {

								// Insyght.CANVAS_MENU_POPUP.clearVpRoot();
								// if(mouseY >
								// (Q_INSERTION_Y_POINT-((Q_INSERTION_Y_POINT-Q_INSERTION_LBEZIER_Y_RTC)/2))
								// &&
								// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX).getListHAPI().size()>1){

								// clicked on other match
								// System.err.println("clicked on other match");
								if (((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getqEnumBlockType().toString()
										.startsWith("Q_CUT_")) {
									// show nothing because cut
									// return false;
								} else {

									if (associatedGenomePanel
											.getGenomePanelItem()
											.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
											.get(associatedGenomePanel
													.getGenomePanelItem()
													.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
													+ selectedSliceX).getListHAPI()
											.size() > 1) {
										// Insyght.CANVAS_MENU_POPUP.selectedSymbolListOffShoots.setEnabled(true);
										MainTabPanelView.CANVAS_MENU_POPUP
												.enableSelectedSymbolListOffShoots(true);
										MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolListOffShoots
												.setScheduledCommand(new ScheduledCommand() {
													@Override
													public void execute() {
														final PopupPanel pop = new PopupPanel(
																true);
														VerticalPanel vpRoot = new VerticalPanel();
														ScrollPanel scRoot = new ScrollPanel(
																vpRoot);
														MainTabPanelView.CANVAS_MENU_POPUP
																.addShowOffShoots(
																		selectedSliceX,
																		associatedGenomePanel
																				.getGenomePanelItem()
																				.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
																				.get(associatedGenomePanel
																						.getGenomePanelItem()
																						.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																						+ selectedSliceX),
																		// (associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX),
																		siIT.getIndexInFullArray(),
																		Insyght.APP_CONTROLER
																				.getLIST_GENOME_PANEL()
																				.indexOf(
																						associatedGenomePanel),
																		vpRoot,
																		scRoot, pop);
														pop.setWidget(scRoot);
														// pop.showRelativeTo(Insyght.CANVAS_MENU_POPUP);
														pop.setPopupPositionAndShow(new PositionCallback() {
															@Override
															public void setPosition(
																	int offsetWidth,
																	int offsetHeight) {
																int left = canvasBgk
																		.getAbsoluteLeft()
																		+ (sliceXClicked * SLICE_CANVAS_WIDTH);
																int top = canvasBgk
																		.getAbsoluteTop()
																		- offsetHeight
																		+ 5;
																pop.setPopupPosition(
																		left, top);
															}
														});
														MainTabPanelView.CANVAS_MENU_POPUP
																.hide();
													}
												});
									}

									// return true;
								}

								// test data, to improve apr?s que
								// qAbsolutePropoGenomicRegion extends
								// qAbsolutePropoElement
								if (((AbsoPropQGenoRegiInserItem) siIT
										.getAbsoluteProportionQComparableQSpanItem())
										.getQGenomicRegionInsertionNumberGene() > 0) {

									// MainTabPanelView.CANVAS_MENU_POPUP.enableSelectedSymbolReferenceGene(true);

									ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedIT = new ArrayList<Integer>();
									listElementIdsThenStartPbthenStopPBLoopedIT
											.add(((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqOrigamiElementId());
									listElementIdsThenStartPbthenStopPBLoopedIT
											.add(((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqPbStartQGenomicRegionInsertionInElement());
									listElementIdsThenStartPbthenStopPBLoopedIT
											.add(((AbsoPropQGenoRegiInserItem) siIT
													.getAbsoluteProportionQComparableQSpanItem())
													.getqPbStopQGenomicRegionInsertionInElement());
									
									MainTabPanelView.CANVAS_MENU_POPUP
										.addTransfertGeneSetToHomologBrowsingView(listElementIdsThenStartPbthenStopPBLoopedIT);
									
									MainTabPanelView.CANVAS_MENU_POPUP
											.addTransfertGeneSetToAnnotationsComparator(
													null,
													listElementIdsThenStartPbthenStopPBLoopedIT);

									MainTabPanelView.CANVAS_MENU_POPUP
											.addExportGeneSequence(
													null,
													null,
													null,
													-1,
													null,
													null,
													null,
													-1,
													-1,
													-1,
													((AbsoPropQGenoRegiInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem())
															.getqOrigamiElementId(),
													((AbsoPropQGenoRegiInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem())
															.getqPbStartQGenomicRegionInsertionInElement(),
													((AbsoPropQGenoRegiInserItem) siIT
															.getAbsoluteProportionQComparableQSpanItem())
															.getqPbStopQGenomicRegionInsertionInElement(),
													-1, -1, -1,
													null);

									if (((AbsoPropQGenoRegiInserItem) siIT
											.getAbsoluteProportionQComparableQSpanItem())
											.getqEnumBlockType().toString()
											.startsWith("Q_CUT_")) {
										// display hide instead of show
										int startIndexInFullArrayIT = -1;
										int stopIndexInFullArrayIT = -1;
										int displayIndexStart = -1;
										if (((AbsoPropQGenoRegiInserItem) siIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqEnumBlockType().toString()
												.startsWith("Q_CUT_START_")) {
											startIndexInFullArrayIT = siIT
													.getIndexInFullArray(); //
											displayIndexStart = associatedGenomePanel
													.getGenomePanelItem()
													.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed();
											// find stop now
											// stopIndexInFullArrayIT =
											// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getIdxRefOtherPartForQSCutStartOrStopTypes();
											BUILDMENUCONTEXTUELFINDCUTSTOP: for (int i = startIndexInFullArrayIT; i < associatedGenomePanel
													.getGenomePanelItem()
													.getFullAssociatedlistAbsoluteProportionItemToDisplay()
													.size(); i++) {
												for (int j = 0; j < associatedGenomePanel
														.getGenomePanelItem()
														.getFullAssociatedlistAbsoluteProportionItemToDisplay()
														.get(i).getListHAPI()
														.size(); j++) {
													HolderAbsoluteProportionItem hapiIT = associatedGenomePanel
															.getGenomePanelItem()
															.getFullAssociatedlistAbsoluteProportionItemToDisplay()
															.get(i).getListHAPI()
															.get(j);
													if (hapiIT
															.getAbsoluteProportionQComparableQSpanItem() != null) {
														if (hapiIT
																.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
															if (((AbsoPropQGenoRegiInserItem) hapiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.getqEnumBlockType()
																	.toString()
																	.startsWith(
																			"Q_CUT_STOP_")) {
																if (((AbsoPropQGenoRegiInserItem) siIT
																		.getAbsoluteProportionQComparableQSpanItem())
																		.getqPercentStart() == ((AbsoPropQGenoRegiInserItem) hapiIT
																		.getAbsoluteProportionQComparableQSpanItem())
																		.getqPercentStart()
																		&& ((AbsoPropQGenoRegiInserItem) siIT
																				.getAbsoluteProportionQComparableQSpanItem())
																				.getqPercentStop() == ((AbsoPropQGenoRegiInserItem) hapiIT
																				.getAbsoluteProportionQComparableQSpanItem())
																				.getqPercentStop()
																// &&
																// ((AbsoluteProportionQGenomicRegionInsertionItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStart()
																// ==
																// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStart()
																// &&
																// ((AbsoluteProportionQGenomicRegionInsertionItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStop()
																// ==
																// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStop()
																) {
																	// found cut
																	// stop
																	stopIndexInFullArrayIT = i;
																	break BUILDMENUCONTEXTUELFINDCUTSTOP;
																}
															}
														}
													}
												}
											}
											if (startIndexInFullArrayIT < 0
													|| stopIndexInFullArrayIT < 0) {
												//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper part cut start AbsoluteProportionQGenomicRegionInsertionItem startIndexInArrayIT < 0 || stopIndexInArrayIT < 0");
												Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp clicked " +
														"upper part cut start AbsoluteProportionQGenomicRegionInsertionItem startIndexInArrayIT < 0 || stopIndexInArrayIT < 0"));
												return false;
											} else {
												MainTabPanelView.CANVAS_MENU_POPUP
														.addHideGenesInSyntenyRegion(
																startIndexInFullArrayIT,
																stopIndexInFullArrayIT,
																displayIndexStart,
																false);
												// return true;
											}

										} else if (((AbsoPropQGenoRegiInserItem) siIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqEnumBlockType().toString()
												.startsWith("Q_CUT_STOP_")) {
											stopIndexInFullArrayIT = siIT
													.getIndexInFullArray(); // indexStartBirdSynteny+selectedSliceX;
											// find start now
											// startIndexInFullArrayIT =
											// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getIdxRefOtherPartForQSCutStartOrStopTypes();
											BUILDMENUCONTEXTUELFINDCUTSTART: for (int i = stopIndexInFullArrayIT; i >= 0; i--) {
												for (int j = 0; j < associatedGenomePanel
														.getGenomePanelItem()
														.getFullAssociatedlistAbsoluteProportionItemToDisplay()
														.get(i).getListHAPI()
														.size(); j++) {
													HolderAbsoluteProportionItem hapiIT = associatedGenomePanel
															.getGenomePanelItem()
															.getFullAssociatedlistAbsoluteProportionItemToDisplay()
															.get(i).getListHAPI()
															.get(j);
													if (hapiIT
															.getAbsoluteProportionQComparableQSpanItem() != null) {
														if (hapiIT
																.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem) {
															if (((AbsoPropQGenoRegiInserItem) hapiIT
																	.getAbsoluteProportionQComparableQSpanItem())
																	.getqEnumBlockType()
																	.toString()
																	.startsWith(
																			"Q_CUT_START_")) {
																if (((AbsoPropQGenoRegiInserItem) siIT
																		.getAbsoluteProportionQComparableQSpanItem())
																		.getqPercentStart() == ((AbsoPropQGenoRegiInserItem) hapiIT
																		.getAbsoluteProportionQComparableQSpanItem())
																		.getqPercentStart()
																		&& ((AbsoPropQGenoRegiInserItem) siIT
																				.getAbsoluteProportionQComparableQSpanItem())
																				.getqPercentStop() == ((AbsoPropQGenoRegiInserItem) hapiIT
																				.getAbsoluteProportionQComparableQSpanItem())
																				.getqPercentStop()
																// &&
																// ((AbsoluteProportionQGenomicRegionInsertionItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStart()
																// ==
																// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStart()
																// &&
																// ((AbsoluteProportionQGenomicRegionInsertionItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStop()
																// ==
																// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStop()
																) {
																	// found cut
																	// start
																	startIndexInFullArrayIT = i;
																	break BUILDMENUCONTEXTUELFINDCUTSTART;
																}
															}
														}
													}
												}
											}
											if (startIndexInFullArrayIT < 0
													|| stopIndexInFullArrayIT < 0) {
												//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper part cut start AbsoluteProportionQGenomicRegionInsertionItem startIndexInArrayIT < 0 || stopIndexInArrayIT < 0");
												Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp clicked upper" +
														" part cut start AbsoluteProportionQGenomicRegionInsertionItem startIndexInArrayIT < 0 || stopIndexInArrayIT < 0"));
												return false;
											} else {
												displayIndexStart = (associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed())
														- (stopIndexInFullArrayIT - startIndexInFullArrayIT);
												if (displayIndexStart < 0) {
													displayIndexStart = 0;
												}
												MainTabPanelView.CANVAS_MENU_POPUP
														.addHideGenesInSyntenyRegion(
																startIndexInFullArrayIT,
																stopIndexInFullArrayIT,
																displayIndexStart,
																false);
												// return true;
											}

										} else {
											//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper part getQEnumBlockType not start or stop");
											Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp clicked upper " +
													"part getQEnumBlockType not start or stop"));
											return false;
										}
										// return false;
									} else {
										// show gene
										
										ArrayList<Integer> alStartStopElementIdLoopedIT = new ArrayList<Integer>();
										alStartStopElementIdLoopedIT.add(((AbsoPropQGenoRegiInserItem) siIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqOrigamiElementId());
										alStartStopElementIdLoopedIT.add(((AbsoPropQGenoRegiInserItem) siIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqPbStartQGenomicRegionInsertionInElement());
										alStartStopElementIdLoopedIT.add(((AbsoPropQGenoRegiInserItem) siIT
												.getAbsoluteProportionQComparableQSpanItem())
												.getqPbStopQGenomicRegionInsertionInElement());

										MainTabPanelView.CANVAS_MENU_POPUP.addListGenesInSelectedSymbol(
												-1, 
												alStartStopElementIdLoopedIT);
										
										MainTabPanelView.CANVAS_MENU_POPUP
												.addShowGenesInQGenomicRegionInsertion(
														((AbsoPropQGenoRegiInserItem) siIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStartQGenomicRegionInsertionInElement(),
														((AbsoPropQGenoRegiInserItem) siIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStopQGenomicRegionInsertionInElement(),
														((AbsoPropQGenoRegiInserItem) siIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqOrigamiElementId(),
														// (associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX),
														siIT.getIndexInFullArray(),
														Insyght.APP_CONTROLER
																.getLIST_GENOME_PANEL()
																.indexOf(
																		associatedGenomePanel),
														((AbsoPropQGenoRegiInserItem) siIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqAccnum(),
														((AbsoPropQGenoRegiInserItem) siIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getqPbStartOfElementInOrga(),
														((AbsoPropQGenoRegiInserItem) siIT
																.getAbsoluteProportionQComparableQSpanItem())
																.getQsizeOfOragnismInPb());
										// return true;
									}
								} else {
									// return false;
								}
								// }

							} else if (siIT
									.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem) {
								// do nothing for now
								// return false;
							} else {
								//System.err.println("ERROR in q buildMenuContextuelInPopUp clicked upper partunrecognized class for object sent");
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in q buildMenuContextuelInPopUp clicked upper partunrecognized" +
										" class for object sent"));
								return false;
							}

						} else if (!selectedQ) {

							// check if
							// siIT.getAbsoluteProportionSComparableSSpanItem() !=
							// null ou previous/next genomic insertion
							AbsoPropSCompaSSpanItem apscssiIT = null;
							boolean previousSOfNextSlice = false;
							boolean nextSOfPreviousSlice = false;

							// System.err.println("here1");

							if (siIT.getAbsoluteProportionSComparableSSpanItem() != null) {
								apscssiIT = siIT
										.getAbsoluteProportionSComparableSSpanItem();

							} else {
								if (selectedPreviousSOfNextSlice) {
									// System.err.println("here");
									int idxNextSlice = associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
											+ sliceXClicked + 1;
									if (idxNextSlice >= 0
											&& idxNextSlice < associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.size()) {
										HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.get(idxNextSlice)
												.getListHAPI()
												.get(associatedGenomePanel
														.getGenomePanelItem()
														.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																idxNextSlice));
										if (siNextSlice
												.getAbsoluteProportionQComparableQSSpanItem() != null) {
											if (siNextSlice
													.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
												apscssiIT = ((AbsoPropQSSyntItem) siNextSlice
														.getAbsoluteProportionQComparableQSSpanItem())
														.getPreviousGenomicRegionSInsertion();
												previousSOfNextSlice = true;
											}
										} else if (siNextSlice
												.getAbsoluteProportionSComparableSSpanItem() != null) {
											if (siNextSlice
													.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
												if (((AbsoPropSGenoRegiInserItem) siNextSlice
														.getAbsoluteProportionSComparableSSpanItem())
														.getSEnumBlockType()
														.toString()
														.startsWith(
																"S_MISSING_TARGET_Q_")) {
													apscssiIT = ((AbsoPropSGenoRegiInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getsIfMissingTargetPreviousGenomicRegionSInsertion();
													previousSOfNextSlice = true;
												}
											}
										}
									}
								} else if (selectedNextSOfPreviousSlice) {
									int idxPreviousSlice = associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
											+ sliceXClicked - 1;
									if (idxPreviousSlice >= 0
											&& idxPreviousSlice < associatedGenomePanel
													.getGenomePanelItem()
													.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
													.size()) {
										HolderAbsoluteProportionItem siNextSlice = associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.get(idxPreviousSlice)
												.getListHAPI()
												.get(associatedGenomePanel
														.getGenomePanelItem()
														.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
																idxPreviousSlice));
										if (siNextSlice
												.getAbsoluteProportionQComparableQSSpanItem() != null) {
											if (siNextSlice
													.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
												apscssiIT = ((AbsoPropQSSyntItem) siNextSlice
														.getAbsoluteProportionQComparableQSSpanItem())
														.getNextGenomicRegionSInsertion();
												nextSOfPreviousSlice = true;
											}
										} else if (siNextSlice
												.getAbsoluteProportionSComparableSSpanItem() != null) {
											if (siNextSlice
													.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
												if (((AbsoPropSGenoRegiInserItem) siNextSlice
														.getAbsoluteProportionSComparableSSpanItem())
														.getSEnumBlockType()
														.toString()
														.startsWith(
																"S_MISSING_TARGET_Q_")) {
													apscssiIT = ((AbsoPropSGenoRegiInserItem) siNextSlice
															.getAbsoluteProportionSComparableSSpanItem())
															.getsIfMissingTargetNextGenomicRegionSInsertion();
													nextSOfPreviousSlice = true;
												}
											}
										}

									}
								}
							}

							if (apscssiIT != null) {
								if (apscssiIT instanceof AbsoPropSGeneInserItem) {

									// Insyght.CANVAS_MENU_POPUP.clearVpRoot();

									// System.err.println("clicked on other match");
									if (((AbsoPropSGeneInserItem) apscssiIT)
											.getSEnumBlockType().toString()
											.startsWith("S_CUT_")) {
										// show nothing because cut
										// return false;
									} else {

										if (associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.get(associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
														+ selectedSliceX)
												.getListHAPI().size() > 1) {
											// Insyght.CANVAS_MENU_POPUP.selectedSymbolListOffShoots.setEnabled(true);
											MainTabPanelView.CANVAS_MENU_POPUP
													.enableSelectedSymbolListOffShoots(true);
											MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolListOffShoots
													.setScheduledCommand(new ScheduledCommand() {
														@Override
														public void execute() {
															final PopupPanel pop = new PopupPanel(
																	true);
															VerticalPanel vpRoot = new VerticalPanel();
															ScrollPanel scRoot = new ScrollPanel(
																	vpRoot);
															MainTabPanelView.CANVAS_MENU_POPUP
																	.addShowOffShoots(
																			selectedSliceX,
																			associatedGenomePanel
																					.getGenomePanelItem()
																					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
																					.get(associatedGenomePanel
																							.getGenomePanelItem()
																							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																							+ selectedSliceX),
																			// (associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX),
																			siIT.getIndexInFullArray(),
																			Insyght.APP_CONTROLER
																					.getLIST_GENOME_PANEL()
																					.indexOf(
																							associatedGenomePanel),
																			vpRoot,
																			scRoot,
																			pop);
															pop.setWidget(scRoot);
															// pop.showRelativeTo(Insyght.CANVAS_MENU_POPUP);
															pop.setPopupPositionAndShow(new PositionCallback() {
																@Override
																public void setPosition(
																		int offsetWidth,
																		int offsetHeight) {
																	int left = canvasBgk
																			.getAbsoluteLeft()
																			+ (sliceXClicked * SLICE_CANVAS_WIDTH);
																	int top = canvasBgk
																			.getAbsoluteTop()
																			- offsetHeight
																			+ 5;
																	pop.setPopupPosition(
																			left,
																			top);
																}
															});
															MainTabPanelView.CANVAS_MENU_POPUP
																	.hide();
														}
													});
										}

										MainTabPanelView.CANVAS_MENU_POPUP
												.addExportGeneSequence(
														null,
														null,
														null,
														-1,
														((AbsoPropSGeneInserItem) apscssiIT)
																.getsMostSignificantGeneNameAsStrippedString(),
														((AbsoPropSGeneInserItem) apscssiIT)
																.getsLocusTagAsStrippedString(),
														((AbsoPropSGeneInserItem) apscssiIT)
																.getsAccnum(),
														((AbsoPropSGeneInserItem) apscssiIT)
																.getsGeneId(), -1,
														-1, -1, -1, -1, -1, -1, -1,
														null);

										// return true;
									}


								} else if (apscssiIT instanceof AbsoPropSGenoRegiInserItem) {
									// if(mouseY <
									// (S_INSERTION_Y_POINT+((S_INSERTION_LBEZIER_Y_RTC-S_INSERTION_Y_POINT)/2))
									// &&
									// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX).getListHAPI().size()>1){
									// clicked on other match
									// System.err.println("clicked on other match");
									if (((AbsoPropSGenoRegiInserItem) apscssiIT)
											.getSEnumBlockType().toString()
											.startsWith("S_CUT_")) {
										// show nothing because cut
										// return false;
									} else {

										if (associatedGenomePanel
												.getGenomePanelItem()
												.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
												.get(associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
														+ selectedSliceX)
												.getListHAPI().size() > 1) {
											// Insyght.CANVAS_MENU_POPUP.selectedSymbolListOffShoots.setEnabled(true);
											MainTabPanelView.CANVAS_MENU_POPUP
													.enableSelectedSymbolListOffShoots(true);
											MainTabPanelView.CANVAS_MENU_POPUP.selectedSymbolListOffShoots
													.setScheduledCommand(new ScheduledCommand() {
														@Override
														public void execute() {
															final PopupPanel pop = new PopupPanel(
																	true);
															VerticalPanel vpRoot = new VerticalPanel();
															ScrollPanel scRoot = new ScrollPanel(
																	vpRoot);
															MainTabPanelView.CANVAS_MENU_POPUP
																	.addShowOffShoots(
																			selectedSliceX,
																			associatedGenomePanel
																					.getGenomePanelItem()
																					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
																					.get(associatedGenomePanel
																							.getGenomePanelItem()
																							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
																							+ selectedSliceX),
																			// (associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX),
																			siIT.getIndexInFullArray(),
																			Insyght.APP_CONTROLER
																					.getLIST_GENOME_PANEL()
																					.indexOf(
																							associatedGenomePanel),
																			vpRoot,
																			scRoot,
																			pop);
															pop.setWidget(scRoot);
															// pop.showRelativeTo(Insyght.CANVAS_MENU_POPUP);
															pop.setPopupPositionAndShow(new PositionCallback() {
																@Override
																public void setPosition(
																		int offsetWidth,
																		int offsetHeight) {
																	int left = canvasBgk
																			.getAbsoluteLeft()
																			+ (sliceXClicked * SLICE_CANVAS_WIDTH);
																	int top = canvasBgk
																			.getAbsoluteTop()
																			- offsetHeight
																			+ 5;
																	pop.setPopupPosition(
																			left,
																			top);
																}
															});
															MainTabPanelView.CANVAS_MENU_POPUP
																	.hide();
														}
													});
										}

										// return true;
									}
									// }else{
									// clicked on box

									if (((AbsoPropSGenoRegiInserItem) apscssiIT)
											.getSGenomicRegionInsertionNumberGene() > 0) {

										MainTabPanelView.CANVAS_MENU_POPUP
												.addExportGeneSequence(
														null,
														null,
														null,
														-1,
														null,
														null,
														null,
														-1,
														-1,
														-1,
														-1,
														-1,
														-1,
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsOrigamiElementId(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStartSGenomicRegionInsertionInElement(),
														((AbsoPropSGenoRegiInserItem) apscssiIT)
																.getsPbStopSGenomicRegionInsertionInElement(),
																null);

										if (((AbsoPropSGenoRegiInserItem) apscssiIT)
												.getSEnumBlockType().toString()
												.startsWith("S_CUT_")) {
											// display hide instead of show
											int startIndexInFullArrayIT = -1;
											int stopIndexInFullArrayIT = -1;
											int displayIndexStart = -1;
											if (((AbsoPropSGenoRegiInserItem) apscssiIT)
													.getSEnumBlockType().toString()
													.startsWith("S_CUT_START_")) {
												startIndexInFullArrayIT = siIT
														.getIndexInFullArray(); //
												displayIndexStart = associatedGenomePanel
														.getGenomePanelItem()
														.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed();
												// find stop now
												// stopIndexInFullArrayIT =
												// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getIdxRefOtherPartForQSCutStartOrStopTypes();
												BUILDMENUCONTEXTUELFINDCUTSTOP: for (int i = startIndexInFullArrayIT; i < associatedGenomePanel
														.getGenomePanelItem()
														.getFullAssociatedlistAbsoluteProportionItemToDisplay()
														.size(); i++) {
													for (int j = 0; j < associatedGenomePanel
															.getGenomePanelItem()
															.getFullAssociatedlistAbsoluteProportionItemToDisplay()
															.get(i).getListHAPI()
															.size(); j++) {
														HolderAbsoluteProportionItem hapiIT = associatedGenomePanel
																.getGenomePanelItem()
																.getFullAssociatedlistAbsoluteProportionItemToDisplay()
																.get(i)
																.getListHAPI()
																.get(j);
														if (hapiIT
																.getAbsoluteProportionSComparableSSpanItem() != null) {
															if (hapiIT
																	.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
																if (((AbsoPropSGenoRegiInserItem) hapiIT
																		.getAbsoluteProportionSComparableSSpanItem())
																		.getSEnumBlockType()
																		.toString()
																		.startsWith(
																				"S_CUT_STOP_")) {
																	if (((AbsoPropSGenoRegiInserItem) apscssiIT)
																			.getsPercentStart() == ((AbsoPropSGenoRegiInserItem) hapiIT
																			.getAbsoluteProportionSComparableSSpanItem())
																			.getsPercentStart()
																			&& ((AbsoPropSGenoRegiInserItem) apscssiIT)
																					.getsPercentStop() == ((AbsoPropSGenoRegiInserItem) hapiIT
																					.getAbsoluteProportionSComparableSSpanItem())
																					.getsPercentStop()
																	// &&
																	// ((AbsoluteProportionQGenomicRegionInsertionItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStart()
																	// ==
																	// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStart()
																	// &&
																	// ((AbsoluteProportionQGenomicRegionInsertionItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStop()
																	// ==
																	// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStop()
																	) {
																		// found cut
																		// stop
																		stopIndexInFullArrayIT = i;
																		break BUILDMENUCONTEXTUELFINDCUTSTOP;
																	}
																}
															}
														}
													}
												}
												if (startIndexInFullArrayIT < 0
														|| stopIndexInFullArrayIT < 0) {
													//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper part cut start getAbsoluteProportionSComparableSSpanItem startIndexInArrayIT < 0 || stopIndexInArrayIT < 0");
													Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp clicked " +
															"upper part cut start getAbsoluteProportionSComparableSSpanItem startIndexInArrayIT < 0 || stopIndexInArrayIT < 0"));
													return false;
												} else {
													MainTabPanelView.CANVAS_MENU_POPUP
															.addHideGenesInSyntenyRegion(
																	startIndexInFullArrayIT,
																	stopIndexInFullArrayIT,
																	displayIndexStart,
																	true);
													// return true;
												}

											} else if (((AbsoPropSGenoRegiInserItem) apscssiIT)
													.getSEnumBlockType().toString()
													.startsWith("S_CUT_STOP_")) {
												stopIndexInFullArrayIT = siIT
														.getIndexInFullArray(); // indexStartBirdSynteny+selectedSliceX;
												// find start now
												// startIndexInFullArrayIT =
												// ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getIdxRefOtherPartForQSCutStartOrStopTypes();
												BUILDMENUCONTEXTUELFINDCUTSTART: for (int i = stopIndexInFullArrayIT; i >= 0; i--) {
													for (int j = 0; j < associatedGenomePanel
															.getGenomePanelItem()
															.getFullAssociatedlistAbsoluteProportionItemToDisplay()
															.get(i).getListHAPI()
															.size(); j++) {
														HolderAbsoluteProportionItem hapiIT = associatedGenomePanel
																.getGenomePanelItem()
																.getFullAssociatedlistAbsoluteProportionItemToDisplay()
																.get(i)
																.getListHAPI()
																.get(j);
														if (hapiIT
																.getAbsoluteProportionSComparableSSpanItem() != null) {
															if (hapiIT
																	.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem) {
																if (((AbsoPropSGenoRegiInserItem) hapiIT
																		.getAbsoluteProportionSComparableSSpanItem())
																		.getSEnumBlockType()
																		.toString()
																		.startsWith(
																				"S_CUT_START_")) {
																	if (((AbsoPropSGenoRegiInserItem) apscssiIT)
																			.getsPercentStart() == ((AbsoPropSGenoRegiInserItem) hapiIT
																			.getAbsoluteProportionSComparableSSpanItem())
																			.getsPercentStart()
																			&& ((AbsoPropSGenoRegiInserItem) apscssiIT)
																					.getsPercentStop() == ((AbsoPropSGenoRegiInserItem) hapiIT
																					.getAbsoluteProportionSComparableSSpanItem())
																					.getsPercentStop()
																	// &&
																	// ((AbsoluteProportionQGenomicRegionInsertionItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStart()
																	// ==
																	// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStart()
																	// &&
																	// ((AbsoluteProportionQGenomicRegionInsertionItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStop()
																	// ==
																	// ((AbsoluteProportionQGenomicRegionInsertionItem)hapiIT.getAbsoluteProportionQComparableQSpanItem()).getsPercentStop()
																	) {
																		// found cut
																		// start
																		startIndexInFullArrayIT = i;
																		break BUILDMENUCONTEXTUELFINDCUTSTART;
																	}
																}
															}
														}
													}
												}
												if (startIndexInFullArrayIT < 0
														|| stopIndexInFullArrayIT < 0) {
													//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper part cut start getAbsoluteProportionSComparableSSpanItem startIndexInArrayIT < 0 || stopIndexInArrayIT < 0");
													Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp clicked upper" +
															" part cut start getAbsoluteProportionSComparableSSpanItem startIndexInArrayIT < 0 || stopIndexInArrayIT < 0"));
													return false;
												} else {
													displayIndexStart = (associatedGenomePanel
															.getGenomePanelItem()
															.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed())
															- (stopIndexInFullArrayIT - startIndexInFullArrayIT);
													if (displayIndexStart < 0) {
														displayIndexStart = 0;
													}
													MainTabPanelView.CANVAS_MENU_POPUP
															.addHideGenesInSyntenyRegion(
																	startIndexInFullArrayIT,
																	stopIndexInFullArrayIT,
																	displayIndexStart,
																	true);
													// return true;
												}

											} else {
												//System.err.println("ERROR in qs buildMenuContextuelInPopUp clicked upper part getSEnumBlockType not start or stop");
												Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs buildMenuContextuelInPopUp clicked upper part " +
														"getSEnumBlockType not start or stop"));
												return false;
											}
											// return false;
										} else {
											// show gene
											MainTabPanelView.CANVAS_MENU_POPUP
													.addShowGenesInSGenomicRegionInsertion(
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsPbStartSGenomicRegionInsertionInElement(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsPbStopSGenomicRegionInsertionInElement(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsOrigamiElementId(),
															// (associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+selectedSliceX),
															siIT.getIndexInFullArray(),
															Insyght.APP_CONTROLER
																	.getLIST_GENOME_PANEL()
																	.indexOf(
																			associatedGenomePanel),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsAccnum(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsPbStartOfElementInOrga(),
															((AbsoPropSGenoRegiInserItem) apscssiIT)
																	.getsSizeOfOragnismInPb(),
															previousSOfNextSlice,
															nextSOfPreviousSlice);
											// return true;
										}
									} else {
										// return false;
									}

									// }

								} else if (apscssiIT instanceof AbsoPropSElemItem) {
									// do nothing for now
									// return false;
								} else {
									//System.err.println("ERROR in s buildMenuContextuelInPopUp clicked upper partunrecognized class for object sent");
									Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in s buildMenuContextuelInPopUp clicked upper partunrecognized " +
											"class for object sent"));
									return false;
								}
							}
						}
					}
				}
				
			}
		}

		return true;

	}

	private double checkIfWithinBorderMarkerScaffoldAera(double doubleToCheck) {
		if (doubleToCheck < SCAFFOLD_X_START_POINT + 4) {
			return SCAFFOLD_X_START_POINT + 4;
		} else if (doubleToCheck > SCAFFOLD_X_STOP_POINT - 3) {
			return SCAFFOLD_X_STOP_POINT - 3;
		} else {
			return doubleToCheck;
		}
	}

	// private void drawPathMarkerScaffoldQ(double qPercentStart, double
	// qPercentStop, boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART){
	private void drawPathSyntenyMarkerScaffoldQ(double qPercent,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART, boolean enlargeABit) {

		if (qPercent < 0) {
			qPercent = 0;
		}
		if (qPercent > 1) {
			qPercent = 1;
		}

		int toAddToYAxis = -1;
		if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
			toAddToYAxis = TOTAL_CANVAS_HEIGHT_UPPER_PART;
		} else {
			toAddToYAxis = 0;
		}

		int correctPointTriangle = -1;
		int widthTriangle = -1;
		if (enlargeABit) {
			correctPointTriangle = 1;
			widthTriangle = (WIDTH_SIZE_PB_TEXT / 4) + 4;// UNIT_WIDTH/2
		} else {
			correctPointTriangle = 3;
			widthTriangle = WIDTH_SIZE_PB_TEXT / 4;// UNIT_WIDTH/2
		}

		contextBgk.beginPath();
		double xTopPointTriangleCenterInit = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * qPercent) + 0.5;
		double xTopPointTriangleCenter = checkIfWithinBorderMarkerScaffoldAera(xTopPointTriangleCenterInit);
		contextBgk.moveTo(xTopPointTriangleCenter, toAddToYAxis
				+ Y_AXIS_TO_DRAW_AT_TOP - correctPointTriangle + 0.5);
		double xTopPointTriangleRight = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * qPercent) + widthTriangle + 0.5;
		if (xTopPointTriangleRight >= SCAFFOLD_X_STOP_POINT - 2) {
			xTopPointTriangleRight = xTopPointTriangleCenter;
		}
		contextBgk.lineTo(xTopPointTriangleRight, toAddToYAxis
				+ Y_AXIS_TO_DRAW_AT_TOP - widthTriangle + 0.5);
		double xTopPointTriangleLeft = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * qPercent) - widthTriangle + 0.5;
		if (xTopPointTriangleLeft < SCAFFOLD_X_START_POINT + 3) {
			xTopPointTriangleLeft = xTopPointTriangleCenter;
		}
		contextBgk.lineTo(xTopPointTriangleLeft, toAddToYAxis
				+ Y_AXIS_TO_DRAW_AT_TOP - widthTriangle + 0.5);
		contextBgk.closePath();

	}

	// private void drawPathMarkerScaffoldS(double sPercentStart, double
	// sPercentStop, boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART){
	private void drawPathSyntenyMarkerScaffoldS(double sPercent,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART, boolean enlargeABit) {

		
		if (sPercent < 0) {
			sPercent = 0;
		}
		if (sPercent > 1) {
			sPercent = 1;
		}

		int toAddToYAxis = -1;
		if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
			toAddToYAxis = TOTAL_CANVAS_HEIGHT_UPPER_PART;
		} else {
			toAddToYAxis = 0;
		}

		int correctPointTriangle = -1;
		int widthTriangle = -1;
		if (enlargeABit) {
			correctPointTriangle = 1;
			widthTriangle = (WIDTH_SIZE_PB_TEXT / 4) + 4;
		} else {
			correctPointTriangle = 3;
			widthTriangle = WIDTH_SIZE_PB_TEXT / 4;
		}

		contextBgk.beginPath();
		double xBottomPointTriangleCenterInit = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * sPercent) + 0.5;
		double xBottomPointTriangleCenter = checkIfWithinBorderMarkerScaffoldAera(xBottomPointTriangleCenterInit);

		contextBgk.moveTo(xBottomPointTriangleCenter, toAddToYAxis
				+ Y_AXIS_TO_DRAW_AT_BOTTOM + correctPointTriangle + 0.5);
		double xBottomPointTriangleRight = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * sPercent) + widthTriangle + 0.5;
		if (xBottomPointTriangleRight >= SCAFFOLD_X_STOP_POINT - 2) {
			xBottomPointTriangleRight = xBottomPointTriangleCenter;
		}
		contextBgk.lineTo(xBottomPointTriangleRight, toAddToYAxis
				+ Y_AXIS_TO_DRAW_AT_BOTTOM + widthTriangle + 0.5);
		double xBottomTriangleLeft = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * sPercent) - widthTriangle + 0.5;
		if (xBottomTriangleLeft <= SCAFFOLD_X_START_POINT + 2) {
			xBottomTriangleLeft = xBottomPointTriangleCenter;
		}
		contextBgk.lineTo(xBottomTriangleLeft, toAddToYAxis
				+ Y_AXIS_TO_DRAW_AT_BOTTOM + widthTriangle + 0.5);
		contextBgk.closePath();

	}

	public int checkIfSyntenyMarkerScaffoldIsInPath(boolean checkQ,
			boolean checkS, double qPercentStart, double qPercentStop,
			double sPercentStart, double sPercentStop) {

		// context.setFillStyle(redrawColorBlue);

		if (checkQ) {
			// drawPathMarkerScaffoldQ(qPercentStart, qPercentStop, false);

			// System.err.println("mouseX="+mouseX);
			// System.err.println("mouseY-TOTAL_CANVAS_HEIGHT_UPPER_PART="+(mouseY-TOTAL_CANVAS_HEIGHT_UPPER_PART));
			drawPathSyntenyMarkerScaffoldQ(
					(qPercentStart + (qPercentStop - qPercentStart) / 2),
					false, true);
			// contextBgk.fill();
			if (contextBgk.isPointInPath(mouseX, mouseY)) {// (mouseY-TOTAL_CANVAS_HEIGHT_UPPER_PART)
				// clickedQPart = true;
				return 1;
			}

		}

		if (checkS) {
			// drawPathMarkerScaffoldS(sPercentStart, sPercentStop, false);
			drawPathSyntenyMarkerScaffoldS(
					(sPercentStart + (sPercentStop - sPercentStart) / 2),
					false, true);
			if (contextBgk.isPointInPath(mouseX, mouseY)) {// was
															// (mouseY-TOTAL_CANVAS_HEIGHT_UPPER_PART)
				// clickedQPart = false;
				return -1;
			}
			// context.fill();
		}

		return 0;
	}

	public void highlightPropRepreForAllDisplayedSlices(boolean inBlack,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART) {

		if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
			contextBgk.translate(0, TOTAL_CANVAS_HEIGHT_UPPER_PART);
		}

		for (int i = 0; i < NUMBER_SLICE_WIDTH; i++) {
			if (associatedGenomePanel.getGenomePanelItem()
					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
					+ i >= associatedGenomePanel
					.getGenomePanelItem()
					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
					.size()) {
				break;
			}

			
//			HolderAbsoluteProportionItem siIT =
//					associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexStartBirdSynteny+i).get(associatedGenomePanel.getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(indexStartBirdSynteny+i));
			HolderAbsoluteProportionItem siIT = associatedGenomePanel
					.getGenomePanelItem()
					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
					.get(associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
							+ i).getListHAPI()
					.get(associatedGenomePanel.getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(associatedGenomePanel.getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+i));


				if (siIT.getAbsoluteProportionQComparableQSSpanItem() != null) {

					double qPercentStartIT = claculateRelativePercentage(
							Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getReferenceGenomePanelItem()
									.getListAbsoluteProportionElementItemQ()
									.get(0).getQsizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSSpanItem()
									.getqPercentStart(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopQGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartQGenome());
					double qPercentStopIT = claculateRelativePercentage(
							Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getReferenceGenomePanelItem()
									.getListAbsoluteProportionElementItemQ()
									.get(0).getQsizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSSpanItem()
									.getqPercentStop(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopQGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartQGenome());
					double sPercentStartIT = claculateRelativePercentage(
							associatedGenomePanel.getGenomePanelItem()
									.getListAbsoluteProportionElementItemS()
									.get(0).getsSizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSSpanItem()
									.getsPercentStart(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopSGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartSGenome());
					double sPercentStopIT = claculateRelativePercentage(
							associatedGenomePanel.getGenomePanelItem()
									.getListAbsoluteProportionElementItemS()
									.get(0).getsSizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSSpanItem()
									.getsPercentStop(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopSGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartSGenome());

					if (qPercentStartIT >= 0 && qPercentStartIT <= 1
							&& qPercentStopIT >= 0 && qPercentStopIT <= 1
							&& sPercentStartIT >= 0 && sPercentStartIT <= 1
							&& sPercentStopIT >= 0 && sPercentStopIT <= 1) {
						// System.err.println("here");
						drawSyntenyRepersentationProportionateQS(
								siIT.getAbsoluteProportionQComparableQSSpanItem(),
								qPercentStartIT, qPercentStopIT,
								sPercentStartIT, sPercentStopIT, true, false,
								inBlack);

					}

				} else {
					if (siIT.getAbsoluteProportionQComparableQSpanItem() != null) {

						double qPercentStartIT = claculateRelativePercentage(
								Insyght.APP_CONTROLER
										.getCurrentRefGenomePanelAndListOrgaResult()
										.getReferenceGenomePanelItem()
										.getListAbsoluteProportionElementItemQ()
										.get(0).getQsizeOfOragnismInPb(),
								siIT.getAbsoluteProportionQComparableQSpanItem()
										.getqPercentStart(),
								associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStopQGenome(),
								associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStartQGenome());
						double qPercentStopIT = claculateRelativePercentage(
								Insyght.APP_CONTROLER
										.getCurrentRefGenomePanelAndListOrgaResult()
										.getReferenceGenomePanelItem()
										.getListAbsoluteProportionElementItemQ()
										.get(0).getQsizeOfOragnismInPb(),
								siIT.getAbsoluteProportionQComparableQSpanItem()
										.getqPercentStop(),
								associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStopQGenome(),
								associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStartQGenome());

						if (qPercentStartIT >= 0 && qPercentStartIT <= 1
								&& qPercentStopIT >= 0 && qPercentStopIT <= 1) {

							drawGenomicRegionRepersentationProportionateQ(
									siIT.getAbsoluteProportionQComparableQSpanItem(),
									qPercentStartIT, qPercentStopIT, true,
									false, inBlack);

						}

					}
					if (siIT.getAbsoluteProportionSComparableSSpanItem() != null) {

						double sPercentStartIT = claculateRelativePercentage(
								associatedGenomePanel
										.getGenomePanelItem()
										.getListAbsoluteProportionElementItemS()
										.get(0).getsSizeOfOragnismInPb(),
								siIT.getAbsoluteProportionSComparableSSpanItem()
										.getsPercentStart(),
								associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStopSGenome(),
								associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStartSGenome());
						double sPercentStopIT = claculateRelativePercentage(
								associatedGenomePanel
										.getGenomePanelItem()
										.getListAbsoluteProportionElementItemS()
										.get(0).getsSizeOfOragnismInPb(),
								siIT.getAbsoluteProportionSComparableSSpanItem()
										.getsPercentStop(),
								associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStopSGenome(),
								associatedGenomePanel.getGenomePanelItem()
										.getPercentSyntenyShowStartSGenome());

						if (sPercentStartIT >= 0 && sPercentStartIT <= 1
								&& sPercentStopIT >= 0 && sPercentStopIT <= 1) {

							drawGenomicRegionRepersentationProportionateS(
									siIT.getAbsoluteProportionSComparableSSpanItem(),
									sPercentStartIT, sPercentStopIT, true,
									false, inBlack);

						}
					} else {

						int idxPrevious = associatedGenomePanel
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
								+ i - 1;
						if (idxPrevious >= 0
								&& idxPrevious < associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.size()) {
							HolderAbsoluteProportionItem siITPrevious = associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(idxPrevious)
									.getListHAPI()
									.get(associatedGenomePanel
											.getGenomePanelItem()
											.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
													idxPrevious));
							if (siITPrevious != null) {
								if (siITPrevious
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (siITPrevious
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem
											|| siITPrevious
													.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										// draw s previous et next ?

										AbsoPropSGenoRegiInserItem apsgriITNext = ((AbsoPropQSSyntItem) siITPrevious
												.getAbsoluteProportionQComparableQSSpanItem())
												.getNextGenomicRegionSInsertion();
										if (apsgriITNext != null) {
											double sPercentStartIT = claculateRelativePercentage(
													associatedGenomePanel
															.getGenomePanelItem()
															.getListAbsoluteProportionElementItemS()
															.get(0)
															.getsSizeOfOragnismInPb(),
													apsgriITNext
															.getsPercentStart(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopSGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartSGenome());
											double sPercentStopIT = claculateRelativePercentage(
													associatedGenomePanel
															.getGenomePanelItem()
															.getListAbsoluteProportionElementItemS()
															.get(0)
															.getsSizeOfOragnismInPb(),
													apsgriITNext
															.getsPercentStop(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopSGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartSGenome());
											if (sPercentStartIT >= 0
													&& sPercentStartIT <= 1
													&& sPercentStopIT >= 0
													&& sPercentStopIT <= 1) {

												drawGenomicRegionRepersentationProportionateS(
														apsgriITNext,
														sPercentStartIT,
														sPercentStopIT, true,
														false, inBlack);

											}
										}
									}
								}
							}

						}

						int idxNext = associatedGenomePanel
								.getGenomePanelItem()
								.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
								+ i + 1;
						if (idxNext >= 0
								&& idxNext < associatedGenomePanel
										.getGenomePanelItem()
										.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
										.size()) {
							HolderAbsoluteProportionItem siITNext = associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.get(idxNext)
									.getListHAPI()
									.get(associatedGenomePanel
											.getGenomePanelItem()
											.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
													idxNext));
							if (siITNext != null) {
								if (siITNext
										.getAbsoluteProportionQComparableQSSpanItem() != null) {
									if (siITNext
											.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem
											|| siITNext
													.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
										// draw s previous et next ?

										AbsoPropSGenoRegiInserItem apsgriITPrevious = ((AbsoPropQSSyntItem) siITNext
												.getAbsoluteProportionQComparableQSSpanItem())
												.getPreviousGenomicRegionSInsertion();
										if (apsgriITPrevious != null) {
											double sPercentStartIT = claculateRelativePercentage(
													associatedGenomePanel
															.getGenomePanelItem()
															.getListAbsoluteProportionElementItemS()
															.get(0)
															.getsSizeOfOragnismInPb(),
													apsgriITPrevious
															.getsPercentStart(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopSGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartSGenome());
											double sPercentStopIT = claculateRelativePercentage(
													associatedGenomePanel
															.getGenomePanelItem()
															.getListAbsoluteProportionElementItemS()
															.get(0)
															.getsSizeOfOragnismInPb(),
													apsgriITPrevious
															.getsPercentStop(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStopSGenome(),
													associatedGenomePanel
															.getGenomePanelItem()
															.getPercentSyntenyShowStartSGenome());
											if (sPercentStartIT >= 0
													&& sPercentStartIT <= 1
													&& sPercentStopIT >= 0
													&& sPercentStopIT <= 1) {

												drawGenomicRegionRepersentationProportionateS(
														apsgriITPrevious,
														sPercentStartIT,
														sPercentStopIT, true,
														false, inBlack);

											}
										}
									}
								}
							}
						}
					}
				}
			//}
		}

		if (!inBlack) {
			drawScaffoldGenome(true, 0, 1, true, false, false);
			drawScaffoldGenome(false, 0, 1, true, false, false);
		}

		if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
			restoreToOriginCoordinateSpace();
		}

	}

	public void drawSyntenyMarkerScaffoldAllDisplayedSlices(
			boolean bottomRedrawOnTop, boolean inRed,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART
	// ,boolean doHighlightProportionateRepresentation, boolean
	// doDrawMarkerScaffold
	) {

		for (int i = 0; i < NUMBER_SLICE_WIDTH; i++) {
			if (associatedGenomePanel.getGenomePanelItem()
					.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
					+ i >= associatedGenomePanel
					.getGenomePanelItem()
					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
					.size()) {
				break;
			}

			// To draw only shown off root
			HolderAbsoluteProportionItem siIT = associatedGenomePanel
					.getGenomePanelItem()
					.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
					.get(associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
							+ i)
					.getListHAPI()
					.get(associatedGenomePanel
							.getGenomePanelItem()
							.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
									associatedGenomePanel
											.getGenomePanelItem()
											.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
											+ i));
			

			if (siIT.getAbsoluteProportionQComparableQSSpanItem() != null) {
				double qPercentStartIT = claculateRelativePercentage(
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getReferenceGenomePanelItem()
								.getListAbsoluteProportionElementItemQ().get(0)
								.getQsizeOfOragnismInPb(), siIT
								.getAbsoluteProportionQComparableQSSpanItem()
								.getqPercentStart(), associatedGenomePanel
								.getGenomePanelItem()
								.getPercentSyntenyShowStopQGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartQGenome());
				double qPercentStopIT = claculateRelativePercentage(
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getReferenceGenomePanelItem()
								.getListAbsoluteProportionElementItemQ().get(0)
								.getQsizeOfOragnismInPb(), siIT
								.getAbsoluteProportionQComparableQSSpanItem()
								.getqPercentStop(), associatedGenomePanel
								.getGenomePanelItem()
								.getPercentSyntenyShowStopQGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartQGenome());
				double sPercentStartIT = claculateRelativePercentage(
						associatedGenomePanel.getGenomePanelItem()
								.getListAbsoluteProportionElementItemS().get(0)
								.getsSizeOfOragnismInPb(), siIT
								.getAbsoluteProportionQComparableQSSpanItem()
								.getsPercentStart(), associatedGenomePanel
								.getGenomePanelItem()
								.getPercentSyntenyShowStopSGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartSGenome());
				double sPercentStopIT = claculateRelativePercentage(
						associatedGenomePanel.getGenomePanelItem()
								.getListAbsoluteProportionElementItemS().get(0)
								.getsSizeOfOragnismInPb(), siIT
								.getAbsoluteProportionQComparableQSSpanItem()
								.getsPercentStop(), associatedGenomePanel
								.getGenomePanelItem()
								.getPercentSyntenyShowStopSGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartSGenome());

				String colorBgkIT = "";
				if (siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
					colorBgkIT = getBkgColorAssociatedWithColorIDAsString(((AbsoPropQSGeneHomoItem) siIT
							.getAbsoluteProportionQComparableQSSpanItem())
							.getQsStyleItem().getBkgColor());
				} else {
					colorBgkIT = getDefaultBkgColorAccordingToQSBlockType(siIT
							.getAbsoluteProportionQComparableQSSpanItem()
							.getQsEnumBlockType());
				}

				if (qPercentStartIT >= 0 && qPercentStartIT <= 1
						&& qPercentStopIT >= 0 && qPercentStopIT <= 1
						&& sPercentStartIT >= 0 && sPercentStartIT <= 1
						&& sPercentStopIT >= 0 && sPercentStopIT <= 1) {

					// if(doDrawMarkerScaffold){
					drawSyntenyMarkerScaffold(true, true, qPercentStartIT,
							qPercentStopIT, sPercentStartIT, sPercentStopIT,
							bottomRedrawOnTop, inRed,
							addTOTAL_CANVAS_HEIGHT_UPPER_PART, colorBgkIT);
					// }

				}
				// }
				// }

			} else {
				if (siIT.getAbsoluteProportionQComparableQSpanItem() != null) {

					double qPercentStartIT = claculateRelativePercentage(
							Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getReferenceGenomePanelItem()
									.getListAbsoluteProportionElementItemQ()
									.get(0).getQsizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSpanItem()
									.getqPercentStart(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopQGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartQGenome());
					double qPercentStopIT = claculateRelativePercentage(
							Insyght.APP_CONTROLER
									.getCurrentRefGenomePanelAndListOrgaResult()
									.getReferenceGenomePanelItem()
									.getListAbsoluteProportionElementItemQ()
									.get(0).getQsizeOfOragnismInPb(),
							siIT.getAbsoluteProportionQComparableQSpanItem()
									.getqPercentStop(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopQGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartQGenome());

					String colorBgkIT = "";
					if (siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem) {
						colorBgkIT = getBkgColorAssociatedWithColorIDAsString(((AbsoPropQGeneInserItem) siIT
								.getAbsoluteProportionQComparableQSpanItem())
								.getqStyleItem().getBkgColor());
					} else {
						colorBgkIT = getDefaultBkgColorAccordingToQBlockType(siIT
								.getAbsoluteProportionQComparableQSpanItem()
								.getqEnumBlockType());
					}

					if (qPercentStartIT >= 0 && qPercentStartIT <= 1
							&& qPercentStopIT >= 0 && qPercentStopIT <= 1) {

						// if(doDrawMarkerScaffold){
						drawSyntenyMarkerScaffold(true, false, qPercentStartIT,
								qPercentStopIT, -1, -1, bottomRedrawOnTop,
								inRed, addTOTAL_CANVAS_HEIGHT_UPPER_PART,
								colorBgkIT);
						// }

					}

				}
				if (siIT.getAbsoluteProportionSComparableSSpanItem() != null) {

					double sPercentStartIT = claculateRelativePercentage(
							associatedGenomePanel.getGenomePanelItem()
									.getListAbsoluteProportionElementItemS()
									.get(0).getsSizeOfOragnismInPb(),
							siIT.getAbsoluteProportionSComparableSSpanItem()
									.getsPercentStart(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopSGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartSGenome());
					double sPercentStopIT = claculateRelativePercentage(
							associatedGenomePanel.getGenomePanelItem()
									.getListAbsoluteProportionElementItemS()
									.get(0).getsSizeOfOragnismInPb(),
							siIT.getAbsoluteProportionSComparableSSpanItem()
									.getsPercentStop(), associatedGenomePanel
									.getGenomePanelItem()
									.getPercentSyntenyShowStopSGenome(),
							associatedGenomePanel.getGenomePanelItem()
									.getPercentSyntenyShowStartSGenome());

					if (sPercentStartIT >= 0 && sPercentStartIT <= 1
							&& sPercentStopIT >= 0 && sPercentStopIT <= 1) {


						// if(doDrawMarkerScaffold){
						String colorBgkIT = "";
						if (siIT.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem) {
							colorBgkIT = getBkgColorAssociatedWithColorIDAsString(((AbsoPropSGeneInserItem) siIT
									.getAbsoluteProportionSComparableSSpanItem())
									.getsStyleItem().getBkgColor());
						} else {
							colorBgkIT = getDefaultBkgColorAccordingToSBlockType(siIT
									.getAbsoluteProportionSComparableSSpanItem()
									.getSEnumBlockType());
						}

						drawSyntenyMarkerScaffold(false, true, -1, -1,
								sPercentStartIT, sPercentStopIT,
								bottomRedrawOnTop, inRed,
								addTOTAL_CANVAS_HEIGHT_UPPER_PART, colorBgkIT);

					}
				} else {

					int idxPrevious = associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
							+ i - 1;
					if (idxPrevious >= 0
							&& idxPrevious < associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.size()) {
						HolderAbsoluteProportionItem siITPrevious = associatedGenomePanel
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.get(idxPrevious)
								.getListHAPI()
								.get(associatedGenomePanel
										.getGenomePanelItem()
										.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
												idxPrevious));
						if (siITPrevious != null) {
							if (siITPrevious
									.getAbsoluteProportionQComparableQSSpanItem() != null) {
								if (siITPrevious
										.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem
										|| siITPrevious
												.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
									// draw s previous et next ?

									AbsoPropSGenoRegiInserItem apsgriITNext = ((AbsoPropQSSyntItem) siITPrevious
											.getAbsoluteProportionQComparableQSSpanItem())
											.getNextGenomicRegionSInsertion();
									if (apsgriITNext != null) {
										double sPercentStartIT = claculateRelativePercentage(
												associatedGenomePanel
														.getGenomePanelItem()
														.getListAbsoluteProportionElementItemS()
														.get(0)
														.getsSizeOfOragnismInPb(),
												apsgriITNext.getsPercentStart(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome());
										double sPercentStopIT = claculateRelativePercentage(
												associatedGenomePanel
														.getGenomePanelItem()
														.getListAbsoluteProportionElementItemS()
														.get(0)
														.getsSizeOfOragnismInPb(),
												apsgriITNext.getsPercentStop(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome());
										if (sPercentStartIT >= 0
												&& sPercentStartIT <= 1
												&& sPercentStopIT >= 0
												&& sPercentStopIT <= 1) {

											// if(doHighlightProportionateRepresentation){
											// drawGenomicRegionRepersentationProportionateS(apsgriITNext,
											// sPercentStartIT,
											// sPercentStopIT,
											// bottomRedrawOnTop, inRed,
											// true);
											// }

											// if(doDrawMarkerScaffold){
											drawSyntenyMarkerScaffold(
													false,
													true,
													-1,
													-1,
													sPercentStartIT,
													sPercentStopIT,
													bottomRedrawOnTop,
													inRed,
													addTOTAL_CANVAS_HEIGHT_UPPER_PART,
													getDefaultBkgColorAccordingToSBlockType(apsgriITNext
															.getSEnumBlockType()));
											// }

										}
									}
								}
							}
						}

					}

					int idxNext = associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()
							+ i + 1;
					if (idxNext >= 0
							&& idxNext < associatedGenomePanel
									.getGenomePanelItem()
									.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
									.size()) {
						HolderAbsoluteProportionItem siITNext = associatedGenomePanel
								.getGenomePanelItem()
								.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
								.get(idxNext)
								.getListHAPI()
								.get(associatedGenomePanel
										.getGenomePanelItem()
										.getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(
												idxNext));
						if (siITNext != null) {
							if (siITNext
									.getAbsoluteProportionQComparableQSSpanItem() != null) {
								if (siITNext
										.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem
										|| siITNext
												.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem) {
									// draw s previous et next ?

									AbsoPropSGenoRegiInserItem apsgriITPrevious = ((AbsoPropQSSyntItem) siITNext
											.getAbsoluteProportionQComparableQSSpanItem())
											.getPreviousGenomicRegionSInsertion();
									if (apsgriITPrevious != null) {
										double sPercentStartIT = claculateRelativePercentage(
												associatedGenomePanel
														.getGenomePanelItem()
														.getListAbsoluteProportionElementItemS()
														.get(0)
														.getsSizeOfOragnismInPb(),
												apsgriITPrevious
														.getsPercentStart(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome());
										double sPercentStopIT = claculateRelativePercentage(
												associatedGenomePanel
														.getGenomePanelItem()
														.getListAbsoluteProportionElementItemS()
														.get(0)
														.getsSizeOfOragnismInPb(),
												apsgriITPrevious
														.getsPercentStop(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStopSGenome(),
												associatedGenomePanel
														.getGenomePanelItem()
														.getPercentSyntenyShowStartSGenome());
										if (sPercentStartIT >= 0
												&& sPercentStartIT <= 1
												&& sPercentStopIT >= 0
												&& sPercentStopIT <= 1) {

											// if(doHighlightProportionateRepresentation){
											// drawGenomicRegionRepersentationProportionateS(apsgriITPrevious,
											// sPercentStartIT,
											// sPercentStopIT,
											// bottomRedrawOnTop, inRed,
											// true);
											// }

											// if(doDrawMarkerScaffold){
											drawSyntenyMarkerScaffold(
													false,
													true,
													-1,
													-1,
													sPercentStartIT,
													sPercentStopIT,
													bottomRedrawOnTop,
													inRed,
													addTOTAL_CANVAS_HEIGHT_UPPER_PART,
													getDefaultBkgColorAccordingToSBlockType(apsgriITPrevious
															.getSEnumBlockType()));
											// }

										}
									}
								}
							}
						}
					}
				}
			}
			// }//boucle for each hidden off root
		}
	}

	private void drawElementMarkerScaffold(boolean q,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART) {

		contextBgk.setStrokeStyle(redrawColorGrey6);
		contextBgk.setLineWidth(2);
		int toAddToYAxis = -1;
		if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
			toAddToYAxis = TOTAL_CANVAS_HEIGHT_UPPER_PART;
		} else {
			toAddToYAxis = 0;
		}
		int correctPointTriangle = 1;
		int widthHeightTrait = (UNIT_WIDTH / 2) + 2;

		if (q) {
			// draw Q element marker
			for (int i = 0; i < associatedGenomePanel.getGenomePanelItem()
					.getListAbsoluteProportionElementItemQ().size(); i++) {
				AbsoPropQElemItem qapei = associatedGenomePanel
						.getGenomePanelItem()
						.getListAbsoluteProportionElementItemQ().get(i);

				double xPointStopPercent = claculateRelativePercentage(
						Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getReferenceGenomePanelItem()
								.getListAbsoluteProportionElementItemQ().get(0)
								.getQsizeOfOragnismInPb(),
						qapei.getqPercentStop(), associatedGenomePanel
								.getGenomePanelItem()
								.getPercentSyntenyShowStopQGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartQGenome());
				double xPointStop = SCAFFOLD_X_START_POINT
						+ (TOTAL_LENGHT_SCAFFOLD * xPointStopPercent) + 0.5;

				if (xPointStop <= SCAFFOLD_X_START_POINT + 4) {
					continue;
				} else if (xPointStop >= SCAFFOLD_X_STOP_POINT - 4) {
					continue;
				}
				// draw stop sign
				contextBgk.beginPath();
				contextBgk.moveTo(xPointStop, toAddToYAxis
						+ Y_AXIS_TO_DRAW_AT_TOP - correctPointTriangle + 0.5);
				contextBgk.lineTo(xPointStop, toAddToYAxis
						+ Y_AXIS_TO_DRAW_AT_TOP - widthHeightTrait + 0.5);
				contextBgk.moveTo(xPointStop - (widthHeightTrait / 2),
						toAddToYAxis + Y_AXIS_TO_DRAW_AT_TOP - widthHeightTrait
								+ 0.5);
				contextBgk.lineTo(xPointStop + (widthHeightTrait / 2),
						toAddToYAxis + Y_AXIS_TO_DRAW_AT_TOP - widthHeightTrait
								+ 0.5);
				contextBgk.stroke();
			}
		} else {
			// draw S element marker
			for (int i = 0; i < associatedGenomePanel.getGenomePanelItem()
					.getListAbsoluteProportionElementItemS().size(); i++) {
				AbsoPropSElemItem sapei = associatedGenomePanel
						.getGenomePanelItem()
						.getListAbsoluteProportionElementItemS().get(i);
				double xPointStopPercent = claculateRelativePercentage(
						associatedGenomePanel.getGenomePanelItem()
								.getListAbsoluteProportionElementItemS().get(0)
								.getsSizeOfOragnismInPb(),
						sapei.getsPercentStop(), associatedGenomePanel
								.getGenomePanelItem()
								.getPercentSyntenyShowStopSGenome(),
						associatedGenomePanel.getGenomePanelItem()
								.getPercentSyntenyShowStartSGenome());
				double xPointStop = SCAFFOLD_X_START_POINT
						+ (TOTAL_LENGHT_SCAFFOLD * xPointStopPercent) + 0.5;
				if (xPointStop <= SCAFFOLD_X_START_POINT + 4) {
					continue;
				} else if (xPointStop >= SCAFFOLD_X_STOP_POINT - 4) {
					continue;
				}

				// draw stop sign
				contextBgk.beginPath();
				contextBgk
						.moveTo(xPointStop, toAddToYAxis
								+ Y_AXIS_TO_DRAW_AT_BOTTOM
								+ correctPointTriangle + 0.5);
				contextBgk.lineTo(xPointStop, toAddToYAxis
						+ Y_AXIS_TO_DRAW_AT_BOTTOM + widthHeightTrait + 0.5);
				contextBgk.moveTo(xPointStop - (widthHeightTrait / 2),
						toAddToYAxis + Y_AXIS_TO_DRAW_AT_BOTTOM
								+ widthHeightTrait + 0.5);
				contextBgk.lineTo(xPointStop + (widthHeightTrait / 2),
						toAddToYAxis + Y_AXIS_TO_DRAW_AT_BOTTOM
								+ widthHeightTrait + 0.5);
				contextBgk.stroke();
			}
		}
		contextBgk.setStrokeStyle(redrawColorBlack);
		contextBgk.setLineWidth(1);

	}

	// public void
	// drawSyntenyMarkerScaffold(AbsoluteProportionSyntenyItem.SyntenyEnumBlockType
	// blockType, double qPercentStart, double qPercentStop, double
	// sPercentStart, double sPercentStop, boolean redrawOnTop, boolean inRed,
	// boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
	public void drawSyntenyMarkerScaffold(boolean q, boolean s,
			double qPercentStart, double qPercentStop, double sPercentStart,
			double sPercentStop, boolean redrawOnTop, boolean inRed,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART,
			// boolean inGreen,
			String colorSent) {

		// if start > stop take stop as the end of element
		if (qPercentStart > qPercentStop) {
			qPercentStop = qPercentStart;
		}
		if (sPercentStart > sPercentStop) {
			sPercentStop = sPercentStart;
		}

		// int correctPointTriangle = 3;
		// int widthTriangle = UNIT_WIDTH/2;
		if (inRed) {
			contextBgk.setFillStyle(redrawColorRed);
			contextBgk.setStrokeStyle(redrawColorRed);
		} else {
			contextBgk.setStrokeStyle("#DC143C");// redrawColorBlack,
													// redrawColorLightCoral
			contextBgk.setFillStyle(colorSent);
			// if(inGreen){
			// contextBgk.setFillStyle(redrawColorLightGreenBkg);
			// contextBgk.setStrokeStyle(redrawColorDarkGreen);
			// }else{
			// contextBgk.setFillStyle(redrawColorLightCoral);
			// contextBgk.setStrokeStyle(redrawColorBlack);
			// }

		}

		if (q) {
			// draw top
			drawPathSyntenyMarkerScaffoldQ(
					(qPercentStart + (qPercentStop - qPercentStart) / 2),
					addTOTAL_CANVAS_HEIGHT_UPPER_PART, false);
			contextBgk.fill();
			contextBgk.stroke();
		}

		if (s) {
			// draw bottom
			drawPathSyntenyMarkerScaffoldS(
					(sPercentStart + (sPercentStop - sPercentStart) / 2),
					addTOTAL_CANVAS_HEIGHT_UPPER_PART, false);
			contextBgk.fill();
			contextBgk.stroke();
		}

	}

	public void drawSyntenyRepersentationProportionateQS(
			AbsoPropQCompaQSSpanItem apqcqsIT,
			double qPercentStart, double qPercentStop, double sPercentStart,
			double sPercentStop, boolean redrawOnTop, boolean inRed,
			boolean isHighlighted) {

		// if start > stop take stop as the end of element
		if (qPercentStart > qPercentStop) {
			qPercentStop = qPercentStart;
		}
		if (sPercentStart > sPercentStop) {
			sPercentStop = sPercentStart;
		}

		QSEnumBlockType qsBlockType = apqcqsIT.getQsEnumBlockType();

		// define color for gene
		String color = null;
		if (apqcqsIT instanceof AbsoPropQSGeneHomoItem) {
			color = getBkgColorAssociatedWithColorIDAsString(((AbsoPropQSGeneHomoItem) apqcqsIT)
					.getQsStyleItem().getBkgColor());
		}

		if ((TOTAL_LENGHT_SCAFFOLD * qPercentStop)
				- (TOTAL_LENGHT_SCAFFOLD * qPercentStart) < 1
				&& (TOTAL_LENGHT_SCAFFOLD * sPercentStop)
						- (TOTAL_LENGHT_SCAFFOLD * sPercentStart) < 1) {
			// pixel stop is same as pixel start for both q and s
			drawLineBarRepersentationProportionate(qPercentStop, sPercentStop,
					redrawOnTop, inRed, isHighlighted);
		} else if (qsBlockType.compareTo(QSEnumBlockType.QS_HOMOLOGS_BLOCK) == 0) {
			// redrawColorFrostedPeachForSyntenyHomologBlock
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionateHomologRegion(color, false,
					qPercentStart, qPercentStop, sPercentStart, sPercentStop,
					redrawOnTop, inRed, isHighlighted);
		} else if (qsBlockType
				.compareTo(QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK) == 0) {
			// redrawColorGreenVeilForSyntenyReverseBlock
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionateHomologRegion(color, true,
					qPercentStart, qPercentStop, sPercentStart, sPercentStop,
					redrawOnTop, inRed, isHighlighted);
		} else if (qsBlockType
				.compareTo(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK) == 0) {
			drawLineBarRepersentationProportionate(qPercentStart,
					sPercentStart, redrawOnTop, inRed, isHighlighted);
		} else if (qsBlockType
				.compareTo(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK) == 0) {
			drawLineBarRepersentationProportionate(qPercentStop, sPercentStop,
					redrawOnTop, inRed, isHighlighted);
		} else if (qsBlockType
				.compareTo(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawLineBarRepersentationProportionate(qPercentStart,
					sPercentStart, redrawOnTop, inRed, isHighlighted);
		} else if (qsBlockType
				.compareTo(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawLineBarRepersentationProportionate(qPercentStop, sPercentStop,
					redrawOnTop, inRed, isHighlighted);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawSyntenyRepersentationProportionateQS : unrecognized QS_SyntenyEnumBlockType : "
					+ qsBlockType));
		}

	}

	public void drawGenomicRegionRepersentationProportionateQ(
			AbsoPropQCompaQSpanItem apqcSent,
			double qPercentStart, double qPercentStop, boolean redrawOnTop,
			boolean inRed, boolean isHighlighted) {

		// if start > stop take stop as the end of element
		if (qPercentStart > qPercentStop) {
			qPercentStop = qPercentStart;
		}

		QEnumBlockType qBlockType = apqcSent.getqEnumBlockType();

		// define color for gene
		String color = null;
		if (apqcSent instanceof AbsoPropQGeneInserItem) {
			color = getBkgColorAssociatedWithColorIDAsString(((AbsoPropQGeneInserItem) apqcSent)
					.getqStyleItem().getBkgColor());
		}

		if ((TOTAL_LENGHT_SCAFFOLD * qPercentStop)
				- (TOTAL_LENGHT_SCAFFOLD * qPercentStart) < 1) {
			// pixel stop is same as pixel start for both q and s

			drawLineBarRepersentationProportionate(qPercentStop, -1,
					redrawOnTop, inRed, isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorOstrichFeatherForSyntenyQInsertionAsString;
			}
			drawSyntenyRepersentationProportionateInsertion(color, false,
					qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);

		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_LEFT")) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionateMissingTarget(color, true,
					false, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_RIGHT")) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionateMissingTarget(color, true,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);

		
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_LEFT")) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionateMissingTarget(color, true,
					false, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_RIGHT")) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionateMissingTarget(color, true,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);

		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			// colorHomologBlock :
			// colorFrostedPeachForSyntenyHomologBlockAsString
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, false,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_INSERTION) == 0) {
			// Q insertion : colorOstrichFeatherForSyntenyQInsertionAsString
			if (color == null) {
				// default color
				color = colorOstrichFeatherForSyntenyQInsertionAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, false,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			// colorReverseBlock : colorGreenVeilForSyntenyReverseBlockAsString
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, false,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, false, true,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorOstrichFeatherForSyntenyQInsertionAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, false, true,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, false, true,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, true,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			// Window.alert("Q_PARTIAL_RIGHTANDLEFT_INSERTION : ");
			if (color == null) {
				// default color
				color = colorOstrichFeatherForSyntenyQInsertionAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, true,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, true,
					true, qPercentStart, qPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_CUT_START_Q_INSERTION) == 0) {
			drawLineBarRepersentationProportionate(qPercentStart, -1,
					redrawOnTop, inRed, isHighlighted);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_CUT_STOP_Q_INSERTION) == 0) {
			drawLineBarRepersentationProportionate(qPercentStop, -1,
					redrawOnTop, inRed, isHighlighted);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawSyntenyRepersentationProportionateQ : unrecognized Q_SyntenyEnumBlockType : "
					+ qBlockType));
		}
	}

	public void drawGenomicRegionRepersentationProportionateS(
			AbsoPropSCompaSSpanItem apscSent,
			double sPercentStart, double sPercentStop, boolean redrawOnTop,
			boolean inRed, boolean isHighlighted) {

		// if start > stop take stop as the end of element
		if (sPercentStart > sPercentStop) {
			sPercentStop = sPercentStart;
		}

		SEnumBlockType sBlockType = apscSent.getSEnumBlockType();

		// define color for gene
		String color = null;
		if (apscSent instanceof AbsoPropSGeneInserItem) {
			color = getBkgColorAssociatedWithColorIDAsString(((AbsoPropSGeneInserItem) apscSent)
					.getsStyleItem().getBkgColor());
		}

		if ((TOTAL_LENGHT_SCAFFOLD * sPercentStop)
				- (TOTAL_LENGHT_SCAFFOLD * sPercentStart) < 1) {
			// pixel stop is same as pixel start for both q and s

			drawLineBarRepersentationProportionate(-1, sPercentStop,
					redrawOnTop, inRed, isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorFarHorizonForSyntenySInsertionAsString;
			}
			drawSyntenyRepersentationProportionateInsertion(color, true,
					sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
			
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_LEFT")) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionateMissingTarget(color, false,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_RIGHT")) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionateMissingTarget(color, false,
					true, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);

			
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_LEFT")) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionateMissingTarget(color, false,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_RIGHT")) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionateMissingTarget(color, false,
					true, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);

		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, false,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_INSERTION) == 0) {
			// S_INSERTION : colorFarHorizonForSyntenySInsertionAsString
			if (color == null) {
				// default color
				color = colorFarHorizonForSyntenySInsertionAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, false,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, false,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, false, true,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorFarHorizonForSyntenySInsertionAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, false, true,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, false, true,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			// Window.alert(" S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK : ");
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, true,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			// Window.alert(" S_PARTIAL_RIGHTANDLEFT_INSERTION : ");
			if (color == null) {
				// default color
				color = colorFarHorizonForSyntenySInsertionAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, true,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			// Window.alert(" S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK : ");
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawSyntenyRepersentationProportionatePartial(color, true, true,
					false, sPercentStart, sPercentStop, redrawOnTop, inRed,
					isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_CUT_START_S_INSERTION) == 0) {
			drawLineBarRepersentationProportionate(-1, sPercentStart,
					redrawOnTop, inRed, isHighlighted);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_CUT_STOP_S_INSERTION) == 0) {
			drawLineBarRepersentationProportionate(-1, sPercentStop,
					redrawOnTop, inRed, isHighlighted);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawSyntenyRepersentationProportionateS : unrecognized S_SyntenyEnumBlockType : "
					+ sBlockType));
		}
	}

	private void drawLineBarRepersentationProportionate(double percentAtQ,
			double percentAtS, boolean redrawOnTop, boolean inRed,
			boolean isHighlighted) {

		int pixelHeightInsertionBox = UNIT_WIDTH / 2;
		double drawPercentAtQ = -1;
		double drawPercentAtS = -1;
		if (percentAtQ >= 0) {
			drawPercentAtQ = SCAFFOLD_X_START_POINT
					+ (TOTAL_LENGHT_SCAFFOLD * percentAtQ);
		}
		if (percentAtS >= 0) {
			drawPercentAtS = SCAFFOLD_X_START_POINT
					+ (TOTAL_LENGHT_SCAFFOLD * percentAtS);
		}

		contextBgk.beginPath();

		// Y_AXIS_TO_DRAW_AT_BOTTOM
		// pixelHeightInsertionBox;
		// Y_AXIS_TO_DRAW_AT_TOP+1;

		if (drawPercentAtQ >= 0) {
			contextBgk.moveTo(drawPercentAtQ, Y_AXIS_TO_DRAW_AT_TOP + 1);
		} else {
			if (drawPercentAtS >= 0) {
				contextBgk.moveTo(drawPercentAtS, Y_AXIS_TO_DRAW_AT_BOTTOM
						- pixelHeightInsertionBox - 1);
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in drawVerticalBarRepersentationProportionate : both percentAtQ and percentAtS are < 0 : drawPercentAtQ = "
						+ drawPercentAtQ
						+ " ; drawPercentAtS = "
						+ drawPercentAtS));
			}
		}
		if (drawPercentAtS >= 0) {
			contextBgk.lineTo(drawPercentAtS, Y_AXIS_TO_DRAW_AT_BOTTOM);
		} else {
			if (drawPercentAtQ >= 0) {
				contextBgk.lineTo(drawPercentAtQ, Y_AXIS_TO_DRAW_AT_TOP
						+ pixelHeightInsertionBox + 1);
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in drawVerticalBarRepersentationProportionate : both percentAtQ and percentAtS are < 0 : drawPercentAtQ = "
						+ drawPercentAtQ
						+ " ; drawPercentAtS = "
						+ drawPercentAtS));
			}
		}

		if (isHighlighted) {
			contextBgk.setLineWidth(1);
			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
			} else {
				contextBgk.setStrokeStyle(redrawColorBlack);
			}
			contextBgk.stroke();
		} else {

			if (redrawOnTop
					&& associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.size() < LIMIT_PROPORTIONATE_DRAWING_LOWER_QUALITY) {
				// clear in white
				contextBgk.setLineWidth(1.5);
				contextBgk.setStrokeStyle(redrawColorWhite);
				contextBgk.stroke();
			}

			contextBgk.setLineWidth(1.4);
			contextBgk.setStrokeStyle(redrawColorAzure3);
			// contextBgk.setStrokeStyle(redrawColorBlack);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		}

	}

	private void drawSyntenyRepersentationProportionatePartial(String colorBkg,
			boolean left, boolean right, boolean q,
			double relativePercentStart, double relativePercentStop,
			boolean redrawOnTop, boolean inRed, boolean isHighlighted) {

		// System.out.println("draw drawSyntenyRepersentationProportionatePartial : colorBkg : "+colorBkg+" ; right = "+right+" ; q = "+q+" ; relativePercentStart = "+relativePercentStart+" ; relativePercentStop="+relativePercentStop);

		// int pixelHeightInsertionBox =
		// ((Y_AXIS_TO_DRAW_AT_BOTTOM-Y_AXIS_TO_DRAW_AT_TOP)/2)-2;
		int pixelHeightInsertionBox = UNIT_WIDTH / 2;

		int yAxis = -1;

		if (!q) {
			yAxis = Y_AXIS_TO_DRAW_AT_BOTTOM - pixelHeightInsertionBox - 1;
		} else {
			yAxis = Y_AXIS_TO_DRAW_AT_TOP + 1;
		}
		double start = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * relativePercentStart);
		double stop = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * relativePercentStop);

		

		contextBgk.beginPath();
		// draw arrow
		double xArrowHead = -1;
		int xArrowHeadWidth = -1;

		if (stop - start < 5) {
			xArrowHeadWidth = (int) ((stop - start) - 1);
			if (xArrowHeadWidth < 0) {
				xArrowHeadWidth = 0;
			}
		} else if (stop - start < 13) {
			xArrowHeadWidth = 5;
		} else if (stop - start < 25) {
			xArrowHeadWidth = 7;
		} else {
			xArrowHeadWidth = 9;
		}

		if (right) {
			xArrowHead = stop - 1;
			contextBgk.moveTo(xArrowHead, yAxis + (pixelHeightInsertionBox / 2)
					+ 1);
			contextBgk.lineTo(start + (stop - start) / 2, yAxis
					+ (pixelHeightInsertionBox / 2) + 1);
			contextBgk.moveTo(xArrowHead - xArrowHeadWidth, yAxis
					+ (pixelHeightInsertionBox / 4) + 1);
			contextBgk.lineTo(xArrowHead, yAxis + (pixelHeightInsertionBox / 2)
					+ 1);
			contextBgk.lineTo(xArrowHead - xArrowHeadWidth, yAxis
					+ (3 * pixelHeightInsertionBox / 4) + 1);
		}

		if (left) {
			// left
			xArrowHead = start + 1;
			xArrowHeadWidth = -xArrowHeadWidth;
			contextBgk.moveTo(xArrowHead, yAxis + (pixelHeightInsertionBox / 2)
					+ 1);
			contextBgk.lineTo(start + (stop - start) / 2, yAxis
					+ (pixelHeightInsertionBox / 2) + 1);
			contextBgk.moveTo(xArrowHead - xArrowHeadWidth, yAxis
					+ (pixelHeightInsertionBox / 4) + 1);
			contextBgk.lineTo(xArrowHead, yAxis + (pixelHeightInsertionBox / 2)
					+ 1);
			contextBgk.lineTo(xArrowHead - xArrowHeadWidth, yAxis
					+ (3 * pixelHeightInsertionBox / 4) + 1);
		}

		// draw contour
		contextBgk.moveTo(start, yAxis + 0.5);
		contextBgk.lineTo(stop, yAxis + 0.5);
		if (!right) {
			contextBgk.lineTo(stop, yAxis + pixelHeightInsertionBox + 0.5);
		} else {
			contextBgk.moveTo(stop, yAxis + pixelHeightInsertionBox + 0.5);
		}
		contextBgk.lineTo(start, yAxis + pixelHeightInsertionBox + 0.5);
		if (!left) {
			contextBgk.lineTo(start, yAxis + 0.5);
		}

		if (isHighlighted) {
			contextBgk.setLineWidth(1);
			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
			} else {
				contextBgk.setStrokeStyle(redrawColorBlack);
			}
			contextBgk.strokeRect(start, yAxis + 0.5,
					(int) Math.floor(stop - start) + 0.5,
					pixelHeightInsertionBox);
		} else {

			if (redrawOnTop
					&& associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.size() < LIMIT_PROPORTIONATE_DRAWING_LOWER_QUALITY) {
				// clear in white
				contextBgk.setLineWidth(1.5);
				contextBgk.setStrokeStyle(redrawColorWhite);
				contextBgk.strokeRect(start, yAxis + 0.5,
						(int) Math.floor(stop - start) + 0.5,
						pixelHeightInsertionBox);
			}

			contextBgk.setLineWidth(1.4);
			contextBgk.setStrokeStyle(redrawColorAzure3);
			// contextBgk.setStrokeStyle(redrawColorBlack);
			contextBgk.strokeRect(start, yAxis + 0.5,
					(int) Math.floor(stop - start) + 0.5,
					pixelHeightInsertionBox);
			contextBgk.setLineWidth(1);
		}

	}

	private void drawSyntenyRepersentationProportionateMissingTarget(
			String colorBkg, boolean QMissingTargetS, boolean right,
			double relativePercentStart, double relativePercentStop,
			boolean redrawOnTop, boolean inRed, boolean isHighlighted) {

		// System.out.println("drawSyntenyRepersentationProportionateMissingTarget");

		// int pixelHeightInsertionBox =
		// (Y_AXIS_TO_DRAW_AT_BOTTOM-Y_AXIS_TO_DRAW_AT_TOP)/2;
		int pixelHeightInsertionBox = UNIT_WIDTH / 2;

		int yAxis = -1;
		double start = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * relativePercentStart);
		double stop = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * relativePercentStop);
		double xStartTwisted = -1;
		double xStopTwisted = -1;

		if (!QMissingTargetS) {
			yAxis = Y_AXIS_TO_DRAW_AT_BOTTOM - 1;
			pixelHeightInsertionBox = -pixelHeightInsertionBox;
		} else {
			yAxis = Y_AXIS_TO_DRAW_AT_TOP + 1;
		}

		if (right) {
			xStartTwisted = start + 8;
			if (xStartTwisted > SCAFFOLD_X_STOP_POINT) {
				xStartTwisted = SCAFFOLD_X_STOP_POINT;
			}
			xStopTwisted = stop + 8;
			if (xStopTwisted < SCAFFOLD_X_START_POINT) {
				xStopTwisted = SCAFFOLD_X_START_POINT;
			}
		} else {
			xStartTwisted = start - 8;
			if (xStartTwisted < SCAFFOLD_X_START_POINT) {
				xStartTwisted = SCAFFOLD_X_START_POINT;
			}
			xStopTwisted = stop - 8;
			if (xStopTwisted > SCAFFOLD_X_STOP_POINT) {
				xStopTwisted = SCAFFOLD_X_STOP_POINT;
			}
		}

		

		// draw shape
		double mediumX = start + ((stop - start) / 2);
		// double mediumXTwisted =
		// xStartTwisted+((xStopTwisted-xStartTwisted)/2);
		int xArrowHeadWidth = -1;
		int yArrowHeadWidth = -1;

		contextBgk.beginPath();
		if (stop - start < 7) {
			// draw a single line only
			contextBgk.moveTo(mediumX, yAxis);
			contextBgk.lineTo(mediumX, yAxis + pixelHeightInsertionBox);
		} else if (stop - start < 16) {
			xArrowHeadWidth = 7;
			yArrowHeadWidth = 7;
			// arrow
			if (right) {
				xArrowHeadWidth = -xArrowHeadWidth;
			}
			if (QMissingTargetS) {
				yArrowHeadWidth = -yArrowHeadWidth;
			}
			contextBgk.moveTo(mediumX, yAxis);
			contextBgk.lineTo(mediumX, yAxis + pixelHeightInsertionBox
					+ yArrowHeadWidth);
			contextBgk.lineTo(mediumX - xArrowHeadWidth, yAxis
					+ pixelHeightInsertionBox);
			contextBgk.moveTo(mediumX - xArrowHeadWidth + xArrowHeadWidth,
					yAxis + pixelHeightInsertionBox);
			contextBgk.lineTo(mediumX - xArrowHeadWidth, yAxis
					+ pixelHeightInsertionBox);
			contextBgk.lineTo(mediumX - xArrowHeadWidth, yAxis
					+ pixelHeightInsertionBox + yArrowHeadWidth);
		} else {
			xArrowHeadWidth = 9;
			yArrowHeadWidth = 9;
			if (right) {
				xArrowHeadWidth = -xArrowHeadWidth;
			}
			if (QMissingTargetS) {
				yArrowHeadWidth = -yArrowHeadWidth;
			}
			// draw line on each side
			contextBgk.moveTo(start, yAxis);
			contextBgk.lineTo(xStartTwisted, yAxis + pixelHeightInsertionBox);
			contextBgk.moveTo(xStopTwisted, yAxis + pixelHeightInsertionBox);
			contextBgk.lineTo(stop, yAxis);
			// arrow
			contextBgk.moveTo(mediumX, yAxis);
			contextBgk.lineTo(mediumX, yAxis + pixelHeightInsertionBox
					+ yArrowHeadWidth);
			contextBgk.lineTo(mediumX - xArrowHeadWidth, yAxis
					+ pixelHeightInsertionBox);
			contextBgk.moveTo(mediumX - xArrowHeadWidth + xArrowHeadWidth,
					yAxis + pixelHeightInsertionBox);
			contextBgk.lineTo(mediumX - xArrowHeadWidth, yAxis
					+ pixelHeightInsertionBox);
			contextBgk.lineTo(mediumX - xArrowHeadWidth, yAxis
					+ pixelHeightInsertionBox + yArrowHeadWidth);
		}

		if (isHighlighted) {
			contextBgk.setLineWidth(1);
			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
			} else {
				contextBgk.setStrokeStyle(redrawColorBlack);
			}

			// shape with arrow
			contextBgk.stroke();
			// simple rectangle
			// contextBgk.strokeRect(start, yAxis+0.5, (int)
			// Math.floor(stop-start)+0.5, pixelHeightInsertionBox);
		} else {

			// draw contour
			if (redrawOnTop
					&& associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.size() < LIMIT_PROPORTIONATE_DRAWING_LOWER_QUALITY) {
				// clear in white
				contextBgk.setLineWidth(1.5);
				contextBgk.setStrokeStyle(redrawColorWhite);
				// shape with arrow
				contextBgk.stroke();
				// simple rectangle
				// contextBgk.strokeRect(start, yAxis+0.5, (int)
				// Math.floor(stop-start)+0.5, pixelHeightInsertionBox);
			}

			contextBgk.setLineWidth(1.4);
			contextBgk.setStrokeStyle(redrawColorAzure3);
			// contextBgk.setStrokeStyle(redrawColorBlack);
			// shape with arrow
			contextBgk.stroke();
			// simple rectangle
			// contextBgk.strokeRect(start, yAxis+0.5, (int)
			// Math.floor(stop-start)+0.5, pixelHeightInsertionBox);
			contextBgk.setLineWidth(1);
		}

	}

	private void drawSyntenyRepersentationProportionateInsertion(
			String colorSent, boolean isSInsertion, double percentStart,
			double percentStop, boolean redrawOnTop, boolean inRed,
			boolean isHighlighted) {

		int pixelHeightInsertionBox = UNIT_WIDTH / 2;
		int yAxis = -1;

		if (isSInsertion) {
			yAxis = Y_AXIS_TO_DRAW_AT_BOTTOM - pixelHeightInsertionBox - 1;
		} else {
			yAxis = Y_AXIS_TO_DRAW_AT_TOP + 1;
		}
		contextBgk.setFillStyle(colorSent);
		double start = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * percentStart);
		double stop = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * percentStop);

		if (!redrawOnTop &&
		// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()<LIMIT_PROPORTIONATE_DRAWING_LOWER_QUALITY
				(((start - stop) > LIMIT_WIDTH_PROPORTIONATE_BKG_PX) || ((stop - start) > LIMIT_WIDTH_PROPORTIONATE_BKG_PX))) {
			// //draw background
			contextBgk.setGlobalAlpha(0.5);
			contextBgk.fillRect(start, yAxis + 0.5,
					(int) Math.floor(stop - start) + 0.5,
					pixelHeightInsertionBox);
			contextBgk.setGlobalAlpha(1);
		}

		if (isHighlighted) {
			contextBgk.setLineWidth(1);
			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
			} else {
				contextBgk.setStrokeStyle(redrawColorBlack);
			}
			contextBgk.strokeRect(start, yAxis + 0.5,
					(int) Math.floor(stop - start) + 0.5,
					pixelHeightInsertionBox);
		} else {

			// draw contour
			if (redrawOnTop
					&& associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.size() < LIMIT_PROPORTIONATE_DRAWING_LOWER_QUALITY) {
				// clear in white
				contextBgk.setLineWidth(1.5);
				contextBgk.setStrokeStyle(redrawColorWhite);
				contextBgk.strokeRect(start, yAxis + 0.5,
						(int) Math.floor(stop - start) + 0.5,
						pixelHeightInsertionBox);
			}

			contextBgk.setLineWidth(1.4);
			contextBgk.setStrokeStyle(redrawColorAzure3);
			// contextBgk.setStrokeStyle(redrawColorBlack);
			contextBgk.strokeRect(start, yAxis + 0.5,
					(int) Math.floor(stop - start) + 0.5,
					pixelHeightInsertionBox);
			contextBgk.setLineWidth(1);
		}

	}

	private void drawSyntenyRepersentationProportionateHomologRegion(
			String colorSent, boolean isInverse, double percentStartUpper,
			double percentStopUpper, double percentStartBottom,
			double percentStopBottom, boolean redrawOnTop, boolean inRed,
			boolean isHighlighted) {

		// System.out.println("percentStartUpper="+percentStartUpper+";percentStopUpper="+percentStopUpper
		// +";percentStartBottom="+percentStartBottom+";percentStopBottom="+percentStopBottom);

		// draw path
		contextBgk.beginPath();
		double pxStartUpper = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * percentStartUpper) + 0.5;
		double pxStopUpper = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * percentStopUpper) + 0.5;
		double pxStartBottom = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * percentStartBottom) + 0.5;
		double pxStopBottom = SCAFFOLD_X_START_POINT
				+ (TOTAL_LENGHT_SCAFFOLD * percentStopBottom) + 0.5;

		contextBgk.moveTo(pxStartUpper, Y_AXIS_TO_DRAW_AT_TOP + 1 + 0.5);
		contextBgk.lineTo(pxStopUpper, Y_AXIS_TO_DRAW_AT_TOP + 1 + 0.5);
		if (isInverse) {
			contextBgk
					.lineTo(pxStartBottom, Y_AXIS_TO_DRAW_AT_BOTTOM - 1 + 0.5);
			contextBgk.lineTo(pxStopBottom, Y_AXIS_TO_DRAW_AT_BOTTOM - 1 + 0.5);
		} else {
			contextBgk.lineTo(pxStopBottom, Y_AXIS_TO_DRAW_AT_BOTTOM - 1 + 0.5);
			contextBgk
					.lineTo(pxStartBottom, Y_AXIS_TO_DRAW_AT_BOTTOM - 1 + 0.5);

		}
		contextBgk.closePath();

		contextBgk.setFillStyle(colorSent);

		// System.err.println("here");
		if (!redrawOnTop &&
		// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()<LIMIT_PROPORTIONATE_DRAWING_LOWER_QUALITY
				(((pxStopUpper - pxStartUpper) > LIMIT_WIDTH_PROPORTIONATE_BKG_PX)
						|| ((pxStartUpper - pxStopUpper) > LIMIT_WIDTH_PROPORTIONATE_BKG_PX)
						|| ((pxStopBottom - pxStartBottom) > LIMIT_WIDTH_PROPORTIONATE_BKG_PX) || ((pxStartBottom - pxStopBottom) > LIMIT_WIDTH_PROPORTIONATE_BKG_PX))) {
			// draw background
			// System.err.println("oui bkg");
			contextBgk.setGlobalAlpha(0.5);
			contextBgk.fill();
			contextBgk.setGlobalAlpha(1);
		}

		if (isHighlighted) {
			contextBgk.setLineWidth(1);
			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
			} else {
				contextBgk.setStrokeStyle(redrawColorBlack);
			}
			contextBgk.stroke();
		} else {

			// draw contour
			if (redrawOnTop
					&& associatedGenomePanel
							.getGenomePanelItem()
							.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
							.size() < LIMIT_PROPORTIONATE_DRAWING_LOWER_QUALITY) {
				// clear in white
				contextBgk.setLineWidth(1.5);
				contextBgk.setStrokeStyle(redrawColorWhite);
				contextBgk.stroke();
			}

			contextBgk.setLineWidth(1.4);
			contextBgk.setStrokeStyle(redrawColorAzure3);
			// contextBgk.setStrokeStyle(redrawColorBlack);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		}

	}

	protected void clearScaffoldGenome(boolean q) {
		contextBgk.setFillStyle(redrawColorWhite);
		if (q) {
			// System.out.println("clearing q for zoom");
			// contextBgk.setFillStyle(redrawColorRed);
			// contextBgk.fillRect(0, TOTAL_CANVAS_HEIGHT_UPPER_PART+1,
			// TOTAL_CANVAS_WIDTH, Y_AXIS_TO_DRAW_AT_BOTTOM-(UNIT_WIDTH*0.8));
			contextBgk.fillRect(0, TOTAL_CANVAS_HEIGHT_UPPER_PART + 1,
					TOTAL_CANVAS_WIDTH, Y_AXIS_TO_DRAW_AT_BOTTOM
							- (UNIT_WIDTH * 0.8));
		} else {
			// System.out.println("clearing s for zoom");
			// contextBgk.setFillStyle(redrawColorRed);
			// contextBgk.fillRect(0,
			// TOTAL_CANVAS_HEIGHT_UPPER_PART+Y_AXIS_TO_DRAW_AT_BOTTOM-(UNIT_WIDTH*0.8),
			// TOTAL_CANVAS_WIDTH, TOTAL_CANVAS_HEIGHT_UPPER_PART);
			contextBgk.fillRect(0, TOTAL_CANVAS_HEIGHT_UPPER_PART
					+ Y_AXIS_TO_DRAW_AT_BOTTOM - (UNIT_WIDTH * 0.8),
					TOTAL_CANVAS_WIDTH, TOTAL_CANVAS_HEIGHT_UPPER_PART);
		}
	}

	public void clearMarkerBottom(boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART,
			boolean q, boolean s) {

		int toAddExtra = 0;

		if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
			toAddExtra = TOTAL_CANVAS_HEIGHT_UPPER_PART;
		}

		contextBgk.setFillStyle(redrawColorWhite);
		// clear top
		if (q) {
			
			contextBgk.fillRect(SCAFFOLD_X_START_POINT + 2 + 0.5,
					//toAddExtra + Y_AXIS_TO_DRAW_AT_TOP - 4 - (UNIT_WIDTH / 2) + 0.5,
					toAddExtra + Y_AXIS_TO_DRAW_AT_TOP - 4 - (WIDTH_SIZE_PB_TEXT / 4) + 0.5,
					TOTAL_LENGHT_SCAFFOLD - 3,
					//(UNIT_WIDTH / 2) + 3
					(WIDTH_SIZE_PB_TEXT / 4) + 3
					);
			
		}
		// clear bottom
		if (s) {
			contextBgk.fillRect(SCAFFOLD_X_START_POINT + 2 + 0.5, toAddExtra
					+ Y_AXIS_TO_DRAW_AT_BOTTOM + 2 + 0.5,
					TOTAL_LENGHT_SCAFFOLD - 4, (UNIT_WIDTH / 2) + 5);
		}

	}

	public void clearSymbolicSlice(int idxsliceSent) {
		restoreToOriginCoordinateSpace();
		contextBgk.setFillStyle(redrawColorWhite);//redrawColorWhite
		contextBgk.fillRect((addToLeftToCenterDrawing + (SLICE_CANVAS_WIDTH * idxsliceSent) + 1), //3
				1,
				(SLICE_CANVAS_WIDTH - 2), //6
				TOTAL_CANVAS_HEIGHT_UPPER_PART - 2);
	}

	public void clearSymbolicSliceSOnly(int idxsliceSent, boolean leftSide,
			boolean rightSide) {
		restoreToOriginCoordinateSpace();
		contextBgk.setFillStyle(redrawColorWhite);
		if (leftSide && rightSide) {
			contextBgk.fillRect(
							(addToLeftToCenterDrawing + (SLICE_CANVAS_WIDTH * idxsliceSent)),
							S_INSERTION_Y_POINT - 2, (SLICE_CANVAS_WIDTH),
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									- S_INSERTION_Y_POINT - 1);
		} else if (leftSide) {
			contextBgk
					.fillRect(
							(addToLeftToCenterDrawing + (SLICE_CANVAS_WIDTH * idxsliceSent)),
							S_INSERTION_Y_POINT - 2, (SLICE_CANVAS_WIDTH / 2),
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									- S_INSERTION_Y_POINT - 1);
		} else if (rightSide) {
			contextBgk
					.fillRect(
							(addToLeftToCenterDrawing
									+ (SLICE_CANVAS_WIDTH * idxsliceSent) + (SLICE_CANVAS_WIDTH / 2)),
							S_INSERTION_Y_POINT - 2, (SLICE_CANVAS_WIDTH / 2),
							TOTAL_CANVAS_HEIGHT_UPPER_PART
									- S_INSERTION_Y_POINT - 1);
		}
	}

	
	public void clearSymbolicSliceQOnly(int idxsliceSent) {
		restoreToOriginCoordinateSpace();
		contextBgk.setFillStyle(redrawColorWhite);
		contextBgk.fillRect((addToLeftToCenterDrawing + (SLICE_CANVAS_WIDTH * idxsliceSent) + 1),
				0,
				(SLICE_CANVAS_WIDTH - 2),
				Q_INSERTION_Y_POINT + 1);
	}
	

	public void clearWholeCanvas() {
		contextBgk.setFillStyle(redrawColorWhite);
		// contextBgk.clearRect(0,0,TOTAL_CANVAS_WIDTH, TOTAL_CANVAS_HEIGHT);
		contextBgk.fillRect(0, 0, TOTAL_CANVAS_WIDTH, TOTAL_CANVAS_HEIGHT);
	}

	public void clearLowerPartCanvas(boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
		// System.err.println("clearLowerPartCanvas");
		if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
			contextBgk.setFillStyle(redrawColorWhite);
			contextBgk.fillRect(0, TOTAL_CANVAS_HEIGHT_UPPER_PART,
					TOTAL_CANVAS_WIDTH, TOTAL_CANVAS_HEIGHT);
		} else {
			contextBgk.setFillStyle(redrawColorWhite);
			contextBgk.fillRect(0, 0, TOTAL_CANVAS_WIDTH, TOTAL_CANVAS_HEIGHT);
		}

	}

	private void drawScaffoldGenomeText(boolean q,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
		// draw text genome
		int yAxisPercentToDrawAt = -1;
		String textToShow = "";
		String[] strainSplitted = null;
		if (q) {
			yAxisPercentToDrawAt = Y_AXIS_TO_DRAW_AT_TOP;
			if (Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem().getSpecies() != null) {
				textToShow += Character.toUpperCase(Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getReferenceGenomePanelItem().getSpecies().charAt(0))
						+ ".";
				textToShow += " "
						+ Insyght.APP_CONTROLER
								.getCurrentRefGenomePanelAndListOrgaResult()
								.getReferenceGenomePanelItem().getSpecies()
								.split("\\s")[1];
			}
			if (Insyght.APP_CONTROLER
					.getCurrentRefGenomePanelAndListOrgaResult()
					.getReferenceGenomePanelItem().getStrain() != null){
				strainSplitted = Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getReferenceGenomePanelItem().getStrain().split("\\s");
			}
			
			
		} else {
			yAxisPercentToDrawAt = Y_AXIS_TO_DRAW_AT_BOTTOM;
			if (associatedGenomePanel
					.getGenomePanelItem().getSpecies() != null) {
				textToShow += Character.toUpperCase(associatedGenomePanel
						.getGenomePanelItem().getSpecies().charAt(0))
						+ ".";
				textToShow += " "
						+ associatedGenomePanel.getGenomePanelItem().getSpecies()
								.split("\\s")[1];
			}
			if (associatedGenomePanel.getGenomePanelItem()
					.getStrain() != null) {
				strainSplitted = associatedGenomePanel.getGenomePanelItem()
						.getStrain().split("\\s");
			}
			
		}
		if (strainSplitted != null) {
			for (int i = 0; i < strainSplitted.length; i++) {
				if (i > 4) {
					break;
				}
				textToShow += " " + strainSplitted[i];
			}
		}

		if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
			yAxisPercentToDrawAt += TOTAL_CANVAS_HEIGHT_UPPER_PART;
		}
		contextBgk.setFillStyle(redrawColorBlack);
		contextBgk.fillText(textToShow, SCAFFOLD_X_START_GENOME_TEXT,
				yAxisPercentToDrawAt + 3, (TOTAL_CANVAS_WIDTH
						- SCAFFOLD_X_START_GENOME_TEXT - 5));
	}

	// private void drawScaffoldGenomeSize(boolean q, double percentStartGenome,
	// double percentStopGenome, double percentStartDraw, double
	// percentStopDraw, boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
	private void drawScaffoldGenomeSize(boolean q, double percentStartDraw,
			double percentStopDraw, boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART) {

		int yAxisPercentToDrawAt = -1;
		int sizeGenome = -1;
		double percentStartGenome = -1;
		double percentStopGenome = -1;
		
		
		
		if (q) {
			yAxisPercentToDrawAt = Y_AXIS_TO_DRAW_AT_TOP;
			try {
				sizeGenome = Insyght.APP_CONTROLER
						.getCurrentRefGenomePanelAndListOrgaResult()
						.getReferenceGenomePanelItem()
						.getListAbsoluteProportionElementItemQ().get(0)
						.getQsizeOfOragnismInPb();
			} catch (Exception e) {
				//else previous statement give rise to an of bound error when no alignments are found to be shown = associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemS() empty
				sizeGenome = -1;
			}
			percentStartGenome = associatedGenomePanel.getGenomePanelItem()
					.getPercentSyntenyShowStartQGenome();
			percentStopGenome = associatedGenomePanel.getGenomePanelItem()
					.getPercentSyntenyShowStopQGenome();
		} else {

			yAxisPercentToDrawAt = Y_AXIS_TO_DRAW_AT_BOTTOM;
			try {
				sizeGenome = associatedGenomePanel.getGenomePanelItem()
						.getListAbsoluteProportionElementItemS().get(0)
						.getsSizeOfOragnismInPb();
			} catch (Exception e) {
				//else previous statement give rise to an of bound error when no alignments are found to be shown = associatedGenomePanel.getGenomePanelItem().getListAbsoluteProportionElementItemS() empty
				sizeGenome = -1;
			}
			percentStartGenome = associatedGenomePanel.getGenomePanelItem()
					.getPercentSyntenyShowStartSGenome();
			percentStopGenome = associatedGenomePanel.getGenomePanelItem()
					.getPercentSyntenyShowStopSGenome();
			
		}
		
		if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
			yAxisPercentToDrawAt += TOTAL_CANVAS_HEIGHT_UPPER_PART;
		}

		String formattedQStart = "";
		String unitQStart = "bp";
		if (percentStartGenome == 0) {
			formattedQStart = "0";
		} else {
			Double unformatedQStart = sizeGenome * percentStartGenome;
			// if(unformatedQStart > 1000000000000000){
			// unformatedQStart = (unformatedQStart / 1000000000000000);
			// unitQStart = "Pb";
			// }else if(unformatedQStart > 1000000000000){
			// unformatedQStart = (unformatedQStart / 1000000000000);
			// unitQStart = "Tb";
			// }else
			if (unformatedQStart > 1000000000) {
				unformatedQStart = (unformatedQStart / 1000000000);
				unitQStart = "Gb";
			} else if (unformatedQStart > 1000000) {
				unformatedQStart = (unformatedQStart / 1000000);
				unitQStart = "Mb";
			} else if (unformatedQStart > 1000) {
				unformatedQStart = (unformatedQStart / 1000);
				unitQStart = "Kb";
			}
			formattedQStart = NumberFormat.getFormat("#.###").format(
					unformatedQStart);// "#.##E0#"
		}

		String formattedQStop = "";
		String unitQStop = "bp";
		Double unformattedQStop = sizeGenome * percentStopGenome;
		if (unformattedQStop > 1000000000) {
			unformattedQStop = (unformattedQStop / 1000000000);
			unitQStop = "Gb";
		} else if (unformattedQStop > 1000000) {
			unformattedQStop = (unformattedQStop / 1000000);
			unitQStop = "Mb";
		} else if (unformattedQStop > 1000) {
			unformattedQStop = (unformattedQStop / 1000);
			unitQStop = "Kb";
		}
		formattedQStop = NumberFormat.getFormat("#.###").format(
				unformattedQStop);// "#.##E0#"
		contextBgk.setFillStyle(redrawColorBlack);

		// draw textStartUpper
		double xtext = SCAFFOLD_X_START_POINT
				+ (percentStartDraw * TOTAL_LENGHT_SCAFFOLD)
				- WIDTH_SIZE_PB_TEXT - 5;
		contextBgk.fillText(formattedQStart, xtext,
				yAxisPercentToDrawAt - 5 + 0.5, WIDTH_SIZE_PB_TEXT);
		contextBgk.setTextBaseline(TextBaseline.TOP);
		contextBgk.fillText(unitQStart, xtext + 4,
				yAxisPercentToDrawAt - 5 + 2.5);
		contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);

		// draw textStopUpper
		xtext = SCAFFOLD_X_STOP_POINT
				- ((1 - percentStopDraw) * TOTAL_LENGHT_SCAFFOLD) + 3;// -
																		// ((formattedQStop.length()*UNIT_WIDTH)/3))
		contextBgk.fillText(formattedQStop, xtext,
				yAxisPercentToDrawAt - 5 + 0.5, WIDTH_SIZE_PB_TEXT);
		contextBgk.setTextBaseline(TextBaseline.TOP);
		contextBgk.fillText(unitQStop, xtext + 4,
				yAxisPercentToDrawAt - 5 + 2.5);// + (UNIT_WIDTH / 2) + 0.5
		contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);

	}

	// public void drawScaffoldGenome(boolean q, double percentStartGenomeInit,
	// double percentStopGenomeInit, double percentStartDraw, double
	// percentStopDraw, boolean redrawOnTop, boolean inRed, boolean
	// addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
	public void drawScaffoldGenome(boolean q, double percentStartDrawInit,
			double percentStopDrawInit, boolean redrawOnTop, boolean inRed,
			boolean addTOTAL_CANVAS_HEIGHT_UPPER_PART) {

		// if start > stop take stop as the end of element
		if (percentStartDrawInit > percentStopDrawInit) {
			percentStopDrawInit = percentStartDrawInit;
		}

		// System.err.println("drawScaffoldGenome "+redrawOnTop);

		if (percentStartDrawInit < 0) {
			percentStartDrawInit = 0;
		} else if (percentStartDrawInit > 1) {
			percentStartDrawInit = 1;
		}

		if (percentStopDrawInit < 0) {
			percentStopDrawInit = 0;
		} else if (percentStopDrawInit > 1) {
			percentStopDrawInit = 1;
		}

		double percentStartDraw = -1;
		double percentStopDraw = -1;

		if (percentStopDrawInit < percentStartDrawInit) {
			percentStartDraw = percentStopDrawInit;
			percentStopDraw = percentStartDrawInit;
		} else {
			percentStartDraw = percentStartDrawInit;
			percentStopDraw = percentStopDrawInit;
		}

		// System.out.println("percentStart : "+percentStart);
		// System.out.println("percentStop : "+percentStop);

		int yAxisPercentToDrawAt = -1;
		int dashAtTheEndOfScaffold = -1;
		// int extraSpaceForStartStopText = -1;
		if (q) {
			yAxisPercentToDrawAt = Y_AXIS_TO_DRAW_AT_TOP;
			if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
				yAxisPercentToDrawAt += TOTAL_CANVAS_HEIGHT_UPPER_PART;
			}
			dashAtTheEndOfScaffold = -5;
			// extraSpaceForStartStopText = 0;
		} else {
			yAxisPercentToDrawAt = Y_AXIS_TO_DRAW_AT_BOTTOM;
			if (addTOTAL_CANVAS_HEIGHT_UPPER_PART) {
				yAxisPercentToDrawAt += TOTAL_CANVAS_HEIGHT_UPPER_PART;
			}
			dashAtTheEndOfScaffold = 5;
			// extraSpaceForStartStopText = 0;//UNIT_WIDTH/6
		}

		if (!redrawOnTop) {
			drawScaffoldGenomeText(q, addTOTAL_CANVAS_HEIGHT_UPPER_PART);
			drawScaffoldGenomeSize(q, 0, 1, addTOTAL_CANVAS_HEIGHT_UPPER_PART);
		}

		// draw path
		contextBgk.beginPath();
		if (percentStartDraw == 0.0) {
			contextBgk.moveTo(SCAFFOLD_X_START_POINT + 0.5,
					yAxisPercentToDrawAt + dashAtTheEndOfScaffold + 0.5);
			contextBgk.lineTo(SCAFFOLD_X_START_POINT + 0.5,
					yAxisPercentToDrawAt + 0.5);
		} else {
			contextBgk.moveTo(SCAFFOLD_X_START_POINT
					+ (TOTAL_LENGHT_SCAFFOLD * percentStartDraw) + 0.5,
					yAxisPercentToDrawAt + 0.5);
		}
		if (percentStopDraw == 1.0) {
			contextBgk.lineTo(SCAFFOLD_X_STOP_POINT + 0.5,
					yAxisPercentToDrawAt + 0.5);
			contextBgk.lineTo(SCAFFOLD_X_STOP_POINT + 0.5, yAxisPercentToDrawAt
					+ dashAtTheEndOfScaffold + 0.5);
		} else {
			contextBgk.lineTo(SCAFFOLD_X_START_POINT
					+ (TOTAL_LENGHT_SCAFFOLD * percentStopDraw) + 0.5,
					yAxisPercentToDrawAt + 0.5);
		}

		// draw contour
		// clear in white
		contextBgk.setLineWidth(3);
		// contextBgk.setStrokeStyle(redrawColorWhite);
		// contextBgk.stroke();
		if (inRed) {
			// stroke in red
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.stroke();
		} else {
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.stroke();
		}

		contextBgk.setLineWidth(1);

	}

	public String getDefaultBkgColorAccordingToQSBlockType(
			AbsoPropQSSyntItem.QSEnumBlockType qsBlockType) {
		if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK) == 0) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK) == 0) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK) == 0) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK) == 0) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK) == 0) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK) == 0) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else {
			Window.alert("drawBlockQSSynteny : unrecognized QS_SyntenyEnumBlockType : "
					+ qsBlockType);
			return null;
		}
	}

	public void drawBlockQSSynteny(
			AbsoPropQSSyntItem.QSEnumBlockType qsBlockType,
			//String text,
			int numberGenesInSynteny,
			boolean redrawOnTop,
			boolean inRed,
			int numberVariations,
			boolean isContainedWithinAnotherMotherSynteny,
			boolean isSprungOffAnotherMotherSynteny,
			boolean isMotherOfSprungOffSyntenies,
			boolean isMotherOfContainedOtherSyntenies,
			AbsoPropSGenoRegiInserItem previousGenomicRegionSInsertion,
			AbsoPropSGenoRegiInserItem nextGenomicRegionSInsertion,
			boolean allowFetchMissingData, int idxHAPInFullArray,
			int idxSliceDisplayed
	// boolean drawMainQSSynteny, boolean drawPreviousGenomicRegionSInsertion,
	// boolean drawNextGenomicRegionSInsertion
	) {

		String text = numberGenesInSynteny + " CDS";
		// if(drawMainQSSynteny){
		if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK) == 0) {
			drawBlockSyntenyHomolog(text, redrawOnTop, inRed, numberVariations,
					isContainedWithinAnotherMotherSynteny,
					isSprungOffAnotherMotherSynteny,
					isMotherOfSprungOffSyntenies,
					isMotherOfContainedOtherSyntenies);
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawBlockSyntenyReverseHomolog(text, redrawOnTop, inRed,
					numberVariations, isContainedWithinAnotherMotherSynteny,
					isSprungOffAnotherMotherSynteny,
					isMotherOfSprungOffSyntenies,
					isMotherOfContainedOtherSyntenies);
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK) == 0) {
			drawBlockCut(colorFrostedPeachForSyntenyHomologBlockAsString, true,
					true, false, false, text, redrawOnTop, inRed,
					numberVariations);
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK) == 0) {
			drawBlockCut(colorFrostedPeachForSyntenyHomologBlockAsString,
					false, true, false, false, text, redrawOnTop, inRed,
					numberVariations);
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawBlockCut(colorGreenVeilForSyntenyReverseBlockAsString, true,
					true, false, false, text, redrawOnTop, inRed,
					numberVariations);
		} else if (qsBlockType
				.compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawBlockCut(colorGreenVeilForSyntenyReverseBlockAsString, false,
					true, false, false, text, redrawOnTop, inRed,
					numberVariations);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawBlockQSSynteny : unrecognized QS_SyntenyEnumBlockType : "
					+ qsBlockType));
		}
		// }

	}

	public String getDefaultBkgColorAccordingToQBlockType(
			AbsoPropQCompaQSpanItem.QEnumBlockType qBlockType) {

		if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_INSERTION) == 0) {
			return colorOstrichFeatherForSyntenyQInsertionAsString;
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_LEFT")) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_RIGHT")) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_LEFT")) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_RIGHT")) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			// colorHomologBlock :
			// colorFrostedPeachForSyntenyHomologBlockAsString
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_INSERTION) == 0) {
			// Q insertion : colorOstrichFeatherForSyntenyQInsertion
			return colorOstrichFeatherForSyntenyQInsertionAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			// colorReverseBlock : colorGreenVeilForSyntenyReverseBlock
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_INSERTION) == 0) {
			return colorOstrichFeatherForSyntenyQInsertionAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			return colorOstrichFeatherForSyntenyQInsertionAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_CUT_START_Q_INSERTION) == 0) {
			return colorOstrichFeatherForSyntenyQInsertionAsString;
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_CUT_STOP_Q_INSERTION) == 0) {
			return colorOstrichFeatherForSyntenyQInsertionAsString;
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawBlockQGenomicRegionInsertionOrMissingTarget : unrecognized Q_SyntenyEnumBlockType : "
					+ qBlockType));
			return null;
		}

	}

	public void drawBlockQGenomicRegionInsertionOrMissingTarget(
			AbsoPropQCompaQSpanItem.QEnumBlockType qBlockType,
			String text, boolean redrawOnTop, boolean inRed, boolean noGene,
			boolean isAnnotationCollision, boolean isAnchoredToStartElement,
			boolean isAnchoredToStopElement, int offShoots) {

		if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_INSERTION) == 0) {
			if (isAnnotationCollision) {
				drawBlockQAnnotationCollision(redrawOnTop, inRed);
			} else if (noGene) {
				drawBlockQNoGeneGenomicRegionInsertion(text, redrawOnTop,
						inRed, isAnchoredToStartElement,
						isAnchoredToStopElement);
			} else {
				drawBlockQGenomicRegionInsertion(text,
						colorOstrichFeatherForSyntenyQInsertionAsString,
						redrawOnTop, inRed, isAnchoredToStartElement,
						isAnchoredToStopElement);
			}
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_LEFT")) {
			drawBlockSyntenyMissingTarget(
					colorFrostedPeachForSyntenyHomologBlockAsString, false,
					false, text, redrawOnTop, inRed, offShoots);
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_RIGHT")) {
			drawBlockSyntenyMissingTarget(
					colorFrostedPeachForSyntenyHomologBlockAsString, true,
					false, text, redrawOnTop, inRed, offShoots);
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_LEFT")) {
			drawBlockSyntenyMissingTarget(
					colorGreenVeilForSyntenyReverseBlockAsString, false, false,
					text, redrawOnTop, inRed, offShoots);
		} else if (qBlockType.toString().startsWith(
				"Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_")
				&& qBlockType.toString().endsWith("_RIGHT")) {
			drawBlockSyntenyMissingTarget(
					colorGreenVeilForSyntenyReverseBlockAsString, true, false,
					text, redrawOnTop, inRed, offShoots);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			// colorHomologBlock :
			// colorFrostedPeachForSyntenyHomologBlockAsString
			drawBlockPartial(colorFrostedPeachForSyntenyHomologBlockAsString,
					true, false, true, false, text, redrawOnTop, inRed);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_INSERTION) == 0) {
			// Q insertion : colorOstrichFeatherForSyntenyQInsertion
			drawBlockPartial(colorOstrichFeatherForSyntenyQInsertionAsString,
					true, false, true, false, text, redrawOnTop, inRed);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			// colorReverseBlock : colorGreenVeilForSyntenyReverseBlock
			drawBlockPartial(colorGreenVeilForSyntenyReverseBlockAsString,
					true, false, true, false, text, redrawOnTop, inRed);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorFrostedPeachForSyntenyHomologBlockAsString,
					false, true, true, false, text, redrawOnTop, inRed);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_INSERTION) == 0) {
			drawBlockPartial(colorOstrichFeatherForSyntenyQInsertionAsString,
					false, true, true, false, text, redrawOnTop, inRed);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorGreenVeilForSyntenyReverseBlockAsString,
					false, true, true, false, text, redrawOnTop, inRed);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorFrostedPeachForSyntenyHomologBlockAsString,
					true, true, true, false, text, redrawOnTop, inRed);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			drawBlockPartial(colorOstrichFeatherForSyntenyQInsertionAsString,
					true, true, true, false, text, redrawOnTop, inRed);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorGreenVeilForSyntenyReverseBlockAsString,
					true, true, true, false, text, redrawOnTop, inRed);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_CUT_START_Q_INSERTION) == 0) {
			drawBlockCut(colorOstrichFeatherForSyntenyQInsertionAsString, true,
					false, true, false, text, redrawOnTop, inRed, -1);
		} else if (qBlockType
				.compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_CUT_STOP_Q_INSERTION) == 0) {
			drawBlockCut(colorOstrichFeatherForSyntenyQInsertionAsString,
					false, false, true, false, text, redrawOnTop, inRed, -1);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawBlockQGenomicRegionInsertionOrMissingTarget : unrecognized Q_SyntenyEnumBlockType : "
					+ qBlockType));
		}

	}

	public String getDefaultBkgColorAccordingToSBlockType(
			AbsoPropSCompaSSpanItem.SEnumBlockType sBlockType) {
		if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_INSERTION) == 0) {
			return colorFarHorizonForSyntenySInsertionAsString;
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_LEFT")) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_RIGHT")) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_LEFT")) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_RIGHT")) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_INSERTION) == 0) {
			// S_INSERTION : colorFarHorizonForSyntenySInsertion
			return colorFarHorizonForSyntenySInsertionAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_INSERTION) == 0) {
			return colorFarHorizonForSyntenySInsertionAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			return colorFrostedPeachForSyntenyHomologBlockAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			return colorFarHorizonForSyntenySInsertionAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			return colorGreenVeilForSyntenyReverseBlockAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_CUT_START_S_INSERTION) == 0) {
			return colorFarHorizonForSyntenySInsertionAsString;
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_CUT_STOP_S_INSERTION) == 0) {
			return colorFarHorizonForSyntenySInsertionAsString;
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawBlockSGenomicRegionInsertionOrMissingTarget : unrecognized S_SyntenyEnumBlockType : "
					+ sBlockType));
			return null;
		}
	}

	public void drawBlockSGenomicRegionInsertionOrMissingTarget(
			AbsoPropSCompaSSpanItem.SEnumBlockType sBlockType,
			String text, boolean redrawOnTop, boolean inRed, boolean noGene,
			boolean anchoredToPrevious, boolean anchoredToNext,
			boolean isAnnotationCollision, boolean isAnchoredToStartElement,
			boolean isAnchoredToStopElement, int offShoots) {

		if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_INSERTION) == 0) {
			if (isAnnotationCollision) {
				drawBlockSAnnotationCollision(redrawOnTop, inRed,
						anchoredToPrevious, anchoredToNext);
			} else if (noGene) {
				// drawBlockSNoGeneGenomicRegionInsertion(String text, boolean
				// redrawOnTop, boolean inRed, boolean anchoredToPrevious,
				// boolean anchoredToNext
				drawBlockSNoGeneGenomicRegionInsertion(text, redrawOnTop,
						inRed, anchoredToPrevious, anchoredToNext,
						isAnchoredToStartElement, isAnchoredToStopElement);
			} else {
				drawBlockSGenomicRegionInsertion(text,
						colorFarHorizonForSyntenySInsertionAsString,
						redrawOnTop, inRed, anchoredToPrevious, anchoredToNext,
						isAnchoredToStartElement, isAnchoredToStopElement);
			}
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_LEFT")) {
			drawBlockSyntenyMissingTarget(
					colorFrostedPeachForSyntenyHomologBlockAsString, false,
					true, text, redrawOnTop, inRed, offShoots);
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_RIGHT")) {
			drawBlockSyntenyMissingTarget(
					colorFrostedPeachForSyntenyHomologBlockAsString, true,
					true, text, redrawOnTop, inRed, offShoots);
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_LEFT")) {
			drawBlockSyntenyMissingTarget(
					colorGreenVeilForSyntenyReverseBlockAsString, false, true,
					text, redrawOnTop, inRed, offShoots);
		} else if (sBlockType.toString().startsWith(
				"S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_")
				&& sBlockType.toString().endsWith("_RIGHT")) {
			drawBlockSyntenyMissingTarget(
					colorGreenVeilForSyntenyReverseBlockAsString, true, true,
					text, redrawOnTop, inRed, offShoots);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorFrostedPeachForSyntenyHomologBlockAsString,
					true, false, false, true, text, redrawOnTop, inRed);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_INSERTION) == 0) {
			// S_INSERTION : colorFarHorizonForSyntenySInsertion
			drawBlockPartial(colorFarHorizonForSyntenySInsertionAsString, true,
					false, false, true, text, redrawOnTop, inRed);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorGreenVeilForSyntenyReverseBlockAsString,
					true, false, false, true, text, redrawOnTop, inRed);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorFrostedPeachForSyntenyHomologBlockAsString,
					false, true, false, true, text, redrawOnTop, inRed);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_INSERTION) == 0) {
			drawBlockPartial(colorFarHorizonForSyntenySInsertionAsString,
					false, true, false, true, text, redrawOnTop, inRed);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorGreenVeilForSyntenyReverseBlockAsString,
					false, true, false, true, text, redrawOnTop, inRed);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorFrostedPeachForSyntenyHomologBlockAsString,
					true, true, false, true, text, redrawOnTop, inRed);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			drawBlockPartial(colorFarHorizonForSyntenySInsertionAsString, true,
					true, false, true, text, redrawOnTop, inRed);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawBlockPartial(colorGreenVeilForSyntenyReverseBlockAsString,
					true, true, false, true, text, redrawOnTop, inRed);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_CUT_START_S_INSERTION) == 0) {
			drawBlockCut(colorFarHorizonForSyntenySInsertionAsString, true,
					false, false, true, text, redrawOnTop, inRed, -1);
		} else if (sBlockType
				.compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_CUT_STOP_S_INSERTION) == 0) {
			drawBlockCut(colorFarHorizonForSyntenySInsertionAsString, false,
					false, false, true, text, redrawOnTop, inRed, -1);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawBlockSGenomicRegionInsertionOrMissingTarget : unrecognized S_SyntenyEnumBlockType : "
					+ sBlockType));
		}
	}

	public void drawBlockQSGeneHomology(
			AbsoPropQSGeneHomoItem absoluteProportionQComparableQSGeneHomologyItemSent,
			boolean redrawOnTop,
			boolean inRed,
			boolean drawBlockGeneHomologyOuterContour,
			boolean drawBlockGeneHomologyBkg,
			boolean drawBlockGeneHomologyMatchZone,
			boolean drawBlockGeneHomologyGeneNameUp,
			boolean drawBlockGeneHomologyGeneNameDown,
			boolean drawBlockGeneHomologyOtherHomologues,
			int numberOtherMatch,
			// boolean geneHomologyGeneNameUpClicked,
			// boolean geneHomologyGeneNameDownClicked,
			// boolean geneHomologyMatchZoneClicked,
			boolean allowFetchMissingData,
			int idxHAPInFullArray,
			int idxSliceDisplayed,
			AbsoPropSGenoRegiInserItem previousGenomicRegionSInsertion,
			AbsoPropSGenoRegiInserItem nextGenomicRegionSInsertion
	// ,boolean drawMainQS, boolean drawPreviousGenomicRegionSInsertion, boolean
	// drawNextGenomicRegionSInsertion
	) {

		// default blocktype
		if (absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsEnumBlockType() == null) {
			absoluteProportionQComparableQSGeneHomologyItemSent
					.setQsEnumBlockType(QSEnumBlockType.QS_HOMOLOGS_BLOCK);
		}

		// if(drawMainQS){
		if ((absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsQMostSignificantGeneNameAsStrippedString().compareTo(
						"NO_NAME") == 0 || absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsSMostSignificantGeneNameAsStrippedString().compareTo(
						"NO_NAME") == 0)
				&& allowFetchMissingData) {

//			@SuppressWarnings("unused")
//			ResultLoadingDialog rld = new ResultLoadingDialog(
//					DetailledInfoStack.missingInfoForGeneHomology,
//					idxHAPInFullArray,
//					(associatedGenomePanel
//							.getGenomePanelItem()
//							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + idxSliceDisplayed),
//					idxSliceDisplayed,
//					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(
//							associatedGenomePanel),
//					associatedGenomePanel
//							.getGenomePanelItem()
//							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//					CenterSLPAllGenomePanels.START_INDEX, associatedGenomePanel
//							.getGenomePanelItem()
//							.getPercentSyntenyShowStartQGenome(),
//					associatedGenomePanel.getGenomePanelItem()
//							.getPercentSyntenyShowStopQGenome(),
//					associatedGenomePanel.getGenomePanelItem()
//							.getPercentSyntenyShowStartSGenome(),
//					associatedGenomePanel.getGenomePanelItem()
//							.getPercentSyntenyShowStopSGenome(),
//					absoluteProportionQComparableQSGeneHomologyItemSent,
//					numberOtherMatch, false, inRed, Insyght.APP_CONTROLER
//							.getCurrentRefGenomePanelAndListOrgaResult()
//							.getViewTypeInsyght()
//			// ,geneHomologyGeneNameUpClicked,geneHomologyGeneNameDownClicked,geneHomologyMatchZoneClicked
//			);
			
			RLD.getMissingInfoForGeneHomology(
					idxHAPInFullArray,
					(associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + idxSliceDisplayed),
					idxSliceDisplayed,
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(
							associatedGenomePanel),
					associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
					CenterSLPAllGenomePanels.START_INDEX, associatedGenomePanel
							.getGenomePanelItem()
							.getPercentSyntenyShowStartQGenome(),
					associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStopQGenome(),
					associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStartSGenome(),
					associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStopSGenome(),
					absoluteProportionQComparableQSGeneHomologyItemSent,
					numberOtherMatch, false, inRed, Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght()
							// ,geneHomologyGeneNameUpClicked,geneHomologyGeneNameDownClicked,geneHomologyMatchZoneClicked
					);

					
		}

		if (!redrawOnTop) {
			drawBlockGeneHomologyOuterContour = true;
			drawBlockGeneHomologyBkg = true;
			drawBlockGeneHomologyMatchZone = true;
			drawBlockGeneHomologyGeneNameUp = true;
			drawBlockGeneHomologyGeneNameDown = true;
			drawBlockGeneHomologyOtherHomologues = true;
		}

		// System.err.println(drawBlockGeneHomologyGeneNameDown);

		// define color for gene
		String color = null;
		color = getBkgColorAssociatedWithColorIDAsString(absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsStyleItem().getBkgColor());

		String strippedLocusQ = absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsQMostSignificantGeneNameAsStrippedString();

		String strippedLocusS = absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsSMostSignificantGeneNameAsStrippedString();

		if (absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsEnumBlockType()
				.compareTo(
						AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK) == 0) {
			drawBlockGeneHomology(false,
					absoluteProportionQComparableQSGeneHomologyItemSent,
					drawBlockGeneHomologyOuterContour,
					drawBlockGeneHomologyBkg, drawBlockGeneHomologyMatchZone,
					drawBlockGeneHomologyGeneNameUp,
					drawBlockGeneHomologyGeneNameDown,
					drawBlockGeneHomologyOtherHomologues, inRed,
					numberOtherMatch
			// ,geneHomologyGeneNameUpClicked,geneHomologyGeneNameDownClicked,geneHomologyMatchZoneClicked
			);
		} else if (absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsEnumBlockType()
				.compareTo(
						AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK) == 0) {
			drawBlockGeneHomology(true,
					absoluteProportionQComparableQSGeneHomologyItemSent,
					drawBlockGeneHomologyOuterContour,
					drawBlockGeneHomologyBkg, drawBlockGeneHomologyMatchZone,
					drawBlockGeneHomologyGeneNameUp,
					drawBlockGeneHomologyGeneNameDown,
					drawBlockGeneHomologyOtherHomologues, inRed,
					numberOtherMatch
			// ,geneHomologyGeneNameUpClicked,geneHomologyGeneNameDownClicked,geneHomologyMatchZoneClicked
			);
		} else if (absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsEnumBlockType()
				.compareTo(
						AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK) == 0) {
			// redrawColorFrostedPeachForSyntenyHomologBlock
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawBlockCut(color, true, true, false, false, strippedLocusQ
					+ " - " + strippedLocusS, redrawOnTop, inRed,
					numberOtherMatch);
		} else if (absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsEnumBlockType()
				.compareTo(
						AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawBlockCut(color, false, true, false, false, strippedLocusQ
					+ " - " + strippedLocusS, redrawOnTop, inRed,
					numberOtherMatch);
		} else if (absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsEnumBlockType()
				.compareTo(
						AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawBlockCut(color, true, true, false, false, strippedLocusQ
					+ " - " + strippedLocusS, redrawOnTop, inRed,
					numberOtherMatch);
		} else if (absoluteProportionQComparableQSGeneHomologyItemSent
				.getQsEnumBlockType()
				.compareTo(
						AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawBlockCut(color, false, true, false, false, strippedLocusQ
					+ " - " + strippedLocusS, redrawOnTop, inRed,
					numberOtherMatch);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawBlockQSGeneHomology : unrecognized QS_SyntenyEnumBlockType : "
					+ absoluteProportionQComparableQSGeneHomologyItemSent
							.getQsEnumBlockType()));
		}
		// }

		
	}

	public void drawBlockQGeneInsertionOrMissingTarget(
			AbsoPropQGeneInserItem absoluteProportionQComparableQGeneInsertionItemSent,
			boolean redrawOnTop, boolean inRed, int numberOtherMatch,
			boolean allowFetchMissingData, int idxHAPInFullArray,
			int idxSliceDisplayed) {

		// default blocktype
		if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType() == null) {
			absoluteProportionQComparableQGeneInsertionItemSent
					.setqEnumBlockType(QEnumBlockType.Q_INSERTION);
		}

		if (absoluteProportionQComparableQGeneInsertionItemSent
				.getQMostSignificantGeneNameAsStrippedString().compareTo(
						"NO_NAME") == 0
				&& allowFetchMissingData) {

//			@SuppressWarnings("unused")
//			ResultLoadingDialog rld = new ResultLoadingDialog(
//					DetailledInfoStack.missingInfoForQGeneInsertion,
//					idxHAPInFullArray,
//					(associatedGenomePanel
//							.getGenomePanelItem()
//							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + idxSliceDisplayed),
//					idxSliceDisplayed,
//					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(
//							associatedGenomePanel),
//					associatedGenomePanel
//							.getGenomePanelItem()
//							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//					CenterSLPAllGenomePanels.START_INDEX, associatedGenomePanel
//							.getGenomePanelItem()
//							.getPercentSyntenyShowStartQGenome(),
//					associatedGenomePanel.getGenomePanelItem()
//							.getPercentSyntenyShowStopQGenome(),
//					associatedGenomePanel.getGenomePanelItem()
//							.getPercentSyntenyShowStartSGenome(),
//					associatedGenomePanel.getGenomePanelItem()
//							.getPercentSyntenyShowStopSGenome(),
//					absoluteProportionQComparableQGeneInsertionItemSent,
//					numberOtherMatch, false, false, Insyght.APP_CONTROLER
//							.getCurrentRefGenomePanelAndListOrgaResult()
//							.getViewTypeInsyght());
			RLD.getMissingInfoForQGeneInsertion(
					idxHAPInFullArray,
					(associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + idxSliceDisplayed),
					idxSliceDisplayed,
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(
							associatedGenomePanel),
					associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
					CenterSLPAllGenomePanels.START_INDEX, associatedGenomePanel
							.getGenomePanelItem()
							.getPercentSyntenyShowStartQGenome(),
					associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStopQGenome(),
					associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStartSGenome(),
					associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStopSGenome(),
					absoluteProportionQComparableQGeneInsertionItemSent,
					numberOtherMatch, false, false, Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght());
					
		}

		// define color for gene
		String color = null;
		color = getBkgColorAssociatedWithColorIDAsString(absoluteProportionQComparableQGeneInsertionItemSent
				.getqStyleItem().getBkgColor());

		String strippedLocus = absoluteProportionQComparableQGeneInsertionItemSent
				.getQMostSignificantGeneNameAsStrippedString();

		if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_INSERTION) == 0) {
			// drawBlockQGeneInsertionInsertion(absoluteProportionQComparableQGeneInsertionItemSent,
			// redrawOnTop, inRed);
			drawBlockQGeneRegionInsertion(strippedLocus, color, redrawOnTop,
					inRed);
			
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType().toString()
				.startsWith("Q_MISSING_TARGET_S_HOMOLOGS_")
				&& absoluteProportionQComparableQGeneInsertionItemSent
						.getqEnumBlockType().toString().endsWith("_LEFT")) {
			drawBlockQGeneHomologyMissingTargetS(
					absoluteProportionQComparableQGeneInsertionItemSent, false,
					redrawOnTop, inRed, numberOtherMatch);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType().toString()
				.startsWith("Q_MISSING_TARGET_S_HOMOLOGS_")
				&& absoluteProportionQComparableQGeneInsertionItemSent
						.getqEnumBlockType().toString().endsWith("_RIGHT")) {
			drawBlockQGeneHomologyMissingTargetS(
					absoluteProportionQComparableQGeneInsertionItemSent, true,
					redrawOnTop, inRed, numberOtherMatch);

			
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType().toString()
				.startsWith("Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_")
				&& absoluteProportionQComparableQGeneInsertionItemSent
						.getqEnumBlockType().toString().endsWith("_LEFT")) {
			drawBlockQGeneHomologyMissingTargetS(
					absoluteProportionQComparableQGeneInsertionItemSent, false,
					redrawOnTop, inRed, numberOtherMatch);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType().toString()
				.startsWith("Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_")
				&& absoluteProportionQComparableQGeneInsertionItemSent
						.getqEnumBlockType().toString().endsWith("_RIGHT")) {
			drawBlockQGeneHomologyMissingTargetS(
					absoluteProportionQComparableQGeneInsertionItemSent, true,
					redrawOnTop, inRed, numberOtherMatch);

		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			// colorHomologBlock :
			// colorFrostedPeachForSyntenyHomologBlockAsString
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawBlockPartial(color, true, false, true, false, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_INSERTION) == 0) {
			// Q insertion : colorOstrichFeatherForSyntenyQInsertion
			if (color == null) {
				// default color
				color = colorOstrichFeatherForSyntenyQInsertionAsString;
			}
			drawBlockPartial(color, true, false, true, false, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			// colorReverseBlock : colorGreenVeilForSyntenyReverseBlock
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawBlockPartial(color, true, false, true, false, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawBlockPartial(color, false, true, true, false, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorOstrichFeatherForSyntenyQInsertionAsString;
			}
			drawBlockPartial(color, false, true, true, false, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawBlockPartial(color, false, true, true, false, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawBlockPartial(color, true, true, true, false, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorOstrichFeatherForSyntenyQInsertionAsString;
			}
			drawBlockPartial(color, true, true, true, false, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawBlockPartial(color, true, true, true, false, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_CUT_START_Q_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorOstrichFeatherForSyntenyQInsertionAsString;
			}
			drawBlockCut(color, true, false, true, false, strippedLocus,
					redrawOnTop, inRed, numberOtherMatch);
		} else if (absoluteProportionQComparableQGeneInsertionItemSent
				.getqEnumBlockType()
				.compareTo(
						AbsoPropQCompaQSpanItem.QEnumBlockType.Q_CUT_STOP_Q_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorOstrichFeatherForSyntenyQInsertionAsString;
			}
			drawBlockCut(color, false, false, true, false, strippedLocus,
					redrawOnTop, inRed, numberOtherMatch);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawBlockQGeneInsertionOrMissingTarget : unrecognized Q_SyntenyEnumBlockType : "
					+ absoluteProportionQComparableQGeneInsertionItemSent
							.getqEnumBlockType()));
		}

	}

	public void drawBlockSGeneInsertionOrMissingTarget(
			AbsoPropSGeneInserItem absoluteProportionSComparableSGeneInsertionItemSent,
			boolean redrawOnTop, boolean inRed, int numberOtherMatch,
			boolean allowFetchMissingData, int idxHAPInFullArray,
			int idxSliceDisplayed) {

		// default blocktype
		if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType() == null) {
			absoluteProportionSComparableSGeneInsertionItemSent
					.setSEnumBlockType(SEnumBlockType.S_INSERTION);
		}

		if (absoluteProportionSComparableSGeneInsertionItemSent
				.getsMostSignificantGeneNameAsStrippedString().compareTo(
						"NO_NAME") == 0
				&& allowFetchMissingData) {

//			@SuppressWarnings("unused")
//			ResultLoadingDialog rld = new ResultLoadingDialog(
//					DetailledInfoStack.missingInfoForSGeneInsertion,
//					idxHAPInFullArray,
//					(associatedGenomePanel
//							.getGenomePanelItem()
//							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + idxSliceDisplayed),
//					idxSliceDisplayed,
//					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(
//							associatedGenomePanel),
//					associatedGenomePanel
//							.getGenomePanelItem()
//							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
//					CenterSLPAllGenomePanels.START_INDEX, associatedGenomePanel
//							.getGenomePanelItem()
//							.getPercentSyntenyShowStartQGenome(),
//					associatedGenomePanel.getGenomePanelItem()
//							.getPercentSyntenyShowStopQGenome(),
//					associatedGenomePanel.getGenomePanelItem()
//							.getPercentSyntenyShowStartSGenome(),
//					associatedGenomePanel.getGenomePanelItem()
//							.getPercentSyntenyShowStopSGenome(),
//					absoluteProportionSComparableSGeneInsertionItemSent,
//					numberOtherMatch, false, false, Insyght.APP_CONTROLER
//							.getCurrentRefGenomePanelAndListOrgaResult()
//							.getViewTypeInsyght());
			RLD.getMissingInfoForSGeneInsertion(
					idxHAPInFullArray,
					(associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() + idxSliceDisplayed),
					idxSliceDisplayed,
					Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().indexOf(
							associatedGenomePanel),
					associatedGenomePanel
							.getGenomePanelItem()
							.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(),
					CenterSLPAllGenomePanels.START_INDEX, associatedGenomePanel
							.getGenomePanelItem()
							.getPercentSyntenyShowStartQGenome(),
					associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStopQGenome(),
					associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStartSGenome(),
					associatedGenomePanel.getGenomePanelItem()
							.getPercentSyntenyShowStopSGenome(),
					absoluteProportionSComparableSGeneInsertionItemSent,
					numberOtherMatch, false, false, Insyght.APP_CONTROLER
							.getCurrentRefGenomePanelAndListOrgaResult()
							.getViewTypeInsyght());

		}

		String color = null;
		color = getBkgColorAssociatedWithColorIDAsString(absoluteProportionSComparableSGeneInsertionItemSent
				.getsStyleItem().getBkgColor());

		String strippedLocus = absoluteProportionSComparableSGeneInsertionItemSent
				.getsMostSignificantGeneNameAsStrippedString();

		if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_INSERTION) == 0) {

			drawBlockSGeneRegionInsertion(strippedLocus, color, redrawOnTop,
					inRed);
			// drawBlockSGeneRegionInsertion(absoluteProportionSComparableSGeneInsertionItemSent.getsMostSignificantGeneName(),
			// color, redrawOnTop, inRed);

			
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType().toString()
				.startsWith("S_MISSING_TARGET_Q_HOMOLOGS_")
				&& absoluteProportionSComparableSGeneInsertionItemSent
						.getSEnumBlockType().toString().endsWith("_LEFT")) {
			drawBlockSGeneHomologyMissingTargetQ(
					absoluteProportionSComparableSGeneInsertionItemSent, false,
					redrawOnTop, inRed, numberOtherMatch);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType().toString()
				.startsWith("S_MISSING_TARGET_Q_HOMOLOGS_")
				&& absoluteProportionSComparableSGeneInsertionItemSent
						.getSEnumBlockType().toString().endsWith("_RIGHT")) {
			drawBlockSGeneHomologyMissingTargetQ(
					absoluteProportionSComparableSGeneInsertionItemSent, true,
					redrawOnTop, inRed, numberOtherMatch);

			
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType().toString()
				.startsWith("S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_")
				&& absoluteProportionSComparableSGeneInsertionItemSent
						.getSEnumBlockType().toString().endsWith("_LEFT")) {
			drawBlockSGeneHomologyMissingTargetQ(
					absoluteProportionSComparableSGeneInsertionItemSent, false,
					redrawOnTop, inRed, numberOtherMatch);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType().toString()
				.startsWith("S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_")
				&& absoluteProportionSComparableSGeneInsertionItemSent
						.getSEnumBlockType().toString().endsWith("_RIGHT")) {
			drawBlockSGeneHomologyMissingTargetQ(
					absoluteProportionSComparableSGeneInsertionItemSent, true,
					redrawOnTop, inRed, numberOtherMatch);

		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawBlockPartial(color, true, false, false, true, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_INSERTION) == 0) {
			// S_INSERTION : colorFarHorizonForSyntenySInsertion
			if (color == null) {
				// default color
				color = colorFarHorizonForSyntenySInsertionAsString;
			}
			drawBlockPartial(color, true, false, false, true, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawBlockPartial(color, true, false, false, true, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawBlockPartial(color, false, true, false, true, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorFarHorizonForSyntenySInsertionAsString;
			}
			drawBlockPartial(color, false, true, false, true, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawBlockPartial(color, false, true, false, true, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorFrostedPeachForSyntenyHomologBlockAsString;
			}
			drawBlockPartial(color, true, true, false, true, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorFarHorizonForSyntenySInsertionAsString;
			}
			drawBlockPartial(color, true, true, false, true, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK) == 0) {
			if (color == null) {
				// default color
				color = colorGreenVeilForSyntenyReverseBlockAsString;
			}
			drawBlockPartial(color, true, true, false, true, strippedLocus,
					redrawOnTop, inRed);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_CUT_START_S_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorFarHorizonForSyntenySInsertionAsString;
			}
			drawBlockCut(color, true, false, false, true, strippedLocus,
					redrawOnTop, inRed, numberOtherMatch);
		} else if (absoluteProportionSComparableSGeneInsertionItemSent
				.getSEnumBlockType()
				.compareTo(
						AbsoPropSCompaSSpanItem.SEnumBlockType.S_CUT_STOP_S_INSERTION) == 0) {
			if (color == null) {
				// default color
				color = colorFarHorizonForSyntenySInsertionAsString;
			}
			drawBlockCut(color, false, false, false, true, strippedLocus,
					redrawOnTop, inRed, numberOtherMatch);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawBlockSGenomicRegionInsertionOrMissingTarget : unrecognized S_SyntenyEnumBlockType : "
					+ absoluteProportionSComparableSGeneInsertionItemSent
							.getSEnumBlockType()));
		}

	}

	

	private void roundedRect(Context2d ctx, double x, double y, int width,
			int height, int radius) {
		ctx.beginPath();
		ctx.moveTo(x, y + radius);
		ctx.lineTo(x, y + height - radius);
		ctx.quadraticCurveTo(x, y + height, x + radius, y + height);
		ctx.lineTo(x + width - radius, y + height);
		ctx.quadraticCurveTo(x + width, y + height, x + width, y + height
				- radius);
		ctx.lineTo(x + width, y + radius);
		ctx.quadraticCurveTo(x + width, y, x + width - radius, y);
		ctx.lineTo(x + radius, y);
		ctx.quadraticCurveTo(x, y, x, y + radius);
		// ctx.closePath();
		// ctx.stroke();
	}

	private void drawBlockSAnnotationCollision(boolean redrawOnTop,
			boolean inRed, boolean anchoredToPrevious, boolean anchoredToNext) {

		if (anchoredToPrevious && anchoredToNext) {

			contextBgk.beginPath();

			contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK + 0.5,
					S_INSERTION_Y_LBEZIER_LTC);
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ (double) (W_RECT_HOMOLOG_BLOCK / 8),
							S_INSERTION_Y_LBEZIER_LTC
									+ ((S_INSERTION_Y_POINT - S_INSERTION_Y_LBEZIER_LTC) / 2));
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + 0.5, S_INSERTION_Y_POINT);

			contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK
					+ ((double) W_RECT_HOMOLOG_BLOCK / 4),
					S_INSERTION_Y_LBEZIER_LTC);
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 8),
							S_INSERTION_Y_LBEZIER_LTC
									+ ((S_INSERTION_Y_POINT - S_INSERTION_Y_LBEZIER_LTC) / 2));
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((double) W_RECT_HOMOLOG_BLOCK / 4), S_INSERTION_Y_POINT);

			contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK
					+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 4),
					S_INSERTION_Y_LBEZIER_LTC);
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ (5 * (double) W_RECT_HOMOLOG_BLOCK / 8),
							S_INSERTION_Y_LBEZIER_LTC
									+ ((S_INSERTION_Y_POINT - S_INSERTION_Y_LBEZIER_LTC) / 2));
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 4),
					S_INSERTION_Y_POINT);

			contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
					S_INSERTION_Y_LBEZIER_LTC);
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ (7 * (double) W_RECT_HOMOLOG_BLOCK / 8),
							S_INSERTION_Y_LBEZIER_LTC
									+ ((S_INSERTION_Y_POINT - S_INSERTION_Y_LBEZIER_LTC) / 2));
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
					S_INSERTION_Y_POINT);

			// clear in white
			contextBgk.setStrokeStyle(redrawColorWhite);
			contextBgk.setLineWidth(5);
			contextBgk.stroke();

			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
				contextBgk.setLineWidth(4);
				contextBgk.stroke();
				contextBgk.setLineWidth(1);
			} else {
				contextBgk.setLineWidth(2);
				contextBgk.setStrokeStyle(redrawColorGrey6);
				contextBgk.stroke();
			}
			contextBgk.setLineWidth(1);

		} else if (anchoredToPrevious) {

			contextBgk.beginPath();

			contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK + 0.5,
					S_INSERTION_Y_LBEZIER_LTC);
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ (double) (W_RECT_HOMOLOG_BLOCK / 8),
							S_INSERTION_Y_LBEZIER_LTC
									+ ((S_INSERTION_Y_POINT - S_INSERTION_Y_LBEZIER_LTC) / 2));
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + 0.5, S_INSERTION_Y_POINT);

			contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK
					+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 8),
					S_INSERTION_Y_LBEZIER_LTC);
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ ((double) W_RECT_HOMOLOG_BLOCK / 4),
							S_INSERTION_Y_LBEZIER_LTC
									+ ((S_INSERTION_Y_POINT - S_INSERTION_Y_LBEZIER_LTC) / 2));
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 8),
					S_INSERTION_Y_POINT);

			// clear in white
			contextBgk.setStrokeStyle(redrawColorWhite);
			contextBgk.setLineWidth(5);
			contextBgk.stroke();

			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
				contextBgk.setLineWidth(4);
				contextBgk.stroke();
				contextBgk.setLineWidth(1);
			} else {
				contextBgk.setLineWidth(2);
				contextBgk.setStrokeStyle(redrawColorGrey6);
				contextBgk.stroke();
			}
			contextBgk.setLineWidth(1);

		} else if (anchoredToNext) {

			contextBgk.beginPath();

			contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK
					+ (5 * (double) W_RECT_HOMOLOG_BLOCK / 8),
					S_INSERTION_Y_LBEZIER_LTC);
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 4),
							S_INSERTION_Y_LBEZIER_LTC
									+ ((S_INSERTION_Y_POINT - S_INSERTION_Y_LBEZIER_LTC) / 2));
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ (5 * (double) W_RECT_HOMOLOG_BLOCK / 8),
					S_INSERTION_Y_POINT);

			contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
					S_INSERTION_Y_LBEZIER_LTC);
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ (7 * (double) W_RECT_HOMOLOG_BLOCK / 8),
							S_INSERTION_Y_LBEZIER_LTC
									+ ((S_INSERTION_Y_POINT - S_INSERTION_Y_LBEZIER_LTC) / 2));
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
					S_INSERTION_Y_POINT);

			// clear in white
			contextBgk.setStrokeStyle(redrawColorWhite);
			contextBgk.setLineWidth(5);
			contextBgk.stroke();

			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
				contextBgk.setLineWidth(4);
				contextBgk.stroke();
				contextBgk.setLineWidth(1);
			} else {
				contextBgk.setLineWidth(2);
				contextBgk.setStrokeStyle(redrawColorGrey6);
				contextBgk.stroke();
			}
			contextBgk.setLineWidth(1);

		}

	}

	private void drawBlockQAnnotationCollision(boolean redrawOnTop,
			boolean inRed) {

		contextBgk.beginPath();

		contextBgk
				.moveTo(X_RECT_HOMOLOG_BLOCK + 0.5, Q_INSERTION_Y_LBEZIER_LTC);
		contextBgk
				.lineTo(X_RECT_HOMOLOG_BLOCK
						+ (double) (W_RECT_HOMOLOG_BLOCK / 8),
						Q_INSERTION_Y_LBEZIER_LTC
								+ ((Q_INSERTION_Y_POINT - Q_INSERTION_Y_LBEZIER_LTC) / 2));
		contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + 0.5, Q_INSERTION_Y_POINT);

		contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK
				+ ((double) W_RECT_HOMOLOG_BLOCK / 4),
				Q_INSERTION_Y_LBEZIER_LTC);
		contextBgk
				.lineTo(X_RECT_HOMOLOG_BLOCK
						+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 8),
						Q_INSERTION_Y_LBEZIER_LTC
								+ ((Q_INSERTION_Y_POINT - Q_INSERTION_Y_LBEZIER_LTC) / 2));
		contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
				+ ((double) W_RECT_HOMOLOG_BLOCK / 4), Q_INSERTION_Y_POINT);

		contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK
				+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 4),
				Q_INSERTION_Y_LBEZIER_LTC);
		contextBgk
				.lineTo(X_RECT_HOMOLOG_BLOCK
						+ (5 * (double) W_RECT_HOMOLOG_BLOCK / 8),
						Q_INSERTION_Y_LBEZIER_LTC
								+ ((Q_INSERTION_Y_POINT - Q_INSERTION_Y_LBEZIER_LTC) / 2));
		contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
				+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 4), Q_INSERTION_Y_POINT);

		contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
				Q_INSERTION_Y_LBEZIER_LTC);
		contextBgk
				.lineTo(X_RECT_HOMOLOG_BLOCK
						+ (7 * (double) W_RECT_HOMOLOG_BLOCK / 8),
						Q_INSERTION_Y_LBEZIER_LTC
								+ ((Q_INSERTION_Y_POINT - Q_INSERTION_Y_LBEZIER_LTC) / 2));
		contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
				Q_INSERTION_Y_POINT);

		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.stroke();

		if (inRed) {
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		} else {
			contextBgk.setLineWidth(2);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.stroke();
		}
		contextBgk.setLineWidth(1);

	}

	private void drawBlockQNoGeneGenomicRegionInsertion(String text,
			boolean redrawOnTop, boolean inRed,
			boolean isAnchoredToStartElement, boolean isAnchoredToStopElement) {

		// System.err.println("here");

		// between X_RECT_HOMOLOG_BLOCK+0.5, Y_RECT_HOMOLOG_BLOCK+0.5
		// and X_RECT_HOMOLOG_BLOCK+W_RECT_HOMOLOG_BLOCK,
		// Q_INSERTION_Y_POINT-0.5
		// ((double)Q_INSERTION_Y_POINT-(double)Y_RECT_HOMOLOG_BLOCK+4)
		contextBgk.beginPath();
		contextBgk
				.moveTo(X_RECT_HOMOLOG_BLOCK + 0.5,
						(double) Y_RECT_HOMOLOG_BLOCK
								+ (((double) Q_INSERTION_Y_POINT - (double) Y_RECT_HOMOLOG_BLOCK) / 2));
		contextBgk
				.lineTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
						(double) Y_RECT_HOMOLOG_BLOCK
								+ (((double) Q_INSERTION_Y_POINT - (double) Y_RECT_HOMOLOG_BLOCK) / 2));
		

		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.stroke();

		if (inRed) {
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		} else {
			contextBgk.setLineWidth(2);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.stroke();
		}
		contextBgk.setLineWidth(1);

		if (!redrawOnTop) {
			contextBgk.setFillStyle(redrawColorBlack);
			// contextBgk.fillText(textToShow, xtext, (Y_TEXT/2)-3,
			// SLICE_CANVAS_WIDTH-2);
			contextBgk.setTextAlign(TextAlign.CENTER);
			contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2, (Y_TEXT / 2) - 3,
					SLICE_CANVAS_WIDTH - 14);

			if (isAnchoredToStartElement) {
				contextBgk.setTextAlign(TextAlign.LEFT);
				Double maxTextWidth = (double) ((Y_RECT_HOMOLOG_BLOCK
						+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 2);
				if (maxTextWidth < 1) {
					maxTextWidth = 1.0;
				}
				contextBgk.fillText("5'", 2,
				// Q_INSERTION_Y_ARROW_LC - 2,
						Y_RECT_HOMOLOG_BLOCK, maxTextWidth);
			}
			if (isAnchoredToStopElement) {
				contextBgk.setTextAlign(TextAlign.RIGHT);
				Double maxTextWidth = (double) ((Y_RECT_HOMOLOG_BLOCK
						+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 2);
				if (maxTextWidth < 1) {
					maxTextWidth = 1.0;
				}
				contextBgk.fillText("3'", SLICE_CANVAS_WIDTH - 2,
				// Q_INSERTION_Y_ARROW_LC - 2,
						Y_RECT_HOMOLOG_BLOCK, maxTextWidth);
			}
			contextBgk.setTextAlign(TextAlign.LEFT);

		}

	}

	private void drawBlockQGeneRegionInsertion(String text,
			String bkgColorAsColorString, boolean redrawOnTop, boolean inRed) {

		// point : Q_INSERTION_X_POINT+0.5,Q_INSERTION_Y_POINT+0.5
		// top left : Q_INSERTION_X_LBEZIER_LTC+0.5,
		// Q_INSERTION_Y_LBEZIER_LTC+0.5
		// height : H_RECT_HOMOLOG_BLOCK
		// width : (Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC)
		int widthMaxText = (Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC) - 4;
		if (widthMaxText < 1) {
			widthMaxText = 1;
		}
		int raduisIT = 10;
		// draw path

		if (!redrawOnTop) {
			// draw background
			roundedRect(contextBgk, Q_INSERTION_X_LBEZIER_LTC + 0.5,
					Q_INSERTION_Y_LBEZIER_LTC + 0.5,
					(Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC),
					H_RECT_HOMOLOG_BLOCK - 3, raduisIT);
			contextBgk.closePath();

			CanvasGradient cg = contextBgk.createRadialGradient(
					SLICE_CANVAS_WIDTH / 2, TOTAL_CANVAS_HEIGHT_UPPER_PART / 4,
					0, SLICE_CANVAS_WIDTH / 2,
					TOTAL_CANVAS_HEIGHT_UPPER_PART / 4, SLICE_CANVAS_WIDTH / 2);
			cg.addColorStop(0, "#fff");
			cg.addColorStop(0.5, bkgColorAsColorString);// ori was
														// colorOstrichFeatherForSyntenyQInsertionAsString
			contextBgk.setFillStyle(cg);
			contextBgk.fill();

			contextBgk.setFillStyle(redrawColorBlack);
			// contextBgk.fillText(textToShow, xtext, (Y_TEXT/2)-3,
			// SLICE_CANVAS_WIDTH-2);
			contextBgk.setTextAlign(TextAlign.CENTER);
			contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2,
					((1.2 * Y_TEXT) / 2) - 3, widthMaxText);
			contextBgk.setTextAlign(TextAlign.LEFT);
		}

		roundedRect(contextBgk, Q_INSERTION_X_LBEZIER_LTC + 0.5,
				Q_INSERTION_Y_LBEZIER_LTC + 0.5,
				(Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC),
				H_RECT_HOMOLOG_BLOCK - 3, raduisIT);
		// deal with arraow head
		// contextBgk.beginPath();
		contextBgk.moveTo(Q_INSERTION_X_LBEZIER_LTC + raduisIT + 0.5,
				Q_INSERTION_Y_LBEZIER_LTC + H_RECT_HOMOLOG_BLOCK - 3 + 0.5);
		contextBgk.lineTo(Q_INSERTION_X_POINT + 0.5, Q_INSERTION_Y_POINT + 0.5);
		contextBgk.lineTo(Q_INSERTION_X_LBEZIER_LTC
				+ (Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC)
				- raduisIT, Q_INSERTION_Y_LBEZIER_LTC + H_RECT_HOMOLOG_BLOCK
				- 3 + 0.5);

		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.stroke();

		if (inRed) {
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		} else {
			contextBgk.setLineWidth(1);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.stroke();
		}
	}

	private void drawBlockSGeneRegionInsertion(String text,
			String bkgColorAsColorString, boolean redrawOnTop, boolean inRed) {

		// point : S_INSERTION_X_POINT+0.5,S_INSERTION_Y_POINT+0.5
		// top left : S_INSERTION_X_LBEZIER_LTC+0.5,
		// S_INSERTION_Y_LBEZIER_LTC-H_RECT_HOMOLOG_BLOCK+0.5
		// height : H_RECT_HOMOLOG_BLOCK
		// width : (S_INSERTION_LBEZIER_X_RTC - S_INSERTION_X_LBEZIER_LTC)
		int widthMaxText = (S_INSERTION_LBEZIER_X_RTC - S_INSERTION_X_LBEZIER_LTC) - 4;
		if (widthMaxText < 1) {
			widthMaxText = 1;
		}
		int raduisIT = 10;
		// draw path

		if (!redrawOnTop) {
			// draw background
			roundedRect(contextBgk, S_INSERTION_X_LBEZIER_LTC + 0.5,
					S_INSERTION_Y_LBEZIER_LTC - H_RECT_HOMOLOG_BLOCK + 3 + 0.5,
					(S_INSERTION_LBEZIER_X_RTC - S_INSERTION_X_LBEZIER_LTC),
					H_RECT_HOMOLOG_BLOCK - 3, raduisIT);
			contextBgk.closePath();
			CanvasGradient cg = contextBgk.createRadialGradient(
					SLICE_CANVAS_WIDTH / 2,
					3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4, 0,
					SLICE_CANVAS_WIDTH / 2,
					3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4,
					SLICE_CANVAS_WIDTH / 2);
			cg.addColorStop(0, "#fff");
			cg.addColorStop(0.5, bkgColorAsColorString);// ori was
														// colorFarHorizonForSyntenySInsertionAsString
			contextBgk.setFillStyle(cg);
			contextBgk.fill();

			contextBgk.setFillStyle(redrawColorBlack);
			// contextBgk.fillText(textToShow, xtext, 3*Y_TEXT/2,
			// SLICE_CANVAS_WIDTH-2);
			contextBgk.setTextAlign(TextAlign.CENTER);
			contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2,
					(2.7 * Y_TEXT) / 2, widthMaxText);
			contextBgk.setTextAlign(TextAlign.LEFT);
		}

		roundedRect(contextBgk, S_INSERTION_X_LBEZIER_LTC + 0.5,
				S_INSERTION_Y_LBEZIER_LTC - H_RECT_HOMOLOG_BLOCK + 3 + 0.5,
				(S_INSERTION_LBEZIER_X_RTC - S_INSERTION_X_LBEZIER_LTC),
				H_RECT_HOMOLOG_BLOCK - 3, raduisIT);
		// deal with arraow head
		// contextBgk.beginPath();
		contextBgk.moveTo(S_INSERTION_X_LBEZIER_LTC + raduisIT + 0.5,
				S_INSERTION_Y_LBEZIER_LTC - H_RECT_HOMOLOG_BLOCK + 3 + 0.5);
		contextBgk.lineTo(S_INSERTION_X_POINT + 0.5, S_INSERTION_Y_POINT + 0.5);
		contextBgk.lineTo(S_INSERTION_X_LBEZIER_LTC
				+ (S_INSERTION_LBEZIER_X_RTC - S_INSERTION_X_LBEZIER_LTC)
				- raduisIT, S_INSERTION_Y_LBEZIER_LTC - H_RECT_HOMOLOG_BLOCK
				+ 3 + 0.5);

		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.stroke();

		if (inRed) {
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			contextBgk.stroke();

			contextBgk.setLineWidth(1);
		} else {
			contextBgk.setLineWidth(1);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.stroke();
		}

	}

	private void drawBlockGeneHomology(
			boolean reverse,
			AbsoPropQSGeneHomoItem absoluteProportionQComparableQSGeneHomologyItemSent,
			boolean drawOuterContour, boolean drawBkg,
			boolean drawGeneMatchZone, boolean drawGeneNameUp,
			boolean drawGeneNameDown, boolean drawOtherHomologues,
			boolean inRed, int numberOtherHomologs
	// ,boolean geneHomologyGeneNameUpClicked,boolean
	// geneHomologyGeneNameDownClicked,boolean geneHomologyMatchZoneClicked
	) {

		// corners :
		// corners up left : Q_INSERTION_X_LBEZIER_LTC+0.5,
		// Q_INSERTION_Y_LBEZIER_LTC+0.5
		// width : (Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC)
		// height : (S_INSERTION_Y_ARROW_LC - Q_INSERTION_Y_LBEZIER_LTC)
		// text up gene (normal) : contextBgk.fillText(text,
		// SLICE_CANVAS_WIDTH/2, (Y_TEXT/2)-3, SLICE_CANVAS_WIDTH-2);
		// text down gene :
		// (S_INSERTION_X_ARROW_LMC+0.5,S_INSERTION_Y_ARROW_LMC+0.5);
		// text down : x other : S_INSERTION_LBEZIER_Y_RTC+0.5);
		// baseline up gene match : X_RECT_HOMOLOG_BLOCK+0.5,
		// Y_RECT_HOMOLOG_BLOCK+0.5, W_RECT_HOMOLOG_BLOCK, ....
		// baseline down gene match :
		// S_INSERTION_X_POINT+0.5,S_INSERTION_Y_POINT+0.5);
		int widthMaxText = (Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC) - 10;
		int radiusIT = 15;

		// draw path
		roundedRect(contextBgk, Q_INSERTION_X_LBEZIER_LTC + 0.5,
				Q_INSERTION_Y_LBEZIER_LTC + 3 + 0.5,
				(Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC),
				(S_INSERTION_Y_ARROW_LC - Q_INSERTION_Y_LBEZIER_LTC) - 3,
				radiusIT);
		contextBgk.closePath();

		if (drawBkg) {
			// draw background
			CanvasGradient cg = contextBgk.createRadialGradient(
					Q_INSERTION_X_POINT + 0.5, Q_INSERTION_Y_POINT + 0.5, 0,
					Q_INSERTION_X_POINT + 0.5, Q_INSERTION_Y_POINT + 0.5,
					SLICE_CANVAS_WIDTH);
			cg.addColorStop(0, "#fff");
			cg.addColorStop(
					0.5,
					getBkgColorAssociatedWithColorIDAsString(absoluteProportionQComparableQSGeneHomologyItemSent
							.getQsStyleItem().getBkgColor()));// ori was
																// colorFarHorizonForSyntenySInsertionAsString
			contextBgk.setFillStyle(cg);
			contextBgk.fill();

		}

		if (drawOuterContour) {
			// draw contour
			// clear in white
			contextBgk.setStrokeStyle(redrawColorWhite);
			contextBgk.setLineWidth(5);
			contextBgk.stroke();
			if (inRed) {
				// System.err.println("contour in red");
				contextBgk.setStrokeStyle(redrawColorRed);
				contextBgk.setLineWidth(4);
				contextBgk.stroke();
				contextBgk.setLineWidth(1);
			} else {
				// System.err.println("contour in black");
				contextBgk.setLineWidth(1);
				contextBgk.setStrokeStyle(redrawColorGrey6);
				contextBgk.stroke();
			}
		}

		if (drawGeneMatchZone) {
			// draw match zone

			double xStartMatchZone = Q_INSERTION_X_LBEZIER_LTC + 10 + 0.5;
			double yStartMatchZone = Y_RECT_HOMOLOG_BLOCK + 0.5;
			double widthMatchZone = (Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC) - 20;
			double heightMatchZone = S_INSERTION_Y_POINT - Y_RECT_HOMOLOG_BLOCK;
			contextBgk.setLineWidth(1);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.strokeRect(xStartMatchZone, yStartMatchZone,
					widthMatchZone, heightMatchZone);

			/*
			 * //clear in white contextBgk.setStrokeStyle(redrawColorWhite);
			 * contextBgk.setLineWidth(2);
			 * contextBgk.strokeRect(xStartMatchZone, yStartMatchZone,
			 * widthMatchZone, heightMatchZone);
			 * 
			 * if(inRed){ contextBgk.setStrokeStyle(redrawColorRed);
			 * contextBgk.setLineWidth(4);
			 * contextBgk.strokeRect(xStartMatchZone, yStartMatchZone,
			 * widthMatchZone, heightMatchZone); contextBgk.setLineWidth(1);
			 * }else{ contextBgk.setLineWidth(1);
			 * contextBgk.setStrokeStyle(redrawColorDarkGrey);
			 * contextBgk.strokeRect(xStartMatchZone, yStartMatchZone,
			 * widthMatchZone, heightMatchZone); }
			 */

			drawGeneMatchItem(
					absoluteProportionQComparableQSGeneHomologyItemSent
							.getListLightGeneMatchItem(),
					xStartMatchZone, yStartMatchZone, widthMatchZone,
					heightMatchZone, inRed
			// ,geneHomologyMatchZoneClicked
			);

		}

		if (drawGeneNameUp) {

			if (absoluteProportionQComparableQSGeneHomologyItemSent
					.getQsStyleItem().isStyleAsOrtholog()) {
				// contextBgk.setFont(FONT_SIZE+"px Courier New");
				contextBgk.setFont("bold " + (FONT_SIZE - 1.5) + "px arial");
			}

			contextBgk.setTextAlign(TextAlign.CENTER);

			String strippedLocusQ = absoluteProportionQComparableQSGeneHomologyItemSent
					.getQsQMostSignificantGeneNameAsStrippedString();

			// clear in white
			contextBgk.setFillStyle(redrawColorWhite);
			contextBgk.fillText(strippedLocusQ, SLICE_CANVAS_WIDTH / 2,
					(Y_TEXT / 2) - 3, widthMaxText);

			// if (inRed && geneHomologyGeneNameUpClicked) {
			// contextBgk.setFillStyle(redrawColorRed);
			// } else {
			contextBgk.setFillStyle(redrawColorBlack);
			// }

			// text gene name up
			contextBgk.fillText(strippedLocusQ, SLICE_CANVAS_WIDTH / 2,
					(Y_TEXT / 2) - 3, widthMaxText);
			contextBgk.setTextAlign(TextAlign.LEFT);

			if (absoluteProportionQComparableQSGeneHomologyItemSent
					.getQsStyleItem().isStyleAsOrtholog()) {
				contextBgk.setFont(FONT_SIZE + "px Courier New");
				// contextBgk.setFont("bold "+FONT_SIZE+"px arial");
			}

		}

		if (drawGeneNameDown) {

			if (absoluteProportionQComparableQSGeneHomologyItemSent
					.getQsStyleItem().isStyleAsOrtholog()) {
				// contextBgk.setFont(FONT_SIZE+"px Courier New");
				contextBgk.setFont("bold " + (FONT_SIZE - 1.5) + "px arial");
			}

			contextBgk.setTextBaseline(TextBaseline.BOTTOM);
			contextBgk.setTextAlign(TextAlign.CENTER);

			String strippedLocusS = absoluteProportionQComparableQSGeneHomologyItemSent
					.getQsSMostSignificantGeneNameAsStrippedString();

			// clear in white
			contextBgk.setFillStyle(redrawColorWhite);
			contextBgk.fillText(strippedLocusS, SLICE_CANVAS_WIDTH / 2,
					S_INSERTION_Y_ARROW_LMC, widthMaxText);

			// if (inRed && geneHomologyGeneNameDownClicked) {
			// contextBgk.setFillStyle(redrawColorRed);
			// } else {
			contextBgk.setFillStyle(redrawColorBlack);
			// }

			// text
			contextBgk.fillText(strippedLocusS, SLICE_CANVAS_WIDTH / 2,
					S_INSERTION_Y_ARROW_LMC, widthMaxText);
			contextBgk.setTextAlign(TextAlign.LEFT);
			contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);

			if (absoluteProportionQComparableQSGeneHomologyItemSent
					.getQsStyleItem().isStyleAsOrtholog()) {
				contextBgk.setFont(FONT_SIZE + "px Courier New");
				// contextBgk.setFont("bold "+FONT_SIZE+"px arial");
			}

		}

		if (drawOtherHomologues) {
			// text down : x other : S_INSERTION_LBEZIER_Y_RTC
			// limit clear up : S_INSERTION_Y_ARROW_LMC +3 +0.5
			// limit clear down : S_INSERTION_LBEZIER_Y_RTC
			// contextBgk.strokeRect(Q_INSERTION_X_LBEZIER_LTC+5+0.5,
			// S_INSERTION_Y_ARROW_LMC +3 +0.5, widthMaxText+5+0.5,
			// (S_INSERTION_LBEZIER_Y_RTC)-(S_INSERTION_Y_ARROW_LMC +3 +0.5));
			if (numberOtherHomologs > 0) {
				contextBgk.setTextBaseline(TextBaseline.BOTTOM);
				contextBgk.setTextAlign(TextAlign.CENTER);

				// clear in white
				contextBgk.setFillStyle(redrawColorWhite);
				contextBgk.fillText("other matchs:" + numberOtherHomologs,
						SLICE_CANVAS_WIDTH / 2, (TOTAL_CANVAS_HEIGHT_UPPER_PART
								- PADDING + 2), widthMaxText);

				// if(inRed){
				// contextBgk.setFillStyle(redrawColorRed);
				// }else{
				// contextBgk.setFillStyle(redrawColorBlack);
				// }

				// text
				contextBgk.setFillStyle(redrawColorBlack);
				contextBgk.fillText("other matchs:" + numberOtherHomologs,
						SLICE_CANVAS_WIDTH / 2, (TOTAL_CANVAS_HEIGHT_UPPER_PART
								- PADDING + 2), widthMaxText);
				contextBgk.setTextAlign(TextAlign.LEFT);
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
			}

		}

	}

	private void drawGeneMatchItem(
			ArrayList<LightGeneMatchItem> listLightGeneMatchItem,
			double xStartMatchZone, double yStartMatchZone,
			double widthMatchZone, double heightMatchZone, boolean inRed
	// ,boolean geneHomologyMatchZoneClicked
	) {

		// baseline up gene match : X_RECT_HOMOLOG_BLOCK+0.5,
		// Y_RECT_HOMOLOG_BLOCK+0.5, W_RECT_HOMOLOG_BLOCK, ....
		// baseline down gene match :
		// S_INSERTION_X_POINT+0.5,S_INSERTION_Y_POINT+0.5);
		contextBgk.setLineWidth(1);
		// if (inRed && geneHomologyMatchZoneClicked) {
		// contextBgk.setStrokeStyle(redrawColorRed);
		// contextBgk.setFillStyle(redrawColorLightCoral);
		// } else {
		contextBgk.setStrokeStyle(redrawColorBlack);
		//contextBgk.setFillStyle(redrawColorDarkGrey);
		// }

		for (int i = 0; i < listLightGeneMatchItem.size(); i++) {
			LightGeneMatchItem lgmiIT = listLightGeneMatchItem.get(i);
			
			if(lgmiIT.getIdentity() > 85){
				contextBgk.setFillStyle(redrawColorBlack);
			} else if (lgmiIT.getIdentity() > 50) {
				contextBgk.setFillStyle(redrawColorGrey4);
			} else if (lgmiIT.getIdentity() > 35) {
				contextBgk.setFillStyle(redrawColorGrey6);
			} else if (lgmiIT.getIdentity() > 23) {
				contextBgk.setFillStyle(redrawColorGrey9);
			} else {
				contextBgk.setFillStyle(redrawColorGreyC);
			}
			
			double qStartLgmiIT = (xStartMatchZone)
					+ ((widthMatchZone) * lgmiIT.getQFirstFrac());
			double qStopLgmiIT = (xStartMatchZone)
					+ ((widthMatchZone) * (lgmiIT.getQFirstFrac() + lgmiIT
							.getQAlignFrac()));
			double sStartLgmiIT = (xStartMatchZone)
					+ ((widthMatchZone) * lgmiIT.getSFirstFrac());
			double sStopLgmiIT = (xStartMatchZone)
					+ ((widthMatchZone) * (lgmiIT.getSFirstFrac() + lgmiIT
							.getSAlignFrac()));

			contextBgk.beginPath();
			contextBgk.moveTo(qStartLgmiIT, yStartMatchZone + 0.5);
			contextBgk.lineTo(sStartLgmiIT, yStartMatchZone + heightMatchZone
					- 1 + 0.5);
			contextBgk.lineTo(sStopLgmiIT, yStartMatchZone + heightMatchZone
					- 1 + 0.5);
			contextBgk.lineTo(qStopLgmiIT, yStartMatchZone + 0.5);
			contextBgk.closePath();

			contextBgk.fill();
			contextBgk.stroke();

		}

	}

	private void drawBlockQGeneHomologyMissingTargetS(
			AbsoPropQGeneInserItem absoluteProportionQComparableQGeneInsertionItemSent,
			boolean missingTargetIsAtRight, boolean redrawOnTop, boolean inRed,
			int numberOtherHomologs) {

		String strippedLocus = absoluteProportionQComparableQGeneInsertionItemSent
				.getQMostSignificantGeneNameAsStrippedString();

		drawBlockSyntenyMissingTarget(
				getBkgColorAssociatedWithColorIDAsString(absoluteProportionQComparableQGeneInsertionItemSent
						.getqStyleItem().getBkgColor()),
				missingTargetIsAtRight, false, strippedLocus, redrawOnTop,
				inRed, numberOtherHomologs);

	}

	private void drawBlockSGeneHomologyMissingTargetQ(
			AbsoPropSGeneInserItem absoluteProportionSComparableSGeneInsertionItemSent,
			boolean missingTargetIsAtRight, boolean redrawOnTop, boolean inRed,
			int numberOtherHomologs) {

		String strippedLocus = absoluteProportionSComparableSGeneInsertionItemSent
				.getsMostSignificantGeneNameAsStrippedString();

		drawBlockSyntenyMissingTarget(
				getBkgColorAssociatedWithColorIDAsString(absoluteProportionSComparableSGeneInsertionItemSent
						.getsStyleItem().getBkgColor()),
				missingTargetIsAtRight, true, strippedLocus, redrawOnTop,
				inRed, numberOtherHomologs);

	}

	private void drawBlockPartial(String colorBkg, boolean partialLeft,
			boolean partialRight, boolean drawAtTop, boolean drawAtBottom,
			String text, boolean redrawOnTop, boolean inRed) {

		int yCorrectForMiddleUOrBottom = -1;

		// according to block type put it in the middle, or up or bottom
		if (drawAtTop) {
			// draw at top
			yCorrectForMiddleUOrBottom = -(H_RECT_HOMOLOG_BLOCK / 2) - 3;
		} else if (drawAtBottom) {
			// draw at bottom
			yCorrectForMiddleUOrBottom = (H_RECT_HOMOLOG_BLOCK / 2) + 3;
		} else {
			// draw in middle
			yCorrectForMiddleUOrBottom = 0;
		}

		int yStartLinearGradient = TOTAL_CANVAS_HEIGHT_UPPER_PART / 2;
		int xStopLinearGradient = -1;
		int yStopLinearGradient = TOTAL_CANVAS_HEIGHT_UPPER_PART / 2;
		int xArrowPoint = -1;
		int widthArrowPoint = H_RECT_HOMOLOG_BLOCK / 2;
		double xTextStart = -1;

		if (!redrawOnTop) {

			// draw background et text

			if (partialLeft && partialRight) {
				// partial left and right
				int xStartLinearGradient = 0;
				xStopLinearGradient = SLICE_CANVAS_WIDTH;
				CanvasGradient cg = contextBgk.createLinearGradient(
						xStartLinearGradient, yStartLinearGradient,
						xStopLinearGradient, yStopLinearGradient);
				cg.addColorStop(0, "#fff");
				cg.addColorStop(0.5, colorBkg);
				cg.addColorStop(1, "#fff");
				contextBgk.setFillStyle(cg);
				contextBgk
						.fillRect(X_RECT_HOMOLOG_BLOCK + 0.5,
								Y_RECT_HOMOLOG_BLOCK
										+ yCorrectForMiddleUOrBottom + 0.5,
								W_RECT_HOMOLOG_BLOCK, H_RECT_HOMOLOG_BLOCK);

			} else if (partialRight) {
				// partial right
				int xStartLinearGradient = SLICE_CANVAS_WIDTH / 2;
				xStopLinearGradient = SLICE_CANVAS_WIDTH;
				CanvasGradient cg = contextBgk.createLinearGradient(
						xStartLinearGradient, yStartLinearGradient,
						xStopLinearGradient, yStopLinearGradient);
				cg.addColorStop(0, colorBkg);
				cg.addColorStop(0.5, "#fff");
				contextBgk.setFillStyle(cg);
				contextBgk
						.fillRect(X_RECT_HOMOLOG_BLOCK + 0.5,
								Y_RECT_HOMOLOG_BLOCK
										+ yCorrectForMiddleUOrBottom + 0.5,
								W_RECT_HOMOLOG_BLOCK, H_RECT_HOMOLOG_BLOCK);

				// draw text
				xTextStart = X_RECT_HOMOLOG_BLOCK + 2;
				contextBgk.setFillStyle(redrawColorGrey6);
				contextBgk.fillText(text, xTextStart, Y_TEXT
						+ yCorrectForMiddleUOrBottom - 3,
						(W_RECT_HOMOLOG_BLOCK / 2) - 3);

			} else if (partialLeft) {
				// partial left
				int xStartLinearGradient = SLICE_CANVAS_WIDTH / 2;
				xStopLinearGradient = 0;
				CanvasGradient cg = contextBgk.createLinearGradient(
						xStartLinearGradient, yStartLinearGradient,
						xStopLinearGradient, yStopLinearGradient);
				cg.addColorStop(0, colorBkg);
				cg.addColorStop(0.5, "#fff");
				contextBgk.setFillStyle(cg);
				contextBgk
						.fillRect(X_RECT_HOMOLOG_BLOCK + 0.5,
								Y_RECT_HOMOLOG_BLOCK
										+ yCorrectForMiddleUOrBottom + 0.5,
								W_RECT_HOMOLOG_BLOCK, H_RECT_HOMOLOG_BLOCK);

				// draw text
				xTextStart = (SLICE_CANVAS_WIDTH / 2) + 2;
				contextBgk.setFillStyle(redrawColorGrey6);
				contextBgk.fillText(text, xTextStart, Y_TEXT
						+ yCorrectForMiddleUOrBottom - 3,
						(W_RECT_HOMOLOG_BLOCK / 2) - 3);
			}

		}

		contextBgk.beginPath();
		// draw path contour
		contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK + 0.5, Y_RECT_HOMOLOG_BLOCK
				+ yCorrectForMiddleUOrBottom + 0.5);
		contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK + 0.5,
				Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom + 0.5);
		if (!partialRight) {
			contextBgk.lineTo(
					X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ H_RECT_HOMOLOG_BLOCK + 0.5);
		} else {
			contextBgk.moveTo(
					X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ H_RECT_HOMOLOG_BLOCK + 0.5);
		}
		contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + 0.5, Y_RECT_HOMOLOG_BLOCK
				+ yCorrectForMiddleUOrBottom + H_RECT_HOMOLOG_BLOCK + 0.5);
		if (!partialLeft) {
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + 0.5, Y_RECT_HOMOLOG_BLOCK
					+ yCorrectForMiddleUOrBottom + 0.5);
		}

		// draw path bck arrow

		if (partialLeft) {
			// partial left
			xArrowPoint = X_RECT_HOMOLOG_BLOCK + 1;
			contextBgk.moveTo((SLICE_CANVAS_WIDTH / 2) + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.lineTo(xArrowPoint + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.moveTo(xArrowPoint + widthArrowPoint + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom
							+ (widthArrowPoint / 2) + 0.5);
			contextBgk.lineTo(xArrowPoint + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.lineTo(xArrowPoint + widthArrowPoint + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom
							- (widthArrowPoint / 2) + 0.5);
		}
		if (partialRight) {
			// partial right
			xArrowPoint = X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK - 2;
			widthArrowPoint = -widthArrowPoint;
			contextBgk.moveTo((SLICE_CANVAS_WIDTH / 2) + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.lineTo(xArrowPoint + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.moveTo(xArrowPoint + widthArrowPoint + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom
							+ (widthArrowPoint / 2) + 0.5);
			contextBgk.lineTo(xArrowPoint + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.lineTo(xArrowPoint + widthArrowPoint + 0.5,
					(TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
							+ yCorrectForMiddleUOrBottom
							- (widthArrowPoint / 2) + 0.5);
		}

		// draw contour
		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.setLineCap(LineCap.SQUARE);
		contextBgk.stroke();
		contextBgk.setLineCap(LineCap.BUTT);

		if (inRed) {
			// stroke in red
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			// contextBgk.strokeRect(X_RECT_HOMOLOG_BLOCK+0.5,
			// Y_RECT_HOMOLOG_BLOCK+yCorrectForMiddleUOrBottom+0.5,
			// W_RECT_HOMOLOG_BLOCK, H_RECT_HOMOLOG_BLOCK);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		} else {
			// stroke in black
			contextBgk.setLineWidth(1);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			// contextBgk.strokeRect(X_RECT_HOMOLOG_BLOCK+0.5,
			// Y_RECT_HOMOLOG_BLOCK+yCorrectForMiddleUOrBottom+0.5,
			// W_RECT_HOMOLOG_BLOCK, H_RECT_HOMOLOG_BLOCK);
			contextBgk.stroke();
		}

	}

	// String colorBkg, boolean partialRight, boolean drawAtTop, boolean
	// drawAtBottom, String numberGenes, boolean redrawOnTop, boolean inRed
	private void drawBlockSyntenyMissingTarget(String colorBkg,
			boolean missingTargetIsAtRight, boolean SMissingTargetQ,
			String text, boolean redrawOnTop, boolean inRed,
			int numberVariations) {

		// twisted rectangle
		int xStartPoint = -1;
		int yStartPoint = -1;
		int xStopPoint = -1;
		int yStopPoint = -1;
		int yText = -1;

		// arrow
		int xArrowButt = -1;
		int xArrowHead = -1;
		// int yArrowButt = -1;
		// int xLenghtArrow = -1;
		// int yLenghtArrow = -1;

		// int correctShiftXtext = UNIT_WIDTH/2;
		int xArrowHeadLenght = (UNIT_WIDTH / 2);
		int yArrowHeadLenght = (UNIT_WIDTH / 2);

		if (SMissingTargetQ) {
			// SMissingTargetQ
			// yStartPoint = S_INSERTION_Y_POINT;
			// yStopPoint = S_INSERTION_LBEZIER_Y_RTC;
			if (numberVariations > 0) {
				yStartPoint = S_INSERTION_LBEZIER_Y_RTC;
				yStopPoint = S_INSERTION_Y_POINT
						+ ((S_INSERTION_LBEZIER_Y_RTC - S_INSERTION_Y_POINT) / 2);
			} else {
				yStartPoint = S_INSERTION_LBEZIER_Y_RTC;
				yStopPoint = S_INSERTION_Y_POINT;
			}
			yText = (3 * Y_TEXT) / 2;
			// yLenghtArrow = -(yStartPoint-yStopPoint)/2;
			yArrowHeadLenght = -yArrowHeadLenght;
		} else {
			// QMissingTargetS
			if (numberVariations > 0) {
				yStartPoint = Q_INSERTION_LBEZIER_Y_RTC;
				yStopPoint = Q_INSERTION_Y_POINT
						- ((Q_INSERTION_Y_POINT - Q_INSERTION_LBEZIER_Y_RTC) / 2);
			} else {
				yStartPoint = Q_INSERTION_LBEZIER_Y_RTC;
				yStopPoint = Q_INSERTION_Y_POINT;
			}
			yText = (Y_TEXT / 2) - 3;
			// yLenghtArrow = (yStopPoint-yStartPoint)/2;
		}

		if (missingTargetIsAtRight) {
			// missingTargetIsAt right
			xStartPoint = X_RECT_HOMOLOG_BLOCK;
			xStopPoint = X_RECT_HOMOLOG_BLOCK + UNIT_WIDTH;
			xArrowHeadLenght = -xArrowHeadLenght;
			// correctShiftXtext = -correctShiftXtext;
		} else {
			// missingTargetIsAt left
			xStartPoint = X_RECT_HOMOLOG_BLOCK + UNIT_WIDTH;
			xStopPoint = X_RECT_HOMOLOG_BLOCK;

		}
		xArrowButt = xStartPoint + (W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH) / 2;
		xArrowHead = xStopPoint + (W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH) / 2;

		if (!redrawOnTop) {

			// draw path shape
			contextBgk.beginPath();
			contextBgk.moveTo(xStartPoint, yStartPoint);
			int cpx = xStartPoint;
			int cpy = yStopPoint;
			double cp1x = xStartPoint + (cpx - xStartPoint)
					* RATIO_BEZIZE_CURVE_TO;
			double cp1y = yStartPoint + (cpy - yStartPoint)
					* RATIO_BEZIZE_CURVE_TO;
			double cp2x = cp1x + (xStopPoint - xStartPoint)
					* (1 - RATIO_BEZIZE_CURVE_TO);
			double cp2y = cp1y + (yStopPoint - yStartPoint)
					* (1 - RATIO_BEZIZE_CURVE_TO);
			// and now call cubic Bezier curve to function
			contextBgk.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, xStopPoint,
					yStopPoint);
			contextBgk.lineTo(xStopPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH,
					yStopPoint);
			int cpxBis = xStartPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH;
			int cpyBis = yStopPoint;
			double cp1xBis = (xStopPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH)
					+ (cpxBis - (xStopPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH))
					* RATIO_BEZIZE_CURVE_TO;
			double cp1yBis = yStopPoint + (cpyBis - yStopPoint)
					* RATIO_BEZIZE_CURVE_TO;
			double cp2xBis = cp1xBis
					+ ((xStartPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH) - (xStopPoint
							+ W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH))
					* (1 - RATIO_BEZIZE_CURVE_TO);
			double cp2yBis = cp1yBis + (yStartPoint - yStopPoint)
					* (1 - RATIO_BEZIZE_CURVE_TO);
			// and now call cubic Bezier curve to function
			contextBgk.bezierCurveTo(cp1xBis, cp1yBis, cp2xBis, cp2yBis,
					(xStartPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH),
					yStartPoint);
			contextBgk.closePath();

			// draw background
			CanvasGradient cg = contextBgk.createLinearGradient(xStartPoint,
					yStartPoint, xStartPoint, yStopPoint);
			cg.addColorStop(0, colorBkg);
			cg.addColorStop(0.65, "#fff");
			contextBgk.setFillStyle(cg);
			contextBgk.fill();

			// draw text
			/*
			 * String textToShow = ""; if(numberGenes.length()>10){ textToShow =
			 * numberGenes.substring(0, 10)+".."; }else{ textToShow =
			 * numberGenes; }
			 */
			// System.out.println("text lenght = "+text.length());

			// int xtext = (int)Math.floor((SLICE_CANVAS_WIDTH -
			// (text.length()*UNIT_WIDTH*CORRECT_FOR_TEXT_CENTER))/2);
			contextBgk.setFillStyle(redrawColorGrey6);
			// contextBgk.fillText(text, xtext+correctShiftXtext, yText,
			// SLICE_CANVAS_WIDTH-2);

			if (missingTargetIsAtRight) {
				contextBgk.setTextAlign(TextAlign.LEFT);
				contextBgk.fillText(text, 5, yText, SLICE_CANVAS_WIDTH - 14);

			} else {
				contextBgk.setTextAlign(TextAlign.RIGHT);
				contextBgk.fillText(text, SLICE_CANVAS_WIDTH - 5, yText,
						SLICE_CANVAS_WIDTH - 2);
				contextBgk.setTextAlign(TextAlign.LEFT);
			}

			if (numberVariations > 0) {
				// draw text other
				contextBgk.setFillStyle(redrawColorBlack);
				if (SMissingTargetQ) {
					contextBgk.setTextBaseline(TextBaseline.BOTTOM);
				} else {
					contextBgk.setTextBaseline(TextBaseline.TOP);
				}
				contextBgk.setTextAlign(TextAlign.CENTER);
				int widthMaxText = (Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC) - 10;
				contextBgk.fillText("Off shoots:" + numberVariations,
						SLICE_CANVAS_WIDTH / 2, yStopPoint, widthMaxText);
				contextBgk.setTextAlign(TextAlign.LEFT);
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
			}
		}

		// draw path shape
		contextBgk.beginPath();
		contextBgk.moveTo(xStartPoint, yStartPoint);
		double ratio = 0.5522847498307933984022516322796; // 0.5522847498307933984022516322796
															// if the Bezier is
															// approximating an
															// elliptic arc with
															// best fitting //
															// original : 2.0 /
															// 3.0
		int cpx = xStartPoint;
		int cpy = yStopPoint;
		double cp1x = xStartPoint + (cpx - xStartPoint) * ratio;
		double cp1y = yStartPoint + (cpy - yStartPoint) * ratio;
		double cp2x = cp1x + (xStopPoint - xStartPoint) * (1 - ratio);
		double cp2y = cp1y + (yStopPoint - yStartPoint) * (1 - ratio);
		// and now call cubic Bezier curve to function
		contextBgk
				.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, xStopPoint, yStopPoint);
		contextBgk.moveTo(xStopPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH,
				yStopPoint);
		int cpxBis = xStartPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH;
		int cpyBis = yStopPoint;
		double cp1xBis = (xStopPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH)
				+ (cpxBis - (xStopPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH))
				* ratio;
		double cp1yBis = yStopPoint + (cpyBis - yStopPoint) * ratio;
		double cp2xBis = cp1xBis
				+ ((xStartPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH) - (xStopPoint
						+ W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH)) * (1 - ratio);
		double cp2yBis = cp1yBis + (yStartPoint - yStopPoint) * (1 - ratio);
		// and now call cubic Bezier curve to function
		contextBgk.bezierCurveTo(cp1xBis, cp1yBis, cp2xBis, cp2yBis,
				(xStartPoint + W_RECT_HOMOLOG_BLOCK - UNIT_WIDTH), yStartPoint);
		// contextBgk.closePath();

		// draw path arrow
		// contextBgk.beginPath();
		contextBgk.moveTo(xArrowButt + 0.5, yStartPoint + 0.5);
		// contextBgk.lineTo(xArrowButt+0.5, yStopPoint-(UNIT_WIDTH/2)+0.5);
		contextBgk.lineTo(xArrowHead + 0.5, yStopPoint + 0.5);
		contextBgk
				.moveTo(xArrowHead + xArrowHeadLenght + 0.5, yStopPoint + 0.5);
		contextBgk.lineTo(xArrowHead + 0.5, yStopPoint + 0.5);
		contextBgk
				.lineTo(xArrowHead + 0.5, yStopPoint - yArrowHeadLenght + 0.5);
		// contextBgk.closePath();
		// draw arrow
		// contextBgk.setLineWidth(3);
		// contextBgk.setStrokeStyle(redrawColorDarkGrey);
		// contextBgk.stroke();

		// draw contour
		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineCap(LineCap.SQUARE);
		contextBgk.setLineWidth(5);
		contextBgk.stroke();
		contextBgk.setLineCap(LineCap.BUTT);

		if (inRed) {
			// stroke in red
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		} else {
			// stroke in black
			contextBgk.setLineWidth(1);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.stroke();
		}

	}

	private void drawBlockSNoGeneGenomicRegionInsertion(String text,
			boolean redrawOnTop, boolean inRed, boolean anchoredToPrevious,
			boolean anchoredToNext, boolean isAnchoredToStartElement,
			boolean isAnchoredToStopElement) {

		// System.err.println("here");

		// between X_RECT_HOMOLOG_BLOCK+0.5, Y_RECT_HOMOLOG_BLOCK+0.5
		// and X_RECT_HOMOLOG_BLOCK+W_RECT_HOMOLOG_BLOCK,
		// Q_INSERTION_Y_POINT-0.5
		// ((double)(Y_RECT_HOMOLOG_BLOCK+H_RECT_HOMOLOG_BLOCK)-4-(double)S_INSERTION_Y_POINT)
		contextBgk.beginPath();
		if (anchoredToPrevious) {
			contextBgk
					.moveTo(X_RECT_HOMOLOG_BLOCK + 0.5,
							(double) (S_INSERTION_Y_POINT)
									+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			// contextBgk
			// .quadraticCurveTo(
			// X_RECT_HOMOLOG_BLOCK
			// + (double) (W_RECT_HOMOLOG_BLOCK / 8),
			// (double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK),
			// X_RECT_HOMOLOG_BLOCK
			// + ((double) W_RECT_HOMOLOG_BLOCK / 4),
			// (S_INSERTION_Y_POINT)
			// + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) -
			// (double) S_INSERTION_Y_POINT) / 2));
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ ((double) W_RECT_HOMOLOG_BLOCK / 4),
							(S_INSERTION_Y_POINT)
									+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			if (anchoredToNext) {
				// contextBgk
				// .quadraticCurveTo(
				// X_RECT_HOMOLOG_BLOCK
				// + (3 * (double) W_RECT_HOMOLOG_BLOCK / 8),
				// S_INSERTION_Y_POINT,
				// X_RECT_HOMOLOG_BLOCK
				// + ((double) W_RECT_HOMOLOG_BLOCK / 2),
				// (S_INSERTION_Y_POINT)
				// + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) -
				// (double) S_INSERTION_Y_POINT) / 2));
				contextBgk
						.lineTo(X_RECT_HOMOLOG_BLOCK
								+ ((double) W_RECT_HOMOLOG_BLOCK / 2),
								(S_INSERTION_Y_POINT)
										+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			} else {
				// contextBgk
				// .quadraticCurveTo(
				// X_RECT_HOMOLOG_BLOCK
				// + (5 * (double) W_RECT_HOMOLOG_BLOCK / 16),
				// S_INSERTION_Y_POINT,
				// X_RECT_HOMOLOG_BLOCK
				// + (3 * (double) W_RECT_HOMOLOG_BLOCK / 8),
				// (S_INSERTION_Y_POINT)
				// + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) -
				// (double) S_INSERTION_Y_POINT) / 2));
				contextBgk
						.lineTo(X_RECT_HOMOLOG_BLOCK
								+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 8),
								(S_INSERTION_Y_POINT)
										+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));

			}
		}

		if (anchoredToNext) {
			if (anchoredToPrevious) {
				contextBgk
						.moveTo(X_RECT_HOMOLOG_BLOCK
								+ ((double) W_RECT_HOMOLOG_BLOCK / 2),
								(S_INSERTION_Y_POINT)
										+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
				// contextBgk
				// .quadraticCurveTo(
				// X_RECT_HOMOLOG_BLOCK
				// + (5 * (double) W_RECT_HOMOLOG_BLOCK / 8),
				// (double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK),
				// X_RECT_HOMOLOG_BLOCK
				// + (3 * (double) W_RECT_HOMOLOG_BLOCK / 4),
				// (S_INSERTION_Y_POINT)
				// + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) -
				// (double) S_INSERTION_Y_POINT) / 2));
				contextBgk
						.lineTo(X_RECT_HOMOLOG_BLOCK
								+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 4),
								(S_INSERTION_Y_POINT)
										+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			} else {
				contextBgk
						.moveTo(X_RECT_HOMOLOG_BLOCK
								+ (5 * (double) W_RECT_HOMOLOG_BLOCK / 8),
								(S_INSERTION_Y_POINT)
										+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
				// contextBgk
				// .quadraticCurveTo(
				// X_RECT_HOMOLOG_BLOCK
				// + (11 * (double) W_RECT_HOMOLOG_BLOCK / 16),
				// (double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK),
				// X_RECT_HOMOLOG_BLOCK
				// + (3 * (double) W_RECT_HOMOLOG_BLOCK / 4),
				// (S_INSERTION_Y_POINT)
				// + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) -
				// (double) S_INSERTION_Y_POINT) / 2));
				contextBgk
						.lineTo(X_RECT_HOMOLOG_BLOCK
								+ (3 * (double) W_RECT_HOMOLOG_BLOCK / 4),
								(S_INSERTION_Y_POINT)
										+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			}
			// contextBgk
			// .quadraticCurveTo(
			// X_RECT_HOMOLOG_BLOCK
			// + (7 * (double) W_RECT_HOMOLOG_BLOCK / 8),
			// S_INSERTION_Y_POINT,
			// X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
			// (S_INSERTION_Y_POINT)
			// + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) -
			// (double) S_INSERTION_Y_POINT) / 2));
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
							(S_INSERTION_Y_POINT)
									+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
		}

		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.stroke();

		if (inRed) {
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		} else {
			contextBgk.setLineWidth(2);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.stroke();
		}

		contextBgk.setLineWidth(1);
		contextBgk.setStrokeStyle(redrawColorGrey6);

		if (!redrawOnTop) {
			contextBgk.setFillStyle(redrawColorBlack);
			// contextBgk.fillText(textToShow, xtext, (Y_TEXT/2)-3,
			// SLICE_CANVAS_WIDTH-2);
			if (anchoredToPrevious && anchoredToNext) {
				contextBgk.setTextAlign(TextAlign.CENTER);
				contextBgk.setTextBaseline(TextBaseline.TOP);
				contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2,
						(Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK),
						SLICE_CANVAS_WIDTH - 14);
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
				if (isAnchoredToStopElement) {
					contextBgk.setTextAlign(TextAlign.RIGHT);
					Double maxTextWidth = (double) ((Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 2);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk
							.fillText(
									"3'",
									SLICE_CANVAS_WIDTH - 2,
									S_INSERTION_Y_POINT
											+ (Y_RECT_HOMOLOG_BLOCK
													+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT)
											- 4, maxTextWidth);
				}
				if (isAnchoredToStartElement) {
					contextBgk.setTextAlign(TextAlign.LEFT);
					Double maxTextWidth = (double) ((Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 2);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk
							.fillText(
									"5'",
									2,
									S_INSERTION_Y_POINT
											+ (Y_RECT_HOMOLOG_BLOCK
													+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT)
											- 4, maxTextWidth);
				}
			} else if (anchoredToPrevious) {
				contextBgk.setTextAlign(TextAlign.LEFT);
				// 2.8 * Y_TEXT
				contextBgk.setTextBaseline(TextBaseline.TOP);
				contextBgk.fillText(text, 5,
						(Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK),
						(SLICE_CANVAS_WIDTH / 3));
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);

				if (!isAnchoredToStopElement) {
					contextBgk
							.strokeRect(
									X_RECT_HOMOLOG_BLOCK
											+ ((double) W_RECT_HOMOLOG_BLOCK / 2)
											- 2
											- (Y_RECT_HOMOLOG_BLOCK
													+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT),
									S_INSERTION_Y_POINT,
									(Y_RECT_HOMOLOG_BLOCK
											+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 1,
									(Y_RECT_HOMOLOG_BLOCK
											+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 1);
				} else {
					contextBgk.setTextAlign(TextAlign.LEFT);
					Double maxTextWidth = (double) (Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk
							.fillText(
									"3'",
									X_RECT_HOMOLOG_BLOCK
											+ ((double) W_RECT_HOMOLOG_BLOCK / 2)
											- 2
											- (Y_RECT_HOMOLOG_BLOCK
													+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT)
											+ 1,
									S_INSERTION_Y_POINT
											+ (Y_RECT_HOMOLOG_BLOCK
													+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT)
											- 4, maxTextWidth);
				}
			} else if (anchoredToNext) {
				contextBgk.setTextAlign(TextAlign.RIGHT);
				contextBgk.setTextBaseline(TextBaseline.TOP);
				contextBgk.fillText(text, SLICE_CANVAS_WIDTH - 5,
						(Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK),
						(SLICE_CANVAS_WIDTH / 3));
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
				if (!isAnchoredToStartElement) {
					contextBgk
							.strokeRect(
									X_RECT_HOMOLOG_BLOCK
											+ ((double) W_RECT_HOMOLOG_BLOCK / 2)
											+ 2,
									S_INSERTION_Y_POINT,
									(Y_RECT_HOMOLOG_BLOCK
											+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 1,
									(Y_RECT_HOMOLOG_BLOCK
											+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 1);
				} else {
					contextBgk.setTextAlign(TextAlign.LEFT);
					Double maxTextWidth = (double) (Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk
							.fillText(
									"5'",
									X_RECT_HOMOLOG_BLOCK
											+ ((double) W_RECT_HOMOLOG_BLOCK / 2)
											+ 2,
									S_INSERTION_Y_POINT
											+ (Y_RECT_HOMOLOG_BLOCK
													+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT)
											- 4, maxTextWidth);
				}
			}
			contextBgk.setTextAlign(TextAlign.LEFT);

		}

	}

	private void drawBlockSGenomicRegionInsertion(String text, String color,
			boolean redrawOnTop, boolean inRed, boolean anchoredToPrevious,
			boolean anchoredToNext, boolean isAnchoredToStartElement,
			boolean isAnchoredToStopElement) {

		if (anchoredToPrevious && anchoredToNext) {
			// draw path
			contextBgk.beginPath();
			contextBgk
					.moveTo(X_RECT_HOMOLOG_BLOCK + 0.5,
							(double) (S_INSERTION_Y_POINT)
									+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			contextBgk
					.lineTo(X_RECT_HOMOLOG_BLOCK
							+ ((double) W_RECT_HOMOLOG_BLOCK / 2),
							(S_INSERTION_Y_POINT)
									+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			// contextBgk.moveTo(S_INSERTION_X_POINT + 0.5,
			// S_INSERTION_Y_POINT + 0.5);
			// contextBgk.lineTo(S_INSERTION_X_ARROW_LC + 0.5,
			// S_INSERTION_Y_ARROW_LC + 0.5);
			// contextBgk.lineTo(S_INSERTION_X_ARROW_LMC + 0.5,
			// S_INSERTION_Y_ARROW_LMC + 0.5);
			// contextBgk.bezierCurveTo(S_INSERTION_X_LBEZIER_cp1x + 0.5,
			// S_INSERTION_Y_LBEZIER_cp1y + 0.5,
			// S_INSERTION_X_LBEZIER_cp2x + 0.5,
			// S_INSERTION_Y_LBEZIER_cp2y + 0.5,
			// S_INSERTION_X_LBEZIER_LTC + 0.5,
			// S_INSERTION_Y_LBEZIER_LTC + 0.5);
			// contextBgk.lineTo(S_INSERTION_LBEZIER_X_RTC + 0.5,
			// S_INSERTION_LBEZIER_Y_RTC + 0.5);
			// contextBgk.bezierCurveTo(S_INSERTION_X_RBEZIER_cp1x + 0.5,
			// S_INSERTION_Y_RBEZIER_cp1y + 0.5,
			// S_INSERTION_X_RBEZIER_cp2x + 0.5,
			// S_INSERTION_Y_RBEZIER_cp2y + 0.5,
			// S_INSERTION_X_RBEZIER_RBC + 0.5,
			// S_INSERTION_Y_RBEZIER_RBC + 0.5);
			// contextBgk.lineTo(S_INSERTION_X_ARROW_RC + 0.5,
			// S_INSERTION_Y_ARROW_RC + 0.5);
			// contextBgk.closePath();

			if (!redrawOnTop) {
				// draw background
				CanvasGradient cg = contextBgk.createRadialGradient(
						SLICE_CANVAS_WIDTH / 2,
						3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4, 0,
						SLICE_CANVAS_WIDTH / 2,
						3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4,
						SLICE_CANVAS_WIDTH / 2);
				cg.addColorStop(0, "#fff");
				cg.addColorStop(0.5, color);// ori was
											// colorFarHorizonForSyntenySInsertionAsString
				contextBgk.setFillStyle(cg);
				// contextBgk.fill();
				contextBgk
						.fillRect(
								(S_INSERTION_X_LBEZIER_LTC + 0.5),
								((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
								((S_INSERTION_LBEZIER_X_RTC + 0.5) / 8)
										- (S_INSERTION_X_LBEZIER_LTC + 0.5),
								(S_INSERTION_LBEZIER_Y_RTC + 0.5)
										- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
										- 3);

				// draw text
				contextBgk.setFillStyle(redrawColorBlack);
				contextBgk.setTextAlign(TextAlign.CENTER);
				contextBgk.setTextBaseline(TextBaseline.TOP);
				contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2,
						(Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - 2,
						SLICE_CANVAS_WIDTH - 14);
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);

				if (isAnchoredToStopElement) {
					contextBgk.setTextAlign(TextAlign.RIGHT);
					Double maxTextWidth = (double) ((Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 2);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk
							.fillText(
									"3'",
									SLICE_CANVAS_WIDTH - 2,
									S_INSERTION_Y_POINT
											+ (Y_RECT_HOMOLOG_BLOCK
													+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT)
											- 4, maxTextWidth);
				}
				if (isAnchoredToStartElement) {
					contextBgk.setTextAlign(TextAlign.LEFT);
					Double maxTextWidth = (double) ((Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 2);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk
							.fillText(
									"5'",
									2,
									S_INSERTION_Y_POINT
											+ (Y_RECT_HOMOLOG_BLOCK
													+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT)
											- 4, maxTextWidth);
				}
				contextBgk.setTextAlign(TextAlign.LEFT);
			}

			// draw contour
			// clear in white
			contextBgk.setStrokeStyle(redrawColorWhite);
			contextBgk.setLineWidth(5);
			contextBgk.stroke();
			contextBgk
					.strokeRect(
							(S_INSERTION_X_LBEZIER_LTC + 0.5),
							((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
							((S_INSERTION_LBEZIER_X_RTC + 0.5) / 8)
									- (S_INSERTION_X_LBEZIER_LTC + 0.5),
							(S_INSERTION_LBEZIER_Y_RTC + 0.5)
									- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
									- 3);
			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
				contextBgk.setLineWidth(4);
				contextBgk.stroke();
				contextBgk
						.strokeRect(
								(S_INSERTION_X_LBEZIER_LTC + 0.5),
								((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
								((S_INSERTION_LBEZIER_X_RTC + 0.5) / 8)
										- (S_INSERTION_X_LBEZIER_LTC + 0.5),
								(S_INSERTION_LBEZIER_Y_RTC + 0.5)
										- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
										- 3);

				contextBgk.setLineWidth(1);
			} else {
				contextBgk.setLineWidth(1);
				contextBgk.setStrokeStyle(redrawColorGrey6);
				contextBgk.stroke();
				contextBgk
						.strokeRect(
								(S_INSERTION_X_LBEZIER_LTC + 0.5),
								((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
								((S_INSERTION_LBEZIER_X_RTC + 0.5) / 8)
										- (S_INSERTION_X_LBEZIER_LTC + 0.5),
								(S_INSERTION_LBEZIER_Y_RTC + 0.5)
										- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
										- 3);

			}

		} else if (anchoredToPrevious) {

			// draw path
			contextBgk.beginPath();
			contextBgk
					.moveTo((S_INSERTION_X_ARROW_LC / 2) + 3 + 0.5,
							(double) (S_INSERTION_Y_POINT)
									+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			contextBgk
					.lineTo((S_INSERTION_X_ARROW_RC / 2) - 5 + 0.5,
							(double) (S_INSERTION_Y_POINT)
									+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			// contextBgk.moveTo((S_INSERTION_X_POINT / 2) + 0.5,
			// S_INSERTION_Y_POINT + 0.5);
			// contextBgk.lineTo((S_INSERTION_X_ARROW_LC / 2) + 3 + 0.5,
			// S_INSERTION_Y_ARROW_LC + 0.5);
			// contextBgk.lineTo((S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5,
			// S_INSERTION_Y_ARROW_LMC + 0.5);
			// contextBgk.bezierCurveTo(
			// (S_INSERTION_X_LBEZIER_cp1x / 2) + 1.5 + 0.5,
			// S_INSERTION_Y_LBEZIER_cp1y + 0.5,
			// (S_INSERTION_X_LBEZIER_cp2x / 2) + 1.5 + 0.5,
			// S_INSERTION_Y_LBEZIER_cp2y + 0.5,
			// (S_INSERTION_X_LBEZIER_LTC / 2) + 1.5 + 0.5,
			// S_INSERTION_Y_LBEZIER_LTC + 0.5);
			// contextBgk.lineTo((S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5,
			// S_INSERTION_LBEZIER_Y_RTC + 0.5);
			// contextBgk.bezierCurveTo(
			// (S_INSERTION_X_RBEZIER_cp1x / 2) - 2.5 + 0.5,
			// S_INSERTION_Y_RBEZIER_cp1y + 0.5,
			// (S_INSERTION_X_RBEZIER_cp2x / 2) - 2.5 + 0.5,
			// S_INSERTION_Y_RBEZIER_cp2y + 0.5,
			// (S_INSERTION_X_RBEZIER_RBC / 2) - 2.5 + 0.5,
			// S_INSERTION_Y_RBEZIER_RBC + 0.5);
			// contextBgk.lineTo((S_INSERTION_X_ARROW_RC / 2) - 5 + 0.5,
			// S_INSERTION_Y_ARROW_RC + 0.5);
			// contextBgk.closePath();

			if (!redrawOnTop) {
				// draw background
				CanvasGradient cg = contextBgk.createRadialGradient(
						SLICE_CANVAS_WIDTH / 4,
						3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4, 0,
						SLICE_CANVAS_WIDTH / 4,
						3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4,
						SLICE_CANVAS_WIDTH / 4);
				cg.addColorStop(0, "#fff");
				cg.addColorStop(0.5, color);// ori was
											// colorFarHorizonForSyntenySInsertionAsString
				contextBgk.setFillStyle(cg);
				// contextBgk.fill();
				contextBgk
						.fillRect(
								((S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
								((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
								((S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5)
										- ((S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
								(S_INSERTION_LBEZIER_Y_RTC + 0.5)
										- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
										- 3);

				// draw text
				contextBgk.setFillStyle(redrawColorBlack);
				contextBgk.setTextAlign(TextAlign.CENTER);
				contextBgk.setTextBaseline(TextBaseline.TOP);
				contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 4,
						(Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - 4,
						(SLICE_CANVAS_WIDTH / 5));
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);

				if (isAnchoredToStartElement) {
					contextBgk.setTextAlign(TextAlign.LEFT);
					Double maxTextWidth = (double) (Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk.fillText("5'", X_RECT_HOMOLOG_BLOCK
							+ ((double) W_RECT_HOMOLOG_BLOCK / 2) + 2,
							(Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK - 2),
							maxTextWidth);
				}
				if (isAnchoredToStopElement) {
					contextBgk.setTextAlign(TextAlign.LEFT);
					Double maxTextWidth = (double) (Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT - 3);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk
							.fillText(
									"3'",
									(X_RECT_HOMOLOG_BLOCK
											+ ((double) W_RECT_HOMOLOG_BLOCK / 2)
											+ 1 - (Y_RECT_HOMOLOG_BLOCK
											+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT)),
									(Y_RECT_HOMOLOG_BLOCK
											+ H_RECT_HOMOLOG_BLOCK - 4),
									maxTextWidth);
				}
				contextBgk.setTextAlign(TextAlign.LEFT);
			}

			// draw contour
			// clear in white
			contextBgk.setStrokeStyle(redrawColorWhite);
			contextBgk.setLineWidth(5);
			contextBgk.stroke();
			contextBgk
					.strokeRect(
							((S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
							((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
							((S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5)
									- ((S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
							(S_INSERTION_LBEZIER_Y_RTC + 0.5)
									- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
									- 3);
			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
				contextBgk.setLineWidth(4);
				contextBgk.stroke();
				contextBgk
						.strokeRect(
								((S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
								((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
								((S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5)
										- ((S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
								(S_INSERTION_LBEZIER_Y_RTC + 0.5)
										- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
										- 3);
				contextBgk.setLineWidth(1);
			} else {
				contextBgk.setLineWidth(1);
				contextBgk.setStrokeStyle(redrawColorGrey6);
				contextBgk.stroke();
				contextBgk
						.strokeRect(
								((S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
								((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
								((S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5)
										- ((S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
								(S_INSERTION_LBEZIER_Y_RTC + 0.5)
										- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
										- 3);
			}

		} else if (anchoredToNext) {

			// draw path
			contextBgk.beginPath();
			contextBgk
					.moveTo((SLICE_CANVAS_WIDTH / 2)
							+ (S_INSERTION_X_ARROW_LC / 2) + 3 + 0.5,
							(double) (S_INSERTION_Y_POINT)
									+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			contextBgk
					.lineTo((SLICE_CANVAS_WIDTH / 2)
							+ (S_INSERTION_X_ARROW_RC / 2) - 5 + 0.5,
							(double) (S_INSERTION_Y_POINT)
									+ (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2));
			// contextBgk.moveTo((SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_POINT / 2) + 0.5,
			// S_INSERTION_Y_POINT + 0.5);
			// contextBgk.lineTo((SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_ARROW_LC / 2) + 3 + 0.5,
			// S_INSERTION_Y_ARROW_LC + 0.5);
			// contextBgk.lineTo((SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5,
			// S_INSERTION_Y_ARROW_LMC + 0.5);
			// contextBgk.bezierCurveTo((SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_LBEZIER_cp1x / 2) + 1.5 + 0.5,
			// S_INSERTION_Y_LBEZIER_cp1y + 0.5, (SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_LBEZIER_cp2x / 2) + 1.5 + 0.5,
			// S_INSERTION_Y_LBEZIER_cp2y + 0.5, (SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_LBEZIER_LTC / 2) + 1.5 + 0.5,
			// S_INSERTION_Y_LBEZIER_LTC + 0.5);
			// contextBgk.lineTo((SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5,
			// S_INSERTION_LBEZIER_Y_RTC + 0.5);
			// contextBgk.bezierCurveTo((SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_RBEZIER_cp1x / 2) - 2.5 + 0.5,
			// S_INSERTION_Y_RBEZIER_cp1y + 0.5, (SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_RBEZIER_cp2x / 2) - 2.5 + 0.5,
			// S_INSERTION_Y_RBEZIER_cp2y + 0.5, (SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_RBEZIER_RBC / 2) - 2.5 + 0.5,
			// S_INSERTION_Y_RBEZIER_RBC + 0.5);
			// contextBgk.lineTo((SLICE_CANVAS_WIDTH / 2)
			// + (S_INSERTION_X_ARROW_RC / 2) - 5 + 0.5,
			// S_INSERTION_Y_ARROW_RC + 0.5);
			// contextBgk.closePath();

			if (!redrawOnTop) {
				// draw background
				CanvasGradient cg = contextBgk.createRadialGradient(
						(3 * SLICE_CANVAS_WIDTH / 4),
						3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4, 0,
						(3 * SLICE_CANVAS_WIDTH / 4),
						3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4,
						(3 * SLICE_CANVAS_WIDTH / 4));
				cg.addColorStop(0, "#fff");
				cg.addColorStop(0.5, color);// ori was
											// colorFarHorizonForSyntenySInsertionAsString
				contextBgk.setFillStyle(cg);
				// contextBgk.fill();
				contextBgk
						.fillRect(
								((SLICE_CANVAS_WIDTH / 2)
										+ (S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
								((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
								((SLICE_CANVAS_WIDTH / 2)
										+ (S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5)
										- ((SLICE_CANVAS_WIDTH / 2)
												+ (S_INSERTION_X_ARROW_LMC / 2)
												+ 1.5 + 0.5),
								(S_INSERTION_LBEZIER_Y_RTC + 0.5)
										- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
										- 3);

				// draw text
				contextBgk.setFillStyle(redrawColorBlack);
				// contextBgk.fillText(textToShow, xtext, 3*Y_TEXT/2,
				// SLICE_CANVAS_WIDTH-2);
				contextBgk.setTextAlign(TextAlign.CENTER);

				contextBgk.setTextBaseline(TextBaseline.TOP);
				contextBgk.fillText(text, (SLICE_CANVAS_WIDTH / 2)
						+ (SLICE_CANVAS_WIDTH / 4),
						(Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - 4,
						(SLICE_CANVAS_WIDTH / 5));
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);

				if (isAnchoredToStartElement) {
					contextBgk.setTextAlign(TextAlign.LEFT);
					Double maxTextWidth = (double) (Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk.fillText("5'", X_RECT_HOMOLOG_BLOCK
							+ ((double) W_RECT_HOMOLOG_BLOCK / 2) + 2,
							(Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK - 2),
							maxTextWidth);
				}
				if (isAnchoredToStopElement) {
					contextBgk.setTextAlign(TextAlign.LEFT);
					Double maxTextWidth = (double) (Y_RECT_HOMOLOG_BLOCK
							+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT);
					if (maxTextWidth < 1) {
						maxTextWidth = 1.0;
					}
					contextBgk
							.fillText(
									"3'",
									X_RECT_HOMOLOG_BLOCK
											+ ((double) W_RECT_HOMOLOG_BLOCK / 2)
											- 2
											- (Y_RECT_HOMOLOG_BLOCK
													+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT)
											+ 1, (Y_RECT_HOMOLOG_BLOCK
											+ H_RECT_HOMOLOG_BLOCK - 2),
									maxTextWidth);
				}
				contextBgk.setTextAlign(TextAlign.LEFT);
			}

			// draw contour
			// clear in white
			contextBgk.setStrokeStyle(redrawColorWhite);
			contextBgk.setLineWidth(5);
			contextBgk.stroke();
			contextBgk
					.strokeRect(
							((SLICE_CANVAS_WIDTH / 2)
									+ (S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
							((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
							((SLICE_CANVAS_WIDTH / 2)
									+ (S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5)
									- ((SLICE_CANVAS_WIDTH / 2)
											+ (S_INSERTION_X_ARROW_LMC / 2)
											+ 1.5 + 0.5),
							(S_INSERTION_LBEZIER_Y_RTC + 0.5)
									- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
									- 3);

			if (inRed) {
				contextBgk.setStrokeStyle(redrawColorRed);
				contextBgk.setLineWidth(4);
				contextBgk.stroke();
				contextBgk
						.strokeRect(
								((SLICE_CANVAS_WIDTH / 2)
										+ (S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
								((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
								((SLICE_CANVAS_WIDTH / 2)
										+ (S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5)
										- ((SLICE_CANVAS_WIDTH / 2)
												+ (S_INSERTION_X_ARROW_LMC / 2)
												+ 1.5 + 0.5),
								(S_INSERTION_LBEZIER_Y_RTC + 0.5)
										- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
										- 3);

				contextBgk.setLineWidth(1);
			} else {
				contextBgk.setLineWidth(1);
				contextBgk.setStrokeStyle(redrawColorGrey6);
				contextBgk.stroke();
				contextBgk
						.strokeRect(
								((SLICE_CANVAS_WIDTH / 2)
										+ (S_INSERTION_X_ARROW_LMC / 2) + 1.5 + 0.5),
								((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2)),
								((SLICE_CANVAS_WIDTH / 2)
										+ (S_INSERTION_LBEZIER_X_RTC / 2) - 2.5 + 0.5)
										- ((SLICE_CANVAS_WIDTH / 2)
												+ (S_INSERTION_X_ARROW_LMC / 2)
												+ 1.5 + 0.5),
								(S_INSERTION_LBEZIER_Y_RTC + 0.5)
										- ((double) (S_INSERTION_Y_POINT) + (((double) (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK) - (double) S_INSERTION_Y_POINT) / 2))
										- 3);
			}
		}
	}

	private void drawBlockSyntenyReverseHomolog(String text,
			boolean redrawOnTop, boolean inRed, int numberVariations,
			boolean isContainedWithinAnotherMotherSynteny,
			boolean isSprungOffAnotherMotherSynteny,
			boolean isMotherOfSprungOffSyntenies,
			boolean isMotherOfContainedOtherSyntenies) {

		// draw path
		contextBgk.beginPath();
		contextBgk.moveTo(REVERSE_X_TLC + 0.5, REVERSE_Y_TLC + 0.5);
		contextBgk.bezierCurveTo(REVERSE_X_1_cp1x + 0.5,
				REVERSE_Y_1_cp1y + 0.5, REVERSE_X_1_cp2x + 0.5,
				REVERSE_Y_1_cp2y + 0.5, REVERSE_X_BRC + 0.5,
				REVERSE_Y_BRC + 0.5);
		contextBgk.lineTo(REVERSE_X_TRC + 0.5, REVERSE_Y_TRC + 0.5);
		contextBgk.bezierCurveTo(REVERSE_X_2_cp1x + 0.5,
				REVERSE_Y_2_cp1y + 0.5, REVERSE_X_2_cp2x + 0.5,
				REVERSE_Y_2_cp2y + 0.5, REVERSE_X_BLC + 0.5,
				REVERSE_Y_BLC + 0.5);
		contextBgk.closePath();

		// draw background
		CanvasGradient cg = contextBgk.createRadialGradient(
				SLICE_CANVAS_WIDTH / 2, TOTAL_CANVAS_HEIGHT_UPPER_PART / 2, 0,
				SLICE_CANVAS_WIDTH / 2, TOTAL_CANVAS_HEIGHT_UPPER_PART / 2,
				SLICE_CANVAS_WIDTH / 2);
		cg.addColorStop(0, "#fff");
		cg.addColorStop(0.5, colorGreenVeilForSyntenyReverseBlockAsString);
		contextBgk.setFillStyle(cg);
		contextBgk.fill();

		// draw contour
		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.stroke();

		if (inRed) {
			CanvasGradient cg2 = contextBgk.createRadialGradient(
					SLICE_CANVAS_WIDTH / 2, TOTAL_CANVAS_HEIGHT_UPPER_PART / 2,
					0, SLICE_CANVAS_WIDTH / 2,
					TOTAL_CANVAS_HEIGHT_UPPER_PART / 2, SLICE_CANVAS_WIDTH / 2);
			cg2.addColorStop(0, "#fff");
			cg2.addColorStop(0.5, "#FF0000");
			contextBgk.setStrokeStyle(cg2);
			contextBgk.setLineWidth(4);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		} else {

			contextBgk.setLineWidth(1);
			CanvasGradient cg2 = contextBgk.createRadialGradient(
					SLICE_CANVAS_WIDTH / 2, TOTAL_CANVAS_HEIGHT_UPPER_PART / 2,
					0, SLICE_CANVAS_WIDTH / 2,
					TOTAL_CANVAS_HEIGHT_UPPER_PART / 2, SLICE_CANVAS_WIDTH / 2);
			cg2.addColorStop(0, "#fff");
			cg2.addColorStop(0.5, "#696969");
			contextBgk.setStrokeStyle(cg2);
			contextBgk.stroke();
		}

		// draw text
		/*
		 * String textToShow = ""; if(text.length()>10){ textToShow =
		 * text.substring(0, 10)+".."; }else{ textToShow = text; }
		 */
		// System.out.println("text lenght = "+text.length());
		// int xtext = (int)Math.floor((SLICE_CANVAS_WIDTH -
		// (textToShow.length()*UNIT_WIDTH*CORRECT_FOR_TEXT_CENTER))/2);
		contextBgk.setFillStyle(redrawColorBlack);
		// contextBgk.fillText(textToShow, xtext, Y_TEXT-3,
		// SLICE_CANVAS_WIDTH-2);
		contextBgk.setTextAlign(TextAlign.CENTER);
		// text box
		contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2, Y_TEXT - 3,
				SLICE_CANVAS_WIDTH - 2);
		if (numberVariations > 0) {
			// text variations
			contextBgk.setTextBaseline(TextBaseline.BOTTOM);
			contextBgk.setFillStyle(redrawColorDarkGreen);
			contextBgk.fillText("Off shoots: " + numberVariations,
					SLICE_CANVAS_WIDTH / 2, Y_TEXT + H_RECT_HOMOLOG_BLOCK,
					SLICE_CANVAS_WIDTH - 14);
			contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
		}
		contextBgk.setTextAlign(TextAlign.LEFT);

	}

	private void drawBlockQGenomicRegionInsertion(final String text,
			final String bkgColor, boolean redrawOnTop, boolean inRed,
			boolean isAnchoredToStartElement, boolean isAnchoredToStopElement) {

		// init image

		// draw path
		contextBgk.beginPath();
		contextBgk
				.moveTo(X_RECT_HOMOLOG_BLOCK + 0.5,
						(double) Y_RECT_HOMOLOG_BLOCK
								+ (((double) Q_INSERTION_Y_POINT - (double) Y_RECT_HOMOLOG_BLOCK) / 2));
		contextBgk
				.lineTo(X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK,
						(double) Y_RECT_HOMOLOG_BLOCK
								+ (((double) Q_INSERTION_Y_POINT - (double) Y_RECT_HOMOLOG_BLOCK) / 2));

		// contextBgk.moveTo(Q_INSERTION_X_POINT + 0.5, Q_INSERTION_Y_POINT +
		// 0.5);
		// contextBgk.lineTo(Q_INSERTION_X_ARROW_LC + 0.5,
		// Q_INSERTION_Y_ARROW_LC + 0.5);
		// contextBgk.lineTo(Q_INSERTION_X_ARROW_LMC + 0.5,
		// Q_INSERTION_Y_ARROW_LMC + 0.5);
		// contextBgk.bezierCurveTo(Q_INSERTION_X_LBEZIER_cp1x + 0.5,
		// Q_INSERTION_Y_LBEZIER_cp1y + 0.5,
		// Q_INSERTION_X_LBEZIER_cp2x + 0.5,
		// Q_INSERTION_Y_LBEZIER_cp2y + 0.5,
		// Q_INSERTION_X_LBEZIER_LTC + 0.5,
		// Q_INSERTION_Y_LBEZIER_LTC + 0.5);
		// contextBgk.lineTo(Q_INSERTION_LBEZIER_X_RTC + 0.5,
		// Q_INSERTION_LBEZIER_Y_RTC + 0.5);
		// contextBgk.bezierCurveTo(Q_INSERTION_X_RBEZIER_cp1x + 0.5,
		// Q_INSERTION_Y_RBEZIER_cp1y + 0.5,
		// Q_INSERTION_X_RBEZIER_cp2x + 0.5,
		// Q_INSERTION_Y_RBEZIER_cp2y + 0.5,
		// Q_INSERTION_X_RBEZIER_RBC + 0.5,
		// Q_INSERTION_Y_RBEZIER_RBC + 0.5);
		// contextBgk.lineTo(Q_INSERTION_X_ARROW_RC + 0.5,
		// Q_INSERTION_Y_ARROW_RC + 0.5);
		// contextBgk.closePath();

		if (!redrawOnTop) {
			// draw background
			// System.err.println("here");
			CanvasGradient cg = contextBgk.createRadialGradient(
					SLICE_CANVAS_WIDTH / 2, TOTAL_CANVAS_HEIGHT_UPPER_PART / 4,
					0, SLICE_CANVAS_WIDTH / 2,
					TOTAL_CANVAS_HEIGHT_UPPER_PART / 4, SLICE_CANVAS_WIDTH / 2);
			cg.addColorStop(0, "#fff");
			cg.addColorStop(0.5, bkgColor);// ori was
											// colorOstrichFeatherForSyntenyQInsertionAsString
			contextBgk.setFillStyle(cg);
			// contextBgk.fill();
			contextBgk
					.fillRect(
							Q_INSERTION_X_LBEZIER_LTC,
							Q_INSERTION_Y_LBEZIER_LTC,
							(Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC),
							((double) Y_RECT_HOMOLOG_BLOCK + (((double) Q_INSERTION_Y_POINT - (double) Y_RECT_HOMOLOG_BLOCK) / 2))
									- Q_INSERTION_Y_LBEZIER_LTC);

			// draw text
			contextBgk.setFillStyle(redrawColorBlack);
			// contextBgk.fillText(textToShow, xtext, (Y_TEXT/2)-3,
			// SLICE_CANVAS_WIDTH-2);
			contextBgk.setTextAlign(TextAlign.CENTER);
			contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2, (Y_TEXT / 2) - 3,
					((SLICE_CANVAS_WIDTH * 2) / 3));

			// if (isAnchoredToStartElement) {
			// contextBgk.setTextAlign(TextAlign.LEFT);
			// contextBgk
			// .fillText(
			// "5'",
			// 2,
			// Q_INSERTION_Y_POINT - 2,
			// (Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK -
			// S_INSERTION_Y_POINT));
			// }
			// if (isAnchoredToStopElement) {
			// contextBgk.setTextAlign(TextAlign.RIGHT);
			// contextBgk.fillText("3'", SLICE_CANVAS_WIDTH - 2,
			// Q_INSERTION_Y_POINT - 2, (Y_RECT_HOMOLOG_BLOCK
			// + H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT));
			// }

			// NEW
			if (isAnchoredToStartElement) {
				contextBgk.setTextAlign(TextAlign.LEFT);
				Double maxTextWidth = (double) ((Y_RECT_HOMOLOG_BLOCK
						+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 4);
				if (maxTextWidth < 1) {
					maxTextWidth = 1.0;
				}
				contextBgk.fillText("5'", 2,
				// Q_INSERTION_Y_ARROW_LC - 2,
						Y_RECT_HOMOLOG_BLOCK, maxTextWidth);
			}
			if (isAnchoredToStopElement) {
				contextBgk.setTextAlign(TextAlign.RIGHT);
				Double maxTextWidth = (double) ((Y_RECT_HOMOLOG_BLOCK
						+ H_RECT_HOMOLOG_BLOCK - S_INSERTION_Y_POINT) - 4);
				if (maxTextWidth < 1) {
					maxTextWidth = 1.0;
				}
				contextBgk.fillText("3'", SLICE_CANVAS_WIDTH - 2,
				// Q_INSERTION_Y_ARROW_LC - 2,
						Y_RECT_HOMOLOG_BLOCK, maxTextWidth);
			}
			contextBgk.setTextAlign(TextAlign.LEFT);
		}

		// draw contour
		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.stroke();
		contextBgk
				.strokeRect(
						Q_INSERTION_X_LBEZIER_LTC,
						Q_INSERTION_Y_LBEZIER_LTC,
						(Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC),
						((double) Y_RECT_HOMOLOG_BLOCK + (((double) Q_INSERTION_Y_POINT - (double) Y_RECT_HOMOLOG_BLOCK) / 2))
								- Q_INSERTION_Y_LBEZIER_LTC);

		if (inRed) {
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			contextBgk.stroke();
			contextBgk
					.strokeRect(
							Q_INSERTION_X_LBEZIER_LTC,
							Q_INSERTION_Y_LBEZIER_LTC,
							(Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC),
							((double) Y_RECT_HOMOLOG_BLOCK + (((double) Q_INSERTION_Y_POINT - (double) Y_RECT_HOMOLOG_BLOCK) / 2))
									- Q_INSERTION_Y_LBEZIER_LTC);
			contextBgk.setLineWidth(1);
		} else {
			contextBgk.setLineWidth(1);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.stroke();
			contextBgk
					.strokeRect(
							Q_INSERTION_X_LBEZIER_LTC,
							Q_INSERTION_Y_LBEZIER_LTC,
							(Q_INSERTION_LBEZIER_X_RTC - Q_INSERTION_X_LBEZIER_LTC),
							((double) Y_RECT_HOMOLOG_BLOCK + (((double) Q_INSERTION_Y_POINT - (double) Y_RECT_HOMOLOG_BLOCK) / 2))
									- Q_INSERTION_Y_LBEZIER_LTC);

		}

	}

	private void drawBlockSyntenyHomolog(String text, boolean redrawOnTop,
			boolean inRed, int numberVariations,
			boolean isContainedWithinAnotherMotherSynteny,
			boolean isSprungOffAnotherMotherSynteny,
			boolean isMotherOfSprungOffSyntenies,
			boolean isMotherOfContainedOtherSyntenies) {

		if (!redrawOnTop) {
			// draw background
			CanvasGradient cg = contextBgk.createRadialGradient(
					SLICE_CANVAS_WIDTH / 2, TOTAL_CANVAS_HEIGHT_UPPER_PART / 2,
					0, SLICE_CANVAS_WIDTH / 2,
					TOTAL_CANVAS_HEIGHT_UPPER_PART / 2, SLICE_CANVAS_WIDTH / 2);
			cg.addColorStop(0, "#fff");
			cg.addColorStop(0.6,
					colorFrostedPeachForSyntenyHomologBlockAsString);
			contextBgk.setFillStyle(cg);
			contextBgk.fillRect(X_RECT_HOMOLOG_BLOCK + 0.5,
					Y_RECT_HOMOLOG_BLOCK + 0.5, W_RECT_HOMOLOG_BLOCK,
					H_RECT_HOMOLOG_BLOCK);

			// draw text
			/*
			 * String textToShow = ""; if(text.length()>10){ textToShow =
			 * text.substring(0, 10)+".."; }else{ textToShow = text; }
			 */
			// System.out.println("text lenght = "+text.length());
			// int xtext = (int)Math.floor((SLICE_CANVAS_WIDTH -
			// (textToShow.length()*UNIT_WIDTH*CORRECT_FOR_TEXT_CENTER))/2);
			contextBgk.setFillStyle(redrawColorBlack);
			// contextBgk.fillText(textToShow, xtext, Y_TEXT-3,
			// SLICE_CANVAS_WIDTH-2);
			contextBgk.setTextAlign(TextAlign.CENTER);
			// text box
			contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2, Y_TEXT - 3,
					SLICE_CANVAS_WIDTH - 14);
			if (numberVariations > 0) {
				// text variations
				contextBgk.setTextBaseline(TextBaseline.BOTTOM);
				contextBgk.setFillStyle(redrawColorDarkGreen);
				contextBgk.fillText("Off shoots: " + numberVariations,
						SLICE_CANVAS_WIDTH / 2, Y_TEXT + H_RECT_HOMOLOG_BLOCK,
						SLICE_CANVAS_WIDTH - 14);
				contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
			}
			contextBgk.setTextAlign(TextAlign.LEFT);

		}

		// draw contour
		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.strokeRect(X_RECT_HOMOLOG_BLOCK + 0.5,
				Y_RECT_HOMOLOG_BLOCK + 0.5, W_RECT_HOMOLOG_BLOCK,
				H_RECT_HOMOLOG_BLOCK);
		// if(isMotherOfSprungOffSyntenies){
		// contextBgk.beginPath();
		// contextBgk.moveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),
		// Y_RECT_HOMOLOG_BLOCK+0.5);
		// contextBgk.quadraticCurveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),-1,
		// X_RECT_HOMOLOG_BLOCK+0.5+W_RECT_HOMOLOG_BLOCK,-1);
		// contextBgk.stroke();
		// }
		// if(isSprungOffAnotherMotherSynteny){
		// contextBgk.beginPath();
		// contextBgk.moveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),
		// Y_RECT_HOMOLOG_BLOCK+0.5);
		// contextBgk.quadraticCurveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),-1,
		// X_RECT_HOMOLOG_BLOCK+0.5,-1);
		// contextBgk.stroke();
		// }

		if (inRed) {

			// stroke in red
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			contextBgk.strokeRect(X_RECT_HOMOLOG_BLOCK + 0.5,
					Y_RECT_HOMOLOG_BLOCK + 0.5, W_RECT_HOMOLOG_BLOCK,
					H_RECT_HOMOLOG_BLOCK);
			// if(isMotherOfSprungOffSyntenies){
			// contextBgk.beginPath();
			// contextBgk.moveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),
			// Y_RECT_HOMOLOG_BLOCK+0.5);
			// contextBgk.quadraticCurveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),-1,
			// X_RECT_HOMOLOG_BLOCK+0.5+W_RECT_HOMOLOG_BLOCK,-1);
			// contextBgk.stroke();
			// }
			// if(isSprungOffAnotherMotherSynteny){
			// contextBgk.beginPath();
			// contextBgk.moveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),
			// Y_RECT_HOMOLOG_BLOCK+0.5);
			// contextBgk.quadraticCurveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),-1,
			// X_RECT_HOMOLOG_BLOCK+0.5,-1);
			// contextBgk.stroke();
			// }
			contextBgk.setLineWidth(1);

		} else {

			// stroke in black
			contextBgk.setLineWidth(1);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.strokeRect(X_RECT_HOMOLOG_BLOCK + 0.5,
					Y_RECT_HOMOLOG_BLOCK + 0.5, W_RECT_HOMOLOG_BLOCK,
					H_RECT_HOMOLOG_BLOCK);
			// if(isMotherOfSprungOffSyntenies){
			// contextBgk.beginPath();
			// contextBgk.moveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),
			// Y_RECT_HOMOLOG_BLOCK+0.5);
			// contextBgk.quadraticCurveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),-1,
			// X_RECT_HOMOLOG_BLOCK+0.5+W_RECT_HOMOLOG_BLOCK,-1);
			// contextBgk.stroke();
			// }
			// if(isSprungOffAnotherMotherSynteny){
			// contextBgk.beginPath();
			// contextBgk.moveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),
			// Y_RECT_HOMOLOG_BLOCK+0.5);
			// contextBgk.quadraticCurveTo((X_RECT_HOMOLOG_BLOCK+0.5+(W_RECT_HOMOLOG_BLOCK/2)),-1,
			// X_RECT_HOMOLOG_BLOCK+0.5,-1);
			// contextBgk.stroke();
			// }
		}

	}

	private void drawBlockCut(String colorBkg, boolean start,
			boolean drawAtMiddle, boolean drawAtTop, boolean drawAtBottom,
			String text, boolean redrawOnTop, boolean inRed,
			int numberVariations) {

		// X_RECT_HOMOLOG_BLOCK+0.5
		// Y_RECT_HOMOLOG_BLOCK+yCorrectForMiddleUOrBottom+0.5
		// W_RECT_HOMOLOG_BLOCK
		// H_RECT_HOMOLOG_BLOCK
		// UNIT_WIDTH
		// SLICE_CANVAS_WIDTH/2
		// according to block type put it in the middle, or up or bottom

		// clear slice
		contextBgk.setFillStyle(redrawColorWhite);
		if (drawAtTop) {
			contextBgk.fillRect(1, 1, (SLICE_CANVAS_WIDTH - 2),
					TOTAL_CANVAS_HEIGHT_UPPER_PART / 2);
		} else if (drawAtBottom) {
			contextBgk.fillRect(1, TOTAL_CANVAS_HEIGHT_UPPER_PART / 2,
					(SLICE_CANVAS_WIDTH - 2),
					TOTAL_CANVAS_HEIGHT_UPPER_PART / 2);
		} else {
			contextBgk.fillRect(1, 1, (SLICE_CANVAS_WIDTH - 2),
					TOTAL_CANVAS_HEIGHT_UPPER_PART - 2);
		}

		int yCorrectForMiddleUOrBottom = -1;
		int yText = -1;

		if (drawAtTop) {
			// draw at top
			yCorrectForMiddleUOrBottom = -(H_RECT_HOMOLOG_BLOCK / 2) - 3;
			yText = (Y_TEXT / 2) - 3;
		} else if (drawAtBottom) {
			// draw at bottom
			yCorrectForMiddleUOrBottom = (H_RECT_HOMOLOG_BLOCK / 2) + 3;
			yText = (3 * Y_TEXT) / 2;
		} else if (drawAtMiddle) {
			// draw in middle
			yCorrectForMiddleUOrBottom = 0;
			yText = Y_TEXT - 3;
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error drawBlockGenomicRegionCut : neither drawAtMiddle, drawAtTop or drawAtBottom"));
		}
		// draw path
		contextBgk.beginPath();
		if (start) {
			// cut start
			contextBgk.moveTo(X_RECT_HOMOLOG_BLOCK + 0.5, Y_RECT_HOMOLOG_BLOCK
					+ yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) + UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) - UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ (H_RECT_HOMOLOG_BLOCK / 4) + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) + UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ (2 * H_RECT_HOMOLOG_BLOCK / 4) + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) - UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ (3 * H_RECT_HOMOLOG_BLOCK / 4) + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) + UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ H_RECT_HOMOLOG_BLOCK + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK + 0.5, Y_RECT_HOMOLOG_BLOCK
					+ yCorrectForMiddleUOrBottom + H_RECT_HOMOLOG_BLOCK + 0.5);
			contextBgk.closePath();
		} else {
			// cut stop
			contextBgk.moveTo(
					X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) - UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) + UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ (H_RECT_HOMOLOG_BLOCK / 4) + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) - UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ (2 * H_RECT_HOMOLOG_BLOCK / 4) + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) + UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ (3 * H_RECT_HOMOLOG_BLOCK / 4) + 0.5);
			contextBgk.lineTo(X_RECT_HOMOLOG_BLOCK
					+ ((SLICE_CANVAS_WIDTH / 2) - UNIT_WIDTH) + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ H_RECT_HOMOLOG_BLOCK + 0.5);
			contextBgk.lineTo(
					X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK + 0.5,
					Y_RECT_HOMOLOG_BLOCK + yCorrectForMiddleUOrBottom
							+ H_RECT_HOMOLOG_BLOCK + 0.5);
			contextBgk.closePath();
		}

		// if(!redrawOnTop){
		// draw background
		CanvasGradient cg;
		if (start) {
			cg = contextBgk.createLinearGradient(X_RECT_HOMOLOG_BLOCK + 0.5,
					Y_RECT_HOMOLOG_BLOCK + 0.5, (SLICE_CANVAS_WIDTH / 2)
							+ UNIT_WIDTH, Y_RECT_HOMOLOG_BLOCK + 0.5);
			cg.addColorStop(0, colorBkg);
			cg.addColorStop(0.8, "#fff");
			contextBgk.setFillStyle(cg);
		} else {
			cg = contextBgk.createLinearGradient(X_RECT_HOMOLOG_BLOCK
					+ W_RECT_HOMOLOG_BLOCK + 0.5, Y_RECT_HOMOLOG_BLOCK + 0.5,
					(SLICE_CANVAS_WIDTH / 2) - UNIT_WIDTH,
					Y_RECT_HOMOLOG_BLOCK + 0.5);
			cg.addColorStop(0, colorBkg);
			cg.addColorStop(0.8, "#fff");
			contextBgk.setFillStyle(cg);
		}
		contextBgk.fill();

		// }

		// draw contour
		// clear in white
		contextBgk.setStrokeStyle(redrawColorWhite);
		contextBgk.setLineWidth(5);
		contextBgk.stroke();

		if (inRed) {
			contextBgk.setStrokeStyle(redrawColorRed);
			contextBgk.setLineWidth(4);
			contextBgk.stroke();
			contextBgk.setLineWidth(1);
		} else {
			contextBgk.setLineWidth(1);
			contextBgk.setStrokeStyle(redrawColorGrey6);
			contextBgk.stroke();

		}

		// draw text
		// System.out.println("text lenght = "+text.length());
		// int xtext = (int)Math.floor((SLICE_CANVAS_WIDTH -
		// (textToShow.length()*UNIT_WIDTH*CORRECT_FOR_TEXT_CENTER))/2);
		if (drawAtTop) {
			contextBgk.setTextBaseline(TextBaseline.MIDDLE);
		} else if (drawAtBottom) {
			contextBgk.setTextBaseline(TextBaseline.BOTTOM);
		}
		contextBgk.setTextAlign(TextAlign.CENTER);
		contextBgk.setFillStyle(redrawColorWhite);
		contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2, yText,
				SLICE_CANVAS_WIDTH - 14);
		contextBgk.setFillStyle(redrawColorBlack);
		contextBgk.fillText(text, SLICE_CANVAS_WIDTH / 2, yText,
				SLICE_CANVAS_WIDTH - 14);
		contextBgk.setTextAlign(TextAlign.LEFT);
		contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
		if (numberVariations > 0 && drawAtMiddle) {
			// text variations
			contextBgk.setTextAlign(TextAlign.CENTER);
			contextBgk.setTextBaseline(TextBaseline.BOTTOM);
			contextBgk.setFillStyle(redrawColorDarkGreen);
			contextBgk.fillText("Off shoots: " + numberVariations,
					SLICE_CANVAS_WIDTH / 2, Y_TEXT + H_RECT_HOMOLOG_BLOCK,
					SLICE_CANVAS_WIDTH - 14);
			contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
			contextBgk.setTextAlign(TextAlign.LEFT);
		}

	}

	public void drawLoading() {

		// Window.alert("Loading...");

		// System.err.println("draw loading");
		if (CALCULATE_CANVAS_PARAMETERS) {
			// System.err.println("here");
			calculateCanvasParameters();
		}

		if (setCoordinateSpace) {
			initCoordinateSpace();
		}

		// System.out.println("totalCanvasWidth :"+totalCanvasWidth);
		if (TOTAL_CANVAS_WIDTH <= 0) {
			//System.out.println("drawLoading : TOTAL_CANVAS_WIDTH is null");
			//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("drawLoading : TOTAL_CANVAS_WIDTH is null"));
			return;
		}

		restoreToOriginCoordinateSpace();

		clearWholeCanvas();

		// draw text
		contextBgk.setTextBaseline(TextBaseline.MIDDLE);
		contextBgk.setTextAlign(TextAlign.CENTER);
		contextBgk.setFillStyle(redrawColorGreen);
		contextBgk.fillText("Loading, please wait...", TOTAL_CANVAS_WIDTH / 2,
				TOTAL_CANVAS_HEIGHT_UPPER_PART / 2);
		contextBgk.fillText("Loading, please wait...", TOTAL_CANVAS_WIDTH / 2,
				TOTAL_CANVAS_HEIGHT_UPPER_PART
						+ (TOTAL_CANVAS_HEIGHT_UPPER_PART / 2));
		contextBgk.setTextAlign(TextAlign.LEFT);
		contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
		contextBgk.setFillStyle(redrawColorBlack);

	}

	public void drawMssgOnCanvas(String text, boolean inRed) {
		restoreToOriginCoordinateSpace();
		clearWholeCanvas();
		// draw text
		contextBgk.setTextBaseline(TextBaseline.MIDDLE);
		contextBgk.setTextAlign(TextAlign.CENTER);
		if (inRed) {
			contextBgk.setFillStyle(redrawColorRed);
		} else {
			contextBgk.setFillStyle(redrawColorGrey6);
		}
		contextBgk.fillText(text, TOTAL_CANVAS_WIDTH / 2,
				TOTAL_CANVAS_HEIGHT_UPPER_PART / 2, (TOTAL_CANVAS_WIDTH - 10));
		contextBgk.fillText(text, TOTAL_CANVAS_WIDTH / 2,
				TOTAL_CANVAS_HEIGHT_UPPER_PART
						+ (TOTAL_CANVAS_HEIGHT_UPPER_PART / 2),
				(TOTAL_CANVAS_WIDTH - 10));
		contextBgk.setTextAlign(TextAlign.LEFT);
		contextBgk.setTextBaseline(TextBaseline.ALPHABETIC);
		contextBgk.setFillStyle(redrawColorBlack);
	}

	private void checkWetherToAllowForLeftRightSymbolNavigationOrNot() {

		if (associatedGenomePanel.getGenomePanelItem()
				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() <= 0) {
			navigateLeftSpeed1.setEnabled(false);
			navigateLeftSpeed2.setEnabled(false);
			navigateLeftSpeed3.setEnabled(false);
		} else if (associatedGenomePanel.getGenomePanelItem()
				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() <= Math
				.floor(NUMBER_SLICE_WIDTH / 2) - 1) {
			navigateLeftSpeed1.setEnabled(true);
			navigateLeftSpeed2.setEnabled(false);
			navigateLeftSpeed3.setEnabled(false);
		} else if (associatedGenomePanel.getGenomePanelItem()
				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() < NUMBER_SLICE_WIDTH) {
			navigateLeftSpeed1.setEnabled(true);
			navigateLeftSpeed2.setEnabled(true);
			navigateLeftSpeed3.setEnabled(false);
		} else {
			navigateLeftSpeed1.setEnabled(true);
			navigateLeftSpeed2.setEnabled(true);
			navigateLeftSpeed3.setEnabled(true);
		}

		if (associatedGenomePanel.getGenomePanelItem()
				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() >= associatedGenomePanel
				.getGenomePanelItem()
				.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
				.size()
				- NUMBER_SLICE_WIDTH) {
			// indexStartBirdSynteny = alBlockType.size()-1-NUMBER_SLICE_WIDTH;
			navigateRightSpeed1.setEnabled(false);
			navigateRightSpeed2.setEnabled(false);
			navigateRightSpeed3.setEnabled(false);
		} else if (associatedGenomePanel.getGenomePanelItem()
				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() > associatedGenomePanel
				.getGenomePanelItem()
				.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
				.size()
				- 1 - NUMBER_SLICE_WIDTH - Math.floor(NUMBER_SLICE_WIDTH / 2) + 1) {
			// int tempInt = (int) Math.floor(NUMBER_SLICE_WIDTH/2);
			// indexStartBirdSynteny =
			// alBlockType.size()-1-NUMBER_SLICE_WIDTH-tempInt;
			navigateRightSpeed1.setEnabled(true);
			navigateRightSpeed2.setEnabled(false);
			navigateRightSpeed3.setEnabled(false);
		} else if (associatedGenomePanel.getGenomePanelItem()
				.getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() > associatedGenomePanel
				.getGenomePanelItem()
				.getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly()
				.size()
				- (NUMBER_SLICE_WIDTH * 2)) {
			// indexStartBirdSynteny =
			// alBlockType.size()-1-(NUMBER_SLICE_WIDTH*2);
			navigateRightSpeed1.setEnabled(true);
			navigateRightSpeed2.setEnabled(true);
			navigateRightSpeed3.setEnabled(false);
		} else {
			navigateRightSpeed1.setEnabled(true);
			navigateRightSpeed2.setEnabled(true);
			navigateRightSpeed3.setEnabled(true);
		}

	}

	private void calculateCanvasParameters() {

		// System.out.println("calculateCanvasParameters");

		setCoordinateSpace = true;

		TOTAL_CANVAS_WIDTH = canvasBgk.getOffsetWidth();
		TOTAL_CANVAS_HEIGHT = canvasBgk.getOffsetHeight();
		TOTAL_CANVAS_HEIGHT_UPPER_PART = (int) Math
				.floor(TOTAL_CANVAS_HEIGHT / 2);

		UNIT_WIDTH = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(0).buttonNormalSize
				.getOffsetWidth() + DISPLAY_SIZE_MODIFIER_COLUMN;
		WIDTH_SIZE_PB_TEXT = Insyght.APP_CONTROLER.getLIST_GENOME_PANEL()
				.get(0).buttonNormalSize.getOffsetWidth() * 2;
		// font size
		// FONT_SIZE = (int) Math.floor(UNIT_WIDTH * PERCENT_HEIGHT_TEXT) - 2;
		FONT_SIZE = (int) Math.floor(Insyght.APP_CONTROLER
				.getLIST_GENOME_PANEL().get(0).buttonNormalSize
				.getOffsetWidth()
				* PERCENT_HEIGHT_TEXT) - 2;

		SLICE_CANVAS_WIDTH = UNIT_WIDTH * 5;
		// System.out.println("sliceCanvasWidth :"+sliceCanvasWidth);
		double numberSliceWidthAsDouble = (double) TOTAL_CANVAS_WIDTH
				/ (double) SLICE_CANVAS_WIDTH;
		// System.err.println(numberSliceWidthAsDouble);
		NUMBER_SLICE_WIDTH = (int) Math.floor(numberSliceWidthAsDouble);
		// System.err.println(NUMBER_SLICE_WIDTH);

		// if(NUMBER_SLICE_WIDTH >
		// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()){
		// NUMBER_SLICE_WIDTH =
		// associatedGenomePanel.getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size();
		// }
		// System.out.println("numberSliceWidth :"+numberSliceWidth);

		// center drawing and text writting
		// addToLeftToCenterDrawing = (int)Math.floor((TOTAL_CANVAS_WIDTH -
		// (NUMBER_SLICE_WIDTH * SLICE_CANVAS_WIDTH))/2);
		EXTRA_SPACE_AFTER_LAST_SLICE = (int) Math.floor(TOTAL_CANVAS_WIDTH
				- (NUMBER_SLICE_WIDTH * SLICE_CANVAS_WIDTH));

		// default font is 10px sans-serif
		// Serif : Times New Roman, Georgia ; Serif fonts have small lines at
		// the ends on some characters
		// Sans-serif ; Arial, Verdana ; "Sans" means without - these fonts do
		// not have the lines at the ends of characters
		// Monospace ; Courier New, Lucida Console ; All monospace characters
		// have the same width
		// other : Garamond, Verdana, Helvetica

		// deal with upper part of canvas

		Y_TEXT = (int) Math.floor((TOTAL_CANVAS_HEIGHT_UPPER_PART / 2)
				+ ((UNIT_WIDTH * PERCENT_HEIGHT_TEXT) / 2));

		// HOMOLOG_BLOCK
		X_RECT_HOMOLOG_BLOCK = (int) Math
				.floor(((SLICE_CANVAS_WIDTH * (1 - PERCENT_WIDTH_HOMOLOG_BLOCK)) / 2));
		Y_RECT_HOMOLOG_BLOCK = (int) Math
				.floor(((TOTAL_CANVAS_HEIGHT_UPPER_PART * (1 - PERCENT_HEIGHT_HOMOLOG_BLOCK)) / 2));
		W_RECT_HOMOLOG_BLOCK = (int) Math.floor(SLICE_CANVAS_WIDTH
				* PERCENT_WIDTH_HOMOLOG_BLOCK);
		H_RECT_HOMOLOG_BLOCK = (int) Math.floor(TOTAL_CANVAS_HEIGHT_UPPER_PART
				* PERCENT_HEIGHT_HOMOLOG_BLOCK);

		// Q_insertion
		// int unitPercentageWidthQInsertion = (int)
		// Math.floor(((SLICE_CANVAS_WIDTH * (1 - PERCENT_WIDTH_Q_INSERT_BLOCK))
		// / 2));
		// int unitPercentageHeightQInsertion =
		// (int)Math.floor((heightUpperCanvasPart*(1-PERCENT_HEIGHT_Q_INSERT_BLOCK)/2));
		Q_INSERTION_X_POINT = SLICE_CANVAS_WIDTH / 2;
		Q_INSERTION_Y_POINT = (TOTAL_CANVAS_HEIGHT_UPPER_PART / 2) - PADDING;
		// Q_INSERTION_X_ARROW_LC = unitPercentageWidthQInsertion;
		// Q_INSERTION_Y_ARROW_LC = (TOTAL_CANVAS_HEIGHT_UPPER_PART / 4) -
		// PADDING;
		// Q_INSERTION_X_ARROW_LMC = SLICE_CANVAS_WIDTH / 8;
		// Q_INSERTION_Y_ARROW_LMC = (TOTAL_CANVAS_HEIGHT_UPPER_PART / 4)-
		// PADDING;
		// Q_INSERTION_X_LBEZIER_cp1x = (SLICE_CANVAS_WIDTH / 8) - WAVE_EFFECT;
		// Q_INSERTION_Y_LBEZIER_cp1y = (1 * ((TOTAL_CANVAS_HEIGHT_UPPER_PART /
		// 4) - (PADDING * 2)) / 3) + PADDING;
		// Q_INSERTION_X_LBEZIER_cp2x = (SLICE_CANVAS_WIDTH / 8) + WAVE_EFFECT;
		// Q_INSERTION_Y_LBEZIER_cp2y = (2 * ((TOTAL_CANVAS_HEIGHT_UPPER_PART /
		// 4) - (PADDING * 2)) / 3) + PADDING;
		Q_INSERTION_X_LBEZIER_LTC = SLICE_CANVAS_WIDTH / 8;
		// Q_INSERTION_Y_LBEZIER_LTC = PADDING;
		Q_INSERTION_Y_LBEZIER_LTC = Y_RECT_HOMOLOG_BLOCK
				- (H_RECT_HOMOLOG_BLOCK / 2) - 3;
		Q_INSERTION_LBEZIER_X_RTC = (7 * SLICE_CANVAS_WIDTH) / 8;
		Q_INSERTION_LBEZIER_Y_RTC = Q_INSERTION_Y_LBEZIER_LTC;
		// Q_INSERTION_X_RBEZIER_cp1x = (7 * SLICE_CANVAS_WIDTH / 8) +
		// WAVE_EFFECT;
		// Q_INSERTION_Y_RBEZIER_cp1y = (1 * ((TOTAL_CANVAS_HEIGHT_UPPER_PART /
		// 4) - (PADDING * 2)) / 3)
		// + PADDING;
		// Q_INSERTION_X_RBEZIER_cp2x = (7 * SLICE_CANVAS_WIDTH / 8) -
		// WAVE_EFFECT;
		// Q_INSERTION_Y_RBEZIER_cp2y = (2 * ((TOTAL_CANVAS_HEIGHT_UPPER_PART /
		// 4) - (PADDING * 2)) / 3)
		// + PADDING;
		// Q_INSERTION_X_RBEZIER_RBC = (7 * SLICE_CANVAS_WIDTH) / 8;
		// Q_INSERTION_Y_RBEZIER_RBC = (TOTAL_CANVAS_HEIGHT_UPPER_PART / 4)-
		// PADDING;
		// Q_INSERTION_X_ARROW_RC = SLICE_CANVAS_WIDTH-
		// unitPercentageWidthQInsertion;
		// Q_INSERTION_Y_ARROW_RC = (TOTAL_CANVAS_HEIGHT_UPPER_PART / 4) -
		// PADDING;

		// S_insertion
		int unitPercentageWidthSInsertion = (int) Math
				.floor(((SLICE_CANVAS_WIDTH * (1 - PERCENT_WIDTH_S_INSERT_BLOCK)) / 2));
		// int unitPercentageHeightQInsertion =
		// (int)Math.floor((heightUpperCanvasPart*(1-PERCENT_HEIGHT_S_INSERT_BLOCK)/2));
		S_INSERTION_X_POINT = SLICE_CANVAS_WIDTH / 2;
		S_INSERTION_Y_POINT = (TOTAL_CANVAS_HEIGHT_UPPER_PART / 2) + PADDING;
		S_INSERTION_X_ARROW_LC = unitPercentageWidthSInsertion;
		S_INSERTION_Y_ARROW_LC = (3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4)
				+ PADDING;
		S_INSERTION_X_ARROW_LMC = SLICE_CANVAS_WIDTH / 8;
		S_INSERTION_Y_ARROW_LMC = (3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4)
				+ PADDING;
		// S_INSERTION_X_LBEZIER_cp1x = (SLICE_CANVAS_WIDTH / 8) - WAVE_EFFECT;
		// S_INSERTION_Y_LBEZIER_cp1y = TOTAL_CANVAS_HEIGHT_UPPER_PART
		// - (1 * ((TOTAL_CANVAS_HEIGHT_UPPER_PART / 4) - (PADDING * 2)) / 3)
		// - PADDING;
		// S_INSERTION_X_LBEZIER_cp2x = (SLICE_CANVAS_WIDTH / 8) + WAVE_EFFECT;
		// S_INSERTION_Y_LBEZIER_cp2y = TOTAL_CANVAS_HEIGHT_UPPER_PART
		// - (2 * ((TOTAL_CANVAS_HEIGHT_UPPER_PART / 4) - (PADDING * 2)) / 3)
		// - PADDING;
		S_INSERTION_X_LBEZIER_LTC = SLICE_CANVAS_WIDTH / 8;
		// S_INSERTION_Y_LBEZIER_LTC = TOTAL_CANVAS_HEIGHT_UPPER_PART-PADDING;
		S_INSERTION_Y_LBEZIER_LTC = Y_RECT_HOMOLOG_BLOCK
				+ (H_RECT_HOMOLOG_BLOCK / 2) + 3 + H_RECT_HOMOLOG_BLOCK;
		S_INSERTION_LBEZIER_X_RTC = (7 * SLICE_CANVAS_WIDTH) / 8;
		S_INSERTION_LBEZIER_Y_RTC = S_INSERTION_Y_LBEZIER_LTC;
		// S_INSERTION_X_RBEZIER_cp1x = (7 * SLICE_CANVAS_WIDTH / 8) +
		// WAVE_EFFECT;
		// S_INSERTION_Y_RBEZIER_cp1y = TOTAL_CANVAS_HEIGHT_UPPER_PART
		// - (1 * ((TOTAL_CANVAS_HEIGHT_UPPER_PART / 4) - (PADDING * 2)) / 3)
		// - PADDING;
		// S_INSERTION_X_RBEZIER_cp2x = (7 * SLICE_CANVAS_WIDTH / 8) -
		// WAVE_EFFECT;
		// S_INSERTION_Y_RBEZIER_cp2y = TOTAL_CANVAS_HEIGHT_UPPER_PART
		// - (2 * ((TOTAL_CANVAS_HEIGHT_UPPER_PART / 4) - (PADDING * 2)) / 3)
		// - PADDING;
		// S_INSERTION_X_RBEZIER_RBC = (7 * SLICE_CANVAS_WIDTH) / 8;
		// S_INSERTION_Y_RBEZIER_RBC = (3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4)
		// + PADDING;
		S_INSERTION_X_ARROW_RC = SLICE_CANVAS_WIDTH
				- unitPercentageWidthSInsertion;
		// S_INSERTION_Y_ARROW_RC = (3 * TOTAL_CANVAS_HEIGHT_UPPER_PART / 4) +
		// PADDING;

		// REVERSE BLOCK
		REVERSE_X_TLC = X_RECT_HOMOLOG_BLOCK;
		REVERSE_Y_TLC = Y_RECT_HOMOLOG_BLOCK;
		REVERSE_X_1_cp1x = SLICE_CANVAS_WIDTH / 2;
		REVERSE_Y_1_cp1y = Y_RECT_HOMOLOG_BLOCK;
		REVERSE_X_1_cp2x = SLICE_CANVAS_WIDTH / 2;
		REVERSE_Y_1_cp2y = Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK;
		REVERSE_X_BRC = X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK;
		REVERSE_Y_BRC = Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK;
		REVERSE_X_TRC = X_RECT_HOMOLOG_BLOCK + W_RECT_HOMOLOG_BLOCK;
		REVERSE_Y_TRC = Y_RECT_HOMOLOG_BLOCK;
		REVERSE_X_2_cp1x = SLICE_CANVAS_WIDTH / 2;
		REVERSE_Y_2_cp1y = Y_RECT_HOMOLOG_BLOCK;
		REVERSE_X_2_cp2x = SLICE_CANVAS_WIDTH / 2;
		REVERSE_Y_2_cp2y = Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK;
		REVERSE_X_BLC = X_RECT_HOMOLOG_BLOCK;
		REVERSE_Y_BLC = Y_RECT_HOMOLOG_BLOCK + H_RECT_HOMOLOG_BLOCK;

		// bottom drawing
		Y_AXIS_TO_DRAW_AT_BOTTOM = (int) Math
				.floor((1 - PERCENT_HEIGHT_SCAFFOLD_GENOME_TO_TOP)
						* (TOTAL_CANVAS_HEIGHT - TOTAL_CANVAS_HEIGHT_UPPER_PART));
		Y_AXIS_TO_DRAW_AT_TOP = (int) Math
				.floor(PERCENT_HEIGHT_SCAFFOLD_GENOME_TO_TOP
						* (TOTAL_CANVAS_HEIGHT - TOTAL_CANVAS_HEIGHT_UPPER_PART));

		SCAFFOLD_X_START_POINT = 10 + WIDTH_SIZE_PB_TEXT;
		SCAFFOLD_X_STOP_POINT = (int) Math
				.floor(TOTAL_CANVAS_WIDTH
						- (PERCENT_WIDTH_TEXT_SCAFFOLD_GENOME_TO_LEFT * TOTAL_CANVAS_WIDTH))
				- WIDTH_SIZE_PB_TEXT;
		TOTAL_LENGHT_SCAFFOLD = SCAFFOLD_X_STOP_POINT - SCAFFOLD_X_START_POINT;
		SCAFFOLD_X_START_GENOME_TEXT = SCAFFOLD_X_STOP_POINT
				+ WIDTH_SIZE_PB_TEXT + 15;

		CALCULATE_CANVAS_PARAMETERS = false;

	}

	public static double claculateAbsolutePercentage(int sizeGenome,
			double relativePercentFeature,
			double absolutePercentSyntenyShowStopGenome,
			double absolutePercentSyntenyShowStartGenome) {

		// System.out.println("claculateRelativePercentage ; sizeGenome : "+sizeGenome+" ; relativePercentFeature : "+relativePercentFeature+" ; absolutePercentSyntenyShowStopGenome : "+absolutePercentSyntenyShowStopGenome+" ; absolutePercentSyntenyShowStartGenome : "+absolutePercentSyntenyShowStartGenome);

		if (absolutePercentSyntenyShowStopGenome == 1
				&& absolutePercentSyntenyShowStartGenome == 0) {
			return relativePercentFeature;
		}

		double newAbsolutePercentageToReturn = -1;
		double displayStartGenomePb = sizeGenome
				* absolutePercentSyntenyShowStartGenome;
		double displayStopGenomePb = sizeGenome
				* absolutePercentSyntenyShowStopGenome;
		// double newFeaturePb = sizeGenome*percentFeature;
		double deltaFeaturePb = relativePercentFeature
				* (displayStopGenomePb - displayStartGenomePb);

		// newAbsolutePercentageToReturn =
		// (newFeaturePb-newStartGenomePb)/(newStopGenomePb-newStartGenomePb);
		newAbsolutePercentageToReturn = (displayStartGenomePb + deltaFeaturePb)
				/ sizeGenome;

		// System.out.println("newAbsoluePercentageToReturn : "+newAbsolutePercentageToReturn);
		return newAbsolutePercentageToReturn;
	}

	public static double claculateRelativePercentage(int sizeGenome,
			double absolutePercentFeature,
			double absolutePercentSyntenyShowStopGenome,
			double absolutePercentSyntenyShowStartGenome) {

		// System.out.println("claculateRelativePercentage ; sizeGenome : "+sizeGenome+" ; percentFeature : "+absolutePercentFeature+" ; percentSyntenyShowStopGenome : "+absolutePercentSyntenyShowStopGenome+" ; percentSyntenyShowStartGenome : "+absolutePercentSyntenyShowStartGenome);

		if (absolutePercentSyntenyShowStopGenome == 1
				&& absolutePercentSyntenyShowStartGenome == 0) {
			return absolutePercentFeature;
		}

		double newRelativePercentageToReturn = -1;
		double displayStartGenomePb = sizeGenome
				* absolutePercentSyntenyShowStartGenome;
		double displayStopGenomePb = sizeGenome
				* absolutePercentSyntenyShowStopGenome;
		double absoluteDeltaFeaturePb = sizeGenome * absolutePercentFeature;
		newRelativePercentageToReturn = (absoluteDeltaFeaturePb - displayStartGenomePb)
				/ (displayStopGenomePb - displayStartGenomePb);

		// System.out.println("newRelativePercentageToReturn : "+newRelativePercentageToReturn);
		/*
		 * if(newRelativePercentageToReturn < 0){ newRelativePercentageToReturn
		 * = 0; }else if(newRelativePercentageToReturn>1){
		 * newRelativePercentageToReturn = 1; }
		 */
		return newRelativePercentageToReturn;
	}

	public String getBkgColorAssociatedWithColorIDAsString(int colorId) {
		if (colorId == 0) {
			return colorGene0JauneAsString;
		} else if (colorId == 1) {
			return colorGene1VertAsString;
		} else if (colorId == 2) {
			return colorGene2OrangeAsString;
		} else if (colorId == 3) {
			return colorGene3BleuAsString;
		} else if (colorId == 4) {
			return colorGene4RoseAsString;
		} else {
			return colorGeneDefaulGrisPaleAsString;
		}
	}

	public CssColor getBkgColorAssociatedWithColorIDAsCssColor(int colorId) {
		if (colorId == 0) {
			return colorGene0JauneAsCssColor;
		} else if (colorId == 1) {
			return colorGene1VertAsCssColor;
		} else if (colorId == 2) {
			return colorGene2OrangeAsCssColor;
		} else if (colorId == 3) {
			return colorGene3BleuAsCssColor;
		} else if (colorId == 4) {
			return colorGene4RoseAsCssColor;
		} else {
			return colorGeneDefaulGrisPaleAsCssColor;
		}
	}

}
