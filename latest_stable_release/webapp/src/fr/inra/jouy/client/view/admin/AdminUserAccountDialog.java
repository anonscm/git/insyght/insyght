package fr.inra.jouy.client.view.admin;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.RPC.CallForUpdateDB;
import fr.inra.jouy.client.RPC.CallForUpdateDBAsync;
import fr.inra.jouy.shared.pojos.users.PersonsObj;

public class AdminUserAccountDialog extends DialogBox {

	private static AdminUserAccountDialogUiBinder uiBinder = GWT
			.create(AdminUserAccountDialogUiBinder.class);

	private final CallForUpdateDBAsync updateDBService = (CallForUpdateDBAsync) GWT
			.create(CallForUpdateDB.class);

	interface AdminUserAccountDialogUiBinder extends
			UiBinder<Widget, AdminUserAccountDialog> {
	}
	
	
	
	@UiField
	TextBox tbLastName;
	
	@UiField
	TextBox tbFirstName;
	
	@UiField
	TextBox	tbaffiliation;
	
	@UiField
	Button closeButtonB;
	@UiField
	Button submitBasicChangesButton;
	
	@UiField
	PasswordTextBox	ptbOldPssd;
	
	@UiField
	PasswordTextBox ptbNewPssd1;
	
	@UiField
	PasswordTextBox ptbNewPssd2;
	
	@UiField
	Button closeButtonP;
	@UiField
	Button submitPssdChangesButton;
	
	public AdminUserAccountDialog() {
		setText("Change user account settings :");
		setWidget(uiBinder.createAndBindUi(this));
		setAnimationEnabled(true);
		setGlassEnabled(true);
		
		tbLastName.setText(Insyght.APP_CONTROLER.getCurrentUser().getNom());
		tbFirstName.setText(Insyght.APP_CONTROLER.getCurrentUser().getPrenom());
		tbaffiliation.setText(Insyght.APP_CONTROLER.getCurrentUser().getLabo());
		
	}

	@UiHandler("closeButtonB")
	void onCloseButtonBClicked(ClickEvent event) {
		hide();
	}
	
	@UiHandler("closeButtonP")
	void onCloseButtonPClicked(ClickEvent event) {
		hide();
	}

	@UiHandler("submitBasicChangesButton")
	void onSubmitBasicChangesButtonClicked(ClickEvent event) {

		AsyncCallback<PersonsObj> callback = new AsyncCallback<PersonsObj>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				
				//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			public void onSuccess(PersonsObj personObjReturned) {
				
				Insyght.APP_CONTROLER.getCurrentUser().setNom(personObjReturned.getNom());
				Insyght.APP_CONTROLER.getCurrentUser().setPrenom(personObjReturned.getPrenom());
				Insyght.APP_CONTROLER.getCurrentUser().setLabo(personObjReturned.getLabo());
				
				AdminTab.adminTabHTMLProjectAdmin_name.setText("Your last name : "+personObjReturned.getNom());
				AdminTab.adminTabHTMLProjectAdmin_firstName.setText("Your first name : "+personObjReturned.getPrenom());
				AdminTab.adminTabHTMLProjectAdmin_affiliation.setText("Your Lab : "+personObjReturned.getLabo());
				Window.alert("Your account has been successfuly changed.");
				hide();
				
			}

		};

		try {
			updateDBService.updateNameAndAffiInfo(tbLastName.getText(), tbFirstName.getText(), tbaffiliation.getText(), Insyght.APP_CONTROLER.getCurrentUser().getSessionId(), callback);
		} catch (Exception e) {
			//String mssgError = "ERROR : " + e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}
	}

	
	@UiHandler("submitPssdChangesButton")
	void onSubmitPssdChangesButtonClicked(ClickEvent event) {
		

		AsyncCallback<String> callback = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				// do some UI stuff to show failure
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}

			public void onSuccess(String stringReturned) {
				Window.alert("Your password has been successfuly changed.");
				hide();
			}

		};

		if (ptbNewPssd1.getText().compareTo(ptbNewPssd2.getText()) == 0) {
			if(ptbNewPssd1.getText().length() < 6){
				Window.alert("ERROR : your password must have a length greater than 6 characters.");
			}else{

				try {
					updateDBService.updatePssdInfo(ptbNewPssd1.getText(), ptbOldPssd.getText(), Insyght.APP_CONTROLER.getCurrentUser().getSessionId(), callback);
				} catch (Exception e) {
					//String mssgError = "ERROR : " + e.getMessage();
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
				}
			}
		} else {
			Window.alert("ERROR : you have entered 2 differents new passwords");
		}
		
	}

}
