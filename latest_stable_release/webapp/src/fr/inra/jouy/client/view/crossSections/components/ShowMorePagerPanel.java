package fr.inra.jouy.client.view.crossSections.components;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.user.cellview.client.AbstractPager;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasRows;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.crossSections.popUp.ManageListComparedOrganisms;
import fr.inra.jouy.client.view.search.SearchViewImpl;


/**
 * A scrolling pager that automatically increases the range every time the
 * scroll bar reaches the bottom.
 */
public class ShowMorePagerPanel extends AbstractPager{
	  /**
	   * The default increment size.
	   */
	  private static final int DEFAULT_INCREMENT = 20;

	  /**
	   * The increment size.
	   */
	  private int incrementSize = DEFAULT_INCREMENT;

	  /**
	   * The last scroll position.
	   */
	  private int lastScrollPos = 0;

	  /**
	   * The scrollable panel.
	   */
	  private final ScrollPanel scrollable = new ScrollPanel();

	  /**
	   * Construct a new {@link ShowMorePagerPanel}.
	   */
	  public ShowMorePagerPanel() {
	    initWidget(scrollable);
	    // Handle scroll events.
	    scrollable.addScrollHandler(new ScrollHandler() {
	      public void onScroll(ScrollEvent event) {
	        // If scrolling up, ignore the event.
	        int oldScrollPos = lastScrollPos;
	        lastScrollPos = scrollable.getVerticalScrollPosition();
	        if (oldScrollPos >= lastScrollPos) {
	          return;
	        }

	        HasRows display = getDisplay();
	        if (display == null) {
	          return;
	        }
	        int maxScrollTop = scrollable.getWidget().getOffsetHeight()
	            - scrollable.getOffsetHeight();
	        if (lastScrollPos >= maxScrollTop) {
	        	// We are near the end, so increase the page size.
	        	int newPageSize = Math.min(
	        			display.getVisibleRange().getLength() + incrementSize,
	        			display.getRowCount());
	        	display.setVisibleRange(0, newPageSize);

	        	//hack to avoid jump back to selected
		  		if ( MainTabPanelView.tabPanel.getSelectedIndex() == 1) {
					//search tab
		  			//SearchViewImpl.buttonViewAnnotCompa.setFocus(true);
		  			SearchViewImpl.MenuItems_SubmitYourSearch.setFocus(true);
				} else if ( MainTabPanelView.MLCO.isShowing() && ( MainTabPanelView.tabPanel.getSelectedIndex() == 2 || MainTabPanelView.tabPanel.getSelectedIndex() == 4 ) ) {
					ManageListComparedOrganisms.closeWindowFromUpperBar.setFocus(true);
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR ShowMorePagerPanel : not recognized MainTabPanelView.tabPanel.getSelectedIndex() = "
							+ MainTabPanelView.tabPanel.getSelectedIndex()
							));
				}
	          
	          
	        }
	      }
	    });
	  }

	  /**
	   * Get the number of rows by which the range is increased when the scrollbar
	   * reaches the bottom.
	   *
	   * @return the increment size
	   */
	  public int getIncrementSize() {
	    return incrementSize;
	  }

	  @Override
	  public void setDisplay(HasRows display) {
	    assert display instanceof Widget : "display must extend Widget";
	    scrollable.setWidget((Widget) display);
	    super.setDisplay(display);
	  }

	  /**
	   * Set the number of rows by which the range is increased when the scrollbar
	   * reaches the bottom.
	   *
	   * @param incrementSize the incremental number of rows
	   */
	  public void setIncrementSize(int incrementSize) {
	    this.incrementSize = incrementSize;
	  }

	  @Override
	  protected void onRangeOrRowCountChanged() {
		  //Window.alert("onRangeOrRowCountChanged");

	  }

	  
}
