package fr.inra.jouy.client.view.crossSections.popUp;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.RepeatingCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.shared.UtilitiesMethodsShared;



public class ExportCDSSeqDiag extends PopupPanel {

	private static ExportCDSSeqDiagUiBinder uiBinder = GWT
			.create(ExportCDSSeqDiagUiBinder.class);

//	private final CallForInfoDBAsync callForInfoDBService = (CallForInfoDBAsync) GWT
//			.create(CallForInfoDB.class);
	
	interface ExportCDSSeqDiagUiBinder extends UiBinder<Widget, ExportCDSSeqDiag> {
	}

	@UiField DockLayoutPanel rootDlp;
	@UiField HTML close;
	//@UiField HTML HTMLSelectedSymbol;
	@UiField VerticalPanel vpMain;
	/*@UiField VerticalPanel vpExportEntity;
	@UiField ListBox lbExportType;
	@UiField ListBox lbExportFormat;
	@UiField Button submit;
	@UiField HTML feedBackMssg;
	//@UiField VerticalPanel hiddenForm;*/
	public static FormPanel form;
	public static VerticalPanel panelForm = new VerticalPanel();
	//public static VerticalPanel vpExportEntity = new VerticalPanel();
	public static ListBox lbExportType = new ListBox();
	public static ListBox lbExportFormat = new ListBox();  
	public static Button submit;
	public static HTML feedBackMssg;
			
	String refMostSignificantQGeneNameAsStrippedString = null;
	String refMostSignificantQGeneLocusTag = null;
	String refQGeneAccnum = null;
	int refQGeneId = -1;
	String compMostSignificantSGeneNameAsStrippedString = null;
	String compMostSignificantSGeneLocusTag = null;
	String compSGeneAccnum = null;
	int compSGeneId = -1;
	long SyntenyOrigamiAlignmentId = 0;
	int SyntenyNumberGenes = -1;
	int qOrigamiElementId = -1;
	int qPbStartQGenomicRegionInsertionInElement = -1;
	int qPbStopQGenomicRegionInsertionInElement = -1;
	int sOrigamiElementId = -1;
	int sPbStartSGenomicRegionInsertionInElement = -1;
	int sPbStopSGenomicRegionInsertionInElement = -1;
	ArrayList<Integer> refGeneSetIds = null;
	public static boolean IS_RESIZING = false;
	public static boolean BUTTON_IS_CLOSE_POP_UP = false;
	static int countLoopCheckCookiesIT = 0;
	
	@UiHandler("close")
	void onCloseClick(ClickEvent e) {
		hide();
	}
	
	/*@UiHandler("submit")
	void onSubmitClick(ClickEvent e) {
		parseFormAndGetResult();
	}*/
	
	@Override
	protected void onUnload() {
//		panelForm.clear();
//		feedBackMssg.setText("");
//		submit.setEnabled(true);
//		BUTTON_IS_CLOSE_POP_UP = false;
//		submit.setText("Submit");
	}
//	@Override
//	protected void onDetach() {
//		clearSearchMain(true, true); //must be before super.onDetach i norder to have the all list of compared organism in main list despite filters by user
//		clearSearchFeatured(true, true);
//		super.onDetach();
//	}
	
	public ExportCDSSeqDiag() {
		
		setWidget(uiBinder.createAndBindUi(this));
		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				if (isShowing() && ! IS_RESIZING) {
					IS_RESIZING = true;
					setHeightWidthAndShowCenter();
					IS_RESIZING = false;
				}
			}
        });
		setAnimationEnabled(true);
		setGlassEnabled(true);
		setAutoHideEnabled(true);
		//feedBackMssg.setText("");
		

		//form = new FormPanel(Insyght.DOWNLOAD_FRAME); // can be a frame in the tab to allow download
		//form = new FormPanel("_blank"); // equivalent to target=_blank in HTML, open a new tab
		//form.setEncoding(FormPanel.ENCODING_MULTIPART); //create bug
		
		form = new FormPanel();		
		form.setMethod(FormPanel.METHOD_POST);
		form.setWidget(panelForm);
		form.addSubmitHandler(new FormPanel.SubmitHandler() {
		      public void onSubmit(SubmitEvent event) {
		        // This event is fired just before the form is submitted. We can take
		        // this opportunity to perform validation.
		    	  
		    	boolean atLeastOneCbChecked = false;
				for(int i=0;i<panelForm.getWidgetCount();i++){
					Widget wIT = panelForm.getWidget(i);
					if(wIT instanceof CheckBox){
						if(((CheckBox)wIT).getValue()){
							atLeastOneCbChecked = true;
						}
					}
				}
				if ( ! Cookies.isCookieEnabled()) {
		        	feedBackMssg.setHTML("<span style=\"color:red;\">Cookies must be enable for this functionality</span>");
		        	event.cancel();	
				} else if ( ! atLeastOneCbChecked) {
		        	feedBackMssg.setHTML("<span style=\"color:red;\">Please select what to export (checkbox)</span>");
		        	event.cancel();
		        } else {
		        	feedBackMssg.setHTML(
		        			"<span style=\"color:green;\">Your query have been submitted."
		        			+ " The download is being processed, please wait a few seconds..."
//		        			+ " The download should start soon in a new browser tab that will automatically closes when it is done (manually closing this new tab will cancel the download)."
//		        			+ " It usually takes a few seconds for the download to complete. You can continue using Insyght in the meantime."
		        			+ "</span>"
		        			);
		        	submit.setText("Download in progress...");

		        	countLoopCheckCookiesIT = 0;
		    		Insyght.INSYGHT_LOADING_MASK.show();
					Insyght.INSYGHT_LOADING_MASK.center();
		    		
		    		RepeatingCommand repeatingCommandIT = new RepeatingCommand() {
						@Override
						public boolean execute() {
							if (Cookies.getCookie("testDownloadDone") != null) {
								//GWT.log("found cookies ! ");
								
								feedBackMssg.setHTML(
					        			"<span style=\"color:green;\">"
					        			+ " The download is completed."
//					        			+ " The download should start soon in a new browser tab that will automatically closes when it is done (manually closing this new tab will cancel the download)."
//					        			+ " It usually takes a few seconds for the download to complete. You can continue using Insyght in the meantime."
					        			+ "</span>"
					        			);
					        	BUTTON_IS_CLOSE_POP_UP = true;
					    		submit.setText("Continue using Insyght");
					    		Insyght.INSYGHT_LOADING_MASK.hide();
					    		Cookies.removeCookie("testDownloadDone", "/");
								return false;
							} else {
								if (countLoopCheckCookiesIT < 20) {
									//GWT.log("no cookies...");
									countLoopCheckCookiesIT++;
									return true;
								} else {
									//GWT.log("time out ! ");
									feedBackMssg.setHTML("<span style=\"color:red;\">An error occured, the download has timed out.</span>");
						        	BUTTON_IS_CLOSE_POP_UP = true;
						    		submit.setText("Continue using Insyght");
						    		Insyght.INSYGHT_LOADING_MASK.hide();
						    		Cookies.removeCookie("testDownloadDone", "/");
									return false;
								}

							}
						}
					};
		    		Scheduler.get().scheduleFixedDelay(
		    				repeatingCommandIT
		    				, 2000 //delay ms, 2000 = 2 second
		    				);
		    		
		    		
		        }
		      }
		    });
//		//only works when not a download !!
//		form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
//		      public void onSubmitComplete(SubmitCompleteEvent event) {
//		        // When the form submission is successfully completed, this event is
//		        // fired. Assuming the service returned a response of type text/html,
//		        // we can get the result text here (see the FormPanel documentation for
//		        // further explanation).
//		        //Window.alert(event.getResults());
//		    	//Window.alert("all Done !!");
//		    	String dataURI = "data:text/plain;charset=utf-8,"+event.getResults();
//		    	//String dataURI = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==";
//		    	Window.open(dataURI, "_blank", "status=0,toolbar=0,menubar=0,location=0");//
//		        hide();
//		      }
//		    });
		
	    lbExportType.setName("lbExportType");
	    lbExportFormat.setName("lbExportFormat");
	    vpMain.add(form);
		//lbExportType.clear();
		//lbExportFormat.clear();
		lbExportType.addItem("Protein sequence(s) of", "proteinSequence");
		lbExportType.addItem("DNA sequence(s) of", "dnaSequence");
		
		lbExportFormat.addItem("in fasta format", "fasta");
		
		
	    
	    submit = new Button("Submit");
	    submit.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (BUTTON_IS_CLOSE_POP_UP) {
					hide();
				} else {
					parseFormAndGetResult();
				}
				
			}
		});
	    vpMain.add(submit);
	    
	    feedBackMssg = new HTML();
	    vpMain.add(feedBackMssg);

		
	}

	public void displayPoPup(
			final String refMostSignificantQGeneNameAsStrippedStringSent
			, final String refMostSignificantQGeneLocusTagSent
			, final String refQGeneAccnumSent
			, final int refQGeneIdSent
			, final String compMostSignificantSGeneNameAsStrippedStringSent
			, final String compMostSignificantSGeneLocusTagSent
			, final String compSGeneAccnumSent
			, final int compSGeneIdSent
			, final long SyntenyOrigamiAlignmentIdSent
			, final int SyntenyNumberGenesSent
			, final int qOrigamiElementIdSent
			, final int qPbStartQGenomicRegionInsertionInElementSent
			, final int qPbStopQGenomicRegionInsertionInElementSent
			, final int sOrigamiElementIdSent
			, final int sPbStartSGenomicRegionInsertionInElementSent
			, final int sPbStopSGenomicRegionInsertionInElementSent
			, final ArrayList<Integer> refGeneSetIdsSent
			){
		setData(
				refMostSignificantQGeneNameAsStrippedStringSent
				, refMostSignificantQGeneLocusTagSent
				, refQGeneAccnumSent
				, refQGeneIdSent
				, compMostSignificantSGeneNameAsStrippedStringSent
				, compMostSignificantSGeneLocusTagSent
				, compSGeneAccnumSent
				, compSGeneIdSent
				, SyntenyOrigamiAlignmentIdSent
				, SyntenyNumberGenesSent
				, qOrigamiElementIdSent
				, qPbStartQGenomicRegionInsertionInElementSent
				, qPbStopQGenomicRegionInsertionInElementSent
				, sOrigamiElementIdSent
				, sPbStartSGenomicRegionInsertionInElementSent
				, sPbStopSGenomicRegionInsertionInElementSent
				, refGeneSetIdsSent
				);
		setHeightWidthAndShowCenter();

	}
	
	
	private void setHeightWidthAndShowCenter() {
		int clientWidth = Window.getClientWidth();
		int clientHeight = Window.getClientHeight();
		rootDlp.setHeight(((3 * clientHeight) / 4) + "px");
		rootDlp.setWidth(((3 * clientWidth) / 4) + "px");
		center();
	}
	
	private void setData(
			final String refMostSignificantQGeneNameAsStrippedStringSent
			, final String refMostSignificantQGeneLocusTagSent
			, final String refQGeneAccnumSent
			, final int refQGeneIdSent
			, final String compMostSignificantSGeneNameAsStrippedStringSent
			, final String compMostSignificantSGeneLocusTagSent
			, final String compSGeneAccnumSent
			, final int compSGeneIdSent
			, final long SyntenyOrigamiAlignmentIdSent
			, final int SyntenyNumberGenesSent
			, final int qOrigamiElementIdSent
			, final int qPbStartQGenomicRegionInsertionInElementSent
			, final int qPbStopQGenomicRegionInsertionInElementSent
			, final int sOrigamiElementIdSent
			, final int sPbStartSGenomicRegionInsertionInElementSent
			, final int sPbStopSGenomicRegionInsertionInElementSent
			, final ArrayList<Integer> refGeneSetIdsSent
			) {
		
		
		refMostSignificantQGeneNameAsStrippedString = refMostSignificantQGeneNameAsStrippedStringSent;
		refMostSignificantQGeneLocusTag = refMostSignificantQGeneLocusTagSent;
		refQGeneAccnum = refQGeneAccnumSent;
		refQGeneId = refQGeneIdSent;
		compMostSignificantSGeneNameAsStrippedString = compMostSignificantSGeneNameAsStrippedStringSent;
		compMostSignificantSGeneLocusTag = compMostSignificantSGeneLocusTagSent;
		compSGeneAccnum = compSGeneAccnumSent;
		compSGeneId = compSGeneIdSent;
		SyntenyOrigamiAlignmentId = SyntenyOrigamiAlignmentIdSent;
		SyntenyNumberGenes = SyntenyNumberGenesSent;
		qOrigamiElementId = qOrigamiElementIdSent;
		qPbStartQGenomicRegionInsertionInElement = qPbStartQGenomicRegionInsertionInElementSent;
		qPbStopQGenomicRegionInsertionInElement = qPbStopQGenomicRegionInsertionInElementSent;
		sOrigamiElementId = sOrigamiElementIdSent;
		sPbStartSGenomicRegionInsertionInElement = sPbStartSGenomicRegionInsertionInElementSent;
		sPbStopSGenomicRegionInsertionInElement = sPbStopSGenomicRegionInsertionInElementSent;
		refGeneSetIds = refGeneSetIdsSent;
		submit.setEnabled(true);
		feedBackMssg.setText("");
		initForm();
		
	}
	
	
	
	
	private void parseFormAndGetResult(){

		try {
			
			//parse form
			/*String stExportEntities = "";
			for(int i=0;i<vpExportEntity.getWidgetCount();i++){
				Widget wIT = vpExportEntity.getWidget(i);
				if(wIT instanceof CheckBox){
					if(((CheckBox)wIT).getValue()){
						if(((CheckBox)wIT).getText().startsWith("Reference CDS:")
								|| ((CheckBox)wIT).getText().startsWith("Compared CDS:")){
							stExportEntities += "&geneId="+((CheckBox)wIT).getFormValue();
						}
						if(((CheckBox)wIT).getText().startsWith("All reference CDSs in the selected synteny")){
							stExportEntities += "&qAlignmentId="+((CheckBox)wIT).getFormValue();
						}
						if(((CheckBox)wIT).getText().startsWith("All compared CDSs in the selected synteny")){
							stExportEntities += "&sAlignmentId="+((CheckBox)wIT).getFormValue();
						}
						if(((CheckBox)wIT).getText().startsWith("All reference CDSs in the selected genomic region without homologs")
								|| ((CheckBox)wIT).getText().startsWith("All compared CDSs in the selected genomic region without homologs")){
							stExportEntities += "&elementId_pbStartInElement_pbStopInElement="+((CheckBox)wIT).getFormValue();
						}
						String geneSetIds = "";
						if(((CheckBox)wIT).getText().startsWith("All reference CDSs in the selected gene set")){
							if ( ! refGeneSetIds.isEmpty()) {
								geneSetIds += UtilitiesMethodsShared.getItemsAsStringFromCollection(refGeneSetIds);
							}
						}
						if(((CheckBox)wIT).getText().startsWith("All compared CDSs in the selected gene set")){
							ArrayList<Integer> alComparedGeneIds = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getAlAbsoPropQSGeneHomoItemQsSGeneId();
							if ( ! alComparedGeneIds.isEmpty()) {
								geneSetIds += UtilitiesMethodsShared.getItemsAsStringFromCollection(alComparedGeneIds);;
							}
						}
						if ( ! geneSetIds.isEmpty()) {
							stExportEntities += "&geneSetIds="
									 + geneSetIds;
						}
					}
				}else{
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in parseFormAndGetResult: widget "+i+" not instanceof CheckBox"));
					return;
				}
			}*/
			
			//String exportType = lbExportType.getValue(lbExportType.getSelectedIndex());
			//String exportFormat = lbExportFormat.getValue(lbExportFormat.getSelectedIndex());
			
			//if(!stExportEntities.isEmpty() && !exportType.isEmpty() && !exportFormat.isEmpty()){
				String url = GWT.getModuleBaseURL() + "downloadService?" 
						//+ stExportEntities
						//+"&exportType="+exportType
						//+"&exportFormat="+exportFormat
						;
				
				//use POST instead of GET else may cause URI is too large >16386 badMessage: 414
				
				//https://groups.google.com/forum/#!topic/google-web-toolkit/IRnjKF7MAB4
				// https://groups.google.com/forum/#!topic/google-web-toolkit/_w77TrM76p0
				
				//GWT.log("HERE1");
				
//				RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url));
//				try {
//				    //Request request = 
//					builder.sendRequest(null, new RequestCallback() {
//				    public void onError(Request request, Throwable exception) {
//				       // Couldn't connect to server (could be timeout, SOP violation, etc.)
//				    	GWT.log("Couldn't connect to server (could be timeout, SOP violation, etc.)");
//				    }
//
//				    public void onResponseReceived(Request request, Response response) {
//				      if (200 == response.getStatusCode()) {
//				          // Process the response in response.getText()
//				    	  GWT.log(response.getText());
//				    	  
//				      } else {
//				        // Handle the error.  Can get the status text from response.getStatusText()
//				    	  GWT.log("Handle the error.  Can get the status text from response.getStatusText()");
//				      }
//				    }
//				  });
//				} catch (RequestException e) {
//				  // Couldn't connect to server
//					GWT.log("Couldn't connect to server");
//				}
				
				form.setAction(url); 
				form.submit();
				// https://groups.google.com/forum/#!topic/google-web-toolkit/UZC6o9Qvi84
				
				//Window.open(url, "_blank", "status=0,toolbar=0,menubar=0,location=0");//
				
			//}else{
			//	feedBackMssg.setHTML("Please select at least one item in each section");
			//}
			
			//callForInfoDBService.getExportResult(alExportEntities, exportType, exportFormat, callback);
			
		} catch (Exception e) {
			//String mssgError = "ERROR in getExportResult : "+ e.getMessage();
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
			//e.printStackTrace();
			hide();
		}
		
	}

	private void initForm() {
		
		BUTTON_IS_CLOSE_POP_UP = false;
		submit.setText("Submit");
		
		panelForm.clear();

	    panelForm.add(lbExportType);
	    
		//lbExportEntity
		if(refMostSignificantQGeneNameAsStrippedString != null){
			CheckBox cb = new CheckBox("Reference CDS: "+refMostSignificantQGeneNameAsStrippedString+ " [" + refMostSignificantQGeneLocusTag + "]");
			cb.setName("Reference CDS");
			cb.setFormValue(""+refQGeneId);
			cb.setValue(false);
			panelForm.add(cb);
		}
		if(compMostSignificantSGeneNameAsStrippedString != null){
			CheckBox cb = new CheckBox("Compared CDS: "+compMostSignificantSGeneNameAsStrippedString+ " [" + compMostSignificantSGeneLocusTag + "]");
			cb.setName("Compared CDS");
			cb.setFormValue(""+compSGeneId);
			cb.setValue(false);
			panelForm.add(cb);
		}
		if(SyntenyOrigamiAlignmentId != 0 && SyntenyNumberGenes > 1){
			CheckBox cb = new CheckBox("All reference CDSs in the selected synteny (~"+SyntenyNumberGenes+" genes)");
			cb.setName("All reference CDSs in the selected synteny");
			cb.setFormValue(""+SyntenyOrigamiAlignmentId);
			cb.setValue(false);
			panelForm.add(cb);
		}
		if(SyntenyOrigamiAlignmentId != 0 && SyntenyNumberGenes > 1){
			CheckBox cb = new CheckBox("All compared CDSs in the selected synteny (~"+SyntenyNumberGenes+" genes)");
			cb.setName("All compared CDSs in the selected synteny");
			cb.setFormValue(""+SyntenyOrigamiAlignmentId);
			cb.setValue(false);
			panelForm.add(cb);
		}
		if(qOrigamiElementId >= 0){
			CheckBox cb = new CheckBox("All reference CDSs in the selected genomic region without homologs");
			cb.setName("All reference CDSs in the selected genomic region without homologs");
			cb.setFormValue(""+qOrigamiElementId+"_"+qPbStartQGenomicRegionInsertionInElement+"_"+qPbStopQGenomicRegionInsertionInElement);
			cb.setValue(false);
			panelForm.add(cb);
		}
		if(sOrigamiElementId >= 0){
			CheckBox cb = new CheckBox("All compared CDSs in the selected genomic region without homologs");
			cb.setName("All compared CDSs in the selected genomic region without homologs");
			cb.setFormValue(""+sOrigamiElementId+"_"+sPbStartSGenomicRegionInsertionInElement+"_"+sPbStopSGenomicRegionInsertionInElement);
			cb.setValue(false);
			panelForm.add(cb);
		}
		if(refGeneSetIds != null && ! refGeneSetIds.isEmpty() ){
			CheckBox cb = new CheckBox("All reference CDSs in the selected gene set");
			cb.setName("All reference CDSs in the selected gene set");
			cb.setFormValue(UtilitiesMethodsShared.getItemsAsStringFromCollection(refGeneSetIds));
			cb.setValue(false);
			panelForm.add(cb);
			
			if (Insyght.APP_CONTROLER.getCurrSelctGenomePanel() != null
					&& Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem() != null
					&& MainTabPanelView.tabPanel.getSelectedIndex() != 1
					) {
				ArrayList<Integer> alComparedGeneIds = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getAlAbsoPropQSGeneHomoItemQsSGeneId();
				if (alComparedGeneIds != null
						&& ! alComparedGeneIds.isEmpty()) {
					CheckBox cb1 = new CheckBox("All compared CDSs in the selected gene set");
					cb.setName("All compared CDSs in the selected gene set");
					cb1.setFormValue(UtilitiesMethodsShared.getItemsAsStringFromCollection(alComparedGeneIds));
					cb1.setValue(false);
					panelForm.add(cb1);
				}
			}
		}
	    //panelForm.add(vpExportEntity);
	    
	    panelForm.add(lbExportFormat);
	    
	}

}
