package fr.inra.jouy.client;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;


import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.IsSerializable;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
import fr.inra.jouy.client.view.search.GetResultDialog;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.AlignmentParametersForSynteny;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;

public class NavigationControler extends History {

	
	public static enum EnumCanonicalURLParams implements IsSerializable{
		tab
		, firstRowShownId
		, referenceOrigamiOrgaId
		, listReferenceOrigamiElementIdsThenStartPbthenStopPBLooped
		, listReferenceOrigamiGeneSetIds
		, listUserSelectedOrigamiOrgaIdsToBeResults
		, listExcludedOrigamiGenomeIds
		//, resultListSortScopeType
		, sortResultListBy_scope
		, sortResultListBy_sortType
		, sortResultListBy_sortOrder
	};
	
	public static enum EnumConvenianceURLParams implements IsSerializable{
		referenceSpecies, referenceStrain, referenceSubstrain, referenceTaxonId, 
		referenceAccnum, 
		referenceGeneSetNames, referenceGeneSetLocusTag
	};

	static RegExp pNavTab = RegExp.compile("&?"+EnumCanonicalURLParams.tab.toString()+"=([\\w_-]+)");
	static RegExp pFirstRowShownId = RegExp.compile("&?"+EnumCanonicalURLParams.firstRowShownId.toString()+"=(\\d+)");
	static RegExp pReferenceOrgaId = RegExp
	.compile("&?"+EnumCanonicalURLParams.referenceOrigamiOrgaId.toString()+"=(\\d+)");
	//static RegExp pResultListSortScopeType = RegExp.compile("&?"+EnumCanonicalURLParams.resultListSortScopeType.toString()+"=(\\w+)");
	static RegExp pSortScopeType_scope = RegExp.compile("&?"+EnumCanonicalURLParams.sortResultListBy_scope.toString()+"=(\\w+)");
	static RegExp pSortScopeType_sortType = RegExp.compile("&?"+EnumCanonicalURLParams.sortResultListBy_sortType.toString()+"=(\\w+)");
	static RegExp pSortScopeType_sortOrder = RegExp.compile("&?"+EnumCanonicalURLParams.sortResultListBy_sortOrder.toString()+"=(\\w+)");
	static RegExp pResultListReferenceGeneSetIds = RegExp
	.compile("&?"+EnumCanonicalURLParams.listReferenceOrigamiGeneSetIds.toString()+"=([\\d,]+)");
	static RegExp pResultReferenceOrigamiElementIdsThenStartPbthenStopPBLooped = RegExp
			.compile("&?"+EnumCanonicalURLParams.listReferenceOrigamiElementIdsThenStartPbthenStopPBLooped.toString()+"=([\\d,]+)");
	static RegExp pResultListUserSelectedOrgaToBeResultsIds = RegExp
	.compile("&?"+EnumCanonicalURLParams.listUserSelectedOrigamiOrgaIdsToBeResults.toString()+"=([\\d,]+)");
	static RegExp pResultListExcludedGenomeIds = RegExp
	.compile("&?"+EnumCanonicalURLParams.listExcludedOrigamiGenomeIds.toString()+"=([\\d,]+)$");
	
	static RegExp pReferenceSpecies = RegExp
			.compile("&?"+EnumConvenianceURLParams.referenceSpecies.toString()+"=([^&]+)");//([\\w\\s\\_\\-\\=\\~\\#\\'\\{\\(\\[\\|\\`\\^\\@\\)\\]\\}\\�\\$\\�\\�\\%\\*\\�\\.\\;\\:\\/\\<\\>\\,\\!\\�]+)
	static RegExp pReferenceStrain = RegExp
			.compile("&?"+EnumConvenianceURLParams.referenceStrain.toString()+"=([^&]+)");
	static RegExp pReferenceSubStrain = RegExp
			.compile("&?"+EnumConvenianceURLParams.referenceSubstrain.toString()+"=([^&]+)");
	static RegExp pReferenceAccnum = RegExp
			.compile("&?"+EnumConvenianceURLParams.referenceAccnum.toString()+"=([^&]+)");
	static RegExp pReferenceTaxonId = RegExp
			.compile("&?"+EnumConvenianceURLParams.referenceTaxonId.toString()+"=(\\d+)");
	static RegExp pReferenceGeneSetNames = RegExp
			.compile("&?"+EnumConvenianceURLParams.referenceGeneSetNames.toString()+"=([^&]+)");
	static RegExp pReferenceGeneSetLocusTag = RegExp
			.compile("&?"+EnumConvenianceURLParams.referenceGeneSetLocusTag.toString()+"=([^&]+)");
	
	public static ArrayList<String> AL_HISTORY_STACK = new ArrayList<String>();
	
	//override History.newItem
	public static void newItem(String historyToken) {
		newItem(historyToken, true);
	}
	
	public static void newItem(String historyToken, boolean issueEvent) {
		String newTocken = NavigationControler.getURLTockenFromCurrentState();
		if(newTocken.compareTo(History.getToken())!=0){
			//replace if url param not canonical
			if(checkTockenIsCononical(History.getToken())){
				History.newItem(newTocken, issueEvent);
				AL_HISTORY_STACK.add(0, newTocken+" ; fire event = "+issueEvent);
				if(AL_HISTORY_STACK.size() > 5){
					AL_HISTORY_STACK.remove(5);
				}
			} else {
				History.replaceItem(newTocken, issueEvent);
				AL_HISTORY_STACK.add(0, newTocken+" ; fire event = "+issueEvent+ " ; replaceItem ");
				if(AL_HISTORY_STACK.size() > 5){
					AL_HISTORY_STACK.remove(5);
				}
			}
		}
	}
	
	
	
	public static void initHistoryHandler(){
		
		addValueChangeHandler(new ValueChangeHandler<String>() {
			
			public void onValueChange(ValueChangeEvent<String> event) {
				String historyToken = event.getValue();



					if(historyToken.isEmpty()){
						Insyght.APP_CONTROLER.clearAppControlerFromAllResultData();
						MainTabPanelView.tabPanel.selectTab(0);
						
					}else{
						
						if(historyToken.contains("PRIVATE_GENOME")){
							//performSearchOnURLArguments = false;
							Window.alert("For security reasons, private genomes can not be shared or recovered via url.");
							//History.newItem("", false);
							return;
						}
						
						SearchItem resultItemToRestore = getSearchItemFromUrlParams(historyToken);
						
						if (resultItemToRestore != null) {
							GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
							@SuppressWarnings("unused")
							GetResultDialog lDiag = new GetResultDialog(
									resultItemToRestore
									//, -1
									//, -1
									//, group1PNavTabIT,
									//, group1PFirstRowShownIdIT
									//, true
									, false
									);
						}
						
					}
				
			}
		});
	}
	
	public static SearchItem getSearchItemFromUrlParams(String historyToken) {

		
		SearchItem resultItemToRestore = new SearchItem();
		
		//navigation
//		MatchResult matcherPNavTab = pNavTab.exec(historyToken);
//		MatchResult matcherPReferenceElementId = pReferenceElementId.exec(historyToken);
//		MatchResult matcherPResultListSortScopeType = pResultListSortScopeType.exec(historyToken);
//		MatchResult matcherPListReferenceGeneSetIds = pResultListReferenceGeneSetIds.exec(historyToken);
//		MatchResult matcherPResultReferenceOrigamiElementIdsThenStartPbthenStopPBLooped = pResultReferenceOrigamiElementIdsThenStartPbthenStopPBLooped.exec(historyToken);
//		MatchResult matcherPListUserSelectedOrgaToBeResultsIds = pResultListUserSelectedOrgaToBeResultsIds.exec(historyToken);
//		MatchResult matcherPListExcludedGenomeIds = pResultListExcludedGenomeIds.exec(historyToken);
//		MatchResult matcherPReferenceSpecies = pReferenceSpecies.exec(historyToken);
//		MatchResult matcherPReferenceStrain = pReferenceStrain.exec(historyToken);
//		MatchResult matcherPReferenceSubStrain = pReferenceSubStrain.exec(historyToken);
//		MatchResult matcherPReferenceAccnum = pReferenceAccnum.exec(historyToken);
//		MatchResult matcherPReferenceTaxonId = pReferenceTaxonId.exec(historyToken);
//		MatchResult matcherPReferenceGeneSetNames = pReferenceGeneSetNames.exec(historyToken);
//		MatchResult matcherPReferenceGeneSetLocusTag = pReferenceGeneSetLocusTag.exec(historyToken);
		
		//deal with major organism info
		OrganismItem oiIt = new OrganismItem();
		int pReferenceOrgaIdIT = getSpecificIntParameterFromURLTockens(
				pReferenceOrgaId, historyToken);
		if(pReferenceOrgaIdIT > 0){
			oiIt.setOrganismId(pReferenceOrgaIdIT);
			resultItemToRestore.setReferenceOrganism(oiIt);
		} else {
			int pReferenceTaxonIdIT = getSpecificIntParameterFromURLTockens(
					pReferenceTaxonId, historyToken);
			if(pReferenceTaxonIdIT > 0){
				oiIt.setTaxonId(pReferenceTaxonIdIT);
				resultItemToRestore.setReferenceOrganism(oiIt);
			} else {
				String pReferenceAccnumIT = getSpecificStringParameterFromURLTockens(
						pReferenceAccnum, historyToken);
				if(!pReferenceAccnumIT.isEmpty()){
					ArrayList<LightElementItem> listOneElementItem = new ArrayList<LightElementItem>();
					LightElementItem theOneLei = new LightElementItem();
					theOneLei.setAccession(pReferenceAccnumIT);
					listOneElementItem.add(theOneLei);
					oiIt.setListAllLightElementItem(listOneElementItem);
					resultItemToRestore.setReferenceOrganism(oiIt);
				} else {
					String pReferenceSpeciesIT = getSpecificStringParameterFromURLTockens(
							pReferenceSpecies, historyToken);
					if(!pReferenceSpeciesIT.isEmpty()){
						oiIt.setSpecies(pReferenceSpeciesIT);
						resultItemToRestore.setReferenceOrganism(oiIt);
					}
				}
			}
		}
		
		if(resultItemToRestore.getReferenceOrganism() != null){
			//deal with secondary info on organism
			String pReferenceStrainIT = getSpecificStringParameterFromURLTockens(
					pReferenceStrain, historyToken);
			if(!pReferenceStrainIT.isEmpty()){
				resultItemToRestore.getReferenceOrganism().setStrain(pReferenceStrainIT);
			}
			String pReferenceSubStrainIT = getSpecificStringParameterFromURLTockens(
					pReferenceSubStrain, historyToken);
			if(!pReferenceSubStrainIT.isEmpty()){
				resultItemToRestore.getReferenceOrganism().setStrain(pReferenceSubStrainIT);
			}
			
			//deal with SortScopeType
			/*String pResultListSortScopeTypeIT = getSpecificStringParameterFromURLTockens(
					pResultListSortScopeType, historyToken);
			if(!pResultListSortScopeTypeIT.isEmpty()){
				SearchItem.EnumResultListSortScopeType erlsstIT = SearchItem.getEnumResultListSortScopeTypeWithString(pResultListSortScopeTypeIT);
				if(erlsstIT != null){
					resultItemToRestore.setResultListSortScopeType(erlsstIT);
				}
			}*/
			String pSortScopeType_scopeIT = getSpecificStringParameterFromURLTockens(
					pSortScopeType_scope
					, historyToken
					);
			if(!pSortScopeType_scopeIT.isEmpty()){
				SearchItem.Enum_comparedGenomes_SortResultListBy_scope erlsstIT = SearchItem.getEnum_comparedGenomes_SortResultListBy_scope_WithString(pSortScopeType_scopeIT);
				if(erlsstIT != null){
					resultItemToRestore.setSortResultListBy_scope(erlsstIT);
				}
			}
			String pSortScopeType_sortTypeIT = getSpecificStringParameterFromURLTockens(
					pSortScopeType_sortType
					, historyToken
					);
			if(!pSortScopeType_sortTypeIT.isEmpty()){
				SearchItem.Enum_comparedGenomes_SortResultListBy_sortType erlsstIT = SearchItem.getEnum_comparedGenomes_SortResultListBy_sortType_WithString(pSortScopeType_sortTypeIT);
				if(erlsstIT != null){
					resultItemToRestore.setSortResultListBy_sortType(erlsstIT);
				}
			}
			String pSortScopeType_sortOrderIT = getSpecificStringParameterFromURLTockens(
					pSortScopeType_sortOrder
					, historyToken
					);
			if(!pSortScopeType_sortOrderIT.isEmpty()){
				SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder erlsstIT = SearchItem.getEnum_comparedGenomes_SortResultListBy_sortOrder_WithString(pSortScopeType_sortOrderIT);
				if(erlsstIT != null){
					resultItemToRestore.setSortResultListBy_sortOrder(erlsstIT);
				}
			}
			
			
			//deal with ReferenceGeneSet
			String pResultListReferenceGeneSetIdsIT = getSpecificStringParameterFromURLTockens(
					pResultListReferenceGeneSetIds, historyToken);
			if(!pResultListReferenceGeneSetIdsIT.isEmpty()){
				ArrayList<LightGeneItem> alIT = new ArrayList<LightGeneItem>();
				String[] arrayStringSplitIt = pResultListReferenceGeneSetIdsIT.split(",");
				for(int i=0;i<arrayStringSplitIt.length;i++){
					int geneIdIt = Integer.parseInt(arrayStringSplitIt[i]);
					LightGeneItem lgiIT = new LightGeneItem();
					lgiIT.setGeneId(geneIdIt);
					alIT.add(lgiIT);
				}
				resultItemToRestore.setListReferenceGeneSet(alIT);
			} else {
				String pResultReferenceOrigamiElementIdsThenStartPbthenStopPBLoopedIT = getSpecificStringParameterFromURLTockens(
						pResultReferenceOrigamiElementIdsThenStartPbthenStopPBLooped, historyToken);
				if(!pResultReferenceOrigamiElementIdsThenStartPbthenStopPBLoopedIT.isEmpty()){
					ArrayList<Integer> alIT = new ArrayList<Integer>();
					String[] arrayStringSplitIt = pResultReferenceOrigamiElementIdsThenStartPbthenStopPBLoopedIT.split(",");
					for(int i=0;i<arrayStringSplitIt.length;i++){
						int intIt = Integer.parseInt(arrayStringSplitIt[i]);
						alIT.add(intIt);
					}
					resultItemToRestore.setReferenceOrigamiElementIdsThenStartPbthenStopPBLooped(alIT);
				} else {
					String pReferenceGeneSetLocusTagIT = getSpecificStringParameterFromURLTockens(
							pReferenceGeneSetLocusTag, historyToken);
					if(!pReferenceGeneSetLocusTagIT.isEmpty()){
						ArrayList<LightGeneItem> alIT = new ArrayList<LightGeneItem>();
						String[] arrayStringSplitIt = pReferenceGeneSetLocusTagIT.split(",");
						for(int i=0;i<arrayStringSplitIt.length;i++){
							String geneLocusTagIt = arrayStringSplitIt[i];
							LightGeneItem lgiIT = new LightGeneItem();
							lgiIT.setLocusTag(geneLocusTagIt);
							alIT.add(lgiIT);
						}
						resultItemToRestore.setListReferenceGeneSet(alIT);
					} else {
						String pReferenceGeneSetNamesIT = getSpecificStringParameterFromURLTockens(
								pReferenceGeneSetNames, historyToken);
						if(!pReferenceGeneSetNamesIT.isEmpty()){
							ArrayList<LightGeneItem> alIT = new ArrayList<LightGeneItem>();
							String[] arrayStringSplitIt = pReferenceGeneSetNamesIT.split(",");
							for(int i=0;i<arrayStringSplitIt.length;i++){
								String geneNameIt = arrayStringSplitIt[i];
								LightGeneItem lgiIT = new LightGeneItem();
								lgiIT.setName(geneNameIt);
								alIT.add(lgiIT);
							}
							resultItemToRestore.setListReferenceGeneSet(alIT);
						}
					}
				}
			}
			
			//deal with UserSelectedOrgaToBeResultsIds
			String pResultListUserSelectedOrgaToBeResultsIdsIT = getSpecificStringParameterFromURLTockens(
					pResultListUserSelectedOrgaToBeResultsIds, historyToken);
			if(!pResultListUserSelectedOrgaToBeResultsIdsIT.isEmpty()){
				ArrayList<LightOrganismItem> alIT = new ArrayList<LightOrganismItem>();
				String[] arrayStringSplitIt = pResultListUserSelectedOrgaToBeResultsIdsIT.split(",");
				for(int i=0;i<arrayStringSplitIt.length;i++){
					int orgaIdIt = Integer.parseInt(arrayStringSplitIt[i]);
					LightOrganismItem loiIT = new LightOrganismItem();
					loiIT.setOrganismId(orgaIdIt);
					alIT.add(loiIT);
				}
				resultItemToRestore.setListUserSelectedOrgaToBeResults(alIT);
			}
			String pResultListExcludedGenomeIdsIT = getSpecificStringParameterFromURLTockens(
					pResultListExcludedGenomeIds, historyToken);
			if(!pResultListExcludedGenomeIdsIT.isEmpty()){
				ArrayList<LightOrganismItem> alIT = new ArrayList<LightOrganismItem>();
				String[] arrayStringSplitIt = pResultListExcludedGenomeIdsIT.split(",");
				for(int i=0;i<arrayStringSplitIt.length;i++){
					int orgaIdIt = Integer.parseInt(arrayStringSplitIt[i]);
					LightOrganismItem loiIT = new LightOrganismItem();
					loiIT.setOrganismId(orgaIdIt);
					alIT.add(loiIT);
				}
				resultItemToRestore.setListExcludedGenome(alIT);
			}
			
		}
		
		
		//need to be before group1PFirstRowShownIdIT
		String group1PNavTabIT = getSpecificStringParameterFromURLTockens(
				pNavTab, historyToken);
		if(!group1PNavTabIT.isEmpty()){
			if (group1PNavTabIT.compareTo(EnumResultViewTypes.homolog_table.toString()) == 0) {
				resultItemToRestore.setViewTypeDesired(EnumResultViewTypes.homolog_table);
			} else if (group1PNavTabIT.compareTo(EnumResultViewTypes.annotations_comparator.toString()) == 0) {
				resultItemToRestore.setViewTypeDesired(EnumResultViewTypes.annotations_comparator);
			} else if (group1PNavTabIT.compareTo(EnumResultViewTypes.genomic_organization.toString()) == 0) {
				resultItemToRestore.setViewTypeDesired(EnumResultViewTypes.genomic_organization);
			}
		}
		
		
		int group1PFirstRowShownIdIT = getSpecificIntParameterFromURLTockens(pFirstRowShownId, historyToken);
		if(group1PFirstRowShownIdIT > 0){
			if (resultItemToRestore.getViewTypeDesired().compareTo(EnumResultViewTypes.homolog_table) == 0
					|| resultItemToRestore.getViewTypeDesired().compareTo(EnumResultViewTypes.genomic_organization) == 0) {
				resultItemToRestore.setFirstRowShownId(group1PFirstRowShownIdIT);
			}
		}
		
		
		int intCheckValidityOfResultItemToRestore = NavigationControler.checkValidityOfResultItemToRestore(resultItemToRestore);
		if(intCheckValidityOfResultItemToRestore == 0){
			//System.err.println("0 = not valid resultItemToRestore, do nothing");
			// 0 = not valid resultItemToRestore, do nothing expect if search or admin tab
			if (group1PNavTabIT.compareTo(EnumResultViewTypes.search.toString()) == 0
					|| group1PNavTabIT.compareTo(EnumResultViewTypes.admin.toString()) == 0) {
				NavigationControler.showCorrectNavigationState(group1PNavTabIT, -1);
			}
			resultItemToRestore = null;
		}else if(intCheckValidityOfResultItemToRestore == 1){
			//System.err.println("1 = valid resultItemToRestore but similar as current one, do nothing");
			// 1 = valid resultItemToRestore but similar as current one
			
			NavigationControler.showCorrectNavigationState(group1PNavTabIT, group1PFirstRowShownIdIT);
//			if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().toString()
//					.compareTo(group1PNavTabIT) != 0){
//				//same object but different view
//				selectTabAccordingToViewType(group1PNavTabIT);
//			}
			resultItemToRestore = null;
		}else if(intCheckValidityOfResultItemToRestore == 2){
			//System.err.println("2 = valid resultItemToRestore, not similar as current one so download it");
			// 2 = valid resultItemToRestore, not similar as current one so download it
			AlignmentParametersForSynteny apfs = new AlignmentParametersForSynteny();
			resultItemToRestore.setAlignmentParametersForSynteny(apfs);
			resultItemToRestore.setAlignmentParametersForSyntenyToStrict(false);
			resultItemToRestore.setExcludeAutomaticallyComputedResults(false);
			//waitForResultToLoad = true;
		}
		
		return resultItemToRestore;
		
	}
	

	public static void showCorrectNavigationState(String viewType, int firstRowShownId){
		
		if(viewType.compareTo(EnumResultViewTypes.search.toString())==0){

			if(MainTabPanelView.getTabPanel().getSelectedIndex() != 1){
				MainTabPanelView.getTabPanel().selectTab(1);
			}
		}else if(viewType.compareTo(EnumResultViewTypes.homolog_table.toString())==0){
			
			//update tab ?
			if(MainTabPanelView.getTabPanel().getSelectedIndex() != 2){
				MainTabPanelView.getTabPanel().selectTab(2);
			}
			
			//update firstRowShownId ?
			// can be null if result not loaded
			if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(1).getGenomePanelItem() != null
					&& firstRowShownId > 0){
					if (firstRowShownId != 
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(1).getGenomePanelItem().getOrganismId()
							){
							Insyght.doActionAfterWaitForLoad("updateResultsDisplay", firstRowShownId, 2);
					}
			}
					
		}else if(viewType.compareTo(EnumResultViewTypes.annotations_comparator.toString())==0){
			if(MainTabPanelView.getTabPanel().getSelectedIndex() != 3){
				MainTabPanelView.getTabPanel().selectTab(3);
			}
		}else if(viewType.compareTo(EnumResultViewTypes.genomic_organization.toString())==0){

			//update tab ?
			if(MainTabPanelView.getTabPanel().getSelectedIndex() != 4){
				MainTabPanelView.getTabPanel().selectTab(4);
			}
			
			//update firstRowShownId ?
			// can be null if result not loaded
			if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(1).getGenomePanelItem() != null
					&& firstRowShownId > 0){
					if (firstRowShownId != 
							Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(1).getGenomePanelItem().getOrganismId()
							){
							Insyght.doActionAfterWaitForLoad("updateResultsDisplay", firstRowShownId, 4);
					}
			}

		}else if(viewType.compareTo(EnumResultViewTypes.admin.toString())==0){
			
			if(MainTabPanelView.getTabPanel().getSelectedIndex() != 5){
				MainTabPanelView.getTabPanel().selectTab(5);
			}
			
		}else{
			//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR GetREsultDialog: getViewTypeInsyght not recognized = "+refGenomePanelAndListOrgaResultReturned.getViewTypeInsyght()));
			//default
			MainTabPanelView.getTabPanel().selectTab(0);
		}
	}
	
	private static String getSpecificStringParameterFromURLTockens(RegExp RegExpSpecificURLparam, String URL) {
		String stToReturn = "";
		MatchResult matcherIT = RegExpSpecificURLparam.exec(URL);
		if(matcherIT != null){
			try {
				stToReturn = matcherIT.getGroup(1);
			} catch (Exception e) {
				  Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
						  new Exception("NavigationControler getSpecificStringParameterFromURLTockens : "+e));
			}
		}
		return stToReturn;
	}
	
	
	private static int getSpecificIntParameterFromURLTockens(RegExp RegExpSpecificURLparam, String URL) {
		
		int intToReturn = -1;
		MatchResult matcherIT = RegExpSpecificURLparam.exec(URL);
		if(matcherIT != null){
			try {
				intToReturn = Integer.parseInt(matcherIT.getGroup(1));
			} catch (Exception e) {
				  Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
						  new Exception("NavigationControler getSpecificIntParameterFromURLTockens : "+e));
			}
		}
		return intToReturn;
	}


	public static String getURLTockenFromCurrentState() {
		
		String newTocken = "";
		
		if(MainTabPanelView.tabPanel.getSelectedIndex() == 1){
			//search tab
			newTocken += "&tab="+EnumResultViewTypes.search.toString();
		} else if (MainTabPanelView.tabPanel.getSelectedIndex() == 2
				|| MainTabPanelView.tabPanel.getSelectedIndex() == 3
				|| MainTabPanelView.tabPanel.getSelectedIndex() == 4) {
			
			//results
			if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult() != null){
				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem() != null){
					if(!Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().isPublic()){
						newTocken +="&PRIVATE_GENOME";
					}else{
						newTocken += 
								"&tab="+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().toString()
								+"&referenceOrigamiOrgaId="+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId();
						
						if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table) == 0
								|| Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization) == 0) {
	
							
							//navigation
	//						if(Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().size()>1){
	//							newTocken += "&firstRowShownId="+Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(1).getGenomePanelItem().getOrganismId();
	//						}
							newTocken += "&firstRowShownId="+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getFirstRowShownId();
							
							//result obj
							//newTocken +="&resultListSortScopeType="+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getResultListSortScopeType().toString();
							newTocken +="&resultSortResultListBy_scope="+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_scope().toString();
							newTocken +="&resultSortResultListBy_sortType="+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortType().toString();
							newTocken +="&resultSortResultListBy_sortOrder="+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortOrder().toString();
							
							
							//Insyght.APP_CONTROLER.getCurrSelectedSyntenyCanvasItemRefId();
							//if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table) == 0){
							if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable() != null) {
								if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable().size() == Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getCountTotalNumberCDSForRefOrganismIfRefGeneSetIsWholeOrga()
										) {
									//whole genome
								} else {
									newTocken += "&listReferenceOrigamiGeneSetIds=";
									for(int i=0;i<Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable().size();i++){
										int geneId = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable().get(i);//.getGeneId();
										if(i==0){
											newTocken += geneId;
										}else{
											newTocken += "," + geneId;
										}
									}
								}
							}
							

							if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults() != null) {
								for(int i=0;i<Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults().size();i++){
									int orgaId = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults().get(i).getOrganismId();
									if(i==0){
										newTocken += "&listUserSelectedOrigamiOrgaIdsToBeResults="+orgaId;
									}else{
										newTocken += "," + orgaId;
									}
								}
							}

							if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListExcludedGenome() != null) {
								for(int i=0;i<Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListExcludedGenome().size();i++){
									int orgaId = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListExcludedGenome().get(i).getOrganismId();
									if(i==0){
										newTocken += "&listExcludedOrigamiGenomeIds="+orgaId;
									}else{
										newTocken += "," + orgaId;
									}
								}
							}

						} else if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.annotations_comparator) == 0) {
							
							//newTocken += "&firstRowShownId="
							if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator() != null){
								if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListGeneIds() != null){
									if (Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListGeneIds().size() == Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getCountTotalNumberCDSForRefOrganismIfRefGeneSetIsWholeOrga()
											) {
										//whole genome
									} else {
										newTocken += "&listReferenceOrigamiGeneSetIds=";
										for(int i=0;i<Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListGeneIds().size();i++){
											int geneId = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListGeneIds().get(i);//.getGeneId();
											if(i==0){
												newTocken += geneId;
											}else{
												newTocken += "," + geneId;
											}
										}
									}
								}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListElementIdsThenStartPbthenStopPBLooped() != null){
									if(!Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListElementIdsThenStartPbthenStopPBLooped().isEmpty()){
										newTocken += "&listReferenceOrigamiElementIdsThenStartPbthenStopPBLooped=";
										for(int i=0;i<Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListElementIdsThenStartPbthenStopPBLooped().size();i++){
											int geneId = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListElementIdsThenStartPbthenStopPBLooped().get(i);
											if(i==0){
												newTocken += geneId;
											}else{
												newTocken += "," + geneId;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		
		}else if(MainTabPanelView.tabPanel.getSelectedIndex() == 5){
			//admin
			newTocken += "&tab="+EnumResultViewTypes.admin.toString();
		}else{
			//default view
			//newTocken += "&tab=HOME-Overview-VideoIntro";
		}
		
		//System.err.println("Producing tocken = "+newTocken);
		return newTocken;
	}
	
	//TODO transfer to object method
	public static int checkValidityOfResultItemToRestore(
			SearchItem resultItemToRestoreSent) {
		// 0 = not valid resultItemToRestore
		// 1 = valid resultItemToRestore but similar as current one
		// 2 = valid resultItemToRestore, not similar as current one so download it

		if(resultItemToRestoreSent.getReferenceOrganism() == null){
			//no item to restore
			return 0;
		}else{
			try{
				
				boolean bothObjectAreSimilar = true;
				
				//referenceOrigamiOrgaId
				if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId()
						!=
						resultItemToRestoreSent.getReferenceOrganism().getOrganismId()){
					
					//System.err.println("bothObjectAreSimilar not referenceOrigamiOrgaId");
					bothObjectAreSimilar = false;
				}
				
				
				if(resultItemToRestoreSent.getViewTypeDesired().compareTo(EnumResultViewTypes.homolog_table)==0
						|| resultItemToRestoreSent.getViewTypeDesired().compareTo(EnumResultViewTypes.genomic_organization)==0){
					
					
					//resultListSortScopeType
					/*if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getResultListSortScopeType()
							.compareTo(
									resultItemToRestoreSent.getResultListSortScopeType())!=0){
						//System.err.println("bothObjectAreSimilar not referenceOrigamiOrgaId");
						bothObjectAreSimilar = false;
					}*/
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_scope()
							.compareTo(
									resultItemToRestoreSent.getSortResultListBy_scope()
									) !=0 ){
						bothObjectAreSimilar = false;
					}
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortType()
							.compareTo(
									resultItemToRestoreSent.getSortResultListBy_sortType()
									) !=0 ){
						bothObjectAreSimilar = false;
					}
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getSortResultListBy_sortOrder()
							.compareTo(
									resultItemToRestoreSent.getSortResultListBy_sortOrder()
									) !=0 ){
						bothObjectAreSimilar = false;
					}

//					listUserSelectedOrigamiOrgaIdsToBeResults
					ArrayList<Integer> alUserSelectedOrgaToBeResultsFromResultItemToRestoreSent = new ArrayList<Integer>();
					ArrayList<Integer> alUserSelectedOrgaToBeResultsFromCurrentRefGenomePanelAndListOrgaResult = new ArrayList<Integer>();
					for(int i=0;i<Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults().size();i++){
						int orgaId = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListUserSelectedOrgaToBeResults().get(i).getOrganismId();
						alUserSelectedOrgaToBeResultsFromCurrentRefGenomePanelAndListOrgaResult.add(orgaId);
					}
					for(int i=0;i<resultItemToRestoreSent.getListUserSelectedOrgaToBeResults().size();i++){
						int orgaId = resultItemToRestoreSent.getListUserSelectedOrgaToBeResults().get(i).getOrganismId();
						alUserSelectedOrgaToBeResultsFromResultItemToRestoreSent.add(orgaId);
					}
					if(alUserSelectedOrgaToBeResultsFromResultItemToRestoreSent.toString()
							.compareTo(
									alUserSelectedOrgaToBeResultsFromCurrentRefGenomePanelAndListOrgaResult.toString())!=0){
						//System.err.println("bothObjectAreSimilar not listUserSelectedOrigamiOrgaIdsToBeResults");
						bothObjectAreSimilar = false;
					}
					
					
//					listExcludedOrigamiGenomeIds
					ArrayList<Integer> alExcludedOrigamiGenomeIdsFromResultItemToRestoreSent = new ArrayList<Integer>();
					ArrayList<Integer> alExcludedOrigamiGenomeIdsFromCurrentRefGenomePanelAndListOrgaResult = new ArrayList<Integer>();
					for(int i=0;i<Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListExcludedGenome().size();i++){
						int orgaId = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListExcludedGenome().get(i).getOrganismId();
						alExcludedOrigamiGenomeIdsFromCurrentRefGenomePanelAndListOrgaResult.add(orgaId);
					}
					for(int i=0;i<resultItemToRestoreSent.getListExcludedGenome().size();i++){
						int orgaId = resultItemToRestoreSent.getListExcludedGenome().get(i).getOrganismId();
						alExcludedOrigamiGenomeIdsFromResultItemToRestoreSent.add(orgaId);
					}
					if(alExcludedOrigamiGenomeIdsFromResultItemToRestoreSent.toString()
							.compareTo(
									alExcludedOrigamiGenomeIdsFromCurrentRefGenomePanelAndListOrgaResult.toString())!=0){
						
						//System.err.println("bothObjectAreSimilar not listExcludedOrigamiGenomeIds");
						bothObjectAreSimilar = false;
					}
					
				
				}

				//listReferenceOrigamiGeneSetIds
				ArrayList<Integer> allistReferenceOrigamiGeneSetIdsFromResultItemToRestoreSent = new ArrayList<Integer>();
				ArrayList<Integer> allistReferenceElementIdsThenStartPbthenStopPBLoopedFromResultItemToRestoreSent = new ArrayList<Integer>();
				for(int i=0;i<resultItemToRestoreSent.getListReferenceGeneSet().size();i++){
					int geneIdId = resultItemToRestoreSent.getListReferenceGeneSet().get(i).getGeneId();
					allistReferenceOrigamiGeneSetIdsFromResultItemToRestoreSent.add(geneIdId);
				}
				for(int i=0;i<resultItemToRestoreSent.getReferenceOrigamiElementIdsThenStartPbthenStopPBLooped().size();i++){
					int intIT = resultItemToRestoreSent.getReferenceOrigamiElementIdsThenStartPbthenStopPBLooped().get(i);
					allistReferenceElementIdsThenStartPbthenStopPBLoopedFromResultItemToRestoreSent.add(intIT);
				}
				if(resultItemToRestoreSent.getViewTypeDesired().compareTo(EnumResultViewTypes.homolog_table)==0){
					String CRIT = "[]";
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable() != null){
						CRIT = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable().toString();
					}
					if(CRIT.compareTo(allistReferenceOrigamiGeneSetIdsFromResultItemToRestoreSent.toString())!=0){
						
						//System.err.println("bothObjectAreSimilar not listReferenceOrigamiGeneSetIds homolog_table");
						//System.err.println("getCurrentRefGenomePanelAndListOrgaResult = "+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getListReferenceGeneSetForHomologsTable().toString());
						//System.err.println("resultItemToRestoreSent = "+resultItemToRestoreSent.getListReferenceGeneSet().toString());
						bothObjectAreSimilar = false;
					}
				}else if(resultItemToRestoreSent.getViewTypeDesired().compareTo(EnumResultViewTypes.annotations_comparator)==0){
					String CRIT = "[]";
					String CRITBis = "[]";
					if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator() != null){
						if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListGeneIds() != null){
							CRIT = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListGeneIds().toString();
						}
						if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListElementIdsThenStartPbthenStopPBLooped() != null){
							CRITBis = Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getRefGeneSetForAnnotationsComparator().getListElementIdsThenStartPbthenStopPBLooped().toString();
						}
					}
					if(CRIT.compareTo(allistReferenceOrigamiGeneSetIdsFromResultItemToRestoreSent.toString())!=0){
						
						//System.err.println("bothObjectAreSimilar not listReferenceOrigamiGeneSetIds annotations_comparator");
						bothObjectAreSimilar = false;
					}
					if(CRITBis.compareTo(allistReferenceElementIdsThenStartPbthenStopPBLoopedFromResultItemToRestoreSent.toString())!=0){
						
						//System.err.println("bothObjectAreSimilar not listReferenceOrigamiGeneSetIds annotations_comparator 2");
						bothObjectAreSimilar = false;
					}
				}
				//resultItemToRestoreSent.getListReferenceGeneSet()
				
				
				if(bothObjectAreSimilar){
					return 1;
				}else{
					return 2;
				}
			}catch (Exception e){
				//no result in current memory
				return 2;
			}
		}
		
		//other case ???
		//return 0;
	}


	protected static int getIndexToDrawWithOrgaId(int firstRowShownIdIT) {
		int indexToRestore = 0;
		for(int i=0;i<Insyght.APP_CONTROLER.getCurrentListFeaturedResultGenomePanelItem().size();i++){
			if(Insyght.APP_CONTROLER.getCurrentListFeaturedResultGenomePanelItem().get(i).getOrganismId()
					== firstRowShownIdIT){
				//found row
				return indexToRestore;
			}
			indexToRestore++;
		}
		for(int i=0;i<Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem().size();i++){
			if(Insyght.APP_CONTROLER.getCurrentListPublicResultGenomePanelItem().get(i).getOrganismId()
					== firstRowShownIdIT){
				//found row
				return indexToRestore;
			}
			indexToRestore++;
		}
		return 0;
	}

	public static boolean checkTockenIsCononical(String token) {
		for (EnumConvenianceURLParams convenianceURLParam : EnumConvenianceURLParams.values()) {
			if(token.contains(convenianceURLParam.toString())){
				return false;
			}
		}
		return true;
	}

	public static String setNewRefOrgaAsURLParameters(
			GenomePanelItem genomePanelItemSent
			) {
		
		String urlIT = Window.Location.getHref();//.getHash();
		//urlIT = urlIT.replaceFirst("&referenceOrigamiOrgaId=\\d+", "&referenceOrigamiOrgaId="+Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getOrganismId());

		int pReferenceOrgaIdIT = getSpecificIntParameterFromURLTockens(
				NavigationControler.pReferenceOrgaId, urlIT);
		if(pReferenceOrgaIdIT > 0){

//			OrganismItem oiIt = new OrganismItem();
//			oiIt.setOrganismId(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getOrganismId());
//			siToSend.setReferenceOrganism(oiIt);
////			siToSend.setReferenceOrganism(
////					Insyght.APP_CONTROLER.findOrganismItemWithOrigamiOrganismId(
////							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getOrganismId()
////					)
////				);
			
			urlIT = urlIT.replaceFirst(
					EnumCanonicalURLParams.referenceOrigamiOrgaId.toString()+"=\\d+"
					, EnumCanonicalURLParams.referenceOrigamiOrgaId.toString()+"="+genomePanelItemSent.getOrganismId()
					);

			
			// translate &listReferenceOrigamiGeneSetIds= correctly
			String pResultListReferenceGeneSetIdsIT = getSpecificStringParameterFromURLTockens(
					pResultListReferenceGeneSetIds, urlIT);
			if(!pResultListReferenceGeneSetIdsIT.isEmpty()){
				ArrayList<Integer> alComparedGeneIds = genomePanelItemSent.getAlAbsoPropQSGeneHomoItemQsSGeneId();
				String listComparedGeneIdsAsString = UtilitiesMethodsShared.getItemsAsStringFromCollection(alComparedGeneIds);
				urlIT = urlIT.replaceFirst(
						EnumCanonicalURLParams.listReferenceOrigamiGeneSetIds.toString()+"=[\\d,]+"
						, EnumCanonicalURLParams.listReferenceOrigamiGeneSetIds.toString()+"="+listComparedGeneIdsAsString
						);
			}
			
			
			// &listUserSelectedOrigamiOrgaIdsToBeResults
			String pResultListUserSelectedOrgaToBeResultsIdsIT = getSpecificStringParameterFromURLTockens(
					pResultListUserSelectedOrgaToBeResultsIds, urlIT);
			if(!pResultListUserSelectedOrgaToBeResultsIdsIT.isEmpty()){
				if (pResultListUserSelectedOrgaToBeResultsIdsIT.matches("\\b"+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId()+"\\b")) {
					// former ref orga already
				} else {
					urlIT = urlIT.replaceFirst(
							EnumCanonicalURLParams.listUserSelectedOrigamiOrgaIdsToBeResults.toString()+"="
							, EnumCanonicalURLParams.listUserSelectedOrigamiOrgaIdsToBeResults.toString()+"="
									+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId()+","
							);
				}
				
			} else {
				urlIT += "&"+EnumCanonicalURLParams.listUserSelectedOrigamiOrgaIdsToBeResults.toString()+"="
						+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getOrganismId();
				
			}
			
		}
		
		
		//TODO pResultReferenceOrigamiElementIdsThenStartPbthenStopPBLoopedIT	
		
		//TODO
		//siToSend.setFirstRowShownId(firstRowShownId);
		
		//TODO not working when navigate back using navigator back button
		
		return urlIT;
		
	}
	
	

//		public static void adaptUIAccordingToNavUrlParam(
//				//String group1PNavTabIT, String group1PFirstRowShownIdIT
//				) {
//			
//			//do not come from loading url but from direct navigation, return
//			if(group1PNavTabIT == null
//					|| group1PFirstRowShownIdIT == null){
//				//System.err.println("do not come from loading url but from direct navigation, return");
//				return;
//			}
//			
//			if(!group1PNavTabIT.isEmpty()){
//				if (group1PNavTabIT.compareTo("SEARCH-selectRefOrgaOrTaxoNode") == 0) {
//					SearchViewImpl.showCorrectStepSearchView("selectRefOrgaOrTaxoNode", false);
//					MainTabPanelView.tabPanel.selectTab(1, false);
//				} else if (group1PNavTabIT.compareTo("SEARCH-VisuGenomicOrgaOrBuildGeneSet") == 0) {
//					
//					if(SearchViewImpl.selectedRefTaxoNode != null
//							|| Insyght.APP_CONTROLER.getUserSearchItem().getReferenceOrganism() != null){
//						if(SearchViewImpl.geneSetBuildMode == 0){
//							//0 = CoreDispGenome
//							SearchViewImpl.showCorrectStepSearchView("chooseRefGeneSet-CoreDispensableGenome", false);
//						}else if(SearchViewImpl.geneSetBuildMode == 1){
//							//browse all gene of organism
//							SearchViewImpl.showCorrectStepSearchView("chooseRefGeneSet-BrowseAllGenes", false);
//						}else if(SearchViewImpl.geneSetBuildMode == 2){
//							//build gene set
//							SearchViewImpl.showCorrectStepSearchView("chooseRefGeneSet-BuildOwnGeneSetWithAvailableMolecules", false);
//						}else if(SearchViewImpl.geneSetBuildMode == 3){
//							//view genomic orga
//							SearchViewImpl.showCorrectStepSearchView("VisuGenomicOrga", false);
//						}else{
//							//???
//							SearchViewImpl.showCorrectStepSearchView("selectRefOrgaOrTaxoNode", false);
//						}
//					}else{
//						SearchViewImpl.showCorrectStepSearchView("selectRefOrgaOrTaxoNode", false);
//					}
//					MainTabPanelView.tabPanel.selectTab(1, false);
//				} else if (group1PNavTabIT.compareTo(EnumResultViewTypes.homolog_table.toString()) == 0) {
//					
//					//&firstRowShownId=
//					if(!group1PFirstRowShownIdIT.isEmpty()){
//						
//						final int firstRowShownIdIT = Integer.parseInt(group1PFirstRowShownIdIT);
//						//System.err.println("firstRowShownIdIT = "+firstRowShownIdIT);
//						Scheduler.get().scheduleDeferred(new ScheduledCommand() {
//								// @Override
//								public void execute() {
//									//if(firstRowShownIdIT != Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(1).getGenomePanelItem().getOrganismId()){
//										
//										waitFewMillisecondsForResultViewToLoad("updateResultsDisplay", firstRowShownIdIT, 2);
////										MainTabPanelView.tabPanel.selectTab(2, true);
////										GenomicOrgaAndHomoTableViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
////										Insyght.APP_CONTROLER.clearSyntenyCanvasSelection();
////										int indexToRestore = getIndexToDrawWithOrgaId(firstRowShownIdIT);
////										//System.err.println("drawing idx = "+indexToRestore);
////										CenterSLPAllGenomePanels.START_INDEX = indexToRestore;
////										CenterSLPAllGenomePanels.updateResultsDisplay(true, true, true);
//										     
//									//}
//								}
//							});
//					}
//				} else if (group1PNavTabIT.compareTo(EnumResultViewTypes.annotations_comparator.toString()) == 0) {
//					MainTabPanelView.tabPanel.selectTab(3, true);
//				} else if (group1PNavTabIT.compareTo(EnumResultViewTypes.genomic_organization.toString()) == 0) {
//					
	//
//					//&firstRowShownId=
//					if(!group1PFirstRowShownIdIT.isEmpty()){
//						//String group1PFirstRowShownIdIT = matcherPFirstRowShownId.getGroup(1);
//						final int firstRowShownIdIT = Integer.parseInt(group1PFirstRowShownIdIT);
//						//System.err.println("firstRowShownIdIT = "+firstRowShownIdIT);
//							Scheduler.get().scheduleDeferred(new ScheduledCommand() {
//								// @Override
//								public void execute() {
//									//if(firstRowShownIdIT != Insyght.APP_CONTROLER.getLIST_GENOME_PANEL().get(1).getGenomePanelItem().getOrganismId()){
//										waitFewMillisecondsForResultViewToLoad("updateResultsDisplay", firstRowShownIdIT, 4);						
////										MainTabPanelView.tabPanel.selectTab(4, true);
////										GenomicOrgaAndHomoTableViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
////										Insyght.APP_CONTROLER.clearSyntenyCanvasSelection();
////										int indexToRestore = getIndexToDrawWithOrgaId(firstRowShownIdIT);
////										//System.err.println("drawing idx = "+indexToRestore);
////										CenterSLPAllGenomePanels.START_INDEX = indexToRestore;
////										CenterSLPAllGenomePanels.updateResultsDisplay(true, true, true);
//									//}
//										
//								}
//								
//							});
//					}
//				} else if (group1PNavTabIT.compareTo("ADMIN") == 0) {
//					MainTabPanelView.tabPanel.selectTab(5, false);
//				} else {
//					//default view HOME->Overview->VideoIntro
//					MainTabPanelView.tabPanel.selectTab(0, true);
//				}
//			} else {
//				//default view HOME->Overview->VideoIntro
//				MainTabPanelView.tabPanel.selectTab(0, true);
//			}
//		}
		
}
