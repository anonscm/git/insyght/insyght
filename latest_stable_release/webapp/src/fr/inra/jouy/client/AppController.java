package fr.inra.jouy.client;


/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import fr.inra.jouy.client.view.crossSections.popUp.ManageListComparedOrganisms;
import fr.inra.jouy.client.view.result.AnnotCompaViewImpl;
import fr.inra.jouy.client.view.result.GenomePanel;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
import fr.inra.jouy.client.view.result.SyntenyCanvas;
import fr.inra.jouy.client.view.search.SearchViewImpl;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.comparators.LightOrganismItemComparators;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem;
import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem.GpiSizeState;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.TaxoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSSyntItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.HolderAbsoluteProportionItem;
import fr.inra.jouy.shared.pojos.users.PersonsObj;


public class AppController  {

	// list all objects
	private ArrayList<OrganismItem> listAllPublicOrganismItem;
	private ArrayList<LightGeneItem> listAllLightGeneItemFromSlectedElement;
	private ArrayList<LightOrganismItem> listAllPublicOrganismItemStep4;
	private OrganismItem defaultOrganismIfNoSelection = null;
	
	// user related
	private PersonsObj currentUser = null;
	private SearchItem userSearchItem = null;
	//private GeneWidget currSelctGeneWidget = null;
	private GenomePanel currSelctGenomePanel = null;
	private String currSelectedSyntenyCanvasItemRefId = null;
	

	// search Panel related
	
	private TaxoItem taxoTree = null;//root taxoItem
	private ArrayList<TaxoItem> listAllTaxoItem = new ArrayList<TaxoItem>();
	private HashMap<Integer,ArrayList<OrganismItem>> ncbiTaxonId2ListOrga = new HashMap<Integer, ArrayList<OrganismItem>>();
	
	// result Panel related
	private RefGenoPanelAndListOrgaResu currentRefGenomePanelAndListOrgaResult = null;
	private ArrayList<GenomePanelItem> currentListPublicResultGenomePanelItem = null;
	private ArrayList<GenomePanelItem> currentListPrivateResultGenomePanelItem = null;
	private ArrayList<GenomePanelItem> currentListFeaturedResultGenomePanelItem = null;
	
	
	//register GWT widget
	//public static ArrayList<ScrollPanel> LIST_SC_GENOME_PANEL;
	//public static ArrayList<GenomePanel> LIST_GENOME_PANEL;
	//private ArrayList<ScrollPanel> LIST_SC_GENOME_PANEL;
	private ArrayList<GenomePanel> LIST_GENOME_PANEL;
	
	public static int NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE = 0;
	
	public static int DATABASE_IS_genes_elements_genome_assembly_convenience = 0;//0 not initialised, 1 true, -1 false
	public static int DATABASE_IS_NO_MIRROR = 0;//0 not initialised, 1 true, -1 false

	public AppController() {
		
		// list all objects
		listAllPublicOrganismItem = new ArrayList<OrganismItem>();
		listAllLightGeneItemFromSlectedElement = new ArrayList<LightGeneItem>();
		setListAllPublicOrganismItemStep4(new ArrayList<LightOrganismItem>());
		
		//user related
		currentUser = new PersonsObj();
		userSearchItem = new SearchItem();
		//userSearchItem.setAlignmentParametersForSyntenyToStrict(false);
		
		
		// search Panel related
		//selectedLightElementItem = new LightElementItem();
		//listSlectedLightGeneItemFromSlectedElement = new ArrayList<LightGeneItem>();
		
		// result Panel related
		setCurrentRefGenomePanelAndListOrgaResult(new RefGenoPanelAndListOrgaResu());
		//setCurrentListPublicResultGenomePanelItem(new ArrayList<GenomePanelItem>());
		resetCurrentListPublicResultGenomePanelItem();
		//setCurrentListPrivateResultGenomePanelItem(new ArrayList<GenomePanelItem>());
		resetCurrentListPrivateResultGenomePanelItem();
		//setCurrentListFeaturedResultGenomePanelItem(new ArrayList<GenomePanelItem>());
		resetCurrentListFeaturedResultGenomePanelItem();
		//currentHomologieBrowsingResultItem = new HomologieBrowsingResultItem();
		
		//display related
		//no need
		
		//register GWT widget
		//setLIST_SC_GENOME_PANEL(new ArrayList<ScrollPanel>());
		setLIST_GENOME_PANEL(new ArrayList<GenomePanel>());
	}

	public void clearSyntenyCanvasSelection(){

		if(Insyght.APP_CONTROLER.getCurrSelctGenomePanel() != null){

			//clear info
			GenoOrgaAndHomoTablViewImpl.clearAllInfoInStack();
			
			//clear selected synteny canvas
			//System.out.println("clearSyntenyCanvasSelection selectedSliceX : "+Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedSliceX);
			
			if (Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedSliceX > -1
					&& Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() > -1) {
				
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.restoreToOriginCoordinateSpace();
				
				//clear style
				int idxArrayOfarrayIT = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedSliceX;
				//int idxCurrIdxWithinArray = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.indexStartBirdSynteny+Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedSliceX;
				int idxCurrIdxWithinArray = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(idxArrayOfarrayIT);
				//System.err.println("clearing idx : "+idxCurrIdxWithinArray);
				HolderAbsoluteProportionItem siIT = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(idxArrayOfarrayIT).getListHAPI().get(idxCurrIdxWithinArray);
				int numberOtherMatch = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed()+Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedSliceX
						).getListHAPI().size()-1;
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.contextBgk.translate(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.addToLeftToCenterDrawing+(SyntenyCanvas.SLICE_CANVAS_WIDTH*Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedSliceX),0);
				if(siIT.getAbsoluteProportionQComparableQSSpanItem() != null){
					if(siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQSGeneHomology((AbsoPropQSGeneHomoItem)siIT.getAbsoluteProportionQComparableQSSpanItem(),
								true, false,
								true, false, true, true, true, false, numberOtherMatch,
								//false, false, false,
								false, -1, -1,
								((AbsoPropQSGeneHomoItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getPreviousGenomicRegionSInsertion(),
								((AbsoPropQSGeneHomoItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getNextGenomicRegionSInsertion()
								//,Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedMainQS,
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedPreviousSOfNextSlice,
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedNextSOfPreviousSlice
								);
					}else if(siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
						
//						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().setCurrDisplayIndexAtSpecifiedAPIToDisplayIndex(
//								idxArrayOfarrayIT
//								, 0);
//						siIT = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(idxArrayOfarrayIT).getListHAPI().get(0);
						
						int numberPairsToAdd = ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getSyntenyNumberGene();
						//numberPairsToAdd += ((AbsoluteProportionQSSyntenyItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsContainedOtherSyntenies().size();
						//numberPairsToAdd += ((AbsoluteProportionQSSyntenyItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsMotherSyntenies().size();
						//numberPairsToAdd += ((AbsoluteProportionQSSyntenyItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getListOrigamiAlignmentIdsSprungOffSyntenies().size();
						
						Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQSSynteny(siIT.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType(),
								numberPairsToAdd,
								true, false, numberOtherMatch,
								((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).isContainedWithinAnotherMotherSynteny(),
								((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).isSprungOffAnotherMotherSynteny(),
								((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).isMotherOfSprungOffSyntenies(),
								((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).isMotherOfContainedOtherSyntenies(),
								((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getPreviousGenomicRegionSInsertion(),
								((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getNextGenomicRegionSInsertion(),
								false, -1, -1
								//,Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedMainQS,
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedPreviousSOfNextSlice,
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedNextSOfPreviousSlice
								);
						//boolean isContainedWithinAnotherMotherSynteny, boolean isSprungOffAnotherMotherSynteny,
						//boolean isMotherOfSprungOffSyntenies, boolean isMotherOfContainedOtherSyntenies
					}else{
						//System.err.println("ERROR in qs clearSyntenyCanvasSelection unrecognized class for object sent");
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in qs clearSyntenyCanvasSelection unrecognized class for object sent"));
					}	
				}else{
					if(siIT.getAbsoluteProportionQComparableQSpanItem() != null && Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedQ){
						if(siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem){
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQGeneInsertionOrMissingTarget((AbsoPropQGeneInserItem)siIT.getAbsoluteProportionQComparableQSpanItem(), true, false, numberOtherMatch,
									false, -1, -1);
						}else if(siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem){
							//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(siIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(), ((AbsoluteProportionQGenomicRegionInsertionItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getQGenomicRegionInsertionNumberGene(), true, false);
							int numberGenes = ((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getQGenomicRegionInsertionNumberGene();
							if(numberGenes < 0){
								int sizePb = 1 + ((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getqPbStopQGenomicRegionInsertionInElement() - ((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getqPbStartQGenomicRegionInsertionInElement();
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(
										siIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(), SyntenyCanvas.convertSizeBpToTextWithAppropriateUnit(sizePb), true, false,
										true,
										((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).isAnnotationCollision(),
										((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).isAnchoredToStartElement(),
										((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).isAnchoredToStopElement(),
										numberOtherMatch);
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQNoGeneRegionInsertion(sizePb+" bp", true, false);
							}else if(numberGenes == 0){
								int sizePb = ((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getqPbStopQGenomicRegionInsertionInElement() - ((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getqPbStartQGenomicRegionInsertionInElement();
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQNoGeneRegionInsertion(sizePb+" bp", true, false);
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(
										siIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(), SyntenyCanvas.convertSizeBpToTextWithAppropriateUnit(sizePb), true, false,
										true,
										((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).isAnnotationCollision(),
										((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).isAnchoredToStartElement(),
										((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).isAnchoredToStopElement(),
										numberOtherMatch);
							}else{
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(
										siIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(), numberGenes+" CDS", true, false,
										false,
										((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).isAnnotationCollision(),
										((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).isAnchoredToStartElement(),
										((AbsoPropQGenoRegiInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).isAnchoredToStopElement(),
										numberOtherMatch);
							}
						}else if(siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem){
							//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(siIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(), ((AbsoluteProportionQElementItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getqElementNumberGene(), true, false);
							int numberGenes = ((AbsoPropQElemItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getqElementNumberGene();
							if(numberGenes < 0){
								int sizePb = 1 + ((AbsoPropQElemItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getQsizeOfElementinPb();
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(
										siIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(), SyntenyCanvas.convertSizeBpToTextWithAppropriateUnit(sizePb), true, false,
										true, false,
										false, false,
										numberOtherMatch);
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQNoGeneRegionInsertion(sizePb+" bp", true, false);
							}else if(numberGenes == 0){
								int sizePb = ((AbsoPropQElemItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getQsizeOfElementinPb();
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQNoGeneRegionInsertion(sizePb+" bp", true, false);
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(
										siIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(), SyntenyCanvas.convertSizeBpToTextWithAppropriateUnit(sizePb), true, false,
										false, false,
										false, false,
										numberOtherMatch);
							}else{
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockQGenomicRegionInsertionOrMissingTarget(
										siIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType(), numberGenes+" CDS", true, false,
										false, false,
										false, false,
										numberOtherMatch);
							}
						}else{
							//System.err.println("ERROR in q clearSyntenyCanvasSelection unrecognized class for object sent");
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in q clearSyntenyCanvasSelection unrecognized class for object sent"));
						}
					}
					if(!Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedQ){//siIT.getAbsoluteProportionSComparableSSpanItem() != null && 
						
						//check if siIT.getAbsoluteProportionSComparableSSpanItem() != null ou previous/next genomic insertion
						AbsoPropSCompaSSpanItem apscssiIT = null;
						if(siIT.getAbsoluteProportionSComparableSSpanItem() != null){
							apscssiIT = siIT.getAbsoluteProportionSComparableSSpanItem();
						}else{
							if(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedPreviousSOfNextSlice){
								int idxNextSlice = idxArrayOfarrayIT+1;
								if(idxNextSlice >= 0 
										&& idxNextSlice < Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()){
									HolderAbsoluteProportionItem siNextSlice = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(idxNextSlice).getListHAPI()
											.get(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(idxNextSlice));
									if(siNextSlice.getAbsoluteProportionQComparableQSSpanItem() != null){
										if(siNextSlice.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
											apscssiIT = ((AbsoPropQSSyntItem)siNextSlice.getAbsoluteProportionQComparableQSSpanItem()).getPreviousGenomicRegionSInsertion();
										}
									}else if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() != null){
										if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem){
											if(((AbsoPropSGenoRegiInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
												apscssiIT = ((AbsoPropSGenoRegiInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetPreviousGenomicRegionSInsertion();
											}
										}else if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem){
											if(((AbsoPropSGeneInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
												apscssiIT = ((AbsoPropSGeneInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getIfMissingTargetPreviousGenomicRegionSInsertion();
											}
										}
									}
								}
							}else if(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedNextSOfPreviousSlice){
								int idxPreviousSlice = idxArrayOfarrayIT-1;
								if(idxPreviousSlice >= 0 
										&& idxPreviousSlice < Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()){
									HolderAbsoluteProportionItem siNextSlice = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(idxPreviousSlice).getListHAPI()
											.get(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(idxPreviousSlice));
									if(siNextSlice.getAbsoluteProportionQComparableQSSpanItem() != null){
										if(siNextSlice.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
											apscssiIT = ((AbsoPropQSSyntItem)siNextSlice.getAbsoluteProportionQComparableQSSpanItem()).getNextGenomicRegionSInsertion();
										}
									}else if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() != null){
										if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem){
											if(((AbsoPropSGenoRegiInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
												apscssiIT = ((AbsoPropSGenoRegiInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetNextGenomicRegionSInsertion();
											}
										}else if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem){
											if(((AbsoPropSGeneInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
												apscssiIT = ((AbsoPropSGeneInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getIfMissingTargetNextGenomicRegionSInsertion();
											}
										}
									}
								}
							}
						}
						
						if(apscssiIT != null){

							if(apscssiIT instanceof AbsoPropSGeneInserItem){
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockSGeneInsertionOrMissingTarget((AbsoPropSGeneInserItem)apscssiIT, true, false, numberOtherMatch,
										false, -1, -1);
							}else if(apscssiIT instanceof AbsoPropSGenoRegiInserItem){
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(), ((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).getSGenomicRegionInsertionNumberGene(), true, false);
								int numberGenes = ((AbsoPropSGenoRegiInserItem)apscssiIT).getSGenomicRegionInsertionNumberGene();
								if(numberGenes <= 0){
									int sizePb = 1+((AbsoPropSGenoRegiInserItem)apscssiIT).getsPbStopSGenomicRegionInsertionInElement() - ((AbsoPropSGenoRegiInserItem)apscssiIT).getsPbStartSGenomicRegionInsertionInElement();
									Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockSGenomicRegionInsertionOrMissingTarget(
											apscssiIT.getSEnumBlockType(), SyntenyCanvas.convertSizeBpToTextWithAppropriateUnit(sizePb), true, false,
											true,
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnchoredToPrevious(),
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnchoredToNext(),
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnnotationCollision(),
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnchoredToStartElement(),
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnchoredToStopElement(),
											numberOtherMatch);
//									Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockSNoGeneRegionInsertion(sizePb+" bp", true, false,
//											((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToPrevious(),
//											((AbsoluteProportionSGenomicRegionInsertionItem)apscssiIT).isAnchoredToNext());
								}else{
									Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockSGenomicRegionInsertionOrMissingTarget(
											apscssiIT.getSEnumBlockType(), numberGenes+" CDS", true, false,
											false,
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnchoredToPrevious(),
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnchoredToNext(),
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnnotationCollision(),
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnchoredToStartElement(),
											((AbsoPropSGenoRegiInserItem)apscssiIT).isAnchoredToStopElement(),
											numberOtherMatch);
								}
							}else if(apscssiIT instanceof AbsoPropSElemItem){
								//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockSGenomicRegionInsertionOrMissingTarget(apscssiIT.getSEnumBlockType(), ((AbsoluteProportionSElementItem)apscssiIT).getsElementNumberGene(), true, false);
								int numberGenes = ((AbsoPropSElemItem)apscssiIT).getsElementNumberGene();
								if(numberGenes <= 0){
									int sizePb = 1+((AbsoPropSElemItem)apscssiIT).getsSizeOfElementinPb();
									Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockSGenomicRegionInsertionOrMissingTarget(
											apscssiIT.getSEnumBlockType(), SyntenyCanvas.convertSizeBpToTextWithAppropriateUnit(sizePb), true, false,
											true, true, true,
											false,
											false, false,
											numberOtherMatch);
									//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockSNoGeneRegionInsertion(sizePb+" bp", true, false);
								}else{
									Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlockSGenomicRegionInsertionOrMissingTarget(
											apscssiIT.getSEnumBlockType(), numberGenes+" CDS", true, false,
											false, false, false,
											false,
											false, false,
											numberOtherMatch);
								}
							}else{
								//System.err.println("ERROR in s clearSyntenyCanvasSelection unrecognized class for object sent");
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in s clearSyntenyCanvasSelection unrecognized class for object sent = "+apscssiIT.getClass().toString()));
							}
						}
						
						
					}
				}
				//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawBlock(siIT.getSyntenyBlockType(), siIT.getSyntenyNumberGene(), siIT.getQNumberGene(), siIT.getSNumberGene(), true, false);
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.restoreToOriginCoordinateSpace();
				if(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getSizeState().compareTo(GenomePanelItem.GpiSizeState.MAX)==0){
					//set style bottom
					Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.contextBgk.translate(0, SyntenyCanvas.TOTAL_CANVAS_HEIGHT_UPPER_PART);
					
					if(siIT.getAbsoluteProportionQComparableQSSpanItem() != null){
						
						if(siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem
								|| 
								siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem
										){
							
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawSyntenyRepersentationProportionateQS(siIT.getAbsoluteProportionQComparableQSSpanItem(),
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(0).getQsizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()),
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(0).getQsizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()),
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
									true, false, true);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawScaffoldGenome(true, 0, 1, true, false, false);
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawScaffoldGenome(false, 0, 1, true, false, false);
							
							String colorBgkIT = "";
							if(siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
								colorBgkIT = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.getBkgColorAssociatedWithColorIDAsString(((AbsoPropQSGeneHomoItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getQsStyleItem().getBkgColor());
							}else{
								colorBgkIT = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.getDefaultBkgColorAccordingToQSBlockType(siIT.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType());
						
							}

							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawSyntenyMarkerScaffold(true, true,
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(0).getQsizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()),
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(0).getQsizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()),
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
									true, false, false,
									colorBgkIT
									);
							
						}else if(siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem
								&& Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedPreviousSOfNextSlice){
							
							AbsoPropSGenoRegiInserItem apsgiiIT = ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getPreviousGenomicRegionSInsertion();
							if(apsgiiIT != null){
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawGenomicRegionRepersentationProportionateS(apsgiiIT,
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apsgiiIT.getsPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apsgiiIT.getsPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										true, false, true);

								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawScaffoldGenome(false, 0, 1, true, false, false);
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawSyntenyMarkerScaffold(false, true,
										-1,
										-1,
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apsgiiIT.getsPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apsgiiIT.getsPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										true, false, false,
										Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.getDefaultBkgColorAccordingToSBlockType(apsgiiIT.getSEnumBlockType())
										);
							
							}
							
						}else if(siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem
								&& Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedNextSOfPreviousSlice){

							AbsoPropSGenoRegiInserItem apsgiiIT = ((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getNextGenomicRegionSInsertion();
							if(apsgiiIT != null){
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawGenomicRegionRepersentationProportionateS(apsgiiIT,
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apsgiiIT.getsPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apsgiiIT.getsPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										true, false, true);

								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawScaffoldGenome(false, 0, 1, true, false, false);
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawSyntenyMarkerScaffold(false, true,
										-1,
										-1,
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apsgiiIT.getsPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apsgiiIT.getsPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										true, false, false,
										Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.getDefaultBkgColorAccordingToSBlockType(apsgiiIT.getSEnumBlockType())
										);
							
							}
							
							
						}else{
							//System.err.println("ERROR in drawSyntenyRepersentationProportionateQS qs clearSyntenyCanvasSelection unrecognized class for object sent");
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in drawSyntenyRepersentationProportionateQS qs clearSyntenyCanvasSelection unrecognized class for object sent"));
						}
						
						
					}else{
						if(siIT.getAbsoluteProportionQComparableQSpanItem() != null && Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedQ){
							
							if(siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem
									|| siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem
									|| siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem){
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawGenomicRegionRepersentationProportionateQ(siIT.getAbsoluteProportionQComparableQSpanItem(),
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(0).getQsizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSpanItem().getqPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()),
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(0).getQsizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSpanItem().getqPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()),
										true, false, true);
							}else{
								//System.err.println("ERROR in drawSyntenyRepersentationProportionateQ q clearSyntenyCanvasSelection unrecognized class for object sent");
								Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in drawSyntenyRepersentationProportionateQ q clearSyntenyCanvasSelection unrecognized class for object sent"));
							}
							
							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawScaffoldGenome(true, 0, 1, true, false, false);
							
							String colorBgkIT = "";
							if(siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem){
								colorBgkIT = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.getBkgColorAssociatedWithColorIDAsString(((AbsoPropQGeneInserItem)siIT.getAbsoluteProportionQComparableQSpanItem()).getqStyleItem().getBkgColor());
							}else{
								colorBgkIT = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.getDefaultBkgColorAccordingToQBlockType(siIT.getAbsoluteProportionQComparableQSpanItem().getqEnumBlockType());
							}

							Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawSyntenyMarkerScaffold(true, false,
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(0).getQsizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSpanItem().getqPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()),
									SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getReferenceGenomePanelItem().getListAbsoluteProportionElementItemQ().get(0).getQsizeOfOragnismInPb(), siIT.getAbsoluteProportionQComparableQSpanItem().getqPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopQGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartQGenome()),
									-1,
									-1,
									true, false, false,
									colorBgkIT
									);
							
						}
						if(!Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedQ){
							//System.err.println("here1");
							//check if siIT.getAbsoluteProportionSComparableSSpanItem() != null ou previous/next genomic insertion
							AbsoPropSCompaSSpanItem apscssiIT = null;
							if(siIT.getAbsoluteProportionSComparableSSpanItem() != null){
								apscssiIT = siIT.getAbsoluteProportionSComparableSSpanItem();
							}else{
								if(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedPreviousSOfNextSlice){
									//System.err.println("here2");
									int idxNextSlice = idxArrayOfarrayIT+1;
									if(idxNextSlice >= 0 
											&& idxNextSlice < Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()){
										HolderAbsoluteProportionItem siNextSlice = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(idxNextSlice).getListHAPI()
												.get(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(idxNextSlice));
										if(siNextSlice.getAbsoluteProportionQComparableQSSpanItem() != null){
											if(siNextSlice.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
												apscssiIT = ((AbsoPropQSSyntItem)siNextSlice.getAbsoluteProportionQComparableQSSpanItem()).getPreviousGenomicRegionSInsertion();
											}
										}else if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() != null){
											//System.err.println("here3");
											if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem){
												if(((AbsoPropSGenoRegiInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
													apscssiIT = ((AbsoPropSGenoRegiInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetPreviousGenomicRegionSInsertion();
												}
											}else if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem){
												//System.err.println("here4");
												if(((AbsoPropSGeneInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
													apscssiIT = ((AbsoPropSGeneInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getIfMissingTargetPreviousGenomicRegionSInsertion();
													//System.err.println("here5 : "+apscssiIT);
												}
											}
										}
									}
								}else if(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedNextSOfPreviousSlice){
									int idxPreviousSlice = idxArrayOfarrayIT-1;
									if(idxPreviousSlice >= 0 
											&& idxPreviousSlice < Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()){
										HolderAbsoluteProportionItem siNextSlice = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(idxPreviousSlice).getListHAPI()
												.get(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(idxPreviousSlice));
										if(siNextSlice.getAbsoluteProportionQComparableQSSpanItem() != null){
											if(siNextSlice.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
												apscssiIT = ((AbsoPropQSSyntItem)siNextSlice.getAbsoluteProportionQComparableQSSpanItem()).getNextGenomicRegionSInsertion();
											}
										}else if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() != null){
											if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem){
												if(((AbsoPropSGenoRegiInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
													apscssiIT = ((AbsoPropSGenoRegiInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetNextGenomicRegionSInsertion();
												}
											}else if(siNextSlice.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem){
												if(((AbsoPropSGeneInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
													apscssiIT = ((AbsoPropSGeneInserItem)siNextSlice.getAbsoluteProportionSComparableSSpanItem()).getIfMissingTargetNextGenomicRegionSInsertion();
												}
											}
										}
									}
								}
							}
							
							if(apscssiIT != null){
								if(apscssiIT instanceof AbsoPropSGenoRegiInserItem
										|| apscssiIT instanceof AbsoPropSElemItem
										|| apscssiIT instanceof AbsoPropSGeneInserItem){
									Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawGenomicRegionRepersentationProportionateS(apscssiIT,
											SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apscssiIT.getsPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
											SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apscssiIT.getsPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
											true, false, true);
								}else{
									//System.err.println("ERROR in drawSyntenyRepersentationProportionateS s clearSyntenyCanvasSelection unrecognized class for object sent");
									Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in drawSyntenyRepersentationProportionateS s clearSyntenyCanvasSelection unrecognized class for object sent"));
								}
								
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawScaffoldGenome(false, 0, 1, true, false, false);
								
								String colorBgkIT = "";
								if(apscssiIT instanceof AbsoPropSGeneInserItem){
									colorBgkIT = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.getBkgColorAssociatedWithColorIDAsString(((AbsoPropSGeneInserItem)apscssiIT).getsStyleItem().getBkgColor());
								}else{
									colorBgkIT = Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.getDefaultBkgColorAccordingToSBlockType(apscssiIT.getSEnumBlockType());
								}
								
								Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.drawSyntenyMarkerScaffold(false, true,
										-1,
										-1,
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apscssiIT.getsPercentStart(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										SyntenyCanvas.claculateRelativePercentage(Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getListAbsoluteProportionElementItemS().get(0).getsSizeOfOragnismInPb(), apscssiIT.getsPercentStop(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStopSGenome(), Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomePanelItem().getPercentSyntenyShowStartSGenome()),
										true, false, false,
										colorBgkIT
										);
							}
						}
					}
					

					Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.restoreToOriginCoordinateSpace();
					
				}
				
				//clear variable 
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedSliceX = -1;
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedQ = false;
				//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedMainQS = false;
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedPreviousSOfNextSlice = false;
				Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedNextSOfPreviousSlice = false;
				setCurrSelectedSyntenyCanvasItemRefId(null);
			}
			
			//clear variable bis
			Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedSliceX = -1;
			Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedQ = false;
			//Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedMainQS = false;
			Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedPreviousSOfNextSlice = false;
			Insyght.APP_CONTROLER.getCurrSelctGenomePanel().syntenyCanvas.selectedNextSOfPreviousSlice = false;
			setCurrSelectedSyntenyCanvasItemRefId(null);
			
			
		}
		// clear selected genome Panel
		//GenomePanel.clearCurrSelectedGenomePanel();
	}
	
	
	public void clearGenomePanelSelection(){
		// clear selected genome Panel
		if (Insyght.APP_CONTROLER.getCurrSelctGenomePanel() != null) {
			//clear info
			//OLD GenomicOrgaAndHomoTableViewImpl.clearGenomeInfoInStack();
			GenoOrgaAndHomoTablViewImpl.clearAllInfoInStack();
			//clear style
			Insyght.APP_CONTROLER.getCurrSelctGenomePanel().getGenomeNameLabel().removeStyleDependentName("GenomePanel-SelectedGenome");
			//clear variable
			Insyght.APP_CONTROLER.setCurrSelctGenomePanel(null);
		}
	}
	
	
	public void clearAllUserSelection(){
		if(Insyght.APP_CONTROLER.getCurrSelctGenomePanel() != null){
			//clearGeneWidgetSelection();
			clearSyntenyCanvasSelection();
			clearGenomePanelSelection();
		}
	}
	
	
	public void clearAppControlerFromAllResultData(){
		setCurrentRefGenomePanelAndListOrgaResult(new RefGenoPanelAndListOrgaResu());
		AnnotCompaViewImpl.listDisplayedGeneIdsAnnotCompaHasChanged = true;
		ManageListComparedOrganisms.listDataProviderLOIMainListHasChanged = true;
		resetAllCurrentListResultGenomePanelItem();
	}
	
	public void resetAllCurrentListResultGenomePanelItem(){
		resetCurrentListPublicResultGenomePanelItem();
		resetCurrentListPrivateResultGenomePanelItem();
		resetCurrentListFeaturedResultGenomePanelItem();
		if(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStart() >= 0
				&& currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStop() >= 0
				&& currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneOnQElementId() >= 0){
			currentRefGenomePanelAndListOrgaResult.setGlobalLookUpForGeneWithQStart(-1);
			currentRefGenomePanelAndListOrgaResult.setGlobalLookUpForGeneWithQStop(-1);
			currentRefGenomePanelAndListOrgaResult.setGlobalLookUpForGeneOnQElementId(-1);
		}
	}
	
	
	public void resetCurrentListPublicResultGenomePanelItem(){
		if(getCurrentListPublicResultGenomePanelItem() == null){
			setCurrentListPublicResultGenomePanelItem(new ArrayList<GenomePanelItem>());
		}else{
			getCurrentListPublicResultGenomePanelItem().clear();
			if(currentRefGenomePanelAndListOrgaResult != null){
				if(currentRefGenomePanelAndListOrgaResult.getLstOrgaResult() != null){
					for(int i=0;i<currentRefGenomePanelAndListOrgaResult.getLstOrgaResult().size();i++){
						GenomePanelItem newGPIIT = new GenomePanelItem(currentRefGenomePanelAndListOrgaResult.getLstOrgaResult().get(i));
						newGPIIT.setReferenceGenome(false);
						//newGPIIT.setResultPosition(i+1);
						if(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStart() >= 0
								&& currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStop() >= 0
								&& currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneOnQElementId() > 0){
							//newGPIIT.setLookUpForSymbolicSliceAtQElementId(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForSymbolicSliceAtQElementId());
							//newGPIIT.setLookUpForSymbolicSliceAtQPosition(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForSymbolicSliceAtQPosition());
							newGPIIT.setLookUpForGeneWithQStart(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStart(), EnumResultViewTypes.genomic_organization);
							newGPIIT.setLookUpForGeneWithQStop(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStop(), EnumResultViewTypes.genomic_organization);
							newGPIIT.setLookUpForGeneOnQElementId(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneOnQElementId(), EnumResultViewTypes.genomic_organization);
						}
						if(currentRefGenomePanelAndListOrgaResult.getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
							newGPIIT.setSizeState(GpiSizeState.INTERMEDIATE);
						}else{
							newGPIIT.setSizeState(GpiSizeState.MAX);
						}
						getCurrentListPublicResultGenomePanelItem().add(newGPIIT);
					}
				}
			}
		}
		
	}
	
	public void resetCurrentListPrivateResultGenomePanelItem(){
		if(getCurrentListPrivateResultGenomePanelItem() == null){
			setCurrentListPrivateResultGenomePanelItem(new ArrayList<GenomePanelItem>());
		}else{
			getCurrentListPrivateResultGenomePanelItem().clear();
			if(Insyght.APP_CONTROLER.getCurrentUser() != null){
				if(Insyght.APP_CONTROLER.getCurrentUser().getListGroupsSubscribedAsOrganismItem() != null){
					for(int i=0;i<Insyght.APP_CONTROLER.getCurrentUser().getListGroupsSubscribedAsOrganismItem().size();i++){
						GenomePanelItem newGPIIT = new GenomePanelItem(Insyght.APP_CONTROLER.getCurrentUser().getListGroupsSubscribedAsOrganismItem().get(i));
						newGPIIT.setReferenceGenome(false);
						//newGPIIT.setResultPosition(i);
						if(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStart() >= 0
								&& currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStop() >= 0
								&& currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneOnQElementId() >= 0){
							newGPIIT.setLookUpForGeneWithQStart(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStart(), EnumResultViewTypes.genomic_organization);
							newGPIIT.setLookUpForGeneWithQStop(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStop(), EnumResultViewTypes.genomic_organization);
							newGPIIT.setLookUpForGeneOnQElementId(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneOnQElementId(), EnumResultViewTypes.genomic_organization);
						}
						if(currentRefGenomePanelAndListOrgaResult.getViewTypeInsyght() != null){
							if(currentRefGenomePanelAndListOrgaResult.getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
								newGPIIT.setSizeState(GpiSizeState.INTERMEDIATE);
							}else{
								newGPIIT.setSizeState(GpiSizeState.MAX);
							}
						}
						getCurrentListPrivateResultGenomePanelItem().add(newGPIIT);
					}
				}
			}
		}
	}
	
	public void resetCurrentListFeaturedResultGenomePanelItem(){
		if(getCurrentListFeaturedResultGenomePanelItem() == null){
			setCurrentListFeaturedResultGenomePanelItem(new ArrayList<GenomePanelItem>());
		}else{
			getCurrentListFeaturedResultGenomePanelItem().clear();
			if(currentRefGenomePanelAndListOrgaResult != null){
				if(currentRefGenomePanelAndListOrgaResult.getListUserSelectedOrgaToBeResults() != null){
					for(int i=0;i<currentRefGenomePanelAndListOrgaResult.getListUserSelectedOrgaToBeResults().size();i++){
						GenomePanelItem newGPIIT = new GenomePanelItem(currentRefGenomePanelAndListOrgaResult.getListUserSelectedOrgaToBeResults().get(i));
						newGPIIT.setReferenceGenome(false);
						//newGPIIT.setResultPosition(i);
						if(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStart() >= 0
								&& currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStop() >= 0
								&& currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneOnQElementId() >= 0){
							newGPIIT.setLookUpForGeneWithQStart(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStart(), EnumResultViewTypes.genomic_organization);
							newGPIIT.setLookUpForGeneWithQStop(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneWithQStop(), EnumResultViewTypes.genomic_organization);
							newGPIIT.setLookUpForGeneOnQElementId(currentRefGenomePanelAndListOrgaResult.getGlobalLookUpForGeneOnQElementId(), EnumResultViewTypes.genomic_organization);
						}
						if(currentRefGenomePanelAndListOrgaResult.getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
							newGPIIT.setSizeState(GpiSizeState.INTERMEDIATE);
						}else{
							newGPIIT.setSizeState(GpiSizeState.MAX);
						}
						getCurrentListFeaturedResultGenomePanelItem().add(newGPIIT);
					}
				}
			}
		}
	}
	
	
	public PersonsObj getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(PersonsObj currentUser) {
		this.currentUser = currentUser;
	}

	public ArrayList<OrganismItem> getListAllPublicOrganismItem() {
		return listAllPublicOrganismItem;
	}

	public void setListAllPublicOrganismItem(ArrayList<OrganismItem> listAllOrganismItem) {
		this.listAllPublicOrganismItem = listAllOrganismItem;
	}

	
	public ArrayList<LightGeneItem> getAllListLightGeneItemFromSlectedElement() {
		return listAllLightGeneItemFromSlectedElement;
	}

	public void setAllListLightGeneItemFromSlectedElement(
			ArrayList<LightGeneItem> listLightGeneItemFromSlectedElement) {
		this.listAllLightGeneItemFromSlectedElement = listLightGeneItemFromSlectedElement;
	}

	public void setUserSearchItem(SearchItem userSearchItem) {
		this.userSearchItem = userSearchItem;
	}

	public SearchItem getUserSearchItem() {
		return userSearchItem;
	}


	public void setCurrSelctGenomePanel(GenomePanel currSelctGenomePanel) {
		this.currSelctGenomePanel = currSelctGenomePanel;
	}

	public GenomePanel getCurrSelctGenomePanel() {
		return currSelctGenomePanel;
	}

	public void setCurrentRefGenomePanelAndListOrgaResult(
			RefGenoPanelAndListOrgaResu currentRefGenomePanelAndListOrgaResult) {
		this.currentRefGenomePanelAndListOrgaResult = currentRefGenomePanelAndListOrgaResult;
	}

	public RefGenoPanelAndListOrgaResu getCurrentRefGenomePanelAndListOrgaResult() {
		return currentRefGenomePanelAndListOrgaResult;
	}

	
	public void setListAllPublicOrganismItemStep4(ArrayList<LightOrganismItem> listAllOrganismItemStep4) {
		this.listAllPublicOrganismItemStep4 = listAllOrganismItemStep4;
	}

	public ArrayList<LightOrganismItem> getListAllPublicOrganismItemStep4() {
		return listAllPublicOrganismItemStep4;
	}

	
	public void setCurrentListPublicResultGenomePanelItem(
			ArrayList<GenomePanelItem> currentListPublicResultGenomePanelItem) {
		this.currentListPublicResultGenomePanelItem = currentListPublicResultGenomePanelItem;
	}

	public ArrayList<GenomePanelItem> getCurrentListPublicResultGenomePanelItem() {
		return currentListPublicResultGenomePanelItem;
	}

	public void setCurrentListPrivateResultGenomePanelItem(
			ArrayList<GenomePanelItem> currentListPrivateResultGenomePanelItem) {
		this.currentListPrivateResultGenomePanelItem = currentListPrivateResultGenomePanelItem;
	}

	public ArrayList<GenomePanelItem> getCurrentListPrivateResultGenomePanelItem() {
		return currentListPrivateResultGenomePanelItem;
	}

	public void setCurrentListFeaturedResultGenomePanelItem(
			ArrayList<GenomePanelItem> currentListFeaturedResultGenomePanelItem) {
		this.currentListFeaturedResultGenomePanelItem = currentListFeaturedResultGenomePanelItem;
	}

	public ArrayList<GenomePanelItem> getCurrentListFeaturedResultGenomePanelItem() {
		return currentListFeaturedResultGenomePanelItem;
	}

	public String getCurrSelectedSyntenyCanvasItemRefId() {
		return currSelectedSyntenyCanvasItemRefId;
	}

	public void setCurrSelectedSyntenyCanvasItemRefId(
			String currSelectedSyntenyCanvasItemRefIdSent) {
		this.currSelectedSyntenyCanvasItemRefId = currSelectedSyntenyCanvasItemRefIdSent;
	}
	
	public ArrayList<GenomePanel> getLIST_GENOME_PANEL() {
		return LIST_GENOME_PANEL;
	}
	public void setLIST_GENOME_PANEL(ArrayList<GenomePanel> lIST_GENOME_PANEL) {
		LIST_GENOME_PANEL = lIST_GENOME_PANEL;
	}

	/**
	 * @return the root taxoItem
	 */
	public TaxoItem getTaxoTree() {
		return taxoTree;
	}
	/**
	 * @param the root taxoItem o be set
	 */
	public void setTaxoTree(TaxoItem taxoTree) {
		this.taxoTree = taxoTree;
	}

	public ArrayList<TaxoItem> getListAllTaxoItem() {
		return listAllTaxoItem;
	}
	
	public void setListAllTaxoItem(ArrayList<TaxoItem> listAllTaxoItem) {
		this.listAllTaxoItem = listAllTaxoItem;
	}

	// not really needed
//	public TaxoItem getUniqueTaxoItemWithPathInTree(ArrayList<Integer> lineageInCellBrowserByIdxSent) {
//		for(int i=0;i<getListAllTaxoItem().size();i++){
//			TaxoItem tiIT = getListAllTaxoItem().get(i);
//			if(tiIT.getFullPathInHierarchie().equals(lineageInCellBrowserByIdxSent)){
//				return tiIT;
//			}
//		}
//		return null;
//	}
	
	
	//return exact match in position 0
	public ArrayList<TaxoItem> getListTaxoItemAndSubNodesWithPathInTree(ArrayList<Integer> lineageInCellBrowserByIdxSent) {
		ArrayList<TaxoItem> alToReturn = new ArrayList<TaxoItem>();
		String hierarAsStringToMatch = UtilitiesMethodsShared.getItemsAsStringFromCollection(lineageInCellBrowserByIdxSent);
		//String hierarAsStringToMatch = lineageInCellBrowserByIdxSent.toString().replaceAll("[\\[\\]]", "");
		for(int i=0;i<getListAllTaxoItem().size();i++){
			TaxoItem tiIT = getListAllTaxoItem().get(i);
			if(ensureTaxoNameNotAHeader(tiIT)){
				if (UtilitiesMethodsShared.getItemsAsStringFromCollection(tiIT.getFullPathInHierarchie()).compareTo(hierarAsStringToMatch) == 0) {
					alToReturn.add(0, tiIT);
				} else	if ( UtilitiesMethodsShared.getItemsAsStringFromCollection(tiIT.getFullPathInHierarchie()).startsWith(hierarAsStringToMatch)){
					alToReturn.add(tiIT);
				}
			}

		}
		return alToReturn;
	}
	
	
	//OLD NOT NEEDED
//	public boolean setPresenceAbsenceEitherOrUndefInTaxoTreeWithTaxoNodeChange(TaxoItem taxoIITSent, String typeOfChange){
//		
//		//typeOfChange :
//		//EitherPresenceOrAbsence
//		//RefTaxoNode
//		//Presence
//		//Absence
//		
//		//set node and downstream
//		ArrayList<TaxoItem> alTaxonItemUnderSelectedNode = Insyght.APP_CONTROLER.getListTaxoItemAndSubNodesWithPathInTree(taxoIITSent.getFullPathInHierarchie());
//		
//		//System.err.println(alTaxonItemUnderSelectedNode.toString());
//		boolean refGenomeIsUnderChangedNode = false;
//		
//		for(TaxoItem tiIT : alTaxonItemUnderSelectedNode){
//			if(typeOfChange.compareTo("RefTaxoNode")==0){
//				
//				if(tiIT.getAssociatedOrganismItem() != null){
//					//System.err.println("downstream RefTaxoNode : "+tiIT.getFullName());
//					tiIT.setRefGenomeInRefNodeContext(true);
//					tiIT.setAssertPresenceInRefNodeContext(false);
//					tiIT.setAssertAbsenceInRefNodeContext(false);
//					tiIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
//					break;
//				}
//			}else if(typeOfChange.compareTo("Presence")==0){
//				if(!tiIT.isRefGenomeInRefNodeContext()){
//					//System.err.println("downstream Presence : "+tiIT.getFullName());
//					tiIT.setRefGenomeInRefNodeContext(false);
//					tiIT.setAssertPresenceInRefNodeContext(true);
//					tiIT.setAssertAbsenceInRefNodeContext(false);
//					tiIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
//				}else{
//					refGenomeIsUnderChangedNode = true;
//				}
//			}else if(typeOfChange.compareTo("Absence")==0){
//				if(!tiIT.isRefGenomeInRefNodeContext()){
//					//System.err.println("downstream Absence : "+tiIT.getFullName());
//					tiIT.setRefGenomeInRefNodeContext(false);
//					tiIT.setAssertPresenceInRefNodeContext(false);
//					tiIT.setAssertAbsenceInRefNodeContext(true);
//					tiIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
//				}else{
//					refGenomeIsUnderChangedNode = true;
//				}
//			}else if(typeOfChange.compareTo("EitherPresenceOrAbsence")==0){
//				if(!tiIT.isRefGenomeInRefNodeContext()){
//					//System.err.println("downstream EitherPresenceOrAbsence : "+tiIT.getFullName());
//					tiIT.setRefGenomeInRefNodeContext(false);
//					tiIT.setAssertPresenceInRefNodeContext(false);
//					tiIT.setAssertAbsenceInRefNodeContext(false);
//					tiIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(true);
//				}else{
//					refGenomeIsUnderChangedNode = true;
//				}
//			}else {
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in setPresenceAbsenceBothOrUndefInTaxoTreeWithTaxoNodeChange : unrecognized typeOfChange"));
//				return false;
//			}
//		}
//		
//		if(refGenomeIsUnderChangedNode){
//			return true;
//		}
//		
//		//set upstream if not type of change RefTaxoNode
//		//if(typeOfChange.compareTo("RefTaxoNode")!=0){
//		getTaxoItemParentRecursivelyAndAssessPresenceAbsenceEitherMixedState(taxoIITSent);
//		//}
//		return false;
//		
//	}
//	
//	private void getTaxoItemParentRecursivelyAndAssessPresenceAbsenceEitherMixedState(
//			TaxoItem taxoIITSent) {
//		//typeOfChange :
//		//EitherPresenceOrAbsence
//		//Presence
//		//Absence
//		//mixed
//		
//		if(taxoIITSent == null){
//			return;
//		}
//		
//		TaxoItem parentTaxoItemIT = taxoIITSent.getParentTaxoItem();
//		boolean parentTaxoItemITStateChanged = false;
//		
//		if(parentTaxoItemIT.getTaxoChildrens().isEmpty()){
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getTaxoItemParentRecursivelyAndAssessPresenceAbsenceEitherMixedState : parentTaxoItemIT.getTaxoChildrens().isEmpty"));
//			return;
//		}
//		
//		//check if all children are consistent or not
//		boolean variableFirstChildRegardingPresence = false;
//		boolean variableFirstChildRegardingAbsence = false;
//		boolean variableFirstChildRegardingEitherPresenceAbsence = false;
//		
//		boolean childrenAreInconsistent = false;
//		int countLoopRealTaxo = 0;
//		for(int i=0;i<parentTaxoItemIT.getTaxoChildrens().size();i++){
//			TaxoItem childOfParentTIIT = parentTaxoItemIT.getTaxoChildrens().get(i);
//			if(ensureTaxoNameNotAHeader(childOfParentTIIT)){
//				if(countLoopRealTaxo == 0){
////					System.err.println("First child : "+childOfParentTIIT.isAssertPresenceInRefNodeContext() +
////							", " + childOfParentTIIT.isAssertAbsenceInRefNodeContext() +
////							", " + childOfParentTIIT.isAssertEitherPresenceOrAbsenceInRefNodeContext()
////							);
//					variableFirstChildRegardingPresence = childOfParentTIIT.isAssertPresenceInRefNodeContext();
//					if(!variableFirstChildRegardingPresence){
//						variableFirstChildRegardingPresence = childOfParentTIIT.isRefGenomeInRefNodeContext();
//					}
//					variableFirstChildRegardingAbsence = childOfParentTIIT.isAssertAbsenceInRefNodeContext();
//					variableFirstChildRegardingEitherPresenceAbsence = childOfParentTIIT.isAssertEitherPresenceOrAbsenceInRefNodeContext();
//					if(!variableFirstChildRegardingPresence
//							&&!variableFirstChildRegardingAbsence
//							&&!variableFirstChildRegardingEitherPresenceAbsence
//							&& !childOfParentTIIT.isRefGenomeInRefNodeContext()
//							){
//						childrenAreInconsistent = true;
//						break;
//					}
//				}else{
//					if(childOfParentTIIT.isRefGenomeInRefNodeContext()){
//						if(!variableFirstChildRegardingPresence){
//							childrenAreInconsistent = true;
//							break;
//						}
//					}
//					if(childOfParentTIIT.isAssertPresenceInRefNodeContext() != variableFirstChildRegardingPresence){
//						childrenAreInconsistent = true;
//						break;
//					}
//					if(childOfParentTIIT.isAssertAbsenceInRefNodeContext() != variableFirstChildRegardingAbsence){
//						childrenAreInconsistent = true;
//						break;
//					}
//					if(childOfParentTIIT.isAssertEitherPresenceOrAbsenceInRefNodeContext() != variableFirstChildRegardingEitherPresenceAbsence){
//						childrenAreInconsistent = true;
//						break;
//					}
//				}
//				countLoopRealTaxo++;
//			}
//		}
//	
//		
//		
//		if(childrenAreInconsistent){
//			//set node as mixed
//			if(!parentTaxoItemIT.isRefGenomeInRefNodeContext()
//					&& (
//							parentTaxoItemIT.isAssertPresenceInRefNodeContext()
//							|| parentTaxoItemIT.isAssertAbsenceInRefNodeContext()
//							|| parentTaxoItemIT.isAssertEitherPresenceOrAbsenceInRefNodeContext()
//						)
//					){
//				//System.err.println("upstream mixed "+parentTaxoItemIT.getFullName());
//				parentTaxoItemITStateChanged = true;
//				parentTaxoItemIT.setAssertPresenceInRefNodeContext(false);
//				parentTaxoItemIT.setAssertAbsenceInRefNodeContext(false);
//				parentTaxoItemIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
//			}
//		}else{
//			if(variableFirstChildRegardingPresence){
//				//children consistent for presence, set as presence if needed
//				if(!parentTaxoItemIT.isRefGenomeInRefNodeContext()
//						&& !parentTaxoItemIT.isAssertPresenceInRefNodeContext()){
//					//System.err.println("upstream presence "+parentTaxoItemIT.getFullName());
//					parentTaxoItemITStateChanged = true;
//					parentTaxoItemIT.setRefGenomeInRefNodeContext(false);
//					parentTaxoItemIT.setAssertPresenceInRefNodeContext(true);
//					parentTaxoItemIT.setAssertAbsenceInRefNodeContext(false);
//					parentTaxoItemIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
//				}
//			}else if(variableFirstChildRegardingAbsence){
//				//children consistent for absence, set as absence if needed
//				if(!parentTaxoItemIT.isRefGenomeInRefNodeContext()
//						&& !parentTaxoItemIT.isAssertAbsenceInRefNodeContext()){
//					//System.err.println("upstream absence "+parentTaxoItemIT.getFullName());
//					parentTaxoItemITStateChanged = true;
//					parentTaxoItemIT.setRefGenomeInRefNodeContext(false);
//					parentTaxoItemIT.setAssertPresenceInRefNodeContext(false);
//					parentTaxoItemIT.setAssertAbsenceInRefNodeContext(true);
//					parentTaxoItemIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(false);
//				}
//			}else if(variableFirstChildRegardingEitherPresenceAbsence){
//				//children consistent for either, set as either if needed
//				if(!parentTaxoItemIT.isRefGenomeInRefNodeContext()
//						&& !parentTaxoItemIT.isAssertEitherPresenceOrAbsenceInRefNodeContext()){
//					//System.err.println("upstream EitherPresenceAbsence "+parentTaxoItemIT.getFullName());
//					parentTaxoItemITStateChanged = true;
//					parentTaxoItemIT.setRefGenomeInRefNodeContext(false);
//					parentTaxoItemIT.setAssertPresenceInRefNodeContext(false);
//					parentTaxoItemIT.setAssertAbsenceInRefNodeContext(false);
//					parentTaxoItemIT.setAssertEitherPresenceOrAbsenceInRefNodeContext(true);
//				}
//			}else {
//				//error
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in getTaxoItemParentRecursivelyAndAssessPresenceAbsenceEitherMixedState : unrecognized variableFirstChildRegarding for "+parentTaxoItemIT.getName()+" taxid = "+parentTaxoItemIT.getNcbiTaxoId()));
//				return;
//			}
//		}
//		
//		if(parentTaxoItemIT.getParentTaxoItem() != null
//				&& parentTaxoItemITStateChanged){
//				getTaxoItemParentRecursivelyAndAssessPresenceAbsenceEitherMixedState(parentTaxoItemIT);
//		}
//		
//	}

	public ArrayList<OrganismItem> getListOrganismItemWithFullPathInHierarchie(ArrayList<Integer> fullPathInHierarchieSent){
		ArrayList<OrganismItem> alToReturn = new ArrayList<OrganismItem>();
		for(int k=0;k<getListAllPublicOrganismItem().size();k++){
			OrganismItem oi = getListAllPublicOrganismItem().get(k);
			//if(oi.getAssociatedTaxoItem().getFullPathInHierarchie().toString().replaceAll("[\\[\\]]", "").startsWith(fullPathInHierarchieSent.toString().replaceAll("[\\[\\]]", ""))){
			if(UtilitiesMethodsShared.getItemsAsStringFromCollection(oi.getAssociatedTaxoItem().getFullPathInHierarchie())
					.startsWith(
							//fullPathInHierarchieSent.toString().replaceAll("[\\[\\]]", "")
							UtilitiesMethodsShared.getItemsAsStringFromCollection(fullPathInHierarchieSent)
							)
				){
				//found right orga
				alToReturn.add(oi);
			}
		}
		//Comparator<OrganismItem> compaByGeneStartComparatorIT = new ByGeneStartLGIComparator();
		//Comparator<LightOrganismItem> compaByFullNameIT = new LightOrganismItemComparators.ByFullNameComparator();
		Collections.sort(alToReturn, LightOrganismItemComparators.byFullNameComparator);
		return alToReturn;
		
	}
	
	public ArrayList<OrganismItem> getListOrganismItemWithStringFullName(
			String selectedSpIT) {
		
		ArrayList<OrganismItem> alToReturn = new ArrayList<OrganismItem>();
		for(int k=0;k<getListAllPublicOrganismItem().size();k++){
			OrganismItem oi = getListAllPublicOrganismItem().get(k);
			if(oi.getFullName().compareTo(selectedSpIT)==0){
				//found right orga
				alToReturn.add(oi);
				break;
			}
		}
		if(alToReturn.isEmpty()){
			//maybe node ?
			for(int i=0;i<getListAllTaxoItem().size();i++){
				TaxoItem tiIT = getListAllTaxoItem().get(i);
				if(tiIT.getName().compareTo(selectedSpIT)==0){
					//found right node
					//String hierarAsStringToMatch = tiIT.getFullPathInHierarchie().toString().replaceAll("[\\[\\]]", "");
					String hierarAsStringToMatch = UtilitiesMethodsShared.getItemsAsStringFromCollection(tiIT.getFullPathInHierarchie());
					for(int k=0;k<getListAllPublicOrganismItem().size();k++){
						OrganismItem oi = getListAllPublicOrganismItem().get(k);
						//if(oi.getAssociatedTaxoItem().getFullPathInHierarchie().toString().replaceAll("[\\[\\]]", "").startsWith(hierarAsStringToMatch)){
						if(UtilitiesMethodsShared.getItemsAsStringFromCollection(oi.getAssociatedTaxoItem().getFullPathInHierarchie()).startsWith(hierarAsStringToMatch)){
							//found right orga, add it
							alToReturn.add(oi);
						}
					}
				}
			}
		}
		
		//Comparator<OrganismItem> compaByGeneStartComparatorIT = new ByGeneStartLGIComparator();
		//Comparator<LightOrganismItem> compaByFullNameIT = new LightOrganismItemComparators.ByFullNameComparator();
		Collections.sort(alToReturn, LightOrganismItemComparators.byFullNameComparator);
		
		return alToReturn;
		
	}
	
	
	public OrganismItem findOrganismItemWithOrigamiOrganismId(int orgaIdSent) {
//		for(int i=0;i<getListAllPublicOrganismItem().size();i++){
//			OrganismItem oiIT = getListAllPublicOrganismItem().get(i);
//			if(oiIT.getOrganismId() == orgaIdSent){
//				return oiIT;
//			}
//		}
//		Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in findOrganismItemWithOrigamiOrganismId : no orga found for organism id = "+orgaIdSent));
//		return null;
		ArrayList<Integer> alIntToSend = new ArrayList<>();
		alIntToSend.add(orgaIdSent);
		ArrayList<OrganismItem> returnedAlOI = findAlPublicOrganismItemWithAlOrganismId(alIntToSend);
		if(returnedAlOI.size() == 1){
			return returnedAlOI.get(0);
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
					new Exception("ERROR in findOrganismItemWithOrigamiOrganismId : returnedAlOI.size is differrent than 1 for organism id = "+orgaIdSent));
		}
		return null;
	}
	
	public ArrayList<OrganismItem> findAlPublicOrganismItemWithAlOrganismId(ArrayList<Integer> alOrgaIdSent) {
		ArrayList<OrganismItem> alToReturn = new ArrayList<>();
		NEXT_ORGA_ID : for(int orgaIdIT : alOrgaIdSent){
			for(int i=0;i<getListAllPublicOrganismItem().size();i++){
				OrganismItem oiIT = getListAllPublicOrganismItem().get(i);
				if(oiIT.getOrganismId() == orgaIdIT){
					alToReturn.add(oiIT);
					continue NEXT_ORGA_ID;
				}
			}
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in findAlPublicOrganismItemWithAlOrganismId : no orga found for organism id = "+orgaIdIT));
		}
		return alToReturn;
	}
	
//	public OrganismItem findOrganismItemWithTaxonItem(TaxoItem taxoItemSent) {
//		for(int i=0;i<getListAllPublicOrganismItem().size();i++){
//			OrganismItem oiIT = getListAllPublicOrganismItem().get(i);
//			if(oiIT.getAssociatedTaxoItem().equals(taxoItemSent)){
//				return oiIT;
//			}
//		}
//		Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in findOrganismItemWithTaxonItem : no orga found for taxoItemSent = "+taxoItemSent.getNcbiTaxoId()));
//		return null;
//	}
	

	
	public TaxoItem findPathInTaxoTreeWithTextString(String stringSent) {
		for(int i=0;i<getListAllTaxoItem().size();i++){
			TaxoItem taxoIIT = getListAllTaxoItem().get(i);
			if(stringSent.compareTo(taxoIIT.getFullName())==0){ // was getName
				if(!taxoIIT.getFullPathInHierarchie().isEmpty()){
					return taxoIIT;//.getFullPathInHierarchie()
				}
			}
		}
		return null;
	}
	
	//TODO Rename
	public TaxoItem findClosestPathInTaxoTreeWithTextString(String stringSent) {
		int currentLowerScore = Integer.MAX_VALUE;
		TaxoItem tiToBeReturned = null;
		for(int i=0;i<getListAllTaxoItem().size();i++){
			TaxoItem taxoIIT = getListAllTaxoItem().get(i);
			if(ensureTaxoNameNotAHeader(taxoIIT)){
				int levDist = getLevenshteinDistance (stringSent.toLowerCase(), taxoIIT.getName().toLowerCase());
				//GWT.log("levDist = "+levDist+ " ( / " + stringSent.length() + ") = "+ (double) ( (double) levDist / (double) stringSent.length() ) +" for "+taxoIIT.getFullName());
				if ( (double) ( (double) levDist / (double) stringSent.length() ) < 0.34 ) {
					if(levDist < currentLowerScore){
						currentLowerScore = levDist;
						tiToBeReturned = taxoIIT;//.getFullPathInHierarchie()
					}
				}
			}
		}
		return tiToBeReturned;
	}
	
	public String findClosestFullNameTaxoTreeWithTextString(String stringSent) {
		int currentLowerScore = Integer.MAX_VALUE;
		String stToBeReturned = "";
		for(int i=0;i<getListAllTaxoItem().size();i++){
			TaxoItem taxoIIT = getListAllTaxoItem().get(i);
			int levDist = getLevenshteinDistance (stringSent.toLowerCase(), taxoIIT.getName().toLowerCase());
			//System.err.println(stringSent+" vs "+taxoIIT.getName()+" : "+levDist);
			if(levDist < currentLowerScore){
				currentLowerScore = levDist;
				stToBeReturned = taxoIIT.getName();
			}
		}
		return stToBeReturned;
	}
	
	public static native int getLevenshteinDistance (String s, String t) /*-{
		var d = []; //2d matrix
	
	    // Step 1
	    var n = s.length;
	    var m = t.length;
	
	    if (n == 0) return m;
	    if (m == 0) return n;
	
	    //Create an array of arrays in javascript (a descending loop is quicker)
	    for (var i = n; i >= 0; i--) d[i] = [];
	
	    // Step 2
	    for (var i = n; i >= 0; i--) d[i][0] = i;
	    for (var j = m; j >= 0; j--) d[0][j] = j;
	
	    // Step 3
	    for (var i = 1; i <= n; i++) {
	        var s_i = s.charAt(i - 1);
	        // Step 4
	        for (var j = 1; j <= m; j++) {
	
	            //Check the jagged ld total so far
	            if (i == j && d[i][j] > 4) return n;
	
	            var t_j = t.charAt(j - 1);
	            var cost = (s_i == t_j) ? 0 : 1; // Step 5
	
	            //Calculate the minimum
	            var mi = d[i - 1][j] + 1;
	            var b = d[i][j - 1] + 1;
	            var c = d[i - 1][j - 1] + cost;
	
	            if (b < mi) mi = b;
	            if (c < mi) mi = c;
	
	            d[i][j] = mi; // Step 6
	
	            //Damerau transposition
	            if (i > 1 && j > 1 && s_i == t.charAt(j - 2) && s.charAt(i - 2) == t_j) {
	                d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);
	            }
	        }
	    }
	    // Step 7
	    return d[n][m];
	    
	}-*/;
	
	
	public void mapOrganismsToTaxoTree(){
		
		HashMap<Integer, ArrayList<OrganismItem>> ncbiTaxoIdITLessOrEgalZero = new HashMap<Integer, ArrayList<OrganismItem>>();
		
		//constructNcbiTaxonIdWithMultipleOrgaIds();
		for(int i=0;i<getListAllPublicOrganismItem().size();i++){
			OrganismItem orgaIT = getListAllPublicOrganismItem().get(i);
			int ncbiTaxoIdIT = orgaIT.getTaxonId();
			
			if (ncbiTaxoIdIT > 0) {
				if (ncbiTaxonId2ListOrga.containsKey(ncbiTaxoIdIT)){
					ArrayList<OrganismItem> currListOrgaIds = ncbiTaxonId2ListOrga.get(ncbiTaxoIdIT);
					currListOrgaIds.add(orgaIT);
				} else {
					ArrayList<OrganismItem> newListOrgaIds = new ArrayList<OrganismItem>();
					newListOrgaIds.add(orgaIT);
					ncbiTaxonId2ListOrga.put(ncbiTaxoIdIT, newListOrgaIds);
				}
			} else {
				//unknown lineage
				if (ncbiTaxoIdITLessOrEgalZero.containsKey(ncbiTaxoIdIT)){
					ArrayList<OrganismItem> currListOrgaIds = ncbiTaxoIdITLessOrEgalZero.get(ncbiTaxoIdIT);
					currListOrgaIds.add(orgaIT);
				} else {
					ArrayList<OrganismItem> newListOrgaIds = new ArrayList<OrganismItem>();
					newListOrgaIds.add(orgaIT);
					ncbiTaxoIdITLessOrEgalZero.put(ncbiTaxoIdIT, newListOrgaIds);
				}
			}

			
		}
		//ncbiTaxonId2CountOrgaIdsGreaterThanTwo.values().removeAll(Collections.singleton(1));
		
		// feed buildTaxoArrayItemRecursivelyAndAddToOracle with root taxoItem
		buildTaxoArrayItemRecursivelyAndAddToOracle(getTaxoTree());
		
		//add all ncbiTaxoIdIT <= 0
		ncbiTaxonId2ListOrga.putAll(ncbiTaxoIdITLessOrEgalZero);
		
		//deal with unknown lineage aka what is left in ncbiTaxonId2ListOrga = unmapped taxon_id <-> organisms
		if( ! ncbiTaxonId2ListOrga.isEmpty()){
			TaxoItem unknownLineage = new TaxoItem("unknown lineage", 0, Insyght.APP_CONTROLER.getTaxoTree());
			getTaxoTree().getTaxoChildrens().add(unknownLineage);
			for (Map.Entry<Integer, ArrayList<OrganismItem>> entry : ncbiTaxonId2ListOrga.entrySet())
			{
			    //System.out.println(entry.getKey() + "/" + entry.getValue());
				for(OrganismItem oiIT : ncbiTaxonId2ListOrga.get(entry.getKey())){

					//create the new leaf to the tree
					TaxoItem orgaUnknownToAdd = new TaxoItem(oiIT.getFullName(), oiIT.getTaxonId(), unknownLineage);
					
					ArrayList<Integer> newALInt = new ArrayList<Integer>();
					newALInt.add(1); // Unknown lineage, other are  0.XXXX
					newALInt.add(unknownLineage.getTaxoChildrens().size());
					orgaUnknownToAdd.setFullPathInHierarchie(newALInt);

					
					//add the new leaf to the tree
					unknownLineage.getTaxoChildrens().add(orgaUnknownToAdd);
					
					//make bidirectional association
					oiIT.setAssociatedTaxoItem(orgaUnknownToAdd);
					orgaUnknownToAdd.setAssociatedOrganismItem(oiIT);
					
					//add to getListAllTaxoItem
					getListAllTaxoItem().add(orgaUnknownToAdd);
					
					//add to oracle
					SearchViewImpl.oracle.add(oiIT.getFullName());
					
				}
			}
		}
		ncbiTaxoIdITLessOrEgalZero.clear();
		ncbiTaxonId2ListOrga.clear();
	}
	

	public void buildTaxoArrayItemRecursivelyAndAddToOracle(TaxoItem taxoIITSent){
		
		if(ncbiTaxonId2ListOrga.containsKey(taxoIITSent.getNcbiTaxoId())){
			//leaf, at least 1 orga at this taxon_id
			if (ncbiTaxonId2ListOrga.get(taxoIITSent.getNcbiTaxoId()).size() > 1){
				// multiple orga for this taxo Item, branch
				
				//add taxoIITSent to getListAllTaxoItem
				getListAllTaxoItem().add(taxoIITSent);
				//add to oracle
				SearchViewImpl.oracle.add(taxoIITSent.getName());		
				
				// keep going down the branches, not after adding the leaf below
				if( ! taxoIITSent.getTaxoChildrens().isEmpty()){
					for(int i=0;i<taxoIITSent.getTaxoChildrens().size();i++){
						TaxoItem childTaxoIIT = taxoIITSent.getTaxoChildrens().get(i);
						buildTaxoArrayItemRecursivelyAndAddToOracle(childTaxoIIT);
					}
				}
				
				for(OrganismItem oiIT : ncbiTaxonId2ListOrga.get(taxoIITSent.getNcbiTaxoId())){
					TaxoItem newLeaf = 	new TaxoItem(taxoIITSent.getName(), taxoIITSent.getNcbiTaxoId(), taxoIITSent);
					ArrayList<Integer> newALInt = new ArrayList<Integer>();
					newALInt.addAll(taxoIITSent.getFullPathInHierarchie());
					newALInt.add(taxoIITSent.getTaxoChildrens().size());
					newLeaf.setFullPathInHierarchie(newALInt);

					//add the new leaf to the tree
					taxoIITSent.getTaxoChildrens().add(newLeaf);

					//make bidirectional association
					oiIT.setAssociatedTaxoItem(newLeaf);
					newLeaf.setAssociatedOrganismItem(oiIT);

					//add newLeaf to getListAllTaxoItem
					getListAllTaxoItem().add(newLeaf);
					
					//add to oracle
					SearchViewImpl.oracle.add(oiIT.getFullName());
					//GWT.log(oiIT.getFullName());
					
				}

				//clear from ncbiTaxonId2ListOrga to see unmapped organisms in the end
				ncbiTaxonId2ListOrga.remove(taxoIITSent.getNcbiTaxoId());
				
			} else if (ncbiTaxonId2ListOrga.get(taxoIITSent.getNcbiTaxoId()).size() == 1) {
				//just one orga at that taxo Item
				if ( ! taxoIITSent.getTaxoChildrens().isEmpty()){
					// this is not a leaf taxoItem, branch just in case

					// keep going down the branches first, not after adding the leaf below
					for(int i=0;i<taxoIITSent.getTaxoChildrens().size();i++){
						TaxoItem childTaxoIIT = taxoIITSent.getTaxoChildrens().get(i);
						buildTaxoArrayItemRecursivelyAndAddToOracle(childTaxoIIT);
					}
					
					//create the new leaf to the tree
					TaxoItem newLeaf = 	new TaxoItem(taxoIITSent.getName(), taxoIITSent.getNcbiTaxoId(), taxoIITSent);
					ArrayList<Integer> newALInt = new ArrayList<Integer>();
					newALInt.addAll(taxoIITSent.getFullPathInHierarchie());
					newALInt.add(taxoIITSent.getTaxoChildrens().size());
					newLeaf.setFullPathInHierarchie(newALInt);

					//add the new leaf to the tree
					taxoIITSent.getTaxoChildrens().add(newLeaf);
					
					//make bidirectional association
					OrganismItem oiIT = ncbiTaxonId2ListOrga.get(taxoIITSent.getNcbiTaxoId()).get(0);
					oiIT.setAssociatedTaxoItem(newLeaf);
					newLeaf.setAssociatedOrganismItem(oiIT);
					
					//add both newLeaf and taxoIITSent to getListAllTaxoItem
					getListAllTaxoItem().add(newLeaf);
					getListAllTaxoItem().add(taxoIITSent);
					
					//add to oracle
					SearchViewImpl.oracle.add(taxoIITSent.getName());
					SearchViewImpl.oracle.add(oiIT.getFullName());

					//clear from ncbiTaxonId2ListOrga to see unmapped organisms in the end
					ncbiTaxonId2ListOrga.remove(taxoIITSent.getNcbiTaxoId());
					
				} else {
					//this is a leaf taxo item, make bidirectional association and add it to getListAllTaxoItem
					OrganismItem oiIT = ncbiTaxonId2ListOrga.get(taxoIITSent.getNcbiTaxoId()).get(0);
					oiIT.setAssociatedTaxoItem(taxoIITSent);
					taxoIITSent.setAssociatedOrganismItem(oiIT);
					//add to getListAllTaxoItem
					getListAllTaxoItem().add(taxoIITSent);
					//add to oracle
					SearchViewImpl.oracle.add(oiIT.getFullName());
					//GWT.log(oiIT.getFullName());
					
					//clear from ncbiTaxonId2ListOrga to see unmapped organisms in the end
					ncbiTaxonId2ListOrga.remove(taxoIITSent.getNcbiTaxoId());
				}
			} else {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in buildTaxoArrayItemRecursivelyAndAddToOracle : ncbiTaxonId2ListOrga.get(taxoIITSent.getNcbiTaxoId()).size() = "+ncbiTaxonId2ListOrga.get(taxoIITSent.getNcbiTaxoId()).size()+" for taxoIITSent.getNcbiTaxoId() = "+taxoIITSent.getNcbiTaxoId()));
			}
			
			
		} else {
			//not leaf
			//add to getListAllTaxoItem
			getListAllTaxoItem().add(taxoIITSent);
			//add to oracle
			if(ensureTaxoNameNotAHeader(taxoIITSent)){
				SearchViewImpl.oracle.add(taxoIITSent.getName());
			}
			// keep going down the branches
			if(taxoIITSent.getTaxoChildrens().isEmpty()){
				if (ensureTaxoNameNotAHeader(taxoIITSent)){
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("ERROR in buildTaxoArrayItemRecursivelyAndAddToOracle : taxoIITSent.getTaxoChildrens().isEmpty() for taxoIITSent = "+taxoIITSent.getName()+" id = "+taxoIITSent.getNcbiTaxoId()));
				} else {
					//ok do nothing
				}
			}else{
				for(int i=0;i<taxoIITSent.getTaxoChildrens().size();i++){
					TaxoItem childTaxoIIT = taxoIITSent.getTaxoChildrens().get(i);
					buildTaxoArrayItemRecursivelyAndAddToOracle(childTaxoIIT);
				}
			}
			
		}
		
		
//		if(!taxoIITSent.getTaxoChildrens().isEmpty()){
//			//not leaf
//			getListAllTaxoItem().add(taxoIITSent);
//			
//			if(ensureTaxoNameNotAHeader(taxoIITSent)){
//				SearchViewImpl.oracle.add(taxoIITSent.getName());
//			}
//			
//			
//			for(int i=0;i<taxoIITSent.getTaxoChildrens().size();i++){
//				TaxoItem childTaxoIIT = taxoIITSent.getTaxoChildrens().get(i);
//				buildTaxoArrayItemRecursivelyAndAddToOracle(childTaxoIIT);
//			}
//			
//		
//		}else{
//			//pre-leaf, do nothing in oracle and rename it after
//			for(OrganismItem oiIT : getListAllPublicOrganismItem()){
//				//if(taxoIITSent.getNcbiTaxoId() == oiIT.getTaxonId()){
//				if(oiIT.getAssociatedTaxoItem().equals(taxoIITSent)){
//					//good one, rename taxoIITSent
//					taxoIITSent.setName(oiIT.getFullName());
////					if (!oiIT.getSubstrain().isEmpty()) {
////						taxoIITSent.setSubStrain(oiIT.getSubstrain());
////					}
//					break;
//				}
//			}
//			getListAllTaxoItem().add(taxoIITSent);
//		}
		
		
	}
	
	//old not used
//	public ArrayList<Integer> getOrInitPathInTaxoTreeWithOrganismItem(OrganismItem oiSent) {
//		for(int i=0;i<getListAllTaxoItem().size();i++){
//			TaxoItem taxoIIT = listAllTaxoItem.get(i);
//			if(oiSent.getTaxonId() == taxoIIT.getNcbiTaxoId()){
//				if(!taxoIIT.getFullPathInHierarchie().isEmpty()){
//					boolean mayBeBranchingForThisTaxoId = false;
//					if ( ! taxoIIT.getTaxoChildrens().isEmpty()){
//						mayBeBranchingForThisTaxoId = true;
//					}
//					if(ncbiTaxonId2CountOrgaIdsGreaterThanTwo.containsKey(oiSent.getTaxonId())){
//						mayBeBranchingForThisTaxoId = true;
//					}
//					
//					//if(oiSent.getSubstrain() != null){
//					if(mayBeBranchingForThisTaxoId){
//						//if(!oiSent.getSubstrain().isEmpty()){
//							TaxoItem newLeaf = 	new TaxoItem(taxoIIT.getName(), taxoIIT.getNcbiTaxoId(), taxoIIT);
//							newLeaf.setListAllLightElementItemAsStringTypeAccnum(oiSent.getListAllLightElementItemAsStringTypeAccnum().toString());
//							if(!oiSent.getSubstrain().isEmpty()){
//								newLeaf.setSubStrain(oiSent.getSubstrain());
//							}
//							newLeaf.setSelectableAsOrganism(true);
//							taxoIIT.getTaxoChildrens().add(newLeaf);
//							getListAllTaxoItem().add(newLeaf);
//							ArrayList<Integer> newALInt = new ArrayList<Integer>();
//							newALInt.addAll(taxoIIT.getFullPathInHierarchie());
//							newALInt.add(taxoIIT.getTaxoChildrens().size()-1);
//							newLeaf.setFullPathInHierarchie(newALInt);
//							return newLeaf.getFullPathInHierarchie();
////						}else{
////							taxoIIT.setSelectableAsOrganism(true);
////							return taxoIIT.getFullPathInHierarchie();
////						}
//					}else{
//						taxoIIT.setListAllLightElementItemAsStringTypeAccnum(oiSent.getListAllLightElementItemAsStringTypeAccnum().toString());
//						taxoIIT.setSelectableAsOrganism(true);
//						return taxoIIT.getFullPathInHierarchie();
//					}
//				}else{
//					//Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, "Error in findPathInTaxoTreeWithTaxonIdAndCreateSubstrainTaxoItemIfNeededAndSetSelectable : taxoIIT.getFullPathInHierarchie().isEmpty() for TaxonIdSent = "+oiSent.getTaxonId());
//					//System.err.println("Error in findPathInTaxoTreeWithTaxonIdAndCreateSubstrainTaxoItemIfNeededAndSetSelectable : taxoIIT.getFullPathInHierarchie().isEmpty() for TaxonIdSent = "+oiSent.getTaxonId());
//				}
//			}
//		}
//		return null;
//	}

	
	public boolean ensureTaxoNameNotAHeader(TaxoItem taxoIITSent) {
		if(taxoIITSent.getName().compareTo("RootOfRoot")!=0
				&& taxoIITSent.getName().compareTo("Domain")!=0
				&& taxoIITSent.getName().compareTo("Phylum")!=0
				&& taxoIITSent.getName().compareTo("Class")!=0
				&& taxoIITSent.getName().compareTo("Order")!=0
				&& taxoIITSent.getName().compareTo("Family")!=0
				&& taxoIITSent.getName().compareTo("Genus")!=0
				&& taxoIITSent.getName().compareTo("Species")!=0){
			return true;
		}
		return false;
	}

	
	

//TODO DEL
	public OrganismItem getDefaultOrganismIfNoSelection() {
		return defaultOrganismIfNoSelection;
	}
	
	
	public void setDefaultOrganismIfNoSelection(
			OrganismItem defaultOrganismIfNoSelection) {
		this.defaultOrganismIfNoSelection = defaultOrganismIfNoSelection;
	}
	
	
	public void setLookUpForGeneForAllResults(int qOrigamiElementId,
			int qPbStartQGeneInElement, int qPbStopQGeneInElement,
			EnumResultViewTypes enumResultViewTypesSent) {
		
//		System.err.println("hero "+qOrigamiElementId+" ; "+qPbStartQGeneInElement
//				+" ; "+qPbStopQGeneInElement+" ; "+enumResultViewTypesSent.toString());
		
		clearAllUserSelection();
		if (getCurrentListPublicResultGenomePanelItem() != null) {
			for (int i = 0; i < getCurrentListPublicResultGenomePanelItem()
					.size(); i++) {
				GenomePanelItem gPIIT = getCurrentListPublicResultGenomePanelItem()
						.get(i);
				
				gPIIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1, enumResultViewTypesSent);
				// gPIIT.setLookUpForSymbolicSliceAtQPosition(qPbStartQGeneInElement);
				// gPIIT.setLookUpForSymbolicSliceAtQElementId(qOrigamiElementId);
				gPIIT.setLookUpForGeneWithQStart(qPbStartQGeneInElement, enumResultViewTypesSent);
				gPIIT.setLookUpForGeneWithQStop(qPbStopQGeneInElement, enumResultViewTypesSent);
				gPIIT.setLookUpForGeneOnQElementId(qOrigamiElementId, enumResultViewTypesSent);
			}
		}
		if (getCurrentListPrivateResultGenomePanelItem() != null) {
			for (int i = 0; i < getCurrentListPrivateResultGenomePanelItem()
					.size(); i++) {
				GenomePanelItem gPIIT = getCurrentListPrivateResultGenomePanelItem()
						.get(i);
				gPIIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1, enumResultViewTypesSent);
				// gPIIT.setLookUpForSymbolicSliceAtQPosition(qPbStartQGeneInElement);
				// gPIIT.setLookUpForSymbolicSliceAtQElementId(qOrigamiElementId);
				gPIIT.setLookUpForGeneWithQStart(qPbStartQGeneInElement, enumResultViewTypesSent);
				gPIIT.setLookUpForGeneWithQStop(qPbStopQGeneInElement, enumResultViewTypesSent);
				gPIIT.setLookUpForGeneOnQElementId(qOrigamiElementId, enumResultViewTypesSent);
			}
		}
		if (getCurrentListFeaturedResultGenomePanelItem() != null) {
			for (int i = 0; i < getCurrentListFeaturedResultGenomePanelItem()
					.size(); i++) {
				GenomePanelItem gPIIT = getCurrentListFeaturedResultGenomePanelItem()
						.get(i);
				gPIIT.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(-1, enumResultViewTypesSent);
				// gPIIT.setLookUpForSymbolicSliceAtQPosition(qPbStartQGeneInElement);
				// gPIIT.setLookUpForSymbolicSliceAtQElementId(qOrigamiElementId);
				gPIIT.setLookUpForGeneWithQStart(qPbStartQGeneInElement, enumResultViewTypesSent);
				gPIIT.setLookUpForGeneWithQStop(qPbStopQGeneInElement, enumResultViewTypesSent);
				gPIIT.setLookUpForGeneOnQElementId(qOrigamiElementId, enumResultViewTypesSent);
			}
		}
		getCurrentRefGenomePanelAndListOrgaResult()
				.getReferenceGenomePanelItem()
				.setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
						-1, enumResultViewTypesSent);
		
		getCurrentRefGenomePanelAndListOrgaResult()
				.getReferenceGenomePanelItem()
				.setLookUpForGeneWithQStart(
						qPbStartQGeneInElement, enumResultViewTypesSent);
		getCurrentRefGenomePanelAndListOrgaResult()
				.getReferenceGenomePanelItem()
				.setLookUpForGeneWithQStop(
						qPbStopQGeneInElement, enumResultViewTypesSent);
		getCurrentRefGenomePanelAndListOrgaResult()
				.getReferenceGenomePanelItem()
				.setLookUpForGeneOnQElementId(qOrigamiElementId, enumResultViewTypesSent);
		
		
	}


	public static boolean getDATABASE_IS_genes_elements_genome_assembly_convenience() {
		if (DATABASE_IS_genes_elements_genome_assembly_convenience == 1) {
			return true;
		} else if (DATABASE_IS_genes_elements_genome_assembly_convenience == -1) {
			return false;
		} else if (DATABASE_IS_genes_elements_genome_assembly_convenience == 0) {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
					new Exception("Error getDATABASE_IS_genes_elements_genome_assembly_convenience : DATABASE_IS_genes_elements_genome_assembly_convenience is 0"));
			return false;
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
					new Exception("Error getDATABASE_IS_genes_elements_genome_assembly_convenience : DATABASE_IS_genes_elements_genome_assembly_convenience is neither 0, 1 or -1"));
			return false;
		}
	}
	
	public static void setDATABASE_IS_genes_elements_genome_assembly_convenience(int DATABASE_IS_genes_elements_genome_assembly_convenienceSent) {
		DATABASE_IS_genes_elements_genome_assembly_convenience = DATABASE_IS_genes_elements_genome_assembly_convenienceSent;
		getDATABASE_IS_genes_elements_genome_assembly_convenience();//check for errors
	}
	
	
	public static boolean getDATABASE_IS_NO_MIRROR() {
		if (DATABASE_IS_NO_MIRROR == 1) {
			return true;
		} else if (DATABASE_IS_NO_MIRROR == -1) {
			return false;
		} else if (DATABASE_IS_NO_MIRROR == 0) {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
					new Exception("Error getDATABASE_IS_NO_MIRROR : DATABASE_IS_NO_MIRROR is 0"));
			return false;
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
					new Exception("Error getDATABASE_IS_NO_MIRROR : DATABASE_IS_NO_MIRROR is neither 0, 1 or -1"));
			return false;
		}
	}
	
	public static void setDATABASE_IS_NO_MIRROR(int DATABASE_IS_NO_MIRRORSent) {
		DATABASE_IS_NO_MIRROR = DATABASE_IS_NO_MIRRORSent;
		getDATABASE_IS_NO_MIRROR();//check for errors
	}

	public static void backFromCallBackCheckingOfLoadingMask() {
		NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE--;
		if (NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE < 0) {
			NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE = 0;
		}
		if (AppController.NUMBER_OF_CALLBACK_WAITING_ON_THIS_PAGE == 0) {
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {
				// @Override
				public void execute() {
					Insyght.INSYGHT_LOADING_MASK.hide();
				}
			});
		}
	}
	
}
