package fr.inra.jouy.client.RPC;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import fr.inra.jouy.shared.TransAPMissGeneHomoInfo;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;
import fr.inra.jouy.shared.pojos.transitComposite.AlItemsOfIntPrimaryIdAttributePlusScore;

@RemoteServiceRelativePath("callForInfoDB")
public interface CallForInfoDB extends RemoteService {

	//ArrayList<OrganismItem> getAllPublicOrganismItem() throws Exception;
	//OLD_AlOrganismItemAndTaxoItem getAllPublicOrganismItemAndTaxoTree() throws Exception;
	ArrayList<LightOrganismItem> loadAllPublicLightOrganisms() throws Exception;
	ArrayList<LightElementItem> loadAllPublicMoleculesOfOrganisms() throws Exception;
	
	ArrayList<String> getDetailledGeneInfoAsListStringForHTMLWithGeneId(
			Integer geneId) throws Exception;

	ArrayList<String> getDetailledGeneInfoAsListStringForHTMLWithSingletonAlignmentId(
			Long alignmentId, boolean referenceGene) throws Exception;
	
	//String getSyntenyInfoAsStringForHTMLWithQGeneIdAndSGeneId(int qGeneId, int sGeneId) throws Exception;

	ArrayList<OrganismItem> getAllPublicOrganismItemWithFilterNameOrId(
			String searchString, boolean wrapAround, boolean caseInsensitive) throws Exception;

	ArrayList<LightOrganismItem> getAllPublicLightOrganismItemWithFilterNameOrId(
			String searchString, boolean wrapAround, boolean caseInsensitive) throws Exception;

	String getBasicElementInfoAsStringWithGeneId(int origamiGeneId) throws Exception;
	
	String getDetailledElementInfoAsStringWithGeneId(int origamiGeneId) throws Exception;
	
	String getDetailledElementInfoAsStringWithOrigamiElementId(int origamiId) throws Exception;

	TransAPMissGeneHomoInfo getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem(
			TransAPMissGeneHomoInfo tapmghiSent, boolean getMatchInfo) throws Exception;

	String getDetailledSyntenyInfoAsStringForHTMLWithAlignmentId(
			long origamiAlignmentId) throws Exception;

	ArrayList<LightGeneItem> getAllLightGeneItemWithListFiltersAndAlElementIds(
			ArrayList<FilterGeneItem> listFilters,
			//int sizeSelectedElement,
			//int selectedElementId
			//ArrayList<Integer> alRefOrganismIds,
			ArrayList<Integer> alRefElementIds
			) throws Exception;


	ArrayList<LightGeneItem> getAllLightGeneItemWithListGeneIds_sortedByGeneId(
			ArrayList<Integer> listGenesIds) throws Exception;

	LightGeneItem getLightGeneItemWithGeneId(int geneId) throws Exception;

	ArrayList<LightGeneItem> getAllLightGeneItemWithElementId(int elementId) throws Exception;

	ArrayList<LightGeneItem> getListQLightGeneItemWithOrigamiSyntenyAlignmentId(Long mainOrigamiSyntenyId) throws Exception;

	ArrayList<LightGeneItem> getListLightGeneItemWithWithAlElementIdAndStartPbAndStopPbLooped(
			ArrayList<Integer> alElementIdTenStartThenStopLooped
			, boolean grabCDSWithAtLeastOnePbInThisStartStopLocus
			) throws Exception;

	ArrayList<Integer> getGeneSetIdsWithOrigamiSyntenyId(
			Long mainOrigamiSyntenyId
			, HashSet<Integer> hsTypeToRetrieve
			) throws Exception;

	ArrayList<Integer> getGeneSetIdsWithListElementIdsThenStartPbthenStopPBLooped(
			ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent
			, boolean grabCDSWithAtLeastOnePbInThisStartStopLocus
			) throws Exception;

	ArrayList<String> getIdentifiersForVisuAlignmentWithQAndSGeneId(
			int qGeneIdForVisuAlignment, int sGeneIdForVisuAlignment) throws Exception;

	AlItemsOfIntPrimaryIdAttributePlusScore getReferenceGenesAndOrthologsOverRepresentatedInPhenotype(
			Integer referenceOrgaId,
			ArrayList<Integer> alSelectedPhenoPlusOrgaIds,
			ArrayList<Integer> alSelectedPhenoMinusOrgaIds
			//ArrayList<Integer> alSelectedEitherPresenceOrAbsenceOrgaIds
			) throws Exception;

	AlItemsOfIntPrimaryIdAttributePlusScore getReferenceGenesMostRepresentedInAlOrganisms(Integer referenceOrgaId,
			ArrayList<Integer> alSelectedPhenoPlusOrgaIds) throws Exception;


	int getDatabaseIsGenesElementsGenomeAssemblyConvenience() throws Exception;

	int getDatabaseIsNoMirror() throws Exception;
	
	String getStatSummaryRefCDS(
			HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneName
			, HashSet<Integer> hsOrgaIdsFeaturedGenomes
			//, HashSet<Integer> hsOrgaIdsMainList
			) throws Exception;




}
