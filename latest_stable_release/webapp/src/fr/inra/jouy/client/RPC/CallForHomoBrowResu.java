package fr.inra.jouy.client.RPC;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.ArrayList;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.inra.jouy.shared.RefGeneSetForAnnotCompa;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotCategoryCompa;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotComparedGene;
import fr.inra.jouy.shared.pojos.annotationsComparator.CompaAnnotRoot;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem;
//import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.EnumResultListSortScopeType;
import fr.inra.jouy.shared.pojos.databaseMapping.AlignmentParametersForSynteny;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.TransAlignmPairs;
import fr.inra.jouy.shared.TransAbsoPropResuGeneSetHold;
import fr.inra.jouy.shared.TransAbsoPropResuHold;
import fr.inra.jouy.shared.TransStartStopGeneInfo;

@RemoteServiceRelativePath("callForHomoBrowResu")
public interface CallForHomoBrowResu extends RemoteService {

	CompaAnnotRoot getCompaAnnotRootWithAlGeneId(
			RefGeneSetForAnnotCompa referenceGeneSetForAnnotationsComparatorSent)
			throws Exception;

	ArrayList<CompaAnnotCategoryCompa> getCompaAnnotRefGenesWithGeneIdAndParams(
			Integer refGeneId, Double paramMaxEvalue,
			int paramMinPercentIdentity, int paramMinPercentAlignLenght,
			ArrayList<Integer> listComparedOrgaIdsSent,
			boolean paramBdbhOnlySent) throws Exception;

	ArrayList<CompaAnnotComparedGene> getAllCompaAnnotComparedGeneWithRefGeneIdAndListGeneIds(
			int currClickedRefGeneId, ArrayList<Integer> listGenesIds)
			throws Exception;

	String exportToEmailCompaAnnotRefGenesWithListGeneIdAndParams(
			String emailAddress,
			RefGeneSetForAnnotCompa referenceGeneSetForAnnotationsComparatorSent,
			Double paramMaxEvalueSent, int paramMinPercentIdentitySent,
			int paramMinPercentAlignLenghtSent,
			ArrayList<Integer> alOrgaIdsSent, boolean paramBdbhOnlySent)
			throws Exception;

	String exportToEmailHomoBroTable(
			String emailAddress
			, ArrayList<Integer> listReferenceGeneSetForHomologsTable
			, int referenceOrgaId
			, HashSet<Integer> hsFeaturedOrganismIds
			, HashSet<Integer> hsMainListOrganismIds
			//, EnumResultListSortScopeType sortScopeType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			, boolean summaryConservationData
			, String fieldDelimiter
			)
			throws Exception;
	
	RefGenoPanelAndListOrgaResu getRefGenomePanelAndListOrgaResult(
			SearchItem siSent, String sessionIdToSend)
					throws Exception;

	ArrayList<LightOrganismItem> getListPublicOrgaResultScopeGeneWithRefOrgaIdAndSlectedGeneAndParamForSynteny(
			int organismId
			, int geneIdIT
			, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<LightOrganismItem> listUserSelectedOrgaToBeResults
			, ArrayList<LightOrganismItem> listExcludedGenome
			//, EnumResultListSortScopeType scopeType
			//, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			)
					throws Exception;

	ArrayList<LightOrganismItem> getListPublicOrgaResultScopeGeneSetWithRefOrgaIdAndListGeneSetAndParamForSynteny(
			int organismId
			, ArrayList<Integer> listReferenceGeneSetForHomologsTable
			, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<LightOrganismItem> listUserSelectedOrgaToBeResults
			, ArrayList<LightOrganismItem> listExcludedGenome
			//, EnumResultListSortScopeType scopeType
			//, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			)
					throws Exception;

	ArrayList<LightOrganismItem> getListPublicOrgaResultScopeWholeOrganismWithRefOrgaIdAndParamForSynteny(
			int organismId
			, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<LightOrganismItem> listUserSelectedOrgaToBeResults
			, ArrayList<LightOrganismItem> listExcludedGenome
			//, EnumResultListSortScopeType scopeType
			//, SearchItem.Enum_comparedGenomes_SortResultListBy_scope sortScopeType_scope
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortType sortScopeType_sortType
			, SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder sortScopeType_sortOrder
			)
					throws Exception;

	TransAbsoPropResuGeneSetHold getTransientAbsoluteProportionType1Or2ResultGeneSetHolderWithOrgaIdAndSOrgaIdAndALQGeneIds(
			//ArrayList<AbsoPropQElemItem> listAbsoluteProportionElementItemQ
			int qOrigamiOrgaIdSent
			, int sOrigamiOrgaIdSent
			//, AlignmentParametersForSynteny alignmentParametersForSynteny
			, ArrayList<Integer> listReferenceGeneSet
			, boolean listAbsoluteProportionElementItemSIsEmpty
			)
					throws Exception;

	TransAbsoPropResuHold getItemAbsoluteProportionElementSAndSyntenyAndQGenomicRegionInsertionWithQArrayListOrigamiElementIdsAndSOrigamiOrgaId(
			//ArrayList<AbsoPropQElemItem> arrayListAbsoProQelementItem,
			int qOrigamiOrgaIdSent
			, int sOrigamiOrgaIdSent
			//, AlignmentParametersForSynteny alignmentParametersForSynteny
			, boolean listAbsoluteProportionElementItemSIsEmpty
			) throws Exception;

	ArrayList<TransStartStopGeneInfo> getListTransientStartStopGeneInfoWithOrigamiElementIdAndStartPbAndStopPb(
			int origamiElementId, int startPb, int stopPb)
					throws Exception;

	ArrayList<TransAlignmPairs> getListTransientAlignmentPairsWithMainSyntenyAndRelated(
			long mainOrigamiSyntenyId,
			ArrayList<Long> listOrigamiAlignmentIdsContainedOtherSyntenies,
			ArrayList<Long> listOrigamiAlignmentIdsMotherSyntenies,
			ArrayList<Long> listOrigamiAlignmentIdsSprungOffSyntenies,
			boolean isContainedWithinAnotherMotherSynteny,
			boolean isMotherOfContainedOtherSyntenies,
			boolean isMotherOfSprungOffSyntenies,
			boolean isSprungOffAnotherMotherSynteny)
					throws Exception;

	ArrayList<TransStartStopGeneInfo> getListTransientStartStopGeneInfoWithListGeneIds(
			ArrayList<Integer> listSGeneIds)
					throws Exception;

	int getNumberGenesWithOrigamiElementId(int origamiElementIdSent)
			throws Exception;

	int getNumberGenesBetweenGenomicRegionWithOrigamiElementIDAndStartPbAndStopPb(
			int origamiElementIdSent, int pbStartInElement, int pbStopInElement)
					throws Exception;


}
