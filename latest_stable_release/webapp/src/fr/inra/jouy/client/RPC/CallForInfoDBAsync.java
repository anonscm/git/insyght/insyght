package fr.inra.jouy.client.RPC;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.user.client.rpc.AsyncCallback;
import fr.inra.jouy.shared.TransAPMissGeneHomoInfo;
import fr.inra.jouy.shared.pojos.applicationItems.FilterGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;
import fr.inra.jouy.shared.pojos.transitComposite.AlItemsOfIntPrimaryIdAttributePlusScore;

public interface CallForInfoDBAsync {

	//void getAllPublicOrganismItem(AsyncCallback<ArrayList<OrganismItem>> callback) throws Exception;
	//void getAllPublicOrganismItemAndTaxoTree(AsyncCallback<OLD_AlOrganismItemAndTaxoItem> callback) throws Exception;
	void loadAllPublicLightOrganisms(AsyncCallback<ArrayList<LightOrganismItem>> callback) throws Exception;
	void loadAllPublicMoleculesOfOrganisms(AsyncCallback<ArrayList<LightElementItem>> callback) throws Exception;
	
	
	void getDetailledGeneInfoAsListStringForHTMLWithGeneId(Integer geneId,
			AsyncCallback<ArrayList<String>> callback) throws Exception;

	void getDetailledGeneInfoAsListStringForHTMLWithSingletonAlignmentId(
			Long alignmentId, boolean referenceGene,
			AsyncCallback<ArrayList<String>> callback) throws Exception;
	
	//void getSyntenyInfoAsStringForHTMLWithQGeneIdAndSGeneId(int qGeneId, int sGeneId, AsyncCallback<String> callback) throws Exception;
	
	void getAllPublicOrganismItemWithFilterNameOrId(String searchString, boolean wrapAround, boolean caseInsensitive,
			AsyncCallback<ArrayList<OrganismItem>> callback) throws Exception;

	void getAllPublicLightOrganismItemWithFilterNameOrId(String searchString,
			boolean wrapAround, boolean caseInsensitive,
			AsyncCallback<ArrayList<LightOrganismItem>> callback) throws Exception;
	
	void getBasicElementInfoAsStringWithGeneId(int origamiGeneId, AsyncCallback<String> callback) throws Exception;
	
	void getDetailledElementInfoAsStringWithGeneId(int origamiGeneId, AsyncCallback<String> callback) throws Exception;
	
	void getDetailledElementInfoAsStringWithOrigamiElementId(int origamiId,
			AsyncCallback<String> callback) throws Exception;

	void getFullGeneHomologyInfoWithPartialAPQCQSGeneHomologyItem(
			TransAPMissGeneHomoInfo tapmghiSent, boolean getMatchInfo, 
			AsyncCallback<TransAPMissGeneHomoInfo> callback) throws Exception;

	void getDetailledSyntenyInfoAsStringForHTMLWithAlignmentId(
			long origamiAlignmentId, AsyncCallback<String> callback) throws Exception;

	void getAllLightGeneItemWithListFiltersAndAlElementIds(
			ArrayList<FilterGeneItem> listFilters,
			//int sizeSelectedElement,
			//int selectedElementId,
			//ArrayList<Integer> alRefOrganismIds,
			ArrayList<Integer> alRefElementIds,
			AsyncCallback<ArrayList<LightGeneItem>> callback) throws Exception;

//	void getAllLightGeneItemWithOrgaIdAndListGeneName(int organismId,
//			ArrayList<String> listGeneNameDNARep,
//			AsyncCallback<ArrayList<LightGeneItem>> callback) throws Exception;

	void getLightGeneItemWithGeneId(int geneId,
			AsyncCallback<LightGeneItem> callback) throws Exception;

	void getAllLightGeneItemWithElementId(int elementId,
			AsyncCallback<ArrayList<LightGeneItem>> callback) throws Exception;

	void getListQLightGeneItemWithOrigamiSyntenyAlignmentId(Long mainOrigamiSyntenyId, AsyncCallback<ArrayList<LightGeneItem>> callback);

	void getListLightGeneItemWithWithAlElementIdAndStartPbAndStopPbLooped(
			ArrayList<Integer> alElementIdTenStartThenStopLooped
			, boolean grabCDSWithAtLeastOnePbInThisStartStopLocus
			, AsyncCallback<ArrayList<LightGeneItem>> callback
			) throws Exception;

	
	void getAllLightGeneItemWithListGeneIds_sortedByGeneId(ArrayList<Integer> listGenesIds,
			AsyncCallback<ArrayList<LightGeneItem>> callback) throws Exception;

	void getGeneSetIdsWithOrigamiSyntenyId(
			Long mainOrigamiSyntenyId
			, HashSet<Integer> hsTypeToRetrieve
			, AsyncCallback<ArrayList<Integer>> callback
			) throws Exception;

	void getGeneSetIdsWithListElementIdsThenStartPbthenStopPBLooped(
			ArrayList<Integer> listElementIdsThenStartPbthenStopPBLoopedSent
			, boolean grabCDSWithAtLeastOnePbInThisStartStopLocus
			, AsyncCallback<ArrayList<Integer>> callback
			) throws Exception;

	void getIdentifiersForVisuAlignmentWithQAndSGeneId(
			int qGeneIdForVisuAlignment, int sGeneIdForVisuAlignment,
			AsyncCallback<ArrayList<String>> callback) throws Exception;

	void getReferenceGenesAndOrthologsOverRepresentatedInPhenotype(
			Integer referenceOrgaId,
			ArrayList<Integer> alSelectedPhenoPlusOrgaIds,
			ArrayList<Integer> alSelectedPhenoMinusOrgaIds,
			//ArrayList<Integer> alSelectedEitherPresenceOrAbsenceOrgaIds,
			AsyncCallback<AlItemsOfIntPrimaryIdAttributePlusScore> callback) throws Exception ;

	void getReferenceGenesMostRepresentedInAlOrganisms(Integer referenceOrgaId,
			ArrayList<Integer> alSelectedPhenoPlusOrgaIds,
			AsyncCallback<AlItemsOfIntPrimaryIdAttributePlusScore> callback) throws Exception;

	void getDatabaseIsGenesElementsGenomeAssemblyConvenience(AsyncCallback<Integer> callback) throws Exception;
	
	void getDatabaseIsNoMirror(AsyncCallback<Integer> callback) throws Exception;
	
	void getStatSummaryRefCDS(
			HashMap<Integer, HashMap<Integer, String>> qOrganismId2qGeneId2qGeneName
			, HashSet<Integer> hsOrgaIdsFeaturedGenomes
			//, HashSet<Integer> hsOrgaIdsMainList
			, AsyncCallback<String> callback) throws Exception;


}
