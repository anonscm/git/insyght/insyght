package fr.inra.jouy.client;


/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
//import com.google.gwt.event.logical.shared.ValueChangeHandler;
//import com.google.gwt.regexp.shared.MatchResult;
//import com.google.gwt.regexp.shared.RegExp;
//import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
//import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import fr.inra.jouy.client.RPC.CallForInfoDB;
import fr.inra.jouy.client.RPC.CallForInfoDBAsync;
import fr.inra.jouy.client.ressources.MyResources;
import fr.inra.jouy.client.view.MainTabPanelView;
import fr.inra.jouy.client.view.crossSections.popUp.PopUpDisplayGeneralInformation;
import fr.inra.jouy.client.view.header.HeaderView;
import fr.inra.jouy.client.view.result.CenterSLPAllGenomePanels;
import fr.inra.jouy.client.view.result.GenoOrgaAndHomoTablViewImpl;
//import fr.inra.jouy.client.view.search.GetResultDialog;
//import fr.inra.jouy.shared.AlignmentParametersForSynteny;
//import fr.inra.jouy.shared.LightElementItem;
//import fr.inra.jouy.shared.LightGeneItem;
//import fr.inra.jouy.shared.LightOrganismItem;
//import fr.inra.jouy.shared.OrganismItem;
//import fr.inra.jouy.shared.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
//import fr.inra.jouy.shared.SearchItem;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Insyght implements EntryPoint {
	
	private final CallForInfoDBAsync callForInfoDBService = (CallForInfoDBAsync) GWT
			.create(CallForInfoDB.class);
	
	private final HeaderView headerView = new HeaderView();
	private final MainTabPanelView mainTabPanelView = new MainTabPanelView();
	public static LoadingMaskDialog INSYGHT_LOADING_MASK = new LoadingMaskDialog();
	public static PopUpDisplayGeneralInformation POP_UP_DISPLAY_GENERAL_INFORMATION = new PopUpDisplayGeneralInformation();
	public static AppController APP_CONTROLER = new AppController();

	public static boolean HOME_VIEW_LOADED = false;
	public static boolean SEARCH_VIEW_LOADED = false;
	public static boolean GENOMIC_ORGA_AND_HOMO_TABLE_VIEW_LOADED = false;
	public static boolean ANNOT_COMPA_VIEW_LOADED = false;
	public static boolean ADMIN_VIEW_LOADED = false;
	public static boolean HEADER_VIEW_LOADED = false;
	public static UncaughtErrorsHandler UNCAUGHT_ERROR_HANDLER = new UncaughtErrorsHandler();
	
	//public static NamedFrame DOWNLOAD_FRAME = new NamedFrame("downloadFrame");
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		// set uncaught exception handler
		
		if(Window.Navigator.getAppName().compareTo("Microsoft Internet Explorer")==0){
			Window.alert("Insyght does NOT support internet explorer and may be unpredictible in this environement. Insyght supports firefox, chrome or safari.");
		}
		
		GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
			public void onUncaughtException(Throwable throwable) {
				
				UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, throwable);
				
			}
		});

		// use a deferred command so that the handler catches onModuleLoad2()
		// exceptions
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			// @Override
			public void execute() {
				onModuleLoad2();
			}
		});

	}

	private void onModuleLoad2() {
		
		
		// database queries prior to UI script execution
		databaseQueriesPriorToUIScriptExecution();
		
		
		// Inject the contents of the CSS file
	    MyResources.INSTANCE.gss().ensureInjected();

		Window.enableScrolling(false);
		Window.setMargin("0px");

		DockLayoutPanel outer = new DockLayoutPanel(Unit.EM);
		// outer.setStyleName("customgwt-DockLayoutPanel-lightBlueBkg");
		outer.addNorth(headerView, 4);
		outer.add(mainTabPanelView);

		// Add the outer panel to the RootLayoutPanel, so that it will be
		// displayed.
		RootLayoutPanel root = RootLayoutPanel.get();
		root.add(outer);

		NavigationControler.initHistoryHandler();
		
		if (NavigationControler.getToken().isEmpty()){
			Insyght.APP_CONTROLER.clearAppControlerFromAllResultData();
			//default view
			MainTabPanelView.tabPanel.selectTab(0);
			//NavigationControler.newItem(NavigationControler.getURLTockenFromCurrentState(), false);
		}else{
			//NavigationControler.fireCurrentHistoryState();
			waitFewMillisecondsForResultViewToLoad("fireCurrentHistoryState", -1, -1);
		}
		
//		DOWNLOAD_FRAME.setWidth("0px");
//		DOWNLOAD_FRAME.setHeight("0px");
//		//frame.getElement().getStyle().setBackgroundColor("gray");
//		DOWNLOAD_FRAME.setVisible(false);
////		DOWNLOAD_FRAME.addAttachHandler(new Handler() {
////			@Override
////			public void onAttachOrDetach(AttachEvent event) {
////				Window.alert("aaaa");
////			}
////		});
////		DOWNLOAD_FRAME.addLoadHandler(new LoadHandler() //NOT WORKING
////		{
////		    public void onLoad(LoadEvent event)
////		    {
////		        //your code here
////		    	Window.alert("jhfjhf");
////		    	GWT.log("download starts...");
////		    }
////		});
//		RootPanel.get().add(DOWNLOAD_FRAME);
		//DOWNLOAD_FRAME = Frame.wrap(Document.get().getElementById(DOWNLOAD_IFRAME));
		
	}
	
	private void databaseQueriesPriorToUIScriptExecution() {
		queryDATABASE_IS_genes_elements_genome_assembly_convenience();
		queryDATABASE_IS_NO_MIRROR();
	}
	
	private void queryDATABASE_IS_genes_elements_genome_assembly_convenience() {
		AsyncCallback<Integer> callback = new AsyncCallback<Integer>(){
			@Override
			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			@Override
			public void onSuccess(Integer DATABASE_IS_genes_elements_genome_assembly_convenienceSent) {
				AppController.setDATABASE_IS_genes_elements_genome_assembly_convenience(DATABASE_IS_genes_elements_genome_assembly_convenienceSent);
			}
		};
		try {
			callForInfoDBService.getDatabaseIsGenesElementsGenomeAssemblyConvenience(callback);
		} catch (Exception e) {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}
	}
	
	private void queryDATABASE_IS_NO_MIRROR() {
		AsyncCallback<Integer> callback = new AsyncCallback<Integer>(){
			@Override
			public void onFailure(Throwable caught) {
				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, caught);
			}
			@Override
			public void onSuccess(Integer DATABASE_IS_NO_MIRRORSent) {
				AppController.setDATABASE_IS_NO_MIRROR(DATABASE_IS_NO_MIRRORSent);
			}
		};
		try {
			callForInfoDBService.getDatabaseIsNoMirror(
						callback);
		} catch (Exception e) {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
		}
	}

	//TODO needed ?
	private static void waitFewMillisecondsForResultViewToLoad(final String actionToDo, final int firstRowShownIdIT, final int selectedTab) {
		//actionToDo
		//fireCurrentHistoryState
		//updateResultsDisplay
		
		if(!GENOMIC_ORGA_AND_HOMO_TABLE_VIEW_LOADED
				&& !ANNOT_COMPA_VIEW_LOADED ){
			// Create a new timer that calls Window.alert().
		    Timer t = new Timer() {
		      @Override
		      public void run() {
		    	  if(!GENOMIC_ORGA_AND_HOMO_TABLE_VIEW_LOADED
							&& !ANNOT_COMPA_VIEW_LOADED){
		    		  waitFewMillisecondsForResultViewToLoad(actionToDo, firstRowShownIdIT, selectedTab);
		    	  }else{
		    		  doActionAfterWaitForLoad(actionToDo, firstRowShownIdIT, selectedTab);
		    	  }
		      }
		    };

		    // Schedule the timer to run once in 0.5 seconds.
		    //System.out.println("wait 0.5s...");
		    t.schedule(500);
		}else{
			 doActionAfterWaitForLoad(actionToDo, firstRowShownIdIT, selectedTab);
		}
		
		
	}

	protected static void doActionAfterWaitForLoad(String actionToDo,
			int firstRowShownIdIT, int selectedTab) {
		
		 if(actionToDo.compareTo("fireCurrentHistoryState")==0){
			 NavigationControler.fireCurrentHistoryState();
		  }else if(actionToDo.compareTo("updateResultsDisplay")==0){
			  
			  int indexToRestore = NavigationControler.getIndexToDrawWithOrgaId(firstRowShownIdIT);
			  //CenterSLPAllGenomePanels.START_INDEX = indexToRestore;
			  
			  if(MainTabPanelView.tabPanel.getSelectedIndex() != selectedTab){
				  GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
				  CenterSLPAllGenomePanels.START_INDEX = indexToRestore;
				  MainTabPanelView.tabPanel.selectTab(selectedTab, true);
			  }else if (CenterSLPAllGenomePanels.START_INDEX != indexToRestore){
				  GenoOrgaAndHomoTablViewImpl.cleanUpCSSHighlightedLabelInQuickNavOnDisplayedPage();
				  CenterSLPAllGenomePanels.START_INDEX = indexToRestore;
				  Insyght.APP_CONTROLER.clearSyntenyCanvasSelection();
				  CenterSLPAllGenomePanels.updateResultsDisplay(true, true, true);
			  }
			  
		  }else{
			  Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in waitFewMillisecondsForResultViewToLoad ; actionToDo ="+actionToDo));
		  }
		
	}




}
