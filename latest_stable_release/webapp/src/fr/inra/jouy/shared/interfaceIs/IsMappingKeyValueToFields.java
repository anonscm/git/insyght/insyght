package fr.inra.jouy.shared.interfaceIs;


/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

public interface IsMappingKeyValueToFields {
	
	//Integer
	public abstract boolean mapKeyIntValueToObjectField(String key, Integer value); // return true if mapping successful, false otherwise
	//String
	public abstract boolean mapKeyStringValueToObjectField(String key, String value); // return true if mapping successful, false otherwise
	//Boolean
	public abstract boolean mapKeyBooleanValueToObjectField(String key, Boolean value); // return true if mapping successful, false otherwise
	//Long
	public abstract boolean mapKeyLongValueToObjectField(String key, Long value); // return true if mapping successful, false otherwise
	//Double
	public abstract boolean mapKeyDoubleValueToObjectField(String key, Double value); // return true if mapping successful, false otherwise
	//Float
	public abstract boolean mapKeyFloatValueToObjectField(String key, Float value); // return true if mapping successful, false otherwise

	// Unknow Type Value
	public abstract boolean mapKeyUnknowTypeValueToObjectField(String key, String value); // return true if mapping successful, false otherwise

	
}
