package fr.inra.jouy.shared.comparators;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.util.Comparator;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.view.crossSections.popUp.ManageListComparedOrganisms;
import fr.inra.jouy.client.view.search.SearchViewImpl;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;

public class LightOrganismItemComparators {
	
	public static ByFullNameComparator byFullNameComparator = new ByFullNameComparator();
	public static ByPositionInResultSetThenByAlphabeticalOrderComparator byPositionInResultSetComparator = new ByPositionInResultSetThenByAlphabeticalOrderComparator();
	public static ByPositionInGetUserSearchItemGetListUserSelectedOrgaToBeResults byPositionInGetUserSearchItemGetListUserSelectedOrgaToBeResults = new ByPositionInGetUserSearchItemGetListUserSelectedOrgaToBeResults();
	public static ByPositionInSearchViewImplComparedGenomes_alOrgaMainList byPositionInSearchViewImplComparedGenomes_alOrgaMainList = new ByPositionInSearchViewImplComparedGenomes_alOrgaMainList();
	public static ByPositionInManageListComparedOrganismsAlOrgaMainList byPositionInManageListComparedOrganismsAlOrgaMainList = new ByPositionInManageListComparedOrganismsAlOrgaMainList();
	public static ByPositionInManageListComparedOrganismsAlOrgaFeaturedList byPositionInManageListComparedOrganismsAlOrgaFeaturedList = new ByPositionInManageListComparedOrganismsAlOrgaFeaturedList();
	
	
	public static class ByPositionInResultSetThenByAlphabeticalOrderComparator implements Comparator<LightOrganismItem> {
		@Override
		public int compare(LightOrganismItem arg0, LightOrganismItem arg1) {
			int intToReturn = arg0.getPositionInResultList() - arg1.getPositionInResultList();
			if (intToReturn == 0) {
				return arg0.getFullName().compareTo(arg1.getFullName());
			} else {
				return intToReturn;
			}
		}
	}

	public static class ByPositionInGetUserSearchItemGetListUserSelectedOrgaToBeResults implements Comparator<LightOrganismItem> {
		@Override
		public int compare(LightOrganismItem arg0, LightOrganismItem arg1) {
			int intToReturn = Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().indexOf(arg0) - Insyght.APP_CONTROLER.getUserSearchItem().getListUserSelectedOrgaToBeResults().indexOf(arg1);
			return intToReturn;
		}
	}
	
	public static class ByPositionInSearchViewImplComparedGenomes_alOrgaMainList implements Comparator<LightOrganismItem> {
		@Override
		public int compare(LightOrganismItem arg0, LightOrganismItem arg1) {
			int intToReturn = SearchViewImpl.comparedGenomes_alOrgaMainList_filtredIn.indexOf(arg0) - SearchViewImpl.comparedGenomes_alOrgaMainList_filtredIn.indexOf(arg1);
			return intToReturn;
		}
	}
	
	public static class ByPositionInManageListComparedOrganismsAlOrgaMainList implements Comparator<LightOrganismItem> {
		@Override
		public int compare(LightOrganismItem arg0, LightOrganismItem arg1) {
			int intToReturn = ManageListComparedOrganisms.alOrgaMainList_filtredIn.indexOf(arg0) - ManageListComparedOrganisms.alOrgaMainList_filtredIn.indexOf(arg1);
			return intToReturn;
		}
	}
	
	public static class ByPositionInManageListComparedOrganismsAlOrgaFeaturedList implements Comparator<LightOrganismItem> {
		@Override
		public int compare(LightOrganismItem arg0, LightOrganismItem arg1) {
			int intToReturn = ManageListComparedOrganisms.alOrgaFeaturedList_filtredIn.indexOf(arg0) - ManageListComparedOrganisms.alOrgaFeaturedList_filtredIn.indexOf(arg1);
			return intToReturn;
		}
	}
	
	public static class ByFullNameComparator implements Comparator<LightOrganismItem> {
		@Override
		public int compare(LightOrganismItem arg0, LightOrganismItem arg1) {
			int compInt = 0;
			if (arg0.getSpecies() != null && arg1.getSpecies() != null) {
				compInt = arg0.getSpecies().compareTo(arg1.getSpecies());
				if (compInt == 0) {
					if (arg0.getStrain() != null && arg1.getStrain() != null ) {
						compInt = arg0.getStrain().compareTo(arg1.getStrain());
						if (compInt == 0) {
							if (arg0.getSubstrain() != null && arg1.getSubstrain() != null ) {
								compInt = arg0.getSubstrain().compareTo(arg1.getSubstrain());
								if (compInt == 0) {
									compInt = arg0.getTaxonId() - arg1.getTaxonId();
								}
							}
						}
					}
				}
			}
			return compInt;
			//return arg0.getFullName().compareTo(arg1.getFullName()); //too slow for 5000 genomes down from 2.2 seconds to 55 milliseconds
		}
	}


	
	/* too slow, sort array and replay recorded move made to mainList
	public static int getBestInsertionIndexForLightOrganismItemWithinArray_byPositionInResultSet(
			LightOrganismItem lOIToMoveBetweenLists
			, ArrayList<LightOrganismItem> listTo
			//, ArrayList<LightOrganismItem> listFrom
			) {
		
		int idxToReturn = 0;//lOIToMoveBetweenLists.getPositionInResultList() - 1;
		
		int countGapsLowerPosition = 0;
		for ( int i = 0 ; i < listFrom.size() ; i++) {
			LightOrganismItem LOIIT = listFrom.get(i);
			if (LOIIT.getPositionInResultList() < positionInResultList_LOIToMove) {
				countGapsLowerPosition++;
			}	
		}
		idxToReturn = positionInResultList_LOIToMove - 1 - countGapsLowerPosition;
		
		
		//look for minimal local around that position in listTo
		
		
		
		if (idxToReturn < 0) {
			idxToReturn = 0;
		}
		if (idxToReturn > listTo.size()) {
			idxToReturn = listTo.size();
		}
		return idxToReturn;
		
	}
	*/
	


}
