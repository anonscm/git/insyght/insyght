package fr.inra.jouy.shared;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.interfaceIs.IsStringifyToJSON;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneMatchItem;
import fr.inra.jouy.shared.pojos.databaseMapping.TransAlignmPairs;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGeneInserItem;

public class TransAPMissGeneHomoInfo extends TransAlignmPairs implements IsSerializable, IsStringifyToJSON {
	
	//private long origamiAlignmentId = 0; // can be negative if isDataFromMirrorQuery
	//private int origamiAlignmentPairsType = -1;
	//private int qGeneId = -1;
	//private int sGeneId = -1;
	private String qName = "";
	private int qStrand = -1;
	private String qLocusTag = "";
	private String sName = "";
	private int sStrand = -1;
	private String sLocusTag = "";
	private ArrayList<LightGeneMatchItem> listLightGeneMatchItem = new ArrayList<LightGeneMatchItem>();
	
	public TransAPMissGeneHomoInfo(){
	}

	public TransAPMissGeneHomoInfo(long alignmentId, int qGeneId, int sGeneId, int type) {
		super(alignmentId, qGeneId, sGeneId, type);
	}


	public TransAPMissGeneHomoInfo(AbsoPropQGeneInserItem apqgii){
		setAlignmentId(apqgii.getqOrigamiAlignmentId());
		setType(apqgii.getqOrigamiAlignmentPairsType());
		setqGeneId(apqgii.getQGeneId());
		setqLocusTag(apqgii.getQLocusTagAsRawString());
		setqName(apqgii.getQNameAsRawString());
		setqStrand(apqgii.getQStrand());
		
		setsGeneId(apqgii.getIfMissingTargetSGeneId());
		setsLocusTag(apqgii.getIfMissingTargetSLocusTagAsRawString());
		setsName(apqgii.getIfMissingTargetSNameAsRawString());
		setsStrand(apqgii.getIfMissingTargetSStrand());
		
	}
	 
	
	public TransAPMissGeneHomoInfo(AbsoPropSGeneInserItem apsgii){
		setAlignmentId(apsgii.getsOrigamiAlignmentId());
		setType(apsgii.getsOrigamiAlignmentPairsType());
		setsGeneId(apsgii.getsGeneId());
		setsLocusTag(apsgii.getsLocusTagAsRawString());
		setsName(apsgii.getsNameAsRawString());
		setsStrand(apsgii.getsStrand());
		
		setqGeneId(apsgii.getIfMissingTargetQGeneId());
		setqLocusTag(apsgii.getIfMissingTargetQLocusTagAsRawString());
		setqName(apsgii.getIfMissingTargetQNameAsRawString());
		setqStrand(apsgii.getIfMissingTargetQStrand());
		
	}
	 
	public TransAPMissGeneHomoInfo(
			AbsoPropQSGeneHomoItem apqsghiSent) {
		setListLightGeneMatchItem(apqsghiSent.getListLightGeneMatchItem());
		setAlignmentId(apqsghiSent.getSyntenyOrigamiAlignmentId());
		setType(apqsghiSent.getQsOrigamiAlignmentPairsType());
		setqGeneId(apqsghiSent.getQsQGeneId());
		setqLocusTag(apqsghiSent.getQsQLocusTagAsRawString());
		setqName(apqsghiSent.getQsQNameAsRawString());
		setqStrand(apqsghiSent.getQsQStrand());
		setsGeneId(apqsghiSent.getQsSGeneId());
		setsLocusTag(apqsghiSent.getQsSLocusTagAsRawString());
		setsName(apqsghiSent.getQsSNameAsRawString());
		setsStrand(apqsghiSent.getQsSStrand());
	}

	/*
	//Done isDataFromMirrorQuery
	public long getOrigamiAlignmentId() {
		return origamiAlignmentId;
	}

	//Done isDataFromMirrorQuery
	public void setOrigamiAlignmentId(long l) {
		this.origamiAlignmentId = l;
	}

	public int getOrigamiAlignmentPairsType() {
		return origamiAlignmentPairsType;
	}

	public void setOrigamiAlignmentPairsType(int origamiAlignmentPairsType) {
		this.origamiAlignmentPairsType = origamiAlignmentPairsType;
	}

	public int getqGeneId() {
		return qGeneId;
	}

	public void setqGeneId(int qGeneId) {
		this.qGeneId = qGeneId;
	}

	public int getsGeneId() {
		return sGeneId;
	}

	public void setsGeneId(int sGeneId) {
		this.sGeneId = sGeneId;
	}
	*/
	




	public String getqName() {
		return qName;
	}

	public void setqName(String qName) {
		this.qName = qName;
	}

	public int getqStrand() {
		return qStrand;
	}

	public void setqStrand(int qStrand) {
		this.qStrand = qStrand;
	}

	public String getqLocusTag() {
		return qLocusTag;
	}

	public void setqLocusTag(String qLocusTag) {
		this.qLocusTag = qLocusTag;
	}

	public ArrayList<LightGeneMatchItem> getListLightGeneMatchItem() {
		return listLightGeneMatchItem;
	}

	public void setListLightGeneMatchItem(
			ArrayList<LightGeneMatchItem> listLightGeneMatchItem) {
		this.listLightGeneMatchItem = listLightGeneMatchItem;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public int getsStrand() {
		return sStrand;
	}

	public void setsStrand(int sStrand) {
		this.sStrand = sStrand;
	}

	public String getsLocusTag() {
		return sLocusTag;
	}

	public void setsLocusTag(String sLocusTag) {
		this.sLocusTag = sLocusTag;
	}

	@Override
	public String stringifyToJSON(boolean encloseWithCurlyBraces, boolean propagateToParentClassViaSuper)
			throws Exception {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String field2String = "";
		if(propagateToParentClassViaSuper){
			field2String += super.stringifyToJSON(false, propagateToParentClassViaSuper);
		}
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("qName",qName,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("qStrand",qStrand,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("qLocusTag",qLocusTag,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("sName",sName,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("sStrand",sStrand,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("sLocusTag",sLocusTag,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringAlIsConvertibleToJSONObject(
				"listLightGeneMatchItem"
				, listLightGeneMatchItem
				, field2String.isEmpty()
				, propagateToParentClassViaSuper
				, false
				);
		
		if( encloseWithCurlyBraces){
			return "{"+field2String+"}";
		} else {
			return field2String;
		}
	}
	
}
