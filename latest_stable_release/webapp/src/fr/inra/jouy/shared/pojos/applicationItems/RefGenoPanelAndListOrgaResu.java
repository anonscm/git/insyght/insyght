package fr.inra.jouy.shared.pojos.applicationItems;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.RefGeneSetForAnnotCompa;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_scope;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortType;
import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.Enum_comparedGenomes_SortResultListBy_sortOrder;
//import fr.inra.jouy.shared.pojos.applicationItems.SearchItem.EnumResultListSortScopeType;
import fr.inra.jouy.shared.pojos.databaseMapping.AlignmentParametersForSynteny;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;

public class RefGenoPanelAndListOrgaResu implements IsSerializable{

	//same as SearchItem :
	/*private String resultListSortScopeType = "";
	//can be : Scope_Gene_Type_AbundanceHomologs
	//can be : Scope_Gene_Type_BestSyntenyScore
	//can be : Scope_Gene_Type_BestAlignemntScore
	//can be : Scope_Gene_Type_AbundanceHomologsAnnotations
	//can be : Scope_GeneSet_Type_AbundanceHomologs
	//can be : Scope_GeneSet_Type_BestSyntenyScore
	//can be : Scope_GeneSet_Type_BestAlignemntScore
	//can be : Scope_Organism_Type_AbundanceHomologs
	//can be : Scope_Organism_Type_BestSyntenyScore
	//can be : Scope_Organism_Type_BestAlignemntScore
	private String defaultVisualisationMode = null; // CAN BE Gene_Set_Browsing OR Genome_Walk OR Genomic_Region_Search
	private LightElementItem referenceElement = null;
	private AlignmentParametersForSynteny alignmentParametersForSynteny = null;
	private boolean excludeAutomaticallyComputedResults = false;
	private ArrayList<GeneItem> listReferenceGeneSet = new ArrayList<GeneItem>(); //GeneItem
	private ArrayList<LightOrganismItem> listUserSelectedOrgaToBeResults = new ArrayList<LightOrganismItem>(); //ElementItem
	private ArrayList<LightOrganismItem> listExcludedGenome = new ArrayList<LightOrganismItem>();*/
	
	public enum EnumResultViewTypes implements IsSerializable {
		homolog_table, annotations_comparator, genomic_organization,
		search, admin
	};
	
	
	//specific to RefGenomePanelAndListOrgaResult :
	private ArrayList<LightOrganismItem> lstOrgaResult = null;
	private GenomePanelItem referenceGenomePanelItem = null;
	//private boolean hasReachExtremeEndDowstream = false;
	//private boolean hasReachExtremeEndUpstream = false;
	AlignmentParametersForSynteny alignmentParametersForSynteny = new AlignmentParametersForSynteny();
	//private EnumResultListSortScopeType resultListSortScopeType = null;
	private Enum_comparedGenomes_SortResultListBy_scope sortResultListBy_scope = null;
	private Enum_comparedGenomes_SortResultListBy_sortType sortResultListBy_sortType = null;
	private Enum_comparedGenomes_SortResultListBy_sortOrder sortResultListBy_sortOrder = null;
	private boolean excludeAutomaticallyComputedResults = false;
	//private ArrayList<LightGeneItem> listReferenceGeneSet = new ArrayList<LightGeneItem>();
	private ArrayList<Integer> listReferenceGeneSetForHomologsTable = new ArrayList<Integer>();
	private RefGeneSetForAnnotCompa refGeneSetForAnnotationsComparator = new RefGeneSetForAnnotCompa();
	private ArrayList<LightOrganismItem> listUserSelectedOrgaToBeResults = new ArrayList<LightOrganismItem>();
	private ArrayList<LightOrganismItem> listExcludedGenome = new ArrayList<LightOrganismItem>();
	private int countTotalNumberCDSForRefOrganismIfRefGeneSetIsWholeOrga = -1;
	
	
	private EnumResultViewTypes viewTypeInsyght = null;
	private int firstRowShownId = -1;
	//private int globalLookUpForSymbolicSliceAtQPosition = -1;
	//private int globalLookUpForSymbolicSliceAtQElementId = -1;
	
	private int globalLookUpForGeneWithQStart = -1;
	private int globalLookUpForGeneWithQStop = -1;
	private int globalLookUpForGeneOnQElementId = -1;
	
	
	public RefGenoPanelAndListOrgaResu(){
		setAlignmentParametersForSyntenyToStrict(false);
	}
	
	
	public ArrayList<LightOrganismItem> getLstOrgaResult() {
		return lstOrgaResult;
	}
	public void setLstOrgaResult(ArrayList<LightOrganismItem> lstOrgaResult) {
		this.lstOrgaResult = lstOrgaResult;
	}
	
	public GenomePanelItem getReferenceGenomePanelItem() {
		return referenceGenomePanelItem;
	}
	public void setReferenceGenomePanelItem(GenomePanelItem referenceGenomePanelItem) {
		this.referenceGenomePanelItem = referenceGenomePanelItem;
	}
	

	public void setListExcludedGenome(ArrayList<LightOrganismItem> listExcludedGenome) {
		this.listExcludedGenome = listExcludedGenome;
	}

	public ArrayList<LightOrganismItem> getListExcludedGenome() {
		return listExcludedGenome;
	}

	/*
	public void setResultListSortScopeType(EnumResultListSortScopeType resultListSortScopeType) {
		this.resultListSortScopeType = resultListSortScopeType;
	}

	public EnumResultListSortScopeType getResultListSortScopeType() {
		return resultListSortScopeType;
	}
	*/

	public Enum_comparedGenomes_SortResultListBy_scope getSortResultListBy_scope() {
		return sortResultListBy_scope;
	}


	public void setSortResultListBy_scope(Enum_comparedGenomes_SortResultListBy_scope sortResultListBy_scope) {
		this.sortResultListBy_scope = sortResultListBy_scope;
	}


	public Enum_comparedGenomes_SortResultListBy_sortType getSortResultListBy_sortType() {
		return sortResultListBy_sortType;
	}


	public void setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType sortResultListBy_sortType) {
		this.sortResultListBy_sortType = sortResultListBy_sortType;
	}


	public Enum_comparedGenomes_SortResultListBy_sortOrder getSortResultListBy_sortOrder() {
		return sortResultListBy_sortOrder;
	}


	public void setSortResultListBy_sortOrder(Enum_comparedGenomes_SortResultListBy_sortOrder sortResultListBy_sortOrder) {
		this.sortResultListBy_sortOrder = sortResultListBy_sortOrder;
	}

	public AlignmentParametersForSynteny getAlignmentParametersForSynteny() {
		return alignmentParametersForSynteny;
	}
	
	public void setAlignmentParametersForSynteny(AlignmentParametersForSynteny alignmentParametersForSyntenySent) {
		this.alignmentParametersForSynteny = alignmentParametersForSyntenySent;
	}
	
	public void setAlignmentParametersForSyntenyToStrict(
			boolean isStrict) {
		if(isStrict){
			//Window.alert("strict params selected");
			alignmentParametersForSynteny.setOrthoScore(1);
			alignmentParametersForSynteny.setHomoScore(-10000);
			alignmentParametersForSynteny.setMismatchPenalty(-10000);
			alignmentParametersForSynteny.setGapCreationPenalty(-10000);
			alignmentParametersForSynteny.setGapExtensionPenalty(-10000);
			alignmentParametersForSynteny.setMinAlignSize(1);
			alignmentParametersForSynteny.setMinScore(2);
			alignmentParametersForSynteny.setOrthologsIncluded(true);
		}else {
			//Window.alert("loose params selected");
			alignmentParametersForSynteny.setOrthoScore(4);
			alignmentParametersForSynteny.setHomoScore(2);
			alignmentParametersForSynteny.setMismatchPenalty(-4);
			alignmentParametersForSynteny.setGapCreationPenalty(-8);
			alignmentParametersForSynteny.setGapExtensionPenalty(-2);
			alignmentParametersForSynteny.setMinAlignSize(1);
			alignmentParametersForSynteny.setMinScore(8);
			alignmentParametersForSynteny.setOrthologsIncluded(true);
		}
	}

	public ArrayList<LightOrganismItem> getListUserSelectedOrgaToBeResults() {
		return listUserSelectedOrgaToBeResults;
	}

	public void setListUserSelectedOrgaToBeResults(
			ArrayList<LightOrganismItem> listUserSelectedOrgaToBeResults) {
		if(listUserSelectedOrgaToBeResults == null){
			this.listUserSelectedOrgaToBeResults.clear();
		}else{
			this.listUserSelectedOrgaToBeResults = listUserSelectedOrgaToBeResults;
		}
	}
	
	public void addToListUserSelectedOrgaToBeResults(
			LightOrganismItem orgaItemSent) {
		this.listUserSelectedOrgaToBeResults.add(orgaItemSent);
	}

	public ArrayList<Integer> getListReferenceGeneSetForHomologsTable() {
		return listReferenceGeneSetForHomologsTable;
	}

	public void setListReferenceGeneSetForHomologsTable(ArrayList<Integer> listReferenceGeneSetSent) {
		if(listReferenceGeneSetSent == null){
			this.listReferenceGeneSetForHomologsTable.clear();
		}else{
			this.listReferenceGeneSetForHomologsTable = listReferenceGeneSetSent;
		}
	}
	
//	public void addGeneToListReferenceGeneSet(GeneItem geneItemSent){
//		this.listReferenceGeneSet.add(geneItemSent);
//	}
	
	public boolean isExcludeAutomaticallyComputedResults() {
		return excludeAutomaticallyComputedResults;
	}

	public void setExcludeAutomaticallyComputedResults(
			boolean excludeAutomaticallyComputedResults) {
		this.excludeAutomaticallyComputedResults = excludeAutomaticallyComputedResults;
	}


	public EnumResultViewTypes getViewTypeInsyght() {
		return viewTypeInsyght;
	}


	public void setViewTypeInsyght(EnumResultViewTypes viewTypeSent) {
		this.viewTypeInsyght = viewTypeSent;
	}


//	public int getGlobalLookUpForSymbolicSliceAtQPosition() {
//		return globalLookUpForSymbolicSliceAtQPosition;
//	}
//
//
//	public void setGlobalLookUpForSymbolicSliceAtQPosition(
//			int globalLookUpForSymbolicSliceAtQPositionSent) {
//		this.globalLookUpForSymbolicSliceAtQPosition = globalLookUpForSymbolicSliceAtQPositionSent;
//	}
//
//
//	public int getGlobalLookUpForSymbolicSliceAtQElementId() {
//		return globalLookUpForSymbolicSliceAtQElementId;
//	}
//
//
//	public void setGlobalLookUpForSymbolicSliceAtQElementId(
//			int globalLookUpForSymbolicSliceAtQElementIdSent) {
//		this.globalLookUpForSymbolicSliceAtQElementId = globalLookUpForSymbolicSliceAtQElementIdSent;
//	}


	public int getGlobalLookUpForGeneWithQStart() {
		return globalLookUpForGeneWithQStart;
	}


	public void setGlobalLookUpForGeneWithQStart(
			int globalLookUpForGeneWithQStart) {
		this.globalLookUpForGeneWithQStart = globalLookUpForGeneWithQStart;
	}


	public int getGlobalLookUpForGeneWithQStop() {
		return globalLookUpForGeneWithQStop;
	}


	public void setGlobalLookUpForGeneWithQStop(int globalLookUpForGeneWithQStop) {
		this.globalLookUpForGeneWithQStop = globalLookUpForGeneWithQStop;
	}


	public int getGlobalLookUpForGeneOnQElementId() {
		return globalLookUpForGeneOnQElementId;
	}


	public void setGlobalLookUpForGeneOnQElementId(
			int globalLookUpForGeneOnQElementId) {
		this.globalLookUpForGeneOnQElementId = globalLookUpForGeneOnQElementId;
	}


	public RefGeneSetForAnnotCompa getRefGeneSetForAnnotationsComparator() {
		return refGeneSetForAnnotationsComparator;
	}


	public void setRefGeneSetForAnnotationsComparator(
			RefGeneSetForAnnotCompa refGeneSetForAnnotationsComparator) {
		this.refGeneSetForAnnotationsComparator = refGeneSetForAnnotationsComparator;
	}


	public int getFirstRowShownId() {
		return firstRowShownId;
	}


	public void setFirstRowShownId(int firstRowShownId) {
		this.firstRowShownId = firstRowShownId;
	}


	public int getCountTotalNumberCDSForRefOrganismIfRefGeneSetIsWholeOrga() {
		return countTotalNumberCDSForRefOrganismIfRefGeneSetIsWholeOrga;
	}

	
	public void setCountTotalNumberCDSForRefOrganismIfRefGeneSetIsWholeOrga(int countTotalNumberCDSForRefOrganism) {
		this.countTotalNumberCDSForRefOrganismIfRefGeneSetIsWholeOrga = countTotalNumberCDSForRefOrganism;
		
	}

	
}
