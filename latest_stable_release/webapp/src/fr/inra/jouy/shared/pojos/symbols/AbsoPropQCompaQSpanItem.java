package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;


public class AbsoPropQCompaQSpanItem extends AbsoPropQCompaItem implements IsSerializable {

	public enum QEnumBlockType implements IsSerializable {
		//basic block
		Q_INSERTION,
		
		//basic block partial
		Q_PARTIAL_LEFT_HOMOLOGS_BLOCK, Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK, Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK, 
		Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK, Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK, Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK, 
		Q_PARTIAL_LEFT_INSERTION, Q_PARTIAL_RIGHT_INSERTION, Q_PARTIAL_RIGHTANDLEFT_INSERTION, 
		
		//basic block missing target
		Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT, Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT, Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT, Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT,
		//cut missing target
		Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_RIGHT, Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_LEFT, Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_RIGHT, Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_LEFT, 
		Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_RIGHT, Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_LEFT, Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_RIGHT, Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_LEFT, 
		
		//cut start and stop
		Q_CUT_START_Q_INSERTION, Q_CUT_STOP_Q_INSERTION
		
	}


	private double qPercentStop = 2;
	private QEnumBlockType qEnumBlockType = null;
	
	
	public AbsoPropQCompaQSpanItem(){
		
	}
	
	public AbsoPropQCompaQSpanItem(AbsoPropQCompaQSpanItem absoluteProportionQComparableQSpanItemSent){
		setqEnumBlockType(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType());
		setqPercentStart(absoluteProportionQComparableQSpanItemSent.getqPercentStart());
		setqPercentStop(absoluteProportionQComparableQSpanItemSent.getqPercentStop());
	}
	
	public AbsoPropQCompaQSpanItem(QEnumBlockType qEnumBlockTypeSent, double qPercentStartSent, double qPercentStopSent){
		setqEnumBlockType(qEnumBlockTypeSent);
		setqPercentStart(qPercentStartSent);
		setqPercentStop(qPercentStopSent);
	}
	
	
	public void setqPercentStop(double qPercentStop) {
		this.qPercentStop = qPercentStop;
	}


	public double getqPercentStop() {
		return qPercentStop;
		/*if(qEnumBlockType.compareTo(QEnumBlockType.Q_CUT_START_Q_INSERTION)==0){
			return getqPercentStart();
		}else{
			return qPercentStop;
		}*/
	}


	public void setqEnumBlockType(QEnumBlockType qEnumBlockType) {
		this.qEnumBlockType = qEnumBlockType;
	}

	public QEnumBlockType getqEnumBlockType() {
		return qEnumBlockType;
	}


}
