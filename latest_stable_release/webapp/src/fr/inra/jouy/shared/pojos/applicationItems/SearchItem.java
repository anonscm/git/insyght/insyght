package fr.inra.jouy.shared.pojos.applicationItems;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import fr.inra.jouy.shared.pojos.databaseMapping.AlignmentParametersForSynteny;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.databaseMapping.OrganismItem;



public class SearchItem implements IsSerializable{
	
	private OrganismItem referenceOrganism = null;
	AlignmentParametersForSynteny alignmentParametersForSynteny = new AlignmentParametersForSynteny();
	
	public static enum Enum_comparedGenomes_SortResultListBy_scope implements IsSerializable {
		SelectedRefGene, RefGeneSet, WholeOrganism
		}
	private Enum_comparedGenomes_SortResultListBy_scope sortResultListBy_scope = null;
	public static enum Enum_comparedGenomes_SortResultListBy_sortType implements IsSerializable {
		AbundanceHomologs, SyntenyScore, AlignemntScore, AbundanceHomologsAnnotations
		}
	private Enum_comparedGenomes_SortResultListBy_sortType sortResultListBy_sortType = null;
	public static enum Enum_comparedGenomes_SortResultListBy_sortOrder implements IsSerializable {
		Descending, Ascending
	}
	private Enum_comparedGenomes_SortResultListBy_sortOrder sortResultListBy_sortOrder = null;
	
	/*
	public enum EnumResultListSortScopeType implements IsSerializable{
		SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC, SCOPE_GENE_TYPE_BESTSYNTENYSCORE_ORDER_ASC, SCOPE_GENE_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC, SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGSANNOTATIONS_ORDER_ASC,
		SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC, SCOPE_GENE_TYPE_BESTSYNTENYSCORE_ORDER_DESC, SCOPE_GENE_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC, SCOPE_GENE_TYPE_ABUNDANCEHOMOLOGSANNOTATIONS_ORDER_DESC,
		SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC, SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_ASC, SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC,
		SCOPE_GENESET_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC, SCOPE_GENESET_TYPE_BESTSYNTENYSCORE_ORDER_DESC, SCOPE_GENESET_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC,
		SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_ASC, SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_ASC, SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_ASC,
		SCOPE_ORGANISM_TYPE_ABUNDANCEHOMOLOGS_ORDER_DESC, SCOPE_ORGANISM_TYPE_BESTSYNTENYSCORE_ORDER_DESC, SCOPE_ORGANISM_TYPE_BESTALIGNEMNTSCORE_ORDER_DESC
	};
	private EnumResultListSortScopeType resultListSortScopeType = null;
	*/
	
	/*public static EnumResultListSortScopeType getEnumResultListSortScopeTypeWithString(
			String pResultListSortScopeTypeIT) {
		for (EnumResultListSortScopeType erlsstIT : EnumResultListSortScopeType.values()) {
			  if(erlsstIT.toString().compareTo(pResultListSortScopeTypeIT) == 0){
				  return erlsstIT;
			  }
		}
		return null;
	}*/
	public static Enum_comparedGenomes_SortResultListBy_scope getEnum_comparedGenomes_SortResultListBy_scope_WithString(
			String scopeIT) {
		for (Enum_comparedGenomes_SortResultListBy_scope erlsstIT : Enum_comparedGenomes_SortResultListBy_scope.values()) {
			  if(erlsstIT.toString().compareTo(scopeIT) == 0){
				  return erlsstIT;
			  }
		}
		return null;
	}
	public static Enum_comparedGenomes_SortResultListBy_sortType getEnum_comparedGenomes_SortResultListBy_sortType_WithString(
			String sortTypeIT) {
		for (Enum_comparedGenomes_SortResultListBy_sortType erlsstIT : Enum_comparedGenomes_SortResultListBy_sortType.values()) {
			  if(erlsstIT.toString().compareTo(sortTypeIT) == 0){
				  return erlsstIT;
			  }
		}
		return null;
	}
	public static Enum_comparedGenomes_SortResultListBy_sortOrder getEnum_comparedGenomes_SortResultListBy_sortOrder_WithString(
			String sortOrderIT) {
		for (Enum_comparedGenomes_SortResultListBy_sortOrder erlsstIT : Enum_comparedGenomes_SortResultListBy_sortOrder.values()) {
			  if(erlsstIT.toString().compareTo(sortOrderIT) == 0){
				  return erlsstIT;
			  }
		}
		return null;
	}
	
	private boolean excludeAutomaticallyComputedResults = false;
	private ArrayList<LightGeneItem> listReferenceGeneSet = new ArrayList<LightGeneItem>();
	private ArrayList<LightOrganismItem> listUserSelectedOrgaToBeResults = new ArrayList<LightOrganismItem>();
	private ArrayList<LightOrganismItem> listExcludedGenome = new ArrayList<LightOrganismItem>();

	//private boolean wholeGenomeAsReferenceGeneSet = false;
	private RefGenoPanelAndListOrgaResu.EnumResultViewTypes viewTypeDesired = null;
	private int firstRowShownId = -1;
	
	private ArrayList<Integer> referenceOrigamiElementIdsThenStartPbthenStopPBLooped = new ArrayList<Integer>();
	
	public SearchItem(){
		setAlignmentParametersForSyntenyToStrict(false);
	}
	

	public ArrayList<LightGeneItem> getListReferenceGeneSet() {
		return listReferenceGeneSet;
	}

	public void setListReferenceGeneSet(ArrayList<LightGeneItem> listReferenceGeneSetSent) {
		if(listReferenceGeneSetSent == null){
			this.listReferenceGeneSet.clear();
		}else{
			this.listReferenceGeneSet = listReferenceGeneSetSent;
		}
	}
	
//	public void addGeneToListReferenceGeneSet(GeneItem geneItemSent){
//		this.listReferenceGeneSet.add(geneItemSent);
//	}
	
	public void addListToListReferenceGeneSet (List<LightGeneItem> listReferenceGeneSetSent) {
		this.listReferenceGeneSet.addAll(listReferenceGeneSetSent);
	}
	
	public boolean isExcludeAutomaticallyComputedResults() {
		return excludeAutomaticallyComputedResults;
	}

	public void setExcludeAutomaticallyComputedResults(
			boolean excludeAutomaticallyComputedResults) {
		this.excludeAutomaticallyComputedResults = excludeAutomaticallyComputedResults;
	}
//
//	public LightElementItem getReferenceElement() {
//		return referenceElement;
//	}
//
//	public void setReferenceElement(LightElementItem referenceElement) {
//		this.referenceElement = referenceElement;
//	}

	public OrganismItem getReferenceOrganism() {
		return referenceOrganism;
	}

	public void setReferenceOrganism(OrganismItem referenceOrganism) {
		this.referenceOrganism = referenceOrganism;
	}
	
	public ArrayList<LightOrganismItem> getListUserSelectedOrgaToBeResults() {
		return listUserSelectedOrgaToBeResults;
	}

	public void setListUserSelectedOrgaToBeResults(
			ArrayList<LightOrganismItem> listUserSelectedOrgaToBeResults) {
		if(listUserSelectedOrgaToBeResults == null){
			this.listUserSelectedOrgaToBeResults.clear();
		}else{
			this.listUserSelectedOrgaToBeResults = listUserSelectedOrgaToBeResults;
		}
	}
	
	public void addToListUserSelectedOrgaToBeResults(
			LightOrganismItem orgaItemSent) {
		if ( ! this.listUserSelectedOrgaToBeResults.contains(orgaItemSent)) {
			this.listUserSelectedOrgaToBeResults.add(orgaItemSent);
		}
	}

	/*
	public boolean isComingFromGenomeWalkUpstreamGeneSet() {
		return comingFromGenomeWalkUpstreamGeneSet;
	}

	public void setComingFromGenomeWalkUpstreamGeneSet(
			boolean comingFromGenomeWalkUpstreamGeneSet) {
		this.comingFromGenomeWalkUpstreamGeneSet = comingFromGenomeWalkUpstreamGeneSet;
	}
	*/
	
	public AlignmentParametersForSynteny getAlignmentParametersForSynteny() {
		return alignmentParametersForSynteny;
	}
	
	public void setAlignmentParametersForSynteny(AlignmentParametersForSynteny alignmentParametersForSyntenySent) {
		this.alignmentParametersForSynteny = alignmentParametersForSyntenySent;
	}
	
	public void setAlignmentParametersForSyntenyToStrict(
			boolean isStrict) {
		if(isStrict){
			//Window.alert("strict params selected");
			alignmentParametersForSynteny.setOrthoScore(1);
			alignmentParametersForSynteny.setHomoScore(-10000);
			alignmentParametersForSynteny.setMismatchPenalty(-10000);
			alignmentParametersForSynteny.setGapCreationPenalty(-10000);
			alignmentParametersForSynteny.setGapExtensionPenalty(-10000);
			alignmentParametersForSynteny.setMinAlignSize(1);
			alignmentParametersForSynteny.setMinScore(2);
			alignmentParametersForSynteny.setOrthologsIncluded(true);
		}else {
			//Window.alert("loose params selected");
			alignmentParametersForSynteny.setOrthoScore(4);
			alignmentParametersForSynteny.setHomoScore(2);
			alignmentParametersForSynteny.setMismatchPenalty(-4);
			alignmentParametersForSynteny.setGapCreationPenalty(-8);
			alignmentParametersForSynteny.setGapExtensionPenalty(-2);
			alignmentParametersForSynteny.setMinAlignSize(1);
			alignmentParametersForSynteny.setMinScore(8);
			alignmentParametersForSynteny.setOrthologsIncluded(true);
		}
	}

//	public void setGenomeWalkDownstreamRequest(boolean genomeWalkDownstreamRequest) {
//		this.genomeWalkDownstreamRequest = genomeWalkDownstreamRequest;
//	}
//
//	public boolean isGenomeWalkDownstreamRequest() {
//		return genomeWalkDownstreamRequest;
//	}

	public void setListExcludedGenome(ArrayList<LightOrganismItem> listExcludedGenome) {
		this.listExcludedGenome = listExcludedGenome;
	}

	public ArrayList<LightOrganismItem> getListExcludedGenome() {
		return listExcludedGenome;
	}

	/*
	public void setResultListSortScopeType(EnumResultListSortScopeType resultListSortScopeType) {
		this.resultListSortScopeType = resultListSortScopeType;
	}

	public EnumResultListSortScopeType getResultListSortScopeType() {
		return resultListSortScopeType;
	}*/



	public Enum_comparedGenomes_SortResultListBy_scope getSortResultListBy_scope() {
		return sortResultListBy_scope;
	}


	public void setSortResultListBy_scope(Enum_comparedGenomes_SortResultListBy_scope sortResultListBy_scope) {
		this.sortResultListBy_scope = sortResultListBy_scope;
	}


	public Enum_comparedGenomes_SortResultListBy_sortType getSortResultListBy_sortType() {
		return sortResultListBy_sortType;
	}


	public void setSortResultListBy_sortType(Enum_comparedGenomes_SortResultListBy_sortType sortResultListBy_sortType) {
		this.sortResultListBy_sortType = sortResultListBy_sortType;
	}


	public Enum_comparedGenomes_SortResultListBy_sortOrder getSortResultListBy_sortOrder() {
		return sortResultListBy_sortOrder;
	}


	public void setSortResultListBy_sortOrder(Enum_comparedGenomes_SortResultListBy_sortOrder sortResultListBy_sortOrder) {
		this.sortResultListBy_sortOrder = sortResultListBy_sortOrder;
	}


	public RefGenoPanelAndListOrgaResu.EnumResultViewTypes getViewTypeDesired() {
		return viewTypeDesired;
	}

	public void setViewTypeDesired(RefGenoPanelAndListOrgaResu.EnumResultViewTypes viewTypeDesired) {
		this.viewTypeDesired = viewTypeDesired;
	}

	public ArrayList<Integer> getReferenceOrigamiElementIdsThenStartPbthenStopPBLooped() {
		return referenceOrigamiElementIdsThenStartPbthenStopPBLooped;
	}

	public void setReferenceOrigamiElementIdsThenStartPbthenStopPBLooped(
			ArrayList<Integer> referenceOrigamiElementIdsThenStartPbthenStopPBLooped) {
		this.referenceOrigamiElementIdsThenStartPbthenStopPBLooped = referenceOrigamiElementIdsThenStartPbthenStopPBLooped;
	}



	public int getFirstRowShownId() {
		return firstRowShownId;
	}



	public void setFirstRowShownId(int firstRowShownId) {
		this.firstRowShownId = firstRowShownId;
	}


}
