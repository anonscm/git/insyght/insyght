package fr.inra.jouy.shared.pojos.applicationItems;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

public class GeneWidgetStyleItem implements IsSerializable{
	
	private GenomePanelItem associatedGenomePanelItem = null;
	private int bkgColor = -1;
	private int positionRowInGrid = -1;
	private int positionColInGrid = -1;
	//private int nextAvailableBkgColor = 0;
	private boolean styleAsOrtholog = false;
	
	public GeneWidgetStyleItem(int bkgColor) {
		super();
		this.bkgColor = bkgColor;
	}

	public GeneWidgetStyleItem() {
		
	}

	public void clearAllReferences(){
		if(associatedGenomePanelItem != null){
			//associatedGenomePanelItem.clearAllReferences();
			associatedGenomePanelItem = null;
		}
	}


	
	
	public GenomePanelItem getAssociatedGenomePanelItem() {
		return associatedGenomePanelItem;
	}

	public void setAssociatedGenomePanelItem(
			GenomePanelItem associatedGenomePanelItem) {
		this.associatedGenomePanelItem = associatedGenomePanelItem;
	}

	public int getBkgColor() {
		return bkgColor;
	}
	
	//-1 for grey default
	public void setBkgColor(int bkgColor) {
		this.bkgColor = bkgColor;
	}

	public int getPositionRowInGrid() {
		return positionRowInGrid;
	}

	public void setPositionRowInGrid(int positionRowInGrid) {
		this.positionRowInGrid = positionRowInGrid;
	}

	public int getPositionColInGrid() {
		return positionColInGrid;
	}

	public void setPositionColInGrid(int positionColInGrid) {
		this.positionColInGrid = positionColInGrid;
	}
	
//	
//	public int getNextAvailableBkgColor() {
//		nextAvailableBkgColor++;
//		if(nextAvailableBkgColor > 4){
//			nextAvailableBkgColor = nextAvailableBkgColor%5;
//		}
//		return nextAvailableBkgColor;
//	}

	public void setStyleAsOrtholog(boolean styleAsOrtholog) {
		this.styleAsOrtholog = styleAsOrtholog;
	}

	public boolean isStyleAsOrtholog() {
		return styleAsOrtholog;
	}

	
}
