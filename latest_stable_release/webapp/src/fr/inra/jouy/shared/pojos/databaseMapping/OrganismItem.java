package fr.inra.jouy.shared.pojos.databaseMapping;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.IsSerializable;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.interfaceIs.IsStringifyToJSON;

public class OrganismItem extends LightOrganismItem implements IsSerializable, IsStringifyToJSON {


//	//TODO add more info on orga : list serotype etc...
	
    private ArrayList<LightElementItem> listAllLightElementItem = new ArrayList<LightElementItem>();
    private TaxoItem associatedTaxoItem = null;
    
    public OrganismItem() {
    }
    
	public OrganismItem(OrganismItem orgaItemSent) {
		super((LightOrganismItem)orgaItemSent);
		setListAllLightElementItem(orgaItemSent.getListAllLightElementItem());
		//setLineageInCellBrowserByIdx(orgaItemSent.getLineageInCellBrowserByIdx());
		//setScore(orgaItemSent.getScore());
		setAssociatedTaxoItem(orgaItemSent.getAssociatedTaxoItem());
    }
	public OrganismItem(LightOrganismItem loSent) {
		super(loSent);
	}
	
	public ArrayList<LightElementItem> getListAllLightElementItem() {
		return listAllLightElementItem;
	}
	
	public ArrayList<String> getListAllLightElementItemAsStringTypeAccnum() {
		
		ArrayList<String> l = new ArrayList<String>();
		
		for(int i=0;i<listAllLightElementItem.size();i++){
			l.add(
				//listAllLightElementItem.get(i).getType()+" ("+
				listAllLightElementItem.get(i).getAccession()
				//+")"
			);
		}
		
		return l;
	}

	public void setListAllLightElementItem(
			ArrayList<LightElementItem> listAllLightElementItem) {
		this.listAllLightElementItem = listAllLightElementItem;
	}

//	public ArrayList<Integer> getLineageInCellBrowserByIdx() {
//		return lineageInCellBrowserByIdx;
//	}
//
//	public void setLineageInCellBrowserByIdx(ArrayList<Integer> lineageInCellBrowser) {
//		this.lineageInCellBrowserByIdx = lineageInCellBrowser;
//	}

	
	public String getFullName(){
		String lightFullNameAsString = lightFullNameAsString();
		String toAddToFullName = "[ ";
		boolean firstIter = true;
		for (String accnumIT : getListAllLightElementItemAsStringTypeAccnum()) {
			if (!firstIter) {
				toAddToFullName += ", ";
			}
			if(toAddToFullName.length() > 50){
				toAddToFullName += "etc.";
				break;
			} else {
				toAddToFullName += accnumIT;
			}
			firstIter = false;
		}
		//String toAddToFullName = getListAllLightElementItemAsStringTypeAccnum().toString();
		toAddToFullName += " ]";
		return lightFullNameAsString+" "+toAddToFullName;
	}


	public TaxoItem getAssociatedTaxoItem() {
		return associatedTaxoItem;
	}

	public void setAssociatedTaxoItem(TaxoItem associatedTaxoItem) {
		this.associatedTaxoItem = associatedTaxoItem;
	}
	
	@Override
	public String stringifyToJSON(boolean encloseWithCurlyBraces, boolean propagateToParentClassViaSuper) throws Exception {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String field2String = "";
		if(propagateToParentClassViaSuper){
			field2String += super.stringifyToJSON(false, propagateToParentClassViaSuper);
		}
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringAlIsConvertibleToJSONObject(
				"listAllLightElementItem",
				listAllLightElementItem,
				field2String.isEmpty(),
				propagateToParentClassViaSuper,
				false
				);
		if(getAssociatedTaxoItem() != null){
			if( ! field2String.isEmpty()){
				field2String += ",";
			}
			field2String += "\"associatedTaxoItem\":\"NcbiTaxoId "+getAssociatedTaxoItem().getNcbiTaxoId()+"\"";
		}
		if( encloseWithCurlyBraces){
			return "{"+field2String+"}";
		} else {
			return field2String;
		}
	}

//	@Override
//	public void fromJSON(String JSON) throws Exception {
//		//"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]
//	    for (MatchResult matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON); matcher != null; matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON)) {
//			String keyIT = matcher.getGroup(1);
//			String valueIT = matcher.getGroup(2);
//			boolean mappingSuccessful = mapKeyUnknowTypeValueToObjectField(keyIT,valueIT);
//			if( ! mappingSuccessful){
//				throw new Exception("The key "+keyIT+" (value = "+valueIT+") has not been mapped to any attribute");
//			}
//	    }
//	}
	
}
