package fr.inra.jouy.shared.pojos.applicationItems;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;
import com.google.gwt.user.client.rpc.IsSerializable;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.shared.pojos.applicationItems.RefGenoPanelAndListOrgaResu.EnumResultViewTypes;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSGeneHomoItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQSSyntItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSElemItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGeneInserItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSGenoRegiInserItem;
import fr.inra.jouy.shared.pojos.symbols.HolderAbsoluteProportionItem;
import fr.inra.jouy.shared.pojos.symbols.SuperHoldAbsoPropItem;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem.QSEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem.QEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem.SEnumBlockType;

public class GenomePanelItem extends LightOrganismItem implements IsSerializable {

	//TODO extend OrganismItem to get access to list accnum ; idem pour liste of orga in the result list
	
	public enum GpiSizeState implements IsSerializable {
		INTERMEDIATE, MAX, MIN
	}

	private GpiSizeState sizeState = null; // can be INTERMEDIATE, MAX or MIN
	private int maxSizePx = 0;
//	private int rowCount = -1;
//	private int colCount = -1;
	//private int resultPosition = -1;
	private boolean isReferenceGenome = false;
//	private GeneItem associatedGeneItemGrid[][] = null; // = new GeneItem[][];
//	private GeneWidgetStyleItem associatedGeneWidgetStyleItemGrid[][] = null;
	//private int score = -1;
	
	//private ArrayList<ArrayList<HolderAbsoluteProportionItem>> fullAssociatedListAbsoluteProportionItemToDisplay = new ArrayList<ArrayList<HolderAbsoluteProportionItem>>();
	//private ArrayList<ArrayList<HolderAbsoluteProportionItem>> partialAssociatedListAbsoluteProportionItemToDisplay = new ArrayList<ArrayList<HolderAbsoluteProportionItem>>();
	//private Integer arrayDisplayedIndexAtSpecifiedAPIToDisplay[] = null;
	//private ArrayList<SuperHolderAbsoluteProportionItem> fullAssociatedListAbsoluteProportionItemToDisplay = new ArrayList<SuperHolderAbsoluteProportionItem>();
	private ArrayList<SuperHoldAbsoPropItem> partialAssociatedListAbsoluteProportionItemToDisplay = new ArrayList<SuperHoldAbsoPropItem>();
	
	private ArrayList<AbsoPropQElemItem> listAbsoluteProportionElementItemQ = new ArrayList<AbsoPropQElemItem>();
	private ArrayList<AbsoPropSElemItem> listAbsoluteProportionElementItemS = new ArrayList<AbsoPropSElemItem>();
	private ArrayList<SuperHoldAbsoPropItem> listAbsoluteProportionSyntenyView = new ArrayList<SuperHoldAbsoPropItem>();
	private ArrayList<SuperHoldAbsoPropItem> listAbsoluteProportionHomologBrowsingView = new ArrayList<SuperHoldAbsoPropItem>();
	
	private boolean isGpiLoadingAdditionalData = false;
	private boolean isGpiErrorWhileLoadingAdditionalData = false;
	private ArrayList<AbsoPropQSGeneHomoItem> tmpListAPQSGeneHomo = new ArrayList<AbsoPropQSGeneHomoItem>();
	//private ArrayList<AbsoluteProportionQComparableItem> tmpListAPQSGeneHomoSortedByS = new ArrayList<AbsoluteProportionQComparableItem>();
	private ArrayList<AbsoPropQGeneInserItem> tmpListAPQGeneInserItem = new ArrayList<AbsoPropQGeneInserItem>();
	private ArrayList<AbsoPropSGeneInserItem> tmpListAPSGeneInserItem = new ArrayList<AbsoPropSGeneInserItem>();
	
	
	private double percentSyntenyShowStartQGenomeSyntenyView = 0;
	private double percentSyntenyShowStopQGenomeSyntenyView = 1;
	private double percentSyntenyShowStartSGenomeSyntenyView = 0;
	private double percentSyntenyShowStopSGenomeSyntenyView = 1;
	private int idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedSyntenyView = -1;
	private double percentSyntenyShowStartQGenomeHomologBrowsingView = 0;
	private double percentSyntenyShowStopQGenomeHomologBrowsingView = 1;
	private double percentSyntenyShowStartSGenomeHomologBrowsingView = 0;
	private double percentSyntenyShowStopSGenomeHomologBrowsingView = 1;
	private int idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedHomologBrowsingView = -1;

	
	private int lookUpForGeneWithQStart_orthoTable = -1;
	private int lookUpForGeneWithQStart_genomicOrga = -1;
	private int lookUpForGeneWithQStop_orthoTable = -1;
	private int lookUpForGeneWithQStop_genomicOrga = -1;
	private int lookUpForGeneOnQElementId_orthoTable = -1;
	private int lookUpForGeneOnQElementId_genomicOrga = -1;
	
	public GenomePanelItem(LightOrganismItem lightOrgaItemSent) {
		super(lightOrgaItemSent);
	}

	
	public GenomePanelItem() {

	}


	private void recalculateHAPIIndexInFullArrayAndPartialIfNeeded(
			ArrayList<SuperHoldAbsoPropItem> alAlHAPISent) {
		
		for(int i=0;i<alAlHAPISent.size();i++){
			for(int j=0;j<alAlHAPISent.get(i).getListHAPI().size();j++){
				HolderAbsoluteProportionItem hapiIT = alAlHAPISent.get(i).getListHAPI().get(j);
				hapiIT.setIndexInFullArray(i);
			}
		}
		
		boolean qHasChangedZoomWise = false;
		boolean sHasChangedZoomWise = false;
		if(getPercentSyntenyShowStartQGenome() != 0 || getPercentSyntenyShowStopQGenome() != 1){
			qHasChangedZoomWise = true;
		}
		
		if(getPercentSyntenyShowStartSGenome() != 0 || getPercentSyntenyShowStopSGenome() != 1){
			sHasChangedZoomWise = true;
		}
		
		
		if(qHasChangedZoomWise || sHasChangedZoomWise){
			this.partialAssociatedListAbsoluteProportionItemToDisplay.clear();
			//zoom q and s
//			if(qHasChangedZoomWise){
//				calculatePartialAssociatedlistAbsoluteProportionItem(true, false);
//			}
//			if(qHasChangedZoomWise){
//				calculatePartialAssociatedlistAbsoluteProportionItem(false, false);
//			}
			calculatePartialAssociatedlistAbsoluteProportionItem(false);
			
		}

	}
//
//	private void turnSContainingFeaturesIntoCutSStartAndStop(
//			ArrayList<AbsoluteProportionSComparableItem> tmpfullAssociatedlistAbsoluteProportionSyntenyItemS) {
//		
//		ArrayList<AbsoluteProportionSComparableItem> cutStopToAddAfterward = new ArrayList<AbsoluteProportionSComparableItem>();
//		for(int i=0;i<tmpfullAssociatedlistAbsoluteProportionSyntenyItemS.size()-1;i++){
//			AbsoluteProportionSComparableItem apscIT = tmpfullAssociatedlistAbsoluteProportionSyntenyItemS.get(i);
//			AbsoluteProportionSComparableItem apscITNext = tmpfullAssociatedlistAbsoluteProportionSyntenyItemS.get(i+1);
//			double sPercentStopIT = -1;
//        	if(apscIT instanceof AbsoluteProportionSComparableSSpanItem){
//        		sPercentStopIT = ((AbsoluteProportionSComparableSSpanItem)apscIT).getsPercentStop();
//        	}
//        	if(sPercentStopIT>0){
//        		if(sPercentStopIT > apscITNext.getsPercentStart()){
//        			//cut apscIT into start and stop cut and add in cutStopToAddAfterward
//        			if(apscIT instanceof AbsoluteProportionSGeneInsertionItem){
//        				
//        				AbsoluteProportionSGeneInsertionItem newStopApscIT = new AbsoluteProportionSGeneInsertionItem((AbsoluteProportionSGeneInsertionItem)apscIT);
//        				newStopApscIT.setSEnumBlockType(SEnumBlockType.S_CUT_STOP_S_INSERTION);
//        				cutStopToAddAfterward.add(newStopApscIT);
//        				((AbsoluteProportionSGeneInsertionItem)apscIT).setSEnumBlockType(SEnumBlockType.S_CUT_START_S_INSERTION);
//
//                	}else if(apscIT instanceof AbsoluteProportionSGenomicRegionInsertionItem){
//        				
//        				//AbsoluteProportionQGenomicRegionInsertionItem newStopApqcIT = ((AbsoluteProportionQGenomicRegionInsertionItem)apqcIT).copyThisAbsoluteProportionQGenomicRegionInsertionItem();
//        				AbsoluteProportionSGenomicRegionInsertionItem newStopApscIT = new AbsoluteProportionSGenomicRegionInsertionItem((AbsoluteProportionSGenomicRegionInsertionItem)apscIT);
//        				newStopApscIT.setSEnumBlockType(SEnumBlockType.S_CUT_STOP_S_INSERTION);
//        				cutStopToAddAfterward.add(newStopApscIT);
//        				((AbsoluteProportionSGenomicRegionInsertionItem)apscIT).setSEnumBlockType(SEnumBlockType.S_CUT_START_S_INSERTION);
//
//        			}else if(apscIT instanceof AbsoluteProportionSElementItem){
//        				
//        				//AbsoluteProportionQElementItem newStopApqcIT = ((AbsoluteProportionQElementItem)apqcIT).copyThisAbsoluteProportionQElementItem();
//        				AbsoluteProportionSElementItem newStopApscIT = new AbsoluteProportionSElementItem((AbsoluteProportionSElementItem)apscIT);
//        				newStopApscIT.setSEnumBlockType(SEnumBlockType.S_CUT_STOP_S_INSERTION);
//        				cutStopToAddAfterward.add(newStopApscIT);
//        				((AbsoluteProportionSElementItem)apscIT).setSEnumBlockType(SEnumBlockType.S_CUT_START_S_INSERTION);
//        				
//        			}else {
//                		System.err.println("Error in turnSContainingFeaturesIntoCutSStartAndStop : apscIT not instanceOf defined cases");
//                		Window.alert("Error in turnSContainingFeaturesIntoCutSStartAndStop : apscIT not instanceOf defined cases");
//                	}
//        		}
//        	}
//		}
//		if(!cutStopToAddAfterward.isEmpty()){
//			tmpfullAssociatedlistAbsoluteProportionSyntenyItemS.addAll(cutStopToAddAfterward);
//			Collections.sort(tmpfullAssociatedlistAbsoluteProportionSyntenyItemS);
//		}
//		
//		
//	}
//
//	
	
//	
//	private void turnQContainingFeaturesIntoCutQStartAndStop(
//			ArrayList<AbsoluteProportionQComparableItem> tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS) {
//		
//		ArrayList<AbsoluteProportionQComparableItem> cutStopToAddAfterward = new ArrayList<AbsoluteProportionQComparableItem>();
//		for(int i=0;i<tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS.size()-1;i++){
//			AbsoluteProportionQComparableItem apqcIT = tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS.get(i);
//			
//			if(apqcIT instanceof AbsoPropQSGeneHomoItem){
//				if(!((AbsoPropQSGeneHomoItem)apqcIT).getTmpListOfOtherQSHomologiesForTheQGene().isEmpty()){
//					continue;
//				}
//			}
//			
//			AbsoluteProportionQComparableItem apqcITNext = tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS.get(i+1);
//			double qPercentStopIT = -1;
//        	if(apqcIT instanceof AbsoluteProportionQComparableQSpanItem){
//        		qPercentStopIT = ((AbsoluteProportionQComparableQSpanItem)apqcIT).getqPercentStop();
//        	}
//        	if(apqcIT instanceof AbsoluteProportionQComparableQSSpanItem){
//        		qPercentStopIT = ((AbsoluteProportionQComparableQSSpanItem)apqcIT).getqPercentStop();
//        	}
//        	if(qPercentStopIT>0){
//        		if(qPercentStopIT > apqcITNext.getqPercentStart()){
//        			//cut apqcIT into start and stop cut and add in cutStopToAddAfterward
//        			if(apqcIT instanceof AbsoluteProportionQGeneInsertionItem){
//        				AbsoluteProportionQGeneInsertionItem newStopApqcIT = new AbsoluteProportionQGeneInsertionItem((AbsoluteProportionQGeneInsertionItem)apqcIT);
//        				newStopApqcIT.setqEnumBlockType(QEnumBlockType.Q_CUT_STOP_Q_INSERTION);
//        				cutStopToAddAfterward.add(newStopApqcIT);
//        				((AbsoluteProportionQGeneInsertionItem)apqcIT).setqEnumBlockType(QEnumBlockType.Q_CUT_START_Q_INSERTION);
//        			
//        			
//        			}else if(apqcIT instanceof AbsoluteProportionQGenomicRegionInsertionItem){
//        				
//        				//AbsoluteProportionQGenomicRegionInsertionItem newStopApqcIT = ((AbsoluteProportionQGenomicRegionInsertionItem)apqcIT).copyThisAbsoluteProportionQGenomicRegionInsertionItem();
//        				AbsoluteProportionQGenomicRegionInsertionItem newStopApqcIT = new AbsoluteProportionQGenomicRegionInsertionItem((AbsoluteProportionQGenomicRegionInsertionItem)apqcIT);
//        				newStopApqcIT.setqEnumBlockType(QEnumBlockType.Q_CUT_STOP_Q_INSERTION);
//        				cutStopToAddAfterward.add(newStopApqcIT);
//        				((AbsoluteProportionQGenomicRegionInsertionItem)apqcIT).setqEnumBlockType(QEnumBlockType.Q_CUT_START_Q_INSERTION);
//        			
//        			}else if(apqcIT instanceof AbsoluteProportionQElementItem){
//        				
//        				//AbsoluteProportionQElementItem newStopApqcIT = ((AbsoluteProportionQElementItem)apqcIT).copyThisAbsoluteProportionQElementItem();
//        				AbsoluteProportionQElementItem newStopApqcIT = new AbsoluteProportionQElementItem((AbsoluteProportionQElementItem)apqcIT);
//        				newStopApqcIT.setqEnumBlockType(QEnumBlockType.Q_CUT_STOP_Q_INSERTION);
//        				cutStopToAddAfterward.add(newStopApqcIT);
//        				
//        				((AbsoluteProportionQElementItem)apqcIT).setqEnumBlockType(QEnumBlockType.Q_CUT_START_Q_INSERTION);
//        				
//        			}else if(apqcIT instanceof AbsoPropQSGeneHomoItem){
//        				if(((AbsoPropQSGeneHomoItem)apqcIT).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
//            				
//            				//AbsoPropQSSyntItem newStopApqcIT = ((AbsoPropQSSyntItem)apqcIT).copyThisAbsoPropQSSyntItem();
//        					AbsoPropQSGeneHomoItem newStopApqcIT = new AbsoPropQSGeneHomoItem((AbsoPropQSGeneHomoItem)apqcIT);
//            				newStopApqcIT.setQsEnumBlockType(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK);
//            				cutStopToAddAfterward.add(newStopApqcIT);
//            				((AbsoPropQSGeneHomoItem)apqcIT).setQsEnumBlockType(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK);
//                		}else if(((AbsoPropQSGeneHomoItem)apqcIT).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
//            				
//                			AbsoPropQSGeneHomoItem newStopApqcIT = new AbsoPropQSGeneHomoItem((AbsoPropQSGeneHomoItem)apqcIT);
//            				newStopApqcIT.setQsEnumBlockType(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK);
//            				cutStopToAddAfterward.add(newStopApqcIT);
//            				((AbsoPropQSGeneHomoItem)apqcIT).setQsEnumBlockType(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK);
//                		}else{
//                    		System.err.println("Error in turnQContainingFeaturesIntoCutQStartAndStop instanceof AbsoPropQSGeneHomoItem : apqcIT.getQsEnumBlockType() not in defined cases");
//                    		Window.alert("Error in turnQContainingFeaturesIntoCutQStartAndStop instanceof AbsoPropQSGeneHomoItem : apqcIT.getQsEnumBlockType() not defined cases");
//                		}
//        				
//                	}else if(apqcIT instanceof AbsoPropQSSyntItem){
//        				
//                		if(((AbsoPropQSSyntItem)apqcIT).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
//            				
//            				//AbsoPropQSSyntItem newStopApqcIT = ((AbsoPropQSSyntItem)apqcIT).copyThisAbsoPropQSSyntItem();
//               				AbsoPropQSSyntItem newStopApqcIT = new AbsoPropQSSyntItem((AbsoPropQSSyntItem)apqcIT);
//            				newStopApqcIT.setQsEnumBlockType(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK);
//            				cutStopToAddAfterward.add(newStopApqcIT);
//            				((AbsoPropQSSyntItem)apqcIT).setQsEnumBlockType(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK);
//                		}else if(((AbsoPropQSSyntItem)apqcIT).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
//            				
//            				AbsoPropQSSyntItem newStopApqcIT = new AbsoPropQSSyntItem((AbsoPropQSSyntItem)apqcIT);
//            				newStopApqcIT.setQsEnumBlockType(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK);
//            				cutStopToAddAfterward.add(newStopApqcIT);
//            				((AbsoPropQSSyntItem)apqcIT).setQsEnumBlockType(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK);
//                		}else{
//                    		System.err.println("Error in turnQContainingFeaturesIntoCutQStartAndStop instanceof AbsoPropQSSyntItem : apqcIT.getQsEnumBlockType() not in defined cases");
//                    		Window.alert("Error in turnQContainingFeaturesIntoCutQStartAndStop instanceof AbsoPropQSSyntItem : apqcIT.getQsEnumBlockType() not defined cases");
//                		}
//                		
//        			}else{
//                		System.err.println("Error in turnQContainingFeaturesIntoCutQStartAndStop : apqcIT not instanceOf defined cases");
//                		Window.alert("Error in turnQContainingFeaturesIntoCutQStartAndStop : apqcIT not instanceOf defined cases");
//                	}
//        			
//        		}
//        	}
//		}
//		if(!cutStopToAddAfterward.isEmpty()){
//			tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS.addAll(cutStopToAddAfterward);
//			Collections.sort(tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS);
//		}
//		
//	}
//
	
//	
//	private void insertQItemsIntoListDisplayedItems(
//			ArrayList<AbsoluteProportionQComparableItem> tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS,
//			ArrayList<SuperHolderAbsoluteProportionItem> alAlHAPISent) {
//		
//		//fill this.fullAssociatedlistAbsoluteProportionSyntenyItem
//		//this.fullAssociatedListAbsoluteProportionItemToDisplay.clear();
//		alAlHAPISent.clear();
//		
//		for(int i=0;i<tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS.size();i++){
//			AbsoluteProportionQComparableItem apqcIT = tmpfullAssociatedlistAbsoluteProportionSyntenyItemQOrQS.get(i);
//			HolderAbsoluteProportionItem newAPS = new HolderAbsoluteProportionItem(i);
//			if (apqcIT instanceof AbsoPropQSGeneHomoItem){
//				newAPS.setAbsoluteProportionQComparableQSSpanItem((AbsoPropQSGeneHomoItem)apqcIT);
//			}else if(apqcIT instanceof AbsoPropQSSyntItem){
//				newAPS.setAbsoluteProportionQComparableQSSpanItem((AbsoPropQSSyntItem)apqcIT);
//			}else if (apqcIT instanceof AbsoluteProportionQGeneInsertionItem){
//				newAPS.setAbsoluteProportionQComparableQSpanItem((AbsoluteProportionQGeneInsertionItem)apqcIT);
//			}else if (apqcIT instanceof AbsoluteProportionQGenomicRegionInsertionItem){
//				newAPS.setAbsoluteProportionQComparableQSpanItem((AbsoluteProportionQGenomicRegionInsertionItem)apqcIT);
//			}else if (apqcIT instanceof AbsoluteProportionQElementItem){
//				newAPS.setAbsoluteProportionQComparableQSpanItem((AbsoluteProportionQElementItem)apqcIT);
//			}else{
//				System.err.println("unrecognized class in sortAndInsertCorrectlyAbsoluteProportionSyntenyItemInFullAssociatedlistAbsoluteProportionSyntenyItem");
//				Window.alert("unrecognized class in sortAndInsertCorrectlyAbsoluteProportionSyntenyItemInFullAssociatedlistAbsoluteProportionSyntenyItem");
//			}
//			SuperHolderAbsoluteProportionItem alHAPI = new SuperHolderAbsoluteProportionItem();
//			alHAPI.getListHAPI().add(newAPS);
//			if(apqcIT instanceof AbsoPropQSGeneHomoItem){
//				//deal with multi dimmension in array
//				if(!((AbsoPropQSGeneHomoItem)apqcIT).getTmpListOfOtherQSHomologiesForTheQGene().isEmpty()){
//					for(int j=0;j<((AbsoPropQSGeneHomoItem)apqcIT).getTmpListOfOtherQSHomologiesForTheQGene().size();j++){
//						AbsoPropQSGeneHomoItem otherAPQSGHI = ((AbsoPropQSGeneHomoItem)apqcIT).getTmpListOfOtherQSHomologiesForTheQGene().get(j);
//						HolderAbsoluteProportionItem newOtherAPS = new HolderAbsoluteProportionItem(i);
//						newOtherAPS.setAbsoluteProportionQComparableQSSpanItem(otherAPQSGHI);
//						alHAPI.getListHAPI().add(newOtherAPS);
//					}
//					((AbsoPropQSGeneHomoItem)apqcIT).getTmpListOfOtherQSHomologiesForTheQGene().clear();
//				}
//			}
//			//this.fullAssociatedListAbsoluteProportionItemToDisplay.add(alHAPI);
//			alAlHAPISent.add(alHAPI);
//		}
//	}
//
//	
	
//	public GenomePanelItem(LightElementItem lightEletItemSent) {
//		this();
//		setElementId(lightEletItemSent.getElementId());
//		setType(lightEletItemSent.getType());
//		setAccession(lightEletItemSent.getAccession());
//		setSize(lightEletItemSent.getSize());
//		setDescription(lightEletItemSent.getDescription());
//		setOrganismId(lightEletItemSent.getOrganismId());
//		setSpecies(lightEletItemSent.getSpecies());
//		setStrain(lightEletItemSent.getStrain());
//		setSubstrain(lightEletItemSent.getSubstrain());
//		setTaxonId(lightEletItemSent.getTaxonId());
//
//	}


//	public void createAssociatedGeneItemGrid(int colRowSent, int colCountSent) {
//		this.associatedGeneItemGrid = new GeneItem[colRowSent][colCountSent];
//	}
//
//	public void createAssociatedGeneWidgetStyleItemGrid(int colRowSent,
//			int colCountSent) {
//		this.associatedGeneWidgetStyleItemGrid = new GeneWidgetStyleItem[colRowSent][colCountSent];
//	}
//
//	public GeneItem getGeneItemAtGridPosition(int rowSent, int colSent) {
//		return associatedGeneItemGrid[rowSent][colSent];
//	}
//
//	public void setGeneItemAtGridPosition(int rowSent, int colSent,
//			GeneItem geneItemSent) {
//		associatedGeneItemGrid[rowSent][colSent] = geneItemSent;
//	}
//
//	public GeneWidgetStyleItem getGeneWidgetStyleItemAtGridPosition(
//			int rowSent, int colSent) {
//		return associatedGeneWidgetStyleItemGrid[rowSent][colSent];
//	}
//
//	public void setGeneWidgetStyleItemAtGridPosition(int rowSent, int colSent,
//			GeneWidgetStyleItem geneWidgetStyleItemSent) {
//		associatedGeneWidgetStyleItemGrid[rowSent][colSent] = geneWidgetStyleItemSent;
//	}
//
//	public void clearAllReferences() {
//		for (int i = 0; i < rowCount; i++) {
//			for (int j = 0; j < SharedAppParams.MAX_NUMBER_IN_GENE_SET_ALLOWED; j++) {
//				// System.out.println("rowCount "+rowCount);
//				// System.out.println("DisplayManager.CURR_NUMBER_DISPLAYED_GENE_SET"+DisplayManager.CURR_NUMBER_DISPLAYED_GENE_SET);
//				GeneItem geneItemToDW = (GeneItem) associatedGeneItemGrid[i][j];
//				if (geneItemToDW != null) {
//					geneItemToDW.clearAllReferences();
//					associatedGeneItemGrid[i][j] = null;
//				}
//
//				GeneWidgetStyleItem geneWidgetStyleItemToDW = (GeneWidgetStyleItem) associatedGeneWidgetStyleItemGrid[i][j];
//				if (geneWidgetStyleItemToDW != null) {
//					geneWidgetStyleItemToDW.clearAllReferences();
//					associatedGeneWidgetStyleItemGrid[i][j] = null;
//				}
//			}
//		}
//	}

	
	
	public GpiSizeState getSizeState() {
		return sizeState;
	}

	public void setSizeState(GpiSizeState sizeState) {
		this.sizeState = sizeState;
	}

	public int getMaxSizePx() {
		return maxSizePx;
	}

	public void setMaxSizePx(int maxSizePx) {
		this.maxSizePx = maxSizePx;
	}

	// public LightElementItem getAssociatedElementItem() {
	// return associatedLightElementItem;
	// }
	// public void setAssociatedElementItem(LightElementItem
	// associatedElementItem) {
	// this.associatedLightElementItem = associatedElementItem;
	// }
//	public int getRowCount() {
//		return rowCount;
//	}
//
//	public void setRowCount(int rowCount) {
//		this.rowCount = rowCount;
//	}
//
//	public int getColCount() {
//		return colCount;
//	}
//
//	public void setColCount(int colCount) {
//		this.colCount = colCount;
//	}

	/*
	public int getResultPosition() {
		return resultPosition;
	}

	public void setResultPosition(int resultPosition) {
		this.resultPosition = resultPosition;
	}*/

	public boolean isReferenceGenome() {
		return isReferenceGenome;
	}

	public void setReferenceGenome(boolean isReferenceGenome) {
		this.isReferenceGenome = isReferenceGenome;
	}

//	public GeneItem[][] getAssociatedGeneItemGrid() {
//		return associatedGeneItemGrid;
//	}
//
//	public void setAssociatedGeneItemGrid(GeneItem[][] associatedGeneItemGrid) {
//		this.associatedGeneItemGrid = associatedGeneItemGrid;
//	}
//
//	public GeneWidgetStyleItem[][] getAssociatedGeneWidgetStyleItemGrid() {
//		return associatedGeneWidgetStyleItemGrid;
//	}
//
//	public void setAssociatedGeneWidgetStyleItemGrid(
//			GeneWidgetStyleItem[][] associatedGeneWidgetStyleItemGrid) {
//		this.associatedGeneWidgetStyleItemGrid = associatedGeneWidgetStyleItemGrid;
//	}

//	public int getScore() {
//		return score;
//	}
//
//	public void setScore(int score) {
//		this.score = score;
//	}

	public void setFullAssociatedlistAbsoluteProportionItemToDisplay(
			ArrayList<SuperHoldAbsoPropItem> fullAssociatedlistAbsoluteProportionItem) {
		//this.fullAssociatedListAbsoluteProportionItemToDisplay = fullAssociatedlistAbsoluteProportionItem;
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			this.listAbsoluteProportionHomologBrowsingView = fullAssociatedlistAbsoluteProportionItem;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			this.listAbsoluteProportionSyntenyView = fullAssociatedlistAbsoluteProportionItem;
		}else{
			System.err.println("Error in setFullAssociatedlistAbsoluteProportionItemToDisplay : unrecognized type");
		}
	}

	public ArrayList<SuperHoldAbsoPropItem> getFullAssociatedlistAbsoluteProportionItemToDisplay(){
		//return this.fullAssociatedListAbsoluteProportionItemToDisplay;
		//System.out.println(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult());
		//System.out.println(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewType());
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			return this.listAbsoluteProportionHomologBrowsingView;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			return this.listAbsoluteProportionSyntenyView;
		}else{
			System.err.println("Error in getFullAssociatedlistAbsoluteProportionItemToDisplay : unrecognized type "+Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght());
			return null;
		}
	}
	
	public void setPartialListAbsoluteProportionItemToDisplay(
			ArrayList<SuperHoldAbsoPropItem> associatedlistAbsoluteProportionItemSent) {
		if(associatedlistAbsoluteProportionItemSent == null){
			associatedlistAbsoluteProportionItemSent = new ArrayList<SuperHoldAbsoPropItem>();
		}
		this.partialAssociatedListAbsoluteProportionItemToDisplay = associatedlistAbsoluteProportionItemSent;
	}

	public ArrayList<SuperHoldAbsoPropItem> getPartialListAbsoluteProportionItemToDisplay() {
		return this.partialAssociatedListAbsoluteProportionItemToDisplay;
	}
	
	public ArrayList<SuperHoldAbsoPropItem> getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly() {
		// System.out.println("return full size : "+fullAssociatedlistAbsoluteProportionItem.size());
		// return fullAssociatedlistAbsoluteProportionItem;
		if (getPercentSyntenyShowStartQGenome() == 0
				&& getPercentSyntenyShowStopQGenome() == 1
				&& getPercentSyntenyShowStartSGenome() == 0
				&& getPercentSyntenyShowStopSGenome() == 1) {
			return getFullAssociatedlistAbsoluteProportionItemToDisplay();
		}else if(getPartialListAbsoluteProportionItemToDisplay() == null){
			setPartialListAbsoluteProportionItemToDisplay(new ArrayList<SuperHoldAbsoPropItem>());
			calculatePartialAssociatedlistAbsoluteProportionItem(false);
			return getPartialListAbsoluteProportionItemToDisplay();
		}else if(getPartialListAbsoluteProportionItemToDisplay().isEmpty()){
			return getFullAssociatedlistAbsoluteProportionItemToDisplay();
		}else{
			return getPartialListAbsoluteProportionItemToDisplay();
		}
	}

	public int getCurrDisplayedIndexAtSpecifiedAPIToDisplayIndex(int indexSent){
		
		return getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(indexSent).getIdxToDisplayInListHAPI();
		
	}
	
	public void setCurrDisplayIndexAtSpecifiedAPIToDisplayIndex(int indexSentAPIToDisplay, int indexSentCurrDisplayIndexAtSpecifiedAPIT){
		
		if(getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly() == getFullAssociatedlistAbsoluteProportionItemToDisplay()){
			//update fullAssociatedListAbsoluteProportionItemToDisplay only
			getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexSentAPIToDisplay).setIdxToDisplayInListHAPI(indexSentCurrDisplayIndexAtSpecifiedAPIT);
		}else{
			//update both fullAssociatedListAbsoluteProportionItemToDisplay and partialAssociatedListAbsoluteProportionItemToDisplay
			this.partialAssociatedListAbsoluteProportionItemToDisplay.get(indexSentAPIToDisplay).setIdxToDisplayInListHAPI(indexSentCurrDisplayIndexAtSpecifiedAPIT);
			getFullAssociatedlistAbsoluteProportionItemToDisplay().get(this.partialAssociatedListAbsoluteProportionItemToDisplay.get(indexSentAPIToDisplay).getListHAPI().get(0).getIndexInFullArray()).setIdxToDisplayInListHAPI(indexSentCurrDisplayIndexAtSpecifiedAPIT);
		}
		
		//		if(arrayDisplayedIndexAtSpecifiedAPIToDisplay == null){
//			arrayDisplayedIndexAtSpecifiedAPIToDisplay = new Integer[getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()];
//		}
//		arrayDisplayedIndexAtSpecifiedAPIToDisplay[indexSentAPIToDisplay] = indexSentCurrDisplayIndexAtSpecifiedAPIT;
	}
	
	public void setListAbsoluteProportionElementItemQ(
			ArrayList<AbsoPropQElemItem> listAbsoluteProportionElementItemQSent) {
		this.listAbsoluteProportionElementItemQ = listAbsoluteProportionElementItemQSent;
	}

	public ArrayList<AbsoPropQElemItem> getListAbsoluteProportionElementItemQ() {
		return listAbsoluteProportionElementItemQ;
	}

	public void setListAbsoluteProportionElementItemS(
			ArrayList<AbsoPropSElemItem> listAbsoluteProportionElementItemSSent) {
		this.listAbsoluteProportionElementItemS = listAbsoluteProportionElementItemSSent;
	}

	public ArrayList<AbsoPropSElemItem> getListAbsoluteProportionElementItemS() {
		return listAbsoluteProportionElementItemS;
	}

	public int getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed() {
		//return idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed;
		
		//System.err.println("HomologBrowsingView "+this.idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedHomologBrowsingView);
		//System.err.println("SyntenyView "+this.idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedSyntenyView);
		
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			//System.err.println("retrurning HomologBrowsingView "+this.idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedHomologBrowsingView);
			return this.idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedHomologBrowsingView;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			//System.err.println("retrurning SyntenyView "+this.idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedSyntenyView);
			return this.idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedSyntenyView;
		}else{
			//System.err.println("Error in getIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed : unrecognized type");
			return -1;
		}
	}


	public void setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed(
			int idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed,
			EnumResultViewTypes enumResultViewTypesSent) {
		
		//if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
		if(enumResultViewTypesSent.compareTo(EnumResultViewTypes.homolog_table)==0){	
			this.idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedHomologBrowsingView = idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed;
		//}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
		}else if(enumResultViewTypesSent.compareTo(EnumResultViewTypes.genomic_organization)==0){	
			this.idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayedSyntenyView = idxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed;
		}else{
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed : unrecognized type"));
			//System.err.println("Error in setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed : unrecognized type");
		}
		
	}

	
	public void setPercentSyntenyShowStartQGenome(
			double percentSyntenyShowStartQGenome,
			boolean doCalculatePartialAssociatedlistAbsoluteProportionItem,
			boolean isDezoom) {
		//this.percentSyntenyShowStartQGenome = percentSyntenyShowStartQGenome;
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			this.percentSyntenyShowStartQGenomeHomologBrowsingView = percentSyntenyShowStartQGenome;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			this.percentSyntenyShowStartQGenomeSyntenyView = percentSyntenyShowStartQGenome;
		}else{
			System.err.println("Error in setPercentSyntenyShowStartQGenome : unrecognized type");
		}
		if (doCalculatePartialAssociatedlistAbsoluteProportionItem) {
			calculatePartialAssociatedlistAbsoluteProportionItem(
					//true, 
					isDezoom);
		}
	}

	public double getPercentSyntenyShowStartQGenome() {
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			if (this.percentSyntenyShowStartQGenomeHomologBrowsingView < 0) {
				this.percentSyntenyShowStartQGenomeHomologBrowsingView = 0;
			} else if (this.percentSyntenyShowStartQGenomeHomologBrowsingView > 1) {
				this.percentSyntenyShowStartQGenomeHomologBrowsingView = 1;
			}
			return this.percentSyntenyShowStartQGenomeHomologBrowsingView;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			if (this.percentSyntenyShowStartQGenomeSyntenyView < 0) {
				this.percentSyntenyShowStartQGenomeSyntenyView = 0;
			} else if (this.percentSyntenyShowStartQGenomeSyntenyView > 1) {
				this.percentSyntenyShowStartQGenomeSyntenyView = 1;
			}
			return this.percentSyntenyShowStartQGenomeSyntenyView;
		}else{
			System.err.println("Error in getPercentSyntenyShowStartQGenome : unrecognized type");
			return -1;
		}
		

	}

	public void setPercentSyntenyShowStopQGenome(
			double percentSyntenyShowStopQGenome,
			boolean doCalculatePartialAssociatedlistAbsoluteProportionItem,
			boolean isDezoom) {
		//this.percentSyntenyShowStopQGenome = percentSyntenyShowStopQGenome;
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			this.percentSyntenyShowStopQGenomeHomologBrowsingView = percentSyntenyShowStopQGenome;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			this.percentSyntenyShowStopQGenomeSyntenyView = percentSyntenyShowStopQGenome;
		}else{
			System.err.println("Error in setPercentSyntenyShowStopQGenome : unrecognized type");

		}
		if (doCalculatePartialAssociatedlistAbsoluteProportionItem) {
			calculatePartialAssociatedlistAbsoluteProportionItem(
					//true, 
					isDezoom);
		}
	}

	public double getPercentSyntenyShowStopQGenome() {
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			if (this.percentSyntenyShowStopQGenomeHomologBrowsingView < 0) {
				this.percentSyntenyShowStopQGenomeHomologBrowsingView = 0;
			} else if (this.percentSyntenyShowStopQGenomeHomologBrowsingView > 1) {
				this.percentSyntenyShowStopQGenomeHomologBrowsingView = 1;
			}
			return this.percentSyntenyShowStopQGenomeHomologBrowsingView;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			if (this.percentSyntenyShowStopQGenomeSyntenyView < 0) {
				this.percentSyntenyShowStopQGenomeSyntenyView = 0;
			} else if (this.percentSyntenyShowStopQGenomeSyntenyView > 1) {
				this.percentSyntenyShowStopQGenomeSyntenyView = 1;
			}
			return this.percentSyntenyShowStopQGenomeSyntenyView;
		}else{
			System.err.println("Error in getPercentSyntenyShowStopQGenome : unrecognized type");
			return -1;
		}
		
	}

	public void setPercentSyntenyShowStartSGenome(
			double percentSyntenyShowStartSGenome,
			boolean doCalculatePartialAssociatedlistAbsoluteProportionItem,
			boolean isDezoom) {
		//this.percentSyntenyShowStartSGenome = percentSyntenyShowStartSGenome;
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			this.percentSyntenyShowStartSGenomeHomologBrowsingView = percentSyntenyShowStartSGenome;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			this.percentSyntenyShowStartSGenomeSyntenyView = percentSyntenyShowStartSGenome;
		}else{
			System.err.println("Error in setPercentSyntenyShowStartSGenome : unrecognized type");
		}
		if (doCalculatePartialAssociatedlistAbsoluteProportionItem) {
			calculatePartialAssociatedlistAbsoluteProportionItem(
					//false, 
					isDezoom);
		}
	}

	public double getPercentSyntenyShowStartSGenome() {
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			if (this.percentSyntenyShowStartSGenomeHomologBrowsingView < 0) {
				this.percentSyntenyShowStartSGenomeHomologBrowsingView = 0;
			} else if (this.percentSyntenyShowStartSGenomeHomologBrowsingView > 1) {
				this.percentSyntenyShowStartSGenomeHomologBrowsingView = 1;
			}
			return this.percentSyntenyShowStartSGenomeHomologBrowsingView;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			if (this.percentSyntenyShowStartSGenomeSyntenyView < 0) {
				this.percentSyntenyShowStartSGenomeSyntenyView = 0;
			} else if (this.percentSyntenyShowStartSGenomeSyntenyView > 1) {
				this.percentSyntenyShowStartSGenomeSyntenyView = 1;
			}
			return this.percentSyntenyShowStartSGenomeSyntenyView;
		}else{
			System.err.println("Error in getPercentSyntenyShowStartSGenome : unrecognized type");
			return -1;
		}
	}

	public void setPercentSyntenyShowStopSGenome(
			double percentSyntenyShowStopSGenome,
			boolean doCalculatePartialAssociatedlistAbsoluteProportionItem,
			boolean isDezoom) {
		//this.percentSyntenyShowStopSGenome = percentSyntenyShowStopSGenome;
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			this.percentSyntenyShowStopSGenomeHomologBrowsingView = percentSyntenyShowStopSGenome;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			this.percentSyntenyShowStopSGenomeSyntenyView = percentSyntenyShowStopSGenome;
		}else{
			System.err.println("Error in setPercentSyntenyShowStopSGenome : unrecognized type");
		}
		if (doCalculatePartialAssociatedlistAbsoluteProportionItem) {
			calculatePartialAssociatedlistAbsoluteProportionItem(
					//false, 
					isDezoom);
		}
	}

	public double getPercentSyntenyShowStopSGenome() {
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			if (this.percentSyntenyShowStopSGenomeHomologBrowsingView < 0) {
				this.percentSyntenyShowStopSGenomeHomologBrowsingView = 0;
			} else if (this.percentSyntenyShowStopSGenomeHomologBrowsingView > 1) {
				this.percentSyntenyShowStopSGenomeHomologBrowsingView = 1;
			}
			return this.percentSyntenyShowStopSGenomeHomologBrowsingView;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			if (this.percentSyntenyShowStopSGenomeSyntenyView < 0) {
				this.percentSyntenyShowStopSGenomeSyntenyView = 0;
			} else if (this.percentSyntenyShowStopSGenomeSyntenyView > 1) {
				this.percentSyntenyShowStopSGenomeSyntenyView = 1;
			}
			return this.percentSyntenyShowStopSGenomeSyntenyView;
		}else{
			System.err.println("Error in getPercentSyntenyShowStartQGenome : unrecognized type");
			return -1;
		}
	}


	public ArrayList<AbsoPropQSGeneHomoItem> getTmpListAPQSGeneHomo() {
		return tmpListAPQSGeneHomo;
	}

	public void setTmpListAPQSGeneHomo(
			ArrayList<AbsoPropQSGeneHomoItem> tmpListAPQSGeneHomo) {
		this.tmpListAPQSGeneHomo = tmpListAPQSGeneHomo;
	}


	public ArrayList<AbsoPropQGeneInserItem> getTmpListAPQGeneInserItem() {
		return tmpListAPQGeneInserItem;
	}

	public void setTmpListAPQGeneInserItem(ArrayList<AbsoPropQGeneInserItem> tmpListAPQGeneInserItem) {
		this.tmpListAPQGeneInserItem = tmpListAPQGeneInserItem;
	}

	
	public ArrayList<AbsoPropSGeneInserItem> getTmpListAPSGeneInserItem() {
		return tmpListAPSGeneInserItem;
	}

	public void setTmpListAPSGeneInserItem(ArrayList<AbsoPropSGeneInserItem> tmpListAPSGeneInserItem) {
		this.tmpListAPSGeneInserItem = tmpListAPSGeneInserItem;
	}


	public boolean isGpiLoadingAdditionalData() {
		return isGpiLoadingAdditionalData;
	}

	public void setGpiLoadingAdditionalData(boolean isGpiLoadingAdditionalData) {
		this.isGpiLoadingAdditionalData = isGpiLoadingAdditionalData;
	}

	public boolean isGpiErrorWhileLoadingAdditionalData() {
		return isGpiErrorWhileLoadingAdditionalData;
	}

	public void setGpiErrorWhileLoadingAdditionalData(
			boolean isGpiErrorWhileLoadingAdditionalData) {
		this.isGpiErrorWhileLoadingAdditionalData = isGpiErrorWhileLoadingAdditionalData;
	}



	public int getLookUpForGeneWithQStart() {
		//return lookUpForGeneWithQStart;
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			return this.lookUpForGeneWithQStart_orthoTable;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			return this.lookUpForGeneWithQStart_genomicOrga;
		}else{
			return -1;
		}
	}


	public void setLookUpForGeneWithQStart(int lookUpForGeneWithQStart, EnumResultViewTypes enumResultViewTypesSent) {
		//TODO separate lookup for homolog browser et genomic context
		//this.lookUpForGeneWithQStart = lookUpForGeneWithQStart;
		if(enumResultViewTypesSent.compareTo(EnumResultViewTypes.homolog_table)==0){	
			this.lookUpForGeneWithQStart_orthoTable = lookUpForGeneWithQStart;
		//}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
		}else if(enumResultViewTypesSent.compareTo(EnumResultViewTypes.genomic_organization)==0){	
			this.lookUpForGeneWithQStart_genomicOrga = lookUpForGeneWithQStart;
		}else{
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in setLookUpForGeneWithQStart : unrecognized type = "+enumResultViewTypesSent.toString()));
			//System.err.println("Error in setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed : unrecognized type");
		}
		
	}


	public int getLookUpForGeneWithQStop() {
		//return lookUpForGeneWithQStop;
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			return this.lookUpForGeneWithQStop_orthoTable;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			return this.lookUpForGeneWithQStop_genomicOrga;
		}else{
			return -1;
		}
	}


	public void setLookUpForGeneWithQStop(int lookUpForGeneWithQStop, EnumResultViewTypes enumResultViewTypesSent) {
		//this.lookUpForGeneWithQStop = lookUpForGeneWithQStop;
		if(enumResultViewTypesSent.compareTo(EnumResultViewTypes.homolog_table)==0){	
			this.lookUpForGeneWithQStop_orthoTable = lookUpForGeneWithQStop;
		//}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
		}else if(enumResultViewTypesSent.compareTo(EnumResultViewTypes.genomic_organization)==0){	
			this.lookUpForGeneWithQStop_genomicOrga = lookUpForGeneWithQStop;
		}else{
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in setLookUpForGeneWithQStop : unrecognized type = "+enumResultViewTypesSent.toString()));
			//System.err.println("Error in setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed : unrecognized type");
		}
	}


	public int getLookUpForGeneOnQElementId() {
		//return lookUpForGeneOnQElementId;
		if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.homolog_table)==0){
			return this.lookUpForGeneOnQElementId_orthoTable;
		}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
			return this.lookUpForGeneOnQElementId_genomicOrga;
		}else{
			return -1;
		}
	}


	public void setLookUpForGeneOnQElementId(int lookUpForGeneOnQElementId, EnumResultViewTypes enumResultViewTypesSent) {
		//this.lookUpForGeneOnQElementId = lookUpForGeneOnQElementId;
		if(enumResultViewTypesSent.compareTo(EnumResultViewTypes.homolog_table)==0){	
			this.lookUpForGeneOnQElementId_orthoTable = lookUpForGeneOnQElementId;
		//}else if(Insyght.APP_CONTROLER.getCurrentRefGenomePanelAndListOrgaResult().getViewTypeInsyght().compareTo(EnumResultViewTypes.genomic_organization)==0){
		}else if(enumResultViewTypesSent.compareTo(EnumResultViewTypes.genomic_organization)==0){	
			this.lookUpForGeneOnQElementId_genomicOrga = lookUpForGeneOnQElementId;
		}else{
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in setLookUpForGeneOnQElementId : unrecognized type = "+enumResultViewTypesSent.toString()));
			//System.err.println("Error in setIdxStartDrawingOnCanvasForArrayAbsoPropItemDisplayed : unrecognized type");
		}
	}


	
	private void calculatePartialAssociatedlistAbsoluteProportionItem(
			//boolean qHasChanged,
			boolean isDezoom) {

		/*System.err.println("zooming "+qHasChanged
				+" ; percentSyntenyShowStartQGenome="+percentSyntenyShowStartQGenome
				+" ; percentSyntenyShowStopQGenome="+percentSyntenyShowStopQGenome
				+" ; percentSyntenyShowStartSGenome="+percentSyntenyShowStartSGenome
				+" ; percentSyntenyShowStopSGenome="+percentSyntenyShowStopSGenome
		);*/
		
		if(getPercentSyntenyShowStartQGenome() == 0 && getPercentSyntenyShowStopQGenome() == 1 
				&& getPercentSyntenyShowStartSGenome() == 0 && getPercentSyntenyShowStopSGenome() == 1){
			//max out zoom both q and s
			partialAssociatedListAbsoluteProportionItemToDisplay.clear();
			//arrayDisplayedIndexAtSpecifiedAPIToDisplay = null;
			
		}else{
			//need calculatePartial

//			if(percentSyntenyShowStartQGenome == 0 && percentSyntenyShowStopQGenome == 1){
//				//zoomed out q
//				partialAssociatedListAbsoluteProportionItemToDisplay.clear();
//				qHasChanged = false;
//			}else if(percentSyntenyShowStartSGenome == 0 && percentSyntenyShowStopSGenome == 1){
//				//zoomed out s
//				partialAssociatedListAbsoluteProportionItemToDisplay.clear();
//				qHasChanged = true;
//			}
			if(isDezoom){
				partialAssociatedListAbsoluteProportionItemToDisplay.clear();
			}
			
			boolean makeACopyOfHolderAbsoluteProportionItem = true;
			if(getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly() == this.partialAssociatedListAbsoluteProportionItemToDisplay){
				makeACopyOfHolderAbsoluteProportionItem = false;
			}
			
			//ArrayList<ArrayList<HolderAbsoluteProportionItem>> tmpListHolderAbsoluteProportionItem = new ArrayList<ArrayList<HolderAbsoluteProportionItem>>();
			ArrayList<SuperHoldAbsoPropItem> tmpListHolderAbsoluteProportionItem = new ArrayList<SuperHoldAbsoPropItem>();
			
			for (int i = 0; i < getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size(); i++) {
				//System.err.println("i="+i);
				
				for(int k=0;k<getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(i).getListHAPI().size();k++){
					HolderAbsoluteProportionItem siIT = null;
					HolderAbsoluteProportionItem siITOri = getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(i).getListHAPI().get(k);
					
					if(makeACopyOfHolderAbsoluteProportionItem){
						siIT = new HolderAbsoluteProportionItem(siITOri);
					}else{
						siIT = siITOri;
					}
					
					boolean doNotAddToQueueMainArrayButMultipleMatchArray = false;
					if(k>0){
						doNotAddToQueueMainArrayButMultipleMatchArray = true;
					}
					
					boolean forceSSpaceInPreviousSlice = false;
					if(siIT.getAbsoluteProportionQComparableQSSpanItem() != null){
						//defineAbsoPropQSSyntItemAccordingToZoom(siIT.getAbsoPropQSSyntItem(), tmpPartialAssociatedlistAbsoluteProportionQComparableSyntenyItem, tmpPartialAssociatedlistAbsoluteProportionSyntenyItemSInsertion);
						
						if(siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem
								|| siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
							int kATAddedQSToTmpList = defineAbsoluteProportionQSItemsAccordingToZoom(siIT, tmpListHolderAbsoluteProportionItem,
									//qHasChanged,
									doNotAddToQueueMainArrayButMultipleMatchArray);
							if(kATAddedQSToTmpList>=0){
								
								// if cut stop, add fake holder for proper count
								if(siIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
									if(((AbsoPropQSSyntItem)siIT.getAbsoluteProportionQComparableQSSpanItem()).getQsEnumBlockType().toString().startsWith("QS_CUT_STOP_")){
										for(int l=1;l<getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(i).getListHAPI().size();l++){
											HolderAbsoluteProportionItem newHAPIFakeOffShoot = new HolderAbsoluteProportionItem();
											tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(newHAPIFakeOffShoot);
										}
									}
								}
								
								//setIdxToDisplayInListHAPI appropriately 
								tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).setIdxToDisplayInListHAPI(getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(i).getIdxToDisplayInListHAPI());
								
								//check for previousGenomiRegionInsertion and if room in tmpArray
								SuperHoldAbsoPropItem shPreviousIT = tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1);
								if(shPreviousIT.getListHAPI().size() > kATAddedQSToTmpList){
									if(shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionQComparableQSSpanItem() != null){
										if(shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
											if(!((AbsoPropQSSyntItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionQComparableQSSpanItem()).getQsEnumBlockType().toString().startsWith("QS_CUT_STOP_")){
												if(((AbsoPropQSSyntItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionQComparableQSSpanItem()).getPreviousGenomicRegionSInsertion() != null){
													AbsoPropSGenoRegiInserItem apssi = (AbsoPropSGenoRegiInserItem) defineAbsoluteProportionSItemAccordingToZoom(
															((AbsoPropQSSyntItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionQComparableQSSpanItem()).getPreviousGenomicRegionSInsertion()
															//, qHasChanged
															);
													if(apssi != null){
														((AbsoPropQSSyntItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionQComparableQSSpanItem()).setPreviousGenomicRegionSInsertion(apssi);
														forceSSpaceInPreviousSlice = true;
													}
												}
											}
											
										}
									}else if(shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem() != null){
										if(shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem){
											if(((AbsoPropSGenoRegiInserItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
												if(((AbsoPropSGenoRegiInserItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetPreviousGenomicRegionSInsertion() != null){
													AbsoPropSGenoRegiInserItem apssi = (AbsoPropSGenoRegiInserItem) defineAbsoluteProportionSItemAccordingToZoom(
															((AbsoPropSGenoRegiInserItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetPreviousGenomicRegionSInsertion()
															//, qHasChanged
															);
													if(apssi != null){
														((AbsoPropSGenoRegiInserItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem()).setsIfMissingTargetPreviousGenomicRegionSInsertion(apssi);
														forceSSpaceInPreviousSlice = true;
													}
												}
											
											}
										}else if(shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem){
											if(((AbsoPropSGeneInserItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
												if(((AbsoPropSGeneInserItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem()).getIfMissingTargetPreviousGenomicRegionSInsertion() != null){
													AbsoPropSGenoRegiInserItem apssi = (AbsoPropSGenoRegiInserItem) defineAbsoluteProportionSItemAccordingToZoom(
															((AbsoPropSGeneInserItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem()).getIfMissingTargetPreviousGenomicRegionSInsertion()
															//, qHasChanged
															);
													if(apssi != null){
														((AbsoPropSGeneInserItem)shPreviousIT.getListHAPI().get(kATAddedQSToTmpList).getAbsoluteProportionSComparableSSpanItem()).setIfMissingTargetPreviousGenomicRegionSInsertion(apssi);
														forceSSpaceInPreviousSlice = true;
													}
												}
											}
										}
									}
								}
								
							}
							
						}else{
							System.err.println("ERROR in qs calculatePartialAssociatedlistAbsoluteProportionItem unrecognized class for object sent");
							//Window.alert("ERROR in qs calculatePartialAssociatedlistAbsoluteProportionItem unrecognized class for object sent");
						}
						
					}else{
						//AbsoluteProportionQGenomicRegionInsertionItem apqsi = null;
						//AbsoluteProportionSGenomicRegionInsertionItem apssi = null;
						AbsoPropQCompaQSpanItem apqsi = null;
						AbsoPropSCompaSSpanItem apssi = null;
						HolderAbsoluteProportionItem newAPSI = new HolderAbsoluteProportionItem(siIT.getIndexInFullArray());
						boolean addToArray = false;
						//boolean forceAddToArrayBecauseOfPreviousSlice = false;
						
						//q
						if(siIT.getAbsoluteProportionQComparableQSpanItem() != null){
							if(siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGeneInserItem){
								apqsi = (AbsoPropQGeneInserItem) defineAbsoluteProportionQItemAccordingToZoom(
										siIT.getAbsoluteProportionQComparableQSpanItem()
										//, qHasChanged
										);
								if(apqsi != null){
									newAPSI.setAbsoluteProportionQComparableQSpanItem(apqsi);
									addToArray = true;
								}
							}else if(siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQGenoRegiInserItem){
								apqsi = (AbsoPropQGenoRegiInserItem) defineAbsoluteProportionQItemAccordingToZoom(
										siIT.getAbsoluteProportionQComparableQSpanItem()
										//, qHasChanged
										);
								if(apqsi != null){
									newAPSI.setAbsoluteProportionQComparableQSpanItem(apqsi);
									addToArray = true;
								}
								
							}else if(siIT.getAbsoluteProportionQComparableQSpanItem() instanceof AbsoPropQElemItem){
								apqsi = (AbsoPropQElemItem) defineAbsoluteProportionQItemAccordingToZoom(
										siIT.getAbsoluteProportionQComparableQSpanItem()
										//, qHasChanged
										);
								if(apqsi != null){
									newAPSI.setAbsoluteProportionQComparableQSpanItem(apqsi);
									addToArray = true;
								}
								
							}else{
								System.err.println("ERROR in q calculatePartialAssociatedlistAbsoluteProportionItem unrecognized class for object sent");
								//Window.alert("ERROR in q calculatePartialAssociatedlistAbsoluteProportionItem unrecognized class for object sent");
							}
						}
						//s
						if(siIT.getAbsoluteProportionSComparableSSpanItem() != null){
							//defineAbsoluteProportionSSyntenyItemAccordingToZoom(siIT.getAbsoluteProportionSSyntenyItem(), tmpPartialAssociatedlistAbsoluteProportionSyntenyItemSInsertion);
							if(siIT.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGeneInserItem){
								apssi = (AbsoPropSGeneInserItem) defineAbsoluteProportionSItemAccordingToZoom(
										siIT.getAbsoluteProportionSComparableSSpanItem()
										//, qHasChanged
										);
								if(apssi != null){
									newAPSI.setAbsoluteProportionSComparableSSpanItem(apssi);
									addToArray = true;
								}

								//check for previousGenomiRegionInsertion and if room in tmpArray
								if(((AbsoPropSGeneInserItem)siIT.getAbsoluteProportionSComparableSSpanItem()).getIfMissingTargetPreviousGenomicRegionSInsertion() != null){
									AbsoPropSGenoRegiInserItem apssiPre = (AbsoPropSGenoRegiInserItem) defineAbsoluteProportionSItemAccordingToZoom(
											((AbsoPropSGeneInserItem)siIT.getAbsoluteProportionSComparableSSpanItem()).getIfMissingTargetPreviousGenomicRegionSInsertion()
											//, qHasChanged
											);
									if(apssiPre != null){
										((AbsoPropSGeneInserItem)siIT.getAbsoluteProportionSComparableSSpanItem()).setIfMissingTargetPreviousGenomicRegionSInsertion(apssiPre);
										forceSSpaceInPreviousSlice = true;
									}
								}
								
							}else if(siIT.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem){
								apssi = (AbsoPropSGenoRegiInserItem) defineAbsoluteProportionSItemAccordingToZoom(
										siIT.getAbsoluteProportionSComparableSSpanItem()
										//, qHasChanged
										);
								if(apssi != null){
									newAPSI.setAbsoluteProportionSComparableSSpanItem(apssi);
									addToArray = true;
									
									//check for previousGenomiRegionInsertion and if room in tmpArray
									if(((AbsoPropSGenoRegiInserItem)siIT.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetPreviousGenomicRegionSInsertion() != null){
										AbsoPropSGenoRegiInserItem apssiPre = (AbsoPropSGenoRegiInserItem) defineAbsoluteProportionSItemAccordingToZoom(
												((AbsoPropSGenoRegiInserItem)siIT.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetPreviousGenomicRegionSInsertion()
												//, qHasChanged
												);
										if(apssiPre != null){
											((AbsoPropSGenoRegiInserItem)siIT.getAbsoluteProportionSComparableSSpanItem()).setsIfMissingTargetPreviousGenomicRegionSInsertion(apssiPre);
											forceSSpaceInPreviousSlice = true;
										}
									}
									
								}
								
							}else if(siIT.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSElemItem){
								apssi = (AbsoPropSElemItem) defineAbsoluteProportionSItemAccordingToZoom(
										siIT.getAbsoluteProportionSComparableSSpanItem()
										//, qHasChanged
										);
								if(apssi != null){
									newAPSI.setAbsoluteProportionSComparableSSpanItem(apssi);
									addToArray = true;
								}
								
							}else{
								System.err.println("ERROR in s calculatePartialAssociatedlistAbsoluteProportionItem unrecognized class for object sent");
								//Window.alert("ERROR in s calculatePartialAssociatedlistAbsoluteProportionItem unrecognized class for object sent");
							}
						}else{

//							int idxPreviousSlice = i-1;
//							if(idxPreviousSlice >= 0 
//									&& idxPreviousSlice < getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()){
							int idxPreviousIT = tmpListHolderAbsoluteProportionItem.size()-1;
							if(idxPreviousIT >= 0){
								SuperHoldAbsoPropItem shPreviousSlice = tmpListHolderAbsoluteProportionItem.get(idxPreviousIT);
								for(int l=0; l<shPreviousSlice.getListHAPI().size(); l++){
									HolderAbsoluteProportionItem hapiPreviousIT = shPreviousSlice.getListHAPI().get(l);
									if(hapiPreviousIT.getAbsoluteProportionQComparableQSSpanItem() != null){
										if(hapiPreviousIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
											if(((AbsoPropQSSyntItem)hapiPreviousIT.getAbsoluteProportionQComparableQSSpanItem()).getNextGenomicRegionSInsertion() != null){
												apssi = (AbsoPropSGenoRegiInserItem) defineAbsoluteProportionSItemAccordingToZoom(
														((AbsoPropQSSyntItem)hapiPreviousIT.getAbsoluteProportionQComparableQSSpanItem()).getNextGenomicRegionSInsertion()
														//, qHasChanged
														);
												if(apssi != null){
													((AbsoPropQSSyntItem)hapiPreviousIT.getAbsoluteProportionQComparableQSSpanItem()).setNextGenomicRegionSInsertion((AbsoPropSGenoRegiInserItem)apssi);
													//forceAddToArrayBecauseOfPreviousSlice = true;
												}
											}
										}
									}else if(hapiPreviousIT.getAbsoluteProportionSComparableSSpanItem() != null){
										if(hapiPreviousIT.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem){
											if(((AbsoPropSGenoRegiInserItem)hapiPreviousIT.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
												if(((AbsoPropSGenoRegiInserItem)hapiPreviousIT.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetNextGenomicRegionSInsertion() != null){
													apssi = ((AbsoPropSGenoRegiInserItem)hapiPreviousIT.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetNextGenomicRegionSInsertion();
													if(apssi != null){
														((AbsoPropSGenoRegiInserItem)hapiPreviousIT.getAbsoluteProportionSComparableSSpanItem()).setsIfMissingTargetNextGenomicRegionSInsertion((AbsoPropSGenoRegiInserItem)apssi);
														//forceAddToArrayBecauseOfPreviousSlice = true;
													}
												}
											}
										}
									}
								}
							}
						}
						
						
						if(addToArray){
							if(doNotAddToQueueMainArrayButMultipleMatchArray
									&& tmpListHolderAbsoluteProportionItem.size() > 0){
								tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(newAPSI);
							}else{
								SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
								alHAPI.getListHAPI().add(newAPSI);
								tmpListHolderAbsoluteProportionItem.add(alHAPI);
							}
						}
//						else if(forceAddToArrayBecauseOfPreviousSlice){
//							SuperHolderAbsoluteProportionItem alHAPI = new SuperHolderAbsoluteProportionItem();
//							alHAPI.getListHAPI().add(newAPSI);
//							tmpListHolderAbsoluteProportionItem.add(alHAPI);
//						}
						
					}
					
					if(forceSSpaceInPreviousSlice){
						int idxDoublePreviousSlice = tmpListHolderAbsoluteProportionItem.size()-2;
						if(idxDoublePreviousSlice >= 0){
							SuperHoldAbsoPropItem shPreviousSlice = tmpListHolderAbsoluteProportionItem.get(idxDoublePreviousSlice);
							boolean spaceIsFree = true;
							for(int l=0; l<shPreviousSlice.getListHAPI().size(); l++){
								HolderAbsoluteProportionItem hapiPreviousIT = shPreviousSlice.getListHAPI().get(l);
								if(hapiPreviousIT.getAbsoluteProportionQComparableQSSpanItem() != null
										|| hapiPreviousIT.getAbsoluteProportionSComparableSSpanItem() != null){
									spaceIsFree = false;
								}
							}
							if(!spaceIsFree){
								HolderAbsoluteProportionItem blankAPSI = new HolderAbsoluteProportionItem(siIT.getIndexInFullArray()-1);
								SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
								alHAPI.getListHAPI().add(blankAPSI);
								tmpListHolderAbsoluteProportionItem.add(tmpListHolderAbsoluteProportionItem.size()-1, alHAPI);
							}
						}else{
							//start of array
							//System.err.println("here");
							HolderAbsoluteProportionItem blankAPSI = new HolderAbsoluteProportionItem(siIT.getIndexInFullArray()-1);
							SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
							alHAPI.getListHAPI().add(blankAPSI);
							tmpListHolderAbsoluteProportionItem.add(0, alHAPI);
						}
						
					}
					
				}
				
			}
			
			this.partialAssociatedListAbsoluteProportionItemToDisplay.clear();
			//arrayDisplayedIndexAtSpecifiedAPIToDisplay = null;
			//crunchQInsertionAndSInsertionIntoQAndSSameSlot(tmpListHolderAbsoluteProportionItem);
			setPartialListAbsoluteProportionItemToDisplay(tmpListHolderAbsoluteProportionItem);
			
			//sortAndInsertCorrectlyAbsoluteProportionSyntenyItemInPartialAssociatedlistAbsoluteProportionSyntenyItem(tmpPartialAssociatedlistAbsoluteProportionQComparableSyntenyItem, tmpPartialAssociatedlistAbsoluteProportionSyntenyItemSInsertion);
			
		}
		
	}

	
	
	private AbsoPropSCompaSSpanItem defineAbsoluteProportionSItemAccordingToZoom(
			AbsoPropSCompaSSpanItem absoluteProportionSComparableSSpanItemSent
			//,boolean qHasChanged
			) {
		
		double sPercentStartOfSent = -1;
		double sPercentStopOfSent = -1;
		if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_START_S_INSERTION)==0){
			sPercentStartOfSent = absoluteProportionSComparableSSpanItemSent.getsPercentStart();
			sPercentStopOfSent = absoluteProportionSComparableSSpanItemSent.getsPercentStart();
		}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_STOP_S_INSERTION)==0){
			sPercentStartOfSent = absoluteProportionSComparableSSpanItemSent.getsPercentStop();
			sPercentStopOfSent = absoluteProportionSComparableSSpanItemSent.getsPercentStop();
		}else{
			sPercentStartOfSent = absoluteProportionSComparableSSpanItemSent.getsPercentStart();
			sPercentStopOfSent = absoluteProportionSComparableSSpanItemSent.getsPercentStop();
		}
		
		
			
			//check if has changed
			if(sPercentStartOfSent >= getPercentSyntenyShowStartSGenome() && sPercentStartOfSent <= getPercentSyntenyShowStopSGenome()
					&& sPercentStopOfSent >= getPercentSyntenyShowStartSGenome() && sPercentStopOfSent <= getPercentSyntenyShowStopSGenome()){
				//whole thing, keep as
				//tmpListAbsoluteProportionSyntenyItem.add(absoluteProportionSyntenyItem);
				return absoluteProportionSComparableSSpanItemSent;
			}else if(sPercentStartOfSent < getPercentSyntenyShowStartSGenome()
					&& sPercentStopOfSent > getPercentSyntenyShowStartSGenome() && sPercentStopOfSent <= getPercentSyntenyShowStopSGenome()){
				//partial left
				if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_PARTIAL_LEFT_")){
					//keep as just update start
				}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_PARTIAL_RIGHT_")){
					//now partial left and right
					if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_INSERTION)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION);
					}
				}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_PARTIAL_RIGHTANDLEFT_")){
					//still partial left and right, shorter
				}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
					//become partial left
					if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else{
						System.err.println("Error in defineAbsoluteProportionSItemAccordingToZoom become partial left : block type not found");
						//Window.alert("Error in defineAbsoluteProportionSItemAccordingToZoom become partial left : block type not found");
					}
					
				}else{
					//S_INSERTION
					//become partial left
					absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_INSERTION);
				}
				absoluteProportionSComparableSSpanItemSent.setsPercentStart(getPercentSyntenyShowStartSGenome());
				//tmpListAbsoluteProportionSyntenyItem.add(absoluteProportionSyntenyItem);
				return absoluteProportionSComparableSSpanItemSent;
			}else if(sPercentStartOfSent >= getPercentSyntenyShowStopSGenome()){
				//out right
				return null;
			}else if(sPercentStopOfSent <= getPercentSyntenyShowStartSGenome()){
				//out left
				return null;
			}else if(sPercentStartOfSent >= getPercentSyntenyShowStartSGenome() && sPercentStartOfSent < getPercentSyntenyShowStopSGenome()
					&& sPercentStopOfSent > getPercentSyntenyShowStopSGenome()){
				//partial right
				if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_PARTIAL_LEFT_")){
					//now partial left and right
					if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_INSERTION)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION);
					}
				}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_PARTIAL_RIGHT_")){
					//keep as just update start
				}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_PARTIAL_RIGHTANDLEFT_")){
					//still partial left and right, shorter
				}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
					//become partial right
					if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else{
						System.err.println("Error in defineAbsoluteProportionSItemAccordingToZoom become partial right : block type not found");
						//Window.alert("Error in defineAbsoluteProportionSItemAccordingToZoom become partial right : block type not found");
					}
					
				}else{
					//S_INSERTION
					//become partial left
					absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_INSERTION);
				}
				absoluteProportionSComparableSSpanItemSent.setsPercentStop(getPercentSyntenyShowStopSGenome());
				//tmpListAbsoluteProportionSyntenyItem.add(absoluteProportionSyntenyItem);
				return absoluteProportionSComparableSSpanItemSent;
			}else if(sPercentStartOfSent < getPercentSyntenyShowStartSGenome()
					&& sPercentStopOfSent > getPercentSyntenyShowStopSGenome()){
				//partial left and right
				if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_PARTIAL_LEFT_")){
					//now partial left and right
					if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_INSERTION)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION);
					}
				}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_PARTIAL_RIGHT_")){
					//now partial left and right
					if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_INSERTION)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION);
					}
				}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_PARTIAL_RIGHTANDLEFT_")){
					//still partial left and right, shorter
				}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
					//become partial left and right
					if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionSComparableSSpanItemSent.getSEnumBlockType().compareTo(AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else{
						System.err.println("Error in defineAbsoluteProportionSItemAccordingToZoom partial left and right : block type not found");
						//Window.alert("Error in defineAbsoluteProportionSItemAccordingToZoom partial left and right : block type not found");
					}
				}else{
					//S_INSERTION
					//become partial left and right
					absoluteProportionSComparableSSpanItemSent.setSEnumBlockType(AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_INSERTION);
				}
				absoluteProportionSComparableSSpanItemSent.setsPercentStart(getPercentSyntenyShowStartSGenome());
				absoluteProportionSComparableSSpanItemSent.setsPercentStop(getPercentSyntenyShowStopSGenome());
				//tmpListAbsoluteProportionSyntenyItem.add(absoluteProportionSyntenyItem);
				return absoluteProportionSComparableSSpanItemSent;
			}else{
//				Window.alert("defineAbsoluteProportionSComparableSSpanItemAccordingToZoom error : doea not fall in any case : absoluteProportionSGenomicRegionInsertionItem.getsPercentStart()="+sPercentStartOfSent+
//						" ; getPercentSyntenyShowStartSGenome()="+getPercentSyntenyShowStartSGenome()+
//						" ; AbsoluteProportionSComparableSSpanItem.getsPercentStart="+sPercentStartOfSent+
//						" ; getPercentSyntenyShowStopSGenome()="+getPercentSyntenyShowStopSGenome()+
//						" ; AbsoluteProportionSComparableSSpanItem.getsPercentStop()="+sPercentStopOfSent+
//						" ; getPercentSyntenyShowStartSGenome()="+getPercentSyntenyShowStartSGenome()+
//						" ; absoluteProportionSyntenyItem.absoluteProportionSGenomicRegionInsertionItem().getsPercentStop()"+sPercentStopOfSent+
//						" ; getPercentSyntenyShowStopSGenome()="+getPercentSyntenyShowStopSGenome());
				System.out.println("defineabsoluteProportionSyntenyItem.absoluteProportionSGenomicRegionInsertionItem()AccordingToZoom error : doea not fall in any case : absoluteProportionSyntenyItem.getAbsoluteProportionSComparableSSpanItem().getsPercentStart()="+sPercentStartOfSent+
						" ; getPercentSyntenyShowStartSGenome()="+getPercentSyntenyShowStartSGenome()+
						" ; absoluteProportionSyntenyItem.absoluteProportionSGenomicRegionInsertionItem().getsPercentStart="+sPercentStartOfSent+
						" ; getPercentSyntenyShowStopSGenome()="+getPercentSyntenyShowStopSGenome()+
						" ; absoluteProportionSyntenyItem.absoluteProportionSGenomicRegionInsertionItem().getsPercentStop()="+sPercentStopOfSent+
						" ; getPercentSyntenyShowStartSGenome()="+getPercentSyntenyShowStartSGenome()+
						" ; absoluteProportionSyntenyItem.absoluteProportionSGenomicRegionInsertionItem().getsPercentStop()"+sPercentStopOfSent+
						" ; getPercentSyntenyShowStopSGenome()="+getPercentSyntenyShowStopSGenome());
			}
			
		//}
		return null;
	}

	private AbsoPropQCompaQSpanItem defineAbsoluteProportionQItemAccordingToZoom(
			AbsoPropQCompaQSpanItem absoluteProportionQComparableQSpanItemSent
			//,boolean qHasChanged
			) {
		
		double qPercentStartOfSent = -1;
		double qPercentStopOfSent = -1;
		if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_START_Q_INSERTION)==0){
			qPercentStartOfSent = absoluteProportionQComparableQSpanItemSent.getqPercentStart();
			qPercentStopOfSent = absoluteProportionQComparableQSpanItemSent.getqPercentStart();
		}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_STOP_Q_INSERTION)==0){
			qPercentStartOfSent = absoluteProportionQComparableQSpanItemSent.getqPercentStop();
			qPercentStopOfSent = absoluteProportionQComparableQSpanItemSent.getqPercentStop();
		}else{
			qPercentStartOfSent = absoluteProportionQComparableQSpanItemSent.getqPercentStart();
			qPercentStopOfSent = absoluteProportionQComparableQSpanItemSent.getqPercentStop();
		}
		
			
			
			if(qPercentStartOfSent >= getPercentSyntenyShowStartQGenome() && qPercentStartOfSent <= getPercentSyntenyShowStopQGenome()
					&& qPercentStopOfSent >= getPercentSyntenyShowStartQGenome() && qPercentStopOfSent <= getPercentSyntenyShowStopQGenome()){
				//whole thing, keep as
				//tmpListAbsoluteProportionSyntenyItem.add(absoluteProportionSyntenyItem);
				return absoluteProportionQComparableQSpanItemSent;
			}else if(qPercentStartOfSent < getPercentSyntenyShowStartQGenome()
					&& qPercentStopOfSent > getPercentSyntenyShowStartQGenome() && qPercentStopOfSent <= getPercentSyntenyShowStopQGenome()){
				//partial left
				if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_PARTIAL_LEFT_")){
					//keep as just update start
				}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_PARTIAL_RIGHT_")){
					//now partial left and right
					if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_INSERTION)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION);
					}
				}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_PARTIAL_RIGHTANDLEFT_")){
					//still partial left and right, shorter
				}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_MISSING_TARGET_S_")){
					//become partial left
					if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
					}else{
						System.err.println("Error in defineAbsoluteProportionQItemAccordingToZoom become partial left : block type not found");
						//Window.alert("Error in defineAbsoluteProportionQItemAccordingToZoom become partial left : block type not found");
					}
				}else{
					//S_INSERTION
					//become partial left
					absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_INSERTION);
				}
				absoluteProportionQComparableQSpanItemSent.setqPercentStart(getPercentSyntenyShowStartQGenome());
				//tmpListAbsoluteProportionSyntenyItem.add(absoluteProportionSyntenyItem);
				return absoluteProportionQComparableQSpanItemSent;
			}else if(qPercentStartOfSent >= getPercentSyntenyShowStopQGenome()){
				//out right
				return null;
			}else if(qPercentStopOfSent <= getPercentSyntenyShowStartQGenome()){
				//out left
				return null;
			}else if(qPercentStartOfSent >= getPercentSyntenyShowStartQGenome() && qPercentStartOfSent < getPercentSyntenyShowStopQGenome()
					&& qPercentStopOfSent > getPercentSyntenyShowStopQGenome()){
				//partial right
				if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_PARTIAL_LEFT_")){
					//now partial left and right
					if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_INSERTION)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION);
					}
				}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_PARTIAL_RIGHT_")){
					//keep as just update start
				}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_PARTIAL_RIGHTANDLEFT_")){
					//still partial left and right, shorter
				}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_MISSING_TARGET_S_")){
					//become partial right
					if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
					}else{
						System.err.println("Error in defineAbsoluteProportionQItemAccordingToZoom become partial right : block type not found");
						//Window.alert("Error in defineAbsoluteProportionQItemAccordingToZoom become partial right : block type not found");
					}
				}else{
					//S_INSERTION
					//become partial left
					absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_INSERTION);
				}
				absoluteProportionQComparableQSpanItemSent.setqPercentStop(getPercentSyntenyShowStopQGenome());
				//tmpListAbsoluteProportionSyntenyItem.add(absoluteProportionSyntenyItem);
				return absoluteProportionQComparableQSpanItemSent;
			}else if(qPercentStartOfSent < getPercentSyntenyShowStartQGenome()
					&& qPercentStopOfSent > getPercentSyntenyShowStopQGenome()){
				//partial left and right
				if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_PARTIAL_LEFT_")){
					//now partial left and right
					if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_INSERTION)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION);
					}
				}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_PARTIAL_RIGHT_")){
					//now partial left and right
					if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_INSERTION)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION);
					}
				}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_PARTIAL_RIGHTANDLEFT_")){
					//still partial left and right, shorter
				}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().toString().startsWith("Q_MISSING_TARGET_S_")){
					//become partial left and right
					if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_LEFT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else if(absoluteProportionQComparableQSpanItemSent.getqEnumBlockType().compareTo(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_RIGHT)==0){
						absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK);
					}else{
						System.err.println("Error in defineAbsoluteProportionQItemAccordingToZoom become partial left and right : block type not found");
						//Window.alert("Error in defineAbsoluteProportionQItemAccordingToZoom become partial left and right : block type not found");
					}
				}else{
					//S_INSERTION
					//become partial left and right
					absoluteProportionQComparableQSpanItemSent.setqEnumBlockType(AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_INSERTION);
				}
				absoluteProportionQComparableQSpanItemSent.setqPercentStart(getPercentSyntenyShowStartQGenome());
				absoluteProportionQComparableQSpanItemSent.setqPercentStop(getPercentSyntenyShowStopQGenome());
				//tmpListAbsoluteProportionSyntenyItem.add(absoluteProportionSyntenyItem);
				return absoluteProportionQComparableQSpanItemSent;
			}else{
//				Window.alert("defineAbsoluteProportionQSyntenyItemAccordingToZoom error : doea not fall in any case : absoluteProportionQGenomicRegionInsertionItem.getqPercentStart()="+qPercentStartOfSent+
//						" ; getPercentSyntenyShowStartSGenome()="+getPercentSyntenyShowStartQGenome()+
//						" ; absoluteProportionQGenomicRegionInsertionItem.getqPercentStart="+qPercentStartOfSent+
//						" ; getPercentSyntenyShowStopSGenome()="+getPercentSyntenyShowStopQGenome()+
//						" ; absoluteProportionQGenomicRegionInsertionItem.getqPercentStop()="+qPercentStopOfSent+
//						" ; getPercentSyntenyShowStartSGenome()="+getPercentSyntenyShowStartQGenome()+
//						" ; absoluteProportionQGenomicRegionInsertionItem.getqPercentStop()"+qPercentStopOfSent+
//						" ; getPercentSyntenyShowStopSGenome()="+getPercentSyntenyShowStopQGenome());
				System.out.println("defineAbsoluteProportionSSyntenyItemAccordingToZoom error : doea not fall in any case : absoluteProportionQGenomicRegionInsertionItem.getqPercentStart()="+qPercentStartOfSent+
						" ; getPercentSyntenyShowStartSGenome()="+getPercentSyntenyShowStartQGenome()+
						" ; absoluteProportionQGenomicRegionInsertionItem.getqPercentStart="+qPercentStartOfSent+
						" ; getPercentSyntenyShowStopSGenome()="+getPercentSyntenyShowStopQGenome()+
						" ; absoluteProportionQGenomicRegionInsertionItem.getqPercentStop()="+qPercentStopOfSent+
						" ; getPercentSyntenyShowStartSGenome()="+getPercentSyntenyShowStartQGenome()+
						" ; absoluteProportionQGenomicRegionInsertionItem.getqPercentStop()"+qPercentStopOfSent+
						" ; getPercentSyntenyShowStopSGenome()="+getPercentSyntenyShowStopQGenome());
			}
		
		//}
		return null;
	}

	
	private int defineAbsoluteProportionQSItemsAccordingToZoom(
			HolderAbsoluteProportionItem holderAbsoluteProportionItem,
			ArrayList<SuperHoldAbsoPropItem> tmpListHolderAbsoluteProportionItem,
			//boolean qHasChanged,
			boolean doNotAddToQueueMainArrayButMultipleMatchArray) {
		
		//can be QS_HOMOLOGS_BLOCK or QS_REVERSE_HOMOLOGS_BLOCK
		double sPercentStartOfSent = -1;
		double sPercentStopOfSent = -1;
		double qPercentStartOfSent = -1;
		double qPercentStopOfSent = -1;
		if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0
				|| holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
			sPercentStartOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart();
			sPercentStopOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart();
			qPercentStartOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart();
			qPercentStopOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart();
		}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0
				|| holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
			sPercentStartOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop();
			sPercentStopOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop();
			qPercentStartOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop();
			qPercentStopOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop();
		}else{
			sPercentStartOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart();
			sPercentStopOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop();
			qPercentStartOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart();
			qPercentStopOfSent = holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop();
		}
		
		

		if(qPercentStartOfSent >= getPercentSyntenyShowStartQGenome() && qPercentStartOfSent <= getPercentSyntenyShowStopQGenome()
				&& qPercentStopOfSent >= getPercentSyntenyShowStartQGenome() && qPercentStopOfSent <= getPercentSyntenyShowStopQGenome()
				&& sPercentStartOfSent >= getPercentSyntenyShowStartSGenome() && sPercentStartOfSent <= getPercentSyntenyShowStopSGenome()
				&& sPercentStopOfSent >= getPercentSyntenyShowStartSGenome() && sPercentStopOfSent <= getPercentSyntenyShowStopSGenome()){
			//whole thing, keep as
			//System.out.println("whole thing, keep as");
			
			if(doNotAddToQueueMainArrayButMultipleMatchArray){
				tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(holderAbsoluteProportionItem);
				return tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().size()-1;
			}else{
				SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
				alHAPI.getListHAPI().add(holderAbsoluteProportionItem);
				tmpListHolderAbsoluteProportionItem.add(alHAPI);
				return 0;
			}
			//return true;
		}else{
			//deal with q side
			
			boolean isQOutRight = false;
			boolean isQOutLeft = false;
			boolean isQWhole = false;
			
//			if(!qHasChanged){
//				isQWhole = true;
//			}else{
				
				if(qPercentStartOfSent >= getPercentSyntenyShowStartQGenome() && qPercentStartOfSent <= getPercentSyntenyShowStopQGenome()
						&& qPercentStopOfSent >= getPercentSyntenyShowStartQGenome() && qPercentStopOfSent <= getPercentSyntenyShowStopQGenome()){
					//whole thing, keep as
					isQWhole = true;
				}else if(qPercentStartOfSent < getPercentSyntenyShowStartQGenome()
						&& qPercentStopOfSent > getPercentSyntenyShowStartQGenome() && qPercentStopOfSent <= getPercentSyntenyShowStopQGenome()){
					//partial left

					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
						
						AbsoPropQGeneInserItem newAbsoluteProportionQGeneInsertionItemIT;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem((AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartQGenome(),
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
							
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem((AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartQGenome(),
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
							
						}else{
							newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem((AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartQGenome(),
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
						}
												holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQGeneInsertionItemIT);
						
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
						
						AbsoPropQGenoRegiInserItem newAbsoluteProportionQGenomicRegionInsertionItemIT;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGenomicRegionInsertionItemIT = new AbsoPropQGenoRegiInserItem(
							(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
							AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK,
							getPercentSyntenyShowStartQGenome(),
							holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGenomicRegionInsertionItemIT = new AbsoPropQGenoRegiInserItem(
							(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
							AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK,
							getPercentSyntenyShowStartQGenome(),
							holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
						}else{
							newAbsoluteProportionQGenomicRegionInsertionItemIT = new AbsoPropQGenoRegiInserItem(
							(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
							AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK,
							getPercentSyntenyShowStartQGenome(),
							holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
						}
						
						holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQGenomicRegionInsertionItemIT);
						//tmpListAbsoluteProportionSyntenyItem.add(newAbsoPropQSSyntItem);
						
					}else{
						System.err.println("ERROR in q partial left defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
						//Window.alert("ERROR in q partial left defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					}
					isQOutLeft = true;
				}else if(qPercentStartOfSent >= getPercentSyntenyShowStopQGenome()){
					//out right
					isQOutRight = true;
				}else if(qPercentStopOfSent <= getPercentSyntenyShowStartQGenome()){
					//out left
					isQOutLeft = true;
				}else if(qPercentStartOfSent >= getPercentSyntenyShowStartQGenome() && qPercentStartOfSent < getPercentSyntenyShowStopQGenome()
						&& qPercentStopOfSent > getPercentSyntenyShowStopQGenome()){
					//partial right

					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
						
						AbsoPropQGeneInserItem newAbsoluteProportionQGeneInsertionItemIT;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
									getPercentSyntenyShowStopQGenome());
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
									getPercentSyntenyShowStopQGenome());
						}else{
							newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
									getPercentSyntenyShowStopQGenome());
						}
						
						
						holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQGeneInsertionItemIT);
						
					
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
						
						AbsoPropQGenoRegiInserItem newAbsoluteProportionQGenomicRegionInsertionItem;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGenomicRegionInsertionItem = new AbsoPropQGenoRegiInserItem(
									(AbsoPropQSSyntItem)holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
									getPercentSyntenyShowStopQGenome());
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGenomicRegionInsertionItem = new AbsoPropQGenoRegiInserItem(
									(AbsoPropQSSyntItem)holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
									getPercentSyntenyShowStopQGenome());
						}else{
							newAbsoluteProportionQGenomicRegionInsertionItem = new AbsoPropQGenoRegiInserItem(
									(AbsoPropQSSyntItem)holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
									getPercentSyntenyShowStopQGenome());
						}
						
						holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQGenomicRegionInsertionItem);
						//tmpListAbsoluteProportionSyntenyItem.add(newAbsoPropQSSyntItem);
					
					}else{
						System.err.println("ERROR in q partial right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
						//Window.alert("ERROR in q partial right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					}
					isQOutRight = true;
					
				}else if(qPercentStartOfSent < getPercentSyntenyShowStartQGenome()
						&& qPercentStopOfSent > getPercentSyntenyShowStopQGenome()){
					//partial left and right
					
					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
						
						AbsoPropQGeneInserItem newAbsoluteProportionQGeneInsertionItemIT;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
									(AbsoPropQSGeneHomoItem)holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartQGenome(),
									getPercentSyntenyShowStopQGenome());
						
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
									(AbsoPropQSGeneHomoItem)holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartQGenome(),
									getPercentSyntenyShowStopQGenome());
						}else{
							newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
									(AbsoPropQSGeneHomoItem)holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartQGenome(),
									getPercentSyntenyShowStopQGenome());
						}
												holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQGeneInsertionItemIT);
						
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
						
						AbsoPropQGenoRegiInserItem newAbsoluteProportionQSyntenyItem;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQSyntenyItem = new AbsoPropQGenoRegiInserItem(
									(AbsoPropQSSyntItem)holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartQGenome(),
									getPercentSyntenyShowStopQGenome());
							
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionQSyntenyItem = new AbsoPropQGenoRegiInserItem(
									(AbsoPropQSSyntItem)holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartQGenome(),
									getPercentSyntenyShowStopQGenome());
						}else{
							newAbsoluteProportionQSyntenyItem = new AbsoPropQGenoRegiInserItem(
									(AbsoPropQSSyntItem)holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropQCompaQSpanItem.QEnumBlockType.Q_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartQGenome(),
									getPercentSyntenyShowStopQGenome());
						}
												holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQSyntenyItem);
						//tmpListAbsoluteProportionSyntenyItem.add(newAbsoPropQSSyntItem);
						
					}else{
						System.err.println("ERROR in q partial left and right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
						//Window.alert("ERROR in q partial left and right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					}
					
					isQOutRight = true;
					isQOutLeft = true;
				}
				
			
			boolean isSOutRight = false;
			boolean isSOutLeft = false;
			boolean isSWhole = false;

				
				if(sPercentStartOfSent >= getPercentSyntenyShowStartSGenome() && sPercentStartOfSent <= getPercentSyntenyShowStopSGenome()
						&& sPercentStopOfSent >= getPercentSyntenyShowStartSGenome() && sPercentStopOfSent <= getPercentSyntenyShowStopSGenome()){
					//whole thing, keep as
					isSWhole = true;
				}else if(sPercentStartOfSent < getPercentSyntenyShowStartSGenome()
						&& sPercentStopOfSent > getPercentSyntenyShowStartSGenome() && sPercentStopOfSent <= getPercentSyntenyShowStopSGenome()){
					//partial left

					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
						
						AbsoPropSGeneInserItem newAbsoluteProportionSGeneInsertionItemIT;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
						}else{
							newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
						}
						
						holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSGeneInsertionItemIT);
						
					
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
						
						AbsoPropSGenoRegiInserItem newAbsoluteProportionSGenomicRegionInsertionItem;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
									(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
									(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
						}else{
							newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
									(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
						}
												holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSGenomicRegionInsertionItem);
						//tmpListAbsoluteProportionSyntenyItem.add(newAbsoluteProportionSSyntenyItem);
					
					}else{
						System.err.println("ERROR in s partial left defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
						//Window.alert("ERROR in s partial left defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					}
					isSOutLeft = true;
				}else if(sPercentStartOfSent >= getPercentSyntenyShowStopSGenome()){
					//out right
					isSOutRight = true;
				}else if(sPercentStopOfSent <= getPercentSyntenyShowStartSGenome()){
					//out left
					isSOutLeft = true;
				}else if(sPercentStartOfSent >= getPercentSyntenyShowStartSGenome() && sPercentStartOfSent < getPercentSyntenyShowStopSGenome()
						&& sPercentStopOfSent > getPercentSyntenyShowStopSGenome()){
					//partial right

					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
						
						AbsoPropSGeneInserItem newAbsoluteProportionSGeneInsertionItemIT;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
									getPercentSyntenyShowStopSGenome());
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
									getPercentSyntenyShowStopSGenome());
						}else{
							newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
									getPercentSyntenyShowStopSGenome());
						}
												holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSGeneInsertionItemIT);
						
					
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
						
						AbsoPropSGenoRegiInserItem newAbsoluteProportionSSyntenyItem;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSSyntenyItem = new AbsoPropSGenoRegiInserItem(
									(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
									getPercentSyntenyShowStopSGenome());
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSSyntenyItem = new AbsoPropSGenoRegiInserItem(
									(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
									getPercentSyntenyShowStopSGenome());
						}else{
							newAbsoluteProportionSSyntenyItem = new AbsoPropSGenoRegiInserItem(
									(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK,
									holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
									getPercentSyntenyShowStopSGenome());
						}
												holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSSyntenyItem);
						//tmpListAbsoluteProportionSyntenyItem.add(newAbsoluteProportionSSyntenyItem);
						
					}else{
						System.err.println("ERROR in s partial right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
						//Window.alert("ERROR in s partial right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					}
					isSOutRight = true;
				}else if(sPercentStartOfSent < getPercentSyntenyShowStartSGenome()
						&& sPercentStopOfSent > getPercentSyntenyShowStopSGenome()){
					//partial left and right

					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
						
						AbsoPropSGeneInserItem newAbsoluteProportionSGeneInsertionItemIT;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									getPercentSyntenyShowStopSGenome());
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									getPercentSyntenyShowStopSGenome());
						}else{
							newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
									(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									getPercentSyntenyShowStopSGenome());
						}
												holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSGeneInsertionItemIT);
						
					
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
						
						AbsoPropSGenoRegiInserItem newAbsoluteProportionSGenomicRegionInsertionItem;
						if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
									(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									getPercentSyntenyShowStopSGenome());
						}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
							newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
									(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_REVERSE_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									getPercentSyntenyShowStopSGenome());
						}else{
							newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
									(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
									AbsoPropSCompaSSpanItem.SEnumBlockType.S_PARTIAL_RIGHTANDLEFT_HOMOLOGS_BLOCK,
									getPercentSyntenyShowStartSGenome(),
									getPercentSyntenyShowStopSGenome());
						}
												holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSGenomicRegionInsertionItem);
						//tmpListAbsoluteProportionSyntenyItem.add(newAbsoluteProportionSSyntenyItem);
					}else{
						System.err.println("ERROR in s partial left and right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
						//Window.alert("ERROR in s partial left and right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					}
					isSOutRight = true;
					isSOutLeft = true;
				}
				
			//}
			
			
			//deal with missing target
			if(isQOutRight && isSWhole){
				//s missing target q right

				if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
					
					AbsoPropSGeneInserItem newAbsoluteProportionSGeneInsertionItemIT;
					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else{
						System.err.println("Error in defineAbsoluteProportionQSItemsAccordingToZoom isQOutRight && isSWhole instanceof AbsoPropQSGeneHomoItem : block type not recognized");
						//Window.alert("Error in defineAbsoluteProportionQSItemsAccordingToZoom isQOutRight && isSWhole instanceof AbsoPropQSGeneHomoItem : block type not recognized");
						newAbsoluteProportionSGeneInsertionItemIT = null;
					}
					
										holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSGeneInsertionItemIT);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSSpanItem(null);
					
					if(doNotAddToQueueMainArrayButMultipleMatchArray){
						tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(holderAbsoluteProportionItem);
						return tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().size()-1;
					}else{
						SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
						alHAPI.getListHAPI().add(holderAbsoluteProportionItem);
						tmpListHolderAbsoluteProportionItem.add(alHAPI);
						return 0;
					}
					//return true;
				}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
					
					AbsoPropSGenoRegiInserItem newAbsoluteProportionSGenomicRegionInsertionItem;
					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else{
						System.err.println("Error in defineAbsoluteProportionQSItemsAccordingToZoom isQOutRight && isSWhole instanceof AbsoPropQSSyntItem : block type not recognized");
						//Window.alert("Error in defineAbsoluteProportionQSItemsAccordingToZoom isQOutRight && isSWhole instanceof AbsoPropQSSyntItem : block type not recognized");
						newAbsoluteProportionSGenomicRegionInsertionItem = null;
					}
					
										holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSGenomicRegionInsertionItem);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSSpanItem(null);
					
					if(doNotAddToQueueMainArrayButMultipleMatchArray){
						tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(holderAbsoluteProportionItem);
						return tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().size()-1;
					}else{
						SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
						alHAPI.getListHAPI().add(holderAbsoluteProportionItem);
						tmpListHolderAbsoluteProportionItem.add(alHAPI);
						return 0;
					}
					//return true;
				}else{
					System.err.println("ERROR in s missing target q right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					//Window.alert("ERROR in s missing target q right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
				}
				
				//return;
			}else if(isQOutLeft && isSWhole){
				//s missing target q left

				if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
					
					AbsoPropSGeneInserItem newAbsoluteProportionSGeneInsertionItemIT;
					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGeneInsertionItemIT = new AbsoPropSGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else{
						System.err.println("Error in defineAbsoluteProportionQSItemsAccordingToZoom isQOutLeft && isSWhole instanceof AbsoPropQSGeneHomoItem : block type not recognized");
						//Window.alert("Error in defineAbsoluteProportionQSItemsAccordingToZoom isQOutLeft && isSWhole instanceof AbsoPropQSGeneHomoItem : block type not recognized");
						newAbsoluteProportionSGeneInsertionItemIT = null;
					}
					
										holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSGeneInsertionItemIT);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSSpanItem(null);
					
					if(doNotAddToQueueMainArrayButMultipleMatchArray){
						tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(holderAbsoluteProportionItem);
						return tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().size()-1;
					}else{
						SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
						alHAPI.getListHAPI().add(holderAbsoluteProportionItem);
						tmpListHolderAbsoluteProportionItem.add(alHAPI);
						return 0;
					}
					//return true;
					
				}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
					
					AbsoPropSGenoRegiInserItem newAbsoluteProportionSGenomicRegionInsertionItem;
					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_START_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_START_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_CUT_STOP_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionSGenomicRegionInsertionItem = new AbsoPropSGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropSCompaSSpanItem.SEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_CUT_STOP_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getsPercentStop());
					}else{
						System.err.println("Error in defineAbsoluteProportionQSItemsAccordingToZoom isQOutLeft && isSWhole instanceof AbsoPropQSSyntItem : block type not recognized");
						//Window.alert("Error in defineAbsoluteProportionQSItemsAccordingToZoom isQOutLeft && isSWhole instanceof AbsoPropQSSyntItem : block type not recognized");
						newAbsoluteProportionSGenomicRegionInsertionItem = null;
					}
					
										//tmpListAbsoluteProportionSyntenyItem.add(newAbsoluteProportionSSyntenyItem);
					holderAbsoluteProportionItem.setAbsoluteProportionSComparableSSpanItem(newAbsoluteProportionSGenomicRegionInsertionItem);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSSpanItem(null);
					
					if(doNotAddToQueueMainArrayButMultipleMatchArray){
						tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(holderAbsoluteProportionItem);
						return tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().size()-1;
					}else{
						SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
						alHAPI.getListHAPI().add(holderAbsoluteProportionItem);
						tmpListHolderAbsoluteProportionItem.add(alHAPI);
						return 0;
					}
					//return true;
				}else{
					System.err.println("ERROR in s missing target q left defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					//Window.alert("ERROR in s missing target q left defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
				}
				//return;
			}else if(isSOutLeft && isQWhole){
				//q missing target s left
				if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
					
					AbsoPropQGeneInserItem newAbsoluteProportionQGeneInsertionItemIT;
					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else{
						System.err.println("Error in defineAbsoluteProportionQSItemsAccordingToZoom isSOutLeft && isQWhole instanceof AbsoPropQSGeneHomoItem : block type not recognized");
						//Window.alert("Error in defineAbsoluteProportionQSItemsAccordingToZoom isSOutLeft && isQWhole instanceof AbsoPropQSGeneHomoItem : block type not recognized");
						newAbsoluteProportionQGeneInsertionItemIT = null;
					}
					
										holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQGeneInsertionItemIT);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSSpanItem(null);
					
					if(doNotAddToQueueMainArrayButMultipleMatchArray){
						tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(holderAbsoluteProportionItem);
						return tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().size()-1;
					}else{
						SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
						alHAPI.getListHAPI().add(holderAbsoluteProportionItem);
						tmpListHolderAbsoluteProportionItem.add(alHAPI);
						return 0;
					}
					//return true;
				}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
					
					AbsoPropQGenoRegiInserItem newAbsoluteProportionQGenomicRegionInsertionItem;
					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGenomicRegionInsertionItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGenomicRegionInsertionItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGenomicRegionInsertionItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGenomicRegionInsertionItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGenomicRegionInsertionItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGenomicRegionInsertionItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_LEFT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else{
						System.err.println("Error in defineAbsoluteProportionQSItemsAccordingToZoom isSOutLeft && isQWhole instanceof AbsoPropQSSyntItem : block type not recognized");
						//Window.alert("Error in defineAbsoluteProportionQSItemsAccordingToZoom isSOutLeft && isQWhole instanceof AbsoPropQSSyntItem : block type not recognized");
						newAbsoluteProportionQGenomicRegionInsertionItem = null;
					}
					
										//tmpListAbsoluteProportionSyntenyItem.add(newAbsoluteProportionQSyntenyItem);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQGenomicRegionInsertionItem);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSSpanItem(null);
					
					if(doNotAddToQueueMainArrayButMultipleMatchArray){
						tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(holderAbsoluteProportionItem);
						return tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().size()-1;
					}else{
						SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
						alHAPI.getListHAPI().add(holderAbsoluteProportionItem);
						tmpListHolderAbsoluteProportionItem.add(alHAPI);
						return 0;
					}
					//return true;
				}else{
					System.err.println("ERROR in q missing target s left defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					//Window.alert("ERROR in q missing target s left defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
				}
				//return;
			}else if(isSOutRight && isQWhole){
				//q missing target s right
				if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem){
					
					AbsoPropQGeneInserItem newAbsoluteProportionQGeneInsertionItemIT;
					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSGeneHomoItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQGeneInsertionItemIT = new AbsoPropQGeneInserItem(
								(AbsoPropQSGeneHomoItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else{
						System.err.println("Error in defineAbsoluteProportionQSItemsAccordingToZoom isSOutRight && isQWhole instanceof AbsoPropQSGeneHomoItem : block type not recognized");
						//Window.alert("Error in defineAbsoluteProportionQSItemsAccordingToZoom isSOutRight && isQWhole instanceof AbsoPropQSGeneHomoItem : block type not recognized");
						newAbsoluteProportionQGeneInsertionItemIT = null;
					}
					
										holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQGeneInsertionItemIT);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSSpanItem(null);
					
					if(doNotAddToQueueMainArrayButMultipleMatchArray){
						tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(holderAbsoluteProportionItem);
						return tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().size()-1;
					}else{
						SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
						alHAPI.getListHAPI().add(holderAbsoluteProportionItem);
						tmpListHolderAbsoluteProportionItem.add(alHAPI);
						return 0;
					}
					//return true;
				}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
					
					AbsoPropQGenoRegiInserItem newAbsoluteProportionQSyntenyItem;
					if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQSyntenyItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQSyntenyItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQSyntenyItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_START_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQSyntenyItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_START_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQSyntenyItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_CUT_STOP_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else if(holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getQsEnumBlockType().compareTo(AbsoPropQSSyntItem.QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
						newAbsoluteProportionQSyntenyItem = new AbsoPropQGenoRegiInserItem(
								(AbsoPropQSSyntItem) holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem(),
								AbsoPropQCompaQSpanItem.QEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_CUT_STOP_RIGHT,
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStart(),
								holderAbsoluteProportionItem.getAbsoluteProportionQComparableQSSpanItem().getqPercentStop());
					}else{
						System.err.println("Error in defineAbsoluteProportionQSItemsAccordingToZoom isSOutRight && isQWhole instanceof AbsoPropQSSyntItem : block type not recognized");
						//Window.alert("Error in defineAbsoluteProportionQSItemsAccordingToZoom isSOutRight && isQWhole instanceof AbsoPropQSSyntItem : block type not recognized");
						newAbsoluteProportionQSyntenyItem = null;
					}
										//tmpListAbsoluteProportionSyntenyItem.add(newAbsoluteProportionQSyntenyItem);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSpanItem(newAbsoluteProportionQSyntenyItem);
					holderAbsoluteProportionItem.setAbsoluteProportionQComparableQSSpanItem(null);
					
					if(doNotAddToQueueMainArrayButMultipleMatchArray){
						tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().add(holderAbsoluteProportionItem);
						return tmpListHolderAbsoluteProportionItem.get(tmpListHolderAbsoluteProportionItem.size()-1).getListHAPI().size()-1;
					}else{
						SuperHoldAbsoPropItem alHAPI = new SuperHoldAbsoPropItem();
						alHAPI.getListHAPI().add(holderAbsoluteProportionItem);
						tmpListHolderAbsoluteProportionItem.add(alHAPI);
						return 0;
					}
					//return true;
				}else{
					System.err.println("ERROR in q missing target s right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
					//Window.alert("ERROR in q missing target s right defineAbsoPropQSSyntItemAccordingToZoom unrecognized class for object sent");
				}
			} 
		}
		return -1;
		//return false;
	}

	
	
	public void insertGenesAtSpecifiedSyntenyItemIndex(
			ArrayList<SuperHoldAbsoPropItem> alAlHAPIToInsert,
			int indexInFullAssociatedListAbsoluteProportionItemToDisplay,
			int idxToDisplayInListHAPI) {
		
		//System.err.println("insertGenesAtSpecifiedSyntenyItemIndex: "+indexInFullAssociatedListAbsoluteProportionItemToDisplay+" ; idxToDisplayInListHAPI = "+idxToDisplayInListHAPI);

//		HolderAbsoluteProportionItem hapiIT = getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getListHAPI()
//				.get(getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getIdxToDisplayInListHAPI());
//		HolderAbsoluteProportionItem hapiIT = getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getListHAPI()
//				.get(0);
		HolderAbsoluteProportionItem hapiIT = getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getListHAPI()
				.get(idxToDisplayInListHAPI);

		if(hapiIT.getAbsoluteProportionQComparableQSSpanItem() == null){
			System.err.println("Error in insertGenesAtSpecifiedSyntenyItemIndex : getAbsoluteProportionQComparableQSSpanItem is null");
    		//Window.alert("Error in insertGenesAtSpecifiedSyntenyItemIndex : getAbsoluteProportionQComparableQSSpanItem is null");
		}else{
			AbsoPropQCompaQSSpanItem apqcIT = hapiIT.getAbsoluteProportionQComparableQSSpanItem();
			if(apqcIT instanceof AbsoPropQSSyntItem){
				//turn mother synteny slot item into cut start et stop
	    		if(((AbsoPropQSSyntItem)apqcIT).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_HOMOLOGS_BLOCK)==0){

	    			//create homology stop
	    			AbsoPropQSSyntItem newStopApqcIT = new AbsoPropQSSyntItem((AbsoPropQSSyntItem)apqcIT);
					newStopApqcIT.setQsEnumBlockType(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK);
					//newStopApqcIT.setIdxRefOtherPartForQSCutStartOrStopTypes(indexInFullAssociatedListAbsoluteProportionItemToDisplay);
					HolderAbsoluteProportionItem newHAPI = new HolderAbsoluteProportionItem();
					newHAPI.setAbsoluteProportionQComparableQSSpanItem(newStopApqcIT);
					SuperHoldAbsoPropItem newAlHAPI = new SuperHoldAbsoPropItem();
					newAlHAPI.getListHAPI().add(newHAPI);
					
					//create fake count off shoot for cut stop
					for(int m=0;m<getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getListHAPI().size()-1;m++){
						HolderAbsoluteProportionItem newHAPIFakeOffShoot = new HolderAbsoluteProportionItem();
						//not good because draw lines in canvas...
//						AbsoPropQSSyntItem fakeApqcqss = new AbsoPropQSSyntItem();
//						fakeApqcqss.setqPercentStart(0);
//						fakeApqcqss.setqPercentStop(1);
//						fakeApqcqss.setsPercentStart(0);
//						fakeApqcqss.setsPercentStop(1);
//						fakeApqcqss.setQsEnumBlockType(QSEnumBlockType.QS_HOMOLOGS_BLOCK);
//						newHAPIFakeOffShoot.setAbsoluteProportionQComparableQSSpanItem(fakeApqcqss);
						newAlHAPI.getListHAPI().add(newHAPIFakeOffShoot);
					}
					
					getFullAssociatedlistAbsoluteProportionItemToDisplay().add(indexInFullAssociatedListAbsoluteProportionItemToDisplay+1,newAlHAPI);
					
	    			//turn into homology start
	    			((AbsoPropQSSyntItem)apqcIT).setQsEnumBlockType(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK);
	    			//((AbsoPropQSSyntItem)apqcIT).setIdxRefOtherPartForQSCutStartOrStopTypes((indexInFullAssociatedListAbsoluteProportionItemToDisplay+1+alAlHAPIToInsert.size()));
	    			
	    			// insertQItemsIntoListDisplayedItems
	    			getFullAssociatedlistAbsoluteProportionItemToDisplay().addAll(indexInFullAssociatedListAbsoluteProportionItemToDisplay+1, alAlHAPIToInsert);
					
	    		}else if(((AbsoPropQSSyntItem)apqcIT).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK)==0){
	    			
	    			//create homology stop
	    			AbsoPropQSSyntItem newStopApqcIT = new AbsoPropQSSyntItem((AbsoPropQSSyntItem)apqcIT);
					newStopApqcIT.setQsEnumBlockType(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK);
					//newStopApqcIT.setIdxRefOtherPartForQSCutStartOrStopTypes(indexInFullAssociatedListAbsoluteProportionItemToDisplay);
					HolderAbsoluteProportionItem newHAPI = new HolderAbsoluteProportionItem();
					newHAPI.setAbsoluteProportionQComparableQSSpanItem(newStopApqcIT);
					SuperHoldAbsoPropItem newAlHAPI = new SuperHoldAbsoPropItem();
					newAlHAPI.getListHAPI().add(newHAPI);
					//create fake count off shoot for cut stop
					for(int m=0;m<getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getListHAPI().size()-1;m++){
						HolderAbsoluteProportionItem newHAPIFakeOffShoot = new HolderAbsoluteProportionItem();
						newAlHAPI.getListHAPI().add(newHAPIFakeOffShoot);
					}
					getFullAssociatedlistAbsoluteProportionItemToDisplay().add(indexInFullAssociatedListAbsoluteProportionItemToDisplay+1,newAlHAPI);
					
	    			//turn into homology start
	    			((AbsoPropQSSyntItem)apqcIT).setQsEnumBlockType(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK);
	    			//((AbsoPropQSSyntItem)apqcIT).setIdxRefOtherPartForQSCutStartOrStopTypes((indexInFullAssociatedListAbsoluteProportionItemToDisplay+1+alAlHAPIToInsert.size()));
	    			
	    			// insertQItemsIntoListDisplayedItems
	    			getFullAssociatedlistAbsoluteProportionItemToDisplay().addAll(indexInFullAssociatedListAbsoluteProportionItemToDisplay+1, alAlHAPIToInsert);
					
	    			
	    		}else{
	    			System.err.println("Error in insertGenesAtSpecifiedSyntenyItemIndex : not QS_HOMOLOGS_BLOCK or QS_REVERSE_HOMOLOGS_BLOCK");
		    		//Window.alert("Error in insertGenesAtSpecifiedSyntenyItemIndex : not QS_HOMOLOGS_BLOCK or QS_REVERSE_HOMOLOGS_BLOCK");
	    		}
			}else{
				System.err.println("Error in insertGenesAtSpecifiedSyntenyItemIndex :not a synteny item");
	    		//Window.alert("Error in insertGenesAtSpecifiedSyntenyItemIndex : not a synteny item");
			}
		}
		
		recalculateHAPIIndexInFullArrayAndPartialIfNeeded(getFullAssociatedlistAbsoluteProportionItemToDisplay());
		
		
	}

	
	public void insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex(
				ArrayList<SuperHoldAbsoPropItem> alAlHAPIToInsert,
				int indexInFullAssociatedListAbsoluteProportionItemToDisplay,
				final boolean previousSOfNextSlice,
				final boolean nextSOfPreviousSlice) {
		
		HolderAbsoluteProportionItem hapiIT = getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getListHAPI()
				.get(getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getIdxToDisplayInListHAPI());
		
		if(hapiIT.getAbsoluteProportionSComparableSSpanItem() == null){
			//System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex : getAbsoluteProportionSComparableSSpanItem is null");
    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex : getAbsoluteProportionSComparableSSpanItem is null");

			//original AbsoluteProportionSGenomicRegionInsertionItem
			AbsoPropSGenoRegiInserItem apscssiIT = null;
			if(previousSOfNextSlice){
				int idxNextSlice = indexInFullAssociatedListAbsoluteProportionItemToDisplay + 1;
				if(idxNextSlice >= 0 
						&& idxNextSlice < getFullAssociatedlistAbsoluteProportionItemToDisplay().size()){
					HolderAbsoluteProportionItem hapiNext = getFullAssociatedlistAbsoluteProportionItemToDisplay().get(idxNextSlice).getListHAPI()
							.get(getFullAssociatedlistAbsoluteProportionItemToDisplay().get(idxNextSlice).getIdxToDisplayInListHAPI());
					if(hapiNext.getAbsoluteProportionQComparableQSSpanItem() != null){
						if(hapiNext.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
							apscssiIT = ((AbsoPropQSSyntItem)hapiNext.getAbsoluteProportionQComparableQSSpanItem()).getPreviousGenomicRegionSInsertion();
						}else{
							System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex previousSOfNextSlice : not instanceof AbsoPropQSSyntItem");
				    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex previousSOfNextSlice : not instanceof AbsoPropQSSyntItem");
						}
					}else if(hapiNext.getAbsoluteProportionSComparableSSpanItem() != null){
						if(hapiNext.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem){
							if(((AbsoPropSGenoRegiInserItem)hapiNext.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
								apscssiIT = ((AbsoPropSGenoRegiInserItem)hapiNext.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetPreviousGenomicRegionSInsertion();
							}else{
								System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex previousSOfNextSlice missing target : not instanceof AbsoPropQSSyntItem");
					    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex previousSOfNextSlice missing target : not instanceof AbsoPropQSSyntItem");
							}
						}
					}else{
						System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex previousSOfNextSlice : hapiNext.getAbsoluteProportionQComparableQSSpanItem() == null");
			    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex previousSOfNextSlice : hapiNext.getAbsoluteProportionQComparableQSSpanItem() == null");
					}
				}
			}else if(nextSOfPreviousSlice){
				int idxPreviousSlice = indexInFullAssociatedListAbsoluteProportionItemToDisplay - 1;
				if(idxPreviousSlice >= 0 
						&& idxPreviousSlice < getFullAssociatedlistAbsoluteProportionItemToDisplay().size()){
					HolderAbsoluteProportionItem hapiNext = getFullAssociatedlistAbsoluteProportionItemToDisplay().get(idxPreviousSlice).getListHAPI()
							.get(getFullAssociatedlistAbsoluteProportionItemToDisplay().get(idxPreviousSlice).getIdxToDisplayInListHAPI());
					if(hapiNext.getAbsoluteProportionQComparableQSSpanItem() != null){
						if(hapiNext.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSSyntItem){
							apscssiIT = ((AbsoPropQSSyntItem)hapiNext.getAbsoluteProportionQComparableQSSpanItem()).getNextGenomicRegionSInsertion();
						}else{
							System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex nextSOfPreviousSlice : not instanceof AbsoPropQSSyntItem");
				    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex nextSOfPreviousSlice : not instanceof AbsoPropQSSyntItem");
						}
					}else if(hapiNext.getAbsoluteProportionSComparableSSpanItem() != null){
						if(hapiNext.getAbsoluteProportionSComparableSSpanItem() instanceof AbsoPropSGenoRegiInserItem){
							if(((AbsoPropSGenoRegiInserItem)hapiNext.getAbsoluteProportionSComparableSSpanItem()).getSEnumBlockType().toString().startsWith("S_MISSING_TARGET_Q_")){
								apscssiIT = ((AbsoPropSGenoRegiInserItem)hapiNext.getAbsoluteProportionSComparableSSpanItem()).getsIfMissingTargetNextGenomicRegionSInsertion();
							}else{
								System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex nextSOfPreviousSlice missing target : not instanceof AbsoPropQSSyntItem");
					    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex nextSOfPreviousSlice missing target : not instanceof AbsoPropQSSyntItem");
							}
						}
					}else{
						System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex nextSOfPreviousSlice : hapiNext.getAbsoluteProportionQComparableQSSpanItem() == null");
			    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex nextSOfPreviousSlice : hapiNext.getAbsoluteProportionQComparableQSSpanItem() == null");
					}
				}
			}
			
			if(apscssiIT != null){
				
				int idxToInsertStuffAt = -1;
				if(previousSOfNextSlice){
					idxToInsertStuffAt = indexInFullAssociatedListAbsoluteProportionItemToDisplay+1;
				}else if(nextSOfPreviousSlice){
					idxToInsertStuffAt = indexInFullAssociatedListAbsoluteProportionItemToDisplay;
				}else{
					System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex : previousSOfNextSlice and nextSOfPreviousSlice false");
		    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex : previousSOfNextSlice and nextSOfPreviousSlice false");
				}

				//create homology stop and insert
				AbsoPropSGenoRegiInserItem newStopApscIT = new AbsoPropSGenoRegiInserItem(apscssiIT);
    			newStopApscIT.setSEnumBlockType(SEnumBlockType.S_CUT_STOP_S_INSERTION);
				//newStopApqcIT.setIdxRefOtherPartForQSCutStartOrStopTypes(indexInFullAssociatedListAbsoluteProportionItemToDisplay);
				HolderAbsoluteProportionItem newHAPIStop = new HolderAbsoluteProportionItem();
				newHAPIStop.setAbsoluteProportionSComparableSSpanItem(newStopApscIT);
				SuperHoldAbsoPropItem newAlHAPIStop = new SuperHoldAbsoPropItem();
				newAlHAPIStop.getListHAPI().add(newHAPIStop);
				getFullAssociatedlistAbsoluteProportionItemToDisplay().add(idxToInsertStuffAt,newAlHAPIStop);
				
				// insertSItemsIntoListDisplayedItems
				getFullAssociatedlistAbsoluteProportionItemToDisplay().addAll(idxToInsertStuffAt, alAlHAPIToInsert);

				//create homology start and insert
				AbsoPropSGenoRegiInserItem newStartApscIT = new AbsoPropSGenoRegiInserItem(apscssiIT);
				newStartApscIT.setSEnumBlockType(SEnumBlockType.S_CUT_START_S_INSERTION);
				//newStopApqcIT.setIdxRefOtherPartForQSCutStartOrStopTypes(indexInFullAssociatedListAbsoluteProportionItemToDisplay);
				HolderAbsoluteProportionItem newHAPI = new HolderAbsoluteProportionItem();
				newHAPI.setAbsoluteProportionSComparableSSpanItem(newStartApscIT);
				SuperHoldAbsoPropItem newAlHAPI = new SuperHoldAbsoPropItem();
				newAlHAPI.getListHAPI().add(newHAPI);
				getFullAssociatedlistAbsoluteProportionItemToDisplay().add(idxToInsertStuffAt,newAlHAPI);
				
			}else{
				System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex : apscssiIT == null");
	    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex : apscssiIT == null");
			}
			
		}else{
			AbsoPropSCompaSSpanItem apscIT = hapiIT.getAbsoluteProportionSComparableSSpanItem();
			if(apscIT instanceof AbsoPropSGenoRegiInserItem){
	    		if(((AbsoPropSGenoRegiInserItem)apscIT).getSEnumBlockType().compareTo(SEnumBlockType.S_INSERTION)==0){
	    			
	    			//create homology stop
	    			AbsoPropSGenoRegiInserItem newStopApscIT = new AbsoPropSGenoRegiInserItem((AbsoPropSGenoRegiInserItem)apscIT);
	    			newStopApscIT.setSEnumBlockType(SEnumBlockType.S_CUT_STOP_S_INSERTION);
					//newStopApqcIT.setIdxRefOtherPartForQSCutStartOrStopTypes(indexInFullAssociatedListAbsoluteProportionItemToDisplay);
					HolderAbsoluteProportionItem newHAPI = new HolderAbsoluteProportionItem();
					newHAPI.setAbsoluteProportionSComparableSSpanItem(newStopApscIT);
					SuperHoldAbsoPropItem newAlHAPI = new SuperHoldAbsoPropItem();
					newAlHAPI.getListHAPI().add(newHAPI);
					getFullAssociatedlistAbsoluteProportionItemToDisplay().add(indexInFullAssociatedListAbsoluteProportionItemToDisplay+1,newAlHAPI);
					
	    			//turn into homology start
	    			((AbsoPropSGenoRegiInserItem)apscIT).setSEnumBlockType(SEnumBlockType.S_CUT_START_S_INSERTION);
	    			//((AbsoPropQSSyntItem)apqcIT).setIdxRefOtherPartForQSCutStartOrStopTypes((indexInFullAssociatedListAbsoluteProportionItemToDisplay+1+alAlHAPIToInsert.size()));
	    			
	    			// insertSItemsIntoListDisplayedItems
	    			getFullAssociatedlistAbsoluteProportionItemToDisplay().addAll(indexInFullAssociatedListAbsoluteProportionItemToDisplay+1, alAlHAPIToInsert);
					
	    		}else{
	    			System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex : not S_INSERTION");
		    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex : not S_INSERTION");
	    		}
			}else{
				System.err.println("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex :not a AbsoluteProportionSGenomicRegionInsertionItem item");
	    		//Window.alert("Error in insertGenesAtSpecifiedSGenomicRegionInsertionItemIndex : not a AbsoluteProportionSGenomicRegionInsertionItem item");
			}
		}
		
		
		recalculateHAPIIndexInFullArrayAndPartialIfNeeded(getFullAssociatedlistAbsoluteProportionItemToDisplay());
		
		
	}

//	public void insertGenesAtSpecifiedQGenomicRegionInsertionItemIndex(
//			ArrayList<AbsoluteProportionQComparableItem> listAbsoluteProportionQGeneInsertionItemSent,
//			int indexInFullOrPartialAssociatedListAbsoluteProportionItemToDisplay) {
	public void insertGenesAtSpecifiedQGenomicRegionInsertionItemIndex(
			ArrayList<SuperHoldAbsoPropItem> alAlHAPIToInsert,
			int indexInFullAssociatedListAbsoluteProportionItemToDisplay) {
		
		HolderAbsoluteProportionItem hapiIT = getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getListHAPI()
				.get(getFullAssociatedlistAbsoluteProportionItemToDisplay().get(indexInFullAssociatedListAbsoluteProportionItemToDisplay).getIdxToDisplayInListHAPI());
		
		if(hapiIT.getAbsoluteProportionQComparableQSpanItem() == null){
			System.err.println("Error in insertGenesAtSpecifiedQGenomicRegionInsertionItemIndex : getAbsoluteProportionQComparableSSpanItem is null");
    		//Window.alert("Error in insertGenesAtSpecifiedQGenomicRegionInsertionItemIndex : getAbsoluteProportionQComparableSSpanItem is null");
		}else{
			AbsoPropQCompaQSpanItem apqcIT = hapiIT.getAbsoluteProportionQComparableQSpanItem();
			if(apqcIT instanceof AbsoPropQGenoRegiInserItem){
	    		if(((AbsoPropQGenoRegiInserItem)apqcIT).getqEnumBlockType().compareTo(QEnumBlockType.Q_INSERTION)==0){
			
	    			//create homology stop
	    			AbsoPropQGenoRegiInserItem newStopApqcIT = new AbsoPropQGenoRegiInserItem((AbsoPropQGenoRegiInserItem)apqcIT);
	    			newStopApqcIT.setqEnumBlockType(QEnumBlockType.Q_CUT_STOP_Q_INSERTION);
					//newStopApqcIT.setIdxRefOtherPartForQSCutStartOrStopTypes(indexInFullAssociatedListAbsoluteProportionItemToDisplay);
					HolderAbsoluteProportionItem newHAPI = new HolderAbsoluteProportionItem();
					newHAPI.setAbsoluteProportionQComparableQSpanItem(newStopApqcIT);
					SuperHoldAbsoPropItem newAlHAPI = new SuperHoldAbsoPropItem();
					newAlHAPI.getListHAPI().add(newHAPI);
					getFullAssociatedlistAbsoluteProportionItemToDisplay().add(indexInFullAssociatedListAbsoluteProportionItemToDisplay+1,newAlHAPI);
					
	    			//turn into homology start
	    			((AbsoPropQGenoRegiInserItem)apqcIT).setqEnumBlockType(QEnumBlockType.Q_CUT_START_Q_INSERTION);
	    			//((AbsoPropQSSyntItem)apqcIT).setIdxRefOtherPartForQSCutStartOrStopTypes((indexInFullAssociatedListAbsoluteProportionItemToDisplay+1+alAlHAPIToInsert.size()));
	    			
	    			// insertQItemsIntoListDisplayedItems
	    			getFullAssociatedlistAbsoluteProportionItemToDisplay().addAll(indexInFullAssociatedListAbsoluteProportionItemToDisplay+1, alAlHAPIToInsert);
					
	    			
	    		}else{
	    			System.err.println("Error in insertGenesAtSpecifiedQGenomicRegionInsertionItemIndex : not Q_INSERTION");
		    		//Window.alert("Error in insertGenesAtSpecifiedQGenomicRegionInsertionItemIndex : not Q_INSERTION");
	    		}
			}else{
				System.err.println("Error in insertGenesAtSpecifiedQGenomicRegionInsertionItemIndex :not a AbsoluteProportionQGenomicRegionInsertionItem item");
	    		//Window.alert("Error in insertGenesAtSpecifiedQGenomicRegionInsertionItemIndex : not a AbsoluteProportionQGenomicRegionInsertionItem item");
			}
		}
		
		
		recalculateHAPIIndexInFullArrayAndPartialIfNeeded(getFullAssociatedlistAbsoluteProportionItemToDisplay());
		
		
		
		
	}
	

	public void deleteGeneHomologyItemsAtSpecifiedIndexInFullArray(
			int startIndexInFullArrayIT, int stopIndexInFullArrayIT,
			boolean doDeleteCutStartAsWell) {
		
		//System.err.println("startIndexInFullArrayIT : "+startIndexInFullArrayIT+" ; stopIndexInFullArrayIT = "+stopIndexInFullArrayIT);
		for(int i=stopIndexInFullArrayIT;i>=startIndexInFullArrayIT;i--){
			if(i==startIndexInFullArrayIT && !doDeleteCutStartAsWell){
				//System.err.println("modifing "+i);
				//cut start, transform back into what it was previously...
//				if(this.fullAssociatedListAbsoluteProportionItemToDisplay.get(i).size() != 1){
//					System.err.println("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : size array != 1");
//		    		Window.alert("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : size array != 1");
//				}else{
				HolderAbsoluteProportionItem hapiIT = getFullAssociatedlistAbsoluteProportionItemToDisplay().get(i).getListHAPI()
						.get(getFullAssociatedlistAbsoluteProportionItemToDisplay().get(i).getIdxToDisplayInListHAPI());
				if(hapiIT.getAbsoluteProportionQComparableQSSpanItem() != null){
					AbsoPropQCompaQSSpanItem apqcIT = hapiIT.getAbsoluteProportionQComparableQSSpanItem();
					if(apqcIT instanceof AbsoPropQSSyntItem){
			    		if(((AbsoPropQSSyntItem)apqcIT).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0){
			    			//turn back into homology
			    			((AbsoPropQSSyntItem)apqcIT).setQsEnumBlockType(QSEnumBlockType.QS_HOMOLOGS_BLOCK);
			    		}else if(((AbsoPropQSSyntItem)apqcIT).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
			    			//turn back into reverse homo
			    			((AbsoPropQSSyntItem)apqcIT).setQsEnumBlockType(QSEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK);
			    		}else{
			    			System.err.println("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : not QS_CUT_START_HOMOLOGS_BLOCK or QS_CUT_START_REVERSE_HOMOLOGS_BLOCK");
				    		//Window.alert("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : not QS_CUT_START_HOMOLOGS_BLOCK or QS_CUT_START_REVERSE_HOMOLOGS_BLOCK");
			    		}
					}else{
						System.err.println("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : i 0 not a AbsoPropQSSyntItem item");
			    		//Window.alert("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : i 0 not a AbsoPropQSSyntItem item");
					}
				}else if(hapiIT.getAbsoluteProportionQComparableQSpanItem() != null){
					AbsoPropQCompaQSpanItem apqcIT = hapiIT.getAbsoluteProportionQComparableQSpanItem();
					if(apqcIT instanceof AbsoPropQGenoRegiInserItem){
						//turn back into homology
		    			((AbsoPropQGenoRegiInserItem)apqcIT).setqEnumBlockType(QEnumBlockType.Q_INSERTION);
					}else{
		    			System.err.println("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : not AbsoluteProportionQGenomicRegionInsertionItem");
			    		//Window.alert("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : not AbsoluteProportionQGenomicRegionInsertionItem");
		    		}
				}else if(hapiIT.getAbsoluteProportionSComparableSSpanItem() != null){
					AbsoPropSCompaSSpanItem apqcIT = hapiIT.getAbsoluteProportionSComparableSSpanItem();
					if(apqcIT instanceof AbsoPropSGenoRegiInserItem){
						//turn back into homology
		    			((AbsoPropSGenoRegiInserItem)apqcIT).setSEnumBlockType(SEnumBlockType.S_INSERTION);
					}else{
		    			System.err.println("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : not AbsoluteProportionSGenomicRegionInsertionItem");
			    		//Window.alert("Error in deleteGeneHomologyItemsAtSpecifiedIndexInFullArray : not AbsoluteProportionSGenomicRegionInsertionItem");
		    		}
				}
				
				
			}else{
				//System.err.println("deleting "+i);
				getFullAssociatedlistAbsoluteProportionItemToDisplay().remove(i);
			}
			
		}
		
		recalculateHAPIIndexInFullArrayAndPartialIfNeeded(getFullAssociatedlistAbsoluteProportionItemToDisplay());
		
	}


	public ArrayList<Integer> getAlAbsoPropQSGeneHomoItemQsSGeneId() {
		ArrayList<Integer> alToReturn = new ArrayList<>();
		for ( int i=0 ; 
				i < getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().size()
				; i++
				) {
			SuperHoldAbsoPropItem sIT = getCopyFullOrAlreadyMadePartialListAbsoluteProportionItemToDisplayAccordingly().get(i);
			for (int j=0;j<sIT.getListHAPI().size();j++) {
				HolderAbsoluteProportionItem hIT = sIT.getListHAPI().get(j);							
				if ( hIT.getAbsoluteProportionQComparableQSSpanItem() != null ){
					if (hIT.getAbsoluteProportionQComparableQSSpanItem() instanceof AbsoPropQSGeneHomoItem) {
						AbsoPropQSGeneHomoItem aIT = (AbsoPropQSGeneHomoItem) hIT.getAbsoluteProportionQComparableQSSpanItem();
						alToReturn.add(aIT.getQsSGeneId());
					}
				}
			}
		}
		return alToReturn;
	}


}
