package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;


public class AbsoPropQCompaQSSpanItem extends AbsoPropQCompaItem implements IsSerializable {

	public enum QSEnumBlockType implements IsSerializable {
		//basic block
		QS_HOMOLOGS_BLOCK, QS_REVERSE_HOMOLOGS_BLOCK,
		
		//cut start and stop
		QS_CUT_START_HOMOLOGS_BLOCK, QS_CUT_STOP_HOMOLOGS_BLOCK,
		QS_CUT_START_REVERSE_HOMOLOGS_BLOCK, QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK
		
	}

	private QSEnumBlockType qsEnumBlockType = null;

	private double qPercentStop = 2;
	private double sPercentStart = -1;
	private double sPercentStop = 2;

	
	public AbsoPropQCompaQSSpanItem(){
		
	}
	
	public AbsoPropQCompaQSSpanItem(AbsoPropQCompaQSSpanItem absoluteProportionQComparableQSSpanItemSent){
		setQsEnumBlockType(absoluteProportionQComparableQSSpanItemSent.getQsEnumBlockType());
		setsPercentStart(absoluteProportionQComparableQSSpanItemSent.getsPercentStart());
		setsPercentStop(absoluteProportionQComparableQSSpanItemSent.getsPercentStop());
		setqPercentStart(absoluteProportionQComparableQSSpanItemSent.getqPercentStart());
		setqPercentStop(absoluteProportionQComparableQSSpanItemSent.getqPercentStop());
	}

	public void setqPercentStop(double qPercentStop) {
		this.qPercentStop = qPercentStop;
	}


	public double getqPercentStop() {
		return qPercentStop;
		/*if(qsEnumBlockType.compareTo(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0
				|| qsEnumBlockType.compareTo(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
			return getqPercentStart();
		}else{
			return qPercentStop;
		}*/
	}

	public void setsPercentStart(double sPercentStart) {
		this.sPercentStart = sPercentStart;
	}


	public double getsPercentStart() {
		return sPercentStart;
		/*if(qsEnumBlockType.compareTo(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0
				|| qsEnumBlockType.compareTo(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
			return getsPercentStop();
		}else{
			return sPercentStart;
		}*/
	}


	public void setsPercentStop(double sPercentStop) {
		this.sPercentStop = sPercentStop;
	}


	public double getsPercentStop() {
		return sPercentStop;
		/*if(qsEnumBlockType.compareTo(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0
				|| qsEnumBlockType.compareTo(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
			return getsPercentStart();
		}else{
			return sPercentStop;
		}*/
	}

	public void setQsEnumBlockType(QSEnumBlockType qsEnumBlockType) {
		this.qsEnumBlockType = qsEnumBlockType;
	}

	public QSEnumBlockType getQsEnumBlockType() {
		return qsEnumBlockType;
	}


}
