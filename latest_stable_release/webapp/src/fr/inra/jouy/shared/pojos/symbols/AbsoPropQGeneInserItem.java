package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.TransAbsoPropResuGeneSet;
import fr.inra.jouy.shared.pojos.applicationItems.GeneWidgetStyleItem;


public class AbsoPropQGeneInserItem extends AbsoPropQElemItem implements IsSerializable {

	//change in SComparableGeneInsertionItem too
	//change in LightGeneItem too
	private int qGeneId = -1;
	//private String qMostSignificantGeneName = null;
	private String qName = null;
	private String qLocusTag = null;
	private int qStrand = 0; // CAN BE 1 OR -1
	private GeneWidgetStyleItem qStyleItem = null;
	private int qPbStartGeneInElement = -1;
	private int qPbStopGeneInElement = -1;
	
	private int ifMissingTargetSGeneId = -1;
	//private String ifMissingTargetSMostSignificantGeneName = null;
	private String ifMissingTargetSName = null;
	private String ifMissingTargetSLocusTag = null;
	private int ifMissingTargetSStrand = 0; // CAN BE 1 OR -1
	//private SuperHolderAbsoluteProportionItem ifMissingTargetListOfOtherHomologiesForTheQGene = new SuperHolderAbsoluteProportionItem();//can be AbsoluteProportionQSGeneHomologyItem or AbsoluteProportionQGeneInsertionItem missing target
	
	private int qOrigamiAlignmentPairsType = -1;
	private long qOrigamiAlignmentId = 0; // can be negative if isDataFromMirrorQuery
	
	public AbsoPropQGeneInserItem(){
		
	}
	

	public AbsoPropQGeneInserItem(AbsoPropQGeneInserItem absoluteProportionQGeneInsertionItemSent){
		super((AbsoPropQElemItem)absoluteProportionQGeneInsertionItemSent);
		
		setQGeneId(absoluteProportionQGeneInsertionItemSent.getQGeneId());
		setQLocusTag(absoluteProportionQGeneInsertionItemSent.getQLocusTagAsRawString());
		setQName(absoluteProportionQGeneInsertionItemSent.getQNameAsRawString());
		setQStrand(absoluteProportionQGeneInsertionItemSent.getQStrand());
		setqStyleItem(absoluteProportionQGeneInsertionItemSent.getqStyleItem());
		setIfMissingTargetSGeneId(absoluteProportionQGeneInsertionItemSent.getIfMissingTargetSGeneId());
		setIfMissingTargetSLocusTag(absoluteProportionQGeneInsertionItemSent.getIfMissingTargetSLocusTagAsRawString());
		setIfMissingTargetSName(absoluteProportionQGeneInsertionItemSent.getIfMissingTargetSNameAsRawString());
		setIfMissingTargetSStrand(absoluteProportionQGeneInsertionItemSent.getIfMissingTargetSStrand());
		setqPbStartGeneInElement(absoluteProportionQGeneInsertionItemSent.getqPbStartGeneInElement());
		setqPbStopGeneInElement(absoluteProportionQGeneInsertionItemSent.getqPbStopGeneInElement());

		setqOrigamiAlignmentPairsType(absoluteProportionQGeneInsertionItemSent.getqOrigamiAlignmentPairsType());
		setqOrigamiAlignmentId(absoluteProportionQGeneInsertionItemSent.getqOrigamiAlignmentId());
		
	}
	
	public AbsoPropQGeneInserItem(TransAbsoPropResuGeneSet transientAbsoluteProportionResultGeneSetSent){
		setQGeneId(transientAbsoluteProportionResultGeneSetSent.getqGeneId());
		setqPbStartGeneInElement(transientAbsoluteProportionResultGeneSetSent.getqStart());
		setqPbStopGeneInElement(transientAbsoluteProportionResultGeneSetSent.getqStop());
		setqOrigamiElementId(transientAbsoluteProportionResultGeneSetSent.getqOrigamiElementId());
	}

	
	public AbsoPropQGeneInserItem(AbsoPropQSGeneHomoItem apqsghiSent,
			QEnumBlockType qEnumBlockTypeSent, double qPercentStartSent, double qPercentStopSent){
		super((AbsoPropQSElemItem)apqsghiSent,
				qEnumBlockTypeSent, qPercentStartSent, qPercentStopSent);
		
		setQGeneId(apqsghiSent.getQsQGeneId());
		setQLocusTag(apqsghiSent.getQsQLocusTagAsRawString());
		setQName(apqsghiSent.getQsQNameAsRawString());
		setQStrand(apqsghiSent.getQsQStrand());
		setqStyleItem(apqsghiSent.getQsStyleItem());
		setIfMissingTargetSGeneId(apqsghiSent.getQsSGeneId());
		setIfMissingTargetSLocusTag(apqsghiSent.getQsSLocusTagAsRawString());
		setIfMissingTargetSName(apqsghiSent.getQsSNameAsRawString());
		setIfMissingTargetSStrand(apqsghiSent.getQsSStrand());
		setqPbStartGeneInElement(apqsghiSent.getQsQPbStartGeneInElement());
		setqPbStopGeneInElement(apqsghiSent.getQsQPbStopGeneInElement());

		setqOrigamiAlignmentPairsType(apqsghiSent.getQsOrigamiAlignmentPairsType());
		setqOrigamiAlignmentId(apqsghiSent.getSyntenyOrigamiAlignmentId());
		
	}
	
	
	public int getQGeneId() {
		return qGeneId;
	}
	
	public void setQGeneId(int qGeneId) {
		this.qGeneId = qGeneId;
	}
	
	public String getQMostSignificantGeneNameAsHTMLPlusLink() {
		if(qName != null && qName.length() > 0){
//			if (qName.contains("UniProtKB") == true ) {
//	            String locus = qName.replaceAll("\\{UniProtKB/.+:.+", "");
//	            String link = qName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
//	            link = link.replaceAll("\\}", "");
//	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
//			}else {
//		        return qName;
//		    }
			return getQNameAsHTMLPlusLink();
		}else if (qLocusTag != null && qLocusTag.length() > 0){
//			String strippedLocusTag = null;
//			strippedLocusTag = qLocusTag.split("\\s", 2)[0];
//			if(strippedLocusTag.length() > 0){
//				return strippedLocusTag;
//			}else{
//				return qLocusTag;
//			}
//			if (qLocusTag.contains("UniProtKB") == true ) {
//	            String locus = qLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
//	            String link = qLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
//	            link = link.replaceAll("\\}", "");
//	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
//			}else {
//	            return qLocusTag;
//			}
			return getQLocusTagAsHTMLPlusLink();
		}else{
			//setQMostSignificantGeneName("NO_NAME");
			return "NO_NAME";
		}
		
	}
	
	public String getQMostSignificantGeneNameAsStrippedString() {
		if(qName != null && qName.length() > 0){
//			if (qName.contains("UniProtKB") == true ) {
//	            String locus = qName.replaceAll("\\{UniProtKB/.+:.+", "");
//				return locus;
//			}else {
//				return qName;
//	        }
			return getQNameAsStrippedString();
		}else if (qLocusTag != null && qLocusTag.length() > 0){
//			String strippedLocusTag = null;
//			strippedLocusTag = qLocusTag.split("\\s", 2)[0];
//			if(strippedLocusTag.length() > 0){
//				return strippedLocusTag;
//			}else{
//				return qLocusTag;
//			}
//			if (qLocusTag.contains("UniProtKB") == true ) {
//	            String locus = qLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
//	            return locus;
//			}else {
//				return qLocusTag;
//	        }
			return getQLocusTagAsStrippedString();
		}else{
			//setQMostSignificantGeneName("NO_NAME");
			return "NO_NAME";
		}
	}
	
//	public void setQMostSignificantGeneName(String mostSignificantGeneName) {
//		this.qMostSignificantGeneName = mostSignificantGeneName;
//	}
	
	public String getQNameAsRawString() {
		return qName;
	}
	public String getQNameAsStrippedString() {
		if(qName != null){
			if (qName.contains("UniProtKB") == true ) {
	            String locus = qName.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return qName;
	        }
		}else{
			return "";
		}
		
	}
	public String getQNameAsHTMLPlusLink() {
		if(qName != null){
			if (qName.contains("UniProtKB") == true ) {
	            String locus = qName.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = qName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return qName;
		    }
		}else{
			return "";
		}
		
	}
	public void setQName(String qName) {
		this.qName = qName;
	}
	public String getQLocusTagAsRawString() {
		return qLocusTag;
	}
	public String getQLocusTagAsStrippedString() {
		if(qLocusTag != null){
			if (qLocusTag.contains("UniProtKB") == true ) {
	            String locus = qLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return qLocusTag;
	        }
		}else{
			return "";
		}
		
	}
	public String getQLocusTagAsHTMLPlusLink() {
		if(qLocusTag != null){
			if (qLocusTag.contains("UniProtKB") == true ) {
	            String locus = qLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = qLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return qLocusTag;
		    }
		}else{
			return "";
		}
		
	}
	
	public void setQLocusTag(String qLocusTag) {
		this.qLocusTag = qLocusTag;
	}

	public int getQStrand() {
		return qStrand;
	}
	public void setQStrand(int qStrand) {
		this.qStrand = qStrand;
	}


	public GeneWidgetStyleItem getqStyleItem() {
		if(qStyleItem == null){
			qStyleItem = new GeneWidgetStyleItem();
		}
		return qStyleItem;
	}


	public void setqStyleItem(GeneWidgetStyleItem qStyleItem) {
		this.qStyleItem = qStyleItem;
	}

	public int getIfMissingTargetSGeneId() {
		return ifMissingTargetSGeneId;
	}


	public void setIfMissingTargetSGeneId(int ifMissingTargetSGeneId) {
		this.ifMissingTargetSGeneId = ifMissingTargetSGeneId;
	}
	
	public String getIfMissingTargetSMostSignificantGeneNameAsHTMLPlusLink() {
		if(ifMissingTargetSName != null && ifMissingTargetSName.length() > 0){
			return getIfMissingTargetSNameAsHTMLPlusLink();
		}else if (ifMissingTargetSLocusTag != null && ifMissingTargetSLocusTag.length() > 0){
			return getIfMissingTargetSLocusTagAsHTMLPlusLink();
		}else{
			return "NO_NAME";
		}
	}

	public String getIfMissingTargetSMostSignificantGeneNameAsStrippedString() {
		if(ifMissingTargetSName != null && ifMissingTargetSName.length() > 0){
			return getIfMissingTargetSNameAsStrippedString();
		}else if (ifMissingTargetSLocusTag != null && ifMissingTargetSLocusTag.length() > 0){
			return getIfMissingTargetSLocusTagAsStrippedString();
		}else{
			return "NO_NAME";
		}
	}

	public String getIfMissingTargetSNameAsRawString() {
		return ifMissingTargetSName;
	}
	
	public String getIfMissingTargetSNameAsStrippedString() {
		if(ifMissingTargetSName != null){
			if (ifMissingTargetSName.contains("UniProtKB") == true ) {
	            String locus = ifMissingTargetSName.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return ifMissingTargetSName;
	        }
		}else{
			return "";
		}
		
	}
	public String getIfMissingTargetSNameAsHTMLPlusLink() {
		if(ifMissingTargetSName != null){
			if (ifMissingTargetSName.contains("UniProtKB") == true ) {
	            String locus = ifMissingTargetSName.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = ifMissingTargetSName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return ifMissingTargetSName;
		    }
		}else{
			return "";
		}
		
	}
	
	public void setIfMissingTargetSName(String ifMissingTargetSName) {
		this.ifMissingTargetSName = ifMissingTargetSName;
	}


	public String getIfMissingTargetSLocusTagAsRawString() {
		return ifMissingTargetSLocusTag;
	}
	public String getIfMissingTargetSLocusTagAsStrippedString() {
		if(ifMissingTargetSLocusTag != null){
			if (ifMissingTargetSLocusTag.contains("UniProtKB") == true ) {
	            String locus = ifMissingTargetSLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return ifMissingTargetSLocusTag;
	        }
		}else{
			return "";
		}
		
	}
	public String getIfMissingTargetSLocusTagAsHTMLPlusLink() {
		if(ifMissingTargetSLocusTag != null){
			if (ifMissingTargetSLocusTag.contains("UniProtKB") == true ) {
	            String locus = ifMissingTargetSLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = ifMissingTargetSLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return ifMissingTargetSLocusTag;
		    }
		}else{
			return "";
		}
		
	}
	
	public void setIfMissingTargetSLocusTag(String ifMissingTargetSLocusTag) {
		this.ifMissingTargetSLocusTag = ifMissingTargetSLocusTag;
	}


	public int getIfMissingTargetSStrand() {
		return ifMissingTargetSStrand;
	}


	public void setIfMissingTargetSStrand(int ifMissingTargetSStrand) {
		this.ifMissingTargetSStrand = ifMissingTargetSStrand;
	}


	public int getqPbStartGeneInElement() {
		return qPbStartGeneInElement;
	}


	public void setqPbStartGeneInElement(int qPbStartGeneInElement) {
		this.qPbStartGeneInElement = qPbStartGeneInElement;
	}


	public int getqPbStopGeneInElement() {
		return qPbStopGeneInElement;
	}


	public void setqPbStopGeneInElement(int qPbStopGeneInElement) {
		this.qPbStopGeneInElement = qPbStopGeneInElement;
	}


	public int getqOrigamiAlignmentPairsType() {
		return qOrigamiAlignmentPairsType;
	}


	public void setqOrigamiAlignmentPairsType(int qOrigamiAlignmentPairsType) {
		this.qOrigamiAlignmentPairsType = qOrigamiAlignmentPairsType;
	}

	//Done isDataFromMirrorQuery
	public long getqOrigamiAlignmentId() {
		return qOrigamiAlignmentId;
	}
	
	//Done isDataFromMirrorQuery
	public void setqOrigamiAlignmentId(long l) {
		this.qOrigamiAlignmentId = l;
	}


}
