package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;


public class AbsoPropQSElemItem extends AbsoPropQCompaQSSpanItem implements IsSerializable {

	private String qsQElementType = null;
	private int qsQOrigamiElementId = -1;
	private String qsQAccnum = null;
	private int qsQElementNumberGene = -1;
	private int qsQOrigamiOrganismId = -1;
	private int qsQSizeOfElementinPb = -1;
	private int qsQPbStartOfElementInOrga = -1;
	private int qsQSizeOfOrganismInPb = -1;
	
	private String qsSElementType = null;
	private int qsSOrigamiElementId = -1;
	private String qsSAccnum = null;
	private int qsSElementNumberGene = -1;
	private int qsSOrigamiOrganismId = -1;
	private int qsSSizeOfElementinPb = -1;
	private int qsSPbStartOfElementInOrga = -1;
	private int qsSSizeOfOrganismInPb = -1;
	
	public AbsoPropQSElemItem(){
		
	}
	
	public AbsoPropQSElemItem(AbsoPropQSElemItem absoluteProportionQSElementsItemSent){
		super((AbsoPropQSElemItem)absoluteProportionQSElementsItemSent);
		
		setQsQAccnum(absoluteProportionQSElementsItemSent.getQsQAccnum());
		setQsQOrigamiElementId(absoluteProportionQSElementsItemSent.getQsQOrigamiElementId());
		setQsQElementType(absoluteProportionQSElementsItemSent.getQsQElementType());
		setQsQElementNumberGene(absoluteProportionQSElementsItemSent.getQsQElementNumberGene());
		setQsQOrigamiOrganismId(absoluteProportionQSElementsItemSent.getQsQOrigamiOrganismId());
		setQsQSizeOfElementinPb(absoluteProportionQSElementsItemSent.getQsQSizeOfElementinPb());
		setQsQPbStartOfElementInOrga(absoluteProportionQSElementsItemSent.getQsQPbStartOfElementInOrga());
		setQsQSizeOfOrganismInPb(absoluteProportionQSElementsItemSent.getQsQSizeOfOrganismInPb());
		
		setQsSAccnum(absoluteProportionQSElementsItemSent.getQsSAccnum());
		setQsSOrigamiElementId(absoluteProportionQSElementsItemSent.getQsSOrigamiElementId());
		setQsSElementType(absoluteProportionQSElementsItemSent.getQsSElementType());
		setQsSElementNumberGene(absoluteProportionQSElementsItemSent.getQsSElementNumberGene());
		setQsSOrigamiOrganismId(absoluteProportionQSElementsItemSent.getQsSOrigamiOrganismId());
		setQsSSizeOfElementinPb(absoluteProportionQSElementsItemSent.getQsSSizeOfElementinPb());
		setQsSPbStartOfElementInOrga(absoluteProportionQSElementsItemSent.getQsSPbStartOfElementInOrga());
		setQsSSizeOfOrganismInPb(absoluteProportionQSElementsItemSent.getQsSSizeOfOrganismInPb());
	}
	
	public String getQsQElementType() {
		return qsQElementType;
	}
	public void setQsQElementType(String qsQElementType) {
		this.qsQElementType = qsQElementType;
	}
	public int getQsQOrigamiElementId() {
		return qsQOrigamiElementId;
	}
	public void setQsQOrigamiElementId(int qsQElementId) {
		this.qsQOrigamiElementId = qsQElementId;
	}
	public String getQsQAccnum() {
		return qsQAccnum;
	}
	public void setQsQAccnum(String qsQAccnum) {
		this.qsQAccnum = qsQAccnum;
	}
	public int getQsQElementNumberGene() {
		return qsQElementNumberGene;
	}
	public void setQsQElementNumberGene(int qsQElementNumberGene) {
		this.qsQElementNumberGene = qsQElementNumberGene;
	}
	public String getQsSElementType() {
		return qsSElementType;
	}
	public void setQsSElementType(String qsSElementType) {
		this.qsSElementType = qsSElementType;
	}
	public int getQsSOrigamiElementId() {
		return qsSOrigamiElementId;
	}
	public void setQsSOrigamiElementId(int qsSElementId) {
		this.qsSOrigamiElementId = qsSElementId;
	}
	public String getQsSAccnum() {
		return qsSAccnum;
	}
	public void setQsSAccnum(String qsSAccnum) {
		this.qsSAccnum = qsSAccnum;
	}
	public int getQsSElementNumberGene() {
		return qsSElementNumberGene;
	}
	public void setQsSElementNumberGene(int qsSElementNumberGene) {
		this.qsSElementNumberGene = qsSElementNumberGene;
	}

	public int getQsQOrigamiOrganismId() {
		return qsQOrigamiOrganismId;
	}

	public void setQsQOrigamiOrganismId(int qsQOrigamiOrganismId) {
		this.qsQOrigamiOrganismId = qsQOrigamiOrganismId;
	}

	public int getQsQSizeOfElementinPb() {
		return qsQSizeOfElementinPb;
	}

	public void setQsQSizeOfElementinPb(int qsQSizeOfElementinPb) {
		this.qsQSizeOfElementinPb = qsQSizeOfElementinPb;
	}

	public int getQsSOrigamiOrganismId() {
		return qsSOrigamiOrganismId;
	}

	public void setQsSOrigamiOrganismId(int qsSOrigamiOrganismId) {
		this.qsSOrigamiOrganismId = qsSOrigamiOrganismId;
	}

	public int getQsSSizeOfElementinPb() {
		return qsSSizeOfElementinPb;
	}

	public void setQsSSizeOfElementinPb(int qsSSizeOfElementinPb) {
		this.qsSSizeOfElementinPb = qsSSizeOfElementinPb;
	}

	public int getQsQPbStartOfElementInOrga() {
		return qsQPbStartOfElementInOrga;
	}

	
	public void setQsQPbStartOfElementInOrga(int qsQPbStartOfElementInOrga) {
		this.qsQPbStartOfElementInOrga = qsQPbStartOfElementInOrga;
	}

	public int getQsQSizeOfOrganismInPb() {
		return qsQSizeOfOrganismInPb;
	}

	public void setQsQSizeOfOrganismInPb(int qsQSizeOfOrganismInPb) {
		this.qsQSizeOfOrganismInPb = qsQSizeOfOrganismInPb;
	}

	public int getQsSPbStartOfElementInOrga() {
		return qsSPbStartOfElementInOrga;
	}

	public void setQsSPbStartOfElementInOrga(int qsSPbStartOfElementInOrga) {
		this.qsSPbStartOfElementInOrga = qsSPbStartOfElementInOrga;
	}

	public int getQsSSizeOfOrganismInPb() {
		return qsSSizeOfOrganismInPb;
	}

	public void setQsSSizeOfOrganismInPb(int qsSSizeOfOrganismInPb) {
		this.qsSSizeOfOrganismInPb = qsSSizeOfOrganismInPb;
	}
	
	
	
}
