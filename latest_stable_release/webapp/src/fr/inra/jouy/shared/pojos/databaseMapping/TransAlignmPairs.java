package fr.inra.jouy.shared.pojos.databaseMapping;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.interfaceIs.IsStringifyToJSON;

public class TransAlignmPairs implements IsSerializable, IsStringifyToJSON {

	private long alignmentId = 0; // can be negative if isDataFromMirrorQuery
	private int qGeneId = -1;
	private int sGeneId = -1;
	private int type = -1;
	//type 1 : strong homolog with avg score 220, average evalue 5.76e-5
	//type 2 : weaker homolog with avg score 137, average evalue 2.2e-4
	//type 3 mismatch
	//type 4 s insertion
	//type 5 q insertion
	
	public TransAlignmPairs(){
	}


	public TransAlignmPairs(long alignmentId, int qGeneId, int sGeneId, int type) {
		super();
		this.alignmentId = alignmentId;
		this.qGeneId = qGeneId;
		this.sGeneId = sGeneId;
		this.type = type;
	}


	//Done isDataFromMirrorQuery
	public long getAlignmentId() {
		return alignmentId;
	}

	// Done isDataFromMirrorQuery
	public void setAlignmentId(long l) {
		this.alignmentId = l;
	}

	public int getqGeneId() {
		return qGeneId;
	}

	public void setqGeneId(int qGeneId) {
		this.qGeneId = qGeneId;
	}

	public int getsGeneId() {
		return sGeneId;
	}

	public void setsGeneId(int sGeneId) {
		this.sGeneId = sGeneId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}


	@Override
	public String stringifyToJSON(boolean encloseWithCurlyBraces, boolean propagateToParentClassViaSuper)
			throws Exception {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String field2String = "";
		//no super.toJSON as does not extends another class
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("alignmentId",alignmentId,0,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("qGeneId",qGeneId,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("sGeneId",sGeneId,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("type",type,-1,field2String.isEmpty());
		if( encloseWithCurlyBraces){
			return "{"+field2String+"}";
		} else {
			return field2String;
		}
	}
	
}
