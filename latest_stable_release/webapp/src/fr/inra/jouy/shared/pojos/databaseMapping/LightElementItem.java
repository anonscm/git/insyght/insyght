package fr.inra.jouy.shared.pojos.databaseMapping;


/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.interfaceIs.IsStringifyToJSON;
import fr.inra.jouy.shared.interfaceIs.IsMappingKeyValueToFields;

public class LightElementItem extends LightOrganismItem implements IsSerializable, IsMappingKeyValueToFields, IsStringifyToJSON {

	private int elementId = -1;//integer
	private String type = "";//character varying(16)
	private String accession = null;//character varying(32)
	private int size = -1;//integer

	
	public LightElementItem(){
	}
	
	public int getElementId() {
		return elementId;
	}
	
	public void setElementId(int elementId) {
		this.elementId = elementId;
	}
	
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getAccession() {
		return accession;
	}
	
	public void setAccession(String accession) {
		this.accession = accession;
	}
	

	public int getSize() {
		//TODO remove hack
		if(size < 0){
			size = 128568759;
		}
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}


	//mapKeyIntValue
	@Override
	public boolean mapKeyIntValueToObjectField(String key, Integer value) {
		boolean keyAlreadyMappedToField = super.mapKeyIntValueToObjectField(key, value);
		if(keyAlreadyMappedToField){
			return true;
		} else {
			//private int elementId = -1;//integer
			//private int size = -1;//integer
			if(key.equals("element_id")){
				setElementId(value);
				return true;
			}
			if (key.equals("size")){
				setSize(value);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean mapKeyStringValueToObjectField(String key, String value) {
		boolean keyAlreadyMappedToField = super.mapKeyStringValueToObjectField(key, value);
		if(keyAlreadyMappedToField){
			return true;
		} else {
			//private String type = "";//character varying(16)
			//private String accession = null;//character varying(32)

			if(key.equals("type")){
				setType(value);
				return true;
			}
			if (key.equals("accession")){
				setAccession(value);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean mapKeyUnknowTypeValueToObjectField(String key, String value) {
		boolean keyAlreadyMappedToField = super.mapKeyUnknowTypeValueToObjectField(key, value);
		if(keyAlreadyMappedToField){
			return true;
		} else {
			// int
			if(key.equals("element_id")){
				return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
			} 
			if (key.equals("size")){
				return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
			} 
			// string
			if(key.equals("type")){
				return mapKeyStringValueToObjectField(key, value);
			} 
			if (key.equals("accession")){
				return mapKeyStringValueToObjectField(key, value);
			} 
		}
		return false;
	}

	
	@Override
	public String stringifyToJSON(boolean encloseWithCurlyBraces, boolean propagateToParentClassViaSuper) throws Exception {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String field2String = "";
		if(propagateToParentClassViaSuper){
			field2String += super.stringifyToJSON(false, propagateToParentClassViaSuper);
		}
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("elementId",elementId,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("type",type,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("accession",accession,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("size",size,-1,field2String.isEmpty());
		if( encloseWithCurlyBraces){
			return "{"+field2String+"}";
		} else {
			return field2String;
		}
	}


//	@Override
//	public void fromJSON(String JSON) throws Exception {
//		//"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]
//	    for (MatchResult matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON); matcher != null; matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON)) {
//			String keyIT = matcher.getGroup(1);
//			String valueIT = matcher.getGroup(2);
//			boolean mappingSuccessful = mapKeyUnknowTypeValueToObjectField(keyIT,valueIT);
//			if( ! mappingSuccessful){
//				throw new Exception("The key "+keyIT+" (value = "+valueIT+") has not been mapped to any attribute");
//			}
//	    }
//	}
	
	
}
