package fr.inra.jouy.shared.pojos.databaseMapping;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.pojos.applicationItems.GenomePanelItem;

public class GeneItem extends LightGeneItem implements IsSerializable {


	private String agmialXref = null;
	private ArrayList<String> listOldLocusTag = new ArrayList<String>();
	private ArrayList<String> listProteinId = new ArrayList<String>();
	private ArrayList<String> listFunction = new ArrayList<String>();

	private ArrayList<String> listProduct = new ArrayList<String>();

	private ArrayList<String> listECNumber = new ArrayList<String>();

	private ArrayList<String> listBiologicalProcess = new ArrayList<String>();
	
	private ArrayList<String> listCellularComponent = new ArrayList<String>();
	private ArrayList<String> listNote = new ArrayList<String>();
	private ArrayList<String> listInference = new ArrayList<String>();
	
	private int lengthResidues = -1;
	
	//private ElementItem associatedElementItem = null;
	
	private GenomePanelItem associatedGenomePanelItem = null;
	/*private SyntenyItem associatedSyntenyItem = null;
	
	private Map<Integer, GeneMatchItem> matchItemHashMap_GeneIdToGeneMatchItem = new HashMap<Integer, GeneMatchItem>(); // organism id to gene Id
	
	private HashMap<Integer, GeneMatchItem> matchItemHashMap_GeneToMatchItem = new HashMap<Integer, GeneMatchItem>();	
	*/
	private int positionRowInGrid = -1;
	private int positionColInGrid = -1;
	
	public GeneItem(){
		
	}
	
	public GeneItem(LightGeneItem lgiSent){
		setGeneId(lgiSent.getGeneId());
		//setMostSignificantGeneName(lgiSent.getMostSignificantGeneName());
		setMostSignificantGeneNameAsStrippedHTMLPlusLink(lgiSent.getMostSignificantGeneNameAsStrippedHTMLPlusLink());
		setMostSignificantGeneNameAsStrippedText(lgiSent.getMostSignificantGeneNameAsStrippedText());
		setName(lgiSent.getNameAsRawString());
		setLocusTag(lgiSent.getLocusTagAsRawString());
		setStart(lgiSent.getStart());
		setStop(lgiSent.getStop());
		setStrand(lgiSent.getStrand());
		setElementId(lgiSent.getElementId());
		setType(lgiSent.getType());
		setAccession(lgiSent.getAccession());
		setSize(lgiSent.getSize());
		setOrganismId(lgiSent.getOrganismId());
	    setSpecies(lgiSent.getSpecies());
	    setStrain(lgiSent.getStrain());
	}
	
	/*
	public GeneItem(int geneIdSent, String geneNameSent, int strandSent, GenomePanelItem associatedGenomePanelItemSent, int Rand, int positionRowInGridSent, int positionColInGridSent, SyntenyItem syntenyItemSent){
		
		setGeneId(geneIdSent);
		setName(geneNameSent);
		setStrand(strandSent);
		this.associatedGenomePanelItem = associatedGenomePanelItemSent;
		//this.associatedOrganismItem = associatedGenomePanelItemSent.getAssociatedOrganismItem();
		//this.associatedElementItem = associatedGenomePanelItemSent.getAssociatedElementItem();
		this.associatedSyntenyItem = syntenyItemSent;
		this.positionRowInGrid = positionRowInGridSent;
		this.positionColInGrid = positionColInGridSent;		
	}
	
	
	
	public GeneItem(int geneIdSent, String geneNameSent, int strandSent, GenomePanelItem associatedGenomePanelItemSent, int Rand, int positionRowInGridSent, int positionColInGridSent, SyntenyItem syntenyItemSent, GeneItem HomologousGeneItemInRefGenomeSent){
		
		this(geneIdSent, geneNameSent, strandSent, associatedGenomePanelItemSent, Rand, positionRowInGridSent, positionColInGridSent, syntenyItemSent);
		//this.listAssociatedGeneMatchItem.add(new GeneMatchItem(this, HomologousGeneItemInRefGenomeSent, Rand, Rand+6, Rand-8, Rand+18));
		this.matchItemHashMap_GeneIdToGeneMatchItem.put(new Integer(HomologousGeneItemInRefGenomeSent.getGeneId()), new GeneMatchItem(getGeneId(), this, HomologousGeneItemInRefGenomeSent.getGeneId(), HomologousGeneItemInRefGenomeSent, Rand, Rand+6, Rand-8, Rand+18));

	}
	*/
	
	/*
	public void setAssociatedSyntenyItem(SyntenyItem associatedSyntenyItemSent){
		this.associatedSyntenyItem = associatedSyntenyItemSent;
	}
	
	public SyntenyItem getAssociatedSyntenyItem(){
		return associatedSyntenyItem;
	}
	*/
	
	
	
	public void setAssociatedGenomePanelItem(GenomePanelItem associatedGenomePanelItemSent){
		this.associatedGenomePanelItem = associatedGenomePanelItemSent;
	}
	
	public GenomePanelItem getAssociatedGenomePanelItem(){
		return associatedGenomePanelItem;
	}
	
	/*
	public GeneMatchItem getGeneMatchItem(int versusGeneItemId){
		GeneMatchItem geneMatchItemToDW = (GeneMatchItem) this.matchItemHashMap_GeneIdToGeneMatchItem.get(new Integer(versusGeneItemId));
		return geneMatchItemToDW;
	}
	
	public void setGeneMatchItem(int versusGeneItemId, GeneMatchItem geneMatchItemSent){
		
		this.matchItemHashMap_GeneIdToGeneMatchItem.put(new Integer(versusGeneItemId), geneMatchItemSent);
		
	}
	*/
	
	public void setPositionRowInGrid(int positionRowInGridSent){
		this.positionRowInGrid = positionRowInGridSent;
	}
	
	public int getPositionRowInGrid(){
		return positionRowInGrid;
	}

	public void setPositionColInGrid(int positionColInGridSent){
		this.positionColInGrid = positionColInGridSent;
	}
	
	public int getPositionColInGrid(){
		return positionColInGrid;
	}
	



	public String getAgmialXref() {
		return agmialXref;
	}
	public void setAgmialXref(String agmialXref) {
		this.agmialXref = agmialXref;
	}

	public ArrayList<String> getListProteinId() {
		return listProteinId;
	}

	public void setListProteinId(ArrayList<String> proteinId) {
		this.listProteinId = proteinId;
	}
	
	public void addToListProteinId(String proteinId) {
		if(proteinId != null){
			this.listProteinId.add(proteinId);
		}
	}

	public ArrayList<String> getListOldLocusTag() {
		return listOldLocusTag;
	}

	public void setListOldLocusTag(ArrayList<String> listOldLocusTag) {
		this.listOldLocusTag = listOldLocusTag;
	}
	
	public void addToListOldLocusTag(String oldLocusTag) {
		if(oldLocusTag != null){
			this.listOldLocusTag.add(oldLocusTag);
		}
	}
	
	public ArrayList<String> getListFunction() {
		return listFunction;
	}
	
	public void setListFunction(ArrayList<String> listFunction) {
		this.listFunction = listFunction;
	}

	public void addToListFunction(String function) {
		if(function != null){
			this.listFunction.add(function);
		}
	}

	public ArrayList<String> getListProduct() {
		return listProduct;
	}

	public void setListProduct(ArrayList<String> listProduct) {
		this.listProduct = listProduct;
	}

	public void addToListProduct(String product) {
		if(product != null){
			this.listProduct.add(product);
		}
	}
	
	public ArrayList<String> getListECNumber() {
		return listECNumber;
	}

	public void setListECNumber(ArrayList<String> listECNumber) {
		this.listECNumber = listECNumber;
	}

	public void addToListECNumber(String ECNumber) {
		if(ECNumber != null){
			this.listECNumber.add(ECNumber);
		}
	}
	
	public ArrayList<String> getListBiologicalProcess() {
		return listBiologicalProcess;
	}

	public void setListBiologicalProcess(ArrayList<String> listBiologicalProcess) {
		this.listBiologicalProcess = listBiologicalProcess;
	}
	
	public void addToListBiologicalProcess(String biologicalProcess) {
		if(biologicalProcess != null){
			this.listBiologicalProcess.add(biologicalProcess);
		}
	}
	
	public ArrayList<String> getListCellularComponent() {
		return listCellularComponent;
	}

	public void setListCellularComponent(ArrayList<String> listCellularComponent) {
		this.listCellularComponent = listCellularComponent;
	}
	
	public void addToListCellularComponent(String cellularComponent) {
		if(cellularComponent != null){
			this.listCellularComponent.add(cellularComponent);
		}
	}

	public ArrayList<String> getListNote() {
		return listNote;
	}

	public void setListNote(ArrayList<String> listNote) {
		this.listNote = listNote;
	}
	
	public void addToListNote(String noteSent) {
		if(noteSent != null){
			this.listNote.add(noteSent);
		}
	}
	
	public ArrayList<String> getListInference() {
		return listInference;
	}

	public void setListInference(ArrayList<String> listInference) {
		this.listInference = listInference;
	}

	public void addToListInference(String inferenceSent) {
		if(inferenceSent != null){
			this.listInference.add(inferenceSent);
		}
	}

	public int getLengthResidues() {
		return lengthResidues;
	}

	public void setLengthResidues(int lengthResidues) {
		this.lengthResidues = lengthResidues;
		
	}
	
	public void setLightElementItemInfo(LightElementItem lightEltItemSent){
		
		setOrganismId(lightEltItemSent.getOrganismId());
		setSpecies(lightEltItemSent.getSpecies());
		setSize(lightEltItemSent.getSize());
		setElementId(lightEltItemSent.getElementId());
		setStrain(lightEltItemSent.getStrain());
		setType(lightEltItemSent.getType());
		
	}

	
	public void clearAllReferences(){
		/*if(associatedElementItem != null){
			associatedElementItem.clearAllReferences();
			associatedElementItem = null;
		}*/
		
		if(associatedGenomePanelItem != null){
			associatedGenomePanelItem = null;
		}
		/*
		if(associatedSyntenyItem != null){
			associatedSyntenyItem.clearAllReferences();
			associatedSyntenyItem = null;
		}
		*/
		listOldLocusTag.clear();
		listProteinId.clear();
		listFunction.clear();
		listProduct.clear();
		listECNumber.clear();
		listBiologicalProcess.clear();
		listCellularComponent.clear();
		listNote.clear();
		listInference.clear();
		lengthResidues = -1;
		/*
		Set<Integer> setOfIntGeneIdToDW=matchItemHashMap_GeneIdToGeneMatchItem.keySet();
		Iterator<Integer> iter = setOfIntGeneIdToDW.iterator();
		while (iter.hasNext()){
		    //System.out.println(iter.next());
			Integer integerGeneIdToDW = (Integer) iter.next();
			GeneMatchItem geneMatchItemToDW = (GeneMatchItem) matchItemHashMap_GeneIdToGeneMatchItem.get(integerGeneIdToDW);
			geneMatchItemToDW.clearAllReferences();
			geneMatchItemToDW = null;
		}
		matchItemHashMap_GeneIdToGeneMatchItem.clear();
		*/
	}




}
