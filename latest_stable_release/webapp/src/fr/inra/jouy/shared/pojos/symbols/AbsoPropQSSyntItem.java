package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.TransAbsoPropQSSyntItem;

public class AbsoPropQSSyntItem extends AbsoPropQSElemItem implements IsSerializable {

	private int syntenyNumberGene = -1;
	private long syntenyOrigamiAlignmentId = 0; // can be negative if isDataFromMirrorQuery
	private int pbQStartSyntenyInElement = -1;
	private int pbQStopSyntenyInElement = -1;
	private int pbSStartSyntenyInElement = -1;
	private int pbSStopSyntenyInElement = -1;
	private int syntenyScore = -1;
	
	//tmp
	//private ArrayList<AbsoluteProportionQSSyntenyItem> listOfOtherQSSyntenyContainedWithin = new ArrayList<AbsoluteProportionQSSyntenyItem>();//can be AbsoluteProportionQSGeneHomologyItem or AbsoluteProportionQGeneInsertionItem missing target
	private ArrayList<AbsoPropQSSyntItem> tmpListOfOtherQSSyntenySprungOff = new ArrayList<AbsoPropQSSyntItem>();//can be AbsoluteProportionQSGeneHomologyItem or AbsoluteProportionQGeneInsertionItem missing target
	private AbsoPropSGenoRegiInserItem previousGenomicRegionSInsertion = null;
	private AbsoPropSGenoRegiInserItem nextGenomicRegionSInsertion = null;
	private AbsoPropQElemItem tmpPreviousGenomicRegionQInsertion = null;
	private AbsoPropQElemItem tmpNextGenomicRegionQInsertion = null;
	
	
	//marquers
	private boolean isContainedWithinAnotherMotherSynteny = false;
	private boolean isSprungOffAnotherMotherSynteny = false;
	private boolean isMotherOfSprungOffSyntenies = false;
	private boolean isMotherOfContainedOtherSyntenies = false;
	private ArrayList<Long> listOrigamiAlignmentIdsMotherSyntenies = new ArrayList<Long>();
	private ArrayList<Long> listOrigamiAlignmentIdsSprungOffSyntenies = new ArrayList<Long>();
	private ArrayList<Long> listOrigamiAlignmentIdsContainedOtherSyntenies = new ArrayList<Long>();
	
	public AbsoPropQSSyntItem(){
	}
	
	public AbsoPropQSSyntItem(AbsoPropQSSyntItem apqssiSent){
		super((AbsoPropQSElemItem)apqssiSent);
		setSyntenyNumberGene(apqssiSent.getSyntenyNumberGene());
		setSyntenyOrigamiAlignmentId(apqssiSent.getSyntenyOrigamiAlignmentId());
		setPbQStartSyntenyInElement(apqssiSent.getPbQStartSyntenyInElement());
		setPbQStopSyntenyInElement(apqssiSent.getPbQStopSyntenyInElement());
		setPbSStartSyntenyInElement(apqssiSent.getPbSStartSyntenyInElement());
		setPbSStopSyntenyInElement(apqssiSent.getPbSStopSyntenyInElement());
		setSyntenyScore(apqssiSent.getSyntenyScore());
		setPreviousGenomicRegionSInsertion(apqssiSent.getPreviousGenomicRegionSInsertion());
		setNextGenomicRegionSInsertion(apqssiSent.getNextGenomicRegionSInsertion());
		//setListOfOtherQSSyntenyContainedWithin(apqssiSent.getListOfOtherQSSyntenyContainedWithin());
		setContainedWithinAnotherMotherSynteny(apqssiSent.isContainedWithinAnotherMotherSynteny);
		setSprungOffAnotherMotherSynteny(apqssiSent.isSprungOffAnotherMotherSynteny);
		setMotherOfSprungOffSyntenies(apqssiSent.isMotherOfSprungOffSyntenies);
		setMotherOfContainedOtherSyntenies(apqssiSent.isMotherOfContainedOtherSyntenies);
		setListOrigamiAlignmentIdsMotherSyntenies(apqssiSent.getListOrigamiAlignmentIdsMotherSyntenies());
		setListOrigamiAlignmentIdsSprungOffSyntenies(apqssiSent.getListOrigamiAlignmentIdsSprungOffSyntenies());
		setListOrigamiAlignmentIdsContainedOtherSyntenies(apqssiSent.getListOrigamiAlignmentIdsContainedOtherSyntenies());
	}
	
	public AbsoPropQSSyntItem(TransAbsoPropQSSyntItem transientAbsoluteProportionQSSyntenyItemSent){
		setPbQStartSyntenyInElement(transientAbsoluteProportionQSSyntenyItemSent.getPbQStartSyntenyInElement());
		setPbQStopSyntenyInElement(transientAbsoluteProportionQSSyntenyItemSent.getPbQStopSyntenyInElement());
		setPbSStartSyntenyInElement(transientAbsoluteProportionQSSyntenyItemSent.getPbSStartSyntenyInElement());
		setPbSStopSyntenyInElement(transientAbsoluteProportionQSSyntenyItemSent.getPbSStopSyntenyInElement());
		setQsQOrigamiElementId(transientAbsoluteProportionQSSyntenyItemSent.getQsQOrigamiElementId());
		setQsSOrigamiElementId(transientAbsoluteProportionQSSyntenyItemSent.getQsSOrigamiElementId());
		setSyntenyNumberGene(transientAbsoluteProportionQSSyntenyItemSent.getSyntenyNumberGene());
		setSyntenyOrigamiAlignmentId(transientAbsoluteProportionQSSyntenyItemSent.getSyntenyOrigamiAlignmentId());
		setSyntenyScore(transientAbsoluteProportionQSSyntenyItemSent.getSyntenyScore());
//		setQsQAccnum(transientAbsoluteProportionQSSyntenyItemSent.getQsQAccnum());
//		setQsQPbStartOfElementInOrga(transientAbsoluteProportionQSSyntenyItemSent.getQsQPbStartOfElementInOrga());
//		setQsQSizeOfOrganismInPb(transientAbsoluteProportionQSSyntenyItemSent.getQsQSizeOfOrganismInPb());
//		setQsSAccnum(transientAbsoluteProportionQSSyntenyItemSent.getQsSAccnum());
//		setQsSPbStartOfElementInOrga(transientAbsoluteProportionQSSyntenyItemSent.getQsSPbStartOfElementInOrga());
//		setQsSSizeOfOrganismInPb(transientAbsoluteProportionQSSyntenyItemSent.getQsSSizeOfOrganismInPb());
	}
	
	
	//Done isDataFromMirrorQuery
	public void setSyntenyOrigamiAlignmentId(long l) {
		this.syntenyOrigamiAlignmentId = l;
	}

	//Done isDataFromMirrorQuery
	public long getSyntenyOrigamiAlignmentId() {
		return syntenyOrigamiAlignmentId;
	}

	public void setSyntenyNumberGene(int syntenyNumberGene) {
		this.syntenyNumberGene = syntenyNumberGene;
	}

	public int getSyntenyNumberGene() {
		return syntenyNumberGene;
	}

	public int getPbQStartSyntenyInElement() {
		return pbQStartSyntenyInElement;
	}

	public void setPbQStartSyntenyInElement(int pbQStartSyntenyInElement) {
		this.pbQStartSyntenyInElement = pbQStartSyntenyInElement;
	}

	public int getPbQStopSyntenyInElement() {
		return pbQStopSyntenyInElement;
	}

	public void setPbQStopSyntenyInElement(int pbQStopSyntenyInElement) {
		this.pbQStopSyntenyInElement = pbQStopSyntenyInElement;
	}

	public int getPbSStartSyntenyInElement() {
		return pbSStartSyntenyInElement;
	}

	public void setPbSStartSyntenyInElement(int pbSStartSyntenyInElement) {
		this.pbSStartSyntenyInElement = pbSStartSyntenyInElement;
	}

	public int getPbSStopSyntenyInElement() {
		return pbSStopSyntenyInElement;
	}

	public void setPbSStopSyntenyInElement(int pbSStopSyntenyInElement) {
		this.pbSStopSyntenyInElement = pbSStopSyntenyInElement;
	}

	public int getSyntenyScore() {
		return syntenyScore;
	}

	public void setSyntenyScore(int syntenyScore) {
		this.syntenyScore = syntenyScore;
	}

//	public ArrayList<AbsoluteProportionQSSyntenyItem> getListOfOtherQSSyntenyContainedWithin() {
//		return listOfOtherQSSyntenyContainedWithin;
//	}
//
//	public void setListOfOtherQSSyntenyContainedWithin(
//			ArrayList<AbsoluteProportionQSSyntenyItem> tmpListOfOtherQSSyntenyContainedWithin) {
//		this.listOfOtherQSSyntenyContainedWithin = tmpListOfOtherQSSyntenyContainedWithin;
//	}

	public ArrayList<AbsoPropQSSyntItem> getTmpListOfOtherQSSyntenySprungOff() {
		return tmpListOfOtherQSSyntenySprungOff;
	}

	public void setTmpListOfOtherQSSyntenySprungOff(
			ArrayList<AbsoPropQSSyntItem> tmpListOfOtherQSSyntenySprungOff) {
		this.tmpListOfOtherQSSyntenySprungOff = tmpListOfOtherQSSyntenySprungOff;
	}

	public boolean isContainedWithinAnotherMotherSynteny() {
		return isContainedWithinAnotherMotherSynteny;
	}

	public void setContainedWithinAnotherMotherSynteny(
			boolean isContainedWithinAnotherMotherSynteny) {
		this.isContainedWithinAnotherMotherSynteny = isContainedWithinAnotherMotherSynteny;
	}

	public boolean isSprungOffAnotherMotherSynteny() {
		return isSprungOffAnotherMotherSynteny;
	}

	public void setSprungOffAnotherMotherSynteny(
			boolean isSprungOffAnotherMotherSynteny) {
		this.isSprungOffAnotherMotherSynteny = isSprungOffAnotherMotherSynteny;
	}

	public AbsoPropSGenoRegiInserItem getPreviousGenomicRegionSInsertion() {
		return previousGenomicRegionSInsertion;
	}

	public void setPreviousGenomicRegionSInsertion(
			AbsoPropSGenoRegiInserItem previousGenomicRegionSInsertion) {
		this.previousGenomicRegionSInsertion = previousGenomicRegionSInsertion;
	}

	public AbsoPropSGenoRegiInserItem getNextGenomicRegionSInsertion() {
		return nextGenomicRegionSInsertion;
	}

	public void setNextGenomicRegionSInsertion(
			AbsoPropSGenoRegiInserItem nextGenomicRegionSInsertion) {
		this.nextGenomicRegionSInsertion = nextGenomicRegionSInsertion;
	}

	public AbsoPropQElemItem getTmpPreviousGenomicRegionQInsertion() {
		return tmpPreviousGenomicRegionQInsertion;
	}

	public void setTmpPreviousGenomicRegionQInsertion(
			AbsoPropQElemItem tmpPreviousGenomicRegionQInsertion) {
		this.tmpPreviousGenomicRegionQInsertion = tmpPreviousGenomicRegionQInsertion;
	}

	public AbsoPropQElemItem getTmpNextGenomicRegionQInsertion() {
		return tmpNextGenomicRegionQInsertion;
	}

	public void setTmpNextGenomicRegionQInsertion(
			AbsoPropQElemItem tmpNextGenomicRegionQInsertion) {
		this.tmpNextGenomicRegionQInsertion = tmpNextGenomicRegionQInsertion;
	}

	public boolean isMotherOfSprungOffSyntenies() {
		return isMotherOfSprungOffSyntenies;
	}

	public void setMotherOfSprungOffSyntenies(boolean isMotherOfSprungOffSyntenies) {
		this.isMotherOfSprungOffSyntenies = isMotherOfSprungOffSyntenies;
	}

	public ArrayList<Long> getListOrigamiAlignmentIdsMotherSyntenies() {
		return listOrigamiAlignmentIdsMotherSyntenies;
	}

	public void setListOrigamiAlignmentIdsMotherSyntenies(
			ArrayList<Long> listOrigamiAlignmentIdsMotherSyntenies) {
		this.listOrigamiAlignmentIdsMotherSyntenies = listOrigamiAlignmentIdsMotherSyntenies;
	}

	public ArrayList<Long> getListOrigamiAlignmentIdsSprungOffSyntenies() {
		return listOrigamiAlignmentIdsSprungOffSyntenies;
	}

	public void setListOrigamiAlignmentIdsSprungOffSyntenies(
			ArrayList<Long> listOrigamiAlignmentIdsSprungOffSyntenies) {
		this.listOrigamiAlignmentIdsSprungOffSyntenies = listOrigamiAlignmentIdsSprungOffSyntenies;
	}

	public boolean isMotherOfContainedOtherSyntenies() {
		return isMotherOfContainedOtherSyntenies;
	}

	public void setMotherOfContainedOtherSyntenies(
			boolean isMotherOfContainedOtherSyntenies) {
		this.isMotherOfContainedOtherSyntenies = isMotherOfContainedOtherSyntenies;
	}

	public ArrayList<Long> getListOrigamiAlignmentIdsContainedOtherSyntenies() {
		return listOrigamiAlignmentIdsContainedOtherSyntenies;
	}

	public void setListOrigamiAlignmentIdsContainedOtherSyntenies(
			ArrayList<Long> listOrigamiAlignmentIdsContainedOtherSyntenies) {
		this.listOrigamiAlignmentIdsContainedOtherSyntenies = listOrigamiAlignmentIdsContainedOtherSyntenies;
	}


	/*
	public static void addQS_SYNTENY_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop, double sPercentStart, double sPercentStop) {
		AbsoluteProportionQSSyntenyItem newAPSI = new AbsoluteProportionQSSyntenyItem();
		newAPSI.setQsSyntenyEnumBlockType(AbsoluteProportionQSSyntenyItem.QS_SyntenyEnumBlockType.QS_HOMOLOGS_BLOCK);
		newAPSI.setQsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQS_SYNTENY_REVERSE_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop, double sPercentStart, double sPercentStop) {
		AbsoluteProportionQSSyntenyItem newAPSI = new AbsoluteProportionQSSyntenyItem();
		newAPSI.setQsSyntenyEnumBlockType(AbsoluteProportionQSSyntenyItem.QS_SyntenyEnumBlockType.QS_REVERSE_HOMOLOGS_BLOCK);
		newAPSI.setQsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}
	 */

	
}
