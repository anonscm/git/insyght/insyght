package fr.inra.jouy.shared.pojos.databaseMapping;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

public class AlignmentParametersForSynteny implements IsSerializable{
	
	private double orthoScore = -1;
	private double homoScore = -1;
	private double mismatchPenalty = -1;
	private double gapCreationPenalty = -1;
	private double gapExtensionPenalty = -1;
	private int minAlignSize = -1;
	private double minScore = -1;
	private boolean orthologsIncluded = true;
	
	
	public double getOrthoScore() {
		return orthoScore;
	}
	public void setOrthoScore(double orthoScore) {
		this.orthoScore = orthoScore;
	}
	public double getHomoScore() {
		return homoScore;
	}
	public void setHomoScore(double homoScore) {
		this.homoScore = homoScore;
	}
	public double getMismatchPenalty() {
		return mismatchPenalty;
	}
	public void setMismatchPenalty(double mismatchPenalty) {
		this.mismatchPenalty = mismatchPenalty;
	}
	public double getGapCreationPenalty() {
		return gapCreationPenalty;
	}
	public void setGapCreationPenalty(double gapCreationPenalty) {
		this.gapCreationPenalty = gapCreationPenalty;
	}
	public double getGapExtensionPenalty() {
		return gapExtensionPenalty;
	}
	public void setGapExtensionPenalty(double gapExtensionPenalty) {
		this.gapExtensionPenalty = gapExtensionPenalty;
	}
	public int getMinAlignSize() {
		return minAlignSize;
	}
	public void setMinAlignSize(int minAlignSize) {
		this.minAlignSize = minAlignSize;
	}
	public double getMinScore() {
		return minScore;
	}
	public void setMinScore(double minScore) {
		this.minScore = minScore;
	}
	public boolean isOrthologsIncluded() {
		return orthologsIncluded;
	}
	public void setOrthologsIncluded(boolean orthologsIncluded) {
		this.orthologsIncluded = orthologsIncluded;
	}
	
	
	
}
