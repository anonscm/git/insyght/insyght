package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.interfaceIs.IsQComparable;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSSpanItem.QSEnumBlockType;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropQCompaQSpanItem.QEnumBlockType;

public class AbsoPropQCompaItem implements Comparable<AbsoPropQCompaItem>, IsQComparable, IsSerializable{
	
	private double qPercentStart = -1;
	public AbsoPropQCompaItem(){
		
	}

	public void setqPercentStart(double qPercentStart) {
		this.qPercentStart = qPercentStart;
	}


	public double getqPercentStart() {
		return qPercentStart;
		/*if(this instanceof AbsoluteProportionQElementItem){
			if(((AbsoluteProportionQElementItem)this).getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_STOP_Q_INSERTION)==0){
				return ((AbsoluteProportionQElementItem)this).getqPercentStop();
			}else{
				return qPercentStart;
			}
		}else if(this instanceof AbsoluteProportionQGenomicRegionInsertionItem){
			if(((AbsoluteProportionQGenomicRegionInsertionItem)this).getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_STOP_Q_INSERTION)==0){
				return ((AbsoluteProportionQGenomicRegionInsertionItem)this).getqPercentStop();
			}else{
				return qPercentStart;
			}
    	}else if(this instanceof AbsoluteProportionQSSyntenyItem){
			if(((AbsoluteProportionQSSyntenyItem)this).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0
					|| ((AbsoluteProportionQSSyntenyItem)this).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
				return ((AbsoluteProportionQSSyntenyItem)this).getqPercentStop();
			}else{
				return qPercentStart;
			}
    	}else{
    		System.err.println("Error in AbsoluteProportionQComparableItem getqPercentStart : apqcIT not instanceOf defined cases");
    		Window.alert("Error in AbsoluteProportionQComparableItem getqPercentStart : apqcIT not instanceOf defined cases");
    		return qPercentStart;
    	}*/
		
	}


	public int compareTo(AbsoPropQCompaItem absoluteProportionQComparableSyntenyItemSent) {
		
    	double qPercentStartThis = -1;
    	double qPercentStartCompa = -1;
    	//double qPercentStartThis = this.getqPercentStart();
    	//double qPercentStartCompa = absoluteProportionQComparableSyntenyItemSent.getqPercentStart();
    	
    	if(this instanceof AbsoPropQCompaQSpanItem){
    		if(((AbsoPropQCompaQSpanItem)this).getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_STOP_Q_INSERTION)==0){
    			qPercentStartThis = ((AbsoPropQCompaQSpanItem)this).getqPercentStop();
    		}else{
    			qPercentStartThis = this.getqPercentStart();
    		}
    	}else if(this instanceof AbsoPropQCompaQSSpanItem){
    		if(((AbsoPropQCompaQSSpanItem)this).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0 || ((AbsoPropQCompaQSSpanItem)this).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
    			qPercentStartThis = ((AbsoPropQCompaQSSpanItem)this).getqPercentStop();
    		}else{
    			qPercentStartThis = this.getqPercentStart();
    		}
    	}else{
    		System.err.println("Error in AbsoluteProportionQComparableItem compareTo : not recognized class sent for this");
    		//Window.alert("Error in AbsoluteProportionQComparableItem compareTo : not recognized class sent for this");
    	}
    	if(absoluteProportionQComparableSyntenyItemSent instanceof AbsoPropQCompaQSpanItem){
    		if(((AbsoPropQCompaQSpanItem)absoluteProportionQComparableSyntenyItemSent).getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_STOP_Q_INSERTION)==0){
    			qPercentStartCompa = ((AbsoPropQCompaQSpanItem)absoluteProportionQComparableSyntenyItemSent).getqPercentStop();
    		}else{
    			qPercentStartCompa = absoluteProportionQComparableSyntenyItemSent.getqPercentStart();
    		}
    	}else if(absoluteProportionQComparableSyntenyItemSent instanceof AbsoPropQCompaQSSpanItem){
    		if(((AbsoPropQCompaQSSpanItem)absoluteProportionQComparableSyntenyItemSent).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0 || ((AbsoPropQCompaQSSpanItem)absoluteProportionQComparableSyntenyItemSent).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
    			qPercentStartCompa = ((AbsoPropQCompaQSSpanItem)absoluteProportionQComparableSyntenyItemSent).getqPercentStop();
    		}else{
    			qPercentStartCompa = absoluteProportionQComparableSyntenyItemSent.getqPercentStart();
    		}
    	}else{
    		System.err.println("Error in AbsoluteProportionQComparableItem compareTo : not recognized class sent for absoluteProportionQComparableSyntenyItemSent");
    		//Window.alert("Error in AbsoluteProportionQComparableItem compareTo : not recognized class sent for absoluteProportionQComparableSyntenyItemSent");
    	}
    	
		
       if(qPercentStartThis > qPercentStartCompa){
            return 1;
        }else if(qPercentStartThis < qPercentStartCompa){
            return -1;
        }else{
        	double qPercentStopThis = -1;
        	double qPercentStopCompa = -1;
        	//double sPercentStopThis = -1;
        	//double sPercentStopCompa = -1;
        	
        	boolean thisIsQCutStop = false;
        	boolean thisIsQCutSart = false;
        	boolean compaIsQCutStop = false;
        	boolean compaIsQCutStart = false;
        	
        	if(this instanceof AbsoPropQCompaQSpanItem){
        		qPercentStopThis = ((AbsoPropQCompaQSpanItem)this).getqPercentStop();
        		if(((AbsoPropQCompaQSpanItem)this).getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_STOP_Q_INSERTION)==0){
        			thisIsQCutStop = true;
        		}
        		if(((AbsoPropQCompaQSpanItem)this).getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_START_Q_INSERTION)==0){
        			thisIsQCutSart = true;
        		}
        	}
        	if(absoluteProportionQComparableSyntenyItemSent instanceof AbsoPropQCompaQSpanItem){
        		qPercentStopCompa = ((AbsoPropQCompaQSpanItem)absoluteProportionQComparableSyntenyItemSent).getqPercentStop();
        		if(((AbsoPropQCompaQSpanItem)absoluteProportionQComparableSyntenyItemSent).getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_STOP_Q_INSERTION)==0){
        			compaIsQCutStop = true;
        		}
        		if(((AbsoPropQCompaQSpanItem)absoluteProportionQComparableSyntenyItemSent).getqEnumBlockType().compareTo(QEnumBlockType.Q_CUT_START_Q_INSERTION)==0){
        			compaIsQCutStart = true;
        		}
        	}
        	if(this instanceof AbsoPropQCompaQSSpanItem){
        		qPercentStopThis = ((AbsoPropQCompaQSSpanItem)this).getqPercentStop();
        		//sPercentStopThis = ((AbsoluteProportionQComparableQSSpanItem)this).getsPercentStop();
        		if(((AbsoPropQCompaQSSpanItem)this).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0 || ((AbsoPropQCompaQSSpanItem)this).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
        			thisIsQCutStop = true;
        		}
        		if(((AbsoPropQCompaQSSpanItem)this).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0 || ((AbsoPropQCompaQSSpanItem)this).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
        			thisIsQCutSart = true;
        		}
        	}
        	if(absoluteProportionQComparableSyntenyItemSent instanceof AbsoPropQCompaQSSpanItem){
        		qPercentStopCompa = ((AbsoPropQCompaQSSpanItem)absoluteProportionQComparableSyntenyItemSent).getqPercentStop();
        		//sPercentStopCompa = ((AbsoluteProportionQComparableQSSpanItem)absoluteProportionQComparableSyntenyItemSent).getsPercentStop();
        		if(((AbsoPropQCompaQSSpanItem)absoluteProportionQComparableSyntenyItemSent).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_HOMOLOGS_BLOCK)==0 || ((AbsoPropQCompaQSSpanItem)absoluteProportionQComparableSyntenyItemSent).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_STOP_REVERSE_HOMOLOGS_BLOCK)==0){
        			compaIsQCutStop = true;
        		}
        		if(((AbsoPropQCompaQSSpanItem)absoluteProportionQComparableSyntenyItemSent).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_START_HOMOLOGS_BLOCK)==0 || ((AbsoPropQCompaQSSpanItem)absoluteProportionQComparableSyntenyItemSent).getQsEnumBlockType().compareTo(QSEnumBlockType.QS_CUT_START_REVERSE_HOMOLOGS_BLOCK)==0){
        			compaIsQCutStart = true;
        		}
        	}
        	
        	if(thisIsQCutStop && compaIsQCutStop){
        		return 0;
        	}else if(thisIsQCutStop && compaIsQCutStart){
        		return -1;
        	}else if(thisIsQCutSart && compaIsQCutStop){
        		return 1;
        	}else if(thisIsQCutSart && compaIsQCutStart){
        		return 0;
        	}else if(qPercentStopThis>0 && qPercentStopCompa>0){
                if(qPercentStopThis > qPercentStopCompa){
                    return -1;
                }else if(qPercentStopThis < qPercentStopCompa){
                    return 1;
                }else{
                	return 0;
                	/*if(sPercentStopThis>0 && sPercentStopCompa>0){
                        if(sPercentStopThis > sPercentStopCompa){
                            return -1;
                        }else if(sPercentStopThis < sPercentStopCompa){
                            return 1;
                        }else{
                            return 0;
                        }
                	}else{
                        return -1;
                	}*/
                }
        	}else{
                return -1;
        	}
        }
	}

	
}
