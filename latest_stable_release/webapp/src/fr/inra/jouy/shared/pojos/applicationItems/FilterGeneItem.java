package fr.inra.jouy.shared.pojos.applicationItems;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;

import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.client.SessionStorageControler;
import fr.inra.jouy.shared.UtilitiesMethodsShared;

public class FilterGeneItem implements IsSerializable{
	
	public enum EnumUpdateTypeLoadGeneListForSelectedFilters implements IsSerializable {
		updateBuildGeneSetSearchTab,
		updateCoreDispSearchTab,
		updateFindGenePopUp_SelectedElement,
		updateFindGenePopUp_GeneListSent
	}
	
	public enum EnumTypeFilterGeneBy implements IsSerializable {
		//No_filter,
		Genomic_locations,
		Presence_absence_homology,
		Gene_name,
		Function,
		Biological_process,
		EC_number,
		Cellular_component,
		Notes_product,
		db_xref,
		Evidence,
		Lenght_protein,
		CountMostRepresPheno,
		PValOverReprPheno,
		Gene_id, //hidden field
		Element_id, //hidden field
		Organism_id //hidden field
		//,Annotated_as_pseudogene_ncRNA
	};

	public enum EnumOperatorFilterGeneBy implements IsSerializable {
		FIRST, COMMA_DOTS_AND_DOTS_COMMA, DOTS_COMMA_AND_COMMA_DOTS, COMMA_DOTS_OR_DOTS_COMMA, DOTS_COMMA_OR_COMMA_DOTS
	};
	
	private EnumTypeFilterGeneBy typeFilterGeneBy;
	private EnumOperatorFilterGeneBy operatorFilterGeneBy;
	private ArrayList<String> listSubFilter = new ArrayList<String>();

	public FilterGeneItem(){
		
	}
	
	public EnumTypeFilterGeneBy getTypeFilterGeneBy() {
		return typeFilterGeneBy;
	}

	public void setTypeFilterGeneBy(EnumTypeFilterGeneBy typeFilterGeneBy) {
		this.typeFilterGeneBy = typeFilterGeneBy;
	}

	public EnumOperatorFilterGeneBy getOperatorFilterGeneBy() {
		return operatorFilterGeneBy;
	}

	public void setOperatorFilterGeneBy(EnumOperatorFilterGeneBy operatorFilterGeneBy) {
		this.operatorFilterGeneBy = operatorFilterGeneBy;
	}

	public ArrayList<String> getListSubFilter() {
		return listSubFilter;
	}

	public void setListSubFilter(ArrayList<String> listSubFilter) {
		this.listSubFilter = listSubFilter;
	}

	public static String getEnglishTraductionOflistFilters(
			ArrayList<FilterGeneItem> listFilters) {

		long countNumberOrganismsPresenceAbsence = 0;
		
		String htmlTextLoading = "<br/>Searching for genes where (";

		int isCurrentInner0Undef1AND2ORTypeBoolean = 0;
		int isCurrentOuterr0Undef1AND2ORTypeBoolean = 0;

		for (int i = 0; i < listFilters.size(); i++) {
			FilterGeneItem fgiIT = listFilters.get(i);
			
			boolean isNullOrIsNotNull = false;

			if (i > 0) {
				htmlTextLoading += "</ul></li>";
			}

			if (fgiIT.getOperatorFilterGeneBy().compareTo(
					EnumOperatorFilterGeneBy.COMMA_DOTS_AND_DOTS_COMMA) == 0) {
				htmlTextLoading += "<ul>AND</ul>";
				if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
						|| isCurrentInner0Undef1AND2ORTypeBoolean == 1) {
					// ok tout vas bien
					isCurrentInner0Undef1AND2ORTypeBoolean = 1;
				} else {
					htmlTextLoading = "ERROR Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis :<br/>"
							+ htmlTextLoading;
					// Window.alert("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis : "+htmlTextLoading);
					// SearchViewImpl.refreshListStep3.setEnabled(true);
					// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
					return htmlTextLoading;
				}
			} else if (fgiIT.getOperatorFilterGeneBy().compareTo(
					EnumOperatorFilterGeneBy.COMMA_DOTS_OR_DOTS_COMMA) == 0) {
				htmlTextLoading += "<ul>OR</ul>";
				if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
						|| isCurrentInner0Undef1AND2ORTypeBoolean == 2) {
					// ok tout vas bien
					isCurrentInner0Undef1AND2ORTypeBoolean = 2;
				} else {
					htmlTextLoading = "ERROR Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis :<br/>"
							+ htmlTextLoading;
					// Window.alert("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis : "+htmlTextLoading);
					// SearchViewImpl.refreshListStep3.setEnabled(true);
					// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
					return htmlTextLoading;
				}
			} else if (fgiIT.getOperatorFilterGeneBy().compareTo(
					EnumOperatorFilterGeneBy.DOTS_COMMA_AND_COMMA_DOTS) == 0) {
				htmlTextLoading += ") AND (";
				isCurrentInner0Undef1AND2ORTypeBoolean = 0;
				if (isCurrentOuterr0Undef1AND2ORTypeBoolean == 0
						|| isCurrentOuterr0Undef1AND2ORTypeBoolean == 1) {
					// ok tout vas bien
					isCurrentOuterr0Undef1AND2ORTypeBoolean = 1;
				} else {
					htmlTextLoading = "ERROR Incoherent outer AND/OR structure: all the logical operator must be of the same kind outside the parenthesis :<br/>"
							+ htmlTextLoading;
					// Window.alert("Incoherent outer AND/OR structure: all the logical operator must be of the same kind outside the parenthesis : "+htmlTextLoading);
					// SearchViewImpl.refreshListStep3.setEnabled(true);
					// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
					return htmlTextLoading;
				}
			} else if (fgiIT.getOperatorFilterGeneBy().compareTo(
					EnumOperatorFilterGeneBy.DOTS_COMMA_OR_COMMA_DOTS) == 0) {
				htmlTextLoading += ") OR (";
				isCurrentInner0Undef1AND2ORTypeBoolean = 0;
				if (isCurrentOuterr0Undef1AND2ORTypeBoolean == 0
						|| isCurrentOuterr0Undef1AND2ORTypeBoolean == 2) {
					// ok tout vas bien
					isCurrentOuterr0Undef1AND2ORTypeBoolean = 2;
				} else {
					htmlTextLoading = "ERROR Incoherent outer AND/OR structure: all the logical operator must be of the same kind outside the parenthesis :<br/>"
							+ htmlTextLoading;
					// Window.alert("Incoherent outer AND/OR structure: all the logical operator must be of the same kind outside the parenthesis : "+htmlTextLoading);
					// SearchViewImpl.refreshListStep3.setEnabled(true);
					// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
					return htmlTextLoading;
				}
			} else if (fgiIT.getOperatorFilterGeneBy().compareTo(
					EnumOperatorFilterGeneBy.FIRST) == 0) {
			} else {
				htmlTextLoading = "ERROR unidentified getOperatorFilterGeneBy :<br/>"
						+ htmlTextLoading;
				// Window.alert("Error in loadGeneListForSelectedFilters, unidentified getOperatorFilterGeneBy");
				// SearchViewImpl.refreshListStep3.setEnabled(true);
				// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
				return htmlTextLoading;
			}

			boolean isDigitRange = false;
			boolean isPresenceAbsenceHomolog = false;
			boolean isHiddenField = false;
			htmlTextLoading += "<ul><li>";

			if (fgiIT.getTypeFilterGeneBy().compareTo(
					EnumTypeFilterGeneBy.Genomic_locations) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Lenght_protein) == 0) {
				isDigitRange = true;
				//isPresenceAbsenceHomolog = false;
				htmlTextLoading += fgiIT.getTypeFilterGeneBy().toString();
			} else if (fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Presence_absence_homology) == 0) {
				//isGenomicLocations = false;
				isPresenceAbsenceHomolog = true;
			} else if (fgiIT.getTypeFilterGeneBy().compareTo(
					EnumTypeFilterGeneBy.Cellular_component) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.db_xref) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.EC_number) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Evidence) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Function) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Biological_process) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Gene_name) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.Notes_product) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.CountMostRepresPheno) == 0
					|| fgiIT.getTypeFilterGeneBy().compareTo(
							EnumTypeFilterGeneBy.PValOverReprPheno) == 0
					// ||
					// fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Annotated_as_pseudogene_ncRNA)==0
			) {
				htmlTextLoading += fgiIT.getTypeFilterGeneBy().toString();
				//isGenomicLocations = false;
				//isPresenceAbsenceHomolog = false;
			} else if (fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Gene_id) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Element_id) == 0
						|| fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.Organism_id) == 0) {
				htmlTextLoading += fgiIT.getTypeFilterGeneBy().toString();
				isHiddenField = true;
				if(fgiIT.getListSubFilter().size() != 2){
					htmlTextLoading = "ERROR loadGeneListForSelectedFilters, fgiIT.getListSubFilter().size() != 2 for hidden field :<br/>"
							+ htmlTextLoading;
					return htmlTextLoading;
				}
			} else {
				htmlTextLoading += fgiIT.getTypeFilterGeneBy().toString();
				htmlTextLoading = "ERROR loadGeneListForSelectedFilters, unidentified getTypeFilterGeneBy :<br/>"
						+ htmlTextLoading;
				// Window.alert("Error in loadGeneListForSelectedFilters, unidentified getTypeFilterGeneBy");
				// SearchViewImpl.refreshListStep3.setEnabled(true);
				// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
				return htmlTextLoading;
			}

			for (int j = 0; j < fgiIT.getListSubFilter().size(); j++) {
				if (isDigitRange) {

					if (j % 3 == 0) {
						htmlTextLoading += " "
								+ fgiIT.getListSubFilter().get(j) + " -";
					} else if ((j + 1) % 3 == 0) {
						if (fgiIT.getListSubFilter().get(j).compareTo("AND") == 0) {
							htmlTextLoading += "</li></ul><ul>AND</ul><ul><li>"
									+ fgiIT.getTypeFilterGeneBy();
							if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
									|| isCurrentInner0Undef1AND2ORTypeBoolean == 1) {
								// ok tout vas bien
								isCurrentInner0Undef1AND2ORTypeBoolean = 1;
							} else {
								htmlTextLoading = "ERROR Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis :<br/>"
										+ htmlTextLoading;
								// Window.alert("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis : "+htmlTextLoading);
								// SearchViewImpl.refreshListStep3.setEnabled(true);
								// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
								return htmlTextLoading;
							}
						} else if (fgiIT.getListSubFilter().get(j)
								.compareTo("OR") == 0) {
							htmlTextLoading += "</li></ul><ul>OR</ul><ul><li>"
									+ fgiIT.getTypeFilterGeneBy();
							if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
									|| isCurrentInner0Undef1AND2ORTypeBoolean == 2) {
								// ok tout vas bien
								isCurrentInner0Undef1AND2ORTypeBoolean = 2;
							} else {
								htmlTextLoading = "ERROR Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis :<br/>"
										+ htmlTextLoading;
								// Window.alert("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis : "+htmlTextLoading);
								// SearchViewImpl.refreshListStep3.setEnabled(true);
								// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
								return htmlTextLoading;
							}
						} else {
							htmlTextLoading = "ERROR isGenomicLocations and j%2 but neither AND or OR :<br/>"
									+ fgiIT.getListSubFilter().get(j)
									+ " in "
									+ htmlTextLoading;
							// Window.alert("Error in loadGeneListForSelectedFilters, isGenomicLocations and j%2 but neither AND or OR : "+fgiIT.getListSubFilter().get(j)+" in "+htmlTextLoading);
							// SearchViewImpl.refreshListStep3.setEnabled(true);
							// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
							return htmlTextLoading;
						}
					} else {
						htmlTextLoading += " "
								+ fgiIT.getListSubFilter().get(j);
					}
				} else if (isPresenceAbsenceHomolog) {

					// System.out.println(fgiIT.getListSubFilter().toString());

					if (j % 3 == 0) {
						// Presence/absence
						if (fgiIT.getListSubFilter().get(j)
								.compareTo("presence") == 0) {
							htmlTextLoading += " Presence homolog in ";
						} else if (fgiIT.getListSubFilter().get(j)
								.compareTo("absence") == 0) {
							htmlTextLoading += " Absence homolog in ";
						} else {
							htmlTextLoading = "ERROR isPresenceAbsenceHomolog and j%3 but neither Presence or absence :<br/>"
									+ fgiIT.getListSubFilter().get(j)
									+ " in "
									+ htmlTextLoading;
							// Window.alert("Error in loadGeneListForSelectedFilters, isPresenceAbsenceHomolog and j%3 but neither Presence or absence : "+fgiIT.getListSubFilter().get(j)+" in "+htmlTextLoading);
							// SearchViewImpl.refreshListStep3.setEnabled(true);
							// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
							return htmlTextLoading;
						}

					}
					if ((j - 1) % 3 == 0) {
						// list orga id
						//htmlTextLoading += " [see list of genomes directly in filter box] ";
						
						countNumberOrganismsPresenceAbsence += fgiIT.getListSubFilter().get(j).chars().filter(ch -> ch == ',').count();
						if (countNumberOrganismsPresenceAbsence > UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined ) {
							htmlTextLoading = "TOO MANY ARGUMENTS : too many organisms to compare for presence / absence homology."
									+ " The maximum number of compared organisms allowed for now is "+UtilitiesMethodsShared.maxNumberOfGenomesInGroupPlusMinusCombined
									//+ "<br/>"
									//+ htmlTextLoading
									;
							return htmlTextLoading;
						} else {
							htmlTextLoading += fgiIT.getListSubFilter().get(j);
						}
						
					}

					if ((j - 2) % 3 == 0) {
						if (fgiIT.getListSubFilter().get(j).compareTo("AND") == 0) {
							htmlTextLoading += "</li></ul><ul>AND</ul><ul><li>";
							if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
									|| isCurrentInner0Undef1AND2ORTypeBoolean == 1) {
								// ok tout vas bien
								isCurrentInner0Undef1AND2ORTypeBoolean = 1;
							} else {
								htmlTextLoading = "ERROR Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis :<br/>"
										+ htmlTextLoading;
								// Window.alert("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis : "+htmlTextLoading);
								// SearchViewImpl.refreshListStep3.setEnabled(true);
								// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
								return htmlTextLoading;
							}
						} else if (fgiIT.getListSubFilter().get(j)
								.compareTo("OR") == 0) {
							htmlTextLoading += "</li></ul><ul>OR</ul><ul><li>";
							if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
									|| isCurrentInner0Undef1AND2ORTypeBoolean == 2) {
								// ok tout vas bien
								isCurrentInner0Undef1AND2ORTypeBoolean = 2;
							} else {
								htmlTextLoading = "ERROR Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis :<br/>"
										+ htmlTextLoading;
								// Window.alert("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis : "+htmlTextLoading);
								// SearchViewImpl.refreshListStep3.setEnabled(true);
								// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
								return htmlTextLoading;
							}
						} else {
							htmlTextLoading = "ERROR isPresenceAbsenceHomolog and (j-2)%3==0 but neither AND or OR :<br/>"
									+ fgiIT.getListSubFilter().get(j)
									+ " in "
									+ htmlTextLoading;
							return htmlTextLoading;
						}
					}
				} else if (isHiddenField) {
					if (j == 1) {
						if (fgiIT.getListSubFilter().get(j-1).compareTo("IN") == 0) {
							htmlTextLoading += " IN ";
						} else if (fgiIT.getListSubFilter().get(j-1).compareTo("NOT IN") == 0) {
							htmlTextLoading += " NOT IN ";
						} else {
							htmlTextLoading = "ERROR isHiddenField and j == 0 but neither IN or NOT IN :<br/>"
									+ fgiIT.getListSubFilter().get(j)
									+ " in "
									+ htmlTextLoading;
							return htmlTextLoading;
						}
						
						if(fgiIT.getListSubFilter().get(j).matches("[\\d,]+")){
							htmlTextLoading += fgiIT.getListSubFilter().get(j);
						} else {
							htmlTextLoading = "ERROR isHiddenField and j == 1 but not .matches(\"[\\d,]+\") :<br/>"
									+ fgiIT.getListSubFilter().get(j)
									+ " in "
									+ htmlTextLoading;
							return htmlTextLoading;
						}
					}
				} else {

					if (fgiIT.getListSubFilter().get(j).compareTo("IS_NULL") == 0
							|| fgiIT.getListSubFilter().get(j)
									.compareTo("IS_NOT_NULL") == 0) {
						if (j > 0) {
							htmlTextLoading = "ERROR Incoherent IS_NULL/IS_NOT_NULL structure: can not permit imbricated AND/OR modifiers with IS_NULL/IS_NOT_NULL :<br/>"
									+ htmlTextLoading;
							// Window.alert("Incoherent IS_NULL/IS_NOT_NULL structure: can not permit imbricated AND/OR modifiers with IS_NULL/IS_NOT_NULL : "+htmlTextLoading);
							// SearchViewImpl.refreshListStep3.setEnabled(true);
							// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
							htmlTextLoading += " "
									+ fgiIT.getListSubFilter().get(j);
							return htmlTextLoading;
						} else {

							isNullOrIsNotNull = true;
						}
					}

					if ((j % 2 != 0) && (j != 0)) {
						if (isNullOrIsNotNull) {
							htmlTextLoading = "ERROR Incoherent IS_NULL/IS_NOT_NULL structure: can not permit imbricated AND/OR modifiers with IS_NULL/IS_NOT_NULL :<br/>"
									+ htmlTextLoading;
							// Window.alert("Incoherent IS_NULL/IS_NOT_NULL structure: can not permit imbricated AND/OR modifiers with IS_NULL/IS_NOT_NULL : "+htmlTextLoading);
							// SearchViewImpl.refreshListStep3.setEnabled(true);
							// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
							htmlTextLoading += " "
									+ fgiIT.getListSubFilter().get(j);
							return htmlTextLoading;
						} else if (fgiIT.getListSubFilter().get(j)
								.compareTo("AND") == 0) {
							htmlTextLoading += "</li></ul><ul>AND</ul><ul><li>"
									+ fgiIT.getTypeFilterGeneBy();
							if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
									|| isCurrentInner0Undef1AND2ORTypeBoolean == 1) {
								// ok tout vas bien

								isCurrentInner0Undef1AND2ORTypeBoolean = 1;
							} else {
								htmlTextLoading = "ERROR Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis :<br/>"
										+ htmlTextLoading;
								// Window.alert("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis : "+htmlTextLoading);
								// SearchViewImpl.refreshListStep3.setEnabled(true);
								// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
								return htmlTextLoading;
							}
						} else if (fgiIT.getListSubFilter().get(j)
								.compareTo("OR") == 0) {
							htmlTextLoading += "</li></ul><ul>OR</ul><ul><li>"
									+ fgiIT.getTypeFilterGeneBy();
							if (isCurrentInner0Undef1AND2ORTypeBoolean == 0
									|| isCurrentInner0Undef1AND2ORTypeBoolean == 2) {
								// ok tout vas bien

								isCurrentInner0Undef1AND2ORTypeBoolean = 2;
							} else {
								htmlTextLoading = "ERROR Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis :<br/>"
										+ htmlTextLoading;
								// Window.alert("Incoherent AND/OR structure: all the logical operator must be of the same kind within an inner parenthesis : "+htmlTextLoading);
								// SearchViewImpl.refreshListStep3.setEnabled(true);
								// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
								return htmlTextLoading;
							}
						} else {
							htmlTextLoading = "ERROR not isGenomicLocations and j%2 but neither AND or OR in<br/>"
									+ htmlTextLoading;
							// Window.alert("Error in loadGeneListForSelectedFilters, not isGenomicLocations and j%2 but neither AND or OR");
							// SearchViewImpl.refreshListStep3.setEnabled(true);
							// SearchViewImpl.HTMLLoadingGeneList.setHTML("error in "+htmlTextLoading);
							return htmlTextLoading;
						}
					} else {
						htmlTextLoading += " "
								+ fgiIT.getListSubFilter().get(j);
					}
				}
			}
		}
		htmlTextLoading += "</ul></li>)";
		
		return htmlTextLoading;

	}
	
	
	public static ArrayList<FilterGeneItem> convertClientSideFilterToListPresenceGeneId(
			ArrayList<FilterGeneItem> listFilters
			//EnumUpdateTypeLoadGeneListForSelectedFilters updateType
			) {
		//updateType -> list of geneItem
		//updateBuildGeneSetSearchTab -> Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet()
		//updateCoreDispSearchTab -> Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet()
		//updateFindGenePopUp_SelectedElement -> ??
		//updateListGenePopUp_GeneListSent -> PopUpListGenesDialog.alReferenceGenesList
		
//		ArrayList<LightGeneItem> alLGIIT = null;
//		if(updateType.compareTo(EnumUpdateTypeLoadGeneListForSelectedFilters.updateBuildGeneSetSearchTab)==0){
//			alLGIIT = Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet();
//		} else if(updateType.compareTo(EnumUpdateTypeLoadGeneListForSelectedFilters.updateCoreDispSearchTab)==0){
//			alLGIIT = Insyght.APP_CONTROLER.getUserSearchItem().getListReferenceGeneSet();
//		} else if(updateType.compareTo(EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_GeneListSent)==0){
//			alLGIIT = PopUpListGenesDialog.alReferenceGenesList;
//		} else if(updateType.compareTo(EnumUpdateTypeLoadGeneListForSelectedFilters.updateFindGenePopUp_SelectedElement)==0){
//			return null;
//		} else {
//			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, new Exception("Error in " +
//					"convertClientSideFilterToListPresenceGeneId : unrecognized updateType = "+updateType.toString()));
//		}

		ArrayList<FilterGeneItem> alToReturn = new ArrayList<>();
		for(FilterGeneItem fgiIT : listFilters){
			if(fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.CountMostRepresPheno)==0){
				
				int ref_orga_id_IT = SessionStorageControler.CountMostRepresPheno_REF_ORGA_ID;
				String prefix_key_individuals_IT = SessionStorageControler.CountMostRepresPheno_PREFIX_KEY_INDIVIDUALS;
				
				Storage sessionStorage = Storage.getSessionStorageIfSupported();
				if(sessionStorage != null){
					FilterGeneItem newFgi = new FilterGeneItem();
					ArrayList<Integer> alGeneIds = new ArrayList<>();
					if(ref_orga_id_IT > 0){
						try {
							String typeComparaisonAndThreshold = fgiIT.getListSubFilter().get(0);
							for(int i=0;i<sessionStorage.getLength();i++){
								String keyIT = sessionStorage.key(i);
								if(keyIT.startsWith(prefix_key_individuals_IT)){
									String geneIdOfKey = keyIT.replace(prefix_key_individuals_IT, "");
									int geneIdIT = Integer.parseInt(geneIdOfKey);
									String stValueIT = sessionStorage.getItem(keyIT);
									int valueIT = Integer.parseInt(stValueIT);
									if(typeComparaisonAndThreshold.matches("^>\\s\\d+$")){
										int threshold = Integer.parseInt(typeComparaisonAndThreshold.replace("> ", ""));
										if(valueIT > threshold){
											alGeneIds.add(geneIdIT);
										}
									} else if (typeComparaisonAndThreshold.matches("^<\\s\\d+$")){
										int threshold = Integer.parseInt(typeComparaisonAndThreshold.replace("< ", ""));
										if(valueIT < threshold){
											alGeneIds.add(geneIdIT);
										}
									} else if (
//											typeComparaisonAndThreshold.compareTo("IS_NULL")==0
//												|| 
												typeComparaisonAndThreshold.compareTo("IS_NOT_NULL")==0){
										alGeneIds.add(geneIdIT);
									} else {
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(
												6, 
												new Exception("Error in convertClientSideFilterToListPresenceGeneId CountMostRepresPheno : unrecognized typeComparaison = "+typeComparaisonAndThreshold));
									}
								}
							}
							ArrayList<String> alToSet = new ArrayList<>();
							if(typeComparaisonAndThreshold.matches("^>\\s\\d+$")
									|| typeComparaisonAndThreshold.matches("^<\\s\\d+$")
									|| typeComparaisonAndThreshold.compareTo("IS_NOT_NULL")==0){
								alToSet.add("IN");
							}
//							else if (typeComparaisonAndThreshold.compareTo("IS_NULL")==0){
//								alToSet.add("NOT IN");
//							}
							String listGeneIds = UtilitiesMethodsShared.getItemsAsStringFromCollection(alGeneIds);
							alToSet.add(listGeneIds);
							newFgi.setListSubFilter(alToSet);
							newFgi.setOperatorFilterGeneBy(fgiIT.getOperatorFilterGeneBy());
							newFgi.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Gene_id);
							alToReturn.add(newFgi);
						} catch (Exception e) {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
							return null;
						}
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
								new Exception("Error in convertClientSideFilterToListPresenceGeneId CountMostRepresPheno :"
										+ " SessionStorageControler.CountMostRepresPheno_REF_ORGA_ID <= 0"));
						return null;
					}
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(
							6, 
							new Exception("Error in convertClientSideFilterToListPresenceGeneId CountMostRepresPheno : sessionStorage is null "));
					return null;
				}
			} else if (fgiIT.getTypeFilterGeneBy().compareTo(EnumTypeFilterGeneBy.PValOverReprPheno)==0) {

				int ref_orga_id_IT = SessionStorageControler.PValOverReprPheno_REF_ORGA_ID;
				String prefix_key_individuals_IT = SessionStorageControler.PValOverReprPheno_PREFIX_KEY_INDIVIDUALS;
				
				Storage sessionStorage = Storage.getSessionStorageIfSupported();
				if(sessionStorage != null){
					FilterGeneItem newFgi = new FilterGeneItem();
					ArrayList<Integer> alGeneIds = new ArrayList<>();
					if(ref_orga_id_IT > 0){
						try {
							String typeComparaisonAndThreshold = fgiIT.getListSubFilter().get(0);
							for(int i=0;i<sessionStorage.getLength();i++){
								String keyIT = sessionStorage.key(i);
								if(keyIT.startsWith(prefix_key_individuals_IT)){
									String geneIdOfKey = keyIT.replace(prefix_key_individuals_IT, "");
									int geneIdIT = Integer.parseInt(geneIdOfKey);
									String stValueIT = sessionStorage.getItem(keyIT);
									Double valueIT = Double.parseDouble(stValueIT);
									if(typeComparaisonAndThreshold.matches("^>\\s[\\d\\.eE-]+$")){
										Double threshold = Double.parseDouble(typeComparaisonAndThreshold.replace("> ", ""));
										if(valueIT > threshold){
											alGeneIds.add(geneIdIT);
										}
									} else if (typeComparaisonAndThreshold.matches("^<\\s[\\d\\.eE-]+$")){
										Double threshold = Double.parseDouble(typeComparaisonAndThreshold.replace("< ", ""));
										if(valueIT < threshold){
											alGeneIds.add(geneIdIT);
										}
									} else if (
//											typeComparaisonAndThreshold.compareTo("IS_NULL")==0
//												|| 
												typeComparaisonAndThreshold.compareTo("IS_NOT_NULL")==0){
										alGeneIds.add(geneIdIT);
									} else {
										Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(
												6, 
												new Exception("Error in convertClientSideFilterToListPresenceGeneId PValOverReprPheno : unrecognized typeComparaison = "+typeComparaisonAndThreshold));
									}
								}
							}
							ArrayList<String> alToSet = new ArrayList<>();
							if(typeComparaisonAndThreshold.matches("^>\\s[\\d\\.eE-]+$")
									|| typeComparaisonAndThreshold.matches("^<\\s[\\d\\.eE-]+$")
									|| typeComparaisonAndThreshold.compareTo("IS_NOT_NULL")==0){
								alToSet.add("IN");
							}
//							else if (typeComparaisonAndThreshold.compareTo("IS_NULL")==0){
//								alToSet.add("NOT IN");
//							}
							String listGeneIds = UtilitiesMethodsShared.getItemsAsStringFromCollection(alGeneIds);
							alToSet.add(listGeneIds);
							newFgi.setListSubFilter(alToSet);
							newFgi.setOperatorFilterGeneBy(fgiIT.getOperatorFilterGeneBy());
							newFgi.setTypeFilterGeneBy(EnumTypeFilterGeneBy.Gene_id);
							alToReturn.add(newFgi);
						} catch (Exception e) {
							Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, e);
							return null;
						}
					} else {
						Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
								new Exception("Error in convertClientSideFilterToListPresenceGeneId PValOverReprPheno :"
										+ " SessionStorageControler.PValOverReprPheno_REF_ORGA_ID <= 0"));
						return null;
					}
				} else {
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(
							6, 
							new Exception("Error in convertClientSideFilterToListPresenceGeneId PValOverReprPheno : sessionStorage is null "));
					return null;
				}
			} else {
				//TODO filter by genomic location client side ?
				//filter other type than client side
				alToReturn.add(fgiIT);
			}
		}
		return alToReturn;
		
	}
	
}
