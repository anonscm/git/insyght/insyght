package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

public class AbsoPropSGenoRegiInserItem extends AbsoPropSElemItem implements IsSerializable {

	
	private int sGenomicRegionInsertionNumberGene = -1;
	private int sPbStartSGenomicRegionInsertionInElement = -1;
	private int sPbStopSGenomicRegionInsertionInElement = -1;
	
	private long sIfMissingTargetOrigamiAlignmentId = -1;
	private AbsoPropSGenoRegiInserItem sIfMissingTargetPreviousGenomicRegionSInsertion = null;
	private AbsoPropSGenoRegiInserItem sIfMissingTargetNextGenomicRegionSInsertion = null;
	
	private boolean anchoredToPrevious = false;
	private boolean anchoredToNext = false;
	private boolean anchoredToStartElement = false;
	private boolean anchoredToStopElement = false;
	
	private boolean isAnnotationCollision = false;
	private boolean isPreviousSOfNextSlice = false;
	private boolean isNextSOfPreviousSlice = false;
	
	public AbsoPropSGenoRegiInserItem(){
		
	}

	public AbsoPropSGenoRegiInserItem(AbsoPropSGenoRegiInserItem apssiSent){
		super((AbsoPropSElemItem)apssiSent);
		setSGenomicRegionInsertionNumberGene(apssiSent.getSGenomicRegionInsertionNumberGene());
		setsPbStartSGenomicRegionInsertionInElement(apssiSent.getsPbStartSGenomicRegionInsertionInElement());
		setsPbStopSGenomicRegionInsertionInElement(apssiSent.getsPbStopSGenomicRegionInsertionInElement());
		setAnchoredToNext(apssiSent.isAnchoredToNext());
		setAnchoredToPrevious(apssiSent.isAnchoredToPrevious());
		setsIfMissingTargetOrigamiAlignmentId(apssiSent.getsIfMissingTargetOrigamiAlignmentId());
		setsIfMissingTargetPreviousGenomicRegionSInsertion(apssiSent.getsIfMissingTargetPreviousGenomicRegionSInsertion());
		setsIfMissingTargetNextGenomicRegionSInsertion(apssiSent.getsIfMissingTargetNextGenomicRegionSInsertion());
		setAnnotationCollision(apssiSent.isAnnotationCollision());
		setAnchoredToStartElement(apssiSent.isAnchoredToStartElement());
		setAnchoredToStopElement(apssiSent.isAnchoredToStopElement());
		setPreviousSOfNextSlice(apssiSent.isPreviousSOfNextSlice());
		setNextSOfPreviousSlice(apssiSent.isNextSOfPreviousSlice());
	}
	
	public AbsoPropSGenoRegiInserItem(AbsoPropQSSyntItem apqssiSent,
			SEnumBlockType sEnumBlockTypeSent, double percentStart, double percentStop){
		super((AbsoPropQSElemItem)apqssiSent,
				sEnumBlockTypeSent, percentStart, percentStop);
		
		setSGenomicRegionInsertionNumberGene(apqssiSent.getSyntenyNumberGene());
		setsPbStartSGenomicRegionInsertionInElement(apqssiSent.getPbSStartSyntenyInElement());
		setsPbStopSGenomicRegionInsertionInElement(apqssiSent.getPbSStopSyntenyInElement());
		setAnchoredToNext(true);
		setAnchoredToPrevious(true);
		setsIfMissingTargetOrigamiAlignmentId(apqssiSent.getSyntenyOrigamiAlignmentId());
		setsIfMissingTargetPreviousGenomicRegionSInsertion(apqssiSent.getPreviousGenomicRegionSInsertion());
		setsIfMissingTargetNextGenomicRegionSInsertion(apqssiSent.getNextGenomicRegionSInsertion());
		setAnnotationCollision(false);
		setAnchoredToStartElement(false);
		setAnchoredToStopElement(false);
		setPreviousSOfNextSlice(false);
		setNextSOfPreviousSlice(false);
		
		
	}
	
	public void setSGenomicRegionInsertionNumberGene(int sNumberGene) {
		this.sGenomicRegionInsertionNumberGene = sNumberGene;
	}

	public int getSGenomicRegionInsertionNumberGene() {
		return sGenomicRegionInsertionNumberGene;
	}

	public int getsPbStartSGenomicRegionInsertionInElement() {
		return sPbStartSGenomicRegionInsertionInElement;
	}

	public void setsPbStartSGenomicRegionInsertionInElement(
			int sPbStartSGenomicRegionInsertionInElement) {
		this.sPbStartSGenomicRegionInsertionInElement = sPbStartSGenomicRegionInsertionInElement;
	}

	public int getsPbStopSGenomicRegionInsertionInElement() {
		return sPbStopSGenomicRegionInsertionInElement;
	}

	public void setsPbStopSGenomicRegionInsertionInElement(
			int sPbStopSGenomicRegionInsertionInElement) {
		this.sPbStopSGenomicRegionInsertionInElement = sPbStopSGenomicRegionInsertionInElement;
	}

	public boolean isAnchoredToPrevious() {
		return anchoredToPrevious;
	}

	public void setAnchoredToPrevious(boolean anchoredToPrevious) {
		this.anchoredToPrevious = anchoredToPrevious;
	}

	public boolean isAnchoredToNext() {
		return anchoredToNext;
	}

	public void setAnchoredToNext(boolean anchoredToNext) {
		this.anchoredToNext = anchoredToNext;
	}

	//Done isDataFromMirrorQuery
	public long getsIfMissingTargetOrigamiAlignmentId() {
		return sIfMissingTargetOrigamiAlignmentId;
	}

	//Done isDataFromMirrorQuery
	public void setsIfMissingTargetOrigamiAlignmentId(
			long l) {
		this.sIfMissingTargetOrigamiAlignmentId = l;
	}

	public boolean isAnnotationCollision() {
		return isAnnotationCollision;
	}

	public void setAnnotationCollision(boolean isAnnotationCollision) {
		this.isAnnotationCollision = isAnnotationCollision;
	}

	public boolean isAnchoredToStartElement() {
		return anchoredToStartElement;
	}

	public void setAnchoredToStartElement(boolean anchoredToStartElement) {
		this.anchoredToStartElement = anchoredToStartElement;
	}

	public boolean isAnchoredToStopElement() {
		return anchoredToStopElement;
	}

	public void setAnchoredToStopElement(boolean anchoredToStopElement) {
		this.anchoredToStopElement = anchoredToStopElement;
	}

	public boolean isPreviousSOfNextSlice() {
		return isPreviousSOfNextSlice;
	}

	public void setPreviousSOfNextSlice(boolean isPreviousSOfNextSlice) {
		this.isPreviousSOfNextSlice = isPreviousSOfNextSlice;
	}

	public boolean isNextSOfPreviousSlice() {
		return isNextSOfPreviousSlice;
	}

	public void setNextSOfPreviousSlice(boolean isNextSOfPreviousSlice) {
		this.isNextSOfPreviousSlice = isNextSOfPreviousSlice;
	}

	public AbsoPropSGenoRegiInserItem getsIfMissingTargetPreviousGenomicRegionSInsertion() {
		return sIfMissingTargetPreviousGenomicRegionSInsertion;
	}

	public void setsIfMissingTargetPreviousGenomicRegionSInsertion(
			AbsoPropSGenoRegiInserItem sIfMissingTargetPreviousGenomicRegionSInsertion) {
		this.sIfMissingTargetPreviousGenomicRegionSInsertion = sIfMissingTargetPreviousGenomicRegionSInsertion;
	}

	public AbsoPropSGenoRegiInserItem getsIfMissingTargetNextGenomicRegionSInsertion() {
		return sIfMissingTargetNextGenomicRegionSInsertion;
	}

	public void setsIfMissingTargetNextGenomicRegionSInsertion(
			AbsoPropSGenoRegiInserItem sIfMissingTargetNextGenomicRegionSInsertion) {
		this.sIfMissingTargetNextGenomicRegionSInsertion = sIfMissingTargetNextGenomicRegionSInsertion;
	}
	

	/*
	public static void addS_INSERTION(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_INSERTION);
		//newAPSI.setSyntenyNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}
	
	
	
	public static void addS_PARTIAL_LEFT_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_PARTIAL_LEFT_HOMOLOGS_BLOCK);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addS_PARTIAL_RIGHT_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addS_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addS_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addS_PARTIAL_LEFT_INSERTION(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_PARTIAL_LEFT_INSERTION);
		//newAPSI.setSyntenyNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addS_PARTIAL_RIGHT_INSERTION(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_PARTIAL_RIGHT_INSERTION);
		//newAPSI.setSyntenyNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}
	


	public static void addS_MISSING_TARGET_Q_HOMOLOGS_BLOCK_RIGHT(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_RIGHT);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addS_MISSING_TARGET_Q_HOMOLOGS_BLOCK_LEFT(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_MISSING_TARGET_Q_HOMOLOGS_BLOCK_LEFT);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addS_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_RIGHT(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_RIGHT);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addS_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_LEFT(
			ArrayList<AbsoluteProportionSSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double sPercentStart, double sPercentStop) {
		AbsoluteProportionSSyntenyItem newAPSI = new AbsoluteProportionSSyntenyItem();
		newAPSI.setsSyntenyEnumBlockType(AbsoluteProportionSSyntenyItem.S_SyntenyEnumBlockType.S_MISSING_TARGET_Q_REVERSE_HOMOLOGS_BLOCK_LEFT);
		newAPSI.setsNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		//newAPSI.setqPercentStart(qPercentStart);
		//newAPSI.setqPercentStop(qPercentStop);
		newAPSI.setsPercentStart(sPercentStart);
		newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}
	*/
	


}
