package fr.inra.jouy.shared.pojos.databaseMapping;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

public class GeneMatchItem extends LightGeneMatchItem implements IsSerializable{

	
	
	private int qOrganismId = -1;
	private int qElementId = -1;
	private int qGeneId = -1;
	private GeneItem associatedQGeneItem = null;	
	private int qLength = -1;
	private int sOrganismId = -1;
	private int sElementId = -1;
	private int sGeneId = -1;
	private GeneItem associatedSGeneItem = null;
	private int sLength = -1;
	private int rank = -1;
	private int qFirst = -1;
	private int qLast = -1;
	private double qLastFrac = -1;
	private int qAlignLength = -1;
	private int sFirst = -1;
	private int sLast = -1;
	private double sLastFrac = -1;
	private int sAlignLength = -1;

	

	
	public GeneMatchItem(){
		
	}
	
	/*
	public GeneMatchItem(int qGeneIdSent, GeneItem associatedQGeneItemSent, int sGeneIdSent, GeneItem associatedSGeneItemSent, int qFirstSent, int qAlignLengthSent, int sFirstSent, int sAlignLengthSent){
		
		this.qGeneId = qGeneIdSent;
		this.associatedQGeneItem = associatedQGeneItemSent;
		this.sGeneId = sGeneIdSent;
		this.associatedSGeneItem = associatedSGeneItemSent;
		this.qFirst = qFirstSent;
		if(qFirstSent == 0 || 1/qFirstSent >= 1 || 1/qFirstSent <= 0){
			this.qFirstFrac = 0.25;///GeneWidget.WIDTH_OF_MATCH_BAR_IN_PX
		}else{
			this.qFirstFrac = 1/qFirstSent;///GeneWidget.WIDTH_OF_MATCH_BAR_IN_PX
		}
		
		this.qAlignLength = qAlignLengthSent;
		if(qAlignLengthSent == 0 || 1/qAlignLengthSent >= 1 || 1/qAlignLengthSent <= 0){
			this.qAlignFrac = 0.25;///GeneWidget.WIDTH_OF_MATCH_BAR_IN_PX
		}else{
			this.qAlignFrac = 1/qAlignLengthSent;///GeneWidget.WIDTH_OF_MATCH_BAR_IN_PX
		}
		this.sFirst = sFirstSent;
		if(sFirstSent == 0 || 1/sFirstSent >= 1 || 1/sFirstSent <= 0){
			this.sFirstFrac = 0.2;///GeneWidget.WIDTH_OF_MATCH_BAR_IN_PX
		}else{
			this.sFirstFrac = 1/sFirstSent;///GeneWidget.WIDTH_OF_MATCH_BAR_IN_PX
		}
		this.sAlignLength = sAlignLengthSent;
		if(sAlignLengthSent == 0 && 1/sAlignLengthSent >= 1 || 1/sAlignLengthSent <= 0){
			this.sAlignFrac = 0.6;///GeneWidget.WIDTH_OF_MATCH_BAR_IN_PX
		}else{
			this.sAlignFrac = 1/sAlignLengthSent;///GeneWidget.WIDTH_OF_MATCH_BAR_IN_PX
		}
		
		
		
	}
	*/
	
	public int getQOrganismId() {
		return qOrganismId;
	}

	public void setQOrganismId(int organismId) {
		qOrganismId = organismId;
	}

	public int getQElementId() {
		return qElementId;
	}

	public void setQElementId(int elementId) {
		qElementId = elementId;
	}

	public int getQGeneId() {
		return qGeneId;
	}

	public void setQGeneId(int geneId) {
		qGeneId = geneId;
	}

	public int getQLength() {
		return qLength;
	}

	public void setQLength(int length) {
		qLength = length;
	}

	public int getSOrganismId() {
		return sOrganismId;
	}

	public void setSOrganismId(int organismId) {
		sOrganismId = organismId;
	}

	public int getSElementId() {
		return sElementId;
	}

	public void setSElementId(int elementId) {
		sElementId = elementId;
	}

	public int getSGeneId() {
		return sGeneId;
	}

	public void setSGeneId(int geneId) {
		sGeneId = geneId;
	}

	public int getSLength() {
		return sLength;
	}

	public void setSLength(int length) {
		sLength = length;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getQFirst() {
		return qFirst;
	}

	public void setQFirst(int first) {
		qFirst = first;
	}


	public int getQLast() {
		return qLast;
	}

	public void setQLast(int last) {
		qLast = last;
	}

	public double getQLastFrac() {
		return qLastFrac;
	}

	public void setQLastFrac(double lastFrac) {
		qLastFrac = lastFrac;
	}

	public int getQAlignLength() {
		return qAlignLength;
	}

	public void setQAlignLength(int alignLength) {
		qAlignLength = alignLength;
	}

	public int getSFirst() {
		return sFirst;
	}

	public void setSFirst(int first) {
		sFirst = first;
	}


	public int getSLast() {
		return sLast;
	}

	public void setSLast(int last) {
		sLast = last;
	}

	public double getSLastFrac() {
		return sLastFrac;
	}

	public void setSLastFrac(double lastFrac) {
		sLastFrac = lastFrac;
	}

	public int getSAlignLength() {
		return sAlignLength;
	}

	public void setSAlignLength(int alignLength) {
		sAlignLength = alignLength;
	}


	public GeneItem getAssociatedQGeneItem() {
		return associatedQGeneItem;
	}

	public void setAssociatedQGeneItem(GeneItem associatedQGeneItem) {
		this.associatedQGeneItem = associatedQGeneItem;
	}

	public GeneItem getAssociatedSGeneItem() {
		return associatedSGeneItem;
	}

	public void setAssociatedSGeneItem(GeneItem associatedSGeneItem) {
		this.associatedSGeneItem = associatedSGeneItem;
	}
	
	
	
	public void clearAllReferences(){
		if(associatedQGeneItem != null){
			//associatedQGeneItem.clearAllReferences();
			associatedQGeneItem = null;
		}
		if(associatedSGeneItem != null){
			//associatedSGeneItem.clearAllReferences();
			associatedSGeneItem = null;
		}
		
	}
}
