package fr.inra.jouy.shared.pojos.databaseMapping;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class TaxoItem implements IsSerializable{

	
	private String name = null;
	private Integer ncbiTaxoId = -1;
	//private String subStrain = null;
	//private boolean isSelectableAsOrganism  = false;
	private OrganismItem associatedOrganismItem = null;
	//private String listAllLightElementItemAsStringTypeAccnum = "";
	
	private TaxoItem parentTaxoItem = null;
	private ArrayList<TaxoItem> taxoChildrens = new ArrayList<TaxoItem>();
	
	private ArrayList<Integer> fullPathInHierarchie = new ArrayList<Integer>();
	private Integer sumOfLeafsUnderneath = -1;
	private boolean isRefGenomeInRefNodeContext = false;
	private boolean isAssertPresenceInRefNodeContext = false;
	private boolean isAssertAbsenceInRefNodeContext = false;
	private boolean isAssertEitherPresenceOrAbsenceInRefNodeContext = false;

	public TaxoItem(){
	}
	
	public TaxoItem(TaxoItem parentTaxoItemSent){
		parentTaxoItem = parentTaxoItemSent;
	}
	
	public TaxoItem(String name, Integer ncbiTaxoId, TaxoItem parentTaxoItemSent) {
		super();
		this.name = name.trim();
		this.ncbiTaxoId = ncbiTaxoId;
		parentTaxoItem = parentTaxoItemSent;
	}
	

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name.trim();
	}

	public String getFullName() {
		if(associatedOrganismItem != null){
			return associatedOrganismItem.getFullName();
		}else {
			return name;
		}
	}
	
	public Integer getNcbiTaxoId() {
		return ncbiTaxoId;
	}

	public void setNcbiTaxoId(Integer ncbiTaxoId) {
		this.ncbiTaxoId = ncbiTaxoId;
	}

//	public boolean isSelectableAsOrganism() {
//		return isSelectableAsOrganism;
//	}
//
//	public void setSelectableAsOrganism(boolean isSelectableAsOrganism) {
//		this.isSelectableAsOrganism = isSelectableAsOrganism;
//	}

	public ArrayList<TaxoItem> getTaxoChildrens() {
		return taxoChildrens;
	}

	public void setTaxoChildrens(ArrayList<TaxoItem> taxoChildrens) {
		this.taxoChildrens = taxoChildrens;
	}

	public ArrayList<Integer> getFullPathInHierarchie() {
		return fullPathInHierarchie;
	}

	public void setFullPathInHierarchie(ArrayList<Integer> fullPathInHierarchie) {
		this.fullPathInHierarchie = fullPathInHierarchie;
	}

	public Integer getSumOfLeafsUnderneath() {
		return sumOfLeafsUnderneath;
	}

	public void setSumOfLeafsUnderneath(Integer sumOfLeafsUnderneath) {
		this.sumOfLeafsUnderneath = sumOfLeafsUnderneath;
	}


	public boolean isRefGenomeInRefNodeContext() {
		return isRefGenomeInRefNodeContext;
	}

	public void setRefGenomeInRefNodeContext(boolean isRefGenomeInRefNodeContext) {
		this.isRefGenomeInRefNodeContext = isRefGenomeInRefNodeContext;
	}

	public boolean isAssertPresenceInRefNodeContext() {
		return isAssertPresenceInRefNodeContext;
	}

	public void setAssertPresenceInRefNodeContext(
			boolean isAssertPresenceInRefNodeContext) {
		this.isAssertPresenceInRefNodeContext = isAssertPresenceInRefNodeContext;
	}

	public boolean isAssertAbsenceInRefNodeContext() {
		return isAssertAbsenceInRefNodeContext;
	}

	public void setAssertAbsenceInRefNodeContext(
			boolean isAssertAbsenceInRefNodeContext) {
		this.isAssertAbsenceInRefNodeContext = isAssertAbsenceInRefNodeContext;
	}

	public boolean isAssertEitherPresenceOrAbsenceInRefNodeContext() {
		return isAssertEitherPresenceOrAbsenceInRefNodeContext;
	}

	public void setAssertEitherPresenceOrAbsenceInRefNodeContext(
			boolean isAssertEitherPresenceOrAbsenceInRefNodeContext) {
		this.isAssertEitherPresenceOrAbsenceInRefNodeContext = isAssertEitherPresenceOrAbsenceInRefNodeContext;
	}

	public TaxoItem getParentTaxoItem() {
		return parentTaxoItem;
	}

	public void setParentTaxoItem(TaxoItem parentTaxoItem) {
		this.parentTaxoItem = parentTaxoItem;
	}

	public OrganismItem getAssociatedOrganismItem() {
		return associatedOrganismItem;
	}

	public void setAssociatedOrganismItem(OrganismItem associatedOrganismItem) {
		this.associatedOrganismItem = associatedOrganismItem;
	}

	
}
