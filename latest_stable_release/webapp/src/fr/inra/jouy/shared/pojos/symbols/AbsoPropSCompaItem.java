package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.interfaceIs.IsSComparable;
import fr.inra.jouy.shared.pojos.symbols.AbsoPropSCompaSSpanItem.SEnumBlockType;

public class AbsoPropSCompaItem implements Comparable<AbsoPropSCompaItem>, IsSComparable, IsSerializable {

	
	private double sPercentStart = -1;
	
	public void setsPercentStart(double sPercentStart) {
		this.sPercentStart = sPercentStart;
	}

	public double getsPercentStart() {
		return sPercentStart;
		/*if(this instanceof AbsoluteProportionSElementItem){
			if(((AbsoluteProportionSElementItem)this).getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_STOP_S_INSERTION)==0){
				return ((AbsoluteProportionSElementItem)this).getsPercentStop();
			}else{
				return sPercentStart;
			}
		}else if(this instanceof AbsoluteProportionSGenomicRegionInsertionItem){
			if(((AbsoluteProportionSGenomicRegionInsertionItem)this).getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_STOP_S_INSERTION)==0){
				return ((AbsoluteProportionSGenomicRegionInsertionItem)this).getsPercentStop();
			}else{
				return sPercentStart;
			}
    	}else{
    		System.err.println("Error in AbsoluteProportionSComparableItem getsPercentStart : apqcIT not instanceOf defined cases");
    		Window.alert("Error in AbsoluteProportionSComparableItem getsPercentStart : apqcIT not instanceOf defined cases");
    		return sPercentStart;
    	}*/
	}

	public int compareTo(AbsoPropSCompaItem absoluteProportionSComparableItemSent) {
       
    	double sPercentStartThis = -1;
    	double sPercentStartCompa = -1;
    	
    	if(this instanceof AbsoPropSCompaSSpanItem){
    		if(((AbsoPropSCompaSSpanItem)this).getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_STOP_S_INSERTION)==0){
    			sPercentStartThis = ((AbsoPropSCompaSSpanItem)this).getsPercentStop();
    		}else{
    			sPercentStartThis = this.getsPercentStart();
    		}
    	}else{
    		System.err.println("Error in AbsoluteProportionSComparableItem compareTo : not recognized class sent for this");
    		//Window.alert("Error in AbsoluteProportionSComparableItem compareTo : not recognized class sent for this");
    	}
    	if(absoluteProportionSComparableItemSent instanceof AbsoPropSCompaSSpanItem){
    		if(((AbsoPropSCompaSSpanItem)absoluteProportionSComparableItemSent).getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_STOP_S_INSERTION)==0){
    			sPercentStartCompa = ((AbsoPropSCompaSSpanItem)absoluteProportionSComparableItemSent).getsPercentStop();
    		}else{
    			sPercentStartCompa = absoluteProportionSComparableItemSent.getsPercentStart();
    		}
    	}else{
    		System.err.println("Error in AbsoluteProportionSComparableItem compareTo : not recognized class sent for absoluteProportionSComparableSyntenyItemSent");
    		//Window.alert("Error in AbsoluteProportionSComparableItem compareTo : not recognized class sent for absoluteProportionSComparableSyntenyItemSent");
    	}
    	
		if(sPercentStartThis > sPercentStartCompa){
            return 1;
        }else if(sPercentStartThis < sPercentStartCompa){
            return -1;
        }else{
        	double sPercentStopThis = -1;
        	double sPercentStopCompa = -1;
        	
        	boolean thisIsSCutStop = false;
        	boolean thisIsSCutSart = false;
        	boolean compaIsSCutStop = false;
        	boolean compaIsSCutStart = false;
        	
        	if(this instanceof AbsoPropSCompaSSpanItem){
        		sPercentStopThis = ((AbsoPropSCompaSSpanItem)this).getsPercentStop();
        		if(((AbsoPropSCompaSSpanItem)this).getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_STOP_S_INSERTION)==0){
        			thisIsSCutStop = true;
        		}
        		if(((AbsoPropSCompaSSpanItem)this).getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_START_S_INSERTION)==0){
        			thisIsSCutSart = true;
        		}
        	}
        	if(absoluteProportionSComparableItemSent instanceof AbsoPropSCompaSSpanItem){
        		sPercentStopCompa = ((AbsoPropSCompaSSpanItem)absoluteProportionSComparableItemSent).getsPercentStop();
        		if(((AbsoPropSCompaSSpanItem)absoluteProportionSComparableItemSent).getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_STOP_S_INSERTION)==0){
        			compaIsSCutStop = true;
        		}
        		if(((AbsoPropSCompaSSpanItem)absoluteProportionSComparableItemSent).getSEnumBlockType().compareTo(SEnumBlockType.S_CUT_START_S_INSERTION)==0){
        			compaIsSCutStart = true;
        		}
        	}
        	
        	if(thisIsSCutStop && compaIsSCutStop){
        		return 0;
        	}else if(thisIsSCutStop && compaIsSCutStart){
        		return -1;
        	}else if(thisIsSCutSart && compaIsSCutStop){
        		return 1;
        	}else if(thisIsSCutSart && compaIsSCutStart){
        		return 0;
        	}else if(thisIsSCutStop){
        		return 1;
        	}else if(compaIsSCutStop){
        		return -1;
        	}else if(sPercentStopThis>0 && sPercentStopCompa>0){
                if(sPercentStopThis > sPercentStopCompa){
                    return -1;
                }else if(sPercentStopThis < sPercentStopCompa){
                    return 1;
                }else{
                	return 0;
                }
        	}else{
                return -1;
        	}
        }
		
	}
	
}
