package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.pojos.applicationItems.GeneWidgetStyleItem;

public class AbsoPropSGeneInserItem extends AbsoPropSElemItem implements IsSerializable{
	
	//change in SComparableGeneInsertionItem too
	//change in LightGeneItem too
	private int sGeneId = -1;
	//private String sMostSignificantGeneName = null;
	private String sName = null;
	private String sLocusTag = null;
	private int sStrand = 0; // CAN BE 1 OR -1
	private GeneWidgetStyleItem sStyleItem = null;
	private int sPbStartGeneInElement = -1;
	private int sPbStopGeneInElement = -1;
	
	private int ifMissingTargetQGeneId = -1;
	//private String ifMissingTargetQMostSignificantGeneName = null;
	private String ifMissingTargetQName = null;
	private String ifMissingTargetQLocusTag = null;
	private int ifMissingTargetQStrand = 0; // CAN BE 1 OR -1
	
	private AbsoPropSGenoRegiInserItem ifMissingTargetPreviousGenomicRegionSInsertion = null;
	private AbsoPropSGenoRegiInserItem ifMissingTargetNextGenomicRegionSInsertion = null;
	
	private int sOrigamiAlignmentPairsType = -1;
	private long sOrigamiAlignmentId = -1;
	
	public AbsoPropSGeneInserItem(){
		
	}
	
	public AbsoPropSGeneInserItem(AbsoPropSGeneInserItem absoluteProportionSGeneInsertionItemSent){
		super((AbsoPropSElemItem)absoluteProportionSGeneInsertionItemSent);
		setsGeneId(absoluteProportionSGeneInsertionItemSent.getsGeneId());
		setsName(absoluteProportionSGeneInsertionItemSent.getsNameAsRawString());
		setsLocusTag(absoluteProportionSGeneInsertionItemSent.getsLocusTagAsRawString());
		setsStrand(absoluteProportionSGeneInsertionItemSent.getsStrand());
		setsStyleItem(absoluteProportionSGeneInsertionItemSent.getsStyleItem());
		setIfMissingTargetQGeneId(absoluteProportionSGeneInsertionItemSent.getIfMissingTargetQGeneId());
		setIfMissingTargetQLocusTag(absoluteProportionSGeneInsertionItemSent.getIfMissingTargetQLocusTagAsRawString());
		//setIfMissingTargetQMostSignificantGeneName(absoluteProportionSGeneInsertionItemSent.getIfMissingTargetQMostSignificantGeneName());
		setIfMissingTargetQName(absoluteProportionSGeneInsertionItemSent.getIfMissingTargetQNameAsRawString());
		setIfMissingTargetQStrand(absoluteProportionSGeneInsertionItemSent.getIfMissingTargetQStrand());
		
		setIfMissingTargetPreviousGenomicRegionSInsertion(absoluteProportionSGeneInsertionItemSent.getIfMissingTargetPreviousGenomicRegionSInsertion());
		setIfMissingTargetNextGenomicRegionSInsertion(absoluteProportionSGeneInsertionItemSent.getIfMissingTargetNextGenomicRegionSInsertion());
		
		setsPbStartGeneInElement(absoluteProportionSGeneInsertionItemSent.getsPbStartGeneInElement());
		setsPbStopGeneInElement(absoluteProportionSGeneInsertionItemSent.getsPbStopGeneInElement());
		
		setsOrigamiAlignmentPairsType(absoluteProportionSGeneInsertionItemSent.getsOrigamiAlignmentPairsType());
		setsOrigamiAlignmentId(absoluteProportionSGeneInsertionItemSent.getsOrigamiAlignmentId());
	}
	
	public AbsoPropSGeneInserItem(AbsoPropQSGeneHomoItem apqsghiSent,
			SEnumBlockType sEnumBlockTypeSent, double percentStartSent, double percentStopSent){
		super((AbsoPropQSElemItem) apqsghiSent,
				sEnumBlockTypeSent, percentStartSent, percentStopSent);
		
		setsGeneId(apqsghiSent.getQsSGeneId());
		setsName(apqsghiSent.getQsSNameAsRawString());
		setsLocusTag(apqsghiSent.getQsSLocusTagAsRawString());
		setsStrand(apqsghiSent.getQsSStrand());
		setsStyleItem(apqsghiSent.getQsStyleItem());
		setIfMissingTargetQGeneId(apqsghiSent.getQsQGeneId());
		setIfMissingTargetQLocusTag(apqsghiSent.getQsQLocusTagAsRawString());
		//setIfMissingTargetQMostSignificantGeneName(absoluteProportionSGeneInsertionItemSent.getIfMissingTargetQMostSignificantGeneName());
		setIfMissingTargetQName(apqsghiSent.getQsQNameAsRawString());
		setIfMissingTargetQStrand(apqsghiSent.getQsQStrand());
		
		setIfMissingTargetPreviousGenomicRegionSInsertion(apqsghiSent.getPreviousGenomicRegionSInsertion());
		setIfMissingTargetNextGenomicRegionSInsertion(apqsghiSent.getNextGenomicRegionSInsertion());
		
		setsPbStartGeneInElement(apqsghiSent.getQsSPbStartGeneInElement());
		setsPbStopGeneInElement(apqsghiSent.getQsSPbStopGeneInElement());
		
		setsOrigamiAlignmentPairsType(apqsghiSent.getQsOrigamiAlignmentPairsType());
		setsOrigamiAlignmentId(apqsghiSent.getSyntenyOrigamiAlignmentId());
		
		
	}
	
	public int getsGeneId() {
		return sGeneId;
	}
	
	public void setsGeneId(int sGeneId) {
		this.sGeneId = sGeneId;
	}
	
	public String getsMostSignificantGeneNameAsHTMLPlusLink() {
		if(sName != null && sName.length() > 0){
//			if (sName.contains("UniProtKB") == true ) {
//	            String locus = sName.replaceAll("\\{UniProtKB/.+:.+", "");
//	            String link = sName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
//	            link = link.replaceAll("\\}", "");
//	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
//			}else {
//		        return sName;
//		    }
			return getsNameAsHTMLPlusLink();
		}else if (sLocusTag != null && sLocusTag.length() > 0){
//			if (sLocusTag.contains("UniProtKB") == true ) {
//	            String locus = sLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
//	            String link = sLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
//	            link = link.replaceAll("\\}", "");
//	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
//			}else {
//	            return sLocusTag;
//			}
			return getsLocusTagAsHTMLPlusLink();
		}else{
			return "NO_NAME";
		}
	}
	
	public String getsMostSignificantGeneNameAsStrippedString() {
		if(sName != null && sName.length() > 0){
//			if (sName.contains("UniProtKB") == true ) {
//	            String locus = sName.replaceAll("\\{UniProtKB/.+:.+", "");
//				return locus;
//			}else {
//				return sName;
//	        }
			return getsNameAsStrippedString();
		}else if (sLocusTag != null && sLocusTag.length() > 0){
//			if (sLocusTag.contains("UniProtKB") == true ) {
//	            String locus = sLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
//	            return locus;
//			}else {
//				return sLocusTag;
//	        }
			return getsLocusTagAsStrippedString();
		}else{
			return "NO_NAME";
		}
	}
	
	public String getsNameAsRawString() {
		return sName;
	}
	public String getsNameAsStrippedString() {
		if(sName != null){
			if (sName.contains("UniProtKB") == true ) {
	            String locus = sName.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return sName;
	        }
		}else{
			return "";
		}
		
	}
	public String getsNameAsHTMLPlusLink() {
		if(sName != null){
			if (sName.contains("UniProtKB") == true ) {
	            String locus = sName.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = sName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return sName;
		    }
		}else{
			return "";
		}
		
	}
	
	public void setsName(String sName) {
		this.sName = sName;
	}
	public String getsLocusTagAsRawString() {
		return sLocusTag;
	}
	public String getsLocusTagAsStrippedString() {
		if(sLocusTag != null){
			if (sLocusTag.contains("UniProtKB") == true ) {
	            String locus = sLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return sLocusTag;
	        }
		}else{
			return "";
		}
		
	}
	public String getsLocusTagAsHTMLPlusLink() {
		if(sLocusTag != null){
			if (sLocusTag.contains("UniProtKB") == true ) {
	            String locus = sLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = sLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return sLocusTag;
		    }
		}else{
			return "";
		}
		
	}
	
	public void setsLocusTag(String sLocusTag) {
		this.sLocusTag = sLocusTag;
	}
	public int getsStrand() {
		return sStrand;
	}
	public void setsStrand(int sStrand) {
		this.sStrand = sStrand;
	}

	public GeneWidgetStyleItem getsStyleItem() {
		if(sStyleItem == null){
			sStyleItem = new GeneWidgetStyleItem();
		}
		return sStyleItem;
	}

	public void setsStyleItem(GeneWidgetStyleItem sStyleItem) {
		this.sStyleItem = sStyleItem;
	}

	public int getIfMissingTargetQGeneId() {
		return ifMissingTargetQGeneId;
	}

	public void setIfMissingTargetQGeneId(int ifMissingTargetQGeneId) {
		this.ifMissingTargetQGeneId = ifMissingTargetQGeneId;
	}

	public String getIfMissingTargetQMostSignificantGeneNameAsHTMLPlusLink() {
		if(ifMissingTargetQName != null && ifMissingTargetQName.length() > 0){
			//setQMostSignificantGeneName(qName);
			return getIfMissingTargetQNameAsHTMLPlusLink();
		}else if (ifMissingTargetQLocusTag != null && ifMissingTargetQLocusTag.length() > 0){
//			String strippedLocusTag = null;
//			strippedLocusTag = ifMissingTargetQLocusTag.split("\\s", 2)[0];
//			if(strippedLocusTag.length() > 0){
//				return strippedLocusTag;
//			}else{
//				return ifMissingTargetQLocusTag;
//			}
			return getIfMissingTargetQLocusTagAsHTMLPlusLink();
		}else{
			return "NO_NAME";
		}
	}
	
	public String getIfMissingTargetQMostSignificantGeneNameAsStrippedString() {
		if(ifMissingTargetQName != null && ifMissingTargetQName.length() > 0){
			//setQMostSignificantGeneName(qName);
			return getIfMissingTargetQNameAsStrippedString();
		}else if (ifMissingTargetQLocusTag != null && ifMissingTargetQLocusTag.length() > 0){
//			String strippedLocusTag = null;
//			strippedLocusTag = ifMissingTargetQLocusTag.split("\\s", 2)[0];
//			if(strippedLocusTag.length() > 0){
//				return strippedLocusTag;
//			}else{
//				return ifMissingTargetQLocusTag;
//			}
			return getIfMissingTargetQLocusTagAsStrippedString();
		}else{
			return "NO_NAME";
		}
	}
//	public void setIfMissingTargetQMostSignificantGeneName(
//			String ifMissingTargetQMostSignificantGeneName) {
//		this.ifMissingTargetQMostSignificantGeneName = ifMissingTargetQMostSignificantGeneName;
//	}

	public String getIfMissingTargetQNameAsRawString() {
		return ifMissingTargetQName;
	}
	public String getIfMissingTargetQNameAsStrippedString() {
		if(ifMissingTargetQName != null){
			if (ifMissingTargetQName.contains("UniProtKB") == true ) {
	            String locus = ifMissingTargetQName.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return ifMissingTargetQName;
	        }
		}else{
			return "";
		}
		
	}
	public String getIfMissingTargetQNameAsHTMLPlusLink() {
		if(ifMissingTargetQName != null){
			if (ifMissingTargetQName.contains("UniProtKB") == true ) {
	            String locus = ifMissingTargetQName.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = ifMissingTargetQName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return ifMissingTargetQName;
		    }
		}else{
			return "";
		}
		
	}
	public void setIfMissingTargetQName(String ifMissingTargetQName) {
		this.ifMissingTargetQName = ifMissingTargetQName;
	}

	public String getIfMissingTargetQLocusTagAsRawString() {
		return ifMissingTargetQLocusTag;
	}
	public String getIfMissingTargetQLocusTagAsStrippedString() {
		if(ifMissingTargetQLocusTag != null){
			if (ifMissingTargetQLocusTag.contains("UniProtKB") == true ) {
	            String locus = ifMissingTargetQLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return ifMissingTargetQLocusTag;
	        }
		}else{
			return "";
		}
		
	}
	
	public String getIfMissingTargetQLocusTagAsHTMLPlusLink() {
		if(ifMissingTargetQLocusTag != null){
			if (ifMissingTargetQLocusTag.contains("UniProtKB") == true ) {
	            String locus = ifMissingTargetQLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = ifMissingTargetQLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return ifMissingTargetQLocusTag;
		    }
		}else{
			return "";
		}
		
	}
	
	public void setIfMissingTargetQLocusTag(String ifMissingTargetQLocusTag) {
		this.ifMissingTargetQLocusTag = ifMissingTargetQLocusTag;
	}

	public int getIfMissingTargetQStrand() {
		return ifMissingTargetQStrand;
	}

	public void setIfMissingTargetQStrand(int ifMissingTargetQStrand) {
		this.ifMissingTargetQStrand = ifMissingTargetQStrand;
	}

	public int getsPbStartGeneInElement() {
		return sPbStartGeneInElement;
	}

	public void setsPbStartGeneInElement(int sPbStartGeneInElement) {
		this.sPbStartGeneInElement = sPbStartGeneInElement;
	}

	public int getsPbStopGeneInElement() {
		return sPbStopGeneInElement;
	}

	public void setsPbStopGeneInElement(int sPbStopGeneInElement) {
		this.sPbStopGeneInElement = sPbStopGeneInElement;
	}

	public int getsOrigamiAlignmentPairsType() {
		return sOrigamiAlignmentPairsType;
	}

	public void setsOrigamiAlignmentPairsType(int sOrigamiAlignmentPairsType) {
		this.sOrigamiAlignmentPairsType = sOrigamiAlignmentPairsType;
	}

	//Done isDataFromMirrorQuery
	public long getsOrigamiAlignmentId() {
		return sOrigamiAlignmentId;
	}

	//Done isDataFromMirrorQuery
	public void setsOrigamiAlignmentId(long l) {
		this.sOrigamiAlignmentId = l;
	}

	public AbsoPropSGenoRegiInserItem getIfMissingTargetPreviousGenomicRegionSInsertion() {
		return ifMissingTargetPreviousGenomicRegionSInsertion;
	}

	public void setIfMissingTargetPreviousGenomicRegionSInsertion(
			AbsoPropSGenoRegiInserItem ifMissingTargetPreviousGenomicRegionSInsertion) {
		this.ifMissingTargetPreviousGenomicRegionSInsertion = ifMissingTargetPreviousGenomicRegionSInsertion;
	}

	public AbsoPropSGenoRegiInserItem getIfMissingTargetNextGenomicRegionSInsertion() {
		return ifMissingTargetNextGenomicRegionSInsertion;
	}

	public void setIfMissingTargetNextGenomicRegionSInsertion(
			AbsoPropSGenoRegiInserItem ifMissingTargetNextGenomicRegionSInsertion) {
		this.ifMissingTargetNextGenomicRegionSInsertion = ifMissingTargetNextGenomicRegionSInsertion;
	}
	
	
}
