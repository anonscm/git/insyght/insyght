package fr.inra.jouy.shared.pojos.databaseMapping;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/


import com.google.gwt.user.client.rpc.IsSerializable;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.interfaceIs.IsStringifyToJSON;
import fr.inra.jouy.shared.interfaceIs.IsMappingKeyValueToFields;

public class LightOrganismItem implements IsSerializable, IsMappingKeyValueToFields, IsStringifyToJSON {

    private int organismId = -1; 
    private String species = null;
    private String strain = null;
    private String substrain = null;
    private int taxonId = -1;
    private Boolean isPublic = null;
    private int score = -1;
    private int positionInResultList = -1;
    
    public LightOrganismItem() {
    	
    }
    
	public LightOrganismItem(LightOrganismItem loSent) {
		setOrganismId(loSent.getOrganismId());
		setSpecies(loSent.getSpecies());
		setStrain(loSent.getStrain());
		setSubstrain(loSent.getSubstrain());
		setTaxonId(loSent.getTaxonId());
		setPublic(loSent.isPublic());
		setScore(loSent.getScore());
		setPositionInResultList(loSent.getPositionInResultList());
    }


	public int getOrganismId() {
		return organismId;
	}

	public void setOrganismId(int organismId) {
		this.organismId = organismId;
	}


	public String getSpecies() {
		return species;
	}


	public void setSpecies(String species) {
		this.species = species;
	}


	public String getStrain() {
		return strain;
	}


	public void setStrain(String strain) {
		this.strain = strain;
	}

	
	public String getSubstrain() {
		return substrain;
	}


	public void setSubstrain(String substrain) {
		this.substrain = substrain;
	}


	public int getTaxonId() {
		return taxonId;
	}


	public void setTaxonId(int taxonId) {
		this.taxonId = taxonId;
	}


	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}


	public boolean isPublic() {
		return isPublic;
	}

	String lightFullNameAsString(){
		String sToReturn = "";
		if(species != null){
			sToReturn += species;
		}
		if(strain != null){
			if(!strain.isEmpty() && species.indexOf(strain) == -1){
				sToReturn += " strain "+strain;
			}
		}
		if(substrain != null){
			if(!substrain.isEmpty() && species.indexOf(strain) == -1){
				sToReturn += " (substrain="+substrain+")";
			}
		}
		if(taxonId >= 0){
			//if(!taxonId.isEmpty()){
				sToReturn += ", taxo_id = "+taxonId;
			//}
		}
		return sToReturn;
	}
	
	public String getFullName(){
		return lightFullNameAsString();
	}

	//Done positionInResultList
	public int getScore() {
		return score;
	}

	//Done positionInResultList
	public void setScore(int score) {
		this.score = score;
	}
	

	public int getPositionInResultList() {
		return positionInResultList;
	}

	public void setPositionInResultList(int positionInResultList) {
		this.positionInResultList = positionInResultList;
		
	}


	//mapKeyIntValue
	
	@Override
	public boolean mapKeyIntValueToObjectField(String key, Integer value) {
	    //private int organismId = -1;
	    //private int taxonId = -1;
	    //private int score = -1;
		if(key.equals("organism_id")){
			setOrganismId(value);
			return true;
		} 
		if (key.equals("taxon_id")){
			setTaxonId(value);
			return true;
		} 
		if (key.equals("score")){
			setScore(value);
			return true;
		}
		if (key.equals("positionInResultList")){
			setPositionInResultList(value);
			return true;
		}
		return false;
	}
	

	@Override
	public boolean mapKeyStringValueToObjectField(String key, String value) {
	    //private String species = null;
	    //private String strain = null;
	    //private String substrain = null;
		if(key.equals("species")){
			setSpecies(value);
			return true;
		} 
		if (key.equals("strain")){
			setStrain(value);
			return true;
		} 
		if (key.equals("substrain")){
			setSubstrain(value);
			return true;
		}
		return false;
	}

	@Override
	public boolean mapKeyBooleanValueToObjectField(String key, Boolean value) {
	    //private boolean isPublic = true;
	    if(key.equals("ispublic")){
			setPublic(value);
			return true;
		}
	    return false;
	}

	@Override
	public boolean mapKeyLongValueToObjectField(String key, Long value) {
		return false;
	}

	@Override
	public boolean mapKeyDoubleValueToObjectField(String key, Double value) {
		return false;
	}

	@Override
	public boolean mapKeyFloatValueToObjectField(String key, Float value) {
		return false;
	}

	@Override
	public boolean mapKeyUnknowTypeValueToObjectField(String key, String value) {
		// int
		if(key.equals("organism_id")){
			return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
		} 
		if (key.equals("taxon_id")){
			return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
		} 
		if (key.equals("score")){
			return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
		}
		if (key.equals("positionInResultList")){
			return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
		}
		// string
		if(key.equals("species")){
			return mapKeyStringValueToObjectField(key, value);
		} 
		if (key.equals("strain")){
			return mapKeyStringValueToObjectField(key, value);
		} 
		if (key.equals("substrain")){
			return mapKeyStringValueToObjectField(key, value);
		}
		//boolean
	    if(key.equals("ispublic")){
			return mapKeyBooleanValueToObjectField(key, Boolean.parseBoolean(value));
		}
		return false;
	}

	@Override
	public String stringifyToJSON(boolean encloseWithCurlyBraces, boolean propagateToParentClassViaSuper) throws Exception {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String field2String = "";
		//no super.toJSON as does not extends another class
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("organismId",organismId,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("species",species,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("strain",strain,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("substrain",substrain,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("taxonId",taxonId,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringBoolean("isPublic", isPublic, field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("score",score,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("positionInResultList",positionInResultList,-1,field2String.isEmpty());
		
		if( encloseWithCurlyBraces){
			return "{"+field2String+"}";
		} else {
			return field2String;
		}
	}



//	@Override
//	public void fromJSON(String JSON) throws Exception {
//		//"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]
//	    for (MatchResult matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON); matcher != null; matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON)) {
//			String keyIT = matcher.getGroup(1);
//			String valueIT = matcher.getGroup(2);
//			boolean mappingSuccessful = mapKeyUnknowTypeValueToObjectField(keyIT,valueIT);
//			if( ! mappingSuccessful){
//				throw new Exception("The key "+keyIT+" (value = "+valueIT+") has not been mapped to any attribute");
//			}
//	    }
//	}
	

	
}


