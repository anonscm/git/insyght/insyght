package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

public class AbsoPropQGenoRegiInserItem extends AbsoPropQElemItem implements IsSerializable {

	private int qGenomicRegionInsertionNumberGene = -1;
	private int qPbStartQGenomicRegionInsertionInElement = -1;
	private int qPbStopQGenomicRegionInsertionInElement = -1;
	
	private long qIfMissingTargetOrigamiAlignementId = 0; //can be negative if isDataFromMirrorQuery
	
	private boolean isAnnotationCollision = false;
	private boolean anchoredToStartElement = false;
	private boolean anchoredToStopElement = false;
	
	public AbsoPropQGenoRegiInserItem(){
		
	}
	
	public AbsoPropQGenoRegiInserItem(AbsoPropQGenoRegiInserItem apqsiSent){
		super((AbsoPropQElemItem)apqsiSent);
		
		setQGenomicRegionInsertionNumberGene(apqsiSent.getQGenomicRegionInsertionNumberGene());
		setqPbStartQGenomicRegionInsertionInElement(apqsiSent.getqPbStartQGenomicRegionInsertionInElement());
		setqPbStopQGenomicRegionInsertionInElement(apqsiSent.getqPbStopQGenomicRegionInsertionInElement());
		setqIfMissingTargetOrigamiAlignementId(apqsiSent.getqIfMissingTargetOrigamiAlignementId());
		setAnnotationCollision(apqsiSent.isAnnotationCollision());
		setAnchoredToStartElement(apqsiSent.isAnchoredToStartElement());
		setAnchoredToStopElement(apqsiSent.isAnchoredToStopElement());
	}
	
	public AbsoPropQGenoRegiInserItem(AbsoPropQSSyntItem apqssiSent,
			QEnumBlockType qEnumBlockTypeSent, double qPercentStartSent, double qPercentStopSent){
		super((AbsoPropQSElemItem)apqssiSent,
				qEnumBlockTypeSent, qPercentStartSent, qPercentStopSent);
		
		setQGenomicRegionInsertionNumberGene(apqssiSent.getSyntenyNumberGene());
		setqPbStartQGenomicRegionInsertionInElement(apqssiSent.getPbQStartSyntenyInElement());
		setqPbStopQGenomicRegionInsertionInElement(apqssiSent.getPbQStopSyntenyInElement());
		setqIfMissingTargetOrigamiAlignementId(apqssiSent.getSyntenyOrigamiAlignmentId());
		setAnnotationCollision(false);
		setAnchoredToStartElement(false);
		setAnchoredToStopElement(false);
		
		
	}
	
	public void setQGenomicRegionInsertionNumberGene(int qNumberGene) {
		this.qGenomicRegionInsertionNumberGene = qNumberGene;
	}

	public int getQGenomicRegionInsertionNumberGene() {
		return qGenomicRegionInsertionNumberGene;
	}

	public int getqPbStartQGenomicRegionInsertionInElement() {
		return qPbStartQGenomicRegionInsertionInElement;
	}

	public void setqPbStartQGenomicRegionInsertionInElement(
			int qPbStartQGenomicRegionInsertionInElement) {
		this.qPbStartQGenomicRegionInsertionInElement = qPbStartQGenomicRegionInsertionInElement;
	}

	public int getqPbStopQGenomicRegionInsertionInElement() {
		return qPbStopQGenomicRegionInsertionInElement;
	}

	public void setqPbStopQGenomicRegionInsertionInElement(
			int qPbStopQGenomicRegionInsertionInElement) {
		this.qPbStopQGenomicRegionInsertionInElement = qPbStopQGenomicRegionInsertionInElement;
	}

	//Done isDataFromMirrorQuery
	public long getqIfMissingTargetOrigamiAlignementId() {
		return qIfMissingTargetOrigamiAlignementId;
	}

	//Done isDataFromMirrorQuery
	public void setqIfMissingTargetOrigamiAlignementId(
			long l) {
		this.qIfMissingTargetOrigamiAlignementId = l;
	}

	public boolean isAnnotationCollision() {
		return isAnnotationCollision;
	}

	public void setAnnotationCollision(boolean isAnnotationCollision) {
		this.isAnnotationCollision = isAnnotationCollision;
	}

	public boolean isAnchoredToStartElement() {
		return anchoredToStartElement;
	}

	public void setAnchoredToStartElement(boolean anchoredToStartElement) {
		this.anchoredToStartElement = anchoredToStartElement;
	}

	public boolean isAnchoredToStopElement() {
		return anchoredToStopElement;
	}

	public void setAnchoredToStopElement(boolean anchoredToStopElement) {
		this.anchoredToStopElement = anchoredToStopElement;
	}


	/*
	public static void addQ_INSERTION(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_INSERTION);
		//newAPSI.setSyntenyNumberGene(numberGene);
		newAPSI.setqNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQ_PARTIAL_LEFT_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_PARTIAL_LEFT_HOMOLOGS_BLOCK);
		newAPSI.setqNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQ_PARTIAL_RIGHT_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_PARTIAL_RIGHT_HOMOLOGS_BLOCK);
		newAPSI.setqNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQ_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_PARTIAL_LEFT_REVERSE_HOMOLOGS_BLOCK);
		newAPSI.setqNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQ_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_PARTIAL_RIGHT_REVERSE_HOMOLOGS_BLOCK);
		newAPSI.setqNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQ_PARTIAL_LEFT_INSERTION(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_PARTIAL_LEFT_INSERTION);
		//newAPSI.setSyntenyNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		newAPSI.setqNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQ_PARTIAL_RIGHT_INSERTION(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_PARTIAL_RIGHT_INSERTION);
		//newAPSI.setSyntenyNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		newAPSI.setqNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQ_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_RIGHT);
		newAPSI.setqNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQ_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_MISSING_TARGET_S_HOMOLOGS_BLOCK_LEFT);
		newAPSI.setqNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}
	
	public static void addQ_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_RIGHT);
		newAPSI.setqNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}

	public static void addQ_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT(
			ArrayList<AbsoluteProportionQComparableSyntenyItem> listAbsoluteProportionSyntenyItemSent,
			int numberGene, double qPercentStart, double qPercentStop) {
		AbsoluteProportionQSyntenyItem newAPSI = new AbsoluteProportionQSyntenyItem();
		newAPSI.setqSyntenyEnumBlockType(AbsoluteProportionQSyntenyItem.Q_SyntenyEnumBlockType.Q_MISSING_TARGET_S_REVERSE_HOMOLOGS_BLOCK_LEFT);
		newAPSI.setqNumberGene(numberGene);
		//newAPSI.setQNumberGene(numberGene);
		//newAPSI.setSNumberGene(numberGene);
		newAPSI.setqPercentStart(qPercentStart);
		newAPSI.setqPercentStop(qPercentStop);
		//newAPSI.setsPercentStart(sPercentStart);
		//newAPSI.setsPercentStop(sPercentStop);
		listAbsoluteProportionSyntenyItemSent.add(newAPSI);
	}
	*/
}
