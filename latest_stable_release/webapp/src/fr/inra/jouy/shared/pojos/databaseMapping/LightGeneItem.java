package fr.inra.jouy.shared.pojos.databaseMapping;


/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/


import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Window;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import fr.inra.jouy.client.SessionStorageControler;
import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.comparators.LightGeneItemComparators;
import fr.inra.jouy.shared.interfaceIs.HasIntPrimaryIdAttribute;
import fr.inra.jouy.shared.interfaceIs.IsStringifyToJSON;
import fr.inra.jouy.shared.interfaceIs.IsMappingKeyValueToFields;


public class LightGeneItem extends LightElementItem implements IsSerializable,
	Comparable<LightGeneItem>, IsStringifyToJSON, HasIntPrimaryIdAttribute, IsMappingKeyValueToFields {
	

	private int geneId = -1;
	//private String mostSignificantGeneName = null;
	private String mostSignificantGeneNameAsStrippedHTMLPlusLink = null;
	private String mostSignificantGeneNameAsStrippedText = null;
	private String name = null;
	private String locusTag = null;
	private int start = -1;
	private int stop = -1;
	private int strand = 0; // CAN BE 1 OR -1
	
	public LightGeneItem() {
		super();
	}
	
	public int getGeneId() {
		return geneId;
	}
	public void setGeneId(int geneId) {
		this.geneId = geneId;
	}
	
	
	public String getMostSignificantGeneNameAsStrippedHTMLPlusLink() {
		if(mostSignificantGeneNameAsStrippedHTMLPlusLink != null){
			return mostSignificantGeneNameAsStrippedHTMLPlusLink;
		}else{
			if(name != null && name.length() > 0){
				return getNameAsHTMLPlusLink();
			}else if (locusTag != null && locusTag.length() > 0){
				return getLocusTagAsHTMLPlusLink();
				
			}else{
				setMostSignificantGeneNameAsStrippedHTMLPlusLink("NO_NAME");
				return "NO_NAME";
			}
		}
	}
	
	public void setMostSignificantGeneNameAsStrippedHTMLPlusLink(String mostSignificantGeneNameAsStrippedHTMLPlusLinkSent) {
		this.mostSignificantGeneNameAsStrippedHTMLPlusLink = mostSignificantGeneNameAsStrippedHTMLPlusLinkSent;
	}
	
	public String getMostSignificantGeneNameAsStrippedText() {
		if(mostSignificantGeneNameAsStrippedText != null){
			return mostSignificantGeneNameAsStrippedText;
		}else{
			if(name != null && name.length() > 0){
				return getNameAsStrippedString();
			}else if (locusTag != null && locusTag.length() > 0){
				return getLocusTagAsStrippedString();
			}else{
				setMostSignificantGeneNameAsStrippedText("NO_NAME");
				return "NO_NAME";
			}
		}
	}
	
	public void setMostSignificantGeneNameAsStrippedText(String mostSignificantGeneNameAsStrippedTextSent) {
		this.mostSignificantGeneNameAsStrippedText = mostSignificantGeneNameAsStrippedTextSent;
	}
	
	public String getNameAsRawString() {
		return name;
	}
	
	public String getNameAsStrippedString() {
		if(name != null){
			if (name.contains("UniProtKB") == true ) {
	            String locus = name.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return name;
	        }
		}else{
			return "";
		}
		
	}
	
	public String getNameAsHTMLPlusLink() {
		if(name != null){
			if (name.contains("UniProtKB") == true ) {
	            String locus = name.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = name.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>"
	            	//+" <a href=\"http://138.102.22.61:8080/PortailBioinfoDev/#&accession="+getAccession()
	  	            //+"&locustag="+getLocusTagAsStrippedString()+"\" target=\"_blank\" color=\"orange\">[IGO]</a>"
	              ;
	            
			}else {
		        return name
			        //+" <a href=\"http://138.102.22.61:8080/PortailBioinfoDev/#&accession="+getAccession()
			        //+"&locustag="+getLocusTagAsStrippedString()+"\" target=\"_blank\" color=\"orange\">[IGO]</a>"
			        ;
		    }
		}else{
			return "";
		}
		
		//return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getLocusTagAsRawString() {
		return locusTag;
	}
	
	public String getLocusTagAsStrippedString() {
		if(locusTag != null){
			if (locusTag.contains("UniProtKB") == true ) {
	            String locus = locusTag.replaceAll("\\{UniProtKB/.+:.+", "");
	            return locus;
			}else {
				return locusTag;
	        }
		}else{
			return "";
		}
		
		//return locusTag;
	}
	public String getLocusTagAsHTMLPlusLink() {
		if(locusTag != null){
			if (locusTag.contains("UniProtKB") == true ) {
	            String locus = locusTag.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = locusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
	            return locusTag;
			}
		}else{
			return "";
		}
		
		//return locusTag;
	}
	
	public void setLocusTag(String locusTag) {
		this.locusTag = locusTag;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getStop() {
		return stop;
	}
	public void setStop(int stop) {
		this.stop = stop;
	}
	public int getStrand() {
		return strand;
	}
	public void setStrand(int strand) {
		this.strand = strand;
	}
	
	public int compareTo(LightGeneItem lgiToCompareTo) {
		return this.getGeneId()-lgiToCompareTo.getGeneId();
	}
	
	@Override
	public boolean mapKeyIntValueToObjectField(String key, Integer value) {
		boolean keyAlreadyMappedToField = super.mapKeyIntValueToObjectField(key, value);
		if(keyAlreadyMappedToField){
			return true;
		} else {
			//private int geneId = -1;
			//private int start = -1;
			//private int stop = -1;
			//private int strand = 0; // CAN BE 1 OR -1
			if(key.equals("geneId")){
				setGeneId(value);
				return true;
			}
			if (key.equals("start")){
				setStart(value);
				return true;
			}
			if(key.equals("stop")){
				setStop(value);
				return true;
			}
			if (key.equals("strand")){
				setStrand(value);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean mapKeyStringValueToObjectField(String key, String value) {
		boolean keyAlreadyMappedToField = super.mapKeyStringValueToObjectField(key, value);
		if(keyAlreadyMappedToField){
			return true;
		} else {
			//private String mostSignificantGeneNameAsStrippedHTMLPlusLink = null;
			//private String mostSignificantGeneNameAsStrippedText = null;
			//private String name = null;
			//private String locusTag = null;
			if(key.equals("mostSignificantGeneNameAsStrippedHTMLPlusLink")){
				setMostSignificantGeneNameAsStrippedHTMLPlusLink(value);
				return true;
			}
			if (key.equals("mostSignificantGeneNameAsStrippedText")){
				setMostSignificantGeneNameAsStrippedText(value);
				return true;
			}
			if (key.equals("name")){
				setName(value);
				return true;
			}
			if (key.equals("locusTag")){
				setLocusTag(value);
				return true;
			}
		}
		return false;
	}


	@Override
	public boolean mapKeyUnknowTypeValueToObjectField(String key, String value) {
		boolean keyAlreadyMappedToField = super.mapKeyUnknowTypeValueToObjectField(key, value);
		if(keyAlreadyMappedToField){
			return true;
		} else {
			// int
			if(key.equals("geneId")){
				return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
			} 
			if (key.equals("start")){
				return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
			} 
			if (key.equals("stop")){
				return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
			}
			if (key.equals("strand")){
				return mapKeyIntValueToObjectField(key, Integer.parseInt(value));
			}
			// string			
			if(key.equals("mostSignificantGeneNameAsStrippedHTMLPlusLink")){
				return mapKeyStringValueToObjectField(key, value);
			} 
			if (key.equals("mostSignificantGeneNameAsStrippedText")){
				return mapKeyStringValueToObjectField(key, value);
			} 
			if (key.equals("name")){
				return mapKeyStringValueToObjectField(key, value);
			}
			if (key.equals("locusTag")){
				return mapKeyStringValueToObjectField(key, value);
			}
		}
		return false;
	}
	
	@Override
	public String stringifyToJSON(boolean encloseWithCurlyBraces, boolean propagateToParentClassViaSuper) throws Exception {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String field2String = "";
		if(propagateToParentClassViaSuper){
			field2String += super.stringifyToJSON(false, propagateToParentClassViaSuper);
		}
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("geneId",geneId,-1,field2String.isEmpty());
		//mostSignificantGeneNameAsStrippedHTMLPlusLink
		//mostSignificantGeneNameAsStrippedText
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("name",name,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringString("locusTag",locusTag,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("start",start,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("stop",stop,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("strand",strand,0,field2String.isEmpty());
		if( encloseWithCurlyBraces){
			return "{"+field2String+"}";
		} else {
			return field2String;
		}
	}
	
//	@Override
//	public void fromJSON(String JSON) throws Exception {
//		//"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]
//	    for (MatchResult matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON); matcher != null; matcher = UtilitiesMethodsShared.pattern_genericJSONObject.exec(JSON)) {
//			String keyIT = matcher.getGroup(1);
//			String valueIT = matcher.getGroup(2);
//			boolean mappingSuccessful = mapKeyUnknowTypeValueToObjectField(keyIT,valueIT);
//			if( ! mappingSuccessful){
//				throw new Exception("The key "+keyIT+" (value = "+valueIT+") has not been mapped to any attribute");
//			}
//	    }
//	}
	
	@Override
	public int getIntPrimaryIdAttribute() {
		return getGeneId();
	}
	
	// static sort

	public static void sortCDSsListByGenomicPosition(
			List<LightGeneItem> listLGISent
			) {
		//Comparator<LightGeneItem> compaByGeneStartComparatorIT = new LightGeneItemComparators.ByStartGeneComparator();
		Collections.sort(listLGISent, LightGeneItemComparators.byStartGeneComparator);
		//PopUpListGenesDialog.refreshGeneList(dataProviderStep3.getList(), false);
	}

	

	public static ArrayList<LightGeneItem> sortCDSsListByPValueOverRepresentationGroupPlus(
			List<LightGeneItem> listLGISent
			) {
		ArrayList<LightGeneItem> sortedAlLGI = null;
		Storage sessionStorage = Storage.getSessionStorageIfSupported();
		if(sessionStorage != null){
			if (SessionStorageControler.PValOverReprPheno_REF_ORGA_ID < 0
					 || SessionStorageControler.PValOverReprPheno_alSelectedPhenoPlusOrgaIds.isEmpty()
					 || SessionStorageControler.PValOverReprPheno_alSelectedPhenoMinusOrgaIds.isEmpty()
					) {
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
//						new Exception("onSortCDSsListByPValueOverRepresentationGroupPlusClick : SessionStorageControler.PValOverReprPheno_REF_ORGA_ID < 0 || SessionStorageControler.PValOverReprPheno_alSelectedPhenoPlusOrgaIds.isEmpty() || SessionStorageControler.PValOverReprPheno_alSelectedPhenoMinusOrgaIds.isEmpty()")
//						);
				sortedAlLGI = new ArrayList<>(listLGISent);
				sortCDSsListByGenomicPosition(sortedAlLGI);
				return sortedAlLGI;
			} else {
				//ok we have this kind of data stored in SessionStorageControler
				String sortedQGeneIdAsString = sessionStorage.getItem(SessionStorageControler.PValOverReprPheno_KEY_SORTED_LIST);
				HashMap<Integer, Integer> geneIds2orderPosition = new HashMap<>();
				String[] sortedQGeneIdAsArray = UtilitiesMethodsShared.getArrayStringFromConcatenatedStringFromMethodGetItemsAsStringFromCollectionDefault(sortedQGeneIdAsString);
				int orderPosition = 0;
				for (String s: sortedQGeneIdAsArray) {
					geneIds2orderPosition.put(Integer.parseInt(s), orderPosition);
					orderPosition++;
				}
				sortedAlLGI = UtilitiesMethodsShared.createNewSortedArrayAccordingToPositionProviderKeyInt(
						listLGISent //dataProviderStep3.getList() // the array to sort
						, geneIds2orderPosition // the position provider with hasPrimaryIdAttribute as key
						, orderPosition
						, true
						, false
						);

			}
		} else {
			Window.alert("Session Storage is not supported in your browser, this functionality is therefore not available."
					+ " Please update your browser to enable this functionality.");
		}
		return sortedAlLGI;
	}

	public static ArrayList<LightGeneItem> sortCDSsListByCountOrthologsGroupPlus(
			List<LightGeneItem> listLGISent
			) {
		ArrayList<LightGeneItem> sortedAlLGI = null;
		Storage sessionStorage = Storage.getSessionStorageIfSupported();
		if(sessionStorage != null){
			if (SessionStorageControler.CountMostRepresPheno_REF_ORGA_ID < 0 || SessionStorageControler.CountMostRepresPheno_alSelectedPhenoPlusOrgaIds.isEmpty()) {
//				Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6, 
//						new Exception("onSortCDSsListByCountOrthologsGroupPlusClick : SessionStorageControler.CountMostRepresPheno_REF_ORGA_ID < 0 || SessionStorageControler.CountMostRepresPheno_alSelectedPhenoPlusOrgaIds.isEmpty()")
//						);
				sortedAlLGI = new ArrayList<>(listLGISent);
				sortCDSsListByGenomicPosition(sortedAlLGI);
				return sortedAlLGI;
			} else {
				//ok we have this kind of data stored in SessionStorageControler
				String sortedQGeneIdAsString = sessionStorage.getItem(SessionStorageControler.CountMostRepresPheno_KEY_SORTED_LIST);
				HashMap<Integer, Integer> geneIds2orderPosition = new HashMap<>();
				String[] sortedQGeneIdAsArray = UtilitiesMethodsShared.getArrayStringFromConcatenatedStringFromMethodGetItemsAsStringFromCollectionDefault(sortedQGeneIdAsString);
				int orderPosition = 0;
				for (String s: sortedQGeneIdAsArray) {
					geneIds2orderPosition.put(Integer.parseInt(s), orderPosition);
					orderPosition++;
				}
				sortedAlLGI = UtilitiesMethodsShared.createNewSortedArrayAccordingToPositionProviderKeyInt(
						listLGISent //dataProviderStep3.getList() // the array to sort
						, geneIds2orderPosition // the position provider with hasPrimaryIdAttribute as key
						, orderPosition
						, true
						, false
						);

			}
		} else {
			Window.alert("Session Storage is not supported in your browser, this functionality is therefore not available."
					+ " Please update your browser to enable this functionality.");
		}
		return sortedAlLGI;
	}


	
	
}

