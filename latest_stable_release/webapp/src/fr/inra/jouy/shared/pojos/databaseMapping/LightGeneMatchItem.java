package fr.inra.jouy.shared.pojos.databaseMapping;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.UtilitiesMethodsShared;
import fr.inra.jouy.shared.interfaceIs.IsStringifyToJSON;

public class LightGeneMatchItem implements IsSerializable, IsStringifyToJSON {
	private double qFirstFrac = -1;
	private double qAlignFrac = -1;
	private double sFirstFrac = -1;
	private double sAlignFrac = -1;
	private double identity = -1;
	private double score = -1;
	private double eValue = -1;
	
	public double getQFirstFrac() {
		return qFirstFrac;
	}

	public void setQFirstFrac(double firstFrac) {
		qFirstFrac = firstFrac;
	}

	public double getQAlignFrac() {
		return qAlignFrac;
	}

	public void setQAlignFrac(double alignFrac) {
		qAlignFrac = alignFrac;
	}

	public double getSFirstFrac() {
		return sFirstFrac;
	}

	public void setSFirstFrac(double firstFrac) {
		sFirstFrac = firstFrac;
	}

	public double getSAlignFrac() {
		return sAlignFrac;
	}

	public void setSAlignFrac(double alignFrac) {
		sAlignFrac = alignFrac;
	}
	

	public double getIdentity() {
		return identity;
	}

	public void setIdentity(double identity) {
		this.identity = identity;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public double getEValue() {
		return eValue;
	}

	public void setEValue(double value) {
		eValue = value;
	}

	@Override
	public String stringifyToJSON(boolean encloseWithCurlyBraces, boolean propagateToParentClassViaSuper)
			throws Exception {

		String field2String = "";
		//no super.toJSON as does not extends another class
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("qFirstFrac",qFirstFrac,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("qAlignFrac",qAlignFrac,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("sFirstFrac",sFirstFrac,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("sAlignFrac",sAlignFrac,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("identity",identity,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("score",score,-1,field2String.isEmpty());
		field2String += UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringComparableNumber("eValue",eValue,-1,field2String.isEmpty());
		if( encloseWithCurlyBraces){
			return "{"+field2String+"}";
		} else {
			return field2String;
		}

	}


}
