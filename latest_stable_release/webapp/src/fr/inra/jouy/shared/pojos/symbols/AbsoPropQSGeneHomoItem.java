package fr.inra.jouy.shared.pojos.symbols;

/*
 Insyght Copyright - INRA
IDDN.FR.001.060011.000.R.C.2013.000.42000
Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

This file is part of Insyght

Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.inra.jouy.shared.TransAbsoPropResuGeneSet;
import fr.inra.jouy.shared.pojos.applicationItems.GeneWidgetStyleItem;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneMatchItem;

public class AbsoPropQSGeneHomoItem extends AbsoPropQSSyntItem implements IsSerializable {

	//change in SComparableGeneInsertionItem too
	//change in LightGeneItem too
	private int qsQGeneId = -1;
	//private String qsQMostSignificantGeneName = null;
	private String qsQName = null;
	private String qsQLocusTag = null;
	private int qsQStrand = 0; // CAN BE 1 OR -1
	private int qsQPbStartGeneInElement = -1;
	private int qsQPbStopGeneInElement = -1;
	
	private int qsSGeneId = -1;
	//private String qsSMostSignificantGeneName = null;
	private String qsSName = null;
	private String qsSLocusTag = null;
	private int qsSStrand = 0; // CAN BE 1 OR -1
	private int qsSPbStartGeneInElement = -1;
	private int qsSPbStopGeneInElement = -1;
	
	private int qsOrigamiAlignmentPairsType = -1;
	private ArrayList<LightGeneMatchItem> listLightGeneMatchItem = new ArrayList<LightGeneMatchItem>();
	private GeneWidgetStyleItem qsStyleItem = null;
	
	private ArrayList<AbsoPropQSGeneHomoItem> tmpListOfOtherQSHomologiesForTheQGene = new ArrayList<AbsoPropQSGeneHomoItem>();//can be AbsoluteProportionQSGeneHomologyItem or AbsoluteProportionQGeneInsertionItem missing target
	private ArrayList<AbsoPropSGeneInserItem> tmpListOfNextAPSGeneInsertion = new ArrayList<AbsoPropSGeneInserItem>();
	private ArrayList<AbsoPropQGeneInserItem> tmpListOfNextAPQGeneInsertion = new ArrayList<AbsoPropQGeneInserItem>();
	private ArrayList<AbsoPropSGeneInserItem> tmpListOfPreviousAPSGeneInsertion = new ArrayList<AbsoPropSGeneInserItem>();
	private ArrayList<AbsoPropQGeneInserItem> tmpListOfPreviousAPQGeneInsertion = new ArrayList<AbsoPropQGeneInserItem>();

	
	public AbsoPropQSGeneHomoItem(){
		
	}
	
	public AbsoPropQSGeneHomoItem(AbsoPropQSGeneHomoItem absoluteProportionQSGeneHomologyItemSent){
		super((AbsoPropQSSyntItem)absoluteProportionQSGeneHomologyItemSent);
		

		setQsQGeneId(absoluteProportionQSGeneHomologyItemSent.getQsQGeneId());
		setQsQLocusTag(absoluteProportionQSGeneHomologyItemSent.getQsQLocusTagAsRawString());
		setQsQName(absoluteProportionQSGeneHomologyItemSent.getQsQNameAsRawString());
		setQsQStrand(absoluteProportionQSGeneHomologyItemSent.getQsQStrand());
		setQsQPbStartGeneInElement(absoluteProportionQSGeneHomologyItemSent.getQsQPbStartGeneInElement());
		setQsQPbStopGeneInElement(absoluteProportionQSGeneHomologyItemSent.getQsQPbStopGeneInElement());
		
		setQsSGeneId(absoluteProportionQSGeneHomologyItemSent.getQsSGeneId());
		setQsSLocusTag(absoluteProportionQSGeneHomologyItemSent.getQsSLocusTagAsRawString());
		setQsSName(absoluteProportionQSGeneHomologyItemSent.getQsSNameAsRawString());
		setQsSStrand(absoluteProportionQSGeneHomologyItemSent.getQsSStrand());
		setQsSPbStartGeneInElement(absoluteProportionQSGeneHomologyItemSent.getQsSPbStartGeneInElement());
		setQsSPbStopGeneInElement(absoluteProportionQSGeneHomologyItemSent.getQsSPbStopGeneInElement());
		
		setQsOrigamiAlignmentPairsType(absoluteProportionQSGeneHomologyItemSent.getQsOrigamiAlignmentPairsType());
		setListLightGeneMatchItem(absoluteProportionQSGeneHomologyItemSent.getListLightGeneMatchItem());
		setQsStyleItem(absoluteProportionQSGeneHomologyItemSent.getQsStyleItem());
	}
	
	public AbsoPropQSGeneHomoItem(TransAbsoPropResuGeneSet transientAbsoluteProportionResultGeneSetSent){
		
		setSyntenyOrigamiAlignmentId(transientAbsoluteProportionResultGeneSetSent.getAlignmentId());
		setQsQGeneId(transientAbsoluteProportionResultGeneSetSent.getqGeneId());
		setQsQPbStartGeneInElement(transientAbsoluteProportionResultGeneSetSent.getqStart());
		setQsQPbStopGeneInElement(transientAbsoluteProportionResultGeneSetSent.getqStop());
		setQsSGeneId(transientAbsoluteProportionResultGeneSetSent.getsGeneId());
		setQsSPbStartGeneInElement(transientAbsoluteProportionResultGeneSetSent.getsStart());
		setQsSPbStopGeneInElement(transientAbsoluteProportionResultGeneSetSent.getsStop());
		setQsOrigamiAlignmentPairsType(transientAbsoluteProportionResultGeneSetSent.getType());
		setQsQOrigamiElementId(transientAbsoluteProportionResultGeneSetSent.getqOrigamiElementId());
		setQsSOrigamiElementId(transientAbsoluteProportionResultGeneSetSent.getsOrigamiElementId());
		
	}
	
	
	
	public int getQsQGeneId() {
		return qsQGeneId;
	}
	public void setQsQGeneId(int qsQGeneId) {
		this.qsQGeneId = qsQGeneId;
	}
	
	public String getQsQMostSignificantGeneNameAsHTMLPlusLink() {
		
		if(qsQName != null && qsQName.length() > 0){
//			if (qsQName.contains("UniProtKB") == true ) {
//	            String locus = qsQName.replaceAll("\\{UniProtKB/.+:.+", "");
//	            String link = qsQName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
//	            link = link.replaceAll("\\}", "");
//	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
//			}else {
//		        return qsQName;
//		    }
			return getQsQNameAsHTMLPlusLink();
		}else if (qsQLocusTag != null && qsQLocusTag.length() > 0){
//			if (qsQLocusTag.contains("UniProtKB") == true ) {
//	            String locus = qsQLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
//	            String link = qsQLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
//	            link = link.replaceAll("\\}", "");
//	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
//			}else {
//	            return qsQLocusTag;
//			}
			return getQsQLocusTagAsHTMLPlusLink();
		}else{
			return "NO_NAME";
		}
			
	}
	
	public String getQsQMostSignificantGeneNameAsStrippedString() {
		
		if(qsQName != null && qsQName.length() > 0){
//			if (qsQName.contains("UniProtKB") == true ) {
//	            String locus = qsQName.replaceAll("\\{UniProtKB/.+:.+", "");
//				return locus;
//			}else {
//				return qsQName;
//	        }
			return getQsQNameAsStrippedString();
		}else if (qsQLocusTag != null && qsQLocusTag.length() > 0){
//			if (qsQLocusTag.contains("UniProtKB") == true ) {
//	            String locus = qsQLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
//	            return locus;
//			}else {
//				return qsQLocusTag;
//	        }
			return getQsQLocusTagAsStrippedString();
		}else{
			return "NO_NAME";
		}
			
	}
	public String getQsQNameAsRawString() {
		return qsQName;
	}
	public String getQsQNameAsStrippedString() {
		if(qsQName != null){
			if (qsQName.contains("UniProtKB") == true ) {
	            String locus = qsQName.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return qsQName;
	        }
		}else{
			return "";
		}
		
	}
	public String getQsQNameAsHTMLPlusLink() {
		if(qsQName != null){
			if (qsQName.contains("UniProtKB") == true ) {
	            String locus = qsQName.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = qsQName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";

	            //  ex: http://138.102.22.61:8080/PortailBioinfoDev/#&accession=AL009126_GR&locustag=BSU00070
	            // "<a href=\"http://138.102.22.61:8080/PortailBioinfoDev/#&accession="+getsAccnum()+"&locustag="+getsLocusTagAsStrippedString()+"\" target=\"_blank\" color=\"orange\">[IGO]</a>"
	            //return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>"
	            		//+ " <a href=\"http://138.102.22.61:8080/PortailBioinfoDev/#&accession="+getQsQAccnum()+"&locustag="+getQsQLocusTagAsStrippedString()+"\" target=\"_blank\" color=\"orange\">[IGO]</a>"
	            		//;
	            
			}else {
		        return qsQName
		        		//+ " <a href=\"http://138.102.22.61:8080/PortailBioinfoDev/#&accession="+getQsQAccnum()+"&locustag="+getQsQLocusTagAsStrippedString()+"\" target=\"_blank\" color=\"orange\">[IGO]</a>"
		        		;
	            
		    }
		}else{
			return "";
		}
		
	}
	
	public void setQsQName(String qsQName) {
		this.qsQName = qsQName;
	}
	public String getQsQLocusTagAsRawString() {
		return qsQLocusTag;
	}
	public String getQsQLocusTagAsStrippedString() {
		if(qsQLocusTag != null){
			if (qsQLocusTag.contains("UniProtKB") == true ) {
	            String locus = qsQLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return qsQLocusTag;
	        }
		}else{
			return "";
		}
		
	}
	public String getQsQLocusTagAsHTMLPlusLink() {
		if(qsQLocusTag != null){
			if (qsQLocusTag.contains("UniProtKB") == true ) {
	            String locus = qsQLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = qsQLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return qsQLocusTag;
		    }
		}else{
			return "";
		}
		
	}
	public void setQsQLocusTag(String qsQLocusTag) {
		this.qsQLocusTag = qsQLocusTag;
	}
	public int getQsQStrand() {
		return qsQStrand;
	}
	public void setQsQStrand(int qsQStrand) {
		this.qsQStrand = qsQStrand;
	}
	public int getQsSGeneId() {
		return qsSGeneId;
	}
	public void setQsSGeneId(int qsSGeneId) {
		this.qsSGeneId = qsSGeneId;
	}
	
	public String getQsSMostSignificantGeneNameAsHTMLPlusLink() {

		if(qsSName != null && qsSName.length() > 0){
//			if (qsSName.contains("UniProtKB") == true ) {
//	            String locus = qsSName.replaceAll("\\{UniProtKB/.+:.+", "");
//	            String link = qsSName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
//	            link = link.replaceAll("\\}", "");
//	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
//			}else {
//		        return qsSName;
//		    }
			return getQsSNameAsHTMLPlusLink();
		}else if (qsSLocusTag != null && qsSLocusTag.length() > 0){
//			if (qsSLocusTag.contains("UniProtKB") == true ) {
//	            String locus = qsSLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
//	            String link = qsSLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
//	            link = link.replaceAll("\\}", "");
//	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
//			}else {
//	            return qsSLocusTag;
//			}
			return getQsSLocusTagAsHTMLPlusLink();
		}else{
			return "NO_NAME";
		}
	}
	
	public String getQsSMostSignificantGeneNameAsStrippedString() {

		if(qsSName != null && qsSName.length() > 0){
//			if (qsSName.contains("UniProtKB") == true ) {
//	            String locus = qsSName.replaceAll("\\{UniProtKB/.+:.+", "");
//				return locus;
//			}else {
//				return qsSName;
//	        }
			return getQsSNameAsStrippedString();
		}else if (qsSLocusTag != null && qsSLocusTag.length() > 0){
//			if (qsSLocusTag.contains("UniProtKB") == true ) {
//	            String locus = qsSLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
//	            return locus;
//			}else {
//				return qsSLocusTag;
//	        }
			return getQsSLocusTagAsStrippedString();
		}else{
			return "NO_NAME";
		}
	}
	
	public String getQsSNameAsRawString() {
		return qsSName;
	}
	public String getQsSNameAsStrippedString() {
		if(qsSName != null){
			if (qsSName.contains("UniProtKB") == true ) {
	            String locus = qsSName.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return qsSName;
	        }
		}else{
			return "";
		}
		
	}
	public String getQsSNameAsHTMLPlusLink() {
		if(qsSName != null){
			if (qsSName.contains("UniProtKB") == true ) {
	            String locus = qsSName.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = qsSName.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return qsSName;
		    }
		}else{
			return "";
		}
		
	}
	public void setQsSName(String qsSName) {
		this.qsSName = qsSName;
	}
	public String getQsSLocusTagAsRawString() {
		return qsSLocusTag;
	}
	public String getQsSLocusTagAsStrippedString() {
		if(qsSLocusTag != null){
			if (qsSLocusTag.contains("UniProtKB") == true ) {
	            String locus = qsSLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
				return locus;
			}else {
				return qsSLocusTag;
	        }
		}else{
			return "";
		}
		
	}
	public String getQsSLocusTagAsHTMLPlusLink() {
		if(qsSLocusTag != null){
			if (qsSLocusTag.contains("UniProtKB") == true ) {
	            String locus = qsSLocusTag.replaceAll("\\{UniProtKB/.+:.+", "");
	            String link = qsSLocusTag.replaceAll(".+\\s\\{UniProtKB/.+:", "");
	            link = link.replaceAll("\\}", "");
	            return "<a href=\"http://www.uniprot.org/uniprot/"+link+"\" target=\"_blank\">"+locus+"</a>";
			}else {
		        return qsSLocusTag;
		    }
		}else{
			return "";
		}
		
	}
	
	public void setQsSLocusTag(String qsSLocusTag) {
		this.qsSLocusTag = qsSLocusTag;
	}
	public int getQsSStrand() {
		return qsSStrand;
	}
	public void setQsSStrand(int qsSStrand) {
		this.qsSStrand = qsSStrand;
	}
	public ArrayList<LightGeneMatchItem> getListLightGeneMatchItem() {
		return listLightGeneMatchItem;
	}
	public void setListLightGeneMatchItem(
			ArrayList<LightGeneMatchItem> listLightGeneMatchItem) {
		this.listLightGeneMatchItem = listLightGeneMatchItem;
	}

	public GeneWidgetStyleItem getQsStyleItem() {
		if(qsStyleItem == null){
			qsStyleItem = new GeneWidgetStyleItem();
		}
		return qsStyleItem;
	}

	public void setQsStyleItem(GeneWidgetStyleItem qSStyleItem) {
		this.qsStyleItem = qSStyleItem;
	}

	
	public ArrayList<AbsoPropQSGeneHomoItem> getTmpListOfOtherQSHomologiesForTheQGene() {
		return tmpListOfOtherQSHomologiesForTheQGene;
	}

	public void setTmpListOfOtherQSHomologiesForTheQGene(
			ArrayList<AbsoPropQSGeneHomoItem> listOfOtherQSHomologiesForTheQGene) {
		this.tmpListOfOtherQSHomologiesForTheQGene = listOfOtherQSHomologiesForTheQGene;
	}

	public int getQsQPbStartGeneInElement() {
		return qsQPbStartGeneInElement;
	}

	public void setQsQPbStartGeneInElement(int qsQPbStartGeneInElement) {
		this.qsQPbStartGeneInElement = qsQPbStartGeneInElement;
	}

	public int getQsQPbStopGeneInElement() {
		return qsQPbStopGeneInElement;
	}

	public void setQsQPbStopGeneInElement(int qsQPbStopGeneInElement) {
		this.qsQPbStopGeneInElement = qsQPbStopGeneInElement;
	}

	public int getQsSPbStartGeneInElement() {
		return qsSPbStartGeneInElement;
	}

	public void setQsSPbStartGeneInElement(int qsSPbStartGeneInElement) {
		this.qsSPbStartGeneInElement = qsSPbStartGeneInElement;
	}

	public int getQsSPbStopGeneInElement() {
		return qsSPbStopGeneInElement;
	}

	public void setQsSPbStopGeneInElement(int qsSPbStopGeneInElement) {
		this.qsSPbStopGeneInElement = qsSPbStopGeneInElement;
	}

	public int getQsOrigamiAlignmentPairsType() {
		return qsOrigamiAlignmentPairsType;
	}

	public void setQsOrigamiAlignmentPairsType(int qsOrigamiAlignmentPairsType) {
		this.qsOrigamiAlignmentPairsType = qsOrigamiAlignmentPairsType;
	}

	public ArrayList<AbsoPropSGeneInserItem> getTmpListOfNextAPSGeneInsertion() {
		return tmpListOfNextAPSGeneInsertion;
	}

	public void setTmpListOfNextAPSGeneInsertion(
			ArrayList<AbsoPropSGeneInserItem> tmpListOfNextAPSGeneInsertion) {
		this.tmpListOfNextAPSGeneInsertion = tmpListOfNextAPSGeneInsertion;
	}

	public ArrayList<AbsoPropQGeneInserItem> getTmpListOfNextAPQGeneInsertion() {
		return tmpListOfNextAPQGeneInsertion;
	}

	public void setTmpListOfNextAPQGeneInsertion(
			ArrayList<AbsoPropQGeneInserItem> tmpListOfNextAPQGeneInsertion) {
		this.tmpListOfNextAPQGeneInsertion = tmpListOfNextAPQGeneInsertion;
	}

	public ArrayList<AbsoPropSGeneInserItem> getTmpListOfPreviousAPSGeneInsertion() {
		return tmpListOfPreviousAPSGeneInsertion;
	}

	public void setTmpListOfPreviousAPSGeneInsertion(
			ArrayList<AbsoPropSGeneInserItem> tmpListOfPreviousAPSGeneInsertion) {
		this.tmpListOfPreviousAPSGeneInsertion = tmpListOfPreviousAPSGeneInsertion;
	}

	public ArrayList<AbsoPropQGeneInserItem> getTmpListOfPreviousAPQGeneInsertion() {
		return tmpListOfPreviousAPQGeneInsertion;
	}

	public void setTmpListOfPreviousAPQGeneInsertion(
			ArrayList<AbsoPropQGeneInserItem> tmpListOfPreviousAPQGeneInsertion) {
		this.tmpListOfPreviousAPQGeneInsertion = tmpListOfPreviousAPQGeneInsertion;
	}
	
	
	
}
