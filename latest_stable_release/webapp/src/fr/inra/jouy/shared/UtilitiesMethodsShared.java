package fr.inra.jouy.shared;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import fr.inra.jouy.client.Insyght;
import fr.inra.jouy.shared.interfaceIs.IsStringifyToJSON;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;
import fr.inra.jouy.shared.pojos.transitComposite.ConvertSetIntegerToSortedRangeItem;
import fr.inra.jouy.shared.pojos.transitComposite.ConvertSetLongToSortedRangeItem;
import fr.inra.jouy.shared.pojos.transitComposite.PairIntegerLowerHigherRange;
import fr.inra.jouy.shared.pojos.transitComposite.PairLongLowerHigherRange;
import fr.inra.jouy.shared.interfaceIs.HasIntPrimaryIdAttribute;


public class UtilitiesMethodsShared {
	
	public static RegExp pattern_max6DigitsAfterPoint = RegExp.compile("(\\d*\\.\\d{6})\\d+(E.+)?");

	
	public static <T extends Number> String formatNumberMax6DigitsAfterPointIfPossible(T numberSent){
    	String formattedValue = "";
		MatchResult matcher = pattern_max6DigitsAfterPoint.exec(numberSent.toString());
    	boolean matchFound = matcher != null; // equivalent to regExp.test(inputStr); 
    	if(matchFound){
    		formattedValue = matcher.getGroup(1);
    		if(matcher.getGroup(2) != null && ! matcher.getGroup(2).isEmpty()){
    			formattedValue += matcher.getGroup(2);
    		}
    	} else {
    		formattedValue = numberSent.toString();
    	}
    	return formattedValue;
	}
	
	public static int maxNumberOfGenomesInGroupPlusMinusCombined = 300; // can cause Java.lang.OutOfMemoryError: Java heap space if more than 190 orga compared and 500 contigs

	 
	//default separatorItems and surroundEachItemWith
	public static <T> String getItemsAsStringFromCollection(Collection<T> collectionIT) {
		String separatorItems = ",";
		String surroundEachItemWith = "";
		return getItemsAsStringFromCollection(collectionIT, separatorItems, surroundEachItemWith);
	}
	
	public static String[] getArrayStringFromConcatenatedStringFromMethodGetItemsAsStringFromCollectionDefault(
			String stringFromGetItemsAsStringFromCollection
			){
		String[] ArrayToReturn = stringFromGetItemsAsStringFromCollection.split(",");
		return ArrayToReturn;
	}
	
	public static <T> String getItemsAsStringFromCollection(Collection<T> collectionIT,
			String separatorItems, String surroundEachItemWith) {
		String stToReturn = "";
		boolean notFirstIter = false;
		for(T itemIT : collectionIT){
			if(notFirstIter){
				stToReturn += separatorItems;
			} else {
				notFirstIter = true;
			}
			stToReturn += surroundEachItemWith+itemIT.toString()+surroundEachItemWith;
		}
		return stToReturn;
	}
	

	public static <T> String get10FirstItemsAsStringFromCollection(Collection<T> collectionIT) {
		String stToReturn = "[";
		int counterIT = 0;
		for(T itemIT : collectionIT){
			if (counterIT > 10) {
				stToReturn += ",... size="+collectionIT.size();
				break;
			}
			if(counterIT > 0){
				stToReturn += ",";
			}
			stToReturn += itemIT.toString();
			counterIT++;

		}
		stToReturn += "]";
		return stToReturn;
	}

	
	//number
	public static <T extends Number> String convertObjectAttribute2JSONString_stringComparableNumber(
			String objectAttribute,
			T value,
			T initValue_skipConvertingAttribute,
			boolean field2StringIsEmpty) {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		HashMap<String, T> hm = new HashMap<>();
		hm.put(objectAttribute, value);
		return convertObjectAttribute2JSONString_entrySetStringComparableNumber(
				hm.entrySet(),
				initValue_skipConvertingAttribute,
				field2StringIsEmpty);
	}
	

	public static <T extends Number> String convertObjectAttribute2JSONString_entrySetStringComparableNumber(
			Set<Entry<String, T>> entrySet,
			T initValue_skipConvertingAttribute,
			boolean field2StringIsEmpty) {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String stToReturn = "";
		for (Map.Entry<String, T> entry : entrySet ) {
			boolean skipConvertingAttribute = false;
			if(initValue_skipConvertingAttribute != null){
				if(initValue_skipConvertingAttribute.toString().compareTo(entry.getValue().toString()) == 0){
					skipConvertingAttribute = true;
				}
			}
			if( ! skipConvertingAttribute){
				if( ! field2StringIsEmpty){
					stToReturn += ",";
				}
				stToReturn += "\""+entry.getKey()+"\":"+entry.getValue();
				field2StringIsEmpty = false;
			}
		}
		return stToReturn;
	}


	public static  <T extends Number & Comparable<T>> String convertObjectAttribute2JSONString_entrySetStringAlComparableNumber(
			Set<Entry<String, ArrayList<T>>> entrySet,
			boolean field2StringIsEmpty) {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String stToReturn = "";
		for (Map.Entry<String, ArrayList<T>> entry : entrySet ) {
			if(entry.getValue() != null){
				//if( ! entry.getValue().isEmpty()){
					if( ! field2StringIsEmpty){
						stToReturn += ",";
					}
					stToReturn += "\""+entry.getKey()+"\":[";
					boolean firstIter = true;
					for(T valueIT : entry.getValue()){
						if(firstIter){
							firstIter = false;
						} else {
							stToReturn += ",";
						}
						if(valueIT == null){
							stToReturn += "null";
						} else {
							stToReturn += valueIT;
						}
					}
					stToReturn += "]";
					field2StringIsEmpty = false;
				//}
			}
		}
		return stToReturn;
	}


	//string
	public static String convertObjectAttribute2JSONString_stringString(
			String objectAttribute,
			String value,
			boolean field2StringIsEmpty) {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		HashMap<String, String> hm = new HashMap<>();
		hm.put(objectAttribute, value);
		return convertObjectAttribute2JSONString_entrySetStringString(
				hm.entrySet(),
				field2StringIsEmpty);
	}


	public static String convertObjectAttribute2JSONString_entrySetStringString(
			Set<Entry<String, String>> entrySet,
			boolean field2StringIsEmpty) {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String stToReturn = "";
		for (Map.Entry<String, String> entry : entrySet ) {
			if(entry.getValue() != null){
				if( ! entry.getValue().isEmpty()){
					if( ! field2StringIsEmpty){
						stToReturn += ",";
					}
					stToReturn += "\""+entry.getKey()+"\":\""+entry.getValue()+"\"";
					field2StringIsEmpty = false;
				}
			}
		}
		return stToReturn;
	}


	public static String convertObjectAttribute2JSONString_entrySetStringAlString(
			Set<Entry<String, ArrayList<String>>> entrySet,
			boolean field2StringIsEmpty) {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String stToReturn = "";
		for (Map.Entry<String, ArrayList<String>> entry : entrySet ) {
			if(entry.getValue() != null){
				//if( ! entry.getValue().isEmpty()){
					if( ! field2StringIsEmpty){
						stToReturn += ",";
					}
					stToReturn += "\""+entry.getKey()+"\":[";
					boolean firstIter = true;
					for(String valueIT : entry.getValue()){
						if(firstIter){
							firstIter = false;
						} else {
							stToReturn += ",";
						}
						if(valueIT == null){
							stToReturn += "null";
						} else {
							stToReturn += "\""+valueIT+"\"";
						}
					}
					stToReturn += "]";
					field2StringIsEmpty = false;
				//}
			}
		}
		return stToReturn;
	}

	// boolean
	public static String convertObjectAttribute2JSONString_stringBoolean(
			String objectAttribute,
			Boolean value,
			boolean field2StringIsEmpty) {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		HashMap<String, Boolean> hm = new HashMap<>();
		hm.put(objectAttribute, value);
		return convertObjectAttribute2JSONString_entrySetStringBoolean(
				hm.entrySet(),
				field2StringIsEmpty);
	}
	
	public static String convertObjectAttribute2JSONString_entrySetStringBoolean(
			Set<Entry<String, Boolean>> entrySet,
			boolean field2StringIsEmpty) {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String stToReturn = "";
		for (Map.Entry<String, Boolean> entry : entrySet ) {
			if(entry.getValue() != null){
				if( ! field2StringIsEmpty){
					stToReturn += ",";
				}
				if(entry.getValue()){
					stToReturn += "\""+entry.getKey()+"\":true";
				} else {
					stToReturn += "\""+entry.getKey()+"\":false";
				}
				field2StringIsEmpty = false;
			}
		}
		return stToReturn;
	}


	public static String convertObjectAttribute2JSONString_entrySetStringAlBoolean(
			Set<Entry<String, ArrayList<Boolean>>> entrySet,
			boolean field2StringIsEmpty) {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String stToReturn = "";
		for (Map.Entry<String, ArrayList<Boolean>> entry : entrySet ) {
			if(entry.getValue() != null){
				//if( ! entry.getValue().isEmpty()){
					if( ! field2StringIsEmpty){
						stToReturn += ",";
					}
					stToReturn += "\""+entry.getKey()+"\":[";
					boolean firstIter = true;
					for(Boolean valueIT : entry.getValue()){
						if(firstIter){
							firstIter = false;
						} else {
							stToReturn += ",";
						}
						if (valueIT == null) {
							stToReturn += "null";
						} else if(valueIT){
							stToReturn += "true";
						} else {
							stToReturn += "false";
						}
					}
					stToReturn += "]";
					field2StringIsEmpty = false;
				//}
			}
		}
		return stToReturn;
	}

	//Object extends IsConvertibleToJSON
	public static <T extends IsStringifyToJSON> String convertObjectAttribute2JSONString_stringIsConvertibleToJSONObject(
			String objectAttribute,
			T objIsConvertibleToJSON,
			boolean field2StringIsEmpty,
			boolean propagateToParentClassViaSuper,
			boolean encloseWithCurlyBraces) throws Exception {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String stToReturn = "";
		if( ! field2StringIsEmpty){
			stToReturn += ",";
		}
		stToReturn += "\""+objectAttribute+"\":";
		if(objIsConvertibleToJSON != null){
			stToReturn += objIsConvertibleToJSON.stringifyToJSON(true, propagateToParentClassViaSuper);
		} else {
			stToReturn += "null";
		}
		if(encloseWithCurlyBraces){
			return "{" + stToReturn + "}";
		} else {
			return stToReturn;
		}
	}
				
				
	public static <T extends IsStringifyToJSON> String convertObjectAttribute2JSONString_stringAlIsConvertibleToJSONObject(
			String objectAttribute,
			ArrayList<T> alIsConvertibleToJSON,
			boolean field2StringIsEmpty,
			boolean propagateToParentClassViaSuper,
			boolean encloseWithCurlyBraces) throws Exception {
		//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
		String stToReturn = "";
//		if( ! listAllLightElementItem.isEmpty()){
		if( ! field2StringIsEmpty){
			stToReturn += ",";
		}
		stToReturn += "\""+objectAttribute+"\":[";
		boolean firstIter = true;
		for(T tIT : alIsConvertibleToJSON){
			if(firstIter){
				firstIter = false;
			} else {
				stToReturn += ",";
			}
			if(tIT != null){
				stToReturn += tIT.stringifyToJSON(true, propagateToParentClassViaSuper);
			} else {
				stToReturn += "null";
			}
		}
		stToReturn += "]";
//		}
		if(encloseWithCurlyBraces){
			return "{" + stToReturn + "}";
		} else {
			return stToReturn;
		}
	}


	public static <T, U> String convertObjectAttribute2JSONString_stringGenericMap(
				String objectAttribute,
				Map<T, U> lhmSent,
				boolean field2StringIsEmpty,
				boolean formatNumberMax6DigitsInFraction,
				boolean encloseWithCurlyBraces) {
			//{"name":"mkyong","age":33,"position":"Developer","salary":7500,"skills":["java","python"]}
			
			StringBuilder field2String = new StringBuilder();
			if( ! field2StringIsEmpty){
				field2String.append(",");
			}
			field2String.append("\""+objectAttribute+"\":{");
			boolean firstIter = true;
			for (Map.Entry<T, U> entry : lhmSent.entrySet()) {
			    T key = entry.getKey();
			    String keyAsString = "";
			    if(key instanceof String){
			    	keyAsString = "\""+key+"\"";
			    } else {
			    	keyAsString = key.toString();
			    }
			    //keyAsString = key.toString();
			    U value = entry.getValue();
			    String valueAsString = "";
			    if(value instanceof String){
			    	valueAsString = "\""+value+"\"";
			    } else if (value instanceof Number
			    		&& formatNumberMax6DigitsInFraction) {
			    	valueAsString = formatNumberMax6DigitsAfterPointIfPossible((Number)value);
			    } else {
			    	valueAsString = value.toString();
			    }
				if(firstIter){
					firstIter = false;
				} else {
					field2String.append(",");
				}
			    field2String.append(keyAsString+":"+valueAsString);
			}
			field2String.append("}");
			if( encloseWithCurlyBraces){
				return "{"+field2String.toString()+"}";
			} else {
				return field2String.toString();
			}
			
		}

	public static <T, U extends HashSet<V>, V extends Comparable<V>> LinkedHashMap<T,ArrayList<V>> sortHashSetValueInMap(
			Map<T, U> mapSent,
			boolean descending) throws Exception {
		LinkedHashMap<T,ArrayList<V>> mapToReturn = new LinkedHashMap<T,ArrayList<V>>();
		Set<Entry<T, U>> entries = mapSent.entrySet();
		Iterator<Entry<T, U>> it = entries.iterator();
		while (it.hasNext()) {
			Map.Entry<T, U> entry = it.next();
			ArrayList<V> list = new ArrayList<V>(entry.getValue());
			Collections.sort(list);
			if (descending) {
				Collections.reverse(list);
			}
			mapToReturn.put(entry.getKey(), list);
		}
		return mapToReturn;
	}


	public static <T extends Comparable<T>, U> LinkedHashMap<T, U> sortMapByKeys(
			Map<T, U> mapSent,
			//T threshold,
			boolean descending) throws Exception {
		LinkedHashMap<T, U> sortedMap = new LinkedHashMap<T, U>();
		TreeSet<T> keys = new TreeSet<T>(mapSent.keySet());
		NavigableSet<T> keysNS;
		if (descending) {
			keysNS = keys.descendingSet();
		} else {
			keysNS = keys;
		}
		for (T key : keysNS) {
		   sortedMap.put(key, mapSent.get(key));
		}
//		ArrayList<T> mapKeys = new ArrayList<T>(mapSent.keySet());
//		Collections.sort(mapKeys);
//		if (descending) {
//			Collections.reverse(mapKeys);
//		}
//		
//		for (int i = 0; i < mapKeys.size(); i++) {
//			T valIT = mapKeys.get(i);
////			if(threshold != null){
////				if(valIT != null && descending && valIT.compareTo(threshold) < 0){
////					break;
////				} else if (valIT != null && ! descending && valIT.compareTo(threshold) > 0){
////					break;
////				}
////			}
//			Set<Entry<T, U>> entries = mapSent.entrySet();
//			Iterator<Entry<T, U>> it = entries.iterator();
//			while (it.hasNext()) {
//				Map.Entry<T, U> entry = it.next();
//				if (valIT.compareTo(entry.getKey()) == 0) {
//					sortedMap.put(entry.getKey(), entry.getValue());
//				}
//			}
//		}
		return sortedMap;
		
	}

	
	// cannot do generic because GWT2.8 reject it for now
	public static LinkedHashMap<Integer, Double> sortMapByDoubleValues(
			Map<Integer, Double> mapSent,
			final boolean descending) {
	    List<Map.Entry<Integer, Double>> list = new LinkedList<>( mapSent.entrySet() );
	    Collections.sort( list, new Comparator<Map.Entry<Integer, Double>>()
	    {
	        @Override
	        public int compare( Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2 )
	        {
	        	if(descending){
	        		return ( o2.getValue() ).compareTo( o1.getValue() );
	        	} else {
	        		return ( o1.getValue() ).compareTo( o2.getValue() );
	        	}
	        }
	    } );
	    LinkedHashMap<Integer, Double> result = new LinkedHashMap<>();
	    for (Map.Entry<Integer, Double> entry : list)
	    {
	        result.put( entry.getKey(), entry.getValue() );
	    }
	    return result;
	}
	
	public static LinkedHashMap<Integer, Integer> sortMapByIntegerValues(
			Map<Integer, Integer> mapSent,
			final boolean descending) {
	    List<Map.Entry<Integer, Integer>> list = new LinkedList<>( mapSent.entrySet() );
	    Collections.sort( list, new Comparator<Map.Entry<Integer, Integer>>()
	    {
	        @Override
	        public int compare( Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2 )
	        {
	        	if(descending){
	        		return ( o2.getValue() ).compareTo( o1.getValue() );
	        	} else {
	        		return ( o1.getValue() ).compareTo( o2.getValue() );
	        	}
	        }
	    } );
	    LinkedHashMap<Integer, Integer> result = new LinkedHashMap<>();
	    for (Map.Entry<Integer, Integer> entry : list)
	    {
	        result.put( entry.getKey(), entry.getValue() );
	    }
	    return result;
	}
	


	public static <T extends HasIntPrimaryIdAttribute> ArrayList<T> createNewSortedArrayAccordingToPositionProviderKeyInt(
			List<T> alItemsToSort
			, HashMap<Integer, Integer> positionProvider
			, int maxPositionInPositionProvider
			, boolean checkEveryItemInListSentIsAccountedFor
			, boolean checkSortedListHasSameSizeAsPositionProvider
			) {
				
		//rq : using new ArrayList<>(alItemsToSort.size); does not work, create empty arryList ??
		//ArrayList<T> alItemsToReturn = new ArrayList<>(maxPositionInPositionProvider);
		ArrayList<T> alItemsToReturn = new ArrayList<>(Collections.nCopies(maxPositionInPositionProvider, null));
		if(positionProvider != null && alItemsToSort != null){
			for(T lgiIT : alItemsToSort){
				if (positionProvider.get(lgiIT.getIntPrimaryIdAttribute()) != null){
					alItemsToReturn.set(
							positionProvider.get(lgiIT.getIntPrimaryIdAttribute()),
							lgiIT);
				} else if (checkEveryItemInListSentIsAccountedFor){
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
							new Exception("failed checkEveryItemInListSentIsAccountedFor : "
									+ "getIntPrimaryIdAttribute = "
									+ lgiIT.getIntPrimaryIdAttribute()
									+ " ; getClass().getCanonicalName = "
									+ lgiIT.getClass().getCanonicalName()
									));
				}
			}
			alItemsToReturn.removeAll(Arrays.asList(null,""));
			alItemsToReturn.trimToSize();
			if(checkSortedListHasSameSizeAsPositionProvider){
				if(
//						positionProvider.size() != alItemsToSort.size()
//						|| 
						alItemsToReturn.size() != alItemsToSort.size()
						){
					Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
							new Exception("failed checkEveryItemInArrayIsAccountedFor : "
									+ "positionProvider.size() = "
									+ positionProvider.size()
									+ " ; alItemsToSort.size() = "
									+ alItemsToSort.size()
									+ " ; alItemsToReturn.size() = "
									+ alItemsToReturn.size()
									));

				}
			}
			
		} else {
			Insyght.UNCAUGHT_ERROR_HANDLER.addAnErrorAndShowForXSeconds(6,
					new Exception("failed positionProvider != null && alLightGeneItem != null"));
		}
		return alItemsToReturn;
	}



	public static String addThousandSeparatorToNumbersInString(String stringIn) {
		if(stringIn != null){
			if(!stringIn.isEmpty()){
				StringBuffer sb = new StringBuffer(stringIn);
				int countInt = 0;
				for(int i=sb.length()-1;i>=0;i--){
					if(sb.charAt(i) == '0'
							|| sb.charAt(i) == '1'
							|| sb.charAt(i) == '2'
							|| sb.charAt(i) == '3'
							|| sb.charAt(i) == '4'
							|| sb.charAt(i) == '5'
							|| sb.charAt(i) == '6'
							|| sb.charAt(i) == '7'
							|| sb.charAt(i) == '8'
							|| sb.charAt(i) == '9'){
						countInt++;
						if(countInt >= 4){
							sb.insert(i+1, ",");
							countInt = 1;
						}
					}else{
						countInt = 0;
					}
				}
				return sb.toString();
			}
		}
		return stringIn;
	}


	public static void addToArrayListNoDups(ArrayList<Integer> toAddTo,
			ArrayList<Integer> iterateOver) {
		for (Integer num : iterateOver) {
			if (toAddTo.indexOf(num) == -1) {
				toAddTo.add(num);
			}
		}
	}


	public static void setLOIPositionInResultListAccordingToArrayIndex(
			ArrayList<LightOrganismItem> alLOIToSetPositionInResultList
			) {
		for (int i=0;i<alLOIToSetPositionInResultList.size();i++) {
			LightOrganismItem LOIIT = alLOIToSetPositionInResultList.get(i);
			LOIIT.setPositionInResultList(i+1);
		}
	}


	//return obj ConvertSetIntegerToSortedRangeItem
	public static ConvertSetIntegerToSortedRangeItem convertSetIntegerToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
			Set<Integer> setIntegerIT
			, boolean allowGapWithinRange
			) {

		ConvertSetIntegerToSortedRangeItem csitsriIT = new ConvertSetIntegerToSortedRangeItem();
		
		int allowedGapWithinRange = 1;
		if (allowGapWithinRange) {
			allowedGapWithinRange = 10;//allow up to x time rows returned by db that will not be part of the IN clause
		}
		
		//WAS
//		ArrayList<ArrayList<Integer>> alToReturn = new ArrayList<>();
//		ArrayList<Integer> positiveSingletonAsIndividual = new ArrayList<>();
//		ArrayList<Integer> positiveRangeAsPair = new ArrayList<>();
//		ArrayList<Integer> negativeSingletonAsIndividual = new ArrayList<>();
//		ArrayList<Integer> negativeRangeAsPair = new ArrayList<>();
		
		if (setIntegerIT == null || setIntegerIT.isEmpty()) {
			// do nothing
		} else if (setIntegerIT.size() == 1) {
			Iterator<Integer> setIterator = setIntegerIT.iterator();
	        while(setIterator.hasNext()){
	        	Integer IntegerIT = setIterator.next();
	        	if (IntegerIT < 0) {
	        		csitsriIT.getNegativeSingletonAsIndividual().add(-IntegerIT);
	        	} else {
	        		csitsriIT.getPositiveSingletonAsIndividual().add(IntegerIT);
	        	}
	        }
		} else {
			ArrayList<Integer> sortedAl = new ArrayList<>(setIntegerIT);
			Collections.sort(sortedAl);
			boolean flagRange = false; // Singleton or Range ; init as Singleton
			boolean flagNegative = false;// Positive or Negative
			Integer lowerRangeIT = sortedAl.get(0);
			if (lowerRangeIT < 0) {
				flagNegative = true;
			}
			Integer previousHigherRangeIT;
			Integer higherRangeIT = null;
						
			for (int i = 1 ; i < sortedAl.size() ; i++) {
				if (higherRangeIT == null) {
					previousHigherRangeIT = lowerRangeIT;
				} else {
					previousHigherRangeIT = higherRangeIT;
				}
				higherRangeIT = sortedAl.get(i);
				
				
				if (lowerRangeIT < 0 && higherRangeIT >= 0
						) {
					//change from flag negative to positive
					//not contiguous
					if (flagRange) {
						if (flagNegative) {
							//negativeRangeAsPair.add(-previousHigherRangeIT);
							//negativeRangeAsPair.add(-lowerRangeIT);
							PairIntegerLowerHigherRange pllhr = new PairIntegerLowerHigherRange();
							pllhr.setLowerRange(-previousHigherRangeIT);
							pllhr.setHigherRange(-lowerRangeIT);
							csitsriIT.getNegativeRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedNegativeIntegerWithinRangesAsIndividual().add(-previousHigherRangeIT);
						} else {
							//positiveRangeAsPair.add(lowerRangeIT);
							//positiveRangeAsPair.add(previousHigherRangeIT);
							PairIntegerLowerHigherRange pllhr = new PairIntegerLowerHigherRange();
							pllhr.setLowerRange(lowerRangeIT);
							pllhr.setHigherRange(previousHigherRangeIT);
							csitsriIT.getPositiveRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedNegativeIntegerWithinRangesAsIndividual().add(previousHigherRangeIT);
						}
					} else {
						if (flagNegative) {
							//negativeSingletonAsIndividual.add(-lowerRangeIT);
							csitsriIT.getNegativeSingletonAsIndividual().add(-lowerRangeIT);
						} else {
							//positiveSingletonAsIndividual.add(lowerRangeIT);
							csitsriIT.getPositiveSingletonAsIndividual().add(lowerRangeIT);
						}
					}
					if (i == sortedAl.size() - 1) {
						//last iteration of array
						//positiveSingletonAsIndividual.add(higherRangeIT);
						csitsriIT.getPositiveSingletonAsIndividual().add(higherRangeIT);
					} else {
						lowerRangeIT = higherRangeIT;
						higherRangeIT = null;
						flagRange = false;
						flagNegative = false;
					}
				} else if (higherRangeIT <= previousHigherRangeIT + allowedGapWithinRange) {
					//contiguous
					if (i == sortedAl.size() - 1) {
						//last iteration of array
						
						if (flagNegative) {
							//negativeRangeAsPair.add(-higherRangeIT);
							//negativeRangeAsPair.add(-lowerRangeIT);
							PairIntegerLowerHigherRange pllhr = new PairIntegerLowerHigherRange();
							pllhr.setLowerRange(-higherRangeIT);
							pllhr.setHigherRange(-lowerRangeIT);
							csitsriIT.getNegativeRangeAsPair().add(pllhr);
							csitsriIT.getCumulatedNegativeIntegerWithinRangesAsIndividual().add(-higherRangeIT);
						} else {
							//positiveRangeAsPair.add(lowerRangeIT);
							//positiveRangeAsPair.add(higherRangeIT);
							PairIntegerLowerHigherRange pllhr = new PairIntegerLowerHigherRange();
							pllhr.setLowerRange(lowerRangeIT);
							pllhr.setHigherRange(higherRangeIT);
							csitsriIT.getPositiveRangeAsPair().add(pllhr);
							csitsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual().add(higherRangeIT);
						}
					} else {
						if (flagRange) {
							
							if (flagNegative) {
								csitsriIT.getCumulatedNegativeIntegerWithinRangesAsIndividual().add(-higherRangeIT);
							} else {
								csitsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual().add(higherRangeIT);
							}
						} else {

							flagRange = true;
							if (flagNegative) {
								csitsriIT.getCumulatedNegativeIntegerWithinRangesAsIndividual().add(-lowerRangeIT);
								csitsriIT.getCumulatedNegativeIntegerWithinRangesAsIndividual().add(-higherRangeIT);
							} else {
								csitsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual().add(lowerRangeIT);
								csitsriIT.getCumulatedPositiveIntegerWithinRangesAsIndividual().add(higherRangeIT);
							}
						}
					}
				} else {
					//not contiguous
					
					if (flagRange) {
						if (flagNegative) {
							//negativeRangeAsPair.add(-previousHigherRangeIT);
							//negativeRangeAsPair.add(-lowerRangeIT);
							PairIntegerLowerHigherRange pllhr = new PairIntegerLowerHigherRange();
							pllhr.setLowerRange(-previousHigherRangeIT);
							pllhr.setHigherRange(-lowerRangeIT);
							csitsriIT.getNegativeRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedNegativeIntegerWithinRangesAsIndividual().add(-previousHigherRangeIT);
						} else {
							//positiveRangeAsPair.add(lowerRangeIT);
							//positiveRangeAsPair.add(previousHigherRangeIT);
							PairIntegerLowerHigherRange pllhr = new PairIntegerLowerHigherRange();
							pllhr.setLowerRange(lowerRangeIT);
							pllhr.setHigherRange(previousHigherRangeIT);
							csitsriIT.getPositiveRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedNegativeIntegerWithinRangesAsIndividual().add(previousHigherRangeIT);
						}
					} else {
						if (flagNegative) {
							//negativeSingletonAsIndividual.add(-lowerRangeIT);
							csitsriIT.getNegativeSingletonAsIndividual().add(-lowerRangeIT);
						} else {
							//positiveSingletonAsIndividual.add(lowerRangeIT);
							csitsriIT.getPositiveSingletonAsIndividual().add(lowerRangeIT);
						}
					}
					if (i == sortedAl.size() - 1) {
						//last iteration of array
						if (flagNegative) {
							//negativeSingletonAsIndividual.add(-higherRangeIT);
							csitsriIT.getNegativeSingletonAsIndividual().add(-higherRangeIT);
						} else {
							//positiveSingletonAsIndividual.add(higherRangeIT);
							csitsriIT.getPositiveSingletonAsIndividual().add(higherRangeIT);
						}
					} else {
						lowerRangeIT = higherRangeIT;
						higherRangeIT = null;
						flagRange = false;
					}
					
				}
			}
		}
//		alToReturn.add(positiveSingletonAsIndividual);
//		alToReturn.add(positiveRangeAsPair);
//		alToReturn.add(negativeSingletonAsIndividual);
//		alToReturn.add(negativeRangeAsPair);
//		return alToReturn;
		return csitsriIT;
		
	}
	
	
	//return obj ConvertSetLongToSortedRangeItem
	public static ConvertSetLongToSortedRangeItem convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(
			Set<Long> setLongIT
			, boolean allowGapWithinRange
			) {

		ConvertSetLongToSortedRangeItem csltsriIT = new ConvertSetLongToSortedRangeItem();
		
		int allowedGapWithinRange = 1;
		if (allowGapWithinRange) {
			allowedGapWithinRange = 10;//allow up to x time rows returned by db that will not be part of the IN clause
		}
		
		//WAS
//		ArrayList<ArrayList<Long>> alToReturn = new ArrayList<>();
//		ArrayList<Long> positiveSingletonAsIndividual = new ArrayList<>();
//		ArrayList<Long> positiveRangeAsPair = new ArrayList<>();
//		ArrayList<Long> negativeSingletonAsIndividual = new ArrayList<>();
//		ArrayList<Long> negativeRangeAsPair = new ArrayList<>();
		
		if (setLongIT == null || setLongIT.isEmpty()) {
			// do nothing
		} else if (setLongIT.size() == 1) {
			Iterator<Long> setIterator = setLongIT.iterator();
	        while(setIterator.hasNext()){
	        	Long longIT = setIterator.next();
	        	if (longIT < 0) {
	        		csltsriIT.getNegativeSingletonAsIndividual().add(-longIT);
	        	} else {
	        		csltsriIT.getPositiveSingletonAsIndividual().add(longIT);
	        	}
	        }
		} else {
			ArrayList<Long> sortedAl = new ArrayList<>(setLongIT);
			Collections.sort(sortedAl);
			boolean flagRange = false; // Singleton or Range ; init as Singleton
			boolean flagNegative = false;// Positive or Negative
			Long lowerRangeIT = sortedAl.get(0);
			if (lowerRangeIT < 0) {
				flagNegative = true;
			}
			Long previousHigherRangeIT;
			Long higherRangeIT = null;
						
			for (int i = 1 ; i < sortedAl.size() ; i++) {
				if (higherRangeIT == null) {
					previousHigherRangeIT = lowerRangeIT;
				} else {
					previousHigherRangeIT = higherRangeIT;
				}
				higherRangeIT = sortedAl.get(i);
				
				
				if (lowerRangeIT < 0 && higherRangeIT >= 0
						) {
					//change from flag negative to positive
					//not contiguous
					if (flagRange) {
						if (flagNegative) {
							//negativeRangeAsPair.add(-previousHigherRangeIT);
							//negativeRangeAsPair.add(-lowerRangeIT);
							PairLongLowerHigherRange pllhr = new PairLongLowerHigherRange();
							pllhr.setLowerRange(-previousHigherRangeIT);
							pllhr.setHigherRange(-lowerRangeIT);
							csltsriIT.getNegativeRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(-previousHigherRangeIT);
						} else {
							//positiveRangeAsPair.add(lowerRangeIT);
							//positiveRangeAsPair.add(previousHigherRangeIT);
							PairLongLowerHigherRange pllhr = new PairLongLowerHigherRange();
							pllhr.setLowerRange(lowerRangeIT);
							pllhr.setHigherRange(previousHigherRangeIT);
							csltsriIT.getPositiveRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(previousHigherRangeIT);
						}
					} else {
						if (flagNegative) {
							//negativeSingletonAsIndividual.add(-lowerRangeIT);
							csltsriIT.getNegativeSingletonAsIndividual().add(-lowerRangeIT);
						} else {
							//positiveSingletonAsIndividual.add(lowerRangeIT);
							csltsriIT.getPositiveSingletonAsIndividual().add(lowerRangeIT);
						}
					}
					if (i == sortedAl.size() - 1) {
						//last iteration of array
						//positiveSingletonAsIndividual.add(higherRangeIT);
						csltsriIT.getPositiveSingletonAsIndividual().add(higherRangeIT);
					} else {
						lowerRangeIT = higherRangeIT;
						higherRangeIT = null;
						flagRange = false;
						flagNegative = false;
					}
				} else if (higherRangeIT <= previousHigherRangeIT + allowedGapWithinRange) {
					//contiguous
					if (flagRange) {
						if (flagNegative) {
							csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(-higherRangeIT);
						} else {
							csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual().add(higherRangeIT);
						}
					} else {
						flagRange = true;
						if (flagNegative) {
							csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(-lowerRangeIT);
							csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(-higherRangeIT);
						} else {
							csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual().add(lowerRangeIT);
							csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual().add(higherRangeIT);
						}
					}
					if (i == sortedAl.size() - 1) {
						//last iteration of array
						if (flagNegative) {
							//negativeRangeAsPair.add(-higherRangeIT);
							//negativeRangeAsPair.add(-lowerRangeIT);
							PairLongLowerHigherRange pllhr = new PairLongLowerHigherRange();
							pllhr.setLowerRange(-higherRangeIT);
							pllhr.setHigherRange(-lowerRangeIT);
							csltsriIT.getNegativeRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(-higherRangeIT);
						} else {
							//positiveRangeAsPair.add(lowerRangeIT);
							//positiveRangeAsPair.add(higherRangeIT);
							PairLongLowerHigherRange pllhr = new PairLongLowerHigherRange();
							pllhr.setLowerRange(lowerRangeIT);
							pllhr.setHigherRange(higherRangeIT);
							csltsriIT.getPositiveRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual().add(higherRangeIT);
						}
					}
//					else {
//						if (flagRange) {
//							
//							if (flagNegative) {
//								csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(-higherRangeIT);
//							} else {
//								csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual().add(higherRangeIT);
//							}
//						} else {
//
//							flagRange = true;
//							if (flagNegative) {
//								csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(-lowerRangeIT);
//								csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(-higherRangeIT);
//							} else {
//								csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual().add(lowerRangeIT);
//								csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual().add(higherRangeIT);
//							}
//						}
//					}
				} else {
					//not contiguous
					
					if (flagRange) {
						if (flagNegative) {
							//negativeRangeAsPair.add(-previousHigherRangeIT);
							//negativeRangeAsPair.add(-lowerRangeIT);
							PairLongLowerHigherRange pllhr = new PairLongLowerHigherRange();
							pllhr.setLowerRange(-previousHigherRangeIT);
							pllhr.setHigherRange(-lowerRangeIT);
							csltsriIT.getNegativeRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(-previousHigherRangeIT);
						} else {
							//positiveRangeAsPair.add(lowerRangeIT);
							//positiveRangeAsPair.add(previousHigherRangeIT);
							PairLongLowerHigherRange pllhr = new PairLongLowerHigherRange();
							pllhr.setLowerRange(lowerRangeIT);
							pllhr.setHigherRange(previousHigherRangeIT);
							csltsriIT.getPositiveRangeAsPair().add(pllhr);
							//csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual().add(previousHigherRangeIT);
						}
					} else {
						if (flagNegative) {
							//negativeSingletonAsIndividual.add(-lowerRangeIT);
							csltsriIT.getNegativeSingletonAsIndividual().add(-lowerRangeIT);
						} else {
							//positiveSingletonAsIndividual.add(lowerRangeIT);
							csltsriIT.getPositiveSingletonAsIndividual().add(lowerRangeIT);
						}
					}
					if (i == sortedAl.size() - 1) {
						//last iteration of array
						if (flagNegative) {
							//negativeSingletonAsIndividual.add(-higherRangeIT);
							csltsriIT.getNegativeSingletonAsIndividual().add(-higherRangeIT);
						} else {
							//positiveSingletonAsIndividual.add(higherRangeIT);
							csltsriIT.getPositiveSingletonAsIndividual().add(higherRangeIT);
						}
					} else {
						lowerRangeIT = higherRangeIT;
						higherRangeIT = null;
						flagRange = false;
					}
					
				}
			}
		}
//		alToReturn.add(positiveSingletonAsIndividual);
//		alToReturn.add(positiveRangeAsPair);
//		alToReturn.add(negativeSingletonAsIndividual);
//		alToReturn.add(negativeRangeAsPair);
//		return alToReturn;
		return csltsriIT;
		
	}

//
//	public static HashSet<Long> isQueryingByRangeWorthIt_withAlLong_singleOrganismArray(ArrayList<Long> alSent) throws Exception {
//		HashSet<Long> hsToReturn = null;
//		if (alSent.size() > 750) {// 750 is an arbitrary number, when a lot of arguments are passed to a query, it is very slow to get them into postresql
//			Long lowerBound = alSent.get(0);
//			Long upperBound = alSent.get(alSent.size()-1);
//			Long diffIT = upperBound-lowerBound;
//			if ( diffIT / alSent.size() < 10 ) { // 10 is an arbitrary number, if there is 30 times or more data returned, not worth it to do range query ?
////				System.err.println("YES isQueryingByRangeWorthIt : "
////						+ " ; alSent.size()="+alSent.size()
////						+ " ; lowerBound="+lowerBound
////						+ " ;upperBound="+upperBound
////						+ " ;diffIT="+diffIT
////						);
//				hsToReturn = new HashSet<>(alSent);
//			}
////			else {
////				System.err.println("NOT isQueryingByRangeWorthIt : "
////						+ " ; alSent.size()="+alSent.size()
////						+ " ; lowerBound="+lowerBound
////						+ " ;upperBound="+upperBound
////						+ " ;diffIT="+diffIT
////						);
////			}
//		}
//		return hsToReturn;
//	}
//
//	public static HashSet<Integer> isQueryingByRangeWorthIt_withAlInt_singleOrganismArray(ArrayList<Integer> alSent) throws Exception {
//		HashSet<Integer> hsToReturn = null;
//		if (alSent.size() > 750) {// 750 is an arbitrary number, when a lot of arguments are passed to a query, it is very slow to get them into postresql
//			Integer lowerBound = alSent.get(0);
//			Integer upperBound = alSent.get(alSent.size()-1);
//			Integer diffIT = upperBound-lowerBound;
//			if ( diffIT / alSent.size() < 10 ) { // 10 is an arbitrary number, if there is 30 times or more data returned, not worth it to do range query ?
////				System.err.println("YES isQueryingByRangeWorthIt : "
////						+ " ; alSent.size()="+alSent.size()
////						+ " ; lowerBound="+lowerBound
////						+ " ;upperBound="+upperBound
////						+ " ;diffIT="+diffIT
////						);
//				hsToReturn = new HashSet<>(alSent);
//			}
////			else {
////				System.err.println("NOT isQueryingByRangeWorthIt : "
////						+ " ; alSent.size()="+alSent.size()
////						+ " ; lowerBound="+lowerBound
////						+ " ;upperBound="+upperBound
////						+ " ;diffIT="+diffIT
////						);
////			}
//		}
//		return hsToReturn;
//	}
//	
//	

	
}
