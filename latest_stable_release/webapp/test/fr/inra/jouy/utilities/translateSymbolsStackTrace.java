package fr.inra.jouy.utilities;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import java.io.BufferedInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;

public class translateSymbolsStackTrace {

	static String pathToSymbolFile = "";
	static Pattern patternStackTrace = Pattern.compile("\\s*at Unknown\\.(.+)\\(.+");
	 
	
	// example of arguments : -symbolFile C:\Users\tlacroix\Documents\Projects\Insyght\war\symbolMap\bacteria\rev326\01A6951EDA7A6FCADDDDC19CA1E406F9\01A6951EDA7A6FCADDDDC19CA1E406F9.symbolMap
	// paste in Run as... -> Run configuration -> arguments...
	public static void main(String[] args) throws Exception {
		System.out.println("Starting translateSymbolsStackTrace at "+LocalDateTime.now());
		
		//get the arguments
		for(int i=0;i<args.length;i++)   {
	        if (args[i].equals("-symbolFile")){
	        	if( (i+1) < args.length){
	        		pathToSymbolFile = args[i+1];
	        		System.out.println("\t with argument -symbolFile "+pathToSymbolFile);
	        		i++;
	        	} else {
	        		throw new Exception("Could not recuperate symbolFile in arguments : args.length = "+args.length
	        				+ " ; i+1 = "+(i+1) );
	        	}
	        }
		}

		// check all mandatory arguments are set
        if(pathToSymbolFile.isEmpty()){
        	throw new Exception("Could not recuperate symbolFile in arguments : no -symbolFile argument have been detected");
        }

		Scanner stdin = null;
		
        try {
        	
        	//check file readable
        	File symbolFile = new File(pathToSymbolFile);
        	if(symbolFile.exists() && symbolFile.canRead()) { 
        	    //ok
        		
        	} else {
            	throw new Exception("File "+pathToSymbolFile+" does not exists or is not readable");
        	}
        	
        	System.out.println("\n\nPlease paste below your stack trace:"
        			+ "\nRemark: multilines accepted, type in \"EOT\" to signal end of stack trace and proceed with the translation.");
        	
			stdin = new Scanner(new BufferedInputStream(System.in));
			ArrayList<String> originalStackTrace = new ArrayList<String>();
			while (stdin.hasNext()) {
				String inLine = stdin.nextLine();
				if(inLine.compareTo("EOT") == 0){
					break;
				}
				originalStackTrace.add(inLine);
			}
			
			System.out.println("\nStarting analysing stack trace, please wait...");
			StringBuilder translatedStackTrace = new StringBuilder();

			for(String originalLineStackTrace : originalStackTrace){
				String translationLine = translateOriginalStringWithSymbolFile(originalLineStackTrace, symbolFile);
				translatedStackTrace.append(translationLine+"\n");
			}
			
			
			System.out.println("\nFinished analysing stack trace."
					+ "\nHere is the translation :\n\n"+translatedStackTrace.toString());
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(stdin != null){
				stdin.close();
			}
		}
        
		System.out.println("\n\nFinished translateSymbolsStackTrace at "+LocalDateTime.now());
		
	}

	private static String translateOriginalStringWithSymbolFile(
			String originalLineStackTrace, File fileSent) throws IOException {
		
		if(originalLineStackTrace.startsWith("Uncaught exception:")){
			return originalLineStackTrace;
		} else {
			Matcher matcher = patternStackTrace.matcher(originalLineStackTrace);
            if (matcher.find()) {
            	String symbolIT = matcher.group(1);
    	        LineIterator it = FileUtils.lineIterator(fileSent, "UTF-8");
    	        try{
    	            while (it.hasNext()){
    	                String line = it.nextLine();
    	                if(line.startsWith(symbolIT+",")){
    	                	return " - "+line;
    	                }
    	            }
    	            return "COULD NOT FIND SYMBOL :  = "+symbolIT
    	            		+"\n\tfrom original line stack trace "+originalLineStackTrace
    	            		+ "\n\tin symbol file " + fileSent.getAbsolutePath();
    	         } finally {LineIterator.closeQuietly(it);}
            } else {
            	return "COULD NOT PARSE : "+originalLineStackTrace;
            }
		}
		
        //return "Error translateOriginalStringWithSymbolFile : originalLineStackTrace = "+originalLineStackTrace;
	}

}
