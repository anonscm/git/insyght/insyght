package fr.inra.jouy.utilities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// this functionality has been discontinued ; see the functionality on phylogenomic profiling 
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
@Deprecated
public class CombinationTaxoNode2StatOverRepresentation  {

	
	//exemple windows arguments -rootTaxoId 1386 -outputFile -symbolFile C:\Users\tlacroix\Documents\Projects\Insyght\CombinationTaxoNode2StatOverRepresentation.out
	public static void main(String[] args) throws Exception {
		
		int rootTaxoId = -1;
		String outputFile = "";
		
		System.out.println("Starting CombinationTaxoNode2StatOverRepresentation at "+LocalDateTime.now());
		
		//get the arguments
		for(int i=0;i<args.length;i++)   {
	        if (args[i].equals("-rootTaxoId")){
	        	if( (i+1) < args.length){
	        		String stRootTaxoId = args[i+1];
	        		rootTaxoId = Integer.parseInt(stRootTaxoId);
	        		System.out.println("\t with argument -rootTaxoId "+rootTaxoId);
	        		i++;
	        	} else {
	        		throw new Exception("Could not recuperate rootTaxoId in arguments : args.length = "+args.length
	        				+ " ; i+1 = "+(i+1) );
	        	}
	        } else if (args[i].equals("-outputFile")){
	        	if( (i+1) < args.length){
	        		outputFile = args[i+1];
	        		System.out.println("\t with argument -outputFile "+outputFile);
	        		i++;
	        	} else {
	        		throw new Exception("Could not recuperate outputFile in arguments : args.length = "+args.length
	        				+ " ; i+1 = "+(i+1) );
	        	}
	        }
		}
		// check validity arguments
		if(rootTaxoId < 0){
			throw new Exception("Could not recuperate rootTaxoId in arguments : -1");
		}
		if(outputFile.isEmpty()){
			throw new Exception("Could not recuperate outputFile in arguments : empty");
		} else {
			//TODO
//        	//check file readable
//        	File symbolFile = new File(outputFile);
//        	if(symbolFile.exists() && symbolFile.canRead()) { 
//        	    //ok
//        	} else {
//            	throw new Exception("File "+outputFile+" does not exists or is not readable");
//        	}
		}

		ArrayList<Integer> alAllOrgaIdsPop = getAlOrgaIdsWithTaxonId(rootTaxoId);
    	Collections.sort(alAllOrgaIdsPop);
    	Integer[] arrAllOrgaIdsPop = new Integer[alAllOrgaIdsPop.size()];
    	for(int i=0 ; i<alAllOrgaIdsPop.size() ; i++){
    		arrAllOrgaIdsPop[i] = alAllOrgaIdsPop.get(i);
    	}
   
		//System.err.println("initial array : "+Arrays.toString(arrAllOrgaIdsPop));
    	
		// get all combinations
		int minObjInCombination = 5;
		
		System.out.println("\tStarting computing combinations for the group + at "+LocalDateTime.now());

		ArrayList<Integer[]> combinationResultGroupPlus = new ArrayList<Integer[]>();
		for(int i=minObjInCombination ; i<(arrAllOrgaIdsPop.length-minObjInCombination) ; i++){
			//get all the combination for the group + array
			combinationsWithinArrayNObjectsTakenRAtATime(
					arrAllOrgaIdsPop, i, 0, new Integer[i], combinationResultGroupPlus);
		}
		System.out.println(
				"\tDone computing combinations for the group + : "
						+ combinationResultGroupPlus.size()
						+ " combinations found");
		
		System.out.println("\tFor each group + combination, computing the combinations for the group - at "+LocalDateTime.now());
		int sizeCombinationsGroupMinus = 0;
		HashMap<Integer[],ArrayList<Integer[]>> hmGroupPlus2GroupMinus = new HashMap<>();
		for(int j=0 ; j < combinationResultGroupPlus.size() ; j++){
			
			Integer[] combinationGroupPlusIT = combinationResultGroupPlus.get(j);
			//System.err.println("combinationGroupPlusIT : "+Arrays.toString(combinationGroupPlusIT));

			List<Integer> alCombinationGroupPlus = Arrays.asList(combinationGroupPlusIT);
			ArrayList<Integer> alCombinationGroupMinus = new ArrayList<>();
			for(Integer orgaIdIT : alAllOrgaIdsPop){
				if( ! alCombinationGroupPlus.contains(orgaIdIT)){
					alCombinationGroupMinus.add(orgaIdIT);
				}
			}
			
			Integer[] arrCombinationGroupMinus = new Integer[alCombinationGroupMinus.size()];
			for(int i=0;i<alCombinationGroupMinus.size();i++) {
				arrCombinationGroupMinus[i] = alCombinationGroupMinus.get(i);
			}
			//get all the combination for the group + array
			ArrayList<Integer[]> combinationResultGroupMinus = new ArrayList<Integer[]>();
			for(int i=minObjInCombination ; i<(arrCombinationGroupMinus.length+1) ; i++){
				combinationsWithinArrayNObjectsTakenRAtATime(
						arrCombinationGroupMinus, i, 0, new Integer[i], combinationResultGroupMinus);
			}
			sizeCombinationsGroupMinus += combinationResultGroupMinus.size();
			if(hmGroupPlus2GroupMinus.containsKey(combinationGroupPlusIT)){
				throw new Exception("hmGroupPlus2GroupMinus already contains key "+combinationGroupPlusIT);
			} else {
				hmGroupPlus2GroupMinus.put(combinationGroupPlusIT, combinationResultGroupMinus);
			}
			
//			//TEST
//			for(int i=0;i<combinationResultGroupMinus.size();i++){
//				System.err.println("\t combinationGroupMinusIT : "+Arrays.toString(combinationResultGroupMinus.get(i)));
//			}

			
		}
		

		System.out.println(
				"\tDone computing combinations for groups +/-, a total of : "
						+ hmGroupPlus2GroupMinus.size() + " group + keys"
						+ " and " + sizeCombinationsGroupMinus  + " group - values"
						+ " = " + (hmGroupPlus2GroupMinus.size() + sizeCombinationsGroupMinus)
						+ " total combinations found");
		
		
		
		System.out.println("Finished CombinationTaxoNode2StatOverRepresentation at "+LocalDateTime.now());
		
	}

	//RQ ex group orga
	// 9 : Fusobacteriales
	// 10 : Oscillatoriales
	// 11 : Lactococcus lactis, Leuconostoc, Klebsiella pneumoniae 
	// 12 : Enterobacter, Yersinia pestis
	// 13 : Enterococcus, Aquificae, Thermotogales, Serratia 
	
	
	//RQ : if minObjInCombination = 3;
	//RQ : for 7 orgaIds => 210 combinations ; if 1 min per combination => run for 3.5 hours
	//RQ : for 8 orgaIds => 1372 combinations ; if 1 min per combination => run for 23 hours
	//RQ : for 9 orgaIds => 6510 combinations ; if 1 min per combination => run for 4.5 days
	//RQ : for 10 orgaIds => 26574 combinations ; if 1 min per combination => run for 19 days
	//RQ : for 11 orgaIds => 99000 combinations ; if 1 min per combination => run for 69 days = 2.2 months
	//RQ : for 12 orgaIds => 347006 combinations ; if 1 min per combination => run for 240 days = 8-9 months
	
	//RQ : if minObjInCombination = 5;
	//RQ : for 12 orgaIds => 31152 combinations ; if 1 min per combination => run for 21 days (/1440)
	//RQ : for 13 orgaIds => 186186 combinations ; if 1 min per combination => run for 129 days (/1440)
	
	//RQ : if minObjInCombination = 6;
	//RQ : for 13 orgaIds => 15444 combinations ; if 1 min per combination => run for 10.7 days (/1440)
	//RQ : for 14 orgaIds => 145002 combinations ; if 1 min per combination => run for 101 days (/1440)
	//RQ : for 15 orgaIds => 958100 combinations ; if 1 min per combination => run for 665 days (/1440)
	
	private static ArrayList<Integer> getAlOrgaIdsWithTaxonId(int rootTaxoId) {
		// TODO or use code elsewhere
		
		//TODO remove mock data
		int numberMockDAta = 13;
		ArrayList<Integer> alToReturn = new ArrayList<>();
		for(int i=0;i<numberMockDAta;i++){
			alToReturn.add(i);
		}

		return alToReturn;
	}

	// we want combination ; i.e. How many different ways can you select 2 letters from the set of letters: X, Y, and Z? (Hint: In this problem, order is NOT important; i.e., XY is considered the same selection as YX.)
	//combinations of n objects taken r at a time
	// http://stattrek.com/probability/combinations-permutations.aspx
	// use in main like:
    // String[] arr = {"A","B","C","D","E","F"};
    // combinationsWithinArrayNObjectsTakenRAtATime(arr, 3, 0, new String[3]);
	// print 
	// [A, B, C]
	// [A, B, D]
	// ...
    static void combinationsWithinArrayNObjectsTakenRAtATime(
    		Integer[] arrayNObjects,
    		int r,
    		int startPositionInArray,
    		Integer[] result,
    		ArrayList<Integer[]> alAlCombinationResult
    		) throws Exception{
        if (r <= 0){
        	//finished computing all combination

    		//System.err.println("result : "+Arrays.toString(result));
        	Integer[] toStore = Arrays.copyOf(result, result.length);
        	alAlCombinationResult.add(toStore);
//        	String combinationIT = UtilitiesMethodsShared.getItemsAsStringFromCollection(Arrays.asList(result));
//        	if(hsCombinationResult.contains(combinationIT)){
//        		throw new Exception("hsCombinationResult already contain combination key : "+combinationIT);
//        	} else {
//        		hsCombinationResult.add(combinationIT);
//        	}
            return;
        }
        
        for (int i = startPositionInArray; i <= arrayNObjects.length-r; i++){
            result[result.length - r] = arrayNObjects[i];
            combinationsWithinArrayNObjectsTakenRAtATime(arrayNObjects, r-1, i+1, result, alAlCombinationResult);
        }
        
    } 
    
}
