package fr.inra.jouy.shared;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Test;
import fr.inra.jouy.shared.pojos.transitComposite.ConvertSetLongToSortedRangeItem;
import fr.inra.jouy.shared.pojos.transitComposite.PairLongLowerHigherRange;

public class TestMethodsShared {

	@Test
	public void test_convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair() throws Exception {
		
		HashSet<Long> hsTest = null;
		//ArrayList<ArrayList<Long>> sortedRange = null;
		ConvertSetLongToSortedRangeItem csltsriIT = null;
		String expectedResult = "";
		HashSet<Long> positiveSingletonAsIndividual = null;
		ArrayList<Long> sortedPositiveSingletonAsIndividual = null;
		ArrayList<PairLongLowerHigherRange> positiveRangeAsPair = null;
		String stPositiveRangeAsPair = "";
		HashSet<Long> cumulatedPositiveLongWithinRangesAsIndividual = null;
		HashSet<Long> negativeSingletonAsIndividual = null;
		ArrayList<PairLongLowerHigherRange> negativeRangeAsPair = null;
		String stNegativeRangeAsPair = "";
		HashSet<Long> cumulatedNegativeLongWithinRangesAsIndividual = null;
		ArrayList<Long> sortedCumulatedNegativeLongWithinRangesAsIndividual = null;
		String test = "";	
								
		// ** TEST1
		test = "test1";
		hsTest = Stream.of(
				Long.valueOf(1), Long.valueOf(2), Long.valueOf(3), Long.valueOf(4))
		         .collect(Collectors.toCollection(HashSet::new));
		csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(hsTest, false);
		
		//main array stores:
		// idx 0 : PositiveSingletonAsIndividual
		//positiveSingletonAsIndividual = sortedRange.get(0);
		positiveSingletonAsIndividual = csltsriIT.getPositiveSingletonAsIndividual();
		//System.out.println("positiveSingletonAsIndividual = "+positiveSingletonAsIndividual.toString());
		expectedResult = "[]";
		if(positiveSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + positiveSingletonAsIndividual.toString()
					);
		}
		// idx 1 : PositiveRangeAsPair
		positiveRangeAsPair = csltsriIT.getPositiveRangeAsPair();// sortedRange.get(1);
		//System.out.println("positiveRangeAsPair = "+positiveRangeAsPair.toString());
		expectedResult = "[[1, 4]]";
//		stPositiveRangeAsPair = "";
//		firstIter = true;
//		for (PairLongLowerHigherRange prapIT : positiveRangeAsPair) {
//			if (firstIter) {
//				firstIter = false;
//				stPositiveRangeAsPair += "[";
//			} else {
//				stPositiveRangeAsPair += ", ";
//			}
//			stPositiveRangeAsPair += "["+prapIT.getLowerRange()+", "+prapIT.getHigherRange()+"]";
//		}
//		if (!firstIter) {
//			stPositiveRangeAsPair += "]";
//		}
		stPositiveRangeAsPair = positiveRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stPositiveRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stPositiveRangeAsPair
					);
		}
		// cumulatedPositiveLongWithinRangesAsIndividual
		cumulatedPositiveLongWithinRangesAsIndividual = csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[1, 2, 3, 4]";
		if(cumulatedPositiveLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedPositiveLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedPositiveLongWithinRangesAsIndividual.toString()
					);
		}
		// idx 2 : NegativeSingletonAsIndividual
		negativeSingletonAsIndividual = csltsriIT.getNegativeSingletonAsIndividual();// sortedRange.get(2);
		//System.out.println("negativeSingletonAsIndividual = "+negativeSingletonAsIndividual.toString());
		expectedResult = "[]";
		if(negativeSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + negativeSingletonAsIndividual.toString()
					);
		}
		// idx 3 : NegativeRangeAsPair
		negativeRangeAsPair = csltsriIT.getNegativeRangeAsPair();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[]";
		stNegativeRangeAsPair = negativeRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stNegativeRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stNegativeRangeAsPair
					);
		}
		// cumulatedNegativeLongWithinRangesAsIndividual
		cumulatedNegativeLongWithinRangesAsIndividual = csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[]";
		if(cumulatedNegativeLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedNegativeLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedNegativeLongWithinRangesAsIndividual.toString()
					);
		}
		

		// ** TEST2
		test = "test2";
		hsTest = Stream.of(
				Long.valueOf(1), Long.valueOf(3), Long.valueOf(4), Long.valueOf(5))
		         .collect(Collectors.toCollection(HashSet::new));
		csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(hsTest, false);
		
		//main array stores:
		// idx 0 : PositiveSingletonAsIndividual
		//positiveSingletonAsIndividual = sortedRange.get(0);
		positiveSingletonAsIndividual = csltsriIT.getPositiveSingletonAsIndividual();
		//System.out.println("positiveSingletonAsIndividual = "+positiveSingletonAsIndividual.toString());
		expectedResult = "[1]";
		if(positiveSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + positiveSingletonAsIndividual.toString()
					);
		}
		
		// idx 1 : PositiveRangeAsPair
		positiveRangeAsPair = csltsriIT.getPositiveRangeAsPair();// sortedRange.get(1);
		//System.out.println("positiveRangeAsPair = "+positiveRangeAsPair.toString());
		expectedResult = "[[3, 5]]";
//		stPositiveRangeAsPair = "";
//		firstIter = true;
//		for (PairLongLowerHigherRange prapIT : positiveRangeAsPair) {
//			if (firstIter) {
//				firstIter = false;
//				stPositiveRangeAsPair += "[";
//			} else {
//				stPositiveRangeAsPair += ", ";
//			}
//			stPositiveRangeAsPair += "["+prapIT.getLowerRange()+", "+prapIT.getHigherRange()+"]";
//		}
//		if (!firstIter) {
//			stPositiveRangeAsPair += "]";
//		}
		stPositiveRangeAsPair = positiveRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stPositiveRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stPositiveRangeAsPair
					);
		}
		// cumulatedPositiveLongWithinRangesAsIndividual
		cumulatedPositiveLongWithinRangesAsIndividual = csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[3, 4, 5]";
		if(cumulatedPositiveLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedPositiveLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedPositiveLongWithinRangesAsIndividual.toString()
					);
		}
		// idx 2 : NegativeSingletonAsIndividual
		negativeSingletonAsIndividual = csltsriIT.getNegativeSingletonAsIndividual();// sortedRange.get(2);
		//System.out.println("negativeSingletonAsIndividual = "+negativeSingletonAsIndividual.toString());
		expectedResult = "[]";
		if(negativeSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + negativeSingletonAsIndividual.toString()
					);
		}
		// idx 3 : NegativeRangeAsPair
		negativeRangeAsPair = csltsriIT.getNegativeRangeAsPair();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[]";
		stNegativeRangeAsPair = negativeRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stNegativeRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stNegativeRangeAsPair
					);
		}
		// cumulatedNegativeLongWithinRangesAsIndividual
		cumulatedNegativeLongWithinRangesAsIndividual = csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[]";
		if(cumulatedNegativeLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedNegativeLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedNegativeLongWithinRangesAsIndividual.toString()
					);
		}
		
		
		// ** TEST3
		test = "test3";
		hsTest = Stream.of(
				Long.valueOf(-4), Long.valueOf(-2), Long.valueOf(-1), Long.valueOf(0)
				, Long.valueOf(1), Long.valueOf(3), Long.valueOf(4), Long.valueOf(5), Long.valueOf(7)
				)
		         .collect(Collectors.toCollection(HashSet::new));
		csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(hsTest, false);
		
		//main array stores:
		// idx 0 : PositiveSingletonAsIndividual
		//positiveSingletonAsIndividual = sortedRange.get(0);
		positiveSingletonAsIndividual = csltsriIT.getPositiveSingletonAsIndividual();
		//System.out.println("positiveSingletonAsIndividual = "+positiveSingletonAsIndividual.toString());
		expectedResult = "[7]";
		if(positiveSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + positiveSingletonAsIndividual.toString()
					);
		}
		
		// idx 1 : PositiveRangeAsPair
		positiveRangeAsPair = csltsriIT.getPositiveRangeAsPair();// sortedRange.get(1);
		//System.out.println("positiveRangeAsPair = "+positiveRangeAsPair.toString());
		expectedResult = "[[0, 1], [3, 5]]";
//		stPositiveRangeAsPair = "";
//		firstIter = true;
//		for (PairLongLowerHigherRange prapIT : positiveRangeAsPair) {
//			if (firstIter) {
//				firstIter = false;
//				stPositiveRangeAsPair += "[";
//			} else {
//				stPositiveRangeAsPair += ", ";
//			}
//			stPositiveRangeAsPair += "["+prapIT.getLowerRange()+", "+prapIT.getHigherRange()+"]";
//		}
//		if (!firstIter) {
//			stPositiveRangeAsPair += "]";
//		}
		stPositiveRangeAsPair = positiveRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stPositiveRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stPositiveRangeAsPair
					);
		}
		// cumulatedPositiveLongWithinRangesAsIndividual
		cumulatedPositiveLongWithinRangesAsIndividual = csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[0, 1, 3, 4, 5]";
		if(cumulatedPositiveLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedPositiveLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedPositiveLongWithinRangesAsIndividual.toString()
					);
		}
		// idx 2 : NegativeSingletonAsIndividual
		negativeSingletonAsIndividual = csltsriIT.getNegativeSingletonAsIndividual();// sortedRange.get(2);
		//System.out.println("negativeSingletonAsIndividual = "+negativeSingletonAsIndividual.toString());
		expectedResult = "[4]";
		if(negativeSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + negativeSingletonAsIndividual.toString()
					);
		}
		// idx 3 : NegativeRangeAsPair
		negativeRangeAsPair = csltsriIT.getNegativeRangeAsPair();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[[1, 2]]";
		stNegativeRangeAsPair = negativeRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stNegativeRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stNegativeRangeAsPair
					);
		}
		// cumulatedNegativeLongWithinRangesAsIndividual
		cumulatedNegativeLongWithinRangesAsIndividual = csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[1, 2]";
		if(cumulatedNegativeLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedNegativeLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedNegativeLongWithinRangesAsIndividual.toString()
					);
		}
		

		// ** TEST4
		test = "test4";
		hsTest = Stream.of(
				Long.valueOf(-55), Long.valueOf(-54), Long.valueOf(-53), Long.valueOf(-51), Long.valueOf(-50)
				, Long.valueOf(-15), Long.valueOf(-12), Long.valueOf(-11), Long.valueOf(-10), Long.valueOf(-1)
				, Long.valueOf(2), Long.valueOf(5), Long.valueOf(7), Long.valueOf(9), Long.valueOf(10), Long.valueOf(12), Long.valueOf(13), Long.valueOf(14), Long.valueOf(15), Long.valueOf(16)
				, Long.valueOf(20), Long.valueOf(21), Long.valueOf(22), Long.valueOf(23), Long.valueOf(24), Long.valueOf(25), Long.valueOf(26)
				)
		         .collect(Collectors.toCollection(HashSet::new));
		csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(hsTest, false);
		
		//main array stores:
		// idx 0 : PositiveSingletonAsIndividual
		//positiveSingletonAsIndividual = sortedRange.get(0);
		positiveSingletonAsIndividual = csltsriIT.getPositiveSingletonAsIndividual();
		//System.out.println("positiveSingletonAsIndividual = "+positiveSingletonAsIndividual.toString());
		expectedResult = "[2, 5, 7]";
		if(positiveSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + positiveSingletonAsIndividual.toString()
					);
		}
		
		// idx 1 : PositiveRangeAsPair
		positiveRangeAsPair = csltsriIT.getPositiveRangeAsPair();// sortedRange.get(1);
		//System.out.println("positiveRangeAsPair = "+positiveRangeAsPair.toString());
		expectedResult = "[[9, 10], [12, 16], [20, 26]]";
//		stPositiveRangeAsPair = "";
//		firstIter = true;
//		for (PairLongLowerHigherRange prapIT : positiveRangeAsPair) {
//			if (firstIter) {
//				firstIter = false;
//				stPositiveRangeAsPair += "[";
//			} else {
//				stPositiveRangeAsPair += ", ";
//			}
//			stPositiveRangeAsPair += "["+prapIT.getLowerRange()+", "+prapIT.getHigherRange()+"]";
//		}
//		if (!firstIter) {
//			stPositiveRangeAsPair += "]";
//		}
		stPositiveRangeAsPair = positiveRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stPositiveRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stPositiveRangeAsPair
					);
		}
		// cumulatedPositiveLongWithinRangesAsIndividual
		cumulatedPositiveLongWithinRangesAsIndividual = csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[9, 10, 12, 13, 14, 15, 16, 20, 21, 22, 23, 24, 25, 26]";
		if(cumulatedPositiveLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedPositiveLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedPositiveLongWithinRangesAsIndividual.toString()
					);
		}
		// idx 2 : NegativeSingletonAsIndividual
		negativeSingletonAsIndividual = csltsriIT.getNegativeSingletonAsIndividual();// sortedRange.get(2);
		//System.out.println("negativeSingletonAsIndividual = "+negativeSingletonAsIndividual.toString());
		expectedResult = "[1, 15]";
		if(negativeSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + negativeSingletonAsIndividual.toString()
					);
		}
		// idx 3 : NegativeRangeAsPair
		negativeRangeAsPair = csltsriIT.getNegativeRangeAsPair();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[[53, 55], [50, 51], [10, 12]]";
		stNegativeRangeAsPair = negativeRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stNegativeRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stNegativeRangeAsPair
					);
		}
		// cumulatedNegativeLongWithinRangesAsIndividual
		cumulatedNegativeLongWithinRangesAsIndividual = csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[10, 11, 12, 50, 51, 53, 54, 55]";
		sortedCumulatedNegativeLongWithinRangesAsIndividual = new ArrayList<>(cumulatedNegativeLongWithinRangesAsIndividual);
		Collections.sort(sortedCumulatedNegativeLongWithinRangesAsIndividual);
		if(sortedCumulatedNegativeLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedNegativeLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + sortedCumulatedNegativeLongWithinRangesAsIndividual.toString()
					);
		}
		
		

		// ** TEST5
		test = "test5";
		hsTest = Stream.of(
				Long.valueOf(-55), Long.valueOf(-54), Long.valueOf(-53), Long.valueOf(-51), Long.valueOf(-50)
				, Long.valueOf(-16), Long.valueOf(-15), Long.valueOf(-12), Long.valueOf(-1) //, Long.valueOf(-10)
				, Long.valueOf(2), Long.valueOf(5), Long.valueOf(7), Long.valueOf(9), Long.valueOf(10), Long.valueOf(12), Long.valueOf(13), Long.valueOf(14), Long.valueOf(15), Long.valueOf(16)
				, Long.valueOf(20), Long.valueOf(21), Long.valueOf(22), Long.valueOf(23), Long.valueOf(24), Long.valueOf(25), Long.valueOf(26), Long.valueOf(123526), Long.valueOf(133526)
				)
		         .collect(Collectors.toCollection(HashSet::new));
		csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(hsTest, true);
		
		//main array stores:
		// idx 0 : PositiveSingletonAsIndividual
		//positiveSingletonAsIndividual = sortedRange.get(0);
		positiveSingletonAsIndividual = csltsriIT.getPositiveSingletonAsIndividual();
		//System.out.println("positiveSingletonAsIndividual = "+positiveSingletonAsIndividual.toString());
		expectedResult = "[123526, 133526]";
		sortedPositiveSingletonAsIndividual = new ArrayList<>(positiveSingletonAsIndividual);
		Collections.sort(sortedPositiveSingletonAsIndividual);
		if(sortedPositiveSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + sortedPositiveSingletonAsIndividual.toString()
					);
		}
		
		// idx 1 : PositiveRangeAsPair
		positiveRangeAsPair = csltsriIT.getPositiveRangeAsPair();// sortedRange.get(1);
		//System.out.println("positiveRangeAsPair = "+positiveRangeAsPair.toString());
		expectedResult = "[[2, 26]]";
//		stPositiveRangeAsPair = "";
//		firstIter = true;
//		for (PairLongLowerHigherRange prapIT : positiveRangeAsPair) {
//			if (firstIter) {
//				firstIter = false;
//				stPositiveRangeAsPair += "[";
//			} else {
//				stPositiveRangeAsPair += ", ";
//			}
//			stPositiveRangeAsPair += "["+prapIT.getLowerRange()+", "+prapIT.getHigherRange()+"]";
//		}
//		if (!firstIter) {
//			stPositiveRangeAsPair += "]";
//		}
		stPositiveRangeAsPair = positiveRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stPositiveRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stPositiveRangeAsPair
					);
		}
		// cumulatedPositiveLongWithinRangesAsIndividual
		cumulatedPositiveLongWithinRangesAsIndividual = csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[2, 5, 7, 9, 10, 12, 13, 14, 15, 16, 20, 21, 22, 23, 24, 25, 26]";
		if(cumulatedPositiveLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedPositiveLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedPositiveLongWithinRangesAsIndividual.toString()
					);
		}
		// idx 2 : NegativeSingletonAsIndividual
		negativeSingletonAsIndividual = csltsriIT.getNegativeSingletonAsIndividual();// sortedRange.get(2);
		//System.out.println("negativeSingletonAsIndividual = "+negativeSingletonAsIndividual.toString());
		expectedResult = "[1]";
		if(negativeSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + negativeSingletonAsIndividual.toString()
					);
		}
		// idx 3 : NegativeRangeAsPair
		negativeRangeAsPair = csltsriIT.getNegativeRangeAsPair();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[[50, 55], [12, 16]]";
		stNegativeRangeAsPair = negativeRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stNegativeRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stNegativeRangeAsPair
					);
		}
		// cumulatedNegativeLongWithinRangesAsIndividual
		cumulatedNegativeLongWithinRangesAsIndividual = csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[12, 15, 16, 50, 51, 53, 54, 55]";
		sortedCumulatedNegativeLongWithinRangesAsIndividual = new ArrayList<>(cumulatedNegativeLongWithinRangesAsIndividual);
		Collections.sort(sortedCumulatedNegativeLongWithinRangesAsIndividual);
		if(sortedCumulatedNegativeLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedNegativeLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + sortedCumulatedNegativeLongWithinRangesAsIndividual.toString()
					);
		}
		

		// ** TEST6
		test = "test6";
		hsTest = Stream.of(
				-5123846182L, -5123846181L
				)
		         .collect(Collectors.toCollection(HashSet::new));
		csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(hsTest, true);
		
		//main array stores:
		// idx 0 : PositiveSingletonAsIndividual
		//positiveSingletonAsIndividual = sortedRange.get(0);
		positiveSingletonAsIndividual = csltsriIT.getPositiveSingletonAsIndividual();
		//System.out.println("positiveSingletonAsIndividual = "+positiveSingletonAsIndividual.toString());
		expectedResult = "[]";
		sortedPositiveSingletonAsIndividual = new ArrayList<>(positiveSingletonAsIndividual);
		Collections.sort(sortedPositiveSingletonAsIndividual);
		if(sortedPositiveSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + sortedPositiveSingletonAsIndividual.toString()
					);
		}
		
		// idx 1 : PositiveRangeAsPair
		positiveRangeAsPair = csltsriIT.getPositiveRangeAsPair();// sortedRange.get(1);
		//System.out.println("positiveRangeAsPair = "+positiveRangeAsPair.toString());
		expectedResult = "[]";
//		stPositiveRangeAsPair = "";
//		firstIter = true;
//		for (PairLongLowerHigherRange prapIT : positiveRangeAsPair) {
//			if (firstIter) {
//				firstIter = false;
//				stPositiveRangeAsPair += "[";
//			} else {
//				stPositiveRangeAsPair += ", ";
//			}
//			stPositiveRangeAsPair += "["+prapIT.getLowerRange()+", "+prapIT.getHigherRange()+"]";
//		}
//		if (!firstIter) {
//			stPositiveRangeAsPair += "]";
//		}
		stPositiveRangeAsPair = positiveRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stPositiveRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stPositiveRangeAsPair
					);
		}
		// cumulatedPositiveLongWithinRangesAsIndividual
		cumulatedPositiveLongWithinRangesAsIndividual = csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[]";
		if(cumulatedPositiveLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedPositiveLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedPositiveLongWithinRangesAsIndividual.toString()
					);
		}
		// idx 2 : NegativeSingletonAsIndividual
		negativeSingletonAsIndividual = csltsriIT.getNegativeSingletonAsIndividual();// sortedRange.get(2);
		//System.out.println("negativeSingletonAsIndividual = "+negativeSingletonAsIndividual.toString());
		expectedResult = "[]";
		if(negativeSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + negativeSingletonAsIndividual.toString()
					);
		}
		// idx 3 : NegativeRangeAsPair
		negativeRangeAsPair = csltsriIT.getNegativeRangeAsPair();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[[5123846181, 5123846182]]";
		stNegativeRangeAsPair = negativeRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stNegativeRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stNegativeRangeAsPair
					);
		}
		// cumulatedNegativeLongWithinRangesAsIndividual
		cumulatedNegativeLongWithinRangesAsIndividual = csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[5123846181, 5123846182]";
		sortedCumulatedNegativeLongWithinRangesAsIndividual = new ArrayList<>(cumulatedNegativeLongWithinRangesAsIndividual);
		Collections.sort(sortedCumulatedNegativeLongWithinRangesAsIndividual);
		if(sortedCumulatedNegativeLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedNegativeLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + sortedCumulatedNegativeLongWithinRangesAsIndividual.toString()
					);
		}
		
		

		// ** TEST6
		test = "test6";
		hsTest = Stream.of(
				5123846180L, 5123846181L
				)
		         .collect(Collectors.toCollection(HashSet::new));
		csltsriIT = UtilitiesMethodsShared.convertSetLongToSortedRange_PositiveSingletonAsIndividualThenPositiveRangeAsPairThenNegativeSingletonAsIndividualThenNegativeRangeAsPair(hsTest, true);
		
		//main array stores:
		// idx 0 : PositiveSingletonAsIndividual
		//positiveSingletonAsIndividual = sortedRange.get(0);
		positiveSingletonAsIndividual = csltsriIT.getPositiveSingletonAsIndividual();
		//System.out.println("positiveSingletonAsIndividual = "+positiveSingletonAsIndividual.toString());
		expectedResult = "[]";
		sortedPositiveSingletonAsIndividual = new ArrayList<>(positiveSingletonAsIndividual);
		Collections.sort(sortedPositiveSingletonAsIndividual);
		if(sortedPositiveSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + sortedPositiveSingletonAsIndividual.toString()
					);
		}
		
		// idx 1 : PositiveRangeAsPair
		positiveRangeAsPair = csltsriIT.getPositiveRangeAsPair();// sortedRange.get(1);
		//System.out.println("positiveRangeAsPair = "+positiveRangeAsPair.toString());
		expectedResult = "[[5123846180, 5123846181]]";
//		stPositiveRangeAsPair = "";
//		firstIter = true;
//		for (PairLongLowerHigherRange prapIT : positiveRangeAsPair) {
//			if (firstIter) {
//				firstIter = false;
//				stPositiveRangeAsPair += "[";
//			} else {
//				stPositiveRangeAsPair += ", ";
//			}
//			stPositiveRangeAsPair += "["+prapIT.getLowerRange()+", "+prapIT.getHigherRange()+"]";
//		}
//		if (!firstIter) {
//			stPositiveRangeAsPair += "]";
//		}
		stPositiveRangeAsPair = positiveRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stPositiveRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : positiveRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stPositiveRangeAsPair
					);
		}
		// cumulatedPositiveLongWithinRangesAsIndividual
		cumulatedPositiveLongWithinRangesAsIndividual = csltsriIT.getCumulatedPositiveLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[5123846180, 5123846181]";
		if(cumulatedPositiveLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedPositiveLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + cumulatedPositiveLongWithinRangesAsIndividual.toString()
					);
		}
		// idx 2 : NegativeSingletonAsIndividual
		negativeSingletonAsIndividual = csltsriIT.getNegativeSingletonAsIndividual();// sortedRange.get(2);
		//System.out.println("negativeSingletonAsIndividual = "+negativeSingletonAsIndividual.toString());
		expectedResult = "[]";
		if(negativeSingletonAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeSingletonAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + negativeSingletonAsIndividual.toString()
					);
		}
		// idx 3 : NegativeRangeAsPair
		negativeRangeAsPair = csltsriIT.getNegativeRangeAsPair();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[]";
		stNegativeRangeAsPair = negativeRangeAsPair.stream().map(
				pairLongLowerHigherRangeIT -> "[" + pairLongLowerHigherRangeIT.getLowerRange() + ", " + pairLongLowerHigherRangeIT.getHigherRange() + "]" 
				).collect(Collectors.toList())
				.toString();
		if(stNegativeRangeAsPair.compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : negativeRangeAsPair"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + stNegativeRangeAsPair
					);
		}
		// cumulatedNegativeLongWithinRangesAsIndividual
		cumulatedNegativeLongWithinRangesAsIndividual = csltsriIT.getCumulatedNegativeLongWithinRangesAsIndividual();// sortedRange.get(3);
		//System.out.println("negativeRangeAsPair = "+negativeRangeAsPair.toString());
		expectedResult = "[]";
		sortedCumulatedNegativeLongWithinRangesAsIndividual = new ArrayList<>(cumulatedNegativeLongWithinRangesAsIndividual);
		Collections.sort(sortedCumulatedNegativeLongWithinRangesAsIndividual);
		if(sortedCumulatedNegativeLongWithinRangesAsIndividual.toString().compareTo(expectedResult) != 0){
			fail("Different results where found for "+test+" : cumulatedNegativeLongWithinRangesAsIndividual"
					+ "\nfrom test =\n"+expectedResult
					+ "\nfrom method =\n" + sortedCumulatedNegativeLongWithinRangesAsIndividual.toString()
					);
		}
		
		
		assertTrue(true);
		
	}
	
	
}
