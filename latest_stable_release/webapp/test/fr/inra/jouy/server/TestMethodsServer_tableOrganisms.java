package fr.inra.jouy.server;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.junit.Test;

import fr.inra.jouy.server.queriesTable.QueriesTableOrganisms;
import fr.inra.jouy.shared.pojos.databaseMapping.LightOrganismItem;

public class TestMethodsServer_tableOrganisms {

	@Test
	public void test_getAllLightOrganismItems() throws Exception {

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		String methodNameToReport = "TestMethodsServer_tableOrganisms test_getAllLightOrganismItems";
		
		boolean onlyPublic = true;
		boolean onlyWithElement = true;
		
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			//get all tuples from table
			ArrayList<LightOrganismItem> alFromTest = new ArrayList<LightOrganismItem>();
			String command = "SELECT organisms.organism_id, organisms.species, organisms.strain, organisms.substrain, organisms.taxon_id, organisms.ispublic"
					//+ ", elements.element_id, elements.type, elements.accession, elements.size"
					+ " FROM organisms"
					//+ ", elements"
					//+ " WHERE elements.organism_id = organisms.organism_id"
					;
			String whereClause = "";
			if(onlyPublic){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organisms.ispublic is true";
			}
			if(onlyWithElement){
				if(whereClause.isEmpty()){
					whereClause += " WHERE";
				} else {
					whereClause += " AND";
				}
				whereClause += " organisms.organism_id IN (select distinct(organism_id) FROM elements)";
			}
			command += whereClause;
			//command += " ORDER BY organisms.organism_id";
			
			//System.err.println(command);
			
			statement = conn.createStatement();
			long milliPrint1 = System.currentTimeMillis();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			while (rs.next()) {
					int organismId = rs.getInt("organism_id");
					String strain = rs.getString("strain");
					String species = rs.getString("species");
					LightOrganismItem loi = null;
//					if (hm.containsKey(organismId)) {
//						loi = hm.get(organismId);
//					} else {
						loi = new LightOrganismItem();
						loi.setOrganismId(organismId);
						loi.setSpecies(species);
						loi.setStrain(strain);
						loi.setSubstrain(rs.getString("substrain"));
						loi.setTaxonId(rs.getInt("taxon_id"));
						loi.setPublic(rs.getBoolean("ispublic"));
//						hm.put(organismId, loi);
						alFromTest.add(loi);
//					}
//					LightElementItem lei = new LightElementItem();
//					lei.setElementId(rs.getInt("element_id"));
//					lei.setOrganismId(organismId);
//					lei.setType(rs.getString("type"));
//					lei.setAccession(rs.getString("accession"));
//					lei.setSize(rs.getInt("size"));
//					lei.setSpecies(species);
//					lei.setStrain(strain);
//					loi.getListAllLightElementItem().add(lei);
			}
			long milliPrint2 = System.currentTimeMillis() - milliPrint1;
			System.out.println("Test SQL query took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);
			
			//test if we get similar answer with method LowLevelQueriesTables

			long milliPrint3 = System.currentTimeMillis();
			ArrayList<LightOrganismItem> alFromMeth = QueriesTableOrganisms.getAlLightOrganismItems_orderByScoreAscIfProvided(
					conn
					, null
					, null
					, null
					, false
					, false
					, onlyPublic
					, onlyWithElement
					);
			long milliPrint4 = System.currentTimeMillis() - milliPrint3;
			System.out.println("Method SQL query took\t" + milliPrint4 + "\tmilliseconds in test method "+methodNameToReport);
			
			Comparator<LightOrganismItem> compaLightOrganismItemByOrgaId = new ByOrgaIdLightOrganismItemComparator();
			Collections.sort(alFromTest, compaLightOrganismItemByOrgaId);
			StringBuilder sb_fromTest = new StringBuilder();
			for(LightOrganismItem loiIT : alFromTest){
				sb_fromTest.append(loiIT.stringifyToJSON(true, true));
			}
			Collections.sort(alFromMeth, compaLightOrganismItemByOrgaId);
			StringBuilder sb_fromMethod = new StringBuilder();
			for(LightOrganismItem loiIT : alFromMeth){
				sb_fromMethod.append(loiIT.stringifyToJSON(true, true));
			}
			
			if(sb_fromTest.toString().compareTo(sb_fromMethod.toString()) != 0){
				fail("Different results where found :"
						+ "\nfrom test =\n" + sb_fromTest.toString()
						+ "\nfrom method =\n" + sb_fromMethod.toString()
						);
			}
//			else {
//				System.err.println("OK similar results where found :"
//						+ "\nfrom test =\n" + sb_fromTest.toString()
//						+ "\nfrom method =\n" + sb_fromMethod.toString()
//						);
//			}
			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}
	
		assertTrue(true);
		
	}

	
	// comparator
	public class ByOrgaIdLightOrganismItemComparator implements Comparator<LightOrganismItem> {
		@Override
		public int compare(LightOrganismItem arg0, LightOrganismItem arg1) {
			return (arg0.getOrganismId() - arg1.getOrganismId());
		}
	}
	
}
