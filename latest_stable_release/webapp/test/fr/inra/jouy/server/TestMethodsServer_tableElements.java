package fr.inra.jouy.server;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import static org.junit.Assert.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;
import org.junit.Test;
import fr.inra.jouy.server.objects.ObjAssociationAsMap;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.server.queriesTable.QueriesTableElements;
import fr.inra.jouy.shared.pojos.databaseMapping.LightElementItem;



public class TestMethodsServer_tableElements {

	//int in table element are : organism_id, size, version
	@Test
	public void test_getIntAttributeWithPrimaryId() throws Exception {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		String methodNameToReport = "TestMethodsServer_tableElements test_getIntAttributeWithPrimaryId";
		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			//get random tuples from table
			ArrayList<Integer> alRandomEltId = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
					"element_id",
					"public.elements",
					methodNameToReport);
			ArrayList<String> alAttributeToRecover = new ArrayList<String>();
			alAttributeToRecover.add("organism_id");
			alAttributeToRecover.add("size");
			alAttributeToRecover.add("version");
			
			ArrayList<Integer> alResult_fromTest = new ArrayList<>();
			for(int i=0 ; i < alRandomEltId.size() ; i++){
				int eltIdIT = alRandomEltId.get(i);
				String attributeIT = alAttributeToRecover.get(i%3);
				String command = "SELECT "+attributeIT
						+ " FROM elements"
						+ " WHERE element_id = "+eltIdIT
						;

				long milliPrint1 = System.currentTimeMillis();
				rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
				if (rs.next()) {
					alResult_fromTest.add(rs.getInt(attributeIT));
				}
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Test SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);
			}
			
			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			for(int i=0 ; i < alRandomEltId.size() ; i++){
				int eltIdIT = alRandomEltId.get(i);
				String attributeIT = alAttributeToRecover.get(i%3);
				long milliPrint1 = System.currentTimeMillis();
				int int_fromMethod = QueriesTableElements.getIntAttributeWithPrimaryId(
						conn,
						eltIdIT,
						attributeIT,
						true,
						false,
						true);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				if(alResult_fromTest.get(i).compareTo(int_fromMethod) != 0){
					fail("Different results where found for element_id "+eltIdIT+" attribute "+attributeIT
							+ ": from test = " + alResult_fromTest.get(i)
							+ " ; from method = " + int_fromMethod
							);
				}
//				else {
//					System.err.println("OK similar results where found for element_id "+eltIdIT+" attribute "+attributeIT
//							+ ": from test = " + alResult_fromTest.get(i)
//							+ " ; from method = " + int_fromMethod
//							);
//				}
			}

			

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}
		assertTrue(true);
	}
	
	
	@Test
	public void test_getAlLightElementItemsPlusOrganismIdFacultatifWithSetOrganismIdsFacultatif() throws Exception {
		
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer_tableElements test_getAlLightElementItemsPlusOrganismIdFacultatifWithSetOrganismIdsFacultatif";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			//get all tuples from table
			ArrayList<LightElementItem> alFromTest = new ArrayList<LightElementItem>();
			String command = "SELECT element_id"
					+ ", organism_id"
					+ ", size, type, accession"
					+ " FROM elements"
					//+ " WHERE organism_id IN (1,5,6,9,8,10,15)"
					;
			statement = conn.createStatement();
			long milliPrint1 = System.currentTimeMillis();
			rs = UtilitiesMethodsServer.executeAndLogSQLQuery(statement, rs, command, methodNameToReport);
			while (rs.next()) {
					LightElementItem lei = new LightElementItem();;
					lei.setElementId(rs.getInt("element_id"));
					lei.setOrganismId(rs.getInt("organism_id"));
					lei.setSize(rs.getInt("size"));
					lei.setType(rs.getString("type"));
					lei.setAccession(rs.getString("accession"));
					alFromTest.add(lei);
			}
			long milliPrint2 = System.currentTimeMillis() - milliPrint1;
			System.out.println("Test SQL query took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);
			
			//test if we get similar answer with method LowLevelQueriesTables
			long milliPrint3 = System.currentTimeMillis();
			HashSet<Integer> alRestrictIds = new HashSet<Integer>();
//			alRestrictIds.add(1);
//			alRestrictIds.add(5);
//			alRestrictIds.add(6);
//			alRestrictIds.add(9);
//			alRestrictIds.add(8);
//			alRestrictIds.add(10);
//			alRestrictIds.add(15);
			ArrayList<LightElementItem> alFromMeth = QueriesTableElements.getAlLightElementItemsPlusOrganismIdFacultatifWithSetOrganismIdsFacultatif(
					conn
					, true
					, alRestrictIds
					, true
					);
			long milliPrint4 = System.currentTimeMillis() - milliPrint3;
			System.out.println("Method SQL query took\t" + milliPrint4 + "\tmilliseconds in test method "+methodNameToReport);
			
			Comparator<LightElementItem> compaLightElementItemByElementId = new ByElementIdLightElementItemComparator();
			Collections.sort(alFromTest, compaLightElementItemByElementId);
			StringBuilder sb_fromTest = new StringBuilder();
			for(LightElementItem leiIT : alFromTest){
				sb_fromTest.append(leiIT.stringifyToJSON(true, true));
			}
			Collections.sort(alFromMeth, compaLightElementItemByElementId);
			StringBuilder sb_fromMethod = new StringBuilder();
			for(LightElementItem leiIT : alFromMeth){
				sb_fromMethod.append(leiIT.stringifyToJSON(true, true));
			}
			if(sb_fromTest.toString().compareTo(sb_fromMethod.toString()) != 0){
				fail("Different results where found :"
						+ "\nfrom test =\n" + sb_fromTest.toString()
						+ "\nfrom method =\n" + sb_fromMethod.toString()
						);
			}
//			else {
//				System.err.println("OK similar results where found :"
//						+ "\nfrom test =\n" + sb_fromTest.toString()
//						+ "\nfrom method =\n" + sb_fromMethod.toString()
//						);
//			}
			

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}
		assertTrue(true);
	}
			

	@Test
	public void test_getAccessionWithElementId() throws Exception {
		
		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
		boolean closeConn = false;
		
		String methodNameToReport = "TestMethodsServer_tableElements test_getAccessionWithElementId";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();
			
			//get x random tuples from table
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsString_aliase2columnName.put("accession", "accession");
			ObjSQLCommand objSQLCmmdIT = new ObjSQLCommand(
					chmColumnSelectAsInt_aliase2columnName,
					chmColumnSelectAsString_aliase2columnName,
					null, null);
			
			ArrayList<ObjAssociationAsMap> alOaam = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
					conn,
					"element_id",
					"public.elements",
					objSQLCmmdIT,
					//UtilitiesMethodsTest.getApproximateSizeOfTable(conn, "public", "elements"),
					methodNameToReport);
			
			
			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			for(int i=0;i<alOaam.size();i++){
				int elementId_test = alOaam.get(i).getRsColumn2Int().get("element_id");
				String accnum_test = alOaam.get(i).getRsColumn2String().get("accession");
				long milliPrint1 = System.currentTimeMillis();
				String accnum_method = QueriesTableElements.getAccessionWithElementId(
						conn,
						elementId_test,
						true);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				if(accnum_test.compareTo(accnum_method) != 0){
					fail("Different results where found for element_id "+elementId_test
							+ ": from test = " + accnum_test
							+ " ; from method = " + accnum_method
							);
				}
//				else {
//					System.err.println("OK similar results where found for accnum "+associatedAccnumIT
//							+ ": int from test = " + primaryKeyIT
//							+ " ; int from method = " + elementIdFromMeth);
//				}
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try
			
		
		assertTrue(true);
	}

			
			
	@Test
	public void test_getListAccnumWithOrgaId() throws Exception {
		
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		String methodNameToReport = "TestMethodsServer_tableElements test_getListAccnumWithOrgaId";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();


			//get x random primaryKey from other table
			ArrayList<Integer> alRandomInt = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
					"organism_id",
					"public.organisms",
					methodNameToReport);
			
			//get x random tuples from table
			ArrayList<ArrayList<String>> alAlAccnumsFromTest = new ArrayList<ArrayList<String>>();
			for(int i=0; i < alRandomInt.size() ; i++){
				int primaryKeyIT = alRandomInt.get(i);
				String commandGetTuplesFromTableElements = "SELECT accession"
						+ " FROM elements"
						+ " WHERE organism_id = "+primaryKeyIT;
				long milliPrint1 = System.currentTimeMillis();
				rs = statement.executeQuery(commandGetTuplesFromTableElements);
				ArrayList<String> alAccnums = new ArrayList<String>();
				while (rs.next()) {
					alAccnums.add(rs.getString("accession"));
				}
				if(alAccnums.isEmpty()){
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("Error, no row returned for command "+commandGetTuplesFromTableElements));
				} else {
					alAlAccnumsFromTest.add(alAccnums);
				}
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Test SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);
			}

			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			for(int i=0;i<alRandomInt.size();i++){
				int primaryKeyIT = alRandomInt.get(i);
				ArrayList<String> alAccnumFromTest = alAlAccnumsFromTest.get(i);
				long milliPrint1 = System.currentTimeMillis();
				HashSet<String> hsAccnumFromMeth = QueriesTableElements.getHsAccessions_withOrgaId(
						conn,
						primaryKeyIT
						);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				Collections.sort(alAccnumFromTest);
				ArrayList<String> alAccnumFromMeth = new ArrayList<>(hsAccnumFromMeth);
				Collections.sort(alAccnumFromMeth);
				if(alAccnumFromTest.toString().compareTo(alAccnumFromMeth.toString()) != 0){
					fail("Different results where found for OrgaId "+primaryKeyIT
							+ ": from test = " + alAccnumFromTest.toString()
							+ " ; from method = " + alAccnumFromMeth.toString()
							);
				}
//				else {
//					System.err.println("OK similar results where found for OrgaId "+primaryKeyIT
//							+ ": from test = " + alAccnumFromTest.toString()
//							+ " ; from method = " + alAccnumFromMeth.toString());
//				}
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				try {
					rs.close();
					rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				try {
					statement.close();
					statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		assertTrue(true);
	}
	
	@Test
	public void test_getElementIdWithAccession() throws Exception {
		
		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
		boolean closeConn = false;
		
		String methodNameToReport = "TestMethodsServer_tableElements test_getElementIdWithAccession";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			//get x random tuples from table
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsString_aliase2columnName.put("accession", "accession");
			ObjSQLCommand objSQLCmmdIT = new ObjSQLCommand(
					chmColumnSelectAsInt_aliase2columnName,
					chmColumnSelectAsString_aliase2columnName,
					null, null);
			
			ArrayList<ObjAssociationAsMap> alOaam = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
					conn,
					"element_id",
					"public.elements",
					objSQLCmmdIT,
					//UtilitiesMethodsTest.getApproximateSizeOfTable(conn, "public", "elements"),
					methodNameToReport);
			
			
			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			for(int i=0;i<alOaam.size();i++){
				int elementId_test = alOaam.get(i).getRsColumn2Int().get("element_id");
				String accession_test = alOaam.get(i).getRsColumn2String().get("accession");
				long milliPrint1 = System.currentTimeMillis();
				int elementId_meth = QueriesTableElements.getElementIdWithAccession(
						conn,
						accession_test,
						true, true);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				if(elementId_test != elementId_meth){
					fail("Different results where found for accnum "+accession_test
							+ ": from test = " + elementId_test
							+ " ; from method = " + elementId_meth
							);
				}
//				else {
//					System.err.println("OK similar results where found for accnum "+associatedAccnumIT
//							+ ": int from test = " + primaryKeyIT
//							+ " and int from method = " + elementIdFromMeth);
//				}
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {

				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try
			
		
		assertTrue(true);
	}


	// comparator
	public class ByElementIdLightElementItemComparator implements Comparator<LightElementItem> {
		@Override
		public int compare(LightElementItem arg0, LightElementItem arg1) {
			return (arg0.getElementId() - arg1.getElementId());
		}
	}
	
}
