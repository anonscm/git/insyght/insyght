package fr.inra.jouy.server;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */

import static org.junit.Assert.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.junit.Test;
import fr.inra.jouy.server.objects.ObjAssociationAsMap;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.server.queriesTable.QueriesTableElements;
import fr.inra.jouy.server.queriesTable.QueriesTableGenes;
import fr.inra.jouy.server.queriesTable.QueriesTableMicadoQualifiers;
import fr.inra.jouy.shared.UtilitiesMethodsShared;

public class TestMethodsServer_tableQualifiers {

	
	@Test
	public void test_getGeneQualifierWithAccnumAndFeatIdAndTypeQual() throws Exception {
		
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		String methodNameToReport = "TestMethodsServer_tableQualifiers test_getGeneQualifierWithAccnumAndFeatIdAndTypeQual";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			//get x random tuples from table
			ArrayList<String> alTyeQual_OR = new ArrayList<String>();
			alTyeQual_OR.add("gene_name");
			alTyeQual_OR.add("gene");
			alTyeQual_OR.add("locus_tag");
			alTyeQual_OR.add("db_xref");
			alTyeQual_OR.add("EC_number");
			alTyeQual_OR.add("product");
			alTyeQual_OR.add("protein_id");
			alTyeQual_OR.add("codon_start");
			alTyeQual_OR.add("gene_synonym");
			ArrayList<ObjAssociationAsMap> approuvedAlOaam = getRandomGenesFromTableQualifiersWithTyeQual(
					conn,
					alTyeQual_OR,
					UtilitiesMethodsTest.numberOfRandomRepetitionByDefault,
					methodNameToReport);
			

			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			int j = 0;
			for(ObjAssociationAsMap oaamIT : approuvedAlOaam){
				String accessionFromTest = oaamIT.getRsColumn2String().get("accession");
				int codeFeatFromTest = oaamIT.getRsColumn2Int().get("code_feat");
				//int codeFeatFromTest = oaamIT.getRsColumn2Int().get("code_qual");
				String typeQualFromTest = oaamIT.getRsColumn2String().get("type_qual");
				//String qualifierFromTest = oaamIT.getRsColumn2String().get("qualifier");
				
				ArrayList<String> alFullTypeQualFromTest = new ArrayList<String>();
				alFullTypeQualFromTest.add(typeQualFromTest);
				//add random typeQual
				if(j == 1){
					alFullTypeQualFromTest.add("locus_tag");
				} else if (j == 2) {
					alFullTypeQualFromTest.add("db_xref");
				} else if (j == 3) {
					alFullTypeQualFromTest.add("product");
					alFullTypeQualFromTest.add("EC_number");
				} else if (j == 4) {
					alFullTypeQualFromTest.add("gene_name");
					alFullTypeQualFromTest.add("codon_start");
				}
				
				String commandTypeQualClause = "";
				boolean firstIter = true;
				for(String typeQualIT : alFullTypeQualFromTest){
					if(firstIter){
						commandTypeQualClause += "type_qual = '"+typeQualIT+"'";
						firstIter = false;
					} else {
						commandTypeQualClause += " OR type_qual = '"+typeQualIT+"'";
					}
				}


				String commandFullTest = "SELECT qualifier FROM micado.qualifiers WHERE accession = '" + accessionFromTest
						+"' AND code_feat = " + codeFeatFromTest
						+" AND (" + commandTypeQualClause + ")"
						;
				rs = statement.executeQuery(commandFullTest);
				ArrayList<String> alFullQualifierFromTest = new ArrayList<String>();
				while (rs.next()) {
					alFullQualifierFromTest.add(rs.getString("qualifier"));
				}

				
				long milliPrint1 = System.currentTimeMillis();
				ArrayList<String> alResFromMeth = QueriesTableMicadoQualifiers.getAlGeneQualifiers_withAccnumAndAlTypeQual_optionalFeatId(
						conn,
						accessionFromTest,
						codeFeatFromTest,
						alFullTypeQualFromTest,
						true,
						true
						);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+j+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				Collections.sort(alFullQualifierFromTest);
				Collections.sort(alResFromMeth);
				if(alFullQualifierFromTest.toString().compareTo(alResFromMeth.toString()) != 0){
					fail("Different results where found for accessionFromTest "+accessionFromTest
							+" and codeFeatFromTest " + codeFeatFromTest
							+" and typeQualFromTest " + alFullTypeQualFromTest.toString()
							+ ": from test = " + alFullQualifierFromTest.toString()
							+ " ; from method = " + alResFromMeth.toString()
							);
				}
//				else {
//					System.err.println("Ok similar results where found for accessionFromTest "+accessionFromTest
//							+" and codeFeatFromTest " + codeFeatFromTest
//							+" and typeQualFromTest " + alFullTypeQualFromTest.toString()
//							+ ": from test = " + alFullQualifierFromTest.toString()
//							+ " ; from method = " + alResFromMeth.toString());
//				}
				j++;
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				try {
					rs.close();
					rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				try {
					statement.close();
					statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try
			
		
		assertTrue(true);
			
	}


	@Test
	public void test_checkIfQualifierIsReallyAKnownAnnotation() throws Exception {

		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
		boolean closeConn = false;
		
		String methodNameToReport = "TestMethodsServer_tableQualifiers test_checkIfQualifierIsReallyAKnownAnnotation";
		LinkedHashMap<String, Integer> hmQualifierIsReallyAKnownAnnotation2counts = new LinkedHashMap<>();
		ArrayList<String> alLinesQualifierIsReallyAKnownAnnotation = new ArrayList<>();
		LinkedHashMap<String, Integer> hmQualifierIsNOTAKnownAnnotation2counts = new LinkedHashMap<>();
		ArrayList<String> alLlinesQualifierIsNOTAKnownAnnotation = new ArrayList<>();
		ArrayList<String> alMeaningfulFunctionalAnnotationTypeQualToMatch = new ArrayList<>();
		alMeaningfulFunctionalAnnotationTypeQualToMatch.add("product");
		alMeaningfulFunctionalAnnotationTypeQualToMatch.add("cellular_component");
		alMeaningfulFunctionalAnnotationTypeQualToMatch.add("biological_process");
		alMeaningfulFunctionalAnnotationTypeQualToMatch.add("function");
		alMeaningfulFunctionalAnnotationTypeQualToMatch.add("EC_number");
//		alMeaningfulFunctionalAnnotationTypeQualToMatch.add("standard_name");
//		alMeaningfulFunctionalAnnotationTypeQualToMatch.add("gene_synonym");
//		alMeaningfulFunctionalAnnotationTypeQualToMatch.add("gene");
//		alMeaningfulFunctionalAnnotationTypeQualToMatch.add("gene_name");
		
		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			//1. get random accnum from table organism
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsInt_aliase2columnName.put("organism_id", "organism_id");
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			//chmColumnSelectAsString_aliase2columnName.put("accession", "accession");
			ObjSQLCommand objSQLCmmdIT = new ObjSQLCommand(
					chmColumnSelectAsInt_aliase2columnName,
					chmColumnSelectAsString_aliase2columnName,
					null, null);
			ArrayList<ObjAssociationAsMap> alOaamOrganisms = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
					conn,
					"organism_id",
					"organisms",
					objSQLCmmdIT,
					//tableSize,
					methodNameToReport);
			
			for(ObjAssociationAsMap oaamOrganismIT : alOaamOrganisms){
				int organismIdIT = oaamOrganismIT.getRsColumn2Int().get("organism_id");
				HashSet<String> hsAccessions = QueriesTableElements.getHsAccessions_withOrgaId(conn, organismIdIT);
				
				for (String accessionIT : hsAccessions) {
					ArrayList<String> alQualifiers = QueriesTableMicadoQualifiers.getAlGeneQualifiers_withAccnumAndAlTypeQual_optionalFeatId(
							conn
							, accessionIT
							, -1
							, alMeaningfulFunctionalAnnotationTypeQualToMatch
							, false
							, true
							);
					
					for (String qualifierIT : alQualifiers) {
						if (QueriesTableMicadoQualifiers.checkIfQualifierIsReallyAKnownAnnotation(qualifierIT)) {
							if (hmQualifierIsReallyAKnownAnnotation2counts.containsKey(qualifierIT)) {
								int countIT = hmQualifierIsReallyAKnownAnnotation2counts.get(qualifierIT);
								countIT++;
								hmQualifierIsReallyAKnownAnnotation2counts.put(qualifierIT, countIT);
							} else {
								hmQualifierIsReallyAKnownAnnotation2counts.put(qualifierIT, 1);
							}
						} else {
							//hmQualifierIsNOTAKnownAnnotation2counts
							//linesQualifierIsNOTAKnownAnnotation.add(qualifierIT);
							if (hmQualifierIsNOTAKnownAnnotation2counts.containsKey(qualifierIT)) {
								int countIT = hmQualifierIsNOTAKnownAnnotation2counts.get(qualifierIT);
								countIT++;
								hmQualifierIsNOTAKnownAnnotation2counts.put(qualifierIT, countIT);
							} else {
								hmQualifierIsNOTAKnownAnnotation2counts.put(qualifierIT, 1);
							}
						}
					}
				}
			}
			
			LinkedHashMap<String, Integer> sortedHmQualifierIsReallyAKnownAnnotation2counts = hmQualifierIsReallyAKnownAnnotation2counts.entrySet().stream()
			.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
			.collect(Collectors.toMap(
	                Map.Entry::getKey, 
	                Map.Entry::getValue, 
	                (x,y)-> {throw new AssertionError();},
	                LinkedHashMap::new
	        ));
			LinkedHashMap<String, Integer> sortedHmQualifierIsNOTAKnownAnnotation2counts = hmQualifierIsNOTAKnownAnnotation2counts.entrySet().stream()
			.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
			.collect(Collectors.toMap(
	                Map.Entry::getKey, 
	                Map.Entry::getValue, 
	                (x,y)-> {throw new AssertionError();},
	                LinkedHashMap::new
	        ));
			
			for (Map.Entry<String, Integer> entry : sortedHmQualifierIsReallyAKnownAnnotation2counts.entrySet()) {
				alLinesQualifierIsReallyAKnownAnnotation.add(entry.getKey()+" ("+entry.getValue()+")");
			}
			//alLlinesQualifierIsNOTAKnownAnnotation
			for (Map.Entry<String, Integer> entry : sortedHmQualifierIsNOTAKnownAnnotation2counts.entrySet()) {
				alLlinesQualifierIsNOTAKnownAnnotation.add(entry.getKey()+" ("+entry.getValue()+")");
			}
			
			
			//print in file
			Path fileIsReallyAKnownAnnotation = Paths.get(System.getProperty("user.home")+"/test_qualifierIsReallyAKnownAnnotation.txt");
			Files.write(fileIsReallyAKnownAnnotation, alLinesQualifierIsReallyAKnownAnnotation, StandardCharsets.UTF_8,
			        Files.exists(fileIsReallyAKnownAnnotation) ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE
			        		);
			Path fileIsNOTAKnownAnnotation = Paths.get(System.getProperty("user.home")+"/test_qualifierIsNOTAKnownAnnotation.txt");
			Files.write(fileIsNOTAKnownAnnotation, alLlinesQualifierIsNOTAKnownAnnotation, StandardCharsets.UTF_8,
			        Files.exists(fileIsNOTAKnownAnnotation) ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE
			        		);

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		assertTrue(true);
		
	}

		
		
	@Test
	public void test_getAlAssocAccnumCodeFeatWithAlListAccnumAndTypeQual2QueryString_MatchEqual() throws Exception {

		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
		boolean closeConn = false;
		
		String methodNameToReport = "TestMethodsServer_tableQualifiers getAlAssocAccnumCodeFeatWithAlListAccnumAndTypeQual2QueryString_MatchEqual";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			//get x random tuples from table
			ArrayList<String> alTyeQual_OR = new ArrayList<String>();
			alTyeQual_OR.add("gene_name");
			alTyeQual_OR.add("gene");
			alTyeQual_OR.add("locus_tag");
			alTyeQual_OR.add("db_xref");
			alTyeQual_OR.add("EC_number");
			alTyeQual_OR.add("product");
			alTyeQual_OR.add("protein_id");
			alTyeQual_OR.add("codon_start");
			alTyeQual_OR.add("gene_synonym");
			ArrayList<ObjAssociationAsMap> approuvedAlOaam = getRandomGenesFromTableQualifiersWithTyeQual(
					conn,
					alTyeQual_OR,
					UtilitiesMethodsTest.numberOfRandomRepetitionByDefault,
					methodNameToReport
					);
			

			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			int j = 0;
			for(ObjAssociationAsMap oaamIT : approuvedAlOaam){
				//ArrayList<String>  listAccnumFromTest = oaamIT.getRsColumn2AlString().get("listAccnum");
				int organismIdFromTest = oaamIT.getRsColumn2Int().get("organism_id");
				String qualifierFromTest =  oaamIT.getRsColumn2String().get("qualifier");
				String typeQualFromTest = oaamIT.getRsColumn2String().get("type_qual");
				String accessionFromTest = oaamIT.getRsColumn2String().get("accession");
				int codeFeatFromTest = oaamIT.getRsColumn2Int().get("code_feat");
				
				HashSet<String> listAccnumFromTest = QueriesTableElements.getHsAccessions_withOrgaId(conn, organismIdFromTest);//.getAlAccnumWithOrgaId(conn, organismIdFromTest);
				
				long milliPrint1 = System.currentTimeMillis();
				ArrayList<ObjAssociationAsMap> typeQual2QueryString_OR = new ArrayList<ObjAssociationAsMap>();
				typeQual2QueryString_OR.add(new ObjAssociationAsMap(typeQualFromTest, qualifierFromTest));
				ArrayList<ObjAssociationAsMap> resFromMeth = QueriesTableMicadoQualifiers.getAlAssocAccnumCodeFeatWithAlListAccnumAndTypeQual2QueryString_MatchEqual(
						conn, 
						listAccnumFromTest,
						typeQual2QueryString_OR,
						true,
						true);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+j+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				boolean foundMatch = false;
				for(ObjAssociationAsMap resFromMethIT : resFromMeth){
					if(resFromMethIT.getRsColumn2String().get("accession").compareTo(accessionFromTest) == 0
							&& resFromMethIT.getRsColumn2Int().get("code_feat") == codeFeatFromTest ){
						foundMatch = true;
						break;
					}
				}
				if(!foundMatch){
					fail("Test result not found for listAccnum "+listAccnumFromTest.toString()
							+" and typeQual " + typeQualFromTest
							+" and qualifier " + qualifierFromTest
							+ ": accession = " + accessionFromTest
							+ " and codeFeat = " + codeFeatFromTest
							);
				}
//				else {
//					System.err.println("Ok Test result found found for listAccnum "+listAccnumFromTest.toString()
//							+" and typeQual " + typeQualFromTest
//							+" and qualifier " + qualifierFromTest
//							+ ": accession = " + accessionFromTest
//							+ " and codeFeat = " + codeFeatFromTest
//							);
//				}
				j++;
			}
			

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		assertTrue(true);
	}


	// return .getRsColumn2String().get("accession");
	// return .getRsColumn2Int().get("code_feat");
	// return .getRsColumn2Int().get("code_qual");
	// return .getRsColumn2String().get("type_qual");
	// return .getRsColumn2String().get("qualifier");
	// return .getRsColumn2Int().get("organism_id");
	private ArrayList<ObjAssociationAsMap> getRandomGenesFromTableQualifiersWithTyeQual(
			Connection conn, ArrayList<String> alTyeQual_OR,
			int numberOfRandomRepetitionWanted,
			String methodNameToReport) throws Exception {
		

		ArrayList<ObjAssociationAsMap> alToReturn = new ArrayList<ObjAssociationAsMap>();
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			int numberOfWhileIteration = 0;
			
			CONSTRUCT_approuvedAlOaam : while (alToReturn.size() < numberOfRandomRepetitionWanted){
				numberOfWhileIteration++;
				if(numberOfWhileIteration > 50){
					fail(methodNameToReport+" getRandomGenesWithTyeQual numberOfWhileIteration > 50");
				}

				//1. get random accnum from table elements
				ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
				chmColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
				ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
				chmColumnSelectAsString_aliase2columnName.put("accession", "accession");
				ObjSQLCommand objSQLCmmdIT = new ObjSQLCommand(
						chmColumnSelectAsInt_aliase2columnName,
						chmColumnSelectAsString_aliase2columnName,
						null, null);
				ArrayList<ObjAssociationAsMap> alOaamElements = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
						conn,
						"element_id",
						"elements",
						objSQLCmmdIT,
						//tableSize,
						methodNameToReport);
				
				for(ObjAssociationAsMap oaamElementIT : alOaamElements){
					String accessionIT = oaamElementIT.getRsColumn2String().get("accession");
					int elementIdIT = oaamElementIT.getRsColumn2Int().get("element_id");
					// select row from micado.qualifiers where accnum and alTyeQual_OR
					String command = "SELECT count(*) AS count"
							+ " FROM micado.qualifiers"
							+ " WHERE accession = '"+accessionIT+"'"
							+ " AND type_qual IN ("
							+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alTyeQual_OR, ",", "'")
							+ ")"
							;
					rs = statement.executeQuery(command);
					int countIT = -1;
					if (rs.next()) {
						countIT = rs.getInt("count");
					} else{
						continue;
					}
					command = "SELECT code_feat, code_qual, type_qual, qualifier"
							+ " FROM micado.qualifiers"
							+ " WHERE accession = '"+accessionIT+"'"
							+ " AND type_qual IN ("
							+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alTyeQual_OR, ",", "'")
							+ ")"
							+ " OFFSET floor(random()*"+countIT+") LIMIT 1;"
							;
					rs.close();
					rs = statement.executeQuery(command);
					int codeFeatIT = -1;
					int codeQualIT = -1;
					String typeQualIT = "";
					String qualifierIT = "";
					if (rs.next()) {
						codeFeatIT = rs.getInt("code_feat");
						codeQualIT = rs.getInt("code_qual");
						typeQualIT = rs.getString("type_qual");
						qualifierIT = rs.getString("qualifier");
					} else{
						continue;
					}
					
					//make sure what we get is in table genes (else can be chr or not cds),
					int organismIdIT = QueriesTableGenes.getOrganismIdWithElementIdAndCodeFeat(conn,
							elementIdIT,
							codeFeatIT,
							false);

					// if so, set field and add to approuvedAlOaam
					if(organismIdIT > 0){
						//ok
						// get list accnum with organismIdFromTest
						//ArrayList<String> listAccnumIT = QueriesTableElements.getListAccnumWithOrgaId(conn, organismIdFromTest);
						// make new obj with list accnum and geneName
						ObjAssociationAsMap approuvedOaam = new ObjAssociationAsMap();
						approuvedOaam.getRsColumn2String().put("accession", accessionIT);
						approuvedOaam.getRsColumn2Int().put("code_feat", codeFeatIT);
						approuvedOaam.getRsColumn2Int().put("code_qual", codeQualIT);
						approuvedOaam.getRsColumn2String().put("type_qual", typeQualIT);
						approuvedOaam.getRsColumn2String().put("qualifier", qualifierIT);
						approuvedOaam.getRsColumn2Int().put("organism_id", organismIdIT);
						//add to approuvedAlOaam ; if size approuvedAlOaam ok, break from loop
						alToReturn.add(approuvedOaam);
						if(alToReturn.size() >= numberOfRandomRepetitionWanted){
							break CONSTRUCT_approuvedAlOaam;
						}
					}
				}
				
			} // CONSTRUCT_approuvedAlOaam

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				try {
					rs.close();
					rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				try {
					statement.close();
					statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		
		return alToReturn;
		
	}
			

}
