package fr.inra.jouy.server;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;

import fr.inra.jouy.server.objects.ObjAssociationAsMap;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.server.queriesTable.QueriesTableElements;
import fr.inra.jouy.server.queriesTable.QueriesTableGenes;
import fr.inra.jouy.server.queriesTable.QueriesTableMicadoQualifiers;
import fr.inra.jouy.shared.pojos.databaseMapping.LightGeneItem;

public class TestMethodsServer_tableGenes {

	@Test
	public void test_getListGenesIdsWithAlOrganismIds() throws Exception {

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer_tableGenes test_getListGenesIdsWithAlOrganismIds";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			//get x random tuples from table
			ArrayList<Integer> alRandomOrgaIds = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
					"organism_id",
					"public.organisms",
					methodNameToReport);
			
			ArrayList<ArrayList<Integer>> alListGeneIds_fromTest = new ArrayList<>();
			for(int i = 0 ; i < alRandomOrgaIds.size() ; i++){
				int orgaIdIT = alRandomOrgaIds.get(i);
				long milliPrint1 = System.currentTimeMillis();
				ArrayList<Integer> listGeneIds = new ArrayList<>();
				String commandGetTuplesFromTable = "";
				if( i == 0 && alRandomOrgaIds.size()>= 2 ){
					commandGetTuplesFromTable = "Select gene_id from genes WHERE organism_id = " + orgaIdIT + " OR organism_id = "+alRandomOrgaIds.get(i+1);
				} else {
					commandGetTuplesFromTable = "Select gene_id from genes WHERE organism_id = " + orgaIdIT;
				}
					
				rs = statement.executeQuery(commandGetTuplesFromTable);
				while (rs.next()) {
					listGeneIds.add(rs.getInt("gene_id"));
				}
				alListGeneIds_fromTest.add(listGeneIds);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Test SQL query took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);
			}
			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			ArrayList<ArrayList<Integer>> alListGeneIds_fromMethod = new ArrayList<>();
			
			for(int i = 0 ; i < alRandomOrgaIds.size() ; i++){
				int orgaIdIT = alRandomOrgaIds.get(i);
				long milliPrint3 = System.currentTimeMillis();
				ArrayList<Integer> alOrgaIdToSend = new ArrayList<>();
				if( i == 0 && alRandomOrgaIds.size()>= 2 ){
					alOrgaIdToSend.add(orgaIdIT);
					alOrgaIdToSend.add(alRandomOrgaIds.get(i+1));
				} else {
					alOrgaIdToSend.add(orgaIdIT);
				}
				
				ArrayList<Integer> listGeneIds = QueriesTableGenes.getAlGenesIdsWithAlOrganismIds_optionalAlGeneIds(
						conn
						, alOrgaIdToSend
						, null
						, false
						);
				long milliPrint4 = System.currentTimeMillis() - milliPrint3;
				System.out.println("Method SQL query took\t" + milliPrint4 + "\tmilliseconds in test method "+methodNameToReport);
				alListGeneIds_fromMethod.add(listGeneIds);
			}

			for(ArrayList<Integer> listGeneIdsIT : alListGeneIds_fromTest){
				Collections.sort(listGeneIdsIT);
			}
			for(ArrayList<Integer> listGeneIdsIT : alListGeneIds_fromMethod){
				Collections.sort(listGeneIdsIT);
			}
			
			for(int i=0;i<alListGeneIds_fromTest.size();i++){
				ArrayList<Integer> listGeneIds_fromTest = alListGeneIds_fromTest.get(i);
				ArrayList<Integer> listGeneIds_fromMethod = alListGeneIds_fromMethod.get(i);
				if( listGeneIds_fromTest.toString().compareTo(listGeneIds_fromMethod.toString()) != 0 ){
					fail("Different results where found for list gene_id at "+i+" :"
							+ "\nfrom test = \n" + listGeneIds_fromTest.toString()
							+ "\nfrom method = \n" + listGeneIds_fromMethod.toString()
							);
				}
//				else {
//					System.err.println("Ok similar results where found for list gene_id at "+i+" :"
//							+ "\nfrom test = \n" + listGeneIds_fromTest.toString()
//							+ "\nfrom method = \n" + listGeneIds_fromMethod.toString()
//							);
//				}
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try
		assertTrue(true);
	}
	
	@Test
	public void test_getListGenesIdsWithElementId_optionalPbStartAndPbStop() throws Exception {

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer_tableGenes test_getListGenesIdsWithElementId_optionalPbStartAndPbStop";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			//get x random tuples from table
			ArrayList<Integer> alRandomEltId = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
					"element_id",
					"public.elements",
					methodNameToReport);
			// build the Al of listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent
			ArrayList<ArrayList<Integer>> AlListElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent = new ArrayList<>();
			for(int i=0;i<alRandomEltId.size();i++){
				int randomEltIdIT = alRandomEltId.get(i);
				ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent = new ArrayList<>();
				listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(randomEltIdIT);
				if(i == 0 && alRandomEltId.size() > 2){
					listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(10000);
					listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(Math.floorDiv(QueriesTableElements.getIntAttributeWithPrimaryId(
							conn,
							randomEltIdIT,
							"size",
							true,
							true,
							false
							), 2));
					listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(alRandomEltId.get(i+1));
					listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(null);
					listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(Math.floorDiv(QueriesTableElements.getIntAttributeWithPrimaryId(
							conn,
							alRandomEltId.get(i+1),
							"size",
							true,
							true,
							false
							), 4));

				} else if (i == 2){
					randomEltIdIT = alRandomEltId.get(i);
					listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(Math.floorDiv(QueriesTableElements.getIntAttributeWithPrimaryId(
							conn,
							randomEltIdIT,
							"size",
							true,
							true,
							false
							), 3));
					//listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(null);
				} else if (i == 3){
					randomEltIdIT = alRandomEltId.get(i);
					listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(null);
					listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(Math.floorDiv(QueriesTableElements.getIntAttributeWithPrimaryId(
							conn,
							randomEltIdIT,
							"size",
							true,
							true,
							false
							), 5));
				}
				AlListElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.add(listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent);
			}
			ArrayList<ArrayList<Integer>> alListGeneIds_fromTest = new ArrayList<>();
			for(int i=0;i<AlListElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.size();i++){
				ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent = AlListElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i);
				
				long milliPrint1 = System.currentTimeMillis();
				ArrayList<Integer> listGeneIds = new ArrayList<>();
				for (int j = 0; j < listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent
						.size(); j += 3) {
					String commandGetTuplesFromTable = "Select gene_id from genes WHERE element_id = " + listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(j);
					if(listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.size() > (j+1)
							&& listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(j+1) != null){
						commandGetTuplesFromTable += " AND start >= " + listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(j+1);// was stop >
					}
					if(listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.size() > (j+2)
							&& listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(j+2) != null){
						commandGetTuplesFromTable += " AND stop <= " + listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(j+2); // was start <
					}	
					
					rs = statement.executeQuery(commandGetTuplesFromTable);
					while (rs.next()) {
						listGeneIds.add(rs.getInt("gene_id"));
					}
				}
				alListGeneIds_fromTest.add(listGeneIds);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Test SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);
			}
			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			ArrayList<ArrayList<Integer>> alListGeneIds_fromMethod = new ArrayList<>();
			for(int i=0;i<AlListElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.size();i++){
				ArrayList<Integer> listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent = AlListElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent.get(i);
				long milliPrint3 = System.currentTimeMillis();
				ArrayList<Integer> listGeneIds = QueriesTableGenes.getAlGenesIdsWithElementId_optionalPbStartAndPbStop_optionalListGeneIdsToExclude(
						conn
						, listElementIdsThenOptionalStartPbthenOptionalStopPBLoopedSent
						, null
						, false // grabCDSWithAtLeastOnePbInThisStartStopLocus
						//, false // ifGrabOnlyCDSWith30ntOrMoreBetweenStartStopToSearch
						, -1 // ifGrabOnlyCDSWithXXntOrMoreBetweenStartStopToSearch
						, true // orderCDSByStartAsc
						, false // shouldBePresentInTable
						);
				long milliPrint4 = System.currentTimeMillis() - milliPrint3;
				System.out.println("Method SQL query "+i+" took\t" + milliPrint4 + "\tmilliseconds in test method "+methodNameToReport);
				alListGeneIds_fromMethod.add(listGeneIds);
			}
			
			for(ArrayList<Integer> listGeneIdsIT : alListGeneIds_fromTest){
				Collections.sort(listGeneIdsIT);
			}
			for(ArrayList<Integer> listGeneIdsIT : alListGeneIds_fromMethod){
				Collections.sort(listGeneIdsIT);
			}
			
			for(int i=0;i<alListGeneIds_fromTest.size();i++){
				ArrayList<Integer> listGeneIds_fromTest = alListGeneIds_fromTest.get(i);
				ArrayList<Integer> listGeneIds_fromMethod = alListGeneIds_fromMethod.get(i);
				if( listGeneIds_fromTest.toString().compareTo(listGeneIds_fromMethod.toString()) != 0 ){
					fail("Different results where found for list gene_id at "+i+" :"
							+ "\nfrom test = \n" + listGeneIds_fromTest.toString()
							+ "\nfrom method = \n" + listGeneIds_fromMethod.toString()
							);
				}
//				else {
//					System.err.println("Ok similar results where found for list gene_id at "+i+" :"
//							+ "\nfrom test = \n" + listGeneIds_fromTest.toString()
//							+ "\nfrom method = \n" + listGeneIds_fromMethod.toString()
//							);
//				}
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				try {
					rs.close();
					rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				try {
					statement.close();
					statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try
		assertTrue(true);
	}
	
	@Test
	public void test_getALLightGeneItemWithCollectionGeneIds() throws Exception {

		int sizeOfGeneListToTest = 200;
		
		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer_tableGenes test_getALLightGeneItemWithCollectionGeneIds";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			//get x random tuples from table
			long milliPrint1 = System.currentTimeMillis();
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
			chmColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
			chmColumnSelectAsInt_aliase2columnName.put("feature_id", "feature_id");
			chmColumnSelectAsInt_aliase2columnName.put("strand", "strand");
			chmColumnSelectAsInt_aliase2columnName.put("start", "start");
			chmColumnSelectAsInt_aliase2columnName.put("stop", "stop");
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsString_aliase2columnName.put("name", "name");
			ObjSQLCommand objSQLCmmdToDEfineSelectColumnIT = new ObjSQLCommand(
					chmColumnSelectAsInt_aliase2columnName,
					chmColumnSelectAsString_aliase2columnName,
					null, null);
			ArrayList<ObjAssociationAsMap> alOaam = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
					conn,
					"gene_id",
					"public.genes",
					objSQLCmmdToDEfineSelectColumnIT,
					sizeOfGeneListToTest,
					false,
					methodNameToReport);
			//complete each LightGeneItem with data from table qualifiers for locus tag and accnum
			
			ArrayList<LightGeneItem> alLGI_fromTest = new ArrayList<>();
			ArrayList<Integer> alGeneIds_fromTest = new ArrayList<>();
			
			for(ObjAssociationAsMap oaamIT : alOaam){
				int gene_id_FromTest = oaamIT.getRsColumn2Int().get("gene_id");
				alGeneIds_fromTest.add(gene_id_FromTest);
				int element_id_FromTest = oaamIT.getRsColumn2Int().get("element_id");
				int feature_id_FromTest = oaamIT.getRsColumn2Int().get("feature_id");
				int strand_FromTest = oaamIT.getRsColumn2Int().get("strand");
				int start_FromTest = oaamIT.getRsColumn2Int().get("start");
				int stop_FromTest = oaamIT.getRsColumn2Int().get("stop");
				String name_FromTest = oaamIT.getRsColumn2String().get("name");
				String accnum_FromTest = QueriesTableElements.getAccessionWithElementId(
						conn,
						element_id_FromTest,
						true);
				String nameQualifier_FromTest = QueriesTableMicadoQualifiers.getGeneNameWithAccnumAndFeatId_firstOnly(
						conn,
						accnum_FromTest,
						feature_id_FromTest,
						true,
						false);
				if( ! nameQualifier_FromTest.isEmpty()){
					name_FromTest = nameQualifier_FromTest;
				}
				
				String locusTag_FromTest = QueriesTableMicadoQualifiers.getLocusTagWithAccnumAndFeatId_firstOnly(
						conn,
						accnum_FromTest,
						feature_id_FromTest,
						true,
						false);
				LightGeneItem lgiIT = new LightGeneItem();
				//element
				lgiIT.setElementId(element_id_FromTest);
				lgiIT.setAccession(accnum_FromTest);
				//gene
				lgiIT.setGeneId(gene_id_FromTest);
				lgiIT.setName(name_FromTest);
				lgiIT.setLocusTag(locusTag_FromTest);
				lgiIT.setStart(start_FromTest);
				lgiIT.setStop(stop_FromTest);
				lgiIT.setStrand(strand_FromTest);
				alLGI_fromTest.add(lgiIT);
				
			}
			long milliPrint2 = System.currentTimeMillis() - milliPrint1;
			System.out.println("Method Test took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			long milliPrint3 = System.currentTimeMillis();
			ArrayList<LightGeneItem> alLGI_fromMethod = QueriesTableGenes.getAlLightGeneItemWithCollectionGeneIds_sortedByGeneId(
					conn, 
					alGeneIds_fromTest,
					true);
			long milliPrint4 = System.currentTimeMillis() - milliPrint3;
			System.out.println("Method SQL query took\t" + milliPrint4 + "\tmilliseconds in test method "+methodNameToReport);

			
			Collections.sort(alLGI_fromTest);
			StringBuilder sb_fromTest = new StringBuilder();
			for(LightGeneItem lgiIT : alLGI_fromTest){
				sb_fromTest.append(lgiIT.stringifyToJSON(true, false));
			}
			Collections.sort(alLGI_fromMethod);
			StringBuilder sb_fromMethod = new StringBuilder();
			for(LightGeneItem lgiIT : alLGI_fromMethod){
				sb_fromMethod.append(lgiIT.stringifyToJSON(true, false));
			}
			
			if( sb_fromTest.toString().compareTo(sb_fromMethod.toString()) != 0 ){
				fail("Different results where found for list gene_id :"
						+ "\nfrom test = \n" + sb_fromTest.toString()
						+ "\nfrom method = \n" + sb_fromMethod.toString()
						);
			}
//			else {
//				System.err.println("Ok similar results where found for list gene_id "
//						+ ": from test = " + sb_fromTest.toString()
//						+ " ; from method = " + sb_fromMethod.toString()
//						);
//			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		assertTrue(true);
	}
	
	@Test
	public void test_getProteinSequenceWithGeneId() throws Exception {

		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer_tableGenes test_getProteinSequenceWithGeneId";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			//get x random tuples from table
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
				//no column residues
			} else {
				chmColumnSelectAsString_aliase2columnName.put("residues", "residues");
			}
			
			ObjSQLCommand objSQLCmmdToDEfineSelectColumnIT = new ObjSQLCommand(
					chmColumnSelectAsInt_aliase2columnName,
					chmColumnSelectAsString_aliase2columnName,
					null, null);
			
			ArrayList<ObjAssociationAsMap> alOaam = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
					conn,
					"gene_id",
					"public.genes",
					objSQLCmmdToDEfineSelectColumnIT,
					//tableSize,
					methodNameToReport);

			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			int j = 0;
			for(ObjAssociationAsMap oaamIT : alOaam){
				int geneIdFromTest = oaamIT.getRsColumn2Int().get("gene_id");
				String residuesFromTest = "";
				if (UtilitiesMethodsServer.isDatabaseGenesElementsGenomeAssemblyConvenience(conn)) {
					Statement statement = null;
					ResultSet rs = null;
					try {
						if (conn == null) {
							conn = DatabaseConf.getConnection_db();
							closeConn = true;
						}
						statement = conn.createStatement();
						String commandGetTuplesFromTable = "SELECT micado.prot_feat.proteine AS residues"
								+ " FROM genes, micado.prot_feat"
								+ " WHERE genes.gene_id = "+geneIdFromTest
								+ " AND genes.accession = micado.prot_feat.accession"
								+ " AND genes.feature_id = micado.prot_feat.code_feat";
						
						rs = statement.executeQuery(commandGetTuplesFromTable);
						if (rs.next()) {
							residuesFromTest = rs.getString("residues");
						}
					} catch (Exception ex) {
						UtilitiesMethodsServer.reportException(methodNameToReport, ex);
					} finally {
							UtilitiesMethodsServer.closeRsAndStatement(rs, statement, methodNameToReport);
							if (closeConn) {
								DatabaseConf.freeConnection(conn, methodNameToReport);
							}
					}// try
					
				
				} else {
					residuesFromTest = oaamIT.getRsColumn2String().get("residues");
				}
				
				long milliPrint1 = System.currentTimeMillis();
				String resFromMeth = QueriesTableGenes.getProteinSequenceWithGeneId(
						conn,
						geneIdFromTest,
						true
						);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+j+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				//Collections.sort(alAccnumFromTest);
				//Collections.sort(alAccnumFromMeth);
				if( resFromMeth.compareTo(residuesFromTest) != 0 ){
					fail("Different results where found for gene_id "+ geneIdFromTest
							+ ": from test = " + residuesFromTest
							+ " ; from method = " + resFromMeth
							);
				}
//				else {
//					System.err.println("Ok similar results where found for gene_id "+ geneIdFromTest
//							+ ": from test residues = " + residuesFromTest
//							+ " ; from method residues = " + resFromMeth
//							);
//				}
				j++;
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {

				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		assertTrue(true);
		
	}
	
	
	
	@Test
	public void test_getElementIdAndFeatureIdWithGeneId() throws Exception {

		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer_tableGenes getElementIdAndFeatureIdWithGeneId";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			//get x random tuples from table
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
			chmColumnSelectAsInt_aliase2columnName.put("organism_id", "organism_id");
			chmColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
			chmColumnSelectAsInt_aliase2columnName.put("feature_id", "feature_id");
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			//empty
			ObjSQLCommand objSQLCmmdToDEfineSelectColumnIT = new ObjSQLCommand(
					chmColumnSelectAsInt_aliase2columnName,
					chmColumnSelectAsString_aliase2columnName,
					null, null);
			
			ArrayList<ObjAssociationAsMap> alOaam = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
					conn,
					"gene_id",
					"public.genes",
					objSQLCmmdToDEfineSelectColumnIT,
					methodNameToReport);
			
			

			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			int j = 0;
			for(ObjAssociationAsMap oaamIT : alOaam){
				int geneIdFromTest = oaamIT.getRsColumn2Int().get("gene_id");
				int elementIdFromTest = oaamIT.getRsColumn2Int().get("element_id");
				int featureIdFromTest = oaamIT.getRsColumn2Int().get("feature_id");
				
				long milliPrint1 = System.currentTimeMillis();
				ObjAssociationAsMap resFromMeth = QueriesTableGenes.getElementIdAndFeatureIdWithGeneId(
						conn,
						geneIdFromTest,
						true
						);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+j+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				//Collections.sort(alAccnumFromTest);
				//Collections.sort(alAccnumFromMeth);
				if( resFromMeth.getRsColumn2Int().get("element_id") != elementIdFromTest
						|| resFromMeth.getRsColumn2Int().get("feature_id") != featureIdFromTest ){
					fail("Different results where found for gene_id "+ geneIdFromTest
							+ ": from test elementIdFromTest = " + elementIdFromTest
							+ " and featureIdFromTest = " + featureIdFromTest
							+ " ; from method elementIdFromMethod = " + resFromMeth.getRsColumn2Int().get("element_id")
							+ " ; from method featureIdFromMethod = " + resFromMeth.getRsColumn2Int().get("feature_id")
							);
				}		
//				else {
//					System.err.println("Ok similar results where found for gene_id "+ geneIdFromTest
//					+ ": from test elementIdFromTest = " + elementIdFromTest
//					+ " and featureIdFromTest = " + featureIdFromTest
//					+ " ; from method elementIdFromMethod = " + resFromMeth.getRsColumn2Int().get("element_id")
//					+ " ; from method featureIdFromMethod = " + resFromMeth.getRsColumn2Int().get("feature_id"));
//				}
				j++;
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		assertTrue(true);
	}
	
				
				
				
	@Test
	public void test_getOrganismIdWithElementIdAndCodeFeat() throws Exception {

		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer_tableGenes test_getOrganismIdWithElementIdAndCodeFeat";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			//get x random tuples from table
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
			chmColumnSelectAsInt_aliase2columnName.put("organism_id", "organism_id");
			chmColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
			chmColumnSelectAsInt_aliase2columnName.put("feature_id", "feature_id");
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			//empty
			ObjSQLCommand objSQLCmmdToDEfineSelectColumnIT = new ObjSQLCommand(
					chmColumnSelectAsInt_aliase2columnName,
					chmColumnSelectAsString_aliase2columnName,
					null, null);
			
			ArrayList<ObjAssociationAsMap> alOaam = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
					conn,
					"gene_id",
					"public.genes",
					objSQLCmmdToDEfineSelectColumnIT,
					//tableSize,
					methodNameToReport);
			
			

			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			int j = 0;
			for(ObjAssociationAsMap oaamIT : alOaam){
				int resFromTest = oaamIT.getRsColumn2Int().get("organism_id");
				long milliPrint1 = System.currentTimeMillis();
				int resFromMeth = QueriesTableGenes.getOrganismIdWithElementIdAndCodeFeat(
						conn,
						oaamIT.getRsColumn2Int().get("element_id"),
						oaamIT.getRsColumn2Int().get("feature_id"),
						true
						);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+j+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				//Collections.sort(alAccnumFromTest);
				//Collections.sort(alAccnumFromMeth);
				if(resFromTest != resFromMeth){
					fail("Different results where found for element_id "+oaamIT.getRsColumn2Int().get("element_id")
							+" and feature_id " + oaamIT.getRsColumn2Int().get("feature_id")
							+ ": from test = " + resFromTest
							+ " ; from method = " + resFromMeth
							);
				}
//				else {
//					System.err.println("Ok similar results where found for element_id "+oaamIT.getRsColumn2Int().get("element_id")
//							+" and feature_id " + oaamIT.getRsColumn2Int().get("feature_id")
//							+ ": from test = " + resFromTest
//							+ " ; from method = " + resFromMeth);
//				}
				j++;
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		assertTrue(true);
	}
	
	
	@Test
	public void test_getGeneIdWithElementIdAndCodeFeat() throws Exception {

		Connection conn = null;
//		Statement statement = null;
//		ResultSet rs = null;
		boolean closeConn = false;
		
		String methodNameToReport = "TestMethodsServer_tableGenes test_getGeneIdWithElementIdAndCodeFeat";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			//statement = conn.createStatement();

			//get x random tuples from table
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			chmColumnSelectAsInt_aliase2columnName.put("gene_id", "gene_id");
			chmColumnSelectAsInt_aliase2columnName.put("element_id", "element_id");
			chmColumnSelectAsInt_aliase2columnName.put("feature_id", "feature_id");
			ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			//empty
			ObjSQLCommand objSQLCmmdToDEfineSelectColumnIT = new ObjSQLCommand(
					chmColumnSelectAsInt_aliase2columnName,
					chmColumnSelectAsString_aliase2columnName,
					null, null);
			
			ArrayList<ObjAssociationAsMap> alOaam = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
					conn,
					"gene_id",
					"public.genes",
					objSQLCmmdToDEfineSelectColumnIT,
					//tableSize,
					methodNameToReport);
			
			

			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			int j = 0;
			for(ObjAssociationAsMap oaamIT : alOaam){
				int resFromTest = oaamIT.getRsColumn2Int().get("gene_id");
				long milliPrint1 = System.currentTimeMillis();
				int resFromMeth = QueriesTableGenes.getGeneIdWithElementIdAndCodeFeat(
						conn,
						oaamIT.getRsColumn2Int().get("element_id"),
						oaamIT.getRsColumn2Int().get("feature_id"),
						true
						);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+j+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				//Collections.sort(alAccnumFromTest);
				//Collections.sort(alAccnumFromMeth);
				if(resFromTest != resFromMeth){
					fail("Different results where found for element_id "+oaamIT.getRsColumn2Int().get("element_id")
							+" and feature_id " + oaamIT.getRsColumn2Int().get("feature_id")
							+ ": from test = " + resFromTest
							+ " ; from method = " + resFromMeth
							);
				}
//				else {
//					System.err.println("Ok similar results where found for element_id "+oaamIT.getRsColumn2Int().get("element_id")
//							+" and feature_id " + oaamIT.getRsColumn2Int().get("feature_id")
//							+ ": from test = " + resFromTest
//							+ " ; from method = " + resFromMeth);
//				}
				j++;
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		assertTrue(true);
		
	}
	
	@Test
	public void test_getGeneIdWithGeneNameAndOrganismId_checkTableGeneOnly_firstMatchOnly() throws Exception {

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		
		String methodNameToReport = "TestMethodsServer_tableGenes test_getGeneIdWithGeneNameAndOrganismId_checkTableGeneOnly_firstMatchOnly";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			//get x random primaryKey from other table of interest
			ArrayList<Integer> alRandomInt = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
					"organism_id",
					"public.organisms",
					//tableSize,
					methodNameToReport);

			//get x random tuples from table
			ArrayList<ObjAssociationAsMap> alFromTest = new ArrayList<ObjAssociationAsMap>();
			for(int i=0; i < alRandomInt.size() ; i++){
				int primaryKeyIT = alRandomInt.get(i);
				String commandGetTuplesFromTable = "SELECT gene_id, name"
						+ " FROM genes"
						+ " WHERE organism_id = "+primaryKeyIT
						+ " AND name IS NOT NULL AND name != ''";
				long milliPrint1 = System.currentTimeMillis();
				rs = statement.executeQuery(commandGetTuplesFromTable);
				if (rs.next()) {
					ObjAssociationAsMap oaam = new ObjAssociationAsMap();
					oaam.getRsColumn2Int().put("gene_id", rs.getInt("gene_id"));
					oaam.getRsColumn2Int().put("organism_id", primaryKeyIT);
					oaam.getRsColumn2String().put("name", rs.getString("name"));
					alFromTest.add(oaam);
				}
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Test SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);
			}

			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			int j = 0;
			for(ObjAssociationAsMap oaamIT : alFromTest){
				int resFromTest = oaamIT.getRsColumn2Int().get("gene_id");
				long milliPrint1 = System.currentTimeMillis();
				int resFromMeth = QueriesTableGenes.getGeneIdWithGeneNameAndOrganismId_checkTableGeneOnly_firstMatchOnly(
						conn,
						oaamIT.getRsColumn2String().get("name"),
						oaamIT.getRsColumn2Int().get("organism_id"),
						false, false
						);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+j+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				//Collections.sort(alAccnumFromTest);
				//Collections.sort(alAccnumFromMeth);
				if(resFromTest != resFromMeth){
					fail("Different results where found for OrgaId "+oaamIT.getRsColumn2Int().get("organism_id")
							+" and name " + oaamIT.getRsColumn2String().get("name")
							+ ": from test = " + resFromTest
							+ " ; from method = " + resFromMeth
							);
				}
//				else {
//					System.err.println("Ok similar results where found for OrgaId "+oaamIT.getRsColumn2Int().get("organism_id")
//							+" and name " + oaamIT.getRsColumn2String().get("name")
//							+ ": from test = " + resFromTest
//							+ " but from method = " + resFromMeth);
//				}
				j++;
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				try {
					rs.close();
					rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				try {
					statement.close();
					statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

		assertTrue(true);
		
	}

}
