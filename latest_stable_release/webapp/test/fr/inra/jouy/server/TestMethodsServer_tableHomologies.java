package fr.inra.jouy.server;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */



import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;

import org.junit.Test;

import fr.inra.jouy.server.queriesTable.QueriesTableHomologs;
import fr.inra.jouy.shared.UtilitiesMethodsShared;

public class TestMethodsServer_tableHomologies {

	
	
	@Test
	public void test_getHmqGeneId2HsGeneIdsHomologsWithQOrgaId_optionalListQGeneIdsOrSorgaIdToRestrictSearch() throws Exception {


		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer_tableHomologies test_getHmqGeneId2HsGeneIdsHomologsWithQOrgaId_optionalListQGeneIdsOrSorgaIdToRestrictSearch";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			//get x random primaryKey from other table
			ArrayList<Integer> alRandomOrgaId = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
					"organism_id",
					"public.organisms",
					methodNameToReport);
			ArrayList<ArrayList<Integer>> alAlRandomGeneId = new ArrayList<ArrayList<Integer>>();
			ArrayList<ArrayList<Integer>> alAlRandomSOrganismId = new ArrayList<ArrayList<Integer>>();
			//get all tuples from table
			ArrayList<HashMap<Integer, HashSet<Integer>>> alFromTest = new ArrayList<HashMap<Integer,HashSet<Integer>>>();
			for(int i=0; i < alRandomOrgaId.size() ; i++){
				int primaryKeyIT = alRandomOrgaId.get(i);
				String commandGetTuplesTest = "";
				
				if(i >= 2){
					commandGetTuplesTest = "SELECT q_gene_id, s_gene_id"
								+ " FROM homologies"
								+ " WHERE q_organism_id = "+primaryKeyIT;
					if (i == 2) {
						commandGetTuplesTest += " AND s_organism_id != "+primaryKeyIT;
					} else if (i == 3) {
						ArrayList<Integer> alRandomSOrgaId = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
								"organism_id",
								"public.organisms",
								methodNameToReport);
						alAlRandomSOrganismId.add(alRandomSOrgaId);
						commandGetTuplesTest += " AND s_organism_id IN ("
								+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alRandomSOrgaId)
								+ ")"
								;
						
					}
				} else {
					ArrayList<Integer> alRandomGeneId = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
							"gene_id",
							"public.genes",
							methodNameToReport);
					alAlRandomGeneId.add(alRandomGeneId);
					commandGetTuplesTest = "SELECT q_gene_id, s_gene_id"
							+ " FROM homologies"
							+ " WHERE q_gene_id IN ("+UtilitiesMethodsShared.getItemsAsStringFromCollection(alRandomGeneId)+")";
				}

				long milliPrint1 = System.currentTimeMillis();
				rs = statement.executeQuery(commandGetTuplesTest);
				HashMap<Integer, HashSet<Integer>> hmqGeneId2HsSOrgaIdIT = new HashMap<Integer, HashSet<Integer>>();
				while (rs.next()) {
					int qGeneId = rs.getInt("q_gene_id");
					int sGeneId = rs.getInt("s_gene_id");
					if( hmqGeneId2HsSOrgaIdIT.containsKey(qGeneId) ){
						HashSet<Integer> existingHs = hmqGeneId2HsSOrgaIdIT.get(qGeneId);
						existingHs.add(sGeneId);
					} else {
						HashSet<Integer> newHs = new HashSet<Integer>();
						newHs.add(sGeneId);
						hmqGeneId2HsSOrgaIdIT.put(qGeneId, newHs);
					}
				}
				alFromTest.add(hmqGeneId2HsSOrgaIdIT);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Test SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);
			}

			//test if we get similar answer with method LowLevelQueriesTables
			for(int i=0;i<alRandomOrgaId.size();i++){
				int primaryKeyIT = alRandomOrgaId.get(i);
				HashMap<Integer, HashSet<Integer>> hmFromTest = alFromTest.get(i);
				long milliPrint1 = System.currentTimeMillis();
				HashMap<Integer, HashSet<Integer>> hmFromMeth;
				if(i >= 2){
					if(i == 2){
						hmFromMeth = QueriesTableHomologs.getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId(
								conn,
								primaryKeyIT,
								null,
								null,
								true
								, false
								);
					} else if (i == 3) {
						hmFromMeth = QueriesTableHomologs.getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId(
								conn,
								primaryKeyIT,
								null,
								alAlRandomSOrganismId.get(0),
								false
								, false
								);
					} else {
						hmFromMeth = QueriesTableHomologs.getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId(
								conn,
								primaryKeyIT,
								null,
								null,
								false
								, false
								);
					}
					
				} else {
					ArrayList<Integer> alRandomGeneId = alAlRandomGeneId.get(i);
					hmFromMeth = QueriesTableHomologs.getHmqGeneId2HsGeneIdsHomologsWithAlQGeneIdsOrQOrgaId_optionalRestrictAlSorgaId(
							conn,
							primaryKeyIT,
							alRandomGeneId,
							null,
							false
							, false
							);
				}

				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				// sortHashMapBy Key
				LinkedHashMap<Integer, HashSet<Integer>> sortedHmFromTest = UtilitiesMethodsShared
						.sortMapByKeys(
							hmFromTest,
							//null, // threshold
							true // descending
							);
				LinkedHashMap<Integer, HashSet<Integer>> sortedHmFromMeth = UtilitiesMethodsShared
						.sortMapByKeys(
							hmFromMeth,
							//null, // threshold
							true // descending
							);
				
				// for each key, sort HSet
				LinkedHashMap<Integer, ArrayList<Integer>> sortedSortedHmFromTest = UtilitiesMethodsShared
						.sortHashSetValueInMap(
							sortedHmFromTest,
							true // descending
							);
				
				// for each key, sort HSet
				LinkedHashMap<Integer, ArrayList<Integer>> sortedSortedHmFromMeth = UtilitiesMethodsShared
						.sortHashSetValueInMap(
							sortedHmFromMeth,
							true // descending
							);
				
				// compare GenericHashMap2JSONString
				if( UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap(
						"sortedSortedHm",
						sortedSortedHmFromTest,
						true,
						true,true)
						.compareTo( 
								UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap(
										"sortedSortedHm",
										sortedSortedHmFromMeth,
										true,
										true,true)
								) != 0 ){
					fail("Different results where found for OrgaId "+primaryKeyIT
							+ ": from test = " + UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap("sortedSortedHm",
									sortedSortedHmFromTest,
									true,
									true,true)
							+ " ; from method = " + UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap("sortedSortedHm",
									sortedSortedHmFromMeth,
									true,
									true,true)
							);
				}
				
//				else {
//					System.err.println("OK similar results where found for OrgaId "+primaryKeyIT
//							+ ": from test = " + UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap("sortedSortedHm",
//																	sortedSortedHmFromTest,
//																	true,true,true).substring(0, 10)
//							+ " ; from method = " + UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap("sortedSortedHm",
//																	sortedSortedHmFromMeth,
//																	true,true,true).substring(0, 10)
//							);
//				}
				
				
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}
		assertTrue(true);
	}
	
	
	@Test
	public void test_getHmqGeneId2HsSOrgaIdFromTableHomologsWithQOrgaId_optionalListQGeneIdsToRestrictSearch() throws Exception {
		

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer_tableHomologies test_getHmqGeneId2HsSOrgaIdFromTableHomologsWithQOrgaId_optionalListQGeneIdsToRestrictSearch";

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			//get x random primaryKey from other table
			ArrayList<Integer> alRandomOrgaId = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
					"organism_id",
					"public.organisms",
					methodNameToReport);
			ArrayList<ArrayList<Integer>> alAlRandomGeneId = new ArrayList<ArrayList<Integer>>();
			ArrayList<ArrayList<Integer>> alAlRandomSOrganismId = new ArrayList<ArrayList<Integer>>();
			//get all tuples from table
			ArrayList<HashMap<Integer, HashSet<Integer>>> alFromTest = new ArrayList<HashMap<Integer,HashSet<Integer>>>();
			for(int i=0; i < alRandomOrgaId.size() ; i++){
				int primaryKeyIT = alRandomOrgaId.get(i);
				String commandGetTuplesTest = "";
				
				if(i >= 2){
					commandGetTuplesTest = "SELECT q_gene_id, s_organism_id"
								+ " FROM homologies"
								+ " WHERE q_organism_id = "+primaryKeyIT;
					if (i == 2) {
						commandGetTuplesTest += " AND s_organism_id != "+primaryKeyIT;
					} else if (i == 3) {
						ArrayList<Integer> alRandomSOrgaId = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
								"organism_id",
								"public.organisms",
								methodNameToReport);
						alAlRandomSOrganismId.add(alRandomSOrgaId);
						commandGetTuplesTest += " AND s_organism_id IN ("
								+ UtilitiesMethodsShared.getItemsAsStringFromCollection(alRandomSOrgaId)
								+ ")"
								;
						
					}
				} else {
					ArrayList<Integer> alRandomGeneId = UtilitiesMethodsTest.getAlRandomPrimaryKey(conn, 
							"gene_id",
							"public.genes",
							methodNameToReport);
					alAlRandomGeneId.add(alRandomGeneId);
					commandGetTuplesTest = "SELECT q_gene_id, s_organism_id"
							+ " FROM homologies"
							+ " WHERE q_gene_id IN ("+UtilitiesMethodsShared.getItemsAsStringFromCollection(alRandomGeneId)+")";
				}

				long milliPrint1 = System.currentTimeMillis();
				rs = statement.executeQuery(commandGetTuplesTest);
				HashMap<Integer, HashSet<Integer>> hmqGeneId2HsSOrgaIdIT = new HashMap<Integer, HashSet<Integer>>();
				while (rs.next()) {
					int qGeneId = rs.getInt("q_gene_id");
					int sOrganismId = rs.getInt("s_organism_id");
					if( hmqGeneId2HsSOrgaIdIT.containsKey(qGeneId) ){
						HashSet<Integer> existingHs = hmqGeneId2HsSOrgaIdIT.get(qGeneId);
						existingHs.add(sOrganismId);
					} else {
						HashSet<Integer> newHs = new HashSet<Integer>();
						newHs.add(sOrganismId);
						hmqGeneId2HsSOrgaIdIT.put(qGeneId, newHs);
					}
				}
				alFromTest.add(hmqGeneId2HsSOrgaIdIT);
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Test SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);
			}
			
			//test if we get similar answer with method LowLevelQueriesTables
			for(int i=0;i<alRandomOrgaId.size();i++){
				int primaryKeyIT = alRandomOrgaId.get(i);
				HashMap<Integer, HashSet<Integer>> hmFromTest = alFromTest.get(i);
				long milliPrint1 = System.currentTimeMillis();
				HashMap<Integer, HashSet<Integer>> hmFromMeth;
				if(i >= 2){
					if(i == 2){
						hmFromMeth = QueriesTableHomologs.getHmqGeneId2HsSOrgaIdWithAlQGeneIdsOrQOrgaId_optionalRestrictSorgaId(
								conn,
								primaryKeyIT,
								null,
								null,
								true
								, false
								);
					} else if (i == 3) {
						hmFromMeth = QueriesTableHomologs.getHmqGeneId2HsSOrgaIdWithAlQGeneIdsOrQOrgaId_optionalRestrictSorgaId(
								conn,
								primaryKeyIT,
								null,
								alAlRandomSOrganismId.get(0),
								false
								, false
								);
					} else {
						hmFromMeth = QueriesTableHomologs.getHmqGeneId2HsSOrgaIdWithAlQGeneIdsOrQOrgaId_optionalRestrictSorgaId(
								conn,
								primaryKeyIT,
								null,
								null,
								false
								, false
								);
					}
					
				} else {
					ArrayList<Integer> alRandomGeneId = alAlRandomGeneId.get(i);
					hmFromMeth = QueriesTableHomologs.getHmqGeneId2HsSOrgaIdWithAlQGeneIdsOrQOrgaId_optionalRestrictSorgaId(
							conn,
							primaryKeyIT,
							alRandomGeneId,
							null,
							false
							, false
							);
				}

				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds in test method "+methodNameToReport);

				// sortHashMapBy Key
				LinkedHashMap<Integer, HashSet<Integer>> sortedHmFromTest = UtilitiesMethodsShared
						.sortMapByKeys(
							hmFromTest,
							//null, // threshold
							true // descending
							);
				LinkedHashMap<Integer, HashSet<Integer>> sortedHmFromMeth = UtilitiesMethodsShared
						.sortMapByKeys(
							hmFromMeth,
							//null, // threshold
							true // descending
							);
				
				// for each key, sort HSet
				LinkedHashMap<Integer, ArrayList<Integer>> sortedSortedHmFromTest = UtilitiesMethodsShared
						.sortHashSetValueInMap(
							sortedHmFromTest,
							true // descending
							);
				
				// for each key, sort HSet
				LinkedHashMap<Integer, ArrayList<Integer>> sortedSortedHmFromMeth = UtilitiesMethodsShared
						.sortHashSetValueInMap(
							sortedHmFromMeth,
							true // descending
							);
				
				// compare GenericHashMap2JSONString
				if( UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap(
						"sortedSortedHm",
						sortedSortedHmFromTest,
						true,
						true,true)
						.compareTo( 
								UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap(
										"sortedSortedHm",
										sortedSortedHmFromMeth,
										true,
										true,true)
								) != 0 ){
					fail("Different results where found for OrgaId "+primaryKeyIT
							+ ": from test = " + UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap("sortedSortedHm",
									sortedSortedHmFromTest,
									true,
									true,true)
							+ " ; from method = " + UtilitiesMethodsShared.convertObjectAttribute2JSONString_stringGenericMap("sortedSortedHm",
									sortedSortedHmFromMeth,
									true,
									true,true)
							);
				}
//				else {
//					System.err.println("OK similar results where found for OrgaId "+primaryKeyIT
//							+ ": from test = " + UtilitiesMethodsServer.Map2JSON("sortedSortedHm",
//																	sortedSortedHmFromTest,
//																	true,true,true).substring(0, 10)
//							+ " ; from method = " + UtilitiesMethodsServer.Map2JSON("sortedSortedHm",
//																	sortedSortedHmFromMeth,
//																	true,true,true).substring(0, 10)
//							);
//				}
				
				
			}

		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			try {
				rs.close();
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			try {
				statement.close();
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}
		assertTrue(true);
	}

}
