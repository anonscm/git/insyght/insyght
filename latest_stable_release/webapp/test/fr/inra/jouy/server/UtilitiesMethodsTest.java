package fr.inra.jouy.server;

/*
 Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import fr.inra.jouy.server.objects.ObjAssociationAsMap;
import fr.inra.jouy.server.objects.ObjSQLCommand;

public class UtilitiesMethodsTest {
	
	public static int numberOfRandomRepetitionByDefault = 5;
	//private methods
	
	public static Long getApproximateSizeOfTable(Connection conn,
			//String schema,
			String schema_table
			) throws Exception {
		
		Long lgToReturn = new Long(-1);
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		String methodNameToReport = "TestMethodsServer getApproximateSizeOfTable";
		
		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();
			
			String commandGetApproximateSizeOfTable = "SELECT reltuples AS ct FROM pg_class WHERE oid = '"+schema_table+"'::regclass";
			rs = statement.executeQuery(commandGetApproximateSizeOfTable);
			if (rs.next()) {
				lgToReturn = rs.getLong("ct");
				if (rs.next()) {
					UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("Error, more than 1 row returned for command "+commandGetApproximateSizeOfTable));
				}
			}else{
				UtilitiesMethodsServer.reportException(methodNameToReport, new Exception("Error, no row returned for command "+commandGetApproximateSizeOfTable));
			}
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				try {
					rs.close();
					rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				try {
					statement.close();
					statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try
		return lgToReturn;
	}

	public static ArrayList<Integer> getAlRandomPrimaryKey(
			Connection conn,
			String colmunPrimaryKey,
			String schema_table,
			String methodNameToReport
			) throws Exception {
		
		return getAlRandomPrimaryKey(
				conn,
				colmunPrimaryKey,
				schema_table,
				numberOfRandomRepetitionByDefault,
				 methodNameToReport
				);
	}
	
	public static ArrayList<Integer> getAlRandomPrimaryKey(
			Connection conn,
			String colmunPrimaryKey,
			String schema_table,
			int numberOfRandomRepetitionWanted,
			String methodNameToReport
			) throws Exception {

		
		ArrayList<Integer> alToReturn = new ArrayList<Integer>();
		
		ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
		chmColumnSelectAsInt_aliase2columnName.put(colmunPrimaryKey, colmunPrimaryKey);
		
		ObjSQLCommand objSQLCmmdIT = new ObjSQLCommand(
				chmColumnSelectAsInt_aliase2columnName, 
				new ConcurrentHashMap<String, String>(), 
				null, null);
		
		ArrayList<ObjAssociationAsMap> alItermediate = getAlRandomRowsFromTableWithColmunPrimaryKey(conn,
				colmunPrimaryKey,
				schema_table,
				objSQLCmmdIT,
				numberOfRandomRepetitionWanted,
				true,
				methodNameToReport);
		
		for (ObjAssociationAsMap oaomIT : alItermediate){
			alToReturn.add(oaomIT.getRsColumn2Int().get(colmunPrimaryKey));
		}
		
		return alToReturn;
	}

	public static ArrayList<ObjAssociationAsMap> getAlRandomRowsFromTableWithColmunPrimaryKey(
			Connection conn,
			String colmunPrimaryKey,
			String schema_table,
			ObjSQLCommand objSQLCmmdIT, //only for ChmColumnSelectAs...
			String methodNameToReport
			) throws Exception {
		return getAlRandomRowsFromTableWithColmunPrimaryKey(
				conn,
				colmunPrimaryKey,
				schema_table,
				objSQLCmmdIT, //only for ChmColumnSelectAs...
				numberOfRandomRepetitionByDefault,
				true,
				methodNameToReport);
	}
	
	public static ArrayList<ObjAssociationAsMap> getAlRandomRowsFromTableWithColmunPrimaryKey(
			Connection conn,
			String colmunPrimaryKey,
			String schema_table,
			ObjSQLCommand objSQLCmmdIT, //only for ChmColumnSelectAs...
			int numberOfRandomRepetitionWanted,
			boolean shouldBeNotEmptyOrZero,
			String methodNameToReport
			) throws Exception {

		
		ArrayList<ObjAssociationAsMap> alToReturn = new ArrayList<ObjAssociationAsMap>();
		
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;

		try {
		
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			
			statement = conn.createStatement();
		
			//int numberOfRandomRepetitionPlusFewPercent = numberOfRandomRepetition*2;
			int numberOfRandomRepetitionPlusFewPercent = (int) Math.round(numberOfRandomRepetitionWanted+(numberOfRandomRepetitionWanted*0.5));
			Long tableSize = getApproximateSizeOfTable(conn, schema_table);
			//from http://stackoverflow.com/questions/8674718/best-way-to-select-random-rows-postgresql
			String command = "WITH RECURSIVE random_pick AS ("
							   + " SELECT "+objSQLCmmdIT.getAllColumnsToSelectInCommand()
							   + " FROM  ("
							      + " SELECT 1 + trunc(random() * "+tableSize+")::int AS "+colmunPrimaryKey
							      + " FROM   generate_series(1, "+numberOfRandomRepetitionPlusFewPercent+")" //  -- numberOfRandomRepetition + few percent - adapt to your needs
							      + " LIMIT  "+numberOfRandomRepetitionPlusFewPercent //                      -- hint for query planner"
							      + " ) r"
							   + " JOIN "+schema_table+" b USING ("+colmunPrimaryKey+")" //             -- eliminate miss
							   + " UNION" //                               -- eliminate dupe
							   + " SELECT "+objSQLCmmdIT.getAllColumnsToSelectInCommand("b.")
							   + " FROM  ("
							      + " SELECT 1 + trunc(random() * "+tableSize+")::int AS "+colmunPrimaryKey
							      + " FROM   random_pick r" //             -- plus 3 percent - adapt to your needs
							      + " LIMIT  "+numberOfRandomRepetitionPlusFewPercent //                       -- less than 1000, hint for query planner
							      + " ) r"
							   + " JOIN "+schema_table+" b USING ("+colmunPrimaryKey+")"//             -- eliminate miss
							   + " )"
							+ " SELECT "+objSQLCmmdIT.getAllColumnsToSelectInCommand()
							+ " FROM   random_pick"
							+ " LIMIT  "+numberOfRandomRepetitionWanted ;//  -- actual limit
			
			rs = statement.executeQuery(command);
			
			alToReturn = objSQLCmmdIT.parseRsIntoClassExtendsAbstractFieldsMapper(
					ObjAssociationAsMap.class,
					rs, shouldBeNotEmptyOrZero, methodNameToReport
					);

			
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport, ex);
		} finally {
				try {
					rs.close();
					rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				try {
					statement.close();
					statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
				} catch (Exception ex) {
					UtilitiesMethodsServer.reportException(methodNameToReport, ex);
				}
				if (closeConn) {
					DatabaseConf.freeConnection(conn, methodNameToReport);
				}
		}// try

	    return alToReturn;
		
	}


}
