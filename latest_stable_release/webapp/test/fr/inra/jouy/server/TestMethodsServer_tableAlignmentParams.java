package fr.inra.jouy.server;

/*
  Insyght Copyright - INRA
 IDDN.FR.001.060011.000.R.C.2013.000.42000
 Auteurs: Thomas Lacroix (thomas.lacroix@jouy.inra.fr)

 This file is part of Insyght

 Insyght is a web application whose purpose is to explore the landscape of conserved and idiosyncratic genomic regions across multiple genomes and their rearrangements throughout evolution. Its unique display is based on the association of symbolic and proportional views. The basic idea is that the user can browse and interact with a variety of symbols that constitute the chain of annotation events: homologs, conserved syntenies, genomic regions insertions / deletions, etc... The symbols are tightly integrated with a display representing the same annotation events drawn proportionally according to their genomic positions and joined up by trapezoids if they are homologous. The symbols highlight a region of interest and provide legibility while the proportional display simultaneously allows grasping genomic locations and complex rearrangements scattered across the genomes and occurring at different scales.

 This software is governed by the CeCILL-B license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL-B
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-B license and that you accept its terms.
 */


import static org.junit.Assert.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import org.junit.Test;
import fr.inra.jouy.server.objects.ObjAssociationAsMap;
import fr.inra.jouy.server.objects.ObjSQLCommand;
import fr.inra.jouy.server.queriesTable.QueriesTableAlignmentParams;


public class TestMethodsServer_tableAlignmentParams {

	@Test
	public void test_getOrigamiAlignmentParamIdWithOrigamiQandSElementIdAndParamForSynteny() throws Exception {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		boolean closeConn = false;
		String methodNameToReport = "TestMethodsServer_tableAlignmentParams test_getOrigamiAlignmentParamIdWithOrigamiQandSElementIdAndParamForSyntenya";
		

		try {
			if (conn == null) {
				conn = DatabaseConf.getConnection_db();
				closeConn = true;
			}
			statement = conn.createStatement();

			//get x random tuples from table
			ConcurrentHashMap<String, String> chmColumnSelectAsInt_aliase2columnName = new ConcurrentHashMap<String, String>();
			//chmColumnSelectAsInt_aliase2columnName.put("alignment_param_id", "alignment_param_id");
			chmColumnSelectAsInt_aliase2columnName.put("q_element_id", "q_element_id");
			chmColumnSelectAsInt_aliase2columnName.put("s_element_id", "s_element_id");
			//ConcurrentHashMap<String, String> chmColumnSelectAsString_aliase2columnName = new ConcurrentHashMap<String, String>();
			//chmColumnSelectAsString_aliase2columnName.put("accession", "accession");
			ConcurrentHashMap<String, String> chm_alColumnSelectAsLong_aliase2columnName = new ConcurrentHashMap<String, String>();
			chm_alColumnSelectAsLong_aliase2columnName.put("alignment_param_id", "alignment_param_id");
			ObjSQLCommand objSQLCmmdIT = new ObjSQLCommand(
					chmColumnSelectAsInt_aliase2columnName
					, null
					, null
					, chm_alColumnSelectAsLong_aliase2columnName
					, null
					, null
					, null
					, null);
			
			ArrayList<ObjAssociationAsMap> alOaam = UtilitiesMethodsTest.getAlRandomRowsFromTableWithColmunPrimaryKey(
					conn,
					"alignment_param_id",
					"public.alignment_params",
					objSQLCmmdIT,
					methodNameToReport);
			
			//for each random tuple, test if we get similar answer with method LowLevelQueriesTables
			for(int i=0;i<alOaam.size();i++){
				Long alignment_param_id_test = alOaam.get(i).getRsColumn2Long().get("alignment_param_id");
				int q_element_id_test = alOaam.get(i).getRsColumn2Int().get("q_element_id");
				int s_element_id_test = alOaam.get(i).getRsColumn2Int().get("s_element_id");
				long milliPrint1 = System.currentTimeMillis();
				
				// reverse some cases for isDatabaseNoMirror
				if (UtilitiesMethodsServer.isDatabaseNoMirror(conn) && ( i == 1 | i == 3 ) ) {
					//System.err.println("Reversing q-s element_id to test isDatabaseNoMirror ; q_element_id_test is now =" +s_element_id_test
					//			+ " and s_element_id_test is now = " +  q_element_id_test
					//			);
					int tmp_q_element_id_test = q_element_id_test;
					q_element_id_test = s_element_id_test;
					s_element_id_test = tmp_q_element_id_test;
				}
				
				Long alignment_param_id_method = QueriesTableAlignmentParams.getAlignmentParamIdWithQandSElementIdAndParamForSynteny(
						conn
						, q_element_id_test
						, s_element_id_test
						//, false
						, false
						);
				if (alignment_param_id_method < 0) {
					alignment_param_id_method = -alignment_param_id_method;
				}
				
				if(alignment_param_id_test != alignment_param_id_method){
					fail("Different results where found for q_element_id_test=" + q_element_id_test
							+ " and s_element_id_test=" + s_element_id_test
							+ ": from test = " + alignment_param_id_test
							+ " ; from method  = " + alignment_param_id_method
							);
				}
				/*
				else {
					System.err.println("OK similar results where found for q_element_id_test=" + q_element_id_test
							+ " and s_element_id_test=" + s_element_id_test
							+ ": from test = " + alignment_param_id_test
							+ " ; from method  = " + alignment_param_id_method
							);
				}*/
				long milliPrint2 = System.currentTimeMillis() - milliPrint1;
				System.out.println("Method SQL query "+i+" took\t" + milliPrint2 + "\tmilliseconds for 2 test methods "+methodNameToReport);

			}
				
				
		} catch (Exception ex) {
			UtilitiesMethodsServer.reportException(methodNameToReport,ex);
		} finally {
			try {
				if(rs != null){rs.close();}
				rs = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			try {
				if(statement!=null){statement.close();}
				statement = null; // needed for connection pool, see apache tomcat-8.0-doc/jndi-datasource-examples-howto.html
			} catch (Exception ex) {
				UtilitiesMethodsServer.reportException(methodNameToReport, ex);
			}
			if (closeConn) {
				DatabaseConf.freeConnection(conn, methodNameToReport);
			}
		}
		assertTrue(true);
	}
	
	
}
